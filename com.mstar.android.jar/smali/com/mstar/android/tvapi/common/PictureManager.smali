.class public final Lcom/mstar/android/tvapi/common/PictureManager;
.super Ljava/lang/Object;
.source "PictureManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mstar/android/tvapi/common/PictureManager$1;,
        Lcom/mstar/android/tvapi/common/PictureManager$EventHandler;,
        Lcom/mstar/android/tvapi/common/PictureManager$EVENT;
    }
.end annotation


# static fields
.field private static _pictureManager:Lcom/mstar/android/tvapi/common/PictureManager;


# instance fields
.field private mEventHandler:Lcom/mstar/android/tvapi/common/PictureManager$EventHandler;

.field private mNativeContext:I

.field private mOnEventListener:Lcom/mstar/android/tvapi/common/listener/OnPictureEventListener;

.field private mPictureManagerContext:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    :try_start_0
    const-string v1, "picturemanager_jni"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    invoke-static {}, Lcom/mstar/android/tvapi/common/PictureManager;->native_init()V
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const/4 v1, 0x0

    sput-object v1, Lcom/mstar/android/tvapi/common/PictureManager;->_pictureManager:Lcom/mstar/android/tvapi/common/PictureManager;

    return-void

    :catch_0
    move-exception v0

    sget-object v1, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Cannot load picturemanager_jni library:\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/UnsatisfiedLinkError;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Lcom/mstar/android/tvapi/common/PictureManager$EventHandler;

    invoke-direct {v1, p0, p0, v0}, Lcom/mstar/android/tvapi/common/PictureManager$EventHandler;-><init>(Lcom/mstar/android/tvapi/common/PictureManager;Lcom/mstar/android/tvapi/common/PictureManager;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/mstar/android/tvapi/common/PictureManager;->mEventHandler:Lcom/mstar/android/tvapi/common/PictureManager$EventHandler;

    :goto_0
    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-direct {p0, v1}, Lcom/mstar/android/tvapi/common/PictureManager;->native_setup(Ljava/lang/Object;)V

    return-void

    :cond_0
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    if-eqz v0, :cond_1

    new-instance v1, Lcom/mstar/android/tvapi/common/PictureManager$EventHandler;

    invoke-direct {v1, p0, p0, v0}, Lcom/mstar/android/tvapi/common/PictureManager$EventHandler;-><init>(Lcom/mstar/android/tvapi/common/PictureManager;Lcom/mstar/android/tvapi/common/PictureManager;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/mstar/android/tvapi/common/PictureManager;->mEventHandler:Lcom/mstar/android/tvapi/common/PictureManager$EventHandler;

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/mstar/android/tvapi/common/PictureManager;->mEventHandler:Lcom/mstar/android/tvapi/common/PictureManager$EventHandler;

    goto :goto_0
.end method

.method private static PostEvent_4K2KPhotoDisableDualview(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/common/PictureManager;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, v1, Lcom/mstar/android/tvapi/common/PictureManager;->mEventHandler:Lcom/mstar/android/tvapi/common/PictureManager$EventHandler;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/PictureManager;->mEventHandler:Lcom/mstar/android/tvapi/common/PictureManager$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/common/PictureManager$EVENT;->EV_4K2K_PHOTO_DISABLE_DUALVIEW:Lcom/mstar/android/tvapi/common/PictureManager$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/PictureManager$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/common/PictureManager$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/PictureManager;->mEventHandler:Lcom/mstar/android/tvapi/common/PictureManager$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/common/PictureManager$EventHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private static PostEvent_4K2KPhotoDisablePip(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/common/PictureManager;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, v1, Lcom/mstar/android/tvapi/common/PictureManager;->mEventHandler:Lcom/mstar/android/tvapi/common/PictureManager$EventHandler;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/PictureManager;->mEventHandler:Lcom/mstar/android/tvapi/common/PictureManager$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/common/PictureManager$EVENT;->EV_4K2K_PHOTO_DISABLE_PIP:Lcom/mstar/android/tvapi/common/PictureManager$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/PictureManager$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/common/PictureManager$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/PictureManager;->mEventHandler:Lcom/mstar/android/tvapi/common/PictureManager$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/common/PictureManager$EventHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private static PostEvent_4K2KPhotoDisablePop(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/common/PictureManager;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, v1, Lcom/mstar/android/tvapi/common/PictureManager;->mEventHandler:Lcom/mstar/android/tvapi/common/PictureManager$EventHandler;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/PictureManager;->mEventHandler:Lcom/mstar/android/tvapi/common/PictureManager$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/common/PictureManager$EVENT;->EV_4K2K_PHOTO_DISABLE_POP:Lcom/mstar/android/tvapi/common/PictureManager$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/PictureManager$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/common/PictureManager$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/PictureManager;->mEventHandler:Lcom/mstar/android/tvapi/common/PictureManager$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/common/PictureManager$EventHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private static PostEvent_4K2KPhotoDisableTravelingmode(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/common/PictureManager;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, v1, Lcom/mstar/android/tvapi/common/PictureManager;->mEventHandler:Lcom/mstar/android/tvapi/common/PictureManager$EventHandler;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/PictureManager;->mEventHandler:Lcom/mstar/android/tvapi/common/PictureManager$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/common/PictureManager$EVENT;->EV_4K2K_PHOTO_DISABLE_TRAVELINGMODE:Lcom/mstar/android/tvapi/common/PictureManager$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/PictureManager$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/common/PictureManager$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/PictureManager;->mEventHandler:Lcom/mstar/android/tvapi/common/PictureManager$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/common/PictureManager$EventHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private static PostEvent_SetAspectratio(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/common/PictureManager;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, v1, Lcom/mstar/android/tvapi/common/PictureManager;->mEventHandler:Lcom/mstar/android/tvapi/common/PictureManager$EventHandler;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/PictureManager;->mEventHandler:Lcom/mstar/android/tvapi/common/PictureManager$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/common/PictureManager$EVENT;->EV_SET_ASPECTRATIO:Lcom/mstar/android/tvapi/common/PictureManager$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/PictureManager$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/common/PictureManager$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/PictureManager;->mEventHandler:Lcom/mstar/android/tvapi/common/PictureManager$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/common/PictureManager$EventHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private static PostEvent_SnServiceDeadth(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    :try_start_0
    const-class v2, Lcom/mstar/android/tvapi/common/PictureManager;

    monitor-enter v2
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    const-string v1, "mstar.str.suspending"

    const-string v3, "1"

    invoke-static {v1, v3}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x0

    sput-object v1, Lcom/mstar/android/tvapi/common/PictureManager;->_pictureManager:Lcom/mstar/android/tvapi/common/PictureManager;

    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "PostEvent_SnServiceDeadth: set SystemProperties \'mstar.str.suspending\' =1"

    invoke-virtual {v1, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    monitor-exit v2

    :goto_0
    return-void

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v1
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/mstar/android/tvapi/common/PictureManager;)I
    .locals 1
    .param p0    # Lcom/mstar/android/tvapi/common/PictureManager;

    iget v0, p0, Lcom/mstar/android/tvapi/common/PictureManager;->mNativeContext:I

    return v0
.end method

.method static synthetic access$100(Lcom/mstar/android/tvapi/common/PictureManager;)Lcom/mstar/android/tvapi/common/listener/OnPictureEventListener;
    .locals 1
    .param p0    # Lcom/mstar/android/tvapi/common/PictureManager;

    iget-object v0, p0, Lcom/mstar/android/tvapi/common/PictureManager;->mOnEventListener:Lcom/mstar/android/tvapi/common/listener/OnPictureEventListener;

    return-object v0
.end method

.method public static getInstance()Lcom/mstar/android/tvapi/common/PictureManager;
    .locals 2

    sget-object v0, Lcom/mstar/android/tvapi/common/PictureManager;->_pictureManager:Lcom/mstar/android/tvapi/common/PictureManager;

    if-nez v0, :cond_1

    const-class v1, Lcom/mstar/android/tvapi/common/PictureManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/mstar/android/tvapi/common/PictureManager;->_pictureManager:Lcom/mstar/android/tvapi/common/PictureManager;

    if-nez v0, :cond_0

    new-instance v0, Lcom/mstar/android/tvapi/common/PictureManager;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/common/PictureManager;-><init>()V

    sput-object v0, Lcom/mstar/android/tvapi/common/PictureManager;->_pictureManager:Lcom/mstar/android/tvapi/common/PictureManager;

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    sget-object v0, Lcom/mstar/android/tvapi/common/PictureManager;->_pictureManager:Lcom/mstar/android/tvapi/common/PictureManager;

    return-object v0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private native native_disableAllOsdWindow()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private native native_disableOsdWindow(I)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_finalize()V
.end method

.method private final native native_getDemoMode()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private native native_getPixelInfo(IIII)Lcom/mstar/android/tvapi/common/vo/ScreenPixelInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private native native_getPixelRgb(ISSI)Lcom/mstar/android/tvapi/common/vo/Rgb_Data;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_getReproduceRate()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_getResolution()B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private static final native native_init()V
.end method

.method private final native native_selectWindow(I)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_setAspectRatio(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_setDemoMode(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_setFilm(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private native native_setLocalDimmingMode(I)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_setMfc(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_setMpegNoiseReduction(I)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_setNoiseReduction(I)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private native native_setOsdWindow(IIIII)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_setPictureModeBrightness(II)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_setRGBBypass(Z)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_setReproduceRate(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_setResolution(B)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_setSRLevel(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_setup(Ljava/lang/Object;)V
.end method

.method private static postEventFromNative(Ljava/lang/Object;IIILjava/lang/Object;)V
    .locals 2
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # Ljava/lang/Object;

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "picturemanager callback  \n"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public native autoHDMIColorRange()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public native disableAllDualWinMode()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final disableAllOsdWindow()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/mstar/android/tvapi/common/PictureManager;->native_disableAllOsdWindow()Z

    move-result v0

    return v0
.end method

.method public native disableBacklight()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native disableDlc()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final disableOsdWindow(Lcom/mstar/android/tvapi/common/vo/EnumMfcOsdWindow;)Z
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumMfcOsdWindow;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumMfcOsdWindow;->ordinal()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/mstar/android/tvapi/common/PictureManager;->native_disableOsdWindow(I)Z

    move-result v0

    return v0
.end method

.method public final native disableOverScan()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public native enableBacklight()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native enableDlc()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native enableOverScan()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public native enter4K2KMode(Z)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method protected finalize()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    invoke-direct {p0}, Lcom/mstar/android/tvapi/common/PictureManager;->native_finalize()V

    const/4 v0, 0x0

    sput-object v0, Lcom/mstar/android/tvapi/common/PictureManager;->_pictureManager:Lcom/mstar/android/tvapi/common/PictureManager;

    return-void
.end method

.method public native forceFreerun(ZZ)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native freezeImage()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native getBacklight()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native getBacklightMaxValue()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native getBacklightMinValue()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public native getCustomerPqRuleNumber()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final getDemoMode()Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/mstar/android/tvapi/common/PictureManager;->native_getDemoMode()I

    move-result v0

    invoke-static {v0}, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;->getOrdinalThroughValue(I)I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;->values()[Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

    move-result-object v2

    aget-object v2, v2, v1

    return-object v2

    :cond_0
    new-instance v2, Lcom/mstar/android/tvapi/common/exception/TvCommonException;

    const-string v3, "get demomode error "

    invoke-direct {v2, v3}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public native getDlcAverageLuma()S
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public native getDlcHistogramMax()S
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public native getDlcHistogramMin()S
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public native getDlcLumArray(I)[I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public native getDlcLumAverageTemporary()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public native getDlcLumTotalCount()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native getDynamicContrastCurve()[I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native getPanelWidthHeight()Lcom/mstar/android/tvapi/common/vo/PanelProperty;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public getPixelInfo(IIII)Lcom/mstar/android/tvapi/common/vo/ScreenPixelInfo;
    .locals 2
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/mstar/android/tvapi/common/PictureManager;->native_getPixelInfo(IIII)Lcom/mstar/android/tvapi/common/vo/ScreenPixelInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    iget v1, v0, Lcom/mstar/android/tvapi/common/vo/ScreenPixelInfo;->tmpStage:I

    invoke-static {v1}, Lcom/mstar/android/tvapi/common/vo/ScreenPixelInfo$EnumPixelRGBStage;->valueOf(I)Lcom/mstar/android/tvapi/common/vo/ScreenPixelInfo$EnumPixelRGBStage;

    move-result-object v1

    iput-object v1, v0, Lcom/mstar/android/tvapi/common/vo/ScreenPixelInfo;->enStage:Lcom/mstar/android/tvapi/common/vo/ScreenPixelInfo$EnumPixelRGBStage;

    :cond_0
    return-object v0
.end method

.method public getPixelRgb(Lcom/mstar/android/tvapi/common/vo/GetPixelRgbStage$EnumGetPixelRgbStage;SSLcom/mstar/android/tvapi/common/vo/EnumScalerWindow;)Lcom/mstar/android/tvapi/common/vo/Rgb_Data;
    .locals 2
    .param p1    # Lcom/mstar/android/tvapi/common/vo/GetPixelRgbStage$EnumGetPixelRgbStage;
    .param p2    # S
    .param p3    # S
    .param p4    # Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/GetPixelRgbStage$EnumGetPixelRgbStage;->getValue()I

    move-result v0

    invoke-virtual {p4}, Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;->ordinal()I

    move-result v1

    invoke-direct {p0, v0, p2, p3, v1}, Lcom/mstar/android/tvapi/common/PictureManager;->native_getPixelRgb(ISSI)Lcom/mstar/android/tvapi/common/vo/Rgb_Data;

    move-result-object v0

    return-object v0
.end method

.method public final getReproduceRate()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/mstar/android/tvapi/common/PictureManager;->native_getReproduceRate()I

    move-result v0

    return v0
.end method

.method public final getResolution()B
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/mstar/android/tvapi/common/PictureManager;->native_getResolution()B

    move-result v0

    return v0
.end method

.method public native getStatusNumberByCustomerPqRule(I)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native isImageFreezed()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native isOverscanEnabled()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public native moveWindow()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method protected release()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    const/4 v0, 0x0

    sput-object v0, Lcom/mstar/android/tvapi/common/PictureManager;->_pictureManager:Lcom/mstar/android/tvapi/common/PictureManager;

    return-void
.end method

.method public final native scaleWindow()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public selectWindow(Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;)Z
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;->ordinal()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/mstar/android/tvapi/common/PictureManager;->native_selectWindow(I)Z

    move-result v0

    return v0
.end method

.method public final setAspectRatio(Lcom/mstar/android/tvapi/common/vo/EnumVideoArcType;)V
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumVideoArcType;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumVideoArcType;->ordinal()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/mstar/android/tvapi/common/PictureManager;->native_setAspectRatio(I)V

    return-void
.end method

.method public final native setBacklight(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public native setColorRange(Z)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native setColorTemperature(Lcom/mstar/android/tvapi/common/vo/ColorTemperature;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native setCropWindow(Lcom/mstar/android/tvapi/common/vo/VideoWindowType;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public native setDebugMode(Z)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public setDemoMode(Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;)V
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;->getValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/mstar/android/tvapi/common/PictureManager;->native_setDemoMode(I)V

    return-void
.end method

.method public final native setDisplayWindow(Lcom/mstar/android/tvapi/common/vo/VideoWindowType;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native setDynamicContrastCurve([I[I[I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public setFilm(Lcom/mstar/android/tvapi/common/vo/Film$EnumFilm;)V
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/vo/Film$EnumFilm;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/Film$EnumFilm;->getValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/mstar/android/tvapi/common/PictureManager;->native_setFilm(I)V

    return-void
.end method

.method public native setLocalDimmingBrightLevel(S)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public setLocalDimmingMode(Lcom/mstar/android/tvapi/common/vo/EnumLocalDimmingMode;)Z
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumLocalDimmingMode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumLocalDimmingMode;->ordinal()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/mstar/android/tvapi/common/PictureManager;->native_setLocalDimmingMode(I)Z

    move-result v0

    return v0
.end method

.method public final setMfc(Lcom/mstar/android/tvapi/common/vo/EnumMfcMode;)V
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumMfcMode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumMfcMode;->ordinal()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/mstar/android/tvapi/common/PictureManager;->native_setMfc(I)V

    return-void
.end method

.method public final setMpegNoiseReduction(Lcom/mstar/android/tvapi/common/vo/MpegNoiseReduction$EnumMpegNoiseReduction;)Z
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/vo/MpegNoiseReduction$EnumMpegNoiseReduction;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/MpegNoiseReduction$EnumMpegNoiseReduction;->getValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/mstar/android/tvapi/common/PictureManager;->native_setMpegNoiseReduction(I)Z

    move-result v0

    return v0
.end method

.method public final setNoiseReduction(Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;)Z
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;->getValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/mstar/android/tvapi/common/PictureManager;->native_setNoiseReduction(I)Z

    move-result v0

    return v0
.end method

.method public setOnPictureEventListener(Lcom/mstar/android/tvapi/common/listener/OnPictureEventListener;)V
    .locals 0
    .param p1    # Lcom/mstar/android/tvapi/common/listener/OnPictureEventListener;

    iput-object p1, p0, Lcom/mstar/android/tvapi/common/PictureManager;->mOnEventListener:Lcom/mstar/android/tvapi/common/listener/OnPictureEventListener;

    return-void
.end method

.method public final setOsdWindow(Lcom/mstar/android/tvapi/common/vo/EnumMfcOsdWindow;IIII)Z
    .locals 6
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumMfcOsdWindow;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumMfcOsdWindow;->ordinal()I

    move-result v1

    move-object v0, p0

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/mstar/android/tvapi/common/PictureManager;->native_setOsdWindow(IIIII)Z

    move-result v0

    return v0
.end method

.method public final native setOutputPattern(ZIII)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native setOverscan(IIII)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final setPictureModeBrightness(Lcom/mstar/android/tvapi/common/vo/SetLocationType$EnumSetLocationType;I)V
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/vo/SetLocationType$EnumSetLocationType;
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/SetLocationType$EnumSetLocationType;->getValue()I

    move-result v0

    invoke-direct {p0, v0, p2}, Lcom/mstar/android/tvapi/common/PictureManager;->native_setPictureModeBrightness(II)V

    return-void
.end method

.method public final native setPictureModeBrightness(S)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native setPictureModeColor(S)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native setPictureModeContrast(S)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native setPictureModeSharpness(S)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native setPictureModeTint(S)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final setRGBBypass(Z)V
    .locals 0
    .param p1    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/mstar/android/tvapi/common/PictureManager;->native_setRGBBypass(Z)V

    return-void
.end method

.method public setReproduceRate(I)V
    .locals 0
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/mstar/android/tvapi/common/PictureManager;->native_setReproduceRate(I)V

    return-void
.end method

.method public setResolution(B)V
    .locals 0
    .param p1    # B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/mstar/android/tvapi/common/PictureManager;->native_setResolution(B)V

    return-void
.end method

.method public final setSRLevel(I)V
    .locals 0
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/mstar/android/tvapi/common/PictureManager;->native_setSRLevel(I)V

    return-void
.end method

.method public native setStatusByCustomerPqRule(II)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public native setSwingLevel(S)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public native setTurnOffLocalDimmingBacklight(Z)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public native setUltraClear(Z)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native setWindowInvisible()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native setWindowVisible()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public native switchDlcCurve(S)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native unFreezeImage()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method
