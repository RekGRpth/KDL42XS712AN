.class public final Lcom/mstar/android/tvapi/common/CecManager;
.super Ljava/lang/Object;
.source "CecManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mstar/android/tvapi/common/CecManager$1;,
        Lcom/mstar/android/tvapi/common/CecManager$EventHandler;,
        Lcom/mstar/android/tvapi/common/CecManager$EVENT;
    }
.end annotation


# static fields
.field private static _cecManager:Lcom/mstar/android/tvapi/common/CecManager;


# instance fields
.field private mCecmanagerContext:I

.field private mEventHandler:Lcom/mstar/android/tvapi/common/CecManager$EventHandler;

.field private mNativeContext:I

.field private mOnEventListener:Lcom/mstar/android/tvapi/common/listener/OnCecEventListener;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v1, 0x0

    sput-object v1, Lcom/mstar/android/tvapi/common/CecManager;->_cecManager:Lcom/mstar/android/tvapi/common/CecManager;

    :try_start_0
    const-string v1, "cecmanager_jni"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    invoke-static {}, Lcom/mstar/android/tvapi/common/CecManager;->native_init()V
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    sget-object v1, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Cannot load cecmanager_jni library:\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/UnsatisfiedLinkError;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Lcom/mstar/android/tvapi/common/CecManager$EventHandler;

    invoke-direct {v1, p0, p0, v0}, Lcom/mstar/android/tvapi/common/CecManager$EventHandler;-><init>(Lcom/mstar/android/tvapi/common/CecManager;Lcom/mstar/android/tvapi/common/CecManager;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/mstar/android/tvapi/common/CecManager;->mEventHandler:Lcom/mstar/android/tvapi/common/CecManager$EventHandler;

    :goto_0
    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-direct {p0, v1}, Lcom/mstar/android/tvapi/common/CecManager;->native_setup(Ljava/lang/Object;)V

    return-void

    :cond_0
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    if-eqz v0, :cond_1

    new-instance v1, Lcom/mstar/android/tvapi/common/CecManager$EventHandler;

    invoke-direct {v1, p0, p0, v0}, Lcom/mstar/android/tvapi/common/CecManager$EventHandler;-><init>(Lcom/mstar/android/tvapi/common/CecManager;Lcom/mstar/android/tvapi/common/CecManager;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/mstar/android/tvapi/common/CecManager;->mEventHandler:Lcom/mstar/android/tvapi/common/CecManager$EventHandler;

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/mstar/android/tvapi/common/CecManager;->mEventHandler:Lcom/mstar/android/tvapi/common/CecManager$EventHandler;

    goto :goto_0
.end method

.method private static PostEvent_ImageViewOn(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/common/CecManager;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, v1, Lcom/mstar/android/tvapi/common/CecManager;->mEventHandler:Lcom/mstar/android/tvapi/common/CecManager$EventHandler;

    if-eqz v2, :cond_1

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/CecManager;->mEventHandler:Lcom/mstar/android/tvapi/common/CecManager$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/common/CecManager$EVENT;->EV_CEC_IMAGE_VIEW_ON:Lcom/mstar/android/tvapi/common/CecManager$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/CecManager$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/common/CecManager$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/CecManager;->mEventHandler:Lcom/mstar/android/tvapi/common/CecManager$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/common/CecManager$EventHandler;->sendMessage(Landroid/os/Message;)Z

    :cond_1
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "\n NativeCEC callback, PostEvent_ImageViewOn"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static PostEvent_SnServiceDeadth(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    :try_start_0
    const-class v2, Lcom/mstar/android/tvapi/common/CecManager;

    monitor-enter v2
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    const-string v1, "mstar.str.suspending"

    const-string v3, "1"

    invoke-static {v1, v3}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x0

    sput-object v1, Lcom/mstar/android/tvapi/common/CecManager;->_cecManager:Lcom/mstar/android/tvapi/common/CecManager;

    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "PostEvent_SnServiceDeadth: set SystemProperties \'mstar.str.suspending\' =1"

    invoke-virtual {v1, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    monitor-exit v2

    :goto_0
    return-void

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v1
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method private static PostEvent_TextViewOn(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/common/CecManager;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, v1, Lcom/mstar/android/tvapi/common/CecManager;->mEventHandler:Lcom/mstar/android/tvapi/common/CecManager$EventHandler;

    if-eqz v2, :cond_1

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/CecManager;->mEventHandler:Lcom/mstar/android/tvapi/common/CecManager$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/common/CecManager$EVENT;->EV_CEC_TEXT_VIEW_ON:Lcom/mstar/android/tvapi/common/CecManager$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/CecManager$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/common/CecManager$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/CecManager;->mEventHandler:Lcom/mstar/android/tvapi/common/CecManager$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/common/CecManager$EventHandler;->sendMessage(Landroid/os/Message;)Z

    :cond_1
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "\n NativeCEC callback, PostEvent_TextViewOn"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/mstar/android/tvapi/common/CecManager;)I
    .locals 1
    .param p0    # Lcom/mstar/android/tvapi/common/CecManager;

    iget v0, p0, Lcom/mstar/android/tvapi/common/CecManager;->mNativeContext:I

    return v0
.end method

.method static synthetic access$100(Lcom/mstar/android/tvapi/common/CecManager;)Lcom/mstar/android/tvapi/common/listener/OnCecEventListener;
    .locals 1
    .param p0    # Lcom/mstar/android/tvapi/common/CecManager;

    iget-object v0, p0, Lcom/mstar/android/tvapi/common/CecManager;->mOnEventListener:Lcom/mstar/android/tvapi/common/listener/OnCecEventListener;

    return-object v0
.end method

.method protected static getInstance()Lcom/mstar/android/tvapi/common/CecManager;
    .locals 2

    sget-object v0, Lcom/mstar/android/tvapi/common/CecManager;->_cecManager:Lcom/mstar/android/tvapi/common/CecManager;

    if-nez v0, :cond_1

    const-class v1, Lcom/mstar/android/tvapi/common/CecManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/mstar/android/tvapi/common/CecManager;->_cecManager:Lcom/mstar/android/tvapi/common/CecManager;

    if-nez v0, :cond_0

    new-instance v0, Lcom/mstar/android/tvapi/common/CecManager;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/common/CecManager;-><init>()V

    sput-object v0, Lcom/mstar/android/tvapi/common/CecManager;->_cecManager:Lcom/mstar/android/tvapi/common/CecManager;

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    sget-object v0, Lcom/mstar/android/tvapi/common/CecManager;->_cecManager:Lcom/mstar/android/tvapi/common/CecManager;

    return-object v0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private final native native_finalize()V
.end method

.method private static final native native_init()V
.end method

.method private final native native_setMenuLanguage(I)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_setStreamPath(I)V
.end method

.method private final native native_setup(Ljava/lang/Object;)V
.end method

.method private static postEventFromNative(Ljava/lang/Object;IIILjava/lang/Object;)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # Ljava/lang/Object;

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/common/CecManager;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, v1, Lcom/mstar/android/tvapi/common/CecManager;->mEventHandler:Lcom/mstar/android/tvapi/common/CecManager$EventHandler;

    if-eqz v2, :cond_1

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/CecManager;->mEventHandler:Lcom/mstar/android/tvapi/common/CecManager$EventHandler;

    invoke-virtual {v2, p1, p2, p3, p4}, Lcom/mstar/android/tvapi/common/CecManager$EventHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/CecManager;->mEventHandler:Lcom/mstar/android/tvapi/common/CecManager$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/common/CecManager$EventHandler;->sendMessage(Landroid/os/Message;)Z

    :cond_1
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "\n NativeCEC callback, postEventFromNative"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public final native cecRealIr(I)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native cecStandby(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native deviceListGetItemIndex(II)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native deviceListGetListStr(II)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native disableDeviceMenu()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native enableDeviceMenu()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method protected finalize()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    invoke-direct {p0}, Lcom/mstar/android/tvapi/common/CecManager;->native_finalize()V

    const/4 v0, 0x0

    sput-object v0, Lcom/mstar/android/tvapi/common/CecManager;->_cecManager:Lcom/mstar/android/tvapi/common/CecManager;

    return-void
.end method

.method public final native getCECListCnt(I)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native getCecConfiguration()Lcom/mstar/android/tvapi/common/vo/CecSetting;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native getDeviceName(I)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method protected release()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    const/4 v0, 0x0

    sput-object v0, Lcom/mstar/android/tvapi/common/CecManager;->_cecManager:Lcom/mstar/android/tvapi/common/CecManager;

    return-void
.end method

.method public final native sendCecKey(I)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native setCecConfiguration(Lcom/mstar/android/tvapi/common/vo/CecSetting;)V
.end method

.method public final native setDebugMode(Z)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public setMenuLanguage(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;)I
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/mstar/android/tvapi/common/CecManager;->native_setMenuLanguage(I)I

    move-result v0

    return v0
.end method

.method public setOnCecEventListener(Lcom/mstar/android/tvapi/common/listener/OnCecEventListener;)V
    .locals 0
    .param p1    # Lcom/mstar/android/tvapi/common/listener/OnCecEventListener;

    iput-object p1, p0, Lcom/mstar/android/tvapi/common/CecManager;->mOnEventListener:Lcom/mstar/android/tvapi/common/listener/OnCecEventListener;

    return-void
.end method

.method public setStreamPath(Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;)V
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;->ordinal()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/mstar/android/tvapi/common/CecManager;->native_setStreamPath(I)V

    return-void
.end method
