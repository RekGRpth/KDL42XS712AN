.class public final enum Lcom/mstar/android/tvapi/common/TvManager$EVENT;
.super Ljava/lang/Enum;
.source "TvManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mstar/android/tvapi/common/TvManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401c
    name = "EVENT"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/tvapi/common/TvManager$EVENT;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/tvapi/common/TvManager$EVENT;

.field public static final enum EV_4K2K_HDMI_DISABLE_DUALVIEW:Lcom/mstar/android/tvapi/common/TvManager$EVENT;

.field public static final enum EV_4K2K_HDMI_DISABLE_PIP:Lcom/mstar/android/tvapi/common/TvManager$EVENT;

.field public static final enum EV_4K2K_HDMI_DISABLE_POP:Lcom/mstar/android/tvapi/common/TvManager$EVENT;

.field public static final enum EV_4K2K_HDMI_DISABLE_TRAVELINGMODE:Lcom/mstar/android/tvapi/common/TvManager$EVENT;

.field public static final enum EV_ATSC_POPUP_DIALOG:Lcom/mstar/android/tvapi/common/TvManager$EVENT;

.field public static final enum EV_DEATH:Lcom/mstar/android/tvapi/common/TvManager$EVENT;

.field public static final enum EV_DTV_READY_POPUP_DIALOG:Lcom/mstar/android/tvapi/common/TvManager$EVENT;

.field public static final enum EV_MAX:Lcom/mstar/android/tvapi/common/TvManager$EVENT;

.field public static final enum EV_SCARTMUTE_OSD_MODE:Lcom/mstar/android/tvapi/common/TvManager$EVENT;

.field public static final enum EV_SCREEN_SAVER_MODE:Lcom/mstar/android/tvapi/common/TvManager$EVENT;

.field public static final enum EV_SIGNAL_LOCK:Lcom/mstar/android/tvapi/common/TvManager$EVENT;

.field public static final enum EV_SIGNAL_UNLOCK:Lcom/mstar/android/tvapi/common/TvManager$EVENT;

.field public static final enum EV_UNITY_EVENT:Lcom/mstar/android/tvapi/common/TvManager$EVENT;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/common/TvManager$EVENT;

    const-string v1, "EV_DTV_READY_POPUP_DIALOG"

    invoke-direct {v0, v1, v3}, Lcom/mstar/android/tvapi/common/TvManager$EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/TvManager$EVENT;->EV_DTV_READY_POPUP_DIALOG:Lcom/mstar/android/tvapi/common/TvManager$EVENT;

    new-instance v0, Lcom/mstar/android/tvapi/common/TvManager$EVENT;

    const-string v1, "EV_SCARTMUTE_OSD_MODE"

    invoke-direct {v0, v1, v4}, Lcom/mstar/android/tvapi/common/TvManager$EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/TvManager$EVENT;->EV_SCARTMUTE_OSD_MODE:Lcom/mstar/android/tvapi/common/TvManager$EVENT;

    new-instance v0, Lcom/mstar/android/tvapi/common/TvManager$EVENT;

    const-string v1, "EV_SIGNAL_UNLOCK"

    invoke-direct {v0, v1, v5}, Lcom/mstar/android/tvapi/common/TvManager$EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/TvManager$EVENT;->EV_SIGNAL_UNLOCK:Lcom/mstar/android/tvapi/common/TvManager$EVENT;

    new-instance v0, Lcom/mstar/android/tvapi/common/TvManager$EVENT;

    const-string v1, "EV_SIGNAL_LOCK"

    invoke-direct {v0, v1, v6}, Lcom/mstar/android/tvapi/common/TvManager$EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/TvManager$EVENT;->EV_SIGNAL_LOCK:Lcom/mstar/android/tvapi/common/TvManager$EVENT;

    new-instance v0, Lcom/mstar/android/tvapi/common/TvManager$EVENT;

    const-string v1, "EV_UNITY_EVENT"

    invoke-direct {v0, v1, v7}, Lcom/mstar/android/tvapi/common/TvManager$EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/TvManager$EVENT;->EV_UNITY_EVENT:Lcom/mstar/android/tvapi/common/TvManager$EVENT;

    new-instance v0, Lcom/mstar/android/tvapi/common/TvManager$EVENT;

    const-string v1, "EV_SCREEN_SAVER_MODE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/TvManager$EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/TvManager$EVENT;->EV_SCREEN_SAVER_MODE:Lcom/mstar/android/tvapi/common/TvManager$EVENT;

    new-instance v0, Lcom/mstar/android/tvapi/common/TvManager$EVENT;

    const-string v1, "EV_ATSC_POPUP_DIALOG"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/TvManager$EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/TvManager$EVENT;->EV_ATSC_POPUP_DIALOG:Lcom/mstar/android/tvapi/common/TvManager$EVENT;

    new-instance v0, Lcom/mstar/android/tvapi/common/TvManager$EVENT;

    const-string v1, "EV_DEATH"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/TvManager$EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/TvManager$EVENT;->EV_DEATH:Lcom/mstar/android/tvapi/common/TvManager$EVENT;

    new-instance v0, Lcom/mstar/android/tvapi/common/TvManager$EVENT;

    const-string v1, "EV_4K2K_HDMI_DISABLE_PIP"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/TvManager$EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/TvManager$EVENT;->EV_4K2K_HDMI_DISABLE_PIP:Lcom/mstar/android/tvapi/common/TvManager$EVENT;

    new-instance v0, Lcom/mstar/android/tvapi/common/TvManager$EVENT;

    const-string v1, "EV_4K2K_HDMI_DISABLE_POP"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/TvManager$EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/TvManager$EVENT;->EV_4K2K_HDMI_DISABLE_POP:Lcom/mstar/android/tvapi/common/TvManager$EVENT;

    new-instance v0, Lcom/mstar/android/tvapi/common/TvManager$EVENT;

    const-string v1, "EV_4K2K_HDMI_DISABLE_DUALVIEW"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/TvManager$EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/TvManager$EVENT;->EV_4K2K_HDMI_DISABLE_DUALVIEW:Lcom/mstar/android/tvapi/common/TvManager$EVENT;

    new-instance v0, Lcom/mstar/android/tvapi/common/TvManager$EVENT;

    const-string v1, "EV_4K2K_HDMI_DISABLE_TRAVELINGMODE"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/TvManager$EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/TvManager$EVENT;->EV_4K2K_HDMI_DISABLE_TRAVELINGMODE:Lcom/mstar/android/tvapi/common/TvManager$EVENT;

    new-instance v0, Lcom/mstar/android/tvapi/common/TvManager$EVENT;

    const-string v1, "EV_MAX"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/TvManager$EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/TvManager$EVENT;->EV_MAX:Lcom/mstar/android/tvapi/common/TvManager$EVENT;

    const/16 v0, 0xd

    new-array v0, v0, [Lcom/mstar/android/tvapi/common/TvManager$EVENT;

    sget-object v1, Lcom/mstar/android/tvapi/common/TvManager$EVENT;->EV_DTV_READY_POPUP_DIALOG:Lcom/mstar/android/tvapi/common/TvManager$EVENT;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mstar/android/tvapi/common/TvManager$EVENT;->EV_SCARTMUTE_OSD_MODE:Lcom/mstar/android/tvapi/common/TvManager$EVENT;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/android/tvapi/common/TvManager$EVENT;->EV_SIGNAL_UNLOCK:Lcom/mstar/android/tvapi/common/TvManager$EVENT;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mstar/android/tvapi/common/TvManager$EVENT;->EV_SIGNAL_LOCK:Lcom/mstar/android/tvapi/common/TvManager$EVENT;

    aput-object v1, v0, v6

    sget-object v1, Lcom/mstar/android/tvapi/common/TvManager$EVENT;->EV_UNITY_EVENT:Lcom/mstar/android/tvapi/common/TvManager$EVENT;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/mstar/android/tvapi/common/TvManager$EVENT;->EV_SCREEN_SAVER_MODE:Lcom/mstar/android/tvapi/common/TvManager$EVENT;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/mstar/android/tvapi/common/TvManager$EVENT;->EV_ATSC_POPUP_DIALOG:Lcom/mstar/android/tvapi/common/TvManager$EVENT;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/mstar/android/tvapi/common/TvManager$EVENT;->EV_DEATH:Lcom/mstar/android/tvapi/common/TvManager$EVENT;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/mstar/android/tvapi/common/TvManager$EVENT;->EV_4K2K_HDMI_DISABLE_PIP:Lcom/mstar/android/tvapi/common/TvManager$EVENT;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/mstar/android/tvapi/common/TvManager$EVENT;->EV_4K2K_HDMI_DISABLE_POP:Lcom/mstar/android/tvapi/common/TvManager$EVENT;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/mstar/android/tvapi/common/TvManager$EVENT;->EV_4K2K_HDMI_DISABLE_DUALVIEW:Lcom/mstar/android/tvapi/common/TvManager$EVENT;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/mstar/android/tvapi/common/TvManager$EVENT;->EV_4K2K_HDMI_DISABLE_TRAVELINGMODE:Lcom/mstar/android/tvapi/common/TvManager$EVENT;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/mstar/android/tvapi/common/TvManager$EVENT;->EV_MAX:Lcom/mstar/android/tvapi/common/TvManager$EVENT;

    aput-object v2, v0, v1

    sput-object v0, Lcom/mstar/android/tvapi/common/TvManager$EVENT;->$VALUES:[Lcom/mstar/android/tvapi/common/TvManager$EVENT;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/tvapi/common/TvManager$EVENT;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/tvapi/common/TvManager$EVENT;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/common/TvManager$EVENT;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/tvapi/common/TvManager$EVENT;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/common/TvManager$EVENT;->$VALUES:[Lcom/mstar/android/tvapi/common/TvManager$EVENT;

    invoke-virtual {v0}, [Lcom/mstar/android/tvapi/common/TvManager$EVENT;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/tvapi/common/TvManager$EVENT;

    return-object v0
.end method
