.class Lcom/mstar/android/tvapi/common/PvrManager$EventHandler;
.super Landroid/os/Handler;
.source "PvrManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mstar/android/tvapi/common/PvrManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EventHandler"
.end annotation


# instance fields
.field private mMSrv:Lcom/mstar/android/tvapi/common/PvrManager;

.field final synthetic this$0:Lcom/mstar/android/tvapi/common/PvrManager;


# direct methods
.method public constructor <init>(Lcom/mstar/android/tvapi/common/PvrManager;Lcom/mstar/android/tvapi/common/PvrManager;Landroid/os/Looper;)V
    .locals 0
    .param p2    # Lcom/mstar/android/tvapi/common/PvrManager;
    .param p3    # Landroid/os/Looper;

    iput-object p1, p0, Lcom/mstar/android/tvapi/common/PvrManager$EventHandler;->this$0:Lcom/mstar/android/tvapi/common/PvrManager;

    invoke-direct {p0, p3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object p2, p0, Lcom/mstar/android/tvapi/common/PvrManager$EventHandler;->mMSrv:Lcom/mstar/android/tvapi/common/PvrManager;

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1    # Landroid/os/Message;

    iget-object v1, p0, Lcom/mstar/android/tvapi/common/PvrManager$EventHandler;->mMSrv:Lcom/mstar/android/tvapi/common/PvrManager;

    # getter for: Lcom/mstar/android/tvapi/common/PvrManager;->mNativeContext:I
    invoke-static {v1}, Lcom/mstar/android/tvapi/common/PvrManager;->access$000(Lcom/mstar/android/tvapi/common/PvrManager;)I

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/PvrManager$EVENT;->values()[Lcom/mstar/android/tvapi/common/PvrManager$EVENT;

    move-result-object v0

    iget v1, p1, Landroid/os/Message;->what:I

    sget-object v2, Lcom/mstar/android/tvapi/common/PvrManager$EVENT;->EV_MAX:Lcom/mstar/android/tvapi/common/PvrManager$EVENT;

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/PvrManager$EVENT;->ordinal()I

    move-result v2

    if-gt v1, v2, :cond_2

    iget v1, p1, Landroid/os/Message;->what:I

    sget-object v2, Lcom/mstar/android/tvapi/common/PvrManager$EVENT;->EV_PVR_NOTIFY_USB_INSERTED:Lcom/mstar/android/tvapi/common/PvrManager$EVENT;

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/PvrManager$EVENT;->ordinal()I

    move-result v2

    if-ge v1, v2, :cond_3

    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Native post event out of bound:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Landroid/os/Message;->what:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    sget-object v1, Lcom/mstar/android/tvapi/common/PvrManager$1;->$SwitchMap$com$mstar$android$tvapi$common$PvrManager$EVENT:[I

    iget v2, p1, Landroid/os/Message;->what:I

    aget-object v2, v0, v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/PvrManager$EVENT;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    sget-object v1, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown message type "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Landroid/os/Message;->what:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_0
    iget-object v1, p0, Lcom/mstar/android/tvapi/common/PvrManager$EventHandler;->this$0:Lcom/mstar/android/tvapi/common/PvrManager;

    # getter for: Lcom/mstar/android/tvapi/common/PvrManager;->mOnEventListener:Lcom/mstar/android/tvapi/common/PvrManager$OnPvrEventListener;
    invoke-static {v1}, Lcom/mstar/android/tvapi/common/PvrManager;->access$100(Lcom/mstar/android/tvapi/common/PvrManager;)Lcom/mstar/android/tvapi/common/PvrManager$OnPvrEventListener;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mstar/android/tvapi/common/PvrManager$EventHandler;->this$0:Lcom/mstar/android/tvapi/common/PvrManager;

    # getter for: Lcom/mstar/android/tvapi/common/PvrManager;->mOnEventListener:Lcom/mstar/android/tvapi/common/PvrManager$OnPvrEventListener;
    invoke-static {v1}, Lcom/mstar/android/tvapi/common/PvrManager;->access$100(Lcom/mstar/android/tvapi/common/PvrManager;)Lcom/mstar/android/tvapi/common/PvrManager$OnPvrEventListener;

    move-result-object v1

    iget-object v2, p0, Lcom/mstar/android/tvapi/common/PvrManager$EventHandler;->mMSrv:Lcom/mstar/android/tvapi/common/PvrManager;

    iget v3, p1, Landroid/os/Message;->what:I

    iget v4, p1, Landroid/os/Message;->arg1:I

    iget v5, p1, Landroid/os/Message;->arg2:I

    invoke-interface {v1, v2, v3, v4, v5}, Lcom/mstar/android/tvapi/common/PvrManager$OnPvrEventListener;->onPvrNotifyUsbInserted(Lcom/mstar/android/tvapi/common/PvrManager;III)Z

    goto/16 :goto_0

    :pswitch_1
    iget-object v1, p0, Lcom/mstar/android/tvapi/common/PvrManager$EventHandler;->this$0:Lcom/mstar/android/tvapi/common/PvrManager;

    # getter for: Lcom/mstar/android/tvapi/common/PvrManager;->mOnEventListener:Lcom/mstar/android/tvapi/common/PvrManager$OnPvrEventListener;
    invoke-static {v1}, Lcom/mstar/android/tvapi/common/PvrManager;->access$100(Lcom/mstar/android/tvapi/common/PvrManager;)Lcom/mstar/android/tvapi/common/PvrManager$OnPvrEventListener;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mstar/android/tvapi/common/PvrManager$EventHandler;->this$0:Lcom/mstar/android/tvapi/common/PvrManager;

    # getter for: Lcom/mstar/android/tvapi/common/PvrManager;->mOnEventListener:Lcom/mstar/android/tvapi/common/PvrManager$OnPvrEventListener;
    invoke-static {v1}, Lcom/mstar/android/tvapi/common/PvrManager;->access$100(Lcom/mstar/android/tvapi/common/PvrManager;)Lcom/mstar/android/tvapi/common/PvrManager$OnPvrEventListener;

    move-result-object v1

    iget-object v2, p0, Lcom/mstar/android/tvapi/common/PvrManager$EventHandler;->mMSrv:Lcom/mstar/android/tvapi/common/PvrManager;

    iget v3, p1, Landroid/os/Message;->what:I

    iget v4, p1, Landroid/os/Message;->arg1:I

    iget v5, p1, Landroid/os/Message;->arg2:I

    invoke-interface {v1, v2, v3, v4, v5}, Lcom/mstar/android/tvapi/common/PvrManager$OnPvrEventListener;->onPvrNotifyUsbRemoved(Lcom/mstar/android/tvapi/common/PvrManager;III)Z

    goto/16 :goto_0

    :pswitch_2
    iget-object v1, p0, Lcom/mstar/android/tvapi/common/PvrManager$EventHandler;->this$0:Lcom/mstar/android/tvapi/common/PvrManager;

    # getter for: Lcom/mstar/android/tvapi/common/PvrManager;->mOnEventListener:Lcom/mstar/android/tvapi/common/PvrManager$OnPvrEventListener;
    invoke-static {v1}, Lcom/mstar/android/tvapi/common/PvrManager;->access$100(Lcom/mstar/android/tvapi/common/PvrManager;)Lcom/mstar/android/tvapi/common/PvrManager$OnPvrEventListener;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mstar/android/tvapi/common/PvrManager$EventHandler;->this$0:Lcom/mstar/android/tvapi/common/PvrManager;

    # getter for: Lcom/mstar/android/tvapi/common/PvrManager;->mOnEventListener:Lcom/mstar/android/tvapi/common/PvrManager$OnPvrEventListener;
    invoke-static {v1}, Lcom/mstar/android/tvapi/common/PvrManager;->access$100(Lcom/mstar/android/tvapi/common/PvrManager;)Lcom/mstar/android/tvapi/common/PvrManager$OnPvrEventListener;

    move-result-object v1

    iget-object v2, p0, Lcom/mstar/android/tvapi/common/PvrManager$EventHandler;->mMSrv:Lcom/mstar/android/tvapi/common/PvrManager;

    iget v3, p1, Landroid/os/Message;->what:I

    iget v4, p1, Landroid/os/Message;->arg1:I

    iget v5, p1, Landroid/os/Message;->arg2:I

    invoke-interface {v1, v2, v3, v4, v5}, Lcom/mstar/android/tvapi/common/PvrManager$OnPvrEventListener;->onPvrNotifyFormatFinished(Lcom/mstar/android/tvapi/common/PvrManager;III)Z

    goto/16 :goto_0

    :pswitch_3
    iget-object v1, p0, Lcom/mstar/android/tvapi/common/PvrManager$EventHandler;->this$0:Lcom/mstar/android/tvapi/common/PvrManager;

    # getter for: Lcom/mstar/android/tvapi/common/PvrManager;->mOnEventListener:Lcom/mstar/android/tvapi/common/PvrManager$OnPvrEventListener;
    invoke-static {v1}, Lcom/mstar/android/tvapi/common/PvrManager;->access$100(Lcom/mstar/android/tvapi/common/PvrManager;)Lcom/mstar/android/tvapi/common/PvrManager$OnPvrEventListener;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mstar/android/tvapi/common/PvrManager$EventHandler;->this$0:Lcom/mstar/android/tvapi/common/PvrManager;

    # getter for: Lcom/mstar/android/tvapi/common/PvrManager;->mOnEventListener:Lcom/mstar/android/tvapi/common/PvrManager$OnPvrEventListener;
    invoke-static {v1}, Lcom/mstar/android/tvapi/common/PvrManager;->access$100(Lcom/mstar/android/tvapi/common/PvrManager;)Lcom/mstar/android/tvapi/common/PvrManager$OnPvrEventListener;

    move-result-object v1

    iget-object v2, p0, Lcom/mstar/android/tvapi/common/PvrManager$EventHandler;->mMSrv:Lcom/mstar/android/tvapi/common/PvrManager;

    iget v3, p1, Landroid/os/Message;->what:I

    iget v4, p1, Landroid/os/Message;->arg1:I

    iget v5, p1, Landroid/os/Message;->arg2:I

    invoke-interface {v1, v2, v3, v4, v5}, Lcom/mstar/android/tvapi/common/PvrManager$OnPvrEventListener;->onPvrNotifyPlaybackStop(Lcom/mstar/android/tvapi/common/PvrManager;III)Z

    goto/16 :goto_0

    :pswitch_4
    iget-object v1, p0, Lcom/mstar/android/tvapi/common/PvrManager$EventHandler;->this$0:Lcom/mstar/android/tvapi/common/PvrManager;

    # getter for: Lcom/mstar/android/tvapi/common/PvrManager;->mOnEventListener:Lcom/mstar/android/tvapi/common/PvrManager$OnPvrEventListener;
    invoke-static {v1}, Lcom/mstar/android/tvapi/common/PvrManager;->access$100(Lcom/mstar/android/tvapi/common/PvrManager;)Lcom/mstar/android/tvapi/common/PvrManager$OnPvrEventListener;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mstar/android/tvapi/common/PvrManager$EventHandler;->this$0:Lcom/mstar/android/tvapi/common/PvrManager;

    # getter for: Lcom/mstar/android/tvapi/common/PvrManager;->mOnEventListener:Lcom/mstar/android/tvapi/common/PvrManager$OnPvrEventListener;
    invoke-static {v1}, Lcom/mstar/android/tvapi/common/PvrManager;->access$100(Lcom/mstar/android/tvapi/common/PvrManager;)Lcom/mstar/android/tvapi/common/PvrManager$OnPvrEventListener;

    move-result-object v1

    iget-object v2, p0, Lcom/mstar/android/tvapi/common/PvrManager$EventHandler;->mMSrv:Lcom/mstar/android/tvapi/common/PvrManager;

    iget v3, p1, Landroid/os/Message;->what:I

    iget v4, p1, Landroid/os/Message;->arg1:I

    iget v5, p1, Landroid/os/Message;->arg2:I

    invoke-interface {v1, v2, v3, v4, v5}, Lcom/mstar/android/tvapi/common/PvrManager$OnPvrEventListener;->onPvrNotifyPlaybackBegin(Lcom/mstar/android/tvapi/common/PvrManager;III)Z

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
