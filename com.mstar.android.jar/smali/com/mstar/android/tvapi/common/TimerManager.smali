.class public final Lcom/mstar/android/tvapi/common/TimerManager;
.super Ljava/lang/Object;
.source "TimerManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mstar/android/tvapi/common/TimerManager$1;,
        Lcom/mstar/android/tvapi/common/TimerManager$EventHandler;,
        Lcom/mstar/android/tvapi/common/TimerManager$OnTimerEventListener;,
        Lcom/mstar/android/tvapi/common/TimerManager$EVENT;
    }
.end annotation


# static fields
.field private static _timerManager:Lcom/mstar/android/tvapi/common/TimerManager;

.field private static mOnEventListener:Lcom/mstar/android/tvapi/common/TimerManager$OnTimerEventListener;


# instance fields
.field private mEventHandler:Lcom/mstar/android/tvapi/common/TimerManager$EventHandler;

.field private mNativeContext:I

.field private mTimerManagerContext:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v1, 0x0

    sput-object v1, Lcom/mstar/android/tvapi/common/TimerManager;->_timerManager:Lcom/mstar/android/tvapi/common/TimerManager;

    :try_start_0
    const-string v1, "timermanager_jni"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    invoke-static {}, Lcom/mstar/android/tvapi/common/TimerManager;->native_init()V
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    sget-object v1, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Cannot load timermanager_jni library:\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/UnsatisfiedLinkError;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Lcom/mstar/android/tvapi/common/TimerManager$EventHandler;

    invoke-direct {v1, p0, p0, v0}, Lcom/mstar/android/tvapi/common/TimerManager$EventHandler;-><init>(Lcom/mstar/android/tvapi/common/TimerManager;Lcom/mstar/android/tvapi/common/TimerManager;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/mstar/android/tvapi/common/TimerManager;->mEventHandler:Lcom/mstar/android/tvapi/common/TimerManager$EventHandler;

    :goto_0
    const-string v2, ""

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "looper is null "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-nez v0, :cond_2

    const/4 v1, 0x1

    :goto_1
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-direct {p0, v1}, Lcom/mstar/android/tvapi/common/TimerManager;->native_setup(Ljava/lang/Object;)V

    return-void

    :cond_0
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    if-eqz v0, :cond_1

    new-instance v1, Lcom/mstar/android/tvapi/common/TimerManager$EventHandler;

    invoke-direct {v1, p0, p0, v0}, Lcom/mstar/android/tvapi/common/TimerManager$EventHandler;-><init>(Lcom/mstar/android/tvapi/common/TimerManager;Lcom/mstar/android/tvapi/common/TimerManager;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/mstar/android/tvapi/common/TimerManager;->mEventHandler:Lcom/mstar/android/tvapi/common/TimerManager$EventHandler;

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/mstar/android/tvapi/common/TimerManager;->mEventHandler:Lcom/mstar/android/tvapi/common/TimerManager$EventHandler;

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private static PostEvent_DestroyCountDown(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/common/TimerManager;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, v1, Lcom/mstar/android/tvapi/common/TimerManager;->mEventHandler:Lcom/mstar/android/tvapi/common/TimerManager$EventHandler;

    if-eqz v2, :cond_1

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/TimerManager;->mEventHandler:Lcom/mstar/android/tvapi/common/TimerManager$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/common/TimerManager$EVENT;->EV_DESTROY_COUNTDOWN:Lcom/mstar/android/tvapi/common/TimerManager$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TimerManager$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/common/TimerManager$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/TimerManager;->mEventHandler:Lcom/mstar/android/tvapi/common/TimerManager$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/common/TimerManager$EventHandler;->sendMessage(Landroid/os/Message;)Z

    :cond_1
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "\n Native Timer callback, PostEvent_DestroyCountDown"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static PostEvent_EpgTimeUp(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/common/TimerManager;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, v1, Lcom/mstar/android/tvapi/common/TimerManager;->mEventHandler:Lcom/mstar/android/tvapi/common/TimerManager$EventHandler;

    if-eqz v2, :cond_1

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/TimerManager;->mEventHandler:Lcom/mstar/android/tvapi/common/TimerManager$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/common/TimerManager$EVENT;->EV_EPG_TIME_UP:Lcom/mstar/android/tvapi/common/TimerManager$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TimerManager$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/common/TimerManager$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/TimerManager;->mEventHandler:Lcom/mstar/android/tvapi/common/TimerManager$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/common/TimerManager$EventHandler;->sendMessage(Landroid/os/Message;)Z

    :cond_1
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "\n Native Timer callback, PostEvent_EpgTimeUp"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static PostEvent_EpgTimerCountDown(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/common/TimerManager;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, v1, Lcom/mstar/android/tvapi/common/TimerManager;->mEventHandler:Lcom/mstar/android/tvapi/common/TimerManager$EventHandler;

    if-eqz v2, :cond_1

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/TimerManager;->mEventHandler:Lcom/mstar/android/tvapi/common/TimerManager$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/common/TimerManager$EVENT;->EV_EPGTIMER_COUNTDOWN:Lcom/mstar/android/tvapi/common/TimerManager$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TimerManager$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/common/TimerManager$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/TimerManager;->mEventHandler:Lcom/mstar/android/tvapi/common/TimerManager$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/common/TimerManager$EventHandler;->sendMessage(Landroid/os/Message;)Z

    :cond_1
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "\n Native Timer callback, PostEvent_EpgTimerCountDown"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static PostEvent_EpgTimerRecordStart(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/common/TimerManager;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, v1, Lcom/mstar/android/tvapi/common/TimerManager;->mEventHandler:Lcom/mstar/android/tvapi/common/TimerManager$EventHandler;

    if-eqz v2, :cond_1

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/TimerManager;->mEventHandler:Lcom/mstar/android/tvapi/common/TimerManager$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/common/TimerManager$EVENT;->EV_EPGTIMER_RECORD_START:Lcom/mstar/android/tvapi/common/TimerManager$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TimerManager$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/common/TimerManager$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/TimerManager;->mEventHandler:Lcom/mstar/android/tvapi/common/TimerManager$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/common/TimerManager$EventHandler;->sendMessage(Landroid/os/Message;)Z

    :cond_1
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "\n Native Timer callback, PostEvent_EpgTimerRecordStart"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static PostEvent_LastMinuteWarn(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/common/TimerManager;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, v1, Lcom/mstar/android/tvapi/common/TimerManager;->mEventHandler:Lcom/mstar/android/tvapi/common/TimerManager$EventHandler;

    if-eqz v2, :cond_1

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/TimerManager;->mEventHandler:Lcom/mstar/android/tvapi/common/TimerManager$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/common/TimerManager$EVENT;->EV_LASTMINUTE_WARN:Lcom/mstar/android/tvapi/common/TimerManager$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TimerManager$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/common/TimerManager$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/TimerManager;->mEventHandler:Lcom/mstar/android/tvapi/common/TimerManager$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/common/TimerManager$EventHandler;->sendMessage(Landroid/os/Message;)Z

    :cond_1
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "\n Native Timer callback, PostEvent_LastMinuteWarn"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static PostEvent_OadTimeScan(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/common/TimerManager;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, v1, Lcom/mstar/android/tvapi/common/TimerManager;->mEventHandler:Lcom/mstar/android/tvapi/common/TimerManager$EventHandler;

    if-eqz v2, :cond_1

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/TimerManager;->mEventHandler:Lcom/mstar/android/tvapi/common/TimerManager$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/common/TimerManager$EVENT;->EV_OAD_TIMESCAN:Lcom/mstar/android/tvapi/common/TimerManager$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TimerManager$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/common/TimerManager$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/TimerManager;->mEventHandler:Lcom/mstar/android/tvapi/common/TimerManager$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/common/TimerManager$EventHandler;->sendMessage(Landroid/os/Message;)Z

    :cond_1
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "\n Native Timer callback, PostEvent_OadTimeScan"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static PostEvent_OneSecondBeat(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/common/TimerManager;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, v1, Lcom/mstar/android/tvapi/common/TimerManager;->mEventHandler:Lcom/mstar/android/tvapi/common/TimerManager$EventHandler;

    if-eqz v2, :cond_1

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/TimerManager;->mEventHandler:Lcom/mstar/android/tvapi/common/TimerManager$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/common/TimerManager$EVENT;->EV_ONESECOND_BEAT:Lcom/mstar/android/tvapi/common/TimerManager$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TimerManager$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/common/TimerManager$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/TimerManager;->mEventHandler:Lcom/mstar/android/tvapi/common/TimerManager$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/common/TimerManager$EventHandler;->sendMessage(Landroid/os/Message;)Z

    :cond_1
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "\n Native Timer callback, PostEvent_OneSecondBeat"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static PostEvent_Power(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/common/TimerManager;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, v1, Lcom/mstar/android/tvapi/common/TimerManager;->mEventHandler:Lcom/mstar/android/tvapi/common/TimerManager$EventHandler;

    if-eqz v2, :cond_1

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/TimerManager;->mEventHandler:Lcom/mstar/android/tvapi/common/TimerManager$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/common/TimerManager$EVENT;->EV_TIMER_POWOER_EVENT:Lcom/mstar/android/tvapi/common/TimerManager$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TimerManager$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/common/TimerManager$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/TimerManager;->mEventHandler:Lcom/mstar/android/tvapi/common/TimerManager$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/common/TimerManager$EventHandler;->sendMessage(Landroid/os/Message;)Z

    :cond_1
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "\n Native Timer callback, PostEvent_Power"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static PostEvent_PvrNotifyRecordStop(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/common/TimerManager;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, v1, Lcom/mstar/android/tvapi/common/TimerManager;->mEventHandler:Lcom/mstar/android/tvapi/common/TimerManager$EventHandler;

    if-eqz v2, :cond_1

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/TimerManager;->mEventHandler:Lcom/mstar/android/tvapi/common/TimerManager$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/common/TimerManager$EVENT;->EV_PVR_NOTIFY_RECORD_STOP:Lcom/mstar/android/tvapi/common/TimerManager$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TimerManager$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/common/TimerManager$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/TimerManager;->mEventHandler:Lcom/mstar/android/tvapi/common/TimerManager$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/common/TimerManager$EventHandler;->sendMessage(Landroid/os/Message;)Z

    :cond_1
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "\n Native Timer callback, PostEvent_PvrNotifyRecordStop"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static PostEvent_SignalLock(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/common/TimerManager;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, v1, Lcom/mstar/android/tvapi/common/TimerManager;->mEventHandler:Lcom/mstar/android/tvapi/common/TimerManager$EventHandler;

    if-eqz v2, :cond_1

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/TimerManager;->mEventHandler:Lcom/mstar/android/tvapi/common/TimerManager$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/common/TimerManager$EVENT;->EV_SIGNAL_LOCK:Lcom/mstar/android/tvapi/common/TimerManager$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TimerManager$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/common/TimerManager$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/TimerManager;->mEventHandler:Lcom/mstar/android/tvapi/common/TimerManager$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/common/TimerManager$EventHandler;->sendMessage(Landroid/os/Message;)Z

    :cond_1
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "\n Native Timer callback, PostEvent_SignalLock"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static PostEvent_SnServiceDeadth(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    :try_start_0
    const-class v2, Lcom/mstar/android/tvapi/common/TimerManager;

    monitor-enter v2
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    const-string v1, "mstar.str.suspending"

    const-string v3, "1"

    invoke-static {v1, v3}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x0

    sput-object v1, Lcom/mstar/android/tvapi/common/TimerManager;->_timerManager:Lcom/mstar/android/tvapi/common/TimerManager;

    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "PostEvent_SnServiceDeadth: set SystemProperties \'mstar.str.suspending\' =1"

    invoke-virtual {v1, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    monitor-exit v2

    :goto_0
    return-void

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v1
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method private static PostEvent_SystemClkChg(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/common/TimerManager;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, v1, Lcom/mstar/android/tvapi/common/TimerManager;->mEventHandler:Lcom/mstar/android/tvapi/common/TimerManager$EventHandler;

    if-eqz v2, :cond_1

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/TimerManager;->mEventHandler:Lcom/mstar/android/tvapi/common/TimerManager$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/common/TimerManager$EVENT;->EV_TIMER_SYSTEMCLKCHG_EVENT:Lcom/mstar/android/tvapi/common/TimerManager$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TimerManager$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/common/TimerManager$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/TimerManager;->mEventHandler:Lcom/mstar/android/tvapi/common/TimerManager$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/common/TimerManager$EventHandler;->sendMessage(Landroid/os/Message;)Z

    :cond_1
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "\n Native Timer callback, PostEvent_SystemClkChg"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static PostEvent_UpdateLastMinute(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/common/TimerManager;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, v1, Lcom/mstar/android/tvapi/common/TimerManager;->mEventHandler:Lcom/mstar/android/tvapi/common/TimerManager$EventHandler;

    if-eqz v2, :cond_1

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/TimerManager;->mEventHandler:Lcom/mstar/android/tvapi/common/TimerManager$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/common/TimerManager$EVENT;->EV_UPDATE_LASTMINUTE:Lcom/mstar/android/tvapi/common/TimerManager$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TimerManager$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/common/TimerManager$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/TimerManager;->mEventHandler:Lcom/mstar/android/tvapi/common/TimerManager$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/common/TimerManager$EventHandler;->sendMessage(Landroid/os/Message;)Z

    :cond_1
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "\n Native Timer callback, PostEvent_UpdateLastMinute"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/mstar/android/tvapi/common/TimerManager;)I
    .locals 1
    .param p0    # Lcom/mstar/android/tvapi/common/TimerManager;

    iget v0, p0, Lcom/mstar/android/tvapi/common/TimerManager;->mNativeContext:I

    return v0
.end method

.method static synthetic access$100()Lcom/mstar/android/tvapi/common/TimerManager$OnTimerEventListener;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/common/TimerManager;->mOnEventListener:Lcom/mstar/android/tvapi/common/TimerManager$OnTimerEventListener;

    return-object v0
.end method

.method protected static getInstance()Lcom/mstar/android/tvapi/common/TimerManager;
    .locals 2

    sget-object v0, Lcom/mstar/android/tvapi/common/TimerManager;->_timerManager:Lcom/mstar/android/tvapi/common/TimerManager;

    if-nez v0, :cond_1

    const-class v1, Lcom/mstar/android/tvapi/common/TimerManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/mstar/android/tvapi/common/TimerManager;->_timerManager:Lcom/mstar/android/tvapi/common/TimerManager;

    if-nez v0, :cond_0

    new-instance v0, Lcom/mstar/android/tvapi/common/TimerManager;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/common/TimerManager;-><init>()V

    sput-object v0, Lcom/mstar/android/tvapi/common/TimerManager;->_timerManager:Lcom/mstar/android/tvapi/common/TimerManager;

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    sget-object v0, Lcom/mstar/android/tvapi/common/TimerManager;->_timerManager:Lcom/mstar/android/tvapi/common/TimerManager;

    return-object v0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private final native native_addEpgEvent(Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private native native_disablePowerOffMode(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_finalize()V
.end method

.method private final native native_getSleeperState()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_getTimeZone()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private static final native native_init()V
.end method

.method private final native native_isEpgTimerSettingValid(Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_setOnTime(Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;ZZI)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_setSleepModeTime(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_setTimeZone(IZ)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_setup(Ljava/lang/Object;)V
.end method

.method private static postEventFromNative(Ljava/lang/Object;IIILjava/lang/Object;)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # Ljava/lang/Object;

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/common/TimerManager;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, v1, Lcom/mstar/android/tvapi/common/TimerManager;->mEventHandler:Lcom/mstar/android/tvapi/common/TimerManager$EventHandler;

    if-eqz v2, :cond_1

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/TimerManager;->mEventHandler:Lcom/mstar/android/tvapi/common/TimerManager$EventHandler;

    invoke-virtual {v2, p1, p2, p3, p4}, Lcom/mstar/android/tvapi/common/TimerManager$EventHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/TimerManager;->mEventHandler:Lcom/mstar/android/tvapi/common/TimerManager$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/common/TimerManager$EventHandler;->sendMessage(Landroid/os/Message;)Z

    :cond_1
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "\n Native Timer callback, postEventFromNative"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public addEpgEvent(Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;)Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;
    .locals 3
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/mstar/android/tvapi/common/TimerManager;->native_addEpgEvent(Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;)I

    move-result v0

    sget-object v1, Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;->E_NONE:Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;->ordinal()I

    move-result v1

    if-lt v0, v1, :cond_0

    sget-object v1, Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;->E_FULL:Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;->ordinal()I

    move-result v1

    if-le v0, v1, :cond_1

    :cond_0
    new-instance v1, Lcom/mstar/android/tvapi/common/exception/TvCommonException;

    const-string v2, "native_addEpgEvent failed"

    invoke-direct {v1, v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    invoke-static {}, Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;->values()[Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;

    move-result-object v1

    aget-object v1, v1, v0

    return-object v1
.end method

.method public final native cancelEpgTimerEvent(IZ)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native convertSeconds2StTime(I)Landroid/text/format/Time;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native convertStTime2Seconds(Landroid/text/format/Time;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native delAllEpgEvent()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native delEpgEvent(I)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native deletePastEpgTimer()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final disablePowerOffMode(Lcom/mstar/android/tvapi/common/vo/EnumOffTimerMode;)V
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumOffTimerMode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumOffTimerMode;->ordinal()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/mstar/android/tvapi/common/TimerManager;->native_disablePowerOffMode(I)V

    return-void
.end method

.method public final native execEpgTimerAction()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method protected finalize()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    invoke-direct {p0}, Lcom/mstar/android/tvapi/common/TimerManager;->native_finalize()V

    const/4 v0, 0x0

    sput-object v0, Lcom/mstar/android/tvapi/common/TimerManager;->_timerManager:Lcom/mstar/android/tvapi/common/TimerManager;

    return-void
.end method

.method public final native getClkTime()Landroid/text/format/Time;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public native getClockOffset()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native getEpgTimerEventByIndex(I)Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native getEpgTimerEventCount()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native getEpgTimerRecordingProgram()Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native getNextNDayClkTimeInSeconds(S)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native getOffModeStatus()Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native getOnTime()Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public native getRtcClock()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native getSleepModeTime()S
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public getSleeperState()Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/mstar/android/tvapi/common/TimerManager;->native_getSleeperState()I

    move-result v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;->E_OFF:Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;->ordinal()I

    move-result v1

    if-lt v0, v1, :cond_0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;->E_TOTAL:Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;->ordinal()I

    move-result v1

    if-le v0, v1, :cond_1

    :cond_0
    new-instance v1, Lcom/mstar/android/tvapi/common/exception/TvCommonException;

    const-string v2, "getSleeperState failed"

    invoke-direct {v1, v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;->values()[Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;

    move-result-object v1

    aget-object v1, v1, v0

    return-object v1
.end method

.method public final native getStClkTime()Landroid/text/format/Time;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native getStOnTime()Landroid/text/format/Time;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final getTimeZone()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/mstar/android/tvapi/common/TimerManager;->native_getTimeZone()I

    move-result v1

    invoke-static {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->getOrdinalThroughValue(I)I

    move-result v0

    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;

    move-result-object v2

    aget-object v2, v2, v0

    return-object v2

    :cond_0
    new-instance v2, Lcom/mstar/android/tvapi/common/exception/TvCommonException;

    const-string v3, "gettimezone  error"

    invoke-direct {v2, v3}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public final native isEpgScheduleRecordRemiderExist(I)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public isEpgTimerSettingValid(Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;)Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;
    .locals 3
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/mstar/android/tvapi/common/TimerManager;->native_isEpgTimerSettingValid(Lcom/mstar/android/tvapi/common/vo/EpgEventTimerInfo;)I

    move-result v0

    sget-object v1, Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;->E_NONE:Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;->ordinal()I

    move-result v1

    if-lt v0, v1, :cond_0

    sget-object v1, Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;->E_FULL:Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;->ordinal()I

    move-result v1

    if-le v0, v1, :cond_1

    :cond_0
    new-instance v1, Lcom/mstar/android/tvapi/common/exception/TvCommonException;

    const-string v2, "isEpgTimerSettingValid failed"

    invoke-direct {v1, v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    invoke-static {}, Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;->values()[Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;

    move-result-object v1

    aget-object v1, v1, v0

    return-object v1
.end method

.method public final native isTimeFormat12HRs()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method protected release()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    const/4 v0, 0x0

    sput-object v0, Lcom/mstar/android/tvapi/common/TimerManager;->_timerManager:Lcom/mstar/android/tvapi/common/TimerManager;

    return-void
.end method

.method public final native setClkTime(Landroid/text/format/Time;Z)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native setDebugMode(Z)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native setOffModeStatus(Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;Z)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final setOnTime(Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;ZZLcom/mstar/android/tvapi/common/vo/EnumEpgTimerActType;)V
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;
    .param p2    # Z
    .param p3    # Z
    .param p4    # Lcom/mstar/android/tvapi/common/vo/EnumEpgTimerActType;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p4}, Lcom/mstar/android/tvapi/common/vo/EnumEpgTimerActType;->ordinal()I

    move-result v0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/mstar/android/tvapi/common/TimerManager;->native_setOnTime(Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;ZZI)V

    return-void
.end method

.method public setOnTimerEventListener(Lcom/mstar/android/tvapi/common/TimerManager$OnTimerEventListener;)V
    .locals 0
    .param p1    # Lcom/mstar/android/tvapi/common/TimerManager$OnTimerEventListener;

    sput-object p1, Lcom/mstar/android/tvapi/common/TimerManager;->mOnEventListener:Lcom/mstar/android/tvapi/common/TimerManager$OnTimerEventListener;

    return-void
.end method

.method public setSleepModeTime(Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;)V
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;->ordinal()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/mstar/android/tvapi/common/TimerManager;->native_setSleepModeTime(I)V

    return-void
.end method

.method public native setSleepTime(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native setTimeFormat12HRs()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native setTimeFormat24HRs()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final setTimeZone(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;Z)V
    .locals 2
    .param p1    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;
    .param p2    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->getValue()I

    move-result v0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/mstar/android/tvapi/common/TimerManager;->native_setTimeZone(IZ)V

    return-void
.end method
