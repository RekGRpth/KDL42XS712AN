.class public Lcom/mstar/android/tvapi/common/exception/TvIpcException;
.super Lcom/mstar/android/tvapi/common/exception/TvCommonException;
.source "TvIpcException.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    const-string v0, "Exception happened in native call!! "

    invoke-direct {p0, v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/Exception;

    invoke-direct {p0, p1, p2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method
