.class public interface abstract Lcom/mstar/android/tvapi/common/exception/TvExceptionConstant;
.super Ljava/lang/Object;
.source "TvExceptionConstant.java"


# static fields
.field public static final EXCEPTION_MSG_COMMON:Ljava/lang/String; = "Exception happened "

.field public static final EXCEPTION_MSG_IPC_FAIL:Ljava/lang/String; = "Exception happened in ipc!! "

.field public static final EXCEPTION_MSG_JNI_FAIL:Ljava/lang/String; = "Exception happened in jni!! "

.field public static final EXCEPTION_MSG_NATIVE_CALL_FAIL:Ljava/lang/String; = "Exception happened in native call!! "

.field public static final EXCEPTION_MSG_OUT_OF_BOUND:Ljava/lang/String; = "Exception happened in bound"

.field public static final EXCEPTION_TYPE_IPC_FAIL:S = 0x2s

.field public static final EXCEPTION_TYPE_JNI_FAIL:S = 0x3s

.field public static final EXCEPTION_TYPE_NATIVE_CALL_FAIL:S = 0x1s

.field public static final EXCEPTION_TYPE_NATIVE_COMMON:S
