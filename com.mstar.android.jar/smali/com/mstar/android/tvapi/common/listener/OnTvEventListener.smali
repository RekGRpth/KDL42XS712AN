.class public interface abstract Lcom/mstar/android/tvapi/common/listener/OnTvEventListener;
.super Ljava/lang/Object;
.source "OnTvEventListener.java"


# virtual methods
.method public abstract on4k2kHDMIDisableDualView(III)Z
.end method

.method public abstract on4k2kHDMIDisablePip(III)Z
.end method

.method public abstract on4k2kHDMIDisablePop(III)Z
.end method

.method public abstract on4k2kHDMIDisableTravelingMode(III)Z
.end method

.method public abstract onAtscPopupDialog(III)Z
.end method

.method public abstract onDeadthEvent(III)Z
.end method

.method public abstract onDtvReadyPopupDialog(III)Z
.end method

.method public abstract onScartMuteOsdMode(I)Z
.end method

.method public abstract onScreenSaverMode(III)Z
.end method

.method public abstract onSignalLock(I)Z
.end method

.method public abstract onSignalUnlock(I)Z
.end method

.method public abstract onUnityEvent(III)Z
.end method
