.class public interface abstract Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;
.super Ljava/lang/Object;
.source "OnTvPlayerEventListener.java"


# virtual methods
.method public abstract on4k2kHDMIDisableDualView(III)Z
.end method

.method public abstract on4k2kHDMIDisablePip(III)Z
.end method

.method public abstract on4k2kHDMIDisablePop(III)Z
.end method

.method public abstract on4k2kHDMIDisableTravelingMode(III)Z
.end method

.method public abstract onEpgUpdateList(II)Z
.end method

.method public abstract onHbbtvUiEvent(ILcom/mstar/android/tvapi/common/vo/HbbtvEventInfo;)Z
.end method

.method public abstract onPopupDialog(III)Z
.end method

.method public abstract onPvrNotifyAlwaysTimeShiftProgramNotReady(I)Z
.end method

.method public abstract onPvrNotifyAlwaysTimeShiftProgramReady(I)Z
.end method

.method public abstract onPvrNotifyCiPlusProtection(I)Z
.end method

.method public abstract onPvrNotifyCiPlusRetentionLimitUpdate(II)Z
.end method

.method public abstract onPvrNotifyOverRun(I)Z
.end method

.method public abstract onPvrNotifyParentalControl(II)Z
.end method

.method public abstract onPvrNotifyPlaybackBegin(I)Z
.end method

.method public abstract onPvrNotifyPlaybackSpeedChange(I)Z
.end method

.method public abstract onPvrNotifyPlaybackStop(I)Z
.end method

.method public abstract onPvrNotifyPlaybackTime(II)Z
.end method

.method public abstract onPvrNotifyRecordSize(II)Z
.end method

.method public abstract onPvrNotifyRecordStop(I)Z
.end method

.method public abstract onPvrNotifyRecordTime(II)Z
.end method

.method public abstract onPvrNotifyTimeShiftOverwritesAfter(II)Z
.end method

.method public abstract onPvrNotifyTimeShiftOverwritesBefore(II)Z
.end method

.method public abstract onPvrNotifyUsbRemoved(II)Z
.end method

.method public abstract onScreenSaverMode(II)Z
.end method

.method public abstract onSignalLock(I)Z
.end method

.method public abstract onSignalUnLock(I)Z
.end method

.method public abstract onTvProgramInfoReady(I)Z
.end method
