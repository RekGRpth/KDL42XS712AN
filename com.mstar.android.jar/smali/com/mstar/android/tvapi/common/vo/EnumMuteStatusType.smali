.class public final enum Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;
.super Ljava/lang/Enum;
.source "EnumMuteStatusType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

.field public static final enum MUTE_STATUS_BBYBLOCKAUDIOMUTE:Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

.field public static final enum MUTE_STATUS_BBYDURINGLIMITEDTIMEAUDIOMUTE:Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

.field public static final enum MUTE_STATUS_BBYSCANINOUTCHGCHCHG:Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

.field public static final enum MUTE_STATUS_BBYSYNCAUDIOMUTE:Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

.field public static final enum MUTE_STATUS_BBYUSERAUDIOMUTE:Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

.field public static final enum MUTE_STATUS_BBYVCHIPAUDIOMUTE:Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

.field public static final enum MUTE_STATUS_BCIAUDIOMUTE:Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

.field public static final enum MUTE_STATUS_BINTERNAL1AUDIOMUTE:Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

.field public static final enum MUTE_STATUS_BINTERNAL2AUDIOMUTE:Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

.field public static final enum MUTE_STATUS_BINTERNAL3AUDIOMUTE:Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

.field public static final enum MUTE_STATUS_BINTERNAL4AUDIOMUTE:Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

.field public static final enum MUTE_STATUS_BISAUDIOMODECHANGED:Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

.field public static final enum MUTE_STATUS_BISSRSWOWENABLED:Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

.field public static final enum MUTE_STATUS_BMHEGAPMUTE:Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

.field public static final enum MUTE_STATUS_BMOMENTAUDIOMUTE:Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

.field public static final enum MUTE_STATUS_BPERMANENTAUDIOMUTE:Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

.field public static final enum MUTE_STATUS_BUSRHPAUDIOMUTE:Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

.field public static final enum MUTE_STATUS_BUSRSCART1AUDIOMUTE:Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

.field public static final enum MUTE_STATUS_BUSRSCART2AUDIOMUTE:Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

.field public static final enum MUTE_STATUS_BUSRSPDIFAUDIOMUTE:Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

.field public static final enum MUTE_STATUS_BUSRSPKRAUDIOMUTE:Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

.field public static final enum MUTE_STATUS_SOURCESWITCHAUDIOMUTE:Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

    const-string v1, "MUTE_STATUS_BISAUDIOMODECHANGED"

    invoke-direct {v0, v1, v3}, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;->MUTE_STATUS_BISAUDIOMODECHANGED:Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

    const-string v1, "MUTE_STATUS_BPERMANENTAUDIOMUTE"

    invoke-direct {v0, v1, v4}, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;->MUTE_STATUS_BPERMANENTAUDIOMUTE:Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

    const-string v1, "MUTE_STATUS_BMOMENTAUDIOMUTE"

    invoke-direct {v0, v1, v5}, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;->MUTE_STATUS_BMOMENTAUDIOMUTE:Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

    const-string v1, "MUTE_STATUS_BBYUSERAUDIOMUTE"

    invoke-direct {v0, v1, v6}, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;->MUTE_STATUS_BBYUSERAUDIOMUTE:Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

    const-string v1, "MUTE_STATUS_BBYSYNCAUDIOMUTE"

    invoke-direct {v0, v1, v7}, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;->MUTE_STATUS_BBYSYNCAUDIOMUTE:Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

    const-string v1, "MUTE_STATUS_BBYVCHIPAUDIOMUTE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;->MUTE_STATUS_BBYVCHIPAUDIOMUTE:Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

    const-string v1, "MUTE_STATUS_BBYBLOCKAUDIOMUTE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;->MUTE_STATUS_BBYBLOCKAUDIOMUTE:Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

    const-string v1, "MUTE_STATUS_BINTERNAL1AUDIOMUTE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;->MUTE_STATUS_BINTERNAL1AUDIOMUTE:Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

    const-string v1, "MUTE_STATUS_BINTERNAL2AUDIOMUTE"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;->MUTE_STATUS_BINTERNAL2AUDIOMUTE:Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

    const-string v1, "MUTE_STATUS_BINTERNAL3AUDIOMUTE"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;->MUTE_STATUS_BINTERNAL3AUDIOMUTE:Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

    const-string v1, "MUTE_STATUS_BINTERNAL4AUDIOMUTE"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;->MUTE_STATUS_BINTERNAL4AUDIOMUTE:Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

    const-string v1, "MUTE_STATUS_BBYDURINGLIMITEDTIMEAUDIOMUTE"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;->MUTE_STATUS_BBYDURINGLIMITEDTIMEAUDIOMUTE:Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

    const-string v1, "MUTE_STATUS_BBYSCANINOUTCHGCHCHG"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;->MUTE_STATUS_BBYSCANINOUTCHGCHCHG:Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

    const-string v1, "MUTE_STATUS_BISSRSWOWENABLED"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;->MUTE_STATUS_BISSRSWOWENABLED:Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

    const-string v1, "MUTE_STATUS_BMHEGAPMUTE"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;->MUTE_STATUS_BMHEGAPMUTE:Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

    const-string v1, "MUTE_STATUS_BCIAUDIOMUTE"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;->MUTE_STATUS_BCIAUDIOMUTE:Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

    const-string v1, "MUTE_STATUS_SOURCESWITCHAUDIOMUTE"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;->MUTE_STATUS_SOURCESWITCHAUDIOMUTE:Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

    const-string v1, "MUTE_STATUS_BUSRSPKRAUDIOMUTE"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;->MUTE_STATUS_BUSRSPKRAUDIOMUTE:Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

    const-string v1, "MUTE_STATUS_BUSRHPAUDIOMUTE"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;->MUTE_STATUS_BUSRHPAUDIOMUTE:Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

    const-string v1, "MUTE_STATUS_BUSRSPDIFAUDIOMUTE"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;->MUTE_STATUS_BUSRSPDIFAUDIOMUTE:Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

    const-string v1, "MUTE_STATUS_BUSRSCART1AUDIOMUTE"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;->MUTE_STATUS_BUSRSCART1AUDIOMUTE:Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

    const-string v1, "MUTE_STATUS_BUSRSCART2AUDIOMUTE"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;->MUTE_STATUS_BUSRSCART2AUDIOMUTE:Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

    const/16 v0, 0x16

    new-array v0, v0, [Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;->MUTE_STATUS_BISAUDIOMODECHANGED:Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;->MUTE_STATUS_BPERMANENTAUDIOMUTE:Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;->MUTE_STATUS_BMOMENTAUDIOMUTE:Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;->MUTE_STATUS_BBYUSERAUDIOMUTE:Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;->MUTE_STATUS_BBYSYNCAUDIOMUTE:Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;->MUTE_STATUS_BBYVCHIPAUDIOMUTE:Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;->MUTE_STATUS_BBYBLOCKAUDIOMUTE:Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;->MUTE_STATUS_BINTERNAL1AUDIOMUTE:Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;->MUTE_STATUS_BINTERNAL2AUDIOMUTE:Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;->MUTE_STATUS_BINTERNAL3AUDIOMUTE:Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;->MUTE_STATUS_BINTERNAL4AUDIOMUTE:Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;->MUTE_STATUS_BBYDURINGLIMITEDTIMEAUDIOMUTE:Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;->MUTE_STATUS_BBYSCANINOUTCHGCHCHG:Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;->MUTE_STATUS_BISSRSWOWENABLED:Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;->MUTE_STATUS_BMHEGAPMUTE:Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;->MUTE_STATUS_BCIAUDIOMUTE:Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;->MUTE_STATUS_SOURCESWITCHAUDIOMUTE:Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;->MUTE_STATUS_BUSRSPKRAUDIOMUTE:Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;->MUTE_STATUS_BUSRHPAUDIOMUTE:Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;->MUTE_STATUS_BUSRSPDIFAUDIOMUTE:Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;->MUTE_STATUS_BUSRSCART1AUDIOMUTE:Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;->MUTE_STATUS_BUSRSCART2AUDIOMUTE:Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

    invoke-virtual {v0}, [Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;

    return-object v0
.end method
