.class public final enum Lcom/mstar/android/tvapi/common/vo/ScreenPixelInfo$EnumPixelRGBStage;
.super Ljava/lang/Enum;
.source "ScreenPixelInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mstar/android/tvapi/common/vo/ScreenPixelInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EnumPixelRGBStage"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/tvapi/common/vo/ScreenPixelInfo$EnumPixelRGBStage;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/tvapi/common/vo/ScreenPixelInfo$EnumPixelRGBStage;

.field public static final enum PIXEL_STAGE_AFTER_DLC:Lcom/mstar/android/tvapi/common/vo/ScreenPixelInfo$EnumPixelRGBStage;

.field public static final enum PIXEL_STAGE_AFTER_OSD:Lcom/mstar/android/tvapi/common/vo/ScreenPixelInfo$EnumPixelRGBStage;

.field public static final enum PIXEL_STAGE_MAX:Lcom/mstar/android/tvapi/common/vo/ScreenPixelInfo$EnumPixelRGBStage;

.field public static final enum PIXEL_STAGE_PRE_GAMMA:Lcom/mstar/android/tvapi/common/vo/ScreenPixelInfo$EnumPixelRGBStage;

.field private static seq:I


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/ScreenPixelInfo$EnumPixelRGBStage;

    const-string v1, "PIXEL_STAGE_AFTER_DLC"

    invoke-direct {v0, v1, v3, v4}, Lcom/mstar/android/tvapi/common/vo/ScreenPixelInfo$EnumPixelRGBStage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/ScreenPixelInfo$EnumPixelRGBStage;->PIXEL_STAGE_AFTER_DLC:Lcom/mstar/android/tvapi/common/vo/ScreenPixelInfo$EnumPixelRGBStage;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/ScreenPixelInfo$EnumPixelRGBStage;

    const-string v1, "PIXEL_STAGE_PRE_GAMMA"

    invoke-direct {v0, v1, v4, v5}, Lcom/mstar/android/tvapi/common/vo/ScreenPixelInfo$EnumPixelRGBStage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/ScreenPixelInfo$EnumPixelRGBStage;->PIXEL_STAGE_PRE_GAMMA:Lcom/mstar/android/tvapi/common/vo/ScreenPixelInfo$EnumPixelRGBStage;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/ScreenPixelInfo$EnumPixelRGBStage;

    const-string v1, "PIXEL_STAGE_AFTER_OSD"

    invoke-direct {v0, v1, v5, v6}, Lcom/mstar/android/tvapi/common/vo/ScreenPixelInfo$EnumPixelRGBStage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/ScreenPixelInfo$EnumPixelRGBStage;->PIXEL_STAGE_AFTER_OSD:Lcom/mstar/android/tvapi/common/vo/ScreenPixelInfo$EnumPixelRGBStage;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/ScreenPixelInfo$EnumPixelRGBStage;

    const-string v1, "PIXEL_STAGE_MAX"

    const/16 v2, 0xff

    invoke-direct {v0, v1, v6, v2}, Lcom/mstar/android/tvapi/common/vo/ScreenPixelInfo$EnumPixelRGBStage;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/ScreenPixelInfo$EnumPixelRGBStage;->PIXEL_STAGE_MAX:Lcom/mstar/android/tvapi/common/vo/ScreenPixelInfo$EnumPixelRGBStage;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/mstar/android/tvapi/common/vo/ScreenPixelInfo$EnumPixelRGBStage;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/ScreenPixelInfo$EnumPixelRGBStage;->PIXEL_STAGE_AFTER_DLC:Lcom/mstar/android/tvapi/common/vo/ScreenPixelInfo$EnumPixelRGBStage;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/ScreenPixelInfo$EnumPixelRGBStage;->PIXEL_STAGE_PRE_GAMMA:Lcom/mstar/android/tvapi/common/vo/ScreenPixelInfo$EnumPixelRGBStage;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/ScreenPixelInfo$EnumPixelRGBStage;->PIXEL_STAGE_AFTER_OSD:Lcom/mstar/android/tvapi/common/vo/ScreenPixelInfo$EnumPixelRGBStage;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/ScreenPixelInfo$EnumPixelRGBStage;->PIXEL_STAGE_MAX:Lcom/mstar/android/tvapi/common/vo/ScreenPixelInfo$EnumPixelRGBStage;

    aput-object v1, v0, v6

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/ScreenPixelInfo$EnumPixelRGBStage;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/ScreenPixelInfo$EnumPixelRGBStage;

    sput v3, Lcom/mstar/android/tvapi/common/vo/ScreenPixelInfo$EnumPixelRGBStage;->seq:I

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/mstar/android/tvapi/common/vo/ScreenPixelInfo$EnumPixelRGBStage;->value:I

    return-void
.end method

.method public static valueOf(I)Lcom/mstar/android/tvapi/common/vo/ScreenPixelInfo$EnumPixelRGBStage;
    .locals 1
    .param p0    # I

    sparse-switch p0, :sswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :sswitch_0
    sget-object v0, Lcom/mstar/android/tvapi/common/vo/ScreenPixelInfo$EnumPixelRGBStage;->PIXEL_STAGE_AFTER_DLC:Lcom/mstar/android/tvapi/common/vo/ScreenPixelInfo$EnumPixelRGBStage;

    goto :goto_0

    :sswitch_1
    sget-object v0, Lcom/mstar/android/tvapi/common/vo/ScreenPixelInfo$EnumPixelRGBStage;->PIXEL_STAGE_PRE_GAMMA:Lcom/mstar/android/tvapi/common/vo/ScreenPixelInfo$EnumPixelRGBStage;

    goto :goto_0

    :sswitch_2
    sget-object v0, Lcom/mstar/android/tvapi/common/vo/ScreenPixelInfo$EnumPixelRGBStage;->PIXEL_STAGE_AFTER_OSD:Lcom/mstar/android/tvapi/common/vo/ScreenPixelInfo$EnumPixelRGBStage;

    goto :goto_0

    :sswitch_3
    sget-object v0, Lcom/mstar/android/tvapi/common/vo/ScreenPixelInfo$EnumPixelRGBStage;->PIXEL_STAGE_MAX:Lcom/mstar/android/tvapi/common/vo/ScreenPixelInfo$EnumPixelRGBStage;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x3 -> :sswitch_2
        0xff -> :sswitch_3
    .end sparse-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/tvapi/common/vo/ScreenPixelInfo$EnumPixelRGBStage;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/tvapi/common/vo/ScreenPixelInfo$EnumPixelRGBStage;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/common/vo/ScreenPixelInfo$EnumPixelRGBStage;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/tvapi/common/vo/ScreenPixelInfo$EnumPixelRGBStage;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/ScreenPixelInfo$EnumPixelRGBStage;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/ScreenPixelInfo$EnumPixelRGBStage;

    invoke-virtual {v0}, [Lcom/mstar/android/tvapi/common/vo/ScreenPixelInfo$EnumPixelRGBStage;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/tvapi/common/vo/ScreenPixelInfo$EnumPixelRGBStage;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/ScreenPixelInfo$EnumPixelRGBStage;->value:I

    return v0
.end method
