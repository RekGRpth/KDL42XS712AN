.class public final enum Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;
.super Ljava/lang/Enum;
.source "EnumCecDeviceLa.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;

.field public static final enum E_AUDIO_SYS:Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;

.field public static final enum E_BROADCAST:Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;

.field public static final enum E_FREE_USE:Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;

.field public static final enum E_PLAYBACK1:Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;

.field public static final enum E_PLAYBACK2:Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;

.field public static final enum E_PLYBACK3:Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;

.field public static final enum E_RECORDER1:Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;

.field public static final enum E_RECORDER2:Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;

.field public static final enum E_RECORER3:Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;

.field public static final enum E_RESERVED1:Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;

.field public static final enum E_RESERVED2:Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;

.field public static final enum E_TUNER1:Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;

.field public static final enum E_TUNER2:Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;

.field public static final enum E_TUNER3:Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;

.field public static final enum E_TUNER4:Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;

.field public static final enum E_TV:Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;

.field public static final enum E_UNREGISTERED:Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;

    const-string v1, "E_TV"

    invoke-direct {v0, v1, v3}, Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;->E_TV:Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;

    const-string v1, "E_RECORDER1"

    invoke-direct {v0, v1, v4}, Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;->E_RECORDER1:Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;

    const-string v1, "E_RECORDER2"

    invoke-direct {v0, v1, v5}, Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;->E_RECORDER2:Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;

    const-string v1, "E_TUNER1"

    invoke-direct {v0, v1, v6}, Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;->E_TUNER1:Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;

    const-string v1, "E_PLAYBACK1"

    invoke-direct {v0, v1, v7}, Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;->E_PLAYBACK1:Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;

    const-string v1, "E_AUDIO_SYS"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;->E_AUDIO_SYS:Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;

    const-string v1, "E_TUNER2"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;->E_TUNER2:Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;

    const-string v1, "E_TUNER3"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;->E_TUNER3:Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;

    const-string v1, "E_PLAYBACK2"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;->E_PLAYBACK2:Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;

    const-string v1, "E_RECORER3"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;->E_RECORER3:Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;

    const-string v1, "E_TUNER4"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;->E_TUNER4:Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;

    const-string v1, "E_PLYBACK3"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;->E_PLYBACK3:Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;

    const-string v1, "E_RESERVED1"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;->E_RESERVED1:Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;

    const-string v1, "E_RESERVED2"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;->E_RESERVED2:Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;

    const-string v1, "E_FREE_USE"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;->E_FREE_USE:Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;

    const-string v1, "E_UNREGISTERED"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;->E_UNREGISTERED:Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;

    const-string v1, "E_BROADCAST"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;->E_BROADCAST:Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;

    const/16 v0, 0x11

    new-array v0, v0, [Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;->E_TV:Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;->E_RECORDER1:Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;->E_RECORDER2:Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;->E_TUNER1:Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;

    aput-object v1, v0, v6

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;->E_PLAYBACK1:Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;->E_AUDIO_SYS:Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;->E_TUNER2:Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;->E_TUNER3:Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;->E_PLAYBACK2:Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;->E_RECORER3:Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;->E_TUNER4:Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;->E_PLYBACK3:Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;->E_RESERVED1:Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;->E_RESERVED2:Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;->E_FREE_USE:Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;->E_UNREGISTERED:Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;->E_BROADCAST:Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;

    aput-object v2, v0, v1

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;

    invoke-virtual {v0}, [Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;

    return-object v0
.end method
