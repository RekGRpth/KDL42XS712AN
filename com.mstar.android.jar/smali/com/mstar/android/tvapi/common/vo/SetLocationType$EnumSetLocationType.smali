.class public final enum Lcom/mstar/android/tvapi/common/vo/SetLocationType$EnumSetLocationType;
.super Ljava/lang/Enum;
.source "SetLocationType.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mstar/android/tvapi/common/vo/SetLocationType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EnumSetLocationType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/tvapi/common/vo/SetLocationType$EnumSetLocationType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/tvapi/common/vo/SetLocationType$EnumSetLocationType;

.field public static final enum E_SET_LOCATION_AFTER_DLC:Lcom/mstar/android/tvapi/common/vo/SetLocationType$EnumSetLocationType;

.field public static final enum E_SET_LOCATION_AFTER_GAMMA:Lcom/mstar/android/tvapi/common/vo/SetLocationType$EnumSetLocationType;

.field public static final enum E_SET_LOCATION_AFTER_MEMORY:Lcom/mstar/android/tvapi/common/vo/SetLocationType$EnumSetLocationType;

.field public static final enum E_SET_LOCATION_BEFORE_GAMMA:Lcom/mstar/android/tvapi/common/vo/SetLocationType$EnumSetLocationType;

.field private static seq:I


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/SetLocationType$EnumSetLocationType;

    const-string v1, "E_SET_LOCATION_BEFORE_GAMMA"

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/SetLocationType$EnumSetLocationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/SetLocationType$EnumSetLocationType;->E_SET_LOCATION_BEFORE_GAMMA:Lcom/mstar/android/tvapi/common/vo/SetLocationType$EnumSetLocationType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/SetLocationType$EnumSetLocationType;

    const-string v1, "E_SET_LOCATION_AFTER_GAMMA"

    invoke-direct {v0, v1, v3, v4}, Lcom/mstar/android/tvapi/common/vo/SetLocationType$EnumSetLocationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/SetLocationType$EnumSetLocationType;->E_SET_LOCATION_AFTER_GAMMA:Lcom/mstar/android/tvapi/common/vo/SetLocationType$EnumSetLocationType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/SetLocationType$EnumSetLocationType;

    const-string v1, "E_SET_LOCATION_AFTER_MEMORY"

    invoke-direct {v0, v1, v4, v5}, Lcom/mstar/android/tvapi/common/vo/SetLocationType$EnumSetLocationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/SetLocationType$EnumSetLocationType;->E_SET_LOCATION_AFTER_MEMORY:Lcom/mstar/android/tvapi/common/vo/SetLocationType$EnumSetLocationType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/SetLocationType$EnumSetLocationType;

    const-string v1, "E_SET_LOCATION_AFTER_DLC"

    invoke-direct {v0, v1, v5, v6}, Lcom/mstar/android/tvapi/common/vo/SetLocationType$EnumSetLocationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/SetLocationType$EnumSetLocationType;->E_SET_LOCATION_AFTER_DLC:Lcom/mstar/android/tvapi/common/vo/SetLocationType$EnumSetLocationType;

    new-array v0, v6, [Lcom/mstar/android/tvapi/common/vo/SetLocationType$EnumSetLocationType;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/SetLocationType$EnumSetLocationType;->E_SET_LOCATION_BEFORE_GAMMA:Lcom/mstar/android/tvapi/common/vo/SetLocationType$EnumSetLocationType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/SetLocationType$EnumSetLocationType;->E_SET_LOCATION_AFTER_GAMMA:Lcom/mstar/android/tvapi/common/vo/SetLocationType$EnumSetLocationType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/SetLocationType$EnumSetLocationType;->E_SET_LOCATION_AFTER_MEMORY:Lcom/mstar/android/tvapi/common/vo/SetLocationType$EnumSetLocationType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/SetLocationType$EnumSetLocationType;->E_SET_LOCATION_AFTER_DLC:Lcom/mstar/android/tvapi/common/vo/SetLocationType$EnumSetLocationType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/SetLocationType$EnumSetLocationType;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/SetLocationType$EnumSetLocationType;

    sput v2, Lcom/mstar/android/tvapi/common/vo/SetLocationType$EnumSetLocationType;->seq:I

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/mstar/android/tvapi/common/vo/SetLocationType$EnumSetLocationType;->value:I

    invoke-static {p3}, Lcom/mstar/android/tvapi/common/vo/SetLocationType$EnumSetLocationType;->setHashtableValue(I)V

    return-void
.end method

.method public static getOrdinalThroughValue(I)I
    .locals 3
    .param p0    # I

    # getter for: Lcom/mstar/android/tvapi/common/vo/SetLocationType;->htEnumSetLocationType:Ljava/util/Hashtable;
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/SetLocationType;->access$000()Ljava/util/Hashtable;

    move-result-object v1

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    :goto_0
    return v1

    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method private static setHashtableValue(I)V
    .locals 4
    .param p0    # I

    # getter for: Lcom/mstar/android/tvapi/common/vo/SetLocationType;->htEnumSetLocationType:Ljava/util/Hashtable;
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/SetLocationType;->access$000()Ljava/util/Hashtable;

    move-result-object v0

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, p0}, Ljava/lang/Integer;-><init>(I)V

    new-instance v2, Ljava/lang/Integer;

    sget v3, Lcom/mstar/android/tvapi/common/vo/SetLocationType$EnumSetLocationType;->seq:I

    invoke-direct {v2, v3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget v0, Lcom/mstar/android/tvapi/common/vo/SetLocationType$EnumSetLocationType;->seq:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/mstar/android/tvapi/common/vo/SetLocationType$EnumSetLocationType;->seq:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/tvapi/common/vo/SetLocationType$EnumSetLocationType;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/tvapi/common/vo/SetLocationType$EnumSetLocationType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/common/vo/SetLocationType$EnumSetLocationType;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/tvapi/common/vo/SetLocationType$EnumSetLocationType;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/SetLocationType$EnumSetLocationType;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/SetLocationType$EnumSetLocationType;

    invoke-virtual {v0}, [Lcom/mstar/android/tvapi/common/vo/SetLocationType$EnumSetLocationType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/tvapi/common/vo/SetLocationType$EnumSetLocationType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/SetLocationType$EnumSetLocationType;->value:I

    return v0
.end method
