.class public final enum Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;
.super Ljava/lang/Enum;
.source "MuteType.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mstar/android/tvapi/common/vo/MuteType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EnumMuteType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

.field public static final enum E_BYBLOCK:Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

.field public static final enum E_BYSYNC:Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

.field public static final enum E_BYUSER:Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

.field public static final enum E_BYVCHIP:Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

.field public static final enum E_CI:Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

.field public static final enum E_DURING_LIMITED_TIME:Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

.field public static final enum E_INTERNAL1:Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

.field public static final enum E_INTERNAL2:Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

.field public static final enum E_INTERNAL3:Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

.field public static final enum E_MHEGAP:Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

.field public static final enum E_MOMENT:Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

.field public static final enum E_MUTE_ALL:Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

.field public static final enum E_MUTE_PERMANENT:Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

.field public static final enum E_MUTE_USER_DATA_IN:Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

.field public static final enum E_SCAN:Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

.field public static final enum E_SOURCESWITCH:Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

.field public static final enum E_USER_HP:Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

.field public static final enum E_USER_SCART1:Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

.field public static final enum E_USER_SCART2:Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

.field public static final enum E_USER_SPDIF:Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

.field public static final enum E_USER_SPEAKER:Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

.field private static seq:I


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const/16 v8, 0x8

    const/4 v7, 0x4

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

    const-string v1, "E_MUTE_PERMANENT"

    invoke-direct {v0, v1, v4, v5}, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;->E_MUTE_PERMANENT:Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

    const-string v1, "E_MOMENT"

    invoke-direct {v0, v1, v5, v6}, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;->E_MOMENT:Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

    const-string v1, "E_BYUSER"

    invoke-direct {v0, v1, v6, v7}, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;->E_BYUSER:Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

    const-string v1, "E_BYSYNC"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v2, v8}, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;->E_BYSYNC:Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

    const-string v1, "E_BYVCHIP"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v7, v2}, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;->E_BYVCHIP:Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

    const-string v1, "E_BYBLOCK"

    const/4 v2, 0x5

    const/16 v3, 0x20

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;->E_BYBLOCK:Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

    const-string v1, "E_INTERNAL1"

    const/4 v2, 0x6

    const/16 v3, 0x40

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;->E_INTERNAL1:Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

    const-string v1, "E_INTERNAL2"

    const/4 v2, 0x7

    const/16 v3, 0x80

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;->E_INTERNAL2:Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

    const-string v1, "E_INTERNAL3"

    const/16 v2, 0x100

    invoke-direct {v0, v1, v8, v2}, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;->E_INTERNAL3:Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

    const-string v1, "E_DURING_LIMITED_TIME"

    const/16 v2, 0x9

    const/16 v3, 0x200

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;->E_DURING_LIMITED_TIME:Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

    const-string v1, "E_MHEGAP"

    const/16 v2, 0xa

    const/16 v3, 0x400

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;->E_MHEGAP:Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

    const-string v1, "E_CI"

    const/16 v2, 0xb

    const/16 v3, 0x800

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;->E_CI:Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

    const-string v1, "E_SCAN"

    const/16 v2, 0xc

    const/16 v3, 0x1000

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;->E_SCAN:Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

    const-string v1, "E_SOURCESWITCH"

    const/16 v2, 0xd

    const/16 v3, 0x2000

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;->E_SOURCESWITCH:Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

    const-string v1, "E_USER_SPEAKER"

    const/16 v2, 0xe

    const/16 v3, 0x4000

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;->E_USER_SPEAKER:Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

    const-string v1, "E_USER_HP"

    const/16 v2, 0xf

    const v3, 0x8000

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;->E_USER_HP:Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

    const-string v1, "E_USER_SPDIF"

    const/16 v2, 0x10

    const/high16 v3, 0x10000

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;->E_USER_SPDIF:Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

    const-string v1, "E_USER_SCART1"

    const/16 v2, 0x11

    const/high16 v3, 0x20000

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;->E_USER_SCART1:Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

    const-string v1, "E_USER_SCART2"

    const/16 v2, 0x12

    const/high16 v3, 0x40000

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;->E_USER_SCART2:Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

    const-string v1, "E_MUTE_ALL"

    const/16 v2, 0x13

    const/high16 v3, 0x80000

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;->E_MUTE_ALL:Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

    const-string v1, "E_MUTE_USER_DATA_IN"

    const/16 v2, 0x14

    const/high16 v3, 0x100000

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;->E_MUTE_USER_DATA_IN:Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

    const/16 v0, 0x15

    new-array v0, v0, [Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;->E_MUTE_PERMANENT:Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;->E_MOMENT:Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;->E_BYUSER:Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

    aput-object v1, v0, v6

    const/4 v1, 0x3

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;->E_BYSYNC:Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

    aput-object v2, v0, v1

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;->E_BYVCHIP:Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;->E_BYBLOCK:Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;->E_INTERNAL1:Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;->E_INTERNAL2:Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

    aput-object v2, v0, v1

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;->E_INTERNAL3:Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

    aput-object v1, v0, v8

    const/16 v1, 0x9

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;->E_DURING_LIMITED_TIME:Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;->E_MHEGAP:Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;->E_CI:Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;->E_SCAN:Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;->E_SOURCESWITCH:Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;->E_USER_SPEAKER:Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;->E_USER_HP:Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;->E_USER_SPDIF:Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;->E_USER_SCART1:Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;->E_USER_SCART2:Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;->E_MUTE_ALL:Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;->E_MUTE_USER_DATA_IN:Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

    sput v4, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;->seq:I

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;->value:I

    invoke-static {p3}, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;->setHashtableValue(I)V

    return-void
.end method

.method public static getOrdinalThroughValue(I)I
    .locals 3
    .param p0    # I

    # getter for: Lcom/mstar/android/tvapi/common/vo/MuteType;->htEnumMuteType:Ljava/util/Hashtable;
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/MuteType;->access$000()Ljava/util/Hashtable;

    move-result-object v1

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    :goto_0
    return v1

    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method private static setHashtableValue(I)V
    .locals 4
    .param p0    # I

    # getter for: Lcom/mstar/android/tvapi/common/vo/MuteType;->htEnumMuteType:Ljava/util/Hashtable;
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/MuteType;->access$000()Ljava/util/Hashtable;

    move-result-object v0

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, p0}, Ljava/lang/Integer;-><init>(I)V

    new-instance v2, Ljava/lang/Integer;

    sget v3, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;->seq:I

    invoke-direct {v2, v3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget v0, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;->seq:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;->seq:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

    invoke-virtual {v0}, [Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;->value:I

    return v0
.end method
