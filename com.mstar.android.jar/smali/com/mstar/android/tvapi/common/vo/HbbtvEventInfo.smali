.class public Lcom/mstar/android/tvapi/common/vo/HbbtvEventInfo;
.super Ljava/lang/Object;
.source "HbbtvEventInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mstar/android/tvapi/common/vo/HbbtvEventInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public eEvent:I

.field public param:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/HbbtvEventInfo$1;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/common/vo/HbbtvEventInfo$1;-><init>()V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/HbbtvEventInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    const/16 v3, 0xa

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-array v1, v3, [I

    iput-object v1, p0, Lcom/mstar/android/tvapi/common/vo/HbbtvEventInfo;->param:[I

    iput v2, p0, Lcom/mstar/android/tvapi/common/vo/HbbtvEventInfo;->eEvent:I

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    iget-object v1, p0, Lcom/mstar/android/tvapi/common/vo/HbbtvEventInfo;->param:[I

    aput v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1    # Landroid/os/Parcel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0xa

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/mstar/android/tvapi/common/vo/HbbtvEventInfo;->param:[I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/HbbtvEventInfo;->eEvent:I

    iget-object v0, p0, Lcom/mstar/android/tvapi/common/vo/HbbtvEventInfo;->param:[I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readIntArray([I)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/HbbtvEventInfo;->eEvent:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/mstar/android/tvapi/common/vo/HbbtvEventInfo;->param:[I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeIntArray([I)V

    return-void
.end method
