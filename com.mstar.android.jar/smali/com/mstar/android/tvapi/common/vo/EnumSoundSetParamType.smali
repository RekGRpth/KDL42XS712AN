.class public final enum Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;
.super Ljava/lang/Enum;
.source "EnumSoundSetParamType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

.field public static final enum E_SOUND_SET_PARAM_ABSOLUTEEQ_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

.field public static final enum E_SOUND_SET_PARAM_ABSOULEBASS_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

.field public static final enum E_SOUND_SET_PARAM_AUDIODELAY_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

.field public static final enum E_SOUND_SET_PARAM_AVC_AT_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

.field public static final enum E_SOUND_SET_PARAM_AVC_MODE_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

.field public static final enum E_SOUND_SET_PARAM_AVC_RT_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

.field public static final enum E_SOUND_SET_PARAM_AVC_THRESHOLD_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

.field public static final enum E_SOUND_SET_PARAM_BALANCE_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

.field public static final enum E_SOUND_SET_PARAM_BASS_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

.field public static final enum E_SOUND_SET_PARAM_DCOFFET_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

.field public static final enum E_SOUND_SET_PARAM_DMAREADER_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

.field public static final enum E_SOUND_SET_PARAM_DRC_THRESHOLD_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

.field public static final enum E_SOUND_SET_PARAM_EQ_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

.field public static final enum E_SOUND_SET_PARAM_NR_THRESHOLD_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

.field public static final enum E_SOUND_SET_PARAM_PEQ_32K_A0_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

.field public static final enum E_SOUND_SET_PARAM_PEQ_32K_A1_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

.field public static final enum E_SOUND_SET_PARAM_PEQ_32K_A2_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

.field public static final enum E_SOUND_SET_PARAM_PEQ_32K_B1_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

.field public static final enum E_SOUND_SET_PARAM_PEQ_32K_B2_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

.field public static final enum E_SOUND_SET_PARAM_PEQ_48K_A0_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

.field public static final enum E_SOUND_SET_PARAM_PEQ_48K_A1_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

.field public static final enum E_SOUND_SET_PARAM_PEQ_48K_A2_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

.field public static final enum E_SOUND_SET_PARAM_PEQ_48K_B1_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

.field public static final enum E_SOUND_SET_PARAM_PEQ_48K_B2_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

.field public static final enum E_SOUND_SET_PARAM_PRESCALE_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

.field public static final enum E_SOUND_SET_PARAM_SURROUND_LPFGAIN_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

.field public static final enum E_SOUND_SET_PARAM_SURROUND_XA_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

.field public static final enum E_SOUND_SET_PARAM_SURROUND_XB_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

.field public static final enum E_SOUND_SET_PARAM_SURROUND_XK_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

.field public static final enum E_SOUND_SET_PARAM_TREBLE_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    const-string v1, "E_SOUND_SET_PARAM_PRESCALE_"

    invoke-direct {v0, v1, v3}, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;->E_SOUND_SET_PARAM_PRESCALE_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    const-string v1, "E_SOUND_SET_PARAM_BALANCE_"

    invoke-direct {v0, v1, v4}, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;->E_SOUND_SET_PARAM_BALANCE_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    const-string v1, "E_SOUND_SET_PARAM_EQ_"

    invoke-direct {v0, v1, v5}, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;->E_SOUND_SET_PARAM_EQ_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    const-string v1, "E_SOUND_SET_PARAM_SURROUND_XA_"

    invoke-direct {v0, v1, v6}, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;->E_SOUND_SET_PARAM_SURROUND_XA_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    const-string v1, "E_SOUND_SET_PARAM_SURROUND_XB_"

    invoke-direct {v0, v1, v7}, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;->E_SOUND_SET_PARAM_SURROUND_XB_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    const-string v1, "E_SOUND_SET_PARAM_SURROUND_XK_"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;->E_SOUND_SET_PARAM_SURROUND_XK_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    const-string v1, "E_SOUND_SET_PARAM_SURROUND_LPFGAIN_"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;->E_SOUND_SET_PARAM_SURROUND_LPFGAIN_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    const-string v1, "E_SOUND_SET_PARAM_TREBLE_"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;->E_SOUND_SET_PARAM_TREBLE_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    const-string v1, "E_SOUND_SET_PARAM_BASS_"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;->E_SOUND_SET_PARAM_BASS_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    const-string v1, "E_SOUND_SET_PARAM_ABSOULEBASS_"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;->E_SOUND_SET_PARAM_ABSOULEBASS_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    const-string v1, "E_SOUND_SET_PARAM_AVC_MODE_"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;->E_SOUND_SET_PARAM_AVC_MODE_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    const-string v1, "E_SOUND_SET_PARAM_NR_THRESHOLD_"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;->E_SOUND_SET_PARAM_NR_THRESHOLD_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    const-string v1, "E_SOUND_SET_PARAM_AVC_THRESHOLD_"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;->E_SOUND_SET_PARAM_AVC_THRESHOLD_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    const-string v1, "E_SOUND_SET_PARAM_AVC_AT_"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;->E_SOUND_SET_PARAM_AVC_AT_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    const-string v1, "E_SOUND_SET_PARAM_AVC_RT_"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;->E_SOUND_SET_PARAM_AVC_RT_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    const-string v1, "E_SOUND_SET_PARAM_AUDIODELAY_"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;->E_SOUND_SET_PARAM_AUDIODELAY_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    const-string v1, "E_SOUND_SET_PARAM_DCOFFET_"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;->E_SOUND_SET_PARAM_DCOFFET_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    const-string v1, "E_SOUND_SET_PARAM_PEQ_48K_A0_"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;->E_SOUND_SET_PARAM_PEQ_48K_A0_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    const-string v1, "E_SOUND_SET_PARAM_PEQ_48K_A1_"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;->E_SOUND_SET_PARAM_PEQ_48K_A1_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    const-string v1, "E_SOUND_SET_PARAM_PEQ_48K_A2_"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;->E_SOUND_SET_PARAM_PEQ_48K_A2_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    const-string v1, "E_SOUND_SET_PARAM_PEQ_48K_B1_"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;->E_SOUND_SET_PARAM_PEQ_48K_B1_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    const-string v1, "E_SOUND_SET_PARAM_PEQ_48K_B2_"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;->E_SOUND_SET_PARAM_PEQ_48K_B2_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    const-string v1, "E_SOUND_SET_PARAM_PEQ_32K_A0_"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;->E_SOUND_SET_PARAM_PEQ_32K_A0_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    const-string v1, "E_SOUND_SET_PARAM_PEQ_32K_A1_"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;->E_SOUND_SET_PARAM_PEQ_32K_A1_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    const-string v1, "E_SOUND_SET_PARAM_PEQ_32K_A2_"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;->E_SOUND_SET_PARAM_PEQ_32K_A2_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    const-string v1, "E_SOUND_SET_PARAM_PEQ_32K_B1_"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;->E_SOUND_SET_PARAM_PEQ_32K_B1_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    const-string v1, "E_SOUND_SET_PARAM_PEQ_32K_B2_"

    const/16 v2, 0x1a

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;->E_SOUND_SET_PARAM_PEQ_32K_B2_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    const-string v1, "E_SOUND_SET_PARAM_ABSOLUTEEQ_"

    const/16 v2, 0x1b

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;->E_SOUND_SET_PARAM_ABSOLUTEEQ_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    const-string v1, "E_SOUND_SET_PARAM_DRC_THRESHOLD_"

    const/16 v2, 0x1c

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;->E_SOUND_SET_PARAM_DRC_THRESHOLD_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    const-string v1, "E_SOUND_SET_PARAM_DMAREADER_"

    const/16 v2, 0x1d

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;->E_SOUND_SET_PARAM_DMAREADER_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    const/16 v0, 0x1e

    new-array v0, v0, [Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;->E_SOUND_SET_PARAM_PRESCALE_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;->E_SOUND_SET_PARAM_BALANCE_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;->E_SOUND_SET_PARAM_EQ_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;->E_SOUND_SET_PARAM_SURROUND_XA_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;->E_SOUND_SET_PARAM_SURROUND_XB_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;->E_SOUND_SET_PARAM_SURROUND_XK_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;->E_SOUND_SET_PARAM_SURROUND_LPFGAIN_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;->E_SOUND_SET_PARAM_TREBLE_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;->E_SOUND_SET_PARAM_BASS_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;->E_SOUND_SET_PARAM_ABSOULEBASS_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;->E_SOUND_SET_PARAM_AVC_MODE_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;->E_SOUND_SET_PARAM_NR_THRESHOLD_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;->E_SOUND_SET_PARAM_AVC_THRESHOLD_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;->E_SOUND_SET_PARAM_AVC_AT_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;->E_SOUND_SET_PARAM_AVC_RT_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;->E_SOUND_SET_PARAM_AUDIODELAY_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;->E_SOUND_SET_PARAM_DCOFFET_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;->E_SOUND_SET_PARAM_PEQ_48K_A0_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;->E_SOUND_SET_PARAM_PEQ_48K_A1_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;->E_SOUND_SET_PARAM_PEQ_48K_A2_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;->E_SOUND_SET_PARAM_PEQ_48K_B1_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;->E_SOUND_SET_PARAM_PEQ_48K_B2_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;->E_SOUND_SET_PARAM_PEQ_32K_A0_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;->E_SOUND_SET_PARAM_PEQ_32K_A1_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;->E_SOUND_SET_PARAM_PEQ_32K_A2_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;->E_SOUND_SET_PARAM_PEQ_32K_B1_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;->E_SOUND_SET_PARAM_PEQ_32K_B2_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;->E_SOUND_SET_PARAM_ABSOLUTEEQ_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;->E_SOUND_SET_PARAM_DRC_THRESHOLD_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;->E_SOUND_SET_PARAM_DMAREADER_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    invoke-virtual {v0}, [Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    return-object v0
.end method
