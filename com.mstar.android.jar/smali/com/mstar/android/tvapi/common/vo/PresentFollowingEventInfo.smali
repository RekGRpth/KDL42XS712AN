.class public Lcom/mstar/android/tvapi/common/vo/PresentFollowingEventInfo;
.super Ljava/lang/Object;
.source "PresentFollowingEventInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mstar/android/tvapi/common/vo/PresentFollowingEventInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public componentInfo:Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;

.field public eventInfo:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/PresentFollowingEventInfo$1;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/common/vo/PresentFollowingEventInfo$1;-><init>()V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/PresentFollowingEventInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;-><init>()V

    iput-object v0, p0, Lcom/mstar/android/tvapi/common/vo/PresentFollowingEventInfo;->componentInfo:Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;-><init>()V

    iput-object v0, p0, Lcom/mstar/android/tvapi/common/vo/PresentFollowingEventInfo;->eventInfo:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1    # Landroid/os/Parcel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;

    iput-object v0, p0, Lcom/mstar/android/tvapi/common/vo/PresentFollowingEventInfo;->componentInfo:Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;

    sget-object v0, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    iput-object v0, p0, Lcom/mstar/android/tvapi/common/vo/PresentFollowingEventInfo;->eventInfo:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget-object v0, p0, Lcom/mstar/android/tvapi/common/vo/PresentFollowingEventInfo;->componentInfo:Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;

    invoke-virtual {v0, p1, p2}, Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;->writeToParcel(Landroid/os/Parcel;I)V

    iget-object v0, p0, Lcom/mstar/android/tvapi/common/vo/PresentFollowingEventInfo;->eventInfo:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-virtual {v0, p1, p2}, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->writeToParcel(Landroid/os/Parcel;I)V

    return-void
.end method
