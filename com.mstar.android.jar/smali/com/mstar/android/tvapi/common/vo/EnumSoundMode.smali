.class public final enum Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;
.super Ljava/lang/Enum;
.source "EnumSoundMode.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;",
        ">;",
        "Landroid/os/Parcelable;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum E_MOVIE:Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;

.field public static final enum E_MUSIC:Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;

.field public static final enum E_NUM:Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;

.field public static final enum E_ONSITE1:Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;

.field public static final enum E_ONSITE2:Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;

.field public static final enum E_SPORTS:Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;

.field public static final enum E_STANDARD:Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;

.field public static final enum E_USER:Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;

    const-string v1, "E_STANDARD"

    invoke-direct {v0, v1, v3}, Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;->E_STANDARD:Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;

    const-string v1, "E_MUSIC"

    invoke-direct {v0, v1, v4}, Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;->E_MUSIC:Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;

    const-string v1, "E_MOVIE"

    invoke-direct {v0, v1, v5}, Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;->E_MOVIE:Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;

    const-string v1, "E_SPORTS"

    invoke-direct {v0, v1, v6}, Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;->E_SPORTS:Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;

    const-string v1, "E_USER"

    invoke-direct {v0, v1, v7}, Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;->E_USER:Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;

    const-string v1, "E_ONSITE1"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;->E_ONSITE1:Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;

    const-string v1, "E_ONSITE2"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;->E_ONSITE2:Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;

    const-string v1, "E_NUM"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;->E_NUM:Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;

    const/16 v0, 0x8

    new-array v0, v0, [Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;->E_STANDARD:Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;->E_MUSIC:Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;->E_MOVIE:Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;->E_SPORTS:Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;

    aput-object v1, v0, v6

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;->E_USER:Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;->E_ONSITE1:Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;->E_ONSITE2:Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;->E_NUM:Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;

    aput-object v2, v0, v1

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundMode$1;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/common/vo/EnumSoundMode$1;-><init>()V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;

    invoke-virtual {v0}, [Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;

    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    invoke-virtual {p0}, Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
