.class public final enum Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting$EnumTimerPeriod;
.super Ljava/lang/Enum;
.source "ThreeDimensionSetting.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EnumTimerPeriod"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting$EnumTimerPeriod;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting$EnumTimerPeriod;

.field public static final enum E_TIMER_PERIOD_120:Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting$EnumTimerPeriod;

.field public static final enum E_TIMER_PERIOD_30:Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting$EnumTimerPeriod;

.field public static final enum E_TIMER_PERIOD_60:Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting$EnumTimerPeriod;

.field public static final enum E_TIMER_PERIOD_90:Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting$EnumTimerPeriod;

.field public static final enum E_TIMER_PERIOD_NUM:Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting$EnumTimerPeriod;

.field public static final enum E_TIMER_PERIOD_OFF:Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting$EnumTimerPeriod;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting$EnumTimerPeriod;

    const-string v1, "E_TIMER_PERIOD_OFF"

    invoke-direct {v0, v1, v4, v4}, Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting$EnumTimerPeriod;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting$EnumTimerPeriod;->E_TIMER_PERIOD_OFF:Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting$EnumTimerPeriod;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting$EnumTimerPeriod;

    const-string v1, "E_TIMER_PERIOD_30"

    const v2, 0x1b7740

    invoke-direct {v0, v1, v5, v2}, Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting$EnumTimerPeriod;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting$EnumTimerPeriod;->E_TIMER_PERIOD_30:Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting$EnumTimerPeriod;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting$EnumTimerPeriod;

    const-string v1, "E_TIMER_PERIOD_60"

    const v2, 0x36ee80

    invoke-direct {v0, v1, v6, v2}, Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting$EnumTimerPeriod;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting$EnumTimerPeriod;->E_TIMER_PERIOD_60:Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting$EnumTimerPeriod;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting$EnumTimerPeriod;

    const-string v1, "E_TIMER_PERIOD_90"

    const v2, 0x5265c0

    invoke-direct {v0, v1, v7, v2}, Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting$EnumTimerPeriod;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting$EnumTimerPeriod;->E_TIMER_PERIOD_90:Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting$EnumTimerPeriod;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting$EnumTimerPeriod;

    const-string v1, "E_TIMER_PERIOD_120"

    const v2, 0x6ddd00

    invoke-direct {v0, v1, v8, v2}, Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting$EnumTimerPeriod;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting$EnumTimerPeriod;->E_TIMER_PERIOD_120:Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting$EnumTimerPeriod;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting$EnumTimerPeriod;

    const-string v1, "E_TIMER_PERIOD_NUM"

    const/4 v2, 0x5

    const v3, 0x6ddd01

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting$EnumTimerPeriod;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting$EnumTimerPeriod;->E_TIMER_PERIOD_NUM:Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting$EnumTimerPeriod;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting$EnumTimerPeriod;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting$EnumTimerPeriod;->E_TIMER_PERIOD_OFF:Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting$EnumTimerPeriod;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting$EnumTimerPeriod;->E_TIMER_PERIOD_30:Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting$EnumTimerPeriod;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting$EnumTimerPeriod;->E_TIMER_PERIOD_60:Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting$EnumTimerPeriod;

    aput-object v1, v0, v6

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting$EnumTimerPeriod;->E_TIMER_PERIOD_90:Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting$EnumTimerPeriod;

    aput-object v1, v0, v7

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting$EnumTimerPeriod;->E_TIMER_PERIOD_120:Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting$EnumTimerPeriod;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting$EnumTimerPeriod;->E_TIMER_PERIOD_NUM:Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting$EnumTimerPeriod;

    aput-object v2, v0, v1

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting$EnumTimerPeriod;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting$EnumTimerPeriod;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting$EnumTimerPeriod;->value:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting$EnumTimerPeriod;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting$EnumTimerPeriod;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting$EnumTimerPeriod;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting$EnumTimerPeriod;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting$EnumTimerPeriod;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting$EnumTimerPeriod;

    invoke-virtual {v0}, [Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting$EnumTimerPeriod;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting$EnumTimerPeriod;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting$EnumTimerPeriod;->value:I

    return v0
.end method
