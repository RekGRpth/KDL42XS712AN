.class public final enum Lcom/mstar/android/tvapi/common/vo/EnumVideo3DArcType;
.super Ljava/lang/Enum;
.source "EnumVideo3DArcType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/tvapi/common/vo/EnumVideo3DArcType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumVideo3DArcType;

.field public static final enum E_AUTO:Lcom/mstar/android/tvapi/common/vo/EnumVideo3DArcType;

.field public static final enum E_CENTER:Lcom/mstar/android/tvapi/common/vo/EnumVideo3DArcType;

.field public static final enum E_FULL:Lcom/mstar/android/tvapi/common/vo/EnumVideo3DArcType;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumVideo3DArcType;

    const-string v1, "E_FULL"

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumVideo3DArcType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumVideo3DArcType;->E_FULL:Lcom/mstar/android/tvapi/common/vo/EnumVideo3DArcType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumVideo3DArcType;

    const-string v1, "E_AUTO"

    invoke-direct {v0, v1, v3}, Lcom/mstar/android/tvapi/common/vo/EnumVideo3DArcType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumVideo3DArcType;->E_AUTO:Lcom/mstar/android/tvapi/common/vo/EnumVideo3DArcType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumVideo3DArcType;

    const-string v1, "E_CENTER"

    invoke-direct {v0, v1, v4}, Lcom/mstar/android/tvapi/common/vo/EnumVideo3DArcType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumVideo3DArcType;->E_CENTER:Lcom/mstar/android/tvapi/common/vo/EnumVideo3DArcType;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/mstar/android/tvapi/common/vo/EnumVideo3DArcType;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumVideo3DArcType;->E_FULL:Lcom/mstar/android/tvapi/common/vo/EnumVideo3DArcType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumVideo3DArcType;->E_AUTO:Lcom/mstar/android/tvapi/common/vo/EnumVideo3DArcType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumVideo3DArcType;->E_CENTER:Lcom/mstar/android/tvapi/common/vo/EnumVideo3DArcType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumVideo3DArcType;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumVideo3DArcType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/tvapi/common/vo/EnumVideo3DArcType;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/tvapi/common/vo/EnumVideo3DArcType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/common/vo/EnumVideo3DArcType;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/tvapi/common/vo/EnumVideo3DArcType;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/EnumVideo3DArcType;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumVideo3DArcType;

    invoke-virtual {v0}, [Lcom/mstar/android/tvapi/common/vo/EnumVideo3DArcType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/tvapi/common/vo/EnumVideo3DArcType;

    return-object v0
.end method
