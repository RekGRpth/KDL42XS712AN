.class public Lcom/mstar/android/tvapi/common/vo/UserMmSetting;
.super Ljava/lang/Object;
.source "UserMmSetting.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mstar/android/tvapi/common/vo/UserMmSetting;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public previewOn:B

.field public reserved:B

.field public resumePlay:B

.field public slideShowMode:S

.field public slideShowTime:S

.field public subtitleBgColor:S

.field public subtitleFontColor:S

.field public subtitleSpecific:S


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/UserMmSetting$1;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/common/vo/UserMmSetting$1;-><init>()V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/UserMmSetting;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-short v0, p0, Lcom/mstar/android/tvapi/common/vo/UserMmSetting;->subtitleSpecific:S

    iput-short v0, p0, Lcom/mstar/android/tvapi/common/vo/UserMmSetting;->subtitleBgColor:S

    iput-short v0, p0, Lcom/mstar/android/tvapi/common/vo/UserMmSetting;->subtitleFontColor:S

    iput-short v0, p0, Lcom/mstar/android/tvapi/common/vo/UserMmSetting;->slideShowTime:S

    iput-short v0, p0, Lcom/mstar/android/tvapi/common/vo/UserMmSetting;->slideShowMode:S

    iput-byte v0, p0, Lcom/mstar/android/tvapi/common/vo/UserMmSetting;->previewOn:B

    iput-byte v0, p0, Lcom/mstar/android/tvapi/common/vo/UserMmSetting;->resumePlay:B

    iput-byte v0, p0, Lcom/mstar/android/tvapi/common/vo/UserMmSetting;->reserved:B

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1    # Landroid/os/Parcel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/common/vo/UserMmSetting;->subtitleSpecific:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/common/vo/UserMmSetting;->subtitleBgColor:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/common/vo/UserMmSetting;->subtitleFontColor:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/common/vo/UserMmSetting;->slideShowTime:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/common/vo/UserMmSetting;->slideShowMode:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    iput-byte v0, p0, Lcom/mstar/android/tvapi/common/vo/UserMmSetting;->previewOn:B

    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    iput-byte v0, p0, Lcom/mstar/android/tvapi/common/vo/UserMmSetting;->resumePlay:B

    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    iput-byte v0, p0, Lcom/mstar/android/tvapi/common/vo/UserMmSetting;->reserved:B

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget-short v0, p0, Lcom/mstar/android/tvapi/common/vo/UserMmSetting;->subtitleSpecific:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/common/vo/UserMmSetting;->subtitleBgColor:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/common/vo/UserMmSetting;->subtitleFontColor:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/common/vo/UserMmSetting;->slideShowTime:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/common/vo/UserMmSetting;->slideShowMode:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-byte v0, p0, Lcom/mstar/android/tvapi/common/vo/UserMmSetting;->previewOn:B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    iget-byte v0, p0, Lcom/mstar/android/tvapi/common/vo/UserMmSetting;->resumePlay:B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    iget-byte v0, p0, Lcom/mstar/android/tvapi/common/vo/UserMmSetting;->reserved:B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    return-void
.end method
