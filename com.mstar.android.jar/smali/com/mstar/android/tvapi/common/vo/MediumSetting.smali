.class public Lcom/mstar/android/tvapi/common/vo/MediumSetting;
.super Ljava/lang/Object;
.source "MediumSetting.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mstar/android/tvapi/common/vo/MediumSetting;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public antennaPower:C

.field public antennaType:I

.field public cableSystem:I

.field public checkSum:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/MediumSetting$1;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/common/vo/MediumSetting$1;-><init>()V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/MediumSetting;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/MediumSetting;->checkSum:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/MediumSetting;->antennaType:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/MediumSetting;->cableSystem:I

    iput-char v0, p0, Lcom/mstar/android/tvapi/common/vo/MediumSetting;->antennaPower:C

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1    # Landroid/os/Parcel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/MediumSetting;->checkSum:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/MediumSetting;->antennaType:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/MediumSetting;->cableSystem:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-char v0, v0

    iput-char v0, p0, Lcom/mstar/android/tvapi/common/vo/MediumSetting;->antennaPower:C

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/MediumSetting;->checkSum:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/MediumSetting;->antennaType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/MediumSetting;->cableSystem:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-char v0, p0, Lcom/mstar/android/tvapi/common/vo/MediumSetting;->antennaPower:C

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
