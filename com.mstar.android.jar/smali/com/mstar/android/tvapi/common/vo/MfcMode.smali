.class public Lcom/mstar/android/tvapi/common/vo/MfcMode;
.super Ljava/lang/Object;
.source "MfcMode.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mstar/android/tvapi/common/vo/MfcMode;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public mfc:Lcom/mstar/android/tvapi/common/vo/EnumMfc;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/MfcMode$1;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/common/vo/MfcMode$1;-><init>()V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/MfcMode;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .param p1    # Landroid/os/Parcel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/EnumMfc;->E_HIGH:Lcom/mstar/android/tvapi/common/vo/EnumMfc;

    iput-object v0, p0, Lcom/mstar/android/tvapi/common/vo/MfcMode;->mfc:Lcom/mstar/android/tvapi/common/vo/EnumMfc;

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumMfc;->values()[Lcom/mstar/android/tvapi/common/vo/EnumMfc;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/mstar/android/tvapi/common/vo/MfcMode;->mfc:Lcom/mstar/android/tvapi/common/vo/EnumMfc;

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget-object v0, p0, Lcom/mstar/android/tvapi/common/vo/MfcMode;->mfc:Lcom/mstar/android/tvapi/common/vo/EnumMfc;

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/EnumMfc;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
