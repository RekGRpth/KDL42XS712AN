.class public final enum Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;
.super Ljava/lang/Enum;
.source "EnumPvrStatus.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

.field public static final enum E_ERROR:Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

.field public static final enum E_ERROR_BAD_VIDEO_AUDIO:Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

.field public static final enum E_ERROR_CANT_RECORD_DATA_ONLY_PROGRAM:Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

.field public static final enum E_ERROR_CI_PLUS_COPY_PROTECTION:Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

.field public static final enum E_ERROR_CI_PLUS_UNPLUGIN:Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

.field public static final enum E_ERROR_CI_PLUS_UNSUPPORT:Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

.field public static final enum E_ERROR_GINGA_RUNNING:Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

.field public static final enum E_ERROR_NO_DISK_DETECTED:Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

.field public static final enum E_ERROR_NO_SIGNAL:Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

.field public static final enum E_ERROR_READ_ONLY_FILE_SYSTEM:Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

.field public static final enum E_ERROR_RECORD_OUT_OF_DISK_SPACE:Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

.field public static final enum E_ERROR_RETENTION_LIMIT_EXPIRED:Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

.field public static final enum E_ERROR_TIMESHIFT_OUT_OF_DISK_SPACE:Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

.field public static final enum E_ERROR_UNSUPPORTED_FILE_SYSTEM:Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

.field public static final enum E_NUM:Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

.field public static final enum E_SUCCESS:Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

.field public static final enum E_SUCCESS_CI_PLUS_COPY_PROTECTION:Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    const-string v1, "E_SUCCESS"

    invoke-direct {v0, v1, v3}, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;->E_SUCCESS:Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    const-string v1, "E_ERROR_NO_DISK_DETECTED"

    invoke-direct {v0, v1, v4}, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;->E_ERROR_NO_DISK_DETECTED:Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    const-string v1, "E_ERROR_UNSUPPORTED_FILE_SYSTEM"

    invoke-direct {v0, v1, v5}, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;->E_ERROR_UNSUPPORTED_FILE_SYSTEM:Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    const-string v1, "E_ERROR_READ_ONLY_FILE_SYSTEM"

    invoke-direct {v0, v1, v6}, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;->E_ERROR_READ_ONLY_FILE_SYSTEM:Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    const-string v1, "E_ERROR_TIMESHIFT_OUT_OF_DISK_SPACE"

    invoke-direct {v0, v1, v7}, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;->E_ERROR_TIMESHIFT_OUT_OF_DISK_SPACE:Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    const-string v1, "E_SUCCESS_CI_PLUS_COPY_PROTECTION"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;->E_SUCCESS_CI_PLUS_COPY_PROTECTION:Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    const-string v1, "E_ERROR_CI_PLUS_COPY_PROTECTION"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;->E_ERROR_CI_PLUS_COPY_PROTECTION:Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    const-string v1, "E_ERROR_CI_PLUS_UNPLUGIN"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;->E_ERROR_CI_PLUS_UNPLUGIN:Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    const-string v1, "E_ERROR_CI_PLUS_UNSUPPORT"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;->E_ERROR_CI_PLUS_UNSUPPORT:Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    const-string v1, "E_ERROR_CANT_RECORD_DATA_ONLY_PROGRAM"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;->E_ERROR_CANT_RECORD_DATA_ONLY_PROGRAM:Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    const-string v1, "E_ERROR_NO_SIGNAL"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;->E_ERROR_NO_SIGNAL:Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    const-string v1, "E_ERROR_RECORD_OUT_OF_DISK_SPACE"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;->E_ERROR_RECORD_OUT_OF_DISK_SPACE:Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    const-string v1, "E_ERROR_GINGA_RUNNING"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;->E_ERROR_GINGA_RUNNING:Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    const-string v1, "E_ERROR_BAD_VIDEO_AUDIO"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;->E_ERROR_BAD_VIDEO_AUDIO:Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    const-string v1, "E_ERROR_RETENTION_LIMIT_EXPIRED"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;->E_ERROR_RETENTION_LIMIT_EXPIRED:Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    const-string v1, "E_ERROR"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;->E_ERROR:Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    const-string v1, "E_NUM"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;->E_NUM:Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    const/16 v0, 0x11

    new-array v0, v0, [Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;->E_SUCCESS:Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;->E_ERROR_NO_DISK_DETECTED:Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;->E_ERROR_UNSUPPORTED_FILE_SYSTEM:Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;->E_ERROR_READ_ONLY_FILE_SYSTEM:Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    aput-object v1, v0, v6

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;->E_ERROR_TIMESHIFT_OUT_OF_DISK_SPACE:Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;->E_SUCCESS_CI_PLUS_COPY_PROTECTION:Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;->E_ERROR_CI_PLUS_COPY_PROTECTION:Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;->E_ERROR_CI_PLUS_UNPLUGIN:Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;->E_ERROR_CI_PLUS_UNSUPPORT:Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;->E_ERROR_CANT_RECORD_DATA_ONLY_PROGRAM:Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;->E_ERROR_NO_SIGNAL:Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;->E_ERROR_RECORD_OUT_OF_DISK_SPACE:Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;->E_ERROR_GINGA_RUNNING:Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;->E_ERROR_BAD_VIDEO_AUDIO:Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;->E_ERROR_RETENTION_LIMIT_EXPIRED:Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;->E_ERROR:Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;->E_NUM:Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    aput-object v2, v0, v1

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    invoke-virtual {v0}, [Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    return-object v0
.end method
