.class public final enum Lcom/mstar/android/tvapi/common/vo/EnumMfcOsdWindow;
.super Ljava/lang/Enum;
.source "EnumMfcOsdWindow.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/tvapi/common/vo/EnumMfcOsdWindow;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumMfcOsdWindow;

.field public static final enum MFC_OSD_WIN0:Lcom/mstar/android/tvapi/common/vo/EnumMfcOsdWindow;

.field public static final enum MFC_OSD_WIN1:Lcom/mstar/android/tvapi/common/vo/EnumMfcOsdWindow;

.field public static final enum MFC_OSD_WIN2:Lcom/mstar/android/tvapi/common/vo/EnumMfcOsdWindow;

.field public static final enum MFC_OSD_WIN3:Lcom/mstar/android/tvapi/common/vo/EnumMfcOsdWindow;

.field public static final enum MFC_OSD_WIN4:Lcom/mstar/android/tvapi/common/vo/EnumMfcOsdWindow;

.field public static final enum MFC_OSD_WIN_NUM:Lcom/mstar/android/tvapi/common/vo/EnumMfcOsdWindow;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumMfcOsdWindow;

    const-string v1, "MFC_OSD_WIN0"

    invoke-direct {v0, v1, v3}, Lcom/mstar/android/tvapi/common/vo/EnumMfcOsdWindow;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumMfcOsdWindow;->MFC_OSD_WIN0:Lcom/mstar/android/tvapi/common/vo/EnumMfcOsdWindow;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumMfcOsdWindow;

    const-string v1, "MFC_OSD_WIN1"

    invoke-direct {v0, v1, v4}, Lcom/mstar/android/tvapi/common/vo/EnumMfcOsdWindow;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumMfcOsdWindow;->MFC_OSD_WIN1:Lcom/mstar/android/tvapi/common/vo/EnumMfcOsdWindow;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumMfcOsdWindow;

    const-string v1, "MFC_OSD_WIN2"

    invoke-direct {v0, v1, v5}, Lcom/mstar/android/tvapi/common/vo/EnumMfcOsdWindow;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumMfcOsdWindow;->MFC_OSD_WIN2:Lcom/mstar/android/tvapi/common/vo/EnumMfcOsdWindow;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumMfcOsdWindow;

    const-string v1, "MFC_OSD_WIN3"

    invoke-direct {v0, v1, v6}, Lcom/mstar/android/tvapi/common/vo/EnumMfcOsdWindow;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumMfcOsdWindow;->MFC_OSD_WIN3:Lcom/mstar/android/tvapi/common/vo/EnumMfcOsdWindow;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumMfcOsdWindow;

    const-string v1, "MFC_OSD_WIN4"

    invoke-direct {v0, v1, v7}, Lcom/mstar/android/tvapi/common/vo/EnumMfcOsdWindow;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumMfcOsdWindow;->MFC_OSD_WIN4:Lcom/mstar/android/tvapi/common/vo/EnumMfcOsdWindow;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumMfcOsdWindow;

    const-string v1, "MFC_OSD_WIN_NUM"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumMfcOsdWindow;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumMfcOsdWindow;->MFC_OSD_WIN_NUM:Lcom/mstar/android/tvapi/common/vo/EnumMfcOsdWindow;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/mstar/android/tvapi/common/vo/EnumMfcOsdWindow;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumMfcOsdWindow;->MFC_OSD_WIN0:Lcom/mstar/android/tvapi/common/vo/EnumMfcOsdWindow;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumMfcOsdWindow;->MFC_OSD_WIN1:Lcom/mstar/android/tvapi/common/vo/EnumMfcOsdWindow;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumMfcOsdWindow;->MFC_OSD_WIN2:Lcom/mstar/android/tvapi/common/vo/EnumMfcOsdWindow;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumMfcOsdWindow;->MFC_OSD_WIN3:Lcom/mstar/android/tvapi/common/vo/EnumMfcOsdWindow;

    aput-object v1, v0, v6

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumMfcOsdWindow;->MFC_OSD_WIN4:Lcom/mstar/android/tvapi/common/vo/EnumMfcOsdWindow;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumMfcOsdWindow;->MFC_OSD_WIN_NUM:Lcom/mstar/android/tvapi/common/vo/EnumMfcOsdWindow;

    aput-object v2, v0, v1

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumMfcOsdWindow;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumMfcOsdWindow;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/tvapi/common/vo/EnumMfcOsdWindow;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/tvapi/common/vo/EnumMfcOsdWindow;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/common/vo/EnumMfcOsdWindow;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/tvapi/common/vo/EnumMfcOsdWindow;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/EnumMfcOsdWindow;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumMfcOsdWindow;

    invoke-virtual {v0}, [Lcom/mstar/android/tvapi/common/vo/EnumMfcOsdWindow;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/tvapi/common/vo/EnumMfcOsdWindow;

    return-object v0
.end method
