.class public final enum Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;
.super Ljava/lang/Enum;
.source "EnumFirstServiceType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;

.field public static final enum E_AC_DC_BOOT:Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;

.field public static final enum E_AUTO_SCAN:Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;

.field public static final enum E_DEFAULT:Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;

.field public static final enum E_MENU_SCAN:Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;

.field public static final enum E_ON_TIME_BOOT:Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;

    const-string v1, "E_ON_TIME_BOOT"

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;->E_ON_TIME_BOOT:Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;

    const-string v1, "E_AC_DC_BOOT"

    invoke-direct {v0, v1, v3}, Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;->E_AC_DC_BOOT:Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;

    const-string v1, "E_AUTO_SCAN"

    invoke-direct {v0, v1, v4}, Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;->E_AUTO_SCAN:Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;

    const-string v1, "E_MENU_SCAN"

    invoke-direct {v0, v1, v5}, Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;->E_MENU_SCAN:Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;

    const-string v1, "E_DEFAULT"

    invoke-direct {v0, v1, v6}, Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;->E_DEFAULT:Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;->E_ON_TIME_BOOT:Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;->E_AC_DC_BOOT:Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;->E_AUTO_SCAN:Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;->E_MENU_SCAN:Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;->E_DEFAULT:Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;

    aput-object v1, v0, v6

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;

    invoke-virtual {v0}, [Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;

    return-object v0
.end method
