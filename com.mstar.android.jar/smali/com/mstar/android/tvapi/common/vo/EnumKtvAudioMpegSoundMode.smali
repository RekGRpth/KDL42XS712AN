.class public final enum Lcom/mstar/android/tvapi/common/vo/EnumKtvAudioMpegSoundMode;
.super Ljava/lang/Enum;
.source "EnumKtvAudioMpegSoundMode.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/tvapi/common/vo/EnumKtvAudioMpegSoundMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumKtvAudioMpegSoundMode;

.field public static final enum E_KTV_AUD_MPEG_SOUNDMODE_LL:Lcom/mstar/android/tvapi/common/vo/EnumKtvAudioMpegSoundMode;

.field public static final enum E_KTV_AUD_MPEG_SOUNDMODE_LR:Lcom/mstar/android/tvapi/common/vo/EnumKtvAudioMpegSoundMode;

.field public static final enum E_KTV_AUD_MPEG_SOUNDMODE_MIX_LR:Lcom/mstar/android/tvapi/common/vo/EnumKtvAudioMpegSoundMode;

.field public static final enum E_KTV_AUD_MPEG_SOUNDMODE_RR:Lcom/mstar/android/tvapi/common/vo/EnumKtvAudioMpegSoundMode;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumKtvAudioMpegSoundMode;

    const-string v1, "E_KTV_AUD_MPEG_SOUNDMODE_LL"

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumKtvAudioMpegSoundMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumKtvAudioMpegSoundMode;->E_KTV_AUD_MPEG_SOUNDMODE_LL:Lcom/mstar/android/tvapi/common/vo/EnumKtvAudioMpegSoundMode;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumKtvAudioMpegSoundMode;

    const-string v1, "E_KTV_AUD_MPEG_SOUNDMODE_RR"

    invoke-direct {v0, v1, v3}, Lcom/mstar/android/tvapi/common/vo/EnumKtvAudioMpegSoundMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumKtvAudioMpegSoundMode;->E_KTV_AUD_MPEG_SOUNDMODE_RR:Lcom/mstar/android/tvapi/common/vo/EnumKtvAudioMpegSoundMode;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumKtvAudioMpegSoundMode;

    const-string v1, "E_KTV_AUD_MPEG_SOUNDMODE_LR"

    invoke-direct {v0, v1, v4}, Lcom/mstar/android/tvapi/common/vo/EnumKtvAudioMpegSoundMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumKtvAudioMpegSoundMode;->E_KTV_AUD_MPEG_SOUNDMODE_LR:Lcom/mstar/android/tvapi/common/vo/EnumKtvAudioMpegSoundMode;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumKtvAudioMpegSoundMode;

    const-string v1, "E_KTV_AUD_MPEG_SOUNDMODE_MIX_LR"

    invoke-direct {v0, v1, v5}, Lcom/mstar/android/tvapi/common/vo/EnumKtvAudioMpegSoundMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumKtvAudioMpegSoundMode;->E_KTV_AUD_MPEG_SOUNDMODE_MIX_LR:Lcom/mstar/android/tvapi/common/vo/EnumKtvAudioMpegSoundMode;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/mstar/android/tvapi/common/vo/EnumKtvAudioMpegSoundMode;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumKtvAudioMpegSoundMode;->E_KTV_AUD_MPEG_SOUNDMODE_LL:Lcom/mstar/android/tvapi/common/vo/EnumKtvAudioMpegSoundMode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumKtvAudioMpegSoundMode;->E_KTV_AUD_MPEG_SOUNDMODE_RR:Lcom/mstar/android/tvapi/common/vo/EnumKtvAudioMpegSoundMode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumKtvAudioMpegSoundMode;->E_KTV_AUD_MPEG_SOUNDMODE_LR:Lcom/mstar/android/tvapi/common/vo/EnumKtvAudioMpegSoundMode;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumKtvAudioMpegSoundMode;->E_KTV_AUD_MPEG_SOUNDMODE_MIX_LR:Lcom/mstar/android/tvapi/common/vo/EnumKtvAudioMpegSoundMode;

    aput-object v1, v0, v5

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumKtvAudioMpegSoundMode;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumKtvAudioMpegSoundMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/tvapi/common/vo/EnumKtvAudioMpegSoundMode;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/tvapi/common/vo/EnumKtvAudioMpegSoundMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/common/vo/EnumKtvAudioMpegSoundMode;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/tvapi/common/vo/EnumKtvAudioMpegSoundMode;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/EnumKtvAudioMpegSoundMode;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumKtvAudioMpegSoundMode;

    invoke-virtual {v0}, [Lcom/mstar/android/tvapi/common/vo/EnumKtvAudioMpegSoundMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/tvapi/common/vo/EnumKtvAudioMpegSoundMode;

    return-object v0
.end method
