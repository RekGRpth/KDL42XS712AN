.class public final enum Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;
.super Ljava/lang/Enum;
.source "TvOsType.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mstar/android/tvapi/common/vo/TvOsType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EnumCountry"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

.field public static final enum E_ALGERIA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

.field public static final enum E_ARAB:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

.field public static final enum E_ARGENTINA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

.field public static final enum E_AUSTRALIA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

.field public static final enum E_AUSTRIA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

.field public static final enum E_AZERBAIJAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

.field public static final enum E_BELGIUM:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

.field public static final enum E_BELIZE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

.field public static final enum E_BOLIVIA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

.field public static final enum E_BRAZIL:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

.field public static final enum E_BULGARIA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

.field public static final enum E_CANADA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

.field public static final enum E_CHILE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

.field public static final enum E_CHINA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

.field public static final enum E_COSTARICA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

.field public static final enum E_COUNTRY_NUM:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

.field public static final enum E_CROATIA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

.field public static final enum E_CZECH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

.field public static final enum E_DENMARK:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

.field public static final enum E_ECUADOR:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

.field public static final enum E_EGYPT:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

.field public static final enum E_ESTONIA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

.field public static final enum E_ETHIOPIA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

.field public static final enum E_FIJI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

.field public static final enum E_FINLAND:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

.field public static final enum E_FRANCE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

.field public static final enum E_GERMANY:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

.field public static final enum E_GREECE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

.field public static final enum E_GUATEMALA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

.field public static final enum E_HEBREW:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

.field public static final enum E_HUNGARY:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

.field public static final enum E_INDIA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

.field public static final enum E_INDONESIA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

.field public static final enum E_IRAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

.field public static final enum E_IRAQ:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

.field public static final enum E_IRELAND:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

.field public static final enum E_ISRAEL:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

.field public static final enum E_ITALY:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

.field public static final enum E_JAPAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

.field public static final enum E_JORDAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

.field public static final enum E_KUWAIT:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

.field public static final enum E_LATVIA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

.field public static final enum E_LIBYA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

.field public static final enum E_LITHUANIA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

.field public static final enum E_LUXEMBOURG:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

.field public static final enum E_MALDIVES:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

.field public static final enum E_MEXICO:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

.field public static final enum E_MOROCCO:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

.field public static final enum E_NAMIBIA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

.field public static final enum E_NETHERLANDS:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

.field public static final enum E_NEWZEALAND:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

.field public static final enum E_NICARAGUA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

.field public static final enum E_NIGERIA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

.field public static final enum E_NORWAY:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

.field public static final enum E_OTHERS:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

.field public static final enum E_PAKISTAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

.field public static final enum E_PARAGUAY:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

.field public static final enum E_PERU:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

.field public static final enum E_PHILIPPINES:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

.field public static final enum E_POLAND:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

.field public static final enum E_PORTUGAL:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

.field public static final enum E_QATAR:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

.field public static final enum E_RUMANIA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

.field public static final enum E_RUSSIA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

.field public static final enum E_SAUDI_ARABIA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

.field public static final enum E_SERBIA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

.field public static final enum E_SLOVAKIA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

.field public static final enum E_SLOVENIA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

.field public static final enum E_SOUTHAFRICA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

.field public static final enum E_SOUTHKOREA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

.field public static final enum E_SPAIN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

.field public static final enum E_SWEDEN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

.field public static final enum E_SWITZERLAND:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

.field public static final enum E_TAIWAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

.field public static final enum E_TAJIKISTAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

.field public static final enum E_THAILAND:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

.field public static final enum E_TUNIS:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

.field public static final enum E_TURKEY:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

.field public static final enum E_UK:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

.field public static final enum E_URUGUAY:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

.field public static final enum E_US:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

.field public static final enum E_UZBEK:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

.field public static final enum E_VENEZUELA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

.field public static final enum E_YEMEN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

.field public static final enum E_ZEMBABWE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    const-string v1, "E_AUSTRALIA"

    invoke-direct {v0, v1, v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_AUSTRALIA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    const-string v1, "E_AUSTRIA"

    invoke-direct {v0, v1, v4}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_AUSTRIA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    const-string v1, "E_BELGIUM"

    invoke-direct {v0, v1, v5}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_BELGIUM:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    const-string v1, "E_BULGARIA"

    invoke-direct {v0, v1, v6}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_BULGARIA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    const-string v1, "E_CROATIA"

    invoke-direct {v0, v1, v7}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_CROATIA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    const-string v1, "E_CZECH"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_CZECH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    const-string v1, "E_DENMARK"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_DENMARK:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    const-string v1, "E_FINLAND"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_FINLAND:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    const-string v1, "E_FRANCE"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_FRANCE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    const-string v1, "E_GERMANY"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_GERMANY:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    const-string v1, "E_GREECE"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_GREECE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    const-string v1, "E_HUNGARY"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_HUNGARY:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    const-string v1, "E_ITALY"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_ITALY:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    const-string v1, "E_LUXEMBOURG"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_LUXEMBOURG:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    const-string v1, "E_NETHERLANDS"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_NETHERLANDS:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    const-string v1, "E_NORWAY"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_NORWAY:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    const-string v1, "E_POLAND"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_POLAND:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    const-string v1, "E_PORTUGAL"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_PORTUGAL:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    const-string v1, "E_RUMANIA"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_RUMANIA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    const-string v1, "E_RUSSIA"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_RUSSIA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    const-string v1, "E_SERBIA"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_SERBIA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    const-string v1, "E_SLOVENIA"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_SLOVENIA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    const-string v1, "E_SPAIN"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_SPAIN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    const-string v1, "E_SWEDEN"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_SWEDEN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    const-string v1, "E_SWITZERLAND"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_SWITZERLAND:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    const-string v1, "E_UK"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_UK:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    const-string v1, "E_NEWZEALAND"

    const/16 v2, 0x1a

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_NEWZEALAND:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    const-string v1, "E_ARAB"

    const/16 v2, 0x1b

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_ARAB:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    const-string v1, "E_ESTONIA"

    const/16 v2, 0x1c

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_ESTONIA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    const-string v1, "E_HEBREW"

    const/16 v2, 0x1d

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_HEBREW:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    const-string v1, "E_LATVIA"

    const/16 v2, 0x1e

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_LATVIA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    const-string v1, "E_SLOVAKIA"

    const/16 v2, 0x1f

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_SLOVAKIA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    const-string v1, "E_TURKEY"

    const/16 v2, 0x20

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_TURKEY:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    const-string v1, "E_IRELAND"

    const/16 v2, 0x21

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_IRELAND:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    const-string v1, "E_JAPAN"

    const/16 v2, 0x22

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_JAPAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    const-string v1, "E_PHILIPPINES"

    const/16 v2, 0x23

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_PHILIPPINES:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    const-string v1, "E_THAILAND"

    const/16 v2, 0x24

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_THAILAND:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    const-string v1, "E_MALDIVES"

    const/16 v2, 0x25

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_MALDIVES:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    const-string v1, "E_URUGUAY"

    const/16 v2, 0x26

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_URUGUAY:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    const-string v1, "E_PERU"

    const/16 v2, 0x27

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_PERU:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    const-string v1, "E_ARGENTINA"

    const/16 v2, 0x28

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_ARGENTINA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    const-string v1, "E_CHILE"

    const/16 v2, 0x29

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_CHILE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    const-string v1, "E_VENEZUELA"

    const/16 v2, 0x2a

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_VENEZUELA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    const-string v1, "E_ECUADOR"

    const/16 v2, 0x2b

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_ECUADOR:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    const-string v1, "E_COSTARICA"

    const/16 v2, 0x2c

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_COSTARICA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    const-string v1, "E_PARAGUAY"

    const/16 v2, 0x2d

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_PARAGUAY:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    const-string v1, "E_BOLIVIA"

    const/16 v2, 0x2e

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_BOLIVIA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    const-string v1, "E_BELIZE"

    const/16 v2, 0x2f

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_BELIZE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    const-string v1, "E_NICARAGUA"

    const/16 v2, 0x30

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_NICARAGUA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    const-string v1, "E_GUATEMALA"

    const/16 v2, 0x31

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_GUATEMALA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    const-string v1, "E_CHINA"

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_CHINA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    const-string v1, "E_TAIWAN"

    const/16 v2, 0x33

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_TAIWAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    const-string v1, "E_BRAZIL"

    const/16 v2, 0x34

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_BRAZIL:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    const-string v1, "E_CANADA"

    const/16 v2, 0x35

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_CANADA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    const-string v1, "E_MEXICO"

    const/16 v2, 0x36

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_MEXICO:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    const-string v1, "E_US"

    const/16 v2, 0x37

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_US:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    const-string v1, "E_SOUTHKOREA"

    const/16 v2, 0x38

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_SOUTHKOREA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    const-string v1, "E_FIJI"

    const/16 v2, 0x39

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_FIJI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    const-string v1, "E_UZBEK"

    const/16 v2, 0x3a

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_UZBEK:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    const-string v1, "E_TAJIKISTAN"

    const/16 v2, 0x3b

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_TAJIKISTAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    const-string v1, "E_ETHIOPIA"

    const/16 v2, 0x3c

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_ETHIOPIA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    const-string v1, "E_AZERBAIJAN"

    const/16 v2, 0x3d

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_AZERBAIJAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    const-string v1, "E_SOUTHAFRICA"

    const/16 v2, 0x3e

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_SOUTHAFRICA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    const-string v1, "E_ALGERIA"

    const/16 v2, 0x3f

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_ALGERIA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    const-string v1, "E_EGYPT"

    const/16 v2, 0x40

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_EGYPT:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    const-string v1, "E_SAUDI_ARABIA"

    const/16 v2, 0x41

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_SAUDI_ARABIA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    const-string v1, "E_IRAN"

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_IRAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    const-string v1, "E_IRAQ"

    const/16 v2, 0x43

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_IRAQ:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    const-string v1, "E_NAMIBIA"

    const/16 v2, 0x44

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_NAMIBIA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    const-string v1, "E_JORDAN"

    const/16 v2, 0x45

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_JORDAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    const-string v1, "E_KUWAIT"

    const/16 v2, 0x46

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_KUWAIT:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    const-string v1, "E_INDONESIA"

    const/16 v2, 0x47

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_INDONESIA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    const-string v1, "E_ISRAEL"

    const/16 v2, 0x48

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_ISRAEL:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    const-string v1, "E_QATAR"

    const/16 v2, 0x49

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_QATAR:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    const-string v1, "E_NIGERIA"

    const/16 v2, 0x4a

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_NIGERIA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    const-string v1, "E_ZEMBABWE"

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_ZEMBABWE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    const-string v1, "E_LITHUANIA"

    const/16 v2, 0x4c

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_LITHUANIA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    const-string v1, "E_MOROCCO"

    const/16 v2, 0x4d

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_MOROCCO:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    const-string v1, "E_TUNIS"

    const/16 v2, 0x4e

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_TUNIS:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    const-string v1, "E_INDIA"

    const/16 v2, 0x4f

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_INDIA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    const-string v1, "E_YEMEN"

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_YEMEN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    const-string v1, "E_LIBYA"

    const/16 v2, 0x51

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_LIBYA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    const-string v1, "E_PAKISTAN"

    const/16 v2, 0x52

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_PAKISTAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    const-string v1, "E_OTHERS"

    const/16 v2, 0x53

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_OTHERS:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    const-string v1, "E_COUNTRY_NUM"

    const/16 v2, 0x54

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_COUNTRY_NUM:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    const/16 v0, 0x55

    new-array v0, v0, [Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_AUSTRALIA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_AUSTRIA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_BELGIUM:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_BULGARIA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    aput-object v1, v0, v6

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_CROATIA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_CZECH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_DENMARK:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_FINLAND:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_FRANCE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_GERMANY:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_GREECE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_HUNGARY:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_ITALY:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_LUXEMBOURG:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_NETHERLANDS:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_NORWAY:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_POLAND:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_PORTUGAL:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_RUMANIA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_RUSSIA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_SERBIA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_SLOVENIA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_SPAIN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_SWEDEN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_SWITZERLAND:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_UK:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_NEWZEALAND:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_ARAB:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_ESTONIA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_HEBREW:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_LATVIA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_SLOVAKIA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_TURKEY:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_IRELAND:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_JAPAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_PHILIPPINES:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_THAILAND:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_MALDIVES:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_URUGUAY:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_PERU:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_ARGENTINA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_CHILE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_VENEZUELA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_ECUADOR:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_COSTARICA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_PARAGUAY:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_BOLIVIA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_BELIZE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    aput-object v2, v0, v1

    const/16 v1, 0x30

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_NICARAGUA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    aput-object v2, v0, v1

    const/16 v1, 0x31

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_GUATEMALA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    aput-object v2, v0, v1

    const/16 v1, 0x32

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_CHINA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    aput-object v2, v0, v1

    const/16 v1, 0x33

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_TAIWAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    aput-object v2, v0, v1

    const/16 v1, 0x34

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_BRAZIL:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    aput-object v2, v0, v1

    const/16 v1, 0x35

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_CANADA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    aput-object v2, v0, v1

    const/16 v1, 0x36

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_MEXICO:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    aput-object v2, v0, v1

    const/16 v1, 0x37

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_US:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    aput-object v2, v0, v1

    const/16 v1, 0x38

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_SOUTHKOREA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    aput-object v2, v0, v1

    const/16 v1, 0x39

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_FIJI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_UZBEK:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_TAJIKISTAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_ETHIOPIA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_AZERBAIJAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_SOUTHAFRICA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_ALGERIA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    aput-object v2, v0, v1

    const/16 v1, 0x40

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_EGYPT:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    aput-object v2, v0, v1

    const/16 v1, 0x41

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_SAUDI_ARABIA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    aput-object v2, v0, v1

    const/16 v1, 0x42

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_IRAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    aput-object v2, v0, v1

    const/16 v1, 0x43

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_IRAQ:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    aput-object v2, v0, v1

    const/16 v1, 0x44

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_NAMIBIA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    aput-object v2, v0, v1

    const/16 v1, 0x45

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_JORDAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    aput-object v2, v0, v1

    const/16 v1, 0x46

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_KUWAIT:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    aput-object v2, v0, v1

    const/16 v1, 0x47

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_INDONESIA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    aput-object v2, v0, v1

    const/16 v1, 0x48

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_ISRAEL:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    aput-object v2, v0, v1

    const/16 v1, 0x49

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_QATAR:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    aput-object v2, v0, v1

    const/16 v1, 0x4a

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_NIGERIA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    aput-object v2, v0, v1

    const/16 v1, 0x4b

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_ZEMBABWE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    aput-object v2, v0, v1

    const/16 v1, 0x4c

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_LITHUANIA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    aput-object v2, v0, v1

    const/16 v1, 0x4d

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_MOROCCO:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    aput-object v2, v0, v1

    const/16 v1, 0x4e

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_TUNIS:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    aput-object v2, v0, v1

    const/16 v1, 0x4f

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_INDIA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    aput-object v2, v0, v1

    const/16 v1, 0x50

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_YEMEN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    aput-object v2, v0, v1

    const/16 v1, 0x51

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_LIBYA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    aput-object v2, v0, v1

    const/16 v1, 0x52

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_PAKISTAN:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    aput-object v2, v0, v1

    const/16 v1, 0x53

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_OTHERS:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    aput-object v2, v0, v1

    const/16 v1, 0x54

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->E_COUNTRY_NUM:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    aput-object v2, v0, v1

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    invoke-virtual {v0}, [Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    return-object v0
.end method
