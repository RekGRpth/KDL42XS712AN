.class public final enum Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;
.super Ljava/lang/Enum;
.source "MweType.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mstar/android/tvapi/common/vo/MweType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EnumMweType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

.field public static final enum E_CENTERBASEDSCALE:Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

.field public static final enum E_DEFAULT:Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

.field public static final enum E_DYNAMICCOMPARE:Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

.field public static final enum E_ENHANCE:Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

.field public static final enum E_EN_MS_MWE_CUSTOMER1:Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

.field public static final enum E_EN_MS_MWE_CUSTOMER2:Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

.field public static final enum E_EN_MS_MWE_CUSTOMER3:Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

.field public static final enum E_EN_MS_MWE_CUSTOMER4:Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

.field public static final enum E_EN_MS_MWE_CUSTOMER5:Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

.field public static final enum E_EN_MS_MWE_CUSTOMER6:Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

.field public static final enum E_EN_MS_MWE_CUSTOMER7:Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

.field public static final enum E_EN_MS_MWE_CUSTOMER8:Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

.field public static final enum E_GOLDENEYES:Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

.field public static final enum E_HIGH_SPEED_MOVEMENT_PROCESSING:Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

.field public static final enum E_LED_BACKLIGHT_CONTROL:Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

.field public static final enum E_MOVEALONG:Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

.field public static final enum E_NUM:Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

.field public static final enum E_OFF:Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

.field public static final enum E_OPTIMIZE:Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

.field public static final enum E_SIDE_BY_SIDE:Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

.field public static final enum E_SQUAREMOVE:Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

.field public static final enum E_TRUE_COLOR_ANALYSIS_ASCENSION:Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

.field private static seq:I


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

    const-string v1, "E_OFF"

    invoke-direct {v0, v1, v4, v4}, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;->E_OFF:Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

    const-string v1, "E_OPTIMIZE"

    invoke-direct {v0, v1, v5, v5}, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;->E_OPTIMIZE:Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

    const-string v1, "E_ENHANCE"

    invoke-direct {v0, v1, v6, v6}, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;->E_ENHANCE:Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

    const-string v1, "E_SIDE_BY_SIDE"

    invoke-direct {v0, v1, v7, v7}, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;->E_SIDE_BY_SIDE:Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

    const-string v1, "E_DYNAMICCOMPARE"

    invoke-direct {v0, v1, v8, v8}, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;->E_DYNAMICCOMPARE:Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

    const-string v1, "E_CENTERBASEDSCALE"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;->E_CENTERBASEDSCALE:Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

    const-string v1, "E_MOVEALONG"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;->E_MOVEALONG:Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

    const-string v1, "E_GOLDENEYES"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;->E_GOLDENEYES:Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

    const-string v1, "E_TRUE_COLOR_ANALYSIS_ASCENSION"

    const/16 v2, 0x8

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;->E_TRUE_COLOR_ANALYSIS_ASCENSION:Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

    const-string v1, "E_LED_BACKLIGHT_CONTROL"

    const/16 v2, 0x9

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;->E_LED_BACKLIGHT_CONTROL:Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

    const-string v1, "E_HIGH_SPEED_MOVEMENT_PROCESSING"

    const/16 v2, 0xa

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;->E_HIGH_SPEED_MOVEMENT_PROCESSING:Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

    const-string v1, "E_SQUAREMOVE"

    const/16 v2, 0xb

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;->E_SQUAREMOVE:Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

    const-string v1, "E_EN_MS_MWE_CUSTOMER1"

    const/16 v2, 0xc

    const/16 v3, 0xc

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;->E_EN_MS_MWE_CUSTOMER1:Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

    const-string v1, "E_EN_MS_MWE_CUSTOMER2"

    const/16 v2, 0xd

    const/16 v3, 0xd

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;->E_EN_MS_MWE_CUSTOMER2:Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

    const-string v1, "E_EN_MS_MWE_CUSTOMER3"

    const/16 v2, 0xe

    const/16 v3, 0xe

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;->E_EN_MS_MWE_CUSTOMER3:Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

    const-string v1, "E_EN_MS_MWE_CUSTOMER4"

    const/16 v2, 0xf

    const/16 v3, 0xf

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;->E_EN_MS_MWE_CUSTOMER4:Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

    const-string v1, "E_EN_MS_MWE_CUSTOMER5"

    const/16 v2, 0x10

    const/16 v3, 0x10

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;->E_EN_MS_MWE_CUSTOMER5:Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

    const-string v1, "E_EN_MS_MWE_CUSTOMER6"

    const/16 v2, 0x11

    const/16 v3, 0x11

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;->E_EN_MS_MWE_CUSTOMER6:Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

    const-string v1, "E_EN_MS_MWE_CUSTOMER7"

    const/16 v2, 0x12

    const/16 v3, 0x12

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;->E_EN_MS_MWE_CUSTOMER7:Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

    const-string v1, "E_EN_MS_MWE_CUSTOMER8"

    const/16 v2, 0x13

    const/16 v3, 0x13

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;->E_EN_MS_MWE_CUSTOMER8:Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

    const-string v1, "E_DEFAULT"

    const/16 v2, 0x14

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;->E_OPTIMIZE:Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;->getValue()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;->E_DEFAULT:Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

    const-string v1, "E_NUM"

    const/16 v2, 0x15

    const/16 v3, 0x15

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;->E_NUM:Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

    const/16 v0, 0x16

    new-array v0, v0, [Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;->E_OFF:Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;->E_OPTIMIZE:Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;->E_ENHANCE:Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;->E_SIDE_BY_SIDE:Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;->E_DYNAMICCOMPARE:Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;->E_CENTERBASEDSCALE:Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;->E_MOVEALONG:Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;->E_GOLDENEYES:Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;->E_TRUE_COLOR_ANALYSIS_ASCENSION:Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;->E_LED_BACKLIGHT_CONTROL:Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;->E_HIGH_SPEED_MOVEMENT_PROCESSING:Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;->E_SQUAREMOVE:Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;->E_EN_MS_MWE_CUSTOMER1:Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;->E_EN_MS_MWE_CUSTOMER2:Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;->E_EN_MS_MWE_CUSTOMER3:Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;->E_EN_MS_MWE_CUSTOMER4:Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;->E_EN_MS_MWE_CUSTOMER5:Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;->E_EN_MS_MWE_CUSTOMER6:Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;->E_EN_MS_MWE_CUSTOMER7:Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;->E_EN_MS_MWE_CUSTOMER8:Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;->E_DEFAULT:Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;->E_NUM:Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

    sput v4, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;->seq:I

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;->value:I

    invoke-static {p3}, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;->setHashtableValue(I)V

    return-void
.end method

.method public static getOrdinalThroughValue(I)I
    .locals 3
    .param p0    # I

    # getter for: Lcom/mstar/android/tvapi/common/vo/MweType;->htEnumMweType:Ljava/util/Hashtable;
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/MweType;->access$000()Ljava/util/Hashtable;

    move-result-object v1

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    :goto_0
    return v1

    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method private static setHashtableValue(I)V
    .locals 4
    .param p0    # I

    # getter for: Lcom/mstar/android/tvapi/common/vo/MweType;->htEnumMweType:Ljava/util/Hashtable;
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/MweType;->access$000()Ljava/util/Hashtable;

    move-result-object v0

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, p0}, Ljava/lang/Integer;-><init>(I)V

    new-instance v2, Ljava/lang/Integer;

    sget v3, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;->seq:I

    invoke-direct {v2, v3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget v0, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;->seq:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;->seq:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

    invoke-virtual {v0}, [Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;->value:I

    return v0
.end method
