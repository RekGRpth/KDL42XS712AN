.class public Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;
.super Ljava/lang/Object;
.source "DtvSoundEffect.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;",
            ">;"
        }
    .end annotation
.end field

.field public static final MAX_EQ_BAND_NUM:I = 0x5

.field public static final MAX_PEQ_BAND_NUM:I = 0x5


# instance fields
.field public avcAttachTime:I

.field public avcReleaseTime:I

.field public avcThreshold:I

.field public balance:I

.field public bass:I

.field public echoTime:I

.field public eqBandNumber:S

.field public noiseReductionThreshold:I

.field public peqBandNumber:S

.field public preScale:I

.field public soundDrcThreshold:I

.field public soundParameterEqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;

.field public soundParameterPeqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterPeq;

.field public surroundXaValue:I

.field public surroundXbValue:I

.field public surroundXkValue:I

.field public treble:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect$1;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect$1;-><init>()V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    const/4 v3, 0x5

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-array v1, v3, [Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;

    iput-object v1, p0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundParameterEqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;

    new-array v1, v3, [Lcom/mstar/android/tvapi/common/vo/SoundParameterPeq;

    iput-object v1, p0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundParameterPeqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterPeq;

    iput v2, p0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->preScale:I

    iput v2, p0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->treble:I

    iput v2, p0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->bass:I

    iput v2, p0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->balance:I

    iput-short v2, p0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->eqBandNumber:S

    iput-short v2, p0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->peqBandNumber:S

    iput v2, p0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->avcThreshold:I

    iput v2, p0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->avcAttachTime:I

    iput v2, p0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->avcReleaseTime:I

    iput v2, p0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->surroundXaValue:I

    iput v2, p0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->surroundXbValue:I

    iput v2, p0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->surroundXkValue:I

    iput v2, p0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundDrcThreshold:I

    iput v2, p0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->noiseReductionThreshold:I

    iput v2, p0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->echoTime:I

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    iget-object v1, p0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundParameterEqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;

    new-instance v2, Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;

    invoke-direct {v2}, Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;-><init>()V

    aput-object v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v3, :cond_1

    iget-object v1, p0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundParameterPeqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterPeq;

    new-instance v2, Lcom/mstar/android/tvapi/common/vo/SoundParameterPeq;

    invoke-direct {v2}, Lcom/mstar/android/tvapi/common/vo/SoundParameterPeq;-><init>()V

    aput-object v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 4
    .param p1    # Landroid/os/Parcel;

    const/4 v3, 0x5

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-array v1, v3, [Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;

    iput-object v1, p0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundParameterEqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;

    new-array v1, v3, [Lcom/mstar/android/tvapi/common/vo/SoundParameterPeq;

    iput-object v1, p0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundParameterPeqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterPeq;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->preScale:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->treble:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->bass:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->balance:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    int-to-short v1, v1

    iput-short v1, p0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->eqBandNumber:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    int-to-short v1, v1

    iput-short v1, p0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->peqBandNumber:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->avcThreshold:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->avcAttachTime:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->avcReleaseTime:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->surroundXaValue:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->surroundXbValue:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->surroundXkValue:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundDrcThreshold:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->noiseReductionThreshold:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->echoTime:I

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    iget-object v2, p0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundParameterEqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v1, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;

    aput-object v1, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v3, :cond_1

    iget-object v2, p0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundParameterPeqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterPeq;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/SoundParameterPeq;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v1, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/common/vo/SoundParameterPeq;

    aput-object v1, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    const/4 v1, 0x0

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->preScale:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->treble:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->bass:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->balance:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->eqBandNumber:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->peqBandNumber:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundParameterEqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelableArray([Landroid/os/Parcelable;I)V

    iget-object v0, p0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundParameterPeqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterPeq;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelableArray([Landroid/os/Parcelable;I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->avcThreshold:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->avcAttachTime:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->avcReleaseTime:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->surroundXaValue:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->surroundXbValue:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->surroundXkValue:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundDrcThreshold:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->noiseReductionThreshold:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->echoTime:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
