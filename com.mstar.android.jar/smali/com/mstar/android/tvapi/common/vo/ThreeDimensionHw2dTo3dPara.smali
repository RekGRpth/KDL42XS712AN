.class public Lcom/mstar/android/tvapi/common/vo/ThreeDimensionHw2dTo3dPara;
.super Ljava/lang/Object;
.source "ThreeDimensionHw2dTo3dPara.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mstar/android/tvapi/common/vo/ThreeDimensionHw2dTo3dPara;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public artificialGain:I

.field public concave:I

.field public eleSel:I

.field public gain:I

.field public hw2dTo3dParaVersion:I

.field public modSel:I

.field public offset:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/ThreeDimensionHw2dTo3dPara$1;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/common/vo/ThreeDimensionHw2dTo3dPara$1;-><init>()V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/ThreeDimensionHw2dTo3dPara;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/ThreeDimensionHw2dTo3dPara;->hw2dTo3dParaVersion:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/ThreeDimensionHw2dTo3dPara;->concave:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/ThreeDimensionHw2dTo3dPara;->gain:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/ThreeDimensionHw2dTo3dPara;->offset:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/ThreeDimensionHw2dTo3dPara;->artificialGain:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/ThreeDimensionHw2dTo3dPara;->eleSel:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/ThreeDimensionHw2dTo3dPara;->modSel:I

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1    # Landroid/os/Parcel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/ThreeDimensionHw2dTo3dPara;->hw2dTo3dParaVersion:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/ThreeDimensionHw2dTo3dPara;->concave:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/ThreeDimensionHw2dTo3dPara;->gain:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/ThreeDimensionHw2dTo3dPara;->offset:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/ThreeDimensionHw2dTo3dPara;->artificialGain:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/ThreeDimensionHw2dTo3dPara;->eleSel:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/ThreeDimensionHw2dTo3dPara;->modSel:I

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/ThreeDimensionHw2dTo3dPara;->hw2dTo3dParaVersion:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/ThreeDimensionHw2dTo3dPara;->concave:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/ThreeDimensionHw2dTo3dPara;->gain:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/ThreeDimensionHw2dTo3dPara;->offset:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/ThreeDimensionHw2dTo3dPara;->artificialGain:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/ThreeDimensionHw2dTo3dPara;->eleSel:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/ThreeDimensionHw2dTo3dPara;->modSel:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
