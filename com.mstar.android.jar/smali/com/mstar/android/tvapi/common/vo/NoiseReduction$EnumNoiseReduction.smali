.class public final enum Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;
.super Ljava/lang/Enum;
.source "NoiseReduction.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mstar/android/tvapi/common/vo/NoiseReduction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EnumNoiseReduction"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;

.field public static final enum E_NR_AUTO:Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;

.field public static final enum E_NR_HIGH:Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;

.field public static final enum E_NR_LOW:Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;

.field public static final enum E_NR_MIDDLE:Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;

.field public static final enum E_NR_MIN:Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;

.field public static final enum E_NR_NUM:Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;

.field public static final enum E_NR_OFF:Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;

.field private static seq:I


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;

    const-string v1, "E_NR_MIN"

    invoke-direct {v0, v1, v4, v4}, Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;->E_NR_MIN:Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;

    const-string v1, "E_NR_OFF"

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;->E_NR_MIN:Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;->getValue()I

    move-result v2

    invoke-direct {v0, v1, v5, v2}, Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;->E_NR_OFF:Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;

    const-string v1, "E_NR_LOW"

    invoke-direct {v0, v1, v6, v5}, Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;->E_NR_LOW:Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;

    const-string v1, "E_NR_MIDDLE"

    invoke-direct {v0, v1, v7, v6}, Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;->E_NR_MIDDLE:Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;

    const-string v1, "E_NR_HIGH"

    invoke-direct {v0, v1, v8, v7}, Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;->E_NR_HIGH:Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;

    const-string v1, "E_NR_AUTO"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2, v8}, Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;->E_NR_AUTO:Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;

    const-string v1, "E_NR_NUM"

    const/4 v2, 0x6

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;->E_NR_NUM:Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;

    const/4 v0, 0x7

    new-array v0, v0, [Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;->E_NR_MIN:Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;->E_NR_OFF:Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;->E_NR_LOW:Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;

    aput-object v1, v0, v6

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;->E_NR_MIDDLE:Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;

    aput-object v1, v0, v7

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;->E_NR_HIGH:Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;->E_NR_AUTO:Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;->E_NR_NUM:Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;

    aput-object v2, v0, v1

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;

    sput v4, Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;->seq:I

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;->value:I

    invoke-static {p3}, Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;->setHashtableValue(I)V

    return-void
.end method

.method public static getOrdinalThroughValue(I)I
    .locals 3
    .param p0    # I

    # getter for: Lcom/mstar/android/tvapi/common/vo/NoiseReduction;->htEnumNoiseReduction:Ljava/util/Hashtable;
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/NoiseReduction;->access$000()Ljava/util/Hashtable;

    move-result-object v1

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    :goto_0
    return v1

    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method private static setHashtableValue(I)V
    .locals 4
    .param p0    # I

    # getter for: Lcom/mstar/android/tvapi/common/vo/NoiseReduction;->htEnumNoiseReduction:Ljava/util/Hashtable;
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/NoiseReduction;->access$000()Ljava/util/Hashtable;

    move-result-object v0

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, p0}, Ljava/lang/Integer;-><init>(I)V

    new-instance v2, Ljava/lang/Integer;

    sget v3, Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;->seq:I

    invoke-direct {v2, v3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget v0, Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;->seq:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;->seq:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;

    invoke-virtual {v0}, [Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;->value:I

    return v0
.end method
