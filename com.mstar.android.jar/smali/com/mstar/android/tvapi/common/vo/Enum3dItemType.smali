.class public final enum Lcom/mstar/android/tvapi/common/vo/Enum3dItemType;
.super Ljava/lang/Enum;
.source "Enum3dItemType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/tvapi/common/vo/Enum3dItemType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/tvapi/common/vo/Enum3dItemType;

.field public static final enum EN_3D_ITEM_ASPECTRATIO:Lcom/mstar/android/tvapi/common/vo/Enum3dItemType;

.field public static final enum EN_3D_ITEM_GAIN:Lcom/mstar/android/tvapi/common/vo/Enum3dItemType;

.field public static final enum EN_3D_ITEM_OFFSET:Lcom/mstar/android/tvapi/common/vo/Enum3dItemType;

.field public static final enum EN_3D_QUERYITEM_NUM:Lcom/mstar/android/tvapi/common/vo/Enum3dItemType;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/Enum3dItemType;

    const-string v1, "EN_3D_ITEM_GAIN"

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/Enum3dItemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/Enum3dItemType;->EN_3D_ITEM_GAIN:Lcom/mstar/android/tvapi/common/vo/Enum3dItemType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/Enum3dItemType;

    const-string v1, "EN_3D_ITEM_OFFSET"

    invoke-direct {v0, v1, v3}, Lcom/mstar/android/tvapi/common/vo/Enum3dItemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/Enum3dItemType;->EN_3D_ITEM_OFFSET:Lcom/mstar/android/tvapi/common/vo/Enum3dItemType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/Enum3dItemType;

    const-string v1, "EN_3D_ITEM_ASPECTRATIO"

    invoke-direct {v0, v1, v4}, Lcom/mstar/android/tvapi/common/vo/Enum3dItemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/Enum3dItemType;->EN_3D_ITEM_ASPECTRATIO:Lcom/mstar/android/tvapi/common/vo/Enum3dItemType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/Enum3dItemType;

    const-string v1, "EN_3D_QUERYITEM_NUM"

    invoke-direct {v0, v1, v5}, Lcom/mstar/android/tvapi/common/vo/Enum3dItemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/Enum3dItemType;->EN_3D_QUERYITEM_NUM:Lcom/mstar/android/tvapi/common/vo/Enum3dItemType;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/mstar/android/tvapi/common/vo/Enum3dItemType;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/Enum3dItemType;->EN_3D_ITEM_GAIN:Lcom/mstar/android/tvapi/common/vo/Enum3dItemType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/Enum3dItemType;->EN_3D_ITEM_OFFSET:Lcom/mstar/android/tvapi/common/vo/Enum3dItemType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/Enum3dItemType;->EN_3D_ITEM_ASPECTRATIO:Lcom/mstar/android/tvapi/common/vo/Enum3dItemType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/Enum3dItemType;->EN_3D_QUERYITEM_NUM:Lcom/mstar/android/tvapi/common/vo/Enum3dItemType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/Enum3dItemType;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/Enum3dItemType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/tvapi/common/vo/Enum3dItemType;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/tvapi/common/vo/Enum3dItemType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/common/vo/Enum3dItemType;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/tvapi/common/vo/Enum3dItemType;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/Enum3dItemType;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/Enum3dItemType;

    invoke-virtual {v0}, [Lcom/mstar/android/tvapi/common/vo/Enum3dItemType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/tvapi/common/vo/Enum3dItemType;

    return-object v0
.end method
