.class public final enum Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;
.super Ljava/lang/Enum;
.source "EnumSleepTimeState.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;

.field public static final enum E_10MIN:Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;

.field public static final enum E_120MIN:Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;

.field public static final enum E_180MIN:Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;

.field public static final enum E_20MIN:Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;

.field public static final enum E_240MIN:Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;

.field public static final enum E_30MIN:Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;

.field public static final enum E_60MIN:Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;

.field public static final enum E_90MIN:Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;

.field public static final enum E_OFF:Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;

.field public static final enum E_TOTAL:Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;

    const-string v1, "E_OFF"

    invoke-direct {v0, v1, v3}, Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;->E_OFF:Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;

    const-string v1, "E_10MIN"

    invoke-direct {v0, v1, v4}, Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;->E_10MIN:Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;

    const-string v1, "E_20MIN"

    invoke-direct {v0, v1, v5}, Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;->E_20MIN:Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;

    const-string v1, "E_30MIN"

    invoke-direct {v0, v1, v6}, Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;->E_30MIN:Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;

    const-string v1, "E_60MIN"

    invoke-direct {v0, v1, v7}, Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;->E_60MIN:Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;

    const-string v1, "E_90MIN"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;->E_90MIN:Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;

    const-string v1, "E_120MIN"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;->E_120MIN:Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;

    const-string v1, "E_180MIN"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;->E_180MIN:Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;

    const-string v1, "E_240MIN"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;->E_240MIN:Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;

    const-string v1, "E_TOTAL"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;->E_TOTAL:Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;

    const/16 v0, 0xa

    new-array v0, v0, [Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;->E_OFF:Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;->E_10MIN:Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;->E_20MIN:Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;->E_30MIN:Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;

    aput-object v1, v0, v6

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;->E_60MIN:Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;->E_90MIN:Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;->E_120MIN:Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;->E_180MIN:Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;->E_240MIN:Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;->E_TOTAL:Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;

    aput-object v2, v0, v1

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;

    invoke-virtual {v0}, [Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;

    return-object v0
.end method
