.class public Lcom/mstar/android/tvapi/common/vo/UsbUpgradeCfg;
.super Ljava/lang/Object;
.source "UsbUpgradeCfg.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mstar/android/tvapi/common/vo/UsbUpgradeCfg;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public IsUpgradeApplication:Z

.field public IsUpgradeConfig:Z

.field public IsUpgradeKernel:Z

.field public IsUpgradeMslib:Z

.field public IsUpgradeRootfs:Z

.field public acPath:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/UsbUpgradeCfg$1;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/common/vo/UsbUpgradeCfg$1;-><init>()V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/UsbUpgradeCfg;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v0, p0, Lcom/mstar/android/tvapi/common/vo/UsbUpgradeCfg;->IsUpgradeKernel:Z

    iput-boolean v0, p0, Lcom/mstar/android/tvapi/common/vo/UsbUpgradeCfg;->IsUpgradeRootfs:Z

    iput-boolean v0, p0, Lcom/mstar/android/tvapi/common/vo/UsbUpgradeCfg;->IsUpgradeMslib:Z

    iput-boolean v0, p0, Lcom/mstar/android/tvapi/common/vo/UsbUpgradeCfg;->IsUpgradeApplication:Z

    iput-boolean v0, p0, Lcom/mstar/android/tvapi/common/vo/UsbUpgradeCfg;->IsUpgradeConfig:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/mstar/android/tvapi/common/vo/UsbUpgradeCfg;->acPath:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3
    .param p1    # Landroid/os/Parcel;

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/mstar/android/tvapi/common/vo/UsbUpgradeCfg;->IsUpgradeKernel:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/mstar/android/tvapi/common/vo/UsbUpgradeCfg;->IsUpgradeRootfs:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/mstar/android/tvapi/common/vo/UsbUpgradeCfg;->IsUpgradeMslib:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_3

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lcom/mstar/android/tvapi/common/vo/UsbUpgradeCfg;->IsUpgradeApplication:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_4

    :goto_4
    iput-boolean v1, p0, Lcom/mstar/android/tvapi/common/vo/UsbUpgradeCfg;->IsUpgradeConfig:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mstar/android/tvapi/common/vo/UsbUpgradeCfg;->acPath:Ljava/lang/String;

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_2

    :cond_3
    move v0, v2

    goto :goto_3

    :cond_4
    move v1, v2

    goto :goto_4
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-boolean v0, p0, Lcom/mstar/android/tvapi/common/vo/UsbUpgradeCfg;->IsUpgradeKernel:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/mstar/android/tvapi/common/vo/UsbUpgradeCfg;->IsUpgradeRootfs:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/mstar/android/tvapi/common/vo/UsbUpgradeCfg;->IsUpgradeMslib:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/mstar/android/tvapi/common/vo/UsbUpgradeCfg;->IsUpgradeApplication:Z

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/mstar/android/tvapi/common/vo/UsbUpgradeCfg;->IsUpgradeConfig:Z

    if-eqz v0, :cond_4

    :goto_4
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/mstar/android/tvapi/common/vo/UsbUpgradeCfg;->acPath:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_2

    :cond_3
    move v0, v2

    goto :goto_3

    :cond_4
    move v1, v2

    goto :goto_4
.end method
