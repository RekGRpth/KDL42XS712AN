.class public final enum Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;
.super Ljava/lang/Enum;
.source "KtvInfoType.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mstar/android/tvapi/common/vo/KtvInfoType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EnumKtvInfoType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

.field public static final enum E_KTV_PARAM_TYPE1:Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

.field public static final enum E_KTV_PARAM_TYPE10:Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

.field public static final enum E_KTV_PARAM_TYPE11:Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

.field public static final enum E_KTV_PARAM_TYPE12:Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

.field public static final enum E_KTV_PARAM_TYPE13:Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

.field public static final enum E_KTV_PARAM_TYPE14:Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

.field public static final enum E_KTV_PARAM_TYPE15:Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

.field public static final enum E_KTV_PARAM_TYPE16:Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

.field public static final enum E_KTV_PARAM_TYPE2:Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

.field public static final enum E_KTV_PARAM_TYPE3:Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

.field public static final enum E_KTV_PARAM_TYPE4:Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

.field public static final enum E_KTV_PARAM_TYPE5:Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

.field public static final enum E_KTV_PARAM_TYPE6:Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

.field public static final enum E_KTV_PARAM_TYPE7:Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

.field public static final enum E_KTV_PARAM_TYPE8:Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

.field public static final enum E_KTV_PARAM_TYPE9:Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

.field public static final enum E_KTV_SETINFO_ADC1_GAIN:Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

.field public static final enum E_KTV_SETINFO_ADC_GAIN:Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

.field public static final enum E_KTV_SETINFO_BG_MUSIC_SOUNDMODE:Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

.field public static final enum E_KTV_SETINFO_DEC_MUTE:Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

.field public static final enum E_KTV_SETINFO_DEC_PLAY_WO_OUT:Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

.field public static final enum E_KTV_SETINFO_MIC_SOUNDMODE:Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

.field public static final enum E_KTV_SETINFO_MODEL:Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

.field public static final enum E_KTV_SETNFO_END:Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

.field private static seq:I


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

    const-string v1, "E_KTV_SETINFO_MODEL"

    invoke-direct {v0, v1, v4, v4}, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;->E_KTV_SETINFO_MODEL:Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

    const-string v1, "E_KTV_SETINFO_ADC_GAIN"

    invoke-direct {v0, v1, v5, v5}, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;->E_KTV_SETINFO_ADC_GAIN:Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

    const-string v1, "E_KTV_SETINFO_DEC_MUTE"

    invoke-direct {v0, v1, v6, v6}, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;->E_KTV_SETINFO_DEC_MUTE:Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

    const-string v1, "E_KTV_SETINFO_DEC_PLAY_WO_OUT"

    invoke-direct {v0, v1, v7, v7}, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;->E_KTV_SETINFO_DEC_PLAY_WO_OUT:Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

    const-string v1, "E_KTV_SETINFO_ADC1_GAIN"

    invoke-direct {v0, v1, v8, v8}, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;->E_KTV_SETINFO_ADC1_GAIN:Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

    const-string v1, "E_KTV_SETINFO_MIC_SOUNDMODE"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;->E_KTV_SETINFO_MIC_SOUNDMODE:Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

    const-string v1, "E_KTV_SETINFO_BG_MUSIC_SOUNDMODE"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;->E_KTV_SETINFO_BG_MUSIC_SOUNDMODE:Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

    const-string v1, "E_KTV_SETNFO_END"

    const/4 v2, 0x7

    const/16 v3, 0x3f

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;->E_KTV_SETNFO_END:Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

    const-string v1, "E_KTV_PARAM_TYPE1"

    const/16 v2, 0x8

    const/16 v3, 0x80

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;->E_KTV_PARAM_TYPE1:Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

    const-string v1, "E_KTV_PARAM_TYPE2"

    const/16 v2, 0x9

    const/16 v3, 0x81

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;->E_KTV_PARAM_TYPE2:Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

    const-string v1, "E_KTV_PARAM_TYPE3"

    const/16 v2, 0xa

    const/16 v3, 0x82

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;->E_KTV_PARAM_TYPE3:Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

    const-string v1, "E_KTV_PARAM_TYPE4"

    const/16 v2, 0xb

    const/16 v3, 0x83

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;->E_KTV_PARAM_TYPE4:Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

    const-string v1, "E_KTV_PARAM_TYPE5"

    const/16 v2, 0xc

    const/16 v3, 0x84

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;->E_KTV_PARAM_TYPE5:Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

    const-string v1, "E_KTV_PARAM_TYPE6"

    const/16 v2, 0xd

    const/16 v3, 0x85

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;->E_KTV_PARAM_TYPE6:Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

    const-string v1, "E_KTV_PARAM_TYPE7"

    const/16 v2, 0xe

    const/16 v3, 0x86

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;->E_KTV_PARAM_TYPE7:Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

    const-string v1, "E_KTV_PARAM_TYPE8"

    const/16 v2, 0xf

    const/16 v3, 0x87

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;->E_KTV_PARAM_TYPE8:Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

    const-string v1, "E_KTV_PARAM_TYPE9"

    const/16 v2, 0x10

    const/16 v3, 0x88

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;->E_KTV_PARAM_TYPE9:Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

    const-string v1, "E_KTV_PARAM_TYPE10"

    const/16 v2, 0x11

    const/16 v3, 0x89

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;->E_KTV_PARAM_TYPE10:Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

    const-string v1, "E_KTV_PARAM_TYPE11"

    const/16 v2, 0x12

    const/16 v3, 0x8a

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;->E_KTV_PARAM_TYPE11:Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

    const-string v1, "E_KTV_PARAM_TYPE12"

    const/16 v2, 0x13

    const/16 v3, 0x8b

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;->E_KTV_PARAM_TYPE12:Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

    const-string v1, "E_KTV_PARAM_TYPE13"

    const/16 v2, 0x14

    const/16 v3, 0x8c

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;->E_KTV_PARAM_TYPE13:Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

    const-string v1, "E_KTV_PARAM_TYPE14"

    const/16 v2, 0x15

    const/16 v3, 0x8e

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;->E_KTV_PARAM_TYPE14:Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

    const-string v1, "E_KTV_PARAM_TYPE15"

    const/16 v2, 0x16

    const/16 v3, 0x8f

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;->E_KTV_PARAM_TYPE15:Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

    const-string v1, "E_KTV_PARAM_TYPE16"

    const/16 v2, 0x17

    const/16 v3, 0x90

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;->E_KTV_PARAM_TYPE16:Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

    const/16 v0, 0x18

    new-array v0, v0, [Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;->E_KTV_SETINFO_MODEL:Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;->E_KTV_SETINFO_ADC_GAIN:Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;->E_KTV_SETINFO_DEC_MUTE:Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;->E_KTV_SETINFO_DEC_PLAY_WO_OUT:Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;->E_KTV_SETINFO_ADC1_GAIN:Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;->E_KTV_SETINFO_MIC_SOUNDMODE:Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;->E_KTV_SETINFO_BG_MUSIC_SOUNDMODE:Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;->E_KTV_SETNFO_END:Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;->E_KTV_PARAM_TYPE1:Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;->E_KTV_PARAM_TYPE2:Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;->E_KTV_PARAM_TYPE3:Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;->E_KTV_PARAM_TYPE4:Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;->E_KTV_PARAM_TYPE5:Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;->E_KTV_PARAM_TYPE6:Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;->E_KTV_PARAM_TYPE7:Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;->E_KTV_PARAM_TYPE8:Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;->E_KTV_PARAM_TYPE9:Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;->E_KTV_PARAM_TYPE10:Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;->E_KTV_PARAM_TYPE11:Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;->E_KTV_PARAM_TYPE12:Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;->E_KTV_PARAM_TYPE13:Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;->E_KTV_PARAM_TYPE14:Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;->E_KTV_PARAM_TYPE15:Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;->E_KTV_PARAM_TYPE16:Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;->value:I

    invoke-static {p3}, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;->setHashtableValue(I)V

    return-void
.end method

.method public static getOrdinalThroughValue(I)I
    .locals 3
    .param p0    # I

    # getter for: Lcom/mstar/android/tvapi/common/vo/KtvInfoType;->htEnumKtvInfoType:Ljava/util/Hashtable;
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/KtvInfoType;->access$000()Ljava/util/Hashtable;

    move-result-object v1

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    :goto_0
    return v1

    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method private static setHashtableValue(I)V
    .locals 4
    .param p0    # I

    # getter for: Lcom/mstar/android/tvapi/common/vo/KtvInfoType;->htEnumKtvInfoType:Ljava/util/Hashtable;
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/KtvInfoType;->access$000()Ljava/util/Hashtable;

    move-result-object v0

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, p0}, Ljava/lang/Integer;-><init>(I)V

    new-instance v2, Ljava/lang/Integer;

    sget v3, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;->seq:I

    invoke-direct {v2, v3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget v0, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;->seq:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;->seq:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

    invoke-virtual {v0}, [Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;->value:I

    return v0
.end method
