.class final Lcom/mstar/android/tvapi/common/vo/EnumVideoItem$1;
.super Ljava/lang/Object;
.source "EnumVideoItem.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mstar/android/tvapi/common/vo/EnumVideoItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/mstar/android/tvapi/common/vo/EnumVideoItem;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/mstar/android/tvapi/common/vo/EnumVideoItem;
    .locals 2
    .param p1    # Landroid/os/Parcel;

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumVideoItem;->values()[Lcom/mstar/android/tvapi/common/vo/EnumVideoItem;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1
    .param p1    # Landroid/os/Parcel;

    invoke-virtual {p0, p1}, Lcom/mstar/android/tvapi/common/vo/EnumVideoItem$1;->createFromParcel(Landroid/os/Parcel;)Lcom/mstar/android/tvapi/common/vo/EnumVideoItem;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/mstar/android/tvapi/common/vo/EnumVideoItem;
    .locals 1
    .param p1    # I

    new-array v0, p1, [Lcom/mstar/android/tvapi/common/vo/EnumVideoItem;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1
    .param p1    # I

    invoke-virtual {p0, p1}, Lcom/mstar/android/tvapi/common/vo/EnumVideoItem$1;->newArray(I)[Lcom/mstar/android/tvapi/common/vo/EnumVideoItem;

    move-result-object v0

    return-object v0
.end method
