.class public final enum Lcom/mstar/android/tvapi/common/vo/EnumUrsaUpgradeStatus;
.super Ljava/lang/Enum;
.source "EnumUrsaUpgradeStatus.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/tvapi/common/vo/EnumUrsaUpgradeStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumUrsaUpgradeStatus;

.field public static final enum E_URSA_UPGRADE_ERROR:Lcom/mstar/android/tvapi/common/vo/EnumUrsaUpgradeStatus;

.field public static final enum E_URSA_UPGRADE_FAIL:Lcom/mstar/android/tvapi/common/vo/EnumUrsaUpgradeStatus;

.field public static final enum E_URSA_UPGRADE_FILE_NOT_FOUND:Lcom/mstar/android/tvapi/common/vo/EnumUrsaUpgradeStatus;

.field public static final enum E_URSA_UPGRADE_SUCCESS:Lcom/mstar/android/tvapi/common/vo/EnumUrsaUpgradeStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumUrsaUpgradeStatus;

    const-string v1, "E_URSA_UPGRADE_ERROR"

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumUrsaUpgradeStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumUrsaUpgradeStatus;->E_URSA_UPGRADE_ERROR:Lcom/mstar/android/tvapi/common/vo/EnumUrsaUpgradeStatus;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumUrsaUpgradeStatus;

    const-string v1, "E_URSA_UPGRADE_SUCCESS"

    invoke-direct {v0, v1, v3}, Lcom/mstar/android/tvapi/common/vo/EnumUrsaUpgradeStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumUrsaUpgradeStatus;->E_URSA_UPGRADE_SUCCESS:Lcom/mstar/android/tvapi/common/vo/EnumUrsaUpgradeStatus;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumUrsaUpgradeStatus;

    const-string v1, "E_URSA_UPGRADE_FAIL"

    invoke-direct {v0, v1, v4}, Lcom/mstar/android/tvapi/common/vo/EnumUrsaUpgradeStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumUrsaUpgradeStatus;->E_URSA_UPGRADE_FAIL:Lcom/mstar/android/tvapi/common/vo/EnumUrsaUpgradeStatus;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumUrsaUpgradeStatus;

    const-string v1, "E_URSA_UPGRADE_FILE_NOT_FOUND"

    invoke-direct {v0, v1, v5}, Lcom/mstar/android/tvapi/common/vo/EnumUrsaUpgradeStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumUrsaUpgradeStatus;->E_URSA_UPGRADE_FILE_NOT_FOUND:Lcom/mstar/android/tvapi/common/vo/EnumUrsaUpgradeStatus;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/mstar/android/tvapi/common/vo/EnumUrsaUpgradeStatus;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumUrsaUpgradeStatus;->E_URSA_UPGRADE_ERROR:Lcom/mstar/android/tvapi/common/vo/EnumUrsaUpgradeStatus;

    aput-object v1, v0, v2

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumUrsaUpgradeStatus;->E_URSA_UPGRADE_SUCCESS:Lcom/mstar/android/tvapi/common/vo/EnumUrsaUpgradeStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumUrsaUpgradeStatus;->E_URSA_UPGRADE_FAIL:Lcom/mstar/android/tvapi/common/vo/EnumUrsaUpgradeStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumUrsaUpgradeStatus;->E_URSA_UPGRADE_FILE_NOT_FOUND:Lcom/mstar/android/tvapi/common/vo/EnumUrsaUpgradeStatus;

    aput-object v1, v0, v5

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumUrsaUpgradeStatus;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumUrsaUpgradeStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/tvapi/common/vo/EnumUrsaUpgradeStatus;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/tvapi/common/vo/EnumUrsaUpgradeStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/common/vo/EnumUrsaUpgradeStatus;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/tvapi/common/vo/EnumUrsaUpgradeStatus;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/EnumUrsaUpgradeStatus;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumUrsaUpgradeStatus;

    invoke-virtual {v0}, [Lcom/mstar/android/tvapi/common/vo/EnumUrsaUpgradeStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/tvapi/common/vo/EnumUrsaUpgradeStatus;

    return-object v0
.end method
