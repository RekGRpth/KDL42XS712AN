.class public Lcom/mstar/android/tvapi/common/vo/ColorTemperature;
.super Ljava/lang/Object;
.source "ColorTemperature.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mstar/android/tvapi/common/vo/ColorTemperature;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public blueOffset:S

.field public buleGain:S

.field public greenGain:S

.field public greenOffset:S

.field public redGain:S

.field public redOffset:S


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/ColorTemperature$1;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/common/vo/ColorTemperature$1;-><init>()V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/ColorTemperature;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-short v0, p0, Lcom/mstar/android/tvapi/common/vo/ColorTemperature;->redGain:S

    iput-short v0, p0, Lcom/mstar/android/tvapi/common/vo/ColorTemperature;->greenGain:S

    iput-short v0, p0, Lcom/mstar/android/tvapi/common/vo/ColorTemperature;->buleGain:S

    iput-short v0, p0, Lcom/mstar/android/tvapi/common/vo/ColorTemperature;->redOffset:S

    iput-short v0, p0, Lcom/mstar/android/tvapi/common/vo/ColorTemperature;->greenOffset:S

    iput-short v0, p0, Lcom/mstar/android/tvapi/common/vo/ColorTemperature;->blueOffset:S

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1    # Landroid/os/Parcel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/common/vo/ColorTemperature;->redGain:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/common/vo/ColorTemperature;->greenGain:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/common/vo/ColorTemperature;->buleGain:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/common/vo/ColorTemperature;->redOffset:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/common/vo/ColorTemperature;->greenOffset:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/common/vo/ColorTemperature;->blueOffset:S

    return-void
.end method

.method public constructor <init>(SSSSSS)V
    .locals 0
    .param p1    # S
    .param p2    # S
    .param p3    # S
    .param p4    # S
    .param p5    # S
    .param p6    # S

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-short p1, p0, Lcom/mstar/android/tvapi/common/vo/ColorTemperature;->redGain:S

    iput-short p2, p0, Lcom/mstar/android/tvapi/common/vo/ColorTemperature;->greenGain:S

    iput-short p3, p0, Lcom/mstar/android/tvapi/common/vo/ColorTemperature;->greenGain:S

    iput-short p4, p0, Lcom/mstar/android/tvapi/common/vo/ColorTemperature;->redOffset:S

    iput-short p5, p0, Lcom/mstar/android/tvapi/common/vo/ColorTemperature;->greenOffset:S

    iput-short p6, p0, Lcom/mstar/android/tvapi/common/vo/ColorTemperature;->blueOffset:S

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget-short v0, p0, Lcom/mstar/android/tvapi/common/vo/ColorTemperature;->redGain:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/common/vo/ColorTemperature;->greenGain:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/common/vo/ColorTemperature;->buleGain:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/common/vo/ColorTemperature;->redOffset:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/common/vo/ColorTemperature;->greenOffset:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/common/vo/ColorTemperature;->blueOffset:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
