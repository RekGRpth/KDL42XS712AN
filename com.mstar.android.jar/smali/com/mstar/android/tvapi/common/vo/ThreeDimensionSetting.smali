.class public Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting;
.super Ljava/lang/Object;
.source "ThreeDimensionSetting.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting$EnumTimerPeriod;,
        Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting$EnumAutoStart;,
        Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting$Enum2dDisplayFormat;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public en2DFormat:Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting$Enum2dDisplayFormat;

.field public en3DFormat:Lcom/mstar/android/tvapi/common/vo/Enum3dDisplayFormat;

.field public en3DTimerPeriod:I

.field public enAutoStart:Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting$EnumAutoStart;

.field public enDisplayMode:Lcom/mstar/android/tvapi/common/vo/EnumDisplayMode;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting$1;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting$1;-><init>()V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/EnumDisplayMode;->E_MODE_2D:Lcom/mstar/android/tvapi/common/vo/EnumDisplayMode;

    iput-object v0, p0, Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting;->enDisplayMode:Lcom/mstar/android/tvapi/common/vo/EnumDisplayMode;

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/Enum3dDisplayFormat;->E_CKB:Lcom/mstar/android/tvapi/common/vo/Enum3dDisplayFormat;

    iput-object v0, p0, Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting;->en3DFormat:Lcom/mstar/android/tvapi/common/vo/Enum3dDisplayFormat;

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting$Enum2dDisplayFormat;->E_FP:Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting$Enum2dDisplayFormat;

    iput-object v0, p0, Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting;->en2DFormat:Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting$Enum2dDisplayFormat;

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting$EnumAutoStart;->E_2D:Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting$EnumAutoStart;

    iput-object v0, p0, Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting;->enAutoStart:Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting$EnumAutoStart;

    const/4 v0, 0x0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting;->en3DTimerPeriod:I

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .param p1    # Landroid/os/Parcel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumDisplayMode;->values()[Lcom/mstar/android/tvapi/common/vo/EnumDisplayMode;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting;->enDisplayMode:Lcom/mstar/android/tvapi/common/vo/EnumDisplayMode;

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/Enum3dDisplayFormat;->values()[Lcom/mstar/android/tvapi/common/vo/Enum3dDisplayFormat;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting;->en3DFormat:Lcom/mstar/android/tvapi/common/vo/Enum3dDisplayFormat;

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting$Enum2dDisplayFormat;->values()[Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting$Enum2dDisplayFormat;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting;->en2DFormat:Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting$Enum2dDisplayFormat;

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting$EnumAutoStart;->values()[Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting$EnumAutoStart;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting;->enAutoStart:Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting$EnumAutoStart;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting;->en3DTimerPeriod:I

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget-object v0, p0, Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting;->enDisplayMode:Lcom/mstar/android/tvapi/common/vo/EnumDisplayMode;

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/EnumDisplayMode;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting;->en3DFormat:Lcom/mstar/android/tvapi/common/vo/Enum3dDisplayFormat;

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/Enum3dDisplayFormat;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting;->en2DFormat:Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting$Enum2dDisplayFormat;

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting$Enum2dDisplayFormat;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting;->enAutoStart:Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting$EnumAutoStart;

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting$EnumAutoStart;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/ThreeDimensionSetting;->en3DTimerPeriod:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
