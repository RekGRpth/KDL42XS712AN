.class public final enum Lcom/mstar/android/tvapi/common/vo/EnumPipInputSource;
.super Ljava/lang/Enum;
.source "EnumPipInputSource.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/tvapi/common/vo/EnumPipInputSource;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumPipInputSource;

.field public static final enum E_MAIN_INPUT_SOURCE:Lcom/mstar/android/tvapi/common/vo/EnumPipInputSource;

.field public static final enum E_SUB_INPUT_SOURCE:Lcom/mstar/android/tvapi/common/vo/EnumPipInputSource;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumPipInputSource;

    const-string v1, "E_MAIN_INPUT_SOURCE"

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumPipInputSource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumPipInputSource;->E_MAIN_INPUT_SOURCE:Lcom/mstar/android/tvapi/common/vo/EnumPipInputSource;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumPipInputSource;

    const-string v1, "E_SUB_INPUT_SOURCE"

    invoke-direct {v0, v1, v3}, Lcom/mstar/android/tvapi/common/vo/EnumPipInputSource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumPipInputSource;->E_SUB_INPUT_SOURCE:Lcom/mstar/android/tvapi/common/vo/EnumPipInputSource;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/mstar/android/tvapi/common/vo/EnumPipInputSource;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumPipInputSource;->E_MAIN_INPUT_SOURCE:Lcom/mstar/android/tvapi/common/vo/EnumPipInputSource;

    aput-object v1, v0, v2

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumPipInputSource;->E_SUB_INPUT_SOURCE:Lcom/mstar/android/tvapi/common/vo/EnumPipInputSource;

    aput-object v1, v0, v3

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumPipInputSource;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumPipInputSource;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/tvapi/common/vo/EnumPipInputSource;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/tvapi/common/vo/EnumPipInputSource;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/common/vo/EnumPipInputSource;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/tvapi/common/vo/EnumPipInputSource;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/EnumPipInputSource;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumPipInputSource;

    invoke-virtual {v0}, [Lcom/mstar/android/tvapi/common/vo/EnumPipInputSource;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/tvapi/common/vo/EnumPipInputSource;

    return-object v0
.end method
