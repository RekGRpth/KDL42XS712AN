.class public Lcom/mstar/android/tvapi/common/vo/SoundModeSetting;
.super Ljava/lang/Object;
.source "SoundModeSetting.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mstar/android/tvapi/common/vo/SoundModeSetting;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public balance:S

.field public bass:S

.field public enSoundAudioChannel:Lcom/mstar/android/tvapi/common/vo/EnumAudioMode;

.field public eqBand1:S

.field public eqBand2:S

.field public eqBand3:S

.field public eqBand4:S

.field public eqBand5:S

.field public eqBand6:S

.field public eqBand7:S

.field public treble:S

.field public userMode:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/SoundModeSetting$1;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/common/vo/SoundModeSetting$1;-><init>()V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/SoundModeSetting;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-short v0, p0, Lcom/mstar/android/tvapi/common/vo/SoundModeSetting;->bass:S

    iput-short v0, p0, Lcom/mstar/android/tvapi/common/vo/SoundModeSetting;->treble:S

    iput-short v0, p0, Lcom/mstar/android/tvapi/common/vo/SoundModeSetting;->eqBand1:S

    iput-short v0, p0, Lcom/mstar/android/tvapi/common/vo/SoundModeSetting;->eqBand2:S

    iput-short v0, p0, Lcom/mstar/android/tvapi/common/vo/SoundModeSetting;->eqBand3:S

    iput-short v0, p0, Lcom/mstar/android/tvapi/common/vo/SoundModeSetting;->eqBand4:S

    iput-short v0, p0, Lcom/mstar/android/tvapi/common/vo/SoundModeSetting;->eqBand5:S

    iput-short v0, p0, Lcom/mstar/android/tvapi/common/vo/SoundModeSetting;->eqBand6:S

    iput-short v0, p0, Lcom/mstar/android/tvapi/common/vo/SoundModeSetting;->eqBand7:S

    iput-boolean v0, p0, Lcom/mstar/android/tvapi/common/vo/SoundModeSetting;->userMode:Z

    iput-short v0, p0, Lcom/mstar/android/tvapi/common/vo/SoundModeSetting;->balance:S

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAudioMode;->E_LL:Lcom/mstar/android/tvapi/common/vo/EnumAudioMode;

    iput-object v0, p0, Lcom/mstar/android/tvapi/common/vo/SoundModeSetting;->enSoundAudioChannel:Lcom/mstar/android/tvapi/common/vo/EnumAudioMode;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .param p1    # Landroid/os/Parcel;

    const/4 v0, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    int-to-short v1, v1

    iput-short v1, p0, Lcom/mstar/android/tvapi/common/vo/SoundModeSetting;->bass:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    int-to-short v1, v1

    iput-short v1, p0, Lcom/mstar/android/tvapi/common/vo/SoundModeSetting;->treble:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    int-to-short v1, v1

    iput-short v1, p0, Lcom/mstar/android/tvapi/common/vo/SoundModeSetting;->eqBand1:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    int-to-short v1, v1

    iput-short v1, p0, Lcom/mstar/android/tvapi/common/vo/SoundModeSetting;->eqBand2:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    int-to-short v1, v1

    iput-short v1, p0, Lcom/mstar/android/tvapi/common/vo/SoundModeSetting;->eqBand3:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    int-to-short v1, v1

    iput-short v1, p0, Lcom/mstar/android/tvapi/common/vo/SoundModeSetting;->eqBand4:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    int-to-short v1, v1

    iput-short v1, p0, Lcom/mstar/android/tvapi/common/vo/SoundModeSetting;->eqBand5:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    int-to-short v1, v1

    iput-short v1, p0, Lcom/mstar/android/tvapi/common/vo/SoundModeSetting;->eqBand6:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    int-to-short v1, v1

    iput-short v1, p0, Lcom/mstar/android/tvapi/common/vo/SoundModeSetting;->eqBand7:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/mstar/android/tvapi/common/vo/SoundModeSetting;->userMode:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/common/vo/SoundModeSetting;->balance:S

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumAudioMode;->values()[Lcom/mstar/android/tvapi/common/vo/EnumAudioMode;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/mstar/android/tvapi/common/vo/SoundModeSetting;->enSoundAudioChannel:Lcom/mstar/android/tvapi/common/vo/EnumAudioMode;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget-short v0, p0, Lcom/mstar/android/tvapi/common/vo/SoundModeSetting;->bass:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/common/vo/SoundModeSetting;->treble:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/common/vo/SoundModeSetting;->eqBand1:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/common/vo/SoundModeSetting;->eqBand2:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/common/vo/SoundModeSetting;->eqBand3:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/common/vo/SoundModeSetting;->eqBand4:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/common/vo/SoundModeSetting;->eqBand5:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/common/vo/SoundModeSetting;->eqBand6:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/common/vo/SoundModeSetting;->eqBand7:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/mstar/android/tvapi/common/vo/SoundModeSetting;->userMode:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/common/vo/SoundModeSetting;->balance:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/mstar/android/tvapi/common/vo/SoundModeSetting;->enSoundAudioChannel:Lcom/mstar/android/tvapi/common/vo/EnumAudioMode;

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/EnumAudioMode;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
