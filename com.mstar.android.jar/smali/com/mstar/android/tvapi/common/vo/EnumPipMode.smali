.class public final enum Lcom/mstar/android/tvapi/common/vo/EnumPipMode;
.super Ljava/lang/Enum;
.source "EnumPipMode.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/tvapi/common/vo/EnumPipMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumPipMode;

.field public static final enum E_NONE_MODE:Lcom/mstar/android/tvapi/common/vo/EnumPipMode;

.field public static final enum E_PIP_MODE:Lcom/mstar/android/tvapi/common/vo/EnumPipMode;

.field public static final enum E_POP_MODE:Lcom/mstar/android/tvapi/common/vo/EnumPipMode;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumPipMode;

    const-string v1, "E_NONE_MODE"

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumPipMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumPipMode;->E_NONE_MODE:Lcom/mstar/android/tvapi/common/vo/EnumPipMode;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumPipMode;

    const-string v1, "E_PIP_MODE"

    invoke-direct {v0, v1, v3}, Lcom/mstar/android/tvapi/common/vo/EnumPipMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumPipMode;->E_PIP_MODE:Lcom/mstar/android/tvapi/common/vo/EnumPipMode;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumPipMode;

    const-string v1, "E_POP_MODE"

    invoke-direct {v0, v1, v4}, Lcom/mstar/android/tvapi/common/vo/EnumPipMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumPipMode;->E_POP_MODE:Lcom/mstar/android/tvapi/common/vo/EnumPipMode;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/mstar/android/tvapi/common/vo/EnumPipMode;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumPipMode;->E_NONE_MODE:Lcom/mstar/android/tvapi/common/vo/EnumPipMode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumPipMode;->E_PIP_MODE:Lcom/mstar/android/tvapi/common/vo/EnumPipMode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumPipMode;->E_POP_MODE:Lcom/mstar/android/tvapi/common/vo/EnumPipMode;

    aput-object v1, v0, v4

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumPipMode;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumPipMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/tvapi/common/vo/EnumPipMode;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/tvapi/common/vo/EnumPipMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/common/vo/EnumPipMode;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/tvapi/common/vo/EnumPipMode;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/EnumPipMode;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumPipMode;

    invoke-virtual {v0}, [Lcom/mstar/android/tvapi/common/vo/EnumPipMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/tvapi/common/vo/EnumPipMode;

    return-object v0
.end method
