.class public final enum Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;
.super Ljava/lang/Enum;
.source "DtvProgramSignalInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EnumProgramDemodType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;

.field public static final enum E_PROGRAM_DEMOD_ATSC:Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;

.field public static final enum E_PROGRAM_DEMOD_ATSC_16QAM:Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;

.field public static final enum E_PROGRAM_DEMOD_ATSC_256QAM:Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;

.field public static final enum E_PROGRAM_DEMOD_ATSC_64QAM:Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;

.field public static final enum E_PROGRAM_DEMOD_ATSC_QPSK:Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;

.field public static final enum E_PROGRAM_DEMOD_ATSC_VSB:Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;

.field public static final enum E_PROGRAM_DEMOD_ATV:Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;

.field public static final enum E_PROGRAM_DEMOD_DTMB:Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;

.field public static final enum E_PROGRAM_DEMOD_DVB_C:Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;

.field public static final enum E_PROGRAM_DEMOD_DVB_S:Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;

.field public static final enum E_PROGRAM_DEMOD_DVB_T:Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;

.field public static final enum E_PROGRAM_DEMOD_DVB_T2:Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;

.field public static final enum E_PROGRAM_DEMOD_ISDB:Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;

.field public static final enum E_PROGRAM_DEMOD_MAX:Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;

.field public static final enum E_PROGRAM_DEMOD_NULL:Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;

.field private static seq:I


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/16 v5, 0xd

    const/4 v4, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;

    const-string v1, "E_PROGRAM_DEMOD_ATV"

    invoke-direct {v0, v1, v4, v4}, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;->E_PROGRAM_DEMOD_ATV:Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;

    const-string v1, "E_PROGRAM_DEMOD_DVB_T"

    invoke-direct {v0, v1, v6, v6}, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;->E_PROGRAM_DEMOD_DVB_T:Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;

    const-string v1, "E_PROGRAM_DEMOD_DVB_C"

    invoke-direct {v0, v1, v7, v7}, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;->E_PROGRAM_DEMOD_DVB_C:Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;

    const-string v1, "E_PROGRAM_DEMOD_DVB_S"

    invoke-direct {v0, v1, v8, v8}, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;->E_PROGRAM_DEMOD_DVB_S:Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;

    const-string v1, "E_PROGRAM_DEMOD_DTMB"

    const/4 v2, 0x4

    const/4 v3, 0x4

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;->E_PROGRAM_DEMOD_DTMB:Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;

    const-string v1, "E_PROGRAM_DEMOD_ATSC"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;->E_PROGRAM_DEMOD_ATSC:Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;

    const-string v1, "E_PROGRAM_DEMOD_ATSC_VSB"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;->E_PROGRAM_DEMOD_ATSC_VSB:Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;

    const-string v1, "E_PROGRAM_DEMOD_ATSC_QPSK"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;->E_PROGRAM_DEMOD_ATSC_QPSK:Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;

    const-string v1, "E_PROGRAM_DEMOD_ATSC_16QAM"

    const/16 v2, 0x8

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;->E_PROGRAM_DEMOD_ATSC_16QAM:Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;

    const-string v1, "E_PROGRAM_DEMOD_ATSC_64QAM"

    const/16 v2, 0x9

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;->E_PROGRAM_DEMOD_ATSC_64QAM:Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;

    const-string v1, "E_PROGRAM_DEMOD_ATSC_256QAM"

    const/16 v2, 0xa

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;->E_PROGRAM_DEMOD_ATSC_256QAM:Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;

    const-string v1, "E_PROGRAM_DEMOD_DVB_T2"

    const/16 v2, 0xb

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;->E_PROGRAM_DEMOD_DVB_T2:Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;

    const-string v1, "E_PROGRAM_DEMOD_ISDB"

    const/16 v2, 0xc

    const/16 v3, 0xc

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;->E_PROGRAM_DEMOD_ISDB:Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;

    const-string v1, "E_PROGRAM_DEMOD_MAX"

    invoke-direct {v0, v1, v5, v5}, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;->E_PROGRAM_DEMOD_MAX:Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;

    const-string v1, "E_PROGRAM_DEMOD_NULL"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2, v5}, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;->E_PROGRAM_DEMOD_NULL:Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;

    const/16 v0, 0xf

    new-array v0, v0, [Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;->E_PROGRAM_DEMOD_ATV:Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;->E_PROGRAM_DEMOD_DVB_T:Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;->E_PROGRAM_DEMOD_DVB_C:Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;->E_PROGRAM_DEMOD_DVB_S:Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;

    aput-object v1, v0, v8

    const/4 v1, 0x4

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;->E_PROGRAM_DEMOD_DTMB:Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;->E_PROGRAM_DEMOD_ATSC:Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;->E_PROGRAM_DEMOD_ATSC_VSB:Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;->E_PROGRAM_DEMOD_ATSC_QPSK:Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;->E_PROGRAM_DEMOD_ATSC_16QAM:Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;->E_PROGRAM_DEMOD_ATSC_64QAM:Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;->E_PROGRAM_DEMOD_ATSC_256QAM:Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;->E_PROGRAM_DEMOD_DVB_T2:Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;->E_PROGRAM_DEMOD_ISDB:Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;

    aput-object v2, v0, v1

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;->E_PROGRAM_DEMOD_MAX:Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;

    aput-object v1, v0, v5

    const/16 v1, 0xe

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;->E_PROGRAM_DEMOD_NULL:Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;

    sput v4, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;->seq:I

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;->value:I

    invoke-static {p3}, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;->setHashtableValue(I)V

    return-void
.end method

.method public static getOrdinalThroughValue(I)I
    .locals 3
    .param p0    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvOutOfBoundException;
        }
    .end annotation

    # getter for: Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo;->enumhash:Ljava/util/Hashtable;
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo;->access$000()Ljava/util/Hashtable;

    move-result-object v1

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    return v1

    :cond_0
    new-instance v1, Lcom/mstar/android/tvapi/common/exception/TvOutOfBoundException;

    invoke-direct {v1}, Lcom/mstar/android/tvapi/common/exception/TvOutOfBoundException;-><init>()V

    throw v1
.end method

.method private static setHashtableValue(I)V
    .locals 4
    .param p0    # I

    # getter for: Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo;->enumhash:Ljava/util/Hashtable;
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo;->access$000()Ljava/util/Hashtable;

    move-result-object v0

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, p0}, Ljava/lang/Integer;-><init>(I)V

    new-instance v2, Ljava/lang/Integer;

    sget v3, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;->seq:I

    invoke-direct {v2, v3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget v0, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;->seq:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;->seq:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;

    invoke-virtual {v0}, [Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo$EnumProgramDemodType;->value:I

    return v0
.end method
