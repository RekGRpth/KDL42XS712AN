.class public final enum Lcom/mstar/android/tvapi/common/vo/Film$EnumFilm;
.super Ljava/lang/Enum;
.source "Film.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mstar/android/tvapi/common/vo/Film;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EnumFilm"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/tvapi/common/vo/Film$EnumFilm;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/tvapi/common/vo/Film$EnumFilm;

.field public static final enum E_MIN:Lcom/mstar/android/tvapi/common/vo/Film$EnumFilm;

.field public static final enum E_NUM:Lcom/mstar/android/tvapi/common/vo/Film$EnumFilm;

.field public static final enum E_OFF:Lcom/mstar/android/tvapi/common/vo/Film$EnumFilm;

.field public static final enum E_ON:Lcom/mstar/android/tvapi/common/vo/Film$EnumFilm;

.field private static seq:I


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/Film$EnumFilm;

    const-string v1, "E_MIN"

    invoke-direct {v0, v1, v3, v3}, Lcom/mstar/android/tvapi/common/vo/Film$EnumFilm;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/Film$EnumFilm;->E_MIN:Lcom/mstar/android/tvapi/common/vo/Film$EnumFilm;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/Film$EnumFilm;

    const-string v1, "E_OFF"

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/MpegNoiseReduction$EnumMpegNoiseReduction;->E_MPEG_NR_MIN:Lcom/mstar/android/tvapi/common/vo/MpegNoiseReduction$EnumMpegNoiseReduction;

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/vo/MpegNoiseReduction$EnumMpegNoiseReduction;->ordinal()I

    move-result v2

    invoke-direct {v0, v1, v4, v2}, Lcom/mstar/android/tvapi/common/vo/Film$EnumFilm;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/Film$EnumFilm;->E_OFF:Lcom/mstar/android/tvapi/common/vo/Film$EnumFilm;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/Film$EnumFilm;

    const-string v1, "E_ON"

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/MpegNoiseReduction$EnumMpegNoiseReduction;->E_MPEG_NR_MIN:Lcom/mstar/android/tvapi/common/vo/MpegNoiseReduction$EnumMpegNoiseReduction;

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/vo/MpegNoiseReduction$EnumMpegNoiseReduction;->ordinal()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-direct {v0, v1, v5, v2}, Lcom/mstar/android/tvapi/common/vo/Film$EnumFilm;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/Film$EnumFilm;->E_ON:Lcom/mstar/android/tvapi/common/vo/Film$EnumFilm;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/Film$EnumFilm;

    const-string v1, "E_NUM"

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/MpegNoiseReduction$EnumMpegNoiseReduction;->E_MPEG_NR_MIN:Lcom/mstar/android/tvapi/common/vo/MpegNoiseReduction$EnumMpegNoiseReduction;

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/vo/MpegNoiseReduction$EnumMpegNoiseReduction;->ordinal()I

    move-result v2

    add-int/lit8 v2, v2, 0x2

    invoke-direct {v0, v1, v6, v2}, Lcom/mstar/android/tvapi/common/vo/Film$EnumFilm;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/Film$EnumFilm;->E_NUM:Lcom/mstar/android/tvapi/common/vo/Film$EnumFilm;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/mstar/android/tvapi/common/vo/Film$EnumFilm;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/Film$EnumFilm;->E_MIN:Lcom/mstar/android/tvapi/common/vo/Film$EnumFilm;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/Film$EnumFilm;->E_OFF:Lcom/mstar/android/tvapi/common/vo/Film$EnumFilm;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/Film$EnumFilm;->E_ON:Lcom/mstar/android/tvapi/common/vo/Film$EnumFilm;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/Film$EnumFilm;->E_NUM:Lcom/mstar/android/tvapi/common/vo/Film$EnumFilm;

    aput-object v1, v0, v6

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/Film$EnumFilm;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/Film$EnumFilm;

    sput v3, Lcom/mstar/android/tvapi/common/vo/Film$EnumFilm;->seq:I

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/mstar/android/tvapi/common/vo/Film$EnumFilm;->value:I

    invoke-static {p3}, Lcom/mstar/android/tvapi/common/vo/Film$EnumFilm;->setHashtableValue(I)V

    return-void
.end method

.method public static getOrdinalThroughValue(I)I
    .locals 3
    .param p0    # I

    # getter for: Lcom/mstar/android/tvapi/common/vo/Film;->htEnumFilm:Ljava/util/Hashtable;
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/Film;->access$000()Ljava/util/Hashtable;

    move-result-object v1

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    :goto_0
    return v1

    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method private static setHashtableValue(I)V
    .locals 4
    .param p0    # I

    # getter for: Lcom/mstar/android/tvapi/common/vo/Film;->htEnumFilm:Ljava/util/Hashtable;
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/Film;->access$000()Ljava/util/Hashtable;

    move-result-object v0

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, p0}, Ljava/lang/Integer;-><init>(I)V

    new-instance v2, Ljava/lang/Integer;

    sget v3, Lcom/mstar/android/tvapi/common/vo/Film$EnumFilm;->seq:I

    invoke-direct {v2, v3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget v0, Lcom/mstar/android/tvapi/common/vo/Film$EnumFilm;->seq:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/mstar/android/tvapi/common/vo/Film$EnumFilm;->seq:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/tvapi/common/vo/Film$EnumFilm;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/tvapi/common/vo/Film$EnumFilm;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/common/vo/Film$EnumFilm;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/tvapi/common/vo/Film$EnumFilm;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/Film$EnumFilm;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/Film$EnumFilm;

    invoke-virtual {v0}, [Lcom/mstar/android/tvapi/common/vo/Film$EnumFilm;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/tvapi/common/vo/Film$EnumFilm;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/Film$EnumFilm;->value:I

    return v0
.end method
