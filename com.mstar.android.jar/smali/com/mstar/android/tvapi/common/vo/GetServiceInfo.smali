.class public Lcom/mstar/android/tvapi/common/vo/GetServiceInfo;
.super Ljava/lang/Object;
.source "GetServiceInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mstar/android/tvapi/common/vo/GetServiceInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public favorite:S

.field public isDelete:Z

.field public isHide:Z

.field public isLock:Z

.field public isScramble:Z

.field public isSkip:Z

.field public isVisible:Z

.field public majorNum:S

.field public minorNum:S

.field public number:I

.field public nvodTimeShiftServiceNum:S

.field public progId:I

.field public queryIndex:I

.field public screenMuteStatus:I

.field public serviceName:Ljava/lang/String;

.field public serviceType:S

.field timeShiftedServiceIds:[Lcom/mstar/android/tvapi/common/vo/DtvTripleId;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/GetServiceInfo$1;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/common/vo/GetServiceInfo$1;-><init>()V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/GetServiceInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v2, p0, Lcom/mstar/android/tvapi/common/vo/GetServiceInfo;->queryIndex:I

    iput v2, p0, Lcom/mstar/android/tvapi/common/vo/GetServiceInfo;->number:I

    iput-short v2, p0, Lcom/mstar/android/tvapi/common/vo/GetServiceInfo;->majorNum:S

    iput-short v2, p0, Lcom/mstar/android/tvapi/common/vo/GetServiceInfo;->minorNum:S

    iput v2, p0, Lcom/mstar/android/tvapi/common/vo/GetServiceInfo;->progId:I

    iput-short v2, p0, Lcom/mstar/android/tvapi/common/vo/GetServiceInfo;->favorite:S

    iput-boolean v2, p0, Lcom/mstar/android/tvapi/common/vo/GetServiceInfo;->isLock:Z

    iput-boolean v2, p0, Lcom/mstar/android/tvapi/common/vo/GetServiceInfo;->isSkip:Z

    iput-boolean v2, p0, Lcom/mstar/android/tvapi/common/vo/GetServiceInfo;->isScramble:Z

    iput-boolean v2, p0, Lcom/mstar/android/tvapi/common/vo/GetServiceInfo;->isDelete:Z

    iput-boolean v2, p0, Lcom/mstar/android/tvapi/common/vo/GetServiceInfo;->isVisible:Z

    iput-boolean v2, p0, Lcom/mstar/android/tvapi/common/vo/GetServiceInfo;->isHide:Z

    iput-short v2, p0, Lcom/mstar/android/tvapi/common/vo/GetServiceInfo;->serviceType:S

    iput v2, p0, Lcom/mstar/android/tvapi/common/vo/GetServiceInfo;->screenMuteStatus:I

    const-string v1, ""

    iput-object v1, p0, Lcom/mstar/android/tvapi/common/vo/GetServiceInfo;->serviceName:Ljava/lang/String;

    iput-short v2, p0, Lcom/mstar/android/tvapi/common/vo/GetServiceInfo;->nvodTimeShiftServiceNum:S

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/mstar/android/tvapi/common/vo/GetServiceInfo;->timeShiftedServiceIds:[Lcom/mstar/android/tvapi/common/vo/DtvTripleId;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/mstar/android/tvapi/common/vo/GetServiceInfo;->timeShiftedServiceIds:[Lcom/mstar/android/tvapi/common/vo/DtvTripleId;

    new-instance v2, Lcom/mstar/android/tvapi/common/vo/DtvTripleId;

    invoke-direct {v2}, Lcom/mstar/android/tvapi/common/vo/DtvTripleId;-><init>()V

    aput-object v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 4
    .param p1    # Landroid/os/Parcel;

    const/4 v3, 0x0

    const/4 v2, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/mstar/android/tvapi/common/vo/GetServiceInfo;->queryIndex:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/mstar/android/tvapi/common/vo/GetServiceInfo;->number:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    int-to-short v1, v1

    iput-short v1, p0, Lcom/mstar/android/tvapi/common/vo/GetServiceInfo;->majorNum:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    int-to-short v1, v1

    iput-short v1, p0, Lcom/mstar/android/tvapi/common/vo/GetServiceInfo;->minorNum:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/mstar/android/tvapi/common/vo/GetServiceInfo;->progId:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    int-to-short v1, v1

    iput-short v1, p0, Lcom/mstar/android/tvapi/common/vo/GetServiceInfo;->favorite:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-ne v1, v2, :cond_0

    move v1, v2

    :goto_0
    iput-boolean v1, p0, Lcom/mstar/android/tvapi/common/vo/GetServiceInfo;->isLock:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-ne v1, v2, :cond_1

    move v1, v2

    :goto_1
    iput-boolean v1, p0, Lcom/mstar/android/tvapi/common/vo/GetServiceInfo;->isSkip:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-ne v1, v2, :cond_2

    move v1, v2

    :goto_2
    iput-boolean v1, p0, Lcom/mstar/android/tvapi/common/vo/GetServiceInfo;->isScramble:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-ne v1, v2, :cond_3

    move v1, v2

    :goto_3
    iput-boolean v1, p0, Lcom/mstar/android/tvapi/common/vo/GetServiceInfo;->isDelete:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-ne v1, v2, :cond_4

    move v1, v2

    :goto_4
    iput-boolean v1, p0, Lcom/mstar/android/tvapi/common/vo/GetServiceInfo;->isVisible:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-ne v1, v2, :cond_5

    :goto_5
    iput-boolean v2, p0, Lcom/mstar/android/tvapi/common/vo/GetServiceInfo;->isHide:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    int-to-short v1, v1

    iput-short v1, p0, Lcom/mstar/android/tvapi/common/vo/GetServiceInfo;->serviceType:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/mstar/android/tvapi/common/vo/GetServiceInfo;->screenMuteStatus:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/mstar/android/tvapi/common/vo/GetServiceInfo;->serviceName:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    int-to-short v1, v1

    iput-short v1, p0, Lcom/mstar/android/tvapi/common/vo/GetServiceInfo;->nvodTimeShiftServiceNum:S

    const/4 v0, 0x0

    :goto_6
    iget-object v1, p0, Lcom/mstar/android/tvapi/common/vo/GetServiceInfo;->timeShiftedServiceIds:[Lcom/mstar/android/tvapi/common/vo/DtvTripleId;

    array-length v1, v1

    if-ge v0, v1, :cond_6

    iget-object v2, p0, Lcom/mstar/android/tvapi/common/vo/GetServiceInfo;->timeShiftedServiceIds:[Lcom/mstar/android/tvapi/common/vo/DtvTripleId;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/DtvTripleId;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v1, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/common/vo/DtvTripleId;

    aput-object v1, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_0
    move v1, v3

    goto :goto_0

    :cond_1
    move v1, v3

    goto :goto_1

    :cond_2
    move v1, v3

    goto :goto_2

    :cond_3
    move v1, v3

    goto :goto_3

    :cond_4
    move v1, v3

    goto :goto_4

    :cond_5
    move v2, v3

    goto :goto_5

    :cond_6
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/GetServiceInfo;->queryIndex:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/GetServiceInfo;->number:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/common/vo/GetServiceInfo;->majorNum:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/common/vo/GetServiceInfo;->minorNum:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/GetServiceInfo;->progId:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/common/vo/GetServiceInfo;->favorite:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/mstar/android/tvapi/common/vo/GetServiceInfo;->isLock:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/mstar/android/tvapi/common/vo/GetServiceInfo;->isSkip:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/mstar/android/tvapi/common/vo/GetServiceInfo;->isScramble:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/mstar/android/tvapi/common/vo/GetServiceInfo;->isDelete:Z

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/mstar/android/tvapi/common/vo/GetServiceInfo;->isVisible:Z

    if-eqz v0, :cond_4

    move v0, v1

    :goto_4
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/mstar/android/tvapi/common/vo/GetServiceInfo;->isHide:Z

    if-eqz v0, :cond_5

    :goto_5
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/common/vo/GetServiceInfo;->serviceType:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/GetServiceInfo;->screenMuteStatus:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/mstar/android/tvapi/common/vo/GetServiceInfo;->serviceName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/common/vo/GetServiceInfo;->nvodTimeShiftServiceNum:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/mstar/android/tvapi/common/vo/GetServiceInfo;->timeShiftedServiceIds:[Lcom/mstar/android/tvapi/common/vo/DtvTripleId;

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelableArray([Landroid/os/Parcelable;I)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_2

    :cond_3
    move v0, v2

    goto :goto_3

    :cond_4
    move v0, v2

    goto :goto_4

    :cond_5
    move v1, v2

    goto :goto_5
.end method
