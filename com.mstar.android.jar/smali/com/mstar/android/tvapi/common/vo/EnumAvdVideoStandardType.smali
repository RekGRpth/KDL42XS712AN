.class public final enum Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;
.super Ljava/lang/Enum;
.source "EnumAvdVideoStandardType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

.field public static final enum AUTO:Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

.field public static final enum MAX:Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

.field public static final enum NOTSTANDARD:Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

.field public static final enum NTSC_44:Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

.field public static final enum NTSC_M:Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

.field public static final enum PAL_60:Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

.field public static final enum PAL_BGHI:Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

.field public static final enum PAL_M:Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

.field public static final enum PAL_N:Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

.field public static final enum SECAM:Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    const-string v1, "PAL_BGHI"

    invoke-direct {v0, v1, v3}, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->PAL_BGHI:Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    const-string v1, "NTSC_M"

    invoke-direct {v0, v1, v4}, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->NTSC_M:Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    const-string v1, "SECAM"

    invoke-direct {v0, v1, v5}, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->SECAM:Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    const-string v1, "NTSC_44"

    invoke-direct {v0, v1, v6}, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->NTSC_44:Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    const-string v1, "PAL_M"

    invoke-direct {v0, v1, v7}, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->PAL_M:Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    const-string v1, "PAL_N"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->PAL_N:Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    const-string v1, "PAL_60"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->PAL_60:Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    const-string v1, "NOTSTANDARD"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->NOTSTANDARD:Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    const-string v1, "AUTO"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->AUTO:Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    const-string v1, "MAX"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->MAX:Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    const/16 v0, 0xa

    new-array v0, v0, [Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->PAL_BGHI:Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->NTSC_M:Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->SECAM:Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->NTSC_44:Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->PAL_M:Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->PAL_N:Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->PAL_60:Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->NOTSTANDARD:Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->AUTO:Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->MAX:Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    invoke-virtual {v0}, [Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    return-object v0
.end method
