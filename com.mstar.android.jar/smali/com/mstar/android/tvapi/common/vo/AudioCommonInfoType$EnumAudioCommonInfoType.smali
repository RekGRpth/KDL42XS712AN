.class public final enum Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;
.super Ljava/lang/Enum;
.source "AudioCommonInfoType.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EnumAudioCommonInfoType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

.field public static final enum E_AUDIO_COMMON_INFO_TYPE_1ms_PTS_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

.field public static final enum E_AUDIO_COMMON_INFO_TYPE_33Bit_PTS_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

.field public static final enum E_AUDIO_COMMON_INFO_TYPE_33Bit_STCPTS_DIFF_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

.field public static final enum E_AUDIO_COMMON_INFO_TYPE_ADC1_InputGain_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

.field public static final enum E_AUDIO_COMMON_INFO_TYPE_ADC_InputGain_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

.field public static final enum E_AUDIO_COMMON_INFO_TYPE_AD_OutputStyle_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

.field public static final enum E_AUDIO_COMMON_INFO_TYPE_ChannelMode_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

.field public static final enum E_AUDIO_COMMON_INFO_TYPE_CompressBin_DDRAddress_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

.field public static final enum E_AUDIO_COMMON_INFO_TYPE_CompressBin_LoadCode_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

.field public static final enum E_AUDIO_COMMON_INFO_TYPE_DEC1_BufferAddr_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

.field public static final enum E_AUDIO_COMMON_INFO_TYPE_DEC1_BufferSize_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

.field public static final enum E_AUDIO_COMMON_INFO_TYPE_DEC1_ESBufferSize_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

.field public static final enum E_AUDIO_COMMON_INFO_TYPE_DEC1_MMResidualPCM_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

.field public static final enum E_AUDIO_COMMON_INFO_TYPE_DEC1_MMTag_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

.field public static final enum E_AUDIO_COMMON_INFO_TYPE_DEC1_PCMBufferSize_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

.field public static final enum E_AUDIO_COMMON_INFO_TYPE_DEC1_setBufferProcess_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

.field public static final enum E_AUDIO_COMMON_INFO_TYPE_DEC2_BufferAddr_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

.field public static final enum E_AUDIO_COMMON_INFO_TYPE_DEC2_BufferSize_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

.field public static final enum E_AUDIO_COMMON_INFO_TYPE_DEC2_MMResidualPCM_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

.field public static final enum E_AUDIO_COMMON_INFO_TYPE_DEC2_MMTag_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

.field public static final enum E_AUDIO_COMMON_INFO_TYPE_DMAReader_BufferLevel_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

.field public static final enum E_AUDIO_COMMON_INFO_TYPE_DMAReader_Command_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

.field public static final enum E_AUDIO_COMMON_INFO_TYPE_DecOutMode_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

.field public static final enum E_AUDIO_COMMON_INFO_TYPE_DecStatus:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

.field public static final enum E_AUDIO_COMMON_INFO_TYPE_DecodeErrorCnt_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

.field public static final enum E_AUDIO_COMMON_INFO_TYPE_GetSCMS_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

.field public static final enum E_AUDIO_COMMON_INFO_TYPE_Get_CurSynthRate_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

.field public static final enum E_AUDIO_COMMON_INFO_TYPE_Get_MENU_KEY_CNT_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

.field public static final enum E_AUDIO_COMMON_INFO_TYPE_Get_MENU_WT_PTR_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

.field public static final enum E_AUDIO_COMMON_INFO_TYPE_Get_UNI_ES_MEMCNT_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

.field public static final enum E_AUDIO_COMMON_INFO_TYPE_KTV_SetType_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

.field public static final enum E_AUDIO_COMMON_INFO_TYPE_MMFileSize_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

.field public static final enum E_AUDIO_COMMON_INFO_TYPE_MM_FFx2_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

.field public static final enum E_AUDIO_COMMON_INFO_TYPE_PTS_info_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

.field public static final enum E_AUDIO_COMMON_INFO_TYPE_RTSP_Mem_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

.field public static final enum E_AUDIO_COMMON_INFO_TYPE_ReadByte_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

.field public static final enum E_AUDIO_COMMON_INFO_TYPE_SampleRate_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

.field public static final enum E_AUDIO_COMMON_INFO_TYPE_SetSCMS_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

.field public static final enum E_AUDIO_COMMON_INFO_TYPE_Set_MENU_WT_PTR_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

.field public static final enum E_AUDIO_COMMON_INFO_TYPE_Set_UNI_ES_Wptr_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

.field public static final enum E_AUDIO_COMMON_INFO_TYPE_Set_UNI_NEED_DECODE_FRMCNT_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

.field public static final enum E_AUDIO_COMMON_INFO_TYPE_SoundMode_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

.field public static final enum E_AUDIO_COMMON_INFO_TYPE_WriteByte_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

.field public static final enum E_AUDIO_COMMON_INFO_TYPE_getHDMI_CopyRight_C_Bit_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

.field public static final enum E_AUDIO_COMMON_INFO_TYPE_getHDMI_CopyRight_L_Bit_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

.field public static final enum E_AUDIO_COMMON_INFO_TYPE_getNR_Status_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

.field public static final enum E_AUDIO_COMMON_INFO_TYPE_getSPDIF_FS_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

.field public static final enum E_AUDIO_COMMON_INFO_TYPE_getSignal_Energy_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

.field public static final enum E_AUDIO_COMMON_INFO_TYPE_hdmiTx_outFreq_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

.field public static final enum E_AUDIO_COMMON_INFO_TYPE_hdmiTx_outType_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

.field public static final enum E_AUDIO_COMMON_INFO_TYPE_setBypassSPDIF_PAPB_chk_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

.field public static final enum E_AUDIO_COMMON_INFO_TYPE_setES_REQ_SZ_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

.field public static final enum E_AUDIO_COMMON_INFO_TYPE_setNR_Threshold_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

.field public static final enum E_AUDIO_COMMON_INFO_TYPE_setSPDIF_FS_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

.field public static final enum E_AUDIO_COMMON_INFO_TYPE_setSpdifDelay_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

.field public static final enum E_AUDIO_COMMON_INFO_TYPE_setSpdif_BufferProcess_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

.field private static seq:I


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    const-string v1, "E_AUDIO_COMMON_INFO_TYPE_DecStatus"

    invoke-direct {v0, v1, v4, v4}, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_DecStatus:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    const-string v1, "E_AUDIO_COMMON_INFO_TYPE_SampleRate_"

    invoke-direct {v0, v1, v5, v5}, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_SampleRate_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    const-string v1, "E_AUDIO_COMMON_INFO_TYPE_SoundMode_"

    invoke-direct {v0, v1, v6, v6}, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_SoundMode_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    const-string v1, "E_AUDIO_COMMON_INFO_TYPE_DecOutMode_"

    invoke-direct {v0, v1, v7, v7}, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_DecOutMode_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    const-string v1, "E_AUDIO_COMMON_INFO_TYPE_ChannelMode_"

    invoke-direct {v0, v1, v8, v8}, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_ChannelMode_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    const-string v1, "E_AUDIO_COMMON_INFO_TYPE_MMFileSize_"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_MMFileSize_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    const-string v1, "E_AUDIO_COMMON_INFO_TYPE_33Bit_PTS_"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_33Bit_PTS_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    const-string v1, "E_AUDIO_COMMON_INFO_TYPE_33Bit_STCPTS_DIFF_"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_33Bit_STCPTS_DIFF_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    const-string v1, "E_AUDIO_COMMON_INFO_TYPE_1ms_PTS_"

    const/16 v2, 0x8

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_1ms_PTS_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    const-string v1, "E_AUDIO_COMMON_INFO_TYPE_DEC1_BufferSize_"

    const/16 v2, 0x9

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_DEC1_BufferSize_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    const-string v1, "E_AUDIO_COMMON_INFO_TYPE_DEC1_BufferAddr_"

    const/16 v2, 0xa

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_DEC1_BufferAddr_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    const-string v1, "E_AUDIO_COMMON_INFO_TYPE_DEC1_MMTag_"

    const/16 v2, 0xb

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_DEC1_MMTag_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    const-string v1, "E_AUDIO_COMMON_INFO_TYPE_DEC1_MMResidualPCM_"

    const/16 v2, 0xc

    const/16 v3, 0xc

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_DEC1_MMResidualPCM_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    const-string v1, "E_AUDIO_COMMON_INFO_TYPE_DEC1_ESBufferSize_"

    const/16 v2, 0xd

    const/16 v3, 0xd

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_DEC1_ESBufferSize_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    const-string v1, "E_AUDIO_COMMON_INFO_TYPE_DEC1_PCMBufferSize_"

    const/16 v2, 0xe

    const/16 v3, 0xe

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_DEC1_PCMBufferSize_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    const-string v1, "E_AUDIO_COMMON_INFO_TYPE_DEC2_BufferSize_"

    const/16 v2, 0xf

    const/16 v3, 0xf

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_DEC2_BufferSize_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    const-string v1, "E_AUDIO_COMMON_INFO_TYPE_DEC2_BufferAddr_"

    const/16 v2, 0x10

    const/16 v3, 0x10

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_DEC2_BufferAddr_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    const-string v1, "E_AUDIO_COMMON_INFO_TYPE_DEC2_MMTag_"

    const/16 v2, 0x11

    const/16 v3, 0x11

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_DEC2_MMTag_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    const-string v1, "E_AUDIO_COMMON_INFO_TYPE_DEC2_MMResidualPCM_"

    const/16 v2, 0x12

    const/16 v3, 0x12

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_DEC2_MMResidualPCM_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    const-string v1, "E_AUDIO_COMMON_INFO_TYPE_DecodeErrorCnt_"

    const/16 v2, 0x13

    const/16 v3, 0x13

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_DecodeErrorCnt_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    const-string v1, "E_AUDIO_COMMON_INFO_TYPE_MM_FFx2_"

    const/16 v2, 0x14

    const/16 v3, 0x14

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_MM_FFx2_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    const-string v1, "E_AUDIO_COMMON_INFO_TYPE_setBypassSPDIF_PAPB_chk_"

    const/16 v2, 0x15

    const/16 v3, 0x15

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_setBypassSPDIF_PAPB_chk_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    const-string v1, "E_AUDIO_COMMON_INFO_TYPE_CompressBin_LoadCode_"

    const/16 v2, 0x16

    const/16 v3, 0x16

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_CompressBin_LoadCode_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    const-string v1, "E_AUDIO_COMMON_INFO_TYPE_CompressBin_DDRAddress_"

    const/16 v2, 0x17

    const/16 v3, 0x17

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_CompressBin_DDRAddress_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    const-string v1, "E_AUDIO_COMMON_INFO_TYPE_DMAReader_BufferLevel_"

    const/16 v2, 0x18

    const/16 v3, 0x18

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_DMAReader_BufferLevel_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    const-string v1, "E_AUDIO_COMMON_INFO_TYPE_DMAReader_Command_"

    const/16 v2, 0x19

    const/16 v3, 0x19

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_DMAReader_Command_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    const-string v1, "E_AUDIO_COMMON_INFO_TYPE_SetSCMS_"

    const/16 v2, 0x1a

    const/16 v3, 0x1a

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_SetSCMS_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    const-string v1, "E_AUDIO_COMMON_INFO_TYPE_GetSCMS_"

    const/16 v2, 0x1b

    const/16 v3, 0x1b

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_GetSCMS_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    const-string v1, "E_AUDIO_COMMON_INFO_TYPE_ADC_InputGain_"

    const/16 v2, 0x1c

    const/16 v3, 0x1c

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_ADC_InputGain_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    const-string v1, "E_AUDIO_COMMON_INFO_TYPE_KTV_SetType_"

    const/16 v2, 0x1d

    const/16 v3, 0x1d

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_KTV_SetType_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    const-string v1, "E_AUDIO_COMMON_INFO_TYPE_getSignal_Energy_"

    const/16 v2, 0x1e

    const/16 v3, 0x1e

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_getSignal_Energy_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    const-string v1, "E_AUDIO_COMMON_INFO_TYPE_getNR_Status_"

    const/16 v2, 0x1f

    const/16 v3, 0x1f

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_getNR_Status_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    const-string v1, "E_AUDIO_COMMON_INFO_TYPE_setNR_Threshold_"

    const/16 v2, 0x20

    const/16 v3, 0x20

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_setNR_Threshold_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    const-string v1, "E_AUDIO_COMMON_INFO_TYPE_setSPDIF_FS_"

    const/16 v2, 0x21

    const/16 v3, 0x21

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_setSPDIF_FS_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    const-string v1, "E_AUDIO_COMMON_INFO_TYPE_getSPDIF_FS_"

    const/16 v2, 0x22

    const/16 v3, 0x22

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_getSPDIF_FS_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    const-string v1, "E_AUDIO_COMMON_INFO_TYPE_setSpdifDelay_"

    const/16 v2, 0x23

    const/16 v3, 0x23

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_setSpdifDelay_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    const-string v1, "E_AUDIO_COMMON_INFO_TYPE_ReadByte_"

    const/16 v2, 0x24

    const/16 v3, 0x24

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_ReadByte_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    const-string v1, "E_AUDIO_COMMON_INFO_TYPE_WriteByte_"

    const/16 v2, 0x25

    const/16 v3, 0x25

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_WriteByte_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    const-string v1, "E_AUDIO_COMMON_INFO_TYPE_hdmiTx_outType_"

    const/16 v2, 0x26

    const/16 v3, 0x26

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_hdmiTx_outType_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    const-string v1, "E_AUDIO_COMMON_INFO_TYPE_hdmiTx_outFreq_"

    const/16 v2, 0x27

    const/16 v3, 0x27

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_hdmiTx_outFreq_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    const-string v1, "E_AUDIO_COMMON_INFO_TYPE_PTS_info_"

    const/16 v2, 0x28

    const/16 v3, 0x28

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_PTS_info_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    const-string v1, "E_AUDIO_COMMON_INFO_TYPE_RTSP_Mem_"

    const/16 v2, 0x29

    const/16 v3, 0x29

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_RTSP_Mem_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    const-string v1, "E_AUDIO_COMMON_INFO_TYPE_setSpdif_BufferProcess_"

    const/16 v2, 0x2a

    const/16 v3, 0x2a

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_setSpdif_BufferProcess_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    const-string v1, "E_AUDIO_COMMON_INFO_TYPE_DEC1_setBufferProcess_"

    const/16 v2, 0x2b

    const/16 v3, 0x2b

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_DEC1_setBufferProcess_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    const-string v1, "E_AUDIO_COMMON_INFO_TYPE_setES_REQ_SZ_"

    const/16 v2, 0x2c

    const/16 v3, 0x2c

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_setES_REQ_SZ_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    const-string v1, "E_AUDIO_COMMON_INFO_TYPE_AD_OutputStyle_"

    const/16 v2, 0x2d

    const/16 v3, 0x2d

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_AD_OutputStyle_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    const-string v1, "E_AUDIO_COMMON_INFO_TYPE_getHDMI_CopyRight_C_Bit_"

    const/16 v2, 0x2e

    const/16 v3, 0x2e

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_getHDMI_CopyRight_C_Bit_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    const-string v1, "E_AUDIO_COMMON_INFO_TYPE_getHDMI_CopyRight_L_Bit_"

    const/16 v2, 0x2f

    const/16 v3, 0x2f

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_getHDMI_CopyRight_L_Bit_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    const-string v1, "E_AUDIO_COMMON_INFO_TYPE_Set_UNI_NEED_DECODE_FRMCNT_"

    const/16 v2, 0x30

    const/16 v3, 0x30

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_Set_UNI_NEED_DECODE_FRMCNT_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    const-string v1, "E_AUDIO_COMMON_INFO_TYPE_Set_UNI_ES_Wptr_"

    const/16 v2, 0x31

    const/16 v3, 0x31

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_Set_UNI_ES_Wptr_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    const-string v1, "E_AUDIO_COMMON_INFO_TYPE_Get_UNI_ES_MEMCNT_"

    const/16 v2, 0x32

    const/16 v3, 0x32

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_Get_UNI_ES_MEMCNT_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    const-string v1, "E_AUDIO_COMMON_INFO_TYPE_Set_MENU_WT_PTR_"

    const/16 v2, 0x33

    const/16 v3, 0x33

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_Set_MENU_WT_PTR_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    const-string v1, "E_AUDIO_COMMON_INFO_TYPE_Get_MENU_WT_PTR_"

    const/16 v2, 0x34

    const/16 v3, 0x34

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_Get_MENU_WT_PTR_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    const-string v1, "E_AUDIO_COMMON_INFO_TYPE_Get_MENU_KEY_CNT_"

    const/16 v2, 0x35

    const/16 v3, 0x35

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_Get_MENU_KEY_CNT_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    const-string v1, "E_AUDIO_COMMON_INFO_TYPE_Get_CurSynthRate_"

    const/16 v2, 0x36

    const/16 v3, 0x36

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_Get_CurSynthRate_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    const-string v1, "E_AUDIO_COMMON_INFO_TYPE_ADC1_InputGain_"

    const/16 v2, 0x37

    const/16 v3, 0x37

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_ADC1_InputGain_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    const/16 v0, 0x38

    new-array v0, v0, [Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_DecStatus:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_SampleRate_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_SoundMode_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_DecOutMode_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_ChannelMode_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_MMFileSize_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_33Bit_PTS_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_33Bit_STCPTS_DIFF_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_1ms_PTS_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_DEC1_BufferSize_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_DEC1_BufferAddr_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_DEC1_MMTag_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_DEC1_MMResidualPCM_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_DEC1_ESBufferSize_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_DEC1_PCMBufferSize_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_DEC2_BufferSize_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_DEC2_BufferAddr_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_DEC2_MMTag_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_DEC2_MMResidualPCM_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_DecodeErrorCnt_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_MM_FFx2_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_setBypassSPDIF_PAPB_chk_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_CompressBin_LoadCode_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_CompressBin_DDRAddress_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_DMAReader_BufferLevel_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_DMAReader_Command_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_SetSCMS_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_GetSCMS_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_ADC_InputGain_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_KTV_SetType_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_getSignal_Energy_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_getNR_Status_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_setNR_Threshold_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_setSPDIF_FS_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_getSPDIF_FS_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_setSpdifDelay_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_ReadByte_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_WriteByte_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_hdmiTx_outType_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_hdmiTx_outFreq_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_PTS_info_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_RTSP_Mem_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_setSpdif_BufferProcess_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_DEC1_setBufferProcess_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_setES_REQ_SZ_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_AD_OutputStyle_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_getHDMI_CopyRight_C_Bit_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_getHDMI_CopyRight_L_Bit_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    aput-object v2, v0, v1

    const/16 v1, 0x30

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_Set_UNI_NEED_DECODE_FRMCNT_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    aput-object v2, v0, v1

    const/16 v1, 0x31

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_Set_UNI_ES_Wptr_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    aput-object v2, v0, v1

    const/16 v1, 0x32

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_Get_UNI_ES_MEMCNT_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    aput-object v2, v0, v1

    const/16 v1, 0x33

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_Set_MENU_WT_PTR_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    aput-object v2, v0, v1

    const/16 v1, 0x34

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_Get_MENU_WT_PTR_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    aput-object v2, v0, v1

    const/16 v1, 0x35

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_Get_MENU_KEY_CNT_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    aput-object v2, v0, v1

    const/16 v1, 0x36

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_Get_CurSynthRate_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    aput-object v2, v0, v1

    const/16 v1, 0x37

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->E_AUDIO_COMMON_INFO_TYPE_ADC1_InputGain_:Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->value:I

    invoke-static {p3}, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->setHashtableValue(I)V

    return-void
.end method

.method public static getOrdinalThroughValue(I)I
    .locals 3
    .param p0    # I

    # getter for: Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType;->htEnumAudioCommonInfoType:Ljava/util/Hashtable;
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType;->access$000()Ljava/util/Hashtable;

    move-result-object v1

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    :goto_0
    return v1

    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method private static setHashtableValue(I)V
    .locals 4
    .param p0    # I

    # getter for: Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType;->htEnumAudioCommonInfoType:Ljava/util/Hashtable;
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType;->access$000()Ljava/util/Hashtable;

    move-result-object v0

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, p0}, Ljava/lang/Integer;-><init>(I)V

    new-instance v2, Ljava/lang/Integer;

    sget v3, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->seq:I

    invoke-direct {v2, v3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->seq:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->seq:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    invoke-virtual {v0}, [Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->value:I

    return v0
.end method
