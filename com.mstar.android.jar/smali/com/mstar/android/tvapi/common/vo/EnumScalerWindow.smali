.class public final enum Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;
.super Ljava/lang/Enum;
.source "EnumScalerWindow.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;

.field public static final enum E_MAIN_WINDOW:Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;

.field public static final enum E_MAX_WINDOW:Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;

.field public static final enum E_SUB_WINDOW:Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;

    const-string v1, "E_MAIN_WINDOW"

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;->E_MAIN_WINDOW:Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;

    const-string v1, "E_SUB_WINDOW"

    invoke-direct {v0, v1, v3}, Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;->E_SUB_WINDOW:Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;

    const-string v1, "E_MAX_WINDOW"

    invoke-direct {v0, v1, v4}, Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;->E_MAX_WINDOW:Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;->E_MAIN_WINDOW:Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;

    aput-object v1, v0, v2

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;->E_SUB_WINDOW:Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;->E_MAX_WINDOW:Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;

    aput-object v1, v0, v4

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;

    invoke-virtual {v0}, [Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;

    return-object v0
.end method
