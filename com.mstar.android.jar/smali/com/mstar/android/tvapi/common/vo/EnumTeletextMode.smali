.class public final enum Lcom/mstar/android/tvapi/common/vo/EnumTeletextMode;
.super Ljava/lang/Enum;
.source "EnumTeletextMode.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/tvapi/common/vo/EnumTeletextMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumTeletextMode;

.field public static final enum TTX_MODE_CLOCK:Lcom/mstar/android/tvapi/common/vo/EnumTeletextMode;

.field public static final enum TTX_MODE_NORMAL:Lcom/mstar/android/tvapi/common/vo/EnumTeletextMode;

.field public static final enum TTX_MODE_STATUS:Lcom/mstar/android/tvapi/common/vo/EnumTeletextMode;

.field public static final enum TTX_MODE_SUBTITLE:Lcom/mstar/android/tvapi/common/vo/EnumTeletextMode;

.field public static final enum TTX_MODE_SUBTITLE_NAVIGATION:Lcom/mstar/android/tvapi/common/vo/EnumTeletextMode;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextMode;

    const-string v1, "TTX_MODE_NORMAL"

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumTeletextMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextMode;->TTX_MODE_NORMAL:Lcom/mstar/android/tvapi/common/vo/EnumTeletextMode;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextMode;

    const-string v1, "TTX_MODE_CLOCK"

    invoke-direct {v0, v1, v3}, Lcom/mstar/android/tvapi/common/vo/EnumTeletextMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextMode;->TTX_MODE_CLOCK:Lcom/mstar/android/tvapi/common/vo/EnumTeletextMode;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextMode;

    const-string v1, "TTX_MODE_SUBTITLE"

    invoke-direct {v0, v1, v4}, Lcom/mstar/android/tvapi/common/vo/EnumTeletextMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextMode;->TTX_MODE_SUBTITLE:Lcom/mstar/android/tvapi/common/vo/EnumTeletextMode;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextMode;

    const-string v1, "TTX_MODE_STATUS"

    invoke-direct {v0, v1, v5}, Lcom/mstar/android/tvapi/common/vo/EnumTeletextMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextMode;->TTX_MODE_STATUS:Lcom/mstar/android/tvapi/common/vo/EnumTeletextMode;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextMode;

    const-string v1, "TTX_MODE_SUBTITLE_NAVIGATION"

    invoke-direct {v0, v1, v6}, Lcom/mstar/android/tvapi/common/vo/EnumTeletextMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextMode;->TTX_MODE_SUBTITLE_NAVIGATION:Lcom/mstar/android/tvapi/common/vo/EnumTeletextMode;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/mstar/android/tvapi/common/vo/EnumTeletextMode;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumTeletextMode;->TTX_MODE_NORMAL:Lcom/mstar/android/tvapi/common/vo/EnumTeletextMode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumTeletextMode;->TTX_MODE_CLOCK:Lcom/mstar/android/tvapi/common/vo/EnumTeletextMode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumTeletextMode;->TTX_MODE_SUBTITLE:Lcom/mstar/android/tvapi/common/vo/EnumTeletextMode;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumTeletextMode;->TTX_MODE_STATUS:Lcom/mstar/android/tvapi/common/vo/EnumTeletextMode;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumTeletextMode;->TTX_MODE_SUBTITLE_NAVIGATION:Lcom/mstar/android/tvapi/common/vo/EnumTeletextMode;

    aput-object v1, v0, v6

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextMode;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumTeletextMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/tvapi/common/vo/EnumTeletextMode;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextMode;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/tvapi/common/vo/EnumTeletextMode;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextMode;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumTeletextMode;

    invoke-virtual {v0}, [Lcom/mstar/android/tvapi/common/vo/EnumTeletextMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/tvapi/common/vo/EnumTeletextMode;

    return-object v0
.end method
