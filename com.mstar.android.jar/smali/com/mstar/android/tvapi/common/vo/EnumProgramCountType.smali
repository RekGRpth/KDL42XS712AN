.class public final enum Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;
.super Ljava/lang/Enum;
.source "EnumProgramCountType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;

.field public static final enum E_COUNT_ATV:Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;

.field public static final enum E_COUNT_ATV_DTV:Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;

.field public static final enum E_COUNT_DTV:Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;

.field public static final enum E_COUNT_DTV_DATA:Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;

.field public static final enum E_COUNT_DTV_RADIO:Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;

.field public static final enum E_COUNT_DTV_TV:Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;

.field public static final enum E_COUNT_TYPE_MAX:Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;

    const-string v1, "E_COUNT_ATV_DTV"

    invoke-direct {v0, v1, v3}, Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;->E_COUNT_ATV_DTV:Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;

    const-string v1, "E_COUNT_ATV"

    invoke-direct {v0, v1, v4}, Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;->E_COUNT_ATV:Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;

    const-string v1, "E_COUNT_DTV"

    invoke-direct {v0, v1, v5}, Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;->E_COUNT_DTV:Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;

    const-string v1, "E_COUNT_DTV_TV"

    invoke-direct {v0, v1, v6}, Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;->E_COUNT_DTV_TV:Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;

    const-string v1, "E_COUNT_DTV_RADIO"

    invoke-direct {v0, v1, v7}, Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;->E_COUNT_DTV_RADIO:Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;

    const-string v1, "E_COUNT_DTV_DATA"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;->E_COUNT_DTV_DATA:Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;

    const-string v1, "E_COUNT_TYPE_MAX"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;->E_COUNT_TYPE_MAX:Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;

    const/4 v0, 0x7

    new-array v0, v0, [Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;->E_COUNT_ATV_DTV:Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;->E_COUNT_ATV:Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;->E_COUNT_DTV:Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;->E_COUNT_DTV_TV:Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;->E_COUNT_DTV_RADIO:Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;->E_COUNT_DTV_DATA:Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;->E_COUNT_TYPE_MAX:Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;

    invoke-virtual {v0}, [Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;

    return-object v0
.end method
