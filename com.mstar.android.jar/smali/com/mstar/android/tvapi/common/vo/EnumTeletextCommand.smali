.class public final enum Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;
.super Ljava/lang/Enum;
.source "EnumTeletextCommand.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

.field public static final enum CLEAR_LIST:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

.field public static final enum CLOCK:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

.field public static final enum CYAN:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

.field public static final enum DIGIT_0:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

.field public static final enum DIGIT_1:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

.field public static final enum DIGIT_2:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

.field public static final enum DIGIT_3:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

.field public static final enum DIGIT_4:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

.field public static final enum DIGIT_5:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

.field public static final enum DIGIT_6:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

.field public static final enum DIGIT_7:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

.field public static final enum DIGIT_8:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

.field public static final enum DIGIT_9:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

.field public static final enum GOTO_PAGE:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

.field public static final enum GREEN:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

.field public static final enum HOLD:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

.field public static final enum INDEX:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

.field public static final enum LIST:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

.field public static final enum MAX_TEXT_COMMAND:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

.field public static final enum MIX:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

.field public static final enum MIX_TEXT:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

.field public static final enum NORMAL_MODE_NEXT_PHASE:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

.field public static final enum PAGE_DOWN:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

.field public static final enum PAGE_LEFT:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

.field public static final enum PAGE_RIGHT:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

.field public static final enum PAGE_UP:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

.field public static final enum RED:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

.field public static final enum REVEAL:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

.field public static final enum SETUP_BEFORE_UPDATE_PAGE_HANDLER:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

.field public static final enum SIZE:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

.field public static final enum STATUS_DISPLAY:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

.field public static final enum SUBPAGE:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

.field public static final enum SUBTITLE_NAVIGATION:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

.field public static final enum SUBTITLE_TTX_ON:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

.field public static final enum TEXT:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

.field public static final enum TIME:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

.field public static final enum TV:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

.field public static final enum UPDATE:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

.field public static final enum YELLOW:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    const-string v1, "DIGIT_0"

    invoke-direct {v0, v1, v3}, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->DIGIT_0:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    const-string v1, "DIGIT_1"

    invoke-direct {v0, v1, v4}, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->DIGIT_1:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    const-string v1, "DIGIT_2"

    invoke-direct {v0, v1, v5}, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->DIGIT_2:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    const-string v1, "DIGIT_3"

    invoke-direct {v0, v1, v6}, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->DIGIT_3:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    const-string v1, "DIGIT_4"

    invoke-direct {v0, v1, v7}, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->DIGIT_4:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    const-string v1, "DIGIT_5"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->DIGIT_5:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    const-string v1, "DIGIT_6"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->DIGIT_6:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    const-string v1, "DIGIT_7"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->DIGIT_7:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    const-string v1, "DIGIT_8"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->DIGIT_8:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    const-string v1, "DIGIT_9"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->DIGIT_9:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    const-string v1, "PAGE_UP"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->PAGE_UP:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    const-string v1, "PAGE_DOWN"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->PAGE_DOWN:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    const-string v1, "SUBPAGE"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->SUBPAGE:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    const-string v1, "PAGE_RIGHT"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->PAGE_RIGHT:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    const-string v1, "PAGE_LEFT"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->PAGE_LEFT:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    const-string v1, "RED"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->RED:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    const-string v1, "GREEN"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->GREEN:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    const-string v1, "YELLOW"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->YELLOW:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    const-string v1, "CYAN"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->CYAN:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    const-string v1, "MIX"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->MIX:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    const-string v1, "TEXT"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->TEXT:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    const-string v1, "TV"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->TV:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    const-string v1, "UPDATE"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->UPDATE:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    const-string v1, "INDEX"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->INDEX:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    const-string v1, "HOLD"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->HOLD:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    const-string v1, "LIST"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->LIST:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    const-string v1, "TIME"

    const/16 v2, 0x1a

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->TIME:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    const-string v1, "SIZE"

    const/16 v2, 0x1b

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->SIZE:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    const-string v1, "REVEAL"

    const/16 v2, 0x1c

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->REVEAL:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    const-string v1, "CLOCK"

    const/16 v2, 0x1d

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->CLOCK:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    const-string v1, "SUBTITLE_TTX_ON"

    const/16 v2, 0x1e

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->SUBTITLE_TTX_ON:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    const-string v1, "SUBTITLE_NAVIGATION"

    const/16 v2, 0x1f

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->SUBTITLE_NAVIGATION:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    const-string v1, "STATUS_DISPLAY"

    const/16 v2, 0x20

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->STATUS_DISPLAY:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    const-string v1, "CLEAR_LIST"

    const/16 v2, 0x21

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->CLEAR_LIST:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    const-string v1, "MIX_TEXT"

    const/16 v2, 0x22

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->MIX_TEXT:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    const-string v1, "SETUP_BEFORE_UPDATE_PAGE_HANDLER"

    const/16 v2, 0x23

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->SETUP_BEFORE_UPDATE_PAGE_HANDLER:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    const-string v1, "NORMAL_MODE_NEXT_PHASE"

    const/16 v2, 0x24

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->NORMAL_MODE_NEXT_PHASE:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    const-string v1, "GOTO_PAGE"

    const/16 v2, 0x25

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->GOTO_PAGE:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    const-string v1, "MAX_TEXT_COMMAND"

    const/16 v2, 0x26

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->MAX_TEXT_COMMAND:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    const/16 v0, 0x27

    new-array v0, v0, [Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->DIGIT_0:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->DIGIT_1:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->DIGIT_2:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->DIGIT_3:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    aput-object v1, v0, v6

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->DIGIT_4:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->DIGIT_5:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->DIGIT_6:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->DIGIT_7:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->DIGIT_8:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->DIGIT_9:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->PAGE_UP:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->PAGE_DOWN:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->SUBPAGE:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->PAGE_RIGHT:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->PAGE_LEFT:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->RED:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->GREEN:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->YELLOW:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->CYAN:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->MIX:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->TEXT:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->TV:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->UPDATE:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->INDEX:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->HOLD:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->LIST:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->TIME:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->SIZE:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->REVEAL:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->CLOCK:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->SUBTITLE_TTX_ON:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->SUBTITLE_NAVIGATION:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->STATUS_DISPLAY:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->CLEAR_LIST:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->MIX_TEXT:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->SETUP_BEFORE_UPDATE_PAGE_HANDLER:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->NORMAL_MODE_NEXT_PHASE:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->GOTO_PAGE:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->MAX_TEXT_COMMAND:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    aput-object v2, v0, v1

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    invoke-virtual {v0}, [Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    return-object v0
.end method
