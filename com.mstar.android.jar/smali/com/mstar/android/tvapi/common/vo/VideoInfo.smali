.class public Lcom/mstar/android/tvapi/common/vo/VideoInfo;
.super Ljava/lang/Object;
.source "VideoInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mstar/android/tvapi/common/vo/VideoInfo$EnumScanType;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mstar/android/tvapi/common/vo/VideoInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private enScanType:I

.field public frameRate:I

.field public hResolution:I

.field public modeIndex:I

.field public vResolution:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/VideoInfo$1;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/common/vo/VideoInfo$1;-><init>()V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/VideoInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/VideoInfo;->hResolution:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/VideoInfo;->vResolution:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/VideoInfo;->frameRate:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/VideoInfo;->enScanType:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/VideoInfo;->modeIndex:I

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1    # Landroid/os/Parcel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/VideoInfo;->hResolution:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/VideoInfo;->vResolution:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/VideoInfo;->frameRate:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/VideoInfo;->enScanType:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/VideoInfo;->modeIndex:I

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getScanType()Lcom/mstar/android/tvapi/common/vo/VideoInfo$EnumScanType;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/VideoInfo;->enScanType:I

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/VideoInfo$EnumScanType;->E_PROGRESSIVE:Lcom/mstar/android/tvapi/common/vo/VideoInfo$EnumScanType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/VideoInfo$EnumScanType;->ordinal()I

    move-result v1

    if-lt v0, v1, :cond_0

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/VideoInfo;->enScanType:I

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/VideoInfo$EnumScanType;->E_INTERLACED:Lcom/mstar/android/tvapi/common/vo/VideoInfo$EnumScanType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/VideoInfo$EnumScanType;->ordinal()I

    move-result v1

    if-le v0, v1, :cond_1

    :cond_0
    new-instance v0, Lcom/mstar/android/tvapi/common/exception/TvCommonException;

    const-string v1, "enScanType is not in the range "

    invoke-direct {v0, v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/VideoInfo$EnumScanType;->values()[Lcom/mstar/android/tvapi/common/vo/VideoInfo$EnumScanType;

    move-result-object v0

    iget v1, p0, Lcom/mstar/android/tvapi/common/vo/VideoInfo;->enScanType:I

    aget-object v0, v0, v1

    return-object v0
.end method

.method public setScanType(Lcom/mstar/android/tvapi/common/vo/VideoInfo$EnumScanType;)V
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/vo/VideoInfo$EnumScanType;

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/VideoInfo$EnumScanType;->ordinal()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/VideoInfo;->enScanType:I

    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/VideoInfo;->hResolution:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/VideoInfo;->vResolution:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/VideoInfo;->frameRate:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/VideoInfo;->enScanType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/VideoInfo;->modeIndex:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
