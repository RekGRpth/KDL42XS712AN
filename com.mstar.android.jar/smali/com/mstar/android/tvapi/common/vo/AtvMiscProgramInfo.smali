.class public Lcom/mstar/android/tvapi/common/vo/AtvMiscProgramInfo;
.super Ljava/lang/Object;
.source "AtvMiscProgramInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mstar/android/tvapi/common/vo/AtvMiscProgramInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public bIsAutoColorSystem:Z

.field public eAudioMode:B

.field public eAudioStandard:B

.field public eMedium:Z

.field public eVideoStandard:B

.field public eVolumeCompensation:B

.field public isAutoFrequencyTuning:Z

.field public isDirectTuned:Z

.field public isDualAudioSelected:B

.field public isHide:Z

.field public isLock:Z

.field public isRealtimeAudioDetectionEnabled:Z

.field public isSkip:Z

.field public u8AutoFrequencyTuningOffset:S

.field public u8ChannelNumber:S

.field public u8Favorite:B

.field public unused:B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/AtvMiscProgramInfo$1;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/common/vo/AtvMiscProgramInfo$1;-><init>()V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/AtvMiscProgramInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-byte v0, p0, Lcom/mstar/android/tvapi/common/vo/AtvMiscProgramInfo;->eAudioStandard:B

    iput-boolean v0, p0, Lcom/mstar/android/tvapi/common/vo/AtvMiscProgramInfo;->isSkip:Z

    iput-boolean v0, p0, Lcom/mstar/android/tvapi/common/vo/AtvMiscProgramInfo;->isHide:Z

    iput-byte v0, p0, Lcom/mstar/android/tvapi/common/vo/AtvMiscProgramInfo;->eVideoStandard:B

    iput-byte v0, p0, Lcom/mstar/android/tvapi/common/vo/AtvMiscProgramInfo;->isDualAudioSelected:B

    iput-byte v0, p0, Lcom/mstar/android/tvapi/common/vo/AtvMiscProgramInfo;->eVolumeCompensation:B

    iput-byte v0, p0, Lcom/mstar/android/tvapi/common/vo/AtvMiscProgramInfo;->eAudioMode:B

    iput-boolean v0, p0, Lcom/mstar/android/tvapi/common/vo/AtvMiscProgramInfo;->isRealtimeAudioDetectionEnabled:Z

    iput-byte v0, p0, Lcom/mstar/android/tvapi/common/vo/AtvMiscProgramInfo;->u8Favorite:B

    iput-boolean v0, p0, Lcom/mstar/android/tvapi/common/vo/AtvMiscProgramInfo;->eMedium:Z

    iput-boolean v0, p0, Lcom/mstar/android/tvapi/common/vo/AtvMiscProgramInfo;->isLock:Z

    iput-short v0, p0, Lcom/mstar/android/tvapi/common/vo/AtvMiscProgramInfo;->u8ChannelNumber:S

    iput-boolean v0, p0, Lcom/mstar/android/tvapi/common/vo/AtvMiscProgramInfo;->isAutoFrequencyTuning:Z

    iput-boolean v0, p0, Lcom/mstar/android/tvapi/common/vo/AtvMiscProgramInfo;->isDirectTuned:Z

    iput-short v0, p0, Lcom/mstar/android/tvapi/common/vo/AtvMiscProgramInfo;->u8AutoFrequencyTuningOffset:S

    iput-boolean v0, p0, Lcom/mstar/android/tvapi/common/vo/AtvMiscProgramInfo;->bIsAutoColorSystem:Z

    iput-byte v0, p0, Lcom/mstar/android/tvapi/common/vo/AtvMiscProgramInfo;->unused:B

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3
    .param p1    # Landroid/os/Parcel;

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    iput-byte v0, p0, Lcom/mstar/android/tvapi/common/vo/AtvMiscProgramInfo;->eAudioStandard:B

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/mstar/android/tvapi/common/vo/AtvMiscProgramInfo;->isSkip:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/mstar/android/tvapi/common/vo/AtvMiscProgramInfo;->isHide:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    iput-byte v0, p0, Lcom/mstar/android/tvapi/common/vo/AtvMiscProgramInfo;->eVideoStandard:B

    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    iput-byte v0, p0, Lcom/mstar/android/tvapi/common/vo/AtvMiscProgramInfo;->isDualAudioSelected:B

    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    iput-byte v0, p0, Lcom/mstar/android/tvapi/common/vo/AtvMiscProgramInfo;->eVolumeCompensation:B

    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    iput-byte v0, p0, Lcom/mstar/android/tvapi/common/vo/AtvMiscProgramInfo;->eAudioMode:B

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/mstar/android/tvapi/common/vo/AtvMiscProgramInfo;->isRealtimeAudioDetectionEnabled:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    iput-byte v0, p0, Lcom/mstar/android/tvapi/common/vo/AtvMiscProgramInfo;->u8Favorite:B

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_3

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lcom/mstar/android/tvapi/common/vo/AtvMiscProgramInfo;->eMedium:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_4

    move v0, v1

    :goto_4
    iput-boolean v0, p0, Lcom/mstar/android/tvapi/common/vo/AtvMiscProgramInfo;->isLock:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/common/vo/AtvMiscProgramInfo;->u8ChannelNumber:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_5

    move v0, v1

    :goto_5
    iput-boolean v0, p0, Lcom/mstar/android/tvapi/common/vo/AtvMiscProgramInfo;->isAutoFrequencyTuning:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_6

    move v0, v1

    :goto_6
    iput-boolean v0, p0, Lcom/mstar/android/tvapi/common/vo/AtvMiscProgramInfo;->isDirectTuned:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/common/vo/AtvMiscProgramInfo;->u8AutoFrequencyTuningOffset:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_7

    :goto_7
    iput-boolean v1, p0, Lcom/mstar/android/tvapi/common/vo/AtvMiscProgramInfo;->bIsAutoColorSystem:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    iput-byte v0, p0, Lcom/mstar/android/tvapi/common/vo/AtvMiscProgramInfo;->unused:B

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_2

    :cond_3
    move v0, v2

    goto :goto_3

    :cond_4
    move v0, v2

    goto :goto_4

    :cond_5
    move v0, v2

    goto :goto_5

    :cond_6
    move v0, v2

    goto :goto_6

    :cond_7
    move v1, v2

    goto :goto_7
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-byte v0, p0, Lcom/mstar/android/tvapi/common/vo/AtvMiscProgramInfo;->eAudioStandard:B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    iget-boolean v0, p0, Lcom/mstar/android/tvapi/common/vo/AtvMiscProgramInfo;->isSkip:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/mstar/android/tvapi/common/vo/AtvMiscProgramInfo;->isHide:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-byte v0, p0, Lcom/mstar/android/tvapi/common/vo/AtvMiscProgramInfo;->eVideoStandard:B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    iget-byte v0, p0, Lcom/mstar/android/tvapi/common/vo/AtvMiscProgramInfo;->isDualAudioSelected:B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    iget-byte v0, p0, Lcom/mstar/android/tvapi/common/vo/AtvMiscProgramInfo;->eVolumeCompensation:B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    iget-byte v0, p0, Lcom/mstar/android/tvapi/common/vo/AtvMiscProgramInfo;->eAudioMode:B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    iget-boolean v0, p0, Lcom/mstar/android/tvapi/common/vo/AtvMiscProgramInfo;->isRealtimeAudioDetectionEnabled:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-byte v0, p0, Lcom/mstar/android/tvapi/common/vo/AtvMiscProgramInfo;->u8Favorite:B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    iget-boolean v0, p0, Lcom/mstar/android/tvapi/common/vo/AtvMiscProgramInfo;->eMedium:Z

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/mstar/android/tvapi/common/vo/AtvMiscProgramInfo;->isLock:Z

    if-eqz v0, :cond_4

    move v0, v1

    :goto_4
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/common/vo/AtvMiscProgramInfo;->u8ChannelNumber:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/mstar/android/tvapi/common/vo/AtvMiscProgramInfo;->isAutoFrequencyTuning:Z

    if-eqz v0, :cond_5

    move v0, v1

    :goto_5
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/mstar/android/tvapi/common/vo/AtvMiscProgramInfo;->isDirectTuned:Z

    if-eqz v0, :cond_6

    move v0, v1

    :goto_6
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/common/vo/AtvMiscProgramInfo;->u8AutoFrequencyTuningOffset:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/mstar/android/tvapi/common/vo/AtvMiscProgramInfo;->bIsAutoColorSystem:Z

    if-eqz v0, :cond_7

    :goto_7
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    iget-byte v0, p0, Lcom/mstar/android/tvapi/common/vo/AtvMiscProgramInfo;->unused:B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_2

    :cond_3
    move v0, v2

    goto :goto_3

    :cond_4
    move v0, v2

    goto :goto_4

    :cond_5
    move v0, v2

    goto :goto_5

    :cond_6
    move v0, v2

    goto :goto_6

    :cond_7
    move v1, v2

    goto :goto_7
.end method
