.class public final enum Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;
.super Ljava/lang/Enum;
.source "EnumThreeDVideoDisplayFormat.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;",
        ">;",
        "Landroid/os/Parcelable;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum E_ThreeD_Video_DISPLAYFORMAT_2DTO3D:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

.field public static final enum E_ThreeD_Video_DISPLAYFORMAT_AUTO:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

.field public static final enum E_ThreeD_Video_DISPLAYFORMAT_CHECK_BOARD:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

.field public static final enum E_ThreeD_Video_DISPLAYFORMAT_COUNT:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

.field public static final enum E_ThreeD_Video_DISPLAYFORMAT_FRAME_ALTERNATIVE:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

.field public static final enum E_ThreeD_Video_DISPLAYFORMAT_FRAME_PACKING:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

.field public static final enum E_ThreeD_Video_DISPLAYFORMAT_LINE_ALTERNATIVE:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

.field public static final enum E_ThreeD_Video_DISPLAYFORMAT_NONE:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

.field public static final enum E_ThreeD_Video_DISPLAYFORMAT_PIXEL_ALTERNATIVE:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

.field public static final enum E_ThreeD_Video_DISPLAYFORMAT_SIDE_BY_SIDE:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

.field public static final enum E_ThreeD_Video_DISPLAYFORMAT_TOP_BOTTOM:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    const-string v1, "E_ThreeD_Video_DISPLAYFORMAT_NONE"

    invoke-direct {v0, v1, v3}, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;->E_ThreeD_Video_DISPLAYFORMAT_NONE:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    const-string v1, "E_ThreeD_Video_DISPLAYFORMAT_SIDE_BY_SIDE"

    invoke-direct {v0, v1, v4}, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;->E_ThreeD_Video_DISPLAYFORMAT_SIDE_BY_SIDE:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    const-string v1, "E_ThreeD_Video_DISPLAYFORMAT_TOP_BOTTOM"

    invoke-direct {v0, v1, v5}, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;->E_ThreeD_Video_DISPLAYFORMAT_TOP_BOTTOM:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    const-string v1, "E_ThreeD_Video_DISPLAYFORMAT_FRAME_PACKING"

    invoke-direct {v0, v1, v6}, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;->E_ThreeD_Video_DISPLAYFORMAT_FRAME_PACKING:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    const-string v1, "E_ThreeD_Video_DISPLAYFORMAT_LINE_ALTERNATIVE"

    invoke-direct {v0, v1, v7}, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;->E_ThreeD_Video_DISPLAYFORMAT_LINE_ALTERNATIVE:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    const-string v1, "E_ThreeD_Video_DISPLAYFORMAT_2DTO3D"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;->E_ThreeD_Video_DISPLAYFORMAT_2DTO3D:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    const-string v1, "E_ThreeD_Video_DISPLAYFORMAT_AUTO"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;->E_ThreeD_Video_DISPLAYFORMAT_AUTO:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    const-string v1, "E_ThreeD_Video_DISPLAYFORMAT_CHECK_BOARD"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;->E_ThreeD_Video_DISPLAYFORMAT_CHECK_BOARD:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    const-string v1, "E_ThreeD_Video_DISPLAYFORMAT_PIXEL_ALTERNATIVE"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;->E_ThreeD_Video_DISPLAYFORMAT_PIXEL_ALTERNATIVE:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    const-string v1, "E_ThreeD_Video_DISPLAYFORMAT_FRAME_ALTERNATIVE"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;->E_ThreeD_Video_DISPLAYFORMAT_FRAME_ALTERNATIVE:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    const-string v1, "E_ThreeD_Video_DISPLAYFORMAT_COUNT"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;->E_ThreeD_Video_DISPLAYFORMAT_COUNT:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    const/16 v0, 0xb

    new-array v0, v0, [Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;->E_ThreeD_Video_DISPLAYFORMAT_NONE:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;->E_ThreeD_Video_DISPLAYFORMAT_SIDE_BY_SIDE:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;->E_ThreeD_Video_DISPLAYFORMAT_TOP_BOTTOM:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;->E_ThreeD_Video_DISPLAYFORMAT_FRAME_PACKING:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    aput-object v1, v0, v6

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;->E_ThreeD_Video_DISPLAYFORMAT_LINE_ALTERNATIVE:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;->E_ThreeD_Video_DISPLAYFORMAT_2DTO3D:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;->E_ThreeD_Video_DISPLAYFORMAT_AUTO:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;->E_ThreeD_Video_DISPLAYFORMAT_CHECK_BOARD:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;->E_ThreeD_Video_DISPLAYFORMAT_PIXEL_ALTERNATIVE:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;->E_ThreeD_Video_DISPLAYFORMAT_FRAME_ALTERNATIVE:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;->E_ThreeD_Video_DISPLAYFORMAT_COUNT:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    aput-object v2, v0, v1

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat$1;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat$1;-><init>()V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    invoke-virtual {v0}, [Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    invoke-virtual {p0}, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
