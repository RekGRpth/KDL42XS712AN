.class public Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;
.super Landroid/text/format/Time;
.source "TimerPowerOffModeStatus.java"


# instance fields
.field private enTimerPeriod:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/text/format/Time;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;->enTimerPeriod:I

    return-void
.end method


# virtual methods
.method public getTimerPeriod()Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;
    .locals 2

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;->values()[Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;

    move-result-object v0

    iget v1, p0, Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;->enTimerPeriod:I

    aget-object v0, v0, v1

    return-object v0
.end method

.method public setTimerPeriod(Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;)V
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;->ordinal()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;->enTimerPeriod:I

    return-void
.end method
