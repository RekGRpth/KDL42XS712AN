.class public final enum Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;
.super Ljava/lang/Enum;
.source "EnumFavoriteId.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;

.field public static final enum E_FAVORITE_ID_1:Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;

.field public static final enum E_FAVORITE_ID_2:Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;

.field public static final enum E_FAVORITE_ID_3:Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;

.field public static final enum E_FAVORITE_ID_4:Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;

.field public static final enum E_FAVORITE_ID_5:Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;

.field public static final enum E_FAVORITE_ID_6:Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;

.field public static final enum E_FAVORITE_ID_7:Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;

.field public static final enum E_FAVORITE_ID_8:Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const/4 v8, 0x3

    const/4 v7, 0x0

    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;

    const-string v1, "E_FAVORITE_ID_1"

    invoke-direct {v0, v1, v7, v4}, Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;->E_FAVORITE_ID_1:Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;

    const-string v1, "E_FAVORITE_ID_2"

    invoke-direct {v0, v1, v4, v5}, Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;->E_FAVORITE_ID_2:Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;

    const-string v1, "E_FAVORITE_ID_3"

    invoke-direct {v0, v1, v5, v6}, Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;->E_FAVORITE_ID_3:Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;

    const-string v1, "E_FAVORITE_ID_4"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v8, v2}, Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;->E_FAVORITE_ID_4:Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;

    const-string v1, "E_FAVORITE_ID_5"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v6, v2}, Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;->E_FAVORITE_ID_5:Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;

    const-string v1, "E_FAVORITE_ID_6"

    const/4 v2, 0x5

    const/16 v3, 0x20

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;->E_FAVORITE_ID_6:Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;

    const-string v1, "E_FAVORITE_ID_7"

    const/4 v2, 0x6

    const/16 v3, 0x40

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;->E_FAVORITE_ID_7:Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;

    const-string v1, "E_FAVORITE_ID_8"

    const/4 v2, 0x7

    const/16 v3, 0x80

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;->E_FAVORITE_ID_8:Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;

    const/16 v0, 0x8

    new-array v0, v0, [Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;->E_FAVORITE_ID_1:Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;

    aput-object v1, v0, v7

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;->E_FAVORITE_ID_2:Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;->E_FAVORITE_ID_3:Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;->E_FAVORITE_ID_4:Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;

    aput-object v1, v0, v8

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;->E_FAVORITE_ID_5:Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;

    aput-object v1, v0, v6

    const/4 v1, 0x5

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;->E_FAVORITE_ID_6:Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;->E_FAVORITE_ID_7:Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;->E_FAVORITE_ID_8:Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;

    aput-object v2, v0, v1

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;->value:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;

    invoke-virtual {v0}, [Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;->value:I

    return v0
.end method
