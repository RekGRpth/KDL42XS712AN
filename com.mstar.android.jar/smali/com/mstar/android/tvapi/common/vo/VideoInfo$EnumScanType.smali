.class public final enum Lcom/mstar/android/tvapi/common/vo/VideoInfo$EnumScanType;
.super Ljava/lang/Enum;
.source "VideoInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mstar/android/tvapi/common/vo/VideoInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EnumScanType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/tvapi/common/vo/VideoInfo$EnumScanType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/tvapi/common/vo/VideoInfo$EnumScanType;

.field public static final enum E_INTERLACED:Lcom/mstar/android/tvapi/common/vo/VideoInfo$EnumScanType;

.field public static final enum E_PROGRESSIVE:Lcom/mstar/android/tvapi/common/vo/VideoInfo$EnumScanType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/VideoInfo$EnumScanType;

    const-string v1, "E_PROGRESSIVE"

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/VideoInfo$EnumScanType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/VideoInfo$EnumScanType;->E_PROGRESSIVE:Lcom/mstar/android/tvapi/common/vo/VideoInfo$EnumScanType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/VideoInfo$EnumScanType;

    const-string v1, "E_INTERLACED"

    invoke-direct {v0, v1, v3}, Lcom/mstar/android/tvapi/common/vo/VideoInfo$EnumScanType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/VideoInfo$EnumScanType;->E_INTERLACED:Lcom/mstar/android/tvapi/common/vo/VideoInfo$EnumScanType;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/mstar/android/tvapi/common/vo/VideoInfo$EnumScanType;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/VideoInfo$EnumScanType;->E_PROGRESSIVE:Lcom/mstar/android/tvapi/common/vo/VideoInfo$EnumScanType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/VideoInfo$EnumScanType;->E_INTERLACED:Lcom/mstar/android/tvapi/common/vo/VideoInfo$EnumScanType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/VideoInfo$EnumScanType;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/VideoInfo$EnumScanType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/tvapi/common/vo/VideoInfo$EnumScanType;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/tvapi/common/vo/VideoInfo$EnumScanType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/common/vo/VideoInfo$EnumScanType;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/tvapi/common/vo/VideoInfo$EnumScanType;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/VideoInfo$EnumScanType;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/VideoInfo$EnumScanType;

    invoke-virtual {v0}, [Lcom/mstar/android/tvapi/common/vo/VideoInfo$EnumScanType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/tvapi/common/vo/VideoInfo$EnumScanType;

    return-object v0
.end method
