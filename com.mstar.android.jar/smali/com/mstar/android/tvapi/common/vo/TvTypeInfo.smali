.class public Lcom/mstar/android/tvapi/common/vo/TvTypeInfo;
.super Ljava/lang/Object;
.source "TvTypeInfo.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mstar/android/tvapi/common/vo/TvTypeInfo$EnumStbSystemType;,
        Lcom/mstar/android/tvapi/common/vo/TvTypeInfo$EnumAudioSystemType;,
        Lcom/mstar/android/tvapi/common/vo/TvTypeInfo$EnumAtvSystemType;,
        Lcom/mstar/android/tvapi/common/vo/TvTypeInfo$EnumTvProductType;
    }
.end annotation


# instance fields
.field private atvType:I

.field public audioType:I

.field public dtvType:S

.field public ipEnableType:I

.field public routePath:Ljava/lang/String;

.field public stbType:I

.field private tvType:I


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/TvTypeInfo;->tvType:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/TvTypeInfo;->atvType:I

    iput-short v0, p0, Lcom/mstar/android/tvapi/common/vo/TvTypeInfo;->dtvType:S

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/TvTypeInfo;->audioType:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/TvTypeInfo;->stbType:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/TvTypeInfo;->ipEnableType:I

    return-void
.end method


# virtual methods
.method public getAtvType()Lcom/mstar/android/tvapi/common/vo/TvTypeInfo$EnumAtvSystemType;
    .locals 2

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/TvTypeInfo;->atvType:I

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvTypeInfo$EnumAtvSystemType;->E_ATV_SYSTEM_TYPE_NTSC_ENABLE:Lcom/mstar/android/tvapi/common/vo/TvTypeInfo$EnumAtvSystemType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvTypeInfo$EnumAtvSystemType;->ordinal()I

    move-result v1

    if-lt v0, v1, :cond_0

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/TvTypeInfo;->atvType:I

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvTypeInfo$EnumAtvSystemType;->E_ATV_SYSTEM_TYPE_ATV_SYSTEM_ID_MAX:Lcom/mstar/android/tvapi/common/vo/TvTypeInfo$EnumAtvSystemType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvTypeInfo$EnumAtvSystemType;->ordinal()I

    move-result v1

    if-le v0, v1, :cond_1

    :cond_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvTypeInfo$EnumAtvSystemType;->values()[Lcom/mstar/android/tvapi/common/vo/TvTypeInfo$EnumAtvSystemType;

    move-result-object v0

    iget v1, p0, Lcom/mstar/android/tvapi/common/vo/TvTypeInfo;->atvType:I

    aget-object v0, v0, v1

    :goto_0
    return-object v0

    :cond_1
    sget-object v0, Lcom/mstar/android/tvapi/common/vo/TvTypeInfo$EnumAtvSystemType;->E_ATV_SYSTEM_TYPE_ATV_SYSTEM_ID_MAX:Lcom/mstar/android/tvapi/common/vo/TvTypeInfo$EnumAtvSystemType;

    goto :goto_0
.end method

.method public getAudioType()Lcom/mstar/android/tvapi/common/vo/TvTypeInfo$EnumAudioSystemType;
    .locals 2

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/TvTypeInfo;->audioType:I

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvTypeInfo$EnumAudioSystemType;->E_AUDIO_SYSTEM_TYPE_BTSC_ENABLE:Lcom/mstar/android/tvapi/common/vo/TvTypeInfo$EnumAudioSystemType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvTypeInfo$EnumAudioSystemType;->ordinal()I

    move-result v1

    if-lt v0, v1, :cond_0

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/TvTypeInfo;->audioType:I

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvTypeInfo$EnumAudioSystemType;->E_AUDIO_SYSTEM_TYPE_AUDIO_SYSTEM_ID_MAX:Lcom/mstar/android/tvapi/common/vo/TvTypeInfo$EnumAudioSystemType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvTypeInfo$EnumAudioSystemType;->ordinal()I

    move-result v1

    if-le v0, v1, :cond_1

    :cond_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvTypeInfo$EnumAudioSystemType;->values()[Lcom/mstar/android/tvapi/common/vo/TvTypeInfo$EnumAudioSystemType;

    move-result-object v0

    iget v1, p0, Lcom/mstar/android/tvapi/common/vo/TvTypeInfo;->atvType:I

    aget-object v0, v0, v1

    :goto_0
    return-object v0

    :cond_1
    sget-object v0, Lcom/mstar/android/tvapi/common/vo/TvTypeInfo$EnumAudioSystemType;->E_AUDIO_SYSTEM_TYPE_AUDIO_SYSTEM_ID_MAX:Lcom/mstar/android/tvapi/common/vo/TvTypeInfo$EnumAudioSystemType;

    goto :goto_0
.end method

.method public getStbType()Lcom/mstar/android/tvapi/common/vo/TvTypeInfo$EnumStbSystemType;
    .locals 2

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/TvTypeInfo;->stbType:I

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvTypeInfo$EnumStbSystemType;->E_STB_SYSTEM_TYPE_STB_DISABLE:Lcom/mstar/android/tvapi/common/vo/TvTypeInfo$EnumStbSystemType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvTypeInfo$EnumStbSystemType;->ordinal()I

    move-result v1

    if-lt v0, v1, :cond_0

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/TvTypeInfo;->stbType:I

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvTypeInfo$EnumStbSystemType;->E_STB_SYSTEM_TYPE_STB_ENABLE:Lcom/mstar/android/tvapi/common/vo/TvTypeInfo$EnumStbSystemType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvTypeInfo$EnumStbSystemType;->ordinal()I

    move-result v1

    if-le v0, v1, :cond_1

    :cond_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvTypeInfo$EnumStbSystemType;->values()[Lcom/mstar/android/tvapi/common/vo/TvTypeInfo$EnumStbSystemType;

    move-result-object v0

    iget v1, p0, Lcom/mstar/android/tvapi/common/vo/TvTypeInfo;->atvType:I

    aget-object v0, v0, v1

    :goto_0
    return-object v0

    :cond_1
    sget-object v0, Lcom/mstar/android/tvapi/common/vo/TvTypeInfo$EnumStbSystemType;->E_STB_SYSTEM_TYPE_STB_DISABLE:Lcom/mstar/android/tvapi/common/vo/TvTypeInfo$EnumStbSystemType;

    goto :goto_0
.end method

.method public gettvtype()Lcom/mstar/android/tvapi/common/vo/TvTypeInfo$EnumTvProductType;
    .locals 2

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/TvTypeInfo;->tvType:I

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvTypeInfo$EnumTvProductType;->E_TV_PRODUCT_TYPE_ATV_Only:Lcom/mstar/android/tvapi/common/vo/TvTypeInfo$EnumTvProductType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvTypeInfo$EnumTvProductType;->ordinal()I

    move-result v1

    if-lt v0, v1, :cond_0

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/TvTypeInfo;->tvType:I

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvTypeInfo$EnumTvProductType;->E_TV_PRODUCT_TYPE_ID_MAX:Lcom/mstar/android/tvapi/common/vo/TvTypeInfo$EnumTvProductType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvTypeInfo$EnumTvProductType;->ordinal()I

    move-result v1

    if-le v0, v1, :cond_1

    :cond_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvTypeInfo$EnumTvProductType;->values()[Lcom/mstar/android/tvapi/common/vo/TvTypeInfo$EnumTvProductType;

    move-result-object v0

    iget v1, p0, Lcom/mstar/android/tvapi/common/vo/TvTypeInfo;->tvType:I

    aget-object v0, v0, v1

    :goto_0
    return-object v0

    :cond_1
    sget-object v0, Lcom/mstar/android/tvapi/common/vo/TvTypeInfo$EnumTvProductType;->E_TV_PRODUCT_TYPE_None:Lcom/mstar/android/tvapi/common/vo/TvTypeInfo$EnumTvProductType;

    goto :goto_0
.end method

.method public setAtvType(Lcom/mstar/android/tvapi/common/vo/TvTypeInfo$EnumAtvSystemType;)V
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/vo/TvTypeInfo$EnumAtvSystemType;

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/TvTypeInfo$EnumAtvSystemType;->ordinal()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/TvTypeInfo;->atvType:I

    return-void
.end method

.method public setAudioType(Lcom/mstar/android/tvapi/common/vo/TvTypeInfo$EnumAudioSystemType;)V
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/vo/TvTypeInfo$EnumAudioSystemType;

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/TvTypeInfo$EnumAudioSystemType;->ordinal()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/TvTypeInfo;->audioType:I

    return-void
.end method

.method public setStbType(Lcom/mstar/android/tvapi/common/vo/TvTypeInfo$EnumStbSystemType;)V
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/vo/TvTypeInfo$EnumStbSystemType;

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/TvTypeInfo$EnumStbSystemType;->ordinal()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/TvTypeInfo;->stbType:I

    return-void
.end method

.method public settvtype(Lcom/mstar/android/tvapi/common/vo/TvTypeInfo$EnumTvProductType;)V
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/vo/TvTypeInfo$EnumTvProductType;

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/TvTypeInfo$EnumTvProductType;->ordinal()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/TvTypeInfo;->tvType:I

    return-void
.end method
