.class public final enum Lcom/mstar/android/tvapi/common/vo/EnumDatabaseTypeIndex;
.super Ljava/lang/Enum;
.source "EnumDatabaseTypeIndex.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/tvapi/common/vo/EnumDatabaseTypeIndex;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumDatabaseTypeIndex;

.field public static final enum E_DB_3DVIDEOROUTERINDEX:Lcom/mstar/android/tvapi/common/vo/EnumDatabaseTypeIndex;

.field public static final enum E_DB_FACTORYCOLORTEMPEXINDEX:Lcom/mstar/android/tvapi/common/vo/EnumDatabaseTypeIndex;

.field public static final enum E_DB_INDEX_MAX:Lcom/mstar/android/tvapi/common/vo/EnumDatabaseTypeIndex;

.field public static final enum E_DB_NONLINEARADJUSTINDEX:Lcom/mstar/android/tvapi/common/vo/EnumDatabaseTypeIndex;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumDatabaseTypeIndex;

    const-string v1, "E_DB_3DVIDEOROUTERINDEX"

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumDatabaseTypeIndex;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumDatabaseTypeIndex;->E_DB_3DVIDEOROUTERINDEX:Lcom/mstar/android/tvapi/common/vo/EnumDatabaseTypeIndex;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumDatabaseTypeIndex;

    const-string v1, "E_DB_FACTORYCOLORTEMPEXINDEX"

    invoke-direct {v0, v1, v3}, Lcom/mstar/android/tvapi/common/vo/EnumDatabaseTypeIndex;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumDatabaseTypeIndex;->E_DB_FACTORYCOLORTEMPEXINDEX:Lcom/mstar/android/tvapi/common/vo/EnumDatabaseTypeIndex;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumDatabaseTypeIndex;

    const-string v1, "E_DB_NONLINEARADJUSTINDEX"

    invoke-direct {v0, v1, v4}, Lcom/mstar/android/tvapi/common/vo/EnumDatabaseTypeIndex;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumDatabaseTypeIndex;->E_DB_NONLINEARADJUSTINDEX:Lcom/mstar/android/tvapi/common/vo/EnumDatabaseTypeIndex;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumDatabaseTypeIndex;

    const-string v1, "E_DB_INDEX_MAX"

    invoke-direct {v0, v1, v5}, Lcom/mstar/android/tvapi/common/vo/EnumDatabaseTypeIndex;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumDatabaseTypeIndex;->E_DB_INDEX_MAX:Lcom/mstar/android/tvapi/common/vo/EnumDatabaseTypeIndex;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/mstar/android/tvapi/common/vo/EnumDatabaseTypeIndex;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumDatabaseTypeIndex;->E_DB_3DVIDEOROUTERINDEX:Lcom/mstar/android/tvapi/common/vo/EnumDatabaseTypeIndex;

    aput-object v1, v0, v2

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumDatabaseTypeIndex;->E_DB_FACTORYCOLORTEMPEXINDEX:Lcom/mstar/android/tvapi/common/vo/EnumDatabaseTypeIndex;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumDatabaseTypeIndex;->E_DB_NONLINEARADJUSTINDEX:Lcom/mstar/android/tvapi/common/vo/EnumDatabaseTypeIndex;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumDatabaseTypeIndex;->E_DB_INDEX_MAX:Lcom/mstar/android/tvapi/common/vo/EnumDatabaseTypeIndex;

    aput-object v1, v0, v5

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumDatabaseTypeIndex;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumDatabaseTypeIndex;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/tvapi/common/vo/EnumDatabaseTypeIndex;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/tvapi/common/vo/EnumDatabaseTypeIndex;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/common/vo/EnumDatabaseTypeIndex;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/tvapi/common/vo/EnumDatabaseTypeIndex;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/EnumDatabaseTypeIndex;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumDatabaseTypeIndex;

    invoke-virtual {v0}, [Lcom/mstar/android/tvapi/common/vo/EnumDatabaseTypeIndex;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/tvapi/common/vo/EnumDatabaseTypeIndex;

    return-object v0
.end method
