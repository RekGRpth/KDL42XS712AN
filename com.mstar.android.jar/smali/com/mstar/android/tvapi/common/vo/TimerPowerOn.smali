.class public Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;
.super Landroid/text/format/Time;
.source "TimerPowerOn.java"


# instance fields
.field public channelNumber:I

.field private enBootMode:I

.field private enTimerPeriod:I

.field private enTvSource:I

.field public volume:S


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/text/format/Time;-><init>()V

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;->enTimerPeriod:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;->enTvSource:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;->channelNumber:I

    iput-short v0, p0, Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;->volume:S

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;->enBootMode:I

    return-void
.end method


# virtual methods
.method public getBootMode()Lcom/mstar/android/tvapi/common/vo/EnumTimerBootType;
    .locals 2

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumTimerBootType;->values()[Lcom/mstar/android/tvapi/common/vo/EnumTimerBootType;

    move-result-object v0

    iget v1, p0, Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;->enBootMode:I

    aget-object v0, v0, v1

    return-object v0
.end method

.method public getTimerPeriod()Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;
    .locals 2

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;->values()[Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;

    move-result-object v0

    iget v1, p0, Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;->enTimerPeriod:I

    aget-object v0, v0, v1

    return-object v0
.end method

.method public getTvSource()Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;
    .locals 2

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;->values()[Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;

    move-result-object v0

    iget v1, p0, Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;->enTvSource:I

    aget-object v0, v0, v1

    return-object v0
.end method

.method public setBootMode(Lcom/mstar/android/tvapi/common/vo/EnumTimerBootType;)V
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumTimerBootType;

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumTimerBootType;->ordinal()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;->enBootMode:I

    return-void
.end method

.method public setTimerPeriod(Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;)V
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;->ordinal()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;->enTimerPeriod:I

    return-void
.end method

.method public setTvSource(Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;)V
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;->ordinal()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;->enTvSource:I

    return-void
.end method
