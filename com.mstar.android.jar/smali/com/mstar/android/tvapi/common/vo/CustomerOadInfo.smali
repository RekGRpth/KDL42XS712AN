.class public Lcom/mstar/android/tvapi/common/vo/CustomerOadInfo;
.super Ljava/lang/Object;
.source "CustomerOadInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mstar/android/tvapi/common/vo/CustomerOadInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public hwModel:I

.field public hwVersion:I

.field public oui:I

.field public swApModel:I

.field public swApVersion:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/CustomerOadInfo$1;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/common/vo/CustomerOadInfo$1;-><init>()V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/CustomerOadInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/CustomerOadInfo;->oui:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/CustomerOadInfo;->hwModel:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/CustomerOadInfo;->hwVersion:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/CustomerOadInfo;->swApModel:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/CustomerOadInfo;->swApVersion:I

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1    # Landroid/os/Parcel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/CustomerOadInfo;->oui:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/CustomerOadInfo;->hwModel:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/CustomerOadInfo;->hwVersion:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/CustomerOadInfo;->swApModel:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/CustomerOadInfo;->swApVersion:I

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/CustomerOadInfo;->oui:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/CustomerOadInfo;->hwModel:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/CustomerOadInfo;->hwVersion:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/CustomerOadInfo;->swApModel:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/CustomerOadInfo;->swApVersion:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
