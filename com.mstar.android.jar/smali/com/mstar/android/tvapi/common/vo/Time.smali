.class public Lcom/mstar/android/tvapi/common/vo/Time;
.super Ljava/lang/Object;
.source "Time.java"


# static fields
.field public static final E_TIMER_BOOT_ON_TIMER:I = 0x0

.field public static final E_TIMER_BOOT_RECORDER:I = 0x2

.field public static final E_TIMER_BOOT_REMINDER:I = 0x1


# instance fields
.field public autoSleepFlag:Z

.field public clockMode:Z

.field public eTimeZoneInfo:S

.field public enOnTimeState:S

.field public is12Hour:Z

.field public isAutoSync:Z

.field public isDaylightSaving:Z

.field public offTimeFlag:I

.field public offTimeState:S

.field public offTimerInfoHour:S

.field public offTimerInfoMin:S

.field public offsetTime:I

.field public onTimeFlag:S

.field public onTimeTvSrc:S

.field public onTimerChannel:I

.field public onTimerInfoHour:S

.field public onTimerInfoMin:S

.field public onTimerVolume:S

.field public timeDataCS:I

.field public timerBootMode:I


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/Time;->timeDataCS:I

    iput-short v0, p0, Lcom/mstar/android/tvapi/common/vo/Time;->onTimeFlag:S

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/Time;->offTimeFlag:I

    iput-short v0, p0, Lcom/mstar/android/tvapi/common/vo/Time;->offTimeState:S

    iput-short v0, p0, Lcom/mstar/android/tvapi/common/vo/Time;->offTimerInfoHour:S

    iput-short v0, p0, Lcom/mstar/android/tvapi/common/vo/Time;->offTimerInfoMin:S

    iput-short v0, p0, Lcom/mstar/android/tvapi/common/vo/Time;->enOnTimeState:S

    iput-short v0, p0, Lcom/mstar/android/tvapi/common/vo/Time;->onTimerInfoHour:S

    iput-short v0, p0, Lcom/mstar/android/tvapi/common/vo/Time;->onTimerInfoMin:S

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/Time;->onTimerChannel:I

    iput-short v0, p0, Lcom/mstar/android/tvapi/common/vo/Time;->onTimeTvSrc:S

    iput-short v0, p0, Lcom/mstar/android/tvapi/common/vo/Time;->onTimerVolume:S

    iput-short v0, p0, Lcom/mstar/android/tvapi/common/vo/Time;->eTimeZoneInfo:S

    iput-boolean v0, p0, Lcom/mstar/android/tvapi/common/vo/Time;->is12Hour:Z

    iput-boolean v0, p0, Lcom/mstar/android/tvapi/common/vo/Time;->isAutoSync:Z

    iput-boolean v0, p0, Lcom/mstar/android/tvapi/common/vo/Time;->clockMode:Z

    iput-boolean v0, p0, Lcom/mstar/android/tvapi/common/vo/Time;->autoSleepFlag:Z

    iput-boolean v0, p0, Lcom/mstar/android/tvapi/common/vo/Time;->isDaylightSaving:Z

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/Time;->offsetTime:I

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/Time;->timerBootMode:I

    return-void
.end method
