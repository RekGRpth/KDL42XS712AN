.class public final enum Lcom/mstar/android/tvapi/common/vo/EnumOffTimerMode;
.super Ljava/lang/Enum;
.source "EnumOffTimerMode.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/tvapi/common/vo/EnumOffTimerMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumOffTimerMode;

.field public static final enum OFF_MODE_AUTOSLEEP:Lcom/mstar/android/tvapi/common/vo/EnumOffTimerMode;

.field public static final enum OFF_MODE_NUM:Lcom/mstar/android/tvapi/common/vo/EnumOffTimerMode;

.field public static final enum OFF_MODE_SLEEP:Lcom/mstar/android/tvapi/common/vo/EnumOffTimerMode;

.field public static final enum OFF_MODE_TIMER:Lcom/mstar/android/tvapi/common/vo/EnumOffTimerMode;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumOffTimerMode;

    const-string v1, "OFF_MODE_TIMER"

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumOffTimerMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumOffTimerMode;->OFF_MODE_TIMER:Lcom/mstar/android/tvapi/common/vo/EnumOffTimerMode;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumOffTimerMode;

    const-string v1, "OFF_MODE_SLEEP"

    invoke-direct {v0, v1, v3}, Lcom/mstar/android/tvapi/common/vo/EnumOffTimerMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumOffTimerMode;->OFF_MODE_SLEEP:Lcom/mstar/android/tvapi/common/vo/EnumOffTimerMode;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumOffTimerMode;

    const-string v1, "OFF_MODE_AUTOSLEEP"

    invoke-direct {v0, v1, v4}, Lcom/mstar/android/tvapi/common/vo/EnumOffTimerMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumOffTimerMode;->OFF_MODE_AUTOSLEEP:Lcom/mstar/android/tvapi/common/vo/EnumOffTimerMode;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumOffTimerMode;

    const-string v1, "OFF_MODE_NUM"

    invoke-direct {v0, v1, v5}, Lcom/mstar/android/tvapi/common/vo/EnumOffTimerMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumOffTimerMode;->OFF_MODE_NUM:Lcom/mstar/android/tvapi/common/vo/EnumOffTimerMode;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/mstar/android/tvapi/common/vo/EnumOffTimerMode;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumOffTimerMode;->OFF_MODE_TIMER:Lcom/mstar/android/tvapi/common/vo/EnumOffTimerMode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumOffTimerMode;->OFF_MODE_SLEEP:Lcom/mstar/android/tvapi/common/vo/EnumOffTimerMode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumOffTimerMode;->OFF_MODE_AUTOSLEEP:Lcom/mstar/android/tvapi/common/vo/EnumOffTimerMode;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumOffTimerMode;->OFF_MODE_NUM:Lcom/mstar/android/tvapi/common/vo/EnumOffTimerMode;

    aput-object v1, v0, v5

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumOffTimerMode;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumOffTimerMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/tvapi/common/vo/EnumOffTimerMode;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/tvapi/common/vo/EnumOffTimerMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/common/vo/EnumOffTimerMode;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/tvapi/common/vo/EnumOffTimerMode;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/EnumOffTimerMode;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumOffTimerMode;

    invoke-virtual {v0}, [Lcom/mstar/android/tvapi/common/vo/EnumOffTimerMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/tvapi/common/vo/EnumOffTimerMode;

    return-object v0
.end method
