.class public final enum Lcom/mstar/android/tvapi/common/vo/Enum3dDisplayFormat;
.super Ljava/lang/Enum;
.source "Enum3dDisplayFormat.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/tvapi/common/vo/Enum3dDisplayFormat;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/tvapi/common/vo/Enum3dDisplayFormat;

.field public static final enum E_CKB:Lcom/mstar/android/tvapi/common/vo/Enum3dDisplayFormat;

.field public static final enum E_FIDA:Lcom/mstar/android/tvapi/common/vo/Enum3dDisplayFormat;

.field public static final enum E_FP:Lcom/mstar/android/tvapi/common/vo/Enum3dDisplayFormat;

.field public static final enum E_FRAME:Lcom/mstar/android/tvapi/common/vo/Enum3dDisplayFormat;

.field public static final enum E_LAP:Lcom/mstar/android/tvapi/common/vo/Enum3dDisplayFormat;

.field public static final enum E_NUM:Lcom/mstar/android/tvapi/common/vo/Enum3dDisplayFormat;

.field public static final enum E_SBS:Lcom/mstar/android/tvapi/common/vo/Enum3dDisplayFormat;

.field public static final enum E_TAB:Lcom/mstar/android/tvapi/common/vo/Enum3dDisplayFormat;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/Enum3dDisplayFormat;

    const-string v1, "E_SBS"

    invoke-direct {v0, v1, v3}, Lcom/mstar/android/tvapi/common/vo/Enum3dDisplayFormat;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/Enum3dDisplayFormat;->E_SBS:Lcom/mstar/android/tvapi/common/vo/Enum3dDisplayFormat;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/Enum3dDisplayFormat;

    const-string v1, "E_TAB"

    invoke-direct {v0, v1, v4}, Lcom/mstar/android/tvapi/common/vo/Enum3dDisplayFormat;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/Enum3dDisplayFormat;->E_TAB:Lcom/mstar/android/tvapi/common/vo/Enum3dDisplayFormat;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/Enum3dDisplayFormat;

    const-string v1, "E_FP"

    invoke-direct {v0, v1, v5}, Lcom/mstar/android/tvapi/common/vo/Enum3dDisplayFormat;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/Enum3dDisplayFormat;->E_FP:Lcom/mstar/android/tvapi/common/vo/Enum3dDisplayFormat;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/Enum3dDisplayFormat;

    const-string v1, "E_FRAME"

    invoke-direct {v0, v1, v6}, Lcom/mstar/android/tvapi/common/vo/Enum3dDisplayFormat;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/Enum3dDisplayFormat;->E_FRAME:Lcom/mstar/android/tvapi/common/vo/Enum3dDisplayFormat;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/Enum3dDisplayFormat;

    const-string v1, "E_LAP"

    invoke-direct {v0, v1, v7}, Lcom/mstar/android/tvapi/common/vo/Enum3dDisplayFormat;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/Enum3dDisplayFormat;->E_LAP:Lcom/mstar/android/tvapi/common/vo/Enum3dDisplayFormat;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/Enum3dDisplayFormat;

    const-string v1, "E_FIDA"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/Enum3dDisplayFormat;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/Enum3dDisplayFormat;->E_FIDA:Lcom/mstar/android/tvapi/common/vo/Enum3dDisplayFormat;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/Enum3dDisplayFormat;

    const-string v1, "E_CKB"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/Enum3dDisplayFormat;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/Enum3dDisplayFormat;->E_CKB:Lcom/mstar/android/tvapi/common/vo/Enum3dDisplayFormat;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/Enum3dDisplayFormat;

    const-string v1, "E_NUM"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/Enum3dDisplayFormat;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/Enum3dDisplayFormat;->E_NUM:Lcom/mstar/android/tvapi/common/vo/Enum3dDisplayFormat;

    const/16 v0, 0x8

    new-array v0, v0, [Lcom/mstar/android/tvapi/common/vo/Enum3dDisplayFormat;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/Enum3dDisplayFormat;->E_SBS:Lcom/mstar/android/tvapi/common/vo/Enum3dDisplayFormat;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/Enum3dDisplayFormat;->E_TAB:Lcom/mstar/android/tvapi/common/vo/Enum3dDisplayFormat;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/Enum3dDisplayFormat;->E_FP:Lcom/mstar/android/tvapi/common/vo/Enum3dDisplayFormat;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/Enum3dDisplayFormat;->E_FRAME:Lcom/mstar/android/tvapi/common/vo/Enum3dDisplayFormat;

    aput-object v1, v0, v6

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/Enum3dDisplayFormat;->E_LAP:Lcom/mstar/android/tvapi/common/vo/Enum3dDisplayFormat;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/Enum3dDisplayFormat;->E_FIDA:Lcom/mstar/android/tvapi/common/vo/Enum3dDisplayFormat;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/Enum3dDisplayFormat;->E_CKB:Lcom/mstar/android/tvapi/common/vo/Enum3dDisplayFormat;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/Enum3dDisplayFormat;->E_NUM:Lcom/mstar/android/tvapi/common/vo/Enum3dDisplayFormat;

    aput-object v2, v0, v1

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/Enum3dDisplayFormat;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/Enum3dDisplayFormat;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/tvapi/common/vo/Enum3dDisplayFormat;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/tvapi/common/vo/Enum3dDisplayFormat;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/common/vo/Enum3dDisplayFormat;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/tvapi/common/vo/Enum3dDisplayFormat;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/Enum3dDisplayFormat;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/Enum3dDisplayFormat;

    invoke-virtual {v0}, [Lcom/mstar/android/tvapi/common/vo/Enum3dDisplayFormat;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/tvapi/common/vo/Enum3dDisplayFormat;

    return-object v0
.end method
