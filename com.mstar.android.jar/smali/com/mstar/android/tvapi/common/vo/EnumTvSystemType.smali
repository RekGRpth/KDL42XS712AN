.class public final enum Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;
.super Ljava/lang/Enum;
.source "EnumTvSystemType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;

.field public static final enum E_ATSC:Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;

.field public static final enum E_ATV:Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;

.field public static final enum E_CVBS:Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;

.field public static final enum E_DVBC:Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;

.field public static final enum E_DVBS:Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;

.field public static final enum E_DVBT:Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;

.field public static final enum E_DVI:Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;

.field public static final enum E_HDMI:Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;

.field public static final enum E_ISDB:Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;

.field public static final enum E_JPEG:Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;

.field public static final enum E_KTV:Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;

.field public static final enum E_SCART:Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;

.field public static final enum E_STORAGE:Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;

.field public static final enum E_SVIDEO:Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;

.field public static final enum E_VGA:Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;

.field public static final enum E_YPBPR:Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;

    const-string v1, "E_ATV"

    invoke-direct {v0, v1, v3}, Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;->E_ATV:Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;

    const-string v1, "E_ATSC"

    invoke-direct {v0, v1, v4}, Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;->E_ATSC:Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;

    const-string v1, "E_DVBC"

    invoke-direct {v0, v1, v5}, Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;->E_DVBC:Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;

    const-string v1, "E_DVBS"

    invoke-direct {v0, v1, v6}, Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;->E_DVBS:Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;

    const-string v1, "E_DVBT"

    invoke-direct {v0, v1, v7}, Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;->E_DVBT:Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;

    const-string v1, "E_ISDB"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;->E_ISDB:Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;

    const-string v1, "E_VGA"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;->E_VGA:Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;

    const-string v1, "E_CVBS"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;->E_CVBS:Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;

    const-string v1, "E_SCART"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;->E_SCART:Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;

    const-string v1, "E_YPBPR"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;->E_YPBPR:Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;

    const-string v1, "E_SVIDEO"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;->E_SVIDEO:Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;

    const-string v1, "E_HDMI"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;->E_HDMI:Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;

    const-string v1, "E_DVI"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;->E_DVI:Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;

    const-string v1, "E_KTV"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;->E_KTV:Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;

    const-string v1, "E_JPEG"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;->E_JPEG:Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;

    const-string v1, "E_STORAGE"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;->E_STORAGE:Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;

    const/16 v0, 0x10

    new-array v0, v0, [Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;->E_ATV:Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;->E_ATSC:Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;->E_DVBC:Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;->E_DVBS:Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;->E_DVBT:Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;->E_ISDB:Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;->E_VGA:Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;->E_CVBS:Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;->E_SCART:Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;->E_YPBPR:Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;->E_SVIDEO:Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;->E_HDMI:Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;->E_DVI:Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;->E_KTV:Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;->E_JPEG:Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;->E_STORAGE:Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;

    invoke-virtual {v0}, [Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/tvapi/common/vo/EnumTvSystemType;

    return-object v0
.end method
