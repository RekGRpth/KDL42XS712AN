.class public Lcom/mstar/android/tvapi/common/vo/OnTimeTvDescriptor;
.super Ljava/lang/Object;
.source "OnTimeTvDescriptor.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mstar/android/tvapi/common/vo/OnTimeTvDescriptor;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public enTVSrc:Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;

.field public mChNo:I

.field public mVol:S


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/OnTimeTvDescriptor$1;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/common/vo/OnTimeTvDescriptor$1;-><init>()V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/OnTimeTvDescriptor;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .param p1    # Landroid/os/Parcel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;->values()[Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/mstar/android/tvapi/common/vo/OnTimeTvDescriptor;->enTVSrc:Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/common/vo/OnTimeTvDescriptor;->mChNo:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/common/vo/OnTimeTvDescriptor;->mVol:S

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/mstar/android/tvapi/common/vo/OnTimeTvDescriptor$1;)V
    .locals 0
    .param p1    # Landroid/os/Parcel;
    .param p2    # Lcom/mstar/android/tvapi/common/vo/OnTimeTvDescriptor$1;

    invoke-direct {p0, p1}, Lcom/mstar/android/tvapi/common/vo/OnTimeTvDescriptor;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;IS)V
    .locals 0
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;
    .param p2    # I
    .param p3    # S

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/mstar/android/tvapi/common/vo/OnTimeTvDescriptor;->enTVSrc:Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;

    iput p2, p0, Lcom/mstar/android/tvapi/common/vo/OnTimeTvDescriptor;->mChNo:I

    iput-short p3, p0, Lcom/mstar/android/tvapi/common/vo/OnTimeTvDescriptor;->mVol:S

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget-object v0, p0, Lcom/mstar/android/tvapi/common/vo/OnTimeTvDescriptor;->enTVSrc:Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/OnTimeTvDescriptor;->mChNo:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/common/vo/OnTimeTvDescriptor;->mVol:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
