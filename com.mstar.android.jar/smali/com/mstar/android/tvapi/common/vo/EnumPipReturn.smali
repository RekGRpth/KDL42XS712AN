.class public final enum Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;
.super Ljava/lang/Enum;
.source "EnumPipReturn.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;

.field public static final enum E_PIP_NOT_SUPPORT:Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;

.field public static final enum E_PIP_PIP_MODE_OPENED:Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;

.field public static final enum E_PIP_POP_MODE_NUM:Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;

.field public static final enum E_PIP_POP_MODE_OPENED:Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;

.field public static final enum E_PIP_SUCCESS:Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;

.field public static final enum E_PIP_WINDOW_SETTING_ERROR:Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;

    const-string v1, "E_PIP_NOT_SUPPORT"

    invoke-direct {v0, v1, v3}, Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;->E_PIP_NOT_SUPPORT:Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;

    const-string v1, "E_PIP_WINDOW_SETTING_ERROR"

    invoke-direct {v0, v1, v4}, Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;->E_PIP_WINDOW_SETTING_ERROR:Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;

    const-string v1, "E_PIP_SUCCESS"

    invoke-direct {v0, v1, v5}, Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;->E_PIP_SUCCESS:Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;

    const-string v1, "E_PIP_PIP_MODE_OPENED"

    invoke-direct {v0, v1, v6}, Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;->E_PIP_PIP_MODE_OPENED:Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;

    const-string v1, "E_PIP_POP_MODE_OPENED"

    invoke-direct {v0, v1, v7}, Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;->E_PIP_POP_MODE_OPENED:Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;

    const-string v1, "E_PIP_POP_MODE_NUM"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;->E_PIP_POP_MODE_NUM:Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;->E_PIP_NOT_SUPPORT:Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;->E_PIP_WINDOW_SETTING_ERROR:Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;->E_PIP_SUCCESS:Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;->E_PIP_PIP_MODE_OPENED:Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;

    aput-object v1, v0, v6

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;->E_PIP_POP_MODE_OPENED:Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;->E_PIP_POP_MODE_NUM:Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;

    aput-object v2, v0, v1

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;

    invoke-virtual {v0}, [Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;

    return-object v0
.end method
