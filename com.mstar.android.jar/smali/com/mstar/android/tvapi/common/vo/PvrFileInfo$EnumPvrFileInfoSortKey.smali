.class public final enum Lcom/mstar/android/tvapi/common/vo/PvrFileInfo$EnumPvrFileInfoSortKey;
.super Ljava/lang/Enum;
.source "PvrFileInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mstar/android/tvapi/common/vo/PvrFileInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EnumPvrFileInfoSortKey"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/tvapi/common/vo/PvrFileInfo$EnumPvrFileInfoSortKey;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/tvapi/common/vo/PvrFileInfo$EnumPvrFileInfoSortKey;

.field public static final enum E_SORT_CHANNEL:Lcom/mstar/android/tvapi/common/vo/PvrFileInfo$EnumPvrFileInfoSortKey;

.field public static final enum E_SORT_FILENAME:Lcom/mstar/android/tvapi/common/vo/PvrFileInfo$EnumPvrFileInfoSortKey;

.field public static final enum E_SORT_LCN:Lcom/mstar/android/tvapi/common/vo/PvrFileInfo$EnumPvrFileInfoSortKey;

.field public static final enum E_SORT_MAX_KEY:Lcom/mstar/android/tvapi/common/vo/PvrFileInfo$EnumPvrFileInfoSortKey;

.field public static final enum E_SORT_PROGRAM:Lcom/mstar/android/tvapi/common/vo/PvrFileInfo$EnumPvrFileInfoSortKey;

.field public static final enum E_SORT_TIME:Lcom/mstar/android/tvapi/common/vo/PvrFileInfo$EnumPvrFileInfoSortKey;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/PvrFileInfo$EnumPvrFileInfoSortKey;

    const-string v1, "E_SORT_FILENAME"

    invoke-direct {v0, v1, v3}, Lcom/mstar/android/tvapi/common/vo/PvrFileInfo$EnumPvrFileInfoSortKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/PvrFileInfo$EnumPvrFileInfoSortKey;->E_SORT_FILENAME:Lcom/mstar/android/tvapi/common/vo/PvrFileInfo$EnumPvrFileInfoSortKey;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/PvrFileInfo$EnumPvrFileInfoSortKey;

    const-string v1, "E_SORT_TIME"

    invoke-direct {v0, v1, v4}, Lcom/mstar/android/tvapi/common/vo/PvrFileInfo$EnumPvrFileInfoSortKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/PvrFileInfo$EnumPvrFileInfoSortKey;->E_SORT_TIME:Lcom/mstar/android/tvapi/common/vo/PvrFileInfo$EnumPvrFileInfoSortKey;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/PvrFileInfo$EnumPvrFileInfoSortKey;

    const-string v1, "E_SORT_LCN"

    invoke-direct {v0, v1, v5}, Lcom/mstar/android/tvapi/common/vo/PvrFileInfo$EnumPvrFileInfoSortKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/PvrFileInfo$EnumPvrFileInfoSortKey;->E_SORT_LCN:Lcom/mstar/android/tvapi/common/vo/PvrFileInfo$EnumPvrFileInfoSortKey;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/PvrFileInfo$EnumPvrFileInfoSortKey;

    const-string v1, "E_SORT_CHANNEL"

    invoke-direct {v0, v1, v6}, Lcom/mstar/android/tvapi/common/vo/PvrFileInfo$EnumPvrFileInfoSortKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/PvrFileInfo$EnumPvrFileInfoSortKey;->E_SORT_CHANNEL:Lcom/mstar/android/tvapi/common/vo/PvrFileInfo$EnumPvrFileInfoSortKey;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/PvrFileInfo$EnumPvrFileInfoSortKey;

    const-string v1, "E_SORT_PROGRAM"

    invoke-direct {v0, v1, v7}, Lcom/mstar/android/tvapi/common/vo/PvrFileInfo$EnumPvrFileInfoSortKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/PvrFileInfo$EnumPvrFileInfoSortKey;->E_SORT_PROGRAM:Lcom/mstar/android/tvapi/common/vo/PvrFileInfo$EnumPvrFileInfoSortKey;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/PvrFileInfo$EnumPvrFileInfoSortKey;

    const-string v1, "E_SORT_MAX_KEY"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/common/vo/PvrFileInfo$EnumPvrFileInfoSortKey;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/PvrFileInfo$EnumPvrFileInfoSortKey;->E_SORT_MAX_KEY:Lcom/mstar/android/tvapi/common/vo/PvrFileInfo$EnumPvrFileInfoSortKey;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/mstar/android/tvapi/common/vo/PvrFileInfo$EnumPvrFileInfoSortKey;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/PvrFileInfo$EnumPvrFileInfoSortKey;->E_SORT_FILENAME:Lcom/mstar/android/tvapi/common/vo/PvrFileInfo$EnumPvrFileInfoSortKey;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/PvrFileInfo$EnumPvrFileInfoSortKey;->E_SORT_TIME:Lcom/mstar/android/tvapi/common/vo/PvrFileInfo$EnumPvrFileInfoSortKey;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/PvrFileInfo$EnumPvrFileInfoSortKey;->E_SORT_LCN:Lcom/mstar/android/tvapi/common/vo/PvrFileInfo$EnumPvrFileInfoSortKey;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/PvrFileInfo$EnumPvrFileInfoSortKey;->E_SORT_CHANNEL:Lcom/mstar/android/tvapi/common/vo/PvrFileInfo$EnumPvrFileInfoSortKey;

    aput-object v1, v0, v6

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/PvrFileInfo$EnumPvrFileInfoSortKey;->E_SORT_PROGRAM:Lcom/mstar/android/tvapi/common/vo/PvrFileInfo$EnumPvrFileInfoSortKey;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/PvrFileInfo$EnumPvrFileInfoSortKey;->E_SORT_MAX_KEY:Lcom/mstar/android/tvapi/common/vo/PvrFileInfo$EnumPvrFileInfoSortKey;

    aput-object v2, v0, v1

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/PvrFileInfo$EnumPvrFileInfoSortKey;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/PvrFileInfo$EnumPvrFileInfoSortKey;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/tvapi/common/vo/PvrFileInfo$EnumPvrFileInfoSortKey;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/tvapi/common/vo/PvrFileInfo$EnumPvrFileInfoSortKey;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/common/vo/PvrFileInfo$EnumPvrFileInfoSortKey;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/tvapi/common/vo/PvrFileInfo$EnumPvrFileInfoSortKey;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/PvrFileInfo$EnumPvrFileInfoSortKey;->$VALUES:[Lcom/mstar/android/tvapi/common/vo/PvrFileInfo$EnumPvrFileInfoSortKey;

    invoke-virtual {v0}, [Lcom/mstar/android/tvapi/common/vo/PvrFileInfo$EnumPvrFileInfoSortKey;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/tvapi/common/vo/PvrFileInfo$EnumPvrFileInfoSortKey;

    return-object v0
.end method
