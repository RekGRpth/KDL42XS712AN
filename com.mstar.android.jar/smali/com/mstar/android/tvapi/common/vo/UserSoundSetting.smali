.class public Lcom/mstar/android/tvapi/common/vo/UserSoundSetting;
.super Ljava/lang/Object;
.source "UserSoundSetting.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mstar/android/tvapi/common/vo/UserSoundSetting;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public adOutput:Lcom/mstar/android/tvapi/common/vo/EnumSoundAdOutput;

.field public adVolume:S

.field public astSoundModeSettings:[Lcom/mstar/android/tvapi/common/vo/SoundModeSetting;

.field public audysseyDynamicVolume:Lcom/mstar/android/tvapi/common/vo/EnumAudysseyDynamicVolumeMode;

.field public audysseyEq:Lcom/mstar/android/tvapi/common/vo/EnumAudysseyEqMode;

.field public balance:S

.field public checkSum:I

.field public enableAVC:Z

.field public enableAd:Z

.field public enableHi:Z

.field public headPhonePreScale:S

.field public headphoneVolume:S

.field public lineOutPreScale:S

.field public muteFlag:S

.field public primaryFlag:S

.field public scart1PreScale:S

.field public scart2PreScale:S

.field public soundAudioChannel:Lcom/mstar/android/tvapi/common/vo/EnumAudioMode;

.field public soundAudioLanguage1:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

.field public soundAudioLanguage2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

.field public soundMode:Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;

.field public spdifDelay:S

.field public speakerPreScale:S

.field public speakerdelay:S

.field public surround:Lcom/mstar/android/tvapi/common/vo/EnumSurroundType;

.field public surroundSoundMode:Lcom/mstar/android/tvapi/common/vo/EnumSurroundSystemType;

.field public volume:S


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/UserSoundSetting$1;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/common/vo/UserSoundSetting$1;-><init>()V

    sput-object v0, Lcom/mstar/android/tvapi/common/vo/UserSoundSetting;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAudioMode;->E_NUM:Lcom/mstar/android/tvapi/common/vo/EnumAudioMode;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAudioMode;->ordinal()I

    move-result v1

    new-array v1, v1, [Lcom/mstar/android/tvapi/common/vo/SoundModeSetting;

    iput-object v1, p0, Lcom/mstar/android/tvapi/common/vo/UserSoundSetting;->astSoundModeSettings:[Lcom/mstar/android/tvapi/common/vo/SoundModeSetting;

    iput v2, p0, Lcom/mstar/android/tvapi/common/vo/UserSoundSetting;->checkSum:I

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;->E_MOVIE:Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;

    iput-object v1, p0, Lcom/mstar/android/tvapi/common/vo/UserSoundSetting;->soundMode:Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAudysseyDynamicVolumeMode;->E_VOLUME_NUM:Lcom/mstar/android/tvapi/common/vo/EnumAudysseyDynamicVolumeMode;

    iput-object v1, p0, Lcom/mstar/android/tvapi/common/vo/UserSoundSetting;->audysseyDynamicVolume:Lcom/mstar/android/tvapi/common/vo/EnumAudysseyDynamicVolumeMode;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAudysseyEqMode;->E_NUM:Lcom/mstar/android/tvapi/common/vo/EnumAudysseyEqMode;

    iput-object v1, p0, Lcom/mstar/android/tvapi/common/vo/UserSoundSetting;->audysseyEq:Lcom/mstar/android/tvapi/common/vo/EnumAudysseyEqMode;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumSurroundSystemType;->E_BBE:Lcom/mstar/android/tvapi/common/vo/EnumSurroundSystemType;

    iput-object v1, p0, Lcom/mstar/android/tvapi/common/vo/UserSoundSetting;->surroundSoundMode:Lcom/mstar/android/tvapi/common/vo/EnumSurroundSystemType;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumSurroundType;->E_CHAMPAIGN:Lcom/mstar/android/tvapi/common/vo/EnumSurroundType;

    iput-object v1, p0, Lcom/mstar/android/tvapi/common/vo/UserSoundSetting;->surround:Lcom/mstar/android/tvapi/common/vo/EnumSurroundType;

    iput-boolean v2, p0, Lcom/mstar/android/tvapi/common/vo/UserSoundSetting;->enableAVC:Z

    iput-short v2, p0, Lcom/mstar/android/tvapi/common/vo/UserSoundSetting;->volume:S

    iput-short v2, p0, Lcom/mstar/android/tvapi/common/vo/UserSoundSetting;->headphoneVolume:S

    iput-short v2, p0, Lcom/mstar/android/tvapi/common/vo/UserSoundSetting;->balance:S

    iput-short v2, p0, Lcom/mstar/android/tvapi/common/vo/UserSoundSetting;->primaryFlag:S

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ENGLISH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    iput-object v1, p0, Lcom/mstar/android/tvapi/common/vo/UserSoundSetting;->soundAudioLanguage1:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ACHINESE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    iput-object v1, p0, Lcom/mstar/android/tvapi/common/vo/UserSoundSetting;->soundAudioLanguage2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    iput-short v2, p0, Lcom/mstar/android/tvapi/common/vo/UserSoundSetting;->muteFlag:S

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAudioMode;->E_LL:Lcom/mstar/android/tvapi/common/vo/EnumAudioMode;

    iput-object v1, p0, Lcom/mstar/android/tvapi/common/vo/UserSoundSetting;->soundAudioChannel:Lcom/mstar/android/tvapi/common/vo/EnumAudioMode;

    iput-boolean v2, p0, Lcom/mstar/android/tvapi/common/vo/UserSoundSetting;->enableAd:Z

    iput-short v2, p0, Lcom/mstar/android/tvapi/common/vo/UserSoundSetting;->adVolume:S

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumSoundAdOutput;->E_BOTH:Lcom/mstar/android/tvapi/common/vo/EnumSoundAdOutput;

    iput-object v1, p0, Lcom/mstar/android/tvapi/common/vo/UserSoundSetting;->adOutput:Lcom/mstar/android/tvapi/common/vo/EnumSoundAdOutput;

    iput-short v2, p0, Lcom/mstar/android/tvapi/common/vo/UserSoundSetting;->spdifDelay:S

    iput-short v2, p0, Lcom/mstar/android/tvapi/common/vo/UserSoundSetting;->speakerdelay:S

    iput-short v2, p0, Lcom/mstar/android/tvapi/common/vo/UserSoundSetting;->speakerPreScale:S

    iput-short v2, p0, Lcom/mstar/android/tvapi/common/vo/UserSoundSetting;->headPhonePreScale:S

    iput-short v2, p0, Lcom/mstar/android/tvapi/common/vo/UserSoundSetting;->lineOutPreScale:S

    iput-short v2, p0, Lcom/mstar/android/tvapi/common/vo/UserSoundSetting;->scart1PreScale:S

    iput-short v2, p0, Lcom/mstar/android/tvapi/common/vo/UserSoundSetting;->scart2PreScale:S

    iput-boolean v2, p0, Lcom/mstar/android/tvapi/common/vo/UserSoundSetting;->enableHi:Z

    const/4 v0, 0x0

    :goto_0
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAudioMode;->E_NUM:Lcom/mstar/android/tvapi/common/vo/EnumAudioMode;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAudioMode;->ordinal()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/mstar/android/tvapi/common/vo/UserSoundSetting;->astSoundModeSettings:[Lcom/mstar/android/tvapi/common/vo/SoundModeSetting;

    new-instance v2, Lcom/mstar/android/tvapi/common/vo/SoundModeSetting;

    invoke-direct {v2}, Lcom/mstar/android/tvapi/common/vo/SoundModeSetting;-><init>()V

    aput-object v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 5
    .param p1    # Landroid/os/Parcel;

    const/4 v3, 0x0

    const/4 v2, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAudioMode;->E_NUM:Lcom/mstar/android/tvapi/common/vo/EnumAudioMode;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAudioMode;->ordinal()I

    move-result v1

    new-array v1, v1, [Lcom/mstar/android/tvapi/common/vo/SoundModeSetting;

    iput-object v1, p0, Lcom/mstar/android/tvapi/common/vo/UserSoundSetting;->astSoundModeSettings:[Lcom/mstar/android/tvapi/common/vo/SoundModeSetting;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/mstar/android/tvapi/common/vo/UserSoundSetting;->checkSum:I

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;->E_MOVIE:Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;

    iput-object v1, p0, Lcom/mstar/android/tvapi/common/vo/UserSoundSetting;->soundMode:Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumAudysseyDynamicVolumeMode;->values()[Lcom/mstar/android/tvapi/common/vo/EnumAudysseyDynamicVolumeMode;

    move-result-object v1

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v4

    aget-object v1, v1, v4

    iput-object v1, p0, Lcom/mstar/android/tvapi/common/vo/UserSoundSetting;->audysseyDynamicVolume:Lcom/mstar/android/tvapi/common/vo/EnumAudysseyDynamicVolumeMode;

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumAudysseyEqMode;->values()[Lcom/mstar/android/tvapi/common/vo/EnumAudysseyEqMode;

    move-result-object v1

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v4

    aget-object v1, v1, v4

    iput-object v1, p0, Lcom/mstar/android/tvapi/common/vo/UserSoundSetting;->audysseyEq:Lcom/mstar/android/tvapi/common/vo/EnumAudysseyEqMode;

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumSurroundSystemType;->values()[Lcom/mstar/android/tvapi/common/vo/EnumSurroundSystemType;

    move-result-object v1

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v4

    aget-object v1, v1, v4

    iput-object v1, p0, Lcom/mstar/android/tvapi/common/vo/UserSoundSetting;->surroundSoundMode:Lcom/mstar/android/tvapi/common/vo/EnumSurroundSystemType;

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumSurroundType;->values()[Lcom/mstar/android/tvapi/common/vo/EnumSurroundType;

    move-result-object v1

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v4

    aget-object v1, v1, v4

    iput-object v1, p0, Lcom/mstar/android/tvapi/common/vo/UserSoundSetting;->surround:Lcom/mstar/android/tvapi/common/vo/EnumSurroundType;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-ne v1, v2, :cond_0

    move v1, v2

    :goto_0
    iput-boolean v1, p0, Lcom/mstar/android/tvapi/common/vo/UserSoundSetting;->enableAVC:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    int-to-short v1, v1

    iput-short v1, p0, Lcom/mstar/android/tvapi/common/vo/UserSoundSetting;->volume:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    int-to-short v1, v1

    iput-short v1, p0, Lcom/mstar/android/tvapi/common/vo/UserSoundSetting;->headphoneVolume:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    int-to-short v1, v1

    iput-short v1, p0, Lcom/mstar/android/tvapi/common/vo/UserSoundSetting;->balance:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    int-to-short v1, v1

    iput-short v1, p0, Lcom/mstar/android/tvapi/common/vo/UserSoundSetting;->primaryFlag:S

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    move-result-object v1

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v4

    aget-object v1, v1, v4

    iput-object v1, p0, Lcom/mstar/android/tvapi/common/vo/UserSoundSetting;->soundAudioLanguage1:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    move-result-object v1

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v4

    aget-object v1, v1, v4

    iput-object v1, p0, Lcom/mstar/android/tvapi/common/vo/UserSoundSetting;->soundAudioLanguage2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    int-to-short v1, v1

    iput-short v1, p0, Lcom/mstar/android/tvapi/common/vo/UserSoundSetting;->muteFlag:S

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumAudioMode;->values()[Lcom/mstar/android/tvapi/common/vo/EnumAudioMode;

    move-result-object v1

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v4

    aget-object v1, v1, v4

    iput-object v1, p0, Lcom/mstar/android/tvapi/common/vo/UserSoundSetting;->soundAudioChannel:Lcom/mstar/android/tvapi/common/vo/EnumAudioMode;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-ne v1, v2, :cond_1

    move v1, v2

    :goto_1
    iput-boolean v1, p0, Lcom/mstar/android/tvapi/common/vo/UserSoundSetting;->enableAd:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    int-to-short v1, v1

    iput-short v1, p0, Lcom/mstar/android/tvapi/common/vo/UserSoundSetting;->adVolume:S

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumSoundAdOutput;->values()[Lcom/mstar/android/tvapi/common/vo/EnumSoundAdOutput;

    move-result-object v1

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v4

    aget-object v1, v1, v4

    iput-object v1, p0, Lcom/mstar/android/tvapi/common/vo/UserSoundSetting;->adOutput:Lcom/mstar/android/tvapi/common/vo/EnumSoundAdOutput;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    int-to-short v1, v1

    iput-short v1, p0, Lcom/mstar/android/tvapi/common/vo/UserSoundSetting;->spdifDelay:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    int-to-short v1, v1

    iput-short v1, p0, Lcom/mstar/android/tvapi/common/vo/UserSoundSetting;->speakerdelay:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    int-to-short v1, v1

    iput-short v1, p0, Lcom/mstar/android/tvapi/common/vo/UserSoundSetting;->speakerPreScale:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    int-to-short v1, v1

    iput-short v1, p0, Lcom/mstar/android/tvapi/common/vo/UserSoundSetting;->headPhonePreScale:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    int-to-short v1, v1

    iput-short v1, p0, Lcom/mstar/android/tvapi/common/vo/UserSoundSetting;->lineOutPreScale:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    int-to-short v1, v1

    iput-short v1, p0, Lcom/mstar/android/tvapi/common/vo/UserSoundSetting;->scart1PreScale:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    int-to-short v1, v1

    iput-short v1, p0, Lcom/mstar/android/tvapi/common/vo/UserSoundSetting;->scart2PreScale:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-ne v1, v2, :cond_2

    :goto_2
    iput-boolean v2, p0, Lcom/mstar/android/tvapi/common/vo/UserSoundSetting;->enableHi:Z

    const/4 v0, 0x0

    :goto_3
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAudioMode;->E_NUM:Lcom/mstar/android/tvapi/common/vo/EnumAudioMode;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAudioMode;->ordinal()I

    move-result v1

    if-ge v0, v1, :cond_3

    iget-object v2, p0, Lcom/mstar/android/tvapi/common/vo/UserSoundSetting;->astSoundModeSettings:[Lcom/mstar/android/tvapi/common/vo/SoundModeSetting;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/SoundModeSetting;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v1, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/common/vo/SoundModeSetting;

    aput-object v1, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_0
    move v1, v3

    goto/16 :goto_0

    :cond_1
    move v1, v3

    goto :goto_1

    :cond_2
    move v2, v3

    goto :goto_2

    :cond_3
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget v0, p0, Lcom/mstar/android/tvapi/common/vo/UserSoundSetting;->checkSum:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/mstar/android/tvapi/common/vo/UserSoundSetting;->soundMode:Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/mstar/android/tvapi/common/vo/UserSoundSetting;->astSoundModeSettings:[Lcom/mstar/android/tvapi/common/vo/SoundModeSetting;

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelableArray([Landroid/os/Parcelable;I)V

    iget-object v0, p0, Lcom/mstar/android/tvapi/common/vo/UserSoundSetting;->audysseyDynamicVolume:Lcom/mstar/android/tvapi/common/vo/EnumAudysseyDynamicVolumeMode;

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/EnumAudysseyDynamicVolumeMode;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/mstar/android/tvapi/common/vo/UserSoundSetting;->audysseyEq:Lcom/mstar/android/tvapi/common/vo/EnumAudysseyEqMode;

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/EnumAudysseyEqMode;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/mstar/android/tvapi/common/vo/UserSoundSetting;->surroundSoundMode:Lcom/mstar/android/tvapi/common/vo/EnumSurroundSystemType;

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/EnumSurroundSystemType;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/mstar/android/tvapi/common/vo/UserSoundSetting;->surround:Lcom/mstar/android/tvapi/common/vo/EnumSurroundType;

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/EnumSurroundType;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/mstar/android/tvapi/common/vo/UserSoundSetting;->enableAVC:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/common/vo/UserSoundSetting;->volume:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/common/vo/UserSoundSetting;->headphoneVolume:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/common/vo/UserSoundSetting;->balance:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/common/vo/UserSoundSetting;->primaryFlag:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/mstar/android/tvapi/common/vo/UserSoundSetting;->soundAudioLanguage1:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/mstar/android/tvapi/common/vo/UserSoundSetting;->soundAudioLanguage2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/common/vo/UserSoundSetting;->muteFlag:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/mstar/android/tvapi/common/vo/UserSoundSetting;->soundAudioChannel:Lcom/mstar/android/tvapi/common/vo/EnumAudioMode;

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/EnumAudioMode;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/mstar/android/tvapi/common/vo/UserSoundSetting;->enableAd:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/common/vo/UserSoundSetting;->adVolume:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/mstar/android/tvapi/common/vo/UserSoundSetting;->adOutput:Lcom/mstar/android/tvapi/common/vo/EnumSoundAdOutput;

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/EnumSoundAdOutput;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/common/vo/UserSoundSetting;->spdifDelay:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/common/vo/UserSoundSetting;->speakerdelay:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/common/vo/UserSoundSetting;->speakerPreScale:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/common/vo/UserSoundSetting;->headPhonePreScale:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/common/vo/UserSoundSetting;->lineOutPreScale:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/common/vo/UserSoundSetting;->scart1PreScale:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/common/vo/UserSoundSetting;->scart2PreScale:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/mstar/android/tvapi/common/vo/UserSoundSetting;->enableHi:Z

    if-eqz v0, :cond_2

    :goto_2
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v1, v2

    goto :goto_2
.end method
