.class public Lcom/mstar/android/tvapi/common/TvManager;
.super Ljava/lang/Object;
.source "TvManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mstar/android/tvapi/common/TvManager$1;,
        Lcom/mstar/android/tvapi/common/TvManager$EventHandler;,
        Lcom/mstar/android/tvapi/common/TvManager$EVENT;
    }
.end annotation


# static fields
.field private static _tvManager:Lcom/mstar/android/tvapi/common/TvManager;


# instance fields
.field private mEventHandler:Lcom/mstar/android/tvapi/common/TvManager$EventHandler;

.field private mInputSrcStatus:[S

.field private mNativeContext:I

.field private mOnEventListener:Lcom/mstar/android/tvapi/common/listener/OnTvEventListener;

.field private mTvManagerContext:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    :try_start_0
    const-string v1, "tvmanager_jni"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->native_init()V
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const/4 v1, 0x0

    sput-object v1, Lcom/mstar/android/tvapi/common/TvManager;->_tvManager:Lcom/mstar/android/tvapi/common/TvManager;

    return-void

    :catch_0
    move-exception v0

    sget-object v1, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Cannot load tvmanager_jni library:\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/UnsatisfiedLinkError;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Lcom/mstar/android/tvapi/common/TvManager$EventHandler;

    invoke-direct {v1, p0, p0, v0}, Lcom/mstar/android/tvapi/common/TvManager$EventHandler;-><init>(Lcom/mstar/android/tvapi/common/TvManager;Lcom/mstar/android/tvapi/common/TvManager;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/mstar/android/tvapi/common/TvManager;->mEventHandler:Lcom/mstar/android/tvapi/common/TvManager$EventHandler;

    :goto_0
    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-direct {p0, v1}, Lcom/mstar/android/tvapi/common/TvManager;->native_setup(Ljava/lang/Object;)V

    return-void

    :cond_0
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    if-eqz v0, :cond_1

    new-instance v1, Lcom/mstar/android/tvapi/common/TvManager$EventHandler;

    invoke-direct {v1, p0, p0, v0}, Lcom/mstar/android/tvapi/common/TvManager$EventHandler;-><init>(Lcom/mstar/android/tvapi/common/TvManager;Lcom/mstar/android/tvapi/common/TvManager;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/mstar/android/tvapi/common/TvManager;->mEventHandler:Lcom/mstar/android/tvapi/common/TvManager$EventHandler;

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/mstar/android/tvapi/common/TvManager;->mEventHandler:Lcom/mstar/android/tvapi/common/TvManager$EventHandler;

    goto :goto_0
.end method

.method private static PostEvent_4k2kHDMIDisableDualView(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/common/TvManager;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, v1, Lcom/mstar/android/tvapi/common/TvManager;->mEventHandler:Lcom/mstar/android/tvapi/common/TvManager$EventHandler;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/TvManager;->mEventHandler:Lcom/mstar/android/tvapi/common/TvManager$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/common/TvManager$EVENT;->EV_4K2K_HDMI_DISABLE_DUALVIEW:Lcom/mstar/android/tvapi/common/TvManager$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/common/TvManager$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/TvManager;->mEventHandler:Lcom/mstar/android/tvapi/common/TvManager$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/common/TvManager$EventHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private static PostEvent_4k2kHDMIDisablePip(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/common/TvManager;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, v1, Lcom/mstar/android/tvapi/common/TvManager;->mEventHandler:Lcom/mstar/android/tvapi/common/TvManager$EventHandler;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/TvManager;->mEventHandler:Lcom/mstar/android/tvapi/common/TvManager$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/common/TvManager$EVENT;->EV_4K2K_HDMI_DISABLE_PIP:Lcom/mstar/android/tvapi/common/TvManager$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/common/TvManager$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/TvManager;->mEventHandler:Lcom/mstar/android/tvapi/common/TvManager$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/common/TvManager$EventHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private static PostEvent_4k2kHDMIDisablePop(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/common/TvManager;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, v1, Lcom/mstar/android/tvapi/common/TvManager;->mEventHandler:Lcom/mstar/android/tvapi/common/TvManager$EventHandler;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/TvManager;->mEventHandler:Lcom/mstar/android/tvapi/common/TvManager$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/common/TvManager$EVENT;->EV_4K2K_HDMI_DISABLE_POP:Lcom/mstar/android/tvapi/common/TvManager$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/common/TvManager$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/TvManager;->mEventHandler:Lcom/mstar/android/tvapi/common/TvManager$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/common/TvManager$EventHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private static PostEvent_4k2kHDMIDisableTravelingMode(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/common/TvManager;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, v1, Lcom/mstar/android/tvapi/common/TvManager;->mEventHandler:Lcom/mstar/android/tvapi/common/TvManager$EventHandler;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/TvManager;->mEventHandler:Lcom/mstar/android/tvapi/common/TvManager$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/common/TvManager$EVENT;->EV_4K2K_HDMI_DISABLE_TRAVELINGMODE:Lcom/mstar/android/tvapi/common/TvManager$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/common/TvManager$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/TvManager;->mEventHandler:Lcom/mstar/android/tvapi/common/TvManager$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/common/TvManager$EventHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private static PostEvent_DtvReadyPopupDialog(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/common/TvManager;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, v1, Lcom/mstar/android/tvapi/common/TvManager;->mEventHandler:Lcom/mstar/android/tvapi/common/TvManager$EventHandler;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/TvManager;->mEventHandler:Lcom/mstar/android/tvapi/common/TvManager$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/common/TvManager$EVENT;->EV_DTV_READY_POPUP_DIALOG:Lcom/mstar/android/tvapi/common/TvManager$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/common/TvManager$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/TvManager;->mEventHandler:Lcom/mstar/android/tvapi/common/TvManager$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/common/TvManager$EventHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private static PostEvent_PopupDialog(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/common/TvManager;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, v1, Lcom/mstar/android/tvapi/common/TvManager;->mEventHandler:Lcom/mstar/android/tvapi/common/TvManager$EventHandler;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/TvManager;->mEventHandler:Lcom/mstar/android/tvapi/common/TvManager$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/common/TvManager$EVENT;->EV_ATSC_POPUP_DIALOG:Lcom/mstar/android/tvapi/common/TvManager$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/common/TvManager$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/TvManager;->mEventHandler:Lcom/mstar/android/tvapi/common/TvManager$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/common/TvManager$EventHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private static PostEvent_ScartMuteOsdMode(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/common/TvManager;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, v1, Lcom/mstar/android/tvapi/common/TvManager;->mEventHandler:Lcom/mstar/android/tvapi/common/TvManager$EventHandler;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/TvManager;->mEventHandler:Lcom/mstar/android/tvapi/common/TvManager$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/common/TvManager$EVENT;->EV_SCARTMUTE_OSD_MODE:Lcom/mstar/android/tvapi/common/TvManager$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/common/TvManager$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/TvManager;->mEventHandler:Lcom/mstar/android/tvapi/common/TvManager$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/common/TvManager$EventHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private static PostEvent_ScreenSaverMode(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/common/TvManager;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, v1, Lcom/mstar/android/tvapi/common/TvManager;->mEventHandler:Lcom/mstar/android/tvapi/common/TvManager$EventHandler;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/TvManager;->mEventHandler:Lcom/mstar/android/tvapi/common/TvManager$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/common/TvManager$EVENT;->EV_SCREEN_SAVER_MODE:Lcom/mstar/android/tvapi/common/TvManager$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/common/TvManager$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/TvManager;->mEventHandler:Lcom/mstar/android/tvapi/common/TvManager$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/common/TvManager$EventHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private static PostEvent_SignalLock(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/common/TvManager;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, v1, Lcom/mstar/android/tvapi/common/TvManager;->mEventHandler:Lcom/mstar/android/tvapi/common/TvManager$EventHandler;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/TvManager;->mEventHandler:Lcom/mstar/android/tvapi/common/TvManager$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/common/TvManager$EVENT;->EV_SIGNAL_LOCK:Lcom/mstar/android/tvapi/common/TvManager$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/common/TvManager$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/TvManager;->mEventHandler:Lcom/mstar/android/tvapi/common/TvManager$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/common/TvManager$EventHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private static PostEvent_SignalUnlock(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/common/TvManager;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, v1, Lcom/mstar/android/tvapi/common/TvManager;->mEventHandler:Lcom/mstar/android/tvapi/common/TvManager$EventHandler;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/TvManager;->mEventHandler:Lcom/mstar/android/tvapi/common/TvManager$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/common/TvManager$EVENT;->EV_SIGNAL_UNLOCK:Lcom/mstar/android/tvapi/common/TvManager$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/common/TvManager$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/TvManager;->mEventHandler:Lcom/mstar/android/tvapi/common/TvManager$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/common/TvManager$EventHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private static PostEvent_SnServiceDeadth(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/common/TvManager;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, v1, Lcom/mstar/android/tvapi/common/TvManager;->mEventHandler:Lcom/mstar/android/tvapi/common/TvManager$EventHandler;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/TvManager;->mEventHandler:Lcom/mstar/android/tvapi/common/TvManager$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/common/TvManager$EVENT;->EV_DEATH:Lcom/mstar/android/tvapi/common/TvManager$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/common/TvManager$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/TvManager;->mEventHandler:Lcom/mstar/android/tvapi/common/TvManager$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/common/TvManager$EventHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private static PostEvent_UnityEvent(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    const-string v2, "aaron"

    const-string v3, "PostEvent_UnityEvent in 1"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/common/TvManager;

    if-nez v1, :cond_1

    const-string v2, "aaron"

    const-string v3, "PostEvent_UnityEvent in 2"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, v1, Lcom/mstar/android/tvapi/common/TvManager;->mEventHandler:Lcom/mstar/android/tvapi/common/TvManager$EventHandler;

    if-eqz v2, :cond_0

    const-string v2, "aaron"

    const-string v3, "PostEvent_UnityEvent in 3"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/TvManager;->mEventHandler:Lcom/mstar/android/tvapi/common/TvManager$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/common/TvManager$EVENT;->EV_UNITY_EVENT:Lcom/mstar/android/tvapi/common/TvManager$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/common/TvManager$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    const-string v2, "aaron"

    const-string v3, "PostEvent_UnityEvent in 4"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/TvManager;->mEventHandler:Lcom/mstar/android/tvapi/common/TvManager$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/common/TvManager$EventHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/mstar/android/tvapi/common/TvManager;)I
    .locals 1
    .param p0    # Lcom/mstar/android/tvapi/common/TvManager;

    iget v0, p0, Lcom/mstar/android/tvapi/common/TvManager;->mNativeContext:I

    return v0
.end method

.method static synthetic access$100(Lcom/mstar/android/tvapi/common/TvManager;)Lcom/mstar/android/tvapi/common/listener/OnTvEventListener;
    .locals 1
    .param p0    # Lcom/mstar/android/tvapi/common/TvManager;

    iget-object v0, p0, Lcom/mstar/android/tvapi/common/TvManager;->mOnEventListener:Lcom/mstar/android/tvapi/common/listener/OnTvEventListener;

    return-object v0
.end method

.method public static getInstance()Lcom/mstar/android/tvapi/common/TvManager;
    .locals 2

    sget-object v0, Lcom/mstar/android/tvapi/common/TvManager;->_tvManager:Lcom/mstar/android/tvapi/common/TvManager;

    if-nez v0, :cond_1

    const-class v1, Lcom/mstar/android/tvapi/common/TvManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/mstar/android/tvapi/common/TvManager;->_tvManager:Lcom/mstar/android/tvapi/common/TvManager;

    if-nez v0, :cond_0

    new-instance v0, Lcom/mstar/android/tvapi/common/TvManager;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/common/TvManager;-><init>()V

    sput-object v0, Lcom/mstar/android/tvapi/common/TvManager;->_tvManager:Lcom/mstar/android/tvapi/common/TvManager;

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    sget-object v0, Lcom/mstar/android/tvapi/common/TvManager;->_tvManager:Lcom/mstar/android/tvapi/common/TvManager;

    return-object v0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private native native_enablePowerOnLogo(I)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private native native_finalize()V
.end method

.method private native native_getCurrentInputSource()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private native native_getCurrentLanguageIndex(Ljava/lang/String;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private native native_getCurrentMainInputSource()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private native native_getCurrentSubInputSource()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private native native_getDivxDrmRegistrationInformation(I)Lcom/mstar/android/tvapi/common/vo/DivxDrmRegistrationInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private native native_getEnvironmentPowerOnLogMode()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private native native_getEnvironmentPowerOnMusicMode()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private native native_getRoutePathDtvType(S)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private static native native_init()V
.end method

.method private native native_setEnvironmentPowerOnLogoMode(I)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private native native_setEnvironmentPowerOnMusicMode(I)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private native native_setInputSource(I)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private native native_setInputSource(IZZZ)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private native native_setLanguage(I)V
.end method

.method private native native_setVideoMute(ZIII)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private native native_setup(Ljava/lang/Object;)V
.end method

.method private native native_startUrsaFirmwareUpgrade(Ljava/lang/String;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private static postEventFromNative(Ljava/lang/Object;III)V
    .locals 3
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I
    .param p3    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/common/TvManager;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, v1, Lcom/mstar/android/tvapi/common/TvManager;->mEventHandler:Lcom/mstar/android/tvapi/common/TvManager$EventHandler;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/TvManager;->mEventHandler:Lcom/mstar/android/tvapi/common/TvManager$EventHandler;

    invoke-virtual {v2, p1, p2, p3}, Lcom/mstar/android/tvapi/common/TvManager$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/TvManager;->mEventHandler:Lcom/mstar/android/tvapi/common/TvManager$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/common/TvManager$EventHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method


# virtual methods
.method public native disableI2c(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public native disablePowerOnLogo()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public native disablePowerOnMusic()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method protected native disableScartOutRgb()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public native disableTvosIr()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public native enableI2c(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public enablePowerOnLogo(Lcom/mstar/android/tvapi/common/vo/EnumPowerOnLogoMode;)Z
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumPowerOnLogoMode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumPowerOnLogoMode;->ordinal()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/mstar/android/tvapi/common/TvManager;->native_enablePowerOnLogo(I)Z

    move-result v0

    return v0
.end method

.method public native enablePowerOnMusic()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method protected native enableScartOutRgb()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public native enterSleepMode(ZZ)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public native enterStrMode()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method protected finalize()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    invoke-direct {p0}, Lcom/mstar/android/tvapi/common/TvManager;->native_finalize()V

    const/4 v0, 0x0

    sput-object v0, Lcom/mstar/android/tvapi/common/TvManager;->_tvManager:Lcom/mstar/android/tvapi/common/TvManager;

    return-void
.end method

.method public finalizeAllManager()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    return-void
.end method

.method public getAudioManager()Lcom/mstar/android/tvapi/common/AudioManager;
    .locals 1

    const/4 v0, 0x0

    invoke-static {}, Lcom/mstar/android/tvapi/common/AudioManager;->getInstance()Lcom/mstar/android/tvapi/common/AudioManager;

    move-result-object v0

    return-object v0
.end method

.method public getCecManager()Lcom/mstar/android/tvapi/common/CecManager;
    .locals 1

    const/4 v0, 0x0

    invoke-static {}, Lcom/mstar/android/tvapi/common/CecManager;->getInstance()Lcom/mstar/android/tvapi/common/CecManager;

    move-result-object v0

    return-object v0
.end method

.method public getChannelManager()Lcom/mstar/android/tvapi/common/ChannelManager;
    .locals 1

    const/4 v0, 0x0

    invoke-static {}, Lcom/mstar/android/tvapi/common/ChannelManager;->getInstance()Lcom/mstar/android/tvapi/common/ChannelManager;

    move-result-object v0

    return-object v0
.end method

.method public native getCurrentDtvRoute()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public getCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/mstar/android/tvapi/common/TvManager;->native_getCurrentInputSource()I

    move-result v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_VGA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    if-lt v0, v1, :cond_0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_NONE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    if-le v0, v1, :cond_1

    :cond_0
    new-instance v1, Lcom/mstar/android/tvapi/common/exception/TvCommonException;

    const-string v2, "native_getCurrentInputSource failed"

    invoke-direct {v1, v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v1

    aget-object v1, v1, v0

    return-object v1
.end method

.method public getCurrentLanguageIndex(Ljava/lang/String;)Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;
    .locals 3
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/mstar/android/tvapi/common/TvManager;->native_getCurrentLanguageIndex(Ljava/lang/String;)I

    move-result v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_CZECH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    if-lt v0, v1, :cond_0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_MAX:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v1

    if-le v0, v1, :cond_1

    :cond_0
    new-instance v1, Lcom/mstar/android/tvapi/common/exception/TvCommonException;

    const-string v2, "getCurrentLanguageIndex failed"

    invoke-direct {v1, v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    move-result-object v1

    aget-object v1, v1, v0

    return-object v1
.end method

.method public getCurrentMainInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/mstar/android/tvapi/common/TvManager;->native_getCurrentMainInputSource()I

    move-result v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_VGA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    if-lt v0, v1, :cond_0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_NONE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    if-le v0, v1, :cond_1

    :cond_0
    new-instance v1, Lcom/mstar/android/tvapi/common/exception/TvCommonException;

    const-string v2, "native_getCurrentMainInputSource failed"

    invoke-direct {v1, v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v1

    aget-object v1, v1, v0

    return-object v1
.end method

.method public getCurrentSubInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/mstar/android/tvapi/common/TvManager;->native_getCurrentSubInputSource()I

    move-result v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_VGA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    if-lt v0, v1, :cond_0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_NONE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    if-le v0, v1, :cond_1

    :cond_0
    new-instance v1, Lcom/mstar/android/tvapi/common/exception/TvCommonException;

    const-string v2, "native_getCurrentSubInputSource failed"

    invoke-direct {v1, v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v1

    aget-object v1, v1, v0

    return-object v1
.end method

.method public getDatabaseManager()Lcom/mstar/android/tvapi/common/DatabaseManager;
    .locals 1

    const/4 v0, 0x0

    invoke-static {}, Lcom/mstar/android/tvapi/common/DatabaseManager;->getInstance()Lcom/mstar/android/tvapi/common/DatabaseManager;

    move-result-object v0

    return-object v0
.end method

.method public native getDefaultDisplay()S
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public getDivxDrmRegistrationInformation(Lcom/mstar/android/tvapi/common/vo/EnumDrmOpMode;)Lcom/mstar/android/tvapi/common/vo/DivxDrmRegistrationInfo;
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumDrmOpMode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumDrmOpMode;->ordinal()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/mstar/android/tvapi/common/TvManager;->native_getDivxDrmRegistrationInformation(I)Lcom/mstar/android/tvapi/common/vo/DivxDrmRegistrationInfo;

    move-result-object v0

    return-object v0
.end method

.method public native getEnvironment(Ljava/lang/String;)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public getEnvironmentPowerOnLogoMode()Lcom/mstar/android/tvapi/common/vo/EnumPowerOnLogoMode;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/mstar/android/tvapi/common/TvManager;->native_getEnvironmentPowerOnLogMode()I

    move-result v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumPowerOnLogoMode;->E_POWERON_LOGO_DEFAULT:Lcom/mstar/android/tvapi/common/vo/EnumPowerOnLogoMode;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumPowerOnLogoMode;->ordinal()I

    move-result v1

    if-lt v0, v1, :cond_0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumPowerOnLogoMode;->E_POWERON_LOGO_MAX:Lcom/mstar/android/tvapi/common/vo/EnumPowerOnLogoMode;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumPowerOnLogoMode;->ordinal()I

    move-result v1

    if-le v0, v1, :cond_1

    :cond_0
    new-instance v1, Lcom/mstar/android/tvapi/common/exception/TvCommonException;

    const-string v2, "get environment value for power on logomode failed \n"

    invoke-direct {v1, v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumPowerOnLogoMode;->values()[Lcom/mstar/android/tvapi/common/vo/EnumPowerOnLogoMode;

    move-result-object v1

    aget-object v1, v1, v0

    return-object v1
.end method

.method public getEnvironmentPowerOnMusicMode()Lcom/mstar/android/tvapi/common/vo/EnumPowerOnMusicMode;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/mstar/android/tvapi/common/TvManager;->native_getEnvironmentPowerOnMusicMode()I

    move-result v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumPowerOnMusicMode;->E_POWERON_MUSIC_OFF:Lcom/mstar/android/tvapi/common/vo/EnumPowerOnMusicMode;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumPowerOnMusicMode;->ordinal()I

    move-result v1

    if-lt v0, v1, :cond_0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumPowerOnMusicMode;->E_POWERON_MUSIC_MAX:Lcom/mstar/android/tvapi/common/vo/EnumPowerOnMusicMode;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumPowerOnMusicMode;->ordinal()I

    move-result v1

    if-le v0, v1, :cond_1

    :cond_0
    new-instance v1, Lcom/mstar/android/tvapi/common/exception/TvCommonException;

    const-string v2, "get evironment for prowe on music mode failed"

    invoke-direct {v1, v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumPowerOnMusicMode;->values()[Lcom/mstar/android/tvapi/common/vo/EnumPowerOnMusicMode;

    move-result-object v1

    aget-object v1, v1, v0

    return-object v1
.end method

.method public getFactoryManager()Lcom/mstar/android/tvapi/factory/FactoryManager;
    .locals 2

    const/4 v0, 0x0

    new-instance v1, Lcom/mstar/android/tvapi/common/TvFactoryManagerProxy;

    invoke-direct {v1}, Lcom/mstar/android/tvapi/common/TvFactoryManagerProxy;-><init>()V

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvFactoryManagerProxy;->getFactoryManagerInstance()Lcom/mstar/android/tvapi/factory/FactoryManager;

    move-result-object v0

    return-object v0
.end method

.method public native getFrcVersion()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public native getGpioDeviceStatus(I)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public getLogoManager()Lcom/mstar/android/tvapi/common/LogoManager;
    .locals 1

    const/4 v0, 0x0

    invoke-static {}, Lcom/mstar/android/tvapi/common/LogoManager;->getInstance()Lcom/mstar/android/tvapi/common/LogoManager;

    move-result-object v0

    return-object v0
.end method

.method public getMhlManager()Lcom/mstar/android/tvapi/common/MhlManager;
    .locals 1

    const/4 v0, 0x0

    invoke-static {}, Lcom/mstar/android/tvapi/common/MhlManager;->getInstance()Lcom/mstar/android/tvapi/common/MhlManager;

    move-result-object v0

    return-object v0
.end method

.method public getParentalcontrolManager()Lcom/mstar/android/tvapi/common/ParentalcontrolManager;
    .locals 1

    const/4 v0, 0x0

    invoke-static {}, Lcom/mstar/android/tvapi/common/ParentalcontrolManager;->getInstance()Lcom/mstar/android/tvapi/common/ParentalcontrolManager;

    move-result-object v0

    return-object v0
.end method

.method public getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;
    .locals 1

    invoke-static {}, Lcom/mstar/android/tvapi/common/PictureManager;->getInstance()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v0

    return-object v0
.end method

.method public getPipManager()Lcom/mstar/android/tvapi/common/PipManager;
    .locals 1

    const/4 v0, 0x0

    invoke-static {}, Lcom/mstar/android/tvapi/common/PipManager;->getInstance()Lcom/mstar/android/tvapi/common/PipManager;

    move-result-object v0

    return-object v0
.end method

.method public getPlayerManager()Lcom/mstar/android/tvapi/common/TvPlayer;
    .locals 2

    const/4 v1, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/common/TvPlayerImplProxy;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/common/TvPlayerImplProxy;-><init>()V

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvPlayerImplProxy;->getPlayerImplInstance()Lcom/mstar/android/tvapi/impl/PlayerImpl;

    move-result-object v1

    return-object v1
.end method

.method public getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;
    .locals 1

    const/4 v0, 0x0

    invoke-static {}, Lcom/mstar/android/tvapi/common/PvrManager;->getInstance()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v0

    return-object v0
.end method

.method public getRoutePathDtvType(S)Lcom/mstar/android/tvapi/dtv/dvb/vo/EnumDvbSystemType;
    .locals 3
    .param p1    # S
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/mstar/android/tvapi/common/TvManager;->native_getRoutePathDtvType(S)I

    move-result v0

    sget-object v1, Lcom/mstar/android/tvapi/dtv/dvb/vo/EnumDvbSystemType;->E_DVB_System_NONE:Lcom/mstar/android/tvapi/dtv/dvb/vo/EnumDvbSystemType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/dtv/dvb/vo/EnumDvbSystemType;->ordinal()I

    move-result v1

    if-lt v0, v1, :cond_0

    sget-object v1, Lcom/mstar/android/tvapi/dtv/dvb/vo/EnumDvbSystemType;->E_DVB_System_NUM:Lcom/mstar/android/tvapi/dtv/dvb/vo/EnumDvbSystemType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/dtv/dvb/vo/EnumDvbSystemType;->ordinal()I

    move-result v1

    if-le v0, v1, :cond_1

    :cond_0
    new-instance v1, Lcom/mstar/android/tvapi/common/exception/TvCommonException;

    const-string v2, "native_getRoutePathDtvType failed"

    invoke-direct {v1, v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    invoke-static {}, Lcom/mstar/android/tvapi/dtv/dvb/vo/EnumDvbSystemType;->values()[Lcom/mstar/android/tvapi/dtv/dvb/vo/EnumDvbSystemType;

    move-result-object v1

    aget-object v1, v1, v0

    return-object v1
.end method

.method public native getSarAdcLevel(S)S
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public getScanManager()Lcom/mstar/android/tvapi/common/ScanManager;
    .locals 2

    const/4 v1, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/common/TvScanImplProxy;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/common/TvScanImplProxy;-><init>()V

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvScanImplProxy;->getScanImplInstance()Lcom/mstar/android/tvapi/impl/ScanManagerImpl;

    move-result-object v1

    return-object v1
.end method

.method public native getSourceList()[I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public native getSystemBoardName()Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public native getSystemCurrentGammaTableNo()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public native getSystemPanelName()Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public native getSystemSoftwareVersion()Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public native getSystemTotalGammaTableNo()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;
    .locals 1

    const/4 v0, 0x0

    invoke-static {}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->getInstance()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v0

    return-object v0
.end method

.method public getTimerManager()Lcom/mstar/android/tvapi/common/TimerManager;
    .locals 1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TimerManager;->getInstance()Lcom/mstar/android/tvapi/common/TimerManager;

    move-result-object v0

    return-object v0
.end method

.method public native getTvInfo()Lcom/mstar/android/tvapi/common/vo/TvTypeInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public native getTvosInterfaceCommand()Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public native isSignalStable(I)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public native isTvBootFinished()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public native isUsbUpgradeFileValid()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public native isUsbUpgradeFileValid(Ljava/lang/String;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public native playPowerOffMusic(Ljava/lang/String;I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public native readFromEeprom(SI)[S
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public native readFromSpiFlashByAddress(II)[S
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public native readFromSpiFlashByBank(II)[S
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public native rebootSystem()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public native resetForMbootUpgrade(Ljava/lang/String;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public native resetForNandUpgrade()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public native resetForNetworkUpgrade()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public native resetForUsbUpgrade(Lcom/mstar/android/tvapi/common/vo/UsbUpgradeCfg;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public native resetPostEvent()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public native resetToFactoryDefault()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public native searchFileInUsb(Ljava/lang/String;Ljava/lang/String;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public native setDebugMode(Z)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public native setDefaultDisplay(S)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public native setEnvironment(Ljava/lang/String;Ljava/lang/String;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public setEnvironmentPowerOnLogoMode(Lcom/mstar/android/tvapi/common/vo/EnumPowerOnLogoMode;)Z
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumPowerOnLogoMode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumPowerOnLogoMode;->ordinal()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/mstar/android/tvapi/common/TvManager;->native_setEnvironmentPowerOnLogoMode(I)Z

    move-result v0

    return v0
.end method

.method public setEnvironmentPowerOnMusicMode(Lcom/mstar/android/tvapi/common/vo/EnumPowerOnMusicMode;)Z
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumPowerOnMusicMode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumPowerOnMusicMode;->ordinal()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/mstar/android/tvapi/common/TvManager;->native_setEnvironmentPowerOnMusicMode(I)Z

    move-result v0

    return v0
.end method

.method public native setGpioDeviceStatus(IZ)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public setInputSource(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Z
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/mstar/android/tvapi/common/TvManager;->native_setInputSource(I)Z

    move-result v0

    return v0
.end method

.method public setInputSource(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;ZZZ)Z
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    .param p2    # Z
    .param p3    # Z
    .param p4    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v0

    invoke-direct {p0, v0, p2, p3, p4}, Lcom/mstar/android/tvapi/common/TvManager;->native_setInputSource(IZZZ)Z

    move-result v0

    return v0
.end method

.method public setLanguage(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;)V
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/mstar/android/tvapi/common/TvManager;->native_setLanguage(I)V

    return-void
.end method

.method public setOnTvEventListener(Lcom/mstar/android/tvapi/common/listener/OnTvEventListener;)V
    .locals 3
    .param p1    # Lcom/mstar/android/tvapi/common/listener/OnTvEventListener;

    iput-object p1, p0, Lcom/mstar/android/tvapi/common/TvManager;->mOnEventListener:Lcom/mstar/android/tvapi/common/listener/OnTvEventListener;

    const-string v0, "aaron"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "this: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", listener: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public native setTvosCommonCommand(Ljava/lang/String;)[S
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public native setTvosInterfaceCommand(Ljava/lang/String;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public setVideoMute(ZLcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;ILcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Z
    .locals 2
    .param p1    # Z
    .param p2    # Lcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;
    .param p3    # I
    .param p4    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p2}, Lcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;->ordinal()I

    move-result v0

    invoke-virtual {p4}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    invoke-direct {p0, p1, v0, p3, v1}, Lcom/mstar/android/tvapi/common/TvManager;->native_setVideoMute(ZIII)Z

    move-result v0

    return v0
.end method

.method public startUrsaFirmwareUpgrade(Ljava/lang/String;)Lcom/mstar/android/tvapi/common/vo/EnumUrsaUpgradeStatus;
    .locals 3
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/mstar/android/tvapi/common/TvManager;->native_startUrsaFirmwareUpgrade(Ljava/lang/String;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumUrsaUpgradeStatus;->values()[Lcom/mstar/android/tvapi/common/vo/EnumUrsaUpgradeStatus;

    move-result-object v1

    aget-object v1, v1, v0

    return-object v1

    :cond_0
    new-instance v1, Lcom/mstar/android/tvapi/common/exception/TvCommonException;

    const-string v2, "startUrsaFirmwareUpgrade error "

    invoke-direct {v1, v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public native updateCustomerIniFile(Ljava/lang/String;Ljava/lang/String;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public native updatePanelIniFile(Ljava/lang/String;Ljava/lang/String;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public native writeToEeprom(S[S)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public native writeToSpiFlashByAddress(I[S)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public native writeToSpiFlashByBank(I[S)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method
