.class public interface abstract Lcom/mstar/android/tvapi/common/TimerManager$OnTimerEventListener;
.super Ljava/lang/Object;
.source "TimerManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mstar/android/tvapi/common/TimerManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnTimerEventListener"
.end annotation


# virtual methods
.method public abstract onDestroyCountDown(Lcom/mstar/android/tvapi/common/TimerManager;III)Z
.end method

.method public abstract onEpgTimeUp(Lcom/mstar/android/tvapi/common/TimerManager;III)Z
.end method

.method public abstract onEpgTimerCountDown(Lcom/mstar/android/tvapi/common/TimerManager;III)Z
.end method

.method public abstract onEpgTimerRecordStart(Lcom/mstar/android/tvapi/common/TimerManager;III)Z
.end method

.method public abstract onLastMinuteWarn(Lcom/mstar/android/tvapi/common/TimerManager;III)Z
.end method

.method public abstract onOadTimeScan(Lcom/mstar/android/tvapi/common/TimerManager;III)Z
.end method

.method public abstract onOneSecondBeat(Lcom/mstar/android/tvapi/common/TimerManager;III)Z
.end method

.method public abstract onPowerDownTime(Lcom/mstar/android/tvapi/common/TimerManager;III)Z
.end method

.method public abstract onPvrNotifyRecordStop(Lcom/mstar/android/tvapi/common/TimerManager;III)Z
.end method

.method public abstract onSignalLock(Lcom/mstar/android/tvapi/common/TimerManager;III)Z
.end method

.method public abstract onSystemClkChg(Lcom/mstar/android/tvapi/common/TimerManager;III)Z
.end method

.method public abstract onUpdateLastMinute(Lcom/mstar/android/tvapi/common/TimerManager;III)Z
.end method
