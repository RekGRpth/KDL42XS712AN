.class public final Lcom/mstar/android/tvapi/common/PipManager;
.super Ljava/lang/Object;
.source "PipManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mstar/android/tvapi/common/PipManager$1;,
        Lcom/mstar/android/tvapi/common/PipManager$EventHandler;,
        Lcom/mstar/android/tvapi/common/PipManager$EVENT;
    }
.end annotation


# static fields
.field private static _pipManager:Lcom/mstar/android/tvapi/common/PipManager;


# instance fields
.field private mEventHandler:Lcom/mstar/android/tvapi/common/PipManager$EventHandler;

.field private mNativeContext:I

.field private mOnEventListener:Lcom/mstar/android/tvapi/common/listener/OnPipEventListener;

.field private mPipManagerContext:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v1, 0x0

    sput-object v1, Lcom/mstar/android/tvapi/common/PipManager;->_pipManager:Lcom/mstar/android/tvapi/common/PipManager;

    :try_start_0
    const-string v1, "pipmanager_jni"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    invoke-static {}, Lcom/mstar/android/tvapi/common/PipManager;->native_init()V
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    sget-object v1, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Cannot load pipmanager_jni library:\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/UnsatisfiedLinkError;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Lcom/mstar/android/tvapi/common/PipManager$EventHandler;

    invoke-direct {v1, p0, p0, v0}, Lcom/mstar/android/tvapi/common/PipManager$EventHandler;-><init>(Lcom/mstar/android/tvapi/common/PipManager;Lcom/mstar/android/tvapi/common/PipManager;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/mstar/android/tvapi/common/PipManager;->mEventHandler:Lcom/mstar/android/tvapi/common/PipManager$EventHandler;

    :goto_0
    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-direct {p0, v1}, Lcom/mstar/android/tvapi/common/PipManager;->native_setup(Ljava/lang/Object;)V

    return-void

    :cond_0
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    if-eqz v0, :cond_1

    new-instance v1, Lcom/mstar/android/tvapi/common/PipManager$EventHandler;

    invoke-direct {v1, p0, p0, v0}, Lcom/mstar/android/tvapi/common/PipManager$EventHandler;-><init>(Lcom/mstar/android/tvapi/common/PipManager;Lcom/mstar/android/tvapi/common/PipManager;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/mstar/android/tvapi/common/PipManager;->mEventHandler:Lcom/mstar/android/tvapi/common/PipManager$EventHandler;

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/mstar/android/tvapi/common/PipManager;->mEventHandler:Lcom/mstar/android/tvapi/common/PipManager$EventHandler;

    goto :goto_0
.end method

.method private static PostEvent_4k2kUnsupportPip(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/common/PipManager;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, v1, Lcom/mstar/android/tvapi/common/PipManager;->mEventHandler:Lcom/mstar/android/tvapi/common/PipManager$EventHandler;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/PipManager;->mEventHandler:Lcom/mstar/android/tvapi/common/PipManager$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/common/PipManager$EVENT;->EV_4K2K_UNSUPPORT_PIP:Lcom/mstar/android/tvapi/common/PipManager$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/PipManager$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/common/PipManager$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/PipManager;->mEventHandler:Lcom/mstar/android/tvapi/common/PipManager$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/common/PipManager$EventHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private static PostEvent_4k2kUnsupportPop(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/common/PipManager;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, v1, Lcom/mstar/android/tvapi/common/PipManager;->mEventHandler:Lcom/mstar/android/tvapi/common/PipManager$EventHandler;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/PipManager;->mEventHandler:Lcom/mstar/android/tvapi/common/PipManager$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/common/PipManager$EVENT;->EV_4K2K_UNSUPPORT_POP:Lcom/mstar/android/tvapi/common/PipManager$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/PipManager$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/common/PipManager$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/PipManager;->mEventHandler:Lcom/mstar/android/tvapi/common/PipManager$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/common/PipManager$EventHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private static PostEvent_4k2kUnsupportTravelingMode(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/common/PipManager;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, v1, Lcom/mstar/android/tvapi/common/PipManager;->mEventHandler:Lcom/mstar/android/tvapi/common/PipManager$EventHandler;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/PipManager;->mEventHandler:Lcom/mstar/android/tvapi/common/PipManager$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/common/PipManager$EVENT;->EV_4K2K_UNSUPPORT_TRAVELINGMODE:Lcom/mstar/android/tvapi/common/PipManager$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/PipManager$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/common/PipManager$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/PipManager;->mEventHandler:Lcom/mstar/android/tvapi/common/PipManager$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/common/PipManager$EventHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private static PostEvent_EnablePip(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/common/PipManager;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, v1, Lcom/mstar/android/tvapi/common/PipManager;->mEventHandler:Lcom/mstar/android/tvapi/common/PipManager$EventHandler;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/PipManager;->mEventHandler:Lcom/mstar/android/tvapi/common/PipManager$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/common/PipManager$EVENT;->EV_ENABLE_PIP:Lcom/mstar/android/tvapi/common/PipManager$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/PipManager$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/common/PipManager$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/PipManager;->mEventHandler:Lcom/mstar/android/tvapi/common/PipManager$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/common/PipManager$EventHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private static PostEvent_EnablePop(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/common/PipManager;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, v1, Lcom/mstar/android/tvapi/common/PipManager;->mEventHandler:Lcom/mstar/android/tvapi/common/PipManager$EventHandler;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/PipManager;->mEventHandler:Lcom/mstar/android/tvapi/common/PipManager$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/common/PipManager$EVENT;->EV_ENABLE_POP:Lcom/mstar/android/tvapi/common/PipManager$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/PipManager$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/common/PipManager$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/PipManager;->mEventHandler:Lcom/mstar/android/tvapi/common/PipManager$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/common/PipManager$EventHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private static PostEvent_SnServiceDeadth(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    :try_start_0
    const-class v2, Lcom/mstar/android/tvapi/common/PipManager;

    monitor-enter v2
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    const-string v1, "mstar.str.suspending"

    const-string v3, "1"

    invoke-static {v1, v3}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x0

    sput-object v1, Lcom/mstar/android/tvapi/common/PipManager;->_pipManager:Lcom/mstar/android/tvapi/common/PipManager;

    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "PostEvent_SnServiceDeadth: set SystemProperties \'mstar.str.suspending\' =1"

    invoke-virtual {v1, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    monitor-exit v2

    :goto_0
    return-void

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v1
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/mstar/android/tvapi/common/PipManager;)I
    .locals 1
    .param p0    # Lcom/mstar/android/tvapi/common/PipManager;

    iget v0, p0, Lcom/mstar/android/tvapi/common/PipManager;->mNativeContext:I

    return v0
.end method

.method static synthetic access$100(Lcom/mstar/android/tvapi/common/PipManager;)Lcom/mstar/android/tvapi/common/listener/OnPipEventListener;
    .locals 1
    .param p0    # Lcom/mstar/android/tvapi/common/PipManager;

    iget-object v0, p0, Lcom/mstar/android/tvapi/common/PipManager;->mOnEventListener:Lcom/mstar/android/tvapi/common/listener/OnPipEventListener;

    return-object v0
.end method

.method public static getInstance()Lcom/mstar/android/tvapi/common/PipManager;
    .locals 2

    sget-object v0, Lcom/mstar/android/tvapi/common/PipManager;->_pipManager:Lcom/mstar/android/tvapi/common/PipManager;

    if-nez v0, :cond_1

    const-class v1, Lcom/mstar/android/tvapi/common/PipManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/mstar/android/tvapi/common/PipManager;->_pipManager:Lcom/mstar/android/tvapi/common/PipManager;

    if-nez v0, :cond_0

    new-instance v0, Lcom/mstar/android/tvapi/common/PipManager;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/common/PipManager;-><init>()V

    sput-object v0, Lcom/mstar/android/tvapi/common/PipManager;->_pipManager:Lcom/mstar/android/tvapi/common/PipManager;

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    sget-object v0, Lcom/mstar/android/tvapi/common/PipManager;->_pipManager:Lcom/mstar/android/tvapi/common/PipManager;

    return-object v0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private final native native_disablePip(I)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_disablePop(I)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_enablePipMm(ILcom/mstar/android/tvapi/common/vo/VideoWindowType;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_enablePipTv(IILcom/mstar/android/tvapi/common/vo/VideoWindowType;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_enablePopMm(I)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_enablePopTv(II)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_finalize()V
.end method

.method private final native native_getPipMode()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_getPipSupportedSubInputSource(I)[Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private static final native native_init()V
.end method

.method private final native native_isPipSupported(II)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_isPopSupported(II)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_isTravelingModeSupported(III)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_setup(Ljava/lang/Object;)V
.end method

.method private static postEventFromNative(Ljava/lang/Object;IIILjava/lang/Object;)V
    .locals 2
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # Ljava/lang/Object;

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "PipManager callback  \n"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    return-void
.end method

.method private native setPipDisplayFocusWindow(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method


# virtual methods
.method public final native clearFrame()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final disablePip(Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;)Z
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;->ordinal()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/mstar/android/tvapi/common/PipManager;->native_disablePip(I)Z

    move-result v0

    return v0
.end method

.method public final disablePop(Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;)Z
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;->ordinal()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/mstar/android/tvapi/common/PipManager;->native_disablePop(I)Z

    move-result v0

    return v0
.end method

.method public final enablePipMm(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;Lcom/mstar/android/tvapi/common/vo/VideoWindowType;)Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;
    .locals 3
    .param p1    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    .param p2    # Lcom/mstar/android/tvapi/common/vo/VideoWindowType;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    invoke-direct {p0, v1, p2}, Lcom/mstar/android/tvapi/common/PipManager;->native_enablePipMm(ILcom/mstar/android/tvapi/common/vo/VideoWindowType;)I

    move-result v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;->E_PIP_NOT_SUPPORT:Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;->ordinal()I

    move-result v1

    if-lt v0, v1, :cond_0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;->E_PIP_POP_MODE_NUM:Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;->ordinal()I

    move-result v1

    if-le v0, v1, :cond_1

    :cond_0
    new-instance v1, Lcom/mstar/android/tvapi/common/exception/TvCommonException;

    const-string v2, "enablePipMm failed"

    invoke-direct {v1, v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;->values()[Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;

    move-result-object v1

    aget-object v1, v1, v0

    return-object v1
.end method

.method public final enablePipTv(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;Lcom/mstar/android/tvapi/common/vo/VideoWindowType;)Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;
    .locals 3
    .param p1    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    .param p2    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    .param p3    # Lcom/mstar/android/tvapi/common/vo/VideoWindowType;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    invoke-virtual {p2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v2

    invoke-direct {p0, v1, v2, p3}, Lcom/mstar/android/tvapi/common/PipManager;->native_enablePipTv(IILcom/mstar/android/tvapi/common/vo/VideoWindowType;)I

    move-result v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;->E_PIP_NOT_SUPPORT:Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;->ordinal()I

    move-result v1

    if-lt v0, v1, :cond_0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;->E_PIP_POP_MODE_NUM:Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;->ordinal()I

    move-result v1

    if-le v0, v1, :cond_1

    :cond_0
    new-instance v1, Lcom/mstar/android/tvapi/common/exception/TvCommonException;

    const-string v2, "enablePipTv failed"

    invoke-direct {v1, v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;->values()[Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;

    move-result-object v1

    aget-object v1, v1, v0

    return-object v1
.end method

.method public final enablePopMm(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;
    .locals 3
    .param p1    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/mstar/android/tvapi/common/PipManager;->native_enablePopMm(I)I

    move-result v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;->E_PIP_NOT_SUPPORT:Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;->ordinal()I

    move-result v1

    if-lt v0, v1, :cond_0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;->E_PIP_POP_MODE_NUM:Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;->ordinal()I

    move-result v1

    if-le v0, v1, :cond_1

    :cond_0
    new-instance v1, Lcom/mstar/android/tvapi/common/exception/TvCommonException;

    const-string v2, "enablePopMm failed"

    invoke-direct {v1, v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;->values()[Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;

    move-result-object v1

    aget-object v1, v1, v0

    return-object v1
.end method

.method public final enablePopTv(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;
    .locals 3
    .param p1    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    .param p2    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    invoke-virtual {p2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v2

    invoke-direct {p0, v1, v2}, Lcom/mstar/android/tvapi/common/PipManager;->native_enablePopTv(II)I

    move-result v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;->E_PIP_NOT_SUPPORT:Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;->ordinal()I

    move-result v1

    if-lt v0, v1, :cond_0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;->E_PIP_POP_MODE_NUM:Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;->ordinal()I

    move-result v1

    if-le v0, v1, :cond_1

    :cond_0
    new-instance v1, Lcom/mstar/android/tvapi/common/exception/TvCommonException;

    const-string v2, "enablePopTv failed"

    invoke-direct {v1, v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;->values()[Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;

    move-result-object v1

    aget-object v1, v1, v0

    return-object v1
.end method

.method protected finalize()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    invoke-direct {p0}, Lcom/mstar/android/tvapi/common/PipManager;->native_finalize()V

    const/4 v0, 0x0

    sput-object v0, Lcom/mstar/android/tvapi/common/PipManager;->_pipManager:Lcom/mstar/android/tvapi/common/PipManager;

    return-void
.end method

.method public final getPipMode()Lcom/mstar/android/tvapi/common/vo/EnumPipModes;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/mstar/android/tvapi/common/PipManager;->native_getPipMode()I

    move-result v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumPipModes;->E_PIP_MODE_OFF:Lcom/mstar/android/tvapi/common/vo/EnumPipModes;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumPipModes;->ordinal()I

    move-result v1

    if-lt v0, v1, :cond_0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumPipModes;->E_PIP_MODE_TRAVELING:Lcom/mstar/android/tvapi/common/vo/EnumPipModes;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumPipModes;->ordinal()I

    move-result v1

    if-le v0, v1, :cond_1

    :cond_0
    new-instance v1, Lcom/mstar/android/tvapi/common/exception/TvCommonException;

    const-string v2, "get pipmode  failed"

    invoke-direct {v1, v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumPipModes;->values()[Lcom/mstar/android/tvapi/common/vo/EnumPipModes;

    move-result-object v1

    aget-object v1, v1, v0

    return-object v1
.end method

.method public final getPipSupportedSubInputSource(Lcom/mstar/android/tvapi/common/vo/EnumPipMode;)[Z
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumPipMode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumPipMode;->ordinal()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/mstar/android/tvapi/common/PipManager;->native_getPipSupportedSubInputSource(I)[Z

    move-result-object v0

    return-object v0
.end method

.method public final native isPipModeEnabled()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final isPipSupported(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Z
    .locals 2
    .param p1    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    .param p2    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v0

    invoke-virtual {p2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/mstar/android/tvapi/common/PipManager;->native_isPipSupported(II)Z

    move-result v0

    return v0
.end method

.method public final isPopSupported(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Z
    .locals 2
    .param p1    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    .param p2    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v0

    invoke-virtual {p2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/mstar/android/tvapi/common/PipManager;->native_isPopSupported(II)Z

    move-result v0

    return v0
.end method

.method public final isTravelingModeSupported(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Z
    .locals 3
    .param p1    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    .param p2    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v0

    invoke-virtual {p2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/mstar/android/tvapi/common/PipManager;->native_isTravelingModeSupported(III)Z

    move-result v0

    return v0
.end method

.method public final isTravelingModeSupported(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTravelingEngineType;)Z
    .locals 3
    .param p1    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    .param p2    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    .param p3    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTravelingEngineType;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v0

    invoke-virtual {p2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    invoke-virtual {p3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTravelingEngineType;->ordinal()I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/mstar/android/tvapi/common/PipManager;->native_isTravelingModeSupported(III)Z

    move-result v0

    return v0
.end method

.method protected release()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    const/4 v0, 0x0

    sput-object v0, Lcom/mstar/android/tvapi/common/PipManager;->_pipManager:Lcom/mstar/android/tvapi/common/PipManager;

    return-void
.end method

.method public native setDebugMode(Z)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public setOnPipEventListener(Lcom/mstar/android/tvapi/common/listener/OnPipEventListener;)V
    .locals 0
    .param p1    # Lcom/mstar/android/tvapi/common/listener/OnPipEventListener;

    iput-object p1, p0, Lcom/mstar/android/tvapi/common/PipManager;->mOnEventListener:Lcom/mstar/android/tvapi/common/listener/OnPipEventListener;

    return-void
.end method

.method public final setPipDisplayFocusWindow(Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;)V
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;->ordinal()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/mstar/android/tvapi/common/PipManager;->setPipDisplayFocusWindow(I)V

    return-void
.end method

.method public final native setPipSubWindow(Lcom/mstar/android/tvapi/common/vo/VideoWindowType;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method
