.class public final Lcom/mstar/android/tvapi/common/ThreeDimensionManager;
.super Ljava/lang/Object;
.source "ThreeDimensionManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mstar/android/tvapi/common/ThreeDimensionManager$1;,
        Lcom/mstar/android/tvapi/common/ThreeDimensionManager$EventHandler;,
        Lcom/mstar/android/tvapi/common/ThreeDimensionManager$EVENT;
    }
.end annotation


# static fields
.field private static _3dManager:Lcom/mstar/android/tvapi/common/ThreeDimensionManager;


# instance fields
.field private mEventHandler:Lcom/mstar/android/tvapi/common/ThreeDimensionManager$EventHandler;

.field private mNativeContext:I

.field private mOnEventListener:Lcom/mstar/android/tvapi/common/listener/OnThreeDimensionEventListener;

.field private mThreeDimensionManagerContext:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v1, 0x0

    sput-object v1, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->_3dManager:Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    :try_start_0
    const-string v1, "threedimensionmanager_jni"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    invoke-static {}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->native_init()V
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    sget-object v1, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Cannot load threedimensionmanager_jni library:\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/UnsatisfiedLinkError;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Lcom/mstar/android/tvapi/common/ThreeDimensionManager$EventHandler;

    invoke-direct {v1, p0, p0, v0}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager$EventHandler;-><init>(Lcom/mstar/android/tvapi/common/ThreeDimensionManager;Lcom/mstar/android/tvapi/common/ThreeDimensionManager;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->mEventHandler:Lcom/mstar/android/tvapi/common/ThreeDimensionManager$EventHandler;

    :goto_0
    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-direct {p0, v1}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->native_setup(Ljava/lang/Object;)V

    return-void

    :cond_0
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    if-eqz v0, :cond_1

    new-instance v1, Lcom/mstar/android/tvapi/common/ThreeDimensionManager$EventHandler;

    invoke-direct {v1, p0, p0, v0}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager$EventHandler;-><init>(Lcom/mstar/android/tvapi/common/ThreeDimensionManager;Lcom/mstar/android/tvapi/common/ThreeDimensionManager;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->mEventHandler:Lcom/mstar/android/tvapi/common/ThreeDimensionManager$EventHandler;

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->mEventHandler:Lcom/mstar/android/tvapi/common/ThreeDimensionManager$EventHandler;

    goto :goto_0
.end method

.method private static PostEvent_4k2kUnsupportDualView(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, v1, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->mEventHandler:Lcom/mstar/android/tvapi/common/ThreeDimensionManager$EventHandler;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->mEventHandler:Lcom/mstar/android/tvapi/common/ThreeDimensionManager$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/common/ThreeDimensionManager$EVENT;->EV_4K2K_UNSUPPORT_DUALVIEW:Lcom/mstar/android/tvapi/common/ThreeDimensionManager$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->mEventHandler:Lcom/mstar/android/tvapi/common/ThreeDimensionManager$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager$EventHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private static PostEvent_Enable3D(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, v1, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->mEventHandler:Lcom/mstar/android/tvapi/common/ThreeDimensionManager$EventHandler;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->mEventHandler:Lcom/mstar/android/tvapi/common/ThreeDimensionManager$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/common/ThreeDimensionManager$EVENT;->EV_ENABLE_3D:Lcom/mstar/android/tvapi/common/ThreeDimensionManager$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->mEventHandler:Lcom/mstar/android/tvapi/common/ThreeDimensionManager$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager$EventHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private static PostEvent_SnServiceDeadth(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    :try_start_0
    const-class v2, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    monitor-enter v2
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    const-string v1, "mstar.str.suspending"

    const-string v3, "1"

    invoke-static {v1, v3}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x0

    sput-object v1, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->_3dManager:Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "PostEvent_SnServiceDeadth: set SystemProperties \'mstar.str.suspending\' =1"

    invoke-virtual {v1, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    monitor-exit v2

    :goto_0
    return-void

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v1
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/mstar/android/tvapi/common/ThreeDimensionManager;)I
    .locals 1
    .param p0    # Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    iget v0, p0, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->mNativeContext:I

    return v0
.end method

.method static synthetic access$100(Lcom/mstar/android/tvapi/common/ThreeDimensionManager;)Lcom/mstar/android/tvapi/common/listener/OnThreeDimensionEventListener;
    .locals 1
    .param p0    # Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    iget-object v0, p0, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->mOnEventListener:Lcom/mstar/android/tvapi/common/listener/OnThreeDimensionEventListener;

    return-object v0
.end method

.method private final native enable3d(I)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public static getInstance()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;
    .locals 2

    sget-object v0, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->_3dManager:Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    if-nez v0, :cond_1

    const-class v1, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->_3dManager:Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    if-nez v0, :cond_0

    new-instance v0, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;-><init>()V

    sput-object v0, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->_3dManager:Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    sget-object v0, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->_3dManager:Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    return-object v0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private final native native_detect3dFormat(I)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private native native_enable3dDualView(IILcom/mstar/android/tvapi/common/vo/VideoWindowType;Lcom/mstar/android/tvapi/common/vo/VideoWindowType;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_enable3dTo2d(I)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_finalize()V
.end method

.method private final native native_get3dArc()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_getCurrent3dFormat()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private static final native native_init()V
.end method

.method private final native native_query3dCapability(I)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_set3dArc(I)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_setup(Ljava/lang/Object;)V
.end method

.method private static postEventFromNative(Ljava/lang/Object;IIILjava/lang/Object;)V
    .locals 2
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # Ljava/lang/Object;

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "ThreeDimensionManager callback  \n"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final detect3dFormat(Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;)Lcom/mstar/android/tvapi/common/vo/Enum3dType;
    .locals 3
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;->ordinal()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->native_detect3dFormat(I)I

    move-result v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_NONE:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->ordinal()I

    move-result v1

    if-lt v0, v1, :cond_0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_TYPE_NUM:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->ordinal()I

    move-result v1

    if-le v0, v1, :cond_1

    :cond_0
    new-instance v1, Lcom/mstar/android/tvapi/common/exception/TvCommonException;

    const-string v2, "detect3dFormat failed"

    invoke-direct {v1, v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->values()[Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    move-result-object v1

    aget-object v1, v1, v0

    return-object v1
.end method

.method public native disable3dDualView()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native disable3dLrSwitch()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public native disableLow3dQuality()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final enable3d(Lcom/mstar/android/tvapi/common/vo/Enum3dType;)Z
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/vo/Enum3dType;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->ordinal()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->enable3d(I)Z

    move-result v0

    return v0
.end method

.method public enable3dDualView(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;Lcom/mstar/android/tvapi/common/vo/VideoWindowType;Lcom/mstar/android/tvapi/common/vo/VideoWindowType;)Z
    .locals 2
    .param p1    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    .param p2    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    .param p3    # Lcom/mstar/android/tvapi/common/vo/VideoWindowType;
    .param p4    # Lcom/mstar/android/tvapi/common/vo/VideoWindowType;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v0

    invoke-virtual {p2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    invoke-direct {p0, v0, v1, p3, p4}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->native_enable3dDualView(IILcom/mstar/android/tvapi/common/vo/VideoWindowType;Lcom/mstar/android/tvapi/common/vo/VideoWindowType;)Z

    move-result v0

    return v0
.end method

.method public final native enable3dLrSwitch()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final enable3dTo2d(Lcom/mstar/android/tvapi/common/vo/Enum3dType;)Z
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/vo/Enum3dType;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->ordinal()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->native_enable3dTo2d(I)Z

    move-result v0

    return v0
.end method

.method public native enableLow3dQuality()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method protected finalize()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    invoke-direct {p0}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->native_finalize()V

    const/4 v0, 0x0

    sput-object v0, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->_3dManager:Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    return-void
.end method

.method public native generateMvopTiming(III)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final get3dArc()Lcom/mstar/android/tvapi/common/vo/Enum3dAspectRatioType;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->native_get3dArc()I

    move-result v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/Enum3dAspectRatioType;->E_3D_ASPECTRATIO_FULL:Lcom/mstar/android/tvapi/common/vo/Enum3dAspectRatioType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/Enum3dAspectRatioType;->ordinal()I

    move-result v1

    if-lt v0, v1, :cond_0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/Enum3dAspectRatioType;->E_3D_ASPECTRATIO_NUM:Lcom/mstar/android/tvapi/common/vo/Enum3dAspectRatioType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/Enum3dAspectRatioType;->ordinal()I

    move-result v1

    if-le v0, v1, :cond_1

    :cond_0
    new-instance v1, Lcom/mstar/android/tvapi/common/exception/TvCommonException;

    const-string v2, "get3dArc failed"

    invoke-direct {v1, v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/Enum3dAspectRatioType;->values()[Lcom/mstar/android/tvapi/common/vo/Enum3dAspectRatioType;

    move-result-object v1

    aget-object v1, v1, v0

    return-object v1
.end method

.method public final native get3dFormatDetectFlag()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native get3dGain()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native get3dOffset()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final getCurrent3dFormat()Lcom/mstar/android/tvapi/common/vo/Enum3dType;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->native_getCurrent3dFormat()I

    move-result v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_NONE:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->ordinal()I

    move-result v1

    if-lt v0, v1, :cond_0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_TYPE_NUM:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->ordinal()I

    move-result v1

    if-le v0, v1, :cond_1

    :cond_0
    new-instance v1, Lcom/mstar/android/tvapi/common/exception/TvCommonException;

    const-string v2, "getCurrent3dFormat failed"

    invoke-direct {v1, v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->values()[Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    move-result-object v1

    aget-object v1, v1, v0

    return-object v1
.end method

.method public final native getDetect3dFormatParameters()Lcom/mstar/android/tvapi/common/vo/Detect3dFormatParameter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native is3dLrSwitched()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final query3dCapability(Lcom/mstar/android/tvapi/common/vo/Enum3dItemType;)Z
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/vo/Enum3dItemType;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/Enum3dItemType;->ordinal()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->native_query3dCapability(I)Z

    move-result v0

    return v0
.end method

.method protected release()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    const/4 v0, 0x0

    sput-object v0, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->_3dManager:Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    return-void
.end method

.method public final set3dArc(Lcom/mstar/android/tvapi/common/vo/Enum3dAspectRatioType;)Z
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/vo/Enum3dAspectRatioType;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/Enum3dAspectRatioType;->ordinal()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->native_set3dArc(I)Z

    move-result v0

    return v0
.end method

.method public final native set3dFormatDetectFlag(Z)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native set3dGain(I)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native set3dOffset(I)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public native setDebugMode(Z)V
.end method

.method public final native setDetect3dFormatParameters(Lcom/mstar/android/tvapi/common/vo/Detect3dFormatParameter;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public setOnThreeDimensionEventListener(Lcom/mstar/android/tvapi/common/listener/OnThreeDimensionEventListener;)V
    .locals 0
    .param p1    # Lcom/mstar/android/tvapi/common/listener/OnThreeDimensionEventListener;

    iput-object p1, p0, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->mOnEventListener:Lcom/mstar/android/tvapi/common/listener/OnThreeDimensionEventListener;

    return-void
.end method
