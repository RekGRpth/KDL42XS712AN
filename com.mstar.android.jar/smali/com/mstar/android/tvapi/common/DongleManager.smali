.class public final Lcom/mstar/android/tvapi/common/DongleManager;
.super Ljava/lang/Object;
.source "DongleManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mstar/android/tvapi/common/DongleManager$DSThread;
    }
.end annotation


# static fields
.field private static final COMMON_READ_PACKAGE_SIZE_ONE_DONGLE:C = '!'

.field private static final COMMON_READ_PACKAGE_VIRTUAL_SIZE_ONE_DONGLE:C = ' '

.field private static final COMMON_WRITE_PACKAGE_SIZE_ONE_DONGLE:C = '\"'

.field private static final COMMON_WRITE_PACKAGE_VIRTUAL_SIZE_ONE_DONGLE:C = ' '

.field public static final DEVICE_PACKAGE_FROM_DONGLE_TO_TV:I = 0x4

.field public static final DEVICE_PACKAGE_FROM_TV_TO_DONGLE:I = 0x3

.field public static final DONGLE_COMMON_PACKAGE_LABEL:I = 0x6

.field private static final DONGLE_ID:C = '\u0006'

.field public static final DONGLE_UPGRADE_PACKAGE_LABEL:I = 0x0

.field public static final ENTER_DEPTH_STANDBY:I = 0x1

.field public static final ENTER_FORGE_STANDBY:I = 0x3

.field public static final ENTER_SLEEP:I = 0x2

.field public static final INSIDE_DONGLE:I = 0x1

.field public static final INVALID_ACK:I = 0x1

.field private static final KONKA_ACTION_REMOTE_DONGLE_VALID_ACK:Ljava/lang/String; = "konka.dongle.REMOTE_DONGLE_VALID_ACK"

.field private static final MAXNUM_SUPPORT_DONGLE:I = 0x2

.field private static final MAX_READ_PACKAGE_SIZE_ONE_DONGLE:C = '@'

.field private static final MAX_READ_PACKAGE_SIZE_TOTAL_DONGLE:C = 'f'

.field private static final MAX_WRITE_PACKAGE_SIZE_ONE_DONGLE:C = 'A'

.field public static final NOT_ACK:I = 0x0

.field public static final OUTSIDE_DONGLE:I = 0x2

.field public static final PM_PACKAGE_FROM_DONGLE_TO_TV:I = 0x6

.field public static final PM_PACKAGE_FROM_TV_TO_DONGLE:I = 0x5

.field public static final SOFTWARE_PACKAGE_FROM_DONGLE_TO_TV:I = 0x8

.field public static final SOFTWARE_PACKAGE_FROM_TV_TO_DONGLE:I = 0x7

.field private static final TAG:Ljava/lang/String; = "DongleManager"

.field private static final UPGRADE_READ_PACKAGE_SIZE_INSIDE_DONGLE:C = '@'

.field private static final UPGRADE_READ_PACKAGE_VIRTUAL_SIZE_INSIDE_DONGLE:C = '@'

.field private static final UPGRADE_WRITE_PACKAGE_SIZE_INSIDE_DONGLE:C = 'A'

.field private static final UPGRADE_WRITE_PACKAGE_VIRTUAL_SIZE_INSIDE_DONGLE:C = '@'

.field public static final VALID_ACK:I = 0x2

.field public static final VOLUME_PACKAGE_FROM_DONGLE_TO_TV:I = 0x2

.field public static final VOLUME_PACKAGE_FROM_TV_TO_DONGLE:I = 0x1

.field private static bquit:Z

.field private static dsThread:Lcom/mstar/android/tvapi/common/DongleManager$DSThread;

.field private static mDongleManager:Lcom/mstar/android/tvapi/common/DongleManager;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mDongleSupported:Z

.field private rData:[I

.field private wData:[I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v1, 0x0

    sput-object v1, Lcom/mstar/android/tvapi/common/DongleManager;->mDongleManager:Lcom/mstar/android/tvapi/common/DongleManager;

    sput-object v1, Lcom/mstar/android/tvapi/common/DongleManager;->dsThread:Lcom/mstar/android/tvapi/common/DongleManager$DSThread;

    :try_start_0
    const-string v1, "donglemanager_jni"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    sget-object v1, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Cannot load donglemanager_jni library:\n "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/UnsatisfiedLinkError;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    :try_start_0
    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-direct {p0, v1}, Lcom/mstar/android/tvapi/common/DongleManager;->native_setup(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const/16 v1, 0x66

    new-array v1, v1, [I

    iput-object v1, p0, Lcom/mstar/android/tvapi/common/DongleManager;->rData:[I

    const/16 v1, 0x41

    new-array v1, v1, [I

    iput-object v1, p0, Lcom/mstar/android/tvapi/common/DongleManager;->wData:[I

    invoke-direct {p0}, Lcom/mstar/android/tvapi/common/DongleManager;->clearReadBuffer()V

    invoke-direct {p0}, Lcom/mstar/android/tvapi/common/DongleManager;->clearWriteBuffer()V

    iput-object p1, p0, Lcom/mstar/android/tvapi/common/DongleManager;->mContext:Landroid/content/Context;

    return-void

    :catch_0
    move-exception v0

    const-string v1, "DongleManager"

    const-string v2, "dongle manager setup error"

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method static synthetic access$000()Z
    .locals 1

    sget-boolean v0, Lcom/mstar/android/tvapi/common/DongleManager;->bquit:Z

    return v0
.end method

.method static synthetic access$002(Z)Z
    .locals 0
    .param p0    # Z

    sput-boolean p0, Lcom/mstar/android/tvapi/common/DongleManager;->bquit:Z

    return p0
.end method

.method static synthetic access$100(Lcom/mstar/android/tvapi/common/DongleManager;)[I
    .locals 1
    .param p0    # Lcom/mstar/android/tvapi/common/DongleManager;

    iget-object v0, p0, Lcom/mstar/android/tvapi/common/DongleManager;->rData:[I

    return-object v0
.end method

.method static synthetic access$200(Lcom/mstar/android/tvapi/common/DongleManager;[IC)C
    .locals 1
    .param p0    # Lcom/mstar/android/tvapi/common/DongleManager;
    .param p1    # [I
    .param p2    # C
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lcom/mstar/android/tvapi/common/DongleManager;->readDataFromDongle([IC)C

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/mstar/android/tvapi/common/DongleManager;[II)Z
    .locals 1
    .param p0    # Lcom/mstar/android/tvapi/common/DongleManager;
    .param p1    # [I
    .param p2    # I

    invoke-direct {p0, p1, p2}, Lcom/mstar/android/tvapi/common/DongleManager;->processReadData([II)Z

    move-result v0

    return v0
.end method

.method static synthetic access$400(Lcom/mstar/android/tvapi/common/DongleManager;)V
    .locals 0
    .param p0    # Lcom/mstar/android/tvapi/common/DongleManager;

    invoke-direct {p0}, Lcom/mstar/android/tvapi/common/DongleManager;->clearReadBuffer()V

    return-void
.end method

.method private final clearReadBuffer()V
    .locals 3

    const/4 v0, 0x0

    :goto_0
    const/16 v1, 0x66

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/mstar/android/tvapi/common/DongleManager;->rData:[I

    const/4 v2, 0x0

    aput v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private final clearWriteBuffer()V
    .locals 3

    const/4 v0, 0x0

    :goto_0
    const/16 v1, 0x41

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/mstar/android/tvapi/common/DongleManager;->wData:[I

    const/4 v2, 0x0

    aput v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private cookRawData([III)Z
    .locals 17
    .param p1    # [I
    .param p2    # I
    .param p3    # I

    const/4 v13, 0x0

    const/4 v12, 0x0

    const/4 v5, 0x0

    const/4 v9, 0x0

    new-instance v11, Landroid/content/Intent;

    invoke-direct {v11}, Landroid/content/Intent;-><init>()V

    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    const/4 v14, 0x1

    move/from16 v0, p3

    if-ne v0, v14, :cond_0

    const/16 v14, 0x40

    move/from16 v0, p2

    if-eq v0, v14, :cond_0

    const/16 v14, 0x21

    move/from16 v0, p2

    if-eq v0, v14, :cond_0

    const/4 v14, 0x0

    :goto_0
    return v14

    :cond_0
    const/4 v14, 0x2

    move/from16 v0, p3

    if-ne v0, v14, :cond_1

    const/16 v14, 0x21

    move/from16 v0, p2

    if-eq v0, v14, :cond_1

    const/4 v14, 0x0

    goto :goto_0

    :cond_1
    const/16 v14, 0x21

    move/from16 v0, p2

    if-ne v0, v14, :cond_f

    const/4 v14, 0x0

    aget v14, p1, v14

    const/4 v15, 0x6

    if-eq v14, v15, :cond_3

    const-string v14, "DongleManager"

    const-string v15, "read invalid data!!"

    invoke-static {v14, v15}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    :goto_1
    const/4 v14, 0x1

    goto :goto_0

    :cond_3
    const/4 v14, 0x1

    aget v14, p1, v14

    and-int/lit8 v13, v14, 0x1f

    const/4 v14, 0x1

    aget v14, p1, v14

    and-int/lit8 v14, v14, 0x60

    shr-int/lit8 v3, v14, 0x5

    const-string v14, "DongleManager"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "packageType=== "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v14, "DongleManager"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "ackValue=== "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v14, 0x2

    if-eq v13, v14, :cond_2

    const/4 v14, 0x4

    if-ne v13, v14, :cond_a

    const/4 v14, 0x5

    aget v14, p1, v14

    const/4 v15, 0x6

    if-ne v14, v15, :cond_6

    const v7, 0xffff

    const/4 v8, 0x0

    const/4 v4, 0x0

    const-string v14, "DongleManager"

    const-string v15, "connect dongle request! "

    invoke-static {v14, v15}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-nez v3, :cond_4

    const/4 v14, 0x7

    aget v8, p1, v14

    const/16 v14, 0x9

    aget v14, p1, v14

    and-int/lit16 v14, v14, 0xff

    shl-int/lit8 v7, v14, 0x8

    const/16 v14, 0x8

    aget v14, p1, v14

    and-int/lit16 v14, v14, 0xff

    or-int/2addr v7, v14

    const-string v14, "DongleManager"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "deviceId ===="

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v14, "DongleManager"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "deviceType ===="

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v14, "cmdType"

    const-string v15, "request.connect.device"

    invoke-virtual {v6, v14, v15}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v14, "deviceType"

    invoke-virtual {v6, v14, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v14, "deviceId"

    invoke-virtual {v6, v14, v7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v14, "dongleId"

    move/from16 v0, p3

    invoke-virtual {v6, v14, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v14, "com.mstar.android.tvapi.common.DongleManage.request"

    invoke-virtual {v11, v14}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {v11, v6}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    const-string v14, "DongleManager"

    const-string v15, "send connect broadcast "

    invoke-static {v14, v15}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/mstar/android/tvapi/common/DongleManager;->mContext:Landroid/content/Context;

    invoke-virtual {v14, v11}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_1

    :cond_4
    const/4 v2, -0x1

    const/4 v14, 0x6

    aget v14, p1, v14

    if-nez v14, :cond_5

    const/4 v4, 0x0

    :goto_2
    const-string v14, "DongleManager"

    const-string v15, "connect dongle request ack! "

    invoke-static {v14, v15}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v14, 0x7

    aget v2, p1, v14

    const-string v14, "DongleManager"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "ackType ===="

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v14, 0x1

    if-ne v2, v14, :cond_2

    const/16 v14, 0x8

    aget v8, p1, v14

    const/16 v14, 0xa

    aget v14, p1, v14

    and-int/lit16 v14, v14, 0xff

    shl-int/lit8 v7, v14, 0x8

    const/16 v14, 0x9

    aget v14, p1, v14

    and-int/lit16 v14, v14, 0xff

    or-int/2addr v7, v14

    const-string v14, "DongleManager"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "deviceId ===="

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v14, "com.mstar.android.tvapi.common.DongleManage.request"

    invoke-virtual {v11, v14}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v14, "cmdType"

    const-string v15, "ack.agree.or.disagree.connect.device"

    invoke-virtual {v6, v14, v15}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v14, "connectResult"

    invoke-virtual {v6, v14, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v14, "deviceType"

    invoke-virtual {v6, v14, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v14, "deviceId"

    invoke-virtual {v6, v14, v7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v14, "dongleId"

    move/from16 v0, p3

    invoke-virtual {v6, v14, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v11, v6}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    const-string v14, "DongleManager"

    const-string v15, "send connect ack broadcast "

    invoke-static {v14, v15}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/mstar/android/tvapi/common/DongleManager;->mContext:Landroid/content/Context;

    invoke-virtual {v14, v11}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_1

    :cond_5
    const/4 v4, 0x1

    goto/16 :goto_2

    :cond_6
    const/4 v14, 0x5

    aget v14, p1, v14

    const/16 v15, 0x9

    if-ne v14, v15, :cond_9

    const/4 v14, 0x7

    aget v14, p1, v14

    const/4 v15, 0x2

    if-ne v14, v15, :cond_8

    const-string v14, "xieshujia"

    const-string v15, "inflared/2.4 mode switch!"

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v14, 0x2

    if-ne v3, v14, :cond_2

    const/16 v14, 0x20

    new-array v1, v14, [B

    const/4 v10, 0x0

    :goto_3
    const/16 v14, 0x20

    if-ge v10, v14, :cond_7

    add-int/lit8 v14, v10, 0x1

    aget v14, p1, v14

    int-to-byte v14, v14

    aput-byte v14, v1, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_3

    :cond_7
    const-string v14, "konka.dongle.REMOTE_DONGLE_VALID_ACK"

    invoke-virtual {v11, v14}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v14, "DongleValidAck"

    invoke-virtual {v6, v14, v1}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    invoke-virtual {v11, v6}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    const-string v14, "xieshujia"

    const-string v15, "send DongleAck package"

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/mstar/android/tvapi/common/DongleManager;->mContext:Landroid/content/Context;

    invoke-virtual {v14, v11}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_1

    :cond_8
    const-string v14, "xieshujia"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "remote function "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const/16 v16, 0x7

    aget v16, p1, v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :cond_9
    const-string v14, "xieshujia"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "type = "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const/16 v16, 0x5

    aget v16, p1, v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :cond_a
    const/4 v14, 0x3

    if-ne v13, v14, :cond_b

    const/4 v14, 0x5

    aget v14, p1, v14

    const/4 v15, 0x6

    if-ne v14, v15, :cond_2

    const v7, 0xffff

    const/4 v14, 0x7

    aget v14, p1, v14

    goto/16 :goto_1

    :cond_b
    const/4 v14, 0x6

    if-ne v13, v14, :cond_d

    const/4 v14, 0x5

    aget v14, p1, v14

    const/4 v15, 0x4

    if-ne v14, v15, :cond_2

    const-string v14, "xieshujia"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "enter to sleep mode ackValue = "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v14, 0x2

    if-ne v3, v14, :cond_2

    const/16 v14, 0x20

    new-array v1, v14, [B

    const/4 v10, 0x0

    :goto_4
    const/16 v14, 0x20

    if-ge v10, v14, :cond_c

    add-int/lit8 v14, v10, 0x1

    aget v14, p1, v14

    int-to-byte v14, v14

    aput-byte v14, v1, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_4

    :cond_c
    const-string v14, "konka.dongle.REMOTE_DONGLE_VALID_ACK"

    invoke-virtual {v11, v14}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v14, "DongleValidAck"

    invoke-virtual {v6, v14, v1}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    invoke-virtual {v11, v6}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    const-string v14, "xieshujia"

    const-string v15, "send DongleAck package"

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/mstar/android/tvapi/common/DongleManager;->mContext:Landroid/content/Context;

    invoke-virtual {v14, v11}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_1

    :cond_d
    const/16 v14, 0x8

    if-ne v13, v14, :cond_2

    const/4 v14, 0x2

    if-ne v3, v14, :cond_2

    const/16 v14, 0x20

    new-array v1, v14, [B

    const/4 v10, 0x0

    :goto_5
    const/16 v14, 0x20

    if-ge v10, v14, :cond_e

    add-int/lit8 v14, v10, 0x1

    aget v14, p1, v14

    int-to-byte v14, v14

    aput-byte v14, v1, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_5

    :cond_e
    const-string v14, "konka.action.COMMON_DATA_FROM_DONGLE_IS_READY"

    invoke-virtual {v11, v14}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v14, "commonPackage"

    invoke-virtual {v6, v14, v1}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    invoke-virtual {v11, v6}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    const-string v14, "DongleManager"

    const-string v15, "send common package"

    invoke-static {v14, v15}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/mstar/android/tvapi/common/DongleManager;->mContext:Landroid/content/Context;

    invoke-virtual {v14, v11}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_1

    :cond_f
    const/4 v14, 0x0

    aget v14, p1, v14

    and-int/lit8 v13, v14, 0x1f

    const/4 v14, 0x0

    aget v14, p1, v14

    and-int/lit8 v14, v14, 0x60

    shr-int/lit8 v3, v14, 0x5

    const-string v14, "DongleManager"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "packageType=== "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v14, "DongleManager"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "ackValue=== "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v14, 0x40

    new-array v1, v14, [B

    const/4 v10, 0x0

    :goto_6
    const/16 v14, 0x40

    if-ge v10, v14, :cond_10

    aget v14, p1, v10

    int-to-byte v14, v14

    aput-byte v14, v1, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_6

    :cond_10
    const-string v14, "com.mstar.android.tvapi.common.DongleManage.upgrade.package"

    invoke-virtual {v11, v14}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v14, "upgradePackage"

    invoke-virtual {v6, v14, v1}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    invoke-virtual {v11, v6}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    const-string v14, "DongleManager"

    const-string v15, "send upgrade package"

    invoke-static {v14, v15}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/mstar/android/tvapi/common/DongleManager;->mContext:Landroid/content/Context;

    invoke-virtual {v14, v11}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_1
.end method

.method private getDsThread()Lcom/mstar/android/tvapi/common/DongleManager$DSThread;
    .locals 1

    new-instance v0, Lcom/mstar/android/tvapi/common/DongleManager$DSThread;

    invoke-direct {v0, p0}, Lcom/mstar/android/tvapi/common/DongleManager$DSThread;-><init>(Lcom/mstar/android/tvapi/common/DongleManager;)V

    return-object v0
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/mstar/android/tvapi/common/DongleManager;
    .locals 3
    .param p0    # Landroid/content/Context;

    const-class v1, Lcom/mstar/android/tvapi/common/DongleManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/mstar/android/tvapi/common/DongleManager;->mDongleManager:Lcom/mstar/android/tvapi/common/DongleManager;

    if-nez v0, :cond_0

    new-instance v0, Lcom/mstar/android/tvapi/common/DongleManager;

    invoke-direct {v0, p0}, Lcom/mstar/android/tvapi/common/DongleManager;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/mstar/android/tvapi/common/DongleManager;->mDongleManager:Lcom/mstar/android/tvapi/common/DongleManager;

    const-string v0, "DongleManager"

    const-string v2, "new donglemanagerr"

    invoke-static {v0, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-string v0, "DongleManager"

    const-string v1, "return donglemanagerr"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/mstar/android/tvapi/common/DongleManager;->mDongleManager:Lcom/mstar/android/tvapi/common/DongleManager;

    return-object v0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private final native native_finalize()V
.end method

.method private final native native_readDataFromDongle([IC)C
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_setup(Ljava/lang/Object;)V
.end method

.method private final native native_startUpgradeDongle()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_writeDataToDongle([IC)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private processReadData([II)Z
    .locals 11
    .param p1    # [I
    .param p2    # I

    const/4 v1, 0x0

    const/4 v0, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x40

    new-array v4, v8, [I

    const/16 v8, 0x40

    new-array v5, v8, [I

    if-eqz p1, :cond_0

    if-lez p2, :cond_0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, p2, :cond_0

    const-string v8, "DongleManager"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "read date ["

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "] === "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    aget v10, p1, v3

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    const/4 v8, 0x0

    aget v1, p1, v8

    const/4 v8, 0x1

    if-ne v1, v8, :cond_5

    const/4 v8, 0x1

    aget v0, p1, v8

    const/4 v8, 0x1

    if-ne v0, v8, :cond_2

    const/4 v8, 0x2

    aget v6, p1, v8

    const/16 v8, 0x40

    if-eq v6, v8, :cond_1

    const/16 v8, 0x21

    if-eq v6, v8, :cond_1

    const/4 v8, 0x0

    :goto_1
    return v8

    :cond_1
    const/4 v2, 0x0

    :goto_2
    if-ge v2, v6, :cond_c

    add-int/lit8 v8, v2, 0x3

    aget v8, p1, v8

    aput v8, v4, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_2
    const/4 v8, 0x2

    if-ne v0, v8, :cond_4

    const/4 v8, 0x2

    aget v7, p1, v8

    const/16 v8, 0x21

    if-eq v6, v8, :cond_3

    const/4 v8, 0x0

    goto :goto_1

    :cond_3
    const/4 v2, 0x0

    :goto_3
    if-ge v2, v7, :cond_c

    add-int/lit8 v8, v2, 0x3

    aget v8, p1, v8

    aput v8, v5, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_4
    const/4 v8, 0x0

    goto :goto_1

    :cond_5
    const/4 v8, 0x2

    if-ne v1, v8, :cond_b

    const/4 v8, 0x1

    aget v0, p1, v8

    const/4 v8, 0x1

    if-eq v0, v8, :cond_6

    const/4 v8, 0x0

    goto :goto_1

    :cond_6
    const/4 v8, 0x2

    aget v6, p1, v8

    const/16 v8, 0x40

    if-eq v6, v8, :cond_7

    const/16 v8, 0x21

    if-eq v6, v8, :cond_7

    const/4 v8, 0x0

    goto :goto_1

    :cond_7
    const/4 v2, 0x0

    :goto_4
    if-ge v2, v6, :cond_8

    add-int/lit8 v8, v2, 0x3

    aget v8, p1, v8

    aput v8, v4, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    :cond_8
    add-int/lit8 v8, v6, 0x2

    aget v0, p1, v8

    const/4 v8, 0x2

    if-eq v0, v8, :cond_9

    const/4 v8, 0x0

    goto :goto_1

    :cond_9
    add-int/lit8 v8, v6, 0x3

    aget v7, p1, v8

    const/16 v8, 0x21

    if-eq v6, v8, :cond_a

    const/4 v8, 0x0

    goto :goto_1

    :cond_a
    const/4 v2, 0x0

    :goto_5
    if-ge v2, v7, :cond_c

    add-int/lit8 v8, v6, 0x4

    add-int/2addr v8, v2

    aget v8, p1, v8

    aput v8, v5, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    :cond_b
    const/4 v8, 0x0

    goto :goto_1

    :cond_c
    const/4 v8, 0x1

    invoke-direct {p0, v4, v6, v8}, Lcom/mstar/android/tvapi/common/DongleManager;->cookRawData([III)Z

    const/4 v8, 0x2

    invoke-direct {p0, v5, v7, v8}, Lcom/mstar/android/tvapi/common/DongleManager;->cookRawData([III)Z

    const/4 v8, 0x0

    goto :goto_1
.end method

.method private final readDataFromDongle([IC)C
    .locals 1
    .param p1    # [I
    .param p2    # C
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    if-nez p2, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/mstar/android/tvapi/common/DongleManager;->native_readDataFromDongle([IC)C

    move-result v0

    goto :goto_0
.end method

.method private final startUpgradeDongle()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/mstar/android/tvapi/common/DongleManager;->native_startUpgradeDongle()Z

    move-result v0

    return v0
.end method

.method private final writeDataToDongle([IC)Z
    .locals 1
    .param p1    # [I
    .param p2    # C
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/mstar/android/tvapi/common/DongleManager;->native_writeDataToDongle([IC)Z

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public dM_SendUpgradePackage([B)Z
    .locals 10
    .param p1    # [B

    const/16 v9, 0x40

    const/4 v8, 0x1

    const/4 v4, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const-string v5, "DongleManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "packageArray.length === %d"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    array-length v7, p1

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    array-length v5, p1

    if-eq v5, v9, :cond_0

    array-length v5, p1

    const/16 v6, 0x20

    if-eq v5, v6, :cond_0

    :goto_0
    return v4

    :cond_0
    invoke-direct {p0}, Lcom/mstar/android/tvapi/common/DongleManager;->clearWriteBuffer()V

    iget-object v5, p0, Lcom/mstar/android/tvapi/common/DongleManager;->wData:[I

    aput v8, v5, v4

    array-length v4, p1

    if-ne v4, v9, :cond_2

    const/4 v1, 0x0

    :goto_1
    array-length v4, p1

    if-ge v1, v4, :cond_1

    iget-object v4, p0, Lcom/mstar/android/tvapi/common/DongleManager;->wData:[I

    add-int/lit8 v5, v1, 0x1

    aget-byte v6, p1, v1

    aput v6, v4, v5

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    array-length v4, p1

    add-int/lit8 v4, v4, 0x1

    int-to-char v3, v4

    :goto_2
    :try_start_0
    iget-object v4, p0, Lcom/mstar/android/tvapi/common/DongleManager;->wData:[I

    invoke-direct {p0, v4, v3}, Lcom/mstar/android/tvapi/common/DongleManager;->writeDataToDongle([IC)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_3
    move v4, v2

    goto :goto_0

    :cond_2
    iget-object v4, p0, Lcom/mstar/android/tvapi/common/DongleManager;->wData:[I

    const/4 v5, 0x6

    aput v5, v4, v8

    const/4 v1, 0x0

    :goto_4
    array-length v4, p1

    if-ge v1, v4, :cond_3

    iget-object v4, p0, Lcom/mstar/android/tvapi/common/DongleManager;->wData:[I

    add-int/lit8 v5, v1, 0x2

    aget-byte v6, p1, v1

    aput v6, v4, v5

    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_3
    array-length v4, p1

    add-int/lit8 v4, v4, 0x2

    int-to-char v3, v4

    goto :goto_2

    :catch_0
    move-exception v0

    const-string v4, "DongleManager"

    const-string v5, "##### write dongle binder failed"

    invoke-static {v4, v5}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3
.end method

.method public dM_SetPowerOffMode(I)Z
    .locals 10
    .param p1    # I

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x4

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v2, 0x0

    const-string v3, "DongleManager"

    const-string v4, "##### dM_SetPowerOffMode!!"

    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/mstar/android/tvapi/common/DongleManager;->clearWriteBuffer()V

    iget-object v3, p0, Lcom/mstar/android/tvapi/common/DongleManager;->wData:[I

    aput v6, v3, v5

    iget-object v3, p0, Lcom/mstar/android/tvapi/common/DongleManager;->wData:[I

    const/4 v4, 0x6

    aput v4, v3, v6

    if-eq p1, v6, :cond_0

    if-eq p1, v8, :cond_0

    if-ne p1, v9, :cond_1

    :cond_0
    iget-object v3, p0, Lcom/mstar/android/tvapi/common/DongleManager;->wData:[I

    const/4 v4, 0x5

    aput v4, v3, v8

    iget-object v3, p0, Lcom/mstar/android/tvapi/common/DongleManager;->wData:[I

    aput v5, v3, v9

    iget-object v3, p0, Lcom/mstar/android/tvapi/common/DongleManager;->wData:[I

    aput v5, v3, v7

    iget-object v3, p0, Lcom/mstar/android/tvapi/common/DongleManager;->wData:[I

    const/4 v4, 0x5

    aput v5, v3, v4

    iget-object v3, p0, Lcom/mstar/android/tvapi/common/DongleManager;->wData:[I

    const/4 v4, 0x6

    aput v7, v3, v4

    iget-object v3, p0, Lcom/mstar/android/tvapi/common/DongleManager;->wData:[I

    const/4 v4, 0x7

    aput v5, v3, v4

    iget-object v3, p0, Lcom/mstar/android/tvapi/common/DongleManager;->wData:[I

    const/16 v4, 0x8

    aput p1, v3, v4

    const-string v3, "DongleManager"

    const-string v4, "##### enter to sleep mode!!"

    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    :try_start_0
    iget-object v3, p0, Lcom/mstar/android/tvapi/common/DongleManager;->wData:[I

    const/16 v4, 0x22

    invoke-direct {p0, v3, v4}, Lcom/mstar/android/tvapi/common/DongleManager;->writeDataToDongle([IC)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v2

    :goto_1
    const/4 v0, 0x5

    :goto_2
    if-ltz v0, :cond_2

    const-string v3, "DongleManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "count = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    add-int/lit8 v0, v0, -0x1

    const-wide/16 v3, 0x64

    :try_start_1
    invoke-static {v3, v4}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    :cond_1
    iget-object v3, p0, Lcom/mstar/android/tvapi/common/DongleManager;->wData:[I

    const/16 v4, 0x28

    aput v4, v3, v8

    iget-object v3, p0, Lcom/mstar/android/tvapi/common/DongleManager;->wData:[I

    aput v5, v3, v9

    iget-object v3, p0, Lcom/mstar/android/tvapi/common/DongleManager;->wData:[I

    aput v5, v3, v7

    iget-object v3, p0, Lcom/mstar/android/tvapi/common/DongleManager;->wData:[I

    const/4 v4, 0x5

    aput v5, v3, v4

    iget-object v3, p0, Lcom/mstar/android/tvapi/common/DongleManager;->wData:[I

    const/4 v4, 0x6

    aput v7, v3, v4

    iget-object v3, p0, Lcom/mstar/android/tvapi/common/DongleManager;->wData:[I

    const/4 v4, 0x7

    aput v5, v3, v4

    iget-object v3, p0, Lcom/mstar/android/tvapi/common/DongleManager;->wData:[I

    const/16 v4, 0x8

    aput v6, v3, v4

    goto :goto_0

    :catch_1
    move-exception v1

    const-string v3, "DongleManager"

    const-string v4, "##### write dongle binder failed"

    invoke-static {v3, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_2
    return v2
.end method

.method public dM_SetPowerOnTime(J)Z
    .locals 9
    .param p1    # J

    const/16 v8, 0x8

    const/4 v7, 0x6

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/mstar/android/tvapi/common/DongleManager;->clearWriteBuffer()V

    iget-object v2, p0, Lcom/mstar/android/tvapi/common/DongleManager;->wData:[I

    aput v6, v2, v5

    iget-object v2, p0, Lcom/mstar/android/tvapi/common/DongleManager;->wData:[I

    aput v7, v2, v6

    iget-object v2, p0, Lcom/mstar/android/tvapi/common/DongleManager;->wData:[I

    const/4 v3, 0x2

    const/16 v4, 0x28

    aput v4, v2, v3

    iget-object v2, p0, Lcom/mstar/android/tvapi/common/DongleManager;->wData:[I

    const/4 v3, 0x3

    aput v5, v2, v3

    iget-object v2, p0, Lcom/mstar/android/tvapi/common/DongleManager;->wData:[I

    const/4 v3, 0x4

    aput v5, v2, v3

    iget-object v2, p0, Lcom/mstar/android/tvapi/common/DongleManager;->wData:[I

    const/4 v3, 0x5

    aput v5, v2, v3

    iget-object v2, p0, Lcom/mstar/android/tvapi/common/DongleManager;->wData:[I

    aput v6, v2, v7

    iget-object v2, p0, Lcom/mstar/android/tvapi/common/DongleManager;->wData:[I

    const-wide/32 v3, -0x1000000

    and-long/2addr v3, p1

    const/16 v5, 0x18

    shr-long/2addr v3, v5

    long-to-int v3, v3

    aput v3, v2, v8

    iget-object v2, p0, Lcom/mstar/android/tvapi/common/DongleManager;->wData:[I

    const/16 v3, 0x9

    const-wide/32 v4, 0xff0000

    and-long/2addr v4, p1

    const/16 v6, 0x10

    shr-long/2addr v4, v6

    long-to-int v4, v4

    aput v4, v2, v3

    iget-object v2, p0, Lcom/mstar/android/tvapi/common/DongleManager;->wData:[I

    const/16 v3, 0xa

    const-wide/32 v4, 0xff00

    and-long/2addr v4, p1

    shr-long/2addr v4, v8

    long-to-int v4, v4

    aput v4, v2, v3

    iget-object v2, p0, Lcom/mstar/android/tvapi/common/DongleManager;->wData:[I

    const/16 v3, 0xb

    const-wide/16 v4, 0xff

    and-long/2addr v4, p1

    long-to-int v4, v4

    aput v4, v2, v3

    :try_start_0
    iget-object v2, p0, Lcom/mstar/android/tvapi/common/DongleManager;->wData:[I

    const/16 v3, 0x22

    invoke-direct {p0, v2, v3}, Lcom/mstar/android/tvapi/common/DongleManager;->writeDataToDongle([IC)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    const-string v2, "DongleManager"

    const-string v3, "##### write dongle binder failed"

    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public dM_StartUpgradeDongle()Z
    .locals 4

    const/4 v1, 0x0

    :try_start_0
    invoke-direct {p0}, Lcom/mstar/android/tvapi/common/DongleManager;->startUpgradeDongle()Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    const-string v2, "DongleManager"

    const-string v3, "##### write dongle binder failed"

    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public dM_bAgreeCnDevice(ZIII)Z
    .locals 9
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/16 v8, 0x8

    const/4 v7, 0x3

    const/4 v6, 0x1

    const/4 v5, 0x6

    const/4 v4, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/mstar/android/tvapi/common/DongleManager;->clearWriteBuffer()V

    iget-object v2, p0, Lcom/mstar/android/tvapi/common/DongleManager;->wData:[I

    aput p4, v2, v4

    iget-object v2, p0, Lcom/mstar/android/tvapi/common/DongleManager;->wData:[I

    aput v5, v2, v6

    iget-object v2, p0, Lcom/mstar/android/tvapi/common/DongleManager;->wData:[I

    const/4 v3, 0x2

    aput v7, v2, v3

    iget-object v2, p0, Lcom/mstar/android/tvapi/common/DongleManager;->wData:[I

    aput v4, v2, v7

    iget-object v2, p0, Lcom/mstar/android/tvapi/common/DongleManager;->wData:[I

    const/4 v3, 0x4

    aput v4, v2, v3

    iget-object v2, p0, Lcom/mstar/android/tvapi/common/DongleManager;->wData:[I

    const/4 v3, 0x5

    aput v4, v2, v3

    iget-object v2, p0, Lcom/mstar/android/tvapi/common/DongleManager;->wData:[I

    aput v5, v2, v5

    iget-object v2, p0, Lcom/mstar/android/tvapi/common/DongleManager;->wData:[I

    const/4 v3, 0x7

    aput v4, v2, v3

    if-eqz p1, :cond_0

    iget-object v2, p0, Lcom/mstar/android/tvapi/common/DongleManager;->wData:[I

    aput v6, v2, v8

    :goto_0
    iget-object v2, p0, Lcom/mstar/android/tvapi/common/DongleManager;->wData:[I

    const/16 v3, 0x9

    aput p3, v2, v3

    iget-object v2, p0, Lcom/mstar/android/tvapi/common/DongleManager;->wData:[I

    const/16 v3, 0xb

    const v4, 0xff00

    and-int/2addr v4, p2

    shr-int/lit8 v4, v4, 0x8

    aput v4, v2, v3

    iget-object v2, p0, Lcom/mstar/android/tvapi/common/DongleManager;->wData:[I

    const/16 v3, 0xa

    and-int/lit16 v4, p2, 0xff

    aput v4, v2, v3

    :try_start_0
    iget-object v2, p0, Lcom/mstar/android/tvapi/common/DongleManager;->wData:[I

    const/16 v3, 0x22

    invoke-direct {p0, v2, v3}, Lcom/mstar/android/tvapi/common/DongleManager;->writeDataToDongle([IC)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_1
    return v1

    :cond_0
    iget-object v2, p0, Lcom/mstar/android/tvapi/common/DongleManager;->wData:[I

    aput v4, v2, v8

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v2, "DongleManager"

    const-string v3, "##### write dongle binder failed"

    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public dM_switchRemoteCtrMode(I)Z
    .locals 9
    .param p1    # I

    const/4 v8, 0x1

    const/16 v7, 0x9

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v1, 0x0

    const-string v2, "DongleManager"

    const-string v3, "##### dM_switchRemoteCtrMode!!"

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/mstar/android/tvapi/common/DongleManager;->clearWriteBuffer()V

    iget-object v2, p0, Lcom/mstar/android/tvapi/common/DongleManager;->wData:[I

    aput v8, v2, v4

    iget-object v2, p0, Lcom/mstar/android/tvapi/common/DongleManager;->wData:[I

    const/4 v3, 0x6

    aput v3, v2, v8

    if-nez p1, :cond_0

    iget-object v2, p0, Lcom/mstar/android/tvapi/common/DongleManager;->wData:[I

    aput v6, v2, v5

    iget-object v2, p0, Lcom/mstar/android/tvapi/common/DongleManager;->wData:[I

    aput v4, v2, v6

    iget-object v2, p0, Lcom/mstar/android/tvapi/common/DongleManager;->wData:[I

    const/4 v3, 0x4

    aput v4, v2, v3

    iget-object v2, p0, Lcom/mstar/android/tvapi/common/DongleManager;->wData:[I

    const/4 v3, 0x5

    aput v4, v2, v3

    iget-object v2, p0, Lcom/mstar/android/tvapi/common/DongleManager;->wData:[I

    const/4 v3, 0x6

    aput v7, v2, v3

    iget-object v2, p0, Lcom/mstar/android/tvapi/common/DongleManager;->wData:[I

    const/4 v3, 0x7

    aput v4, v2, v3

    iget-object v2, p0, Lcom/mstar/android/tvapi/common/DongleManager;->wData:[I

    const/16 v3, 0x8

    aput v5, v2, v3

    iget-object v2, p0, Lcom/mstar/android/tvapi/common/DongleManager;->wData:[I

    aput v8, v2, v7

    const-string v2, "DongleManager"

    const-string v3, "##### 2.4G mode!!"

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    :try_start_0
    iget-object v2, p0, Lcom/mstar/android/tvapi/common/DongleManager;->wData:[I

    const/16 v3, 0x22

    invoke-direct {p0, v2, v3}, Lcom/mstar/android/tvapi/common/DongleManager;->writeDataToDongle([IC)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_1
    return v1

    :cond_0
    iget-object v2, p0, Lcom/mstar/android/tvapi/common/DongleManager;->wData:[I

    aput v6, v2, v5

    iget-object v2, p0, Lcom/mstar/android/tvapi/common/DongleManager;->wData:[I

    aput v4, v2, v6

    iget-object v2, p0, Lcom/mstar/android/tvapi/common/DongleManager;->wData:[I

    const/4 v3, 0x4

    aput v4, v2, v3

    iget-object v2, p0, Lcom/mstar/android/tvapi/common/DongleManager;->wData:[I

    const/4 v3, 0x5

    aput v4, v2, v3

    iget-object v2, p0, Lcom/mstar/android/tvapi/common/DongleManager;->wData:[I

    const/4 v3, 0x6

    aput v7, v2, v3

    iget-object v2, p0, Lcom/mstar/android/tvapi/common/DongleManager;->wData:[I

    const/4 v3, 0x7

    aput v4, v2, v3

    iget-object v2, p0, Lcom/mstar/android/tvapi/common/DongleManager;->wData:[I

    const/16 v3, 0x8

    aput v5, v2, v3

    iget-object v2, p0, Lcom/mstar/android/tvapi/common/DongleManager;->wData:[I

    aput v4, v2, v7

    const-string v2, "DongleManager"

    const-string v3, "##### infrared mode!!"

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v2, "DongleManager"

    const-string v3, "##### write dongle binder failed"

    invoke-static {v2, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method protected finalize()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    invoke-direct {p0}, Lcom/mstar/android/tvapi/common/DongleManager;->native_finalize()V

    return-void
.end method

.method public final isDongleSupported()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mstar/android/tvapi/common/DongleManager;->mDongleSupported:Z

    return v0
.end method

.method public startMonitor()Z
    .locals 2

    const-string v0, "DongleManager"

    const-string v1, "dongle monitor!!"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/mstar/android/tvapi/common/DongleManager;->dsThread:Lcom/mstar/android/tvapi/common/DongleManager$DSThread;

    if-nez v0, :cond_0

    sget-object v0, Lcom/mstar/android/tvapi/common/DongleManager;->mDongleManager:Lcom/mstar/android/tvapi/common/DongleManager;

    if-eqz v0, :cond_0

    const-string v0, "DongleManager"

    const-string v1, "start monitor!!"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/mstar/android/tvapi/common/DongleManager;->mDongleManager:Lcom/mstar/android/tvapi/common/DongleManager;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/common/DongleManager;->getDsThread()Lcom/mstar/android/tvapi/common/DongleManager$DSThread;

    move-result-object v0

    sput-object v0, Lcom/mstar/android/tvapi/common/DongleManager;->dsThread:Lcom/mstar/android/tvapi/common/DongleManager$DSThread;

    sget-object v0, Lcom/mstar/android/tvapi/common/DongleManager;->dsThread:Lcom/mstar/android/tvapi/common/DongleManager$DSThread;

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/DongleManager$DSThread;->start()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public stopMonitor()Z
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/common/DongleManager;->dsThread:Lcom/mstar/android/tvapi/common/DongleManager$DSThread;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/mstar/android/tvapi/common/DongleManager;->dsThread:Lcom/mstar/android/tvapi/common/DongleManager$DSThread;

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/DongleManager$DSThread;->quit()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
