.class public final Lcom/mstar/android/tvapi/common/MhlManager;
.super Ljava/lang/Object;
.source "MhlManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mstar/android/tvapi/common/MhlManager$EventHandler;
    }
.end annotation


# static fields
.field private static _mhlManager:Lcom/mstar/android/tvapi/common/MhlManager;


# instance fields
.field private mEventHandler:Lcom/mstar/android/tvapi/common/MhlManager$EventHandler;

.field private mMhlmanagerContext:I

.field private mNativeContext:I

.field private mOnEventListener:Lcom/mstar/android/tvapi/common/listener/OnMhlEventListener;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v1, 0x0

    sput-object v1, Lcom/mstar/android/tvapi/common/MhlManager;->_mhlManager:Lcom/mstar/android/tvapi/common/MhlManager;

    :try_start_0
    const-string v1, "mhlmanager_jni"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    invoke-static {}, Lcom/mstar/android/tvapi/common/MhlManager;->native_init()V
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    sget-object v1, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Cannot load mhlmanager_jni library:\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/UnsatisfiedLinkError;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Lcom/mstar/android/tvapi/common/MhlManager$EventHandler;

    invoke-direct {v1, p0, p0, v0}, Lcom/mstar/android/tvapi/common/MhlManager$EventHandler;-><init>(Lcom/mstar/android/tvapi/common/MhlManager;Lcom/mstar/android/tvapi/common/MhlManager;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/mstar/android/tvapi/common/MhlManager;->mEventHandler:Lcom/mstar/android/tvapi/common/MhlManager$EventHandler;

    :goto_0
    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-direct {p0, v1}, Lcom/mstar/android/tvapi/common/MhlManager;->native_setup(Ljava/lang/Object;)V

    return-void

    :cond_0
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    if-eqz v0, :cond_1

    new-instance v1, Lcom/mstar/android/tvapi/common/MhlManager$EventHandler;

    invoke-direct {v1, p0, p0, v0}, Lcom/mstar/android/tvapi/common/MhlManager$EventHandler;-><init>(Lcom/mstar/android/tvapi/common/MhlManager;Lcom/mstar/android/tvapi/common/MhlManager;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/mstar/android/tvapi/common/MhlManager;->mEventHandler:Lcom/mstar/android/tvapi/common/MhlManager$EventHandler;

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/mstar/android/tvapi/common/MhlManager;->mEventHandler:Lcom/mstar/android/tvapi/common/MhlManager$EventHandler;

    goto :goto_0
.end method

.method private static PostEvent_SnServiceDeadth(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    :try_start_0
    const-class v2, Lcom/mstar/android/tvapi/common/MhlManager;

    monitor-enter v2
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    const-string v1, "mstar.str.suspending"

    const-string v3, "1"

    invoke-static {v1, v3}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x0

    sput-object v1, Lcom/mstar/android/tvapi/common/MhlManager;->_mhlManager:Lcom/mstar/android/tvapi/common/MhlManager;

    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "PostEvent_SnServiceDeadth: set SystemProperties \'mstar.str.suspending\' =1"

    invoke-virtual {v1, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    monitor-exit v2

    :goto_0
    return-void

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v1
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/mstar/android/tvapi/common/MhlManager;)I
    .locals 1
    .param p0    # Lcom/mstar/android/tvapi/common/MhlManager;

    iget v0, p0, Lcom/mstar/android/tvapi/common/MhlManager;->mNativeContext:I

    return v0
.end method

.method protected static getInstance()Lcom/mstar/android/tvapi/common/MhlManager;
    .locals 2

    sget-object v0, Lcom/mstar/android/tvapi/common/MhlManager;->_mhlManager:Lcom/mstar/android/tvapi/common/MhlManager;

    if-nez v0, :cond_1

    const-class v1, Lcom/mstar/android/tvapi/common/MhlManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/mstar/android/tvapi/common/MhlManager;->_mhlManager:Lcom/mstar/android/tvapi/common/MhlManager;

    if-nez v0, :cond_0

    new-instance v0, Lcom/mstar/android/tvapi/common/MhlManager;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/common/MhlManager;-><init>()V

    sput-object v0, Lcom/mstar/android/tvapi/common/MhlManager;->_mhlManager:Lcom/mstar/android/tvapi/common/MhlManager;

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    sget-object v0, Lcom/mstar/android/tvapi/common/MhlManager;->_mhlManager:Lcom/mstar/android/tvapi/common/MhlManager;

    return-object v0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private final native native_finalize()V
.end method

.method private static final native native_init()V
.end method

.method private final native native_setup(Ljava/lang/Object;)V
.end method

.method private static postEventFromNative(Ljava/lang/Object;IIILjava/lang/Object;)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # Ljava/lang/Object;

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/common/MhlManager;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, v1, Lcom/mstar/android/tvapi/common/MhlManager;->mEventHandler:Lcom/mstar/android/tvapi/common/MhlManager$EventHandler;

    if-eqz v2, :cond_1

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/MhlManager;->mEventHandler:Lcom/mstar/android/tvapi/common/MhlManager$EventHandler;

    invoke-virtual {v2, p1, p2, p3, p4}, Lcom/mstar/android/tvapi/common/MhlManager$EventHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/MhlManager;->mEventHandler:Lcom/mstar/android/tvapi/common/MhlManager$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/common/MhlManager$EventHandler;->sendMessage(Landroid/os/Message;)Z

    :cond_1
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "\n NativeCEC callback, postEventFromNative"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public final native CbusStatus()Z
.end method

.method public final native IRKeyProcess(IZ)Z
.end method

.method public final native IsMhlPortInUse()Z
.end method

.method protected finalize()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    invoke-direct {p0}, Lcom/mstar/android/tvapi/common/MhlManager;->native_finalize()V

    const/4 v0, 0x0

    sput-object v0, Lcom/mstar/android/tvapi/common/MhlManager;->_mhlManager:Lcom/mstar/android/tvapi/common/MhlManager;

    return-void
.end method

.method public final native getAutoSwitch()Z
.end method

.method protected release()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    const/4 v0, 0x0

    sput-object v0, Lcom/mstar/android/tvapi/common/MhlManager;->_mhlManager:Lcom/mstar/android/tvapi/common/MhlManager;

    return-void
.end method

.method public final native setAutoSwitch(Z)V
.end method

.method public final native setDebugMode(Z)V
.end method

.method public setOnMhlEventListener(Lcom/mstar/android/tvapi/common/listener/OnMhlEventListener;)V
    .locals 0
    .param p1    # Lcom/mstar/android/tvapi/common/listener/OnMhlEventListener;

    iput-object p1, p0, Lcom/mstar/android/tvapi/common/MhlManager;->mOnEventListener:Lcom/mstar/android/tvapi/common/listener/OnMhlEventListener;

    return-void
.end method
