.class public final Lcom/mstar/android/tvapi/common/PvrManager;
.super Ljava/lang/Object;
.source "PvrManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mstar/android/tvapi/common/PvrManager$1;,
        Lcom/mstar/android/tvapi/common/PvrManager$EventHandler;,
        Lcom/mstar/android/tvapi/common/PvrManager$OnPvrEventListener;,
        Lcom/mstar/android/tvapi/common/PvrManager$EVENT;
    }
.end annotation


# static fields
.field public static final E_FILE_SYSTEM_TYPE_EXFAT:I = 0x7

.field public static final E_FILE_SYSTEM_TYPE_EXT2:I = 0x3

.field public static final E_FILE_SYSTEM_TYPE_EXT3:I = 0x4

.field public static final E_FILE_SYSTEM_TYPE_EXT4:I = 0x8

.field public static final E_FILE_SYSTEM_TYPE_INVALID:I = 0x9

.field public static final E_FILE_SYSTEM_TYPE_JFFS2:I = 0x1

.field public static final E_FILE_SYSTEM_TYPE_MSDOS:I = 0x5

.field public static final E_FILE_SYSTEM_TYPE_NTFS:I = 0x6

.field public static final E_FILE_SYSTEM_TYPE_UNKNOWN:I = 0x0

.field public static final E_FILE_SYSTEM_TYPE_VFAT:I = 0x2

.field public static final PVR_FILE_INFO_SORT_CHANNEL:I = 0x3

.field public static final PVR_FILE_INFO_SORT_FILENAME:I = 0x0

.field public static final PVR_FILE_INFO_SORT_LCN:I = 0x2

.field public static final PVR_FILE_INFO_SORT_MAX_KEY:I = 0x5

.field public static final PVR_FILE_INFO_SORT_PROGRAM:I = 0x4

.field public static final PVR_FILE_INFO_SORT_TIME:I = 0x1

.field private static _pvrManager:Lcom/mstar/android/tvapi/common/PvrManager;


# instance fields
.field private mEventHandler:Lcom/mstar/android/tvapi/common/PvrManager$EventHandler;

.field private mNativeContext:I

.field private mOnEventListener:Lcom/mstar/android/tvapi/common/PvrManager$OnPvrEventListener;

.field private mPvrManagerContext:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v1, 0x0

    sput-object v1, Lcom/mstar/android/tvapi/common/PvrManager;->_pvrManager:Lcom/mstar/android/tvapi/common/PvrManager;

    :try_start_0
    const-string v1, "pvrmanager_jni"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    invoke-static {}, Lcom/mstar/android/tvapi/common/PvrManager;->native_init()V
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    sget-object v1, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Cannot load pvrmanager_jni library:\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/UnsatisfiedLinkError;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Lcom/mstar/android/tvapi/common/PvrManager$EventHandler;

    invoke-direct {v1, p0, p0, v0}, Lcom/mstar/android/tvapi/common/PvrManager$EventHandler;-><init>(Lcom/mstar/android/tvapi/common/PvrManager;Lcom/mstar/android/tvapi/common/PvrManager;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/mstar/android/tvapi/common/PvrManager;->mEventHandler:Lcom/mstar/android/tvapi/common/PvrManager$EventHandler;

    :goto_0
    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-direct {p0, v1}, Lcom/mstar/android/tvapi/common/PvrManager;->native_setup(Ljava/lang/Object;)V

    return-void

    :cond_0
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    if-eqz v0, :cond_1

    new-instance v1, Lcom/mstar/android/tvapi/common/PvrManager$EventHandler;

    invoke-direct {v1, p0, p0, v0}, Lcom/mstar/android/tvapi/common/PvrManager$EventHandler;-><init>(Lcom/mstar/android/tvapi/common/PvrManager;Lcom/mstar/android/tvapi/common/PvrManager;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/mstar/android/tvapi/common/PvrManager;->mEventHandler:Lcom/mstar/android/tvapi/common/PvrManager$EventHandler;

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/mstar/android/tvapi/common/PvrManager;->mEventHandler:Lcom/mstar/android/tvapi/common/PvrManager$EventHandler;

    goto :goto_0
.end method

.method private static PostEvent_FormatFinished(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/common/PvrManager;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, v1, Lcom/mstar/android/tvapi/common/PvrManager;->mEventHandler:Lcom/mstar/android/tvapi/common/PvrManager$EventHandler;

    if-eqz v2, :cond_1

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/PvrManager;->mEventHandler:Lcom/mstar/android/tvapi/common/PvrManager$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/common/PvrManager$EVENT;->EV_PVR_NOTIFY_FORMAT_FINISHED:Lcom/mstar/android/tvapi/common/PvrManager$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/PvrManager$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/common/PvrManager$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/PvrManager;->mEventHandler:Lcom/mstar/android/tvapi/common/PvrManager$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/common/PvrManager$EventHandler;->sendMessage(Landroid/os/Message;)Z

    :cond_1
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "\n Native Pvr callback, PostEvent_FormatFinished"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static PostEvent_PlaybackBegin(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/common/PvrManager;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, v1, Lcom/mstar/android/tvapi/common/PvrManager;->mEventHandler:Lcom/mstar/android/tvapi/common/PvrManager$EventHandler;

    if-eqz v2, :cond_1

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/PvrManager;->mEventHandler:Lcom/mstar/android/tvapi/common/PvrManager$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/common/PvrManager$EVENT;->EV_PVR_NOTIFY_PLAYBACK_BEGIN:Lcom/mstar/android/tvapi/common/PvrManager$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/PvrManager$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/common/PvrManager$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/PvrManager;->mEventHandler:Lcom/mstar/android/tvapi/common/PvrManager$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/common/PvrManager$EventHandler;->sendMessage(Landroid/os/Message;)Z

    :cond_1
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "\n Native Pvr callback, PostEvent_Playbackbegin"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static PostEvent_PlaybackStop(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/common/PvrManager;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, v1, Lcom/mstar/android/tvapi/common/PvrManager;->mEventHandler:Lcom/mstar/android/tvapi/common/PvrManager$EventHandler;

    if-eqz v2, :cond_1

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/PvrManager;->mEventHandler:Lcom/mstar/android/tvapi/common/PvrManager$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/common/PvrManager$EVENT;->EV_PVR_NOTIFY_PLAYBACK_STOP:Lcom/mstar/android/tvapi/common/PvrManager$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/PvrManager$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/common/PvrManager$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/PvrManager;->mEventHandler:Lcom/mstar/android/tvapi/common/PvrManager$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/common/PvrManager$EventHandler;->sendMessage(Landroid/os/Message;)Z

    :cond_1
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "\n Native Pvr callback, PostEvent_PlayBackStop"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static PostEvent_SnServiceDeadth(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    :try_start_0
    const-class v2, Lcom/mstar/android/tvapi/common/PvrManager;

    monitor-enter v2
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    const-string v1, "mstar.str.suspending"

    const-string v3, "1"

    invoke-static {v1, v3}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x0

    sput-object v1, Lcom/mstar/android/tvapi/common/PvrManager;->_pvrManager:Lcom/mstar/android/tvapi/common/PvrManager;

    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "PostEvent_SnServiceDeadth: set SystemProperties \'mstar.str.suspending\' =1"

    invoke-virtual {v1, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    monitor-exit v2

    :goto_0
    return-void

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v1
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method private static PostEvent_UsbInserted(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/common/PvrManager;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, v1, Lcom/mstar/android/tvapi/common/PvrManager;->mEventHandler:Lcom/mstar/android/tvapi/common/PvrManager$EventHandler;

    if-eqz v2, :cond_1

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/PvrManager;->mEventHandler:Lcom/mstar/android/tvapi/common/PvrManager$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/common/PvrManager$EVENT;->EV_PVR_NOTIFY_USB_INSERTED:Lcom/mstar/android/tvapi/common/PvrManager$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/PvrManager$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/common/PvrManager$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/PvrManager;->mEventHandler:Lcom/mstar/android/tvapi/common/PvrManager$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/common/PvrManager$EventHandler;->sendMessage(Landroid/os/Message;)Z

    :cond_1
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "\n Native Pvr callback, PostEvent_UsbInserted"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static PostEvent_UsbRemoved(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/common/PvrManager;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, v1, Lcom/mstar/android/tvapi/common/PvrManager;->mEventHandler:Lcom/mstar/android/tvapi/common/PvrManager$EventHandler;

    if-eqz v2, :cond_1

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/PvrManager;->mEventHandler:Lcom/mstar/android/tvapi/common/PvrManager$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/common/PvrManager$EVENT;->EV_PVR_NOTIFY_USB_REMOVED:Lcom/mstar/android/tvapi/common/PvrManager$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/PvrManager$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/common/PvrManager$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/PvrManager;->mEventHandler:Lcom/mstar/android/tvapi/common/PvrManager$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/common/PvrManager$EventHandler;->sendMessage(Landroid/os/Message;)Z

    :cond_1
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "\n Native Pvr callback, PostEvent_UsbRemoved"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/mstar/android/tvapi/common/PvrManager;)I
    .locals 1
    .param p0    # Lcom/mstar/android/tvapi/common/PvrManager;

    iget v0, p0, Lcom/mstar/android/tvapi/common/PvrManager;->mNativeContext:I

    return v0
.end method

.method static synthetic access$100(Lcom/mstar/android/tvapi/common/PvrManager;)Lcom/mstar/android/tvapi/common/PvrManager$OnPvrEventListener;
    .locals 1
    .param p0    # Lcom/mstar/android/tvapi/common/PvrManager;

    iget-object v0, p0, Lcom/mstar/android/tvapi/common/PvrManager;->mOnEventListener:Lcom/mstar/android/tvapi/common/PvrManager$OnPvrEventListener;

    return-object v0
.end method

.method protected static getInstance()Lcom/mstar/android/tvapi/common/PvrManager;
    .locals 2

    sget-object v0, Lcom/mstar/android/tvapi/common/PvrManager;->_pvrManager:Lcom/mstar/android/tvapi/common/PvrManager;

    if-nez v0, :cond_1

    const-class v1, Lcom/mstar/android/tvapi/common/PvrManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/mstar/android/tvapi/common/PvrManager;->_pvrManager:Lcom/mstar/android/tvapi/common/PvrManager;

    if-nez v0, :cond_0

    new-instance v0, Lcom/mstar/android/tvapi/common/PvrManager;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/common/PvrManager;-><init>()V

    sput-object v0, Lcom/mstar/android/tvapi/common/PvrManager;->_pvrManager:Lcom/mstar/android/tvapi/common/PvrManager;

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    sget-object v0, Lcom/mstar/android/tvapi/common/PvrManager;->_pvrManager:Lcom/mstar/android/tvapi/common/PvrManager;

    return-object v0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private final native native_finalize()V
.end method

.method private final native native_getIsBootByRecord()Z
.end method

.method private final native native_getPlaybackSpeed()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_getUsbDeviceLabel(I)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private static final native native_init()V
.end method

.method private final native native_isSupportISDB()Z
.end method

.method private final native native_isSupportStandBy()Z
.end method

.method private final native native_pauseAlwaysTimeShiftPlayback(Z)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_setIsBootByRecord(Z)V
.end method

.method private final native native_setPlaybackSpeed(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_setPvrRecordStandByOnOff(Z)V
.end method

.method private final native native_setup(Ljava/lang/Object;)V
.end method

.method private final native native_startPlayback(Ljava/lang/String;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_startPlayback(Ljava/lang/String;I)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_startPlayback(Ljava/lang/String;II)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_startRecord()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_startTimeShiftPlayback()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_startTimeShiftRecord()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private static postEventFromNative(Ljava/lang/Object;IIILjava/lang/Object;)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # Ljava/lang/Object;

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/common/PvrManager;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, v1, Lcom/mstar/android/tvapi/common/PvrManager;->mEventHandler:Lcom/mstar/android/tvapi/common/PvrManager$EventHandler;

    if-eqz v2, :cond_1

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/PvrManager;->mEventHandler:Lcom/mstar/android/tvapi/common/PvrManager$EventHandler;

    invoke-virtual {v2, p1, p2, p3, p4}, Lcom/mstar/android/tvapi/common/PvrManager$EventHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/PvrManager;->mEventHandler:Lcom/mstar/android/tvapi/common/PvrManager$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/common/PvrManager$EventHandler;->sendMessage(Landroid/os/Message;)Z

    :cond_1
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "\n Native Pvr callback, postEventFromNative"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public final native assignThumbnailFileInfoHandler(Ljava/lang/String;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native captureThumbnail()Lcom/mstar/android/tvapi/common/vo/CaptureThumbnailResult;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public native changeDevice(S)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native checkUsbSpeed()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native clearMetadata()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native createMetadata(Ljava/lang/String;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native deletefile(ILjava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native doPlaybackFastBackward()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native doPlaybackFastForward()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native doPlaybackJumpBackward()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native doPlaybackJumpForward()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method protected finalize()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    invoke-direct {p0}, Lcom/mstar/android/tvapi/common/PvrManager;->native_finalize()V

    const/4 v0, 0x0

    sput-object v0, Lcom/mstar/android/tvapi/common/PvrManager;->_pvrManager:Lcom/mstar/android/tvapi/common/PvrManager;

    return-void
.end method

.method public final native getCurPlaybackTimeInSecond()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native getCurPlaybackingFileName()Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native getCurRecordTimeInSecond()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native getCurRecordingFileName()Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native getEstimateRecordRemainingTime()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native getFileEventName(Ljava/lang/String;)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native getFileLcn(I)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native getFileServiceName(Ljava/lang/String;)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public getIsBootByRecord()Z
    .locals 1

    invoke-direct {p0}, Lcom/mstar/android/tvapi/common/PvrManager;->native_getIsBootByRecord()Z

    move-result v0

    return v0
.end method

.method public final native getMetadataSortKey()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final getPlaybackSpeed()Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/mstar/android/tvapi/common/PvrManager;->native_getPlaybackSpeed()I

    move-result v1

    invoke-static {v1}, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->getOrdinalThroughValue(I)I

    move-result v0

    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->values()[Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    move-result-object v2

    aget-object v2, v2, v0

    return-object v2

    :cond_0
    new-instance v2, Lcom/mstar/android/tvapi/common/exception/TvCommonException;

    const-string v3, "get playbackspeed failed \n"

    invoke-direct {v2, v3}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public final native getPvrFileInfo(II)Lcom/mstar/android/tvapi/common/vo/PvrFileInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native getPvrFileNumber()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native getPvrMountPath()Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native getRecordedFileDurationTime(Ljava/lang/String;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native getThumbnailDisplay(I)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native getThumbnailNumber()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native getThumbnailPath(I)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native getThumbnailTimeStamp(I)[I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native getUsbDeviceIndex()S
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final getUsbDeviceLabel(I)Lcom/mstar/android/tvapi/common/vo/PvrUsbDeviceLabel$EnumPvrUsbDeviceLabel;
    .locals 4
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/mstar/android/tvapi/common/PvrManager;->native_getUsbDeviceLabel(I)I

    move-result v1

    invoke-static {v1}, Lcom/mstar/android/tvapi/common/vo/PvrUsbDeviceLabel$EnumPvrUsbDeviceLabel;->getOrdinalThroughValue(I)I

    move-result v0

    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/PvrUsbDeviceLabel$EnumPvrUsbDeviceLabel;->values()[Lcom/mstar/android/tvapi/common/vo/PvrUsbDeviceLabel$EnumPvrUsbDeviceLabel;

    move-result-object v2

    aget-object v2, v2, v0

    return-object v2

    :cond_0
    new-instance v2, Lcom/mstar/android/tvapi/common/exception/TvCommonException;

    const-string v3, "getusbdevicelabel failed"

    invoke-direct {v2, v3}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public final native getUsbDeviceNumber()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native getUsbPartitionNumber()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native isAlwaysTimeShift()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native isAlwaysTimeShiftPlaybackPaused()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native isAlwaysTimeShiftRecording()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native isMetadataSortAscending()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native isPlaybackParentalLock()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native isPlaybackPaused()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native isPlaybacking()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native isRecordPaused()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native isRecording()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public isSupportISDB()Z
    .locals 1

    invoke-direct {p0}, Lcom/mstar/android/tvapi/common/PvrManager;->native_isSupportISDB()Z

    move-result v0

    return v0
.end method

.method public isSupportStandBy()Z
    .locals 1

    invoke-direct {p0}, Lcom/mstar/android/tvapi/common/PvrManager;->native_isSupportStandBy()Z

    move-result v0

    return v0
.end method

.method public final native isTimeShiftRecording()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native jumpPlaybackTime(I)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native jumpToThumbnail(I)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final pauseAlwaysTimeShiftPlayback(Z)Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;
    .locals 3
    .param p1    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/mstar/android/tvapi/common/PvrManager;->native_pauseAlwaysTimeShiftPlayback(Z)I

    move-result v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;->E_SUCCESS:Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;->ordinal()I

    move-result v1

    if-lt v0, v1, :cond_0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;->E_NUM:Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;->ordinal()I

    move-result v1

    if-le v0, v1, :cond_1

    :cond_0
    new-instance v1, Lcom/mstar/android/tvapi/common/exception/TvCommonException;

    const-string v2, "pauseAlwaysTimeShiftPlayback failed"

    invoke-direct {v1, v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;->values()[Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    move-result-object v1

    aget-object v1, v1, v0

    return-object v1
.end method

.method public final native pauseAlwaysTimeShiftRecord()S
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native pausePlayback()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native pauseRecord()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method protected release()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    const/4 v0, 0x0

    sput-object v0, Lcom/mstar/android/tvapi/common/PvrManager;->_pvrManager:Lcom/mstar/android/tvapi/common/PvrManager;

    return-void
.end method

.method public final native resumePlayback()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native resumeRecord()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native setAlwaysTimeShift(Z)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public native setDebugMode(Z)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public setIsBootByRecord(Z)V
    .locals 0
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/mstar/android/tvapi/common/PvrManager;->native_setIsBootByRecord(Z)V

    return-void
.end method

.method public final native setMetadataSortAscending(Z)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native setMetadataSortKey(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public setOnPvrEventListener(Lcom/mstar/android/tvapi/common/PvrManager$OnPvrEventListener;)V
    .locals 0
    .param p1    # Lcom/mstar/android/tvapi/common/PvrManager$OnPvrEventListener;

    iput-object p1, p0, Lcom/mstar/android/tvapi/common/PvrManager;->mOnEventListener:Lcom/mstar/android/tvapi/common/PvrManager$OnPvrEventListener;

    return-void
.end method

.method public final setPlaybackSpeed(Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;)V
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->getValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/mstar/android/tvapi/common/PvrManager;->native_setPlaybackSpeed(I)V

    return-void
.end method

.method public final native setPlaybackWindow(Lcom/mstar/android/tvapi/common/vo/VideoWindowType;II)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native setPvrParams(Ljava/lang/String;S)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public setPvrRecordStandByOnOff(Z)V
    .locals 0
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/mstar/android/tvapi/common/PvrManager;->native_setPvrRecordStandByOnOff(Z)V

    return-void
.end method

.method public final native setRecordAll(Z)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native setTimeShiftFileSize(J)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native startAlwaysTimeShiftPlayback()S
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native startAlwaysTimeShiftRecord()S
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public startPlayback(Ljava/lang/String;)Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;
    .locals 3
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/mstar/android/tvapi/common/PvrManager;->native_startPlayback(Ljava/lang/String;)I

    move-result v0

    if-ltz v0, :cond_0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;->E_NUM:Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;->ordinal()I

    move-result v1

    if-le v0, v1, :cond_1

    :cond_0
    new-instance v1, Lcom/mstar/android/tvapi/common/exception/TvCommonException;

    const-string v2, "startPlayback failed"

    invoke-direct {v1, v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;->values()[Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    move-result-object v1

    aget-object v1, v1, v0

    return-object v1
.end method

.method public final startPlayback(Ljava/lang/String;I)Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lcom/mstar/android/tvapi/common/PvrManager;->native_startPlayback(Ljava/lang/String;I)I

    move-result v0

    if-ltz v0, :cond_0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;->E_NUM:Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;->ordinal()I

    move-result v1

    if-le v0, v1, :cond_1

    :cond_0
    new-instance v1, Lcom/mstar/android/tvapi/common/exception/TvCommonException;

    const-string v2, "startPlayback failed"

    invoke-direct {v1, v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;->values()[Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    move-result-object v1

    aget-object v1, v1, v0

    return-object v1
.end method

.method public final startPlayback(Ljava/lang/String;II)Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-direct {p0, p1, p2, p3}, Lcom/mstar/android/tvapi/common/PvrManager;->native_startPlayback(Ljava/lang/String;II)I

    move-result v0

    if-ltz v0, :cond_0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;->E_NUM:Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;->ordinal()I

    move-result v1

    if-le v0, v1, :cond_1

    :cond_0
    new-instance v1, Lcom/mstar/android/tvapi/common/exception/TvCommonException;

    const-string v2, "startPlayback failed"

    invoke-direct {v1, v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;->values()[Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    move-result-object v1

    aget-object v1, v1, v0

    return-object v1
.end method

.method public final native startPlaybackLoop(II)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final startRecord()Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/mstar/android/tvapi/common/PvrManager;->native_startRecord()I

    move-result v0

    if-ltz v0, :cond_0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;->E_NUM:Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;->ordinal()I

    move-result v1

    if-le v0, v1, :cond_1

    :cond_0
    new-instance v1, Lcom/mstar/android/tvapi/common/exception/TvCommonException;

    const-string v2, "startRecord failed"

    invoke-direct {v1, v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;->values()[Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    move-result-object v1

    aget-object v1, v1, v0

    return-object v1
.end method

.method public final startTimeShiftPlayback()Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/mstar/android/tvapi/common/PvrManager;->native_startTimeShiftPlayback()I

    move-result v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;->E_SUCCESS:Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;->ordinal()I

    move-result v1

    if-lt v0, v1, :cond_0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;->E_NUM:Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;->ordinal()I

    move-result v1

    if-le v0, v1, :cond_1

    :cond_0
    new-instance v1, Lcom/mstar/android/tvapi/common/exception/TvCommonException;

    const-string v2, "startTimeShiftPlayback failed"

    invoke-direct {v1, v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;->values()[Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    move-result-object v1

    aget-object v1, v1, v0

    return-object v1
.end method

.method public startTimeShiftRecord()Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/mstar/android/tvapi/common/PvrManager;->native_startTimeShiftRecord()I

    move-result v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;->E_SUCCESS:Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;->ordinal()I

    move-result v1

    if-lt v0, v1, :cond_0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;->E_NUM:Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;->ordinal()I

    move-result v1

    if-le v0, v1, :cond_1

    :cond_0
    new-instance v1, Lcom/mstar/android/tvapi/common/exception/TvCommonException;

    const-string v2, "startTimeShiftRecord failed"

    invoke-direct {v1, v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;->values()[Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    move-result-object v1

    aget-object v1, v1, v0

    return-object v1
.end method

.method public final native stepInPlayback()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native stopAlwaysTimeShiftPlayback()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native stopAlwaysTimeShiftRecord()S
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native stopPlayback()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native stopPlaybackLoop()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native stopPvr()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native stopRecord()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native stopTimeShift()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native stopTimeShiftPlayback()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native stopTimeShiftRecord()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method
