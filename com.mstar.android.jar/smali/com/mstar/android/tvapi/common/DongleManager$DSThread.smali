.class Lcom/mstar/android/tvapi/common/DongleManager$DSThread;
.super Ljava/lang/Thread;
.source "DongleManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mstar/android/tvapi/common/DongleManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "DSThread"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mstar/android/tvapi/common/DongleManager;


# direct methods
.method constructor <init>(Lcom/mstar/android/tvapi/common/DongleManager;)V
    .locals 0

    iput-object p1, p0, Lcom/mstar/android/tvapi/common/DongleManager$DSThread;->this$0:Lcom/mstar/android/tvapi/common/DongleManager;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public quit()V
    .locals 1

    const/4 v0, 0x1

    # setter for: Lcom/mstar/android/tvapi/common/DongleManager;->bquit:Z
    invoke-static {v0}, Lcom/mstar/android/tvapi/common/DongleManager;->access$002(Z)Z

    return-void
.end method

.method public run()V
    .locals 6

    const/16 v5, 0x66

    const/4 v2, 0x0

    # setter for: Lcom/mstar/android/tvapi/common/DongleManager;->bquit:Z
    invoke-static {v2}, Lcom/mstar/android/tvapi/common/DongleManager;->access$002(Z)Z

    :goto_0
    # getter for: Lcom/mstar/android/tvapi/common/DongleManager;->bquit:Z
    invoke-static {}, Lcom/mstar/android/tvapi/common/DongleManager;->access$000()Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v1, 0x0

    :try_start_0
    iget-object v2, p0, Lcom/mstar/android/tvapi/common/DongleManager$DSThread;->this$0:Lcom/mstar/android/tvapi/common/DongleManager;

    iget-object v3, p0, Lcom/mstar/android/tvapi/common/DongleManager$DSThread;->this$0:Lcom/mstar/android/tvapi/common/DongleManager;

    # getter for: Lcom/mstar/android/tvapi/common/DongleManager;->rData:[I
    invoke-static {v3}, Lcom/mstar/android/tvapi/common/DongleManager;->access$100(Lcom/mstar/android/tvapi/common/DongleManager;)[I

    move-result-object v3

    const/16 v4, 0x66

    # invokes: Lcom/mstar/android/tvapi/common/DongleManager;->readDataFromDongle([IC)C
    invoke-static {v2, v3, v4}, Lcom/mstar/android/tvapi/common/DongleManager;->access$200(Lcom/mstar/android/tvapi/common/DongleManager;[IC)C
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_1
    if-lez v1, :cond_0

    if-gt v1, v5, :cond_0

    iget-object v2, p0, Lcom/mstar/android/tvapi/common/DongleManager$DSThread;->this$0:Lcom/mstar/android/tvapi/common/DongleManager;

    iget-object v3, p0, Lcom/mstar/android/tvapi/common/DongleManager$DSThread;->this$0:Lcom/mstar/android/tvapi/common/DongleManager;

    # getter for: Lcom/mstar/android/tvapi/common/DongleManager;->rData:[I
    invoke-static {v3}, Lcom/mstar/android/tvapi/common/DongleManager;->access$100(Lcom/mstar/android/tvapi/common/DongleManager;)[I

    move-result-object v3

    # invokes: Lcom/mstar/android/tvapi/common/DongleManager;->processReadData([II)Z
    invoke-static {v2, v3, v1}, Lcom/mstar/android/tvapi/common/DongleManager;->access$300(Lcom/mstar/android/tvapi/common/DongleManager;[II)Z

    :goto_2
    iget-object v2, p0, Lcom/mstar/android/tvapi/common/DongleManager$DSThread;->this$0:Lcom/mstar/android/tvapi/common/DongleManager;

    # invokes: Lcom/mstar/android/tvapi/common/DongleManager;->clearReadBuffer()V
    invoke-static {v2}, Lcom/mstar/android/tvapi/common/DongleManager;->access$400(Lcom/mstar/android/tvapi/common/DongleManager;)V

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v2, "DongleManager"

    const-string v3, "##### read dongle jni failed"

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_0
    const-string v2, "DongleManager"

    const-string v3, "##### read dongle binder error"

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :cond_1
    return-void
.end method
