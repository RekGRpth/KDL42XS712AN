.class public final Lcom/mstar/android/tvapi/common/AudioManager;
.super Ljava/lang/Object;
.source "AudioManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mstar/android/tvapi/common/AudioManager$1;,
        Lcom/mstar/android/tvapi/common/AudioManager$EventHandler;,
        Lcom/mstar/android/tvapi/common/AudioManager$EVENT;
    }
.end annotation


# static fields
.field public static final E_ATVPLAYER_AUTO_TUNING_RECEIVE_EVENT_INTERVAL:I = 0xc3500

.field private static _audioManager:Lcom/mstar/android/tvapi/common/AudioManager;


# instance fields
.field private mAudioManagerContext:I

.field private mEventHandler:Lcom/mstar/android/tvapi/common/AudioManager$EventHandler;

.field private mNativeContext:I

.field private mOnEventListener:Lcom/mstar/android/tvapi/common/listener/OnAudioEventListener;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v1, 0x0

    sput-object v1, Lcom/mstar/android/tvapi/common/AudioManager;->_audioManager:Lcom/mstar/android/tvapi/common/AudioManager;

    :try_start_0
    const-string v1, "audiomanager_jni"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    invoke-static {}, Lcom/mstar/android/tvapi/common/AudioManager;->native_init()V
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    sget-object v1, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Cannot load audiomanager_jni library:\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/UnsatisfiedLinkError;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Lcom/mstar/android/tvapi/common/AudioManager$EventHandler;

    invoke-direct {v1, p0, p0, v0}, Lcom/mstar/android/tvapi/common/AudioManager$EventHandler;-><init>(Lcom/mstar/android/tvapi/common/AudioManager;Lcom/mstar/android/tvapi/common/AudioManager;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/mstar/android/tvapi/common/AudioManager;->mEventHandler:Lcom/mstar/android/tvapi/common/AudioManager$EventHandler;

    :goto_0
    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-direct {p0, v1}, Lcom/mstar/android/tvapi/common/AudioManager;->native_setup(Ljava/lang/Object;)V

    return-void

    :cond_0
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    if-eqz v0, :cond_1

    new-instance v1, Lcom/mstar/android/tvapi/common/AudioManager$EventHandler;

    invoke-direct {v1, p0, p0, v0}, Lcom/mstar/android/tvapi/common/AudioManager$EventHandler;-><init>(Lcom/mstar/android/tvapi/common/AudioManager;Lcom/mstar/android/tvapi/common/AudioManager;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/mstar/android/tvapi/common/AudioManager;->mEventHandler:Lcom/mstar/android/tvapi/common/AudioManager$EventHandler;

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/mstar/android/tvapi/common/AudioManager;->mEventHandler:Lcom/mstar/android/tvapi/common/AudioManager$EventHandler;

    goto :goto_0
.end method

.method private static PostEvent_ApSetVolume(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/common/AudioManager;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, v1, Lcom/mstar/android/tvapi/common/AudioManager;->mEventHandler:Lcom/mstar/android/tvapi/common/AudioManager$EventHandler;

    if-eqz v2, :cond_1

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/AudioManager;->mEventHandler:Lcom/mstar/android/tvapi/common/AudioManager$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/common/AudioManager$EVENT;->EV_AP_SETVOLUME_EVENT:Lcom/mstar/android/tvapi/common/AudioManager$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/AudioManager$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/common/AudioManager$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/AudioManager;->mEventHandler:Lcom/mstar/android/tvapi/common/AudioManager$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/common/AudioManager$EventHandler;->sendMessage(Landroid/os/Message;)Z

    :cond_1
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "\n Native Audio callback, PostEvent_ApSetVolume"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static PostEvent_SnServiceDeadth(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    :try_start_0
    const-class v2, Lcom/mstar/android/tvapi/common/AudioManager;

    monitor-enter v2
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    const-string v1, "mstar.str.suspending"

    const-string v3, "1"

    invoke-static {v1, v3}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x0

    sput-object v1, Lcom/mstar/android/tvapi/common/AudioManager;->_audioManager:Lcom/mstar/android/tvapi/common/AudioManager;

    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "PostEvent_SnServiceDeadth: set SystemProperties \'mstar.str.suspending\' =1"

    invoke-virtual {v1, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    monitor-exit v2

    :goto_0
    return-void

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v1
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/mstar/android/tvapi/common/AudioManager;)I
    .locals 1
    .param p0    # Lcom/mstar/android/tvapi/common/AudioManager;

    iget v0, p0, Lcom/mstar/android/tvapi/common/AudioManager;->mNativeContext:I

    return v0
.end method

.method static synthetic access$100(Lcom/mstar/android/tvapi/common/AudioManager;)Lcom/mstar/android/tvapi/common/listener/OnAudioEventListener;
    .locals 1
    .param p0    # Lcom/mstar/android/tvapi/common/AudioManager;

    iget-object v0, p0, Lcom/mstar/android/tvapi/common/AudioManager;->mOnEventListener:Lcom/mstar/android/tvapi/common/listener/OnAudioEventListener;

    return-object v0
.end method

.method protected static getInstance()Lcom/mstar/android/tvapi/common/AudioManager;
    .locals 2

    sget-object v0, Lcom/mstar/android/tvapi/common/AudioManager;->_audioManager:Lcom/mstar/android/tvapi/common/AudioManager;

    if-nez v0, :cond_1

    const-class v1, Lcom/mstar/android/tvapi/common/AudioManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/mstar/android/tvapi/common/AudioManager;->_audioManager:Lcom/mstar/android/tvapi/common/AudioManager;

    if-nez v0, :cond_0

    new-instance v0, Lcom/mstar/android/tvapi/common/AudioManager;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/common/AudioManager;-><init>()V

    sput-object v0, Lcom/mstar/android/tvapi/common/AudioManager;->_audioManager:Lcom/mstar/android/tvapi/common/AudioManager;

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    sget-object v0, Lcom/mstar/android/tvapi/common/AudioManager;->_audioManager:Lcom/mstar/android/tvapi/common/AudioManager;

    return-object v0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private final native native_SetSoundParameter(III)S
.end method

.method private native native_checkAtvSoundSystem()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_disableKtvMixModeMute(I)S
.end method

.method private final native native_disableMute(I)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private native native_enableAdvancedSoundEffect(II)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private native native_enableBasicSoundEffect(IZ)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_enableKtvMixModeMute(I)S
.end method

.method private final native native_enableMute(I)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_finalize()V
.end method

.method private final native native_getAdvancedSoundEffect(I)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private native native_getAtvInfo()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_getAtvMtsMode()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_getAtvSoundMode()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_getAtvSoundSystem()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private native native_getAudioPrimaryLanguage()I
.end method

.method private native native_getAudioSecondaryLanguage()I
.end method

.method private final native native_getAudioVolume(I)B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_getBasicSoundEffect(I)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_getDtvOutputMode()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_getInputLevel(I)S
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_getInputSource()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_getKtvSoundInfo(I)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_getSoundParameter(II)I
.end method

.method private static final native native_init()V
.end method

.method private final native native_isMuteEnabled(I)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private native native_setADAbsoluteVolume(I)V
.end method

.method private native native_setADEnable(Z)V
.end method

.method private final native native_setAdvancedSoundEffect(ILcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private native native_setAmplifierEqualizerByMode(I)V
.end method

.method private native native_setAtvInfo(II)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_setAtvMtsMode(I)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_setAtvSoundSystem(I)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_setAudioCaptureSource(II)S
.end method

.method private native native_setAudioOutput(ILcom/mstar/android/tvapi/common/vo/AudioOutParameter;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private native native_setAudioPrimaryLanguage(I)V
.end method

.method private native native_setAudioSecondaryLanguage(I)V
.end method

.method private native native_setAudioSource(II)I
.end method

.method private final native native_setAudioVolume(IB)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private native native_setAutoHOHEnable(Z)V
.end method

.method private final native native_setBasicSoundEffect(ILcom/mstar/android/tvapi/common/vo/DtvSoundEffect;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private native native_setCommonAudioInfo(III)Z
.end method

.method private final native native_setDigitalOut(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_setDtvOutputMode(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_setInputLevel(IS)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_setInputSource(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_setKtvMixModeVolume(ISS)S
.end method

.method private final native native_setKtvSoundInfo(III)S
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private native native_setKtvSoundTrack(I)I
.end method

.method private final native native_setMuteStatus(II)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_setToNextAtvMtsMode()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_setup(Ljava/lang/Object;)V
.end method

.method private static postEventFromNative(Ljava/lang/Object;IIILjava/lang/Object;)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # Ljava/lang/Object;

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/common/AudioManager;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, v1, Lcom/mstar/android/tvapi/common/AudioManager;->mEventHandler:Lcom/mstar/android/tvapi/common/AudioManager$EventHandler;

    if-eqz v2, :cond_1

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/AudioManager;->mEventHandler:Lcom/mstar/android/tvapi/common/AudioManager$EventHandler;

    invoke-virtual {v2, p1, p2, p3, p4}, Lcom/mstar/android/tvapi/common/AudioManager$EventHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/AudioManager;->mEventHandler:Lcom/mstar/android/tvapi/common/AudioManager$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/common/AudioManager$EventHandler;->sendMessage(Landroid/os/Message;)Z

    :cond_1
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "\n Native Audio callback , postEventFromNative"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public checkAtvSoundSystem()Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/mstar/android/tvapi/common/AudioManager;->native_checkAtvSoundSystem()I

    move-result v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;->E_RETURN_OK:Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;->ordinal()I

    move-result v1

    if-lt v0, v1, :cond_0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;->E_RETURN_UNSUPPORT:Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;->ordinal()I

    move-result v1

    if-le v0, v1, :cond_1

    :cond_0
    new-instance v1, Lcom/mstar/android/tvapi/common/exception/TvCommonException;

    const-string v2, "native_checkAtvSoundSystem failed"

    invoke-direct {v1, v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;->values()[Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;

    move-result-object v1

    aget-object v1, v1, v0

    return-object v1
.end method

.method public final disableKtvMixModeMute(Lcom/mstar/android/tvapi/common/vo/EnumKtvMixVolumeType;)S
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumKtvMixVolumeType;

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumKtvMixVolumeType;->ordinal()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/mstar/android/tvapi/common/AudioManager;->native_disableKtvMixModeMute(I)S

    move-result v0

    return v0
.end method

.method public disableMute(Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;)Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;
    .locals 3
    .param p1    # Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    const/4 v0, -0x1

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;->getValue()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/mstar/android/tvapi/common/AudioManager;->native_disableMute(I)I

    move-result v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;->E_RETURN_OK:Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;->ordinal()I

    move-result v1

    if-lt v0, v1, :cond_0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;->E_RETURN_UNSUPPORT:Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;->ordinal()I

    move-result v1

    if-le v0, v1, :cond_1

    :cond_0
    new-instance v1, Lcom/mstar/android/tvapi/common/exception/TvCommonException;

    const-string v2, "native_disableMute failed"

    invoke-direct {v1, v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;->values()[Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;

    move-result-object v1

    aget-object v1, v1, v0

    return-object v1
.end method

.method public enableAdvancedSoundEffect(Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;)Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;
    .locals 3
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;
    .param p2    # Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundType;->ordinal()I

    move-result v1

    invoke-virtual {p2}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundSubProcessType;->ordinal()I

    move-result v2

    invoke-direct {p0, v1, v2}, Lcom/mstar/android/tvapi/common/AudioManager;->native_enableAdvancedSoundEffect(II)I

    move-result v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;->E_RETURN_OK:Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;->ordinal()I

    move-result v1

    if-lt v0, v1, :cond_0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;->E_RETURN_UNSUPPORT:Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;->ordinal()I

    move-result v1

    if-le v0, v1, :cond_1

    :cond_0
    new-instance v1, Lcom/mstar/android/tvapi/common/exception/TvCommonException;

    const-string v2, "native_enableAdvancedSoundEffect failed"

    invoke-direct {v1, v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;->values()[Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;

    move-result-object v1

    aget-object v1, v1, v0

    return-object v1
.end method

.method public enableBasicSoundEffect(Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;Z)Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;
    .locals 3
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;
    .param p2    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;->ordinal()I

    move-result v1

    invoke-direct {p0, v1, p2}, Lcom/mstar/android/tvapi/common/AudioManager;->native_enableBasicSoundEffect(IZ)I

    move-result v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;->E_RETURN_OK:Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;->ordinal()I

    move-result v1

    if-lt v0, v1, :cond_0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;->E_RETURN_UNSUPPORT:Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;->ordinal()I

    move-result v1

    if-le v0, v1, :cond_1

    :cond_0
    new-instance v1, Lcom/mstar/android/tvapi/common/exception/TvCommonException;

    const-string v2, "native_checkAtvSoundSystem failed"

    invoke-direct {v1, v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;->values()[Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;

    move-result-object v1

    aget-object v1, v1, v0

    return-object v1
.end method

.method public final enableKtvMixModeMute(Lcom/mstar/android/tvapi/common/vo/EnumKtvMixVolumeType;)S
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumKtvMixVolumeType;

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumKtvMixVolumeType;->ordinal()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/mstar/android/tvapi/common/AudioManager;->native_enableKtvMixModeMute(I)S

    move-result v0

    return v0
.end method

.method public enableMute(Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;)Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;
    .locals 3
    .param p1    # Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    const/4 v0, -0x1

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/MuteType$EnumMuteType;->getValue()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/mstar/android/tvapi/common/AudioManager;->native_enableMute(I)I

    move-result v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;->E_RETURN_OK:Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;->ordinal()I

    move-result v1

    if-lt v0, v1, :cond_0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;->E_RETURN_UNSUPPORT:Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;->ordinal()I

    move-result v1

    if-le v0, v1, :cond_1

    :cond_0
    new-instance v1, Lcom/mstar/android/tvapi/common/exception/TvCommonException;

    const-string v2, "enableMute failed"

    invoke-direct {v1, v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;->values()[Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;

    move-result-object v1

    aget-object v1, v1, v0

    return-object v1
.end method

.method public final native executeAmplifierExtendedCommand(SII[I)S
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method protected finalize()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    invoke-direct {p0}, Lcom/mstar/android/tvapi/common/AudioManager;->native_finalize()V

    const/4 v0, 0x0

    sput-object v0, Lcom/mstar/android/tvapi/common/AudioManager;->_audioManager:Lcom/mstar/android/tvapi/common/AudioManager;

    return-void
.end method

.method public final getAdvancedSoundEffect(Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;)I
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->ordinal()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/mstar/android/tvapi/common/AudioManager;->native_getAdvancedSoundEffect(I)I

    move-result v0

    return v0
.end method

.method public getAtvInfo()Lcom/mstar/android/tvapi/common/vo/EnumAtvInfoType;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/mstar/android/tvapi/common/AudioManager;->native_getAtvInfo()I

    move-result v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAtvInfoType;->E_ATV_HIDEV_INFO:Lcom/mstar/android/tvapi/common/vo/EnumAtvInfoType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAtvInfoType;->ordinal()I

    move-result v1

    if-lt v0, v1, :cond_0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAtvInfoType;->E_ATV_HIDEV_INFO:Lcom/mstar/android/tvapi/common/vo/EnumAtvInfoType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAtvInfoType;->ordinal()I

    move-result v1

    if-le v0, v1, :cond_1

    :cond_0
    new-instance v1, Lcom/mstar/android/tvapi/common/exception/TvCommonException;

    const-string v2, "native_getAtvInfo failed"

    invoke-direct {v1, v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumAtvInfoType;->values()[Lcom/mstar/android/tvapi/common/vo/EnumAtvInfoType;

    move-result-object v1

    aget-object v1, v1, v0

    return-object v1
.end method

.method public getAtvMtsMode()Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/mstar/android/tvapi/common/AudioManager;->native_getAtvMtsMode()I

    move-result v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->E_ATV_AUDIOMODE_INVALID:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->ordinal()I

    move-result v1

    if-lt v0, v1, :cond_0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->E_ATV_AUDIOMODE_NUM:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->ordinal()I

    move-result v1

    if-le v0, v1, :cond_1

    :cond_0
    new-instance v1, Lcom/mstar/android/tvapi/common/exception/TvCommonException;

    const-string v2, "native_getAtvMtsMode failed"

    invoke-direct {v1, v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->values()[Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    move-result-object v1

    aget-object v1, v1, v0

    return-object v1
.end method

.method public getAtvSoundMode()Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/mstar/android/tvapi/common/AudioManager;->native_getAtvMtsMode()I

    move-result v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->E_ATV_AUDIOMODE_INVALID:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->ordinal()I

    move-result v1

    if-lt v0, v1, :cond_0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->E_ATV_AUDIOMODE_NUM:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->ordinal()I

    move-result v1

    if-le v0, v1, :cond_1

    :cond_0
    new-instance v1, Lcom/mstar/android/tvapi/common/exception/TvCommonException;

    const-string v2, "native_getAtvMtsMode failed"

    invoke-direct {v1, v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->values()[Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    move-result-object v1

    aget-object v1, v1, v0

    return-object v1
.end method

.method public getAtvSoundSystem()Lcom/mstar/android/tvapi/common/vo/AtvSystemStandard$EnumAtvSystemStandard;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/mstar/android/tvapi/common/AudioManager;->native_getAtvSoundSystem()I

    move-result v0

    invoke-static {v0}, Lcom/mstar/android/tvapi/common/vo/AtvSystemStandard$EnumAtvSystemStandard;->getOrdinalThroughValue(I)I

    move-result v1

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/AtvSystemStandard$EnumAtvSystemStandard;->E_BG:Lcom/mstar/android/tvapi/common/vo/AtvSystemStandard$EnumAtvSystemStandard;

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/vo/AtvSystemStandard$EnumAtvSystemStandard;->getValue()I

    move-result v2

    if-lt v1, v2, :cond_0

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/AtvSystemStandard$EnumAtvSystemStandard;->E_NUM:Lcom/mstar/android/tvapi/common/vo/AtvSystemStandard$EnumAtvSystemStandard;

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/vo/AtvSystemStandard$EnumAtvSystemStandard;->getValue()I

    move-result v2

    if-le v1, v2, :cond_1

    :cond_0
    new-instance v2, Lcom/mstar/android/tvapi/common/exception/TvCommonException;

    const-string v3, "getAtvSoundSystem failed"

    invoke-direct {v2, v3}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/AtvSystemStandard$EnumAtvSystemStandard;->values()[Lcom/mstar/android/tvapi/common/vo/AtvSystemStandard$EnumAtvSystemStandard;

    move-result-object v2

    aget-object v2, v2, v1

    return-object v2
.end method

.method public getAudioPrimaryLanguage()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;
    .locals 3

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    move-result-object v1

    invoke-direct {p0}, Lcom/mstar/android/tvapi/common/AudioManager;->native_getAudioPrimaryLanguage()I

    move-result v2

    aget-object v1, v1, v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ENGLISH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    goto :goto_0
.end method

.method public getAudioSecondaryLanguage()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;
    .locals 3

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    move-result-object v1

    invoke-direct {p0}, Lcom/mstar/android/tvapi/common/AudioManager;->native_getAudioSecondaryLanguage()I

    move-result v2

    aget-object v1, v1, v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->E_ENGLISH:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    goto :goto_0
.end method

.method public final getAudioVolume(Lcom/mstar/android/tvapi/common/vo/EnumAudioVolumeSourceType;)B
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumAudioVolumeSourceType;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumAudioVolumeSourceType;->ordinal()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/mstar/android/tvapi/common/AudioManager;->native_getAudioVolume(I)B

    move-result v0

    return v0
.end method

.method public final native getAutoVolume()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final getBasicSoundEffect(Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;)I
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumSoundGetParameterType;->ordinal()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/mstar/android/tvapi/common/AudioManager;->native_getBasicSoundEffect(I)I

    move-result v0

    return v0
.end method

.method public getDtvOutputMode()Lcom/mstar/android/tvapi/common/vo/EnumDtvSoundMode;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/mstar/android/tvapi/common/AudioManager;->native_getDtvOutputMode()I

    move-result v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumDtvSoundMode;->E_STEREO:Lcom/mstar/android/tvapi/common/vo/EnumDtvSoundMode;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumDtvSoundMode;->ordinal()I

    move-result v1

    if-lt v0, v1, :cond_0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumDtvSoundMode;->E_NUM:Lcom/mstar/android/tvapi/common/vo/EnumDtvSoundMode;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumDtvSoundMode;->ordinal()I

    move-result v1

    if-le v0, v1, :cond_1

    :cond_0
    new-instance v1, Lcom/mstar/android/tvapi/common/exception/TvCommonException;

    const-string v2, "getDtvOutputMode failed"

    invoke-direct {v1, v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumDtvSoundMode;->values()[Lcom/mstar/android/tvapi/common/vo/EnumDtvSoundMode;

    move-result-object v1

    aget-object v1, v1, v0

    return-object v1
.end method

.method public getInputLevel(Lcom/mstar/android/tvapi/common/vo/EnumAudioInputLevelSourceType;)S
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumAudioInputLevelSourceType;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumAudioInputLevelSourceType;->ordinal()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/mstar/android/tvapi/common/AudioManager;->native_getInputLevel(I)S

    move-result v0

    return v0
.end method

.method public getInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/mstar/android/tvapi/common/AudioManager;->native_getInputSource()I

    move-result v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_VGA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    if-lt v0, v1, :cond_0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_NONE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    if-le v0, v1, :cond_1

    :cond_0
    new-instance v1, Lcom/mstar/android/tvapi/common/exception/TvCommonException;

    const-string v2, "getInputSource failed"

    invoke-direct {v1, v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v1

    aget-object v1, v1, v0

    return-object v1
.end method

.method public final getKtvSoundInfo(Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;)I
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;->getValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/mstar/android/tvapi/common/AudioManager;->native_getKtvSoundInfo(I)I

    move-result v0

    return v0
.end method

.method public final getSoundParameter(Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;I)I
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;
    .param p2    # I

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;->ordinal()I

    move-result v0

    invoke-direct {p0, v0, p2}, Lcom/mstar/android/tvapi/common/AudioManager;->native_getSoundParameter(II)I

    move-result v0

    return v0
.end method

.method public final isMuteEnabled(Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;)Z
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumMuteStatusType;->ordinal()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/mstar/android/tvapi/common/AudioManager;->native_isMuteEnabled(I)Z

    move-result v0

    return v0
.end method

.method protected release()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    const/4 v0, 0x0

    sput-object v0, Lcom/mstar/android/tvapi/common/AudioManager;->_audioManager:Lcom/mstar/android/tvapi/common/AudioManager;

    return-void
.end method

.method public setADAbsoluteVolume(I)V
    .locals 0
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/mstar/android/tvapi/common/AudioManager;->native_setADAbsoluteVolume(I)V

    return-void
.end method

.method public setADEnable(Z)V
    .locals 0
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/mstar/android/tvapi/common/AudioManager;->native_setADEnable(Z)V

    return-void
.end method

.method public final setAdvancedSoundEffect(Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;)Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;
    .locals 3
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;
    .param p2    # Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumAdvancedSoundParameterType;->ordinal()I

    move-result v1

    invoke-direct {p0, v1, p2}, Lcom/mstar/android/tvapi/common/AudioManager;->native_setAdvancedSoundEffect(ILcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;)I

    move-result v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;->E_RETURN_OK:Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;->ordinal()I

    move-result v1

    if-lt v0, v1, :cond_0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;->E_RETURN_UNSUPPORT:Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;->ordinal()I

    move-result v1

    if-le v0, v1, :cond_1

    :cond_0
    new-instance v1, Lcom/mstar/android/tvapi/common/exception/TvCommonException;

    const-string v2, "native_setAdvancedSoundEffect failed"

    invoke-direct {v1, v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;->values()[Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;

    move-result-object v1

    aget-object v1, v1, v0

    return-object v1
.end method

.method public final setAmplifierEqualizerByMode(Lcom/mstar/android/tvapi/common/vo/EnumEqualizerType;)V
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumEqualizerType;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumEqualizerType;->ordinal()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/mstar/android/tvapi/common/AudioManager;->native_setAmplifierEqualizerByMode(I)V

    return-void
.end method

.method public final native setAmplifierMute(Z)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public setAtvInfo(Lcom/mstar/android/tvapi/common/vo/EnumAtvInfoType;Lcom/mstar/android/tvapi/common/vo/EnumSoundHidevMode;)Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;
    .locals 3
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumAtvInfoType;
    .param p2    # Lcom/mstar/android/tvapi/common/vo/EnumSoundHidevMode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumAtvInfoType;->ordinal()I

    move-result v1

    invoke-virtual {p2}, Lcom/mstar/android/tvapi/common/vo/EnumSoundHidevMode;->ordinal()I

    move-result v2

    invoke-direct {p0, v1, v2}, Lcom/mstar/android/tvapi/common/AudioManager;->native_setAtvInfo(II)I

    move-result v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;->E_RETURN_OK:Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;->ordinal()I

    move-result v1

    if-lt v0, v1, :cond_0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;->E_RETURN_UNSUPPORT:Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;->ordinal()I

    move-result v1

    if-le v0, v1, :cond_1

    :cond_0
    new-instance v1, Lcom/mstar/android/tvapi/common/exception/TvCommonException;

    const-string v2, "native_setAtvInfo failed"

    invoke-direct {v1, v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;->values()[Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;

    move-result-object v1

    aget-object v1, v1, v0

    return-object v1
.end method

.method public setAtvMtsMode(Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;)Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;
    .locals 3
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    const/4 v0, -0x1

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->ordinal()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/mstar/android/tvapi/common/AudioManager;->native_setAtvMtsMode(I)I

    move-result v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;->E_RETURN_OK:Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;->ordinal()I

    move-result v1

    if-lt v0, v1, :cond_0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;->E_RETURN_UNSUPPORT:Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;->ordinal()I

    move-result v1

    if-le v0, v1, :cond_1

    :cond_0
    new-instance v1, Lcom/mstar/android/tvapi/common/exception/TvCommonException;

    const-string v2, "native_SetAtvMtsMode failed"

    invoke-direct {v1, v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;->values()[Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;

    move-result-object v1

    aget-object v1, v1, v0

    return-object v1
.end method

.method public setAtvSoundSystem(Lcom/mstar/android/tvapi/common/vo/AtvSystemStandard$EnumAtvSystemStandard;)Z
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/vo/AtvSystemStandard$EnumAtvSystemStandard;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/AtvSystemStandard$EnumAtvSystemStandard;->getValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/mstar/android/tvapi/common/AudioManager;->native_setAtvSoundSystem(I)Z

    move-result v0

    return v0
.end method

.method public final setAudioCaptureSource(Lcom/mstar/android/tvapi/common/vo/EnumAuidoCaptureDeviceType;Lcom/mstar/android/tvapi/common/vo/EnumAuidoCaptureSource;)Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;
    .locals 3
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumAuidoCaptureDeviceType;
    .param p2    # Lcom/mstar/android/tvapi/common/vo/EnumAuidoCaptureSource;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumAuidoCaptureDeviceType;->ordinal()I

    move-result v1

    invoke-virtual {p2}, Lcom/mstar/android/tvapi/common/vo/EnumAuidoCaptureSource;->ordinal()I

    move-result v2

    invoke-direct {p0, v1, v2}, Lcom/mstar/android/tvapi/common/AudioManager;->native_setAudioCaptureSource(II)S

    move-result v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;->E_RETURN_OK:Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;->ordinal()I

    move-result v1

    if-lt v0, v1, :cond_0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;->E_RETURN_UNSUPPORT:Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;->ordinal()I

    move-result v1

    if-le v0, v1, :cond_1

    :cond_0
    new-instance v1, Lcom/mstar/android/tvapi/common/exception/TvCommonException;

    const-string v2, "native_setAudioCaptureSource  failed"

    invoke-direct {v1, v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;->values()[Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;

    move-result-object v1

    aget-object v1, v1, v0

    return-object v1
.end method

.method public setAudioOutput(Lcom/mstar/android/tvapi/common/vo/EnumAudioOutType;Lcom/mstar/android/tvapi/common/vo/AudioOutParameter;)Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;
    .locals 3
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumAudioOutType;
    .param p2    # Lcom/mstar/android/tvapi/common/vo/AudioOutParameter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    const/4 v0, -0x1

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumAudioOutType;->ordinal()I

    move-result v1

    invoke-direct {p0, v1, p2}, Lcom/mstar/android/tvapi/common/AudioManager;->native_setAudioOutput(ILcom/mstar/android/tvapi/common/vo/AudioOutParameter;)I

    move-result v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;->E_RETURN_OK:Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;->ordinal()I

    move-result v1

    if-lt v0, v1, :cond_0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;->E_RETURN_UNSUPPORT:Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;->ordinal()I

    move-result v1

    if-le v0, v1, :cond_1

    :cond_0
    new-instance v1, Lcom/mstar/android/tvapi/common/exception/TvCommonException;

    const-string v2, "setAudioOutput failed"

    invoke-direct {v1, v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;->values()[Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;

    move-result-object v1

    aget-object v1, v1, v0

    return-object v1
.end method

.method public setAudioPrimaryLanguage(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;)V
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/mstar/android/tvapi/common/AudioManager;->native_setAudioPrimaryLanguage(I)V

    return-void
.end method

.method public setAudioSecondaryLanguage(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;)V
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->ordinal()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/mstar/android/tvapi/common/AudioManager;->native_setAudioSecondaryLanguage(I)V

    return-void
.end method

.method public setAudioSource(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;Lcom/mstar/android/tvapi/common/vo/EnumAudioProcessorType;)I
    .locals 2
    .param p1    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    .param p2    # Lcom/mstar/android/tvapi/common/vo/EnumAudioProcessorType;

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v0

    invoke-virtual {p2}, Lcom/mstar/android/tvapi/common/vo/EnumAudioProcessorType;->ordinal()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/mstar/android/tvapi/common/AudioManager;->native_setAudioSource(II)I

    move-result v0

    return v0
.end method

.method public setAudioVolume(Lcom/mstar/android/tvapi/common/vo/EnumAudioVolumeSourceType;B)V
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumAudioVolumeSourceType;
    .param p2    # B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumAudioVolumeSourceType;->ordinal()I

    move-result v0

    invoke-direct {p0, v0, p2}, Lcom/mstar/android/tvapi/common/AudioManager;->native_setAudioVolume(IB)V

    return-void
.end method

.method public setAutoHOHEnable(Z)V
    .locals 0
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/mstar/android/tvapi/common/AudioManager;->native_setAutoHOHEnable(Z)V

    return-void
.end method

.method public final native setAutoVolume(Z)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final setBasicSoundEffect(Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;)Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;
    .locals 3
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;
    .param p2    # Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;->ordinal()I

    move-result v1

    invoke-direct {p0, v1, p2}, Lcom/mstar/android/tvapi/common/AudioManager;->native_setBasicSoundEffect(ILcom/mstar/android/tvapi/common/vo/DtvSoundEffect;)I

    move-result v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;->E_RETURN_OK:Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;->ordinal()I

    move-result v1

    if-lt v0, v1, :cond_0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;->E_RETURN_UNSUPPORT:Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;->ordinal()I

    move-result v1

    if-le v0, v1, :cond_1

    :cond_0
    new-instance v1, Lcom/mstar/android/tvapi/common/exception/TvCommonException;

    const-string v2, "setBasicSoundEffect failed"

    invoke-direct {v1, v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;->values()[Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;

    move-result-object v1

    aget-object v1, v1, v0

    return-object v1
.end method

.method public setCommonAudioInfo(Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;II)Z
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;
    .param p2    # I
    .param p3    # I

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/AudioCommonInfoType$EnumAudioCommonInfoType;->getValue()I

    move-result v0

    invoke-direct {p0, v0, p2, p3}, Lcom/mstar/android/tvapi/common/AudioManager;->native_setCommonAudioInfo(III)Z

    move-result v0

    return v0
.end method

.method public final native setDebugMode(Z)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public setDigitalOut(Lcom/mstar/android/tvapi/common/vo/EnumSpdifType;)V
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumSpdifType;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumSpdifType;->ordinal()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/mstar/android/tvapi/common/AudioManager;->native_setDigitalOut(I)V

    return-void
.end method

.method public setDtvOutputMode(Lcom/mstar/android/tvapi/common/vo/EnumDtvSoundMode;)V
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumDtvSoundMode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumDtvSoundMode;->ordinal()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/mstar/android/tvapi/common/AudioManager;->native_setDtvOutputMode(I)V

    return-void
.end method

.method public setInputLevel(Lcom/mstar/android/tvapi/common/vo/EnumAudioInputLevelSourceType;S)V
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumAudioInputLevelSourceType;
    .param p2    # S
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumAudioInputLevelSourceType;->ordinal()I

    move-result v0

    invoke-direct {p0, v0, p2}, Lcom/mstar/android/tvapi/common/AudioManager;->native_setInputLevel(IS)V

    return-void
.end method

.method public setInputSource(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)V
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/mstar/android/tvapi/common/AudioManager;->native_setInputSource(I)V

    return-void
.end method

.method public final setKtvMixModeVolume(Lcom/mstar/android/tvapi/common/vo/EnumKtvMixVolumeType;SS)S
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumKtvMixVolumeType;
    .param p2    # S
    .param p3    # S

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumKtvMixVolumeType;->ordinal()I

    move-result v0

    invoke-direct {p0, v0, p2, p3}, Lcom/mstar/android/tvapi/common/AudioManager;->native_setKtvMixModeVolume(ISS)S

    move-result v0

    return v0
.end method

.method public final setKtvSoundInfo(Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;II)S
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;
    .param p2    # I
    .param p3    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/KtvInfoType$EnumKtvInfoType;->getValue()I

    move-result v0

    invoke-direct {p0, v0, p2, p3}, Lcom/mstar/android/tvapi/common/AudioManager;->native_setKtvSoundInfo(III)S

    move-result v0

    return v0
.end method

.method public setKtvSoundTrack(Lcom/mstar/android/tvapi/common/vo/EnumKtvAudioMpegSoundMode;)I
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumKtvAudioMpegSoundMode;

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumKtvAudioMpegSoundMode;->ordinal()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/mstar/android/tvapi/common/AudioManager;->native_setKtvSoundTrack(I)I

    move-result v0

    return v0
.end method

.method public final setMuteStatus(ILcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Z
    .locals 1
    .param p1    # I
    .param p2    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v0

    invoke-direct {p0, p1, v0}, Lcom/mstar/android/tvapi/common/AudioManager;->native_setMuteStatus(II)Z

    move-result v0

    return v0
.end method

.method public setOnAudioEventListener(Lcom/mstar/android/tvapi/common/listener/OnAudioEventListener;)V
    .locals 0
    .param p1    # Lcom/mstar/android/tvapi/common/listener/OnAudioEventListener;

    iput-object p1, p0, Lcom/mstar/android/tvapi/common/AudioManager;->mOnEventListener:Lcom/mstar/android/tvapi/common/listener/OnAudioEventListener;

    return-void
.end method

.method public final setSoundParameter(Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;II)S
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;
    .param p2    # I
    .param p3    # I

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;->ordinal()I

    move-result v0

    invoke-direct {p0, v0, p2, p3}, Lcom/mstar/android/tvapi/common/AudioManager;->native_SetSoundParameter(III)S

    move-result v0

    return v0
.end method

.method public final native setSoundSpdifDelay(I)S
.end method

.method public final native setSoundSpeakerDelay(I)S
.end method

.method public final native setSubWooferVolume(ZS)S
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public setToNextAtvMtsMode()Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/mstar/android/tvapi/common/AudioManager;->native_setToNextAtvMtsMode()I

    move-result v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;->E_RETURN_OK:Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;->ordinal()I

    move-result v1

    if-lt v0, v1, :cond_0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;->E_RETURN_UNSUPPORT:Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;->ordinal()I

    move-result v1

    if-le v0, v1, :cond_1

    :cond_0
    new-instance v1, Lcom/mstar/android/tvapi/common/exception/TvCommonException;

    const-string v2, "setToNextAtvMtsMode failed"

    invoke-direct {v1, v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;->values()[Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;

    move-result-object v1

    aget-object v1, v1, v0

    return-object v1
.end method
