.class public interface abstract Lcom/mstar/android/tvapi/atv/listener/OnAtvPlayerEventListener;
.super Ljava/lang/Object;
.source "OnAtvPlayerEventListener.java"


# virtual methods
.method public abstract onAtvAutoTuningScanInfo(ILcom/mstar/android/tvapi/atv/vo/AtvEventScan;)Z
.end method

.method public abstract onAtvManualTuningScanInfo(ILcom/mstar/android/tvapi/atv/vo/AtvEventScan;)Z
.end method

.method public abstract onAtvProgramInfoReady(I)Z
.end method

.method public abstract onSignalLock(I)Z
.end method

.method public abstract onSignalUnLock(I)Z
.end method
