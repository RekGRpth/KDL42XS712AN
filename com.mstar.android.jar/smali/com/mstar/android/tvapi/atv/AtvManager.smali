.class public final Lcom/mstar/android/tvapi/atv/AtvManager;
.super Lcom/mstar/android/tvapi/common/TvManager;
.source "AtvManager.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/mstar/android/tvapi/common/TvManager;-><init>()V

    return-void
.end method

.method public static getAtvPlayerManager()Lcom/mstar/android/tvapi/atv/AtvPlayer;
    .locals 2

    new-instance v0, Lcom/mstar/android/tvapi/atv/AtvPlayerImplProxy;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/atv/AtvPlayerImplProxy;-><init>()V

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/atv/AtvPlayerImplProxy;->getPlayerImplInstance()Lcom/mstar/android/tvapi/impl/PlayerImpl;

    move-result-object v1

    return-object v1
.end method

.method public static getAtvScanManager()Lcom/mstar/android/tvapi/atv/AtvScanManager;
    .locals 2

    new-instance v0, Lcom/mstar/android/tvapi/atv/AtvScanImplProxy;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/atv/AtvScanImplProxy;-><init>()V

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/atv/AtvScanImplProxy;->getScanImplInstance()Lcom/mstar/android/tvapi/impl/ScanManagerImpl;

    move-result-object v1

    return-object v1
.end method
