.class public final enum Lcom/mstar/android/tvapi/atv/vo/EnumAtvManualTuneMode;
.super Ljava/lang/Enum;
.source "EnumAtvManualTuneMode.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/tvapi/atv/vo/EnumAtvManualTuneMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/tvapi/atv/vo/EnumAtvManualTuneMode;

.field public static final enum E_MANUAL_TUNE_MODE_FINE_TUNE_DOWN:Lcom/mstar/android/tvapi/atv/vo/EnumAtvManualTuneMode;

.field public static final enum E_MANUAL_TUNE_MODE_FINE_TUNE_ONE_FREQUENCY:Lcom/mstar/android/tvapi/atv/vo/EnumAtvManualTuneMode;

.field public static final enum E_MANUAL_TUNE_MODE_FINE_TUNE_UP:Lcom/mstar/android/tvapi/atv/vo/EnumAtvManualTuneMode;

.field public static final enum E_MANUAL_TUNE_MODE_SEARCH_ONE_TO_DOWN:Lcom/mstar/android/tvapi/atv/vo/EnumAtvManualTuneMode;

.field public static final enum E_MANUAL_TUNE_MODE_SEARCH_ONE_TO_UP:Lcom/mstar/android/tvapi/atv/vo/EnumAtvManualTuneMode;

.field public static final enum E_MANUAL_TUNE_MODE_UNDEFINE:Lcom/mstar/android/tvapi/atv/vo/EnumAtvManualTuneMode;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/atv/vo/EnumAtvManualTuneMode;

    const-string v1, "E_MANUAL_TUNE_MODE_SEARCH_ONE_TO_UP"

    invoke-direct {v0, v1, v3}, Lcom/mstar/android/tvapi/atv/vo/EnumAtvManualTuneMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/atv/vo/EnumAtvManualTuneMode;->E_MANUAL_TUNE_MODE_SEARCH_ONE_TO_UP:Lcom/mstar/android/tvapi/atv/vo/EnumAtvManualTuneMode;

    new-instance v0, Lcom/mstar/android/tvapi/atv/vo/EnumAtvManualTuneMode;

    const-string v1, "E_MANUAL_TUNE_MODE_SEARCH_ONE_TO_DOWN"

    invoke-direct {v0, v1, v4}, Lcom/mstar/android/tvapi/atv/vo/EnumAtvManualTuneMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/atv/vo/EnumAtvManualTuneMode;->E_MANUAL_TUNE_MODE_SEARCH_ONE_TO_DOWN:Lcom/mstar/android/tvapi/atv/vo/EnumAtvManualTuneMode;

    new-instance v0, Lcom/mstar/android/tvapi/atv/vo/EnumAtvManualTuneMode;

    const-string v1, "E_MANUAL_TUNE_MODE_FINE_TUNE_ONE_FREQUENCY"

    invoke-direct {v0, v1, v5}, Lcom/mstar/android/tvapi/atv/vo/EnumAtvManualTuneMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/atv/vo/EnumAtvManualTuneMode;->E_MANUAL_TUNE_MODE_FINE_TUNE_ONE_FREQUENCY:Lcom/mstar/android/tvapi/atv/vo/EnumAtvManualTuneMode;

    new-instance v0, Lcom/mstar/android/tvapi/atv/vo/EnumAtvManualTuneMode;

    const-string v1, "E_MANUAL_TUNE_MODE_FINE_TUNE_UP"

    invoke-direct {v0, v1, v6}, Lcom/mstar/android/tvapi/atv/vo/EnumAtvManualTuneMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/atv/vo/EnumAtvManualTuneMode;->E_MANUAL_TUNE_MODE_FINE_TUNE_UP:Lcom/mstar/android/tvapi/atv/vo/EnumAtvManualTuneMode;

    new-instance v0, Lcom/mstar/android/tvapi/atv/vo/EnumAtvManualTuneMode;

    const-string v1, "E_MANUAL_TUNE_MODE_FINE_TUNE_DOWN"

    invoke-direct {v0, v1, v7}, Lcom/mstar/android/tvapi/atv/vo/EnumAtvManualTuneMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/atv/vo/EnumAtvManualTuneMode;->E_MANUAL_TUNE_MODE_FINE_TUNE_DOWN:Lcom/mstar/android/tvapi/atv/vo/EnumAtvManualTuneMode;

    new-instance v0, Lcom/mstar/android/tvapi/atv/vo/EnumAtvManualTuneMode;

    const-string v1, "E_MANUAL_TUNE_MODE_UNDEFINE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/atv/vo/EnumAtvManualTuneMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/atv/vo/EnumAtvManualTuneMode;->E_MANUAL_TUNE_MODE_UNDEFINE:Lcom/mstar/android/tvapi/atv/vo/EnumAtvManualTuneMode;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/mstar/android/tvapi/atv/vo/EnumAtvManualTuneMode;

    sget-object v1, Lcom/mstar/android/tvapi/atv/vo/EnumAtvManualTuneMode;->E_MANUAL_TUNE_MODE_SEARCH_ONE_TO_UP:Lcom/mstar/android/tvapi/atv/vo/EnumAtvManualTuneMode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mstar/android/tvapi/atv/vo/EnumAtvManualTuneMode;->E_MANUAL_TUNE_MODE_SEARCH_ONE_TO_DOWN:Lcom/mstar/android/tvapi/atv/vo/EnumAtvManualTuneMode;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/android/tvapi/atv/vo/EnumAtvManualTuneMode;->E_MANUAL_TUNE_MODE_FINE_TUNE_ONE_FREQUENCY:Lcom/mstar/android/tvapi/atv/vo/EnumAtvManualTuneMode;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mstar/android/tvapi/atv/vo/EnumAtvManualTuneMode;->E_MANUAL_TUNE_MODE_FINE_TUNE_UP:Lcom/mstar/android/tvapi/atv/vo/EnumAtvManualTuneMode;

    aput-object v1, v0, v6

    sget-object v1, Lcom/mstar/android/tvapi/atv/vo/EnumAtvManualTuneMode;->E_MANUAL_TUNE_MODE_FINE_TUNE_DOWN:Lcom/mstar/android/tvapi/atv/vo/EnumAtvManualTuneMode;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/mstar/android/tvapi/atv/vo/EnumAtvManualTuneMode;->E_MANUAL_TUNE_MODE_UNDEFINE:Lcom/mstar/android/tvapi/atv/vo/EnumAtvManualTuneMode;

    aput-object v2, v0, v1

    sput-object v0, Lcom/mstar/android/tvapi/atv/vo/EnumAtvManualTuneMode;->$VALUES:[Lcom/mstar/android/tvapi/atv/vo/EnumAtvManualTuneMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/tvapi/atv/vo/EnumAtvManualTuneMode;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/tvapi/atv/vo/EnumAtvManualTuneMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/atv/vo/EnumAtvManualTuneMode;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/tvapi/atv/vo/EnumAtvManualTuneMode;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/atv/vo/EnumAtvManualTuneMode;->$VALUES:[Lcom/mstar/android/tvapi/atv/vo/EnumAtvManualTuneMode;

    invoke-virtual {v0}, [Lcom/mstar/android/tvapi/atv/vo/EnumAtvManualTuneMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/tvapi/atv/vo/EnumAtvManualTuneMode;

    return-object v0
.end method
