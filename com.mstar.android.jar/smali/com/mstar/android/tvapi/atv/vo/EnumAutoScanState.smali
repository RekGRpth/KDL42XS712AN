.class public final enum Lcom/mstar/android/tvapi/atv/vo/EnumAutoScanState;
.super Ljava/lang/Enum;
.source "EnumAutoScanState.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/tvapi/atv/vo/EnumAutoScanState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/tvapi/atv/vo/EnumAutoScanState;

.field public static final enum E_NONE_NTSC_AUTO_SCAN:Lcom/mstar/android/tvapi/atv/vo/EnumAutoScanState;

.field public static final enum E_NTSC_AUTO_SCAN:Lcom/mstar/android/tvapi/atv/vo/EnumAutoScanState;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/atv/vo/EnumAutoScanState;

    const-string v1, "E_NONE_NTSC_AUTO_SCAN"

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/atv/vo/EnumAutoScanState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/atv/vo/EnumAutoScanState;->E_NONE_NTSC_AUTO_SCAN:Lcom/mstar/android/tvapi/atv/vo/EnumAutoScanState;

    new-instance v0, Lcom/mstar/android/tvapi/atv/vo/EnumAutoScanState;

    const-string v1, "E_NTSC_AUTO_SCAN"

    invoke-direct {v0, v1, v3}, Lcom/mstar/android/tvapi/atv/vo/EnumAutoScanState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/atv/vo/EnumAutoScanState;->E_NTSC_AUTO_SCAN:Lcom/mstar/android/tvapi/atv/vo/EnumAutoScanState;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/mstar/android/tvapi/atv/vo/EnumAutoScanState;

    sget-object v1, Lcom/mstar/android/tvapi/atv/vo/EnumAutoScanState;->E_NONE_NTSC_AUTO_SCAN:Lcom/mstar/android/tvapi/atv/vo/EnumAutoScanState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/mstar/android/tvapi/atv/vo/EnumAutoScanState;->E_NTSC_AUTO_SCAN:Lcom/mstar/android/tvapi/atv/vo/EnumAutoScanState;

    aput-object v1, v0, v3

    sput-object v0, Lcom/mstar/android/tvapi/atv/vo/EnumAutoScanState;->$VALUES:[Lcom/mstar/android/tvapi/atv/vo/EnumAutoScanState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/tvapi/atv/vo/EnumAutoScanState;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/tvapi/atv/vo/EnumAutoScanState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/atv/vo/EnumAutoScanState;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/tvapi/atv/vo/EnumAutoScanState;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/atv/vo/EnumAutoScanState;->$VALUES:[Lcom/mstar/android/tvapi/atv/vo/EnumAutoScanState;

    invoke-virtual {v0}, [Lcom/mstar/android/tvapi/atv/vo/EnumAutoScanState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/tvapi/atv/vo/EnumAutoScanState;

    return-object v0
.end method
