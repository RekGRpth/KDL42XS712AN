.class public final enum Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;
.super Ljava/lang/Enum;
.source "EnumGetProgramCtrl.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;

.field public static final enum E_GET_ACTIVE_PROGRAM_COUNT:Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;

.field public static final enum E_GET_CHANNEL_MAX:Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;

.field public static final enum E_GET_CHANNEL_MIN:Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;

.field public static final enum E_GET_CURRENT_PROGRAM_NUMBER:Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;

.field public static final enum E_GET_FIRST_PROGRAM_NUMBER:Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;

.field public static final enum E_GET_NEXT_PROGRAM_NUMBER:Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;

.field public static final enum E_GET_NON_SKIP_PROGRAM_COUNT:Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;

.field public static final enum E_GET_PAST_PROGRAM_NUMBER:Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;

.field public static final enum E_GET_PREV_PROGRAM_NUMBER:Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;

.field public static final enum E_IS_PROGRAM_EMPTY:Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;

.field public static final enum E_IS_PROGRAM_NUMBER_ACTIVE:Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;

    const-string v1, "E_GET_CURRENT_PROGRAM_NUMBER"

    invoke-direct {v0, v1, v3}, Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;->E_GET_CURRENT_PROGRAM_NUMBER:Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;

    new-instance v0, Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;

    const-string v1, "E_GET_FIRST_PROGRAM_NUMBER"

    invoke-direct {v0, v1, v4}, Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;->E_GET_FIRST_PROGRAM_NUMBER:Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;

    new-instance v0, Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;

    const-string v1, "E_GET_NEXT_PROGRAM_NUMBER"

    invoke-direct {v0, v1, v5}, Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;->E_GET_NEXT_PROGRAM_NUMBER:Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;

    new-instance v0, Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;

    const-string v1, "E_GET_PREV_PROGRAM_NUMBER"

    invoke-direct {v0, v1, v6}, Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;->E_GET_PREV_PROGRAM_NUMBER:Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;

    new-instance v0, Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;

    const-string v1, "E_GET_PAST_PROGRAM_NUMBER"

    invoke-direct {v0, v1, v7}, Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;->E_GET_PAST_PROGRAM_NUMBER:Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;

    new-instance v0, Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;

    const-string v1, "E_IS_PROGRAM_NUMBER_ACTIVE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;->E_IS_PROGRAM_NUMBER_ACTIVE:Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;

    new-instance v0, Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;

    const-string v1, "E_IS_PROGRAM_EMPTY"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;->E_IS_PROGRAM_EMPTY:Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;

    new-instance v0, Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;

    const-string v1, "E_GET_ACTIVE_PROGRAM_COUNT"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;->E_GET_ACTIVE_PROGRAM_COUNT:Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;

    new-instance v0, Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;

    const-string v1, "E_GET_NON_SKIP_PROGRAM_COUNT"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;->E_GET_NON_SKIP_PROGRAM_COUNT:Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;

    new-instance v0, Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;

    const-string v1, "E_GET_CHANNEL_MAX"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;->E_GET_CHANNEL_MAX:Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;

    new-instance v0, Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;

    const-string v1, "E_GET_CHANNEL_MIN"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;->E_GET_CHANNEL_MIN:Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;

    const/16 v0, 0xb

    new-array v0, v0, [Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;

    sget-object v1, Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;->E_GET_CURRENT_PROGRAM_NUMBER:Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;->E_GET_FIRST_PROGRAM_NUMBER:Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;->E_GET_NEXT_PROGRAM_NUMBER:Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;->E_GET_PREV_PROGRAM_NUMBER:Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;

    aput-object v1, v0, v6

    sget-object v1, Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;->E_GET_PAST_PROGRAM_NUMBER:Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;->E_IS_PROGRAM_NUMBER_ACTIVE:Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;->E_IS_PROGRAM_EMPTY:Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;->E_GET_ACTIVE_PROGRAM_COUNT:Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;->E_GET_NON_SKIP_PROGRAM_COUNT:Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;->E_GET_CHANNEL_MAX:Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;->E_GET_CHANNEL_MIN:Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;

    aput-object v2, v0, v1

    sput-object v0, Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;->$VALUES:[Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;->$VALUES:[Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;

    invoke-virtual {v0}, [Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;

    return-object v0
.end method
