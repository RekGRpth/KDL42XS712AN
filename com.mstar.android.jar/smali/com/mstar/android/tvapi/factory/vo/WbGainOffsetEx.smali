.class public Lcom/mstar/android/tvapi/factory/vo/WbGainOffsetEx;
.super Ljava/lang/Object;
.source "WbGainOffsetEx.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mstar/android/tvapi/factory/vo/WbGainOffsetEx;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public blueGain:I

.field public blueOffset:I

.field public greenGain:I

.field public greenOffset:I

.field public redGain:I

.field public redOffset:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/mstar/android/tvapi/factory/vo/WbGainOffsetEx$1;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/factory/vo/WbGainOffsetEx$1;-><init>()V

    sput-object v0, Lcom/mstar/android/tvapi/factory/vo/WbGainOffsetEx;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/mstar/android/tvapi/factory/vo/WbGainOffsetEx;->redGain:I

    iput v0, p0, Lcom/mstar/android/tvapi/factory/vo/WbGainOffsetEx;->greenGain:I

    iput v0, p0, Lcom/mstar/android/tvapi/factory/vo/WbGainOffsetEx;->blueGain:I

    iput v0, p0, Lcom/mstar/android/tvapi/factory/vo/WbGainOffsetEx;->redOffset:I

    iput v0, p0, Lcom/mstar/android/tvapi/factory/vo/WbGainOffsetEx;->greenOffset:I

    iput v0, p0, Lcom/mstar/android/tvapi/factory/vo/WbGainOffsetEx;->blueOffset:I

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1    # Landroid/os/Parcel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/factory/vo/WbGainOffsetEx;->redGain:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/factory/vo/WbGainOffsetEx;->greenGain:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/factory/vo/WbGainOffsetEx;->blueGain:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/factory/vo/WbGainOffsetEx;->redOffset:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/factory/vo/WbGainOffsetEx;->greenOffset:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/factory/vo/WbGainOffsetEx;->blueOffset:I

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/mstar/android/tvapi/factory/vo/WbGainOffsetEx$1;)V
    .locals 0
    .param p1    # Landroid/os/Parcel;
    .param p2    # Lcom/mstar/android/tvapi/factory/vo/WbGainOffsetEx$1;

    invoke-direct {p0, p1}, Lcom/mstar/android/tvapi/factory/vo/WbGainOffsetEx;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget v0, p0, Lcom/mstar/android/tvapi/factory/vo/WbGainOffsetEx;->redGain:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/factory/vo/WbGainOffsetEx;->greenGain:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/factory/vo/WbGainOffsetEx;->blueGain:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/factory/vo/WbGainOffsetEx;->redOffset:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/factory/vo/WbGainOffsetEx;->greenOffset:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/factory/vo/WbGainOffsetEx;->blueOffset:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
