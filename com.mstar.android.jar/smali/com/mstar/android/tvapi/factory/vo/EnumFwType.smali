.class public final enum Lcom/mstar/android/tvapi/factory/vo/EnumFwType;
.super Ljava/lang/Enum;
.source "EnumFwType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/tvapi/factory/vo/EnumFwType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/tvapi/factory/vo/EnumFwType;

.field public static final enum E_VIDEO_FW_TYPE_AUDIO:Lcom/mstar/android/tvapi/factory/vo/EnumFwType;

.field public static final enum E_VIDEO_FW_TYPE_HVD:Lcom/mstar/android/tvapi/factory/vo/EnumFwType;

.field public static final enum E_VIDEO_FW_TYPE_MVD:Lcom/mstar/android/tvapi/factory/vo/EnumFwType;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/factory/vo/EnumFwType;

    const-string v1, "E_VIDEO_FW_TYPE_MVD"

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/factory/vo/EnumFwType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/factory/vo/EnumFwType;->E_VIDEO_FW_TYPE_MVD:Lcom/mstar/android/tvapi/factory/vo/EnumFwType;

    new-instance v0, Lcom/mstar/android/tvapi/factory/vo/EnumFwType;

    const-string v1, "E_VIDEO_FW_TYPE_HVD"

    invoke-direct {v0, v1, v3}, Lcom/mstar/android/tvapi/factory/vo/EnumFwType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/factory/vo/EnumFwType;->E_VIDEO_FW_TYPE_HVD:Lcom/mstar/android/tvapi/factory/vo/EnumFwType;

    new-instance v0, Lcom/mstar/android/tvapi/factory/vo/EnumFwType;

    const-string v1, "E_VIDEO_FW_TYPE_AUDIO"

    invoke-direct {v0, v1, v4}, Lcom/mstar/android/tvapi/factory/vo/EnumFwType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/factory/vo/EnumFwType;->E_VIDEO_FW_TYPE_AUDIO:Lcom/mstar/android/tvapi/factory/vo/EnumFwType;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/mstar/android/tvapi/factory/vo/EnumFwType;

    sget-object v1, Lcom/mstar/android/tvapi/factory/vo/EnumFwType;->E_VIDEO_FW_TYPE_MVD:Lcom/mstar/android/tvapi/factory/vo/EnumFwType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/mstar/android/tvapi/factory/vo/EnumFwType;->E_VIDEO_FW_TYPE_HVD:Lcom/mstar/android/tvapi/factory/vo/EnumFwType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mstar/android/tvapi/factory/vo/EnumFwType;->E_VIDEO_FW_TYPE_AUDIO:Lcom/mstar/android/tvapi/factory/vo/EnumFwType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/mstar/android/tvapi/factory/vo/EnumFwType;->$VALUES:[Lcom/mstar/android/tvapi/factory/vo/EnumFwType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/tvapi/factory/vo/EnumFwType;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/tvapi/factory/vo/EnumFwType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/factory/vo/EnumFwType;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/tvapi/factory/vo/EnumFwType;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/factory/vo/EnumFwType;->$VALUES:[Lcom/mstar/android/tvapi/factory/vo/EnumFwType;

    invoke-virtual {v0}, [Lcom/mstar/android/tvapi/factory/vo/EnumFwType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/tvapi/factory/vo/EnumFwType;

    return-object v0
.end method
