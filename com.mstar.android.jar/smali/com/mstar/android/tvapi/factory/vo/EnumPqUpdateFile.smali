.class public final enum Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;
.super Ljava/lang/Enum;
.source "EnumPqUpdateFile.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;

.field public static final enum E_BANDWIDTH_REG_TABLE_FILE:Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;

.field public static final enum E_COLOR_MATRiX_FILE:Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;

.field public static final enum E_DLC_FILE:Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;

.field public static final enum E_GAMMA0_FILE:Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;

.field public static final enum E_MSRV_PQ_SUB_FILE:Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;

.field public static final enum E_PQ_MAIN_EX_FILE:Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;

.field public static final enum E_PQ_MAIN_EX_TEXT_FILE:Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;

.field public static final enum E_PQ_MAIN_FILE:Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;

.field public static final enum E_PQ_MAIN_TEXT_FILE:Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;

.field public static final enum E_PQ_SUB_EX_FILE:Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;

.field public static final enum E_PQ_SUB_EX_TEXT_FILE:Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;

.field public static final enum E_PQ_SUB_TEXT_FILE:Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;

    const-string v1, "E_DLC_FILE"

    invoke-direct {v0, v1, v3}, Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;->E_DLC_FILE:Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;

    new-instance v0, Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;

    const-string v1, "E_COLOR_MATRiX_FILE"

    invoke-direct {v0, v1, v4}, Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;->E_COLOR_MATRiX_FILE:Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;

    new-instance v0, Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;

    const-string v1, "E_BANDWIDTH_REG_TABLE_FILE"

    invoke-direct {v0, v1, v5}, Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;->E_BANDWIDTH_REG_TABLE_FILE:Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;

    new-instance v0, Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;

    const-string v1, "E_PQ_MAIN_FILE"

    invoke-direct {v0, v1, v6}, Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;->E_PQ_MAIN_FILE:Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;

    new-instance v0, Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;

    const-string v1, "E_PQ_MAIN_TEXT_FILE"

    invoke-direct {v0, v1, v7}, Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;->E_PQ_MAIN_TEXT_FILE:Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;

    new-instance v0, Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;

    const-string v1, "E_PQ_MAIN_EX_FILE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;->E_PQ_MAIN_EX_FILE:Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;

    new-instance v0, Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;

    const-string v1, "E_PQ_MAIN_EX_TEXT_FILE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;->E_PQ_MAIN_EX_TEXT_FILE:Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;

    new-instance v0, Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;

    const-string v1, "E_MSRV_PQ_SUB_FILE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;->E_MSRV_PQ_SUB_FILE:Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;

    new-instance v0, Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;

    const-string v1, "E_PQ_SUB_TEXT_FILE"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;->E_PQ_SUB_TEXT_FILE:Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;

    new-instance v0, Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;

    const-string v1, "E_PQ_SUB_EX_FILE"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;->E_PQ_SUB_EX_FILE:Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;

    new-instance v0, Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;

    const-string v1, "E_PQ_SUB_EX_TEXT_FILE"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;->E_PQ_SUB_EX_TEXT_FILE:Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;

    new-instance v0, Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;

    const-string v1, "E_GAMMA0_FILE"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;->E_GAMMA0_FILE:Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;

    const/16 v0, 0xc

    new-array v0, v0, [Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;

    sget-object v1, Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;->E_DLC_FILE:Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;->E_COLOR_MATRiX_FILE:Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;->E_BANDWIDTH_REG_TABLE_FILE:Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;->E_PQ_MAIN_FILE:Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;

    aput-object v1, v0, v6

    sget-object v1, Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;->E_PQ_MAIN_TEXT_FILE:Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;->E_PQ_MAIN_EX_FILE:Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;->E_PQ_MAIN_EX_TEXT_FILE:Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;->E_MSRV_PQ_SUB_FILE:Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;->E_PQ_SUB_TEXT_FILE:Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;->E_PQ_SUB_EX_FILE:Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;->E_PQ_SUB_EX_TEXT_FILE:Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;->E_GAMMA0_FILE:Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;

    aput-object v2, v0, v1

    sput-object v0, Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;->$VALUES:[Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;->$VALUES:[Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;

    invoke-virtual {v0}, [Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;

    return-object v0
.end method
