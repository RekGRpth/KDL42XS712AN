.class public Lcom/mstar/android/tvapi/factory/vo/PictureModeValue;
.super Ljava/lang/Object;
.source "PictureModeValue.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mstar/android/tvapi/factory/vo/PictureModeValue;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public brightness:S

.field public contrast:S

.field public hue:S

.field public saturation:S

.field public sharpness:S


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/mstar/android/tvapi/factory/vo/PictureModeValue$1;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/factory/vo/PictureModeValue$1;-><init>()V

    sput-object v0, Lcom/mstar/android/tvapi/factory/vo/PictureModeValue;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-short v0, p0, Lcom/mstar/android/tvapi/factory/vo/PictureModeValue;->brightness:S

    iput-short v0, p0, Lcom/mstar/android/tvapi/factory/vo/PictureModeValue;->contrast:S

    iput-short v0, p0, Lcom/mstar/android/tvapi/factory/vo/PictureModeValue;->saturation:S

    iput-short v0, p0, Lcom/mstar/android/tvapi/factory/vo/PictureModeValue;->sharpness:S

    iput-short v0, p0, Lcom/mstar/android/tvapi/factory/vo/PictureModeValue;->hue:S

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1    # Landroid/os/Parcel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/factory/vo/PictureModeValue;->brightness:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/factory/vo/PictureModeValue;->contrast:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/factory/vo/PictureModeValue;->saturation:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/factory/vo/PictureModeValue;->sharpness:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/factory/vo/PictureModeValue;->hue:S

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/mstar/android/tvapi/factory/vo/PictureModeValue$1;)V
    .locals 0
    .param p1    # Landroid/os/Parcel;
    .param p2    # Lcom/mstar/android/tvapi/factory/vo/PictureModeValue$1;

    invoke-direct {p0, p1}, Lcom/mstar/android/tvapi/factory/vo/PictureModeValue;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget-short v0, p0, Lcom/mstar/android/tvapi/factory/vo/PictureModeValue;->brightness:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/factory/vo/PictureModeValue;->contrast:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/factory/vo/PictureModeValue;->saturation:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/factory/vo/PictureModeValue;->sharpness:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/factory/vo/PictureModeValue;->hue:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
