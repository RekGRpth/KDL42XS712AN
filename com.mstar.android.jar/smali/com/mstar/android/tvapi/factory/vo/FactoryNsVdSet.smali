.class public Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;
.super Ljava/lang/Object;
.source "FactoryNsVdSet.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public aFEC_43:S

.field public aFEC_44:S

.field public aFEC_66_Bit76:S

.field public aFEC_6E_Bit3210:S

.field public aFEC_6E_Bit7654:S

.field public aFEC_A0:S

.field public aFEC_A1:S

.field public aFEC_CB:S

.field public aFEC_CF_Bit2_ATV:S

.field public aFEC_CF_Bit2_AV:S

.field public aFEC_D4:S

.field public aFEC_D5_Bit2:S

.field public aFEC_D7_HIGH_BOUND:S

.field public aFEC_D7_LOW_BOUND:S

.field public aFEC_D8_Bit3210:S

.field public aFEC_D9_Bit0:S


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet$1;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet$1;-><init>()V

    sput-object v0, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-short v0, p0, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_D4:S

    iput-short v0, p0, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_D8_Bit3210:S

    iput-short v0, p0, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_D5_Bit2:S

    iput-short v0, p0, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_D7_LOW_BOUND:S

    iput-short v0, p0, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_D7_HIGH_BOUND:S

    iput-short v0, p0, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_D9_Bit0:S

    iput-short v0, p0, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_A0:S

    iput-short v0, p0, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_A1:S

    iput-short v0, p0, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_66_Bit76:S

    iput-short v0, p0, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_6E_Bit7654:S

    iput-short v0, p0, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_6E_Bit3210:S

    iput-short v0, p0, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_43:S

    iput-short v0, p0, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_44:S

    iput-short v0, p0, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_CB:S

    iput-short v0, p0, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_CF_Bit2_ATV:S

    iput-short v0, p0, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_CF_Bit2_AV:S

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1    # Landroid/os/Parcel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_D4:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_D8_Bit3210:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_D5_Bit2:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_D7_LOW_BOUND:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_D7_HIGH_BOUND:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_D9_Bit0:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_A0:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_A1:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_66_Bit76:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_6E_Bit7654:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_6E_Bit3210:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_43:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_44:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_CB:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_CF_Bit2_ATV:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_CF_Bit2_AV:S

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet$1;)V
    .locals 0
    .param p1    # Landroid/os/Parcel;
    .param p2    # Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet$1;

    invoke-direct {p0, p1}, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget-short v0, p0, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_D4:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_D8_Bit3210:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_D5_Bit2:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_D7_LOW_BOUND:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_D7_HIGH_BOUND:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_D9_Bit0:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_A0:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_A1:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_66_Bit76:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_6E_Bit7654:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_6E_Bit3210:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_43:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_44:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_CB:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_CF_Bit2_ATV:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_CF_Bit2_AV:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
