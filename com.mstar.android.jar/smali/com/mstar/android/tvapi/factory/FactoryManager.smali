.class public Lcom/mstar/android/tvapi/factory/FactoryManager;
.super Ljava/lang/Object;
.source "FactoryManager.java"


# static fields
.field private static _factoryManager:Lcom/mstar/android/tvapi/factory/FactoryManager;


# instance fields
.field private mFactoryManagerContext:I

.field private mNativeContext:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v1, 0x0

    sput-object v1, Lcom/mstar/android/tvapi/factory/FactoryManager;->_factoryManager:Lcom/mstar/android/tvapi/factory/FactoryManager;

    :try_start_0
    const-string v1, "factorymanager_jni"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    invoke-static {}, Lcom/mstar/android/tvapi/factory/FactoryManager;->native_init()V
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    sget-object v1, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Cannot load factorymanager_jni library:\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/UnsatisfiedLinkError;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-direct {p0, v0}, Lcom/mstar/android/tvapi/factory/FactoryManager;->native_setup(Ljava/lang/Object;)V

    return-void
.end method

.method private static PostEvent_SnServiceDeadth(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    :try_start_0
    const-class v2, Lcom/mstar/android/tvapi/factory/FactoryManager;

    monitor-enter v2
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    const-string v1, "mstar.str.suspending"

    const-string v3, "1"

    invoke-static {v1, v3}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x0

    sput-object v1, Lcom/mstar/android/tvapi/factory/FactoryManager;->_factoryManager:Lcom/mstar/android/tvapi/factory/FactoryManager;

    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "PostEvent_SnServiceDeadth: set SystemProperties \'mstar.str.suspending\' =1"

    invoke-virtual {v1, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    monitor-exit v2

    :goto_0
    return-void

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v1
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method protected static getInstance(Ljava/lang/Object;)Lcom/mstar/android/tvapi/factory/FactoryManager;
    .locals 3
    .param p0    # Ljava/lang/Object;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.mstar.android.tvapi.common.TvFactoryManagerProxy"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v1, Lcom/mstar/android/tvapi/factory/FactoryManager;->_factoryManager:Lcom/mstar/android/tvapi/factory/FactoryManager;

    if-nez v1, :cond_1

    const-class v2, Lcom/mstar/android/tvapi/factory/FactoryManager;

    monitor-enter v2

    :try_start_0
    sget-object v1, Lcom/mstar/android/tvapi/factory/FactoryManager;->_factoryManager:Lcom/mstar/android/tvapi/factory/FactoryManager;

    if-nez v1, :cond_0

    new-instance v1, Lcom/mstar/android/tvapi/factory/FactoryManager;

    invoke-direct {v1}, Lcom/mstar/android/tvapi/factory/FactoryManager;-><init>()V

    sput-object v1, Lcom/mstar/android/tvapi/factory/FactoryManager;->_factoryManager:Lcom/mstar/android/tvapi/factory/FactoryManager;

    :cond_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    sget-object v1, Lcom/mstar/android/tvapi/factory/FactoryManager;->_factoryManager:Lcom/mstar/android/tvapi/factory/FactoryManager;

    return-object v1

    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method private final native getUpdatePqFilePath(I)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native naitve_getWbGainOffset(I)Lcom/mstar/android/tvapi/factory/vo/WbGainOffset;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_finalize()V
.end method

.method private final native native_getAdcGainOffset(II)Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_getDisplayResolution()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private native native_getEnvironmentPowerMode()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private native native_getFwVersion(I)I
.end method

.method private native native_getResolutionMappingIndex(I)S
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_getWbGainOffsetEx(II)Lcom/mstar/android/tvapi/factory/vo/WbGainOffsetEx;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private static final native native_init()V
.end method

.method private final native native_setAdcGainOffset(IILcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private native native_setEnvironmentPowerMode(I)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_setVideoTestPattern(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_setWbGainOffset(ISSSSSS)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_setWbGainOffsetEx(IIIIIIII)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_setup(Ljava/lang/Object;)V
.end method

.method private static postEventFromNative(Ljava/lang/Object;IIILjava/lang/Object;)V
    .locals 0
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public final native autoAdc()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native copySubColorDataToAllSource()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native copyWhiteBalanceSettingToAllSource()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native disablePVRRecordAll()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native disableUart()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native disableWdt()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native enablePVRRecordAll()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native enableUart()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public native enableUartDebug()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native enableWdt()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public finalize()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    invoke-direct {p0}, Lcom/mstar/android/tvapi/factory/FactoryManager;->native_finalize()V

    const/4 v0, 0x0

    sput-object v0, Lcom/mstar/android/tvapi/factory/FactoryManager;->_factoryManager:Lcom/mstar/android/tvapi/factory/FactoryManager;

    return-void
.end method

.method public final getAdcGainOffset(Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;)Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;
    .locals 2
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;
    .param p2    # Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;->ordinal()I

    move-result v0

    invoke-virtual {p2}, Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;->ordinal()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/mstar/android/tvapi/factory/FactoryManager;->native_getAdcGainOffset(II)Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;

    move-result-object v0

    return-object v0
.end method

.method public final getDisplayResolution()Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/mstar/android/tvapi/factory/FactoryManager;->native_getDisplayResolution()I

    move-result v0

    invoke-static {v0}, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->getOrdinalThroughValue(I)I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;->values()[Lcom/mstar/android/tvapi/factory/vo/DisplayResolutionType$EnumDisplayResolutionType;

    move-result-object v2

    aget-object v2, v2, v1

    return-object v2

    :cond_0
    new-instance v2, Lcom/mstar/android/tvapi/common/exception/TvCommonException;

    const-string v3, "funtion getDisplayResolution fail"

    invoke-direct {v2, v3}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public getEnvironmentPowerMode()Lcom/mstar/android/tvapi/factory/vo/EnumAcOnPowerOnMode;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/mstar/android/tvapi/factory/FactoryManager;->native_getEnvironmentPowerMode()I

    move-result v0

    sget-object v1, Lcom/mstar/android/tvapi/factory/vo/EnumAcOnPowerOnMode;->E_ACON_POWERON_SECONDARY:Lcom/mstar/android/tvapi/factory/vo/EnumAcOnPowerOnMode;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/factory/vo/EnumAcOnPowerOnMode;->ordinal()I

    move-result v1

    if-lt v0, v1, :cond_0

    sget-object v1, Lcom/mstar/android/tvapi/factory/vo/EnumAcOnPowerOnMode;->E_ACON_POWERON_MAX:Lcom/mstar/android/tvapi/factory/vo/EnumAcOnPowerOnMode;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/factory/vo/EnumAcOnPowerOnMode;->ordinal()I

    move-result v1

    if-le v0, v1, :cond_1

    :cond_0
    new-instance v1, Lcom/mstar/android/tvapi/common/exception/TvCommonException;

    const-string v2, "get EnvironmentPowerMode failed \n"

    invoke-direct {v1, v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    invoke-static {}, Lcom/mstar/android/tvapi/factory/vo/EnumAcOnPowerOnMode;->values()[Lcom/mstar/android/tvapi/factory/vo/EnumAcOnPowerOnMode;

    move-result-object v1

    aget-object v1, v1, v0

    return-object v1
.end method

.method public native getEnvironmentPowerOnMusicVolume()S
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public getFwVersion(Lcom/mstar/android/tvapi/factory/vo/EnumFwType;)I
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/factory/vo/EnumFwType;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/factory/vo/EnumFwType;->ordinal()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/mstar/android/tvapi/factory/FactoryManager;->native_getFwVersion(I)I

    move-result v0

    return v0
.end method

.method public final getPQVersion(Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;)Ljava/lang/String;
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;->ordinal()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/mstar/android/tvapi/factory/FactoryManager;->native_getPQVersion(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final native getPictureModeValue()Lcom/mstar/android/tvapi/factory/vo/PictureModeValue;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native getQmapCurrentTableIdx(S)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native getQmapIpName(S)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native getQmapIpNum()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native getQmapTableName(SS)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native getQmapTableNum(S)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public getResolutionMappingIndex(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)S
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/mstar/android/tvapi/factory/FactoryManager;->native_getResolutionMappingIndex(I)S

    move-result v0

    return v0
.end method

.method public native getSoftwareVersion()Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native getUartEnv()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final getUpdatePqFilePath(Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;)Ljava/lang/String;
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/factory/vo/EnumPqUpdateFile;->ordinal()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/mstar/android/tvapi/factory/FactoryManager;->getUpdatePqFilePath(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getWbGainOffset(Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;)Lcom/mstar/android/tvapi/factory/vo/WbGainOffset;
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;->getValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/mstar/android/tvapi/factory/FactoryManager;->naitve_getWbGainOffset(I)Lcom/mstar/android/tvapi/factory/vo/WbGainOffset;

    move-result-object v0

    return-object v0
.end method

.method public final getWbGainOffsetEx(Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;I)Lcom/mstar/android/tvapi/factory/vo/WbGainOffsetEx;
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;->getValue()I

    move-result v0

    invoke-direct {p0, v0, p2}, Lcom/mstar/android/tvapi/factory/FactoryManager;->native_getWbGainOffsetEx(II)Lcom/mstar/android/tvapi/factory/vo/WbGainOffsetEx;

    move-result-object v0

    return-object v0
.end method

.method public final native isAgingModeOn()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native isPVRRecordAllOn()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native isUartOn()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native isWdtOn()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native loadPqTable(II)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native native_getPQVersion(I)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public native readBytesFromI2C(I[SS)[S
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public release()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    const/4 v0, 0x0

    sput-object v0, Lcom/mstar/android/tvapi/factory/FactoryManager;->_factoryManager:Lcom/mstar/android/tvapi/factory/FactoryManager;

    return-void
.end method

.method public final native resetDisplayResolution()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native restoreDbFromUsb()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public native restoreFactoryAtvProgramTable(S)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public native restoreFactoryDtvProgramTable(S)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final setAdcGainOffset(Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;)V
    .locals 2
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;
    .param p2    # Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;
    .param p3    # Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;->ordinal()I

    move-result v0

    invoke-virtual {p2}, Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;->ordinal()I

    move-result v1

    invoke-direct {p0, v0, v1, p3}, Lcom/mstar/android/tvapi/factory/FactoryManager;->native_setAdcGainOffset(IILcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;)V

    return-void
.end method

.method public final native setBrightness(S)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native setContrast(S)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public native setDebugMode(Z)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public setEnvironmentPowerMode(Lcom/mstar/android/tvapi/factory/vo/EnumAcOnPowerOnMode;)Z
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/factory/vo/EnumAcOnPowerOnMode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/factory/vo/EnumAcOnPowerOnMode;->ordinal()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/mstar/android/tvapi/factory/FactoryManager;->native_setEnvironmentPowerMode(I)Z

    move-result v0

    return v0
.end method

.method public native setEnvironmentPowerOnMusicVolume(S)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native setFactoryVdInitParameter(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native setFactoryVdParameter(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native setHue(S)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public native setPQParameterViaUsbKey()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native setSaturation(S)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native setSharpness(S)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native setUartEnv(Z)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final setVideoTestPattern(Lcom/mstar/android/tvapi/factory/vo/EnumScreenMute;)V
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/factory/vo/EnumScreenMute;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/factory/vo/EnumScreenMute;->ordinal()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/mstar/android/tvapi/factory/FactoryManager;->native_setVideoTestPattern(I)V

    return-void
.end method

.method public final setWbGainOffset(Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;SSSSSS)V
    .locals 8
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;
    .param p2    # S
    .param p3    # S
    .param p4    # S
    .param p5    # S
    .param p6    # S
    .param p7    # S
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;->getValue()I

    move-result v1

    move-object v0, p0

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    move v7, p7

    invoke-direct/range {v0 .. v7}, Lcom/mstar/android/tvapi/factory/FactoryManager;->native_setWbGainOffset(ISSSSSS)V

    return-void
.end method

.method public final setWbGainOffsetEx(Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;IIIIIILcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)V
    .locals 9
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # I
    .param p7    # I
    .param p8    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;->getValue()I

    move-result v1

    invoke-virtual/range {p8 .. p8}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v8

    move-object v0, p0

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    move/from16 v7, p7

    invoke-direct/range {v0 .. v8}, Lcom/mstar/android/tvapi/factory/FactoryManager;->native_setWbGainOffsetEx(IIIIIIII)V

    return-void
.end method

.method public native stopTvService()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native storeDbToUsb()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public native switchUart()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native updatePqIniFiles()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native updateSscParameter()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native ursaGetVersionInfo()Lcom/mstar/android/tvapi/factory/vo/UrsaVersionInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public native writeBytesToI2C(I[S[S)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method
