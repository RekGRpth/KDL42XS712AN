.class public Lcom/mstar/android/tvapi/impl/PlayerImpl;
.super Ljava/lang/Object;
.source "PlayerImpl.java"

# interfaces
.implements Lcom/mstar/android/tvapi/atv/AtvPlayer;
.implements Lcom/mstar/android/tvapi/dtv/atsc/AtscPlayer;
.implements Lcom/mstar/android/tvapi/dtv/dvb/DvbPlayer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mstar/android/tvapi/impl/PlayerImpl$1;,
        Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;,
        Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;
    }
.end annotation


# static fields
.field private static _playerImpl:Lcom/mstar/android/tvapi/impl/PlayerImpl;


# instance fields
.field private mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

.field private mNativeContext:I

.field private mNativeSurfaceTexture:I

.field private mOnAtvPlayerEventListener:Lcom/mstar/android/tvapi/atv/listener/OnAtvPlayerEventListener;

.field private mOnDtvPlayerEventListener:Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;

.field private mOnTvPlayerEventListener:Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;

.field private mPlayerContext:I

.field private mSurface:Landroid/view/Surface;

.field private mSurfaceHolder:Landroid/view/SurfaceHolder;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v1, 0x0

    sput-object v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->_playerImpl:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    :try_start_0
    const-string v1, "playerimpl_jni"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    invoke-static {}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->native_init()V
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    sget-object v1, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Cannot load playerimpl_jni library:\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/UnsatisfiedLinkError;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    invoke-direct {v1, p0, p0, v0}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;-><init>(Lcom/mstar/android/tvapi/impl/PlayerImpl;Lcom/mstar/android/tvapi/impl/PlayerImpl;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    :goto_0
    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-direct {p0, v1}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->native_setup(Ljava/lang/Object;)V

    return-void

    :cond_0
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    if-eqz v0, :cond_1

    new-instance v1, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    invoke-direct {v1, p0, p0, v0}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;-><init>(Lcom/mstar/android/tvapi/impl/PlayerImpl;Lcom/mstar/android/tvapi/impl/PlayerImpl;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    goto :goto_0
.end method

.method private static PostEvent_4k2kHDMIDisableDualView(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_4K2K_HDMI_DISABLE_DUALVIEW:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private static PostEvent_4k2kHDMIDisablePip(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_4K2K_HDMI_DISABLE_PIP:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private static PostEvent_4k2kHDMIDisablePop(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_4K2K_HDMI_DISABLE_POP:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private static PostEvent_4k2kHDMIDisableTravelingMode(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_4K2K_HDMI_DISABLE_TRAVELINGMODE:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private static PostEvent_AlwaysTimeShiftProgramNotReady(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_PVR_NOTIFY_ALWAYS_TIMESHIFT_PROGRAM_NOTREADY:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private static PostEvent_AlwaysTimeshiftProgramReady(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_PVR_NOTIFY_ALWAYS_TIMESHIFT_PROGRAM_READY:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private static PostEvent_AudioModeChange(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_AUDIO_MODE_CHANGE:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private static PostEvent_AutoUpdateScan(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_DTV_AUTO_UPDATE_SCAN:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private static PostEvent_ChangeTtxStatus(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_CHANGE_TTX_STATUS:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private static PostEvent_ChannelNameReady(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_DTV_CHANNELNAME_READY:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private static PostEvent_CiLoadCredentialFail(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_CI_LOAD_CREDENTIAL_FAIL:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private static PostEvent_CiPlusProtection(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_PVR_NOTIFY_CI_PLUS_PROTECTION:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private static PostEvent_EpgTimerSimulcast(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_EPGTIMER_SIMULCAST:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private static PostEvent_EpgUpdateList(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_EPG_UPDATE_LIST:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private static PostEvent_GingaStatusMode(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_GINGA_STATUS_MODE:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private static PostEvent_HbbtvEvent(Ljava/lang/Object;Lcom/mstar/android/tvapi/common/vo/HbbtvEventInfo;I)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # Lcom/mstar/android/tvapi/common/vo/HbbtvEventInfo;
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_HBBTV_UI_EVENT:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private static PostEvent_HbbtvStatusMode(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_HBBTV_STATUS_MODE:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private static PostEvent_ManualTuning(Ljava/lang/Object;Lcom/mstar/android/tvapi/atv/vo/AtvEventScan;I)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # Lcom/mstar/android/tvapi/atv/vo/AtvEventScan;
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_ATV_MANUAL_TUNING_SCAN_INFO:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private static PostEvent_Mheg5EventHandler(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_MHEG5_EVENT_HANDLER:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private static PostEvent_Mheg5ReturnKey(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_MHEG5_RETURN_KEY:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private static PostEvent_Mheg5StatusMode(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_MHEG5_STATUS_MODE:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private static PostEvent_OadDownload(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_OAD_DOWNLOAD:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private static PostEvent_OadHandler(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_OAD_HANDLER:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private static PostEvent_OadTimeOut(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_OAD_TIMEOUT:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private static PostEvent_OverRun(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_PVR_NOTIFY_OVER_RUN:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private static PostEvent_ParentalControl(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_PVR_NOTIFY_PARENTAL_CONTROL:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private static PostEvent_PopupDialog(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_POPUP_DIALOG:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private static PostEvent_PopupScanDialogFreqChange(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_POPUP_SCAN_DIALOGE_FREQUENCY_CHANGE:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private static PostEvent_PopupScanDialogLossSignal(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_POPUP_SCAN_DIALOGE_LOSS_SIGNAL:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private static PostEvent_PopupScanDialogNewMultiplex(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_POPUP_SCAN_DIALOGE_NEW_MULTIPLEX:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private static PostEvent_PriComponentMissing(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_DTV_PRI_COMPONENT_MISSING:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private static PostEvent_ProgramInfoReady(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_DTV_PROGRAM_INFO_READY:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private static PostEvent_PvrCiPlusRetentionLimit(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_PVR_NOTIFY_CI_PLUS_RETENTION_LIMIT_UPDATE:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private static PostEvent_PvrPlaybackBegin(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_PVR_NOTIFY_PLAYBACK_BEGIN:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private static PostEvent_PvrPlaybackSpeedChange(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_PVR_NOTIFY_PLAYBACK_SPEED_CHANGE:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private static PostEvent_PvrPlaybackStop(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_PVR_NOTIFY_PLAYBACK_STOP:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private static PostEvent_PvrPlaybackTime(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_PVR_NOTIFY_PLAYBACK_TIME:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private static PostEvent_PvrRecordSize(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_PVR_NOTIFY_RECORD_SIZE:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private static PostEvent_PvrRecordStop(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_PVR_NOTIFY_RECORD_STOP:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private static PostEvent_PvrRecordTime(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_PVR_NOTIFY_RECORD_TIME:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private static PostEvent_PvrTimeshiftAfter(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_PVR_NOTIFY_TIMESHIFT_OVERWRITES_AFTER:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private static PostEvent_PvrTimeshiftBefore(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_PVR_NOTIFY_TIMESHIFT_OVERWRITES_BEFORE:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private static PostEvent_RctPresence(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_RCT_PRESENCE:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private static PostEvent_ScreenSaverMode(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_SCREEN_SAVER_MODE:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private static PostEvent_SignalLock(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_SIGNAL_LOCK:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private static PostEvent_SignalUnlock(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_SIGNAL_UNLOCK:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private static PostEvent_SnServiceDeadth(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    :try_start_0
    const-class v2, Lcom/mstar/android/tvapi/impl/PlayerImpl;

    monitor-enter v2
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    const-string v1, "mstar.str.suspending"

    const-string v3, "1"

    invoke-static {v1, v3}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x0

    sput-object v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->_playerImpl:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "PostEvent_SnServiceDeadth: set SystemProperties \'mstar.str.suspending\' =1"

    invoke-virtual {v1, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    monitor-exit v2

    :goto_0
    return-void

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v1
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method private static PostEvent_TsChange(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_TS_CHANGE:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private static PostEvent_UsbRemoved(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_PVR_NOTIFY_USB_REMOVED:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private native _setVideoSurface(Landroid/view/Surface;)V
.end method

.method static synthetic access$000(Lcom/mstar/android/tvapi/impl/PlayerImpl;)I
    .locals 1
    .param p0    # Lcom/mstar/android/tvapi/impl/PlayerImpl;

    iget v0, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mNativeContext:I

    return v0
.end method

.method static synthetic access$100(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;
    .locals 1
    .param p0    # Lcom/mstar/android/tvapi/impl/PlayerImpl;

    iget-object v0, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnDtvPlayerEventListener:Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;

    return-object v0
.end method

.method static synthetic access$200(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/atv/listener/OnAtvPlayerEventListener;
    .locals 1
    .param p0    # Lcom/mstar/android/tvapi/impl/PlayerImpl;

    iget-object v0, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnAtvPlayerEventListener:Lcom/mstar/android/tvapi/atv/listener/OnAtvPlayerEventListener;

    return-object v0
.end method

.method static synthetic access$300(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;
    .locals 1
    .param p0    # Lcom/mstar/android/tvapi/impl/PlayerImpl;

    iget-object v0, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnTvPlayerEventListener:Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;

    return-object v0
.end method

.method protected static getInstance(Ljava/lang/Object;)Lcom/mstar/android/tvapi/impl/PlayerImpl;
    .locals 3
    .param p0    # Ljava/lang/Object;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.mstar.android.tvapi.atv.AtvPlayerImplProxy"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "com.mstar.android.tvapi.dtv.common.DtvPlayerImplProxy"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "com.mstar.android.tvapi.common.TvPlayerImplProxy"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_0
    sget-object v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->_playerImpl:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    if-nez v1, :cond_2

    const-class v2, Lcom/mstar/android/tvapi/impl/PlayerImpl;

    monitor-enter v2

    :try_start_0
    sget-object v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->_playerImpl:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    if-nez v1, :cond_1

    new-instance v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;

    invoke-direct {v1}, Lcom/mstar/android/tvapi/impl/PlayerImpl;-><init>()V

    sput-object v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->_playerImpl:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    :cond_1
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_2
    sget-object v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->_playerImpl:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    return-object v1

    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method private final native native_atsc_setAntennaType(I)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_detectInputSource(I)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_finalize()V
.end method

.method private native native_forceVideoStandard(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_getAspectRatio()Lcom/mstar/android/tvapi/common/vo/VideoArcInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_getChinaDvbcRegion()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_getNitFrequencyByDtvRegion(I)[I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_getRfInfo(II)Lcom/mstar/android/tvapi/dtv/vo/RfInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_getVideoStandard()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_getVideoStandardDetectionState()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private static final native native_init()V
.end method

.method private final native native_openPAT(I)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_openTeletext(I)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_sendTeletextCommand(I)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_setAudioMode(I)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private native native_setAudioMute(II)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_setChannelChangeFreezeMode(Z)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_setChinaDvbcRegion(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_setCountry(I)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_setParental(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_setTimeZone(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_setup(Ljava/lang/Object;)V
.end method

.method private static postEventFromNative(Ljava/lang/Object;IIILjava/lang/Object;)V
    .locals 3
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # Ljava/lang/Object;

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    invoke-virtual {v2, p1, p2, p3, p4}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private static postEvent_AtvAutoTuning(Ljava/lang/Object;Lcom/mstar/android/tvapi/atv/vo/AtvEventScan;I)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # Lcom/mstar/android/tvapi/atv/vo/AtvEventScan;
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_ATV_AUTO_TUNING_SCAN_INFO:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private static postEvent_DtvAutoTuning(Ljava/lang/Object;Lcom/mstar/android/tvapi/dtv/vo/DtvEventScan;I)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # Lcom/mstar/android/tvapi/dtv/vo/DtvEventScan;
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_DTV_AUTO_TUNING_SCAN_INFO:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mEventHandler:Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private updateSurfaceScreenOn()V
    .locals 2

    iget-object v0, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->setKeepScreenOn(Z)V

    :cond_0
    return-void
.end method


# virtual methods
.method public final native autostartApplication()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native closeTeletext()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public detectInputSource(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Z
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->native_detectInputSource(I)Z

    move-result v0

    return v0
.end method

.method public final native disableAft()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native disableAutoClock()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native disableAutoScartOut()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native disableGigna()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native doesCcExist()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native enableAft()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native enableAutoClock()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native enableAutoScartOut()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native enableGinga()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public native enterPassToUnlockByUser(Z)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public native enterPassToUnlockUnratedByUser(Z)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public finalize()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    invoke-direct {p0}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->native_finalize()V

    const/4 v0, 0x0

    sput-object v0, Lcom/mstar/android/tvapi/impl/PlayerImpl;->_playerImpl:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    return-void
.end method

.method public forceVideoStandard(Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;)V
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->ordinal()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->native_forceVideoStandard(I)V

    return-void
.end method

.method public final native getAntennaType()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final getAspectRatio()Lcom/mstar/android/tvapi/common/vo/VideoArcInfo;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->native_getAspectRatio()Lcom/mstar/android/tvapi/common/vo/VideoArcInfo;

    move-result-object v0

    return-object v0
.end method

.method public final native getAudioInfo()Lcom/mstar/android/tvapi/dtv/vo/DtvAudioInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native getCcMode()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public getChinaDvbcRegion()Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->native_getChinaDvbcRegion()I

    move-result v0

    sget-object v1, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_OTHERS:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->ordinal()I

    move-result v1

    if-lt v0, v1, :cond_0

    sget-object v1, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_NUM:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->ordinal()I

    move-result v1

    if-le v0, v1, :cond_1

    :cond_0
    new-instance v1, Lcom/mstar/android/tvapi/common/exception/TvCommonException;

    const-string v2, "getChinaDvbcRegion failed"

    invoke-direct {v1, v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    invoke-static {}, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->values()[Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    move-result-object v1

    aget-object v1, v1, v0

    return-object v1
.end method

.method public native getCountryCode()Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native getCurrentMuxInfo()Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbMuxInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public native getCurrentRatingInformation()Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native getCurrentSignalInformation()Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public native getCurrentVChipBlockStatus()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public getDTVDemodVersion(Lcom/mstar/android/tvapi/dtv/vo/DtvDemodType;)Lcom/mstar/android/tvapi/dtv/vo/DtvDemodVersion;
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/dtv/vo/DtvDemodType;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/dtv/vo/DtvDemodType;->ordinal()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->native_getDTVDemodVersion(I)Lcom/mstar/android/tvapi/dtv/vo/DtvDemodVersion;

    move-result-object v0

    return-object v0
.end method

.method public native getDemodDVBCInfo()Lcom/mstar/android/tvapi/dtv/dvb/dvbc/DtvDemodDvbcInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public native getDemodDVBTInfo()Lcom/mstar/android/tvapi/dtv/dvb/dvbt/DtvDemodDvbtInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native getDtvRouteCount()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native getLanguageCode()Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native getMheg5PfgContent()Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native getMuxInfoByProgramNumber(IS)Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbMuxInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final getNitFrequencyByDtvRegion(Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;)[I
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->ordinal()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->native_getNitFrequencyByDtvRegion(I)[I

    move-result-object v0

    return-object v0
.end method

.method public final native getPhaseRange()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public native getRRTInformation()Lcom/mstar/android/tvapi/dtv/atsc/vo/Region5RatingInformation;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public getRfInfo(Lcom/mstar/android/tvapi/dtv/vo/RfInfo$EnumInfoType;I)Lcom/mstar/android/tvapi/dtv/vo/RfInfo;
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/dtv/vo/RfInfo$EnumInfoType;
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/dtv/vo/RfInfo$EnumInfoType;->ordinal()I

    move-result v0

    invoke-direct {p0, v0, p2}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->native_getRfInfo(II)Lcom/mstar/android/tvapi/dtv/vo/RfInfo;

    move-result-object v0

    return-object v0
.end method

.method public final native getVideoInfo()Lcom/mstar/android/tvapi/common/vo/VideoInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public getVideoStandard()Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->native_getVideoStandard()I

    move-result v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->PAL_BGHI:Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->ordinal()I

    move-result v1

    if-lt v0, v1, :cond_0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->MAX:Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->ordinal()I

    move-result v1

    if-le v0, v1, :cond_1

    :cond_0
    new-instance v1, Lcom/mstar/android/tvapi/common/exception/TvCommonException;

    const-string v2, "native_getVideoStandard failed"

    invoke-direct {v1, v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->values()[Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    move-result-object v1

    aget-object v1, v1, v0

    return-object v1
.end method

.method public getVideoStandardDetectionState()Lcom/mstar/android/tvapi/common/vo/EnumStdDetectionState;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->native_getVideoStandardDetectionState()I

    move-result v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumStdDetectionState;->VERIFY:Lcom/mstar/android/tvapi/common/vo/EnumStdDetectionState;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumStdDetectionState;->ordinal()I

    move-result v1

    if-lt v0, v1, :cond_0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumStdDetectionState;->DUMP:Lcom/mstar/android/tvapi/common/vo/EnumStdDetectionState;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumStdDetectionState;->ordinal()I

    move-result v1

    if-le v0, v1, :cond_1

    :cond_0
    new-instance v1, Lcom/mstar/android/tvapi/common/exception/TvCommonException;

    const-string v2, "native_getVideoStandardDetectionState failed"

    invoke-direct {v1, v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumStdDetectionState;->values()[Lcom/mstar/android/tvapi/common/vo/EnumStdDetectionState;

    move-result-object v1

    aget-object v1, v1, v0

    return-object v1
.end method

.method public final native hasTeletextClockSignal()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native hasTeletextSignal()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native initAtvVif()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native initOfflineDetection()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native isAftEnabled()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native isGingaEnabled()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native isGingaRunning()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native isHdmiMode()Z
.end method

.method public final native isSignalStable()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native isTeletextDisplayed()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native isTeletextSubtitleChannel()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public native native_getDTVDemodVersion(I)Lcom/mstar/android/tvapi/dtv/vo/DtvDemodVersion;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final openPAT(Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;)Z
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->ordinal()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->native_openPAT(I)Z

    move-result v0

    return v0
.end method

.method public openTeletext(Lcom/mstar/android/tvapi/common/vo/EnumTeletextMode;)Z
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumTeletextMode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumTeletextMode;->ordinal()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->native_openTeletext(I)Z

    move-result v0

    return v0
.end method

.method public final native playCurrentProgram()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native processKey(IZ)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public release()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    const/4 v0, 0x0

    sput-object v0, Lcom/mstar/android/tvapi/impl/PlayerImpl;->_playerImpl:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    return-void
.end method

.method public final native saveAtvProgram(I)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native sendMheg5Command(S)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native sendMheg5IcsCommand(IS)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final sendTeletextCommand(Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;)Z
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->ordinal()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->native_sendTeletextCommand(I)Z

    move-result v0

    return v0
.end method

.method public setAntennaType(Lcom/mstar/android/tvapi/common/vo/EnumAntennaType;)V
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumAntennaType;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumAntennaType;->ordinal()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->native_atsc_setAntennaType(I)Z

    return-void
.end method

.method public setAudioMode(Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumDtvSetAudioMode;)Z
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumDtvSetAudioMode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumDtvSetAudioMode;->ordinal()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->native_setAudioMode(I)Z

    move-result v0

    return v0
.end method

.method public setAudioMute(Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Z
    .locals 2
    .param p1    # Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;
    .param p2    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;->getValue()I

    move-result v0

    invoke-virtual {p2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->native_setAudioMute(II)Z

    move-result v0

    return v0
.end method

.method public final setCanadaEngGuideline(Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaEngRatingType;)Z
    .locals 3
    .param p1    # Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaEngRatingType;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    const/4 v2, 0x0

    sget-object v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumVChipRatingType;->E_CANADA_ENG:Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumVChipRatingType;

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumVChipRatingType;->ordinal()I

    move-result v0

    int-to-short v0, v0

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaEngRatingType;->ordinal()I

    move-result v1

    int-to-short v1, v1

    invoke-virtual {p0, v0, v1, v2, v2}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->setVChipGuideline(SSSS)Z

    move-result v0

    return v0
.end method

.method public final setCanadaFreGuideline(Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaFreRatingType;)Z
    .locals 3
    .param p1    # Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaFreRatingType;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    const/4 v2, 0x0

    sget-object v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumVChipRatingType;->E_CANADA_FRE:Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumVChipRatingType;

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumVChipRatingType;->ordinal()I

    move-result v0

    int-to-short v0, v0

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaFreRatingType;->ordinal()I

    move-result v1

    int-to-short v1, v1

    invoke-virtual {p0, v0, v1, v2, v2}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->setVChipGuideline(SSSS)Z

    move-result v0

    return v0
.end method

.method public final native setCcMode(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public setChannelChangeFreezeMode(Z)V
    .locals 0
    .param p1    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->native_setChannelChangeFreezeMode(Z)V

    return-void
.end method

.method public final setChinaDvbcRegion(Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;)V
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->ordinal()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->native_setChinaDvbcRegion(I)V

    return-void
.end method

.method public setCountry(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;)Z
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->ordinal()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->native_setCountry(I)Z

    move-result v0

    return v0
.end method

.method public final native setDebugMode(Z)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public setDisplay(Landroid/view/SurfaceHolder;)V
    .locals 1
    .param p1    # Landroid/view/SurfaceHolder;

    iput-object p1, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    move-result-object v0

    iput-object v0, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mSurface:Landroid/view/Surface;

    :goto_0
    iget-object v0, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mSurface:Landroid/view/Surface;

    invoke-direct {p0, v0}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->_setVideoSurface(Landroid/view/Surface;)V

    invoke-direct {p0}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->updateSurfaceScreenOn()V

    return-void

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mSurface:Landroid/view/Surface;

    goto :goto_0
.end method

.method public final native setDtvRoute(S)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final setDynamicGuideline(SSS)Z
    .locals 1
    .param p1    # S
    .param p2    # S
    .param p3    # S
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    sget-object v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumVChipRatingType;->E_DYNAMIC:Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumVChipRatingType;

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumVChipRatingType;->ordinal()I

    move-result v0

    int-to-short v0, v0

    invoke-virtual {p0, v0, p1, p2, p3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->setVChipGuideline(SSSS)Z

    move-result v0

    return v0
.end method

.method public final native setFavoriteRegion(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native setHPosition(I)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native setHdmiGpio([I)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native setManualTuneByFreq(I)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native setManualTuneByRf(S)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native setMirror(Z)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public setOnAtvPlayerEventListener(Lcom/mstar/android/tvapi/atv/listener/OnAtvPlayerEventListener;)V
    .locals 0
    .param p1    # Lcom/mstar/android/tvapi/atv/listener/OnAtvPlayerEventListener;

    iput-object p1, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnAtvPlayerEventListener:Lcom/mstar/android/tvapi/atv/listener/OnAtvPlayerEventListener;

    return-void
.end method

.method public setOnDtvPlayerEventListener(Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;)V
    .locals 0
    .param p1    # Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;

    iput-object p1, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnDtvPlayerEventListener:Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;

    return-void
.end method

.method public setOnTvPlayerEventListener(Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;)V
    .locals 0
    .param p1    # Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;

    iput-object p1, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnTvPlayerEventListener:Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;

    return-void
.end method

.method public setParental(Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;)V
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/dtv/vo/EnumParentalRating;->ordinal()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->native_setParental(I)V

    return-void
.end method

.method public final native setPhase(I)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native setSize(I)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public setTimeZone(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;)V
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;->getValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->native_setTimeZone(I)V

    return-void
.end method

.method public final setUsaMpaaGuideline(Lcom/mstar/android/tvapi/dtv/atsc/vo/UsaMpaaRatingType$EnumUsaMpaaRatingType;Z)Z
    .locals 4
    .param p1    # Lcom/mstar/android/tvapi/dtv/atsc/vo/UsaMpaaRatingType$EnumUsaMpaaRatingType;
    .param p2    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    const/4 v1, 0x0

    sget-object v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumVChipRatingType;->E_US_MPAA:Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumVChipRatingType;

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumVChipRatingType;->ordinal()I

    move-result v0

    int-to-short v2, v0

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/dtv/atsc/vo/UsaMpaaRatingType$EnumUsaMpaaRatingType;->ordinal()I

    move-result v0

    int-to-short v3, v0

    if-eqz p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    int-to-short v0, v0

    invoke-virtual {p0, v2, v3, v0, v1}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->setVChipGuideline(SSSS)Z

    move-result v0

    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final setUsaTvGuideline(Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumUsaTvRatingType;S)Z
    .locals 3
    .param p1    # Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumUsaTvRatingType;
    .param p2    # S
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    sget-object v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumVChipRatingType;->E_US_TV:Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumVChipRatingType;

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumVChipRatingType;->ordinal()I

    move-result v0

    int-to-short v0, v0

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumUsaTvRatingType;->ordinal()I

    move-result v1

    int-to-short v1, v1

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, p2, v2}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->setVChipGuideline(SSSS)Z

    move-result v0

    return v0
.end method

.method public native setVChipGuideline(SSSS)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native setVPosition(I)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native startApplication(JJ)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public native startAutoStandardDetection()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native startCc()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native startPcModeAtuoTune()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native stopApplication()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native stopCc()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native switchAudioTrack(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native switchDtvRoute(S)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native unlockChannel()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method
