.class Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;
.super Landroid/os/Handler;
.source "PlayerImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mstar/android/tvapi/impl/PlayerImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EventHandler"
.end annotation


# instance fields
.field private mMSrv:Lcom/mstar/android/tvapi/impl/PlayerImpl;

.field final synthetic this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;


# direct methods
.method public constructor <init>(Lcom/mstar/android/tvapi/impl/PlayerImpl;Lcom/mstar/android/tvapi/impl/PlayerImpl;Landroid/os/Looper;)V
    .locals 0
    .param p2    # Lcom/mstar/android/tvapi/impl/PlayerImpl;
    .param p3    # Landroid/os/Looper;

    iput-object p1, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    invoke-direct {p0, p3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object p2, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->mMSrv:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7
    .param p1    # Landroid/os/Message;

    const/4 v6, 0x1

    const/4 v5, 0x0

    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->mMSrv:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mNativeContext:I
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$000(Lcom/mstar/android/tvapi/impl/PlayerImpl;)I

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->values()[Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    move-result-object v1

    iget v3, p1, Landroid/os/Message;->what:I

    sget-object v4, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_MAX:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->ordinal()I

    move-result v4

    if-gt v3, v4, :cond_2

    iget v3, p1, Landroid/os/Message;->what:I

    sget-object v4, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->EV_DTV_CHANNELNAME_READY:Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->ordinal()I

    move-result v4

    if-ge v3, v4, :cond_3

    :cond_2
    const-string v3, "PlayerImpl"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Native post event out of bound:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p1, Landroid/os/Message;->what:I

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    sget-object v3, Lcom/mstar/android/tvapi/impl/PlayerImpl$1;->$SwitchMap$com$mstar$android$tvapi$impl$PlayerImpl$EVENT:[I

    iget v4, p1, Landroid/os/Message;->what:I

    aget-object v4, v1, v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/impl/PlayerImpl$EVENT;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    sget-object v3, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unknown message type "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p1, Landroid/os/Message;->what:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_0
    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnDtvPlayerEventListener:Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$100(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnDtvPlayerEventListener:Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$100(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->what:I

    invoke-interface {v3, v4}, Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;->onDtvChannelNameReady(I)Z

    goto :goto_0

    :pswitch_1
    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnAtvPlayerEventListener:Lcom/mstar/android/tvapi/atv/listener/OnAtvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$200(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/atv/listener/OnAtvPlayerEventListener;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnAtvPlayerEventListener:Lcom/mstar/android/tvapi/atv/listener/OnAtvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$200(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/atv/listener/OnAtvPlayerEventListener;

    move-result-object v4

    iget v5, p1, Landroid/os/Message;->what:I

    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v3, Lcom/mstar/android/tvapi/atv/vo/AtvEventScan;

    invoke-interface {v4, v5, v3}, Lcom/mstar/android/tvapi/atv/listener/OnAtvPlayerEventListener;->onAtvAutoTuningScanInfo(ILcom/mstar/android/tvapi/atv/vo/AtvEventScan;)Z

    goto/16 :goto_0

    :pswitch_2
    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnAtvPlayerEventListener:Lcom/mstar/android/tvapi/atv/listener/OnAtvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$200(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/atv/listener/OnAtvPlayerEventListener;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnAtvPlayerEventListener:Lcom/mstar/android/tvapi/atv/listener/OnAtvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$200(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/atv/listener/OnAtvPlayerEventListener;

    move-result-object v4

    iget v5, p1, Landroid/os/Message;->what:I

    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v3, Lcom/mstar/android/tvapi/atv/vo/AtvEventScan;

    invoke-interface {v4, v5, v3}, Lcom/mstar/android/tvapi/atv/listener/OnAtvPlayerEventListener;->onAtvManualTuningScanInfo(ILcom/mstar/android/tvapi/atv/vo/AtvEventScan;)Z

    goto/16 :goto_0

    :pswitch_3
    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnDtvPlayerEventListener:Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$100(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnDtvPlayerEventListener:Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$100(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;

    move-result-object v4

    iget v5, p1, Landroid/os/Message;->what:I

    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v3, Lcom/mstar/android/tvapi/dtv/vo/DtvEventScan;

    invoke-interface {v4, v5, v3}, Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;->onDtvAutoTuningScanInfo(ILcom/mstar/android/tvapi/dtv/vo/DtvEventScan;)Z

    goto/16 :goto_0

    :pswitch_4
    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnDtvPlayerEventListener:Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$100(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;

    move-result-object v3

    if-eqz v3, :cond_0

    const/4 v2, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    :goto_1
    sget-object v3, Lcom/mstar/android/tvapi/impl/PlayerImpl$1;->$SwitchMap$com$mstar$android$tvapi$common$vo$TvOsType$EnumInputSource:[I

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_1

    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnTvPlayerEventListener:Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$300(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->what:I

    invoke-interface {v3, v4}, Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;->onTvProgramInfoReady(I)Z

    goto/16 :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1

    :pswitch_5
    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnAtvPlayerEventListener:Lcom/mstar/android/tvapi/atv/listener/OnAtvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$200(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/atv/listener/OnAtvPlayerEventListener;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->what:I

    invoke-interface {v3, v4}, Lcom/mstar/android/tvapi/atv/listener/OnAtvPlayerEventListener;->onAtvProgramInfoReady(I)Z

    goto/16 :goto_0

    :pswitch_6
    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnDtvPlayerEventListener:Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$100(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->what:I

    invoke-interface {v3, v4}, Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;->onDtvProgramInfoReady(I)Z

    goto/16 :goto_0

    :pswitch_7
    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnAtvPlayerEventListener:Lcom/mstar/android/tvapi/atv/listener/OnAtvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$200(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/atv/listener/OnAtvPlayerEventListener;

    move-result-object v3

    if-eqz v3, :cond_0

    const/4 v2, 0x0

    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v2

    :goto_2
    sget-object v3, Lcom/mstar/android/tvapi/impl/PlayerImpl$1;->$SwitchMap$com$mstar$android$tvapi$common$vo$TvOsType$EnumInputSource:[I

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_2

    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnTvPlayerEventListener:Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$300(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->what:I

    invoke-interface {v3, v4}, Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;->onSignalLock(I)Z

    goto/16 :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_2

    :pswitch_8
    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnAtvPlayerEventListener:Lcom/mstar/android/tvapi/atv/listener/OnAtvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$200(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/atv/listener/OnAtvPlayerEventListener;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->what:I

    invoke-interface {v3, v4}, Lcom/mstar/android/tvapi/atv/listener/OnAtvPlayerEventListener;->onSignalLock(I)Z

    goto/16 :goto_0

    :pswitch_9
    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnDtvPlayerEventListener:Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$100(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->what:I

    invoke-interface {v3, v4}, Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;->onSignalLock(I)Z

    goto/16 :goto_0

    :pswitch_a
    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnAtvPlayerEventListener:Lcom/mstar/android/tvapi/atv/listener/OnAtvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$200(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/atv/listener/OnAtvPlayerEventListener;

    move-result-object v3

    if-eqz v3, :cond_0

    const/4 v2, 0x0

    :try_start_2
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    :try_end_2
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v2

    :goto_3
    sget-object v3, Lcom/mstar/android/tvapi/impl/PlayerImpl$1;->$SwitchMap$com$mstar$android$tvapi$common$vo$TvOsType$EnumInputSource:[I

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_3

    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnTvPlayerEventListener:Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$300(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->what:I

    invoke-interface {v3, v4}, Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;->onSignalUnLock(I)Z

    goto/16 :goto_0

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_3

    :pswitch_b
    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnAtvPlayerEventListener:Lcom/mstar/android/tvapi/atv/listener/OnAtvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$200(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/atv/listener/OnAtvPlayerEventListener;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->what:I

    invoke-interface {v3, v4}, Lcom/mstar/android/tvapi/atv/listener/OnAtvPlayerEventListener;->onSignalUnLock(I)Z

    goto/16 :goto_0

    :pswitch_c
    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnDtvPlayerEventListener:Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$100(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->what:I

    invoke-interface {v3, v4}, Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;->onSignalUnLock(I)Z

    goto/16 :goto_0

    :pswitch_d
    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnTvPlayerEventListener:Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$300(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnTvPlayerEventListener:Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$300(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->what:I

    iget v5, p1, Landroid/os/Message;->arg1:I

    iget v6, p1, Landroid/os/Message;->arg2:I

    invoke-interface {v3, v4, v5, v6}, Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;->onPopupDialog(III)Z

    goto/16 :goto_0

    :pswitch_e
    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnTvPlayerEventListener:Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$300(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnTvPlayerEventListener:Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$300(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->what:I

    iget v5, p1, Landroid/os/Message;->arg1:I

    invoke-interface {v3, v4, v5}, Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;->onScreenSaverMode(II)Z

    goto/16 :goto_0

    :pswitch_f
    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnDtvPlayerEventListener:Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$100(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnDtvPlayerEventListener:Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$100(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->what:I

    invoke-interface {v3, v4}, Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;->onCiLoadCredentialFail(I)Z

    goto/16 :goto_0

    :pswitch_10
    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnDtvPlayerEventListener:Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$100(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnDtvPlayerEventListener:Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$100(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->what:I

    iget v5, p1, Landroid/os/Message;->arg1:I

    invoke-interface {v3, v4, v5}, Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;->onEpgTimerSimulcast(II)Z

    goto/16 :goto_0

    :pswitch_11
    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnDtvPlayerEventListener:Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$100(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;

    move-result-object v3

    if-eqz v3, :cond_0

    iget v3, p1, Landroid/os/Message;->arg1:I

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnDtvPlayerEventListener:Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$100(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->what:I

    invoke-interface {v3, v4, v6}, Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;->onHbbtvStatusMode(IZ)Z

    goto/16 :goto_0

    :cond_4
    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnDtvPlayerEventListener:Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$100(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->what:I

    invoke-interface {v3, v4, v5}, Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;->onHbbtvStatusMode(IZ)Z

    goto/16 :goto_0

    :pswitch_12
    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnDtvPlayerEventListener:Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$100(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnDtvPlayerEventListener:Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$100(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->what:I

    iget v5, p1, Landroid/os/Message;->arg1:I

    invoke-interface {v3, v4, v5}, Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;->onMheg5StatusMode(II)Z

    goto/16 :goto_0

    :pswitch_13
    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnDtvPlayerEventListener:Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$100(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnDtvPlayerEventListener:Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$100(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->what:I

    iget v5, p1, Landroid/os/Message;->arg1:I

    invoke-interface {v3, v4, v5}, Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;->onMheg5ReturnKey(II)Z

    goto/16 :goto_0

    :pswitch_14
    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnDtvPlayerEventListener:Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$100(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnDtvPlayerEventListener:Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$100(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->what:I

    iget v5, p1, Landroid/os/Message;->arg1:I

    iget v6, p1, Landroid/os/Message;->arg2:I

    invoke-interface {v3, v4, v5, v6}, Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;->onOadHandler(III)Z

    goto/16 :goto_0

    :pswitch_15
    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnDtvPlayerEventListener:Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$100(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnDtvPlayerEventListener:Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$100(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->what:I

    iget v5, p1, Landroid/os/Message;->arg1:I

    invoke-interface {v3, v4, v5}, Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;->onOadDownload(II)Z

    goto/16 :goto_0

    :pswitch_16
    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnTvPlayerEventListener:Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$300(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnTvPlayerEventListener:Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$300(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->what:I

    iget v5, p1, Landroid/os/Message;->arg1:I

    invoke-interface {v3, v4, v5}, Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;->onPvrNotifyPlaybackTime(II)Z

    goto/16 :goto_0

    :pswitch_17
    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnTvPlayerEventListener:Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$300(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnTvPlayerEventListener:Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$300(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->what:I

    invoke-interface {v3, v4}, Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;->onPvrNotifyPlaybackSpeedChange(I)Z

    goto/16 :goto_0

    :pswitch_18
    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnTvPlayerEventListener:Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$300(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnTvPlayerEventListener:Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$300(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->what:I

    iget v5, p1, Landroid/os/Message;->arg1:I

    invoke-interface {v3, v4, v5}, Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;->onPvrNotifyRecordTime(II)Z

    goto/16 :goto_0

    :pswitch_19
    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnTvPlayerEventListener:Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$300(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnTvPlayerEventListener:Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$300(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->what:I

    iget v5, p1, Landroid/os/Message;->arg1:I

    invoke-interface {v3, v4, v5}, Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;->onPvrNotifyRecordSize(II)Z

    goto/16 :goto_0

    :pswitch_1a
    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnTvPlayerEventListener:Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$300(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnTvPlayerEventListener:Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$300(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->what:I

    invoke-interface {v3, v4}, Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;->onPvrNotifyRecordStop(I)Z

    goto/16 :goto_0

    :pswitch_1b
    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnTvPlayerEventListener:Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$300(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnTvPlayerEventListener:Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$300(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->what:I

    invoke-interface {v3, v4}, Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;->onPvrNotifyPlaybackStop(I)Z

    goto/16 :goto_0

    :pswitch_1c
    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnTvPlayerEventListener:Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$300(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnTvPlayerEventListener:Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$300(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->what:I

    invoke-interface {v3, v4}, Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;->onPvrNotifyPlaybackBegin(I)Z

    goto/16 :goto_0

    :pswitch_1d
    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnTvPlayerEventListener:Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$300(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnTvPlayerEventListener:Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$300(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->what:I

    iget v5, p1, Landroid/os/Message;->arg1:I

    invoke-interface {v3, v4, v5}, Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;->onPvrNotifyTimeShiftOverwritesBefore(II)Z

    goto/16 :goto_0

    :pswitch_1e
    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnTvPlayerEventListener:Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$300(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnTvPlayerEventListener:Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$300(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->what:I

    iget v5, p1, Landroid/os/Message;->arg1:I

    invoke-interface {v3, v4, v5}, Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;->onPvrNotifyTimeShiftOverwritesAfter(II)Z

    goto/16 :goto_0

    :pswitch_1f
    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnTvPlayerEventListener:Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$300(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnTvPlayerEventListener:Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$300(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->what:I

    invoke-interface {v3, v4}, Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;->onPvrNotifyOverRun(I)Z

    goto/16 :goto_0

    :pswitch_20
    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnTvPlayerEventListener:Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$300(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnTvPlayerEventListener:Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$300(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->what:I

    iget v5, p1, Landroid/os/Message;->arg1:I

    invoke-interface {v3, v4, v5}, Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;->onPvrNotifyUsbRemoved(II)Z

    goto/16 :goto_0

    :pswitch_21
    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnTvPlayerEventListener:Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$300(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnTvPlayerEventListener:Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$300(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->what:I

    invoke-interface {v3, v4}, Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;->onPvrNotifyCiPlusProtection(I)Z

    goto/16 :goto_0

    :pswitch_22
    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnTvPlayerEventListener:Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$300(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnTvPlayerEventListener:Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$300(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->what:I

    iget v5, p1, Landroid/os/Message;->arg1:I

    invoke-interface {v3, v4, v5}, Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;->onPvrNotifyParentalControl(II)Z

    goto/16 :goto_0

    :pswitch_23
    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnTvPlayerEventListener:Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$300(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnTvPlayerEventListener:Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$300(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->what:I

    invoke-interface {v3, v4}, Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;->onPvrNotifyAlwaysTimeShiftProgramReady(I)Z

    goto/16 :goto_0

    :pswitch_24
    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnTvPlayerEventListener:Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$300(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnTvPlayerEventListener:Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$300(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->what:I

    invoke-interface {v3, v4}, Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;->onPvrNotifyAlwaysTimeShiftProgramNotReady(I)Z

    goto/16 :goto_0

    :pswitch_25
    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnTvPlayerEventListener:Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$300(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnTvPlayerEventListener:Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$300(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->what:I

    iget v5, p1, Landroid/os/Message;->arg1:I

    invoke-interface {v3, v4, v5}, Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;->onPvrNotifyCiPlusRetentionLimitUpdate(II)Z

    goto/16 :goto_0

    :pswitch_26
    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnDtvPlayerEventListener:Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$100(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnDtvPlayerEventListener:Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$100(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->what:I

    invoke-interface {v3, v4}, Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;->onDtvAutoUpdateScan(I)Z

    goto/16 :goto_0

    :pswitch_27
    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnDtvPlayerEventListener:Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$100(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnDtvPlayerEventListener:Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$100(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->what:I

    invoke-interface {v3, v4}, Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;->onTsChange(I)Z

    goto/16 :goto_0

    :pswitch_28
    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnDtvPlayerEventListener:Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$100(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnDtvPlayerEventListener:Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$100(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->what:I

    invoke-interface {v3, v4}, Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;->onPopupScanDialogLossSignal(I)Z

    goto/16 :goto_0

    :pswitch_29
    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnDtvPlayerEventListener:Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$100(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnDtvPlayerEventListener:Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$100(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->what:I

    invoke-interface {v3, v4}, Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;->onPopupScanDialogNewMultiplex(I)Z

    goto/16 :goto_0

    :pswitch_2a
    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnDtvPlayerEventListener:Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$100(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnDtvPlayerEventListener:Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$100(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->what:I

    invoke-interface {v3, v4}, Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;->onPopupScanDialogFrequencyChange(I)Z

    goto/16 :goto_0

    :pswitch_2b
    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnDtvPlayerEventListener:Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$100(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnDtvPlayerEventListener:Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$100(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->what:I

    invoke-interface {v3, v4}, Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;->onRctPresence(I)Z

    goto/16 :goto_0

    :pswitch_2c
    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnDtvPlayerEventListener:Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$100(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;

    move-result-object v3

    if-eqz v3, :cond_0

    iget v3, p1, Landroid/os/Message;->arg1:I

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnDtvPlayerEventListener:Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$100(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->what:I

    invoke-interface {v3, v4, v6}, Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;->onChangeTtxStatus(IZ)Z

    goto/16 :goto_0

    :cond_5
    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnDtvPlayerEventListener:Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$100(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->what:I

    invoke-interface {v3, v4, v5}, Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;->onChangeTtxStatus(IZ)Z

    goto/16 :goto_0

    :pswitch_2d
    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnDtvPlayerEventListener:Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$100(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnDtvPlayerEventListener:Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$100(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->what:I

    invoke-interface {v3, v4}, Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;->onDtvPriComponentMissing(I)Z

    goto/16 :goto_0

    :pswitch_2e
    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnDtvPlayerEventListener:Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$100(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;

    move-result-object v3

    if-eqz v3, :cond_0

    iget v3, p1, Landroid/os/Message;->arg1:I

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnDtvPlayerEventListener:Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$100(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->what:I

    invoke-interface {v3, v4, v6}, Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;->onAudioModeChange(IZ)Z

    goto/16 :goto_0

    :cond_6
    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnDtvPlayerEventListener:Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$100(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->what:I

    invoke-interface {v3, v4, v5}, Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;->onAudioModeChange(IZ)Z

    goto/16 :goto_0

    :pswitch_2f
    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnDtvPlayerEventListener:Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$100(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnDtvPlayerEventListener:Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$100(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->what:I

    iget v5, p1, Landroid/os/Message;->arg1:I

    invoke-interface {v3, v4, v5}, Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;->onMheg5EventHandler(II)Z

    goto/16 :goto_0

    :pswitch_30
    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnDtvPlayerEventListener:Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$100(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnDtvPlayerEventListener:Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$100(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->what:I

    iget v5, p1, Landroid/os/Message;->arg1:I

    invoke-interface {v3, v4, v5}, Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;->onOadTimeout(II)Z

    goto/16 :goto_0

    :pswitch_31
    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnDtvPlayerEventListener:Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$100(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;

    move-result-object v3

    if-eqz v3, :cond_0

    iget v3, p1, Landroid/os/Message;->arg1:I

    if-eqz v3, :cond_7

    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnDtvPlayerEventListener:Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$100(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->what:I

    invoke-interface {v3, v4, v6}, Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;->onGingaStatusMode(IZ)Z

    goto/16 :goto_0

    :cond_7
    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnDtvPlayerEventListener:Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$100(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->what:I

    invoke-interface {v3, v4, v5}, Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;->onGingaStatusMode(IZ)Z

    goto/16 :goto_0

    :pswitch_32
    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnTvPlayerEventListener:Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$300(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnTvPlayerEventListener:Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$300(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;

    move-result-object v4

    iget v5, p1, Landroid/os/Message;->what:I

    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v3, Lcom/mstar/android/tvapi/common/vo/HbbtvEventInfo;

    invoke-interface {v4, v5, v3}, Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;->onHbbtvUiEvent(ILcom/mstar/android/tvapi/common/vo/HbbtvEventInfo;)Z

    goto/16 :goto_0

    :pswitch_33
    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnTvPlayerEventListener:Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$300(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnTvPlayerEventListener:Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$300(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->what:I

    iget v5, p1, Landroid/os/Message;->arg1:I

    invoke-interface {v3, v4, v5}, Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;->onEpgUpdateList(II)Z

    goto/16 :goto_0

    :pswitch_34
    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnTvPlayerEventListener:Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$300(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnTvPlayerEventListener:Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$300(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->what:I

    iget v5, p1, Landroid/os/Message;->arg1:I

    iget v6, p1, Landroid/os/Message;->arg2:I

    invoke-interface {v3, v4, v5, v6}, Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;->on4k2kHDMIDisablePip(III)Z

    goto/16 :goto_0

    :pswitch_35
    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnTvPlayerEventListener:Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$300(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnTvPlayerEventListener:Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$300(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->what:I

    iget v5, p1, Landroid/os/Message;->arg1:I

    iget v6, p1, Landroid/os/Message;->arg2:I

    invoke-interface {v3, v4, v5, v6}, Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;->on4k2kHDMIDisablePop(III)Z

    goto/16 :goto_0

    :pswitch_36
    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnTvPlayerEventListener:Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$300(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnTvPlayerEventListener:Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$300(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->what:I

    iget v5, p1, Landroid/os/Message;->arg1:I

    iget v6, p1, Landroid/os/Message;->arg2:I

    invoke-interface {v3, v4, v5, v6}, Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;->on4k2kHDMIDisableDualView(III)Z

    goto/16 :goto_0

    :pswitch_37
    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnTvPlayerEventListener:Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$300(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/mstar/android/tvapi/impl/PlayerImpl$EventHandler;->this$0:Lcom/mstar/android/tvapi/impl/PlayerImpl;

    # getter for: Lcom/mstar/android/tvapi/impl/PlayerImpl;->mOnTvPlayerEventListener:Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;
    invoke-static {v3}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->access$300(Lcom/mstar/android/tvapi/impl/PlayerImpl;)Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->what:I

    iget v5, p1, Landroid/os/Message;->arg1:I

    iget v6, p1, Landroid/os/Message;->arg2:I

    invoke-interface {v3, v4, v5, v6}, Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;->on4k2kHDMIDisableTravelingMode(III)Z

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_7
        :pswitch_a
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_21
        :pswitch_22
        :pswitch_23
        :pswitch_24
        :pswitch_25
        :pswitch_26
        :pswitch_27
        :pswitch_28
        :pswitch_29
        :pswitch_2a
        :pswitch_2b
        :pswitch_2c
        :pswitch_2d
        :pswitch_2e
        :pswitch_2f
        :pswitch_30
        :pswitch_31
        :pswitch_32
        :pswitch_33
        :pswitch_34
        :pswitch_35
        :pswitch_36
        :pswitch_37
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_5
        :pswitch_6
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_8
        :pswitch_9
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x1
        :pswitch_b
        :pswitch_c
    .end packed-switch
.end method
