.class public Lcom/mstar/android/tvapi/dtv/vo/DtvNetworkRegion;
.super Ljava/lang/Object;
.source "DtvNetworkRegion.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mstar/android/tvapi/dtv/vo/DtvNetworkRegion;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public networkID:S

.field public networkName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/mstar/android/tvapi/dtv/vo/DtvNetworkRegion$1;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/dtv/vo/DtvNetworkRegion$1;-><init>()V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/vo/DtvNetworkRegion;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvNetworkRegion;->networkName:Ljava/lang/String;

    const/4 v0, 0x0

    iput-short v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvNetworkRegion;->networkID:S

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1    # Landroid/os/Parcel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvNetworkRegion;->networkName:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvNetworkRegion;->networkID:S

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvNetworkRegion;->networkName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvNetworkRegion;->networkID:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
