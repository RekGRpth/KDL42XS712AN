.class public final enum Lcom/mstar/android/tvapi/dtv/vo/EnumRfChannelBandwidth;
.super Ljava/lang/Enum;
.source "EnumRfChannelBandwidth.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/tvapi/dtv/vo/EnumRfChannelBandwidth;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/tvapi/dtv/vo/EnumRfChannelBandwidth;

.field public static final enum E_7_MHZ:Lcom/mstar/android/tvapi/dtv/vo/EnumRfChannelBandwidth;

.field public static final enum E_8_MHZ:Lcom/mstar/android/tvapi/dtv/vo/EnumRfChannelBandwidth;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/dtv/vo/EnumRfChannelBandwidth;

    const-string v1, "E_7_MHZ"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v3, v2}, Lcom/mstar/android/tvapi/dtv/vo/EnumRfChannelBandwidth;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/vo/EnumRfChannelBandwidth;->E_7_MHZ:Lcom/mstar/android/tvapi/dtv/vo/EnumRfChannelBandwidth;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/vo/EnumRfChannelBandwidth;

    const-string v1, "E_8_MHZ"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v4, v2}, Lcom/mstar/android/tvapi/dtv/vo/EnumRfChannelBandwidth;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/vo/EnumRfChannelBandwidth;->E_8_MHZ:Lcom/mstar/android/tvapi/dtv/vo/EnumRfChannelBandwidth;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/mstar/android/tvapi/dtv/vo/EnumRfChannelBandwidth;

    sget-object v1, Lcom/mstar/android/tvapi/dtv/vo/EnumRfChannelBandwidth;->E_7_MHZ:Lcom/mstar/android/tvapi/dtv/vo/EnumRfChannelBandwidth;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mstar/android/tvapi/dtv/vo/EnumRfChannelBandwidth;->E_8_MHZ:Lcom/mstar/android/tvapi/dtv/vo/EnumRfChannelBandwidth;

    aput-object v1, v0, v4

    sput-object v0, Lcom/mstar/android/tvapi/dtv/vo/EnumRfChannelBandwidth;->$VALUES:[Lcom/mstar/android/tvapi/dtv/vo/EnumRfChannelBandwidth;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/mstar/android/tvapi/dtv/vo/EnumRfChannelBandwidth;->value:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/tvapi/dtv/vo/EnumRfChannelBandwidth;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/tvapi/dtv/vo/EnumRfChannelBandwidth;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/dtv/vo/EnumRfChannelBandwidth;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/tvapi/dtv/vo/EnumRfChannelBandwidth;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/dtv/vo/EnumRfChannelBandwidth;->$VALUES:[Lcom/mstar/android/tvapi/dtv/vo/EnumRfChannelBandwidth;

    invoke-virtual {v0}, [Lcom/mstar/android/tvapi/dtv/vo/EnumRfChannelBandwidth;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/tvapi/dtv/vo/EnumRfChannelBandwidth;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    iget v0, p0, Lcom/mstar/android/tvapi/dtv/vo/EnumRfChannelBandwidth;->value:I

    return v0
.end method
