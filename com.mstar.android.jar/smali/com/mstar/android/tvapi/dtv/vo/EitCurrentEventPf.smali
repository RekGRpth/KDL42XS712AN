.class public Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;
.super Ljava/lang/Object;
.source "EitCurrentEventPf.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public contentNibbleLevel1:S

.field public contentNibbleLevel2:S

.field public durationInSeconds:I

.field public eventName:Ljava/lang/String;

.field public extendedEventItem:Ljava/lang/String;

.field public extendedEventText:Ljava/lang/String;

.field public isEndTimeDayLightTime:Z

.field public isScrambled:Z

.field public isStartTimeDayLightTime:Z

.field public parentalControl:S

.field public parentalObjectiveContent:S

.field public shortEventText:Ljava/lang/String;

.field public stEndTime:Landroid/text/format/Time;

.field public stStartTime:Landroid/text/format/Time;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf$1;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf$1;-><init>()V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;->eventName:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;->shortEventText:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;->extendedEventItem:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;->extendedEventText:Ljava/lang/String;

    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    iput-object v0, p0, Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;->stStartTime:Landroid/text/format/Time;

    iput-boolean v1, p0, Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;->isStartTimeDayLightTime:Z

    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    iput-object v0, p0, Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;->stEndTime:Landroid/text/format/Time;

    iput-boolean v1, p0, Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;->isEndTimeDayLightTime:Z

    iput v1, p0, Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;->durationInSeconds:I

    iput-boolean v1, p0, Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;->isScrambled:Z

    iput-short v1, p0, Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;->parentalControl:S

    iput-short v1, p0, Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;->parentalObjectiveContent:S

    iput-short v1, p0, Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;->contentNibbleLevel1:S

    iput-short v1, p0, Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;->contentNibbleLevel2:S

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 5
    .param p1    # Landroid/os/Parcel;

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;->eventName:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;->shortEventText:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;->extendedEventItem:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;->extendedEventText:Ljava/lang/String;

    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    iput-object v0, p0, Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;->stStartTime:Landroid/text/format/Time;

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;->stStartTime:Landroid/text/format/Time;

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v3

    invoke-virtual {v0, v3, v4}, Landroid/text/format/Time;->set(J)V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;->isStartTimeDayLightTime:Z

    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    iput-object v0, p0, Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;->stEndTime:Landroid/text/format/Time;

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;->stEndTime:Landroid/text/format/Time;

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v3

    invoke-virtual {v0, v3, v4}, Landroid/text/format/Time;->set(J)V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;->isEndTimeDayLightTime:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;->durationInSeconds:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_2

    :goto_2
    iput-boolean v1, p0, Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;->isScrambled:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;->parentalControl:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;->parentalObjectiveContent:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;->contentNibbleLevel1:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;->contentNibbleLevel2:S

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v1, v2

    goto :goto_2
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 5
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;->eventName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;->shortEventText:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;->extendedEventItem:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;->extendedEventText:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;->stStartTime:Landroid/text/format/Time;

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v3

    invoke-virtual {p1, v3, v4}, Landroid/os/Parcel;->writeLong(J)V

    iget-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;->isStartTimeDayLightTime:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;->stEndTime:Landroid/text/format/Time;

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v3

    invoke-virtual {p1, v3, v4}, Landroid/os/Parcel;->writeLong(J)V

    iget-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;->isEndTimeDayLightTime:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;->durationInSeconds:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;->isScrambled:Z

    if-eqz v0, :cond_2

    :goto_2
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;->parentalControl:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;->parentalObjectiveContent:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;->contentNibbleLevel1:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;->contentNibbleLevel2:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v1, v2

    goto :goto_2
.end method
