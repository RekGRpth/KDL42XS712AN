.class public final enum Lcom/mstar/android/tvapi/dtv/vo/EnumDtvScanStatus;
.super Ljava/lang/Enum;
.source "EnumDtvScanStatus.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/tvapi/dtv/vo/EnumDtvScanStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/tvapi/dtv/vo/EnumDtvScanStatus;

.field public static final enum E_STATUS_AUTOTUNING_PROGRESS:Lcom/mstar/android/tvapi/dtv/vo/EnumDtvScanStatus;

.field public static final enum E_STATUS_EXIT_TO_DL:Lcom/mstar/android/tvapi/dtv/vo/EnumDtvScanStatus;

.field public static final enum E_STATUS_GET_PROGRAMS:Lcom/mstar/android/tvapi/dtv/vo/EnumDtvScanStatus;

.field public static final enum E_STATUS_LCN_CONFLICT:Lcom/mstar/android/tvapi/dtv/vo/EnumDtvScanStatus;

.field public static final enum E_STATUS_SCAN_END:Lcom/mstar/android/tvapi/dtv/vo/EnumDtvScanStatus;

.field public static final enum E_STATUS_SCAN_NONE:Lcom/mstar/android/tvapi/dtv/vo/EnumDtvScanStatus;

.field public static final enum E_STATUS_SCAN_SETFIRSTPROG_DONE:Lcom/mstar/android/tvapi/dtv/vo/EnumDtvScanStatus;

.field public static final enum E_STATUS_SET_FAVORITE_REGION:Lcom/mstar/android/tvapi/dtv/vo/EnumDtvScanStatus;

.field public static final enum E_STATUS_SET_REGION:Lcom/mstar/android/tvapi/dtv/vo/EnumDtvScanStatus;

.field public static final enum E_STATUS_SIGNAL_QUALITY:Lcom/mstar/android/tvapi/dtv/vo/EnumDtvScanStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/dtv/vo/EnumDtvScanStatus;

    const-string v1, "E_STATUS_SCAN_NONE"

    invoke-direct {v0, v1, v3}, Lcom/mstar/android/tvapi/dtv/vo/EnumDtvScanStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/vo/EnumDtvScanStatus;->E_STATUS_SCAN_NONE:Lcom/mstar/android/tvapi/dtv/vo/EnumDtvScanStatus;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/vo/EnumDtvScanStatus;

    const-string v1, "E_STATUS_AUTOTUNING_PROGRESS"

    invoke-direct {v0, v1, v4}, Lcom/mstar/android/tvapi/dtv/vo/EnumDtvScanStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/vo/EnumDtvScanStatus;->E_STATUS_AUTOTUNING_PROGRESS:Lcom/mstar/android/tvapi/dtv/vo/EnumDtvScanStatus;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/vo/EnumDtvScanStatus;

    const-string v1, "E_STATUS_SIGNAL_QUALITY"

    invoke-direct {v0, v1, v5}, Lcom/mstar/android/tvapi/dtv/vo/EnumDtvScanStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/vo/EnumDtvScanStatus;->E_STATUS_SIGNAL_QUALITY:Lcom/mstar/android/tvapi/dtv/vo/EnumDtvScanStatus;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/vo/EnumDtvScanStatus;

    const-string v1, "E_STATUS_GET_PROGRAMS"

    invoke-direct {v0, v1, v6}, Lcom/mstar/android/tvapi/dtv/vo/EnumDtvScanStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/vo/EnumDtvScanStatus;->E_STATUS_GET_PROGRAMS:Lcom/mstar/android/tvapi/dtv/vo/EnumDtvScanStatus;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/vo/EnumDtvScanStatus;

    const-string v1, "E_STATUS_SET_REGION"

    invoke-direct {v0, v1, v7}, Lcom/mstar/android/tvapi/dtv/vo/EnumDtvScanStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/vo/EnumDtvScanStatus;->E_STATUS_SET_REGION:Lcom/mstar/android/tvapi/dtv/vo/EnumDtvScanStatus;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/vo/EnumDtvScanStatus;

    const-string v1, "E_STATUS_SET_FAVORITE_REGION"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/dtv/vo/EnumDtvScanStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/vo/EnumDtvScanStatus;->E_STATUS_SET_FAVORITE_REGION:Lcom/mstar/android/tvapi/dtv/vo/EnumDtvScanStatus;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/vo/EnumDtvScanStatus;

    const-string v1, "E_STATUS_EXIT_TO_DL"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/dtv/vo/EnumDtvScanStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/vo/EnumDtvScanStatus;->E_STATUS_EXIT_TO_DL:Lcom/mstar/android/tvapi/dtv/vo/EnumDtvScanStatus;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/vo/EnumDtvScanStatus;

    const-string v1, "E_STATUS_LCN_CONFLICT"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/dtv/vo/EnumDtvScanStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/vo/EnumDtvScanStatus;->E_STATUS_LCN_CONFLICT:Lcom/mstar/android/tvapi/dtv/vo/EnumDtvScanStatus;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/vo/EnumDtvScanStatus;

    const-string v1, "E_STATUS_SCAN_END"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/dtv/vo/EnumDtvScanStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/vo/EnumDtvScanStatus;->E_STATUS_SCAN_END:Lcom/mstar/android/tvapi/dtv/vo/EnumDtvScanStatus;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/vo/EnumDtvScanStatus;

    const-string v1, "E_STATUS_SCAN_SETFIRSTPROG_DONE"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/dtv/vo/EnumDtvScanStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/vo/EnumDtvScanStatus;->E_STATUS_SCAN_SETFIRSTPROG_DONE:Lcom/mstar/android/tvapi/dtv/vo/EnumDtvScanStatus;

    const/16 v0, 0xa

    new-array v0, v0, [Lcom/mstar/android/tvapi/dtv/vo/EnumDtvScanStatus;

    sget-object v1, Lcom/mstar/android/tvapi/dtv/vo/EnumDtvScanStatus;->E_STATUS_SCAN_NONE:Lcom/mstar/android/tvapi/dtv/vo/EnumDtvScanStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mstar/android/tvapi/dtv/vo/EnumDtvScanStatus;->E_STATUS_AUTOTUNING_PROGRESS:Lcom/mstar/android/tvapi/dtv/vo/EnumDtvScanStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/android/tvapi/dtv/vo/EnumDtvScanStatus;->E_STATUS_SIGNAL_QUALITY:Lcom/mstar/android/tvapi/dtv/vo/EnumDtvScanStatus;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mstar/android/tvapi/dtv/vo/EnumDtvScanStatus;->E_STATUS_GET_PROGRAMS:Lcom/mstar/android/tvapi/dtv/vo/EnumDtvScanStatus;

    aput-object v1, v0, v6

    sget-object v1, Lcom/mstar/android/tvapi/dtv/vo/EnumDtvScanStatus;->E_STATUS_SET_REGION:Lcom/mstar/android/tvapi/dtv/vo/EnumDtvScanStatus;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/mstar/android/tvapi/dtv/vo/EnumDtvScanStatus;->E_STATUS_SET_FAVORITE_REGION:Lcom/mstar/android/tvapi/dtv/vo/EnumDtvScanStatus;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/mstar/android/tvapi/dtv/vo/EnumDtvScanStatus;->E_STATUS_EXIT_TO_DL:Lcom/mstar/android/tvapi/dtv/vo/EnumDtvScanStatus;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/mstar/android/tvapi/dtv/vo/EnumDtvScanStatus;->E_STATUS_LCN_CONFLICT:Lcom/mstar/android/tvapi/dtv/vo/EnumDtvScanStatus;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/mstar/android/tvapi/dtv/vo/EnumDtvScanStatus;->E_STATUS_SCAN_END:Lcom/mstar/android/tvapi/dtv/vo/EnumDtvScanStatus;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/mstar/android/tvapi/dtv/vo/EnumDtvScanStatus;->E_STATUS_SCAN_SETFIRSTPROG_DONE:Lcom/mstar/android/tvapi/dtv/vo/EnumDtvScanStatus;

    aput-object v2, v0, v1

    sput-object v0, Lcom/mstar/android/tvapi/dtv/vo/EnumDtvScanStatus;->$VALUES:[Lcom/mstar/android/tvapi/dtv/vo/EnumDtvScanStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/tvapi/dtv/vo/EnumDtvScanStatus;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/tvapi/dtv/vo/EnumDtvScanStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/dtv/vo/EnumDtvScanStatus;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/tvapi/dtv/vo/EnumDtvScanStatus;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/dtv/vo/EnumDtvScanStatus;->$VALUES:[Lcom/mstar/android/tvapi/dtv/vo/EnumDtvScanStatus;

    invoke-virtual {v0}, [Lcom/mstar/android/tvapi/dtv/vo/EnumDtvScanStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/tvapi/dtv/vo/EnumDtvScanStatus;

    return-object v0
.end method
