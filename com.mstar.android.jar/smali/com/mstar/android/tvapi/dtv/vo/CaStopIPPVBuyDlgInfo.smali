.class public Lcom/mstar/android/tvapi/dtv/vo/CaStopIPPVBuyDlgInfo;
.super Ljava/lang/Object;
.source "CaStopIPPVBuyDlgInfo.java"


# instance fields
.field public bBuyProgram:Z

.field public pbyPinCode:Ljava/lang/String;

.field public sEcmPid:S

.field public sIPPVBuyDlgState:S

.field public sPrice:S

.field public sPriceCode:S


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-short v1, p0, Lcom/mstar/android/tvapi/dtv/vo/CaStopIPPVBuyDlgInfo;->sIPPVBuyDlgState:S

    iput-boolean v1, p0, Lcom/mstar/android/tvapi/dtv/vo/CaStopIPPVBuyDlgInfo;->bBuyProgram:Z

    iput-short v1, p0, Lcom/mstar/android/tvapi/dtv/vo/CaStopIPPVBuyDlgInfo;->sEcmPid:S

    const-string v0, ""

    iput-object v0, p0, Lcom/mstar/android/tvapi/dtv/vo/CaStopIPPVBuyDlgInfo;->pbyPinCode:Ljava/lang/String;

    iput-short v1, p0, Lcom/mstar/android/tvapi/dtv/vo/CaStopIPPVBuyDlgInfo;->sPrice:S

    iput-short v1, p0, Lcom/mstar/android/tvapi/dtv/vo/CaStopIPPVBuyDlgInfo;->sPriceCode:S

    return-void
.end method


# virtual methods
.method public getPbyPinCode()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/vo/CaStopIPPVBuyDlgInfo;->pbyPinCode:Ljava/lang/String;

    return-object v0
.end method

.method public getsEcmPid()S
    .locals 1

    iget-short v0, p0, Lcom/mstar/android/tvapi/dtv/vo/CaStopIPPVBuyDlgInfo;->sEcmPid:S

    return v0
.end method

.method public getsIPPVBuyDlgState()S
    .locals 1

    iget-short v0, p0, Lcom/mstar/android/tvapi/dtv/vo/CaStopIPPVBuyDlgInfo;->sIPPVBuyDlgState:S

    return v0
.end method

.method public getsPrice()S
    .locals 1

    iget-short v0, p0, Lcom/mstar/android/tvapi/dtv/vo/CaStopIPPVBuyDlgInfo;->sPrice:S

    return v0
.end method

.method public getsPriceCode()S
    .locals 1

    iget-short v0, p0, Lcom/mstar/android/tvapi/dtv/vo/CaStopIPPVBuyDlgInfo;->sPriceCode:S

    return v0
.end method

.method public isbBuyProgram()Z
    .locals 1

    iget-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/vo/CaStopIPPVBuyDlgInfo;->bBuyProgram:Z

    return v0
.end method

.method public setPbyPinCode(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mstar/android/tvapi/dtv/vo/CaStopIPPVBuyDlgInfo;->pbyPinCode:Ljava/lang/String;

    return-void
.end method

.method public setbBuyProgram(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mstar/android/tvapi/dtv/vo/CaStopIPPVBuyDlgInfo;->bBuyProgram:Z

    return-void
.end method

.method public setsEcmPid(S)V
    .locals 0
    .param p1    # S

    iput-short p1, p0, Lcom/mstar/android/tvapi/dtv/vo/CaStopIPPVBuyDlgInfo;->sEcmPid:S

    return-void
.end method

.method public setsIPPVBuyDlgState(S)V
    .locals 0
    .param p1    # S

    iput-short p1, p0, Lcom/mstar/android/tvapi/dtv/vo/CaStopIPPVBuyDlgInfo;->sIPPVBuyDlgState:S

    return-void
.end method

.method public setsPrice(S)V
    .locals 0
    .param p1    # S

    iput-short p1, p0, Lcom/mstar/android/tvapi/dtv/vo/CaStopIPPVBuyDlgInfo;->sPrice:S

    return-void
.end method

.method public setsPriceCode(S)V
    .locals 0
    .param p1    # S

    iput-short p1, p0, Lcom/mstar/android/tvapi/dtv/vo/CaStopIPPVBuyDlgInfo;->sPriceCode:S

    return-void
.end method
