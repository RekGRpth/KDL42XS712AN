.class public Lcom/mstar/android/tvapi/dtv/vo/CaEmailHeadInfo;
.super Ljava/lang/Object;
.source "CaEmailHeadInfo.java"


# instance fields
.field public m_bNewEmail:S

.field public pcEmailHead:Ljava/lang/String;

.field public sEmailHeadState:S

.field public wActionID:I

.field public wCreateTime:I

.field public wImportance:S


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-short v1, p0, Lcom/mstar/android/tvapi/dtv/vo/CaEmailHeadInfo;->sEmailHeadState:S

    iput v1, p0, Lcom/mstar/android/tvapi/dtv/vo/CaEmailHeadInfo;->wActionID:I

    iput v1, p0, Lcom/mstar/android/tvapi/dtv/vo/CaEmailHeadInfo;->wCreateTime:I

    iput-short v1, p0, Lcom/mstar/android/tvapi/dtv/vo/CaEmailHeadInfo;->wImportance:S

    const-string v0, ""

    iput-object v0, p0, Lcom/mstar/android/tvapi/dtv/vo/CaEmailHeadInfo;->pcEmailHead:Ljava/lang/String;

    iput-short v1, p0, Lcom/mstar/android/tvapi/dtv/vo/CaEmailHeadInfo;->m_bNewEmail:S

    return-void
.end method


# virtual methods
.method public getM_bNewEmail()S
    .locals 1

    iget-short v0, p0, Lcom/mstar/android/tvapi/dtv/vo/CaEmailHeadInfo;->m_bNewEmail:S

    return v0
.end method

.method public getPcEmailHead()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/vo/CaEmailHeadInfo;->pcEmailHead:Ljava/lang/String;

    return-object v0
.end method

.method public getsEmailHeadState()S
    .locals 1

    iget-short v0, p0, Lcom/mstar/android/tvapi/dtv/vo/CaEmailHeadInfo;->sEmailHeadState:S

    return v0
.end method

.method public getwActionID()I
    .locals 1

    iget v0, p0, Lcom/mstar/android/tvapi/dtv/vo/CaEmailHeadInfo;->wActionID:I

    return v0
.end method

.method public getwCreateTime()I
    .locals 1

    iget v0, p0, Lcom/mstar/android/tvapi/dtv/vo/CaEmailHeadInfo;->wCreateTime:I

    return v0
.end method

.method public getwImportance()S
    .locals 1

    iget-short v0, p0, Lcom/mstar/android/tvapi/dtv/vo/CaEmailHeadInfo;->wImportance:S

    return v0
.end method

.method public setM_bNewEmail(S)V
    .locals 0
    .param p1    # S

    iput-short p1, p0, Lcom/mstar/android/tvapi/dtv/vo/CaEmailHeadInfo;->m_bNewEmail:S

    return-void
.end method

.method public setPcEmailHead(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mstar/android/tvapi/dtv/vo/CaEmailHeadInfo;->pcEmailHead:Ljava/lang/String;

    return-void
.end method

.method public setsEmailHeadState(S)V
    .locals 0
    .param p1    # S

    iput-short p1, p0, Lcom/mstar/android/tvapi/dtv/vo/CaEmailHeadInfo;->sEmailHeadState:S

    return-void
.end method

.method public setwActionID(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mstar/android/tvapi/dtv/vo/CaEmailHeadInfo;->wActionID:I

    return-void
.end method

.method public setwCreateTime(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mstar/android/tvapi/dtv/vo/CaEmailHeadInfo;->wCreateTime:I

    return-void
.end method

.method public setwImportance(S)V
    .locals 0
    .param p1    # S

    iput-short p1, p0, Lcom/mstar/android/tvapi/dtv/vo/CaEmailHeadInfo;->wImportance:S

    return-void
.end method
