.class public Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;
.super Ljava/lang/Object;
.source "DtvEventComponentInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public audioTrackNum:S

.field public ccService:Z

.field private enAspectRatio:I

.field private enGenreType:I

.field private enHd:I

.field public isAd:Z

.field public mheg5Service:Z

.field public parentalRating:S

.field public subtitleNum:S

.field public subtitleService:Z

.field public teletextService:Z

.field private videoType:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo$1;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo$1;-><init>()V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;->videoType:I

    iput-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;->mheg5Service:Z

    iput-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;->subtitleService:Z

    iput-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;->teletextService:Z

    iput-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;->ccService:Z

    iput v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;->enHd:I

    iput-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;->isAd:Z

    iput-short v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;->audioTrackNum:S

    iput-short v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;->subtitleNum:S

    iput v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;->enAspectRatio:I

    iput v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;->enGenreType:I

    iput-short v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;->parentalRating:S

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3
    .param p1    # Landroid/os/Parcel;

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;->videoType:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;->mheg5Service:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;->subtitleService:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;->teletextService:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_3

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;->ccService:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;->enHd:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_4

    :goto_4
    iput-boolean v1, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;->isAd:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;->audioTrackNum:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;->subtitleNum:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;->enAspectRatio:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;->enGenreType:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;->parentalRating:S

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_2

    :cond_3
    move v0, v2

    goto :goto_3

    :cond_4
    move v1, v2

    goto :goto_4
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getAspectRatioCode()Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumAspectRatioCode;
    .locals 2

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumAspectRatioCode;->values()[Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumAspectRatioCode;

    move-result-object v0

    iget v1, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;->enAspectRatio:I

    aget-object v0, v0, v1

    return-object v0
.end method

.method public getDtvVideoQuality()Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumDtvVideoQuality;
    .locals 2

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumDtvVideoQuality;->values()[Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumDtvVideoQuality;

    move-result-object v0

    iget v1, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;->enHd:I

    aget-object v0, v0, v1

    return-object v0
.end method

.method public getGenreType()Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getVideoType()Lcom/mstar/android/tvapi/common/vo/EnumVideoType;
    .locals 2

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumVideoType;->values()[Lcom/mstar/android/tvapi/common/vo/EnumVideoType;

    move-result-object v0

    iget v1, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;->videoType:I

    aget-object v0, v0, v1

    return-object v0
.end method

.method public setAspectRatioCode(Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumAspectRatioCode;)V
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumAspectRatioCode;

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumAspectRatioCode;->ordinal()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;->enAspectRatio:I

    return-void
.end method

.method public setDtvVideoQuality(Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumDtvVideoQuality;)V
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumDtvVideoQuality;

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumDtvVideoQuality;->ordinal()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;->enHd:I

    return-void
.end method

.method public setGenreType(Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;)V
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;->getValue()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;->enGenreType:I

    return-void
.end method

.method public setVideoType(Lcom/mstar/android/tvapi/common/vo/EnumVideoType;)V
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumVideoType;

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumVideoType;->ordinal()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;->videoType:I

    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;->videoType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;->mheg5Service:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;->subtitleService:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;->teletextService:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;->ccService:Z

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;->enHd:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;->isAd:Z

    if-eqz v0, :cond_4

    :goto_4
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;->audioTrackNum:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;->subtitleNum:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;->enAspectRatio:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;->enGenreType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;->parentalRating:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_2

    :cond_3
    move v0, v2

    goto :goto_3

    :cond_4
    move v1, v2

    goto :goto_4
.end method
