.class public Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;
.super Ljava/lang/Object;
.source "EpgEventInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo$EnumEpgFunctionStatus;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;",
            ">;"
        }
    .end annotation
.end field

.field private static enumhash:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public description:Ljava/lang/String;

.field public durationTime:I

.field public endTime:I

.field public eventId:I

.field protected functionStatus:I

.field public genre:S

.field public isScrambled:Z

.field public name:Ljava/lang/String;

.field public originalStartTime:I

.field public parentalRating:S

.field public startTime:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->enumhash:Ljava/util/Hashtable;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo$1;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo$1;-><init>()V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v1, p0, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->startTime:I

    iput v1, p0, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->endTime:I

    iput v1, p0, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->durationTime:I

    const-string v0, ""

    iput-object v0, p0, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->name:Ljava/lang/String;

    iput v1, p0, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->eventId:I

    iput-boolean v1, p0, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->isScrambled:Z

    iput-short v1, p0, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->genre:S

    iput-short v1, p0, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->parentalRating:S

    const-string v0, ""

    iput-object v0, p0, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->description:Ljava/lang/String;

    iput v1, p0, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->originalStartTime:I

    iput v1, p0, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->functionStatus:I

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .param p1    # Landroid/os/Parcel;

    const/4 v0, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->startTime:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->endTime:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->durationTime:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->name:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->eventId:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->isScrambled:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->genre:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->parentalRating:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->description:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->originalStartTime:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->functionStatus:I

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic access$000()Ljava/util/Hashtable;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->enumhash:Ljava/util/Hashtable;

    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getEpgFunctionStatus()Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo$EnumEpgFunctionStatus;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvOutOfBoundException;
        }
    .end annotation

    iget v1, p0, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->functionStatus:I

    invoke-static {v1}, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo$EnumEpgFunctionStatus;->getOrdinalThroughValue(I)I

    move-result v0

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo$EnumEpgFunctionStatus;->values()[Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo$EnumEpgFunctionStatus;

    move-result-object v1

    aget-object v1, v1, v0

    return-object v1
.end method

.method public setEpgFunctionStatus(Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo$EnumEpgFunctionStatus;)V
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo$EnumEpgFunctionStatus;

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo$EnumEpgFunctionStatus;->getValue()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->functionStatus:I

    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget v0, p0, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->startTime:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->endTime:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->durationTime:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->name:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget v0, p0, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->eventId:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->isScrambled:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->genre:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->parentalRating:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->description:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget v0, p0, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->originalStartTime:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->functionStatus:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
