.class public final enum Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;
.super Ljava/lang/Enum;
.source "DtvType.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mstar/android/tvapi/dtv/vo/DtvType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EnumSubtitlingType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;

.field public static final enum E_SUBTITLING_TYPE_HH_16X9:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;

.field public static final enum E_SUBTITLING_TYPE_HH_221X100:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;

.field public static final enum E_SUBTITLING_TYPE_HH_4X3:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;

.field public static final enum E_SUBTITLING_TYPE_HH_HD:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;

.field public static final enum E_SUBTITLING_TYPE_HH_NO:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;

.field public static final enum E_SUBTITLING_TYPE_NORMAL_16X9:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;

.field public static final enum E_SUBTITLING_TYPE_NORMAL_221X100:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;

.field public static final enum E_SUBTITLING_TYPE_NORMAL_4X3:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;

.field public static final enum E_SUBTITLING_TYPE_NORMAL_HD:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;

.field public static final enum E_SUBTITLING_TYPE_NORMAL_NO:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;

.field public static final enum E_SUBTITLING_TYPE_TELETEXT:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;

.field public static final enum E_SUBTITLING_TYPE_TELETEXT_HOH:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x0

    const/4 v5, 0x2

    const/4 v4, 0x1

    new-instance v0, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;

    const-string v1, "E_SUBTITLING_TYPE_TELETEXT"

    invoke-direct {v0, v1, v6, v4}, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;->E_SUBTITLING_TYPE_TELETEXT:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;

    const-string v1, "E_SUBTITLING_TYPE_TELETEXT_HOH"

    invoke-direct {v0, v1, v4, v5}, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;->E_SUBTITLING_TYPE_TELETEXT_HOH:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;

    const-string v1, "E_SUBTITLING_TYPE_NORMAL_NO"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v5, v2}, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;->E_SUBTITLING_TYPE_NORMAL_NO:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;

    const-string v1, "E_SUBTITLING_TYPE_NORMAL_4X3"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v7, v2}, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;->E_SUBTITLING_TYPE_NORMAL_4X3:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;

    const-string v1, "E_SUBTITLING_TYPE_NORMAL_16X9"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v8, v2}, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;->E_SUBTITLING_TYPE_NORMAL_16X9:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;

    const-string v1, "E_SUBTITLING_TYPE_NORMAL_221X100"

    const/4 v2, 0x5

    const/16 v3, 0x13

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;->E_SUBTITLING_TYPE_NORMAL_221X100:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;

    const-string v1, "E_SUBTITLING_TYPE_NORMAL_HD"

    const/4 v2, 0x6

    const/16 v3, 0x14

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;->E_SUBTITLING_TYPE_NORMAL_HD:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;

    const-string v1, "E_SUBTITLING_TYPE_HH_NO"

    const/4 v2, 0x7

    const/16 v3, 0x20

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;->E_SUBTITLING_TYPE_HH_NO:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;

    const-string v1, "E_SUBTITLING_TYPE_HH_4X3"

    const/16 v2, 0x8

    const/16 v3, 0x21

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;->E_SUBTITLING_TYPE_HH_4X3:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;

    const-string v1, "E_SUBTITLING_TYPE_HH_16X9"

    const/16 v2, 0x9

    const/16 v3, 0x22

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;->E_SUBTITLING_TYPE_HH_16X9:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;

    const-string v1, "E_SUBTITLING_TYPE_HH_221X100"

    const/16 v2, 0xa

    const/16 v3, 0x23

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;->E_SUBTITLING_TYPE_HH_221X100:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;

    const-string v1, "E_SUBTITLING_TYPE_HH_HD"

    const/16 v2, 0xb

    const/16 v3, 0x24

    invoke-direct {v0, v1, v2, v3}, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;->E_SUBTITLING_TYPE_HH_HD:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;

    const/16 v0, 0xc

    new-array v0, v0, [Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;

    sget-object v1, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;->E_SUBTITLING_TYPE_TELETEXT:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;->E_SUBTITLING_TYPE_TELETEXT_HOH:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;->E_SUBTITLING_TYPE_NORMAL_NO:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;->E_SUBTITLING_TYPE_NORMAL_4X3:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;->E_SUBTITLING_TYPE_NORMAL_16X9:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;->E_SUBTITLING_TYPE_NORMAL_221X100:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;->E_SUBTITLING_TYPE_NORMAL_HD:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;->E_SUBTITLING_TYPE_HH_NO:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;->E_SUBTITLING_TYPE_HH_4X3:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;->E_SUBTITLING_TYPE_HH_16X9:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;->E_SUBTITLING_TYPE_HH_221X100:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;->E_SUBTITLING_TYPE_HH_HD:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;->$VALUES:[Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;->value:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;->$VALUES:[Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;

    invoke-virtual {v0}, [Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    iget v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;->value:I

    return v0
.end method
