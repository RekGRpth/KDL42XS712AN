.class public Lcom/mstar/android/tvapi/dtv/vo/DtvType;
.super Lcom/mstar/android/tvapi/common/vo/TvOsType;
.source "DtvType.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;,
        Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumAspectRatioCode;,
        Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumDtvVideoQuality;,
        Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumDtvSetAudioMode;,
        Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgDescriptionType;,
        Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumSubtitlingType;
    }
.end annotation


# static fields
.field private static enumhash:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/vo/DtvType;->enumhash:Ljava/util/Hashtable;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/mstar/android/tvapi/common/vo/TvOsType;-><init>()V

    return-void
.end method

.method static synthetic access$000()Ljava/util/Hashtable;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/dtv/vo/DtvType;->enumhash:Ljava/util/Hashtable;

    return-object v0
.end method
