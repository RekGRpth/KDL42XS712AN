.class public Lcom/mstar/android/tvapi/dtv/vo/EpgFirstMatchHdCast;
.super Ljava/lang/Object;
.source "EpgFirstMatchHdCast.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mstar/android/tvapi/dtv/vo/EpgFirstMatchHdCast;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public epgEventInfoVO:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

.field public isResolvable:Z

.field public serviceName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/mstar/android/tvapi/dtv/vo/EpgFirstMatchHdCast$1;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/dtv/vo/EpgFirstMatchHdCast$1;-><init>()V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/vo/EpgFirstMatchHdCast;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;-><init>()V

    iput-object v0, p0, Lcom/mstar/android/tvapi/dtv/vo/EpgFirstMatchHdCast;->epgEventInfoVO:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    const-string v0, ""

    iput-object v0, p0, Lcom/mstar/android/tvapi/dtv/vo/EpgFirstMatchHdCast;->serviceName:Ljava/lang/String;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/vo/EpgFirstMatchHdCast;->isResolvable:Z

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .param p1    # Landroid/os/Parcel;

    const/4 v1, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    iput-object v0, p0, Lcom/mstar/android/tvapi/dtv/vo/EpgFirstMatchHdCast;->epgEventInfoVO:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mstar/android/tvapi/dtv/vo/EpgFirstMatchHdCast;->serviceName:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/vo/EpgFirstMatchHdCast;->isResolvable:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/mstar/android/tvapi/dtv/vo/EpgFirstMatchHdCast$1;)V
    .locals 0
    .param p1    # Landroid/os/Parcel;
    .param p2    # Lcom/mstar/android/tvapi/dtv/vo/EpgFirstMatchHdCast$1;

    invoke-direct {p0, p1}, Lcom/mstar/android/tvapi/dtv/vo/EpgFirstMatchHdCast;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/vo/EpgFirstMatchHdCast;->epgEventInfoVO:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-virtual {v0, p1, p2}, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->writeToParcel(Landroid/os/Parcel;I)V

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/vo/EpgFirstMatchHdCast;->serviceName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/vo/EpgFirstMatchHdCast;->isResolvable:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
