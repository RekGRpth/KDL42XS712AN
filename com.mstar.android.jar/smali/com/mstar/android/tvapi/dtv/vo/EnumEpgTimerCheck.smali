.class public final enum Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;
.super Ljava/lang/Enum;
.source "EnumEpgTimerCheck.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;

.field public static final enum E_ENDTIME_BEFORE_START:Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;

.field public static final enum E_ENDTIME_EXCEED_PERIOD:Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;

.field public static final enum E_FULL:Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;

.field public static final enum E_NONE:Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;

.field public static final enum E_OVERLAY:Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;

.field public static final enum E_PAST:Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;

.field public static final enum E_SUCCESS:Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;

    const-string v1, "E_NONE"

    invoke-direct {v0, v1, v3}, Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;->E_NONE:Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;

    const-string v1, "E_SUCCESS"

    invoke-direct {v0, v1, v4}, Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;->E_SUCCESS:Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;

    const-string v1, "E_PAST"

    invoke-direct {v0, v1, v5}, Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;->E_PAST:Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;

    const-string v1, "E_OVERLAY"

    invoke-direct {v0, v1, v6}, Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;->E_OVERLAY:Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;

    const-string v1, "E_ENDTIME_BEFORE_START"

    invoke-direct {v0, v1, v7}, Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;->E_ENDTIME_BEFORE_START:Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;

    const-string v1, "E_ENDTIME_EXCEED_PERIOD"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;->E_ENDTIME_EXCEED_PERIOD:Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;

    const-string v1, "E_FULL"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;->E_FULL:Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;

    const/4 v0, 0x7

    new-array v0, v0, [Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;

    sget-object v1, Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;->E_NONE:Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;->E_SUCCESS:Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;->E_PAST:Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;->E_OVERLAY:Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;

    aput-object v1, v0, v6

    sget-object v1, Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;->E_ENDTIME_BEFORE_START:Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;->E_ENDTIME_EXCEED_PERIOD:Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;->E_FULL:Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;

    aput-object v2, v0, v1

    sput-object v0, Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;->$VALUES:[Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;->$VALUES:[Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;

    invoke-virtual {v0}, [Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/tvapi/dtv/vo/EnumEpgTimerCheck;

    return-object v0
.end method
