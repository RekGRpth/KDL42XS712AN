.class public Lcom/mstar/android/tvapi/dtv/vo/EpgHdSimulcast;
.super Ljava/lang/Object;
.source "EpgHdSimulcast.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mstar/android/tvapi/dtv/vo/EpgHdSimulcast;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public isHdEeventResolvable:Z

.field public isHdServiceResolvable:Z

.field public isSimulcast:Z

.field public serviceName:Ljava/lang/String;

.field public serviceNumber:I

.field public serviceType:S

.field public stEventInfo:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/mstar/android/tvapi/dtv/vo/EpgHdSimulcast$1;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/dtv/vo/EpgHdSimulcast$1;-><init>()V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/vo/EpgHdSimulcast;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v1, p0, Lcom/mstar/android/tvapi/dtv/vo/EpgHdSimulcast;->isSimulcast:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/mstar/android/tvapi/dtv/vo/EpgHdSimulcast;->serviceName:Ljava/lang/String;

    iput-short v1, p0, Lcom/mstar/android/tvapi/dtv/vo/EpgHdSimulcast;->serviceType:S

    iput v1, p0, Lcom/mstar/android/tvapi/dtv/vo/EpgHdSimulcast;->serviceNumber:I

    iput-boolean v1, p0, Lcom/mstar/android/tvapi/dtv/vo/EpgHdSimulcast;->isHdEeventResolvable:Z

    iput-boolean v1, p0, Lcom/mstar/android/tvapi/dtv/vo/EpgHdSimulcast;->isHdServiceResolvable:Z

    new-instance v0, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;-><init>()V

    iput-object v0, p0, Lcom/mstar/android/tvapi/dtv/vo/EpgHdSimulcast;->stEventInfo:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 3
    .param p1    # Landroid/os/Parcel;

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/vo/EpgHdSimulcast;->isSimulcast:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mstar/android/tvapi/dtv/vo/EpgHdSimulcast;->serviceName:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/dtv/vo/EpgHdSimulcast;->serviceType:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/dtv/vo/EpgHdSimulcast;->serviceNumber:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/vo/EpgHdSimulcast;->isHdEeventResolvable:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_2

    :goto_2
    iput-boolean v1, p0, Lcom/mstar/android/tvapi/dtv/vo/EpgHdSimulcast;->isHdServiceResolvable:Z

    sget-object v0, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    iput-object v0, p0, Lcom/mstar/android/tvapi/dtv/vo/EpgHdSimulcast;->stEventInfo:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v1, v2

    goto :goto_2
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/mstar/android/tvapi/dtv/vo/EpgHdSimulcast$1;)V
    .locals 0
    .param p1    # Landroid/os/Parcel;
    .param p2    # Lcom/mstar/android/tvapi/dtv/vo/EpgHdSimulcast$1;

    invoke-direct {p0, p1}, Lcom/mstar/android/tvapi/dtv/vo/EpgHdSimulcast;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/vo/EpgHdSimulcast;->isSimulcast:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/vo/EpgHdSimulcast;->serviceName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/dtv/vo/EpgHdSimulcast;->serviceType:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/dtv/vo/EpgHdSimulcast;->serviceNumber:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/vo/EpgHdSimulcast;->isHdEeventResolvable:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/vo/EpgHdSimulcast;->isHdServiceResolvable:Z

    if-eqz v0, :cond_2

    :goto_2
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/vo/EpgHdSimulcast;->stEventInfo:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-virtual {v0, p1, p2}, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->writeToParcel(Landroid/os/Parcel;I)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v1, v2

    goto :goto_2
.end method
