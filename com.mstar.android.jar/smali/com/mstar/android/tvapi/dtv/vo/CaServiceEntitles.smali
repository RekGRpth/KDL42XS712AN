.class public Lcom/mstar/android/tvapi/dtv/vo/CaServiceEntitles;
.super Ljava/lang/Object;
.source "CaServiceEntitles.java"


# instance fields
.field public cEntitles:[Lcom/mstar/android/tvapi/dtv/vo/CaServiceEntitle;

.field public sEntitlesState:S

.field public sProductCount:S


# direct methods
.method public constructor <init>()V
    .locals 4

    const/16 v3, 0x12c

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-array v1, v3, [Lcom/mstar/android/tvapi/dtv/vo/CaServiceEntitle;

    iput-object v1, p0, Lcom/mstar/android/tvapi/dtv/vo/CaServiceEntitles;->cEntitles:[Lcom/mstar/android/tvapi/dtv/vo/CaServiceEntitle;

    iput-short v2, p0, Lcom/mstar/android/tvapi/dtv/vo/CaServiceEntitles;->sEntitlesState:S

    iput-short v2, p0, Lcom/mstar/android/tvapi/dtv/vo/CaServiceEntitles;->sProductCount:S

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    iget-object v1, p0, Lcom/mstar/android/tvapi/dtv/vo/CaServiceEntitles;->cEntitles:[Lcom/mstar/android/tvapi/dtv/vo/CaServiceEntitle;

    new-instance v2, Lcom/mstar/android/tvapi/dtv/vo/CaServiceEntitle;

    invoke-direct {v2}, Lcom/mstar/android/tvapi/dtv/vo/CaServiceEntitle;-><init>()V

    aput-object v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method
