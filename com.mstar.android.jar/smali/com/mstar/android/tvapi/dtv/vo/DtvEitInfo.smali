.class public Lcom/mstar/android/tvapi/dtv/vo/DtvEitInfo;
.super Ljava/lang/Object;
.source "DtvEitInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mstar/android/tvapi/dtv/vo/DtvEitInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public eitCurrentEventPf:Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;

.field public present:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/mstar/android/tvapi/dtv/vo/DtvEitInfo$1;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/dtv/vo/DtvEitInfo$1;-><init>()V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/vo/DtvEitInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;-><init>()V

    iput-object v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvEitInfo;->eitCurrentEventPf:Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvEitInfo;->present:Z

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .param p1    # Landroid/os/Parcel;

    const/4 v1, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;

    iput-object v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvEitInfo;->eitCurrentEventPf:Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvEitInfo;->present:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvEitInfo;->eitCurrentEventPf:Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;

    invoke-virtual {v0, p1, p2}, Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;->writeToParcel(Landroid/os/Parcel;I)V

    iget-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/vo/DtvEitInfo;->present:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
