.class public final Lcom/mstar/android/tvapi/dtv/common/CiManager;
.super Ljava/lang/Object;
.source "CiManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mstar/android/tvapi/dtv/common/CiManager$1;,
        Lcom/mstar/android/tvapi/dtv/common/CiManager$EventHandler;,
        Lcom/mstar/android/tvapi/dtv/common/CiManager$CredentialValidDateRange;,
        Lcom/mstar/android/tvapi/dtv/common/CiManager$OnCiEventListener;,
        Lcom/mstar/android/tvapi/dtv/common/CiManager$EVENT;
    }
.end annotation


# static fields
.field private static _cimanager:Lcom/mstar/android/tvapi/dtv/common/CiManager;


# instance fields
.field private mEventHandler:Lcom/mstar/android/tvapi/dtv/common/CiManager$EventHandler;

.field private mNativeContext:I

.field private mOnEventListener:Lcom/mstar/android/tvapi/dtv/common/CiManager$OnCiEventListener;

.field private mcimanagerContext:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    :try_start_0
    const-string v1, "cimanager_jni"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/CiManager;->native_init()V
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const/4 v1, 0x0

    sput-object v1, Lcom/mstar/android/tvapi/dtv/common/CiManager;->_cimanager:Lcom/mstar/android/tvapi/dtv/common/CiManager;

    return-void

    :catch_0
    move-exception v0

    sget-object v1, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Cannot load cimanager_jni library:\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/UnsatisfiedLinkError;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Lcom/mstar/android/tvapi/dtv/common/CiManager$EventHandler;

    invoke-direct {v1, p0, p0, v0}, Lcom/mstar/android/tvapi/dtv/common/CiManager$EventHandler;-><init>(Lcom/mstar/android/tvapi/dtv/common/CiManager;Lcom/mstar/android/tvapi/dtv/common/CiManager;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/mstar/android/tvapi/dtv/common/CiManager;->mEventHandler:Lcom/mstar/android/tvapi/dtv/common/CiManager$EventHandler;

    :goto_0
    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-direct {p0, v1}, Lcom/mstar/android/tvapi/dtv/common/CiManager;->native_setup(Ljava/lang/Object;)V

    return-void

    :cond_0
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    if-eqz v0, :cond_1

    new-instance v1, Lcom/mstar/android/tvapi/dtv/common/CiManager$EventHandler;

    invoke-direct {v1, p0, p0, v0}, Lcom/mstar/android/tvapi/dtv/common/CiManager$EventHandler;-><init>(Lcom/mstar/android/tvapi/dtv/common/CiManager;Lcom/mstar/android/tvapi/dtv/common/CiManager;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/mstar/android/tvapi/dtv/common/CiManager;->mEventHandler:Lcom/mstar/android/tvapi/dtv/common/CiManager$EventHandler;

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/mstar/android/tvapi/dtv/common/CiManager;->mEventHandler:Lcom/mstar/android/tvapi/dtv/common/CiManager$EventHandler;

    goto :goto_0
.end method

.method private static PostEvent_AutotestMessageShown(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/dtv/common/CiManager;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, v1, Lcom/mstar/android/tvapi/dtv/common/CiManager;->mEventHandler:Lcom/mstar/android/tvapi/dtv/common/CiManager$EventHandler;

    if-eqz v2, :cond_1

    iget-object v2, v1, Lcom/mstar/android/tvapi/dtv/common/CiManager;->mEventHandler:Lcom/mstar/android/tvapi/dtv/common/CiManager$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/dtv/common/CiManager$EVENT;->EV_CIMMI_UI_AUTOTEST_MESSAGE_SHOWN:Lcom/mstar/android/tvapi/dtv/common/CiManager$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/dtv/common/CiManager$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/dtv/common/CiManager$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/dtv/common/CiManager;->mEventHandler:Lcom/mstar/android/tvapi/dtv/common/CiManager$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/dtv/common/CiManager$EventHandler;->sendMessage(Landroid/os/Message;)Z

    :cond_1
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "\n NativeCI callback, PostEvent_AutotestMessageShown"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static PostEvent_CardInserted(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/dtv/common/CiManager;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, v1, Lcom/mstar/android/tvapi/dtv/common/CiManager;->mEventHandler:Lcom/mstar/android/tvapi/dtv/common/CiManager$EventHandler;

    if-eqz v2, :cond_1

    iget-object v2, v1, Lcom/mstar/android/tvapi/dtv/common/CiManager;->mEventHandler:Lcom/mstar/android/tvapi/dtv/common/CiManager$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/dtv/common/CiManager$EVENT;->EV_CIMMI_UI_CARD_INSERTED:Lcom/mstar/android/tvapi/dtv/common/CiManager$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/dtv/common/CiManager$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/dtv/common/CiManager$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/dtv/common/CiManager;->mEventHandler:Lcom/mstar/android/tvapi/dtv/common/CiManager$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/dtv/common/CiManager$EventHandler;->sendMessage(Landroid/os/Message;)Z

    :cond_1
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "\n NativeCI callback, PostEvent_CardInserted"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static PostEvent_CardRemoved(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/dtv/common/CiManager;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, v1, Lcom/mstar/android/tvapi/dtv/common/CiManager;->mEventHandler:Lcom/mstar/android/tvapi/dtv/common/CiManager$EventHandler;

    if-eqz v2, :cond_1

    iget-object v2, v1, Lcom/mstar/android/tvapi/dtv/common/CiManager;->mEventHandler:Lcom/mstar/android/tvapi/dtv/common/CiManager$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/dtv/common/CiManager$EVENT;->EV_CIMMI_UI_CARD_REMOVED:Lcom/mstar/android/tvapi/dtv/common/CiManager$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/dtv/common/CiManager$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/dtv/common/CiManager$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/dtv/common/CiManager;->mEventHandler:Lcom/mstar/android/tvapi/dtv/common/CiManager$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/dtv/common/CiManager$EventHandler;->sendMessage(Landroid/os/Message;)Z

    :cond_1
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "\n NativeCI callback, PostEvent_CardRemoved"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static PostEvent_CloseMmi(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/dtv/common/CiManager;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, v1, Lcom/mstar/android/tvapi/dtv/common/CiManager;->mEventHandler:Lcom/mstar/android/tvapi/dtv/common/CiManager$EventHandler;

    if-eqz v2, :cond_1

    iget-object v2, v1, Lcom/mstar/android/tvapi/dtv/common/CiManager;->mEventHandler:Lcom/mstar/android/tvapi/dtv/common/CiManager$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/dtv/common/CiManager$EVENT;->EV_CIMMI_UI_CLOSEMMI:Lcom/mstar/android/tvapi/dtv/common/CiManager$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/dtv/common/CiManager$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/dtv/common/CiManager$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/dtv/common/CiManager;->mEventHandler:Lcom/mstar/android/tvapi/dtv/common/CiManager$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/dtv/common/CiManager$EventHandler;->sendMessage(Landroid/os/Message;)Z

    :cond_1
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "\n NativeCI callback, PostEvent_CloseMmi"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static PostEvent_DataReady(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/dtv/common/CiManager;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, v1, Lcom/mstar/android/tvapi/dtv/common/CiManager;->mEventHandler:Lcom/mstar/android/tvapi/dtv/common/CiManager$EventHandler;

    if-eqz v2, :cond_1

    iget-object v2, v1, Lcom/mstar/android/tvapi/dtv/common/CiManager;->mEventHandler:Lcom/mstar/android/tvapi/dtv/common/CiManager$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/dtv/common/CiManager$EVENT;->EV_CIMMI_UI_DATA_READY:Lcom/mstar/android/tvapi/dtv/common/CiManager$EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/dtv/common/CiManager$EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/dtv/common/CiManager$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/dtv/common/CiManager;->mEventHandler:Lcom/mstar/android/tvapi/dtv/common/CiManager$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/dtv/common/CiManager$EventHandler;->sendMessage(Landroid/os/Message;)Z

    :cond_1
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "\n NativeCI callback, PostEvent_DataReady"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static PostEvent_SnServiceDeadth(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    :try_start_0
    const-class v2, Lcom/mstar/android/tvapi/dtv/common/CiManager;

    monitor-enter v2
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    const-string v1, "mstar.str.suspending"

    const-string v3, "1"

    invoke-static {v1, v3}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x0

    sput-object v1, Lcom/mstar/android/tvapi/dtv/common/CiManager;->_cimanager:Lcom/mstar/android/tvapi/dtv/common/CiManager;

    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "PostEvent_SnServiceDeadth: set SystemProperties \'mstar.str.suspending\' =1"

    invoke-virtual {v1, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    monitor-exit v2

    :goto_0
    return-void

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v1
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/mstar/android/tvapi/dtv/common/CiManager;)I
    .locals 1
    .param p0    # Lcom/mstar/android/tvapi/dtv/common/CiManager;

    iget v0, p0, Lcom/mstar/android/tvapi/dtv/common/CiManager;->mNativeContext:I

    return v0
.end method

.method static synthetic access$100(Lcom/mstar/android/tvapi/dtv/common/CiManager;)Lcom/mstar/android/tvapi/dtv/common/CiManager$OnCiEventListener;
    .locals 1
    .param p0    # Lcom/mstar/android/tvapi/dtv/common/CiManager;

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/common/CiManager;->mOnEventListener:Lcom/mstar/android/tvapi/dtv/common/CiManager$OnCiEventListener;

    return-object v0
.end method

.method protected static getInstance()Lcom/mstar/android/tvapi/dtv/common/CiManager;
    .locals 2

    sget-object v0, Lcom/mstar/android/tvapi/dtv/common/CiManager;->_cimanager:Lcom/mstar/android/tvapi/dtv/common/CiManager;

    if-nez v0, :cond_1

    const-class v1, Lcom/mstar/android/tvapi/dtv/common/CiManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/mstar/android/tvapi/dtv/common/CiManager;->_cimanager:Lcom/mstar/android/tvapi/dtv/common/CiManager;

    if-nez v0, :cond_0

    new-instance v0, Lcom/mstar/android/tvapi/dtv/common/CiManager;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/dtv/common/CiManager;-><init>()V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/common/CiManager;->_cimanager:Lcom/mstar/android/tvapi/dtv/common/CiManager;

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    sget-object v0, Lcom/mstar/android/tvapi/dtv/common/CiManager;->_cimanager:Lcom/mstar/android/tvapi/dtv/common/CiManager;

    return-object v0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private final native native_finalize()V
.end method

.method private final native native_getCardState()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native native_getMMIType()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private static final native native_init()V
.end method

.method private final native native_setup(Ljava/lang/Object;)V
.end method

.method private static postEventFromNative(Ljava/lang/Object;IIILjava/lang/Object;)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # Ljava/lang/Object;

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/dtv/common/CiManager;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, v1, Lcom/mstar/android/tvapi/dtv/common/CiManager;->mEventHandler:Lcom/mstar/android/tvapi/dtv/common/CiManager$EventHandler;

    if-eqz v2, :cond_1

    iget-object v2, v1, Lcom/mstar/android/tvapi/dtv/common/CiManager;->mEventHandler:Lcom/mstar/android/tvapi/dtv/common/CiManager$EventHandler;

    invoke-virtual {v2, p1, p2, p3, p4}, Lcom/mstar/android/tvapi/dtv/common/CiManager$EventHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/dtv/common/CiManager;->mEventHandler:Lcom/mstar/android/tvapi/dtv/common/CiManager$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/dtv/common/CiManager$EventHandler;->sendMessage(Landroid/os/Message;)Z

    :cond_1
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "\n NativeCI callback, postEventFromNative"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public final native answerEnq(Ljava/lang/String;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native answerMenu(S)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native backEnq()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native backMenu()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native close()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native enterMenu()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method protected finalize()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    invoke-direct {p0}, Lcom/mstar/android/tvapi/dtv/common/CiManager;->native_finalize()V

    return-void
.end method

.method public getCardState()Lcom/mstar/android/tvapi/dtv/vo/EnumCardState;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/mstar/android/tvapi/dtv/common/CiManager;->native_getCardState()I

    move-result v1

    sget-object v2, Lcom/mstar/android/tvapi/dtv/vo/EnumCardState;->E_NO:Lcom/mstar/android/tvapi/dtv/vo/EnumCardState;

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/dtv/vo/EnumCardState;->ordinal()I

    move-result v2

    if-lt v1, v2, :cond_0

    sget-object v2, Lcom/mstar/android/tvapi/dtv/vo/EnumCardState;->E_MAX:Lcom/mstar/android/tvapi/dtv/vo/EnumCardState;

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/dtv/vo/EnumCardState;->ordinal()I

    move-result v2

    if-le v1, v2, :cond_1

    :cond_0
    new-instance v2, Lcom/mstar/android/tvapi/common/exception/TvCommonException;

    const-string v3, "getCardState failed"

    invoke-direct {v2, v3}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_1
    invoke-static {}, Lcom/mstar/android/tvapi/dtv/vo/EnumCardState;->values()[Lcom/mstar/android/tvapi/dtv/vo/EnumCardState;

    move-result-object v2

    aget-object v2, v2, v1

    return-object v2
.end method

.method public final native getCiCredentialValidRange()Lcom/mstar/android/tvapi/dtv/common/CiManager$CredentialValidDateRange;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native getEnqAnsLength()S
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native getEnqBlindAnswer()S
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native getEnqLength()S
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native getEnqString()Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native getListBottomLength()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native getListBottomString()Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native getListChoiceNumber()S
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native getListSelectionString(I)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native getListSubtitleLength()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public native getListSubtitleString()Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native getListTitleLength()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public native getListTitleString()Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native getMenuBottomLength()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native getMenuBottomString()Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native getMenuChoiceNumber()S
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native getMenuSelectionString(I)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native getMenuString()Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native getMenuSubtitleLength()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native getMenuSubtitleString()Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native getMenuTitleLength()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native getMenuTitleString()Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public getMmiType()Lcom/mstar/android/tvapi/dtv/vo/EnumMmiType;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/mstar/android/tvapi/dtv/common/CiManager;->native_getMMIType()I

    move-result v1

    if-ltz v1, :cond_0

    sget-object v2, Lcom/mstar/android/tvapi/dtv/vo/EnumMmiType;->E_MAX:Lcom/mstar/android/tvapi/dtv/vo/EnumMmiType;

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/dtv/vo/EnumMmiType;->ordinal()I

    move-result v2

    if-le v1, v2, :cond_1

    :cond_0
    new-instance v2, Lcom/mstar/android/tvapi/common/exception/TvCommonException;

    const-string v3, "getMMIType failed"

    invoke-direct {v2, v3}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_1
    invoke-static {}, Lcom/mstar/android/tvapi/dtv/vo/EnumMmiType;->values()[Lcom/mstar/android/tvapi/dtv/vo/EnumMmiType;

    move-result-object v2

    aget-object v2, v2, v1

    return-object v2
.end method

.method public final native isCiCredentialModeValid(S)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native isCiMenuOn()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native isDataExisted()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native setCiCredentialMode(S)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native setDebugMode(Z)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public setOnCiEventListener(Lcom/mstar/android/tvapi/dtv/common/CiManager$OnCiEventListener;)V
    .locals 0
    .param p1    # Lcom/mstar/android/tvapi/dtv/common/CiManager$OnCiEventListener;

    iput-object p1, p0, Lcom/mstar/android/tvapi/dtv/common/CiManager;->mOnEventListener:Lcom/mstar/android/tvapi/dtv/common/CiManager$OnCiEventListener;

    return-void
.end method
