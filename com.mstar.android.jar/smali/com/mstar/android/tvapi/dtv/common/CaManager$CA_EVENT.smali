.class public final enum Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;
.super Ljava/lang/Enum;
.source "CaManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mstar/android/tvapi/dtv/common/CaManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401c
    name = "CA_EVENT"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;

.field public static final enum EV_CA_ACTION_REQUEST:Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;

.field public static final enum EV_CA_DETITLE_RECEVIED:Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;

.field public static final enum EV_CA_EMAIL_NOTIFY_ICON:Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;

.field public static final enum EV_CA_ENTITLE_CHANGED:Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;

.field public static final enum EV_CA_HIDE_IPPV_DLG:Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;

.field public static final enum EV_CA_HIDE_OSD_MESSAGE:Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;

.field public static final enum EV_CA_LOCKSERVICE:Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;

.field public static final enum EV_CA_OTASTATE:Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;

.field public static final enum EV_CA_REQUEST_FEEDING:Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;

.field public static final enum EV_CA_SHOW_BUY_MESSAGE:Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;

.field public static final enum EV_CA_SHOW_FINGER_MESSAGE:Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;

.field public static final enum EV_CA_SHOW_OSD_MESSAGE:Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;

.field public static final enum EV_CA_SHOW_PROGRESS_STRIP:Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;

.field public static final enum EV_CA_START_IPPV_BUY_DLG:Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;

.field public static final enum EV_CA_UNLOCKSERVICE:Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;

.field public static final enum EV_MAX:Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;

    const-string v1, "EV_CA_START_IPPV_BUY_DLG"

    invoke-direct {v0, v1, v3}, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;->EV_CA_START_IPPV_BUY_DLG:Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;

    const-string v1, "EV_CA_HIDE_IPPV_DLG"

    invoke-direct {v0, v1, v4}, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;->EV_CA_HIDE_IPPV_DLG:Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;

    const-string v1, "EV_CA_EMAIL_NOTIFY_ICON"

    invoke-direct {v0, v1, v5}, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;->EV_CA_EMAIL_NOTIFY_ICON:Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;

    const-string v1, "EV_CA_SHOW_OSD_MESSAGE"

    invoke-direct {v0, v1, v6}, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;->EV_CA_SHOW_OSD_MESSAGE:Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;

    const-string v1, "EV_CA_HIDE_OSD_MESSAGE"

    invoke-direct {v0, v1, v7}, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;->EV_CA_HIDE_OSD_MESSAGE:Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;

    const-string v1, "EV_CA_REQUEST_FEEDING"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;->EV_CA_REQUEST_FEEDING:Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;

    const-string v1, "EV_CA_SHOW_BUY_MESSAGE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;->EV_CA_SHOW_BUY_MESSAGE:Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;

    const-string v1, "EV_CA_SHOW_FINGER_MESSAGE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;->EV_CA_SHOW_FINGER_MESSAGE:Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;

    const-string v1, "EV_CA_SHOW_PROGRESS_STRIP"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;->EV_CA_SHOW_PROGRESS_STRIP:Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;

    const-string v1, "EV_CA_ACTION_REQUEST"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;->EV_CA_ACTION_REQUEST:Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;

    const-string v1, "EV_CA_ENTITLE_CHANGED"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;->EV_CA_ENTITLE_CHANGED:Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;

    const-string v1, "EV_CA_DETITLE_RECEVIED"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;->EV_CA_DETITLE_RECEVIED:Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;

    const-string v1, "EV_CA_LOCKSERVICE"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;->EV_CA_LOCKSERVICE:Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;

    const-string v1, "EV_CA_UNLOCKSERVICE"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;->EV_CA_UNLOCKSERVICE:Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;

    const-string v1, "EV_CA_OTASTATE"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;->EV_CA_OTASTATE:Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;

    const-string v1, "EV_MAX"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;->EV_MAX:Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;

    const/16 v0, 0x10

    new-array v0, v0, [Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;

    sget-object v1, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;->EV_CA_START_IPPV_BUY_DLG:Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;->EV_CA_HIDE_IPPV_DLG:Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;->EV_CA_EMAIL_NOTIFY_ICON:Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;->EV_CA_SHOW_OSD_MESSAGE:Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;

    aput-object v1, v0, v6

    sget-object v1, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;->EV_CA_HIDE_OSD_MESSAGE:Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;->EV_CA_REQUEST_FEEDING:Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;->EV_CA_SHOW_BUY_MESSAGE:Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;->EV_CA_SHOW_FINGER_MESSAGE:Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;->EV_CA_SHOW_PROGRESS_STRIP:Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;->EV_CA_ACTION_REQUEST:Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;->EV_CA_ENTITLE_CHANGED:Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;->EV_CA_DETITLE_RECEVIED:Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;->EV_CA_LOCKSERVICE:Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;->EV_CA_UNLOCKSERVICE:Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;->EV_CA_OTASTATE:Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;->EV_MAX:Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;

    aput-object v2, v0, v1

    sput-object v0, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;->$VALUES:[Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;->$VALUES:[Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;

    invoke-virtual {v0}, [Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;

    return-object v0
.end method
