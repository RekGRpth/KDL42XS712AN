.class public final Lcom/mstar/android/tvapi/dtv/common/EpgManager;
.super Ljava/lang/Object;
.source "EpgManager.java"


# static fields
.field private static final ATSC_TIME_DIFFERENCE:I = 0x12d53d80

.field private static final FALSE:I = 0x0

.field private static final IEPG_MANAGER:Ljava/lang/String; = "mstar.IEpgManager"

.field private static _epgManager:Lcom/mstar/android/tvapi/dtv/common/EpgManager;


# instance fields
.field private mEpgManagerContext:I

.field private mNativeContext:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    :try_start_0
    const-string v1, "epgmanager_jni"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/EpgManager;->native_init()V
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const/4 v1, 0x0

    sput-object v1, Lcom/mstar/android/tvapi/dtv/common/EpgManager;->_epgManager:Lcom/mstar/android/tvapi/dtv/common/EpgManager;

    return-void

    :catch_0
    move-exception v0

    sget-object v1, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Cannot load epgmanager_jni library:\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/UnsatisfiedLinkError;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-direct {p0, v0}, Lcom/mstar/android/tvapi/dtv/common/EpgManager;->native_setup(Ljava/lang/Object;)V

    return-void
.end method

.method private static PostEvent_SnServiceDeadth(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    :try_start_0
    const-class v2, Lcom/mstar/android/tvapi/dtv/common/EpgManager;

    monitor-enter v2
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    const-string v1, "mstar.str.suspending"

    const-string v3, "1"

    invoke-static {v1, v3}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x0

    sput-object v1, Lcom/mstar/android/tvapi/dtv/common/EpgManager;->_epgManager:Lcom/mstar/android/tvapi/dtv/common/EpgManager;

    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "PostEvent_SnServiceDeadth: set SystemProperties \'mstar.str.suspending\' =1"

    invoke-virtual {v1, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    monitor-exit v2

    :goto_0
    return-void

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v1
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method private final native _addingEpgPriority(Landroid/os/Parcel;Landroid/os/Parcel;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native _atsc_getEventInfoByTime(Landroid/os/Parcel;Landroid/os/Parcel;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native _getCridAlternateList(Landroid/os/Parcel;Landroid/os/Parcel;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native _getCridSeriesList(Landroid/os/Parcel;Landroid/os/Parcel;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native _getCridSplitList(Landroid/os/Parcel;Landroid/os/Parcel;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native _getCridStatus(Landroid/os/Parcel;Landroid/os/Parcel;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native _getEitInfo(Landroid/os/Parcel;Landroid/os/Parcel;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native _getEvent1stMatchHdBroadcast(Landroid/os/Parcel;Landroid/os/Parcel;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native _getEvent1stMatchHdSimulcast(Landroid/os/Parcel;Landroid/os/Parcel;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native _getEventDescriptor(Landroid/os/Parcel;Landroid/os/Parcel;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native _getEventExtendInfoByTime(Landroid/os/Parcel;Landroid/os/Parcel;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native _getEventHdSimulcast(Landroid/os/Parcel;Landroid/os/Parcel;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native _getEventInfo(Landroid/os/Parcel;Landroid/os/Parcel;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private native _getEventInfoById(Landroid/os/Parcel;Landroid/os/Parcel;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native _getEventInfoByRctLink(Landroid/os/Parcel;Landroid/os/Parcel;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native _getEventInfoByTime(Landroid/os/Parcel;Landroid/os/Parcel;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native _getFirstEventInformation(Landroid/os/Parcel;Landroid/os/Parcel;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native _getNextEventInformation(Landroid/os/Parcel;Landroid/os/Parcel;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native _getNvodTimeShiftEventInfo(Landroid/os/Parcel;Landroid/os/Parcel;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native _getPresentFollowingEventInfo(Landroid/os/Parcel;Landroid/os/Parcel;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native _getRctTrailerLink(Landroid/os/Parcel;Landroid/os/Parcel;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native beginGetEventInformation(IIII)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native endGetEventInformation()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native getEpgEventOffsetTime(IZ)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native getEventCount(IIIII)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private final native getEventCount(SII)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method protected static getInstance()Lcom/mstar/android/tvapi/dtv/common/EpgManager;
    .locals 2

    sget-object v0, Lcom/mstar/android/tvapi/dtv/common/EpgManager;->_epgManager:Lcom/mstar/android/tvapi/dtv/common/EpgManager;

    if-nez v0, :cond_1

    const-class v1, Lcom/mstar/android/tvapi/dtv/common/EpgManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/mstar/android/tvapi/dtv/common/EpgManager;->_epgManager:Lcom/mstar/android/tvapi/dtv/common/EpgManager;

    if-nez v0, :cond_0

    new-instance v0, Lcom/mstar/android/tvapi/dtv/common/EpgManager;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/dtv/common/EpgManager;-><init>()V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/common/EpgManager;->_epgManager:Lcom/mstar/android/tvapi/dtv/common/EpgManager;

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    sget-object v0, Lcom/mstar/android/tvapi/dtv/common/EpgManager;->_epgManager:Lcom/mstar/android/tvapi/dtv/common/EpgManager;

    return-object v0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private final native native_finalize()V
.end method

.method private static final native native_init()V
.end method

.method private final native native_setup(Ljava/lang/Object;)V
.end method

.method private static postEventFromNative(Ljava/lang/Object;IIILjava/lang/Object;)V
    .locals 2
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # Ljava/lang/Object;

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "EpgManager callback  \n"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final addingEpgPriority(Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo;I)V
    .locals 5
    .param p1    # Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo;
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    const-string v3, "mstar.IEpgManager"

    invoke-virtual {v1, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    const/4 v3, 0x0

    invoke-virtual {p1, v1, v3}, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo;->writeToParcel(Landroid/os/Parcel;I)V

    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeInt(I)V

    invoke-direct {p0, v1, v0}, Lcom/mstar/android/tvapi/dtv/common/EpgManager;->_addingEpgPriority(Landroid/os/Parcel;Landroid/os/Parcel;)I

    invoke-virtual {v0}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-nez v2, :cond_0

    new-instance v3, Lcom/mstar/android/tvapi/common/exception/NativeCallFailException;

    const-string v4, "funtion failed at tvservice "

    invoke-direct {v3, v4}, Lcom/mstar/android/tvapi/common/exception/NativeCallFailException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_0
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    return-void
.end method

.method public final beginToGetEventInformation(IIII)Z
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/mstar/android/tvapi/dtv/common/EpgManager;->beginGetEventInformation(IIII)Z

    move-result v0

    return v0
.end method

.method public final native disableEpgBarkerChannel()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native enableEpgBarkerChannel()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final endToGetEventInformation()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/mstar/android/tvapi/dtv/common/EpgManager;->endGetEventInformation()V

    return-void
.end method

.method protected finalize()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    invoke-direct {p0}, Lcom/mstar/android/tvapi/dtv/common/EpgManager;->native_finalize()V

    return-void
.end method

.method public final getCridAlternateList(SILandroid/text/format/Time;)Ljava/util/ArrayList;
    .locals 17
    .param p1    # S
    .param p2    # I
    .param p3    # Landroid/text/format/Time;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(SI",
            "Landroid/text/format/Time;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mstar/android/tvapi/dtv/vo/EpgCridEventInfo;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v10

    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v9

    const-string v13, "mstar.IEpgManager"

    invoke-virtual {v10, v13}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    const/4 v13, 0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v13}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v13

    const-wide/16 v15, 0x3e8

    div-long/2addr v13, v15

    long-to-int v4, v13

    move/from16 v0, p1

    invoke-virtual {v10, v0}, Landroid/os/Parcel;->writeInt(I)V

    move/from16 v0, p2

    invoke-virtual {v10, v0}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {v10, v4}, Landroid/os/Parcel;->writeInt(I)V

    move-object/from16 v0, p0

    invoke-direct {v0, v10, v9}, Lcom/mstar/android/tvapi/dtv/common/EpgManager;->_getCridAlternateList(Landroid/os/Parcel;Landroid/os/Parcel;)I

    invoke-virtual {v9}, Landroid/os/Parcel;->readInt()I

    move-result v12

    if-nez v12, :cond_0

    new-instance v13, Lcom/mstar/android/tvapi/common/exception/NativeCallFailException;

    const-string v14, "funtion failed at tvservice "

    invoke-direct {v13, v14}, Lcom/mstar/android/tvapi/common/exception/NativeCallFailException;-><init>(Ljava/lang/String;)V

    throw v13

    :cond_0
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v9}, Landroid/os/Parcel;->readInt()I

    move-result v2

    const/4 v7, -0x1

    const/4 v5, -0x1

    const/4 v6, 0x0

    :goto_0
    if-ge v6, v2, :cond_2

    new-instance v1, Lcom/mstar/android/tvapi/dtv/vo/EpgCridEventInfo;

    invoke-direct {v1}, Lcom/mstar/android/tvapi/dtv/vo/EpgCridEventInfo;-><init>()V

    invoke-virtual {v9}, Landroid/os/Parcel;->readInt()I

    move-result v13

    int-to-short v13, v13

    iput-short v13, v1, Lcom/mstar/android/tvapi/dtv/vo/EpgCridEventInfo;->serviceType:S

    invoke-virtual {v9}, Landroid/os/Parcel;->readInt()I

    move-result v13

    iput v13, v1, Lcom/mstar/android/tvapi/dtv/vo/EpgCridEventInfo;->serviceNumber:I

    iget-object v13, v1, Lcom/mstar/android/tvapi/dtv/vo/EpgCridEventInfo;->eventInfo:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-virtual {v9}, Landroid/os/Parcel;->readInt()I

    move-result v14

    iput v14, v13, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->startTime:I

    iget-object v13, v1, Lcom/mstar/android/tvapi/dtv/vo/EpgCridEventInfo;->eventInfo:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-virtual {v9}, Landroid/os/Parcel;->readInt()I

    move-result v14

    iput v14, v13, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->endTime:I

    iget-object v13, v1, Lcom/mstar/android/tvapi/dtv/vo/EpgCridEventInfo;->eventInfo:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-virtual {v9}, Landroid/os/Parcel;->readInt()I

    move-result v14

    iput v14, v13, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->durationTime:I

    iget-object v13, v1, Lcom/mstar/android/tvapi/dtv/vo/EpgCridEventInfo;->eventInfo:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-virtual {v9}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v14

    iput-object v14, v13, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->name:Ljava/lang/String;

    iget-object v13, v1, Lcom/mstar/android/tvapi/dtv/vo/EpgCridEventInfo;->eventInfo:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-virtual {v9}, Landroid/os/Parcel;->readInt()I

    move-result v14

    iput v14, v13, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->eventId:I

    invoke-virtual {v9}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-nez v7, :cond_1

    iget-object v13, v1, Lcom/mstar/android/tvapi/dtv/vo/EpgCridEventInfo;->eventInfo:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    const/4 v14, 0x0

    iput-boolean v14, v13, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->isScrambled:Z

    :goto_1
    iget-object v13, v1, Lcom/mstar/android/tvapi/dtv/vo/EpgCridEventInfo;->eventInfo:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-virtual {v9}, Landroid/os/Parcel;->readInt()I

    move-result v14

    int-to-short v14, v14

    iput-short v14, v13, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->genre:S

    iget-object v13, v1, Lcom/mstar/android/tvapi/dtv/vo/EpgCridEventInfo;->eventInfo:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-virtual {v9}, Landroid/os/Parcel;->readInt()I

    move-result v14

    int-to-short v14, v14

    iput-short v14, v13, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->parentalRating:S

    iget-object v13, v1, Lcom/mstar/android/tvapi/dtv/vo/EpgCridEventInfo;->eventInfo:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-virtual {v9}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v14

    iput-object v14, v13, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->description:Ljava/lang/String;

    iget-object v13, v1, Lcom/mstar/android/tvapi/dtv/vo/EpgCridEventInfo;->eventInfo:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-virtual {v9}, Landroid/os/Parcel;->readInt()I

    move-result v14

    iput v14, v13, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->originalStartTime:I

    invoke-virtual {v9}, Landroid/os/Parcel;->readInt()I

    move-result v5

    const/4 v8, -0x1

    :try_start_0
    invoke-static {v5}, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo$EnumEpgFunctionStatus;->getOrdinalThroughValue(I)I
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvOutOfBoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v8

    iget-object v13, v1, Lcom/mstar/android/tvapi/dtv/vo/EpgCridEventInfo;->eventInfo:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo$EnumEpgFunctionStatus;->values()[Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo$EnumEpgFunctionStatus;

    move-result-object v14

    aget-object v14, v14, v8

    invoke-virtual {v13, v14}, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->setEpgFunctionStatus(Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo$EnumEpgFunctionStatus;)V

    invoke-virtual {v11, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_2
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_0

    :cond_1
    iget-object v13, v1, Lcom/mstar/android/tvapi/dtv/vo/EpgCridEventInfo;->eventInfo:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    const/4 v14, 0x1

    iput-boolean v14, v13, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->isScrambled:Z

    goto :goto_1

    :catch_0
    move-exception v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/exception/TvOutOfBoundException;->printStackTrace()V

    goto :goto_2

    :cond_2
    invoke-virtual {v10}, Landroid/os/Parcel;->recycle()V

    invoke-virtual {v9}, Landroid/os/Parcel;->recycle()V

    return-object v11
.end method

.method public final getCridSeriesList(SILandroid/text/format/Time;)Ljava/util/ArrayList;
    .locals 17
    .param p1    # S
    .param p2    # I
    .param p3    # Landroid/text/format/Time;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(SI",
            "Landroid/text/format/Time;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mstar/android/tvapi/dtv/vo/EpgCridEventInfo;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v10

    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v9

    const-string v13, "mstar.IEpgManager"

    invoke-virtual {v10, v13}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    const/4 v13, 0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v13}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v13

    const-wide/16 v15, 0x3e8

    div-long/2addr v13, v15

    long-to-int v4, v13

    move/from16 v0, p1

    invoke-virtual {v10, v0}, Landroid/os/Parcel;->writeInt(I)V

    move/from16 v0, p2

    invoke-virtual {v10, v0}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {v10, v4}, Landroid/os/Parcel;->writeInt(I)V

    move-object/from16 v0, p0

    invoke-direct {v0, v10, v9}, Lcom/mstar/android/tvapi/dtv/common/EpgManager;->_getCridSeriesList(Landroid/os/Parcel;Landroid/os/Parcel;)I

    invoke-virtual {v9}, Landroid/os/Parcel;->readInt()I

    move-result v12

    if-nez v12, :cond_0

    new-instance v13, Lcom/mstar/android/tvapi/common/exception/NativeCallFailException;

    const-string v14, "funtion failed at tvservice "

    invoke-direct {v13, v14}, Lcom/mstar/android/tvapi/common/exception/NativeCallFailException;-><init>(Ljava/lang/String;)V

    throw v13

    :cond_0
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v9}, Landroid/os/Parcel;->readInt()I

    move-result v2

    const/4 v7, -0x1

    const/4 v5, -0x1

    const/4 v6, 0x0

    :goto_0
    if-ge v6, v2, :cond_2

    new-instance v1, Lcom/mstar/android/tvapi/dtv/vo/EpgCridEventInfo;

    invoke-direct {v1}, Lcom/mstar/android/tvapi/dtv/vo/EpgCridEventInfo;-><init>()V

    invoke-virtual {v9}, Landroid/os/Parcel;->readInt()I

    move-result v13

    int-to-short v13, v13

    iput-short v13, v1, Lcom/mstar/android/tvapi/dtv/vo/EpgCridEventInfo;->serviceType:S

    invoke-virtual {v9}, Landroid/os/Parcel;->readInt()I

    move-result v13

    iput v13, v1, Lcom/mstar/android/tvapi/dtv/vo/EpgCridEventInfo;->serviceNumber:I

    iget-object v13, v1, Lcom/mstar/android/tvapi/dtv/vo/EpgCridEventInfo;->eventInfo:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-virtual {v9}, Landroid/os/Parcel;->readInt()I

    move-result v14

    iput v14, v13, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->startTime:I

    iget-object v13, v1, Lcom/mstar/android/tvapi/dtv/vo/EpgCridEventInfo;->eventInfo:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-virtual {v9}, Landroid/os/Parcel;->readInt()I

    move-result v14

    iput v14, v13, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->endTime:I

    iget-object v13, v1, Lcom/mstar/android/tvapi/dtv/vo/EpgCridEventInfo;->eventInfo:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-virtual {v9}, Landroid/os/Parcel;->readInt()I

    move-result v14

    iput v14, v13, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->durationTime:I

    iget-object v13, v1, Lcom/mstar/android/tvapi/dtv/vo/EpgCridEventInfo;->eventInfo:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-virtual {v9}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v14

    iput-object v14, v13, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->name:Ljava/lang/String;

    iget-object v13, v1, Lcom/mstar/android/tvapi/dtv/vo/EpgCridEventInfo;->eventInfo:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-virtual {v9}, Landroid/os/Parcel;->readInt()I

    move-result v14

    iput v14, v13, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->eventId:I

    invoke-virtual {v9}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-nez v7, :cond_1

    iget-object v13, v1, Lcom/mstar/android/tvapi/dtv/vo/EpgCridEventInfo;->eventInfo:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    const/4 v14, 0x0

    iput-boolean v14, v13, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->isScrambled:Z

    :goto_1
    iget-object v13, v1, Lcom/mstar/android/tvapi/dtv/vo/EpgCridEventInfo;->eventInfo:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-virtual {v9}, Landroid/os/Parcel;->readInt()I

    move-result v14

    int-to-short v14, v14

    iput-short v14, v13, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->genre:S

    iget-object v13, v1, Lcom/mstar/android/tvapi/dtv/vo/EpgCridEventInfo;->eventInfo:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-virtual {v9}, Landroid/os/Parcel;->readInt()I

    move-result v14

    int-to-short v14, v14

    iput-short v14, v13, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->parentalRating:S

    iget-object v13, v1, Lcom/mstar/android/tvapi/dtv/vo/EpgCridEventInfo;->eventInfo:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-virtual {v9}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v14

    iput-object v14, v13, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->description:Ljava/lang/String;

    iget-object v13, v1, Lcom/mstar/android/tvapi/dtv/vo/EpgCridEventInfo;->eventInfo:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-virtual {v9}, Landroid/os/Parcel;->readInt()I

    move-result v14

    iput v14, v13, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->originalStartTime:I

    invoke-virtual {v9}, Landroid/os/Parcel;->readInt()I

    move-result v5

    const/4 v8, -0x1

    :try_start_0
    invoke-static {v5}, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo$EnumEpgFunctionStatus;->getOrdinalThroughValue(I)I
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvOutOfBoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v8

    iget-object v13, v1, Lcom/mstar/android/tvapi/dtv/vo/EpgCridEventInfo;->eventInfo:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo$EnumEpgFunctionStatus;->values()[Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo$EnumEpgFunctionStatus;

    move-result-object v14

    aget-object v14, v14, v8

    invoke-virtual {v13, v14}, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->setEpgFunctionStatus(Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo$EnumEpgFunctionStatus;)V

    invoke-virtual {v11, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_2
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_0

    :cond_1
    iget-object v13, v1, Lcom/mstar/android/tvapi/dtv/vo/EpgCridEventInfo;->eventInfo:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    const/4 v14, 0x1

    iput-boolean v14, v13, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->isScrambled:Z

    goto :goto_1

    :catch_0
    move-exception v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/exception/TvOutOfBoundException;->printStackTrace()V

    goto :goto_2

    :cond_2
    invoke-virtual {v10}, Landroid/os/Parcel;->recycle()V

    invoke-virtual {v9}, Landroid/os/Parcel;->recycle()V

    return-object v11
.end method

.method public final getCridSplitList(SILandroid/text/format/Time;)Ljava/util/ArrayList;
    .locals 17
    .param p1    # S
    .param p2    # I
    .param p3    # Landroid/text/format/Time;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(SI",
            "Landroid/text/format/Time;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mstar/android/tvapi/dtv/vo/EpgCridEventInfo;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v10

    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v9

    const-string v13, "mstar.IEpgManager"

    invoke-virtual {v10, v13}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    const/4 v13, 0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v13}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v13

    const-wide/16 v15, 0x3e8

    div-long/2addr v13, v15

    long-to-int v4, v13

    move/from16 v0, p1

    invoke-virtual {v10, v0}, Landroid/os/Parcel;->writeInt(I)V

    move/from16 v0, p2

    invoke-virtual {v10, v0}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {v10, v4}, Landroid/os/Parcel;->writeInt(I)V

    move-object/from16 v0, p0

    invoke-direct {v0, v10, v9}, Lcom/mstar/android/tvapi/dtv/common/EpgManager;->_getCridSplitList(Landroid/os/Parcel;Landroid/os/Parcel;)I

    invoke-virtual {v9}, Landroid/os/Parcel;->readInt()I

    move-result v12

    if-nez v12, :cond_0

    new-instance v13, Lcom/mstar/android/tvapi/common/exception/NativeCallFailException;

    const-string v14, "funtion failed at tvservice "

    invoke-direct {v13, v14}, Lcom/mstar/android/tvapi/common/exception/NativeCallFailException;-><init>(Ljava/lang/String;)V

    throw v13

    :cond_0
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v9}, Landroid/os/Parcel;->readInt()I

    move-result v2

    const/4 v7, -0x1

    const/4 v5, -0x1

    const/4 v6, 0x0

    :goto_0
    if-ge v6, v2, :cond_2

    new-instance v1, Lcom/mstar/android/tvapi/dtv/vo/EpgCridEventInfo;

    invoke-direct {v1}, Lcom/mstar/android/tvapi/dtv/vo/EpgCridEventInfo;-><init>()V

    invoke-virtual {v9}, Landroid/os/Parcel;->readInt()I

    move-result v13

    int-to-short v13, v13

    iput-short v13, v1, Lcom/mstar/android/tvapi/dtv/vo/EpgCridEventInfo;->serviceType:S

    invoke-virtual {v9}, Landroid/os/Parcel;->readInt()I

    move-result v13

    iput v13, v1, Lcom/mstar/android/tvapi/dtv/vo/EpgCridEventInfo;->serviceNumber:I

    iget-object v13, v1, Lcom/mstar/android/tvapi/dtv/vo/EpgCridEventInfo;->eventInfo:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-virtual {v9}, Landroid/os/Parcel;->readInt()I

    move-result v14

    iput v14, v13, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->startTime:I

    iget-object v13, v1, Lcom/mstar/android/tvapi/dtv/vo/EpgCridEventInfo;->eventInfo:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-virtual {v9}, Landroid/os/Parcel;->readInt()I

    move-result v14

    iput v14, v13, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->endTime:I

    iget-object v13, v1, Lcom/mstar/android/tvapi/dtv/vo/EpgCridEventInfo;->eventInfo:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-virtual {v9}, Landroid/os/Parcel;->readInt()I

    move-result v14

    iput v14, v13, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->durationTime:I

    iget-object v13, v1, Lcom/mstar/android/tvapi/dtv/vo/EpgCridEventInfo;->eventInfo:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-virtual {v9}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v14

    iput-object v14, v13, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->name:Ljava/lang/String;

    iget-object v13, v1, Lcom/mstar/android/tvapi/dtv/vo/EpgCridEventInfo;->eventInfo:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-virtual {v9}, Landroid/os/Parcel;->readInt()I

    move-result v14

    iput v14, v13, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->eventId:I

    invoke-virtual {v9}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-nez v7, :cond_1

    iget-object v13, v1, Lcom/mstar/android/tvapi/dtv/vo/EpgCridEventInfo;->eventInfo:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    const/4 v14, 0x0

    iput-boolean v14, v13, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->isScrambled:Z

    :goto_1
    iget-object v13, v1, Lcom/mstar/android/tvapi/dtv/vo/EpgCridEventInfo;->eventInfo:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-virtual {v9}, Landroid/os/Parcel;->readInt()I

    move-result v14

    int-to-short v14, v14

    iput-short v14, v13, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->genre:S

    iget-object v13, v1, Lcom/mstar/android/tvapi/dtv/vo/EpgCridEventInfo;->eventInfo:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-virtual {v9}, Landroid/os/Parcel;->readInt()I

    move-result v14

    int-to-short v14, v14

    iput-short v14, v13, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->parentalRating:S

    iget-object v13, v1, Lcom/mstar/android/tvapi/dtv/vo/EpgCridEventInfo;->eventInfo:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-virtual {v9}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v14

    iput-object v14, v13, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->description:Ljava/lang/String;

    iget-object v13, v1, Lcom/mstar/android/tvapi/dtv/vo/EpgCridEventInfo;->eventInfo:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-virtual {v9}, Landroid/os/Parcel;->readInt()I

    move-result v14

    iput v14, v13, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->originalStartTime:I

    invoke-virtual {v9}, Landroid/os/Parcel;->readInt()I

    move-result v5

    const/4 v8, -0x1

    :try_start_0
    invoke-static {v5}, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo$EnumEpgFunctionStatus;->getOrdinalThroughValue(I)I
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvOutOfBoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v8

    :goto_2
    iget-object v13, v1, Lcom/mstar/android/tvapi/dtv/vo/EpgCridEventInfo;->eventInfo:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo$EnumEpgFunctionStatus;->values()[Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo$EnumEpgFunctionStatus;

    move-result-object v14

    aget-object v14, v14, v8

    invoke-virtual {v13, v14}, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->setEpgFunctionStatus(Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo$EnumEpgFunctionStatus;)V

    invoke-virtual {v11, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_0

    :cond_1
    iget-object v13, v1, Lcom/mstar/android/tvapi/dtv/vo/EpgCridEventInfo;->eventInfo:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    const/4 v14, 0x1

    iput-boolean v14, v13, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->isScrambled:Z

    goto :goto_1

    :catch_0
    move-exception v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/exception/TvOutOfBoundException;->printStackTrace()V

    goto :goto_2

    :cond_2
    invoke-virtual {v10}, Landroid/os/Parcel;->recycle()V

    invoke-virtual {v9}, Landroid/os/Parcel;->recycle()V

    return-object v11
.end method

.method public final getCridStatus(SILandroid/text/format/Time;)Lcom/mstar/android/tvapi/dtv/vo/EpgCridStatus;
    .locals 14
    .param p1    # S
    .param p2    # I
    .param p3    # Landroid/text/format/Time;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v7

    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v6

    const-string v10, "mstar.IEpgManager"

    invoke-virtual {v7, v10}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    const/4 v10, 0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v10}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v10

    const-wide/16 v12, 0x3e8

    div-long/2addr v10, v12

    long-to-int v1, v10

    invoke-virtual {v7, p1}, Landroid/os/Parcel;->writeInt(I)V

    move/from16 v0, p2

    invoke-virtual {v7, v0}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {v7, v1}, Landroid/os/Parcel;->writeInt(I)V

    invoke-direct {p0, v7, v6}, Lcom/mstar/android/tvapi/dtv/common/EpgManager;->_getCridStatus(Landroid/os/Parcel;Landroid/os/Parcel;)I

    invoke-virtual {v6}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-nez v9, :cond_0

    new-instance v10, Lcom/mstar/android/tvapi/common/exception/NativeCallFailException;

    const-string v11, "funtion failed at tvservice "

    invoke-direct {v10, v11}, Lcom/mstar/android/tvapi/common/exception/NativeCallFailException;-><init>(Ljava/lang/String;)V

    throw v10

    :cond_0
    new-instance v8, Lcom/mstar/android/tvapi/dtv/vo/EpgCridStatus;

    invoke-direct {v8}, Lcom/mstar/android/tvapi/dtv/vo/EpgCridStatus;-><init>()V

    invoke-virtual {v6}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-nez v4, :cond_1

    const/4 v10, 0x0

    iput-boolean v10, v8, Lcom/mstar/android/tvapi/dtv/vo/EpgCridStatus;->isSeries:Z

    :goto_0
    invoke-virtual {v6}, Landroid/os/Parcel;->readInt()I

    move-result v5

    if-nez v5, :cond_2

    const/4 v10, 0x0

    iput-boolean v10, v8, Lcom/mstar/android/tvapi/dtv/vo/EpgCridStatus;->isSplit:Z

    :goto_1
    invoke-virtual {v6}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-nez v2, :cond_3

    const/4 v10, 0x0

    iput-boolean v10, v8, Lcom/mstar/android/tvapi/dtv/vo/EpgCridStatus;->isAlternate:Z

    :goto_2
    invoke-virtual {v6}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-nez v3, :cond_4

    const/4 v10, 0x0

    iput-boolean v10, v8, Lcom/mstar/android/tvapi/dtv/vo/EpgCridStatus;->isRecommend:Z

    :goto_3
    invoke-virtual {v6}, Landroid/os/Parcel;->readInt()I

    move-result v10

    int-to-short v10, v10

    iput-short v10, v8, Lcom/mstar/android/tvapi/dtv/vo/EpgCridStatus;->seriesCount:S

    invoke-virtual {v7}, Landroid/os/Parcel;->recycle()V

    invoke-virtual {v6}, Landroid/os/Parcel;->recycle()V

    return-object v8

    :cond_1
    const/4 v10, 0x1

    iput-boolean v10, v8, Lcom/mstar/android/tvapi/dtv/vo/EpgCridStatus;->isSeries:Z

    goto :goto_0

    :cond_2
    const/4 v10, 0x1

    iput-boolean v10, v8, Lcom/mstar/android/tvapi/dtv/vo/EpgCridStatus;->isSplit:Z

    goto :goto_1

    :cond_3
    const/4 v10, 0x1

    iput-boolean v10, v8, Lcom/mstar/android/tvapi/dtv/vo/EpgCridStatus;->isAlternate:Z

    goto :goto_2

    :cond_4
    const/4 v10, 0x1

    iput-boolean v10, v8, Lcom/mstar/android/tvapi/dtv/vo/EpgCridStatus;->isRecommend:Z

    goto :goto_3
.end method

.method public final getEitInfo(Z)Lcom/mstar/android/tvapi/dtv/vo/DtvEitInfo;
    .locals 16
    .param p1    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v6

    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v5

    const-string v11, "mstar.IEpgManager"

    invoke-virtual {v6, v11}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    const/4 v11, 0x1

    move/from16 v0, p1

    if-ne v0, v11, :cond_0

    const/4 v11, 0x1

    invoke-virtual {v6, v11}, Landroid/os/Parcel;->writeInt(I)V

    :goto_0
    move-object/from16 v0, p0

    invoke-direct {v0, v6, v5}, Lcom/mstar/android/tvapi/dtv/common/EpgManager;->_getEitInfo(Landroid/os/Parcel;Landroid/os/Parcel;)I

    invoke-virtual {v5}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-nez v8, :cond_1

    new-instance v11, Lcom/mstar/android/tvapi/common/exception/NativeCallFailException;

    const-string v12, "funtion failed at tvservice "

    invoke-direct {v11, v12}, Lcom/mstar/android/tvapi/common/exception/NativeCallFailException;-><init>(Ljava/lang/String;)V

    throw v11

    :cond_0
    const/4 v11, 0x0

    invoke-virtual {v6, v11}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    :cond_1
    new-instance v7, Lcom/mstar/android/tvapi/dtv/vo/DtvEitInfo;

    invoke-direct {v7}, Lcom/mstar/android/tvapi/dtv/vo/DtvEitInfo;-><init>()V

    iget-object v11, v7, Lcom/mstar/android/tvapi/dtv/vo/DtvEitInfo;->eitCurrentEventPf:Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;

    invoke-virtual {v5}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v12

    iput-object v12, v11, Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;->eventName:Ljava/lang/String;

    iget-object v11, v7, Lcom/mstar/android/tvapi/dtv/vo/DtvEitInfo;->eitCurrentEventPf:Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;

    invoke-virtual {v5}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v12

    iput-object v12, v11, Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;->shortEventText:Ljava/lang/String;

    iget-object v11, v7, Lcom/mstar/android/tvapi/dtv/vo/DtvEitInfo;->eitCurrentEventPf:Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;

    invoke-virtual {v5}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v12

    iput-object v12, v11, Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;->extendedEventItem:Ljava/lang/String;

    iget-object v11, v7, Lcom/mstar/android/tvapi/dtv/vo/DtvEitInfo;->eitCurrentEventPf:Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;

    invoke-virtual {v5}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v12

    iput-object v12, v11, Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;->extendedEventText:Ljava/lang/String;

    invoke-virtual {v5}, Landroid/os/Parcel;->readInt()I

    move-result v10

    iget-object v11, v7, Lcom/mstar/android/tvapi/dtv/vo/DtvEitInfo;->eitCurrentEventPf:Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;

    iget-object v11, v11, Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;->stStartTime:Landroid/text/format/Time;

    int-to-long v12, v10

    const-wide/16 v14, 0x3e8

    mul-long/2addr v12, v14

    invoke-virtual {v11, v12, v13}, Landroid/text/format/Time;->set(J)V

    iget-object v11, v7, Lcom/mstar/android/tvapi/dtv/vo/DtvEitInfo;->eitCurrentEventPf:Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;

    iget-object v11, v11, Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;->stStartTime:Landroid/text/format/Time;

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/text/format/Time;->normalize(Z)J

    invoke-virtual {v5}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-nez v3, :cond_2

    iget-object v11, v7, Lcom/mstar/android/tvapi/dtv/vo/DtvEitInfo;->eitCurrentEventPf:Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;

    const/4 v12, 0x0

    iput-boolean v12, v11, Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;->isStartTimeDayLightTime:Z

    :goto_1
    invoke-virtual {v5}, Landroid/os/Parcel;->readInt()I

    move-result v9

    iget-object v11, v7, Lcom/mstar/android/tvapi/dtv/vo/DtvEitInfo;->eitCurrentEventPf:Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;

    iget-object v11, v11, Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;->stEndTime:Landroid/text/format/Time;

    int-to-long v12, v9

    const-wide/16 v14, 0x3e8

    mul-long/2addr v12, v14

    invoke-virtual {v11, v12, v13}, Landroid/text/format/Time;->set(J)V

    iget-object v11, v7, Lcom/mstar/android/tvapi/dtv/vo/DtvEitInfo;->eitCurrentEventPf:Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;

    iget-object v11, v11, Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;->stEndTime:Landroid/text/format/Time;

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/text/format/Time;->normalize(Z)J

    invoke-virtual {v5}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-nez v1, :cond_3

    iget-object v11, v7, Lcom/mstar/android/tvapi/dtv/vo/DtvEitInfo;->eitCurrentEventPf:Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;

    const/4 v12, 0x0

    iput-boolean v12, v11, Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;->isEndTimeDayLightTime:Z

    :goto_2
    iget-object v11, v7, Lcom/mstar/android/tvapi/dtv/vo/DtvEitInfo;->eitCurrentEventPf:Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;

    invoke-virtual {v5}, Landroid/os/Parcel;->readInt()I

    move-result v12

    iput v12, v11, Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;->durationInSeconds:I

    invoke-virtual {v5}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-nez v2, :cond_4

    iget-object v11, v7, Lcom/mstar/android/tvapi/dtv/vo/DtvEitInfo;->eitCurrentEventPf:Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;

    const/4 v12, 0x0

    iput-boolean v12, v11, Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;->isScrambled:Z

    :goto_3
    iget-object v11, v7, Lcom/mstar/android/tvapi/dtv/vo/DtvEitInfo;->eitCurrentEventPf:Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;

    invoke-virtual {v5}, Landroid/os/Parcel;->readInt()I

    move-result v12

    int-to-short v12, v12

    iput-short v12, v11, Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;->parentalControl:S

    iget-object v11, v7, Lcom/mstar/android/tvapi/dtv/vo/DtvEitInfo;->eitCurrentEventPf:Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;

    invoke-virtual {v5}, Landroid/os/Parcel;->readInt()I

    move-result v12

    int-to-short v12, v12

    iput-short v12, v11, Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;->parentalObjectiveContent:S

    iget-object v11, v7, Lcom/mstar/android/tvapi/dtv/vo/DtvEitInfo;->eitCurrentEventPf:Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;

    invoke-virtual {v5}, Landroid/os/Parcel;->readInt()I

    move-result v12

    int-to-short v12, v12

    iput-short v12, v11, Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;->contentNibbleLevel1:S

    iget-object v11, v7, Lcom/mstar/android/tvapi/dtv/vo/DtvEitInfo;->eitCurrentEventPf:Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;

    invoke-virtual {v5}, Landroid/os/Parcel;->readInt()I

    move-result v12

    int-to-short v12, v12

    iput-short v12, v11, Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;->contentNibbleLevel2:S

    invoke-virtual {v5}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-nez v4, :cond_5

    const/4 v11, 0x0

    iput-boolean v11, v7, Lcom/mstar/android/tvapi/dtv/vo/DtvEitInfo;->present:Z

    :goto_4
    invoke-virtual {v6}, Landroid/os/Parcel;->recycle()V

    invoke-virtual {v5}, Landroid/os/Parcel;->recycle()V

    return-object v7

    :cond_2
    iget-object v11, v7, Lcom/mstar/android/tvapi/dtv/vo/DtvEitInfo;->eitCurrentEventPf:Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;

    const/4 v12, 0x1

    iput-boolean v12, v11, Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;->isStartTimeDayLightTime:Z

    goto :goto_1

    :cond_3
    iget-object v11, v7, Lcom/mstar/android/tvapi/dtv/vo/DtvEitInfo;->eitCurrentEventPf:Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;

    const/4 v12, 0x1

    iput-boolean v12, v11, Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;->isEndTimeDayLightTime:Z

    goto :goto_2

    :cond_4
    iget-object v11, v7, Lcom/mstar/android/tvapi/dtv/vo/DtvEitInfo;->eitCurrentEventPf:Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;

    const/4 v12, 0x1

    iput-boolean v12, v11, Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;->isScrambled:Z

    goto :goto_3

    :cond_5
    const/4 v11, 0x1

    iput-boolean v11, v7, Lcom/mstar/android/tvapi/dtv/vo/DtvEitInfo;->present:Z

    goto :goto_4
.end method

.method public final getEpgEventOffsetTime(Landroid/text/format/Time;Z)I
    .locals 4
    .param p1    # Landroid/text/format/Time;
    .param p2    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    long-to-int v0, v0

    invoke-direct {p0, v0, p2}, Lcom/mstar/android/tvapi/dtv/common/EpgManager;->getEpgEventOffsetTime(IZ)I

    move-result v0

    return v0
.end method

.method public final getEvent1stMatchHdBroadcast(SILandroid/text/format/Time;)Lcom/mstar/android/tvapi/dtv/vo/EpgFirstMatchHdCast;
    .locals 14
    .param p1    # S
    .param p2    # I
    .param p3    # Landroid/text/format/Time;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v7

    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v6

    const-string v10, "mstar.IEpgManager"

    invoke-virtual {v7, v10}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    const/4 v10, 0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v10}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v10

    const-wide/16 v12, 0x3e8

    div-long/2addr v10, v12

    long-to-int v1, v10

    invoke-virtual {v7, p1}, Landroid/os/Parcel;->writeInt(I)V

    move/from16 v0, p2

    invoke-virtual {v7, v0}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {v7, v1}, Landroid/os/Parcel;->writeInt(I)V

    invoke-direct {p0, v7, v6}, Lcom/mstar/android/tvapi/dtv/common/EpgManager;->_getEvent1stMatchHdBroadcast(Landroid/os/Parcel;Landroid/os/Parcel;)I

    invoke-virtual {v6}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-nez v9, :cond_0

    new-instance v10, Lcom/mstar/android/tvapi/common/exception/NativeCallFailException;

    const-string v11, "funtion failed at tvservice "

    invoke-direct {v10, v11}, Lcom/mstar/android/tvapi/common/exception/NativeCallFailException;-><init>(Ljava/lang/String;)V

    throw v10

    :cond_0
    new-instance v8, Lcom/mstar/android/tvapi/dtv/vo/EpgFirstMatchHdCast;

    invoke-direct {v8}, Lcom/mstar/android/tvapi/dtv/vo/EpgFirstMatchHdCast;-><init>()V

    const/4 v4, -0x1

    const/4 v2, -0x1

    iget-object v10, v8, Lcom/mstar/android/tvapi/dtv/vo/EpgFirstMatchHdCast;->epgEventInfoVO:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-virtual {v6}, Landroid/os/Parcel;->readInt()I

    move-result v11

    iput v11, v10, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->startTime:I

    iget-object v10, v8, Lcom/mstar/android/tvapi/dtv/vo/EpgFirstMatchHdCast;->epgEventInfoVO:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-virtual {v6}, Landroid/os/Parcel;->readInt()I

    move-result v11

    iput v11, v10, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->endTime:I

    iget-object v10, v8, Lcom/mstar/android/tvapi/dtv/vo/EpgFirstMatchHdCast;->epgEventInfoVO:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-virtual {v6}, Landroid/os/Parcel;->readInt()I

    move-result v11

    iput v11, v10, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->durationTime:I

    iget-object v10, v8, Lcom/mstar/android/tvapi/dtv/vo/EpgFirstMatchHdCast;->epgEventInfoVO:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-virtual {v6}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v11

    iput-object v11, v10, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->name:Ljava/lang/String;

    iget-object v10, v8, Lcom/mstar/android/tvapi/dtv/vo/EpgFirstMatchHdCast;->epgEventInfoVO:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-virtual {v6}, Landroid/os/Parcel;->readInt()I

    move-result v11

    iput v11, v10, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->eventId:I

    invoke-virtual {v6}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-nez v4, :cond_1

    iget-object v10, v8, Lcom/mstar/android/tvapi/dtv/vo/EpgFirstMatchHdCast;->epgEventInfoVO:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    const/4 v11, 0x0

    iput-boolean v11, v10, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->isScrambled:Z

    :goto_0
    iget-object v10, v8, Lcom/mstar/android/tvapi/dtv/vo/EpgFirstMatchHdCast;->epgEventInfoVO:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-virtual {v6}, Landroid/os/Parcel;->readInt()I

    move-result v11

    int-to-short v11, v11

    iput-short v11, v10, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->genre:S

    iget-object v10, v8, Lcom/mstar/android/tvapi/dtv/vo/EpgFirstMatchHdCast;->epgEventInfoVO:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-virtual {v6}, Landroid/os/Parcel;->readInt()I

    move-result v11

    int-to-short v11, v11

    iput-short v11, v10, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->parentalRating:S

    iget-object v10, v8, Lcom/mstar/android/tvapi/dtv/vo/EpgFirstMatchHdCast;->epgEventInfoVO:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-virtual {v6}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v11

    iput-object v11, v10, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->description:Ljava/lang/String;

    iget-object v10, v8, Lcom/mstar/android/tvapi/dtv/vo/EpgFirstMatchHdCast;->epgEventInfoVO:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-virtual {v6}, Landroid/os/Parcel;->readInt()I

    move-result v11

    iput v11, v10, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->originalStartTime:I

    invoke-virtual {v6}, Landroid/os/Parcel;->readInt()I

    move-result v2

    invoke-static {v2}, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo$EnumEpgFunctionStatus;->getOrdinalThroughValue(I)I

    move-result v5

    iget-object v10, v8, Lcom/mstar/android/tvapi/dtv/vo/EpgFirstMatchHdCast;->epgEventInfoVO:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo$EnumEpgFunctionStatus;->values()[Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo$EnumEpgFunctionStatus;

    move-result-object v11

    aget-object v11, v11, v5

    invoke-virtual {v10, v11}, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->setEpgFunctionStatus(Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo$EnumEpgFunctionStatus;)V

    invoke-virtual {v6}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v8, Lcom/mstar/android/tvapi/dtv/vo/EpgFirstMatchHdCast;->serviceName:Ljava/lang/String;

    invoke-virtual {v6}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-nez v3, :cond_2

    const/4 v10, 0x0

    iput-boolean v10, v8, Lcom/mstar/android/tvapi/dtv/vo/EpgFirstMatchHdCast;->isResolvable:Z

    :goto_1
    invoke-virtual {v7}, Landroid/os/Parcel;->recycle()V

    invoke-virtual {v6}, Landroid/os/Parcel;->recycle()V

    return-object v8

    :cond_1
    iget-object v10, v8, Lcom/mstar/android/tvapi/dtv/vo/EpgFirstMatchHdCast;->epgEventInfoVO:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    const/4 v11, 0x1

    iput-boolean v11, v10, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->isScrambled:Z

    goto :goto_0

    :cond_2
    const/4 v10, 0x1

    iput-boolean v10, v8, Lcom/mstar/android/tvapi/dtv/vo/EpgFirstMatchHdCast;->isResolvable:Z

    goto :goto_1
.end method

.method public final getEvent1stMatchHdSimulcast(SILandroid/text/format/Time;)Lcom/mstar/android/tvapi/dtv/vo/EpgFirstMatchHdCast;
    .locals 14
    .param p1    # S
    .param p2    # I
    .param p3    # Landroid/text/format/Time;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v7

    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v6

    const-string v10, "mstar.IEpgManager"

    invoke-virtual {v7, v10}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    const/4 v10, 0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v10}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v10

    const-wide/16 v12, 0x3e8

    div-long/2addr v10, v12

    long-to-int v1, v10

    invoke-virtual {v7, p1}, Landroid/os/Parcel;->writeInt(I)V

    move/from16 v0, p2

    invoke-virtual {v7, v0}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {v7, v1}, Landroid/os/Parcel;->writeInt(I)V

    invoke-direct {p0, v7, v6}, Lcom/mstar/android/tvapi/dtv/common/EpgManager;->_getEvent1stMatchHdSimulcast(Landroid/os/Parcel;Landroid/os/Parcel;)I

    invoke-virtual {v6}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-nez v9, :cond_0

    new-instance v10, Lcom/mstar/android/tvapi/common/exception/NativeCallFailException;

    const-string v11, "funtion failed at tvservice "

    invoke-direct {v10, v11}, Lcom/mstar/android/tvapi/common/exception/NativeCallFailException;-><init>(Ljava/lang/String;)V

    throw v10

    :cond_0
    new-instance v8, Lcom/mstar/android/tvapi/dtv/vo/EpgFirstMatchHdCast;

    invoke-direct {v8}, Lcom/mstar/android/tvapi/dtv/vo/EpgFirstMatchHdCast;-><init>()V

    const/4 v4, -0x1

    const/4 v2, -0x1

    iget-object v10, v8, Lcom/mstar/android/tvapi/dtv/vo/EpgFirstMatchHdCast;->epgEventInfoVO:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-virtual {v6}, Landroid/os/Parcel;->readInt()I

    move-result v11

    iput v11, v10, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->startTime:I

    iget-object v10, v8, Lcom/mstar/android/tvapi/dtv/vo/EpgFirstMatchHdCast;->epgEventInfoVO:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-virtual {v6}, Landroid/os/Parcel;->readInt()I

    move-result v11

    iput v11, v10, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->endTime:I

    iget-object v10, v8, Lcom/mstar/android/tvapi/dtv/vo/EpgFirstMatchHdCast;->epgEventInfoVO:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-virtual {v6}, Landroid/os/Parcel;->readInt()I

    move-result v11

    iput v11, v10, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->durationTime:I

    iget-object v10, v8, Lcom/mstar/android/tvapi/dtv/vo/EpgFirstMatchHdCast;->epgEventInfoVO:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-virtual {v6}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v11

    iput-object v11, v10, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->name:Ljava/lang/String;

    iget-object v10, v8, Lcom/mstar/android/tvapi/dtv/vo/EpgFirstMatchHdCast;->epgEventInfoVO:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-virtual {v6}, Landroid/os/Parcel;->readInt()I

    move-result v11

    iput v11, v10, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->eventId:I

    invoke-virtual {v6}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-nez v4, :cond_1

    iget-object v10, v8, Lcom/mstar/android/tvapi/dtv/vo/EpgFirstMatchHdCast;->epgEventInfoVO:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    const/4 v11, 0x0

    iput-boolean v11, v10, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->isScrambled:Z

    :goto_0
    iget-object v10, v8, Lcom/mstar/android/tvapi/dtv/vo/EpgFirstMatchHdCast;->epgEventInfoVO:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-virtual {v6}, Landroid/os/Parcel;->readInt()I

    move-result v11

    int-to-short v11, v11

    iput-short v11, v10, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->genre:S

    iget-object v10, v8, Lcom/mstar/android/tvapi/dtv/vo/EpgFirstMatchHdCast;->epgEventInfoVO:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-virtual {v6}, Landroid/os/Parcel;->readInt()I

    move-result v11

    int-to-short v11, v11

    iput-short v11, v10, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->parentalRating:S

    iget-object v10, v8, Lcom/mstar/android/tvapi/dtv/vo/EpgFirstMatchHdCast;->epgEventInfoVO:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-virtual {v6}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v11

    iput-object v11, v10, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->description:Ljava/lang/String;

    iget-object v10, v8, Lcom/mstar/android/tvapi/dtv/vo/EpgFirstMatchHdCast;->epgEventInfoVO:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-virtual {v6}, Landroid/os/Parcel;->readInt()I

    move-result v11

    iput v11, v10, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->originalStartTime:I

    invoke-virtual {v6}, Landroid/os/Parcel;->readInt()I

    move-result v2

    invoke-static {v2}, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo$EnumEpgFunctionStatus;->getOrdinalThroughValue(I)I

    move-result v5

    iget-object v10, v8, Lcom/mstar/android/tvapi/dtv/vo/EpgFirstMatchHdCast;->epgEventInfoVO:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo$EnumEpgFunctionStatus;->values()[Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo$EnumEpgFunctionStatus;

    move-result-object v11

    aget-object v11, v11, v5

    invoke-virtual {v10, v11}, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->setEpgFunctionStatus(Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo$EnumEpgFunctionStatus;)V

    invoke-virtual {v6}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v8, Lcom/mstar/android/tvapi/dtv/vo/EpgFirstMatchHdCast;->serviceName:Ljava/lang/String;

    invoke-virtual {v6}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-nez v3, :cond_2

    const/4 v10, 0x0

    iput-boolean v10, v8, Lcom/mstar/android/tvapi/dtv/vo/EpgFirstMatchHdCast;->isResolvable:Z

    :goto_1
    invoke-virtual {v7}, Landroid/os/Parcel;->recycle()V

    invoke-virtual {v6}, Landroid/os/Parcel;->recycle()V

    return-object v8

    :cond_1
    iget-object v10, v8, Lcom/mstar/android/tvapi/dtv/vo/EpgFirstMatchHdCast;->epgEventInfoVO:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    const/4 v11, 0x1

    iput-boolean v11, v10, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->isScrambled:Z

    goto :goto_0

    :cond_2
    const/4 v10, 0x1

    iput-boolean v10, v8, Lcom/mstar/android/tvapi/dtv/vo/EpgFirstMatchHdCast;->isResolvable:Z

    goto :goto_1
.end method

.method public final getEventCount(IIIILandroid/text/format/Time;)I
    .locals 6
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # Landroid/text/format/Time;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    const/4 v0, 0x1

    invoke-virtual {p5, v0}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    const-wide/32 v2, 0x12d53d80

    sub-long/2addr v0, v2

    long-to-int v5, v0

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/mstar/android/tvapi/dtv/common/EpgManager;->getEventCount(IIIII)I

    move-result v0

    return v0
.end method

.method public final getEventCount(SILandroid/text/format/Time;)I
    .locals 4
    .param p1    # S
    .param p2    # I
    .param p3    # Landroid/text/format/Time;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    const/4 v0, 0x1

    invoke-virtual {p3, v0}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    long-to-int v0, v0

    invoke-direct {p0, p1, p2, v0}, Lcom/mstar/android/tvapi/dtv/common/EpgManager;->getEventCount(SII)I

    move-result v0

    return v0
.end method

.method public getEventDescriptor(SILandroid/text/format/Time;Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgDescriptionType;)Ljava/lang/String;
    .locals 9
    .param p1    # S
    .param p2    # I
    .param p3    # Landroid/text/format/Time;
    .param p4    # Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgDescriptionType;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    const-string v5, "mstar.IEpgManager"

    invoke-virtual {v2, v5}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    const/4 v5, 0x1

    invoke-virtual {p3, v5}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v5

    const-wide/16 v7, 0x3e8

    div-long/2addr v5, v7

    long-to-int v0, v5

    invoke-virtual {v2, p1}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {v2, p2}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {v2, v0}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {p4}, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgDescriptionType;->ordinal()I

    move-result v5

    invoke-virtual {v2, v5}, Landroid/os/Parcel;->writeInt(I)V

    invoke-direct {p0, v2, v1}, Lcom/mstar/android/tvapi/dtv/common/EpgManager;->_getEventDescriptor(Landroid/os/Parcel;Landroid/os/Parcel;)I

    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-nez v4, :cond_0

    new-instance v5, Lcom/mstar/android/tvapi/common/exception/NativeCallFailException;

    const-string v6, "funtion failed at tvservice "

    invoke-direct {v5, v6}, Lcom/mstar/android/tvapi/common/exception/NativeCallFailException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_0
    invoke-virtual {v1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    return-object v3
.end method

.method public final getEventExtendInfoByTime(IIIILandroid/text/format/Time;)Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;
    .locals 10
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # Landroid/text/format/Time;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    const v9, 0x12d53d80

    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    const-string v5, "mstar.IEpgManager"

    invoke-virtual {v2, v5}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    const/4 v5, 0x1

    invoke-virtual {p5, v5}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v5

    const-wide/16 v7, 0x3e8

    div-long/2addr v5, v7

    const-wide/32 v7, 0x12d53d80

    sub-long/2addr v5, v7

    long-to-int v0, v5

    invoke-virtual {v2, p1}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {v2, p2}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {v2, p3}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {v2, p4}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {v2, v0}, Landroid/os/Parcel;->writeInt(I)V

    invoke-direct {p0, v2, v1}, Lcom/mstar/android/tvapi/dtv/common/EpgManager;->_getEventExtendInfoByTime(Landroid/os/Parcel;Landroid/os/Parcel;)I

    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-nez v4, :cond_0

    new-instance v5, Lcom/mstar/android/tvapi/common/exception/NativeCallFailException;

    const-string v6, "funtion failed at tvservice "

    invoke-direct {v5, v6}, Lcom/mstar/android/tvapi/common/exception/NativeCallFailException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_0
    new-instance v3, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;

    invoke-direct {v3, v1}, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;-><init>(Landroid/os/Parcel;)V

    iget v5, v3, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;->startTime:I

    add-int/2addr v5, v9

    iput v5, v3, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;->startTime:I

    iget v5, v3, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;->endTime:I

    add-int/2addr v5, v9

    iput v5, v3, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;->endTime:I

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    return-object v3
.end method

.method public final getEventHdSimulcast(SILandroid/text/format/Time;S)Ljava/util/ArrayList;
    .locals 19
    .param p1    # S
    .param p2    # I
    .param p3    # Landroid/text/format/Time;
    .param p4    # S
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(SI",
            "Landroid/text/format/Time;",
            "S)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mstar/android/tvapi/dtv/vo/EpgHdSimulcast;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v12

    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v11

    const-string v15, "mstar.IEpgManager"

    invoke-virtual {v12, v15}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    const/4 v15, 0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v15}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v15

    const-wide/16 v17, 0x3e8

    div-long v15, v15, v17

    long-to-int v3, v15

    move/from16 v0, p1

    invoke-virtual {v12, v0}, Landroid/os/Parcel;->writeInt(I)V

    move/from16 v0, p2

    invoke-virtual {v12, v0}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {v12, v3}, Landroid/os/Parcel;->writeInt(I)V

    move/from16 v0, p4

    invoke-virtual {v12, v0}, Landroid/os/Parcel;->writeInt(I)V

    move-object/from16 v0, p0

    invoke-direct {v0, v12, v11}, Lcom/mstar/android/tvapi/dtv/common/EpgManager;->_getEventHdSimulcast(Landroid/os/Parcel;Landroid/os/Parcel;)I

    invoke-virtual {v11}, Landroid/os/Parcel;->readInt()I

    move-result v14

    if-nez v14, :cond_0

    new-instance v15, Lcom/mstar/android/tvapi/common/exception/NativeCallFailException;

    const-string v16, "funtion failed at tvservice "

    invoke-direct/range {v15 .. v16}, Lcom/mstar/android/tvapi/common/exception/NativeCallFailException;-><init>(Ljava/lang/String;)V

    throw v15

    :cond_0
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    const/4 v6, 0x0

    invoke-virtual {v11}, Landroid/os/Parcel;->readInt()I

    move-result v2

    const/4 v6, 0x0

    :goto_0
    if-ge v6, v2, :cond_4

    new-instance v1, Lcom/mstar/android/tvapi/dtv/vo/EpgHdSimulcast;

    invoke-direct {v1}, Lcom/mstar/android/tvapi/dtv/vo/EpgHdSimulcast;-><init>()V

    const/4 v9, -0x1

    const/4 v5, -0x1

    iget-object v15, v1, Lcom/mstar/android/tvapi/dtv/vo/EpgHdSimulcast;->stEventInfo:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-virtual {v11}, Landroid/os/Parcel;->readInt()I

    move-result v16

    move/from16 v0, v16

    iput v0, v15, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->startTime:I

    iget-object v15, v1, Lcom/mstar/android/tvapi/dtv/vo/EpgHdSimulcast;->stEventInfo:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-virtual {v11}, Landroid/os/Parcel;->readInt()I

    move-result v16

    move/from16 v0, v16

    iput v0, v15, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->endTime:I

    iget-object v15, v1, Lcom/mstar/android/tvapi/dtv/vo/EpgHdSimulcast;->stEventInfo:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-virtual {v11}, Landroid/os/Parcel;->readInt()I

    move-result v16

    move/from16 v0, v16

    iput v0, v15, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->durationTime:I

    iget-object v15, v1, Lcom/mstar/android/tvapi/dtv/vo/EpgHdSimulcast;->stEventInfo:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-virtual {v11}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    iput-object v0, v15, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->name:Ljava/lang/String;

    iget-object v15, v1, Lcom/mstar/android/tvapi/dtv/vo/EpgHdSimulcast;->stEventInfo:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-virtual {v11}, Landroid/os/Parcel;->readInt()I

    move-result v16

    move/from16 v0, v16

    iput v0, v15, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->eventId:I

    invoke-virtual {v11}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-nez v9, :cond_1

    iget-object v15, v1, Lcom/mstar/android/tvapi/dtv/vo/EpgHdSimulcast;->stEventInfo:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    const/16 v16, 0x0

    move/from16 v0, v16

    iput-boolean v0, v15, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->isScrambled:Z

    :goto_1
    iget-object v15, v1, Lcom/mstar/android/tvapi/dtv/vo/EpgHdSimulcast;->stEventInfo:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-virtual {v11}, Landroid/os/Parcel;->readInt()I

    move-result v16

    move/from16 v0, v16

    int-to-short v0, v0

    move/from16 v16, v0

    move/from16 v0, v16

    iput-short v0, v15, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->genre:S

    iget-object v15, v1, Lcom/mstar/android/tvapi/dtv/vo/EpgHdSimulcast;->stEventInfo:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-virtual {v11}, Landroid/os/Parcel;->readInt()I

    move-result v16

    move/from16 v0, v16

    int-to-short v0, v0

    move/from16 v16, v0

    move/from16 v0, v16

    iput-short v0, v15, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->parentalRating:S

    iget-object v15, v1, Lcom/mstar/android/tvapi/dtv/vo/EpgHdSimulcast;->stEventInfo:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-virtual {v11}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    iput-object v0, v15, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->description:Ljava/lang/String;

    iget-object v15, v1, Lcom/mstar/android/tvapi/dtv/vo/EpgHdSimulcast;->stEventInfo:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-virtual {v11}, Landroid/os/Parcel;->readInt()I

    move-result v16

    move/from16 v0, v16

    iput v0, v15, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->originalStartTime:I

    invoke-virtual {v11}, Landroid/os/Parcel;->readInt()I

    move-result v5

    const/4 v10, -0x1

    :try_start_0
    invoke-static {v5}, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo$EnumEpgFunctionStatus;->getOrdinalThroughValue(I)I
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvOutOfBoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v10

    iget-object v15, v1, Lcom/mstar/android/tvapi/dtv/vo/EpgHdSimulcast;->stEventInfo:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo$EnumEpgFunctionStatus;->values()[Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo$EnumEpgFunctionStatus;

    move-result-object v16

    aget-object v16, v16, v10

    invoke-virtual/range {v15 .. v16}, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->setEpgFunctionStatus(Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo$EnumEpgFunctionStatus;)V

    invoke-virtual {v11}, Landroid/os/Parcel;->readInt()I

    move-result v15

    int-to-short v15, v15

    iput-short v15, v1, Lcom/mstar/android/tvapi/dtv/vo/EpgHdSimulcast;->serviceType:S

    invoke-virtual {v11}, Landroid/os/Parcel;->readInt()I

    move-result v15

    iput v15, v1, Lcom/mstar/android/tvapi/dtv/vo/EpgHdSimulcast;->serviceNumber:I

    invoke-virtual {v11}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-nez v7, :cond_2

    const/4 v15, 0x0

    iput-boolean v15, v1, Lcom/mstar/android/tvapi/dtv/vo/EpgHdSimulcast;->isHdEeventResolvable:Z

    :goto_2
    invoke-virtual {v11}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-nez v8, :cond_3

    const/4 v15, 0x0

    iput-boolean v15, v1, Lcom/mstar/android/tvapi/dtv/vo/EpgHdSimulcast;->isHdServiceResolvable:Z

    :goto_3
    invoke-virtual {v11}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v15

    iput-object v15, v1, Lcom/mstar/android/tvapi/dtv/vo/EpgHdSimulcast;->serviceName:Ljava/lang/String;

    invoke-virtual {v13, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_4
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_0

    :cond_1
    iget-object v15, v1, Lcom/mstar/android/tvapi/dtv/vo/EpgHdSimulcast;->stEventInfo:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    const/16 v16, 0x1

    move/from16 v0, v16

    iput-boolean v0, v15, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->isScrambled:Z

    goto :goto_1

    :catch_0
    move-exception v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/exception/TvOutOfBoundException;->printStackTrace()V

    goto :goto_4

    :cond_2
    const/4 v15, 0x1

    iput-boolean v15, v1, Lcom/mstar/android/tvapi/dtv/vo/EpgHdSimulcast;->isHdEeventResolvable:Z

    goto :goto_2

    :cond_3
    const/4 v15, 0x1

    iput-boolean v15, v1, Lcom/mstar/android/tvapi/dtv/vo/EpgHdSimulcast;->isHdServiceResolvable:Z

    goto :goto_3

    :cond_4
    invoke-virtual {v12}, Landroid/os/Parcel;->recycle()V

    invoke-virtual {v11}, Landroid/os/Parcel;->recycle()V

    return-object v13
.end method

.method public final getEventInfo(SILandroid/text/format/Time;I)Ljava/util/ArrayList;
    .locals 17
    .param p1    # S
    .param p2    # I
    .param p3    # Landroid/text/format/Time;
    .param p4    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(SI",
            "Landroid/text/format/Time;",
            "I)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v10

    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v9

    const-string v13, "mstar.IEpgManager"

    invoke-virtual {v10, v13}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    const/4 v13, 0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v13}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v13

    const-wide/16 v15, 0x3e8

    div-long/2addr v13, v15

    long-to-int v3, v13

    move/from16 v0, p1

    invoke-virtual {v10, v0}, Landroid/os/Parcel;->writeInt(I)V

    move/from16 v0, p2

    invoke-virtual {v10, v0}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {v10, v3}, Landroid/os/Parcel;->writeInt(I)V

    move/from16 v0, p4

    invoke-virtual {v10, v0}, Landroid/os/Parcel;->writeInt(I)V

    move-object/from16 v0, p0

    invoke-direct {v0, v10, v9}, Lcom/mstar/android/tvapi/dtv/common/EpgManager;->_getEventInfo(Landroid/os/Parcel;Landroid/os/Parcel;)I

    invoke-virtual {v9}, Landroid/os/Parcel;->readInt()I

    move-result v12

    if-nez v12, :cond_0

    new-instance v13, Lcom/mstar/android/tvapi/common/exception/NativeCallFailException;

    const-string v14, "funtion failed at tvservice "

    invoke-direct {v13, v14}, Lcom/mstar/android/tvapi/common/exception/NativeCallFailException;-><init>(Ljava/lang/String;)V

    throw v13

    :cond_0
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v9}, Landroid/os/Parcel;->readInt()I

    move-result v2

    const/4 v7, -0x1

    const/4 v5, -0x1

    const/4 v6, 0x0

    :goto_0
    if-ge v6, v2, :cond_2

    new-instance v1, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-direct {v1}, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;-><init>()V

    invoke-virtual {v9}, Landroid/os/Parcel;->readInt()I

    move-result v13

    iput v13, v1, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->startTime:I

    invoke-virtual {v9}, Landroid/os/Parcel;->readInt()I

    move-result v13

    iput v13, v1, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->endTime:I

    invoke-virtual {v9}, Landroid/os/Parcel;->readInt()I

    move-result v13

    iput v13, v1, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->durationTime:I

    invoke-virtual {v9}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v13

    iput-object v13, v1, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->name:Ljava/lang/String;

    invoke-virtual {v9}, Landroid/os/Parcel;->readInt()I

    move-result v13

    iput v13, v1, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->eventId:I

    invoke-virtual {v9}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-nez v7, :cond_1

    const/4 v13, 0x0

    iput-boolean v13, v1, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->isScrambled:Z

    :goto_1
    invoke-virtual {v9}, Landroid/os/Parcel;->readInt()I

    move-result v13

    int-to-short v13, v13

    iput-short v13, v1, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->genre:S

    invoke-virtual {v9}, Landroid/os/Parcel;->readInt()I

    move-result v13

    int-to-short v13, v13

    iput-short v13, v1, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->parentalRating:S

    invoke-virtual {v9}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v13

    iput-object v13, v1, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->description:Ljava/lang/String;

    invoke-virtual {v9}, Landroid/os/Parcel;->readInt()I

    move-result v13

    iput v13, v1, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->originalStartTime:I

    invoke-virtual {v9}, Landroid/os/Parcel;->readInt()I

    move-result v5

    const/4 v8, -0x1

    :try_start_0
    invoke-static {v5}, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo$EnumEpgFunctionStatus;->getOrdinalThroughValue(I)I
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvOutOfBoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v8

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo$EnumEpgFunctionStatus;->values()[Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo$EnumEpgFunctionStatus;

    move-result-object v13

    aget-object v13, v13, v8

    invoke-virtual {v1, v13}, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->setEpgFunctionStatus(Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo$EnumEpgFunctionStatus;)V

    invoke-virtual {v11, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_2
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    :cond_1
    const/4 v13, 0x1

    iput-boolean v13, v1, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->isScrambled:Z

    goto :goto_1

    :catch_0
    move-exception v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/exception/TvOutOfBoundException;->printStackTrace()V

    goto :goto_2

    :cond_2
    invoke-virtual {v10}, Landroid/os/Parcel;->recycle()V

    invoke-virtual {v9}, Landroid/os/Parcel;->recycle()V

    return-object v11
.end method

.method public final getEventInfoById(SIS)Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;
    .locals 9
    .param p1    # S
    .param p2    # I
    .param p3    # S
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v4

    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v3

    const-string v7, "mstar.IEpgManager"

    invoke-virtual {v4, v7}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {v4, p2}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {v4, p3}, Landroid/os/Parcel;->writeInt(I)V

    invoke-direct {p0, v4, v3}, Lcom/mstar/android/tvapi/dtv/common/EpgManager;->_getEventInfoById(Landroid/os/Parcel;Landroid/os/Parcel;)I

    invoke-virtual {v3}, Landroid/os/Parcel;->readInt()I

    move-result v6

    if-nez v6, :cond_0

    new-instance v7, Lcom/mstar/android/tvapi/common/exception/NativeCallFailException;

    const-string v8, "funtion failed at tvservice "

    invoke-direct {v7, v8}, Lcom/mstar/android/tvapi/common/exception/NativeCallFailException;-><init>(Ljava/lang/String;)V

    throw v7

    :cond_0
    new-instance v5, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-direct {v5}, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;-><init>()V

    const/4 v1, -0x1

    const/4 v0, -0x1

    invoke-virtual {v3}, Landroid/os/Parcel;->readInt()I

    move-result v7

    iput v7, v5, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->startTime:I

    invoke-virtual {v3}, Landroid/os/Parcel;->readInt()I

    move-result v7

    iput v7, v5, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->endTime:I

    invoke-virtual {v3}, Landroid/os/Parcel;->readInt()I

    move-result v7

    iput v7, v5, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->durationTime:I

    invoke-virtual {v3}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v5, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->name:Ljava/lang/String;

    invoke-virtual {v3}, Landroid/os/Parcel;->readInt()I

    move-result v7

    iput v7, v5, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->eventId:I

    invoke-virtual {v3}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-nez v1, :cond_1

    const/4 v7, 0x0

    iput-boolean v7, v5, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->isScrambled:Z

    :goto_0
    invoke-virtual {v3}, Landroid/os/Parcel;->readInt()I

    move-result v7

    int-to-short v7, v7

    iput-short v7, v5, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->genre:S

    invoke-virtual {v3}, Landroid/os/Parcel;->readInt()I

    move-result v7

    int-to-short v7, v7

    iput-short v7, v5, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->parentalRating:S

    invoke-virtual {v3}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v5, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->description:Ljava/lang/String;

    invoke-virtual {v3}, Landroid/os/Parcel;->readInt()I

    move-result v7

    iput v7, v5, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->originalStartTime:I

    invoke-virtual {v3}, Landroid/os/Parcel;->readInt()I

    move-result v0

    invoke-static {v0}, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo$EnumEpgFunctionStatus;->getOrdinalThroughValue(I)I

    move-result v2

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo$EnumEpgFunctionStatus;->values()[Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo$EnumEpgFunctionStatus;

    move-result-object v7

    aget-object v7, v7, v2

    invoke-virtual {v5, v7}, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->setEpgFunctionStatus(Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo$EnumEpgFunctionStatus;)V

    invoke-virtual {v4}, Landroid/os/Parcel;->recycle()V

    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    return-object v5

    :cond_1
    const/4 v7, 0x1

    iput-boolean v7, v5, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->isScrambled:Z

    goto :goto_0
.end method

.method public final getEventInfoByRctLink(Lcom/mstar/android/tvapi/dtv/vo/EpgTrailerLinkInfo;)Ljava/util/ArrayList;
    .locals 13
    .param p1    # Lcom/mstar/android/tvapi/dtv/vo/EpgTrailerLinkInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/mstar/android/tvapi/dtv/vo/EpgTrailerLinkInfo;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mstar/android/tvapi/dtv/vo/EpgCridEventInfo;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v8

    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v7

    const-string v11, "mstar.IEpgManager"

    invoke-virtual {v8, v11}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    iget v11, p1, Lcom/mstar/android/tvapi/dtv/vo/EpgTrailerLinkInfo;->cridType:I

    invoke-virtual {v8, v11}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v11, p1, Lcom/mstar/android/tvapi/dtv/vo/EpgTrailerLinkInfo;->iconId:S

    invoke-virtual {v8, v11}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v11, p1, Lcom/mstar/android/tvapi/dtv/vo/EpgTrailerLinkInfo;->pEventTitle:Ljava/lang/String;

    invoke-virtual {v8, v11}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v11, p1, Lcom/mstar/android/tvapi/dtv/vo/EpgTrailerLinkInfo;->promotionText:Ljava/lang/String;

    invoke-virtual {v8, v11}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v11, p1, Lcom/mstar/android/tvapi/dtv/vo/EpgTrailerLinkInfo;->trailerCrid:Ljava/lang/String;

    invoke-virtual {v8, v11}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    invoke-direct {p0, v8, v7}, Lcom/mstar/android/tvapi/dtv/common/EpgManager;->_getEventInfoByRctLink(Landroid/os/Parcel;Landroid/os/Parcel;)I

    invoke-virtual {v7}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-nez v10, :cond_0

    new-instance v11, Lcom/mstar/android/tvapi/common/exception/NativeCallFailException;

    const-string v12, "funtion failed at tvservice "

    invoke-direct {v11, v12}, Lcom/mstar/android/tvapi/common/exception/NativeCallFailException;-><init>(Ljava/lang/String;)V

    throw v11

    :cond_0
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v7}, Landroid/os/Parcel;->readInt()I

    move-result v1

    const/4 v5, -0x1

    const/4 v3, -0x1

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v1, :cond_2

    new-instance v0, Lcom/mstar/android/tvapi/dtv/vo/EpgCridEventInfo;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/dtv/vo/EpgCridEventInfo;-><init>()V

    invoke-virtual {v7}, Landroid/os/Parcel;->readInt()I

    move-result v11

    int-to-short v11, v11

    iput-short v11, v0, Lcom/mstar/android/tvapi/dtv/vo/EpgCridEventInfo;->serviceType:S

    invoke-virtual {v7}, Landroid/os/Parcel;->readInt()I

    move-result v11

    iput v11, v0, Lcom/mstar/android/tvapi/dtv/vo/EpgCridEventInfo;->serviceNumber:I

    iget-object v11, v0, Lcom/mstar/android/tvapi/dtv/vo/EpgCridEventInfo;->eventInfo:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-virtual {v7}, Landroid/os/Parcel;->readInt()I

    move-result v12

    iput v12, v11, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->startTime:I

    iget-object v11, v0, Lcom/mstar/android/tvapi/dtv/vo/EpgCridEventInfo;->eventInfo:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-virtual {v7}, Landroid/os/Parcel;->readInt()I

    move-result v12

    iput v12, v11, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->endTime:I

    iget-object v11, v0, Lcom/mstar/android/tvapi/dtv/vo/EpgCridEventInfo;->eventInfo:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-virtual {v7}, Landroid/os/Parcel;->readInt()I

    move-result v12

    iput v12, v11, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->durationTime:I

    iget-object v11, v0, Lcom/mstar/android/tvapi/dtv/vo/EpgCridEventInfo;->eventInfo:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-virtual {v7}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v12

    iput-object v12, v11, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->name:Ljava/lang/String;

    iget-object v11, v0, Lcom/mstar/android/tvapi/dtv/vo/EpgCridEventInfo;->eventInfo:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-virtual {v7}, Landroid/os/Parcel;->readInt()I

    move-result v12

    iput v12, v11, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->eventId:I

    invoke-virtual {v7}, Landroid/os/Parcel;->readInt()I

    move-result v5

    if-nez v5, :cond_1

    iget-object v11, v0, Lcom/mstar/android/tvapi/dtv/vo/EpgCridEventInfo;->eventInfo:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    const/4 v12, 0x0

    iput-boolean v12, v11, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->isScrambled:Z

    :goto_1
    iget-object v11, v0, Lcom/mstar/android/tvapi/dtv/vo/EpgCridEventInfo;->eventInfo:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-virtual {v7}, Landroid/os/Parcel;->readInt()I

    move-result v12

    int-to-short v12, v12

    iput-short v12, v11, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->genre:S

    iget-object v11, v0, Lcom/mstar/android/tvapi/dtv/vo/EpgCridEventInfo;->eventInfo:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-virtual {v7}, Landroid/os/Parcel;->readInt()I

    move-result v12

    int-to-short v12, v12

    iput-short v12, v11, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->parentalRating:S

    iget-object v11, v0, Lcom/mstar/android/tvapi/dtv/vo/EpgCridEventInfo;->eventInfo:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-virtual {v7}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v12

    iput-object v12, v11, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->description:Ljava/lang/String;

    iget-object v11, v0, Lcom/mstar/android/tvapi/dtv/vo/EpgCridEventInfo;->eventInfo:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-virtual {v7}, Landroid/os/Parcel;->readInt()I

    move-result v12

    iput v12, v11, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->originalStartTime:I

    invoke-virtual {v7}, Landroid/os/Parcel;->readInt()I

    move-result v3

    const/4 v6, -0x1

    :try_start_0
    invoke-static {v3}, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo$EnumEpgFunctionStatus;->getOrdinalThroughValue(I)I
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvOutOfBoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v6

    iget-object v11, v0, Lcom/mstar/android/tvapi/dtv/vo/EpgCridEventInfo;->eventInfo:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo$EnumEpgFunctionStatus;->values()[Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo$EnumEpgFunctionStatus;

    move-result-object v12

    aget-object v12, v12, v6

    invoke-virtual {v11, v12}, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->setEpgFunctionStatus(Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo$EnumEpgFunctionStatus;)V

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_0

    :cond_1
    iget-object v11, v0, Lcom/mstar/android/tvapi/dtv/vo/EpgCridEventInfo;->eventInfo:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    const/4 v12, 0x1

    iput-boolean v12, v11, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->isScrambled:Z

    goto :goto_1

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/exception/TvOutOfBoundException;->printStackTrace()V

    goto :goto_2

    :cond_2
    invoke-virtual {v8}, Landroid/os/Parcel;->recycle()V

    invoke-virtual {v7}, Landroid/os/Parcel;->recycle()V

    return-object v9
.end method

.method public final getEventInfoByTime(IIIILandroid/text/format/Time;)Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;
    .locals 10
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # Landroid/text/format/Time;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    const v9, 0x12d53d80

    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    const-string v5, "mstar.IEpgManager"

    invoke-virtual {v2, v5}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    const/4 v5, 0x1

    invoke-virtual {p5, v5}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v5

    const-wide/16 v7, 0x3e8

    div-long/2addr v5, v7

    const-wide/32 v7, 0x12d53d80

    sub-long/2addr v5, v7

    long-to-int v0, v5

    invoke-virtual {v2, p1}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {v2, p2}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {v2, p3}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {v2, p4}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {v2, v0}, Landroid/os/Parcel;->writeInt(I)V

    invoke-direct {p0, v2, v1}, Lcom/mstar/android/tvapi/dtv/common/EpgManager;->_atsc_getEventInfoByTime(Landroid/os/Parcel;Landroid/os/Parcel;)I

    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-nez v4, :cond_0

    new-instance v5, Lcom/mstar/android/tvapi/common/exception/NativeCallFailException;

    const-string v6, "funtion failed at tvservice "

    invoke-direct {v5, v6}, Lcom/mstar/android/tvapi/common/exception/NativeCallFailException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_0
    new-instance v3, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;

    invoke-direct {v3, v1}, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;-><init>(Landroid/os/Parcel;)V

    iget v5, v3, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;->startTime:I

    add-int/2addr v5, v9

    iput v5, v3, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;->startTime:I

    iget v5, v3, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;->endTime:I

    add-int/2addr v5, v9

    iput v5, v3, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;->endTime:I

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    return-object v3
.end method

.method public final getEventInfoByTime(SILandroid/text/format/Time;)Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;
    .locals 12
    .param p1    # S
    .param p2    # I
    .param p3    # Landroid/text/format/Time;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v5

    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v4

    const-string v8, "mstar.IEpgManager"

    invoke-virtual {v5, v8}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    const/4 v8, 0x1

    invoke-virtual {p3, v8}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v8

    const-wide/16 v10, 0x3e8

    div-long/2addr v8, v10

    long-to-int v0, v8

    invoke-virtual {v5, p1}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {v5, p2}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {v5, v0}, Landroid/os/Parcel;->writeInt(I)V

    invoke-direct {p0, v5, v4}, Lcom/mstar/android/tvapi/dtv/common/EpgManager;->_getEventInfoByTime(Landroid/os/Parcel;Landroid/os/Parcel;)I

    invoke-virtual {v4}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-nez v7, :cond_0

    new-instance v8, Lcom/mstar/android/tvapi/common/exception/NativeCallFailException;

    const-string v9, "funtion failed at tvservice "

    invoke-direct {v8, v9}, Lcom/mstar/android/tvapi/common/exception/NativeCallFailException;-><init>(Ljava/lang/String;)V

    throw v8

    :cond_0
    new-instance v6, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-direct {v6}, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;-><init>()V

    const/4 v2, -0x1

    const/4 v1, -0x1

    invoke-virtual {v4}, Landroid/os/Parcel;->readInt()I

    move-result v8

    iput v8, v6, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->startTime:I

    invoke-virtual {v4}, Landroid/os/Parcel;->readInt()I

    move-result v8

    iput v8, v6, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->endTime:I

    invoke-virtual {v4}, Landroid/os/Parcel;->readInt()I

    move-result v8

    iput v8, v6, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->durationTime:I

    invoke-virtual {v4}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v8

    iput-object v8, v6, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->name:Ljava/lang/String;

    invoke-virtual {v4}, Landroid/os/Parcel;->readInt()I

    move-result v8

    iput v8, v6, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->eventId:I

    invoke-virtual {v4}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-nez v2, :cond_1

    const/4 v8, 0x0

    iput-boolean v8, v6, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->isScrambled:Z

    :goto_0
    invoke-virtual {v4}, Landroid/os/Parcel;->readInt()I

    move-result v8

    int-to-short v8, v8

    iput-short v8, v6, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->genre:S

    invoke-virtual {v4}, Landroid/os/Parcel;->readInt()I

    move-result v8

    int-to-short v8, v8

    iput-short v8, v6, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->parentalRating:S

    invoke-virtual {v4}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v8

    iput-object v8, v6, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->description:Ljava/lang/String;

    invoke-virtual {v4}, Landroid/os/Parcel;->readInt()I

    move-result v8

    iput v8, v6, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->originalStartTime:I

    invoke-virtual {v4}, Landroid/os/Parcel;->readInt()I

    move-result v1

    invoke-static {v1}, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo$EnumEpgFunctionStatus;->getOrdinalThroughValue(I)I

    move-result v3

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo$EnumEpgFunctionStatus;->values()[Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo$EnumEpgFunctionStatus;

    move-result-object v8

    aget-object v8, v8, v3

    invoke-virtual {v6, v8}, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->setEpgFunctionStatus(Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo$EnumEpgFunctionStatus;)V

    invoke-virtual {v5}, Landroid/os/Parcel;->recycle()V

    invoke-virtual {v4}, Landroid/os/Parcel;->recycle()V

    return-object v6

    :cond_1
    const/4 v8, 0x1

    iput-boolean v8, v6, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->isScrambled:Z

    goto :goto_0
.end method

.method public final getFirstEventInformation(Landroid/text/format/Time;)Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;
    .locals 10
    .param p1    # Landroid/text/format/Time;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    const v9, 0x12d53d80

    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    const-string v5, "mstar.IEpgManager"

    invoke-virtual {v2, v5}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    const/4 v5, 0x1

    invoke-virtual {p1, v5}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v5

    const-wide/16 v7, 0x3e8

    div-long/2addr v5, v7

    const-wide/32 v7, 0x12d53d80

    sub-long/2addr v5, v7

    long-to-int v0, v5

    invoke-virtual {v2, v0}, Landroid/os/Parcel;->writeInt(I)V

    invoke-direct {p0, v2, v1}, Lcom/mstar/android/tvapi/dtv/common/EpgManager;->_getFirstEventInformation(Landroid/os/Parcel;Landroid/os/Parcel;)I

    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-nez v4, :cond_0

    new-instance v5, Lcom/mstar/android/tvapi/common/exception/NativeCallFailException;

    const-string v6, "funtion failed at tvservice "

    invoke-direct {v5, v6}, Lcom/mstar/android/tvapi/common/exception/NativeCallFailException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_0
    new-instance v3, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;

    invoke-direct {v3, v1}, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;-><init>(Landroid/os/Parcel;)V

    iget v5, v3, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;->startTime:I

    add-int/2addr v5, v9

    iput v5, v3, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;->startTime:I

    iget v5, v3, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;->endTime:I

    add-int/2addr v5, v9

    iput v5, v3, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;->endTime:I

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    return-object v3
.end method

.method public final getNextEventInformation()Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    const v5, 0x12d53d80

    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    const-string v4, "mstar.IEpgManager"

    invoke-virtual {v1, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    invoke-direct {p0, v1, v0}, Lcom/mstar/android/tvapi/dtv/common/EpgManager;->_getNextEventInformation(Landroid/os/Parcel;Landroid/os/Parcel;)I

    invoke-virtual {v0}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-nez v3, :cond_0

    new-instance v4, Lcom/mstar/android/tvapi/common/exception/NativeCallFailException;

    const-string v5, "funtion failed at tvservice "

    invoke-direct {v4, v5}, Lcom/mstar/android/tvapi/common/exception/NativeCallFailException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_0
    new-instance v2, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;

    invoke-direct {v2, v0}, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;-><init>(Landroid/os/Parcel;)V

    iget v4, v2, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;->startTime:I

    add-int/2addr v4, v5

    iput v4, v2, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;->startTime:I

    iget v4, v2, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;->endTime:I

    add-int/2addr v4, v5

    iput v4, v2, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;->endTime:I

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    return-object v2
.end method

.method public final native getNvodTimeShiftEventCount(SI)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public getNvodTimeShiftEventInfo(SIILcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgDescriptionType;)Ljava/util/ArrayList;
    .locals 14
    .param p1    # S
    .param p2    # I
    .param p3    # I
    .param p4    # Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgDescriptionType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(SII",
            "Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgDescriptionType;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mstar/android/tvapi/dtv/vo/NvodEventInfo;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v9

    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v8

    const-string v12, "mstar.IEpgManager"

    invoke-virtual {v9, v12}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    invoke-virtual {v9, p1}, Landroid/os/Parcel;->writeInt(I)V

    move/from16 v0, p2

    invoke-virtual {v9, v0}, Landroid/os/Parcel;->writeInt(I)V

    move/from16 v0, p3

    invoke-virtual {v9, v0}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual/range {p4 .. p4}, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgDescriptionType;->ordinal()I

    move-result v12

    invoke-virtual {v9, v12}, Landroid/os/Parcel;->writeInt(I)V

    invoke-direct {p0, v9, v8}, Lcom/mstar/android/tvapi/dtv/common/EpgManager;->_getNvodTimeShiftEventInfo(Landroid/os/Parcel;Landroid/os/Parcel;)I

    invoke-virtual {v8}, Landroid/os/Parcel;->readInt()I

    move-result v11

    if-nez v11, :cond_0

    new-instance v12, Lcom/mstar/android/tvapi/common/exception/NativeCallFailException;

    const-string v13, "funtion failed at tvservice "

    invoke-direct {v12, v13}, Lcom/mstar/android/tvapi/common/exception/NativeCallFailException;-><init>(Ljava/lang/String;)V

    throw v12

    :cond_0
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v8}, Landroid/os/Parcel;->readInt()I

    move-result v2

    const/4 v6, -0x1

    const/4 v4, -0x1

    const/4 v5, 0x0

    :goto_0
    if-ge v5, v2, :cond_2

    new-instance v1, Lcom/mstar/android/tvapi/dtv/vo/NvodEventInfo;

    invoke-direct {v1}, Lcom/mstar/android/tvapi/dtv/vo/NvodEventInfo;-><init>()V

    iget-object v12, v1, Lcom/mstar/android/tvapi/dtv/vo/NvodEventInfo;->epgEventInfo:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-virtual {v8}, Landroid/os/Parcel;->readInt()I

    move-result v13

    iput v13, v12, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->startTime:I

    iget-object v12, v1, Lcom/mstar/android/tvapi/dtv/vo/NvodEventInfo;->epgEventInfo:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-virtual {v8}, Landroid/os/Parcel;->readInt()I

    move-result v13

    iput v13, v12, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->endTime:I

    iget-object v12, v1, Lcom/mstar/android/tvapi/dtv/vo/NvodEventInfo;->epgEventInfo:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-virtual {v8}, Landroid/os/Parcel;->readInt()I

    move-result v13

    iput v13, v12, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->durationTime:I

    iget-object v12, v1, Lcom/mstar/android/tvapi/dtv/vo/NvodEventInfo;->epgEventInfo:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-virtual {v8}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v13

    iput-object v13, v12, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->name:Ljava/lang/String;

    iget-object v12, v1, Lcom/mstar/android/tvapi/dtv/vo/NvodEventInfo;->epgEventInfo:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-virtual {v8}, Landroid/os/Parcel;->readInt()I

    move-result v13

    iput v13, v12, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->eventId:I

    invoke-virtual {v8}, Landroid/os/Parcel;->readInt()I

    move-result v6

    if-nez v6, :cond_1

    iget-object v12, v1, Lcom/mstar/android/tvapi/dtv/vo/NvodEventInfo;->epgEventInfo:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    const/4 v13, 0x0

    iput-boolean v13, v12, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->isScrambled:Z

    :goto_1
    iget-object v12, v1, Lcom/mstar/android/tvapi/dtv/vo/NvodEventInfo;->epgEventInfo:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-virtual {v8}, Landroid/os/Parcel;->readInt()I

    move-result v13

    int-to-short v13, v13

    iput-short v13, v12, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->genre:S

    iget-object v12, v1, Lcom/mstar/android/tvapi/dtv/vo/NvodEventInfo;->epgEventInfo:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-virtual {v8}, Landroid/os/Parcel;->readInt()I

    move-result v13

    int-to-short v13, v13

    iput-short v13, v12, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->parentalRating:S

    iget-object v12, v1, Lcom/mstar/android/tvapi/dtv/vo/NvodEventInfo;->epgEventInfo:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-virtual {v8}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v13

    iput-object v13, v12, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->description:Ljava/lang/String;

    iget-object v12, v1, Lcom/mstar/android/tvapi/dtv/vo/NvodEventInfo;->epgEventInfo:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-virtual {v8}, Landroid/os/Parcel;->readInt()I

    move-result v13

    iput v13, v12, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->originalStartTime:I

    invoke-virtual {v8}, Landroid/os/Parcel;->readInt()I

    move-result v4

    const/4 v7, -0x1

    :try_start_0
    invoke-static {v4}, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo$EnumEpgFunctionStatus;->getOrdinalThroughValue(I)I
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvOutOfBoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v7

    iget-object v12, v1, Lcom/mstar/android/tvapi/dtv/vo/NvodEventInfo;->epgEventInfo:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo$EnumEpgFunctionStatus;->values()[Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo$EnumEpgFunctionStatus;

    move-result-object v13

    aget-object v13, v13, v7

    invoke-virtual {v12, v13}, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->setEpgFunctionStatus(Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo$EnumEpgFunctionStatus;)V

    iget-object v12, v1, Lcom/mstar/android/tvapi/dtv/vo/NvodEventInfo;->timeShiftedServiceIds:Lcom/mstar/android/tvapi/common/vo/DtvTripleId;

    invoke-virtual {v8}, Landroid/os/Parcel;->readInt()I

    move-result v13

    iput v13, v12, Lcom/mstar/android/tvapi/common/vo/DtvTripleId;->originalNetworkId:I

    iget-object v12, v1, Lcom/mstar/android/tvapi/dtv/vo/NvodEventInfo;->timeShiftedServiceIds:Lcom/mstar/android/tvapi/common/vo/DtvTripleId;

    invoke-virtual {v8}, Landroid/os/Parcel;->readInt()I

    move-result v13

    iput v13, v12, Lcom/mstar/android/tvapi/common/vo/DtvTripleId;->transportStreamId:I

    iget-object v12, v1, Lcom/mstar/android/tvapi/dtv/vo/NvodEventInfo;->timeShiftedServiceIds:Lcom/mstar/android/tvapi/common/vo/DtvTripleId;

    invoke-virtual {v8}, Landroid/os/Parcel;->readInt()I

    move-result v13

    iput v13, v12, Lcom/mstar/android/tvapi/common/vo/DtvTripleId;->serviceId:I

    invoke-virtual {v10, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_2
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_0

    :cond_1
    iget-object v12, v1, Lcom/mstar/android/tvapi/dtv/vo/NvodEventInfo;->epgEventInfo:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    const/4 v13, 0x1

    iput-boolean v13, v12, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->isScrambled:Z

    goto :goto_1

    :catch_0
    move-exception v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/exception/TvOutOfBoundException;->printStackTrace()V

    goto :goto_2

    :cond_2
    invoke-virtual {v9}, Landroid/os/Parcel;->recycle()V

    invoke-virtual {v8}, Landroid/os/Parcel;->recycle()V

    return-object v10
.end method

.method public getPresentFollowingEventInfo(SIZLcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgDescriptionType;)Lcom/mstar/android/tvapi/common/vo/PresentFollowingEventInfo;
    .locals 16
    .param p1    # S
    .param p2    # I
    .param p3    # Z
    .param p4    # Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgDescriptionType;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v9

    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v8

    const-string v13, "mstar.IEpgManager"

    invoke-virtual {v9, v13}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    move/from16 v0, p1

    invoke-virtual {v9, v0}, Landroid/os/Parcel;->writeInt(I)V

    move/from16 v0, p2

    invoke-virtual {v9, v0}, Landroid/os/Parcel;->writeInt(I)V

    if-eqz p3, :cond_0

    const/4 v13, 0x1

    invoke-virtual {v9, v13}, Landroid/os/Parcel;->writeInt(I)V

    :goto_0
    invoke-virtual/range {p4 .. p4}, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgDescriptionType;->ordinal()I

    move-result v13

    invoke-virtual {v9, v13}, Landroid/os/Parcel;->writeInt(I)V

    move-object/from16 v0, p0

    invoke-direct {v0, v9, v8}, Lcom/mstar/android/tvapi/dtv/common/EpgManager;->_getPresentFollowingEventInfo(Landroid/os/Parcel;Landroid/os/Parcel;)I

    invoke-virtual {v8}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-nez v7, :cond_1

    new-instance v13, Lcom/mstar/android/tvapi/common/exception/NativeCallFailException;

    const-string v14, "funtion failed at tvservice "

    invoke-direct {v13, v14}, Lcom/mstar/android/tvapi/common/exception/NativeCallFailException;-><init>(Ljava/lang/String;)V

    throw v13

    :cond_0
    const/4 v13, 0x0

    invoke-virtual {v9, v13}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    :cond_1
    new-instance v10, Lcom/mstar/android/tvapi/common/vo/PresentFollowingEventInfo;

    invoke-direct {v10}, Lcom/mstar/android/tvapi/common/vo/PresentFollowingEventInfo;-><init>()V

    iget-object v13, v10, Lcom/mstar/android/tvapi/common/vo/PresentFollowingEventInfo;->componentInfo:Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumVideoType;->values()[Lcom/mstar/android/tvapi/common/vo/EnumVideoType;

    move-result-object v14

    invoke-virtual {v8}, Landroid/os/Parcel;->readInt()I

    move-result v15

    aget-object v14, v14, v15

    invoke-virtual {v13, v14}, Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;->setVideoType(Lcom/mstar/android/tvapi/common/vo/EnumVideoType;)V

    invoke-virtual {v8}, Landroid/os/Parcel;->readInt()I

    move-result v6

    if-nez v6, :cond_2

    iget-object v13, v10, Lcom/mstar/android/tvapi/common/vo/PresentFollowingEventInfo;->componentInfo:Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;

    const/4 v14, 0x0

    iput-boolean v14, v13, Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;->mheg5Service:Z

    :goto_1
    invoke-virtual {v8}, Landroid/os/Parcel;->readInt()I

    move-result v11

    if-nez v11, :cond_3

    iget-object v13, v10, Lcom/mstar/android/tvapi/common/vo/PresentFollowingEventInfo;->componentInfo:Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;

    const/4 v14, 0x0

    iput-boolean v14, v13, Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;->subtitleService:Z

    :goto_2
    invoke-virtual {v8}, Landroid/os/Parcel;->readInt()I

    move-result v12

    if-nez v12, :cond_4

    iget-object v13, v10, Lcom/mstar/android/tvapi/common/vo/PresentFollowingEventInfo;->componentInfo:Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;

    const/4 v14, 0x0

    iput-boolean v14, v13, Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;->teletextService:Z

    :goto_3
    invoke-virtual {v8}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-nez v1, :cond_5

    iget-object v13, v10, Lcom/mstar/android/tvapi/common/vo/PresentFollowingEventInfo;->componentInfo:Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;

    const/4 v14, 0x0

    iput-boolean v14, v13, Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;->ccService:Z

    :goto_4
    iget-object v13, v10, Lcom/mstar/android/tvapi/common/vo/PresentFollowingEventInfo;->componentInfo:Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumDtvVideoQuality;->values()[Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumDtvVideoQuality;

    move-result-object v14

    invoke-virtual {v8}, Landroid/os/Parcel;->readInt()I

    move-result v15

    aget-object v14, v14, v15

    invoke-virtual {v13, v14}, Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;->setDtvVideoQuality(Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumDtvVideoQuality;)V

    invoke-virtual {v8}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-nez v4, :cond_6

    iget-object v13, v10, Lcom/mstar/android/tvapi/common/vo/PresentFollowingEventInfo;->componentInfo:Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;

    const/4 v14, 0x0

    iput-boolean v14, v13, Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;->isAd:Z

    :goto_5
    iget-object v13, v10, Lcom/mstar/android/tvapi/common/vo/PresentFollowingEventInfo;->componentInfo:Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;

    invoke-virtual {v8}, Landroid/os/Parcel;->readInt()I

    move-result v14

    int-to-short v14, v14

    iput-short v14, v13, Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;->audioTrackNum:S

    iget-object v13, v10, Lcom/mstar/android/tvapi/common/vo/PresentFollowingEventInfo;->componentInfo:Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;

    invoke-virtual {v8}, Landroid/os/Parcel;->readInt()I

    move-result v14

    int-to-short v14, v14

    iput-short v14, v13, Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;->subtitleNum:S

    iget-object v13, v10, Lcom/mstar/android/tvapi/common/vo/PresentFollowingEventInfo;->componentInfo:Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumAspectRatioCode;->values()[Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumAspectRatioCode;

    move-result-object v14

    invoke-virtual {v8}, Landroid/os/Parcel;->readInt()I

    move-result v15

    aget-object v14, v14, v15

    invoke-virtual {v13, v14}, Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;->setAspectRatioCode(Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumAspectRatioCode;)V

    invoke-virtual {v8}, Landroid/os/Parcel;->readInt()I

    move-result v13

    invoke-static {v13}, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;->getOrdinalThroughValue(I)I

    move-result v3

    iget-object v13, v10, Lcom/mstar/android/tvapi/common/vo/PresentFollowingEventInfo;->componentInfo:Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;->values()[Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;

    move-result-object v14

    aget-object v14, v14, v3

    invoke-virtual {v13, v14}, Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;->setGenreType(Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgMainGenreType;)V

    iget-object v13, v10, Lcom/mstar/android/tvapi/common/vo/PresentFollowingEventInfo;->componentInfo:Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;

    invoke-virtual {v8}, Landroid/os/Parcel;->readInt()I

    move-result v14

    int-to-short v14, v14

    iput-short v14, v13, Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;->parentalRating:S

    iget-object v13, v10, Lcom/mstar/android/tvapi/common/vo/PresentFollowingEventInfo;->eventInfo:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-virtual {v8}, Landroid/os/Parcel;->readInt()I

    move-result v14

    iput v14, v13, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->originalStartTime:I

    iget-object v13, v10, Lcom/mstar/android/tvapi/common/vo/PresentFollowingEventInfo;->eventInfo:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-virtual {v8}, Landroid/os/Parcel;->readInt()I

    move-result v14

    iput v14, v13, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->durationTime:I

    iget-object v13, v10, Lcom/mstar/android/tvapi/common/vo/PresentFollowingEventInfo;->eventInfo:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-virtual {v8}, Landroid/os/Parcel;->readInt()I

    move-result v14

    iput v14, v13, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->startTime:I

    iget-object v13, v10, Lcom/mstar/android/tvapi/common/vo/PresentFollowingEventInfo;->eventInfo:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-virtual {v8}, Landroid/os/Parcel;->readInt()I

    move-result v14

    iput v14, v13, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->endTime:I

    invoke-virtual {v8}, Landroid/os/Parcel;->readInt()I

    move-result v13

    invoke-static {v13}, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo$EnumEpgFunctionStatus;->getOrdinalThroughValue(I)I

    move-result v2

    iget-object v13, v10, Lcom/mstar/android/tvapi/common/vo/PresentFollowingEventInfo;->eventInfo:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo$EnumEpgFunctionStatus;->values()[Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo$EnumEpgFunctionStatus;

    move-result-object v14

    aget-object v14, v14, v2

    invoke-virtual {v13, v14}, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->setEpgFunctionStatus(Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo$EnumEpgFunctionStatus;)V

    iget-object v13, v10, Lcom/mstar/android/tvapi/common/vo/PresentFollowingEventInfo;->eventInfo:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-virtual {v8}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v14

    iput-object v14, v13, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->name:Ljava/lang/String;

    iget-object v13, v10, Lcom/mstar/android/tvapi/common/vo/PresentFollowingEventInfo;->eventInfo:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-virtual {v8}, Landroid/os/Parcel;->readInt()I

    move-result v14

    iput v14, v13, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->eventId:I

    invoke-virtual {v8}, Landroid/os/Parcel;->readInt()I

    move-result v5

    if-nez v5, :cond_7

    iget-object v13, v10, Lcom/mstar/android/tvapi/common/vo/PresentFollowingEventInfo;->eventInfo:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    const/4 v14, 0x0

    iput-boolean v14, v13, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->isScrambled:Z

    :goto_6
    iget-object v13, v10, Lcom/mstar/android/tvapi/common/vo/PresentFollowingEventInfo;->eventInfo:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-virtual {v8}, Landroid/os/Parcel;->readInt()I

    move-result v14

    int-to-short v14, v14

    iput-short v14, v13, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->genre:S

    iget-object v13, v10, Lcom/mstar/android/tvapi/common/vo/PresentFollowingEventInfo;->eventInfo:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-virtual {v8}, Landroid/os/Parcel;->readInt()I

    move-result v14

    int-to-short v14, v14

    iput-short v14, v13, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->parentalRating:S

    iget-object v13, v10, Lcom/mstar/android/tvapi/common/vo/PresentFollowingEventInfo;->eventInfo:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-virtual {v8}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v14

    iput-object v14, v13, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->description:Ljava/lang/String;

    invoke-virtual {v9}, Landroid/os/Parcel;->recycle()V

    invoke-virtual {v8}, Landroid/os/Parcel;->recycle()V

    return-object v10

    :cond_2
    iget-object v13, v10, Lcom/mstar/android/tvapi/common/vo/PresentFollowingEventInfo;->componentInfo:Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;

    const/4 v14, 0x1

    iput-boolean v14, v13, Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;->mheg5Service:Z

    goto/16 :goto_1

    :cond_3
    iget-object v13, v10, Lcom/mstar/android/tvapi/common/vo/PresentFollowingEventInfo;->componentInfo:Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;

    const/4 v14, 0x1

    iput-boolean v14, v13, Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;->subtitleService:Z

    goto/16 :goto_2

    :cond_4
    iget-object v13, v10, Lcom/mstar/android/tvapi/common/vo/PresentFollowingEventInfo;->componentInfo:Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;

    const/4 v14, 0x1

    iput-boolean v14, v13, Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;->teletextService:Z

    goto/16 :goto_3

    :cond_5
    iget-object v13, v10, Lcom/mstar/android/tvapi/common/vo/PresentFollowingEventInfo;->componentInfo:Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;

    const/4 v14, 0x1

    iput-boolean v14, v13, Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;->ccService:Z

    goto/16 :goto_4

    :cond_6
    iget-object v13, v10, Lcom/mstar/android/tvapi/common/vo/PresentFollowingEventInfo;->componentInfo:Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;

    const/4 v14, 0x1

    iput-boolean v14, v13, Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;->isAd:Z

    goto/16 :goto_5

    :cond_7
    iget-object v13, v10, Lcom/mstar/android/tvapi/common/vo/PresentFollowingEventInfo;->eventInfo:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    const/4 v14, 0x1

    iput-boolean v14, v13, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->isScrambled:Z

    goto :goto_6
.end method

.method public final getRctTrailerLink()Ljava/util/ArrayList;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mstar/android/tvapi/dtv/vo/EpgTrailerLinkInfo;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v4

    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v3

    const-string v7, "mstar.IEpgManager"

    invoke-virtual {v4, v7}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    invoke-direct {p0, v4, v3}, Lcom/mstar/android/tvapi/dtv/common/EpgManager;->_getRctTrailerLink(Landroid/os/Parcel;Landroid/os/Parcel;)I

    invoke-virtual {v3}, Landroid/os/Parcel;->readInt()I

    move-result v6

    if-nez v6, :cond_0

    new-instance v7, Lcom/mstar/android/tvapi/common/exception/NativeCallFailException;

    const-string v8, "funtion failed at tvservice "

    invoke-direct {v7, v8}, Lcom/mstar/android/tvapi/common/exception/NativeCallFailException;-><init>(Ljava/lang/String;)V

    throw v7

    :cond_0
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v3}, Landroid/os/Parcel;->readInt()I

    move-result v1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    new-instance v0, Lcom/mstar/android/tvapi/dtv/vo/EpgTrailerLinkInfo;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/dtv/vo/EpgTrailerLinkInfo;-><init>()V

    invoke-virtual {v3}, Landroid/os/Parcel;->readInt()I

    move-result v7

    iput v7, v0, Lcom/mstar/android/tvapi/dtv/vo/EpgTrailerLinkInfo;->cridType:I

    invoke-virtual {v3}, Landroid/os/Parcel;->readInt()I

    move-result v7

    int-to-short v7, v7

    iput-short v7, v0, Lcom/mstar/android/tvapi/dtv/vo/EpgTrailerLinkInfo;->iconId:S

    invoke-virtual {v3}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v0, Lcom/mstar/android/tvapi/dtv/vo/EpgTrailerLinkInfo;->pEventTitle:Ljava/lang/String;

    invoke-virtual {v3}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v0, Lcom/mstar/android/tvapi/dtv/vo/EpgTrailerLinkInfo;->promotionText:Ljava/lang/String;

    invoke-virtual {v3}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v0, Lcom/mstar/android/tvapi/dtv/vo/EpgTrailerLinkInfo;->trailerCrid:Ljava/lang/String;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v4}, Landroid/os/Parcel;->recycle()V

    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    return-object v5
.end method

.method public final native resetEpgProgramPriority()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native setEpgProgramPriority(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public final native setEpgProgramPriority(SI)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method
