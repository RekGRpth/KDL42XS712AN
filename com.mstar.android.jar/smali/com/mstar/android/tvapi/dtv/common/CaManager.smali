.class public final Lcom/mstar/android/tvapi/dtv/common/CaManager;
.super Ljava/lang/Object;
.source "CaManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mstar/android/tvapi/dtv/common/CaManager$1;,
        Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;,
        Lcom/mstar/android/tvapi/dtv/common/CaManager$OnCaEventListener;,
        Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;
    }
.end annotation


# static fields
.field private static _camanager:Lcom/mstar/android/tvapi/dtv/common/CaManager;

.field public static _current_detitle_type:I

.field public static _current_email_type:I

.field private static _current_event:I

.field private static _current_msg_type:I


# instance fields
.field private mEventHandler:Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;

.field private mNativeContext:I

.field private mOnEventListener:Lcom/mstar/android/tvapi/dtv/common/CaManager$OnCaEventListener;

.field private mcamanagerContext:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x0

    :try_start_0
    const-string v1, "camanager_jni"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/CaManager;->native_init()V
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const/4 v1, 0x0

    sput-object v1, Lcom/mstar/android/tvapi/dtv/common/CaManager;->_camanager:Lcom/mstar/android/tvapi/dtv/common/CaManager;

    sput v4, Lcom/mstar/android/tvapi/dtv/common/CaManager;->_current_event:I

    sput v4, Lcom/mstar/android/tvapi/dtv/common/CaManager;->_current_msg_type:I

    sput v4, Lcom/mstar/android/tvapi/dtv/common/CaManager;->_current_email_type:I

    sput v4, Lcom/mstar/android/tvapi/dtv/common/CaManager;->_current_detitle_type:I

    return-void

    :catch_0
    move-exception v0

    sget-object v1, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Cannot load camanager_jni library:\n "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/UnsatisfiedLinkError;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;

    invoke-direct {v1, p0, p0, v0}, Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;-><init>(Lcom/mstar/android/tvapi/dtv/common/CaManager;Lcom/mstar/android/tvapi/dtv/common/CaManager;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/mstar/android/tvapi/dtv/common/CaManager;->mEventHandler:Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;

    :goto_0
    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-direct {p0, v1}, Lcom/mstar/android/tvapi/dtv/common/CaManager;->native_setup(Ljava/lang/Object;)V

    return-void

    :cond_0
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    if-eqz v0, :cond_1

    new-instance v1, Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;

    invoke-direct {v1, p0, p0, v0}, Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;-><init>(Lcom/mstar/android/tvapi/dtv/common/CaManager;Lcom/mstar/android/tvapi/dtv/common/CaManager;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/mstar/android/tvapi/dtv/common/CaManager;->mEventHandler:Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/mstar/android/tvapi/dtv/common/CaManager;->mEventHandler:Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;

    goto :goto_0
.end method

.method public static final native CaChangePin(Ljava/lang/String;Ljava/lang/String;)S
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public static final native CaDelDetitleChkNum(SI)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public static final native CaDelEmail(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public static final native CaGetACList(S)Lcom/mstar/android/tvapi/dtv/vo/CaACListInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public static final native CaGetCardSN()Lcom/mstar/android/tvapi/dtv/vo/CACardSNInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public static final native CaGetDetitleChkNums(S)Lcom/mstar/android/tvapi/dtv/vo/CaDetitleChkNums;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public static final native CaGetDetitleReaded(S)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public static final native CaGetEmailContent(I)Lcom/mstar/android/tvapi/dtv/vo/CaEmailContentInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public static final native CaGetEmailHead(I)Lcom/mstar/android/tvapi/dtv/vo/CaEmailHeadInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public static final native CaGetEmailHeads(SS)Lcom/mstar/android/tvapi/dtv/vo/CaEmailHeadsInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public static final native CaGetEmailSpaceInfo()Lcom/mstar/android/tvapi/dtv/vo/CaEmailSpaceInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public static final native CaGetEntitleIDs(S)Lcom/mstar/android/tvapi/dtv/vo/CaEntitleIDs;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public static final native CaGetIPPVProgram(S)Lcom/mstar/android/tvapi/dtv/vo/CaIPPVProgramInfos;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public static final native CaGetOperatorChildStatus(S)Lcom/mstar/android/tvapi/dtv/vo/CaOperatorChildStatus;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public static final native CaGetOperatorIds()Lcom/mstar/android/tvapi/dtv/vo/CaOperatorIds;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public static final native CaGetOperatorInfo(S)Lcom/mstar/android/tvapi/dtv/vo/CaOperatorInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public static final native CaGetPlatformID()S
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public static final native CaGetRating()Lcom/mstar/android/tvapi/dtv/vo/CARatingInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public static final native CaGetServiceEntitles(S)Lcom/mstar/android/tvapi/dtv/vo/CaServiceEntitles;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public static final native CaGetSlotIDs(S)Lcom/mstar/android/tvapi/dtv/vo/CaSlotIDs;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public static final native CaGetSlotInfo(SS)Lcom/mstar/android/tvapi/dtv/vo/CaSlotInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public static final native CaGetVer()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public static final native CaGetWorkTime()Lcom/mstar/android/tvapi/dtv/vo/CaWorkTimeInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public static final native CaIsPaired(SLjava/lang/String;)S
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public static final native CaOTAStateConfirm(II)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public static final native CaReadFeedDataFromParent(S)Lcom/mstar/android/tvapi/dtv/vo/CaFeedDataInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public static final native CaRefreshInterface()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public static final native CaSetRating(Ljava/lang/String;S)S
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public static final native CaSetWorkTime(Ljava/lang/String;Lcom/mstar/android/tvapi/dtv/vo/CaWorkTimeInfo;)S
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public static final native CaStopIPPVBuyDlg(Lcom/mstar/android/tvapi/dtv/vo/CaStopIPPVBuyDlgInfo;)S
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public static final native CaWriteFeedDataToChild(SLcom/mstar/android/tvapi/dtv/vo/CaFeedDataInfo;)S
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method private static PostEvent_ActionRequest(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/dtv/common/CaManager;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, v1, Lcom/mstar/android/tvapi/dtv/common/CaManager;->mEventHandler:Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;

    if-eqz v2, :cond_1

    iget-object v2, v1, Lcom/mstar/android/tvapi/dtv/common/CaManager;->mEventHandler:Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;->EV_CA_ACTION_REQUEST:Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/dtv/common/CaManager;->mEventHandler:Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;->sendMessage(Landroid/os/Message;)Z

    :cond_1
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "\n Native CA callback, PostEvent_ActionRequest"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static PostEvent_DetitleReceived(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/dtv/common/CaManager;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, v1, Lcom/mstar/android/tvapi/dtv/common/CaManager;->mEventHandler:Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;

    if-eqz v2, :cond_1

    sput p1, Lcom/mstar/android/tvapi/dtv/common/CaManager;->_current_detitle_type:I

    iget-object v2, v1, Lcom/mstar/android/tvapi/dtv/common/CaManager;->mEventHandler:Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;->EV_CA_DETITLE_RECEVIED:Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/dtv/common/CaManager;->mEventHandler:Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;->sendMessage(Landroid/os/Message;)Z

    :cond_1
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "\n Native CA callback, PostEvent_DetitleReceived"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static PostEvent_EmailNotifyIcon(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/dtv/common/CaManager;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, v1, Lcom/mstar/android/tvapi/dtv/common/CaManager;->mEventHandler:Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;

    if-eqz v2, :cond_1

    sput p1, Lcom/mstar/android/tvapi/dtv/common/CaManager;->_current_email_type:I

    iget-object v2, v1, Lcom/mstar/android/tvapi/dtv/common/CaManager;->mEventHandler:Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;->EV_CA_EMAIL_NOTIFY_ICON:Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/dtv/common/CaManager;->mEventHandler:Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;->sendMessage(Landroid/os/Message;)Z

    :cond_1
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "\n Native CA callback, PostEvent_EmailNotifyIcon"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static PostEvent_EntitleChanged(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/dtv/common/CaManager;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, v1, Lcom/mstar/android/tvapi/dtv/common/CaManager;->mEventHandler:Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;

    if-eqz v2, :cond_1

    iget-object v2, v1, Lcom/mstar/android/tvapi/dtv/common/CaManager;->mEventHandler:Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;->EV_CA_ENTITLE_CHANGED:Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/dtv/common/CaManager;->mEventHandler:Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;->sendMessage(Landroid/os/Message;)Z

    :cond_1
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "\n Native CA callback, PostEvent_EntitleChanged"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static PostEvent_HideIPPVDlg(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/dtv/common/CaManager;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, v1, Lcom/mstar/android/tvapi/dtv/common/CaManager;->mEventHandler:Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;

    if-eqz v2, :cond_1

    iget-object v2, v1, Lcom/mstar/android/tvapi/dtv/common/CaManager;->mEventHandler:Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;->EV_CA_HIDE_IPPV_DLG:Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/dtv/common/CaManager;->mEventHandler:Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;->sendMessage(Landroid/os/Message;)Z

    :cond_1
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "\n Native CA callback, PostEvent_HideIPPVDlg"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static PostEvent_HideOSDMessage(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/dtv/common/CaManager;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, v1, Lcom/mstar/android/tvapi/dtv/common/CaManager;->mEventHandler:Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;

    if-eqz v2, :cond_1

    iget-object v2, v1, Lcom/mstar/android/tvapi/dtv/common/CaManager;->mEventHandler:Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;->EV_CA_HIDE_OSD_MESSAGE:Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/dtv/common/CaManager;->mEventHandler:Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;->sendMessage(Landroid/os/Message;)Z

    :cond_1
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "\n Native CA callback, PostEvent_HideOSDMessage"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static PostEvent_LockService(Ljava/lang/Object;ILcom/mstar/android/tvapi/dtv/vo/CaLockService;)V
    .locals 5
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # Lcom/mstar/android/tvapi/dtv/vo/CaLockService;

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/dtv/common/CaManager;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, v1, Lcom/mstar/android/tvapi/dtv/common/CaManager;->mEventHandler:Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;

    if-eqz v2, :cond_1

    iget-object v2, v1, Lcom/mstar/android/tvapi/dtv/common/CaManager;->mEventHandler:Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;->EV_CA_LOCKSERVICE:Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;->ordinal()I

    move-result v3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, p1, v4, p2}, Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/dtv/common/CaManager;->mEventHandler:Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;->sendMessage(Landroid/os/Message;)Z

    :cond_1
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "\n Native CA callback, PostEvent_LockService"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static PostEvent_OtaState(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/dtv/common/CaManager;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, v1, Lcom/mstar/android/tvapi/dtv/common/CaManager;->mEventHandler:Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;

    if-eqz v2, :cond_1

    iget-object v2, v1, Lcom/mstar/android/tvapi/dtv/common/CaManager;->mEventHandler:Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;->EV_CA_OTASTATE:Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/dtv/common/CaManager;->mEventHandler:Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;->sendMessage(Landroid/os/Message;)Z

    :cond_1
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "\n Native CA callback, PostEvent_OtaState"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static PostEvent_RequestFeeding(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/dtv/common/CaManager;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, v1, Lcom/mstar/android/tvapi/dtv/common/CaManager;->mEventHandler:Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;

    if-eqz v2, :cond_1

    iget-object v2, v1, Lcom/mstar/android/tvapi/dtv/common/CaManager;->mEventHandler:Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;->EV_CA_REQUEST_FEEDING:Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/dtv/common/CaManager;->mEventHandler:Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;->sendMessage(Landroid/os/Message;)Z

    :cond_1
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "\n Native CA callback, PostEvent_RequestFeeding"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static PostEvent_ShowBuyMessage(Ljava/lang/Object;II)V
    .locals 5
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/dtv/common/CaManager;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, v1, Lcom/mstar/android/tvapi/dtv/common/CaManager;->mEventHandler:Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;

    if-eqz v2, :cond_1

    const-string v2, "CaManager"

    const-string v3, "//////////////////////////////////EV_CA_SHOW_BUY_MESSAGE/////////////////////////////////////////////////"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v2, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;->EV_CA_SHOW_BUY_MESSAGE:Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;->ordinal()I

    move-result v2

    sput v2, Lcom/mstar/android/tvapi/dtv/common/CaManager;->_current_event:I

    sput p2, Lcom/mstar/android/tvapi/dtv/common/CaManager;->_current_msg_type:I

    iget-object v2, v1, Lcom/mstar/android/tvapi/dtv/common/CaManager;->mEventHandler:Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;->EV_CA_SHOW_BUY_MESSAGE:Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/dtv/common/CaManager;->mEventHandler:Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;->sendMessage(Landroid/os/Message;)Z

    :cond_1
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "\n Native CA callback, PostEvent_ShowBuyMessage :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static PostEvent_ShowFingerMessage(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/dtv/common/CaManager;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, v1, Lcom/mstar/android/tvapi/dtv/common/CaManager;->mEventHandler:Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;

    if-eqz v2, :cond_1

    iget-object v2, v1, Lcom/mstar/android/tvapi/dtv/common/CaManager;->mEventHandler:Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;->EV_CA_SHOW_FINGER_MESSAGE:Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/dtv/common/CaManager;->mEventHandler:Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;->sendMessage(Landroid/os/Message;)Z

    :cond_1
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "\n Native CA callback, PostEvent_ShowFingerMessage"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static PostEvent_ShowOSDMessage(Ljava/lang/Object;ILjava/lang/String;)V
    .locals 5
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # Ljava/lang/String;

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/dtv/common/CaManager;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, v1, Lcom/mstar/android/tvapi/dtv/common/CaManager;->mEventHandler:Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;

    if-eqz v2, :cond_1

    iget-object v2, v1, Lcom/mstar/android/tvapi/dtv/common/CaManager;->mEventHandler:Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;->EV_CA_SHOW_OSD_MESSAGE:Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;->ordinal()I

    move-result v3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, p1, v4, p2}, Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/dtv/common/CaManager;->mEventHandler:Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;->sendMessage(Landroid/os/Message;)Z

    :cond_1
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "\n Native CA callback, PostEvent_ShowOSDMessage: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static PostEvent_ShowProgressStrip(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/dtv/common/CaManager;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, v1, Lcom/mstar/android/tvapi/dtv/common/CaManager;->mEventHandler:Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;

    if-eqz v2, :cond_1

    iget-object v2, v1, Lcom/mstar/android/tvapi/dtv/common/CaManager;->mEventHandler:Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;->EV_CA_SHOW_PROGRESS_STRIP:Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/dtv/common/CaManager;->mEventHandler:Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;->sendMessage(Landroid/os/Message;)Z

    :cond_1
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "\n Native CA callback, PostEvent_ShowProgressStrip"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static PostEvent_SnServiceDeadth(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    :try_start_0
    const-class v2, Lcom/mstar/android/tvapi/dtv/common/CaManager;

    monitor-enter v2
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    const-string v1, "mstar.str.suspending"

    const-string v3, "1"

    invoke-static {v1, v3}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x0

    sput-object v1, Lcom/mstar/android/tvapi/dtv/common/CaManager;->_camanager:Lcom/mstar/android/tvapi/dtv/common/CaManager;

    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "PostEvent_SnServiceDeadth: set SystemProperties \'mstar.str.suspending\' =1"

    invoke-virtual {v1, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    monitor-exit v2

    :goto_0
    return-void

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v1
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method private static PostEvent_StartIppvBuyDlg(Ljava/lang/Object;ILcom/mstar/android/tvapi/dtv/vo/CaStartIPPVBuyDlgInfo;)V
    .locals 5
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # Lcom/mstar/android/tvapi/dtv/vo/CaStartIPPVBuyDlgInfo;

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/dtv/common/CaManager;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, v1, Lcom/mstar/android/tvapi/dtv/common/CaManager;->mEventHandler:Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;

    if-eqz v2, :cond_1

    iget-object v2, v1, Lcom/mstar/android/tvapi/dtv/common/CaManager;->mEventHandler:Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;->EV_CA_START_IPPV_BUY_DLG:Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;->ordinal()I

    move-result v3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, p1, v4, p2}, Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/dtv/common/CaManager;->mEventHandler:Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;->sendMessage(Landroid/os/Message;)Z

    :cond_1
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "\n Native CA callback, PostEvent_StartIppvBuyDlg"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static PostEvent_UNLockService(Ljava/lang/Object;II)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/dtv/common/CaManager;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, v1, Lcom/mstar/android/tvapi/dtv/common/CaManager;->mEventHandler:Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;

    if-eqz v2, :cond_1

    iget-object v2, v1, Lcom/mstar/android/tvapi/dtv/common/CaManager;->mEventHandler:Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;

    sget-object v3, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;->EV_CA_UNLOCKSERVICE:Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3, p1, p2}, Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/dtv/common/CaManager;->mEventHandler:Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;->sendMessage(Landroid/os/Message;)Z

    :cond_1
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "\n Native CA callback, PostEvent_UNLockService"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/mstar/android/tvapi/dtv/common/CaManager;)I
    .locals 1
    .param p0    # Lcom/mstar/android/tvapi/dtv/common/CaManager;

    iget v0, p0, Lcom/mstar/android/tvapi/dtv/common/CaManager;->mNativeContext:I

    return v0
.end method

.method static synthetic access$100(Lcom/mstar/android/tvapi/dtv/common/CaManager;)Lcom/mstar/android/tvapi/dtv/common/CaManager$OnCaEventListener;
    .locals 1
    .param p0    # Lcom/mstar/android/tvapi/dtv/common/CaManager;

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/common/CaManager;->mOnEventListener:Lcom/mstar/android/tvapi/dtv/common/CaManager$OnCaEventListener;

    return-object v0
.end method

.method public static getCurrentEvent()I
    .locals 1

    sget v0, Lcom/mstar/android/tvapi/dtv/common/CaManager;->_current_event:I

    return v0
.end method

.method public static getCurrentMsgType()I
    .locals 1

    sget v0, Lcom/mstar/android/tvapi/dtv/common/CaManager;->_current_msg_type:I

    return v0
.end method

.method public static getInstance()Lcom/mstar/android/tvapi/dtv/common/CaManager;
    .locals 2

    sget-object v0, Lcom/mstar/android/tvapi/dtv/common/CaManager;->_camanager:Lcom/mstar/android/tvapi/dtv/common/CaManager;

    if-nez v0, :cond_1

    const-class v1, Lcom/mstar/android/tvapi/dtv/common/CaManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/mstar/android/tvapi/dtv/common/CaManager;->_camanager:Lcom/mstar/android/tvapi/dtv/common/CaManager;

    if-nez v0, :cond_0

    new-instance v0, Lcom/mstar/android/tvapi/dtv/common/CaManager;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/dtv/common/CaManager;-><init>()V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/common/CaManager;->_camanager:Lcom/mstar/android/tvapi/dtv/common/CaManager;

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    sget-object v0, Lcom/mstar/android/tvapi/dtv/common/CaManager;->_camanager:Lcom/mstar/android/tvapi/dtv/common/CaManager;

    return-object v0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private final native native_finalize()V
.end method

.method public static final native native_init()V
.end method

.method private final native native_setup(Ljava/lang/Object;)V
.end method

.method private static postEventFromNative(Ljava/lang/Object;IIILjava/lang/Object;)V
    .locals 4
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # Ljava/lang/Object;

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/dtv/common/CaManager;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, v1, Lcom/mstar/android/tvapi/dtv/common/CaManager;->mEventHandler:Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;

    if-eqz v2, :cond_1

    iget-object v2, v1, Lcom/mstar/android/tvapi/dtv/common/CaManager;->mEventHandler:Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;

    invoke-virtual {v2, p1, p2, p3, p4}, Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/mstar/android/tvapi/dtv/common/CaManager;->mEventHandler:Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;->sendMessage(Landroid/os/Message;)Z

    :cond_1
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "\n NativeCA callback, postEventFromNative"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static setCurrentEvent(I)V
    .locals 0
    .param p0    # I

    sput p0, Lcom/mstar/android/tvapi/dtv/common/CaManager;->_current_event:I

    return-void
.end method

.method public static setCurrentMsgType(I)V
    .locals 0
    .param p0    # I

    sput p0, Lcom/mstar/android/tvapi/dtv/common/CaManager;->_current_msg_type:I

    return-void
.end method


# virtual methods
.method protected finalize()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    invoke-direct {p0}, Lcom/mstar/android/tvapi/dtv/common/CaManager;->native_finalize()V

    return-void
.end method

.method public setOnCaEventListener(Lcom/mstar/android/tvapi/dtv/common/CaManager$OnCaEventListener;)V
    .locals 0
    .param p1    # Lcom/mstar/android/tvapi/dtv/common/CaManager$OnCaEventListener;

    iput-object p1, p0, Lcom/mstar/android/tvapi/dtv/common/CaManager;->mOnEventListener:Lcom/mstar/android/tvapi/dtv/common/CaManager$OnCaEventListener;

    return-void
.end method
