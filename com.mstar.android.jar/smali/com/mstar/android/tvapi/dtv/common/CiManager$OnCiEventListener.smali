.class public interface abstract Lcom/mstar/android/tvapi/dtv/common/CiManager$OnCiEventListener;
.super Ljava/lang/Object;
.source "CiManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mstar/android/tvapi/dtv/common/CiManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnCiEventListener"
.end annotation


# virtual methods
.method public abstract onUiAutotestMessageShown(Lcom/mstar/android/tvapi/dtv/common/CiManager;I)Z
.end method

.method public abstract onUiCardInserted(Lcom/mstar/android/tvapi/dtv/common/CiManager;I)Z
.end method

.method public abstract onUiCardRemoved(Lcom/mstar/android/tvapi/dtv/common/CiManager;I)Z
.end method

.method public abstract onUiCloseMmi(Lcom/mstar/android/tvapi/dtv/common/CiManager;I)Z
.end method

.method public abstract onUiDataReady(Lcom/mstar/android/tvapi/dtv/common/CiManager;I)Z
.end method
