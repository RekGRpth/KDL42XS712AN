.class Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;
.super Landroid/os/Handler;
.source "CaManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mstar/android/tvapi/dtv/common/CaManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EventHandler"
.end annotation


# instance fields
.field private mMSrv:Lcom/mstar/android/tvapi/dtv/common/CaManager;

.field final synthetic this$0:Lcom/mstar/android/tvapi/dtv/common/CaManager;


# direct methods
.method public constructor <init>(Lcom/mstar/android/tvapi/dtv/common/CaManager;Lcom/mstar/android/tvapi/dtv/common/CaManager;Landroid/os/Looper;)V
    .locals 0
    .param p2    # Lcom/mstar/android/tvapi/dtv/common/CaManager;
    .param p3    # Landroid/os/Looper;

    iput-object p1, p0, Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;->this$0:Lcom/mstar/android/tvapi/dtv/common/CaManager;

    invoke-direct {p0, p3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object p2, p0, Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;->mMSrv:Lcom/mstar/android/tvapi/dtv/common/CaManager;

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7
    .param p1    # Landroid/os/Message;

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;->mMSrv:Lcom/mstar/android/tvapi/dtv/common/CaManager;

    # getter for: Lcom/mstar/android/tvapi/dtv/common/CaManager;->mNativeContext:I
    invoke-static {v0}, Lcom/mstar/android/tvapi/dtv/common/CaManager;->access$000(Lcom/mstar/android/tvapi/dtv/common/CaManager;)I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;->values()[Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;

    move-result-object v6

    iget v0, p1, Landroid/os/Message;->what:I

    sget-object v1, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;->EV_MAX:Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;->ordinal()I

    move-result v1

    if-gt v0, v1, :cond_2

    iget v0, p1, Landroid/os/Message;->what:I

    sget-object v1, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;->EV_CA_START_IPPV_BUY_DLG:Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;->ordinal()I

    move-result v1

    if-ge v0, v1, :cond_3

    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Native post event out of bound:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    sget-object v0, Lcom/mstar/android/tvapi/dtv/common/CaManager$1;->$SwitchMap$com$mstar$android$tvapi$dtv$common$CaManager$CA_EVENT:[I

    iget v1, p1, Landroid/os/Message;->what:I

    aget-object v1, v6, v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/dtv/common/CaManager$CA_EVENT;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    sget-object v0, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown message type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_0
    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;->this$0:Lcom/mstar/android/tvapi/dtv/common/CaManager;

    # getter for: Lcom/mstar/android/tvapi/dtv/common/CaManager;->mOnEventListener:Lcom/mstar/android/tvapi/dtv/common/CaManager$OnCaEventListener;
    invoke-static {v0}, Lcom/mstar/android/tvapi/dtv/common/CaManager;->access$100(Lcom/mstar/android/tvapi/dtv/common/CaManager;)Lcom/mstar/android/tvapi/dtv/common/CaManager$OnCaEventListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;->this$0:Lcom/mstar/android/tvapi/dtv/common/CaManager;

    # getter for: Lcom/mstar/android/tvapi/dtv/common/CaManager;->mOnEventListener:Lcom/mstar/android/tvapi/dtv/common/CaManager$OnCaEventListener;
    invoke-static {v0}, Lcom/mstar/android/tvapi/dtv/common/CaManager;->access$100(Lcom/mstar/android/tvapi/dtv/common/CaManager;)Lcom/mstar/android/tvapi/dtv/common/CaManager$OnCaEventListener;

    move-result-object v0

    iget-object v1, p0, Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;->mMSrv:Lcom/mstar/android/tvapi/dtv/common/CaManager;

    iget v2, p1, Landroid/os/Message;->what:I

    iget v3, p1, Landroid/os/Message;->arg1:I

    iget v4, p1, Landroid/os/Message;->arg2:I

    iget-object v5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v5, Lcom/mstar/android/tvapi/dtv/vo/CaStartIPPVBuyDlgInfo;

    invoke-interface/range {v0 .. v5}, Lcom/mstar/android/tvapi/dtv/common/CaManager$OnCaEventListener;->onStartIppvBuyDlg(Lcom/mstar/android/tvapi/dtv/common/CaManager;IIILcom/mstar/android/tvapi/dtv/vo/CaStartIPPVBuyDlgInfo;)Z

    goto/16 :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;->this$0:Lcom/mstar/android/tvapi/dtv/common/CaManager;

    # getter for: Lcom/mstar/android/tvapi/dtv/common/CaManager;->mOnEventListener:Lcom/mstar/android/tvapi/dtv/common/CaManager$OnCaEventListener;
    invoke-static {v0}, Lcom/mstar/android/tvapi/dtv/common/CaManager;->access$100(Lcom/mstar/android/tvapi/dtv/common/CaManager;)Lcom/mstar/android/tvapi/dtv/common/CaManager$OnCaEventListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;->this$0:Lcom/mstar/android/tvapi/dtv/common/CaManager;

    # getter for: Lcom/mstar/android/tvapi/dtv/common/CaManager;->mOnEventListener:Lcom/mstar/android/tvapi/dtv/common/CaManager$OnCaEventListener;
    invoke-static {v0}, Lcom/mstar/android/tvapi/dtv/common/CaManager;->access$100(Lcom/mstar/android/tvapi/dtv/common/CaManager;)Lcom/mstar/android/tvapi/dtv/common/CaManager$OnCaEventListener;

    move-result-object v0

    iget-object v1, p0, Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;->mMSrv:Lcom/mstar/android/tvapi/dtv/common/CaManager;

    iget v2, p1, Landroid/os/Message;->what:I

    iget v3, p1, Landroid/os/Message;->arg1:I

    iget v4, p1, Landroid/os/Message;->arg2:I

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/mstar/android/tvapi/dtv/common/CaManager$OnCaEventListener;->onHideIPPVDlg(Lcom/mstar/android/tvapi/dtv/common/CaManager;III)Z

    goto/16 :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;->this$0:Lcom/mstar/android/tvapi/dtv/common/CaManager;

    # getter for: Lcom/mstar/android/tvapi/dtv/common/CaManager;->mOnEventListener:Lcom/mstar/android/tvapi/dtv/common/CaManager$OnCaEventListener;
    invoke-static {v0}, Lcom/mstar/android/tvapi/dtv/common/CaManager;->access$100(Lcom/mstar/android/tvapi/dtv/common/CaManager;)Lcom/mstar/android/tvapi/dtv/common/CaManager$OnCaEventListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;->this$0:Lcom/mstar/android/tvapi/dtv/common/CaManager;

    # getter for: Lcom/mstar/android/tvapi/dtv/common/CaManager;->mOnEventListener:Lcom/mstar/android/tvapi/dtv/common/CaManager$OnCaEventListener;
    invoke-static {v0}, Lcom/mstar/android/tvapi/dtv/common/CaManager;->access$100(Lcom/mstar/android/tvapi/dtv/common/CaManager;)Lcom/mstar/android/tvapi/dtv/common/CaManager$OnCaEventListener;

    move-result-object v0

    iget-object v1, p0, Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;->mMSrv:Lcom/mstar/android/tvapi/dtv/common/CaManager;

    iget v2, p1, Landroid/os/Message;->what:I

    iget v3, p1, Landroid/os/Message;->arg1:I

    iget v4, p1, Landroid/os/Message;->arg2:I

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/mstar/android/tvapi/dtv/common/CaManager$OnCaEventListener;->onEmailNotifyIcon(Lcom/mstar/android/tvapi/dtv/common/CaManager;III)Z

    goto/16 :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;->this$0:Lcom/mstar/android/tvapi/dtv/common/CaManager;

    # getter for: Lcom/mstar/android/tvapi/dtv/common/CaManager;->mOnEventListener:Lcom/mstar/android/tvapi/dtv/common/CaManager$OnCaEventListener;
    invoke-static {v0}, Lcom/mstar/android/tvapi/dtv/common/CaManager;->access$100(Lcom/mstar/android/tvapi/dtv/common/CaManager;)Lcom/mstar/android/tvapi/dtv/common/CaManager$OnCaEventListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;->this$0:Lcom/mstar/android/tvapi/dtv/common/CaManager;

    # getter for: Lcom/mstar/android/tvapi/dtv/common/CaManager;->mOnEventListener:Lcom/mstar/android/tvapi/dtv/common/CaManager$OnCaEventListener;
    invoke-static {v0}, Lcom/mstar/android/tvapi/dtv/common/CaManager;->access$100(Lcom/mstar/android/tvapi/dtv/common/CaManager;)Lcom/mstar/android/tvapi/dtv/common/CaManager$OnCaEventListener;

    move-result-object v0

    iget-object v1, p0, Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;->mMSrv:Lcom/mstar/android/tvapi/dtv/common/CaManager;

    iget v2, p1, Landroid/os/Message;->what:I

    iget v3, p1, Landroid/os/Message;->arg1:I

    iget v4, p1, Landroid/os/Message;->arg2:I

    iget-object v5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v5, Ljava/lang/String;

    invoke-interface/range {v0 .. v5}, Lcom/mstar/android/tvapi/dtv/common/CaManager$OnCaEventListener;->onShowOSDMessage(Lcom/mstar/android/tvapi/dtv/common/CaManager;IIILjava/lang/String;)Z

    goto/16 :goto_0

    :pswitch_4
    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;->this$0:Lcom/mstar/android/tvapi/dtv/common/CaManager;

    # getter for: Lcom/mstar/android/tvapi/dtv/common/CaManager;->mOnEventListener:Lcom/mstar/android/tvapi/dtv/common/CaManager$OnCaEventListener;
    invoke-static {v0}, Lcom/mstar/android/tvapi/dtv/common/CaManager;->access$100(Lcom/mstar/android/tvapi/dtv/common/CaManager;)Lcom/mstar/android/tvapi/dtv/common/CaManager$OnCaEventListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;->this$0:Lcom/mstar/android/tvapi/dtv/common/CaManager;

    # getter for: Lcom/mstar/android/tvapi/dtv/common/CaManager;->mOnEventListener:Lcom/mstar/android/tvapi/dtv/common/CaManager$OnCaEventListener;
    invoke-static {v0}, Lcom/mstar/android/tvapi/dtv/common/CaManager;->access$100(Lcom/mstar/android/tvapi/dtv/common/CaManager;)Lcom/mstar/android/tvapi/dtv/common/CaManager$OnCaEventListener;

    move-result-object v0

    iget-object v1, p0, Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;->mMSrv:Lcom/mstar/android/tvapi/dtv/common/CaManager;

    iget v2, p1, Landroid/os/Message;->what:I

    iget v3, p1, Landroid/os/Message;->arg1:I

    iget v4, p1, Landroid/os/Message;->arg2:I

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/mstar/android/tvapi/dtv/common/CaManager$OnCaEventListener;->onHideOSDMessage(Lcom/mstar/android/tvapi/dtv/common/CaManager;III)Z

    goto/16 :goto_0

    :pswitch_5
    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;->this$0:Lcom/mstar/android/tvapi/dtv/common/CaManager;

    # getter for: Lcom/mstar/android/tvapi/dtv/common/CaManager;->mOnEventListener:Lcom/mstar/android/tvapi/dtv/common/CaManager$OnCaEventListener;
    invoke-static {v0}, Lcom/mstar/android/tvapi/dtv/common/CaManager;->access$100(Lcom/mstar/android/tvapi/dtv/common/CaManager;)Lcom/mstar/android/tvapi/dtv/common/CaManager$OnCaEventListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;->this$0:Lcom/mstar/android/tvapi/dtv/common/CaManager;

    # getter for: Lcom/mstar/android/tvapi/dtv/common/CaManager;->mOnEventListener:Lcom/mstar/android/tvapi/dtv/common/CaManager$OnCaEventListener;
    invoke-static {v0}, Lcom/mstar/android/tvapi/dtv/common/CaManager;->access$100(Lcom/mstar/android/tvapi/dtv/common/CaManager;)Lcom/mstar/android/tvapi/dtv/common/CaManager$OnCaEventListener;

    move-result-object v0

    iget-object v1, p0, Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;->mMSrv:Lcom/mstar/android/tvapi/dtv/common/CaManager;

    iget v2, p1, Landroid/os/Message;->what:I

    iget v3, p1, Landroid/os/Message;->arg1:I

    iget v4, p1, Landroid/os/Message;->arg2:I

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/mstar/android/tvapi/dtv/common/CaManager$OnCaEventListener;->onRequestFeeding(Lcom/mstar/android/tvapi/dtv/common/CaManager;III)Z

    goto/16 :goto_0

    :pswitch_6
    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;->this$0:Lcom/mstar/android/tvapi/dtv/common/CaManager;

    # getter for: Lcom/mstar/android/tvapi/dtv/common/CaManager;->mOnEventListener:Lcom/mstar/android/tvapi/dtv/common/CaManager$OnCaEventListener;
    invoke-static {v0}, Lcom/mstar/android/tvapi/dtv/common/CaManager;->access$100(Lcom/mstar/android/tvapi/dtv/common/CaManager;)Lcom/mstar/android/tvapi/dtv/common/CaManager$OnCaEventListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;->this$0:Lcom/mstar/android/tvapi/dtv/common/CaManager;

    # getter for: Lcom/mstar/android/tvapi/dtv/common/CaManager;->mOnEventListener:Lcom/mstar/android/tvapi/dtv/common/CaManager$OnCaEventListener;
    invoke-static {v0}, Lcom/mstar/android/tvapi/dtv/common/CaManager;->access$100(Lcom/mstar/android/tvapi/dtv/common/CaManager;)Lcom/mstar/android/tvapi/dtv/common/CaManager$OnCaEventListener;

    move-result-object v0

    iget-object v1, p0, Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;->mMSrv:Lcom/mstar/android/tvapi/dtv/common/CaManager;

    iget v2, p1, Landroid/os/Message;->what:I

    iget v3, p1, Landroid/os/Message;->arg1:I

    iget v4, p1, Landroid/os/Message;->arg2:I

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/mstar/android/tvapi/dtv/common/CaManager$OnCaEventListener;->onShowBuyMessage(Lcom/mstar/android/tvapi/dtv/common/CaManager;III)Z

    goto/16 :goto_0

    :pswitch_7
    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;->this$0:Lcom/mstar/android/tvapi/dtv/common/CaManager;

    # getter for: Lcom/mstar/android/tvapi/dtv/common/CaManager;->mOnEventListener:Lcom/mstar/android/tvapi/dtv/common/CaManager$OnCaEventListener;
    invoke-static {v0}, Lcom/mstar/android/tvapi/dtv/common/CaManager;->access$100(Lcom/mstar/android/tvapi/dtv/common/CaManager;)Lcom/mstar/android/tvapi/dtv/common/CaManager$OnCaEventListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;->this$0:Lcom/mstar/android/tvapi/dtv/common/CaManager;

    # getter for: Lcom/mstar/android/tvapi/dtv/common/CaManager;->mOnEventListener:Lcom/mstar/android/tvapi/dtv/common/CaManager$OnCaEventListener;
    invoke-static {v0}, Lcom/mstar/android/tvapi/dtv/common/CaManager;->access$100(Lcom/mstar/android/tvapi/dtv/common/CaManager;)Lcom/mstar/android/tvapi/dtv/common/CaManager$OnCaEventListener;

    move-result-object v0

    iget-object v1, p0, Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;->mMSrv:Lcom/mstar/android/tvapi/dtv/common/CaManager;

    iget v2, p1, Landroid/os/Message;->what:I

    iget v3, p1, Landroid/os/Message;->arg1:I

    iget v4, p1, Landroid/os/Message;->arg2:I

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/mstar/android/tvapi/dtv/common/CaManager$OnCaEventListener;->onShowFingerMessage(Lcom/mstar/android/tvapi/dtv/common/CaManager;III)Z

    goto/16 :goto_0

    :pswitch_8
    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;->this$0:Lcom/mstar/android/tvapi/dtv/common/CaManager;

    # getter for: Lcom/mstar/android/tvapi/dtv/common/CaManager;->mOnEventListener:Lcom/mstar/android/tvapi/dtv/common/CaManager$OnCaEventListener;
    invoke-static {v0}, Lcom/mstar/android/tvapi/dtv/common/CaManager;->access$100(Lcom/mstar/android/tvapi/dtv/common/CaManager;)Lcom/mstar/android/tvapi/dtv/common/CaManager$OnCaEventListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;->this$0:Lcom/mstar/android/tvapi/dtv/common/CaManager;

    # getter for: Lcom/mstar/android/tvapi/dtv/common/CaManager;->mOnEventListener:Lcom/mstar/android/tvapi/dtv/common/CaManager$OnCaEventListener;
    invoke-static {v0}, Lcom/mstar/android/tvapi/dtv/common/CaManager;->access$100(Lcom/mstar/android/tvapi/dtv/common/CaManager;)Lcom/mstar/android/tvapi/dtv/common/CaManager$OnCaEventListener;

    move-result-object v0

    iget-object v1, p0, Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;->mMSrv:Lcom/mstar/android/tvapi/dtv/common/CaManager;

    iget v2, p1, Landroid/os/Message;->what:I

    iget v3, p1, Landroid/os/Message;->arg1:I

    iget v4, p1, Landroid/os/Message;->arg2:I

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/mstar/android/tvapi/dtv/common/CaManager$OnCaEventListener;->onShowProgressStrip(Lcom/mstar/android/tvapi/dtv/common/CaManager;III)Z

    goto/16 :goto_0

    :pswitch_9
    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;->this$0:Lcom/mstar/android/tvapi/dtv/common/CaManager;

    # getter for: Lcom/mstar/android/tvapi/dtv/common/CaManager;->mOnEventListener:Lcom/mstar/android/tvapi/dtv/common/CaManager$OnCaEventListener;
    invoke-static {v0}, Lcom/mstar/android/tvapi/dtv/common/CaManager;->access$100(Lcom/mstar/android/tvapi/dtv/common/CaManager;)Lcom/mstar/android/tvapi/dtv/common/CaManager$OnCaEventListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;->this$0:Lcom/mstar/android/tvapi/dtv/common/CaManager;

    # getter for: Lcom/mstar/android/tvapi/dtv/common/CaManager;->mOnEventListener:Lcom/mstar/android/tvapi/dtv/common/CaManager$OnCaEventListener;
    invoke-static {v0}, Lcom/mstar/android/tvapi/dtv/common/CaManager;->access$100(Lcom/mstar/android/tvapi/dtv/common/CaManager;)Lcom/mstar/android/tvapi/dtv/common/CaManager$OnCaEventListener;

    move-result-object v0

    iget-object v1, p0, Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;->mMSrv:Lcom/mstar/android/tvapi/dtv/common/CaManager;

    iget v2, p1, Landroid/os/Message;->what:I

    iget v3, p1, Landroid/os/Message;->arg1:I

    iget v4, p1, Landroid/os/Message;->arg2:I

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/mstar/android/tvapi/dtv/common/CaManager$OnCaEventListener;->onActionRequest(Lcom/mstar/android/tvapi/dtv/common/CaManager;III)Z

    goto/16 :goto_0

    :pswitch_a
    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;->this$0:Lcom/mstar/android/tvapi/dtv/common/CaManager;

    # getter for: Lcom/mstar/android/tvapi/dtv/common/CaManager;->mOnEventListener:Lcom/mstar/android/tvapi/dtv/common/CaManager$OnCaEventListener;
    invoke-static {v0}, Lcom/mstar/android/tvapi/dtv/common/CaManager;->access$100(Lcom/mstar/android/tvapi/dtv/common/CaManager;)Lcom/mstar/android/tvapi/dtv/common/CaManager$OnCaEventListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;->this$0:Lcom/mstar/android/tvapi/dtv/common/CaManager;

    # getter for: Lcom/mstar/android/tvapi/dtv/common/CaManager;->mOnEventListener:Lcom/mstar/android/tvapi/dtv/common/CaManager$OnCaEventListener;
    invoke-static {v0}, Lcom/mstar/android/tvapi/dtv/common/CaManager;->access$100(Lcom/mstar/android/tvapi/dtv/common/CaManager;)Lcom/mstar/android/tvapi/dtv/common/CaManager$OnCaEventListener;

    move-result-object v0

    iget-object v1, p0, Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;->mMSrv:Lcom/mstar/android/tvapi/dtv/common/CaManager;

    iget v2, p1, Landroid/os/Message;->what:I

    iget v3, p1, Landroid/os/Message;->arg1:I

    iget v4, p1, Landroid/os/Message;->arg2:I

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/mstar/android/tvapi/dtv/common/CaManager$OnCaEventListener;->onEntitleChanged(Lcom/mstar/android/tvapi/dtv/common/CaManager;III)Z

    goto/16 :goto_0

    :pswitch_b
    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;->this$0:Lcom/mstar/android/tvapi/dtv/common/CaManager;

    # getter for: Lcom/mstar/android/tvapi/dtv/common/CaManager;->mOnEventListener:Lcom/mstar/android/tvapi/dtv/common/CaManager$OnCaEventListener;
    invoke-static {v0}, Lcom/mstar/android/tvapi/dtv/common/CaManager;->access$100(Lcom/mstar/android/tvapi/dtv/common/CaManager;)Lcom/mstar/android/tvapi/dtv/common/CaManager$OnCaEventListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;->this$0:Lcom/mstar/android/tvapi/dtv/common/CaManager;

    # getter for: Lcom/mstar/android/tvapi/dtv/common/CaManager;->mOnEventListener:Lcom/mstar/android/tvapi/dtv/common/CaManager$OnCaEventListener;
    invoke-static {v0}, Lcom/mstar/android/tvapi/dtv/common/CaManager;->access$100(Lcom/mstar/android/tvapi/dtv/common/CaManager;)Lcom/mstar/android/tvapi/dtv/common/CaManager$OnCaEventListener;

    move-result-object v0

    iget-object v1, p0, Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;->mMSrv:Lcom/mstar/android/tvapi/dtv/common/CaManager;

    iget v2, p1, Landroid/os/Message;->what:I

    iget v3, p1, Landroid/os/Message;->arg1:I

    iget v4, p1, Landroid/os/Message;->arg2:I

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/mstar/android/tvapi/dtv/common/CaManager$OnCaEventListener;->onDetitleReceived(Lcom/mstar/android/tvapi/dtv/common/CaManager;III)Z

    goto/16 :goto_0

    :pswitch_c
    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;->this$0:Lcom/mstar/android/tvapi/dtv/common/CaManager;

    # getter for: Lcom/mstar/android/tvapi/dtv/common/CaManager;->mOnEventListener:Lcom/mstar/android/tvapi/dtv/common/CaManager$OnCaEventListener;
    invoke-static {v0}, Lcom/mstar/android/tvapi/dtv/common/CaManager;->access$100(Lcom/mstar/android/tvapi/dtv/common/CaManager;)Lcom/mstar/android/tvapi/dtv/common/CaManager$OnCaEventListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;->this$0:Lcom/mstar/android/tvapi/dtv/common/CaManager;

    # getter for: Lcom/mstar/android/tvapi/dtv/common/CaManager;->mOnEventListener:Lcom/mstar/android/tvapi/dtv/common/CaManager$OnCaEventListener;
    invoke-static {v0}, Lcom/mstar/android/tvapi/dtv/common/CaManager;->access$100(Lcom/mstar/android/tvapi/dtv/common/CaManager;)Lcom/mstar/android/tvapi/dtv/common/CaManager$OnCaEventListener;

    move-result-object v0

    iget-object v1, p0, Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;->mMSrv:Lcom/mstar/android/tvapi/dtv/common/CaManager;

    iget v2, p1, Landroid/os/Message;->what:I

    iget v3, p1, Landroid/os/Message;->arg1:I

    iget v4, p1, Landroid/os/Message;->arg2:I

    iget-object v5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v5, Lcom/mstar/android/tvapi/dtv/vo/CaLockService;

    invoke-interface/range {v0 .. v5}, Lcom/mstar/android/tvapi/dtv/common/CaManager$OnCaEventListener;->onLockService(Lcom/mstar/android/tvapi/dtv/common/CaManager;IIILcom/mstar/android/tvapi/dtv/vo/CaLockService;)Z

    goto/16 :goto_0

    :pswitch_d
    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;->this$0:Lcom/mstar/android/tvapi/dtv/common/CaManager;

    # getter for: Lcom/mstar/android/tvapi/dtv/common/CaManager;->mOnEventListener:Lcom/mstar/android/tvapi/dtv/common/CaManager$OnCaEventListener;
    invoke-static {v0}, Lcom/mstar/android/tvapi/dtv/common/CaManager;->access$100(Lcom/mstar/android/tvapi/dtv/common/CaManager;)Lcom/mstar/android/tvapi/dtv/common/CaManager$OnCaEventListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;->this$0:Lcom/mstar/android/tvapi/dtv/common/CaManager;

    # getter for: Lcom/mstar/android/tvapi/dtv/common/CaManager;->mOnEventListener:Lcom/mstar/android/tvapi/dtv/common/CaManager$OnCaEventListener;
    invoke-static {v0}, Lcom/mstar/android/tvapi/dtv/common/CaManager;->access$100(Lcom/mstar/android/tvapi/dtv/common/CaManager;)Lcom/mstar/android/tvapi/dtv/common/CaManager$OnCaEventListener;

    move-result-object v0

    iget-object v1, p0, Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;->mMSrv:Lcom/mstar/android/tvapi/dtv/common/CaManager;

    iget v2, p1, Landroid/os/Message;->what:I

    iget v3, p1, Landroid/os/Message;->arg1:I

    iget v4, p1, Landroid/os/Message;->arg2:I

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/mstar/android/tvapi/dtv/common/CaManager$OnCaEventListener;->onUNLockService(Lcom/mstar/android/tvapi/dtv/common/CaManager;III)Z

    goto/16 :goto_0

    :pswitch_e
    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;->this$0:Lcom/mstar/android/tvapi/dtv/common/CaManager;

    # getter for: Lcom/mstar/android/tvapi/dtv/common/CaManager;->mOnEventListener:Lcom/mstar/android/tvapi/dtv/common/CaManager$OnCaEventListener;
    invoke-static {v0}, Lcom/mstar/android/tvapi/dtv/common/CaManager;->access$100(Lcom/mstar/android/tvapi/dtv/common/CaManager;)Lcom/mstar/android/tvapi/dtv/common/CaManager$OnCaEventListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;->this$0:Lcom/mstar/android/tvapi/dtv/common/CaManager;

    # getter for: Lcom/mstar/android/tvapi/dtv/common/CaManager;->mOnEventListener:Lcom/mstar/android/tvapi/dtv/common/CaManager$OnCaEventListener;
    invoke-static {v0}, Lcom/mstar/android/tvapi/dtv/common/CaManager;->access$100(Lcom/mstar/android/tvapi/dtv/common/CaManager;)Lcom/mstar/android/tvapi/dtv/common/CaManager$OnCaEventListener;

    move-result-object v0

    iget-object v1, p0, Lcom/mstar/android/tvapi/dtv/common/CaManager$EventHandler;->mMSrv:Lcom/mstar/android/tvapi/dtv/common/CaManager;

    iget v2, p1, Landroid/os/Message;->what:I

    iget v3, p1, Landroid/os/Message;->arg1:I

    iget v4, p1, Landroid/os/Message;->arg2:I

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/mstar/android/tvapi/dtv/common/CaManager$OnCaEventListener;->onOtaState(Lcom/mstar/android/tvapi/dtv/common/CaManager;III)Z

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
    .end packed-switch
.end method
