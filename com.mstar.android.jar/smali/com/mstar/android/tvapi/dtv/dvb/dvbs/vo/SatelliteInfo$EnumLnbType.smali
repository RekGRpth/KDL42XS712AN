.class public final enum Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo$EnumLnbType;
.super Ljava/lang/Enum;
.source "SatelliteInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EnumLnbType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo$EnumLnbType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo$EnumLnbType;

.field public static final enum E_C_BAND:Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo$EnumLnbType;

.field public static final enum E_KU_BAND:Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo$EnumLnbType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo$EnumLnbType;

    const-string v1, "E_C_BAND"

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo$EnumLnbType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo$EnumLnbType;->E_C_BAND:Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo$EnumLnbType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo$EnumLnbType;

    const-string v1, "E_KU_BAND"

    invoke-direct {v0, v1, v3}, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo$EnumLnbType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo$EnumLnbType;->E_KU_BAND:Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo$EnumLnbType;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo$EnumLnbType;

    sget-object v1, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo$EnumLnbType;->E_C_BAND:Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo$EnumLnbType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo$EnumLnbType;->E_KU_BAND:Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo$EnumLnbType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo$EnumLnbType;->$VALUES:[Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo$EnumLnbType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo$EnumLnbType;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo$EnumLnbType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo$EnumLnbType;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo$EnumLnbType;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo$EnumLnbType;->$VALUES:[Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo$EnumLnbType;

    invoke-virtual {v0}, [Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo$EnumLnbType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo$EnumLnbType;

    return-object v0
.end method
