.class public Lcom/mstar/android/tvapi/dtv/dvb/dvbt/DtvDemodDvbtInfo;
.super Ljava/lang/Object;
.source "DtvDemodDvbtInfo.java"


# instance fields
.field public SfoValue:F

.field public TotalCfo:F

.field public u16ChannelLength:S

.field public u16DemodState:B

.field public u16Version:S

.field public u8ChLen:B

.field public u8Constel:B

.field public u8DigAci:B

.field public u8Fd:B

.field public u8Fft:B

.field public u8FlagCi:B

.field public u8Gi:B

.field public u8Hiearchy:B

.field public u8HpCr:B

.field public u8LpCr:B

.field public u8PertoneNum:B

.field public u8SnrSel:B

.field public u8TdCoef:B


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-short v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbt/DtvDemodDvbtInfo;->u16Version:S

    iput-byte v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbt/DtvDemodDvbtInfo;->u16DemodState:B

    iput v1, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbt/DtvDemodDvbtInfo;->SfoValue:F

    iput v1, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbt/DtvDemodDvbtInfo;->TotalCfo:F

    iput-short v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbt/DtvDemodDvbtInfo;->u16ChannelLength:S

    iput-byte v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbt/DtvDemodDvbtInfo;->u8Fft:B

    iput-byte v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbt/DtvDemodDvbtInfo;->u8Constel:B

    iput-byte v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbt/DtvDemodDvbtInfo;->u8Gi:B

    iput-byte v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbt/DtvDemodDvbtInfo;->u8HpCr:B

    iput-byte v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbt/DtvDemodDvbtInfo;->u8LpCr:B

    iput-byte v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbt/DtvDemodDvbtInfo;->u8Hiearchy:B

    iput-byte v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbt/DtvDemodDvbtInfo;->u8Fd:B

    iput-byte v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbt/DtvDemodDvbtInfo;->u8ChLen:B

    iput-byte v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbt/DtvDemodDvbtInfo;->u8SnrSel:B

    iput-byte v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbt/DtvDemodDvbtInfo;->u8PertoneNum:B

    iput-byte v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbt/DtvDemodDvbtInfo;->u8DigAci:B

    iput-byte v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbt/DtvDemodDvbtInfo;->u8FlagCi:B

    iput-byte v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbt/DtvDemodDvbtInfo;->u8TdCoef:B

    return-void
.end method
