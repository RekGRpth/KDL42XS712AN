.class public final enum Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo$EnumLnbTypeReal;
.super Ljava/lang/Enum;
.source "SatelliteInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EnumLnbTypeReal"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo$EnumLnbTypeReal;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo$EnumLnbTypeReal;

.field public static final enum E_LNB_5150:Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo$EnumLnbTypeReal;

.field public static final enum E_LNB_9750:Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo$EnumLnbTypeReal;

.field public static final enum E_LNB_UNIVERSAL:Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo$EnumLnbTypeReal;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo$EnumLnbTypeReal;

    const-string v1, "E_LNB_UNIVERSAL"

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo$EnumLnbTypeReal;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo$EnumLnbTypeReal;->E_LNB_UNIVERSAL:Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo$EnumLnbTypeReal;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo$EnumLnbTypeReal;

    const-string v1, "E_LNB_9750"

    invoke-direct {v0, v1, v3}, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo$EnumLnbTypeReal;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo$EnumLnbTypeReal;->E_LNB_9750:Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo$EnumLnbTypeReal;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo$EnumLnbTypeReal;

    const-string v1, "E_LNB_5150"

    invoke-direct {v0, v1, v4}, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo$EnumLnbTypeReal;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo$EnumLnbTypeReal;->E_LNB_5150:Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo$EnumLnbTypeReal;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo$EnumLnbTypeReal;

    sget-object v1, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo$EnumLnbTypeReal;->E_LNB_UNIVERSAL:Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo$EnumLnbTypeReal;

    aput-object v1, v0, v2

    sget-object v1, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo$EnumLnbTypeReal;->E_LNB_9750:Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo$EnumLnbTypeReal;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo$EnumLnbTypeReal;->E_LNB_5150:Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo$EnumLnbTypeReal;

    aput-object v1, v0, v4

    sput-object v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo$EnumLnbTypeReal;->$VALUES:[Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo$EnumLnbTypeReal;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo$EnumLnbTypeReal;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo$EnumLnbTypeReal;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo$EnumLnbTypeReal;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo$EnumLnbTypeReal;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo$EnumLnbTypeReal;->$VALUES:[Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo$EnumLnbTypeReal;

    invoke-virtual {v0}, [Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo$EnumLnbTypeReal;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/SatelliteInfo$EnumLnbTypeReal;

    return-object v0
.end method
