.class public final enum Lcom/mstar/android/tvapi/dtv/dvb/vo/EnumDvbSystemType;
.super Ljava/lang/Enum;
.source "EnumDvbSystemType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/tvapi/dtv/dvb/vo/EnumDvbSystemType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/tvapi/dtv/dvb/vo/EnumDvbSystemType;

.field public static final enum E_DVB_System_DTMB:Lcom/mstar/android/tvapi/dtv/dvb/vo/EnumDvbSystemType;

.field public static final enum E_DVB_System_DVBC:Lcom/mstar/android/tvapi/dtv/dvb/vo/EnumDvbSystemType;

.field public static final enum E_DVB_System_DVBS:Lcom/mstar/android/tvapi/dtv/dvb/vo/EnumDvbSystemType;

.field public static final enum E_DVB_System_DVBS2:Lcom/mstar/android/tvapi/dtv/dvb/vo/EnumDvbSystemType;

.field public static final enum E_DVB_System_DVBT:Lcom/mstar/android/tvapi/dtv/dvb/vo/EnumDvbSystemType;

.field public static final enum E_DVB_System_DVBT2:Lcom/mstar/android/tvapi/dtv/dvb/vo/EnumDvbSystemType;

.field public static final enum E_DVB_System_NONE:Lcom/mstar/android/tvapi/dtv/dvb/vo/EnumDvbSystemType;

.field public static final enum E_DVB_System_NUM:Lcom/mstar/android/tvapi/dtv/dvb/vo/EnumDvbSystemType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/dtv/dvb/vo/EnumDvbSystemType;

    const-string v1, "E_DVB_System_NONE"

    invoke-direct {v0, v1, v3}, Lcom/mstar/android/tvapi/dtv/dvb/vo/EnumDvbSystemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/dvb/vo/EnumDvbSystemType;->E_DVB_System_NONE:Lcom/mstar/android/tvapi/dtv/dvb/vo/EnumDvbSystemType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/dvb/vo/EnumDvbSystemType;

    const-string v1, "E_DVB_System_DVBT"

    invoke-direct {v0, v1, v4}, Lcom/mstar/android/tvapi/dtv/dvb/vo/EnumDvbSystemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/dvb/vo/EnumDvbSystemType;->E_DVB_System_DVBT:Lcom/mstar/android/tvapi/dtv/dvb/vo/EnumDvbSystemType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/dvb/vo/EnumDvbSystemType;

    const-string v1, "E_DVB_System_DVBC"

    invoke-direct {v0, v1, v5}, Lcom/mstar/android/tvapi/dtv/dvb/vo/EnumDvbSystemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/dvb/vo/EnumDvbSystemType;->E_DVB_System_DVBC:Lcom/mstar/android/tvapi/dtv/dvb/vo/EnumDvbSystemType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/dvb/vo/EnumDvbSystemType;

    const-string v1, "E_DVB_System_DVBS"

    invoke-direct {v0, v1, v6}, Lcom/mstar/android/tvapi/dtv/dvb/vo/EnumDvbSystemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/dvb/vo/EnumDvbSystemType;->E_DVB_System_DVBS:Lcom/mstar/android/tvapi/dtv/dvb/vo/EnumDvbSystemType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/dvb/vo/EnumDvbSystemType;

    const-string v1, "E_DVB_System_DVBT2"

    invoke-direct {v0, v1, v7}, Lcom/mstar/android/tvapi/dtv/dvb/vo/EnumDvbSystemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/dvb/vo/EnumDvbSystemType;->E_DVB_System_DVBT2:Lcom/mstar/android/tvapi/dtv/dvb/vo/EnumDvbSystemType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/dvb/vo/EnumDvbSystemType;

    const-string v1, "E_DVB_System_DVBS2"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/dtv/dvb/vo/EnumDvbSystemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/dvb/vo/EnumDvbSystemType;->E_DVB_System_DVBS2:Lcom/mstar/android/tvapi/dtv/dvb/vo/EnumDvbSystemType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/dvb/vo/EnumDvbSystemType;

    const-string v1, "E_DVB_System_DTMB"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/dtv/dvb/vo/EnumDvbSystemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/dvb/vo/EnumDvbSystemType;->E_DVB_System_DTMB:Lcom/mstar/android/tvapi/dtv/dvb/vo/EnumDvbSystemType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/dvb/vo/EnumDvbSystemType;

    const-string v1, "E_DVB_System_NUM"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/dtv/dvb/vo/EnumDvbSystemType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/dvb/vo/EnumDvbSystemType;->E_DVB_System_NUM:Lcom/mstar/android/tvapi/dtv/dvb/vo/EnumDvbSystemType;

    const/16 v0, 0x8

    new-array v0, v0, [Lcom/mstar/android/tvapi/dtv/dvb/vo/EnumDvbSystemType;

    sget-object v1, Lcom/mstar/android/tvapi/dtv/dvb/vo/EnumDvbSystemType;->E_DVB_System_NONE:Lcom/mstar/android/tvapi/dtv/dvb/vo/EnumDvbSystemType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mstar/android/tvapi/dtv/dvb/vo/EnumDvbSystemType;->E_DVB_System_DVBT:Lcom/mstar/android/tvapi/dtv/dvb/vo/EnumDvbSystemType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/android/tvapi/dtv/dvb/vo/EnumDvbSystemType;->E_DVB_System_DVBC:Lcom/mstar/android/tvapi/dtv/dvb/vo/EnumDvbSystemType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mstar/android/tvapi/dtv/dvb/vo/EnumDvbSystemType;->E_DVB_System_DVBS:Lcom/mstar/android/tvapi/dtv/dvb/vo/EnumDvbSystemType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/mstar/android/tvapi/dtv/dvb/vo/EnumDvbSystemType;->E_DVB_System_DVBT2:Lcom/mstar/android/tvapi/dtv/dvb/vo/EnumDvbSystemType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/mstar/android/tvapi/dtv/dvb/vo/EnumDvbSystemType;->E_DVB_System_DVBS2:Lcom/mstar/android/tvapi/dtv/dvb/vo/EnumDvbSystemType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/mstar/android/tvapi/dtv/dvb/vo/EnumDvbSystemType;->E_DVB_System_DTMB:Lcom/mstar/android/tvapi/dtv/dvb/vo/EnumDvbSystemType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/mstar/android/tvapi/dtv/dvb/vo/EnumDvbSystemType;->E_DVB_System_NUM:Lcom/mstar/android/tvapi/dtv/dvb/vo/EnumDvbSystemType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/mstar/android/tvapi/dtv/dvb/vo/EnumDvbSystemType;->$VALUES:[Lcom/mstar/android/tvapi/dtv/dvb/vo/EnumDvbSystemType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/tvapi/dtv/dvb/vo/EnumDvbSystemType;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/tvapi/dtv/dvb/vo/EnumDvbSystemType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/dtv/dvb/vo/EnumDvbSystemType;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/tvapi/dtv/dvb/vo/EnumDvbSystemType;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/dtv/dvb/vo/EnumDvbSystemType;->$VALUES:[Lcom/mstar/android/tvapi/dtv/dvb/vo/EnumDvbSystemType;

    invoke-virtual {v0}, [Lcom/mstar/android/tvapi/dtv/dvb/vo/EnumDvbSystemType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/tvapi/dtv/dvb/vo/EnumDvbSystemType;

    return-object v0
.end method
