.class public Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbSecondaryRegionInfo;
.super Ljava/lang/Object;
.source "DvbSecondaryRegionInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbSecondaryRegionInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public code:S

.field public regionName:Ljava/lang/String;

.field public tertiaryRegionInfos:[Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbTeritaryRegionInfo;

.field public tertiaryRegionNum:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbSecondaryRegionInfo$1;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbSecondaryRegionInfo$1;-><init>()V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbSecondaryRegionInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-short v2, p0, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbSecondaryRegionInfo;->code:S

    const-string v1, ""

    iput-object v1, p0, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbSecondaryRegionInfo;->regionName:Ljava/lang/String;

    iput v2, p0, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbSecondaryRegionInfo;->tertiaryRegionNum:I

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbSecondaryRegionInfo;->tertiaryRegionInfos:[Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbTeritaryRegionInfo;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbSecondaryRegionInfo;->tertiaryRegionInfos:[Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbTeritaryRegionInfo;

    new-instance v2, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbTeritaryRegionInfo;

    invoke-direct {v2}, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbTeritaryRegionInfo;-><init>()V

    aput-object v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 3
    .param p1    # Landroid/os/Parcel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    int-to-short v1, v1

    iput-short v1, p0, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbSecondaryRegionInfo;->code:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbSecondaryRegionInfo;->regionName:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbSecondaryRegionInfo;->tertiaryRegionNum:I

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbSecondaryRegionInfo;->tertiaryRegionInfos:[Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbTeritaryRegionInfo;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget-object v2, p0, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbSecondaryRegionInfo;->tertiaryRegionInfos:[Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbTeritaryRegionInfo;

    sget-object v1, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbTeritaryRegionInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v1, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbTeritaryRegionInfo;

    aput-object v1, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbSecondaryRegionInfo$1;)V
    .locals 0
    .param p1    # Landroid/os/Parcel;
    .param p2    # Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbSecondaryRegionInfo$1;

    invoke-direct {p0, p1}, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbSecondaryRegionInfo;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget-short v1, p0, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbSecondaryRegionInfo;->code:S

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v1, p0, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbSecondaryRegionInfo;->regionName:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget v1, p0, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbSecondaryRegionInfo;->tertiaryRegionNum:I

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbSecondaryRegionInfo;->tertiaryRegionInfos:[Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbTeritaryRegionInfo;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbSecondaryRegionInfo;->tertiaryRegionInfos:[Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbTeritaryRegionInfo;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1, p2}, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbTeritaryRegionInfo;->writeToParcel(Landroid/os/Parcel;I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method
