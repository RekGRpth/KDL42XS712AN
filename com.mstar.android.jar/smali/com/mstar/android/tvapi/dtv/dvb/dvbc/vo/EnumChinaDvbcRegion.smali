.class public final enum Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;
.super Ljava/lang/Enum;
.source "EnumChinaDvbcRegion.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

.field public static final enum E_CN_ANHUI_PROVINCE_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

.field public static final enum E_CN_ANQING_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

.field public static final enum E_CN_BEIJING_GEHUA_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

.field public static final enum E_CN_BENGBU_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

.field public static final enum E_CN_CHANGSHA_GUANGDA_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

.field public static final enum E_CN_CHANGSHA_GUOAN_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

.field public static final enum E_CN_CHENGDU_XING_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

.field public static final enum E_CN_CHENGHUA_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

.field public static final enum E_CN_DALIAN_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

.field public static final enum E_CN_DANJIANG_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

.field public static final enum E_CN_DONGGUAN_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

.field public static final enum E_CN_GUANGZHOU_PROVINCE_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

.field public static final enum E_CN_GUANGZHOU_ZHUJIANG_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

.field public static final enum E_CN_GUIYANG_PROVINCE_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

.field public static final enum E_CN_HAERBIN_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

.field public static final enum E_CN_HAIKOU_CABEL:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

.field public static final enum E_CN_HEFEI_CABEL:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

.field public static final enum E_CN_HUANGGANG_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

.field public static final enum E_CN_HUBEI_CHUTIAN_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

.field public static final enum E_CN_HUHEHAOTE_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

.field public static final enum E_CN_JILIN_PROVINCE_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

.field public static final enum E_CN_JINGZHOU_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

.field public static final enum E_CN_JINHUA_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

.field public static final enum E_CN_LONGYAN_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

.field public static final enum E_CN_LUOYANG_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

.field public static final enum E_CN_MIANYANG_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

.field public static final enum E_CN_NANCHANG_CABEL:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

.field public static final enum E_CN_NANTONG_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

.field public static final enum E_CN_NANYANG_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

.field public static final enum E_CN_NINGBO_CABEL:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

.field public static final enum E_CN_NUM:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

.field public static final enum E_CN_OTHERS:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

.field public static final enum E_CN_QINHUANGDAO_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

.field public static final enum E_CN_QUANZHOU_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

.field public static final enum E_CN_SHAOXING_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

.field public static final enum E_CN_SHIJIAZHUANG_CABEL:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

.field public static final enum E_CN_SHIYAN_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

.field public static final enum E_CN_TANGSHAN_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

.field public static final enum E_CN_WUHAN_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

.field public static final enum E_CN_WULUMUQI_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

.field public static final enum E_CN_XIAMEN_PROVINCE_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

.field public static final enum E_CN_XIAN_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

.field public static final enum E_CN_YANCHENG_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

.field public static final enum E_CN_YICHANG_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

.field public static final enum E_CN_ZHANGZHOU_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

.field public static final enum E_CN_ZHENGZHOU_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    const-string v1, "E_CN_OTHERS"

    invoke-direct {v0, v1, v3}, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_OTHERS:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    const-string v1, "E_CN_BEIJING_GEHUA_CABLE"

    invoke-direct {v0, v1, v4}, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_BEIJING_GEHUA_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    const-string v1, "E_CN_ANHUI_PROVINCE_CABLE"

    invoke-direct {v0, v1, v5}, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_ANHUI_PROVINCE_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    const-string v1, "E_CN_HEFEI_CABEL"

    invoke-direct {v0, v1, v6}, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_HEFEI_CABEL:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    const-string v1, "E_CN_ANQING_CABLE"

    invoke-direct {v0, v1, v7}, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_ANQING_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    const-string v1, "E_CN_BENGBU_CABLE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_BENGBU_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    const-string v1, "E_CN_XIAMEN_PROVINCE_CABLE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_XIAMEN_PROVINCE_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    const-string v1, "E_CN_QUANZHOU_CABLE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_QUANZHOU_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    const-string v1, "E_CN_LONGYAN_CABLE"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_LONGYAN_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    const-string v1, "E_CN_ZHANGZHOU_CABLE"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_ZHANGZHOU_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    const-string v1, "E_CN_GUANGZHOU_ZHUJIANG_CABLE"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_GUANGZHOU_ZHUJIANG_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    const-string v1, "E_CN_GUANGZHOU_PROVINCE_CABLE"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_GUANGZHOU_PROVINCE_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    const-string v1, "E_CN_DONGGUAN_CABLE"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_DONGGUAN_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    const-string v1, "E_CN_GUIYANG_PROVINCE_CABLE"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_GUIYANG_PROVINCE_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    const-string v1, "E_CN_HAIKOU_CABEL"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_HAIKOU_CABEL:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    const-string v1, "E_CN_SHIJIAZHUANG_CABEL"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_SHIJIAZHUANG_CABEL:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    const-string v1, "E_CN_QINHUANGDAO_CABLE"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_QINHUANGDAO_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    const-string v1, "E_CN_TANGSHAN_CABLE"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_TANGSHAN_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    const-string v1, "E_CN_HAERBIN_CABLE"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_HAERBIN_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    const-string v1, "E_CN_ZHENGZHOU_CABLE"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_ZHENGZHOU_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    const-string v1, "E_CN_LUOYANG_CABLE"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_LUOYANG_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    const-string v1, "E_CN_NANYANG_CABLE"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_NANYANG_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    const-string v1, "E_CN_HUBEI_CHUTIAN_CABLE"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_HUBEI_CHUTIAN_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    const-string v1, "E_CN_WUHAN_CABLE"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_WUHAN_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    const-string v1, "E_CN_HUANGGANG_CABLE"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_HUANGGANG_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    const-string v1, "E_CN_JINGZHOU_CABLE"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_JINGZHOU_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    const-string v1, "E_CN_YICHANG_CABLE"

    const/16 v2, 0x1a

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_YICHANG_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    const-string v1, "E_CN_CHANGSHA_GUANGDA_CABLE"

    const/16 v2, 0x1b

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_CHANGSHA_GUANGDA_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    const-string v1, "E_CN_CHANGSHA_GUOAN_CABLE"

    const/16 v2, 0x1c

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_CHANGSHA_GUOAN_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    const-string v1, "E_CN_NANTONG_CABLE"

    const/16 v2, 0x1d

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_NANTONG_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    const-string v1, "E_CN_NANCHANG_CABEL"

    const/16 v2, 0x1e

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_NANCHANG_CABEL:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    const-string v1, "E_CN_JILIN_PROVINCE_CABLE"

    const/16 v2, 0x1f

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_JILIN_PROVINCE_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    const-string v1, "E_CN_DALIAN_CABLE"

    const/16 v2, 0x20

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_DALIAN_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    const-string v1, "E_CN_HUHEHAOTE_CABLE"

    const/16 v2, 0x21

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_HUHEHAOTE_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    const-string v1, "E_CN_XIAN_CABLE"

    const/16 v2, 0x22

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_XIAN_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    const-string v1, "E_CN_CHENGDU_XING_CABLE"

    const/16 v2, 0x23

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_CHENGDU_XING_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    const-string v1, "E_CN_WULUMUQI_CABLE"

    const/16 v2, 0x24

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_WULUMUQI_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    const-string v1, "E_CN_JINHUA_CABLE"

    const/16 v2, 0x25

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_JINHUA_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    const-string v1, "E_CN_NINGBO_CABEL"

    const/16 v2, 0x26

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_NINGBO_CABEL:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    const-string v1, "E_CN_SHAOXING_CABLE"

    const/16 v2, 0x27

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_SHAOXING_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    const-string v1, "E_CN_MIANYANG_CABLE"

    const/16 v2, 0x28

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_MIANYANG_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    const-string v1, "E_CN_SHIYAN_CABLE"

    const/16 v2, 0x29

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_SHIYAN_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    const-string v1, "E_CN_YANCHENG_CABLE"

    const/16 v2, 0x2a

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_YANCHENG_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    const-string v1, "E_CN_CHENGHUA_CABLE"

    const/16 v2, 0x2b

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_CHENGHUA_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    const-string v1, "E_CN_DANJIANG_CABLE"

    const/16 v2, 0x2c

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_DANJIANG_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    const-string v1, "E_CN_NUM"

    const/16 v2, 0x2d

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_NUM:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    const/16 v0, 0x2e

    new-array v0, v0, [Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    sget-object v1, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_OTHERS:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_BEIJING_GEHUA_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_ANHUI_PROVINCE_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_HEFEI_CABEL:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    aput-object v1, v0, v6

    sget-object v1, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_ANQING_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_BENGBU_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_XIAMEN_PROVINCE_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_QUANZHOU_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_LONGYAN_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_ZHANGZHOU_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_GUANGZHOU_ZHUJIANG_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_GUANGZHOU_PROVINCE_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_DONGGUAN_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_GUIYANG_PROVINCE_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_HAIKOU_CABEL:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_SHIJIAZHUANG_CABEL:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_QINHUANGDAO_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_TANGSHAN_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_HAERBIN_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_ZHENGZHOU_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_LUOYANG_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_NANYANG_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_HUBEI_CHUTIAN_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_WUHAN_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_HUANGGANG_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_JINGZHOU_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_YICHANG_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_CHANGSHA_GUANGDA_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_CHANGSHA_GUOAN_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_NANTONG_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_NANCHANG_CABEL:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_JILIN_PROVINCE_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_DALIAN_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_HUHEHAOTE_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_XIAN_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_CHENGDU_XING_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_WULUMUQI_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_JINHUA_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_NINGBO_CABEL:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_SHAOXING_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_MIANYANG_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_SHIYAN_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_YANCHENG_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_CHENGHUA_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_DANJIANG_CABLE:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->E_CN_NUM:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    aput-object v2, v0, v1

    sput-object v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->$VALUES:[Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->$VALUES:[Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    invoke-virtual {v0}, [Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumChinaDvbcRegion;

    return-object v0
.end method
