.class public Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/DvbsTransponderInfo;
.super Ljava/lang/Object;
.source "DvbsTransponderInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/DvbsTransponderInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public frequency:I

.field public polarity:B

.field public rf:S

.field public satelliteId:S

.field public symbolRate:I

.field public tpId:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/DvbsTransponderInfo$1;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/DvbsTransponderInfo$1;-><init>()V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/DvbsTransponderInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/DvbsTransponderInfo;->frequency:I

    iput v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/DvbsTransponderInfo;->symbolRate:I

    iput-byte v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/DvbsTransponderInfo;->polarity:B

    iput-short v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/DvbsTransponderInfo;->satelliteId:S

    iput-short v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/DvbsTransponderInfo;->rf:S

    iput v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/DvbsTransponderInfo;->tpId:I

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1    # Landroid/os/Parcel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/DvbsTransponderInfo;->frequency:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/DvbsTransponderInfo;->symbolRate:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/DvbsTransponderInfo;->polarity:B

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/DvbsTransponderInfo;->satelliteId:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/DvbsTransponderInfo;->rf:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/DvbsTransponderInfo;->tpId:I

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/DvbsTransponderInfo$1;)V
    .locals 0
    .param p1    # Landroid/os/Parcel;
    .param p2    # Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/DvbsTransponderInfo$1;

    invoke-direct {p0, p1}, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/DvbsTransponderInfo;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/DvbsTransponderInfo;->frequency:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/DvbsTransponderInfo;->symbolRate:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-byte v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/DvbsTransponderInfo;->polarity:B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/DvbsTransponderInfo;->satelliteId:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/DvbsTransponderInfo;->rf:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/DvbsTransponderInfo;->tpId:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
