.class public Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfigDb;
.super Ljava/lang/Object;
.source "ScifConfigDb.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfigDb;",
            ">;"
        }
    .end annotation
.end field

.field public static final MAX_NUM_UBAND:I = 0x8


# instance fields
.field public frequencies:[S

.field public hiLOF:S

.field public lowLOF:S

.field public scifConfig:Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfig;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfigDb$1;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfigDb$1;-><init>()V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfigDb;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    const/16 v3, 0x8

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-array v1, v3, [S

    iput-object v1, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfigDb;->frequencies:[S

    new-instance v1, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfig;

    invoke-direct {v1}, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfig;-><init>()V

    iput-object v1, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfigDb;->scifConfig:Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfig;

    iput-short v2, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfigDb;->hiLOF:S

    iput-short v2, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfigDb;->lowLOF:S

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    iget-object v1, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfigDb;->frequencies:[S

    aput-short v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 4
    .param p1    # Landroid/os/Parcel;

    const/16 v3, 0x8

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-array v1, v3, [S

    iput-object v1, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfigDb;->frequencies:[S

    sget-object v1, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v1, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfig;

    iput-object v1, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfigDb;->scifConfig:Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfig;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    int-to-short v1, v1

    iput-short v1, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfigDb;->hiLOF:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    int-to-short v1, v1

    iput-short v1, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfigDb;->lowLOF:S

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    iget-object v1, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfigDb;->frequencies:[S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    int-to-short v2, v2

    aput-short v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfigDb$1;)V
    .locals 0
    .param p1    # Landroid/os/Parcel;
    .param p2    # Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfigDb$1;

    invoke-direct {p0, p1}, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfigDb;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget-object v1, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfigDb;->scifConfig:Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfig;

    invoke-virtual {v1, p1, p2}, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfig;->writeToParcel(Landroid/os/Parcel;I)V

    iget-short v1, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfigDb;->hiLOF:S

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v1, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfigDb;->lowLOF:S

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    const/4 v0, 0x0

    :goto_0
    const/16 v1, 0x8

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfigDb;->frequencies:[S

    aget-short v1, v1, v0

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method
