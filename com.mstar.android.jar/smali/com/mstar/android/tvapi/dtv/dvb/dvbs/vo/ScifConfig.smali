.class public Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfig;
.super Ljava/lang/Object;
.source "ScifConfig.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfig$EnumRfType;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfig;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public numBank:S

.field public numStaPos:S

.field public numUB:S

.field public rfType:Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfig$EnumRfType;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfig$1;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfig$1;-><init>()V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-short v1, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfig;->numStaPos:S

    iput-short v1, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfig;->numBank:S

    sget-object v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfig$EnumRfType;->E_NONE_RF:Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfig$EnumRfType;

    iput-object v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfig;->rfType:Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfig$EnumRfType;

    iput-short v1, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfig;->numUB:S

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .param p1    # Landroid/os/Parcel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfig;->numStaPos:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfig;->numBank:S

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfig$EnumRfType;->values()[Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfig$EnumRfType;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfig;->rfType:Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfig$EnumRfType;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfig;->numUB:S

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfig$1;)V
    .locals 0
    .param p1    # Landroid/os/Parcel;
    .param p2    # Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfig$1;

    invoke-direct {p0, p1}, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfig;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget-short v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfig;->numStaPos:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfig;->numBank:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfig;->rfType:Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfig$EnumRfType;

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfig$EnumRfType;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/android/tvapi/dtv/dvb/dvbs/vo/ScifConfig;->numUB:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
