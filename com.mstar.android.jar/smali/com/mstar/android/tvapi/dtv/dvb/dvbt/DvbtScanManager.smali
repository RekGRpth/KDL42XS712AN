.class public interface abstract Lcom/mstar/android/tvapi/dtv/dvb/dvbt/DvbtScanManager;
.super Ljava/lang/Object;
.source "DvbtScanManager.java"

# interfaces
.implements Lcom/mstar/android/tvapi/dtv/common/DtvScanManager;


# virtual methods
.method public abstract resolveConflictLcn()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract setRegion(Ljava/lang/String;SSI)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method
