.class public interface abstract Lcom/mstar/android/tvapi/dtv/dvb/dvbc/DvbcScanManager;
.super Ljava/lang/Object;
.source "DvbcScanManager.java"

# interfaces
.implements Lcom/mstar/android/tvapi/dtv/common/DtvScanManager;


# virtual methods
.method public abstract getDefaultHomingChannelFrequency()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract getDefaultNetworkId()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract setCableOperator(Lcom/mstar/android/tvapi/common/vo/EnumCableOperator;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract setScanParam(SLcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;IISZ)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract startQuickScan()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method
