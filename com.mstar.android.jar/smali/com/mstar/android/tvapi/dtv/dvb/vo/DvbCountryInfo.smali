.class public Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbCountryInfo;
.super Ljava/lang/Object;
.source "DvbCountryInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbCountryInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public countryCode:[C

.field public primaryRegionCount:I

.field public primaryRegionInfos:[Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbPrimaryRegionInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbCountryInfo$1;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbCountryInfo$1;-><init>()V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbCountryInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v1, 0x3

    new-array v1, v1, [C

    iput-object v1, p0, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbCountryInfo;->countryCode:[C

    iput v2, p0, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbCountryInfo;->primaryRegionCount:I

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbCountryInfo;->countryCode:[C

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbCountryInfo;->countryCode:[C

    aput-char v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbCountryInfo;->primaryRegionInfos:[Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbPrimaryRegionInfo;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbCountryInfo;->primaryRegionInfos:[Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbPrimaryRegionInfo;

    new-instance v2, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbPrimaryRegionInfo;

    invoke-direct {v2}, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbPrimaryRegionInfo;-><init>()V

    aput-object v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 3
    .param p1    # Landroid/os/Parcel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v1, 0x3

    new-array v1, v1, [C

    iput-object v1, p0, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbCountryInfo;->countryCode:[C

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbCountryInfo;->primaryRegionCount:I

    iget-object v1, p0, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbCountryInfo;->countryCode:[C

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readCharArray([C)V

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbCountryInfo;->primaryRegionInfos:[Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbPrimaryRegionInfo;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget-object v2, p0, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbCountryInfo;->primaryRegionInfos:[Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbPrimaryRegionInfo;

    sget-object v1, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbPrimaryRegionInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v1, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbPrimaryRegionInfo;

    aput-object v1, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbCountryInfo$1;)V
    .locals 0
    .param p1    # Landroid/os/Parcel;
    .param p2    # Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbCountryInfo$1;

    invoke-direct {p0, p1}, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbCountryInfo;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget v1, p0, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbCountryInfo;->primaryRegionCount:I

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v1, p0, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbCountryInfo;->countryCode:[C

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeCharArray([C)V

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbCountryInfo;->primaryRegionInfos:[Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbPrimaryRegionInfo;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbCountryInfo;->primaryRegionInfos:[Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbPrimaryRegionInfo;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1, p2}, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbPrimaryRegionInfo;->writeToParcel(Landroid/os/Parcel;I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method
