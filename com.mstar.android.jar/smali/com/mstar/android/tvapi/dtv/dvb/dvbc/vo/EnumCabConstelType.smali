.class public final enum Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;
.super Ljava/lang/Enum;
.source "EnumCabConstelType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;

.field public static final enum E_CAB_INVALID:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;

.field public static final enum E_CAB_QAM128:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;

.field public static final enum E_CAB_QAM16:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;

.field public static final enum E_CAB_QAM256:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;

.field public static final enum E_CAB_QAM32:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;

.field public static final enum E_CAB_QAM64:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;

    const-string v1, "E_CAB_QAM16"

    invoke-direct {v0, v1, v3}, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;->E_CAB_QAM16:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;

    const-string v1, "E_CAB_QAM32"

    invoke-direct {v0, v1, v4}, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;->E_CAB_QAM32:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;

    const-string v1, "E_CAB_QAM64"

    invoke-direct {v0, v1, v5}, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;->E_CAB_QAM64:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;

    const-string v1, "E_CAB_QAM128"

    invoke-direct {v0, v1, v6}, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;->E_CAB_QAM128:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;

    const-string v1, "E_CAB_QAM256"

    invoke-direct {v0, v1, v7}, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;->E_CAB_QAM256:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;

    const-string v1, "E_CAB_INVALID"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;->E_CAB_INVALID:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;

    sget-object v1, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;->E_CAB_QAM16:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;->E_CAB_QAM32:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;->E_CAB_QAM64:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;->E_CAB_QAM128:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;->E_CAB_QAM256:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;->E_CAB_INVALID:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;->$VALUES:[Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;->$VALUES:[Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;

    invoke-virtual {v0}, [Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;

    return-object v0
.end method
