.class public interface abstract Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;
.super Ljava/lang/Object;
.source "OnDtvPlayerEventListener.java"


# virtual methods
.method public abstract onAudioModeChange(IZ)Z
.end method

.method public abstract onChangeTtxStatus(IZ)Z
.end method

.method public abstract onCiLoadCredentialFail(I)Z
.end method

.method public abstract onDtvAutoTuningScanInfo(ILcom/mstar/android/tvapi/dtv/vo/DtvEventScan;)Z
.end method

.method public abstract onDtvAutoUpdateScan(I)Z
.end method

.method public abstract onDtvChannelNameReady(I)Z
.end method

.method public abstract onDtvPriComponentMissing(I)Z
.end method

.method public abstract onDtvProgramInfoReady(I)Z
.end method

.method public abstract onEpgTimerSimulcast(II)Z
.end method

.method public abstract onGingaStatusMode(IZ)Z
.end method

.method public abstract onHbbtvStatusMode(IZ)Z
.end method

.method public abstract onMheg5EventHandler(II)Z
.end method

.method public abstract onMheg5ReturnKey(II)Z
.end method

.method public abstract onMheg5StatusMode(II)Z
.end method

.method public abstract onOadDownload(II)Z
.end method

.method public abstract onOadHandler(III)Z
.end method

.method public abstract onOadTimeout(II)Z
.end method

.method public abstract onPopupScanDialogFrequencyChange(I)Z
.end method

.method public abstract onPopupScanDialogLossSignal(I)Z
.end method

.method public abstract onPopupScanDialogNewMultiplex(I)Z
.end method

.method public abstract onRctPresence(I)Z
.end method

.method public abstract onSignalLock(I)Z
.end method

.method public abstract onSignalUnLock(I)Z
.end method

.method public abstract onTsChange(I)Z
.end method
