.class public Lcom/mstar/android/tvapi/dtv/atsc/vo/Region5RatingInformation;
.super Ljava/lang/Object;
.source "Region5RatingInformation.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mstar/android/tvapi/dtv/atsc/vo/Region5RatingInformation$Region5DimensionInformation;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mstar/android/tvapi/dtv/atsc/vo/Region5RatingInformation;",
            ">;"
        }
    .end annotation
.end field

.field public static final RRT5_DIMENSIONS:I = 0x100

.field public static final RRT5_DIMNAME_LENGTH:I = 0x18

.field public static final RRT5_LEVELS:I = 0xf

.field public static final RRT5_REGNAME_LENGTH:I = 0x24

.field public static final RRT5_TEXT_LENGTH:I = 0x10


# instance fields
.field public dimensionNo:S

.field public regin5Dimensions:[Lcom/mstar/android/tvapi/dtv/atsc/vo/Region5RatingInformation$Region5DimensionInformation;

.field public region5Name:[S


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/Region5RatingInformation$1;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/dtv/atsc/vo/Region5RatingInformation$1;-><init>()V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/Region5RatingInformation;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 5
    .param p1    # Landroid/os/Parcel;

    const/16 v4, 0x100

    const/16 v3, 0x24

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-array v1, v3, [S

    iput-object v1, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/Region5RatingInformation;->region5Name:[S

    new-array v1, v4, [Lcom/mstar/android/tvapi/dtv/atsc/vo/Region5RatingInformation$Region5DimensionInformation;

    iput-object v1, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/Region5RatingInformation;->regin5Dimensions:[Lcom/mstar/android/tvapi/dtv/atsc/vo/Region5RatingInformation$Region5DimensionInformation;

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    iget-object v1, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/Region5RatingInformation;->region5Name:[S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    int-to-short v2, v2

    aput-short v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    int-to-short v1, v1

    iput-short v1, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/Region5RatingInformation;->dimensionNo:S

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v4, :cond_1

    iget-object v1, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/Region5RatingInformation;->regin5Dimensions:[Lcom/mstar/android/tvapi/dtv/atsc/vo/Region5RatingInformation$Region5DimensionInformation;

    new-instance v2, Lcom/mstar/android/tvapi/dtv/atsc/vo/Region5RatingInformation$Region5DimensionInformation;

    invoke-direct {v2, p0, p1}, Lcom/mstar/android/tvapi/dtv/atsc/vo/Region5RatingInformation$Region5DimensionInformation;-><init>(Lcom/mstar/android/tvapi/dtv/atsc/vo/Region5RatingInformation;Landroid/os/Parcel;)V

    aput-object v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/mstar/android/tvapi/dtv/atsc/vo/Region5RatingInformation$1;)V
    .locals 0
    .param p1    # Landroid/os/Parcel;
    .param p2    # Lcom/mstar/android/tvapi/dtv/atsc/vo/Region5RatingInformation$1;

    invoke-direct {p0, p1}, Lcom/mstar/android/tvapi/dtv/atsc/vo/Region5RatingInformation;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    const/4 v0, 0x0

    :goto_0
    const/16 v1, 0x24

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/Region5RatingInformation;->region5Name:[S

    aget-short v1, v1, v0

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-short v1, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/Region5RatingInformation;->dimensionNo:S

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    const/4 v0, 0x0

    :goto_1
    const/16 v1, 0x100

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/Region5RatingInformation;->regin5Dimensions:[Lcom/mstar/android/tvapi/dtv/atsc/vo/Region5RatingInformation$Region5DimensionInformation;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1}, Lcom/mstar/android/tvapi/dtv/atsc/vo/Region5RatingInformation$Region5DimensionInformation;->writeToParcel(Landroid/os/Parcel;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    return-void
.end method
