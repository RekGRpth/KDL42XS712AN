.class public Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscChannelAttribute;
.super Ljava/lang/Object;
.source "AtscProgramInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "AtscChannelAttribute"
.end annotation


# instance fields
.field public favorite:B

.field public isHide:B

.field public isLock:B

.field public isRenamed:B

.field public isScramble:B

.field public isSkipped:B

.field public scrambleChStatus:B

.field public serviceType:S

.field final synthetic this$0:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo;


# direct methods
.method public constructor <init>(Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo;)V
    .locals 1

    const/4 v0, 0x0

    iput-object p1, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscChannelAttribute;->this$0:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-byte v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscChannelAttribute;->scrambleChStatus:B

    iput-byte v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscChannelAttribute;->isScramble:B

    iput-byte v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscChannelAttribute;->isSkipped:B

    iput-byte v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscChannelAttribute;->isLock:B

    iput-byte v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscChannelAttribute;->favorite:B

    iput-byte v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscChannelAttribute;->isHide:B

    iput-byte v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscChannelAttribute;->isRenamed:B

    iput-short v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo$AtscChannelAttribute;->serviceType:S

    return-void
.end method
