.class public Lcom/mstar/android/tvapi/dtv/atsc/vo/Region5RatingInformation$Region5DimensionInformation;
.super Ljava/lang/Object;
.source "Region5RatingInformation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mstar/android/tvapi/dtv/atsc/vo/Region5RatingInformation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "Region5DimensionInformation"
.end annotation


# instance fields
.field public abbRatingText:[Ljava/lang/String;

.field public dimensionName:[S

.field public graduatedScale:C

.field final synthetic this$0:Lcom/mstar/android/tvapi/dtv/atsc/vo/Region5RatingInformation;

.field public valuesDefined:S


# direct methods
.method public constructor <init>(Lcom/mstar/android/tvapi/dtv/atsc/vo/Region5RatingInformation;Landroid/os/Parcel;)V
    .locals 8
    .param p2    # Landroid/os/Parcel;

    const/16 v7, 0x18

    iput-object p1, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/Region5RatingInformation$Region5DimensionInformation;->this$0:Lcom/mstar/android/tvapi/dtv/atsc/vo/Region5RatingInformation;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-array v5, v7, [S

    iput-object v5, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/Region5RatingInformation$Region5DimensionInformation;->dimensionName:[S

    const/16 v5, 0xf

    new-array v5, v5, [Ljava/lang/String;

    iput-object v5, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/Region5RatingInformation$Region5DimensionInformation;->abbRatingText:[Ljava/lang/String;

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v7, :cond_0

    iget-object v5, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/Region5RatingInformation$Region5DimensionInformation;->dimensionName:[S

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v6

    int-to-short v6, v6

    aput-short v6, v5, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    int-to-short v5, v5

    iput-short v5, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/Region5RatingInformation$Region5DimensionInformation;->valuesDefined:S

    invoke-virtual {p2}, Landroid/os/Parcel;->readByte()B

    move-result v5

    int-to-char v5, v5

    iput-char v5, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/Region5RatingInformation$Region5DimensionInformation;->graduatedScale:C

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/Region5RatingInformation$Region5DimensionInformation;->abbRatingText:[Ljava/lang/String;

    array-length v3, v0

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v3, :cond_1

    aget-object v4, v0, v2

    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    return-void
.end method


# virtual methods
.method public writeToParcel(Landroid/os/Parcel;)V
    .locals 6
    .param p1    # Landroid/os/Parcel;

    const/4 v1, 0x0

    :goto_0
    const/16 v5, 0x18

    if-ge v1, v5, :cond_0

    iget-object v5, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/Region5RatingInformation$Region5DimensionInformation;->dimensionName:[S

    aget-short v5, v5, v1

    invoke-virtual {p1, v5}, Landroid/os/Parcel;->writeInt(I)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    iget-short v5, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/Region5RatingInformation$Region5DimensionInformation;->valuesDefined:S

    invoke-virtual {p1, v5}, Landroid/os/Parcel;->writeInt(I)V

    iget-char v5, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/Region5RatingInformation$Region5DimensionInformation;->graduatedScale:C

    int-to-byte v5, v5

    invoke-virtual {p1, v5}, Landroid/os/Parcel;->writeByte(B)V

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/Region5RatingInformation$Region5DimensionInformation;->abbRatingText:[Ljava/lang/String;

    array-length v3, v0

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v3, :cond_1

    aget-object v4, v0, v2

    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    return-void
.end method
