.class public Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;
.super Ljava/lang/Object;
.source "AtscEpgEventInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$AtscEpgRating;,
        Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;",
            ">;"
        }
    .end annotation
.end field

.field private static enumhash:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public bHasCCInfo:Z

.field public durationTime:I

.field public enStrStatus:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;

.field public endTime:I

.field public sExtendedText:Ljava/lang/String;

.field public sName:Ljava/lang/String;

.field public stRating:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$AtscEpgRating;

.field public startTime:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;->enumhash:Ljava/util/Hashtable;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$1;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$1;-><init>()V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$AtscEpgRating;

    invoke-direct {v0, p0}, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$AtscEpgRating;-><init>(Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;)V

    iput-object v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;->stRating:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$AtscEpgRating;

    iput v1, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;->startTime:I

    iput v1, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;->endTime:I

    iput v1, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;->durationTime:I

    sget-object v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;->EN_ATSC_EPG_FUNC_STATUS_UNDEFINED:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;

    iput-object v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;->enStrStatus:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;

    const-string v0, ""

    iput-object v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;->sName:Ljava/lang/String;

    iput-boolean v1, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;->bHasCCInfo:Z

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;->stRating:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$AtscEpgRating;

    iput-byte v1, v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$AtscEpgRating;->ratingRxIsOK:B

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;->stRating:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$AtscEpgRating;

    iput-byte v1, v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$AtscEpgRating;->mpaaFlag:B

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;->stRating:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$AtscEpgRating;

    iput-byte v1, v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$AtscEpgRating;->tvRatingForEntire:B

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;->stRating:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$AtscEpgRating;

    iput-byte v1, v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$AtscEpgRating;->dialog:B

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;->stRating:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$AtscEpgRating;

    iput-byte v1, v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$AtscEpgRating;->language:B

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;->stRating:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$AtscEpgRating;

    iput-byte v1, v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$AtscEpgRating;->sexualContent:B

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;->stRating:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$AtscEpgRating;

    iput-byte v1, v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$AtscEpgRating;->violence:B

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;->stRating:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$AtscEpgRating;

    iput-byte v1, v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$AtscEpgRating;->fantasyViolence:B

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;->stRating:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$AtscEpgRating;

    iput-byte v1, v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$AtscEpgRating;->mpaaRatingD2:B

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;->stRating:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$AtscEpgRating;

    iput-byte v1, v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$AtscEpgRating;->tvRatingForChild:B

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;->stRating:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$AtscEpgRating;

    iput-byte v1, v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$AtscEpgRating;->caEngFlag:B

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;->stRating:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$AtscEpgRating;

    iput-byte v1, v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$AtscEpgRating;->caFreFlag:B

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;->stRating:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$AtscEpgRating;

    iput-byte v1, v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$AtscEpgRating;->caEngRatingD0:B

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;->stRating:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$AtscEpgRating;

    iput-byte v1, v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$AtscEpgRating;->caFreRatingD1:B

    const-string v0, ""

    iput-object v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;->sExtendedText:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3
    .param p1    # Landroid/os/Parcel;

    const/4 v0, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v1, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$AtscEpgRating;

    invoke-direct {v1, p0}, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$AtscEpgRating;-><init>(Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;)V

    iput-object v1, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;->stRating:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$AtscEpgRating;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;->startTime:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;->endTime:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;->durationTime:I

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;->values()[Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;

    move-result-object v1

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    invoke-static {v2}, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;->getOrdinalThroughValue(I)I

    move-result v2

    aget-object v1, v1, v2

    iput-object v1, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;->enStrStatus:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;->sName:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;->bHasCCInfo:Z

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;->stRating:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$AtscEpgRating;

    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v1

    iput-byte v1, v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$AtscEpgRating;->ratingRxIsOK:B

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;->stRating:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$AtscEpgRating;

    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v1

    iput-byte v1, v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$AtscEpgRating;->mpaaFlag:B

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;->stRating:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$AtscEpgRating;

    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v1

    iput-byte v1, v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$AtscEpgRating;->tvRatingForEntire:B

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;->stRating:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$AtscEpgRating;

    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v1

    iput-byte v1, v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$AtscEpgRating;->dialog:B

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;->stRating:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$AtscEpgRating;

    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v1

    iput-byte v1, v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$AtscEpgRating;->language:B

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;->stRating:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$AtscEpgRating;

    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v1

    iput-byte v1, v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$AtscEpgRating;->sexualContent:B

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;->stRating:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$AtscEpgRating;

    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v1

    iput-byte v1, v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$AtscEpgRating;->violence:B

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;->stRating:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$AtscEpgRating;

    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v1

    iput-byte v1, v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$AtscEpgRating;->fantasyViolence:B

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;->stRating:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$AtscEpgRating;

    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v1

    iput-byte v1, v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$AtscEpgRating;->mpaaRatingD2:B

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;->stRating:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$AtscEpgRating;

    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v1

    iput-byte v1, v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$AtscEpgRating;->tvRatingForChild:B

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;->stRating:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$AtscEpgRating;

    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v1

    iput-byte v1, v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$AtscEpgRating;->caEngFlag:B

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;->stRating:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$AtscEpgRating;

    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v1

    iput-byte v1, v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$AtscEpgRating;->caFreFlag:B

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;->stRating:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$AtscEpgRating;

    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v1

    iput-byte v1, v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$AtscEpgRating;->caEngRatingD0:B

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;->stRating:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$AtscEpgRating;

    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v1

    iput-byte v1, v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$AtscEpgRating;->caFreRatingD1:B

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;->sExtendedText:Ljava/lang/String;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic access$000()Ljava/util/Hashtable;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;->enumhash:Ljava/util/Hashtable;

    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;->startTime:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;->endTime:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;->durationTime:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;->enStrStatus:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$EnumAtscEpgFunctionStatus;->getValue()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;->sName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;->bHasCCInfo:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;->stRating:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$AtscEpgRating;

    iget-byte v0, v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$AtscEpgRating;->ratingRxIsOK:B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;->stRating:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$AtscEpgRating;

    iget-byte v0, v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$AtscEpgRating;->mpaaFlag:B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;->stRating:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$AtscEpgRating;

    iget-byte v0, v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$AtscEpgRating;->tvRatingForEntire:B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;->stRating:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$AtscEpgRating;

    iget-byte v0, v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$AtscEpgRating;->dialog:B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;->stRating:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$AtscEpgRating;

    iget-byte v0, v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$AtscEpgRating;->language:B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;->stRating:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$AtscEpgRating;

    iget-byte v0, v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$AtscEpgRating;->sexualContent:B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;->stRating:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$AtscEpgRating;

    iget-byte v0, v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$AtscEpgRating;->violence:B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;->stRating:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$AtscEpgRating;

    iget-byte v0, v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$AtscEpgRating;->fantasyViolence:B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;->stRating:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$AtscEpgRating;

    iget-byte v0, v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$AtscEpgRating;->mpaaRatingD2:B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;->stRating:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$AtscEpgRating;

    iget-byte v0, v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$AtscEpgRating;->tvRatingForChild:B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;->stRating:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$AtscEpgRating;

    iget-byte v0, v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$AtscEpgRating;->caEngFlag:B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;->stRating:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$AtscEpgRating;

    iget-byte v0, v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$AtscEpgRating;->caFreFlag:B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;->stRating:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$AtscEpgRating;

    iget-byte v0, v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$AtscEpgRating;->caEngRatingD0:B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;->stRating:Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$AtscEpgRating;

    iget-byte v0, v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo$AtscEpgRating;->caFreRatingD1:B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    iget-object v0, p0, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;->sExtendedText:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
