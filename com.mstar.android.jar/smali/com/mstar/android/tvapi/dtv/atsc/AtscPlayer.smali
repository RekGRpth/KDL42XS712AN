.class public interface abstract Lcom/mstar/android/tvapi/dtv/atsc/AtscPlayer;
.super Ljava/lang/Object;
.source "AtscPlayer.java"

# interfaces
.implements Lcom/mstar/android/tvapi/dtv/common/DtvPlayer;


# virtual methods
.method public abstract enterPassToUnlockByUser(Z)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract enterPassToUnlockUnratedByUser(Z)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract getCurrentRatingInformation()Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract getCurrentVChipBlockStatus()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract getRRTInformation()Lcom/mstar/android/tvapi/dtv/atsc/vo/Region5RatingInformation;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract setAntennaType(Lcom/mstar/android/tvapi/common/vo/EnumAntennaType;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract setAudioMute(Lcom/mstar/android/tvapi/dtv/atsc/vo/AudioMuteType$EnumAudioMuteType;Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract setCanadaEngGuideline(Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaEngRatingType;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract setCanadaFreGuideline(Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaFreRatingType;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract setDynamicGuideline(SSS)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract setUsaMpaaGuideline(Lcom/mstar/android/tvapi/dtv/atsc/vo/UsaMpaaRatingType$EnumUsaMpaaRatingType;Z)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract setUsaTvGuideline(Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumUsaTvRatingType;S)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method

.method public abstract setVChipGuideline(SSSS)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation
.end method
