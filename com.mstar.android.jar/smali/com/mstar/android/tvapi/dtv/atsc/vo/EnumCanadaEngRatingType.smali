.class public final enum Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaEngRatingType;
.super Ljava/lang/Enum;
.source "EnumCanadaEngRatingType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaEngRatingType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaEngRatingType;

.field public static final enum E_CANADA_ENG_RATING_14Plus:Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaEngRatingType;

.field public static final enum E_CANADA_ENG_RATING_18Plus:Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaEngRatingType;

.field public static final enum E_CANADA_ENG_RATING_C:Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaEngRatingType;

.field public static final enum E_CANADA_ENG_RATING_C8Plus:Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaEngRatingType;

.field public static final enum E_CANADA_ENG_RATING_EXEMPT:Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaEngRatingType;

.field public static final enum E_CANADA_ENG_RATING_G:Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaEngRatingType;

.field public static final enum E_CANADA_ENG_RATING_MAX_LEVEL:Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaEngRatingType;

.field public static final enum E_CANADA_ENG_RATING_PG:Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaEngRatingType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaEngRatingType;

    const-string v1, "E_CANADA_ENG_RATING_EXEMPT"

    invoke-direct {v0, v1, v3}, Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaEngRatingType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaEngRatingType;->E_CANADA_ENG_RATING_EXEMPT:Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaEngRatingType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaEngRatingType;

    const-string v1, "E_CANADA_ENG_RATING_C"

    invoke-direct {v0, v1, v4}, Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaEngRatingType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaEngRatingType;->E_CANADA_ENG_RATING_C:Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaEngRatingType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaEngRatingType;

    const-string v1, "E_CANADA_ENG_RATING_C8Plus"

    invoke-direct {v0, v1, v5}, Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaEngRatingType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaEngRatingType;->E_CANADA_ENG_RATING_C8Plus:Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaEngRatingType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaEngRatingType;

    const-string v1, "E_CANADA_ENG_RATING_G"

    invoke-direct {v0, v1, v6}, Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaEngRatingType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaEngRatingType;->E_CANADA_ENG_RATING_G:Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaEngRatingType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaEngRatingType;

    const-string v1, "E_CANADA_ENG_RATING_PG"

    invoke-direct {v0, v1, v7}, Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaEngRatingType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaEngRatingType;->E_CANADA_ENG_RATING_PG:Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaEngRatingType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaEngRatingType;

    const-string v1, "E_CANADA_ENG_RATING_14Plus"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaEngRatingType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaEngRatingType;->E_CANADA_ENG_RATING_14Plus:Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaEngRatingType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaEngRatingType;

    const-string v1, "E_CANADA_ENG_RATING_18Plus"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaEngRatingType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaEngRatingType;->E_CANADA_ENG_RATING_18Plus:Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaEngRatingType;

    new-instance v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaEngRatingType;

    const-string v1, "E_CANADA_ENG_RATING_MAX_LEVEL"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaEngRatingType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaEngRatingType;->E_CANADA_ENG_RATING_MAX_LEVEL:Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaEngRatingType;

    const/16 v0, 0x8

    new-array v0, v0, [Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaEngRatingType;

    sget-object v1, Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaEngRatingType;->E_CANADA_ENG_RATING_EXEMPT:Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaEngRatingType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaEngRatingType;->E_CANADA_ENG_RATING_C:Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaEngRatingType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaEngRatingType;->E_CANADA_ENG_RATING_C8Plus:Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaEngRatingType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaEngRatingType;->E_CANADA_ENG_RATING_G:Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaEngRatingType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaEngRatingType;->E_CANADA_ENG_RATING_PG:Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaEngRatingType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaEngRatingType;->E_CANADA_ENG_RATING_14Plus:Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaEngRatingType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaEngRatingType;->E_CANADA_ENG_RATING_18Plus:Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaEngRatingType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaEngRatingType;->E_CANADA_ENG_RATING_MAX_LEVEL:Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaEngRatingType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaEngRatingType;->$VALUES:[Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaEngRatingType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaEngRatingType;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaEngRatingType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaEngRatingType;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaEngRatingType;
    .locals 1

    sget-object v0, Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaEngRatingType;->$VALUES:[Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaEngRatingType;

    invoke-virtual {v0}, [Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaEngRatingType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaEngRatingType;

    return-object v0
.end method
