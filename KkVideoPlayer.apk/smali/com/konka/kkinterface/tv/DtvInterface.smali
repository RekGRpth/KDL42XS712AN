.class public interface abstract Lcom/konka/kkinterface/tv/DtvInterface;
.super Ljava/lang/Object;
.source "DtvInterface.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/kkinterface/tv/DtvInterface$AUDIO_MODE;,
        Lcom/konka/kkinterface/tv/DtvInterface$AUDIO_TYPE;,
        Lcom/konka/kkinterface/tv/DtvInterface$AUD_INFO;,
        Lcom/konka/kkinterface/tv/DtvInterface$DTV_EIT_INFO;,
        Lcom/konka/kkinterface/tv/DtvInterface$DTV_SCAN_EVENT;,
        Lcom/konka/kkinterface/tv/DtvInterface$DTV_TRIPLE_ID;,
        Lcom/konka/kkinterface/tv/DtvInterface$DvbcScanParam;,
        Lcom/konka/kkinterface/tv/DtvInterface$EIT_CURRENT_EVENT_PF;,
        Lcom/konka/kkinterface/tv/DtvInterface$EN_ANTENNA_TYPE;,
        Lcom/konka/kkinterface/tv/DtvInterface$EN_EPG_DESCRIPTION_TYPE;,
        Lcom/konka/kkinterface/tv/DtvInterface$EN_SCAN_TYPE;,
        Lcom/konka/kkinterface/tv/DtvInterface$EPG_CRID_EVENT_INFO;,
        Lcom/konka/kkinterface/tv/DtvInterface$EPG_CRID_STATUS;,
        Lcom/konka/kkinterface/tv/DtvInterface$EPG_EVENT_INFO;,
        Lcom/konka/kkinterface/tv/DtvInterface$EPG_EVENT_TIMER_INFO;,
        Lcom/konka/kkinterface/tv/DtvInterface$EPG_FIRST_MATCH_HDCAST;,
        Lcom/konka/kkinterface/tv/DtvInterface$EPG_HD_SIMULCAST;,
        Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;,
        Lcom/konka/kkinterface/tv/DtvInterface$EPG_TRAILER_LINK_INFO;,
        Lcom/konka/kkinterface/tv/DtvInterface$LANG_ISO639;,
        Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;,
        Lcom/konka/kkinterface/tv/DtvInterface$MW_DVB_PROGRAM_INFO;,
        Lcom/konka/kkinterface/tv/DtvInterface$NVOD_EVENT_INFO;,
        Lcom/konka/kkinterface/tv/DtvInterface$RESULT_ADDEVENT;,
        Lcom/konka/kkinterface/tv/DtvInterface$ST_DTV_SPECIFIC_PROGINFO;,
        Lcom/konka/kkinterface/tv/DtvInterface$ST_DVB_CHANNEL_ATTRIBUTE;,
        Lcom/konka/kkinterface/tv/DtvInterface$ST_DVB_PROGRAMINFO;,
        Lcom/konka/kkinterface/tv/DtvInterface$TIME;,
        Lcom/konka/kkinterface/tv/DtvInterface$VIDEO_TYPE;
    }
.end annotation


# static fields
.field public static final MAPI_SI_MAX_SERVICE_NAME:S = 0x32s

.field public static final MAX_AUD_LANG_NUM:S = 0x10s

.field public static final MAX_SERVICE_NAME:S = 0x32s

.field public static final dtvantentype:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "DTMB"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "DVB-C"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "DVB-T"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "MAX"

    aput-object v2, v0, v1

    sput-object v0, Lcom/konka/kkinterface/tv/DtvInterface;->dtvantentype:[Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public abstract DtvStopScan()Z
.end method

.method public abstract addProgramToFavorite(Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;ISI)V
.end method

.method public abstract deleteProgramFromFavorite(Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;ISI)V
.end method

.method public abstract dtvAutoScan()Z
.end method

.method public abstract dtvChangeManualScanRF(S)Z
.end method

.method public abstract dtvFullScan()Z
.end method

.method public abstract dtvGetAntennaType()Lcom/konka/kkinterface/tv/DtvInterface$EN_ANTENNA_TYPE;
.end method

.method public abstract dtvGetRFInfo(Lcom/mstar/android/tvapi/dtv/vo/RfInfo$EnumInfoType;I)Lcom/mstar/android/tvapi/dtv/vo/RfInfo;
.end method

.method public abstract dtvManualScanFreq(I)Z
.end method

.method public abstract dtvManualScanRF(S)Z
.end method

.method public abstract dtvPauseScan()Z
.end method

.method public abstract dtvResumeScan()Z
.end method

.method public abstract dtvSetAntennaType(Lcom/konka/kkinterface/tv/DtvInterface$EN_ANTENNA_TYPE;)V
.end method

.method public abstract dtvStartManualScan()Z
.end method

.method public abstract dtvplayCurrentProgram()Z
.end method

.method public abstract dvbcgetScanParam(Lcom/konka/kkinterface/tv/DtvInterface$DvbcScanParam;)Z
.end method

.method public abstract dvbcsetScanParam(SLcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;IIS)Z
.end method

.method public abstract getCurrentMuxInfo()Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbMuxInfo;
.end method

.method public abstract getCurrentProgramSpecificInfo(Lcom/konka/kkinterface/tv/DtvInterface$ST_DTV_SPECIFIC_PROGINFO;)Z
.end method

.method public abstract getMSrvDtvRoute()S
.end method

.method public abstract getProgramAttribute(Lcom/mstar/android/tvapi/common/vo/EnumProgramAttribute;ISIZ)Z
.end method

.method public abstract getProgramSpecificInfoByIndex(ILcom/konka/kkinterface/tv/DtvInterface$ST_DTV_SPECIFIC_PROGINFO;)Z
.end method

.method public abstract sendDtvScaninfo()V
.end method

.method public abstract setProgramAttribute(Lcom/mstar/android/tvapi/common/vo/EnumProgramAttribute;ISIZ)V
.end method

.method public abstract switchMSrvDtvRouteCmd(S)Z
.end method
