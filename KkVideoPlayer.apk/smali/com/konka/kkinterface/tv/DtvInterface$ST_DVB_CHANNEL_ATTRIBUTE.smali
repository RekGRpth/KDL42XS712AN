.class public Lcom/konka/kkinterface/tv/DtvInterface$ST_DVB_CHANNEL_ATTRIBUTE;
.super Ljava/lang/Object;
.source "DtvInterface.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/kkinterface/tv/DtvInterface;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ST_DVB_CHANNEL_ATTRIBUTE"
.end annotation


# instance fields
.field public u16SignalStrength:I

.field public u8Favorite:Z

.field public u8InvalidCell:B

.field public u8IsDataServiceAvailable:Z

.field public u8IsDelete:Z

.field public u8IsLock:Z

.field public u8IsMHEGIncluded:Z

.field public u8IsMove:Z

.field public u8IsReName:Z

.field public u8IsReplaceDel:Z

.field public u8IsScramble:Z

.field public u8IsServiceIdOnly:Z

.field public u8IsSkipped:Z

.field public u8IsSpecialSrv:Z

.field public u8IsStillPicture:Z

.field public u8NumericSelectionFlag:Z

.field public u8RealServiceType:B

.field public u8Region:B

.field public u8ServiceType:B

.field public u8ServiceTypePrio:B

.field public u8UnconfirmedService:Z

.field public u8VideoType:B

.field public u8VisibleServiceFlag:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
