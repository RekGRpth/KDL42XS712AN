.class public final Lcom/konka/kkvideoplayer/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/kkvideoplayer/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final BROADCAST_ACTION_CALIBRATION:I = 0x7f0a006a

.field public static final BROADCAST_ACTION_SERVER_STAT:I = 0x7f0a0066

.field public static final CALIBRATION:I = 0x7f0a006b

.field public static final CALIBRATION_CANCEL:I = 0x7f0a006c

.field public static final CALIBRATION_SUCCESS:I = 0x7f0a006d

.field public static final FORCE_CALIBRATION:I = 0x7f0a006e

.field public static final FORCE_MEDIA_PLAY_STOP:I = 0x7f0a0071

.field public static final FORCE_SERVER_SETTING:I = 0x7f0a006f

.field public static final FORCE_SYSTEM_SET:I = 0x7f0a0070

.field public static final IDENTIFY_FAIL:I = 0x7f0a0072

.field public static final RUNNING_FIRST_TIME:I = 0x7f0a0073

.field public static final SERVER_GAME:I = 0x7f0a0069

.field public static final SERVER_TYPE:I = 0x7f0a0067

.field public static final SERVER_VOD:I = 0x7f0a0068

.field public static final Your_File_Format_not_support:I = 0x7f0a0028

.field public static final app_name:I = 0x7f0a003c

.field public static final audiofile:I = 0x7f0a004d

.field public static final audiolist:I = 0x7f0a004e

.field public static final audiolrc:I = 0x7f0a0059

.field public static final audiolrcshow:I = 0x7f0a005a

.field public static final audionet:I = 0x7f0a004f

.field public static final audioorder:I = 0x7f0a0057

.field public static final audioplaymode:I = 0x7f0a0055

.field public static final audiorandom:I = 0x7f0a0058

.field public static final audioselectname:I = 0x7f0a005b

.field public static final audiosetting:I = 0x7f0a0054

.field public static final audiosingle:I = 0x7f0a0056

.field public static final best_player:I = 0x7f0a0001

.field public static final circle_mode_single:I = 0x7f0a0034

.field public static final circle_mode_sort:I = 0x7f0a0033

.field public static final circle_mode_total:I = 0x7f0a0035

.field public static final cycleTxt:I = 0x7f0a0014

.field public static final entry_crircle_mode:I = 0x7f0a0064

.field public static final entry_screen_mode:I = 0x7f0a0063

.field public static final entryenglish:I = 0x7f0a005e

.field public static final entrylanguage:I = 0x7f0a005d

.field public static final entrypassword:I = 0x7f0a0062

.field public static final entryserver:I = 0x7f0a0061

.field public static final entrysetting:I = 0x7f0a005c

.field public static final entrysimchinese:I = 0x7f0a005f

.field public static final entrytrachinese:I = 0x7f0a0060

.field public static final error:I = 0x7f0a0006

.field public static final exit:I = 0x7f0a0007

.field public static final exit_cancle:I = 0x7f0a0024

.field public static final exit_message:I = 0x7f0a0022

.field public static final exit_ok:I = 0x7f0a0023

.field public static final exit_title:I = 0x7f0a0021

.field public static final file:I = 0x7f0a003f

.field public static final game_prefix:I = 0x7f0a0074

.field public static final hint_konka_storage:I = 0x7f0a000b

.field public static final hint_konka_storage_nostorage:I = 0x7f0a000c

.field public static final hint_media_scan:I = 0x7f0a000d

.field public static final hint_mode_3d_inscreen_mode:I = 0x7f0a001e

.field public static final hint_mode_4k_inscreen_mode:I = 0x7f0a0020

.field public static final hint_mode_pip_inscreen_mode:I = 0x7f0a001f

.field public static final hint_next_button:I = 0x7f0a000a

.field public static final hint_pre_button:I = 0x7f0a0009

.field public static final htm:I = 0x7f0a0041

.field public static final html:I = 0x7f0a0040

.field public static final http:I = 0x7f0a003d

.field public static final image_mode_auto:I = 0x7f0a002c

.field public static final image_mode_soft:I = 0x7f0a002e

.field public static final image_mode_splendid:I = 0x7f0a002f

.field public static final image_mode_stand:I = 0x7f0a002d

.field public static final intent_from:I = 0x7f0a0000

.field public static final mediafile_buffering:I = 0x7f0a001d

.field public static final mediafile_loading:I = 0x7f0a000f

.field public static final net_check_disconnect:I = 0x7f0a001c

.field public static final nextTxt:I = 0x7f0a0011

.field public static final picfile:I = 0x7f0a0051

.field public static final picsetting:I = 0x7f0a0065

.field public static final playTxt:I = 0x7f0a0010

.field public static final preTxt:I = 0x7f0a001a

.field public static final repeatTxt:I = 0x7f0a0013

.field public static final rtsp:I = 0x7f0a003e

.field public static final screen_mode_auto:I = 0x7f0a0030

.field public static final screen_mode_full:I = 0x7f0a0032

.field public static final screen_mode_orig:I = 0x7f0a0031

.field public static final seek_back:I = 0x7f0a0026

.field public static final seek_fast:I = 0x7f0a0025

.field public static final seekdisable:I = 0x7f0a001b

.field public static final settingTxt:I = 0x7f0a0015

.field public static final setting_circle_mode:I = 0x7f0a003a

.field public static final setting_image_mode:I = 0x7f0a0039

.field public static final setting_screen_mode:I = 0x7f0a0038

.field public static final setting_senior_mode:I = 0x7f0a0036

.field public static final setting_subtitle:I = 0x7f0a003b

.field public static final setting_track:I = 0x7f0a0037

.field public static final smifile:I = 0x7f0a0053

.field public static final smilfile:I = 0x7f0a0052

.field public static final stopTxt:I = 0x7f0a0012

.field public static final str_sys_language:I = 0x7f0a002b

.field public static final subtitleoff:I = 0x7f0a0027

.field public static final system_calibration:I = 0x7f0a0042

.field public static final textfile:I = 0x7f0a004b

.field public static final texttag:I = 0x7f0a0050

.field public static final usb_explore:I = 0x7f0a000e

.field public static final video_media_error_format_unsupport:I = 0x7f0a0003

.field public static final video_play_again:I = 0x7f0a0004

.field public static final video_play_error:I = 0x7f0a0002

.field public static final video_play_error_unsupport:I = 0x7f0a0005

.field public static final video_title:I = 0x7f0a0008

.field public static final videofile:I = 0x7f0a0043

.field public static final videosetting:I = 0x7f0a0044

.field public static final videosetting_pos1:I = 0x7f0a0046

.field public static final videosetting_pos2:I = 0x7f0a0048

.field public static final videosetting_pos3:I = 0x7f0a004a

.field public static final videosetting_url1:I = 0x7f0a0045

.field public static final videosetting_url2:I = 0x7f0a0047

.field public static final videosetting_url3:I = 0x7f0a0049

.field public static final voiceDown:I = 0x7f0a0029

.field public static final voiceDownTxt:I = 0x7f0a0018

.field public static final voiceNoTxt:I = 0x7f0a0017

.field public static final voiceTxt:I = 0x7f0a0016

.field public static final voiceUp:I = 0x7f0a002a

.field public static final voiceUpTxt:I = 0x7f0a0019

.field public static final webfile:I = 0x7f0a004c


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
