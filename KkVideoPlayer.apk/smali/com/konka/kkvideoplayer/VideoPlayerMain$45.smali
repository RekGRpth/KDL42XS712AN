.class Lcom/konka/kkvideoplayer/VideoPlayerMain$45;
.super Landroid/content/BroadcastReceiver;
.source "VideoPlayerMain.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/kkvideoplayer/VideoPlayerMain;->initReceiver()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;


# direct methods
.method constructor <init>(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$45;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v3, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "android.intent.action.MEDIA_MOUNTED"

    invoke-static {v2}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$45;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->myHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$47(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Landroid/os/Handler;

    move-result-object v2

    const/16 v3, 0x12c

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v3, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$45;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->myHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$47(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Landroid/os/Handler;

    move-result-object v2

    const/16 v3, 0x12d

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    invoke-virtual {p2}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x7

    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "umount:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$45;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->currPlay:Lcom/konka/kkvideoplayer/CurrPlayInfo;
    invoke-static {v2}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$2(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Lcom/konka/kkvideoplayer/CurrPlayInfo;

    move-result-object v2

    iget-object v2, v2, Lcom/konka/kkvideoplayer/CurrPlayInfo;->url:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "VIDEOPLAYER EJECT !"

    invoke-static {v2}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$45;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->vv:Lcom/konka/kkvideoplayer/VideoView;
    invoke-static {v2}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$1(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Lcom/konka/kkvideoplayer/VideoView;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$45;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->vv:Lcom/konka/kkvideoplayer/VideoView;
    invoke-static {v2}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$1(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Lcom/konka/kkvideoplayer/VideoView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/konka/kkvideoplayer/VideoView;->stopPlayback()V

    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$45;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    invoke-virtual {v2}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->finish()V

    goto :goto_0
.end method
