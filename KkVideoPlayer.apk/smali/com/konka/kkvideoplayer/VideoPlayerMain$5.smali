.class Lcom/konka/kkvideoplayer/VideoPlayerMain$5;
.super Ljava/lang/Object;
.source "VideoPlayerMain.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/kkvideoplayer/VideoPlayerMain;->initWidget()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;


# direct methods
.method constructor <init>(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$5;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1    # Landroid/view/View;

    const/4 v6, 0x0

    const-string v4, "jump to tv!!!!"

    invoke-static {v4}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$5;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->pipSkin:Lcom/mstar/android/tv/TvPipPopManager;
    invoke-static {v4}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$44(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tv/TvPipPopManager;->isDualViewEnabled()Z

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tv/TvPipPopManager;->getSubInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v2

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v4

    invoke-virtual {v4, v6}, Lcom/mstar/android/tv/TvPipPopManager;->getSubWindowSourceList(Z)[I

    move-result-object v3

    const/4 v0, 0x0

    const/4 v0, 0x0

    :goto_0
    array-length v4, v3

    if-lt v0, v4, :cond_3

    :cond_0
    array-length v4, v3

    if-lt v0, v4, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v4

    aget v5, v3, v6

    aget-object v2, v4, v5

    :cond_1
    iget-object v4, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$5;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    invoke-static {v4}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v4

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getDataBaseManagerInstance()Lcom/konka/kkinterface/tv/DataBaseDesk;

    move-result-object v4

    invoke-interface {v4, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk;->saveInputSource(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Z

    iget-object v4, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$5;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->pipSkin:Lcom/mstar/android/tv/TvPipPopManager;
    invoke-static {v4}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$44(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tv/TvPipPopManager;->isPopEnabled()Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$5;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->pipSkin:Lcom/mstar/android/tv/TvPipPopManager;
    invoke-static {v4}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$44(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tv/TvPipPopManager;->disablePop()Z

    new-instance v1, Landroid/content/Intent;

    const-string v4, "com.konka.hotkey.disablePop"

    invoke-direct {v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$5;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    invoke-virtual {v4, v1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->sendBroadcast(Landroid/content/Intent;)V

    iget-object v4, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$5;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->pipSkin:Lcom/mstar/android/tv/TvPipPopManager;
    invoke-static {v4}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$44(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v4

    invoke-virtual {v4, v6}, Lcom/mstar/android/tv/TvPipPopManager;->setPopOnFlag(Z)Z

    :cond_2
    new-instance v1, Landroid/content/Intent;

    const-string v4, "com.konka.GO_TO_TV"

    invoke-direct {v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$5;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    invoke-virtual {v4, v1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->sendBroadcast(Landroid/content/Intent;)V

    return-void

    :cond_3
    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v4

    aget v5, v3, v0

    if-eq v4, v5, :cond_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
