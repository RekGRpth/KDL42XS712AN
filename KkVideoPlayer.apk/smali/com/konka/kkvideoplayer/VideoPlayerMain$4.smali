.class Lcom/konka/kkvideoplayer/VideoPlayerMain$4;
.super Landroid/os/Handler;
.source "VideoPlayerMain.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/kkvideoplayer/VideoPlayerMain;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;


# direct methods
.method constructor <init>(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$4;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1    # Landroid/os/Message;

    const/4 v4, 0x1

    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    iget v1, p1, Landroid/os/Message;->what:I

    if-ne v1, v4, :cond_0

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$4;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->vv:Lcom/konka/kkvideoplayer/VideoView;
    invoke-static {v1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$1(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Lcom/konka/kkvideoplayer/VideoView;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$4;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->vv:Lcom/konka/kkvideoplayer/VideoView;
    invoke-static {v1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$1(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Lcom/konka/kkvideoplayer/VideoView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/konka/kkvideoplayer/VideoView;->getCurrentPosition()I

    move-result v0

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$4;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->seekBar:Landroid/widget/SeekBar;
    invoke-static {v1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$23(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Landroid/widget/SeekBar;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/SeekBar;->setProgress(I)V

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$4;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->vv:Lcom/konka/kkvideoplayer/VideoView;
    invoke-static {v1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$1(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Lcom/konka/kkvideoplayer/VideoView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/konka/kkvideoplayer/VideoView;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "EEEEEEEEEE SEEKHANDLER"

    invoke-static {v1}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$4;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->mCurrentTime:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$33(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Landroid/widget/TextView;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$4;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->seekHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$34(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Landroid/os/Handler;

    move-result-object v1

    const-wide/16 v2, 0x1f4

    invoke-virtual {v1, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    :cond_0
    return-void
.end method
