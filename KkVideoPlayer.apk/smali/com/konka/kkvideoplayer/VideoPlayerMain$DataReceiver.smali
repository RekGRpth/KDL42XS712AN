.class Lcom/konka/kkvideoplayer/VideoPlayerMain$DataReceiver;
.super Landroid/content/BroadcastReceiver;
.source "VideoPlayerMain.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/kkvideoplayer/VideoPlayerMain;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DataReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;


# direct methods
.method private constructor <init>(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$DataReceiver;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/konka/kkvideoplayer/VideoPlayerMain;Lcom/konka/kkvideoplayer/VideoPlayerMain$DataReceiver;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/kkvideoplayer/VideoPlayerMain$DataReceiver;-><init>(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const/4 v4, 0x0

    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v3, "mediastop"

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    const-string v3, "videofile"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-nez v1, :cond_0

    const-string v3, "MULTISCREEN START URL=========="

    invoke-static {v3}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$DataReceiver;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->progressDialog:Landroid/app/ProgressDialog;
    invoke-static {v3}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$36(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Landroid/app/ProgressDialog;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/ProgressDialog;->show()V

    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$DataReceiver;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # invokes: Lcom/konka/kkvideoplayer/VideoPlayerMain;->playMediaSource(Ljava/lang/String;I)V
    invoke-static {v3, v2, v4}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$37(Lcom/konka/kkvideoplayer/VideoPlayerMain;Ljava/lang/String;I)V

    :goto_0
    return-void

    :cond_0
    const-string v3, "MULTISCREEN finish &&&&&&&&&&&&&&&&"

    invoke-static {v3}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$DataReceiver;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    invoke-virtual {v3}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->finish()V

    goto :goto_0
.end method
