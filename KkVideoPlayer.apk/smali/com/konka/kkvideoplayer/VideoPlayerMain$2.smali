.class Lcom/konka/kkvideoplayer/VideoPlayerMain$2;
.super Ljava/lang/Object;
.source "VideoPlayerMain.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/kkvideoplayer/VideoPlayerMain;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;


# direct methods
.method constructor <init>(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$2;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 3
    .param p1    # Landroid/view/View;
    .param p2    # Z

    const-string v0, "onFocusChange!!!!!!!!!!!"

    invoke-static {v0}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$2;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # invokes: Lcom/konka/kkvideoplayer/VideoPlayerMain;->setMenuHideTimer()V
    invoke-static {v0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$0(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    :goto_0
    return-void

    :sswitch_0
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "hasFocus: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$2;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$2;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->preTxt:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$14(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Landroid/widget/TextView;

    move-result-object v1

    # invokes: Lcom/konka/kkvideoplayer/VideoPlayerMain;->setTextViewVisible(Landroid/widget/TextView;Z)V
    invoke-static {v0, v1, p2}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$15(Lcom/konka/kkvideoplayer/VideoPlayerMain;Landroid/widget/TextView;Z)V

    goto :goto_0

    :sswitch_1
    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$2;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$2;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->playPauseTxt:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$16(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Landroid/widget/TextView;

    move-result-object v1

    # invokes: Lcom/konka/kkvideoplayer/VideoPlayerMain;->setTextViewVisible(Landroid/widget/TextView;Z)V
    invoke-static {v0, v1, p2}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$15(Lcom/konka/kkvideoplayer/VideoPlayerMain;Landroid/widget/TextView;Z)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$2;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$2;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->nextTxt:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$17(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Landroid/widget/TextView;

    move-result-object v1

    # invokes: Lcom/konka/kkvideoplayer/VideoPlayerMain;->setTextViewVisible(Landroid/widget/TextView;Z)V
    invoke-static {v0, v1, p2}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$15(Lcom/konka/kkvideoplayer/VideoPlayerMain;Landroid/widget/TextView;Z)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$2;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$2;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->stopTxt:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$18(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Landroid/widget/TextView;

    move-result-object v1

    # invokes: Lcom/konka/kkvideoplayer/VideoPlayerMain;->setTextViewVisible(Landroid/widget/TextView;Z)V
    invoke-static {v0, v1, p2}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$15(Lcom/konka/kkvideoplayer/VideoPlayerMain;Landroid/widget/TextView;Z)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$2;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$2;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->settingTxt:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$19(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Landroid/widget/TextView;

    move-result-object v1

    # invokes: Lcom/konka/kkvideoplayer/VideoPlayerMain;->setTextViewVisible(Landroid/widget/TextView;Z)V
    invoke-static {v0, v1, p2}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$15(Lcom/konka/kkvideoplayer/VideoPlayerMain;Landroid/widget/TextView;Z)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x7f090010 -> :sswitch_0    # com.konka.kkvideoplayer.R.id.button_prev
        0x7f090014 -> :sswitch_1    # com.konka.kkvideoplayer.R.id.button_play_pause
        0x7f090016 -> :sswitch_2    # com.konka.kkvideoplayer.R.id.button_next
        0x7f090018 -> :sswitch_3    # com.konka.kkvideoplayer.R.id.button_stop
        0x7f09001b -> :sswitch_4    # com.konka.kkvideoplayer.R.id.button_setting
    .end sparse-switch
.end method
