.class Lcom/konka/kkvideoplayer/VideoPlayerMain$12;
.super Ljava/lang/Object;
.source "VideoPlayerMain.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnErrorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/kkvideoplayer/VideoPlayerMain;->initVideoPlay()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;


# direct methods
.method constructor <init>(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$12;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Landroid/media/MediaPlayer;II)Z
    .locals 2
    .param p1    # Landroid/media/MediaPlayer;
    .param p2    # I
    .param p3    # I

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$12;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->progressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$36(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->hide()V

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$12;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->vv:Lcom/konka/kkvideoplayer/VideoView;
    invoke-static {v0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$1(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Lcom/konka/kkvideoplayer/VideoView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/kkvideoplayer/VideoView;->stopPlayback()V

    const/16 v0, -0x138b

    if-ne p3, v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tv/TvPipPopManager;->isPipModeEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$12;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    const v1, 0x7f0a0003    # com.konka.kkvideoplayer.R.string.video_media_error_format_unsupport

    # invokes: Lcom/konka/kkvideoplayer/VideoPlayerMain;->buildHintDialog(I)V
    invoke-static {v0, v1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$60(Lcom/konka/kkvideoplayer/VideoPlayerMain;I)V

    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "onError ID="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    sparse-switch p3, :sswitch_data_0

    :sswitch_0
    const/4 v0, 0x0

    return v0

    :cond_0
    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$12;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    const v1, 0x7f0a0002    # com.konka.kkvideoplayer.R.string.video_play_error

    # invokes: Lcom/konka/kkvideoplayer/VideoPlayerMain;->buildHintDialog(I)V
    invoke-static {v0, v1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$60(Lcom/konka/kkvideoplayer/VideoPlayerMain;I)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x138b -> :sswitch_0
        -0x138a -> :sswitch_0
        -0x1389 -> :sswitch_0
        -0x7d1 -> :sswitch_0
        -0x3f2 -> :sswitch_0
        -0x3ef -> :sswitch_0
        -0x3ec -> :sswitch_0
        -0x3e9 -> :sswitch_0
    .end sparse-switch
.end method
