.class public final enum Lcom/konka/kkvideoplayer/KonkaMediaDataType;
.super Ljava/lang/Enum;
.source "KonkaMediaDataType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/konka/kkvideoplayer/KonkaMediaDataType;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum DIRECTORY_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

.field private static final synthetic ENUM$VALUES:[Lcom/konka/kkvideoplayer/KonkaMediaDataType;

.field public static final enum UNKOWN_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

.field public static final enum VIDEO_3GP_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

.field public static final enum VIDEO_AVI_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

.field public static final enum VIDEO_DAT_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

.field public static final enum VIDEO_DIVX_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

.field public static final enum VIDEO_FLV_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

.field public static final enum VIDEO_M2TS_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

.field public static final enum VIDEO_MKV_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

.field public static final enum VIDEO_MOV_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

.field public static final enum VIDEO_MP4_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

.field public static final enum VIDEO_MPEG_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

.field public static final enum VIDEO_MPG_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

.field public static final enum VIDEO_MVC_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

.field public static final enum VIDEO_RM_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

.field public static final enum VIDEO_RTSP_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

.field public static final enum VIDEO_SMIL_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

.field public static final enum VIDEO_TP_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

.field public static final enum VIDEO_TRP_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

.field public static final enum VIDEO_TS_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

.field public static final enum VIDEO_VOB_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

.field public static final enum VIDEO_WEBM_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

.field public static final enum VIDEO_WMV_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

.field public static test:Z

.field private static typeMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/konka/kkvideoplayer/KonkaMediaDataType;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    new-instance v0, Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    const-string v1, "DIRECTORY_FILE"

    invoke-direct {v0, v1, v4}, Lcom/konka/kkvideoplayer/KonkaMediaDataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->DIRECTORY_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    new-instance v0, Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    const-string v1, "VIDEO_3GP_FILE"

    invoke-direct {v0, v1, v3}, Lcom/konka/kkvideoplayer/KonkaMediaDataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->VIDEO_3GP_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    new-instance v0, Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    const-string v1, "VIDEO_WMV_FILE"

    invoke-direct {v0, v1, v5}, Lcom/konka/kkvideoplayer/KonkaMediaDataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->VIDEO_WMV_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    new-instance v0, Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    const-string v1, "VIDEO_MP4_FILE"

    invoke-direct {v0, v1, v6}, Lcom/konka/kkvideoplayer/KonkaMediaDataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->VIDEO_MP4_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    new-instance v0, Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    const-string v1, "VIDEO_MPG_FILE"

    invoke-direct {v0, v1, v7}, Lcom/konka/kkvideoplayer/KonkaMediaDataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->VIDEO_MPG_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    new-instance v0, Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    const-string v1, "VIDEO_TS_FILE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/konka/kkvideoplayer/KonkaMediaDataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->VIDEO_TS_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    new-instance v0, Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    const-string v1, "VIDEO_RTSP_FILE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/konka/kkvideoplayer/KonkaMediaDataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->VIDEO_RTSP_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    new-instance v0, Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    const-string v1, "VIDEO_SMIL_FILE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/konka/kkvideoplayer/KonkaMediaDataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->VIDEO_SMIL_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    new-instance v0, Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    const-string v1, "VIDEO_RM_FILE"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/konka/kkvideoplayer/KonkaMediaDataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->VIDEO_RM_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    new-instance v0, Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    const-string v1, "VIDEO_VOB_FILE"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/konka/kkvideoplayer/KonkaMediaDataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->VIDEO_VOB_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    new-instance v0, Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    const-string v1, "VIDEO_DAT_FILE"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/konka/kkvideoplayer/KonkaMediaDataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->VIDEO_DAT_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    new-instance v0, Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    const-string v1, "VIDEO_AVI_FILE"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/konka/kkvideoplayer/KonkaMediaDataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->VIDEO_AVI_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    new-instance v0, Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    const-string v1, "VIDEO_MOV_FILE"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/konka/kkvideoplayer/KonkaMediaDataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->VIDEO_MOV_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    new-instance v0, Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    const-string v1, "VIDEO_MKV_FILE"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/konka/kkvideoplayer/KonkaMediaDataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->VIDEO_MKV_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    new-instance v0, Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    const-string v1, "VIDEO_MPEG_FILE"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/konka/kkvideoplayer/KonkaMediaDataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->VIDEO_MPEG_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    new-instance v0, Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    const-string v1, "VIDEO_TP_FILE"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/konka/kkvideoplayer/KonkaMediaDataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->VIDEO_TP_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    new-instance v0, Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    const-string v1, "VIDEO_M2TS_FILE"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/konka/kkvideoplayer/KonkaMediaDataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->VIDEO_M2TS_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    new-instance v0, Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    const-string v1, "VIDEO_WEBM_FILE"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/konka/kkvideoplayer/KonkaMediaDataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->VIDEO_WEBM_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    new-instance v0, Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    const-string v1, "VIDEO_DIVX_FILE"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/konka/kkvideoplayer/KonkaMediaDataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->VIDEO_DIVX_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    new-instance v0, Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    const-string v1, "VIDEO_FLV_FILE"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/konka/kkvideoplayer/KonkaMediaDataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->VIDEO_FLV_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    new-instance v0, Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    const-string v1, "VIDEO_TRP_FILE"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/konka/kkvideoplayer/KonkaMediaDataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->VIDEO_TRP_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    new-instance v0, Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    const-string v1, "VIDEO_MVC_FILE"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lcom/konka/kkvideoplayer/KonkaMediaDataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->VIDEO_MVC_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    new-instance v0, Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    const-string v1, "UNKOWN_FILE"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lcom/konka/kkvideoplayer/KonkaMediaDataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->UNKOWN_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    const/16 v0, 0x17

    new-array v0, v0, [Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    sget-object v1, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->DIRECTORY_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->VIDEO_3GP_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->VIDEO_WMV_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->VIDEO_MP4_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->VIDEO_MPG_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->VIDEO_TS_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->VIDEO_RTSP_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->VIDEO_SMIL_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->VIDEO_RM_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->VIDEO_VOB_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->VIDEO_DAT_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->VIDEO_AVI_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->VIDEO_MOV_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->VIDEO_MKV_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->VIDEO_MPEG_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->VIDEO_TP_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->VIDEO_M2TS_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->VIDEO_WEBM_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->VIDEO_DIVX_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->VIDEO_FLV_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->VIDEO_TRP_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->VIDEO_MVC_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->UNKOWN_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->ENUM$VALUES:[Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    sput-boolean v3, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->test:Z

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->typeMap:Ljava/util/Map;

    sget-object v0, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->typeMap:Ljava/util/Map;

    const-string v1, ".3gp"

    sget-object v2, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->VIDEO_3GP_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->typeMap:Ljava/util/Map;

    const-string v1, ".wmv"

    sget-object v2, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->VIDEO_WMV_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->typeMap:Ljava/util/Map;

    const-string v1, ".mp4"

    sget-object v2, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->VIDEO_MP4_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->typeMap:Ljava/util/Map;

    const-string v1, ".mpg"

    sget-object v2, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->VIDEO_MPG_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->typeMap:Ljava/util/Map;

    const-string v1, ".ts"

    sget-object v2, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->VIDEO_TS_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->typeMap:Ljava/util/Map;

    const-string v1, ".rm"

    sget-object v2, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->VIDEO_RM_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->typeMap:Ljava/util/Map;

    const-string v1, ".rmvb"

    sget-object v2, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->VIDEO_RM_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->typeMap:Ljava/util/Map;

    const-string v1, ".vob"

    sget-object v2, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->VIDEO_VOB_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->typeMap:Ljava/util/Map;

    const-string v1, ".dat"

    sget-object v2, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->VIDEO_DAT_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->typeMap:Ljava/util/Map;

    const-string v1, ".avi"

    sget-object v2, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->VIDEO_AVI_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->typeMap:Ljava/util/Map;

    const-string v1, ".mov"

    sget-object v2, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->VIDEO_MOV_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->typeMap:Ljava/util/Map;

    const-string v1, ".mpeg"

    sget-object v2, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->VIDEO_MPEG_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->typeMap:Ljava/util/Map;

    const-string v1, ".tp"

    sget-object v2, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->VIDEO_TP_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->typeMap:Ljava/util/Map;

    const-string v1, ".m2ts"

    sget-object v2, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->VIDEO_M2TS_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->typeMap:Ljava/util/Map;

    const-string v1, ".mkv"

    sget-object v2, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->VIDEO_MKV_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->typeMap:Ljava/util/Map;

    const-string v1, ".WebM"

    sget-object v2, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->VIDEO_WEBM_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->typeMap:Ljava/util/Map;

    const-string v1, ".Divx"

    sget-object v2, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->VIDEO_DIVX_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->typeMap:Ljava/util/Map;

    const-string v1, ".flv"

    sget-object v2, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->VIDEO_FLV_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->typeMap:Ljava/util/Map;

    const-string v1, ".trp"

    sget-object v2, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->VIDEO_TRP_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->typeMap:Ljava/util/Map;

    const-string v1, ".mvc"

    sget-object v2, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->VIDEO_MVC_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static getFileType(Ljava/lang/String;)Lcom/konka/kkvideoplayer/KonkaMediaDataType;
    .locals 5
    .param p0    # Ljava/lang/String;

    if-nez p0, :cond_1

    sget-object v2, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->UNKOWN_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    :cond_0
    :goto_0
    return-object v2

    :cond_1
    const-string v3, "."

    invoke-virtual {p0, v3}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    if-gez v0, :cond_2

    sget-object v2, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->UNKOWN_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    goto :goto_0

    :cond_2
    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->typeMap:Ljava/util/Map;

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    if-nez v2, :cond_0

    sget-object v2, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->UNKOWN_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/konka/kkvideoplayer/KonkaMediaDataType;
    .locals 1

    const-class v0, Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    return-object v0
.end method

.method public static values()[Lcom/konka/kkvideoplayer/KonkaMediaDataType;
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->ENUM$VALUES:[Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    array-length v1, v0

    new-array v2, v1, [Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
