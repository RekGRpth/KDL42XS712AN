.class Lcom/konka/kkvideoplayer/VideoPlayerMain$BroadCastGotoHome;
.super Landroid/content/BroadcastReceiver;
.source "VideoPlayerMain.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/kkvideoplayer/VideoPlayerMain;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "BroadCastGotoHome"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;


# direct methods
.method constructor <init>(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$BroadCastGotoHome;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.konka.tv.hotkey.service.STOPMM"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$BroadCastGotoHome;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->vv:Lcom/konka/kkvideoplayer/VideoView;
    invoke-static {v0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$1(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Lcom/konka/kkvideoplayer/VideoView;

    move-result-object v0

    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "onStop !!! free mplayer  time: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$BroadCastGotoHome;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->currPlay:Lcom/konka/kkvideoplayer/CurrPlayInfo;
    invoke-static {v1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$2(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Lcom/konka/kkvideoplayer/CurrPlayInfo;

    move-result-object v1

    iget-wide v1, v1, Lcom/konka/kkvideoplayer/CurrPlayInfo;->start:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$BroadCastGotoHome;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->hotKeyStat:I
    invoke-static {v0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$42(Lcom/konka/kkvideoplayer/VideoPlayerMain;)I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$BroadCastGotoHome;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    invoke-virtual {v0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->han_exit_pop()V

    :cond_0
    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$BroadCastGotoHome;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # invokes: Lcom/konka/kkvideoplayer/VideoPlayerMain;->hideMenuShow()V
    invoke-static {v0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$27(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$BroadCastGotoHome;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->seekBar:Landroid/widget/SeekBar;
    invoke-static {v0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$23(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Landroid/widget/SeekBar;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$BroadCastGotoHome;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->seekBar:Landroid/widget/SeekBar;
    invoke-static {v0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$23(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Landroid/widget/SeekBar;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    :cond_1
    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$BroadCastGotoHome;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->vv:Lcom/konka/kkvideoplayer/VideoView;
    invoke-static {v0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$1(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Lcom/konka/kkvideoplayer/VideoView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/kkvideoplayer/VideoView;->stopPlayback()V

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$BroadCastGotoHome;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    const-wide/16 v1, 0x0

    # invokes: Lcom/konka/kkvideoplayer/VideoPlayerMain;->saveHangUpPoint(J)V
    invoke-static {v0, v1, v2}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$43(Lcom/konka/kkvideoplayer/VideoPlayerMain;J)V

    :cond_2
    return-void
.end method
