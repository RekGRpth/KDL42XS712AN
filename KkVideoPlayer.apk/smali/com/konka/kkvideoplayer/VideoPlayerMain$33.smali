.class Lcom/konka/kkvideoplayer/VideoPlayerMain$33;
.super Ljava/lang/Object;
.source "VideoPlayerMain.java"

# interfaces
.implements Landroid/os/MessageQueue$IdleHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/kkvideoplayer/VideoPlayerMain;->showSettingWindow(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;


# direct methods
.method constructor <init>(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$33;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public queueIdle()Z
    .locals 7

    const/16 v6, 0x82

    const/16 v4, 0x7b

    const v3, 0x7f09001d    # com.konka.kkvideoplayer.R.id.id_player_main

    const/16 v5, 0x50

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$33;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->setting_window:Landroid/widget/PopupWindow;
    invoke-static {v1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$91(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Landroid/widget/PopupWindow;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/PopupWindow;->setFocusable(Z)V

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$33;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    invoke-virtual {v1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$33;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->setting_window:Landroid/widget/PopupWindow;
    invoke-static {v1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$91(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Landroid/widget/PopupWindow;

    move-result-object v1

    if-eqz v1, :cond_0

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->screenWidth:I
    invoke-static {}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$92()I

    move-result v1

    const/16 v2, 0x500

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$33;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->setting_window:Landroid/widget/PopupWindow;
    invoke-static {v1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$91(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Landroid/widget/PopupWindow;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$33;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    invoke-virtual {v2, v3}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v1, v2, v5, v6, v4}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    :goto_0
    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$33;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->setting_window:Landroid/widget/PopupWindow;
    invoke-static {v1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$91(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Landroid/widget/PopupWindow;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/PopupWindow;->update()V

    :cond_0
    const/4 v1, 0x0

    return v1

    :cond_1
    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$33;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    invoke-virtual {v1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/konka/kkinterface/tv/CommonDesk;->is1280x720(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$33;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->setting_window:Landroid/widget/PopupWindow;
    invoke-static {v1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$91(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Landroid/widget/PopupWindow;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$33;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    invoke-virtual {v2, v3}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v1, v2, v5, v6, v4}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$33;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->setting_window:Landroid/widget/PopupWindow;
    invoke-static {v1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$91(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Landroid/widget/PopupWindow;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$33;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    invoke-virtual {v2, v3}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const/16 v3, 0xc8

    const/16 v4, 0xbd

    invoke-virtual {v1, v2, v5, v3, v4}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    goto :goto_0
.end method
