.class public Lcom/konka/kkvideoplayer/util/SwitchSource;
.super Landroid/app/Activity;
.source "SwitchSource.java"


# static fields
.field static tvManagerProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/konka/kkvideoplayer/util/SwitchSource;->tvManagerProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    sget-object v0, Lcom/konka/kkvideoplayer/util/SwitchSource;->tvManagerProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v0

    sput-object v0, Lcom/konka/kkvideoplayer/util/SwitchSource;->tvManagerProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    :cond_0
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    sget-object v0, Lcom/konka/kkvideoplayer/util/SwitchSource;->tvManagerProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    sput-object v0, Lcom/konka/kkvideoplayer/util/SwitchSource;->tvManagerProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 3

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    const-string v0, "tank"

    const-string v1, "switch source !!!!!!!!!!!!!!!! tank"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/konka/kkvideoplayer/util/SwitchSource;->tvManagerProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v0

    sput-object v0, Lcom/konka/kkvideoplayer/util/SwitchSource;->tvManagerProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    :cond_0
    sget-object v0, Lcom/konka/kkvideoplayer/util/SwitchSource;->tvManagerProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/konka/kkvideoplayer/util/SwitchSource;->tvManagerProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_STORAGE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-eq v0, v1, :cond_1

    sget-object v0, Lcom/konka/kkvideoplayer/util/SwitchSource;->tvManagerProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_KTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-eq v0, v1, :cond_1

    const-string v0, "-------switchsourece.java-------"

    const-string v1, "------will switchsourece----------"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/konka/kkvideoplayer/util/SwitchSource;->tvManagerProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_STORAGE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/konka/kkinterface/tv/CommonDesk;->SetInputSource(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;Z)V

    :cond_1
    return-void
.end method
