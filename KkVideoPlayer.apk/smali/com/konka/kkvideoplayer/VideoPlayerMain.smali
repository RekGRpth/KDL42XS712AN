.class public Lcom/konka/kkvideoplayer/VideoPlayerMain;
.super Lcom/konka/kkvideoplayer/util/SwitchSource;
.source "VideoPlayerMain.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/kkvideoplayer/VideoPlayerMain$BroadCast3D;,
        Lcom/konka/kkvideoplayer/VideoPlayerMain$BroadCastGotoHome;,
        Lcom/konka/kkvideoplayer/VideoPlayerMain$BroadCastGotoTv;,
        Lcom/konka/kkvideoplayer/VideoPlayerMain$BroadCastPop;,
        Lcom/konka/kkvideoplayer/VideoPlayerMain$CloudyHealthReceiver;,
        Lcom/konka/kkvideoplayer/VideoPlayerMain$CloudyMsgReceiver;,
        Lcom/konka/kkvideoplayer/VideoPlayerMain$DataReceiver;,
        Lcom/konka/kkvideoplayer/VideoPlayerMain$EthStateReceiver;,
        Lcom/konka/kkvideoplayer/VideoPlayerMain$SavePoint;
    }
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$konka$kkinterface$tv$DataBaseDesk$EN_MS_PICTURE:[I = null

.field private static final CIRCLE_FLAG_SINGLE:I = 0x1

.field private static final CIRCLE_FLAG_SORT:I = 0x0

.field private static final CIRCLE_FLAG_TOTAL:I = 0x2

.field private static final CLOUDYHEALTH_MESG:Ljava/lang/String; = "com.konka.service.switchinputsourcemsg"

.field private static final CMD_DMYHANDLER_DOUBLE_CHANNEL_INIT:I = 0xe

.field private static final CMD_DMYHANDLER_SEEK_HINT:I = 0xf

.field private static final CMD_MYHANDLER_DRAW_MEHU:I = 0x7

.field private static final CMD_MYHANDLER_DRAW_MENUPOP:I = 0xa

.field private static final CMD_MYHANDLER_GET_VIDEOLIST:I = 0x6

.field private static final CMD_MYHANDLER_HIDE_MENU:I = 0x8

.field private static final CMD_MYHANDLER_HIDE_SETTINGMENU:I = 0x9

.field private static final CMD_MYHANDLER_MEDIA_SCAN_FINISH:I = 0xc

.field private static final CMD_MYHANDLER_MEDIA_SCAN_START:I = 0xb

.field private static final CMD_MYHANDLER_PREPARE_FINISH:I = 0xd

.field private static final CMD_MYHANDLER_SHOW_PROGRESS:I = 0x0

.field private static final CMD_USB_MOUNT_START:I = 0x12c

.field private static final CMD_USB_UNMOUNT_START:I = 0x12d

.field private static final FADE_OUT_TIMEOUT:I = 0x2710

.field private static final FB_SCREENMODE_3D:I = 0x1

.field private static final FB_SCREENMODE_4K:I = 0x2

.field private static final FB_SCREENMODE_NONE:I = 0x0

.field private static final GET_4K_STATUS_STR:Ljava/lang/String; = "isIn4K2KMode"

.field private static final HANDLE_ENTER_KEY:I = 0x200

.field private static final HANDLE_LEFT_KEY:I = 0x100

.field private static final HANDLE_RIGHT_KEY:I = 0x200

.field private static final INTENT_FROM_TYPE_LOCAL:I = 0x64

.field private static final INTENT_FROM_TYPE_REMOUTE_FILE:I = 0x68

.field private static final INTENT_FROM_TYPE_REMOUTE_HTTP:I = 0x67

.field private static final INTENT_FROM_TYPE_REMOUTE_SHAREIP:I = 0x66

.field private static final MEDIA_SCAN_ROOT_DIR:Ljava/lang/String; = "/mnt/usb/"

.field private static final MESSAGE_CLOUDYHEALTH:Ljava/lang/String; = "com.konka.service.switchinputsourcemsg"

.field private static final MOVIE_PLAY_STATUS:Ljava/lang/String; = "com.konka.MOVIE_PLAY_STATUS"

.field private static final MOVIE_PLAY_STATUS_TYPE_PLAY:I = 0x1

.field private static final MOVIE_PLAY_STATUS_TYPE_STOP:I = 0x0

.field private static final PIP_DISABLE:I = 0x4

.field private static final PIP_ENABLE:I = 0x3

.field private static final POP_DISABLE:I = 0x2

.field private static final POP_ENABLE:I = 0x1

.field private static final SCREEN_DEFAULT:I = 0x2

.field private static final SCREEN_FULL:I = 0x1

.field private static final SCREEN_FULL_RESIZE:I = 0x0

.field private static final TANK:Ljava/lang/String; = "tank"

.field private static final TIMEOUT:I = 0x3e80

.field private static screenHeight:I

.field private static screenWidth:I


# instance fields
.field private AbroadCustomer:Ljava/lang/String;

.field private CURRENT_LANGUAGE:Ljava/lang/String;

.field final MSTAR_800C:Z

.field private OnKey:Landroid/view/View$OnKeyListener;

.field private audioSkin:Lcom/mstar/android/tv/TvAudioManager;

.field private bIsImageSubtitle:Ljava/lang/Boolean;

.field private bMute:Z

.field private bTeacCustomer:Z

.field private bc3D:Lcom/konka/kkvideoplayer/VideoPlayerMain$BroadCast3D;

.field private br:Landroid/content/BroadcastReceiver;

.field private btnArrange:Landroid/widget/ImageButton;

.field private btnCircle:Landroid/widget/Button;

.field private btnImage:Landroid/widget/Button;

.field private btnNext:Landroid/widget/Button;

.field private btnPause:Landroid/widget/Button;

.field private btnPre:Landroid/widget/Button;

.field private btnScreen:Landroid/widget/Button;

.field private btnSetting:Landroid/widget/Button;

.field private btnStop:Landroid/widget/Button;

.field private btnSubtitle:Landroid/widget/Button;

.field private btnTrack:Landroid/widget/Button;

.field cBr:Lcom/konka/kkvideoplayer/VideoPlayerMain$CloudyMsgReceiver;

.field private circleTxt:Landroid/widget/TextView;

.field private commonSkin:Lcom/mstar/android/tv/TvCommonManager;

.field private curRestoreIndex:I

.field private curSavePoint:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/konka/kkvideoplayer/VideoPlayerMain$SavePoint;",
            ">;"
        }
    .end annotation
.end field

.field private currPlay:Lcom/konka/kkvideoplayer/CurrPlayInfo;

.field private currentVolume:I

.field doubleChannel:Landroid/view/View;

.field eBr:Lcom/konka/kkvideoplayer/VideoPlayerMain$EthStateReceiver;

.field private enablePip:Z

.field frontChannel:Landroid/view/View;

.field private hotKeyStat:I

.field private hotKeyStat_3d:I

.field private imageTxt:Landroid/widget/TextView;

.field private intentFrom:I

.field private isAudioSupport:Z

.field private isPaused:Z

.field private isVideoSupport:Z

.field private m3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;

.field private mAudioManager:Landroid/media/AudioManager;

.field private mCircleClick:I

.field private mCircleStr:[Ljava/lang/String;

.field private mCurrentTime:Landroid/widget/TextView;

.field private mDragging:Z

.field private mEndTime:Landroid/widget/TextView;

.field private mFormatBuilder:Ljava/lang/StringBuilder;

.field private mFormatter:Ljava/util/Formatter;

.field private mImageClick:I

.field private mImageStr:[Ljava/lang/String;

.field private mPopStat:I

.field private mReceiver:Landroid/content/BroadcastReceiver;

.field private mResumePlay:Ljava/lang/Boolean;

.field private mResumePoint:J

.field private mScreenClick:I

.field private mScreenStr:[Ljava/lang/String;

.field private mSubtitleClick:I

.field private mSubtitleNum:I

.field private mTitle:Ljava/lang/String;

.field private mTrackClick:I

.field private mTrackStr:[Ljava/lang/String;

.field private maxVolume:I

.field private ms3D:[S

.field private mstarHandler:Landroid/os/Handler;

.field multiScreenReceiver:Lcom/konka/kkvideoplayer/VideoPlayerMain$DataReceiver;

.field private muteTxt:Landroid/widget/TextView;

.field private myHandler:Landroid/os/Handler;

.field private newIntentFlag:I

.field private nextTxt:Landroid/widget/TextView;

.field private onFocusChange:Landroid/view/View$OnFocusChangeListener;

.field private pictureSkin:Lcom/mstar/android/tv/TvPictureManager;

.field private pipSkin:Lcom/mstar/android/tv/TvPipPopManager;

.field private playPauseTxt:Landroid/widget/TextView;

.field private popBr:Lcom/konka/kkvideoplayer/VideoPlayerMain$BroadCastPop;

.field private preTxt:Landroid/widget/TextView;

.field private progressDialog:Landroid/app/ProgressDialog;

.field private rowCircle:Landroid/widget/TableRow;

.field private rowImage:Landroid/widget/TableRow;

.field private rowScreen:Landroid/widget/TableRow;

.field private rowSubtitle:Landroid/widget/TableRow;

.field private rowTrack:Landroid/widget/TableRow;

.field private screenTxt:Landroid/widget/TextView;

.field private seekBar:Landroid/widget/SeekBar;

.field private seekHandler:Landroid/os/Handler;

.field private seekHint:Landroid/app/ProgressDialog;

.field private settingTxt:Landroid/widget/TextView;

.field settingView:Landroid/view/View;

.field private setting_window:Landroid/widget/PopupWindow;

.field private shareIp:Ljava/lang/String;

.field singleChannel:Landroid/view/View;

.field sp:Landroid/content/SharedPreferences;

.field private stopTxt:Landroid/widget/TextView;

.field private subtitleTxt:Landroid/widget/TextView;

.field private textView:Landroid/widget/TextView;

.field private toHome:Lcom/konka/kkvideoplayer/VideoPlayerMain$BroadCastGotoHome;

.field private toTv:Lcom/konka/kkvideoplayer/VideoPlayerMain$BroadCastGotoTv;

.field private trackCount:I

.field private trackTxt:Landroid/widget/TextView;

.field tvChannel:Landroid/widget/ImageButton;

.field twoChannelPlayer:Landroid/view/View;

.field private usbScanFlag:I

.field private videoInfoList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/konka/kkvideoplayer/KonkaVideoInfo;",
            ">;"
        }
    .end annotation
.end field

.field private voiceDownTxt:Landroid/widget/TextView;

.field private voiceUpTxt:Landroid/widget/TextView;

.field private vv:Lcom/konka/kkvideoplayer/VideoView;

.field widgetApp:Landroid/widget/ImageButton;

.field widgetBlog:Landroid/widget/ImageButton;

.field widgetLayout:Landroid/widget/FrameLayout;

.field private widgetListener:Lcom/konka/kkvideoplayer/WidgetListener;

.field widgetNews:Landroid/widget/ImageButton;

.field widgetTop:Landroid/widget/ImageView;

.field widgetVideo:Landroid/widget/ImageButton;


# direct methods
.method static synthetic $SWITCH_TABLE$com$konka$kkinterface$tv$DataBaseDesk$EN_MS_PICTURE()[I
    .locals 3

    sget-object v0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->$SWITCH_TABLE$com$konka$kkinterface$tv$DataBaseDesk$EN_MS_PICTURE:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;->PICTURE_DYNAMIC:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_7

    :goto_1
    :try_start_1
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;->PICTURE_NATURAL:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_6

    :goto_2
    :try_start_2
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;->PICTURE_NORMAL:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_5

    :goto_3
    :try_start_3
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;->PICTURE_NUMS:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_4

    :goto_4
    :try_start_4
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;->PICTURE_SOFT:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_3

    :goto_5
    :try_start_5
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;->PICTURE_SPORTS:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_2

    :goto_6
    :try_start_6
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;->PICTURE_USER:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_1

    :goto_7
    :try_start_7
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;->PICTURE_VIVID:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_0

    :goto_8
    sput-object v0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->$SWITCH_TABLE$com$konka$kkinterface$tv$DataBaseDesk$EN_MS_PICTURE:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_8

    :catch_1
    move-exception v1

    goto :goto_7

    :catch_2
    move-exception v1

    goto :goto_6

    :catch_3
    move-exception v1

    goto :goto_5

    :catch_4
    move-exception v1

    goto :goto_4

    :catch_5
    move-exception v1

    goto :goto_3

    :catch_6
    move-exception v1

    goto :goto_2

    :catch_7
    move-exception v1

    goto :goto_1
.end method

.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput v0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->screenWidth:I

    sput v0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->screenHeight:I

    return-void
.end method

.method public constructor <init>()V
    .locals 6

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v5, 0x0

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/util/SwitchSource;-><init>()V

    iput-boolean v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->MSTAR_800C:Z

    iput v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->intentFrom:I

    iput v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->hotKeyStat:I

    iput v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->hotKeyStat_3d:I

    iput v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->usbScanFlag:I

    iput v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->trackCount:I

    iput-object v5, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->btnArrange:Landroid/widget/ImageButton;

    iput-object v5, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->btnStop:Landroid/widget/Button;

    iput-object v5, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->btnPause:Landroid/widget/Button;

    iput-object v5, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->btnSetting:Landroid/widget/Button;

    iput-object v5, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->btnNext:Landroid/widget/Button;

    iput-object v5, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->btnPre:Landroid/widget/Button;

    iput-object v5, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mEndTime:Landroid/widget/TextView;

    iput-object v5, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mCurrentTime:Landroid/widget/TextView;

    iput-object v5, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->doubleChannel:Landroid/view/View;

    iput-object v5, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->singleChannel:Landroid/view/View;

    iput-object v5, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->frontChannel:Landroid/view/View;

    const/4 v0, 0x2

    iput v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mPopStat:I

    iput-boolean v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->bMute:Z

    iput-object v5, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->vv:Lcom/konka/kkvideoplayer/VideoView;

    iput-object v5, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->seekBar:Landroid/widget/SeekBar;

    iput-boolean v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->isPaused:Z

    iput-object v5, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mAudioManager:Landroid/media/AudioManager;

    const/16 v0, 0x64

    iput v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->maxVolume:I

    iput v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->currentVolume:I

    iput v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mScreenClick:I

    iput-object v5, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->rowScreen:Landroid/widget/TableRow;

    iput-object v5, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mScreenStr:[Ljava/lang/String;

    iput v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mImageClick:I

    iput-object v5, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->rowImage:Landroid/widget/TableRow;

    iput-object v5, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mImageStr:[Ljava/lang/String;

    iput v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mTrackClick:I

    iput-object v5, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->rowTrack:Landroid/widget/TableRow;

    iput-object v5, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mTrackStr:[Ljava/lang/String;

    iput v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mCircleClick:I

    iput-object v5, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->rowCircle:Landroid/widget/TableRow;

    iput-object v5, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mCircleStr:[Ljava/lang/String;

    iput v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mSubtitleClick:I

    iput-object v5, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->rowSubtitle:Landroid/widget/TableRow;

    iput v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mSubtitleNum:I

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->bIsImageSubtitle:Ljava/lang/Boolean;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mResumePlay:Ljava/lang/Boolean;

    const-wide/16 v3, 0x0

    iput-wide v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mResumePoint:J

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->videoInfoList:Ljava/util/List;

    iput-object v5, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->pictureSkin:Lcom/mstar/android/tv/TvPictureManager;

    iput-object v5, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->pipSkin:Lcom/mstar/android/tv/TvPipPopManager;

    iput-object v5, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->commonSkin:Lcom/mstar/android/tv/TvCommonManager;

    iput-object v5, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->audioSkin:Lcom/mstar/android/tv/TvAudioManager;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->curSavePoint:Ljava/util/ArrayList;

    const/4 v0, -0x1

    iput v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->curRestoreIndex:I

    iput-object v5, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->bc3D:Lcom/konka/kkvideoplayer/VideoPlayerMain$BroadCast3D;

    iput-object v5, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->popBr:Lcom/konka/kkvideoplayer/VideoPlayerMain$BroadCastPop;

    iput-object v5, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->toTv:Lcom/konka/kkvideoplayer/VideoPlayerMain$BroadCastGotoTv;

    iput-object v5, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->toHome:Lcom/konka/kkvideoplayer/VideoPlayerMain$BroadCastGotoHome;

    invoke-static {p0}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/CommonDesk;->getPipInfo()Lcom/konka/kkinterface/tv/CommonDesk$PipInfo;

    move-result-object v0

    iget v0, v0, Lcom/konka/kkinterface/tv/CommonDesk$PipInfo;->enablePip:I

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->enablePip:Z

    iput-object v5, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->ms3D:[S

    iput-object v5, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->widgetNews:Landroid/widget/ImageButton;

    iput-object v5, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->widgetApp:Landroid/widget/ImageButton;

    iput-object v5, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->widgetVideo:Landroid/widget/ImageButton;

    iput-object v5, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->widgetBlog:Landroid/widget/ImageButton;

    iput-object v5, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->tvChannel:Landroid/widget/ImageButton;

    iput v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->newIntentFlag:I

    iput-boolean v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->isAudioSupport:Z

    iput-boolean v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->isVideoSupport:Z

    new-instance v0, Lcom/konka/kkvideoplayer/VideoPlayerMain$1;

    invoke-direct {v0, p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain$1;-><init>(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V

    iput-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/konka/kkvideoplayer/VideoPlayerMain$2;

    invoke-direct {v0, p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain$2;-><init>(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V

    iput-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->onFocusChange:Landroid/view/View$OnFocusChangeListener;

    new-instance v0, Lcom/konka/kkvideoplayer/VideoPlayerMain$3;

    invoke-direct {v0, p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain$3;-><init>(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V

    iput-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->OnKey:Landroid/view/View$OnKeyListener;

    new-instance v0, Lcom/konka/kkvideoplayer/VideoPlayerMain$4;

    invoke-direct {v0, p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain$4;-><init>(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V

    iput-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->seekHandler:Landroid/os/Handler;

    return-void

    :cond_0
    move v0, v2

    goto :goto_0
.end method

.method static synthetic access$0(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->setMenuHideTimer()V

    return-void
.end method

.method static synthetic access$1(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Lcom/konka/kkvideoplayer/VideoView;
    .locals 1

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->vv:Lcom/konka/kkvideoplayer/VideoView;

    return-object v0
.end method

.method static synthetic access$10(Lcom/konka/kkvideoplayer/VideoPlayerMain;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->initPopAwindow(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$11(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->handlePreviousKey()V

    return-void
.end method

.method static synthetic access$12(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->handleNextKey()V

    return-void
.end method

.method static synthetic access$13(Lcom/konka/kkvideoplayer/VideoPlayerMain;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->handleJumpTo(I)V

    return-void
.end method

.method static synthetic access$14(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->preTxt:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$15(Lcom/konka/kkvideoplayer/VideoPlayerMain;Landroid/widget/TextView;Z)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->setTextViewVisible(Landroid/widget/TextView;Z)V

    return-void
.end method

.method static synthetic access$16(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->playPauseTxt:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$17(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->nextTxt:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$18(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->stopTxt:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$19(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->settingTxt:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$2(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Lcom/konka/kkvideoplayer/CurrPlayInfo;
    .locals 1

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->currPlay:Lcom/konka/kkvideoplayer/CurrPlayInfo;

    return-object v0
.end method

.method static synthetic access$20(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->bTeacCustomer:Z

    return v0
.end method

.method static synthetic access$21(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Ljava/lang/Boolean;
    .locals 1

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->handleKeyBack()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$22(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->handleKeyMenu()V

    return-void
.end method

.method static synthetic access$23(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Landroid/widget/SeekBar;
    .locals 1

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->seekBar:Landroid/widget/SeekBar;

    return-object v0
.end method

.method static synthetic access$24(Lcom/konka/kkvideoplayer/VideoPlayerMain;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->handleProgressChange(I)V

    return-void
.end method

.method static synthetic access$25(Lcom/konka/kkvideoplayer/VideoPlayerMain;)I
    .locals 1

    iget v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mPopStat:I

    return v0
.end method

.method static synthetic access$26(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Z
    .locals 1

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->isMenuShowing()Z

    move-result v0

    return v0
.end method

.method static synthetic access$27(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->hideMenuShow()V

    return-void
.end method

.method static synthetic access$28(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->handlePauseKey()V

    return-void
.end method

.method static synthetic access$29(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->bMute:Z

    return v0
.end method

.method static synthetic access$3(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->isPaused:Z

    return v0
.end method

.method static synthetic access$30(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Landroid/media/AudioManager;
    .locals 1

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mAudioManager:Landroid/media/AudioManager;

    return-object v0
.end method

.method static synthetic access$31(Lcom/konka/kkvideoplayer/VideoPlayerMain;)I
    .locals 1

    iget v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->currentVolume:I

    return v0
.end method

.method static synthetic access$32(Lcom/konka/kkvideoplayer/VideoPlayerMain;I)V
    .locals 0

    iput p1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->hotKeyStat:I

    return-void
.end method

.method static synthetic access$33(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mCurrentTime:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$34(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->seekHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$35(Lcom/konka/kkvideoplayer/VideoPlayerMain;)I
    .locals 1

    iget v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->intentFrom:I

    return v0
.end method

.method static synthetic access$36(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Landroid/app/ProgressDialog;
    .locals 1

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->progressDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$37(Lcom/konka/kkvideoplayer/VideoPlayerMain;Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->playMediaSource(Ljava/lang/String;I)V

    return-void
.end method

.method static synthetic access$38(Lcom/konka/kkvideoplayer/VideoPlayerMain;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->popSwitch(I)V

    return-void
.end method

.method static synthetic access$39(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->fixScreenToFullForMstarBug()V

    return-void
.end method

.method static synthetic access$4(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Landroid/widget/Button;
    .locals 1

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->btnPause:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$40(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->setOldScreen()V

    return-void
.end method

.method static synthetic access$41(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->setFullScreenWithoutSave()V

    return-void
.end method

.method static synthetic access$42(Lcom/konka/kkvideoplayer/VideoPlayerMain;)I
    .locals 1

    iget v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->hotKeyStat:I

    return v0
.end method

.method static synthetic access$43(Lcom/konka/kkvideoplayer/VideoPlayerMain;J)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->saveHangUpPoint(J)V

    return-void
.end method

.method static synthetic access$44(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Lcom/mstar/android/tv/TvPipPopManager;
    .locals 1

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->pipSkin:Lcom/mstar/android/tv/TvPipPopManager;

    return-object v0
.end method

.method static synthetic access$45(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->shareIp:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$46(Lcom/konka/kkvideoplayer/VideoPlayerMain;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mDragging:Z

    return-void
.end method

.method static synthetic access$47(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->myHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$48(Lcom/konka/kkvideoplayer/VideoPlayerMain;)I
    .locals 1

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->setProgress()I

    move-result v0

    return v0
.end method

.method static synthetic access$49(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->drawMenuShow()V

    return-void
.end method

.method static synthetic access$5(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->clearPauseStat()V

    return-void
.end method

.method static synthetic access$50(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->buildVideoDataSource()V

    return-void
.end method

.method static synthetic access$51(Lcom/konka/kkvideoplayer/VideoPlayerMain;I)V
    .locals 0

    iput p1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->usbScanFlag:I

    return-void
.end method

.method static synthetic access$52(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Ljava/lang/Boolean;
    .locals 1

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mResumePlay:Ljava/lang/Boolean;

    return-object v0
.end method

.method static synthetic access$53(Lcom/konka/kkvideoplayer/VideoPlayerMain;)J
    .locals 2

    iget-wide v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mResumePoint:J

    return-wide v0
.end method

.method static synthetic access$54(Lcom/konka/kkvideoplayer/VideoPlayerMain;Ljava/lang/Boolean;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mResumePlay:Ljava/lang/Boolean;

    return-void
.end method

.method static synthetic access$55(Lcom/konka/kkvideoplayer/VideoPlayerMain;I)V
    .locals 0

    iput p1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mImageClick:I

    return-void
.end method

.method static synthetic access$56(Lcom/konka/kkvideoplayer/VideoPlayerMain;)I
    .locals 1

    iget v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mImageClick:I

    return v0
.end method

.method static synthetic access$57(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->initWidget()V

    return-void
.end method

.method static synthetic access$58(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Landroid/app/ProgressDialog;
    .locals 1

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->seekHint:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$59(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->hideSettingMenu()V

    return-void
.end method

.method static synthetic access$6(Lcom/konka/kkvideoplayer/VideoPlayerMain;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->isPaused:Z

    return-void
.end method

.method static synthetic access$60(Lcom/konka/kkvideoplayer/VideoPlayerMain;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->buildHintDialog(I)V

    return-void
.end method

.method static synthetic access$61(Lcom/konka/kkvideoplayer/VideoPlayerMain;)I
    .locals 1

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->getIfFBScreenModeSet()I

    move-result v0

    return v0
.end method

.method static synthetic access$62(Lcom/konka/kkvideoplayer/VideoPlayerMain;I)V
    .locals 0

    iput p1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mScreenClick:I

    return-void
.end method

.method static synthetic access$63(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->setSubtitleLayout()V

    return-void
.end method

.method static synthetic access$64(Lcom/konka/kkvideoplayer/VideoPlayerMain;)I
    .locals 1

    iget v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mScreenClick:I

    return v0
.end method

.method static synthetic access$65(Lcom/konka/kkvideoplayer/VideoPlayerMain;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->setVideoScale(I)V

    return-void
.end method

.method static synthetic access$66(Lcom/konka/kkvideoplayer/VideoPlayerMain;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->setSubTitleText(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$67(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->resetSensorSetting()V

    return-void
.end method

.method static synthetic access$68(Lcom/konka/kkvideoplayer/VideoPlayerMain;)I
    .locals 1

    iget v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mSubtitleClick:I

    return v0
.end method

.method static synthetic access$69(Lcom/konka/kkvideoplayer/VideoPlayerMain;I)V
    .locals 0

    iput p1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mSubtitleNum:I

    return-void
.end method

.method static synthetic access$7(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->showPauseStat()V

    return-void
.end method

.method static synthetic access$70(Lcom/konka/kkvideoplayer/VideoPlayerMain;)I
    .locals 1

    iget v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mSubtitleNum:I

    return v0
.end method

.method static synthetic access$71(Lcom/konka/kkvideoplayer/VideoPlayerMain;Ljava/lang/Boolean;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->bIsImageSubtitle:Ljava/lang/Boolean;

    return-void
.end method

.method static synthetic access$72(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Lcom/konka/kkinterface/tv/S3DDesk;
    .locals 1

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->m3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    return-object v0
.end method

.method static synthetic access$73(Lcom/konka/kkvideoplayer/VideoPlayerMain;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->isAudioSupport:Z

    return-void
.end method

.method static synthetic access$74(Lcom/konka/kkvideoplayer/VideoPlayerMain;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->isVideoSupport:Z

    return-void
.end method

.method static synthetic access$75(Lcom/konka/kkvideoplayer/VideoPlayerMain;)I
    .locals 1

    iget v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mCircleClick:I

    return v0
.end method

.method static synthetic access$76(Lcom/konka/kkvideoplayer/VideoPlayerMain;Ljava/lang/String;)Lcom/konka/kkvideoplayer/KonkaVideoInfo;
    .locals 1

    invoke-direct {p0, p1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->getNextVideo(Ljava/lang/String;)Lcom/konka/kkvideoplayer/KonkaVideoInfo;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$77(Lcom/konka/kkvideoplayer/VideoPlayerMain;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->initPlayParam(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$78(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->cleanPopBackground()V

    return-void
.end method

.method static synthetic access$79(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Landroid/widget/TableRow;
    .locals 1

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->rowScreen:Landroid/widget/TableRow;

    return-object v0
.end method

.method static synthetic access$8(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->handleStopKey()V

    return-void
.end method

.method static synthetic access$80(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->enablePip:Z

    return v0
.end method

.method static synthetic access$81(Lcom/konka/kkvideoplayer/VideoPlayerMain;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->handleScreenModeKey(I)V

    return-void
.end method

.method static synthetic access$83(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Landroid/widget/TableRow;
    .locals 1

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->rowImage:Landroid/widget/TableRow;

    return-object v0
.end method

.method static synthetic access$84(Lcom/konka/kkvideoplayer/VideoPlayerMain;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->handleImagModeKey(I)V

    return-void
.end method

.method static synthetic access$85(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Landroid/widget/TableRow;
    .locals 1

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->rowCircle:Landroid/widget/TableRow;

    return-object v0
.end method

.method static synthetic access$86(Lcom/konka/kkvideoplayer/VideoPlayerMain;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->handleCircleModeKey(I)V

    return-void
.end method

.method static synthetic access$87(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Landroid/widget/TableRow;
    .locals 1

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->rowSubtitle:Landroid/widget/TableRow;

    return-object v0
.end method

.method static synthetic access$88(Lcom/konka/kkvideoplayer/VideoPlayerMain;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->handleSubtitleKey(I)V

    return-void
.end method

.method static synthetic access$89(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Landroid/widget/TableRow;
    .locals 1

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->rowTrack:Landroid/widget/TableRow;

    return-object v0
.end method

.method static synthetic access$9(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->handleKeyMenuForSpeech()V

    return-void
.end method

.method static synthetic access$90(Lcom/konka/kkvideoplayer/VideoPlayerMain;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->handleAudioTrackKey(I)V

    return-void
.end method

.method static synthetic access$91(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Landroid/widget/PopupWindow;
    .locals 1

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->setting_window:Landroid/widget/PopupWindow;

    return-object v0
.end method

.method static synthetic access$92()I
    .locals 1

    sget v0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->screenWidth:I

    return v0
.end method

.method static synthetic access$93(Lcom/konka/kkvideoplayer/VideoPlayerMain;Ljava/io/File;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->getMediaFileInDir(Ljava/io/File;)V

    return-void
.end method

.method static synthetic access$94(Lcom/konka/kkvideoplayer/VideoPlayerMain;Ljava/io/File;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->getMediaFileInDirNoRecursion(Ljava/io/File;)V

    return-void
.end method

.method private buildHintDialog(I)V
    .locals 3
    .param p1    # I

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0a0006    # com.konka.kkvideoplayer.R.string.error

    invoke-virtual {p0, v1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {p0, p1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a0007    # com.konka.kkvideoplayer.R.string.exit

    invoke-virtual {p0, v1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/konka/kkvideoplayer/VideoPlayerMain$43;

    invoke-direct {v2, p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain$43;-><init>(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/konka/kkvideoplayer/VideoPlayerMain$44;

    invoke-direct {v1, p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain$44;-><init>(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    return-void
.end method

.method private buildVideoDataSource()V
    .locals 6

    const/4 v5, 0x0

    const-string v1, "buildVideoDataSource Thread begin !!!!!!"

    invoke-static {v1}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    iget v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->intentFrom:I

    const/16 v2, 0x64

    if-ne v1, v2, :cond_0

    new-instance v1, Ljava/io/File;

    const-string v2, "/mnt/usb/"

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->getMediaFiles(Ljava/io/File;)V

    :goto_0
    return-void

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "currdir "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->currPlay:Lcom/konka/kkvideoplayer/CurrPlayInfo;

    iget-object v2, v2, Lcom/konka/kkvideoplayer/CurrPlayInfo;->url:Ljava/lang/String;

    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->currPlay:Lcom/konka/kkvideoplayer/CurrPlayInfo;

    iget-object v3, v3, Lcom/konka/kkvideoplayer/CurrPlayInfo;->url:Ljava/lang/String;

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v2, v5, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->currPlay:Lcom/konka/kkvideoplayer/CurrPlayInfo;

    iget-object v1, v1, Lcom/konka/kkvideoplayer/CurrPlayInfo;->url:Ljava/lang/String;

    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->currPlay:Lcom/konka/kkvideoplayer/CurrPlayInfo;

    iget-object v2, v2, Lcom/konka/kkvideoplayer/CurrPlayInfo;->url:Ljava/lang/String;

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v5, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->getMediaFilesNoRecursion(Ljava/io/File;)V

    goto :goto_0
.end method

.method private check3DScreen()V
    .locals 3

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->getCurrent3dFormat()Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    move-result-object v1

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_NONE:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    if-eq v1, v2, :cond_0

    const-string v2, "setOnKeyListener 3d \u6a21\u5f0f !!!!!but not set\n"

    invoke-static {v2}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method private checkIntent(Landroid/content/Intent;)I
    .locals 4
    .param p1    # Landroid/content/Intent;

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-nez v0, :cond_2

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "/mnt/usb/sda1/wmv_720p_4.5M.wmv"

    :cond_0
    :goto_0
    invoke-direct {p0, v0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->getPlayList(Landroid/os/Bundle;)V

    const-string v2, "host_ip"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->shareIp:Ljava/lang/String;

    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->shareIp:Ljava/lang/String;

    if-eqz v2, :cond_4

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SHAREIP IS "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->shareIp:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    const/16 v2, 0x66

    iput v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->intentFrom:I

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->initShareNetCheck()V

    :goto_1
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "=================play url:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->currPlay:Lcom/konka/kkvideoplayer/CurrPlayInfo;

    iget-object v3, v3, Lcom/konka/kkvideoplayer/CurrPlayInfo;->url:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    const/4 v2, 0x1

    :cond_1
    :goto_2
    return v2

    :cond_2
    const v3, 0x7f0a0043    # com.konka.kkvideoplayer.R.string.videofile

    invoke-virtual {p0, v3}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->currPlay:Lcom/konka/kkvideoplayer/CurrPlayInfo;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->currPlay:Lcom/konka/kkvideoplayer/CurrPlayInfo;

    iget-object v3, v3, Lcom/konka/kkvideoplayer/CurrPlayInfo;->url:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_3

    const-string v3, "Must url not update !!!!!!!!"

    invoke-static {v3}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    goto :goto_2

    :cond_3
    const-string v2, "Must url  update !!!!!!!!"

    invoke-static {v2}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->currPlay:Lcom/konka/kkvideoplayer/CurrPlayInfo;

    iput-object v1, v2, Lcom/konka/kkvideoplayer/CurrPlayInfo;->url:Ljava/lang/String;

    goto :goto_0

    :cond_4
    const-string v2, "INTENT_FROM_TYPE_REMOUTE_FILE "

    invoke-static {v2}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    const/16 v2, 0x68

    iput v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->intentFrom:I

    goto :goto_1
.end method

.method private cleanPopBackground()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->rowTrack:Landroid/widget/TableRow;

    invoke-virtual {v0, v1}, Landroid/widget/TableRow;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->rowScreen:Landroid/widget/TableRow;

    invoke-virtual {v0, v1}, Landroid/widget/TableRow;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->rowImage:Landroid/widget/TableRow;

    invoke-virtual {v0, v1}, Landroid/widget/TableRow;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->rowCircle:Landroid/widget/TableRow;

    invoke-virtual {v0, v1}, Landroid/widget/TableRow;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->rowSubtitle:Landroid/widget/TableRow;

    invoke-virtual {v0, v1}, Landroid/widget/TableRow;->setBackgroundResource(I)V

    return-void
.end method

.method private cleanProgress()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->seekBar:Landroid/widget/SeekBar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->seekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v2}, Landroid/widget/SeekBar;->setProgress(I)V

    :cond_0
    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mEndTime:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mEndTime:Landroid/widget/TextView;

    invoke-direct {p0, v2}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->stringForTime(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mCurrentTime:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mCurrentTime:Landroid/widget/TextView;

    invoke-direct {p0, v2}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->stringForTime(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    return-void
.end method

.method private clearPauseStat()V
    .locals 2

    const v1, 0x7f090020    # com.konka.kkvideoplayer.R.id.id_player_stat

    invoke-virtual {p0, v1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const v1, 0x7f020056    # com.konka.kkvideoplayer.R.drawable.commend_title_noselect

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    return-void
.end method

.method private decodePath(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x0

    const/4 v1, 0x7

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    const-string v1, "http://"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "GET ORIG URL=============="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    invoke-static {p1}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "GET decode URL============="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "local url do not decode URL============="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private drawMenuShow()V
    .locals 1

    new-instance v0, Lcom/konka/kkvideoplayer/VideoPlayerMain$11;

    invoke-direct {v0, p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain$11;-><init>(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V

    invoke-virtual {p0, v0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method private exit()V
    .locals 10

    const/4 v9, -0x1

    const/4 v8, -0x2

    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    const v6, 0x7f0a0021    # com.konka.kkvideoplayer.R.string.exit_title

    invoke-virtual {v3, v6}, Landroid/app/AlertDialog;->setTitle(I)V

    const v6, 0x7f0a0022    # com.konka.kkvideoplayer.R.string.exit_message

    invoke-virtual {p0, v6}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    const v6, 0x7f0a0023    # com.konka.kkvideoplayer.R.string.exit_ok

    invoke-virtual {p0, v6}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->getString(I)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Lcom/konka/kkvideoplayer/VideoPlayerMain$46;

    invoke-direct {v7, p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain$46;-><init>(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V

    invoke-virtual {v3, v9, v6, v7}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    const v6, 0x7f0a0024    # com.konka.kkvideoplayer.R.string.exit_cancle

    invoke-virtual {p0, v6}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->getString(I)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Lcom/konka/kkvideoplayer/VideoPlayerMain$47;

    invoke-direct {v7, p0, v3}, Lcom/konka/kkvideoplayer/VideoPlayerMain$47;-><init>(Lcom/konka/kkvideoplayer/VideoPlayerMain;Landroid/app/AlertDialog;)V

    invoke-virtual {v3, v8, v6, v7}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    invoke-virtual {v3}, Landroid/app/AlertDialog;->show()V

    invoke-virtual {v3}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v4

    invoke-virtual {v5, v4}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    invoke-virtual {v3, v9}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v3, v8}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    const/4 v6, 0x1

    invoke-virtual {v0, v6}, Landroid/widget/Button;->setFocusableInTouchMode(Z)V

    invoke-virtual {v1}, Landroid/widget/Button;->clearFocus()V

    return-void
.end method

.method private fixScreenToFullForMstarBug()V
    .locals 1

    const-string v0, "fixScreenToFullForMstarBug to resize!"

    invoke-static {v0}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    iget v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mScreenClick:I

    if-nez v0, :cond_0

    const-string v0, "already SCREEN_FULL_RESIZE !!!!!!!!!!!!! do nothing"

    invoke-static {v0}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mScreenClick:I

    iget v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mScreenClick:I

    invoke-direct {p0, v0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->setVideoScale(I)V

    goto :goto_0
.end method

.method private getDisplayInfo()V
    .locals 4

    invoke-virtual {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v1

    sput v1, Lcom/konka/kkvideoplayer/VideoPlayerMain;->screenWidth:I

    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v1

    sput v1, Lcom/konka/kkvideoplayer/VideoPlayerMain;->screenHeight:I

    const-string v1, "TANK"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "get display info...screenWidth: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v3, Lcom/konka/kkvideoplayer/VideoPlayerMain;->screenWidth:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "screenHeight: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/konka/kkvideoplayer/VideoPlayerMain;->screenHeight:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private getIfFBScreenModeSet()I
    .locals 6

    const/4 v4, 0x1

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->getCurrent3dFormat()Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    move-result-object v2

    invoke-virtual {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v5

    invoke-interface {v5}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getS3DManagerInstance()Lcom/konka/kkinterface/tv/S3DDesk;

    move-result-object v3

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/S3DDesk;->getDisplay3DTo2DMode()Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;

    move-result-object v0

    sget-object v5, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_NONE:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    if-ne v2, v5, :cond_0

    sget-object v5, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;->DB_ThreeD_Video_3DTO2D_NONE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;

    if-eq v0, v5, :cond_1

    :cond_0
    const-string v5, "fb screen mode set :3d mode!!!!!\n"

    invoke-static {v5}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    :goto_0
    return v4

    :cond_1
    const-string v5, "isIn4K2KMode"

    invoke-static {v5}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->getValueByTvosCommon(Ljava/lang/String;)S

    move-result v5

    if-ne v5, v4, :cond_2

    const-string v4, "fb screen mode set :4k mode!!!!!\n"

    invoke-static {v4}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v4, 0x2

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    :cond_2
    const-string v4, "screen mode set not fb!!!!!\n"

    invoke-static {v4}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    const/4 v4, 0x0

    goto :goto_0
.end method

.method private getMediaFileInDir(Ljava/io/File;)V
    .locals 5
    .param p1    # Ljava/io/File;

    if-nez p1, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    array-length v4, v0

    if-ge v1, v4, :cond_0

    sget-object v3, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->UNKOWN_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    aget-object v4, v0, v1

    invoke-virtual {v4}, Ljava/io/File;->canRead()Z

    move-result v4

    if-eqz v4, :cond_2

    aget-object v4, v0, v1

    invoke-virtual {v4}, Ljava/io/File;->isFile()Z

    move-result v4

    if-eqz v4, :cond_3

    aget-object v4, v0, v1

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->getFileType(Ljava/lang/String;)Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    move-result-object v3

    sget-object v4, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->UNKOWN_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    if-ne v3, v4, :cond_3

    :cond_2
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    aget-object v4, v0, v1

    invoke-virtual {v4}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-eqz v4, :cond_4

    aget-object v4, v0, v1

    invoke-direct {p0, v4}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->getMediaFileInDir(Ljava/io/File;)V

    goto :goto_1

    :cond_4
    new-instance v2, Lcom/konka/kkvideoplayer/KonkaVideoInfo;

    invoke-direct {v2}, Lcom/konka/kkvideoplayer/KonkaVideoInfo;-><init>()V

    iget-object v4, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->videoInfoList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v2, v4}, Lcom/konka/kkvideoplayer/KonkaVideoInfo;->setId(I)V

    aget-object v4, v0, v1

    invoke-virtual {v4}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/konka/kkvideoplayer/KonkaVideoInfo;->setPath(Ljava/lang/String;)V

    aget-object v4, v0, v1

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/konka/kkvideoplayer/KonkaVideoInfo;->setTitle(Ljava/lang/String;)V

    const v4, 0x7f02005f    # com.konka.kkvideoplayer.R.drawable.icon_localvideo_default

    invoke-virtual {v2, v4}, Lcom/konka/kkvideoplayer/KonkaVideoInfo;->setIcon(I)V

    iget-object v4, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->videoInfoList:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method private getMediaFileInDirNoRecursion(Ljava/io/File;)V
    .locals 6
    .param p1    # Ljava/io/File;

    if-nez p1, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    array-length v4, v0

    if-ge v1, v4, :cond_0

    sget-object v3, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->UNKOWN_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    aget-object v4, v0, v1

    invoke-virtual {v4}, Ljava/io/File;->canRead()Z

    move-result v4

    if-eqz v4, :cond_2

    aget-object v4, v0, v1

    invoke-virtual {v4}, Ljava/io/File;->isFile()Z

    move-result v4

    if-eqz v4, :cond_3

    aget-object v4, v0, v1

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->getFileType(Ljava/lang/String;)Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    move-result-object v3

    sget-object v4, Lcom/konka/kkvideoplayer/KonkaMediaDataType;->UNKOWN_FILE:Lcom/konka/kkvideoplayer/KonkaMediaDataType;

    if-ne v3, v4, :cond_3

    :cond_2
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    aget-object v4, v0, v1

    invoke-virtual {v4}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-eqz v4, :cond_4

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "dir="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v5, v0, v1

    invoke-virtual {v5}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    goto :goto_1

    :cond_4
    new-instance v2, Lcom/konka/kkvideoplayer/KonkaVideoInfo;

    invoke-direct {v2}, Lcom/konka/kkvideoplayer/KonkaVideoInfo;-><init>()V

    iget-object v4, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->videoInfoList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v2, v4}, Lcom/konka/kkvideoplayer/KonkaVideoInfo;->setId(I)V

    aget-object v4, v0, v1

    invoke-virtual {v4}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/konka/kkvideoplayer/KonkaVideoInfo;->setPath(Ljava/lang/String;)V

    aget-object v4, v0, v1

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/konka/kkvideoplayer/KonkaVideoInfo;->setTitle(Ljava/lang/String;)V

    const v4, 0x7f02005f    # com.konka.kkvideoplayer.R.drawable.icon_localvideo_default

    invoke-virtual {v2, v4}, Lcom/konka/kkvideoplayer/KonkaVideoInfo;->setIcon(I)V

    iget-object v4, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->videoInfoList:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method private getMediaFiles(Ljava/io/File;)V
    .locals 1
    .param p1    # Ljava/io/File;

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->videoInfoList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    new-instance v0, Lcom/konka/kkvideoplayer/VideoPlayerMain$41;

    invoke-direct {v0, p0, p1}, Lcom/konka/kkvideoplayer/VideoPlayerMain$41;-><init>(Lcom/konka/kkvideoplayer/VideoPlayerMain;Ljava/io/File;)V

    invoke-virtual {v0}, Lcom/konka/kkvideoplayer/VideoPlayerMain$41;->start()V

    return-void
.end method

.method private getMediaFilesNoRecursion(Ljava/io/File;)V
    .locals 1
    .param p1    # Ljava/io/File;

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->videoInfoList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    new-instance v0, Lcom/konka/kkvideoplayer/VideoPlayerMain$42;

    invoke-direct {v0, p0, p1}, Lcom/konka/kkvideoplayer/VideoPlayerMain$42;-><init>(Lcom/konka/kkvideoplayer/VideoPlayerMain;Ljava/io/File;)V

    invoke-virtual {v0}, Lcom/konka/kkvideoplayer/VideoPlayerMain$42;->start()V

    return-void
.end method

.method private getNextVideo(Ljava/lang/String;)Lcom/konka/kkvideoplayer/KonkaVideoInfo;
    .locals 4
    .param p1    # Ljava/lang/String;

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->getVideoByPath(Ljava/lang/String;)Lcom/konka/kkvideoplayer/KonkaVideoInfo;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-object v1

    :cond_1
    invoke-virtual {v0}, Lcom/konka/kkvideoplayer/KonkaVideoInfo;->getId()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->videoInfoList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lt v2, v3, :cond_2

    iget v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mCircleClick:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->videoInfoList:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/konka/kkvideoplayer/KonkaVideoInfo;

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->videoInfoList:Ljava/util/List;

    invoke-virtual {v0}, Lcom/konka/kkvideoplayer/KonkaVideoInfo;->getId()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/konka/kkvideoplayer/KonkaVideoInfo;

    goto :goto_0
.end method

.method private getPlayList(Landroid/os/Bundle;)V
    .locals 7
    .param p1    # Landroid/os/Bundle;

    const/4 v4, 0x0

    if-eqz p1, :cond_0

    const-string v5, "com.konka.mm.movie.list"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "videoInfoList iiiiiiiiiiiiiii size="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->videoInfoList:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->videoInfoList:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->clear()V

    if-eqz v3, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lt v0, v5, :cond_1

    :cond_0
    return-void

    :cond_1
    if-nez v0, :cond_2

    if-nez v4, :cond_2

    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    :cond_2
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    new-instance v1, Lcom/konka/kkvideoplayer/KonkaVideoInfo;

    invoke-direct {v1}, Lcom/konka/kkvideoplayer/KonkaVideoInfo;-><init>()V

    invoke-virtual {v1, v2}, Lcom/konka/kkvideoplayer/KonkaVideoInfo;->setPath(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Lcom/konka/kkvideoplayer/KonkaVideoInfo;->setId(I)V

    iget-object v5, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->videoInfoList:Ljava/util/List;

    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private getPrevVideo(Ljava/lang/String;)Lcom/konka/kkvideoplayer/KonkaVideoInfo;
    .locals 4
    .param p1    # Ljava/lang/String;

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->getVideoByPath(Ljava/lang/String;)Lcom/konka/kkvideoplayer/KonkaVideoInfo;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-object v1

    :cond_1
    invoke-virtual {v0}, Lcom/konka/kkvideoplayer/KonkaVideoInfo;->getId()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-gez v2, :cond_2

    iget v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mCircleClick:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->videoInfoList:Ljava/util/List;

    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->videoInfoList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/konka/kkvideoplayer/KonkaVideoInfo;

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->videoInfoList:Ljava/util/List;

    invoke-virtual {v0}, Lcom/konka/kkvideoplayer/KonkaVideoInfo;->getId()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/konka/kkvideoplayer/KonkaVideoInfo;

    goto :goto_0
.end method

.method private getStoreInfo()V
    .locals 3

    const/4 v2, 0x0

    const v0, 0x7f0a005c    # com.konka.kkvideoplayer.R.string.entrysetting

    invoke-virtual {p0, v0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->sp:Landroid/content/SharedPreferences;

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->sp:Landroid/content/SharedPreferences;

    const-string v1, "entry_screen_mode"

    const/4 v2, 0x1
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mScreenClick:I
    const/4 v2, 0x0

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->pipSkin:Lcom/mstar/android/tv/TvPipPopManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->pipSkin:Lcom/mstar/android/tv/TvPipPopManager;

    invoke-virtual {v0}, Lcom/mstar/android/tv/TvPipPopManager;->isPipModeEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "getStoreInfo !!!!!!!!!!!!"

    invoke-static {v0}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    iput v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mScreenClick:I

    :cond_0
    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->sp:Landroid/content/SharedPreferences;

    const-string v1, "entry_crircle_mode"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mCircleClick:I

    return-void
.end method

.method public static getValueByTvosCommon(Ljava/lang/String;)S
    .locals 5
    .param p0    # Ljava/lang/String;

    const/4 v2, -0x1

    if-eqz p0, :cond_0

    const-string v3, ""

    if-ne p0, v3, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    if-eqz v3, :cond_4

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3, p0}, Lcom/mstar/android/tvapi/common/TvManager;->setTvosCommonCommand(Ljava/lang/String;)[S

    move-result-object v1

    if-eqz v1, :cond_2

    array-length v3, v1

    if-nez v3, :cond_3

    :cond_2
    const-string v3, "tvos get value fail"

    invoke-static {v3}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :cond_3
    :try_start_1
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "tvos get value"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v4, 0x0

    aget-short v4, v1, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    const/4 v3, 0x0

    aget-short v2, v1, v3

    goto :goto_0

    :cond_4
    const-string v3, "TvManger.getInstance() == null"

    invoke-static {v3}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method private getVideoByPath(Ljava/lang/String;)Lcom/konka/kkvideoplayer/KonkaVideoInfo;
    .locals 5
    .param p1    # Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->videoInfoList:Ljava/util/List;

    if-nez v3, :cond_1

    move-object v1, v2

    :cond_0
    :goto_0
    return-object v1

    :cond_1
    const/4 v0, 0x0

    :goto_1
    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->videoInfoList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lt v0, v3, :cond_2

    move-object v1, v2

    goto :goto_0

    :cond_2
    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->videoInfoList:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/konka/kkvideoplayer/KonkaVideoInfo;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "info: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/konka/kkvideoplayer/KonkaVideoInfo;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/konka/kkvideoplayer/KonkaVideoInfo;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method private gotoHangUpPoint()V
    .locals 6

    const-wide/16 v4, 0x0

    const/4 v3, 0x1

    iget v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->newIntentFlag:I

    if-ne v0, v3, :cond_0

    invoke-virtual {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->checkIntent(Landroid/content/Intent;)I

    move-result v0

    if-ne v0, v3, :cond_0

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->currPlay:Lcom/konka/kkvideoplayer/CurrPlayInfo;

    iput-wide v4, v0, Lcom/konka/kkvideoplayer/CurrPlayInfo;->start:J

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->currPlay:Lcom/konka/kkvideoplayer/CurrPlayInfo;

    iget-object v0, v0, Lcom/konka/kkvideoplayer/CurrPlayInfo;->url:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->setTitle(Ljava/lang/String;)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->newIntentFlag:I

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "gotoHangUpPoint !!!---- start "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->currPlay:Lcom/konka/kkvideoplayer/CurrPlayInfo;

    iget-wide v1, v1, Lcom/konka/kkvideoplayer/CurrPlayInfo;->start:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->vv:Lcom/konka/kkvideoplayer/VideoView;

    invoke-virtual {v0}, Lcom/konka/kkvideoplayer/VideoView;->stopPlayback()V

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->vv:Lcom/konka/kkvideoplayer/VideoView;

    invoke-virtual {v0}, Lcom/konka/kkvideoplayer/VideoView;->reset()V

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->vv:Lcom/konka/kkvideoplayer/VideoView;

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->currPlay:Lcom/konka/kkvideoplayer/CurrPlayInfo;

    iget-object v1, v1, Lcom/konka/kkvideoplayer/CurrPlayInfo;->url:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/konka/kkvideoplayer/VideoView;->setVideoPath(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->clearPauseStat()V

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->currPlay:Lcom/konka/kkvideoplayer/CurrPlayInfo;

    iget-wide v0, v0, Lcom/konka/kkvideoplayer/CurrPlayInfo;->start:J

    cmp-long v0, v0, v4

    if-lez v0, :cond_1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mResumePlay:Ljava/lang/Boolean;

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->currPlay:Lcom/konka/kkvideoplayer/CurrPlayInfo;

    iget-wide v0, v0, Lcom/konka/kkvideoplayer/CurrPlayInfo;->start:J

    iput-wide v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mResumePoint:J

    :cond_1
    return-void
.end method

.method private handleAudioTrackKey(I)V
    .locals 3
    .param p1    # I

    iget v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->trackCount:I

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    const/16 v1, 0x100

    if-ne p1, v1, :cond_3

    iget v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mTrackClick:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mTrackClick:I

    :goto_1
    iget v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mTrackClick:I

    iget v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->trackCount:I

    if-lt v1, v2, :cond_1

    const/4 v1, 0x0

    iput v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mTrackClick:I

    :cond_1
    iget v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mTrackClick:I

    if-gez v1, :cond_2

    iget v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->trackCount:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mTrackClick:I

    :cond_2
    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->vv:Lcom/konka/kkvideoplayer/VideoView;

    iget v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mTrackClick:I

    invoke-virtual {v1, v2}, Lcom/konka/kkvideoplayer/VideoView;->setAudioTrack(I)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mTrackClick:I

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->trackCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->btnTrack:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->cleanPopBackground()V

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->rowTrack:Landroid/widget/TableRow;

    const v2, 0x7f02007f    # com.konka.kkvideoplayer.R.drawable.video_icon_set_select_bar

    invoke-virtual {v1, v2}, Landroid/widget/TableRow;->setBackgroundResource(I)V

    const-string v1, "............................"

    invoke-static {v1}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->setMenuHideTimer()V

    goto :goto_0

    :cond_3
    iget v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mTrackClick:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mTrackClick:I

    goto :goto_1
.end method

.method private handleCircleModeKey(I)V
    .locals 3
    .param p1    # I

    const/16 v0, 0x100

    if-ne p1, v0, :cond_2

    iget v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mCircleClick:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mCircleClick:I

    :goto_0
    iget v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mCircleClick:I

    const/4 v1, 0x3

    if-lt v0, v1, :cond_0

    const/4 v0, 0x0

    iput v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mCircleClick:I

    :cond_0
    iget v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mCircleClick:I

    if-gez v0, :cond_1

    const/4 v0, 0x2

    iput v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mCircleClick:I

    :cond_1
    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->btnCircle:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mCircleStr:[Ljava/lang/String;

    iget v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mCircleClick:I

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f0a0064    # com.konka.kkvideoplayer.R.string.entry_crircle_mode

    invoke-virtual {p0, v0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mCircleClick:I

    invoke-direct {p0, v0, v1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->saveValueInt(Ljava/lang/String;I)V

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->cleanPopBackground()V

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->rowCircle:Landroid/widget/TableRow;

    const v1, 0x7f02007f    # com.konka.kkvideoplayer.R.drawable.video_icon_set_select_bar

    invoke-virtual {v0, v1}, Landroid/widget/TableRow;->setBackgroundResource(I)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "............................mCircleClick "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mCircleClick:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->setMenuHideTimer()V

    return-void

    :cond_2
    iget v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mCircleClick:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mCircleClick:I

    goto :goto_0
.end method

.method private handleImagModeKey(I)V
    .locals 3
    .param p1    # I

    const/16 v0, 0x100

    if-ne p1, v0, :cond_2

    iget v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mImageClick:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mImageClick:I

    :goto_0
    iget v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mImageClick:I

    const/4 v1, 0x4

    if-lt v0, v1, :cond_0

    const/4 v0, 0x0

    iput v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mImageClick:I

    :cond_0
    iget v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mImageClick:I

    if-gez v0, :cond_1

    const/4 v0, 0x3

    iput v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mImageClick:I

    :cond_1
    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->btnImage:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mImageStr:[Ljava/lang/String;

    iget v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mImageClick:I

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mImageClick:I

    invoke-virtual {p0, v0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->setImageMode(I)V

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->cleanPopBackground()V

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->rowImage:Landroid/widget/TableRow;

    const v1, 0x7f02007f    # com.konka.kkvideoplayer.R.drawable.video_icon_set_select_bar

    invoke-virtual {v0, v1}, Landroid/widget/TableRow;->setBackgroundResource(I)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "............................:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mImageClick:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->setMenuHideTimer()V

    return-void

    :cond_2
    iget v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mImageClick:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mImageClick:I

    goto :goto_0
.end method

.method private handleJumpTo(I)V
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->vv:Lcom/konka/kkvideoplayer/VideoView;

    mul-int/lit16 v1, p1, 0x3e8

    invoke-virtual {v0, v1}, Lcom/konka/kkvideoplayer/VideoView;->seekTo(I)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->hide()V

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mCurrentTime:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mCurrentTime:Landroid/widget/TextView;

    invoke-direct {p0, p1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->stringForTime(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->setMenuHideTimer()V

    return-void
.end method

.method private handleKeyBack()Ljava/lang/Boolean;
    .locals 1

    const-string v0, "handleKeyBack"

    invoke-static {v0}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->isMenuShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->hideMenuShow()V

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0
.end method

.method private handleKeyMenu()V
    .locals 1

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->isMenuShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "handleKeyMenu!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!hidemenushow"

    invoke-static {v0}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->hideMenuShow()V

    :goto_0
    return-void

    :cond_0
    const-string v0, "handleKeyMenu!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!showmenu"

    invoke-static {v0}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->drawMenuShow()V

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->setMenuHideTimer()V

    goto :goto_0
.end method

.method private handleKeyMenuForSpeech()V
    .locals 1

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->isMenuShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const-string v0, "handleKeyMenu!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!showmenu"

    invoke-static {v0}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->drawMenuShow()V

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->setMenuHideTimer()V

    goto :goto_0
.end method

.method private handleNextKey()V
    .locals 7

    const/16 v6, 0x11

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget v4, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->usbScanFlag:I

    if-ne v4, v3, :cond_1

    invoke-virtual {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f0a000d    # com.konka.kkvideoplayer.R.string.hint_media_scan

    invoke-static {v4, v5, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1, v6, v2, v2}, Landroid/widget/Toast;->setGravity(III)V

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->check3DScreen()V

    iget-object v4, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->currPlay:Lcom/konka/kkvideoplayer/CurrPlayInfo;

    iget-object v4, v4, Lcom/konka/kkvideoplayer/CurrPlayInfo;->url:Ljava/lang/String;

    invoke-direct {p0, v4}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->getNextVideo(Ljava/lang/String;)Lcom/konka/kkvideoplayer/KonkaVideoInfo;

    move-result-object v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f0a000a    # com.konka.kkvideoplayer.R.string.hint_next_button

    invoke-static {v4, v5, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1, v6, v2, v2}, Landroid/widget/Toast;->setGravity(III)V

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->clearPauseStat()V

    invoke-virtual {v0}, Lcom/konka/kkvideoplayer/KonkaVideoInfo;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->initPlayParam(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v4}, Landroid/app/ProgressDialog;->show()V

    iget-boolean v4, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->isPaused:Z

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->btnPause:Landroid/widget/Button;

    const v5, 0x7f020011    # com.konka.kkvideoplayer.R.drawable.button_style_play

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setBackgroundResource(I)V

    iget-boolean v4, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->isPaused:Z

    if-eqz v4, :cond_3

    :goto_1
    iput-boolean v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->isPaused:Z

    goto :goto_0

    :cond_3
    move v2, v3

    goto :goto_1
.end method

.method private handlePauseKey()V
    .locals 2

    const-string v0, "invisible!!!!!!!!!!!!!!!!!"

    invoke-static {v0}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->vv:Lcom/konka/kkvideoplayer/VideoView;

    invoke-virtual {v0}, Lcom/konka/kkvideoplayer/VideoView;->isPlayerSane()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->vv:Lcom/konka/kkvideoplayer/VideoView;

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->currPlay:Lcom/konka/kkvideoplayer/CurrPlayInfo;

    iget-object v1, v1, Lcom/konka/kkvideoplayer/CurrPlayInfo;->url:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/konka/kkvideoplayer/VideoView;->setVideoPath(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-boolean v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->isPaused:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->vv:Lcom/konka/kkvideoplayer/VideoView;

    invoke-virtual {v0}, Lcom/konka/kkvideoplayer/VideoView;->start()V

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->btnPause:Landroid/widget/Button;

    const v1, 0x7f020010    # com.konka.kkvideoplayer.R.drawable.button_style_pause

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->clearPauseStat()V

    const-string v0, "current is restart "

    invoke-static {v0}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    :goto_1
    iget-boolean v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->isPaused:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    :goto_2
    iput-boolean v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->isPaused:Z

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->vv:Lcom/konka/kkvideoplayer/VideoView;

    invoke-virtual {v0}, Lcom/konka/kkvideoplayer/VideoView;->pause()V

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->btnPause:Landroid/widget/Button;

    const v1, 0x7f020011    # com.konka.kkvideoplayer.R.drawable.button_style_play

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->showPauseStat()V

    const-string v0, "current is pause !!!!!"

    invoke-static {v0}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    const/4 v0, 0x1

    goto :goto_2
.end method

.method private handlePreviousKey()V
    .locals 7

    const/16 v6, 0x11

    const/4 v5, 0x1

    const/4 v4, 0x0

    iget v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->usbScanFlag:I

    if-ne v2, v5, :cond_1

    invoke-virtual {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0a000d    # com.konka.kkvideoplayer.R.string.hint_media_scan

    invoke-static {v2, v3, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1, v6, v4, v4}, Landroid/widget/Toast;->setGravity(III)V

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->check3DScreen()V

    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->currPlay:Lcom/konka/kkvideoplayer/CurrPlayInfo;

    iget-object v2, v2, Lcom/konka/kkvideoplayer/CurrPlayInfo;->url:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->getPrevVideo(Ljava/lang/String;)Lcom/konka/kkvideoplayer/KonkaVideoInfo;

    move-result-object v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0a0009    # com.konka.kkvideoplayer.R.string.hint_pre_button

    invoke-static {v2, v3, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1, v6, v4, v4}, Landroid/widget/Toast;->setGravity(III)V

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->clearPauseStat()V

    invoke-virtual {v0}, Lcom/konka/kkvideoplayer/KonkaVideoInfo;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->initPlayParam(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v2}, Landroid/app/ProgressDialog;->show()V

    iget-boolean v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->isPaused:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->btnPause:Landroid/widget/Button;

    const v3, 0x7f020011    # com.konka.kkvideoplayer.R.drawable.button_style_play

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method private handleProgressChange(I)V
    .locals 8
    .param p1    # I

    iget-object v4, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->vv:Lcom/konka/kkvideoplayer/VideoView;

    invoke-virtual {v4}, Lcom/konka/kkvideoplayer/VideoView;->getDuration()I

    move-result v4

    int-to-long v0, v4

    int-to-long v4, p1

    mul-long/2addr v4, v0

    const-wide/16 v6, 0x3e8

    div-long v2, v4, v6

    iget-object v4, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v4}, Landroid/app/ProgressDialog;->show()V

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->vv:Lcom/konka/kkvideoplayer/VideoView;

    long-to-int v5, v2

    invoke-virtual {v4, v5}, Lcom/konka/kkvideoplayer/VideoView;->seekTo(I)V

    iget-object v4, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v4}, Landroid/app/ProgressDialog;->hide()V

    iget-object v4, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mCurrentTime:Landroid/widget/TextView;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mCurrentTime:Landroid/widget/TextView;

    long-to-int v5, v2

    invoke-direct {p0, v5}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->stringForTime(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->setMenuHideTimer()V

    return-void
.end method

.method private handleScreenModeKey(I)V
    .locals 3
    .param p1    # I

    const/16 v0, 0x100

    if-ne p1, v0, :cond_2

    iget v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mScreenClick:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mScreenClick:I

    :goto_0
    iget v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mScreenClick:I

    const/4 v1, 0x3

    if-lt v0, v1, :cond_0

    const/4 v0, 0x0

    iput v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mScreenClick:I

    :cond_0
    iget v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mScreenClick:I

    if-gez v0, :cond_1

    const/4 v0, 0x2

    iput v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mScreenClick:I

    :cond_1
    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->btnScreen:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mScreenStr:[Ljava/lang/String;

    iget v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mScreenClick:I

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mScreenClick:I

    invoke-direct {p0, v0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->setVideoScale(I)V

    const v0, 0x7f0a0063    # com.konka.kkvideoplayer.R.string.entry_screen_mode

    invoke-virtual {p0, v0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mScreenClick:I

    invoke-direct {p0, v0, v1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->saveValueInt(Ljava/lang/String;I)V

    const-string v0, "handleScreenModeKey"

    invoke-static {v0}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->cleanPopBackground()V

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->rowScreen:Landroid/widget/TableRow;

    const v1, 0x7f02007f    # com.konka.kkvideoplayer.R.drawable.video_icon_set_select_bar

    invoke-virtual {v0, v1}, Landroid/widget/TableRow;->setBackgroundResource(I)V

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->setMenuHideTimer()V

    return-void

    :cond_2
    iget v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mScreenClick:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mScreenClick:I

    goto :goto_0
.end method

.method private handleStopKey()V
    .locals 4

    iget-boolean v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->bMute:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mAudioManager:Landroid/media/AudioManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mAudioManager:Landroid/media/AudioManager;

    const/4 v1, 0x3

    iget v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->currentVolume:I

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/media/AudioManager;->setStreamVolume(III)V

    :cond_0
    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->vv:Lcom/konka/kkvideoplayer/VideoView;

    invoke-virtual {v0}, Lcom/konka/kkvideoplayer/VideoView;->stopPlayback()V

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->vv:Lcom/konka/kkvideoplayer/VideoView;

    invoke-virtual {v0}, Lcom/konka/kkvideoplayer/VideoView;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "hello world destroy!!! stop"

    invoke-static {v0}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    :cond_1
    const-string v0, "hello world will exec finish 2"

    invoke-static {v0}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->finish()V

    return-void
.end method

.method private handleSubtitleKey(I)V
    .locals 5
    .param p1    # I

    const v4, 0x7f02007f    # com.konka.kkvideoplayer.R.drawable.video_icon_set_select_bar

    const/4 v3, 0x0

    iget v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mSubtitleNum:I

    if-nez v1, :cond_0

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->cleanPopBackground()V

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->rowSubtitle:Landroid/widget/TableRow;

    invoke-virtual {v1, v4}, Landroid/widget/TableRow;->setBackgroundResource(I)V

    const-string v1, "............................"

    invoke-static {v1}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->setMenuHideTimer()V

    :goto_0
    return-void

    :cond_0
    const/16 v1, 0x100

    if-ne p1, v1, :cond_3

    iget v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mSubtitleClick:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mSubtitleClick:I

    :goto_1
    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->bIsImageSubtitle:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v1, "check it is imagesubtitle!!!"

    invoke-static {v1}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    iget v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mSubtitleClick:I

    iget v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mSubtitleNum:I

    if-ne v1, v2, :cond_1

    iput v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mSubtitleClick:I

    :cond_1
    iget v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mSubtitleClick:I

    if-gez v1, :cond_2

    iget v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mSubtitleNum:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mSubtitleClick:I

    :cond_2
    :goto_2
    iget v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mSubtitleClick:I

    iget v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mSubtitleNum:I

    if-ne v1, v2, :cond_6

    invoke-virtual {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0027    # com.konka.kkvideoplayer.R.string.subtitleoff

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->vv:Lcom/konka/kkvideoplayer/VideoView;

    invoke-virtual {v1}, Lcom/konka/kkvideoplayer/VideoView;->offSubtitleTrack()V

    const-string v1, "===============video subtitle is off!!"

    invoke-static {v1}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    :goto_3
    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->btnSubtitle:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->cleanPopBackground()V

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->rowSubtitle:Landroid/widget/TableRow;

    invoke-virtual {v1, v4}, Landroid/widget/TableRow;->setBackgroundResource(I)V

    const-string v1, "............................"

    invoke-static {v1}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->setMenuHideTimer()V

    goto :goto_0

    :cond_3
    iget v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mSubtitleClick:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mSubtitleClick:I

    goto :goto_1

    :cond_4
    iget v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mSubtitleClick:I

    iget v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mSubtitleNum:I

    if-le v1, v2, :cond_5

    iput v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mSubtitleClick:I

    :cond_5
    iget v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mSubtitleClick:I

    if-gez v1, :cond_2

    iget v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mSubtitleNum:I

    iput v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mSubtitleClick:I

    goto :goto_2

    :cond_6
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mSubtitleClick:I

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mSubtitleNum:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->vv:Lcom/konka/kkvideoplayer/VideoView;

    invoke-virtual {v1}, Lcom/konka/kkvideoplayer/VideoView;->onSubtitleTrack()V

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->bIsImageSubtitle:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->vv:Lcom/konka/kkvideoplayer/VideoView;

    iget v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mSubtitleClick:I

    or-int/lit16 v2, v2, 0x100

    invoke-virtual {v1, v2}, Lcom/konka/kkvideoplayer/VideoView;->setSubtitleTrack(I)V

    goto :goto_3

    :cond_7
    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->vv:Lcom/konka/kkvideoplayer/VideoView;

    iget v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mSubtitleClick:I

    invoke-virtual {v1, v2}, Lcom/konka/kkvideoplayer/VideoView;->setSubtitleTrack(I)V

    goto :goto_3
.end method

.method private hideMenuShow()V
    .locals 2

    invoke-static {}, Landroid/os/Looper;->myQueue()Landroid/os/MessageQueue;

    move-result-object v0

    new-instance v1, Lcom/konka/kkvideoplayer/VideoPlayerMain$10;

    invoke-direct {v1, p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain$10;-><init>(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V

    invoke-virtual {v0, v1}, Landroid/os/MessageQueue;->addIdleHandler(Landroid/os/MessageQueue$IdleHandler;)V

    return-void
.end method

.method private hideSettingMenu()V
    .locals 1

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->setting_window:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->setting_window:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "setting_window exit "

    invoke-static {v0}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->setting_window:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    :cond_0
    return-void
.end method

.method private initButtonControl()V
    .locals 2

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->btnPre:Landroid/widget/Button;

    if-nez v0, :cond_1

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "rrrrrrrrrrrrrrrrrrrr"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->btnPre:Landroid/widget/Button;

    new-instance v1, Lcom/konka/kkvideoplayer/VideoPlayerMain$36;

    invoke-direct {v1, p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain$36;-><init>(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->btnPause:Landroid/widget/Button;

    new-instance v1, Lcom/konka/kkvideoplayer/VideoPlayerMain$37;

    invoke-direct {v1, p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain$37;-><init>(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->btnNext:Landroid/widget/Button;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->btnNext:Landroid/widget/Button;

    new-instance v1, Lcom/konka/kkvideoplayer/VideoPlayerMain$38;

    invoke-direct {v1, p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain$38;-><init>(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->btnStop:Landroid/widget/Button;

    new-instance v1, Lcom/konka/kkvideoplayer/VideoPlayerMain$39;

    invoke-direct {v1, p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain$39;-><init>(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->btnSetting:Landroid/widget/Button;

    if-nez v0, :cond_2

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "rrrrrrrrrrrrrrrrrrrr"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->btnSetting:Landroid/widget/Button;

    new-instance v1, Lcom/konka/kkvideoplayer/VideoPlayerMain$40;

    invoke-direct {v1, p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain$40;-><init>(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->setBtnsOnFocusListeners()V

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->setBtnsOnKeyListeners()V

    goto :goto_0
.end method

.method private initHandle()V
    .locals 1

    new-instance v0, Lcom/konka/kkvideoplayer/VideoPlayerMain$8;

    invoke-direct {v0, p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain$8;-><init>(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V

    iput-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mstarHandler:Landroid/os/Handler;

    new-instance v0, Lcom/konka/kkvideoplayer/VideoPlayerMain$9;

    invoke-direct {v0, p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain$9;-><init>(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V

    iput-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->myHandler:Landroid/os/Handler;

    return-void
.end method

.method private initMenu()V
    .locals 1

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tv/TvPipPopManager;->isPipEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->han_pop_open()V

    :cond_0
    const-string v0, "init Menu !!!!!!!!!!=="

    invoke-static {v0}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->enablePip:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->pipSkin:Lcom/mstar/android/tv/TvPipPopManager;

    invoke-virtual {v0}, Lcom/mstar/android/tv/TvPipPopManager;->isPopEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "init Menu !!!!!!!!!!==1"

    invoke-static {v0}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->reInitMenu(I)V

    :goto_0
    return-void

    :cond_1
    const-string v0, "init Menu !!!!!!!!!!==2"

    invoke-static {v0}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->reInitMenu(I)V

    goto :goto_0
.end method

.method private initMstarSkin()V
    .locals 1

    invoke-static {}, Lcom/mstar/android/tv/TvPictureManager;->getInstance()Lcom/mstar/android/tv/TvPictureManager;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->pictureSkin:Lcom/mstar/android/tv/TvPictureManager;

    iget-boolean v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->enablePip:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->pipSkin:Lcom/mstar/android/tv/TvPipPopManager;

    :cond_0
    invoke-static {}, Lcom/mstar/android/tv/TvAudioManager;->getInstance()Lcom/mstar/android/tv/TvAudioManager;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->audioSkin:Lcom/mstar/android/tv/TvAudioManager;

    invoke-static {}, Lcom/mstar/android/tv/TvCommonManager;->getInstance()Lcom/mstar/android/tv/TvCommonManager;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->commonSkin:Lcom/mstar/android/tv/TvCommonManager;

    return-void
.end method

.method private initPlayParam(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->currPlay:Lcom/konka/kkvideoplayer/CurrPlayInfo;

    if-eqz v1, :cond_0

    if-eqz p1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "init play param url["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->currPlay:Lcom/konka/kkvideoplayer/CurrPlayInfo;

    iput-object p1, v1, Lcom/konka/kkvideoplayer/CurrPlayInfo;->url:Ljava/lang/String;

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->currPlay:Lcom/konka/kkvideoplayer/CurrPlayInfo;

    const-wide/16 v2, -0x1

    iput-wide v2, v1, Lcom/konka/kkvideoplayer/CurrPlayInfo;->duration:J

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->currPlay:Lcom/konka/kkvideoplayer/CurrPlayInfo;

    const-wide/16 v2, 0x0

    iput-wide v2, v1, Lcom/konka/kkvideoplayer/CurrPlayInfo;->start:J

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->vv:Lcom/konka/kkvideoplayer/VideoView;

    invoke-virtual {v1}, Lcom/konka/kkvideoplayer/VideoView;->stopPlayback()V

    const v1, 0x7f09001f    # com.konka.kkvideoplayer.R.id.layout_stat_play

    invoke-virtual {p0, v1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->currPlay:Lcom/konka/kkvideoplayer/CurrPlayInfo;

    iget-object v1, v1, Lcom/konka/kkvideoplayer/CurrPlayInfo;->url:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->setTitle(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->currPlay:Lcom/konka/kkvideoplayer/CurrPlayInfo;

    iget-object v1, v1, Lcom/konka/kkvideoplayer/CurrPlayInfo;->url:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-direct {p0, v1, v2}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->playMediaSource(Ljava/lang/String;I)V

    invoke-static {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->unFreeze(Landroid/app/Activity;)V

    :cond_0
    return-void
.end method

.method private initPopAwindow(Landroid/view/View;)V
    .locals 6
    .param p1    # Landroid/view/View;

    const/16 v5, 0x148

    const/16 v4, 0x12c

    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->setting_window:Landroid/widget/PopupWindow;

    if-nez v2, :cond_0

    const-string v2, "layout_inflater"

    invoke-virtual {p0, v2}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    const v2, 0x7f030003    # com.konka.kkvideoplayer.R.layout.player_setting

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->settingView:Landroid/view/View;

    invoke-virtual {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v0

    sget v2, Lcom/konka/kkvideoplayer/VideoPlayerMain;->screenWidth:I

    const/16 v3, 0x500

    if-ne v2, v3, :cond_1

    new-instance v2, Landroid/widget/PopupWindow;

    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->settingView:Landroid/view/View;

    invoke-direct {v2, v3, v4, v5}, Landroid/widget/PopupWindow;-><init>(Landroid/view/View;II)V

    iput-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->setting_window:Landroid/widget/PopupWindow;

    :goto_0
    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->setting_window:Landroid/widget/PopupWindow;

    new-instance v3, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v3}, Landroid/graphics/drawable/BitmapDrawable;-><init>()V

    invoke-virtual {v2, v3}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->setting_window:Landroid/widget/PopupWindow;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/PopupWindow;->setFocusable(Z)V

    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->settingView:Landroid/view/View;

    new-instance v3, Lcom/konka/kkvideoplayer/VideoPlayerMain$34;

    invoke-direct {v3, p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain$34;-><init>(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->setListenTrack()V

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->setListenScreenMode()V

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->setListenImageMode()V

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->setListenCircleMode()V

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->setListenSubtitle()V

    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->settingView:Landroid/view/View;

    new-instance v3, Lcom/konka/kkvideoplayer/VideoPlayerMain$35;

    invoke-direct {v3, p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain$35;-><init>(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    invoke-direct {p0, p1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->showSettingWindow(Landroid/view/View;)V

    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v0, v2}, Lcom/konka/kkinterface/tv/CommonDesk;->is1280x720(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_2

    new-instance v2, Landroid/widget/PopupWindow;

    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->settingView:Landroid/view/View;

    invoke-direct {v2, v3, v4, v5}, Landroid/widget/PopupWindow;-><init>(Landroid/view/View;II)V

    iput-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->setting_window:Landroid/widget/PopupWindow;

    goto :goto_0

    :cond_2
    new-instance v2, Landroid/widget/PopupWindow;

    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->settingView:Landroid/view/View;

    const/16 v4, 0x2bc

    const/16 v5, 0x1ae

    invoke-direct {v2, v3, v4, v5}, Landroid/widget/PopupWindow;-><init>(Landroid/view/View;II)V

    iput-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->setting_window:Landroid/widget/PopupWindow;

    goto :goto_0
.end method

.method private initProgressBar()V
    .locals 3

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->seekBar:Landroid/widget/SeekBar;

    const/16 v1, 0x3e8

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setMax(I)V

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->seekBar:Landroid/widget/SeekBar;

    new-instance v1, Lcom/konka/kkvideoplayer/VideoPlayerMain$7;

    invoke-direct {v1, p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain$7;-><init>(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mFormatBuilder:Ljava/lang/StringBuilder;

    new-instance v0, Ljava/util/Formatter;

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mFormatBuilder:Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/util/Formatter;-><init>(Ljava/lang/Appendable;Ljava/util/Locale;)V

    iput-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mFormatter:Ljava/util/Formatter;

    return-void
.end method

.method private initProgressDialog()V
    .locals 6

    const/4 v5, 0x0

    const/16 v4, -0x82

    const/4 v3, 0x1

    new-instance v1, Landroid/app/ProgressDialog;

    invoke-direct {v1, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->progressDialog:Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1, v5}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->progressDialog:Landroid/app/ProgressDialog;

    const v2, 0x7f0a001d    # com.konka.kkvideoplayer.R.string.mediafile_buffering

    invoke-virtual {p0, v2}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->show()V

    iget v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mPopStat:I

    if-ne v1, v3, :cond_0

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    iput v4, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    :cond_0
    new-instance v1, Landroid/app/ProgressDialog;

    invoke-direct {v1, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->seekHint:Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->seekHint:Landroid/app/ProgressDialog;

    invoke-virtual {v1, v5}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->seekHint:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->hide()V

    iget v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mPopStat:I

    if-ne v1, v3, :cond_1

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->seekHint:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    iput v4, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->seekHint:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    :cond_1
    return-void
.end method

.method private initReceiver()V
    .locals 10

    new-instance v8, Lcom/konka/kkvideoplayer/VideoPlayerMain$EthStateReceiver;

    invoke-direct {v8, p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain$EthStateReceiver;-><init>(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V

    iput-object v8, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->eBr:Lcom/konka/kkvideoplayer/VideoPlayerMain$EthStateReceiver;

    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    const-string v8, "android.net.ethernet.ETHERNET_STATE_CHANGED"

    invoke-virtual {v1, v8}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->eBr:Lcom/konka/kkvideoplayer/VideoPlayerMain$EthStateReceiver;

    invoke-virtual {p0, v8, v1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    new-instance v8, Lcom/konka/kkvideoplayer/VideoPlayerMain$CloudyMsgReceiver;

    invoke-direct {v8, p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain$CloudyMsgReceiver;-><init>(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V

    iput-object v8, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->cBr:Lcom/konka/kkvideoplayer/VideoPlayerMain$CloudyMsgReceiver;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v8, "com.konka.service.switchinputsourcemsg"

    invoke-virtual {v0, v8}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->cBr:Lcom/konka/kkvideoplayer/VideoPlayerMain$CloudyMsgReceiver;

    invoke-virtual {p0, v8, v0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    new-instance v8, Lcom/konka/kkvideoplayer/VideoPlayerMain$DataReceiver;

    const/4 v9, 0x0

    invoke-direct {v8, p0, v9}, Lcom/konka/kkvideoplayer/VideoPlayerMain$DataReceiver;-><init>(Lcom/konka/kkvideoplayer/VideoPlayerMain;Lcom/konka/kkvideoplayer/VideoPlayerMain$DataReceiver;)V

    iput-object v8, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->multiScreenReceiver:Lcom/konka/kkvideoplayer/VideoPlayerMain$DataReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V

    const-string v8, "com.konka.MultiScreeenPlayer.MultiScreenMediaPlayerActivity"

    invoke-virtual {v2, v8}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->multiScreenReceiver:Lcom/konka/kkvideoplayer/VideoPlayerMain$DataReceiver;

    invoke-virtual {p0, v8, v2}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    new-instance v8, Lcom/konka/kkvideoplayer/VideoPlayerMain$BroadCast3D;

    invoke-direct {v8, p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain$BroadCast3D;-><init>(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V

    iput-object v8, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->bc3D:Lcom/konka/kkvideoplayer/VideoPlayerMain$BroadCast3D;

    new-instance v3, Landroid/content/IntentFilter;

    invoke-direct {v3}, Landroid/content/IntentFilter;-><init>()V

    const-string v8, "com.konka.tv.hotkey.MM_3DENABLE"

    invoke-virtual {v3, v8}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v8, "com.konka.tv.hotkey.MM_3DDISABLE"

    invoke-virtual {v3, v8}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->bc3D:Lcom/konka/kkvideoplayer/VideoPlayerMain$BroadCast3D;

    invoke-virtual {p0, v8, v3}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    new-instance v8, Lcom/konka/kkvideoplayer/VideoPlayerMain$BroadCastGotoTv;

    invoke-direct {v8, p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain$BroadCastGotoTv;-><init>(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V

    iput-object v8, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->toTv:Lcom/konka/kkvideoplayer/VideoPlayerMain$BroadCastGotoTv;

    new-instance v5, Landroid/content/IntentFilter;

    invoke-direct {v5}, Landroid/content/IntentFilter;-><init>()V

    const-string v8, "com.konka.GO_TO_TV"

    invoke-virtual {v5, v8}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->toTv:Lcom/konka/kkvideoplayer/VideoPlayerMain$BroadCastGotoTv;

    invoke-virtual {p0, v8, v5}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    new-instance v8, Lcom/konka/kkvideoplayer/VideoPlayerMain$BroadCastGotoHome;

    invoke-direct {v8, p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain$BroadCastGotoHome;-><init>(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V

    iput-object v8, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->toHome:Lcom/konka/kkvideoplayer/VideoPlayerMain$BroadCastGotoHome;

    new-instance v4, Landroid/content/IntentFilter;

    invoke-direct {v4}, Landroid/content/IntentFilter;-><init>()V

    const-string v8, "com.konka.tv.hotkey.service.STOPMM"

    invoke-virtual {v4, v8}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->toHome:Lcom/konka/kkvideoplayer/VideoPlayerMain$BroadCastGotoHome;

    invoke-virtual {p0, v8, v4}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    new-instance v8, Lcom/konka/kkvideoplayer/VideoPlayerMain$BroadCastPop;

    invoke-direct {v8, p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain$BroadCastPop;-><init>(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V

    iput-object v8, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->popBr:Lcom/konka/kkvideoplayer/VideoPlayerMain$BroadCastPop;

    new-instance v7, Landroid/content/IntentFilter;

    invoke-direct {v7}, Landroid/content/IntentFilter;-><init>()V

    const-string v8, "com.konka.hotkey.enablePop"

    invoke-virtual {v7, v8}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v8, "com.konka.hotkey.disablePop"

    invoke-virtual {v7, v8}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v8, "com.konka.hotkey.disablePip"

    invoke-virtual {v7, v8}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v8, "com.konka.hotkey.enablePip"

    invoke-virtual {v7, v8}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v8, "com.konka.hotkey.enable3dDualView"

    invoke-virtual {v7, v8}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->popBr:Lcom/konka/kkvideoplayer/VideoPlayerMain$BroadCastPop;

    invoke-virtual {p0, v8, v7}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    new-instance v8, Lcom/konka/kkvideoplayer/VideoPlayerMain$45;

    invoke-direct {v8, p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain$45;-><init>(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V

    iput-object v8, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->br:Landroid/content/BroadcastReceiver;

    new-instance v6, Landroid/content/IntentFilter;

    const-string v8, "android.intent.action.MEDIA_MOUNTED"

    invoke-direct {v6, v8}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const-string v8, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v6, v8}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v8, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v6, v8}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v8, "android.intent.action.MEDIA_REMOVED"

    invoke-virtual {v6, v8}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v8, "android.intent.action.MEDIA_BAD_REMOVAL"

    invoke-virtual {v6, v8}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v8, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v6, v8}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v8, "android.intent.action.UMS_CONNECTED"

    invoke-virtual {v6, v8}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v8, "android.intent.action.UMS_DISCONNECTED"

    invoke-virtual {v6, v8}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v8, "file"

    invoke-virtual {v6, v8}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->br:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v8, v6}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method private initShareNetCheck()V
    .locals 1

    new-instance v0, Lcom/konka/kkvideoplayer/VideoPlayerMain$6;

    invoke-direct {v0, p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain$6;-><init>(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V

    invoke-virtual {v0}, Lcom/konka/kkvideoplayer/VideoPlayerMain$6;->start()V

    return-void
.end method

.method private initVAFBroadcastReceiver()V
    .locals 2

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "konka.voice.control.action.PLAYER_CONTROL"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method private initVideoPlay()V
    .locals 2

    const v0, 0x7f09001e    # com.konka.kkvideoplayer.R.id.vv

    invoke-virtual {p0, v0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/konka/kkvideoplayer/VideoView;

    iput-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->vv:Lcom/konka/kkvideoplayer/VideoView;

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->vv:Lcom/konka/kkvideoplayer/VideoView;

    if-nez v0, :cond_0

    const-string v0, "NULL=========="

    invoke-static {v0}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->vv:Lcom/konka/kkvideoplayer/VideoView;

    new-instance v1, Lcom/konka/kkvideoplayer/VideoPlayerMain$12;

    invoke-direct {v1, p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain$12;-><init>(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V

    invoke-virtual {v0, v1}, Lcom/konka/kkvideoplayer/VideoView;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->vv:Lcom/konka/kkvideoplayer/VideoView;

    new-instance v1, Lcom/konka/kkvideoplayer/VideoPlayerMain$13;

    invoke-direct {v1, p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain$13;-><init>(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V

    invoke-virtual {v0, v1}, Lcom/konka/kkvideoplayer/VideoView;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->vv:Lcom/konka/kkvideoplayer/VideoView;

    new-instance v1, Lcom/konka/kkvideoplayer/VideoPlayerMain$14;

    invoke-direct {v1, p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain$14;-><init>(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V

    invoke-virtual {v0, v1}, Lcom/konka/kkvideoplayer/VideoView;->setOnInfoListener(Landroid/media/MediaPlayer$OnInfoListener;)V

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->vv:Lcom/konka/kkvideoplayer/VideoView;

    new-instance v1, Lcom/konka/kkvideoplayer/VideoPlayerMain$15;

    invoke-direct {v1, p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain$15;-><init>(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V

    invoke-virtual {v0, v1}, Lcom/konka/kkvideoplayer/VideoView;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->vv:Lcom/konka/kkvideoplayer/VideoView;

    new-instance v1, Lcom/konka/kkvideoplayer/VideoPlayerMain$16;

    invoke-direct {v1, p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain$16;-><init>(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V

    invoke-virtual {v0, v1}, Lcom/konka/kkvideoplayer/VideoView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    invoke-virtual {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->playIntent(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private initWidget()V
    .locals 2

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->widgetLayout:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_0

    const-string v0, "already init !!!!!!!"

    invoke-static {v0}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->doubleChannel:Landroid/view/View;

    const v1, 0x7f090005    # com.konka.kkvideoplayer.R.id.id_widget_manager

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->widgetLayout:Landroid/widget/FrameLayout;

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->doubleChannel:Landroid/view/View;

    const v1, 0x7f090006    # com.konka.kkvideoplayer.R.id.id_widget_top

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->widgetTop:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->widgetTop:Landroid/widget/ImageView;

    const v1, 0x7f02008d    # com.konka.kkvideoplayer.R.drawable.widget_news_top

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    new-instance v0, Lcom/konka/kkvideoplayer/WidgetListener;

    invoke-direct {v0, p0}, Lcom/konka/kkvideoplayer/WidgetListener;-><init>(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V

    iput-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->widgetListener:Lcom/konka/kkvideoplayer/WidgetListener;

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->doubleChannel:Landroid/view/View;

    const v1, 0x7f090007    # com.konka.kkvideoplayer.R.id.id_widget_news

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->widgetNews:Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->widgetNews:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->widgetListener:Lcom/konka/kkvideoplayer/WidgetListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->doubleChannel:Landroid/view/View;

    const v1, 0x7f090008    # com.konka.kkvideoplayer.R.id.id_widget_app

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->widgetApp:Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->widgetApp:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->widgetListener:Lcom/konka/kkvideoplayer/WidgetListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->doubleChannel:Landroid/view/View;

    const v1, 0x7f090009    # com.konka.kkvideoplayer.R.id.id_widget_video

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->widgetVideo:Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->widgetVideo:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->widgetListener:Lcom/konka/kkvideoplayer/WidgetListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->doubleChannel:Landroid/view/View;

    const v1, 0x7f09000a    # com.konka.kkvideoplayer.R.id.id_widget_blog

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->widgetBlog:Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->widgetBlog:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->widgetListener:Lcom/konka/kkvideoplayer/WidgetListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->doubleChannel:Landroid/view/View;

    const v1, 0x7f090004    # com.konka.kkvideoplayer.R.id.id_button_tv_channel

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->tvChannel:Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->tvChannel:Landroid/widget/ImageButton;

    new-instance v1, Lcom/konka/kkvideoplayer/VideoPlayerMain$5;

    invoke-direct {v1, p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain$5;-><init>(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0
.end method

.method public static isConneted2Network(Landroid/app/Activity;)Z
    .locals 6
    .param p0    # Landroid/app/Activity;

    const/4 v3, 0x1

    const/4 v2, 0x0

    const-string v4, "connectivity"

    invoke-virtual {p0, v4}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    invoke-virtual {v1}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v4

    const/16 v5, 0x9

    if-eq v4, v5, :cond_2

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v4

    if-ne v4, v3, :cond_0

    :cond_2
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v4

    if-eqz v4, :cond_0

    move v2, v3

    goto :goto_0
.end method

.method private isMenuShowing()Z
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->isPopEnable()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->twoChannelPlayer:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->isShown()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->frontChannel:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->isShown()Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method private isPopEnable()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mPopStat:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private keepMenu()V
    .locals 4

    const/16 v3, 0x8

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->myHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->myHandler:Landroid/os/Handler;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->myHandler:Landroid/os/Handler;

    const-wide/16 v1, 0x2710

    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    return-void
.end method

.method private playIntent(Landroid/content/Intent;)V
    .locals 10
    .param p1    # Landroid/content/Intent;

    const/4 v5, 0x0

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-nez v0, :cond_2

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v5, "/mnt/usb/sda1/wmv_720p_4.5M.wmv"

    :cond_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-nez v7, :cond_3

    :cond_1
    const-string v6, "intent_from"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_5

    const-string v6, "FEINGHUI INTENT!!!!!!!!!!!"

    invoke-static {v6}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->getPlayList(Landroid/os/Bundle;)V

    const-string v6, "host_ip"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->shareIp:Ljava/lang/String;

    iget-object v6, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->shareIp:Ljava/lang/String;

    if-eqz v6, :cond_4

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "SHAREIP IS "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->shareIp:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    const/16 v6, 0x66

    iput v6, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->intentFrom:I

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->initShareNetCheck()V

    :goto_1
    new-instance v6, Lcom/konka/kkvideoplayer/CurrPlayInfo;

    invoke-direct {v6}, Lcom/konka/kkvideoplayer/CurrPlayInfo;-><init>()V

    iput-object v6, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->currPlay:Lcom/konka/kkvideoplayer/CurrPlayInfo;

    invoke-direct {p0, v5}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->initPlayParam(Ljava/lang/String;)V

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "=================play url:"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->currPlay:Lcom/konka/kkvideoplayer/CurrPlayInfo;

    iget-object v7, v7, Lcom/konka/kkvideoplayer/CurrPlayInfo;->url:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    :goto_2
    return-void

    :cond_2
    const v6, 0x7f0a0043    # com.konka.kkvideoplayer.R.string.videofile

    invoke-virtual {p0, v6}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_0

    goto :goto_2

    :cond_3
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const-string v7, "BAI"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "set: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_4
    const-string v6, "INTENT_FROM_TYPE_REMOUTE_FILE "

    invoke-static {v6}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    const/16 v6, 0x68

    iput v6, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->intentFrom:I

    goto :goto_1

    :cond_5
    const-string v6, "TANK INTENT!!!!!!!!!!!"

    invoke-static {v6}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    const/16 v6, 0x64

    iput v6, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->intentFrom:I

    const-string v6, "usbexplore_list"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    const/4 v6, 0x0

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/List;

    iput-object v6, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->videoInfoList:Ljava/util/List;

    iget-object v6, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->videoInfoList:Ljava/util/List;

    if-nez v6, :cond_6

    const-string v6, "TANK INTENT!!!!!!!!!!! null"

    invoke-static {v6}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->myHandler:Landroid/os/Handler;

    const/4 v7, 0x6

    invoke-virtual {v6, v7}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_1

    :cond_6
    const-string v6, "TANK INTENT!!!!!!!!!!! good!"

    invoke-static {v6}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method private playMediaSource(Ljava/lang/String;I)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # I

    invoke-direct {p0, p1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->decodePath(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->currPlay:Lcom/konka/kkvideoplayer/CurrPlayInfo;

    iput-object p1, v0, Lcom/konka/kkvideoplayer/CurrPlayInfo;->url:Ljava/lang/String;

    int-to-long v0, p2

    invoke-direct {p0, v0, v1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->saveHangUpPoint(J)V

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->currPlay:Lcom/konka/kkvideoplayer/CurrPlayInfo;

    const-wide/16 v1, -0x1

    iput-wide v1, v0, Lcom/konka/kkvideoplayer/CurrPlayInfo;->duration:J

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->vv:Lcom/konka/kkvideoplayer/VideoView;

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->currPlay:Lcom/konka/kkvideoplayer/CurrPlayInfo;

    iget-object v1, v1, Lcom/konka/kkvideoplayer/CurrPlayInfo;->url:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->trim(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/konka/kkvideoplayer/VideoView;->setVideoPath(Ljava/lang/String;)V

    return-void
.end method

.method private popSwitch(I)V
    .locals 0
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->reInitMenu(I)V

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->drawMenuShow()V

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->setMenuHideTimer()V

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->setSubtitleLayout()V

    return-void
.end method

.method private reInitMenu(I)V
    .locals 7
    .param p1    # I

    const/4 v6, 0x0

    const/4 v5, 0x0

    const v3, 0x7f09001d    # com.konka.kkvideoplayer.R.id.id_player_main

    invoke-virtual {p0, v3}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "reInitMenu !!!!!!!!!!- --- s"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    const/4 v3, 0x1

    if-ne v3, p1, :cond_3

    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->singleChannel:Landroid/view/View;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->singleChannel:Landroid/view/View;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    iget v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mScreenClick:I

    if-eqz v3, :cond_1

    const-string v3, "switch to SCREEN_FULL_RESIZE"

    invoke-static {v3}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    iput v5, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mScreenClick:I

    iget v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mScreenClick:I

    invoke-direct {p0, v3}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->setVideoScale(I)V

    :cond_1
    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->doubleChannel:Landroid/view/View;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->doubleChannel:Landroid/view/View;

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->doubleChannel:Landroid/view/View;

    iput-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->frontChannel:Landroid/view/View;

    invoke-direct {p0, p1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->reInitMenuMember(I)V

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->setMenuHideTimer()V

    const-string v3, "reInitMenu !!!!!!!!!!-1"

    invoke-static {v3}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_2
    const/high16 v3, 0x7f030000    # com.konka.kkvideoplayer.R.layout.double_channel

    invoke-virtual {v1, v3, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    const v4, 0x7f090001    # com.konka.kkvideoplayer.R.id.id_layout_doublechannel

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    iput-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->doubleChannel:Landroid/view/View;

    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->doubleChannel:Landroid/view/View;

    iput-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->frontChannel:Landroid/view/View;

    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->doubleChannel:Landroid/view/View;

    const v4, 0x7f090002    # com.konka.kkvideoplayer.R.id.id_doublechannel_left

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->twoChannelPlayer:Landroid/view/View;

    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->twoChannelPlayer:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->requestFocus()Z

    invoke-direct {p0, p1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->reInitMenuMember(I)V

    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->twoChannelPlayer:Landroid/view/View;

    new-instance v4, Lcom/konka/kkvideoplayer/VideoPlayerMain$32;

    invoke-direct {v4, p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain$32;-><init>(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_1
    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->setMenuHideTimer()V

    goto :goto_0

    :cond_3
    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->doubleChannel:Landroid/view/View;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->doubleChannel:Landroid/view/View;

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    :cond_4
    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->singleChannel:Landroid/view/View;

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->singleChannel:Landroid/view/View;

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->singleChannel:Landroid/view/View;

    iput-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->frontChannel:Landroid/view/View;

    invoke-direct {p0, p1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->reInitMenuMember(I)V

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->setMenuHideTimer()V

    goto :goto_0

    :cond_5
    const v3, 0x7f030005    # com.konka.kkvideoplayer.R.layout.single_channel_player

    invoke-virtual {v1, v3, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    const v4, 0x7f090034    # com.konka.kkvideoplayer.R.id.id_singlechannel

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->removeView(Landroid/view/View;)V

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    iput-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->singleChannel:Landroid/view/View;

    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->singleChannel:Landroid/view/View;

    iput-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->frontChannel:Landroid/view/View;

    invoke-direct {p0, p1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->reInitMenuMember(I)V

    goto :goto_1
.end method

.method private reInitMenuMember(I)V
    .locals 2
    .param p1    # I

    const-string v0, "reInitMenuMember !!!!!!"

    invoke-static {v0}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->frontChannel:Landroid/view/View;

    const v1, 0x7f09000d    # com.konka.kkvideoplayer.R.id.konka_titile

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->textView:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->frontChannel:Landroid/view/View;

    const v1, 0x7f090013    # com.konka.kkvideoplayer.R.id.video_pre_info

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->preTxt:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->frontChannel:Landroid/view/View;

    const v1, 0x7f090015    # com.konka.kkvideoplayer.R.id.video_play_pause_info

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->playPauseTxt:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->frontChannel:Landroid/view/View;

    const v1, 0x7f090017    # com.konka.kkvideoplayer.R.id.video_next_info

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->nextTxt:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->frontChannel:Landroid/view/View;

    const v1, 0x7f090019    # com.konka.kkvideoplayer.R.id.video_stop_info

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->stopTxt:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->frontChannel:Landroid/view/View;

    const v1, 0x7f09001c    # com.konka.kkvideoplayer.R.id.video_setting_info

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->settingTxt:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->frontChannel:Landroid/view/View;

    const v1, 0x7f09000b    # com.konka.kkvideoplayer.R.id.seekbar_videoplayer

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->seekBar:Landroid/widget/SeekBar;

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->frontChannel:Landroid/view/View;

    const v1, 0x7f090010    # com.konka.kkvideoplayer.R.id.button_prev

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->btnPre:Landroid/widget/Button;

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->frontChannel:Landroid/view/View;

    const v1, 0x7f090014    # com.konka.kkvideoplayer.R.id.button_play_pause

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->btnPause:Landroid/widget/Button;

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->frontChannel:Landroid/view/View;

    const v1, 0x7f090018    # com.konka.kkvideoplayer.R.id.button_stop

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->btnStop:Landroid/widget/Button;

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->frontChannel:Landroid/view/View;

    const v1, 0x7f090016    # com.konka.kkvideoplayer.R.id.button_next

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->btnNext:Landroid/widget/Button;

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->frontChannel:Landroid/view/View;

    const v1, 0x7f09001b    # com.konka.kkvideoplayer.R.id.button_setting

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->btnSetting:Landroid/widget/Button;

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->frontChannel:Landroid/view/View;

    const v1, 0x7f090011    # com.konka.kkvideoplayer.R.id.duration

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mEndTime:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->frontChannel:Landroid/view/View;

    const v1, 0x7f09000f    # com.konka.kkvideoplayer.R.id.has_played

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mCurrentTime:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->currPlay:Lcom/konka/kkvideoplayer/CurrPlayInfo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->currPlay:Lcom/konka/kkvideoplayer/CurrPlayInfo;

    iget-object v0, v0, Lcom/konka/kkvideoplayer/CurrPlayInfo;->url:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->setTitle(Ljava/lang/String;)V

    :cond_0
    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->initProgressBar()V

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->initButtonControl()V

    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->myHandler:Landroid/os/Handler;

    const/16 v1, 0xe

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_1
    iput p1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mPopStat:I

    return-void
.end method

.method private readSavePoint()V
    .locals 8

    iget-object v5, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->curSavePoint:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    const v5, 0x7f0a0044    # com.konka.kkvideoplayer.R.string.videosetting

    invoke-virtual {p0, v5}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x2

    invoke-virtual {p0, v5, v6}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    const v5, 0x7f0a0045    # com.konka.kkvideoplayer.R.string.videosetting_url1

    invoke-virtual {p0, v5}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-interface {v3, v5, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const v5, 0x7f0a0046    # com.konka.kkvideoplayer.R.string.videosetting_pos1

    invoke-virtual {p0, v5}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->getString(I)Ljava/lang/String;

    move-result-object v5

    const-wide/16 v6, -0x1

    invoke-interface {v3, v5, v6, v7}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    if-eqz v4, :cond_0

    const-wide/16 v5, 0x0

    cmp-long v5, v0, v5

    if-ltz v5, :cond_0

    new-instance v2, Lcom/konka/kkvideoplayer/VideoPlayerMain$SavePoint;

    invoke-direct {v2, p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain$SavePoint;-><init>(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V

    iput-object v4, v2, Lcom/konka/kkvideoplayer/VideoPlayerMain$SavePoint;->url:Ljava/lang/String;

    iput-wide v0, v2, Lcom/konka/kkvideoplayer/VideoPlayerMain$SavePoint;->pos:J

    iget-object v5, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->curSavePoint:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method private resetSensorSetting()V
    .locals 3

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->settingView:Landroid/view/View;

    if-nez v1, :cond_0

    const-string v1, "layout_inflater"

    invoke-virtual {p0, v1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    const v1, 0x7f030003    # com.konka.kkvideoplayer.R.layout.player_setting

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->settingView:Landroid/view/View;

    :cond_0
    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mResumePlay:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x0

    iput v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mSubtitleNum:I

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->setListenTrack()V

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->setListenScreenMode()V

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->setListenImageMode()V

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->setListenCircleMode()V

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->setListenSubtitle()V

    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->vv:Lcom/konka/kkvideoplayer/VideoView;

    iget v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mTrackClick:I

    invoke-virtual {v1, v2}, Lcom/konka/kkvideoplayer/VideoView;->setAudioTrack(I)V

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->vv:Lcom/konka/kkvideoplayer/VideoView;

    iget v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mSubtitleClick:I

    invoke-virtual {v1, v2}, Lcom/konka/kkvideoplayer/VideoView;->setSubtitleTrack(I)V

    goto :goto_0
.end method

.method private saveAndResume(I)V
    .locals 6
    .param p1    # I

    move v0, p1

    const/4 v5, 0x1

    if-ne p1, v5, :cond_0

    iget v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mScreenClick:I

    iget v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mImageClick:I

    iget v4, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mTrackClick:I

    iget v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mCircleClick:I

    iget v5, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mSubtitleClick:I

    :cond_0
    return-void
.end method

.method private saveHangUpPoint(J)V
    .locals 1
    .param p1    # J

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->currPlay:Lcom/konka/kkvideoplayer/CurrPlayInfo;

    iput-wide p1, v0, Lcom/konka/kkvideoplayer/CurrPlayInfo;->start:J

    return-void
.end method

.method private saveValueInt(Ljava/lang/String;I)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # I

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->sp:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method private setBtnsOnFocusListeners()V
    .locals 2

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->btnPre:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->onFocusChange:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->btnPause:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->onFocusChange:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->btnSetting:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->onFocusChange:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->btnNext:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->onFocusChange:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->btnStop:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->onFocusChange:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    return-void
.end method

.method private setBtnsOnKeyListeners()V
    .locals 2

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->btnPre:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->OnKey:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->btnPause:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->OnKey:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->btnSetting:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->OnKey:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->btnNext:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->OnKey:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->btnStop:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->OnKey:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->seekBar:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->OnKey:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    return-void
.end method

.method private setFullScreen()V
    .locals 2

    const-string v0, "set full screen =============\n"

    invoke-static {v0}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    iget v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mScreenClick:I

    if-nez v0, :cond_0

    const-string v0, "already SCREEN_FULL_RESIZE !!!!!!!!!!!!! do nothing"

    invoke-static {v0}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mScreenClick:I

    const v0, 0x7f0a0063    # com.konka.kkvideoplayer.R.string.entry_screen_mode

    invoke-virtual {p0, v0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mScreenClick:I

    invoke-direct {p0, v0, v1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->saveValueInt(Ljava/lang/String;I)V

    iget v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mScreenClick:I

    invoke-direct {p0, v0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->setVideoScale(I)V

    goto :goto_0
.end method

.method private setFullScreenWithoutSave()V
    .locals 1

    const-string v0, "set full screen without save =============\n"

    invoke-static {v0}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    iget v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mScreenClick:I

    if-nez v0, :cond_0

    const-string v0, "already SCREEN_FULL_RESIZE !!!!!!!!!!!!! do nothing"

    invoke-static {v0}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->setVideoScale(I)V

    goto :goto_0
.end method

.method private setListenCircleMode()V
    .locals 7

    const/high16 v6, 0x41700000    # 15.0f

    const/high16 v5, 0x41500000    # 13.0f

    const/high16 v4, 0x41200000    # 10.0f

    const v3, 0x7f09002e    # com.konka.kkvideoplayer.R.id.circle_modeStr

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->settingView:Landroid/view/View;

    const v1, 0x7f09002f    # com.konka.kkvideoplayer.R.id.setting_circle

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->btnCircle:Landroid/widget/Button;

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const v2, 0x7f0a0033    # com.konka.kkvideoplayer.R.string.circle_mode_sort

    invoke-virtual {p0, v2}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const v2, 0x7f0a0034    # com.konka.kkvideoplayer.R.string.circle_mode_single

    invoke-virtual {p0, v2}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const v2, 0x7f0a0035    # com.konka.kkvideoplayer.R.string.circle_mode_total

    invoke-virtual {p0, v2}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mCircleStr:[Ljava/lang/String;

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->btnCircle:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mCircleStr:[Ljava/lang/String;

    iget v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mCircleClick:I

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->btnCircle:Landroid/widget/Button;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(I)V

    invoke-virtual {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a002b    # com.konka.kkvideoplayer.R.string.str_sys_language

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->CURRENT_LANGUAGE:Ljava/lang/String;

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->CURRENT_LANGUAGE:Ljava/lang/String;

    const-string v1, "ar"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->settingView:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->circleTxt:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->circleTxt:Landroid/widget/TextView;

#    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setTextSize(F)V

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->btnCircle:Landroid/widget/Button;

#    invoke-virtual {v0, v6}, Landroid/widget/Button;->setTextSize(F)V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->settingView:Landroid/view/View;

    const v1, 0x7f09002d    # com.konka.kkvideoplayer.R.id.setting_row_circle

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TableRow;

    iput-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->rowCircle:Landroid/widget/TableRow;

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->btnCircle:Landroid/widget/Button;

    new-instance v1, Lcom/konka/kkvideoplayer/VideoPlayerMain$23;

    invoke-direct {v1, p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain$23;-><init>(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->btnCircle:Landroid/widget/Button;

    new-instance v1, Lcom/konka/kkvideoplayer/VideoPlayerMain$24;

    invoke-direct {v1, p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain$24;-><init>(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->btnCircle:Landroid/widget/Button;

    new-instance v1, Lcom/konka/kkvideoplayer/VideoPlayerMain$25;

    invoke-direct {v1, p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain$25;-><init>(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->CURRENT_LANGUAGE:Ljava/lang/String;

    const-string v1, "fa"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->CURRENT_LANGUAGE:Ljava/lang/String;

    const-string v1, "kd"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->settingView:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->circleTxt:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->circleTxt:Landroid/widget/TextView;

#    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setTextSize(F)V

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->btnCircle:Landroid/widget/Button;

#    invoke-virtual {v0, v5}, Landroid/widget/Button;->setTextSize(F)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->CURRENT_LANGUAGE:Ljava/lang/String;

    const-string v1, "ru"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->settingView:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->circleTxt:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->circleTxt:Landroid/widget/TextView;

#    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextSize(F)V

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->btnCircle:Landroid/widget/Button;

#    invoke-virtual {v0, v4}, Landroid/widget/Button;->setTextSize(F)V

    goto :goto_0
.end method

.method private setListenImageMode()V
    .locals 9

    const/4 v8, 0x0

    const/high16 v7, 0x41700000    # 15.0f

    const/high16 v6, 0x41500000    # 13.0f

    const/high16 v5, 0x41200000    # 10.0f

    const v4, 0x7f09002b    # com.konka.kkvideoplayer.R.id.image_modeStr

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->settingView:Landroid/view/View;

    const v2, 0x7f09002c    # com.konka.kkvideoplayer.R.id.setting_image_mode

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->btnImage:Landroid/widget/Button;

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/String;

    const v2, 0x7f0a002c    # com.konka.kkvideoplayer.R.string.image_mode_auto

    invoke-virtual {p0, v2}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v8

    const/4 v2, 0x1

    const v3, 0x7f0a002d    # com.konka.kkvideoplayer.R.string.image_mode_stand

    invoke-virtual {p0, v3}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const v3, 0x7f0a002e    # com.konka.kkvideoplayer.R.string.image_mode_soft

    invoke-virtual {p0, v3}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const v3, 0x7f0a002f    # com.konka.kkvideoplayer.R.string.image_mode_splendid

    invoke-virtual {p0, v3}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    iput-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mImageStr:[Ljava/lang/String;

    invoke-static {p0}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getPictureManagerInstance()Lcom/konka/kkinterface/tv/PictureDesk;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/PictureDesk;->GetPictureModeIdx()Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "DDDDDDDDDDDDDDDDDD "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    invoke-static {}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->$SWITCH_TABLE$com$konka$kkinterface$tv$DataBaseDesk$EN_MS_PICTURE()[I

    move-result-object v1

    invoke-virtual {v0}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    :goto_0
    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->btnImage:Landroid/widget/Button;

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setTextColor(I)V

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->btnImage:Landroid/widget/Button;

    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mImageStr:[Ljava/lang/String;

    iget v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mImageClick:I

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a002b    # com.konka.kkvideoplayer.R.string.str_sys_language

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->CURRENT_LANGUAGE:Ljava/lang/String;

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->CURRENT_LANGUAGE:Ljava/lang/String;

    const-string v2, "ar"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->settingView:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->imageTxt:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->imageTxt:Landroid/widget/TextView;

#    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setTextSize(F)V

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->btnImage:Landroid/widget/Button;

#    invoke-virtual {v1, v7}, Landroid/widget/Button;->setTextSize(F)V

    :cond_0
    :goto_1
    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->settingView:Landroid/view/View;

    const v2, 0x7f09002a    # com.konka.kkvideoplayer.R.id.setting_row_image_mode

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TableRow;

    iput-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->rowImage:Landroid/widget/TableRow;

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->btnImage:Landroid/widget/Button;

    new-instance v2, Lcom/konka/kkvideoplayer/VideoPlayerMain$20;

    invoke-direct {v2, p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain$20;-><init>(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->btnImage:Landroid/widget/Button;

    new-instance v2, Lcom/konka/kkvideoplayer/VideoPlayerMain$21;

    invoke-direct {v2, p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain$21;-><init>(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->btnImage:Landroid/widget/Button;

    new-instance v2, Lcom/konka/kkvideoplayer/VideoPlayerMain$22;

    invoke-direct {v2, p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain$22;-><init>(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void

    :pswitch_0
    const/4 v1, 0x1

    iput v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mImageClick:I

    goto :goto_0

    :pswitch_1
    iput v8, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mImageClick:I

    goto :goto_0

    :pswitch_2
    const/4 v1, 0x2

    iput v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mImageClick:I

    goto :goto_0

    :pswitch_3
    const/4 v1, 0x3

    iput v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mImageClick:I

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->CURRENT_LANGUAGE:Ljava/lang/String;

    const-string v2, "fa"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->CURRENT_LANGUAGE:Ljava/lang/String;

    const-string v2, "kd"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_2
    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->settingView:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->imageTxt:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->imageTxt:Landroid/widget/TextView;

#    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setTextSize(F)V

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->btnImage:Landroid/widget/Button;

#    invoke-virtual {v1, v6}, Landroid/widget/Button;->setTextSize(F)V

    goto :goto_1

    :cond_3
    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->CURRENT_LANGUAGE:Ljava/lang/String;

    const-string v2, "ru"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->settingView:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->imageTxt:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->imageTxt:Landroid/widget/TextView;

#    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setTextSize(F)V

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->btnImage:Landroid/widget/Button;

#    invoke-virtual {v1, v5}, Landroid/widget/Button;->setTextSize(F)V

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private setListenScreenMode()V
    .locals 8

    const/4 v7, 0x0

    const/high16 v6, 0x41700000    # 15.0f

    const/high16 v5, 0x41500000    # 13.0f

    const/high16 v4, 0x41200000    # 10.0f

    const v3, 0x7f090028    # com.konka.kkvideoplayer.R.id.screen_modeStr

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->settingView:Landroid/view/View;

    const v1, 0x7f090029    # com.konka.kkvideoplayer.R.id.setting_screen_mode

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->btnScreen:Landroid/widget/Button;

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const v1, 0x7f0a0032    # com.konka.kkvideoplayer.R.string.screen_mode_full

    invoke-virtual {p0, v1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v7

    const/4 v1, 0x1

    const v2, 0x7f0a0030    # com.konka.kkvideoplayer.R.string.screen_mode_auto

    invoke-virtual {p0, v2}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const v2, 0x7f0a0031    # com.konka.kkvideoplayer.R.string.screen_mode_orig

    invoke-virtual {p0, v2}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mScreenStr:[Ljava/lang/String;

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->getIfFBScreenModeSet()I

    move-result v0

    if-eqz v0, :cond_0

    iput v7, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mScreenClick:I

    :cond_0
    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->btnScreen:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mScreenStr:[Ljava/lang/String;

    iget v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mScreenClick:I

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->btnScreen:Landroid/widget/Button;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(I)V

    invoke-virtual {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a002b    # com.konka.kkvideoplayer.R.string.str_sys_language

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->CURRENT_LANGUAGE:Ljava/lang/String;

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->CURRENT_LANGUAGE:Ljava/lang/String;

    const-string v1, "ar"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->settingView:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->screenTxt:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->screenTxt:Landroid/widget/TextView;

#    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setTextSize(F)V

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->btnScreen:Landroid/widget/Button;

#    invoke-virtual {v0, v6}, Landroid/widget/Button;->setTextSize(F)V

    const-string v0, "===================="

    const-string v1, "ar"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->settingView:Landroid/view/View;

    const v1, 0x7f090027    # com.konka.kkvideoplayer.R.id.setting_row_screen_mode

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TableRow;

    iput-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->rowScreen:Landroid/widget/TableRow;

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->btnScreen:Landroid/widget/Button;

    new-instance v1, Lcom/konka/kkvideoplayer/VideoPlayerMain$17;

    invoke-direct {v1, p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain$17;-><init>(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->btnScreen:Landroid/widget/Button;

    new-instance v1, Lcom/konka/kkvideoplayer/VideoPlayerMain$18;

    invoke-direct {v1, p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain$18;-><init>(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->btnScreen:Landroid/widget/Button;

    new-instance v1, Lcom/konka/kkvideoplayer/VideoPlayerMain$19;

    invoke-direct {v1, p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain$19;-><init>(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void

    :cond_2
    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->CURRENT_LANGUAGE:Ljava/lang/String;

    const-string v1, "fa"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->CURRENT_LANGUAGE:Ljava/lang/String;

    const-string v1, "kd"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_3
    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->settingView:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->screenTxt:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->screenTxt:Landroid/widget/TextView;

#    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setTextSize(F)V

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->btnScreen:Landroid/widget/Button;

#    invoke-virtual {v0, v5}, Landroid/widget/Button;->setTextSize(F)V

    const-string v0, "===================="

    const-string v1, "kd"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->CURRENT_LANGUAGE:Ljava/lang/String;

    const-string v1, "ru"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->settingView:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->screenTxt:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->screenTxt:Landroid/widget/TextView;

#    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextSize(F)V

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->btnScreen:Landroid/widget/Button;

#    invoke-virtual {v0, v4}, Landroid/widget/Button;->setTextSize(F)V

    goto :goto_0
.end method

.method private setListenSubtitle()V
    .locals 8

    const/high16 v7, 0x41700000    # 15.0f

    const/high16 v6, 0x41500000    # 13.0f

    const/high16 v5, 0x41200000    # 10.0f

    const v4, 0x7f090031    # com.konka.kkvideoplayer.R.id.subtitleStr

    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->settingView:Landroid/view/View;

    const v3, 0x7f090032    # com.konka.kkvideoplayer.R.id.setting_subtitle

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->btnSubtitle:Landroid/widget/Button;

    iget v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mSubtitleNum:I

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->vv:Lcom/konka/kkvideoplayer/VideoView;

    iget-object v2, v2, Lcom/konka/kkvideoplayer/VideoView;->mMediaPlayer:Lcom/mstar/android/media/MMediaPlayer;

    invoke-virtual {v2}, Lcom/mstar/android/media/MMediaPlayer;->getAllSubtitleTrackInfo()Lcom/mstar/android/media/SubtitleTrackInfo;

    move-result-object v1

    if-nez v1, :cond_0

    const-string v2, "SubtitleTrackInfo subInfo !!!!!!!!! null"

    invoke-static {v2}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v1}, Lcom/mstar/android/media/SubtitleTrackInfo;->getAllSubtitleCount()I

    move-result v2

    iput v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mSubtitleNum:I

    :cond_1
    iget v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mSubtitleNum:I

    const/4 v3, 0x1

    if-lt v2, v3, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "1/"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mSubtitleNum:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->btnSubtitle:Landroid/widget/Button;

    invoke-virtual {v2, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->btnSubtitle:Landroid/widget/Button;

    const/4 v3, -0x1

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setTextColor(I)V

    invoke-virtual {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a002b    # com.konka.kkvideoplayer.R.string.str_sys_language

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->CURRENT_LANGUAGE:Ljava/lang/String;

    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->CURRENT_LANGUAGE:Ljava/lang/String;

    const-string v3, "ar"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->settingView:Landroid/view/View;

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->subtitleTxt:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->subtitleTxt:Landroid/widget/TextView;

#    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setTextSize(F)V

    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->btnSubtitle:Landroid/widget/Button;

#    invoke-virtual {v2, v7}, Landroid/widget/Button;->setTextSize(F)V

    :cond_2
    :goto_2
    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->settingView:Landroid/view/View;

    const v3, 0x7f090030    # com.konka.kkvideoplayer.R.id.setting_row_subtitle

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TableRow;

    iput-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->rowSubtitle:Landroid/widget/TableRow;

    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->btnSubtitle:Landroid/widget/Button;

    new-instance v3, Lcom/konka/kkvideoplayer/VideoPlayerMain$26;

    invoke-direct {v3, p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain$26;-><init>(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->btnSubtitle:Landroid/widget/Button;

    new-instance v3, Lcom/konka/kkvideoplayer/VideoPlayerMain$27;

    invoke-direct {v3, p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain$27;-><init>(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->btnSubtitle:Landroid/widget/Button;

    new-instance v3, Lcom/konka/kkvideoplayer/VideoPlayerMain$28;

    invoke-direct {v3, p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain$28;-><init>(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    :cond_3
    const-string v0, "0/0"

    goto :goto_1

    :cond_4
    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->CURRENT_LANGUAGE:Ljava/lang/String;

    const-string v3, "kd"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->CURRENT_LANGUAGE:Ljava/lang/String;

    const-string v3, "fa"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    :cond_5
    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->settingView:Landroid/view/View;

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->subtitleTxt:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->subtitleTxt:Landroid/widget/TextView;

#    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setTextSize(F)V

    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->btnSubtitle:Landroid/widget/Button;

#    invoke-virtual {v2, v6}, Landroid/widget/Button;->setTextSize(F)V

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->CURRENT_LANGUAGE:Ljava/lang/String;

    const-string v3, "ru"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->settingView:Landroid/view/View;

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->subtitleTxt:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->subtitleTxt:Landroid/widget/TextView;

#    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setTextSize(F)V

    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->btnSubtitle:Landroid/widget/Button;

#    invoke-virtual {v2, v5}, Landroid/widget/Button;->setTextSize(F)V

    goto :goto_2
.end method

.method private setListenTrack()V
    .locals 10

    const/4 v5, 0x0

    const/high16 v9, 0x41700000    # 15.0f

    const/high16 v8, 0x41500000    # 13.0f

    const/high16 v7, 0x41200000    # 10.0f

    const v6, 0x7f090025    # com.konka.kkvideoplayer.R.id.setting_trackStr

    iput v5, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->trackCount:I

    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->settingView:Landroid/view/View;

    const v4, 0x7f090026    # com.konka.kkvideoplayer.R.id.setting_track

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->btnTrack:Landroid/widget/Button;

    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->vv:Lcom/konka/kkvideoplayer/VideoView;

    invoke-virtual {v3, v5}, Lcom/konka/kkvideoplayer/VideoView;->getAudioTrackInfo(Z)Lcom/mstar/android/media/AudioTrackInfo;

    move-result-object v2

    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->vv:Lcom/konka/kkvideoplayer/VideoView;

    iget-object v3, v3, Lcom/konka/kkvideoplayer/VideoView;->mMediaPlayer:Lcom/mstar/android/media/MMediaPlayer;

    const/4 v4, 0x1

    const/4 v5, 0x1

    invoke-virtual {v3, v4, v5}, Lcom/mstar/android/media/MMediaPlayer;->getMetadata(ZZ)Landroid/media/Metadata;

    move-result-object v0

    if-nez v0, :cond_1

    const-string v1, "0/0"

    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->btnTrack:Landroid/widget/Button;

    invoke-virtual {v3, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    iget v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->trackCount:I

    if-nez v3, :cond_2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "0/"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->trackCount:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_1
    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->btnTrack:Landroid/widget/Button;

    invoke-virtual {v3, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a002b    # com.konka.kkvideoplayer.R.string.str_sys_language

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->CURRENT_LANGUAGE:Ljava/lang/String;

    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->CURRENT_LANGUAGE:Ljava/lang/String;

    const-string v4, "ar"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->settingView:Landroid/view/View;

    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->trackTxt:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->trackTxt:Landroid/widget/TextView;

#    invoke-virtual {v3, v9}, Landroid/widget/TextView;->setTextSize(F)V

    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->btnTrack:Landroid/widget/Button;

#    invoke-virtual {v3, v9}, Landroid/widget/Button;->setTextSize(F)V

    :cond_0
    :goto_2
    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->btnTrack:Landroid/widget/Button;

    const/4 v4, -0x1

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setTextColor(I)V

    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->settingView:Landroid/view/View;

    const v4, 0x7f090024    # com.konka.kkvideoplayer.R.id.setting_row_track

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TableRow;

    iput-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->rowTrack:Landroid/widget/TableRow;

    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->btnTrack:Landroid/widget/Button;

    new-instance v4, Lcom/konka/kkvideoplayer/VideoPlayerMain$29;

    invoke-direct {v4, p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain$29;-><init>(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->btnTrack:Landroid/widget/Button;

    new-instance v4, Lcom/konka/kkvideoplayer/VideoPlayerMain$30;

    invoke-direct {v4, p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain$30;-><init>(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->btnTrack:Landroid/widget/Button;

    new-instance v4, Lcom/konka/kkvideoplayer/VideoPlayerMain$31;

    invoke-direct {v4, p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain$31;-><init>(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void

    :cond_1
    const/16 v3, 0x23

    invoke-virtual {v0, v3}, Landroid/media/Metadata;->getInt(I)I

    move-result v3

    iput v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->trackCount:I

    goto :goto_0

    :cond_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "1/"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->trackCount:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_3
    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->CURRENT_LANGUAGE:Ljava/lang/String;

    const-string v4, "kd"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->CURRENT_LANGUAGE:Ljava/lang/String;

    const-string v4, "fa"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    :cond_4
    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->settingView:Landroid/view/View;

    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->trackTxt:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->trackTxt:Landroid/widget/TextView;

#    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setTextSize(F)V

    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->btnTrack:Landroid/widget/Button;

#    invoke-virtual {v3, v8}, Landroid/widget/Button;->setTextSize(F)V

    goto :goto_2

    :cond_5
    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->CURRENT_LANGUAGE:Ljava/lang/String;

    const-string v4, "ru"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->settingView:Landroid/view/View;

    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->trackTxt:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->trackTxt:Landroid/widget/TextView;

#    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setTextSize(F)V

    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->btnTrack:Landroid/widget/Button;

#    invoke-virtual {v3, v7}, Landroid/widget/Button;->setTextSize(F)V

    goto/16 :goto_2
.end method

.method private setMenuHideTimer()V
    .locals 4

    const/16 v3, 0x8

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->myHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->myHandler:Landroid/os/Handler;

    const-wide/16 v1, 0x2710

    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    return-void
.end method

.method private setOldScreen()V
    .locals 3

    const-string v0, "set old screen =============\n"

    invoke-static {v0}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    const v0, 0x7f0a005c    # com.konka.kkvideoplayer.R.string.entrysetting

    invoke-virtual {p0, v0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->sp:Landroid/content/SharedPreferences;

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->sp:Landroid/content/SharedPreferences;

    const-string v1, "entry_screen_mode"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mScreenClick:I

    iget v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mScreenClick:I

    invoke-direct {p0, v0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->setVideoScale(I)V

    return-void
.end method

.method private setProgress()I
    .locals 9

    const/4 v8, 0x0

    iget-object v4, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->vv:Lcom/konka/kkvideoplayer/VideoView;

    if-nez v4, :cond_1

    const-string v4, "SETPROGRESS FALSE"

    invoke-static {v4}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return v8

    :cond_1
    iget-object v4, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->vv:Lcom/konka/kkvideoplayer/VideoView;

    invoke-virtual {v4}, Lcom/konka/kkvideoplayer/VideoView;->getCurrentPosition()I

    move-result v3

    iget-object v4, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->vv:Lcom/konka/kkvideoplayer/VideoView;

    invoke-virtual {v4}, Lcom/konka/kkvideoplayer/VideoView;->getDuration()I

    move-result v0

    if-lez v3, :cond_2

    int-to-long v4, v3

    invoke-direct {p0, v4, v5}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->saveHangUpPoint(J)V

    :cond_2
    iget-object v4, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->seekBar:Landroid/widget/SeekBar;

    if-eqz v4, :cond_3

    if-lez v0, :cond_3

    const-wide/16 v4, 0x3e8

    int-to-long v6, v3

    mul-long/2addr v4, v6

    int-to-long v6, v0

    div-long v1, v4, v6

    iget-object v4, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->seekBar:Landroid/widget/SeekBar;

    long-to-int v5, v1

    invoke-virtual {v4, v5}, Landroid/widget/SeekBar;->setProgress(I)V

    :cond_3
    iget-object v4, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mEndTime:Landroid/widget/TextView;

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mEndTime:Landroid/widget/TextView;

    invoke-direct {p0, v0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->stringForTime(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_4
    iget-object v4, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mCurrentTime:Landroid/widget/TextView;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mCurrentTime:Landroid/widget/TextView;

    invoke-direct {p0, v3}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->stringForTime(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private setSavePoint()V
    .locals 3

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->currPlay:Lcom/konka/kkvideoplayer/CurrPlayInfo;

    iget-object v0, v0, Lcom/konka/kkvideoplayer/CurrPlayInfo;->url:Ljava/lang/String;

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->vv:Lcom/konka/kkvideoplayer/VideoView;

    invoke-virtual {v1}, Lcom/konka/kkvideoplayer/VideoView;->getCurrentPosition()I

    move-result v1

    int-to-long v1, v1

    invoke-direct {p0, v0, v1, v2}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->writeSavePoint(Ljava/lang/String;J)V

    return-void
.end method

.method private setSubTitleText(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->setSubtitleLayout()V

    const v1, 0x7f090022    # com.konka.kkvideoplayer.R.id.text_subtitle

    invoke-virtual {p0, v1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private setSubtitleLayout()V
    .locals 5

    const v1, 0x7f090022    # com.konka.kkvideoplayer.R.id.text_subtitle

    invoke-virtual {p0, v1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-boolean v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->enablePip:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->pipSkin:Lcom/mstar/android/tv/TvPipPopManager;

    invoke-virtual {v1}, Lcom/mstar/android/tv/TvPipPopManager;->isPopEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/widget/TextView;->getPaddingLeft()I

    move-result v1

    invoke-virtual {v0}, Landroid/widget/TextView;->getPaddingTop()I

    move-result v2

    const/16 v3, 0x1f4

    invoke-virtual {v0}, Landroid/widget/TextView;->getPaddingBottom()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v0}, Landroid/widget/TextView;->getPaddingLeft()I

    move-result v1

    invoke-virtual {v0}, Landroid/widget/TextView;->getPaddingTop()I

    move-result v2

    const/4 v3, 0x0

    invoke-virtual {v0}, Landroid/widget/TextView;->getPaddingBottom()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    goto :goto_0
.end method

.method private setTextViewVisible(Landroid/widget/TextView;Z)V
    .locals 1
    .param p1    # Landroid/widget/TextView;
    .param p2    # Z

    if-eqz p2, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private setTitle(Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;

    const/16 v4, 0x46

    const-string v2, "/"

    invoke-virtual {p1, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    const-string v2, "."

    invoke-virtual {p1, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    if-le v1, v0, :cond_0

    const/4 v1, 0x0

    :cond_0
    add-int/lit8 v2, v1, 0x1

    invoke-virtual {p1, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mTitle:Ljava/lang/String;

    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mTitle:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-le v2, v4, :cond_1

    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mTitle:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mTitle:Ljava/lang/String;

    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mTitle:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "..."

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mTitle:Ljava/lang/String;

    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "title:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mTitle:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->textView:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mTitle:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private setVideoScale(I)V
    .locals 16
    .param p1    # I

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "flag setvideoScale ="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->btnScreen:Landroid/widget/Button;

    if-eqz v1, :cond_0

    const-string v1, "setVideoScale menu set mScreenClick"

    invoke-static {v1}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->btnScreen:Landroid/widget/Button;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mScreenStr:[Ljava/lang/String;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mScreenClick:I

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->is1280x720(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v1, 0x556

    sput v1, Lcom/konka/kkvideoplayer/VideoPlayerMain;->screenWidth:I

    const/16 v1, 0x2d0

    sput v1, Lcom/konka/kkvideoplayer/VideoPlayerMain;->screenHeight:I

    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->vv:Lcom/konka/kkvideoplayer/VideoView;

    invoke-virtual {v1}, Lcom/konka/kkvideoplayer/VideoView;->getVideoWidth()I

    move-result v1

    int-to-double v6, v1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->vv:Lcom/konka/kkvideoplayer/VideoView;

    invoke-virtual {v1}, Lcom/konka/kkvideoplayer/VideoView;->getVideoHeight()I

    move-result v1

    int-to-double v8, v1

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    const-string v1, "tank"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "screenWidth: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v3, Lcom/konka/kkvideoplayer/VideoPlayerMain;->screenWidth:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " screenHeight: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/konka/kkvideoplayer/VideoPlayerMain;->screenHeight:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "tank"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "VideoWidth: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " VideoHeight: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8, v9}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->vv:Lcom/konka/kkvideoplayer/VideoView;

    sget v2, Lcom/konka/kkvideoplayer/VideoPlayerMain;->screenWidth:I

    int-to-double v2, v2

    sget v4, Lcom/konka/kkvideoplayer/VideoPlayerMain;->screenHeight:I

    int-to-double v4, v4

    invoke-virtual/range {v1 .. v9}, Lcom/konka/kkvideoplayer/VideoView;->calculateZoom(DDDD)D

    move-result-wide v14

    sget v1, Lcom/konka/kkvideoplayer/VideoPlayerMain;->screenWidth:I

    int-to-double v1, v1

    mul-double v3, v6, v14

    sub-double/2addr v1, v3

    const-wide/high16 v3, 0x4000000000000000L    # 2.0

    div-double/2addr v1, v3

    double-to-int v10, v1

    sget v1, Lcom/konka/kkvideoplayer/VideoPlayerMain;->screenHeight:I

    int-to-double v1, v1

    mul-double v3, v8, v14

    sub-double/2addr v1, v3

    const-wide/high16 v3, 0x4000000000000000L    # 2.0

    div-double/2addr v1, v3

    double-to-int v12, v1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "video width"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "video height:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8, v9}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "zoom:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v14, v15}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->vv:Lcom/konka/kkvideoplayer/VideoView;

    mul-double v2, v6, v14

    double-to-int v2, v2

    mul-double v3, v8, v14

    double-to-int v3, v3

    invoke-virtual {v1, v10, v12, v2, v3}, Lcom/konka/kkvideoplayer/VideoView;->setVideoScaleFrameLayout(IIII)V

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "**!!!!!!****video width"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "video height:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8, v9}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "zoom:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v14, v15}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v2, 0x400

    invoke-virtual {v1, v2}, Landroid/view/Window;->addFlags(I)V

    goto/16 :goto_0

    :pswitch_1
    const-string v1, "tank"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "screenWidth: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v3, Lcom/konka/kkvideoplayer/VideoPlayerMain;->screenWidth:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " screenHeight: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/konka/kkvideoplayer/VideoPlayerMain;->screenHeight:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->vv:Lcom/konka/kkvideoplayer/VideoView;

    const/4 v2, 0x0

    const/4 v3, 0x0

    sget v4, Lcom/konka/kkvideoplayer/VideoPlayerMain;->screenWidth:I

    sget v5, Lcom/konka/kkvideoplayer/VideoPlayerMain;->screenHeight:I

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/konka/kkvideoplayer/VideoView;->setVideoScaleFrameLayout(IIII)V

    invoke-virtual/range {p0 .. p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v2, 0x400

    invoke-virtual {v1, v2}, Landroid/view/Window;->addFlags(I)V

    goto/16 :goto_0

    :pswitch_2
    sget v1, Lcom/konka/kkvideoplayer/VideoPlayerMain;->screenWidth:I

    int-to-double v1, v1

    sub-double/2addr v1, v6

    const-wide/high16 v3, 0x4000000000000000L    # 2.0

    div-double/2addr v1, v3

    double-to-int v11, v1

    sget v1, Lcom/konka/kkvideoplayer/VideoPlayerMain;->screenHeight:I

    int-to-double v1, v1

    sub-double/2addr v1, v8

    const-wide/high16 v3, 0x4000000000000000L    # 2.0

    div-double/2addr v1, v3

    double-to-int v13, v1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->vv:Lcom/konka/kkvideoplayer/VideoView;

    double-to-int v2, v6

    double-to-int v3, v8

    invoke-virtual {v1, v11, v13, v2, v3}, Lcom/konka/kkvideoplayer/VideoView;->setVideoScaleFrameLayout(IIII)V

    const-string v1, "tank"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "screenWidth: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v3, Lcom/konka/kkvideoplayer/VideoPlayerMain;->screenWidth:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " screenHeight: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/konka/kkvideoplayer/VideoPlayerMain;->screenHeight:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " VideoWidth1:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "VideoHeight1:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8, v9}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual/range {p0 .. p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v2, 0x400

    invoke-virtual {v1, v2}, Landroid/view/Window;->clearFlags(I)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private showPauseStat()V
    .locals 2

    const v1, 0x7f090020    # com.konka.kkvideoplayer.R.id.id_player_stat

    invoke-virtual {p0, v1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const v1, 0x7f02004c    # com.konka.kkvideoplayer.R.drawable.com_stat_pause

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    return-void
.end method

.method private showSettingWindow(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    invoke-static {}, Landroid/os/Looper;->myQueue()Landroid/os/MessageQueue;

    move-result-object v0

    new-instance v1, Lcom/konka/kkvideoplayer/VideoPlayerMain$33;

    invoke-direct {v1, p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain$33;-><init>(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V

    invoke-virtual {v0, v1}, Landroid/os/MessageQueue;->addIdleHandler(Landroid/os/MessageQueue$IdleHandler;)V

    return-void
.end method

.method private stringForTime(I)Ljava/lang/String;
    .locals 9
    .param p1    # I

    const/4 v8, 0x0

    div-int/lit16 v3, p1, 0x3e8

    rem-int/lit8 v2, v3, 0x3c

    div-int/lit8 v4, v3, 0x3c

    rem-int/lit8 v1, v4, 0x3c

    div-int/lit16 v0, v3, 0xe10

    iget-object v4, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mFormatBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->setLength(I)V

    iget-object v4, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mFormatter:Ljava/util/Formatter;

    const-string v5, "%02d:%02d:%02d"

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    const/4 v7, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method private trim(Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p1    # Ljava/lang/String;

    return-object p1
.end method

.method public static unFreeze(Landroid/app/Activity;)V
    .locals 2
    .param p0    # Landroid/app/Activity;

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.konka.tv.hotkey.service.UNFREEZE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method

.method private writeSavePoint(Ljava/lang/String;J)V
    .locals 7
    .param p1    # Ljava/lang/String;
    .param p2    # J

    const v6, 0x7f0a0046    # com.konka.kkvideoplayer.R.string.videosetting_pos1

    const v5, 0x7f0a0045    # com.konka.kkvideoplayer.R.string.videosetting_url1

    const v2, 0x7f0a0044    # com.konka.kkvideoplayer.R.string.videosetting

    invoke-virtual {p0, v2}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    invoke-virtual {p0, v2, v3}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-virtual {p0, v5}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-virtual {p0, v6}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-wide/16 v3, -0x1

    invoke-interface {v1, v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    invoke-virtual {p0, v5}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-virtual {p0, v6}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, p2, p3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method


# virtual methods
.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 10
    .param p1    # Landroid/view/KeyEvent;

    const-wide/16 v8, 0x1f4

    const/16 v7, 0xf

    const/4 v6, 0x3

    const/4 v5, 0x0

    const/4 v2, 0x1

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v3

    if-nez v3, :cond_0

    iget-boolean v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->bTeacCustomer:Z

    if-nez v3, :cond_b

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "dispatchKeyEvent key code="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    sparse-switch v0, :sswitch_data_0

    :cond_0
    :goto_0
    :sswitch_0
    invoke-super {p0, p1}, Lcom/konka/kkvideoplayer/util/SwitchSource;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v2

    :cond_1
    :goto_1
    return v2

    :sswitch_1
    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->seekBar:Landroid/widget/SeekBar;

    invoke-virtual {v2}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, " pre progress"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    add-int/lit8 v2, v1, -0x32

    invoke-direct {p0, v2}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->handleProgressChange(I)V

    goto :goto_0

    :sswitch_2
    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->seekBar:Landroid/widget/SeekBar;

    invoke-virtual {v2}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "fast progress"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    add-int/lit8 v2, v1, 0x32

    invoke-direct {p0, v2}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->handleProgressChange(I)V

    goto :goto_0

    :sswitch_3
    const-string v3, "KKvideoplayer hot key"

    const-string v4, "Hot key Play/Pause"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mPopStat:I

    if-eq v3, v2, :cond_0

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->isMenuShowing()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->hideMenuShow()V

    :cond_2
    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->handlePauseKey()V

    goto :goto_0

    :sswitch_4
    const-string v2, "KKvideoplayer hot key"

    const-string v3, "Hot key stop"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->bMute:Z

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mAudioManager:Landroid/media/AudioManager;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mAudioManager:Landroid/media/AudioManager;

    iget v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->currentVolume:I

    invoke-virtual {v2, v6, v3, v5}, Landroid/media/AudioManager;->setStreamVolume(III)V

    :cond_3
    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->vv:Lcom/konka/kkvideoplayer/VideoView;

    invoke-virtual {v2}, Lcom/konka/kkvideoplayer/VideoView;->stopPlayback()V

    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->vv:Lcom/konka/kkvideoplayer/VideoView;

    invoke-virtual {v2}, Lcom/konka/kkvideoplayer/VideoView;->isPlaying()Z

    move-result v2

    if-eqz v2, :cond_4

    const-string v2, "hello world destroy!!! stop"

    invoke-static {v2}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    :cond_4
    const-string v2, "hello world will exec finish 2"

    invoke-static {v2}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->finish()V

    goto/16 :goto_0

    :sswitch_5
    const-string v2, "KKvideoplayer hot key"

    const-string v3, "Hot key Prev"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->handlePreviousKey()V

    goto/16 :goto_0

    :sswitch_6
    const-string v2, "KKvideoplayer hot key"

    const-string v3, "Hot key Next"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->handleNextKey()V

    goto/16 :goto_0

    :sswitch_7
    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->handleKeyBack()Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_5

    const-string v2, "KeyEvent.KEYCODE_BACK !!!!!! true exit no hint"

    invoke-static {v2}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/konka/kkvideoplayer/util/SwitchSource;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v2

    goto/16 :goto_1

    :cond_5
    const-string v3, "KeyEvent.KEYCODE_BACK !!!!!! false"

    invoke-static {v3}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    goto/16 :goto_1

    :sswitch_8
    const-string v2, "KEYCODE_MENU !!!!!!a "

    invoke-static {v2}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/view/KeyEvent;->isLongPress()Z

    move-result v2

    if-nez v2, :cond_long_press

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->handleKeyMenu()V

    goto :goto_continue

    :cond_long_press
    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->initPopAwindow(Landroid/view/View;)V

    :goto_continue
    invoke-super {p0, p1}, Lcom/konka/kkvideoplayer/util/SwitchSource;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v2

    goto/16 :goto_1

    :sswitch_9
    iget v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mPopStat:I

    if-eq v3, v2, :cond_0

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->isMenuShowing()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->setMenuHideTimer()V

    :sswitch_a
    iget v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mPopStat:I

    if-eq v3, v2, :cond_0

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->isMenuShowing()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->setMenuHideTimer()V

    goto/16 :goto_0

    :cond_6
    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->handlePreviousKey()V

    goto/16 :goto_1

    :cond_7
    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->handleNextKey()V

    goto/16 :goto_1

    :sswitch_b
    iget v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mPopStat:I

    if-eq v3, v2, :cond_0

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->isMenuShowing()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->setMenuHideTimer()V

    goto/16 :goto_0

    :cond_8
    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v2}, Landroid/app/ProgressDialog;->show()V

    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->seekBar:Landroid/widget/SeekBar;

    invoke-virtual {v2}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "back progress"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    add-int/lit8 v2, v1, -0x32

    invoke-direct {p0, v2}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->handleProgressChange(I)V

    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->seekHint:Landroid/app/ProgressDialog;

    const v3, 0x7f0a0026    # com.konka.kkvideoplayer.R.string.seek_back

    invoke-virtual {p0, v3}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->seekHint:Landroid/app/ProgressDialog;

    invoke-virtual {v2}, Landroid/app/ProgressDialog;->show()V

    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->myHandler:Landroid/os/Handler;

    invoke-virtual {v2, v7, v8, v9}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    :sswitch_c
    iget v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mPopStat:I

    if-eq v3, v2, :cond_0

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->isMenuShowing()Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->setMenuHideTimer()V

    goto/16 :goto_0

    :cond_9
    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->seekBar:Landroid/widget/SeekBar;

    invoke-virtual {v2}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "fast progress--"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    add-int/lit8 v2, v1, 0x32

    invoke-direct {p0, v2}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->handleProgressChange(I)V

    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->seekHint:Landroid/app/ProgressDialog;

    const v3, 0x7f0a0025    # com.konka.kkvideoplayer.R.string.seek_fast

    invoke-virtual {p0, v3}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->seekHint:Landroid/app/ProgressDialog;

    invoke-virtual {v2}, Landroid/app/ProgressDialog;->show()V

    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->myHandler:Landroid/os/Handler;

    invoke-virtual {v2, v7, v8, v9}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    :sswitch_d
    iget v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mPopStat:I

    if-eq v3, v2, :cond_0

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->isMenuShowing()Z

    move-result v3

    if-eqz v3, :cond_a

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->setMenuHideTimer()V

    const-string v2, "KEYCODE_ENTER "

    invoke-static {v2}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_a
    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->handlePauseKey()V

    goto/16 :goto_1

    :sswitch_e
    const-string v3, "KEYCODE_VOLUME_UP "

    invoke-static {v3}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    iput v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->hotKeyStat:I

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->hideMenuShow()V

    goto/16 :goto_0

    :sswitch_f
    const-string v3, "KEYCODE_VOLUME_DOWN "

    invoke-static {v3}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    iput v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->hotKeyStat:I

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->hideMenuShow()V

    goto/16 :goto_0

    :sswitch_10
    const-string v3, "KEYCODE_3D_MODE "

    invoke-static {v3}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    iput v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->hotKeyStat:I

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->hideMenuShow()V

    goto/16 :goto_0

    :sswitch_11_
    const/16 v3, 0x200

    invoke-direct {p0, v3}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->handleSubtitleKey(I)V

    goto/16 :goto_0

    :sswitch_12_
    const/16 v3, 0x200

    invoke-direct {p0, v3}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->handleAudioTrackKey(I)V

    goto/16 :goto_0

    :sswitch_13_
    const/16 v3, 0x200

    invoke-direct {p0, v3}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->handleImagModeKey(I)V

    goto/16 :goto_0

    :sswitch_14_
    const/16 v3, 0x200

    invoke-direct {p0, v3}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->handleScreenModeKey(I)V

    goto/16 :goto_0

    :cond_b
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "dispatchKeyEvent key code="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    sparse-switch v0, :sswitch_data_1

    goto/16 :goto_0

    :sswitch_11
    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->handleKeyBack()Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_f

    const-string v2, "KeyEvent.KEYCODE_BACK !!!!!! true exit no hint"

    invoke-static {v2}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/konka/kkvideoplayer/util/SwitchSource;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v2

    goto/16 :goto_1

    :sswitch_12
    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->seekBar:Landroid/widget/SeekBar;

    invoke-virtual {v2}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, " pre progress"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    add-int/lit8 v2, v1, -0x32

    invoke-direct {p0, v2}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->handleProgressChange(I)V

    goto/16 :goto_0

    :sswitch_13
    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->seekBar:Landroid/widget/SeekBar;

    invoke-virtual {v2}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "fast progress"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    add-int/lit8 v2, v1, 0x32

    invoke-direct {p0, v2}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->handleProgressChange(I)V

    goto/16 :goto_0

    :sswitch_14
    const-string v3, "KKvideoplayer hot key"

    const-string v4, "Hot key Play/Pause"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mPopStat:I

    if-eq v3, v2, :cond_0

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->isMenuShowing()Z

    move-result v2

    if-eqz v2, :cond_c

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->hideMenuShow()V

    :cond_c
    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->handlePauseKey()V

    goto/16 :goto_0

    :sswitch_15
    const-string v2, "KKvideoplayer hot key"

    const-string v3, "Hot key stop"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->bMute:Z

    if-eqz v2, :cond_d

    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mAudioManager:Landroid/media/AudioManager;

    if-eqz v2, :cond_d

    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mAudioManager:Landroid/media/AudioManager;

    iget v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->currentVolume:I

    invoke-virtual {v2, v6, v3, v5}, Landroid/media/AudioManager;->setStreamVolume(III)V

    :cond_d
    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->vv:Lcom/konka/kkvideoplayer/VideoView;

    invoke-virtual {v2}, Lcom/konka/kkvideoplayer/VideoView;->stopPlayback()V

    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->vv:Lcom/konka/kkvideoplayer/VideoView;

    invoke-virtual {v2}, Lcom/konka/kkvideoplayer/VideoView;->isPlaying()Z

    move-result v2

    if-eqz v2, :cond_e

    const-string v2, "hello world destroy!!! stop"

    invoke-static {v2}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    :cond_e
    const-string v2, "hello world will exec finish 2"

    invoke-static {v2}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->finish()V

    goto/16 :goto_0

    :sswitch_16
    const-string v2, "KKvideoplayer hot key"

    const-string v3, "Hot key Prev"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->handlePreviousKey()V

    goto/16 :goto_0

    :sswitch_17
    const-string v2, "KKvideoplayer hot key"

    const-string v3, "Hot key Next"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->handleNextKey()V

    goto/16 :goto_0

    :cond_f
    const-string v3, "KeyEvent.KEYCODE_BACK !!!!!! false"

    invoke-static {v3}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    goto/16 :goto_1

    :sswitch_18
    const-string v2, "KEYCODE_MENU !!!!!!a "

    invoke-static {v2}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->handleKeyMenu()V

    invoke-super {p0, p1}, Lcom/konka/kkvideoplayer/util/SwitchSource;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v2

    goto/16 :goto_1

    :sswitch_19
    iget v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mPopStat:I

    if-eq v3, v2, :cond_0

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->isMenuShowing()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->setMenuHideTimer()V

    :sswitch_1a
    iget v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mPopStat:I

    if-eq v3, v2, :cond_0

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->isMenuShowing()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->setMenuHideTimer()V

    goto/16 :goto_0

    :sswitch_1b
    iget v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mPopStat:I

    if-eq v3, v2, :cond_0

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->isMenuShowing()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->setMenuHideTimer()V

    goto/16 :goto_0

    :sswitch_1c
    iget v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mPopStat:I

    if-eq v3, v2, :cond_0

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->isMenuShowing()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->setMenuHideTimer()V

    goto/16 :goto_0

    :sswitch_1d
    iget v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mPopStat:I

    if-eq v3, v2, :cond_0

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->isMenuShowing()Z

    move-result v3

    if-eqz v3, :cond_10

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->setMenuHideTimer()V

    const-string v2, "KEYCODE_ENTER "

    invoke-static {v2}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_10
    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->handlePauseKey()V

    goto/16 :goto_1

    :sswitch_1e
    const-string v3, "KEYCODE_VOLUME_UP "

    invoke-static {v3}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    iput v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->hotKeyStat:I

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->hideMenuShow()V

    goto/16 :goto_0

    :sswitch_1f
    const-string v3, "KEYCODE_VOLUME_DOWN "

    invoke-static {v3}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    iput v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->hotKeyStat:I

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->hideMenuShow()V

    goto/16 :goto_0

    :sswitch_20
    const-string v3, "KEYCODE_3D_MODE "

    invoke-static {v3}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    iput v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->hotKeyStat:I

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->hideMenuShow()V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_7
        0x13 -> :sswitch_9
        0x14 -> :sswitch_a
        0x15 -> :sswitch_b
        0x16 -> :sswitch_c
        0x18 -> :sswitch_e
        0x19 -> :sswitch_f
        0x42 -> :sswitch_d
        0x52 -> :sswitch_8
        0xb7 -> :sswitch_0
        0xb9 -> :sswitch_1
        0xba -> :sswitch_2
        0xce -> :sswitch_10
        0xfb -> :sswitch_12_
        0xfc -> :sswitch_13_
        0x102 -> :sswitch_11_
        0x106 -> :sswitch_6
        0x10e -> :sswitch_14_
        0x12e -> :sswitch_3
        0x12f -> :sswitch_4
        0x130 -> :sswitch_2
        0x131 -> :sswitch_1
        0x133 -> :sswitch_5
        0x205 -> :sswitch_10
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x4 -> :sswitch_11
        0x13 -> :sswitch_19
        0x14 -> :sswitch_1a
        0x15 -> :sswitch_1b
        0x16 -> :sswitch_1c
        0x18 -> :sswitch_1e
        0x19 -> :sswitch_1f
        0x42 -> :sswitch_1d
        0x52 -> :sswitch_18
        0x56 -> :sswitch_15
        0x7e -> :sswitch_14
        0xb7 -> :sswitch_0
        0xce -> :sswitch_20
        0x12e -> :sswitch_17
        0x12f -> :sswitch_12
        0x132 -> :sswitch_16
        0x133 -> :sswitch_13
        0x205 -> :sswitch_20
    .end sparse-switch
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1    # Landroid/view/MotionEvent;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    const-string v0, "dispatchTouchEvent !!"

    invoke-static {v0}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->isMenuShowing()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->drawMenuShow()V

    :cond_0
    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->setMenuHideTimer()V

    :cond_1
    invoke-super {p0, p1}, Lcom/konka/kkvideoplayer/util/SwitchSource;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public dispatchTrackballEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    invoke-super {p0, p1}, Lcom/konka/kkvideoplayer/util/SwitchSource;->dispatchTrackballEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method getImageMode()I
    .locals 4

    const/4 v1, 0x1

    invoke-static {p0}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v2

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getPictureManagerInstance()Lcom/konka/kkinterface/tv/PictureDesk;

    move-result-object v2

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/PictureDesk;->GetPictureModeIdx()Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "DDDDDDDDDDDDDDDDDD "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    invoke-static {}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->$SWITCH_TABLE$com$konka$kkinterface$tv$DataBaseDesk$EN_MS_PICTURE()[I

    move-result-object v2

    invoke-virtual {v0}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    :goto_0
    return v1

    :pswitch_0
    const/4 v1, 0x1

    goto :goto_0

    :pswitch_1
    const/4 v1, 0x0

    goto :goto_0

    :pswitch_2
    const/4 v1, 0x2

    goto :goto_0

    :pswitch_3
    const/4 v1, 0x3

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method han_exit_pop()V
    .locals 1

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->pipSkin:Lcom/mstar/android/tv/TvPipPopManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->pipSkin:Lcom/mstar/android/tv/TvPipPopManager;

    invoke-virtual {v0}, Lcom/mstar/android/tv/TvPipPopManager;->isPopEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->pipSkin:Lcom/mstar/android/tv/TvPipPopManager;

    invoke-virtual {v0}, Lcom/mstar/android/tv/TvPipPopManager;->disablePop()Z

    :cond_0
    return-void
.end method

.method han_pop_open()V
    .locals 7

    const/4 v6, 0x0

    const-string v4, "aaa han_pop_open !!!!!!!!!!!--"

    invoke-static {v4}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->pipSkin:Lcom/mstar/android/tv/TvPipPopManager;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->pipSkin:Lcom/mstar/android/tv/TvPipPopManager;

    invoke-virtual {v4}, Lcom/mstar/android/tv/TvPipPopManager;->getIsPopOn()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tv/TvPipPopManager;->isDualViewEnabled()Z

    move-result v4

    if-eqz v4, :cond_1

    const-string v4, "isDualViewEnabled \u5982\u679c\u662f\u540c\u6b65\u770b\u72b6\u6001\u5c31\u5173\u6389POP"

    invoke-static {v4}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->pipSkin:Lcom/mstar/android/tv/TvPipPopManager;

    invoke-virtual {v4}, Lcom/mstar/android/tv/TvPipPopManager;->disablePop()Z

    iget-object v4, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->pipSkin:Lcom/mstar/android/tv/TvPipPopManager;

    invoke-virtual {v4, v6}, Lcom/mstar/android/tv/TvPipPopManager;->setPopOnFlag(Z)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v4, "han_pop_open !!!!!!!!!!!3 "

    invoke-static {v4}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->commonSkin:Lcom/mstar/android/tv/TvCommonManager;

    invoke-virtual {v4}, Lcom/mstar/android/tv/TvCommonManager;->getCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v1

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "inputSource:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tv/TvPipPopManager;->getSubInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v2

    iget-object v4, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->pipSkin:Lcom/mstar/android/tv/TvPipPopManager;

    invoke-virtual {v4, v6}, Lcom/mstar/android/tv/TvPipPopManager;->getSubWindowSourceList(Z)[I

    move-result-object v3

    const/4 v0, 0x0

    const/4 v0, 0x0

    :goto_1
    array-length v4, v3

    if-lt v0, v4, :cond_3

    :goto_2
    array-length v4, v3

    if-lt v0, v4, :cond_2

    const-string v4, "han_pop_open !!!!!!!!!!!5 "

    invoke-static {v4}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v4

    aget v5, v3, v6

    aget-object v2, v4, v5

    :cond_2
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "subSrc==EnumPipReturn=:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->pipSkin:Lcom/mstar/android/tv/TvPipPopManager;

    invoke-virtual {v4, v1, v2}, Lcom/mstar/android/tv/TvPipPopManager;->enablePopTV(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;

    goto :goto_0

    :cond_3
    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v4

    aget v5, v3, v0

    if-ne v4, v5, :cond_4

    const-string v4, "han_pop_open !!!!!!!!!!!4 "

    invoke-static {v4}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    goto :goto_2

    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public is1280x720(Landroid/content/Context;)Z
    .locals 4
    .param p1    # Landroid/content/Context;

    const-string v2, "window"

    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager;

    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    iget v2, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    const/16 v3, 0x500

    if-ne v2, v3, :cond_0

    iget v2, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    const/16 v3, 0x2d0

    if-ne v2, v3, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public isFileExists(Ljava/lang/String;)Z
    .locals 2
    .param p1    # Ljava/lang/String;

    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method isMVCSource()Z
    .locals 8

    const/4 v4, 0x1

    const/4 v3, 0x0

    const-string v2, "MVC"

    :try_start_0
    iget-object v5, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->vv:Lcom/konka/kkvideoplayer/VideoView;

    iget-object v5, v5, Lcom/konka/kkvideoplayer/VideoView;->mMediaPlayer:Lcom/mstar/android/media/MMediaPlayer;

    const/4 v6, 0x1

    const/4 v7, 0x1

    invoke-virtual {v5, v6, v7}, Lcom/mstar/android/media/MMediaPlayer;->getMetadata(ZZ)Landroid/media/Metadata;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v3

    :cond_1
    const/16 v5, 0x1b

    invoke-virtual {v1, v5}, Landroid/media/Metadata;->has(I)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v5, 0x1b

    invoke-virtual {v1, v5}, Landroid/media/Metadata;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    if-eqz v5, :cond_0

    move v3, v4

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    const/4 v2, 0x1

    const-string v0, "onCreate-----------================"

    invoke-static {v0}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/konka/kkvideoplayer/util/SwitchSource;->onCreate(Landroid/os/Bundle;)V

    invoke-static {p0}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getS3DManagerInstance()Lcom/konka/kkinterface/tv/S3DDesk;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->m3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    invoke-static {p0}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/CommonDesk;->getCustomerInfo()Lcom/konka/kkinterface/tv/CommonDesk$CustomerInfo;

    move-result-object v0

    iget-object v0, v0, Lcom/konka/kkinterface/tv/CommonDesk$CustomerInfo;->strCustomerName:Ljava/lang/String;

    iput-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->AbroadCustomer:Ljava/lang/String;

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->AbroadCustomer:Ljava/lang/String;

    const-string v1, "teac"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->bTeacCustomer:Z

    invoke-virtual {p0, v2}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->requestWindowFeature(I)Z

    invoke-virtual {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setSystemUiVisibility(I)V

    const v0, 0x7f030002    # com.konka.kkvideoplayer.R.layout.player_main

    invoke-virtual {p0, v0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->setContentView(I)V

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->initReceiver()V

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->initMstarSkin()V

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->initHandle()V

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->initMenu()V

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->initVideoPlay()V

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->initProgressDialog()V

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->initShareNetCheck()V

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->getDisplayInfo()V

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->getStoreInfo()V

    invoke-virtual {p0, v2}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->sendStatusBroadCast(I)V

    return-void
.end method

.method protected onDestroy()V
    .locals 0

    invoke-static {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->unFreeze(Landroid/app/Activity;)V

    invoke-super {p0}, Lcom/konka/kkvideoplayer/util/SwitchSource;->onDestroy()V

    invoke-virtual {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->releaseSource()V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const-string v0, "onKeyDown !!!!!!"

    invoke-static {v0}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    invoke-super {p0, p1, p2}, Lcom/konka/kkvideoplayer/util/SwitchSource;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 1
    .param p1    # Landroid/content/Intent;

    invoke-super {p0, p1}, Lcom/konka/kkvideoplayer/util/SwitchSource;->onNewIntent(Landroid/content/Intent;)V

    invoke-virtual {p0, p1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->setIntent(Landroid/content/Intent;)V

    const/4 v0, 0x1

    iput v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->newIntentFlag:I

    const-string v0, "onNewIntent !!!!!!!!!!"

    invoke-static {v0}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    return-void
.end method

.method protected onPause()V
    .locals 0

    invoke-super {p0}, Lcom/konka/kkvideoplayer/util/SwitchSource;->onPause()V

    return-void
.end method

.method protected onResume()V
    .locals 2

    const/4 v1, 0x1

    const-string v0, "onResume !!!!!============"

    invoke-static {v0}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    invoke-super {p0}, Lcom/konka/kkvideoplayer/util/SwitchSource;->onResume()V

    iget v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->hotKeyStat:I

    if-eq v0, v1, :cond_1

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tv/TvPipPopManager;->isPipEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->han_pop_open()V

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->pipSkin:Lcom/mstar/android/tv/TvPipPopManager;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->pipSkin:Lcom/mstar/android/tv/TvPipPopManager;

    invoke-virtual {v0}, Lcom/mstar/android/tv/TvPipPopManager;->getIsPopOn()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "===== init to double channel"

    invoke-static {v0}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->reInitMenu(I)V

    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->gotoHangUpPoint()V

    :cond_1
    const/4 v0, 0x0

    iput v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->hotKeyStat:I

    return-void

    :cond_2
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->reInitMenu(I)V

    goto :goto_0
.end method

.method protected onStart()V
    .locals 2

    const-string v1, "onStart ---------------======================"

    invoke-static {v1}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    invoke-super {p0}, Lcom/konka/kkvideoplayer/util/SwitchSource;->onStart()V

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.konka.smartcontrol.palyer_start"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->sendBroadcast(Landroid/content/Intent;)V

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->initVAFBroadcastReceiver()V

    return-void
.end method

.method protected onStop()V
    .locals 4

    const-string v1, "onStop new ============="

    invoke-static {v1}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->vv:Lcom/konka/kkvideoplayer/VideoView;

    if-eqz v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onStop !!! free mplayer  time: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->currPlay:Lcom/konka/kkvideoplayer/CurrPlayInfo;

    iget-wide v2, v2, Lcom/konka/kkvideoplayer/CurrPlayInfo;->start:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    iget v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->hotKeyStat:I

    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->han_exit_pop()V

    :cond_0
    const/4 v1, 0x0

    iput v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->hotKeyStat:I

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->vv:Lcom/konka/kkvideoplayer/VideoView;

    invoke-virtual {v1}, Lcom/konka/kkvideoplayer/VideoView;->offSubtitleTrack()V

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->vv:Lcom/konka/kkvideoplayer/VideoView;

    invoke-virtual {v1}, Lcom/konka/kkvideoplayer/VideoView;->stopPlayback()V

    :cond_1
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.konka.smartcontrol.palyer_exit"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->sendBroadcast(Landroid/content/Intent;)V

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    invoke-super {p0}, Lcom/konka/kkvideoplayer/util/SwitchSource;->onStop()V

    return-void
.end method

.method public pingHost(Ljava/lang/String;I)Z
    .locals 11
    .param p1    # Ljava/lang/String;
    .param p2    # I

    const/4 v8, 0x0

    const/4 v2, 0x0

    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v7

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "ping -w "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    :try_start_0
    invoke-virtual {v7, v6}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v5

    if-nez v5, :cond_0

    :try_start_1
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    :goto_0
    return v8

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    :cond_0
    :try_start_2
    new-instance v3, Ljava/io/BufferedReader;

    new-instance v9, Ljava/io/InputStreamReader;

    invoke-virtual {v5}, Ljava/lang/Process;->getInputStream()Ljava/io/InputStream;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v3, v9}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    const/4 v4, 0x0

    :try_start_3
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v4

    const-string v9, "bytes from"

    invoke-virtual {v4, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_1

    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, " ok"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_7
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :try_start_4
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    :goto_1
    const/4 v8, 0x1

    move-object v2, v3

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    :catch_2
    move-exception v1

    :goto_2
    :try_start_5
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    goto :goto_0

    :catch_3
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    :catchall_0
    move-exception v8

    :goto_3
    :try_start_6
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    :goto_4
    throw v8

    :catch_4
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    :cond_1
    :try_start_7
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5

    :goto_5
    :try_start_8
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_6

    :goto_6
    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, " fail "

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    move-object v2, v3

    goto :goto_0

    :catch_5
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    :catch_6
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6

    :catchall_1
    move-exception v8

    move-object v2, v3

    goto :goto_3

    :catch_7
    move-exception v1

    move-object v2, v3

    goto :goto_2
.end method

.method public quitPopSkin()V
    .locals 2

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->pipSkin:Lcom/mstar/android/tv/TvPipPopManager;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->pipSkin:Lcom/mstar/android/tv/TvPipPopManager;

    invoke-virtual {v1}, Lcom/mstar/android/tv/TvPipPopManager;->isPopEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.konka.hotkey.disablePop"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->sendBroadcast(Landroid/content/Intent;)V

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->pipSkin:Lcom/mstar/android/tv/TvPipPopManager;

    invoke-virtual {v1}, Lcom/mstar/android/tv/TvPipPopManager;->disablePop()Z

    goto :goto_0
.end method

.method public releaseSource()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->myHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->myHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->myHandler:Landroid/os/Handler;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    :cond_0
    invoke-virtual {p0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->quitPopSkin()V

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->vv:Lcom/konka/kkvideoplayer/VideoView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->vv:Lcom/konka/kkvideoplayer/VideoView;

    invoke-virtual {v0}, Lcom/konka/kkvideoplayer/VideoView;->stopPlayback()V

    :cond_1
    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->setting_window:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->setting_window:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    :cond_2
    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->br:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->br:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    :cond_3
    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->multiScreenReceiver:Lcom/konka/kkvideoplayer/VideoPlayerMain$DataReceiver;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->multiScreenReceiver:Lcom/konka/kkvideoplayer/VideoPlayerMain$DataReceiver;

    invoke-virtual {p0, v0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    :cond_4
    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->eBr:Lcom/konka/kkvideoplayer/VideoPlayerMain$EthStateReceiver;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->eBr:Lcom/konka/kkvideoplayer/VideoPlayerMain$EthStateReceiver;

    invoke-virtual {p0, v0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    :cond_5
    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->cBr:Lcom/konka/kkvideoplayer/VideoPlayerMain$CloudyMsgReceiver;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->cBr:Lcom/konka/kkvideoplayer/VideoPlayerMain$CloudyMsgReceiver;

    invoke-virtual {p0, v0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    :cond_6
    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->popBr:Lcom/konka/kkvideoplayer/VideoPlayerMain$BroadCastPop;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->popBr:Lcom/konka/kkvideoplayer/VideoPlayerMain$BroadCastPop;

    invoke-virtual {p0, v0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    :cond_7
    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->bc3D:Lcom/konka/kkvideoplayer/VideoPlayerMain$BroadCast3D;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->bc3D:Lcom/konka/kkvideoplayer/VideoPlayerMain$BroadCast3D;

    invoke-virtual {p0, v0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    :cond_8
    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->progressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    iput-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->progressDialog:Landroid/app/ProgressDialog;

    :cond_9
    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->seekHint:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->seekHint:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    iput-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->seekHint:Landroid/app/ProgressDialog;

    :cond_a
    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->toHome:Lcom/konka/kkvideoplayer/VideoPlayerMain$BroadCastGotoHome;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->toHome:Lcom/konka/kkvideoplayer/VideoPlayerMain$BroadCastGotoHome;

    invoke-virtual {p0, v0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iput-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->toHome:Lcom/konka/kkvideoplayer/VideoPlayerMain$BroadCastGotoHome;

    :cond_b
    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->toTv:Lcom/konka/kkvideoplayer/VideoPlayerMain$BroadCastGotoTv;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->toTv:Lcom/konka/kkvideoplayer/VideoPlayerMain$BroadCastGotoTv;

    invoke-virtual {p0, v0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iput-object v2, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->toTv:Lcom/konka/kkvideoplayer/VideoPlayerMain$BroadCastGotoTv;

    :cond_c
    iget v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->mPopStat:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_d

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->widgetListener:Lcom/konka/kkvideoplayer/WidgetListener;

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->widgetListener:Lcom/konka/kkvideoplayer/WidgetListener;

    invoke-virtual {v0}, Lcom/konka/kkvideoplayer/WidgetListener;->destroy()V

    :cond_d
    iput v3, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->intentFrom:I

    invoke-virtual {p0, v3}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->sendStatusBroadCast(I)V

    const-string v0, "release activity resource !!!!!!!!!!!!!!"

    invoke-static {v0}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    return-void
.end method

.method public sendStatusBroadCast(I)V
    .locals 4
    .param p1    # I

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.konka.MOVIE_PLAY_STATUS"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "PlayStatus"

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    invoke-virtual {p0, v1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method

.method setImageMode(I)V
    .locals 2
    .param p1    # I

    packed-switch p1, :pswitch_data_0

    invoke-static {p0}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getPictureManagerInstance()Lcom/konka/kkinterface/tv/PictureDesk;

    move-result-object v0

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;->PICTURE_NORMAL:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;

    invoke-interface {v0, v1}, Lcom/konka/kkinterface/tv/PictureDesk;->SetPictureModeIdx(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;)Z

    :goto_0
    return-void

    :pswitch_0
    invoke-static {p0}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getPictureManagerInstance()Lcom/konka/kkinterface/tv/PictureDesk;

    const-string v0, "PICTURE_DYNAMIC"

    invoke-static {v0}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    invoke-static {p0}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getPictureManagerInstance()Lcom/konka/kkinterface/tv/PictureDesk;

    move-result-object v0

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;->PICTURE_DYNAMIC:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;

    invoke-interface {v0, v1}, Lcom/konka/kkinterface/tv/PictureDesk;->SetPictureModeIdx(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;)Z

    goto :goto_0

    :pswitch_1
    invoke-static {p0}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getPictureManagerInstance()Lcom/konka/kkinterface/tv/PictureDesk;

    move-result-object v0

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;->PICTURE_NORMAL:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;

    invoke-interface {v0, v1}, Lcom/konka/kkinterface/tv/PictureDesk;->SetPictureModeIdx(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;)Z

    goto :goto_0

    :pswitch_2
    invoke-static {p0}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getPictureManagerInstance()Lcom/konka/kkinterface/tv/PictureDesk;

    move-result-object v0

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;->PICTURE_SOFT:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;

    invoke-interface {v0, v1}, Lcom/konka/kkinterface/tv/PictureDesk;->SetPictureModeIdx(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;)Z

    goto :goto_0

    :pswitch_3
    invoke-static {p0}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getPictureManagerInstance()Lcom/konka/kkinterface/tv/PictureDesk;

    move-result-object v0

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;->PICTURE_VIVID:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;

    invoke-interface {v0, v1}, Lcom/konka/kkinterface/tv/PictureDesk;->SetPictureModeIdx(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;)Z

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method setScreenMode(I)V
    .locals 3
    .param p1    # I

    packed-switch p1, :pswitch_data_0

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->pictureSkin:Lcom/mstar/android/tv/TvPictureManager;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumVideoArcType;->E_AUTO:Lcom/mstar/android/tvapi/common/vo/EnumVideoArcType;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tv/TvPictureManager;->setVideoArc(Lcom/mstar/android/tvapi/common/vo/EnumVideoArcType;)Z

    :goto_0
    return-void

    :pswitch_0
    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->pictureSkin:Lcom/mstar/android/tv/TvPictureManager;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumVideoArcType;->E_16x9:Lcom/mstar/android/tvapi/common/vo/EnumVideoArcType;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tv/TvPictureManager;->setVideoArc(Lcom/mstar/android/tvapi/common/vo/EnumVideoArcType;)Z

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setScreenMode========"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_1
    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->pictureSkin:Lcom/mstar/android/tv/TvPictureManager;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumVideoArcType;->E_4x3:Lcom/mstar/android/tvapi/common/vo/EnumVideoArcType;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tv/TvPictureManager;->setVideoArc(Lcom/mstar/android/tvapi/common/vo/EnumVideoArcType;)Z

    goto :goto_0

    :pswitch_2
    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->pictureSkin:Lcom/mstar/android/tv/TvPictureManager;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumVideoArcType;->E_AUTO:Lcom/mstar/android/tvapi/common/vo/EnumVideoArcType;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tv/TvPictureManager;->setVideoArc(Lcom/mstar/android/tvapi/common/vo/EnumVideoArcType;)Z

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
