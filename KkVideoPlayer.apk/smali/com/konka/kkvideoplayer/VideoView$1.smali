.class Lcom/konka/kkvideoplayer/VideoView$1;
.super Ljava/lang/Object;
.source "VideoView.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnVideoSizeChangedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/kkvideoplayer/VideoView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/kkvideoplayer/VideoView;


# direct methods
.method constructor <init>(Lcom/konka/kkvideoplayer/VideoView;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/kkvideoplayer/VideoView$1;->this$0:Lcom/konka/kkvideoplayer/VideoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onVideoSizeChanged(Landroid/media/MediaPlayer;II)V
    .locals 3
    .param p1    # Landroid/media/MediaPlayer;
    .param p2    # I
    .param p3    # I

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoView$1;->this$0:Lcom/konka/kkvideoplayer/VideoView;

    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getVideoWidth()I

    move-result v1

    invoke-static {v0, v1}, Lcom/konka/kkvideoplayer/VideoView;->access$0(Lcom/konka/kkvideoplayer/VideoView;I)V

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoView$1;->this$0:Lcom/konka/kkvideoplayer/VideoView;

    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getVideoHeight()I

    move-result v1

    invoke-static {v0, v1}, Lcom/konka/kkvideoplayer/VideoView;->access$1(Lcom/konka/kkvideoplayer/VideoView;I)V

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoView$1;->this$0:Lcom/konka/kkvideoplayer/VideoView;

    # getter for: Lcom/konka/kkvideoplayer/VideoView;->mMyChangeLinstener:Lcom/konka/kkvideoplayer/VideoView$MySizeChangeLinstener;
    invoke-static {v0}, Lcom/konka/kkvideoplayer/VideoView;->access$2(Lcom/konka/kkvideoplayer/VideoView;)Lcom/konka/kkvideoplayer/VideoView$MySizeChangeLinstener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoView$1;->this$0:Lcom/konka/kkvideoplayer/VideoView;

    # getter for: Lcom/konka/kkvideoplayer/VideoView;->mMyChangeLinstener:Lcom/konka/kkvideoplayer/VideoView$MySizeChangeLinstener;
    invoke-static {v0}, Lcom/konka/kkvideoplayer/VideoView;->access$2(Lcom/konka/kkvideoplayer/VideoView;)Lcom/konka/kkvideoplayer/VideoView$MySizeChangeLinstener;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkvideoplayer/VideoView$MySizeChangeLinstener;->doMyThings()V

    :cond_0
    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoView$1;->this$0:Lcom/konka/kkvideoplayer/VideoView;

    # getter for: Lcom/konka/kkvideoplayer/VideoView;->mVideoWidth:I
    invoke-static {v0}, Lcom/konka/kkvideoplayer/VideoView;->access$3(Lcom/konka/kkvideoplayer/VideoView;)I

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoView$1;->this$0:Lcom/konka/kkvideoplayer/VideoView;

    # getter for: Lcom/konka/kkvideoplayer/VideoView;->mVideoHeight:I
    invoke-static {v0}, Lcom/konka/kkvideoplayer/VideoView;->access$4(Lcom/konka/kkvideoplayer/VideoView;)I

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoView$1;->this$0:Lcom/konka/kkvideoplayer/VideoView;

    invoke-virtual {v0}, Lcom/konka/kkvideoplayer/VideoView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoView$1;->this$0:Lcom/konka/kkvideoplayer/VideoView;

    # getter for: Lcom/konka/kkvideoplayer/VideoView;->mVideoWidth:I
    invoke-static {v1}, Lcom/konka/kkvideoplayer/VideoView;->access$3(Lcom/konka/kkvideoplayer/VideoView;)I

    move-result v1

    iget-object v2, p0, Lcom/konka/kkvideoplayer/VideoView$1;->this$0:Lcom/konka/kkvideoplayer/VideoView;

    # getter for: Lcom/konka/kkvideoplayer/VideoView;->mVideoHeight:I
    invoke-static {v2}, Lcom/konka/kkvideoplayer/VideoView;->access$4(Lcom/konka/kkvideoplayer/VideoView;)I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/view/SurfaceHolder;->setFixedSize(II)V

    :cond_1
    return-void
.end method
