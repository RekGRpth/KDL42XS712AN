.class Lcom/konka/kkvideoplayer/VideoPlayerMain$7;
.super Ljava/lang/Object;
.source "VideoPlayerMain.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/kkvideoplayer/VideoPlayerMain;->initProgressBar()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;


# direct methods
.method constructor <init>(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$7;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 1
    .param p1    # Landroid/widget/SeekBar;
    .param p2    # I
    .param p3    # Z

    if-nez p3, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$7;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # invokes: Lcom/konka/kkvideoplayer/VideoPlayerMain;->handleProgressChange(I)V
    invoke-static {v0, p2}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$24(Lcom/konka/kkvideoplayer/VideoPlayerMain;I)V

    goto :goto_0
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 2
    .param p1    # Landroid/widget/SeekBar;

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$7;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$46(Lcom/konka/kkvideoplayer/VideoPlayerMain;Z)V

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$7;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # invokes: Lcom/konka/kkvideoplayer/VideoPlayerMain;->setMenuHideTimer()V
    invoke-static {v0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$0(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$7;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->myHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$47(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    const-string v0, "onStartTrackingTouch"

    invoke-static {v0}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 2
    .param p1    # Landroid/widget/SeekBar;

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$7;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    invoke-static {v0, v1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$46(Lcom/konka/kkvideoplayer/VideoPlayerMain;Z)V

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$7;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # invokes: Lcom/konka/kkvideoplayer/VideoPlayerMain;->setProgress()I
    invoke-static {v0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$48(Lcom/konka/kkvideoplayer/VideoPlayerMain;)I

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$7;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # invokes: Lcom/konka/kkvideoplayer/VideoPlayerMain;->setMenuHideTimer()V
    invoke-static {v0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$0(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$7;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->myHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$47(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    const-string v0, "onStopTrackingTouch"

    invoke-static {v0}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    return-void
.end method
