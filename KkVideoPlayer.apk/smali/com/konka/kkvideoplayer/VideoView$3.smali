.class Lcom/konka/kkvideoplayer/VideoView$3;
.super Ljava/lang/Object;
.source "VideoView.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnCompletionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/kkvideoplayer/VideoView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/kkvideoplayer/VideoView;


# direct methods
.method constructor <init>(Lcom/konka/kkvideoplayer/VideoView;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/kkvideoplayer/VideoView$3;->this$0:Lcom/konka/kkvideoplayer/VideoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 2
    .param p1    # Landroid/media/MediaPlayer;

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoView$3;->this$0:Lcom/konka/kkvideoplayer/VideoView;

    # getter for: Lcom/konka/kkvideoplayer/VideoView;->mMediaController:Landroid/widget/MediaController;
    invoke-static {v0}, Lcom/konka/kkvideoplayer/VideoView;->access$7(Lcom/konka/kkvideoplayer/VideoView;)Landroid/widget/MediaController;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoView$3;->this$0:Lcom/konka/kkvideoplayer/VideoView;

    # getter for: Lcom/konka/kkvideoplayer/VideoView;->mMediaController:Landroid/widget/MediaController;
    invoke-static {v0}, Lcom/konka/kkvideoplayer/VideoView;->access$7(Lcom/konka/kkvideoplayer/VideoView;)Landroid/widget/MediaController;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/MediaController;->hide()V

    :cond_0
    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoView$3;->this$0:Lcom/konka/kkvideoplayer/VideoView;

    # getter for: Lcom/konka/kkvideoplayer/VideoView;->mOnCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;
    invoke-static {v0}, Lcom/konka/kkvideoplayer/VideoView;->access$14(Lcom/konka/kkvideoplayer/VideoView;)Landroid/media/MediaPlayer$OnCompletionListener;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/konka/kkvideoplayer/VideoView$3;->this$0:Lcom/konka/kkvideoplayer/VideoView;

    # getter for: Lcom/konka/kkvideoplayer/VideoView;->mOnCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;
    invoke-static {v0}, Lcom/konka/kkvideoplayer/VideoView;->access$14(Lcom/konka/kkvideoplayer/VideoView;)Landroid/media/MediaPlayer$OnCompletionListener;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoView$3;->this$0:Lcom/konka/kkvideoplayer/VideoView;

    iget-object v1, v1, Lcom/konka/kkvideoplayer/VideoView;->mMediaPlayer:Lcom/mstar/android/media/MMediaPlayer;

    invoke-interface {v0, v1}, Landroid/media/MediaPlayer$OnCompletionListener;->onCompletion(Landroid/media/MediaPlayer;)V

    :cond_1
    return-void
.end method
