.class public Lcom/konka/kkvideoplayer/SubtitleTool;
.super Ljava/lang/Object;
.source "SubtitleTool.java"


# static fields
.field public static final SUBTITLE_FORMATE_ASS:I = 0x3

.field public static final SUBTITLE_FORMATE_IDX:I = 0x7

.field public static final SUBTITLE_FORMATE_NULL:I = 0x0

.field public static final SUBTITLE_FORMATE_SMI:I = 0x4

.field public static final SUBTITLE_FORMATE_SRT:I = 0x1

.field public static final SUBTITLE_FORMATE_SSA:I = 0x2

.field public static final SUBTITLE_FORMATE_TXT:I = 0x5


# instance fields
.field private VideoPath:Ljava/lang/String;

.field private cuurentSubtitleType:I


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/konka/kkvideoplayer/SubtitleTool;->cuurentSubtitleType:I

    const-string v0, ""

    iput-object v0, p0, Lcom/konka/kkvideoplayer/SubtitleTool;->VideoPath:Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/kkvideoplayer/SubtitleTool;->VideoPath:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getCuurentSubtitleType()I
    .locals 1

    iget v0, p0, Lcom/konka/kkvideoplayer/SubtitleTool;->cuurentSubtitleType:I

    return v0
.end method

.method public getSubtitlePath()Ljava/lang/String;
    .locals 8

    const/4 v7, 0x0

    iput v7, p0, Lcom/konka/kkvideoplayer/SubtitleTool;->cuurentSubtitleType:I

    const-string v0, ""

    iget-object v4, p0, Lcom/konka/kkvideoplayer/SubtitleTool;->VideoPath:Ljava/lang/String;

    iget-object v5, p0, Lcom/konka/kkvideoplayer/SubtitleTool;->VideoPath:Ljava/lang/String;

    const-string v6, "."

    invoke-virtual {v5, v6}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v4, v7, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, ".srt"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/kkvideoplayer/util/Tools;->isFileExist(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v4, 0x1

    iput v4, p0, Lcom/konka/kkvideoplayer/SubtitleTool;->cuurentSubtitleType:I

    move-object v1, v0

    :goto_0
    return-object v1

    :cond_0
    const-string v0, ""

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, ".ssa"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/kkvideoplayer/util/Tools;->isFileExist(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v4, 0x2

    iput v4, p0, Lcom/konka/kkvideoplayer/SubtitleTool;->cuurentSubtitleType:I

    move-object v1, v0

    goto :goto_0

    :cond_1
    const-string v0, ""

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, ".ass"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/kkvideoplayer/util/Tools;->isFileExist(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v4, 0x3

    iput v4, p0, Lcom/konka/kkvideoplayer/SubtitleTool;->cuurentSubtitleType:I

    move-object v1, v0

    goto :goto_0

    :cond_2
    const-string v0, ""

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, ".smi"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/kkvideoplayer/util/Tools;->isFileExist(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v4, 0x4

    iput v4, p0, Lcom/konka/kkvideoplayer/SubtitleTool;->cuurentSubtitleType:I

    move-object v1, v0

    goto :goto_0

    :cond_3
    const-string v0, ""

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, ".txt"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/kkvideoplayer/util/Tools;->isFileExist(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    const/4 v4, 0x5

    iput v4, p0, Lcom/konka/kkvideoplayer/SubtitleTool;->cuurentSubtitleType:I

    move-object v1, v0

    goto :goto_0

    :cond_4
    const-string v0, ""

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, ".idx"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, ".sub"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, Lcom/konka/kkvideoplayer/util/Tools;->isFileExist(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-static {v2}, Lcom/konka/kkvideoplayer/util/Tools;->isFileExist(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    const/4 v4, 0x7

    iput v4, p0, Lcom/konka/kkvideoplayer/SubtitleTool;->cuurentSubtitleType:I

    move-object v1, v0

    goto/16 :goto_0

    :cond_5
    const-string v0, ""

    move-object v1, v0

    goto/16 :goto_0
.end method

.method public isImageSubtitleType()Z
    .locals 2

    iget v0, p0, Lcom/konka/kkvideoplayer/SubtitleTool;->cuurentSubtitleType:I

    const/4 v1, 0x7

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isTextSubtitleType()Z
    .locals 2

    iget v0, p0, Lcom/konka/kkvideoplayer/SubtitleTool;->cuurentSubtitleType:I

    if-lez v0, :cond_0

    iget v0, p0, Lcom/konka/kkvideoplayer/SubtitleTool;->cuurentSubtitleType:I

    const/4 v1, 0x7

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
