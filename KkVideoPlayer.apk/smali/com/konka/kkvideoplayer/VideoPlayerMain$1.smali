.class Lcom/konka/kkvideoplayer/VideoPlayerMain$1;
.super Landroid/content/BroadcastReceiver;
.source "VideoPlayerMain.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/kkvideoplayer/VideoPlayerMain;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;


# direct methods
.method constructor <init>(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$1;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const/4 v5, 0x1

    const/4 v4, 0x0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v6

    const-string v7, "konka.voice.control.action.PLAYER_CONTROL"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const-string v6, "CmdName"

    invoke-virtual {p2, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v6, "Player_Play"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    const-string v6, "Player_Play"

    invoke-static {v6}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$1;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # invokes: Lcom/konka/kkvideoplayer/VideoPlayerMain;->setMenuHideTimer()V
    invoke-static {v6}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$0(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V

    iget-object v6, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$1;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->vv:Lcom/konka/kkvideoplayer/VideoView;
    invoke-static {v6}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$1(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Lcom/konka/kkvideoplayer/VideoView;

    move-result-object v6

    invoke-virtual {v6}, Lcom/konka/kkvideoplayer/VideoView;->isPlayerSane()Z

    move-result v6

    if-nez v6, :cond_1

    iget-object v4, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$1;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->vv:Lcom/konka/kkvideoplayer/VideoView;
    invoke-static {v4}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$1(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Lcom/konka/kkvideoplayer/VideoView;

    move-result-object v4

    iget-object v5, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$1;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->currPlay:Lcom/konka/kkvideoplayer/CurrPlayInfo;
    invoke-static {v5}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$2(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Lcom/konka/kkvideoplayer/CurrPlayInfo;

    move-result-object v5

    iget-object v5, v5, Lcom/konka/kkvideoplayer/CurrPlayInfo;->url:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/konka/kkvideoplayer/VideoView;->setVideoPath(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v6, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$1;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->isPaused:Z
    invoke-static {v6}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$3(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Z

    move-result v6

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$1;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->vv:Lcom/konka/kkvideoplayer/VideoView;
    invoke-static {v6}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$1(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Lcom/konka/kkvideoplayer/VideoView;

    move-result-object v6

    invoke-virtual {v6}, Lcom/konka/kkvideoplayer/VideoView;->start()V

    iget-object v6, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$1;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->btnPause:Landroid/widget/Button;
    invoke-static {v6}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$4(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Landroid/widget/Button;

    move-result-object v6

    const v7, 0x7f020010    # com.konka.kkvideoplayer.R.drawable.button_style_pause

    invoke-virtual {v6, v7}, Landroid/widget/Button;->setBackgroundResource(I)V

    iget-object v6, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$1;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # invokes: Lcom/konka/kkvideoplayer/VideoPlayerMain;->clearPauseStat()V
    invoke-static {v6}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$5(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V

    const-string v6, "current is restart "

    invoke-static {v6}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$1;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    iget-object v7, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$1;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->isPaused:Z
    invoke-static {v7}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$3(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Z

    move-result v7

    if-eqz v7, :cond_2

    :goto_1
    invoke-static {v6, v4}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$6(Lcom/konka/kkvideoplayer/VideoPlayerMain;Z)V

    goto :goto_0

    :cond_2
    move v4, v5

    goto :goto_1

    :cond_3
    const-string v6, "Player_Pause"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    const-string v6, "Player_Pause"

    invoke-static {v6}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$1;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # invokes: Lcom/konka/kkvideoplayer/VideoPlayerMain;->setMenuHideTimer()V
    invoke-static {v6}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$0(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V

    iget-object v6, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$1;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->vv:Lcom/konka/kkvideoplayer/VideoView;
    invoke-static {v6}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$1(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Lcom/konka/kkvideoplayer/VideoView;

    move-result-object v6

    invoke-virtual {v6}, Lcom/konka/kkvideoplayer/VideoView;->isPlayerSane()Z

    move-result v6

    if-nez v6, :cond_4

    iget-object v4, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$1;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->vv:Lcom/konka/kkvideoplayer/VideoView;
    invoke-static {v4}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$1(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Lcom/konka/kkvideoplayer/VideoView;

    move-result-object v4

    iget-object v5, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$1;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->currPlay:Lcom/konka/kkvideoplayer/CurrPlayInfo;
    invoke-static {v5}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$2(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Lcom/konka/kkvideoplayer/CurrPlayInfo;

    move-result-object v5

    iget-object v5, v5, Lcom/konka/kkvideoplayer/CurrPlayInfo;->url:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/konka/kkvideoplayer/VideoView;->setVideoPath(Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    iget-object v6, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$1;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->isPaused:Z
    invoke-static {v6}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$3(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Z

    move-result v6

    if-nez v6, :cond_0

    iget-object v6, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$1;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->vv:Lcom/konka/kkvideoplayer/VideoView;
    invoke-static {v6}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$1(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Lcom/konka/kkvideoplayer/VideoView;

    move-result-object v6

    invoke-virtual {v6}, Lcom/konka/kkvideoplayer/VideoView;->pause()V

    iget-object v6, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$1;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->btnPause:Landroid/widget/Button;
    invoke-static {v6}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$4(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Landroid/widget/Button;

    move-result-object v6

    const v7, 0x7f020011    # com.konka.kkvideoplayer.R.drawable.button_style_play

    invoke-virtual {v6, v7}, Landroid/widget/Button;->setBackgroundResource(I)V

    iget-object v6, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$1;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # invokes: Lcom/konka/kkvideoplayer/VideoPlayerMain;->showPauseStat()V
    invoke-static {v6}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$7(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V

    const-string v6, "current is pause !!!!!"

    invoke-static {v6}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$1;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    iget-object v7, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$1;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->isPaused:Z
    invoke-static {v7}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$3(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Z

    move-result v7

    if-eqz v7, :cond_5

    :goto_2
    invoke-static {v6, v4}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$6(Lcom/konka/kkvideoplayer/VideoPlayerMain;Z)V

    goto/16 :goto_0

    :cond_5
    move v4, v5

    goto :goto_2

    :cond_6
    const-string v5, "Player_Stop"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    const-string v4, "Player_Stop"

    invoke-static {v4}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$1;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # invokes: Lcom/konka/kkvideoplayer/VideoPlayerMain;->handleStopKey()V
    invoke-static {v4}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$8(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V

    goto/16 :goto_0

    :cond_7
    const-string v5, "Player_Setting"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_8

    iget-object v4, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$1;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # invokes: Lcom/konka/kkvideoplayer/VideoPlayerMain;->handleKeyMenuForSpeech()V
    invoke-static {v4}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$9(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V

    const-string v4, "Player_Setting"

    invoke-static {v4}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$1;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    const/4 v5, 0x0

    # invokes: Lcom/konka/kkvideoplayer/VideoPlayerMain;->initPopAwindow(Landroid/view/View;)V
    invoke-static {v4, v5}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$10(Lcom/konka/kkvideoplayer/VideoPlayerMain;Landroid/view/View;)V

    goto/16 :goto_0

    :cond_8
    const-string v5, "Player_Prev"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_9

    const-string v4, "Player_Prev"

    invoke-static {v4}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$1;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # invokes: Lcom/konka/kkvideoplayer/VideoPlayerMain;->handlePreviousKey()V
    invoke-static {v4}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$11(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V

    goto/16 :goto_0

    :cond_9
    const-string v5, "Player_Next"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_a

    const-string v4, "Player_Next"

    invoke-static {v4}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$1;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # invokes: Lcom/konka/kkvideoplayer/VideoPlayerMain;->handleNextKey()V
    invoke-static {v4}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$12(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V

    goto/16 :goto_0

    :cond_a
    const-string v5, "Player_Jump_A1"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_b

    const-string v5, "Player_Jump_A1"

    invoke-static {v5}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    const-string v5, "Args"

    invoke-virtual {p2, v5}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    array-length v5, v1

    if-lez v5, :cond_0

    aget-object v3, v1, v4

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    mul-int/lit8 v4, v4, 0x3c

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget-object v4, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$1;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v5

    # invokes: Lcom/konka/kkvideoplayer/VideoPlayerMain;->handleJumpTo(I)V
    invoke-static {v4, v5}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$13(Lcom/konka/kkvideoplayer/VideoPlayerMain;I)V

    goto/16 :goto_0

    :cond_b
    const-string v4, "Player_PlayList"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    goto/16 :goto_0
.end method
