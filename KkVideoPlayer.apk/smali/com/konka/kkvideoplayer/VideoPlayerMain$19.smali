.class Lcom/konka/kkvideoplayer/VideoPlayerMain$19;
.super Ljava/lang/Object;
.source "VideoPlayerMain.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/kkvideoplayer/VideoPlayerMain;->setListenScreenMode()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;


# direct methods
.method constructor <init>(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$19;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    const v2, 0x7f0a001e    # com.konka.kkvideoplayer.R.string.hint_mode_3d_inscreen_mode

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$19;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # invokes: Lcom/konka/kkvideoplayer/VideoPlayerMain;->getIfFBScreenModeSet()I
    invoke-static {v1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$61(Lcom/konka/kkvideoplayer/VideoPlayerMain;)I

    move-result v0

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$19;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->enablePip:Z
    invoke-static {v1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$80(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$19;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->pipSkin:Lcom/mstar/android/tv/TvPipPopManager;
    invoke-static {v1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$44(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$19;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    # getter for: Lcom/konka/kkvideoplayer/VideoPlayerMain;->pipSkin:Lcom/mstar/android/tv/TvPipPopManager;
    invoke-static {v1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$44(Lcom/konka/kkvideoplayer/VideoPlayerMain;)Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tv/TvPipPopManager;->isPipModeEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "setOnKeyListener PIP \u6a21\u5f0f\n"

    invoke-static {v1}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$19;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    const v2, 0x7f0a001f    # com.konka.kkvideoplayer.R.string.hint_mode_pip_inscreen_mode

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    :goto_0
    return-void

    :cond_0
    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    const-string v1, "setOnKeyListener 3d \u6a21\u5f0f\n"

    invoke-static {v1}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$19;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_1
    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$19;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    const v2, 0x7f0a0020    # com.konka.kkvideoplayer.R.string.hint_mode_4k_inscreen_mode

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$19;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    invoke-virtual {v1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->isMVCSource()Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, "setOnKeyListener 3d mvc \u6a21\u5f0f\n"

    invoke-static {v1}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$19;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/konka/kkvideoplayer/VideoPlayerMain$19;->this$0:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    const/16 v2, 0x200

    # invokes: Lcom/konka/kkvideoplayer/VideoPlayerMain;->handleScreenModeKey(I)V
    invoke-static {v1, v2}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->access$81(Lcom/konka/kkvideoplayer/VideoPlayerMain;I)V

    goto :goto_0
.end method
