.class public Lcom/konka/kkvideoplayer/WidgetListener;
.super Ljava/lang/Object;
.source "WidgetListener.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private appWidgetID:I

.field private blogWidgetID:I

.field private mAppWidgetHost:Landroid/appwidget/AppWidgetHost;

.field private mAppWidgetManager:Landroid/appwidget/AppWidgetManager;

.field mContext:Lcom/konka/kkvideoplayer/VideoPlayerMain;

.field private newsWidgetID:I

.field private videoWidgetID:I


# direct methods
.method public constructor <init>(Lcom/konka/kkvideoplayer/VideoPlayerMain;)V
    .locals 1
    .param p1    # Lcom/konka/kkvideoplayer/VideoPlayerMain;

    const/4 v0, -0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/konka/kkvideoplayer/WidgetListener;->newsWidgetID:I

    iput v0, p0, Lcom/konka/kkvideoplayer/WidgetListener;->appWidgetID:I

    iput v0, p0, Lcom/konka/kkvideoplayer/WidgetListener;->videoWidgetID:I

    iput v0, p0, Lcom/konka/kkvideoplayer/WidgetListener;->blogWidgetID:I

    iput-object p1, p0, Lcom/konka/kkvideoplayer/WidgetListener;->mContext:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/WidgetListener;->initWidget()V

    const-string v0, "WidgetListener init=="

    invoke-static {v0}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$0(Lcom/konka/kkvideoplayer/WidgetListener;)Landroid/appwidget/AppWidgetManager;
    .locals 1

    iget-object v0, p0, Lcom/konka/kkvideoplayer/WidgetListener;->mAppWidgetManager:Landroid/appwidget/AppWidgetManager;

    return-object v0
.end method

.method static synthetic access$1(Lcom/konka/kkvideoplayer/WidgetListener;)Landroid/appwidget/AppWidgetHost;
    .locals 1

    iget-object v0, p0, Lcom/konka/kkvideoplayer/WidgetListener;->mAppWidgetHost:Landroid/appwidget/AppWidgetHost;

    return-object v0
.end method

.method private completeAddAppWidget(II)V
    .locals 2
    .param p1    # I
    .param p2    # I

    iget-object v0, p0, Lcom/konka/kkvideoplayer/WidgetListener;->mContext:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    new-instance v1, Lcom/konka/kkvideoplayer/WidgetListener$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/konka/kkvideoplayer/WidgetListener$1;-><init>(Lcom/konka/kkvideoplayer/WidgetListener;II)V

    invoke-virtual {v0, v1}, Lcom/konka/kkvideoplayer/VideoPlayerMain;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method private getComponentName(Ljava/lang/String;)Landroid/content/ComponentName;
    .locals 5
    .param p1    # Ljava/lang/String;

    iget-object v3, p0, Lcom/konka/kkvideoplayer/WidgetListener;->mAppWidgetManager:Landroid/appwidget/AppWidgetManager;

    invoke-virtual {v3}, Landroid/appwidget/AppWidgetManager;->getInstalledProviders()Ljava/util/List;

    move-result-object v2

    const/4 v1, 0x0

    :goto_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-lt v1, v3, :cond_1

    const/4 v0, 0x0

    :cond_0
    return-object v0

    :cond_1
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/appwidget/AppWidgetProviderInfo;

    iget-object v0, v3, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "getComponentName:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private getHostID()I
    .locals 3

    new-instance v0, Ljava/util/Random;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Ljava/util/Random;-><init>(J)V

    const/16 v1, 0x1388

    invoke-virtual {v0, v1}, Ljava/util/Random;->nextInt(I)I

    move-result v1

    return v1
.end method

.method private initOneWidget(IILjava/lang/String;)I
    .locals 3
    .param p1    # I
    .param p2    # I
    .param p3    # Ljava/lang/String;

    iget-object v1, p0, Lcom/konka/kkvideoplayer/WidgetListener;->mAppWidgetHost:Landroid/appwidget/AppWidgetHost;

    invoke-virtual {v1}, Landroid/appwidget/AppWidgetHost;->allocateAppWidgetId()I

    move-result p1

    invoke-direct {p0, p3}, Lcom/konka/kkvideoplayer/WidgetListener;->getComponentName(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/konka/kkvideoplayer/WidgetListener;->mAppWidgetManager:Landroid/appwidget/AppWidgetManager;

    invoke-virtual {v1, p1, v0}, Landroid/appwidget/AppWidgetManager;->bindAppWidgetId(ILandroid/content/ComponentName;)V

    invoke-direct {p0, p1, p2}, Lcom/konka/kkvideoplayer/WidgetListener;->completeAddAppWidget(II)V

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "initOneWidget ok !!!!!!!:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    :goto_0
    return p1

    :cond_0
    const-string v1, "initOneWidget end!!!!!!!"

    invoke-static {v1}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    const/4 p1, -0x1

    goto :goto_0
.end method

.method private initWidget()V
    .locals 7

    const v6, 0x7f090007    # com.konka.kkvideoplayer.R.id.id_widget_news

    new-instance v3, Landroid/appwidget/AppWidgetHost;

    iget-object v4, p0, Lcom/konka/kkvideoplayer/WidgetListener;->mContext:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    invoke-direct {p0}, Lcom/konka/kkvideoplayer/WidgetListener;->getHostID()I

    move-result v5

    invoke-direct {v3, v4, v5}, Landroid/appwidget/AppWidgetHost;-><init>(Landroid/content/Context;I)V

    iput-object v3, p0, Lcom/konka/kkvideoplayer/WidgetListener;->mAppWidgetHost:Landroid/appwidget/AppWidgetHost;

    iget-object v3, p0, Lcom/konka/kkvideoplayer/WidgetListener;->mAppWidgetHost:Landroid/appwidget/AppWidgetHost;

    invoke-virtual {v3}, Landroid/appwidget/AppWidgetHost;->startListening()V

    iget-object v3, p0, Lcom/konka/kkvideoplayer/WidgetListener;->mContext:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    invoke-static {v3}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v3

    iput-object v3, p0, Lcom/konka/kkvideoplayer/WidgetListener;->mAppWidgetManager:Landroid/appwidget/AppWidgetManager;

    iget-object v3, p0, Lcom/konka/kkvideoplayer/WidgetListener;->mAppWidgetManager:Landroid/appwidget/AppWidgetManager;

    invoke-virtual {v3}, Landroid/appwidget/AppWidgetManager;->getInstalledProviders()Ljava/util/List;

    move-result-object v2

    const/4 v1, 0x0

    :goto_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-lt v1, v3, :cond_1

    iget v3, p0, Lcom/konka/kkvideoplayer/WidgetListener;->newsWidgetID:I

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    invoke-direct {p0, v6}, Lcom/konka/kkvideoplayer/WidgetListener;->showWidgetItem(I)V

    :cond_0
    return-void

    :cond_1
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/appwidget/AppWidgetProviderInfo;

    iget-object v0, v3, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    invoke-virtual {v0}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "com.konka.info.widget.InfoNewsWidgetForDoubleChannelProvider"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget v3, p0, Lcom/konka/kkvideoplayer/WidgetListener;->newsWidgetID:I

    const-string v4, "com.konka.info.widget.InfoNewsWidgetForDoubleChannelProvider"

    invoke-direct {p0, v3, v6, v4}, Lcom/konka/kkvideoplayer/WidgetListener;->initOneWidget(IILjava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/konka/kkvideoplayer/WidgetListener;->newsWidgetID:I

    :cond_2
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    invoke-virtual {v0}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "com.konka.market.widget.dualchannel"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    invoke-virtual {v0}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "com.konka.PopMoviewidget.PopMovieWidgetProvider"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    invoke-virtual {v0}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "com.konka.weibo.widget.WeiboWidget"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    goto :goto_1
.end method

.method private showWidgetItem(I)V
    .locals 4
    .param p1    # I

    iget-object v3, p0, Lcom/konka/kkvideoplayer/WidgetListener;->mContext:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    iget-object v3, v3, Lcom/konka/kkvideoplayer/VideoPlayerMain;->widgetLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v3}, Landroid/widget/FrameLayout;->getChildCount()I

    move-result v1

    const-string v3, "showWidgetItem start"

    invoke-static {v3}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    const/4 v0, 0x0

    :goto_0
    if-lt v0, v1, :cond_0

    return-void

    :cond_0
    iget-object v3, p0, Lcom/konka/kkvideoplayer/WidgetListener;->mContext:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    iget-object v3, v3, Lcom/konka/kkvideoplayer/VideoPlayerMain;->widgetLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v3, v0}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-ne p1, v3, :cond_1

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    :goto_1
    const-string v3, "showWidgetItem finish"

    invoke-static {v3}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method


# virtual methods
.method destroy()V
    .locals 2

    const/4 v1, 0x0

    const-string v0, "WidgetListener stop=="

    invoke-static {v0}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/kkvideoplayer/WidgetListener;->mAppWidgetHost:Landroid/appwidget/AppWidgetHost;

    invoke-virtual {v0}, Landroid/appwidget/AppWidgetHost;->stopListening()V

    iput-object v1, p0, Lcom/konka/kkvideoplayer/WidgetListener;->mAppWidgetHost:Landroid/appwidget/AppWidgetHost;

    iput-object v1, p0, Lcom/konka/kkvideoplayer/WidgetListener;->mAppWidgetManager:Landroid/appwidget/AppWidgetManager;

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1    # Landroid/view/View;

    const v6, 0x7f09000a    # com.konka.kkvideoplayer.R.id.id_widget_blog

    const v5, 0x7f090009    # com.konka.kkvideoplayer.R.id.id_widget_video

    const v4, 0x7f090008    # com.konka.kkvideoplayer.R.id.id_widget_app

    const v3, 0x7f090007    # com.konka.kkvideoplayer.R.id.id_widget_news

    const/4 v2, -0x1

    const-string v0, "onClick !!!!!!!!!!!!!!"

    invoke-static {v0}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget v0, p0, Lcom/konka/kkvideoplayer/WidgetListener;->newsWidgetID:I

    if-ne v0, v2, :cond_1

    iget v0, p0, Lcom/konka/kkvideoplayer/WidgetListener;->newsWidgetID:I

    const-string v1, "com.konka.info.widget.InfoNewsWidgetForDoubleChannelProvider"

    invoke-direct {p0, v0, v3, v1}, Lcom/konka/kkvideoplayer/WidgetListener;->initOneWidget(IILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/konka/kkvideoplayer/WidgetListener;->newsWidgetID:I

    :cond_1
    iget-object v0, p0, Lcom/konka/kkvideoplayer/WidgetListener;->mContext:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    iget-object v0, v0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->widgetTop:Landroid/widget/ImageView;

    const v1, 0x7f02008d    # com.konka.kkvideoplayer.R.drawable.widget_news_top

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    iget v0, p0, Lcom/konka/kkvideoplayer/WidgetListener;->newsWidgetID:I

    if-eq v0, v2, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "newsWidgetID:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/konka/kkvideoplayer/WidgetListener;->newsWidgetID:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    invoke-direct {p0, v3}, Lcom/konka/kkvideoplayer/WidgetListener;->showWidgetItem(I)V

    goto :goto_0

    :pswitch_1
    iget v0, p0, Lcom/konka/kkvideoplayer/WidgetListener;->appWidgetID:I

    if-ne v0, v2, :cond_2

    iget v0, p0, Lcom/konka/kkvideoplayer/WidgetListener;->appWidgetID:I

    const-string v1, "com.konka.market.widget.dualchannel"

    invoke-direct {p0, v0, v4, v1}, Lcom/konka/kkvideoplayer/WidgetListener;->initOneWidget(IILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/konka/kkvideoplayer/WidgetListener;->appWidgetID:I

    :cond_2
    iget-object v0, p0, Lcom/konka/kkvideoplayer/WidgetListener;->mContext:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    iget-object v0, v0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->widgetTop:Landroid/widget/ImageView;

    const v1, 0x7f020087    # com.konka.kkvideoplayer.R.drawable.widget_app_top

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    iget v0, p0, Lcom/konka/kkvideoplayer/WidgetListener;->appWidgetID:I

    if-eq v0, v2, :cond_0

    invoke-direct {p0, v4}, Lcom/konka/kkvideoplayer/WidgetListener;->showWidgetItem(I)V

    goto :goto_0

    :pswitch_2
    iget v0, p0, Lcom/konka/kkvideoplayer/WidgetListener;->videoWidgetID:I

    if-ne v0, v2, :cond_3

    iget v0, p0, Lcom/konka/kkvideoplayer/WidgetListener;->videoWidgetID:I

    const-string v1, "com.konka.PopMoviewidget.PopMovieWidgetProvider"

    invoke-direct {p0, v0, v5, v1}, Lcom/konka/kkvideoplayer/WidgetListener;->initOneWidget(IILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/konka/kkvideoplayer/WidgetListener;->videoWidgetID:I

    :cond_3
    iget-object v0, p0, Lcom/konka/kkvideoplayer/WidgetListener;->mContext:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    iget-object v0, v0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->widgetTop:Landroid/widget/ImageView;

    const v1, 0x7f020094    # com.konka.kkvideoplayer.R.drawable.widget_video_top

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    iget v0, p0, Lcom/konka/kkvideoplayer/WidgetListener;->videoWidgetID:I

    if-eq v0, v2, :cond_0

    invoke-direct {p0, v5}, Lcom/konka/kkvideoplayer/WidgetListener;->showWidgetItem(I)V

    goto :goto_0

    :pswitch_3
    iget v0, p0, Lcom/konka/kkvideoplayer/WidgetListener;->blogWidgetID:I

    if-ne v0, v2, :cond_4

    iget v0, p0, Lcom/konka/kkvideoplayer/WidgetListener;->blogWidgetID:I

    const-string v1, "com.konka.weibo.widget.WeiboWidget"

    invoke-direct {p0, v0, v6, v1}, Lcom/konka/kkvideoplayer/WidgetListener;->initOneWidget(IILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/konka/kkvideoplayer/WidgetListener;->blogWidgetID:I

    :cond_4
    iget-object v0, p0, Lcom/konka/kkvideoplayer/WidgetListener;->mContext:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    iget-object v0, v0, Lcom/konka/kkvideoplayer/VideoPlayerMain;->widgetTop:Landroid/widget/ImageView;

    const v1, 0x7f02008a    # com.konka.kkvideoplayer.R.drawable.widget_blog_top

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "blogWidgetID:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/konka/kkvideoplayer/WidgetListener;->blogWidgetID:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/kkvideoplayer/util/Tank;->Debug(Ljava/lang/String;)V

    iget v0, p0, Lcom/konka/kkvideoplayer/WidgetListener;->blogWidgetID:I

    if-eq v0, v2, :cond_0

    invoke-direct {p0, v6}, Lcom/konka/kkvideoplayer/WidgetListener;->showWidgetItem(I)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7f090007
        :pswitch_0    # com.konka.kkvideoplayer.R.id.id_widget_news
        :pswitch_1    # com.konka.kkvideoplayer.R.id.id_widget_app
        :pswitch_2    # com.konka.kkvideoplayer.R.id.id_widget_video
        :pswitch_3    # com.konka.kkvideoplayer.R.id.id_widget_blog
    .end packed-switch
.end method
