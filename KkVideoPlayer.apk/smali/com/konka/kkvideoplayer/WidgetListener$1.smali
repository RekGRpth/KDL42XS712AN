.class Lcom/konka/kkvideoplayer/WidgetListener$1;
.super Ljava/lang/Object;
.source "WidgetListener.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/kkvideoplayer/WidgetListener;->completeAddAppWidget(II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/kkvideoplayer/WidgetListener;

.field private final synthetic val$data:I

.field private final synthetic val$tag:I


# direct methods
.method constructor <init>(Lcom/konka/kkvideoplayer/WidgetListener;II)V
    .locals 0

    iput-object p1, p0, Lcom/konka/kkvideoplayer/WidgetListener$1;->this$0:Lcom/konka/kkvideoplayer/WidgetListener;

    iput p2, p0, Lcom/konka/kkvideoplayer/WidgetListener$1;->val$data:I

    iput p3, p0, Lcom/konka/kkvideoplayer/WidgetListener$1;->val$tag:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    iget v0, p0, Lcom/konka/kkvideoplayer/WidgetListener$1;->val$data:I

    const/4 v3, -0x1

    if-ne v0, v3, :cond_0

    iget-object v3, p0, Lcom/konka/kkvideoplayer/WidgetListener$1;->this$0:Lcom/konka/kkvideoplayer/WidgetListener;

    iget-object v3, v3, Lcom/konka/kkvideoplayer/WidgetListener;->mContext:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    const-string v4, "\u6dfb\u52a0\u7a97\u53e3\u5c0f\u90e8\u4ef6\u6709\u8bef"

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    :goto_0
    return-void

    :cond_0
    iget-object v3, p0, Lcom/konka/kkvideoplayer/WidgetListener$1;->this$0:Lcom/konka/kkvideoplayer/WidgetListener;

    # getter for: Lcom/konka/kkvideoplayer/WidgetListener;->mAppWidgetManager:Landroid/appwidget/AppWidgetManager;
    invoke-static {v3}, Lcom/konka/kkvideoplayer/WidgetListener;->access$0(Lcom/konka/kkvideoplayer/WidgetListener;)Landroid/appwidget/AppWidgetManager;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/appwidget/AppWidgetManager;->getAppWidgetInfo(I)Landroid/appwidget/AppWidgetProviderInfo;

    move-result-object v1

    iget-object v3, p0, Lcom/konka/kkvideoplayer/WidgetListener$1;->this$0:Lcom/konka/kkvideoplayer/WidgetListener;

    # getter for: Lcom/konka/kkvideoplayer/WidgetListener;->mAppWidgetHost:Landroid/appwidget/AppWidgetHost;
    invoke-static {v3}, Lcom/konka/kkvideoplayer/WidgetListener;->access$1(Lcom/konka/kkvideoplayer/WidgetListener;)Landroid/appwidget/AppWidgetHost;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/kkvideoplayer/WidgetListener$1;->this$0:Lcom/konka/kkvideoplayer/WidgetListener;

    iget-object v4, v4, Lcom/konka/kkvideoplayer/WidgetListener;->mContext:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    invoke-virtual {v3, v4, v0, v1}, Landroid/appwidget/AppWidgetHost;->createView(Landroid/content/Context;ILandroid/appwidget/AppWidgetProviderInfo;)Landroid/appwidget/AppWidgetHostView;

    move-result-object v2

    iget-object v3, p0, Lcom/konka/kkvideoplayer/WidgetListener$1;->this$0:Lcom/konka/kkvideoplayer/WidgetListener;

    iget-object v3, v3, Lcom/konka/kkvideoplayer/WidgetListener;->mContext:Lcom/konka/kkvideoplayer/VideoPlayerMain;

    iget-object v3, v3, Lcom/konka/kkvideoplayer/VideoPlayerMain;->widgetLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v3, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    iget v3, p0, Lcom/konka/kkvideoplayer/WidgetListener$1;->val$tag:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/appwidget/AppWidgetHostView;->setTag(Ljava/lang/Object;)V

    goto :goto_0
.end method
