.class public Lcom/konka/kkvideoplayer/WidgetLayout;
.super Landroid/widget/LinearLayout;
.source "WidgetLayout.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method public dispatchUnhandledMove(Landroid/view/View;I)Z
    .locals 2
    .param p1    # Landroid/view/View;
    .param p2    # I

    sparse-switch p2, :sswitch_data_0

    :goto_0
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->dispatchUnhandledMove(Landroid/view/View;I)Z

    move-result v1

    return v1

    :sswitch_0
    const v1, 0x7f090002    # com.konka.kkvideoplayer.R.id.id_doublechannel_left

    invoke-virtual {p0, v1}, Lcom/konka/kkvideoplayer/WidgetLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p1}, Landroid/view/View;->clearFocus()V

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    goto :goto_0

    :sswitch_1
    invoke-virtual {p1}, Landroid/view/View;->clearFocus()V

    const v1, 0x7f090007    # com.konka.kkvideoplayer.R.id.id_widget_news

    invoke-virtual {p0, v1}, Lcom/konka/kkvideoplayer/WidgetLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Landroid/view/View;->clearFocus()V

    const v1, 0x7f090004    # com.konka.kkvideoplayer.R.id.id_button_tv_channel

    invoke-virtual {p0, v1}, Lcom/konka/kkvideoplayer/WidgetLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x11 -> :sswitch_0
        0x21 -> :sswitch_2
        0x42 -> :sswitch_0
        0x82 -> :sswitch_1
    .end sparse-switch
.end method
