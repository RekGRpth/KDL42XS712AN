.class public Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;
.super Lcom/konka/kkimplements/tv/mstar/BaseDeskImpl;
.source "S3DDeskImpl.java"

# interfaces
.implements Lcom/konka/kkinterface/tv/S3DDesk;


# static fields
.field private static s3DMgrImpl:Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;


# instance fields
.field private ThreeDSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;

.field private com:Lcom/konka/kkinterface/tv/CommonDesk;

.field private context:Landroid/content/Context;

.field private databaseMgrImpl:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/konka/kkimplements/tv/mstar/BaseDeskImpl;-><init>()V

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->databaseMgrImpl:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->ThreeDSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-static {p1}, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v1, "TvService"

    const-string v2, "S3DManagerImpl constructor!!"

    invoke-interface {v0, v1, v2}, Lcom/konka/kkinterface/tv/CommonDesk;->printfI(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getDataBaseMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->databaseMgrImpl:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->databaseMgrImpl:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getVideo()Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;

    move-result-object v0

    iget-object v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_VIDEO;->ThreeDVideoMode:Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->ThreeDSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;

    iput-object p1, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->context:Landroid/content/Context;

    return-void
.end method

.method public static getS3DMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;
    .locals 1
    .param p0    # Landroid/content/Context;

    sget-object v0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->s3DMgrImpl:Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;

    if-nez v0, :cond_0

    new-instance v0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;

    invoke-direct {v0, p0}, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->s3DMgrImpl:Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;

    :cond_0
    sget-object v0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->s3DMgrImpl:Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;

    return-object v0
.end method

.method private notifyMM3DEnabled(Z)V
    .locals 5
    .param p1    # Z

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_STORAGE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-eq v2, v3, :cond_0

    :goto_0
    return-void

    :cond_0
    if-eqz p1, :cond_1

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v3, "S3DDeskImpl"

    const-string v4, "Notify MM 3D enabled"

    invoke-interface {v2, v3, v4}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.konka.tv.hotkey.MM_3DENABLE"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->context:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0

    :cond_1
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.konka.tv.hotkey.MM_3DDISABLE"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v3, "S3DDeskImpl"

    const-string v4, "Notify MM 3D disabled!"

    invoke-interface {v2, v3, v4}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->context:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    const-wide/16 v2, 0xc8

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public disableLow3dQuality()V
    .locals 2

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->disableLow3dQuality()V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public enableLow3dQuality()V
    .locals 2

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->enableLow3dQuality()V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public get3DDepthMode()I
    .locals 1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->ThreeDSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;

    iget-object v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;->eThreeDVideo3DDepth:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DDEPTH;

    invoke-virtual {v0}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DDEPTH;->ordinal()I

    move-result v0

    return v0
.end method

.method public get3DOffsetMode()I
    .locals 1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->ThreeDSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;

    iget-object v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;->eThreeDVideo3DOffset:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DOFFSET;

    invoke-virtual {v0}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DOFFSET;->ordinal()I

    move-result v0

    return v0
.end method

.method public get3DOutputAspectMode()Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DOUTPUTASPECT;
    .locals 1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->ThreeDSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;

    iget-object v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;->eThreeDVideo3DOutputAspect:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DOUTPUTASPECT;

    return-object v0
.end method

.method public getAutoStartMode()Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_AUTOSTART;
    .locals 1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->ThreeDSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;

    iget-object v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;->eThreeDVideoAutoStart:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_AUTOSTART;

    return-object v0
.end method

.method public getDisplay3DTo2DMode()Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;
    .locals 3

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->ThreeDSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->databaseMgrImpl:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryThreeDVideo3DTo2D(I)Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;

    move-result-object v1

    iput-object v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;->eThreeDVideo3DTo2D:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->ThreeDSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;

    iget-object v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;->eThreeDVideo3DTo2D:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;

    return-object v0
.end method

.method public getDisplayFormat()Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;
    .locals 3

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->ThreeDSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;

    iget-object v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;->eThreeDVideoSelfAdaptiveDetect:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;->DB_ThreeD_Video_SELF_ADAPTIVE_DETECT_OFF:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->ThreeDSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->databaseMgrImpl:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryThreeDVideoDisplayFormat(I)Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    move-result-object v1

    iput-object v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;->eThreeDVideoDisplayFormat:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    :goto_0
    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->ThreeDSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;

    iget-object v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;->eThreeDVideoDisplayFormat:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->ThreeDSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->DB_ThreeD_Video_DISPLAYFORMAT_AUTO:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    iput-object v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;->eThreeDVideoDisplayFormat:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    goto :goto_0
.end method

.method public getLRViewSwitch()Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;
    .locals 5

    const/4 v0, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->is3dLrSwitched()Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->ThreeDSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;->DB_ThreeD_Video_LRVIEWSWITCH_EXCHANGE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;

    iput-object v3, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;->eThreeDVideoLRViewSwitch:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;

    :goto_1
    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->ThreeDSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;

    iget-object v2, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;->eThreeDVideoLRViewSwitch:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;

    return-object v2

    :catch_0
    move-exception v1

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v3, "S3DDeskImpl"

    const-string v4, "Get LRViewSwitch False Exception"

    invoke-interface {v2, v3, v4}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->ThreeDSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;->DB_ThreeD_Video_LRVIEWSWITCH_NOTEXCHANGE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;

    iput-object v3, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;->eThreeDVideoLRViewSwitch:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;

    goto :goto_1
.end method

.method public getSelfAdaptiveDetect()Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;
    .locals 3

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->ThreeDSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->databaseMgrImpl:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryVideo3DSelfAdaptiveDetectMode(I)Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;

    move-result-object v1

    iput-object v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;->eThreeDVideoSelfAdaptiveDetect:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->ThreeDSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;

    iget-object v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;->eThreeDVideoSelfAdaptiveDetect:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;

    return-object v0
.end method

.method public set3DDepthMode(I)Z
    .locals 5
    .param p1    # I

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->ThreeDSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DDEPTH;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DDEPTH;

    move-result-object v2

    aget-object v2, v2, p1

    iput-object v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;->eThreeDVideo3DDepth:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DDEPTH;

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->databaseMgrImpl:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->ThreeDSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->updateVideo3DMode(Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;I)V

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v2, "TvS3DManagerIml"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "3D Depth mode is "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->ThreeDSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;

    iget-object v4, v4, Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;->eThreeDVideo3DDepth:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DDEPTH;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/konka/kkinterface/tv/CommonDesk;->printfI(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->set3dGain(I)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    const/4 v1, 0x1

    return v1

    :catch_0
    move-exception v0

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v2, "S3DDeskImpl"

    const-string v3, "Set set3DDepthMode False Exception"

    invoke-interface {v1, v2, v3}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public set3DOffsetMode(I)Z
    .locals 5
    .param p1    # I

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->ThreeDSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DOFFSET;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DOFFSET;

    move-result-object v2

    aget-object v2, v2, p1

    iput-object v2, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;->eThreeDVideo3DOffset:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DOFFSET;

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->databaseMgrImpl:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->ThreeDSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->updateVideo3DMode(Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;I)V

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v2, "TvS3DManagerIml"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "3D Depth mode is "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->ThreeDSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;

    iget-object v4, v4, Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;->eThreeDVideo3DDepth:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DDEPTH;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/konka/kkinterface/tv/CommonDesk;->printfI(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->set3dOffset(I)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    const/4 v1, 0x1

    return v1

    :catch_0
    move-exception v0

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v2, "S3DDeskImpl"

    const-string v3, "Set set3DDepthMode False Exception"

    invoke-interface {v1, v2, v3}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public set3DOutputAspectMode(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DOUTPUTASPECT;)Z
    .locals 8
    .param p1    # Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DOUTPUTASPECT;

    const/4 v7, 0x1

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/Enum3dAspectRatioType;->E_3D_ASPECTRATIO_FULL:Lcom/mstar/android/tvapi/common/vo/Enum3dAspectRatioType;

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->ThreeDSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;

    iput-object p1, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;->eThreeDVideo3DOutputAspect:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DOUTPUTASPECT;

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->databaseMgrImpl:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->ThreeDSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->updateVideo3DMode(Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;I)V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v3, "TvS3DManagerIml"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "output aspect mode is "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->ThreeDSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;

    iget-object v5, v5, Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;->eThreeDVideo3DOutputAspect:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DOUTPUTASPECT;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Lcom/konka/kkinterface/tv/CommonDesk;->printfI(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    const/4 v3, 0x1

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;->E_BLACK:Lcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v6}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v6

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/mstar/android/tvapi/common/TvManager;->setVideoMute(ZLcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;ILcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DOUTPUTASPECT;->DB_ThreeD_Video_3DOUTPUTASPECT_CENTER:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DOUTPUTASPECT;

    if-ne p1, v2, :cond_3

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/Enum3dAspectRatioType;->E_3D_ASPECTRATIO_CENTER:Lcom/mstar/android/tvapi/common/vo/Enum3dAspectRatioType;

    :goto_1
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->set3dArc(Lcom/mstar/android/tvapi/common/vo/Enum3dAspectRatioType;)Z
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_1
    :goto_2
    :try_start_2
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    const/4 v3, 0x0

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;->E_BLACK:Lcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v6}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v6

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/mstar/android/tvapi/common/TvManager;->setVideoMute(ZLcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;ILcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Z
    :try_end_2
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_2 .. :try_end_2} :catch_2

    :cond_2
    :goto_3
    return v7

    :catch_0
    move-exception v1

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v3, "S3DDeskImpl"

    const-string v4, "setVideoMute False Exception"

    invoke-interface {v2, v3, v4}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :cond_3
    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DOUTPUTASPECT;->DB_ThreeD_Video_3DOUTPUTASPECT_AUTOADAPTED:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DOUTPUTASPECT;

    if-ne p1, v2, :cond_4

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/Enum3dAspectRatioType;->E_3D_ASPECTRATIO_AUTO:Lcom/mstar/android/tvapi/common/vo/Enum3dAspectRatioType;

    goto :goto_1

    :cond_4
    sget-object v0, Lcom/mstar/android/tvapi/common/vo/Enum3dAspectRatioType;->E_3D_ASPECTRATIO_FULL:Lcom/mstar/android/tvapi/common/vo/Enum3dAspectRatioType;

    goto :goto_1

    :catch_1
    move-exception v1

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v3, "S3DDeskImpl"

    const-string v4, "Set set3DDepthMode False Exception"

    invoke-interface {v2, v3, v4}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_2

    :catch_2
    move-exception v1

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v3, "S3DDeskImpl"

    const-string v4, "setVideoMute False Exception"

    invoke-interface {v2, v3, v4}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_3
.end method

.method public set3DTo2D(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;)Z
    .locals 8
    .param p1    # Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;

    const/4 v7, 0x1

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->ThreeDSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;

    iput-object p1, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;->eThreeDVideo3DTo2D:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->databaseMgrImpl:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->ThreeDSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->updateVideo3DMode(Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;I)V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v3, "TvS3DManagerIml"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Display 3DTo2D Mode is "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->ThreeDSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;

    iget-object v5, v5, Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;->eThreeDVideo3DTo2D:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Lcom/konka/kkinterface/tv/CommonDesk;->printfI(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    const/4 v3, 0x1

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;->E_BLACK:Lcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v6}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v6

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/mstar/android/tvapi/common/TvManager;->setVideoMute(ZLcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;ILcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;->DB_ThreeD_Video_3DTO2D_SIDE_BY_SIDE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;

    if-ne p1, v2, :cond_3

    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_SIDE_BY_SIDE_HALF:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->enable3dTo2d(Lcom/mstar/android/tvapi/common/vo/Enum3dType;)Z
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_1
    :goto_1
    :try_start_2
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    const/4 v3, 0x0

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;->E_BLACK:Lcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v6}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v6

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/mstar/android/tvapi/common/TvManager;->setVideoMute(ZLcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;ILcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Z
    :try_end_2
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_2 .. :try_end_2} :catch_7

    :cond_2
    :goto_2
    return v7

    :catch_0
    move-exception v1

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v3, "S3DDeskImpl"

    const-string v4, "setVideoMute False Exception"

    invoke-interface {v2, v3, v4}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v1

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v3, "S3DDeskImpl"

    const-string v4, "Set set3DTo2D False Exception"

    invoke-interface {v2, v3, v4}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1

    :cond_3
    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;->DB_ThreeD_Video_3DTO2D_TOP_BOTTOM:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;

    if-ne p1, v2, :cond_4

    :try_start_3
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_TOP_BOTTOM:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->enable3dTo2d(Lcom/mstar/android/tvapi/common/vo/Enum3dType;)Z
    :try_end_3
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_1

    :catch_2
    move-exception v1

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v3, "S3DDeskImpl"

    const-string v4, "Set set3DTo2D False Exception"

    invoke-interface {v2, v3, v4}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1

    :cond_4
    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;->DB_ThreeD_Video_3DTO2D_FRAME_PACKING:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;

    if-ne p1, v2, :cond_5

    :try_start_4
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_FRAME_PACKING_1080P:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->enable3dTo2d(Lcom/mstar/android/tvapi/common/vo/Enum3dType;)Z
    :try_end_4
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_4 .. :try_end_4} :catch_3

    goto :goto_1

    :catch_3
    move-exception v1

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v3, "S3DDeskImpl"

    const-string v4, "Set set3DTo2D False Exception"

    invoke-interface {v2, v3, v4}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1

    :cond_5
    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;->DB_ThreeD_Video_3DTO2D_LINE_ALTERNATIVE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;

    if-ne p1, v2, :cond_6

    :try_start_5
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_LINE_ALTERNATIVE:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->enable3dTo2d(Lcom/mstar/android/tvapi/common/vo/Enum3dType;)Z
    :try_end_5
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_5 .. :try_end_5} :catch_4

    goto/16 :goto_1

    :catch_4
    move-exception v1

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v3, "S3DDeskImpl"

    const-string v4, "Set set3DTo2D False Exception"

    invoke-interface {v2, v3, v4}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto/16 :goto_1

    :cond_6
    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;->DB_ThreeD_Video_3DTO2D_AUTO:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;

    if-ne p1, v2, :cond_8

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/CommonDesk;->isSignalStable()Z

    move-result v2

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v3, "S3DDeskImpl"

    const-string v4, "Signal Stable!!!"

    invoke-interface {v2, v3, v4}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_6
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;->E_MAIN_WINDOW:Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->detect3dFormat(Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;)Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    move-result-object v0

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v3, "S3DDeskImpl"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Detect 3D formate is"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->enable3dTo2d(Lcom/mstar/android/tvapi/common/vo/Enum3dType;)Z
    :try_end_6
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_6 .. :try_end_6} :catch_5

    goto/16 :goto_1

    :catch_5
    move-exception v1

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v3, "S3DDeskImpl"

    const-string v4, "Set set3dFormatDetectFlag True Exception"

    invoke-interface {v2, v3, v4}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto/16 :goto_1

    :cond_7
    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v3, "S3DDeskImpl"

    const-string v4, "Signal Not Stable!!!"

    invoke-interface {v2, v3, v4}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_8
    :try_start_7
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_NONE:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->enable3dTo2d(Lcom/mstar/android/tvapi/common/vo/Enum3dType;)Z
    :try_end_7
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_7 .. :try_end_7} :catch_6

    goto/16 :goto_1

    :catch_6
    move-exception v1

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v3, "S3DDeskImpl"

    const-string v4, "Set set3DTo2D False Exception"

    invoke-interface {v2, v3, v4}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto/16 :goto_1

    :catch_7
    move-exception v1

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v3, "S3DDeskImpl"

    const-string v4, "setVideoMute False Exception"

    invoke-interface {v2, v3, v4}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto/16 :goto_2
.end method

.method public setAutoStartMode(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_AUTOSTART;)Z
    .locals 4
    .param p1    # Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_AUTOSTART;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->ThreeDSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;

    iput-object p1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;->eThreeDVideoAutoStart:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_AUTOSTART;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->databaseMgrImpl:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->ThreeDSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->updateVideo3DMode(Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;I)V

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v1, "TvS3DManagerIml"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "auto start mode is "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->ThreeDSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;

    iget-object v3, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;->eThreeDVideoAutoStart:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_AUTOSTART;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/konka/kkinterface/tv/CommonDesk;->printfI(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    return v0
.end method

.method public setDisplayFormat(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;)Z
    .locals 8
    .param p1    # Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    const/4 v7, 0x1

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->ThreeDSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;

    iput-object p1, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;->eThreeDVideoDisplayFormat:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->databaseMgrImpl:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->ThreeDSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->updateVideo3DMode(Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;I)V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v3, "TvS3DManagerIml"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Display format is "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->ThreeDSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;

    iget-object v5, v5, Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;->eThreeDVideoDisplayFormat:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Lcom/konka/kkinterface/tv/CommonDesk;->printfI(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    const/4 v3, 0x1

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;->E_BLACK:Lcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v6}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v6

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/mstar/android/tvapi/common/TvManager;->setVideoMute(ZLcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;ILcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->DB_ThreeD_Video_DISPLAYFORMAT_SIDE_BY_SIDE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    if-ne p1, v2, :cond_3

    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    invoke-direct {p0, v2}, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->notifyMM3DEnabled(Z)V

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_SIDE_BY_SIDE_HALF:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->enable3d(Lcom/mstar/android/tvapi/common/vo/Enum3dType;)Z
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_1
    :goto_1
    :try_start_2
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    const/4 v3, 0x0

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;->E_BLACK:Lcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v6}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v6

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/mstar/android/tvapi/common/TvManager;->setVideoMute(ZLcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;ILcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Z
    :try_end_2
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_2 .. :try_end_2} :catch_b

    :cond_2
    :goto_2
    return v7

    :catch_0
    move-exception v1

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v3, "S3DDeskImpl"

    const-string v4, "setVideoMute False Exception"

    invoke-interface {v2, v3, v4}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v1

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v3, "S3DDeskImpl"

    const-string v4, "setDisplayFormat False Exception"

    invoke-interface {v2, v3, v4}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1

    :cond_3
    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->DB_ThreeD_Video_DISPLAYFORMAT_TOP_BOTTOM:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    if-ne p1, v2, :cond_4

    :try_start_3
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    invoke-direct {p0, v2}, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->notifyMM3DEnabled(Z)V

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_TOP_BOTTOM:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->enable3d(Lcom/mstar/android/tvapi/common/vo/Enum3dType;)Z
    :try_end_3
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_1

    :catch_2
    move-exception v1

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v3, "S3DDeskImpl"

    const-string v4, "setDisplayFormat False Exception"

    invoke-interface {v2, v3, v4}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1

    :cond_4
    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->DB_ThreeD_Video_DISPLAYFORMAT_FRAME_PACKING:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    if-ne p1, v2, :cond_5

    :try_start_4
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_FRAME_PACKING_1080P:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->enable3d(Lcom/mstar/android/tvapi/common/vo/Enum3dType;)Z
    :try_end_4
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_4 .. :try_end_4} :catch_3

    goto :goto_1

    :catch_3
    move-exception v1

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v3, "S3DDeskImpl"

    const-string v4, "setDisplayFormat False Exception"

    invoke-interface {v2, v3, v4}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto/16 :goto_1

    :cond_5
    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->DB_ThreeD_Video_DISPLAYFORMAT_LINE_ALTERNATIVE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    if-ne p1, v2, :cond_6

    :try_start_5
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_LINE_ALTERNATIVE:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->enable3d(Lcom/mstar/android/tvapi/common/vo/Enum3dType;)Z
    :try_end_5
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_5 .. :try_end_5} :catch_4

    goto/16 :goto_1

    :catch_4
    move-exception v1

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v3, "S3DDeskImpl"

    const-string v4, "setDisplayFormat False Exception"

    invoke-interface {v2, v3, v4}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto/16 :goto_1

    :cond_6
    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->DB_ThreeD_Video_DISPLAYFORMAT_2DTO3D:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    if-ne p1, v2, :cond_7

    :try_start_6
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    invoke-direct {p0, v2}, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->notifyMM3DEnabled(Z)V

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_2DTO3D:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->enable3d(Lcom/mstar/android/tvapi/common/vo/Enum3dType;)Z

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v2

    invoke-virtual {p0}, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->get3DDepthMode()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->set3dGain(I)Z

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v2

    invoke-virtual {p0}, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->get3DOffsetMode()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->set3dOffset(I)Z
    :try_end_6
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_6 .. :try_end_6} :catch_5

    goto/16 :goto_1

    :catch_5
    move-exception v1

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v3, "S3DDeskImpl"

    const-string v4, "setDisplayFormat False Exception"

    invoke-interface {v2, v3, v4}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto/16 :goto_1

    :cond_7
    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->DB_ThreeD_Video_DISPLAYFORMAT_AUTO:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    if-ne p1, v2, :cond_a

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/CommonDesk;->isSignalStable()Z

    move-result v2

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v3, "S3DDeskImpl"

    const-string v4, "Signal Stable!!!"

    invoke-interface {v2, v3, v4}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_7
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_8

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_NONE:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->enable3d(Lcom/mstar/android/tvapi/common/vo/Enum3dType;)Z
    :try_end_7
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_7 .. :try_end_7} :catch_7

    :cond_8
    :goto_3
    :try_start_8
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;->E_MAIN_WINDOW:Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->detect3dFormat(Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;)Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    move-result-object v0

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v3, "S3DDeskImpl"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Detect 3D formate is"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->enable3d(Lcom/mstar/android/tvapi/common/vo/Enum3dType;)Z
    :try_end_8
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_8 .. :try_end_8} :catch_6

    goto/16 :goto_1

    :catch_6
    move-exception v1

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v3, "S3DDeskImpl"

    const-string v4, "setDisplayFormat False Exception"

    invoke-interface {v2, v3, v4}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto/16 :goto_1

    :catch_7
    move-exception v1

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v3, "S3DDeskImpl"

    const-string v4, "setDisplayFormat False Exception"

    invoke-interface {v2, v3, v4}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_3

    :cond_9
    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v3, "S3DDeskImpl"

    const-string v4, "Signal Not Stable!!!"

    invoke-interface {v2, v3, v4}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_a
    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->DB_ThreeD_Video_DISPLAYFORMAT_CHECK_BOARD:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    if-ne p1, v2, :cond_b

    :try_start_9
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_CHECK_BORAD:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->enable3d(Lcom/mstar/android/tvapi/common/vo/Enum3dType;)Z
    :try_end_9
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_9 .. :try_end_9} :catch_8

    goto/16 :goto_1

    :catch_8
    move-exception v1

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v3, "S3DDeskImpl"

    const-string v4, "setDisplayFormat False Exception"

    invoke-interface {v2, v3, v4}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto/16 :goto_1

    :cond_b
    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->DB_ThreeD_Video_DISPLAYFORMAT_PIXEL_ALTERNATIVE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    if-ne p1, v2, :cond_c

    :try_start_a
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_PIXEL_ALTERNATIVE:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->enable3d(Lcom/mstar/android/tvapi/common/vo/Enum3dType;)Z
    :try_end_a
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_a .. :try_end_a} :catch_9

    goto/16 :goto_1

    :catch_9
    move-exception v1

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v3, "S3DDeskImpl"

    const-string v4, "setDisplayFormat False Exception"

    invoke-interface {v2, v3, v4}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto/16 :goto_1

    :cond_c
    :try_start_b
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_NONE:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->enable3d(Lcom/mstar/android/tvapi/common/vo/Enum3dType;)Z

    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->notifyMM3DEnabled(Z)V
    :try_end_b
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_b .. :try_end_b} :catch_a

    goto/16 :goto_1

    :catch_a
    move-exception v1

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v3, "S3DDeskImpl"

    const-string v4, "setDisplayFormat False Exception"

    invoke-interface {v2, v3, v4}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto/16 :goto_1

    :catch_b
    move-exception v1

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v3, "S3DDeskImpl"

    const-string v4, "setVideoMute False Exception"

    invoke-interface {v2, v3, v4}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto/16 :goto_2
.end method

.method public setLRViewSwitch(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;)Z
    .locals 5
    .param p1    # Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->ThreeDSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;

    iput-object p1, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;->eThreeDVideoLRViewSwitch:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->databaseMgrImpl:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->ThreeDSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->updateVideo3DMode(Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;I)V

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v2, "TvS3DManagerIml"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "set View switch is "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->ThreeDSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;

    iget-object v4, v4, Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;->eThreeDVideoLRViewSwitch:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/konka/kkinterface/tv/CommonDesk;->printfV(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->ThreeDSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;

    iget-object v1, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;->eThreeDVideoLRViewSwitch:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;->DB_ThreeD_Video_LRVIEWSWITCH_EXCHANGE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;

    if-ne v1, v2, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->enable3dLrSwitch()Z

    :cond_0
    :goto_0
    const/4 v1, 0x1

    return v1

    :cond_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->disable3dLrSwitch()Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v2, "S3DDeskImpl"

    const-string v3, "Get LRViewSwitch False Exception"

    invoke-interface {v1, v2, v3}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setSelfAdaptiveDetect(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;)Z
    .locals 8
    .param p1    # Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;

    const/4 v7, 0x1

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;->DB_ThreeD_Video_SELF_ADAPTIVE_DETECT_OFF:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;

    if-ne p1, v2, :cond_3

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->ThreeDSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;

    iput-object p1, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;->eThreeDVideoSelfAdaptiveDetect:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->ThreeDSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->DB_ThreeD_Video_DISPLAYFORMAT_NONE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    iput-object v3, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;->eThreeDVideoDisplayFormat:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    :goto_0
    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->databaseMgrImpl:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->ThreeDSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;

    iget-object v3, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;->eThreeDVideoSelfAdaptiveDetect:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->updateVideo3DAdaptiveDetectMode(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;I)V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->databaseMgrImpl:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->ThreeDSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;

    iget-object v3, v3, Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;->eThreeDVideoDisplayFormat:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->updateVideo3DDisplayFormat(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;I)V

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    const/4 v3, 0x1

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;->E_BLACK:Lcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v6}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v6

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/mstar/android/tvapi/common/TvManager;->setVideoMute(ZLcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;ILcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_1
    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;->DB_ThreeD_Video_SELF_ADAPTIVE_DETECT_WHEN_SOURCE_CHANGE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;

    if-ne p1, v2, :cond_4

    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->set3dFormatDetectFlag(Z)Z
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_1
    :goto_2
    :try_start_2
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    const/4 v3, 0x0

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;->E_BLACK:Lcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v6}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v6

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/mstar/android/tvapi/common/TvManager;->setVideoMute(ZLcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;ILcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Z
    :try_end_2
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_2 .. :try_end_2} :catch_5

    :cond_2
    :goto_3
    return v7

    :cond_3
    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->ThreeDSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;->DB_ThreeD_Video_SELF_ADAPTIVE_DETECT_WHEN_SOURCE_CHANGE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;

    iput-object v3, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;->eThreeDVideoSelfAdaptiveDetect:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->ThreeDSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->DB_ThreeD_Video_DISPLAYFORMAT_AUTO:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    iput-object v3, v2, Lcom/konka/kkinterface/tv/DataBaseDesk$ThreeD_Video_MODE;->eThreeDVideoDisplayFormat:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    goto :goto_0

    :catch_0
    move-exception v1

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v3, "S3DDeskImpl"

    const-string v4, "setVideoMute False Exception"

    invoke-interface {v2, v3, v4}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1

    :catch_1
    move-exception v1

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v3, "S3DDeskImpl"

    const-string v4, "Set set3dFormatDetectFlag True Exception"

    invoke-interface {v2, v3, v4}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_2

    :cond_4
    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;->DB_ThreeD_Video_SELF_ADAPTIVE_DETECT_RIGHT_NOW:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;

    if-ne p1, v2, :cond_6

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/CommonDesk;->isSignalStable()Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v3, "S3DDeskImpl"

    const-string v4, "Signal Stable!!!"

    invoke-interface {v2, v3, v4}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_3
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_1

    const/4 v0, 0x0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;->E_MAIN_WINDOW:Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->detect3dFormat(Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;)Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    move-result-object v0

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v3, "S3DDeskImpl"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Detect 3D formate is"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->enable3d(Lcom/mstar/android/tvapi/common/vo/Enum3dType;)Z

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->set3dFormatDetectFlag(Z)Z
    :try_end_3
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_3 .. :try_end_3} :catch_2

    goto/16 :goto_2

    :catch_2
    move-exception v1

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v3, "S3DDeskImpl"

    const-string v4, "Set set3dFormatDetectFlag True Exception"

    invoke-interface {v2, v3, v4}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto/16 :goto_2

    :cond_5
    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v3, "S3DDeskImpl"

    const-string v4, "Signal Not Stable!!!"

    invoke-interface {v2, v3, v4}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_4
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->set3dFormatDetectFlag(Z)Z
    :try_end_4
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_4 .. :try_end_4} :catch_3

    goto/16 :goto_2

    :catch_3
    move-exception v1

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v3, "S3DDeskImpl"

    const-string v4, "Set set3dFormatDetectFlag True Exception"

    invoke-interface {v2, v3, v4}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto/16 :goto_2

    :cond_6
    :try_start_5
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->set3dFormatDetectFlag(Z)Z

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_NONE:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->enable3d(Lcom/mstar/android/tvapi/common/vo/Enum3dType;)Z
    :try_end_5
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_5 .. :try_end_5} :catch_4

    goto/16 :goto_2

    :catch_4
    move-exception v1

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v3, "S3DDeskImpl"

    const-string v4, "Set set3dFormatDetectFlag False Exception"

    invoke-interface {v2, v3, v4}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto/16 :goto_2

    :catch_5
    move-exception v1

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v3, "S3DDeskImpl"

    const-string v4, "setVideoMute False Exception"

    invoke-interface {v2, v3, v4}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto/16 :goto_3
.end method

.method public setVideoScaleforKTV()Z
    .locals 4

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v1

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_NONE:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->enable3d(Lcom/mstar/android/tvapi/common/vo/Enum3dType;)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    const/4 v1, 0x1

    return v1

    :catch_0
    move-exception v0

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v2, "S3DDeskImpl"

    const-string v3, "setDisplayFormat False Exception"

    invoke-interface {v1, v2, v3}, Lcom/konka/kkinterface/tv/CommonDesk;->printfE(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method
