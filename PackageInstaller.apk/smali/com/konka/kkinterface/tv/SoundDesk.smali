.class public interface abstract Lcom/konka/kkinterface/tv/SoundDesk;
.super Ljava/lang/Object;
.source "SoundDesk.java"

# interfaces
.implements Lcom/konka/kkinterface/tv/BaseDesk;


# virtual methods
.method public abstract adjustSoundMode(Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;Z)Z
.end method

.method public abstract getADAbsoluteVolume()I
.end method

.method public abstract getADEnable()Z
.end method

.method public abstract getAVCMode()Z
.end method

.method public abstract getAtvMtsMode()Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;
.end method

.method public abstract getBalance()S
.end method

.method public abstract getBass()S
.end method

.method public abstract getEqBand10k()S
.end method

.method public abstract getEqBand120()S
.end method

.method public abstract getEqBand1500()S
.end method

.method public abstract getEqBand500()S
.end method

.method public abstract getEqBand5k()S
.end method

.method public abstract getHdmiAudioSource(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Lcom/konka/kkinterface/tv/DataBaseDesk$HdmiAudioSource;
.end method

.method public abstract getMuteFlag()Z
.end method

.method public abstract getSoundMode()Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;
.end method

.method public abstract getSoundSpeakerDelay()I
.end method

.method public abstract getSpdifOutMode()Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SPDIF_MODE;
.end method

.method public abstract getSurroundMode()Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SURROUND_MODE;
.end method

.method public abstract getTreble()S
.end method

.method public abstract getVolume()S
.end method

.method public abstract setADAbsoluteVolume(I)V
.end method

.method public abstract setADEnable(Z)V
.end method

.method public abstract setAVCMode(Z)Z
.end method

.method public abstract setAtvMtsMode(Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;)Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;
.end method

.method public abstract setAudioInputSource(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Z
.end method

.method public abstract setBalance(S)Z
.end method

.method public abstract setBass(S)Z
.end method

.method public abstract setEqBand10k(S)Z
.end method

.method public abstract setEqBand120(S)Z
.end method

.method public abstract setEqBand1500(S)Z
.end method

.method public abstract setEqBand500(S)Z
.end method

.method public abstract setEqBand5k(S)Z
.end method

.method public abstract setHdmiAudioSource(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;Lcom/konka/kkinterface/tv/DataBaseDesk$HdmiAudioSource;)Z
.end method

.method public abstract setKaraADC()Z
.end method

.method public abstract setKaraAudioTrack(I)Z
.end method

.method public abstract setKaraMicNR(I)Z
.end method

.method public abstract setKaraMicVolume(S)Z
.end method

.method public abstract setKaraMixEffect(Z)Z
.end method

.method public abstract setKaraMixVolume(S)Z
.end method

.method public abstract setKaraSystemVolume(S)Z
.end method

.method public abstract setKaraVolume(S)Z
.end method

.method public abstract setMuteFlag(Z)Z
.end method

.method public abstract setSRSDefination(Z)V
.end method

.method public abstract setSRSDynamicClarity(Z)V
.end method

.method public abstract setSRSPara(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SRS_SET_TYPE;I)V
.end method

.method public abstract setSRSTSHD(Z)V
.end method

.method public abstract setSRSTrueBass(Z)V
.end method

.method public abstract setSoundMode(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;)Z
.end method

.method public abstract setSoundMode(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;Z)Z
.end method

.method public abstract setSoundSpeakerDelay(I)Z
.end method

.method public abstract setSpdifOutMode(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SPDIF_MODE;)Z
.end method

.method public abstract setSurroundMode(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SURROUND_MODE;)Z
.end method

.method public abstract setToNextAtvMtsMode()Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;
.end method

.method public abstract setTreble(S)Z
.end method

.method public abstract setVolume(S)Z
.end method
