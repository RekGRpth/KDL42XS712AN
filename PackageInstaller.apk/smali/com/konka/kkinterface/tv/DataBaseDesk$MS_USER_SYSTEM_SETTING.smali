.class public Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;
.super Ljava/lang/Object;
.source "DataBaseDesk.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/kkinterface/tv/DataBaseDesk;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MS_USER_SYSTEM_SETTING"
.end annotation


# instance fields
.field public AudioOnly:S

.field public CECARCStatus:Z

.field public CECAutoPoweroffMode:Z

.field public CECAutoStandbyMode:Z

.field public CECFunctionMode:Z

.field public Country:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

.field public DtvRoute:S

.field public DvbMode:I

.field public MSGState:Z

.field public ModeVal:I

.field public ScartOutRGB:S

.field public StickerDemo:Z

.field public U8SystemAutoTimeType:I

.field public U8Transparency:S

.field public bAlTimeshift:Z

.field public bAudioCloseBacklight:Z

.field public bBlueScreen:Z

.field public bDisableSiAutoUpdate:Z

.field public bEnableAlwaysTimeshift:S

.field public bEnableAutoChannelUpdate:Z

.field public bEnablePVRRecordAll:S

.field public bEnableWDT:S

.field public bLcnArrange:Z

.field public bMemcEnable:Z

.field public bMicOn:Z

.field public bOverScan:Z

.field public bTeletext:Z

.field public bUartBus:Z

.field public checkSum:I

.field public colorWheelMode:Lcom/konka/kkinterface/tv/DataBaseDesk$ColorWheelMode;

.field public eChSwMode:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_CHANNEL_SWITCH_MODE;

.field public eOffDetMode:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_OFFLINE_DET_MODE;

.field public ePWR_Logo:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_POWERON_LOGO;

.field public ePWR_Music:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_POWERON_MUSIC;

.field public eSUPER:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_SUPER;

.field public enCableOperators:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CABLE_OPERATORS;

.field public enInputSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

.field public enLanguage:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

.field public enSPDIFMODE:Lcom/konka/kkinterface/tv/DataBaseDesk$SPDIF_TYPE;

.field public enSatellitePlatform:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SATELLITE_PLATFORM;

.field public fAutoVolume:S

.field public fDcPowerOFFMode:S

.field public fNoChannel:Z

.field public fOADScanAfterWakeup:S

.field public fOadScan:S

.field public fRunInstallationGuide:Z

.field public fSoftwareUpdate:S

.field public m_AutoZoom:S

.field public m_MessageBoxExist:Z

.field public m_u8BrazilVideoStandardType:S

.field public m_u8SoftwareUpdateMode:S

.field public screenSaveMode:Z

.field public smartEnergySaving:Lcom/konka/kkinterface/tv/DataBaseDesk$SmartEnergySavingMode;

.field public ssuSaveMode:Z

.field public standbyNoOperation:I

.field public standbyNoSignal:Z

.field public u16LastOADVersion:I

.field public u32MenuTimeOut:J

.field public u32OSD_Active_Time:J

.field public u8Bandwidth:S

.field public u8ColorRangeMode:S

.field public u8FavoriteRegion:S

.field public u8HDMIAudioSource:S

.field public u8OADTime:S

.field public u8OsdDuration:S

.field public u8TimeShiftSizeType:S


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
