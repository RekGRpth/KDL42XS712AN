.class public Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;
.super Ljava/lang/Object;
.source "DataBaseDesk.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/kkinterface/tv/DataBaseDesk;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MS_FACTORY_CUS_DEF_SETTING"
.end annotation


# instance fields
.field public AudioDelayTime:I

.field public DefOSDLanguage:Ljava/lang/String;

.field public DefTuningCountry:I

.field public HotelEnabled:I

.field public IsRestoreNeeded:I

.field public IsTeletextEnabled:I

.field public PowerOffLogoDspTime:I

.field public PowerOffLogoEnabled:I


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v1, p0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;->DefTuningCountry:I

    const-string v0, "en_US"

    iput-object v0, p0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;->DefOSDLanguage:Ljava/lang/String;

    iput v1, p0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;->IsRestoreNeeded:I

    iput v1, p0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;->HotelEnabled:I

    iput v1, p0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;->PowerOffLogoEnabled:I

    iput v1, p0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;->PowerOffLogoDspTime:I

    const/4 v0, 0x1

    iput v0, p0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;->IsTeletextEnabled:I

    iput v1, p0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;->AudioDelayTime:I

    return-void
.end method
