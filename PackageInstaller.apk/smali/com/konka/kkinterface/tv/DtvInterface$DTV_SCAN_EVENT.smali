.class public Lcom/konka/kkinterface/tv/DtvInterface$DTV_SCAN_EVENT;
.super Ljava/lang/Object;
.source "DtvInterface.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/kkinterface/tv/DtvInterface;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DTV_SCAN_EVENT"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/kkinterface/tv/DtvInterface$DTV_SCAN_EVENT$EN_SCAN_RET_STATUS;
    }
.end annotation


# instance fields
.field public enScanStatus:Lcom/konka/kkinterface/tv/DtvInterface$DTV_SCAN_EVENT$EN_SCAN_RET_STATUS;

.field public u16DTVSrvCount:S

.field public u16DataSrvCount:S

.field public u16RadioSrvCount:S

.field public u16SignalQuality:S

.field public u16SignalStrength:S

.field public u32CurrFrequency:I

.field public u32UsrData:I

.field public u8CurrRFCh:S

.field public u8ScanPercentageNum:S


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/konka/kkinterface/tv/DtvInterface$DTV_SCAN_EVENT$EN_SCAN_RET_STATUS;->STATUS_SCAN_NONE:Lcom/konka/kkinterface/tv/DtvInterface$DTV_SCAN_EVENT$EN_SCAN_RET_STATUS;

    iput-object v0, p0, Lcom/konka/kkinterface/tv/DtvInterface$DTV_SCAN_EVENT;->enScanStatus:Lcom/konka/kkinterface/tv/DtvInterface$DTV_SCAN_EVENT$EN_SCAN_RET_STATUS;

    iput-short v1, p0, Lcom/konka/kkinterface/tv/DtvInterface$DTV_SCAN_EVENT;->u8CurrRFCh:S

    iput-short v1, p0, Lcom/konka/kkinterface/tv/DtvInterface$DTV_SCAN_EVENT;->u8ScanPercentageNum:S

    iput-short v1, p0, Lcom/konka/kkinterface/tv/DtvInterface$DTV_SCAN_EVENT;->u16DTVSrvCount:S

    iput-short v1, p0, Lcom/konka/kkinterface/tv/DtvInterface$DTV_SCAN_EVENT;->u16RadioSrvCount:S

    iput-short v1, p0, Lcom/konka/kkinterface/tv/DtvInterface$DTV_SCAN_EVENT;->u16DataSrvCount:S

    iput-short v1, p0, Lcom/konka/kkinterface/tv/DtvInterface$DTV_SCAN_EVENT;->u16SignalQuality:S

    iput-short v1, p0, Lcom/konka/kkinterface/tv/DtvInterface$DTV_SCAN_EVENT;->u16SignalStrength:S

    iput v1, p0, Lcom/konka/kkinterface/tv/DtvInterface$DTV_SCAN_EVENT;->u32CurrFrequency:I

    iput v1, p0, Lcom/konka/kkinterface/tv/DtvInterface$DTV_SCAN_EVENT;->u32UsrData:I

    return-void
.end method
