.class public Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;
.super Lcom/konka/kkimplements/tv/mstar/BaseDeskImpl;
.source "CommonDeskImpl.java"

# interfaces
.implements Lcom/konka/kkinterface/tv/CommonDesk;


# static fields
.field public static bThreadIsrunning:Z

.field private static commonService:Lcom/konka/kkinterface/tv/CommonDesk;


# instance fields
.field private AlwaysTimeShiftEnable:I

.field private CECEnable:I

.field private CIInfo:I

.field private StickerDemo:I

.field private applicationsInfo:Lcom/konka/kkinterface/tv/CommonDesk$ApplicationsInfo;

.field private audioInfo:Lcom/konka/kkinterface/tv/CommonDesk$AudioInfo;

.field private batteryInfo:Lcom/konka/kkinterface/tv/CommonDesk$BatteryInfo;

.field private bforcechangesource:Z

.field private ciCardInfo:Lcom/konka/kkinterface/tv/CommonDesk$CiCardInfo;

.field private context:Landroid/content/Context;

.field private cpuInfo:Lcom/konka/kkinterface/tv/CommonDesk$CPUInfo;

.field private curSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

.field private customerInfo:Lcom/konka/kkinterface/tv/CommonDesk$CustomerInfo;

.field deskAtvPlayerLister:Lcom/konka/kkimplements/tv/mstar/DeskAtvPlayerEventListener;

.field deskCaLister:Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener;

.field deskCiLister:Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener;

.field deskDtvPlayerLister:Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;

.field deskTimerLister:Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener;

.field deskTvLister:Lcom/konka/kkimplements/tv/mstar/DeskTvEventListener;

.field deskTvPlayerLister:Lcom/konka/kkimplements/tv/mstar/DeskTvPlayerEventListener;

.field private exStorageInfo:Lcom/konka/kkinterface/tv/CommonDesk$ExternalStorageInfo;

.field private inputSourceInfo:Lcom/konka/kkinterface/tv/CommonDesk$InputSourceInfo;

.field private localdimmingInfo:Lcom/konka/kkinterface/tv/CommonDesk$LocalDimmingInfo;

.field private panel4k2k:Lcom/konka/kkinterface/tv/CommonDesk$Panel4K2K;

.field private panelsupportInfo:Lcom/konka/kkinterface/tv/CommonDesk$PanelSupportInfo;

.field private pipInfo:Lcom/konka/kkinterface/tv/CommonDesk$PipInfo;

.field psl:[I

.field private systemBoardInfo:Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;

.field private systemBuildInfo:Lcom/konka/kkinterface/tv/CommonDesk$SystemBuildInfo;

.field private typeOf3DImpl:Lcom/konka/kkinterface/tv/CommonDesk$TypeOf3DImpl;

.field private typeThreeDImp:Lcom/konka/kkinterface/tv/CommonDesk$ThreeDImplType;

.field private ursaSelectType:Lcom/konka/kkinterface/tv/CommonDesk$UrsaSelectEnumType;

.field private videoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

.field private wifiDeviceInfo:Lcom/konka/kkinterface/tv/CommonDesk$WifiDeviceInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->commonService:Lcom/konka/kkinterface/tv/CommonDesk;

    const/4 v0, 0x0

    sput-boolean v0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->bThreadIsrunning:Z

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 7
    .param p1    # Landroid/content/Context;

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/konka/kkimplements/tv/mstar/BaseDeskImpl;-><init>()V

    iput-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->psl:[I

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_NONE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->curSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    iput-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->videoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    iput-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->inputSourceInfo:Lcom/konka/kkinterface/tv/CommonDesk$InputSourceInfo;

    iput-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->systemBoardInfo:Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;

    iput-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->audioInfo:Lcom/konka/kkinterface/tv/CommonDesk$AudioInfo;

    iput-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->batteryInfo:Lcom/konka/kkinterface/tv/CommonDesk$BatteryInfo;

    iput-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->ciCardInfo:Lcom/konka/kkinterface/tv/CommonDesk$CiCardInfo;

    iput-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->pipInfo:Lcom/konka/kkinterface/tv/CommonDesk$PipInfo;

    iput-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->systemBuildInfo:Lcom/konka/kkinterface/tv/CommonDesk$SystemBuildInfo;

    iput-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->applicationsInfo:Lcom/konka/kkinterface/tv/CommonDesk$ApplicationsInfo;

    iput-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->customerInfo:Lcom/konka/kkinterface/tv/CommonDesk$CustomerInfo;

    iput-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->localdimmingInfo:Lcom/konka/kkinterface/tv/CommonDesk$LocalDimmingInfo;

    iput-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->panel4k2k:Lcom/konka/kkinterface/tv/CommonDesk$Panel4K2K;

    iput-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->typeOf3DImpl:Lcom/konka/kkinterface/tv/CommonDesk$TypeOf3DImpl;

    iput-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->typeThreeDImp:Lcom/konka/kkinterface/tv/CommonDesk$ThreeDImplType;

    iput-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->ursaSelectType:Lcom/konka/kkinterface/tv/CommonDesk$UrsaSelectEnumType;

    iput-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->wifiDeviceInfo:Lcom/konka/kkinterface/tv/CommonDesk$WifiDeviceInfo;

    iput-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->panelsupportInfo:Lcom/konka/kkinterface/tv/CommonDesk$PanelSupportInfo;

    iput-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->cpuInfo:Lcom/konka/kkinterface/tv/CommonDesk$CPUInfo;

    iput-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->exStorageInfo:Lcom/konka/kkinterface/tv/CommonDesk$ExternalStorageInfo;

    const/4 v0, 0x1

    iput v0, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->AlwaysTimeShiftEnable:I

    const/4 v0, 0x2

    iput v0, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->CECEnable:I

    const/4 v0, 0x4

    iput v0, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->CIInfo:I

    const/16 v0, 0x8

    iput v0, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->StickerDemo:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->bforcechangesource:Z

    iput-object p1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->context:Landroid/content/Context;

    new-instance v0, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    const/16 v1, 0x780

    const/16 v2, 0x438

    const/16 v3, 0x3c

    const/16 v4, 0xc

    sget-object v5, Lcom/konka/kkinterface/tv/CommonDesk$EN_SCAN_TYPE;->E_PROGRESSIVE:Lcom/konka/kkinterface/tv/CommonDesk$EN_SCAN_TYPE;

    invoke-direct/range {v0 .. v5}, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;-><init>(SSSSLcom/konka/kkinterface/tv/CommonDesk$EN_SCAN_TYPE;)V

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->videoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    invoke-direct {p0}, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->InitSourceList()V

    invoke-static {}, Lcom/konka/kkimplements/tv/mstar/DeskAtvPlayerEventListener;->getInstance()Lcom/konka/kkimplements/tv/mstar/DeskAtvPlayerEventListener;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->deskAtvPlayerLister:Lcom/konka/kkimplements/tv/mstar/DeskAtvPlayerEventListener;

    invoke-static {}, Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;->getInstance()Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->deskDtvPlayerLister:Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;

    invoke-static {}, Lcom/konka/kkimplements/tv/mstar/DeskTvPlayerEventListener;->getInstance()Lcom/konka/kkimplements/tv/mstar/DeskTvPlayerEventListener;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->deskTvPlayerLister:Lcom/konka/kkimplements/tv/mstar/DeskTvPlayerEventListener;

    invoke-static {}, Lcom/konka/kkimplements/tv/mstar/DeskTvEventListener;->getInstance()Lcom/konka/kkimplements/tv/mstar/DeskTvEventListener;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->deskTvLister:Lcom/konka/kkimplements/tv/mstar/DeskTvEventListener;

    invoke-static {}, Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener;->getInstance()Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->deskCiLister:Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener;

    invoke-static {}, Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener;->getInstance()Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->deskCaLister:Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener;

    invoke-static {}, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener;->getInstance()Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->deskTimerLister:Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener;

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPlayerManager()Lcom/mstar/android/tvapi/common/TvPlayer;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->deskTvPlayerLister:Lcom/konka/kkimplements/tv/mstar/DeskTvPlayerEventListener;

    invoke-interface {v0, v1}, Lcom/mstar/android/tvapi/common/TvPlayer;->setOnTvPlayerEventListener(Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;)V

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->deskTvLister:Lcom/konka/kkimplements/tv/mstar/DeskTvEventListener;

    invoke-virtual {v0, v1}, Lcom/mstar/android/tvapi/common/TvManager;->setOnTvEventListener(Lcom/mstar/android/tvapi/common/listener/OnTvEventListener;)V

    :cond_0
    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getDvbPlayerManager()Lcom/mstar/android/tvapi/dtv/dvb/DvbPlayer;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getDvbPlayerManager()Lcom/mstar/android/tvapi/dtv/dvb/DvbPlayer;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->deskDtvPlayerLister:Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;

    invoke-interface {v0, v1}, Lcom/mstar/android/tvapi/dtv/dvb/DvbPlayer;->setOnDtvPlayerEventListener(Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;)V

    :cond_1
    invoke-static {}, Lcom/mstar/android/tvapi/atv/AtvManager;->getAtvPlayerManager()Lcom/mstar/android/tvapi/atv/AtvPlayer;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/mstar/android/tvapi/atv/AtvManager;->getAtvPlayerManager()Lcom/mstar/android/tvapi/atv/AtvPlayer;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->deskAtvPlayerLister:Lcom/konka/kkimplements/tv/mstar/DeskAtvPlayerEventListener;

    invoke-interface {v0, v1}, Lcom/mstar/android/tvapi/atv/AtvPlayer;->setOnAtvPlayerEventListener(Lcom/mstar/android/tvapi/atv/listener/OnAtvPlayerEventListener;)V

    :cond_2
    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getCiManager()Lcom/mstar/android/tvapi/dtv/common/CiManager;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getCiManager()Lcom/mstar/android/tvapi/dtv/common/CiManager;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->deskCiLister:Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener;

    invoke-virtual {v0, v1}, Lcom/mstar/android/tvapi/dtv/common/CiManager;->setOnCiEventListener(Lcom/mstar/android/tvapi/dtv/common/CiManager$OnCiEventListener;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_3
    :goto_0
    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/CaManager;->getInstance()Lcom/mstar/android/tvapi/dtv/common/CaManager;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/CaManager;->getInstance()Lcom/mstar/android/tvapi/dtv/common/CaManager;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->deskCaLister:Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener;

    invoke-virtual {v0, v1}, Lcom/mstar/android/tvapi/dtv/common/CaManager;->setOnCaEventListener(Lcom/mstar/android/tvapi/dtv/common/CaManager$OnCaEventListener;)V

    :cond_4
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getTimerManager()Lcom/mstar/android/tvapi/common/TimerManager;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getTimerManager()Lcom/mstar/android/tvapi/common/TimerManager;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->deskTimerLister:Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener;

    invoke-virtual {v0, v1}, Lcom/mstar/android/tvapi/common/TimerManager;->setOnTimerEventListener(Lcom/mstar/android/tvapi/common/TimerManager$OnTimerEventListener;)V

    :cond_5
    return-void

    :catch_0
    move-exception v6

    invoke-virtual {v6}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method private InitSourceList()V
    .locals 2

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getSourceList()[I

    move-result-object v1

    iput-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->psl:[I
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method static synthetic access$0(Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;)Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    .locals 1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->curSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    return-object v0
.end method

.method static synthetic access$1(Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->bforcechangesource:Z

    return v0
.end method

.method static synthetic access$2(Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->context:Landroid/content/Context;

    return-object v0
.end method

.method private execStartMsrv(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;Z)Z
    .locals 2
    .param p1    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    .param p2    # Z

    iput-object p1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->curSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    iput-boolean p2, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->bforcechangesource:Z

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl$2;

    invoke-direct {v1, p0}, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl$2;-><init>(Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    const/4 v0, 0x1

    return v0
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/CommonDesk;
    .locals 1
    .param p0    # Landroid/content/Context;

    sget-object v0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->commonService:Lcom/konka/kkinterface/tv/CommonDesk;

    if-nez v0, :cond_0

    new-instance v0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;

    invoke-direct {v0, p0}, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->commonService:Lcom/konka/kkinterface/tv/CommonDesk;

    :cond_0
    sget-object v0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->commonService:Lcom/konka/kkinterface/tv/CommonDesk;

    return-object v0
.end method


# virtual methods
.method public ExecSetInputSource(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Z
    .locals 2
    .param p1    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    iput-object p1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->curSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl$1;

    invoke-direct {v1, p0}, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl$1;-><init>(Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    const/4 v0, 0x1

    return v0
.end method

.method public GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    .locals 2

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->curSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->curSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public SetInputSource(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)V
    .locals 4
    .param p1    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    iput-object p1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->curSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    const-string v1, "TvApp"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SetSourceType:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->curSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/mstar/android/tvapi/common/TvManager;->setInputSource(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public SetInputSource(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;Z)V
    .locals 5
    .param p1    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    .param p2    # Z

    iput-object p1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->curSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    const-string v1, "TvApp"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SetSourceType:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->curSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz p2, :cond_1

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/mstar/android/tvapi/common/TvManager;->setInputSource(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v1, p1, v2, v3, v4}, Lcom/mstar/android/tvapi/common/TvManager;->setInputSource(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;ZZZ)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public disableTvosIr()V
    .locals 2

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->disableTvosIr()V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public enterSleepMode(ZZ)Z
    .locals 4
    .param p1    # Z
    .param p2    # Z

    :try_start_0
    const-string v1, "TvApp"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "enterSleepMode:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Lcom/mstar/android/tvapi/common/TvManager;->enterSleepMode(ZZ)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    const/4 v1, 0x0

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public get3DImplType()Lcom/konka/kkinterface/tv/CommonDesk$TypeOf3DImpl;
    .locals 4

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->typeOf3DImpl:Lcom/konka/kkinterface/tv/CommonDesk$TypeOf3DImpl;

    if-nez v1, :cond_0

    new-instance v1, Lcom/konka/kkinterface/tv/CommonDesk$TypeOf3DImpl;

    invoke-direct {v1}, Lcom/konka/kkinterface/tv/CommonDesk$TypeOf3DImpl;-><init>()V

    iput-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->typeOf3DImpl:Lcom/konka/kkinterface/tv/CommonDesk$TypeOf3DImpl;

    new-instance v0, Lcom/konka/kkimplements/common/IniEditor;

    invoke-direct {v0}, Lcom/konka/kkimplements/common/IniEditor;-><init>()V

    const-string v1, "/customercfg/panel/panel.ini"

    invoke-virtual {v0, v1}, Lcom/konka/kkimplements/common/IniEditor;->loadFile(Ljava/lang/String;)Z

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->typeOf3DImpl:Lcom/konka/kkinterface/tv/CommonDesk$TypeOf3DImpl;

    const-string v2, "panel:bSGPanelFlag"

    const-string v3, "0"

    invoke-virtual {v0, v2, v3}, Lcom/konka/kkimplements/common/IniEditor;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, v1, Lcom/konka/kkinterface/tv/CommonDesk$TypeOf3DImpl;->m_typeOf3DImpl:I

    :cond_0
    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->typeOf3DImpl:Lcom/konka/kkinterface/tv/CommonDesk$TypeOf3DImpl;

    return-object v1
.end method

.method public get3DImplement()Lcom/konka/kkinterface/tv/CommonDesk$ThreeDImplType;
    .locals 6

    const/4 v5, 0x1

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->typeThreeDImp:Lcom/konka/kkinterface/tv/CommonDesk$ThreeDImplType;

    if-nez v3, :cond_0

    new-instance v3, Lcom/konka/kkinterface/tv/CommonDesk$ThreeDImplType;

    invoke-direct {v3}, Lcom/konka/kkinterface/tv/CommonDesk$ThreeDImplType;-><init>()V

    iput-object v3, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->typeThreeDImp:Lcom/konka/kkinterface/tv/CommonDesk$ThreeDImplType;

    new-instance v1, Lcom/konka/kkimplements/common/IniEditor;

    invoke-direct {v1}, Lcom/konka/kkimplements/common/IniEditor;-><init>()V

    const-string v3, "/customercfg/model/Customer_1.ini"

    invoke-virtual {v1, v3}, Lcom/konka/kkimplements/common/IniEditor;->loadFile(Ljava/lang/String;)Z

    :try_start_0
    const-string v3, "Ursa:UrsaEnable"

    const-string v4, "0"

    invoke-virtual {v1, v3, v4}, Lcom/konka/kkimplements/common/IniEditor;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    if-ne v2, v5, :cond_1

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->typeThreeDImp:Lcom/konka/kkinterface/tv/CommonDesk$ThreeDImplType;

    const/4 v4, 0x2

    iput v4, v3, Lcom/konka/kkinterface/tv/CommonDesk$ThreeDImplType;->ThreeDImpl:I

    :cond_0
    :goto_1
    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->typeThreeDImp:Lcom/konka/kkinterface/tv/CommonDesk$ThreeDImplType;

    return-object v3

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0

    :cond_1
    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->typeThreeDImp:Lcom/konka/kkinterface/tv/CommonDesk$ThreeDImplType;

    iput v5, v3, Lcom/konka/kkinterface/tv/CommonDesk$ThreeDImplType;->ThreeDImpl:I

    goto :goto_1
.end method

.method public getApplicationsInfo()Lcom/konka/kkinterface/tv/CommonDesk$ApplicationsInfo;
    .locals 4

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->applicationsInfo:Lcom/konka/kkinterface/tv/CommonDesk$ApplicationsInfo;

    if-nez v1, :cond_0

    new-instance v1, Lcom/konka/kkinterface/tv/CommonDesk$ApplicationsInfo;

    invoke-direct {v1}, Lcom/konka/kkinterface/tv/CommonDesk$ApplicationsInfo;-><init>()V

    iput-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->applicationsInfo:Lcom/konka/kkinterface/tv/CommonDesk$ApplicationsInfo;

    new-instance v0, Lcom/konka/kkimplements/common/IniEditor;

    invoke-direct {v0}, Lcom/konka/kkimplements/common/IniEditor;-><init>()V

    const-string v1, "customercfg/model/config.ini"

    invoke-virtual {v0, v1}, Lcom/konka/kkimplements/common/IniEditor;->loadFile(Ljava/lang/String;)Z

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->applicationsInfo:Lcom/konka/kkinterface/tv/CommonDesk$ApplicationsInfo;

    const-string v2, "APPLICATION_OPTIONS:CNTV_ENABLE"

    const-string v3, "0"

    invoke-virtual {v0, v2, v3}, Lcom/konka/kkimplements/common/IniEditor;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, v1, Lcom/konka/kkinterface/tv/CommonDesk$ApplicationsInfo;->enableCNTV:I

    :cond_0
    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->applicationsInfo:Lcom/konka/kkinterface/tv/CommonDesk$ApplicationsInfo;

    return-object v1
.end method

.method public getAudioInfo()Lcom/konka/kkinterface/tv/CommonDesk$AudioInfo;
    .locals 4

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->audioInfo:Lcom/konka/kkinterface/tv/CommonDesk$AudioInfo;

    if-nez v1, :cond_0

    new-instance v1, Lcom/konka/kkinterface/tv/CommonDesk$AudioInfo;

    invoke-direct {v1}, Lcom/konka/kkinterface/tv/CommonDesk$AudioInfo;-><init>()V

    iput-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->audioInfo:Lcom/konka/kkinterface/tv/CommonDesk$AudioInfo;

    new-instance v0, Lcom/konka/kkimplements/common/IniEditor;

    invoke-direct {v0}, Lcom/konka/kkimplements/common/IniEditor;-><init>()V

    const-string v1, "customercfg/model/config.ini"

    invoke-virtual {v0, v1}, Lcom/konka/kkimplements/common/IniEditor;->loadFile(Ljava/lang/String;)Z

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->audioInfo:Lcom/konka/kkinterface/tv/CommonDesk$AudioInfo;

    const-string v2, "MISC_SYSTEM_OPTIONS:AUDIO_SRS_ENABLE"

    const-string v3, "1"

    invoke-virtual {v0, v2, v3}, Lcom/konka/kkimplements/common/IniEditor;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, v1, Lcom/konka/kkinterface/tv/CommonDesk$AudioInfo;->enableSrs:I

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->audioInfo:Lcom/konka/kkinterface/tv/CommonDesk$AudioInfo;

    const-string v2, "MISC_SYSTEM_OPTIONS:AUDIO_COAXIAL_ENABLE"

    const-string v3, "1"

    invoke-virtual {v0, v2, v3}, Lcom/konka/kkimplements/common/IniEditor;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, v1, Lcom/konka/kkinterface/tv/CommonDesk$AudioInfo;->enableCoaxial:I

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->audioInfo:Lcom/konka/kkinterface/tv/CommonDesk$AudioInfo;

    const-string v2, "MISC_SYSTEM_OPTIONS:SOUND_BOX_ENABLE"

    const-string v3, "1"

    invoke-virtual {v0, v2, v3}, Lcom/konka/kkimplements/common/IniEditor;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, v1, Lcom/konka/kkinterface/tv/CommonDesk$AudioInfo;->enableSoundBox:I

    :cond_0
    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->audioInfo:Lcom/konka/kkinterface/tv/CommonDesk$AudioInfo;

    return-object v1
.end method

.method public getBatteryInfo()Lcom/konka/kkinterface/tv/CommonDesk$BatteryInfo;
    .locals 4

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->batteryInfo:Lcom/konka/kkinterface/tv/CommonDesk$BatteryInfo;

    if-nez v1, :cond_0

    new-instance v1, Lcom/konka/kkinterface/tv/CommonDesk$BatteryInfo;

    invoke-direct {v1}, Lcom/konka/kkinterface/tv/CommonDesk$BatteryInfo;-><init>()V

    iput-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->batteryInfo:Lcom/konka/kkinterface/tv/CommonDesk$BatteryInfo;

    new-instance v0, Lcom/konka/kkimplements/common/IniEditor;

    invoke-direct {v0}, Lcom/konka/kkimplements/common/IniEditor;-><init>()V

    const-string v1, "customercfg/model/config.ini"

    invoke-virtual {v0, v1}, Lcom/konka/kkimplements/common/IniEditor;->loadFile(Ljava/lang/String;)Z

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->batteryInfo:Lcom/konka/kkinterface/tv/CommonDesk$BatteryInfo;

    const-string v2, "MISC_SYSTEM_OPTIONS:BATTERY_ENABLE"

    const-string v3, "0"

    invoke-virtual {v0, v2, v3}, Lcom/konka/kkimplements/common/IniEditor;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, v1, Lcom/konka/kkinterface/tv/CommonDesk$BatteryInfo;->enableBattery:I

    :cond_0
    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->batteryInfo:Lcom/konka/kkinterface/tv/CommonDesk$BatteryInfo;

    return-object v1
.end method

.method public getCPUInfo()Lcom/konka/kkinterface/tv/CommonDesk$CPUInfo;
    .locals 4

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->cpuInfo:Lcom/konka/kkinterface/tv/CommonDesk$CPUInfo;

    if-nez v1, :cond_0

    new-instance v1, Lcom/konka/kkinterface/tv/CommonDesk$CPUInfo;

    invoke-direct {v1}, Lcom/konka/kkinterface/tv/CommonDesk$CPUInfo;-><init>()V

    iput-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->cpuInfo:Lcom/konka/kkinterface/tv/CommonDesk$CPUInfo;

    new-instance v0, Lcom/konka/kkimplements/common/IniEditor;

    invoke-direct {v0}, Lcom/konka/kkimplements/common/IniEditor;-><init>()V

    const-string v1, "customercfg/model/config.ini"

    invoke-virtual {v0, v1}, Lcom/konka/kkimplements/common/IniEditor;->loadFile(Ljava/lang/String;)Z

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->cpuInfo:Lcom/konka/kkinterface/tv/CommonDesk$CPUInfo;

    const-string v2, "MISC_SYSTEM_OPTIONS:CPUCORE_TYPE"

    const-string v3, "0"

    invoke-virtual {v0, v2, v3}, Lcom/konka/kkimplements/common/IniEditor;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, v1, Lcom/konka/kkinterface/tv/CommonDesk$CPUInfo;->iCPUCoreType:I

    :cond_0
    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->cpuInfo:Lcom/konka/kkinterface/tv/CommonDesk$CPUInfo;

    return-object v1
.end method

.method public getCiCardInfo()Lcom/konka/kkinterface/tv/CommonDesk$CiCardInfo;
    .locals 4

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->ciCardInfo:Lcom/konka/kkinterface/tv/CommonDesk$CiCardInfo;

    if-nez v1, :cond_0

    new-instance v1, Lcom/konka/kkinterface/tv/CommonDesk$CiCardInfo;

    invoke-direct {v1}, Lcom/konka/kkinterface/tv/CommonDesk$CiCardInfo;-><init>()V

    iput-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->ciCardInfo:Lcom/konka/kkinterface/tv/CommonDesk$CiCardInfo;

    new-instance v0, Lcom/konka/kkimplements/common/IniEditor;

    invoke-direct {v0}, Lcom/konka/kkimplements/common/IniEditor;-><init>()V

    const-string v1, "customercfg/model/config.ini"

    invoke-virtual {v0, v1}, Lcom/konka/kkimplements/common/IniEditor;->loadFile(Ljava/lang/String;)Z

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->ciCardInfo:Lcom/konka/kkinterface/tv/CommonDesk$CiCardInfo;

    const-string v2, "MISC_SYSTEM_OPTIONS:CICARD_ENABLE"

    const-string v3, "1"

    invoke-virtual {v0, v2, v3}, Lcom/konka/kkimplements/common/IniEditor;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, v1, Lcom/konka/kkinterface/tv/CommonDesk$CiCardInfo;->enableCiCard:I

    :cond_0
    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->ciCardInfo:Lcom/konka/kkinterface/tv/CommonDesk$CiCardInfo;

    return-object v1
.end method

.method public getCustomerInfo()Lcom/konka/kkinterface/tv/CommonDesk$CustomerInfo;
    .locals 4

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->customerInfo:Lcom/konka/kkinterface/tv/CommonDesk$CustomerInfo;

    if-nez v1, :cond_0

    new-instance v1, Lcom/konka/kkinterface/tv/CommonDesk$CustomerInfo;

    invoke-direct {v1}, Lcom/konka/kkinterface/tv/CommonDesk$CustomerInfo;-><init>()V

    iput-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->customerInfo:Lcom/konka/kkinterface/tv/CommonDesk$CustomerInfo;

    new-instance v0, Lcom/konka/kkimplements/common/IniEditor;

    invoke-direct {v0}, Lcom/konka/kkimplements/common/IniEditor;-><init>()V

    const-string v1, "customercfg/model/config.ini"

    invoke-virtual {v0, v1}, Lcom/konka/kkimplements/common/IniEditor;->loadFile(Ljava/lang/String;)Z

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->customerInfo:Lcom/konka/kkinterface/tv/CommonDesk$CustomerInfo;

    const-string v2, "MISC_CUSTOMER_CFG:CUSTOMER"

    const-string v3, "default"

    invoke-virtual {v0, v2, v3}, Lcom/konka/kkimplements/common/IniEditor;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/konka/kkinterface/tv/CommonDesk$CustomerInfo;->strCustomerName:Ljava/lang/String;

    :cond_0
    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->customerInfo:Lcom/konka/kkinterface/tv/CommonDesk$CustomerInfo;

    return-object v1
.end method

.method public getExternalStorageInfo()Lcom/konka/kkinterface/tv/CommonDesk$ExternalStorageInfo;
    .locals 4

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->exStorageInfo:Lcom/konka/kkinterface/tv/CommonDesk$ExternalStorageInfo;

    if-nez v1, :cond_0

    new-instance v1, Lcom/konka/kkinterface/tv/CommonDesk$ExternalStorageInfo;

    invoke-direct {v1}, Lcom/konka/kkinterface/tv/CommonDesk$ExternalStorageInfo;-><init>()V

    iput-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->exStorageInfo:Lcom/konka/kkinterface/tv/CommonDesk$ExternalStorageInfo;

    new-instance v0, Lcom/konka/kkimplements/common/IniEditor;

    invoke-direct {v0}, Lcom/konka/kkimplements/common/IniEditor;-><init>()V

    const-string v1, "customercfg/model/config.ini"

    invoke-virtual {v0, v1}, Lcom/konka/kkimplements/common/IniEditor;->loadFile(Ljava/lang/String;)Z

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->exStorageInfo:Lcom/konka/kkinterface/tv/CommonDesk$ExternalStorageInfo;

    const-string v2, "MISC_SYSTEM_OPTIONS:SDCARD_SLOT_ENABLE"

    const-string v3, "1"

    invoke-virtual {v0, v2, v3}, Lcom/konka/kkimplements/common/IniEditor;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, v1, Lcom/konka/kkinterface/tv/CommonDesk$ExternalStorageInfo;->hasSDCardSlot:I

    :cond_0
    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->exStorageInfo:Lcom/konka/kkinterface/tv/CommonDesk$ExternalStorageInfo;

    return-object v1
.end method

.method public getInputSourceInfo()Lcom/konka/kkinterface/tv/CommonDesk$InputSourceInfo;
    .locals 4

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->inputSourceInfo:Lcom/konka/kkinterface/tv/CommonDesk$InputSourceInfo;

    if-nez v1, :cond_0

    new-instance v1, Lcom/konka/kkinterface/tv/CommonDesk$InputSourceInfo;

    invoke-direct {v1}, Lcom/konka/kkinterface/tv/CommonDesk$InputSourceInfo;-><init>()V

    iput-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->inputSourceInfo:Lcom/konka/kkinterface/tv/CommonDesk$InputSourceInfo;

    new-instance v0, Lcom/konka/kkimplements/common/IniEditor;

    invoke-direct {v0}, Lcom/konka/kkimplements/common/IniEditor;-><init>()V

    const-string v1, "customercfg/model/config.ini"

    invoke-virtual {v0, v1}, Lcom/konka/kkimplements/common/IniEditor;->loadFile(Ljava/lang/String;)Z

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->inputSourceInfo:Lcom/konka/kkinterface/tv/CommonDesk$InputSourceInfo;

    const-string v2, "MISC_SYSTEM_OPTIONS:INPUT_SOURCE_ATV_COUNT"

    const-string v3, "1"

    invoke-virtual {v0, v2, v3}, Lcom/konka/kkimplements/common/IniEditor;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, v1, Lcom/konka/kkinterface/tv/CommonDesk$InputSourceInfo;->sourceCountATV:I

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->inputSourceInfo:Lcom/konka/kkinterface/tv/CommonDesk$InputSourceInfo;

    const-string v2, "MISC_SYSTEM_OPTIONS:INPUT_SOURCE_DTV_COUNT"

    const-string v3, "1"

    invoke-virtual {v0, v2, v3}, Lcom/konka/kkimplements/common/IniEditor;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, v1, Lcom/konka/kkinterface/tv/CommonDesk$InputSourceInfo;->sourceCountDTV:I

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->inputSourceInfo:Lcom/konka/kkinterface/tv/CommonDesk$InputSourceInfo;

    const-string v2, "MISC_SYSTEM_OPTIONS:INPUT_SOURCE_AV_COUNT"

    const-string v3, "2"

    invoke-virtual {v0, v2, v3}, Lcom/konka/kkimplements/common/IniEditor;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, v1, Lcom/konka/kkinterface/tv/CommonDesk$InputSourceInfo;->sourceCountAV:I

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->inputSourceInfo:Lcom/konka/kkinterface/tv/CommonDesk$InputSourceInfo;

    const-string v2, "MISC_SYSTEM_OPTIONS:INPUT_SOURCE_YPBPR_COUNT"

    const-string v3, "1"

    invoke-virtual {v0, v2, v3}, Lcom/konka/kkimplements/common/IniEditor;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, v1, Lcom/konka/kkinterface/tv/CommonDesk$InputSourceInfo;->sourceCountYPbPr:I

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->inputSourceInfo:Lcom/konka/kkinterface/tv/CommonDesk$InputSourceInfo;

    const-string v2, "MISC_SYSTEM_OPTIONS:INPUT_SOURCE_VGA_COUNT"

    const-string v3, "1"

    invoke-virtual {v0, v2, v3}, Lcom/konka/kkimplements/common/IniEditor;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, v1, Lcom/konka/kkinterface/tv/CommonDesk$InputSourceInfo;->sourceCountVGA:I

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->inputSourceInfo:Lcom/konka/kkinterface/tv/CommonDesk$InputSourceInfo;

    const-string v2, "MISC_SYSTEM_OPTIONS:INPUT_SOURCE_HDMI_COUNT"

    const-string v3, "3"

    invoke-virtual {v0, v2, v3}, Lcom/konka/kkimplements/common/IniEditor;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, v1, Lcom/konka/kkinterface/tv/CommonDesk$InputSourceInfo;->sourceCountHDMI:I

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->inputSourceInfo:Lcom/konka/kkinterface/tv/CommonDesk$InputSourceInfo;

    const-string v2, "MISC_SYSTEM_OPTIONS:INPUT_SOURCE_STORAGE_COUNT"

    const-string v3, "1"

    invoke-virtual {v0, v2, v3}, Lcom/konka/kkimplements/common/IniEditor;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, v1, Lcom/konka/kkinterface/tv/CommonDesk$InputSourceInfo;->sourceCountSTORAGE:I

    invoke-virtual {v0}, Lcom/konka/kkimplements/common/IniEditor;->unloadFile()V

    :cond_0
    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->inputSourceInfo:Lcom/konka/kkinterface/tv/CommonDesk$InputSourceInfo;

    return-object v1
.end method

.method public getLocalDimmingInfo()Lcom/konka/kkinterface/tv/CommonDesk$LocalDimmingInfo;
    .locals 4

    const-string v1, "DEBUG"

    const-string v2, "getLocalDimmingInfo1"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->localdimmingInfo:Lcom/konka/kkinterface/tv/CommonDesk$LocalDimmingInfo;

    if-nez v1, :cond_0

    const-string v1, "DEBUG"

    const-string v2, "getLocalDimmingInfo2"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Lcom/konka/kkinterface/tv/CommonDesk$LocalDimmingInfo;

    invoke-direct {v1}, Lcom/konka/kkinterface/tv/CommonDesk$LocalDimmingInfo;-><init>()V

    iput-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->localdimmingInfo:Lcom/konka/kkinterface/tv/CommonDesk$LocalDimmingInfo;

    new-instance v0, Lcom/konka/kkimplements/common/IniEditor;

    invoke-direct {v0}, Lcom/konka/kkimplements/common/IniEditor;-><init>()V

    const-string v1, "/customercfg/panel/panel.ini"

    invoke-virtual {v0, v1}, Lcom/konka/kkimplements/common/IniEditor;->loadFile(Ljava/lang/String;)Z

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->localdimmingInfo:Lcom/konka/kkinterface/tv/CommonDesk$LocalDimmingInfo;

    const-string v2, "LocalDIMMING:bLocalDIMMINGEnable"

    const-string v3, "0"

    invoke-virtual {v0, v2, v3}, Lcom/konka/kkimplements/common/IniEditor;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v1, Lcom/konka/kkinterface/tv/CommonDesk$LocalDimmingInfo;->localdimmingenable:I

    :cond_0
    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->localdimmingInfo:Lcom/konka/kkinterface/tv/CommonDesk$LocalDimmingInfo;

    return-object v1
.end method

.method public getPanel4K2K()Lcom/konka/kkinterface/tv/CommonDesk$Panel4K2K;
    .locals 4

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->panel4k2k:Lcom/konka/kkinterface/tv/CommonDesk$Panel4K2K;

    if-nez v1, :cond_0

    new-instance v1, Lcom/konka/kkinterface/tv/CommonDesk$Panel4K2K;

    invoke-direct {v1}, Lcom/konka/kkinterface/tv/CommonDesk$Panel4K2K;-><init>()V

    iput-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->panel4k2k:Lcom/konka/kkinterface/tv/CommonDesk$Panel4K2K;

    new-instance v0, Lcom/konka/kkimplements/common/IniEditor;

    invoke-direct {v0}, Lcom/konka/kkimplements/common/IniEditor;-><init>()V

    const-string v1, "/customercfg/panel/panel.ini"

    invoke-virtual {v0, v1}, Lcom/konka/kkimplements/common/IniEditor;->loadFile(Ljava/lang/String;)Z

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->panel4k2k:Lcom/konka/kkinterface/tv/CommonDesk$Panel4K2K;

    const-string v2, "panel:m_bPanel4K2K"

    const-string v3, "0"

    invoke-virtual {v0, v2, v3}, Lcom/konka/kkimplements/common/IniEditor;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, v1, Lcom/konka/kkinterface/tv/CommonDesk$Panel4K2K;->isPanelWidth4K:I

    :cond_0
    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->panel4k2k:Lcom/konka/kkinterface/tv/CommonDesk$Panel4K2K;

    return-object v1
.end method

.method public getPanelSupportInfo()Lcom/konka/kkinterface/tv/CommonDesk$PanelSupportInfo;
    .locals 5

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->panelsupportInfo:Lcom/konka/kkinterface/tv/CommonDesk$PanelSupportInfo;

    if-nez v2, :cond_0

    new-instance v2, Lcom/konka/kkinterface/tv/CommonDesk$PanelSupportInfo;

    invoke-direct {v2}, Lcom/konka/kkinterface/tv/CommonDesk$PanelSupportInfo;-><init>()V

    iput-object v2, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->panelsupportInfo:Lcom/konka/kkinterface/tv/CommonDesk$PanelSupportInfo;

    new-instance v0, Lcom/konka/kkimplements/common/IniEditor;

    invoke-direct {v0}, Lcom/konka/kkimplements/common/IniEditor;-><init>()V

    const-string v2, "customercfg/panel/panel.ini"

    invoke-virtual {v0, v2}, Lcom/konka/kkimplements/common/IniEditor;->loadFile(Ljava/lang/String;)Z

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->panelsupportInfo:Lcom/konka/kkinterface/tv/CommonDesk$PanelSupportInfo;

    const-string v3, "PANEL_SUPPORT:3DBL_ADJUSTABLE"

    const-string v4, "0"

    invoke-virtual {v0, v3, v4}, Lcom/konka/kkimplements/common/IniEditor;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iput v3, v2, Lcom/konka/kkinterface/tv/CommonDesk$PanelSupportInfo;->isBLAdjustableFor3D:I

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->panelsupportInfo:Lcom/konka/kkinterface/tv/CommonDesk$PanelSupportInfo;

    const-string v3, "PANEL_SUPPORT:bDeepStandbySupportFlag"

    const-string v4, "0"

    invoke-virtual {v0, v3, v4}, Lcom/konka/kkimplements/common/IniEditor;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-ne v3, v1, :cond_1

    :goto_0
    iput-boolean v1, v2, Lcom/konka/kkinterface/tv/CommonDesk$PanelSupportInfo;->isSupportDeepStandby:Z

    :cond_0
    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->panelsupportInfo:Lcom/konka/kkinterface/tv/CommonDesk$PanelSupportInfo;

    return-object v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getPipInfo()Lcom/konka/kkinterface/tv/CommonDesk$PipInfo;
    .locals 4

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->pipInfo:Lcom/konka/kkinterface/tv/CommonDesk$PipInfo;

    if-nez v1, :cond_0

    new-instance v1, Lcom/konka/kkinterface/tv/CommonDesk$PipInfo;

    invoke-direct {v1}, Lcom/konka/kkinterface/tv/CommonDesk$PipInfo;-><init>()V

    iput-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->pipInfo:Lcom/konka/kkinterface/tv/CommonDesk$PipInfo;

    new-instance v0, Lcom/konka/kkimplements/common/IniEditor;

    invoke-direct {v0}, Lcom/konka/kkimplements/common/IniEditor;-><init>()V

    const-string v1, "customercfg/model/config.ini"

    invoke-virtual {v0, v1}, Lcom/konka/kkimplements/common/IniEditor;->loadFile(Ljava/lang/String;)Z

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->pipInfo:Lcom/konka/kkinterface/tv/CommonDesk$PipInfo;

    const-string v2, "MISC_SYSTEM_OPTIONS:PIP_ENABLE"

    const-string v3, "0"

    invoke-virtual {v0, v2, v3}, Lcom/konka/kkimplements/common/IniEditor;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, v1, Lcom/konka/kkinterface/tv/CommonDesk$PipInfo;->enablePip:I

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->pipInfo:Lcom/konka/kkinterface/tv/CommonDesk$PipInfo;

    const-string v2, "MISC_SYSTEM_OPTIONS:DUALVIEW_ENABLE"

    const-string v3, "0"

    invoke-virtual {v0, v2, v3}, Lcom/konka/kkimplements/common/IniEditor;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, v1, Lcom/konka/kkinterface/tv/CommonDesk$PipInfo;->enableDualView:I

    :cond_0
    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->pipInfo:Lcom/konka/kkinterface/tv/CommonDesk$PipInfo;

    return-object v1
.end method

.method public getSourceList()[I
    .locals 1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->psl:[I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->psl:[I

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSystemBoardInfo()Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;
    .locals 8

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->systemBoardInfo:Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;

    if-nez v4, :cond_0

    new-instance v4, Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;

    invoke-direct {v4}, Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;-><init>()V

    iput-object v4, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->systemBoardInfo:Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;

    new-instance v1, Lcom/konka/kkimplements/common/IniEditor;

    invoke-direct {v1}, Lcom/konka/kkimplements/common/IniEditor;-><init>()V

    const-string v4, "/customercfg/model/config.ini"

    invoke-virtual {v1, v4}, Lcom/konka/kkimplements/common/IniEditor;->loadFile(Ljava/lang/String;)Z

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->systemBoardInfo:Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;

    const-string v5, "MISC_BOARD_CFG:PLATFORM"

    const-string v6, "6a801"

    invoke-virtual {v1, v5, v6}, Lcom/konka/kkimplements/common/IniEditor;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;->strPlatform:Ljava/lang/String;

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->systemBoardInfo:Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;

    const-string v5, "MISC_BOARD_CFG:BOARD"

    const-string v6, "LEDxx"

    invoke-virtual {v1, v5, v6}, Lcom/konka/kkimplements/common/IniEditor;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;->strBoard:Ljava/lang/String;

    invoke-virtual {v1}, Lcom/konka/kkimplements/common/IniEditor;->unloadFile()V

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->systemBoardInfo:Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/mstar/android/tvapi/common/TvManager;->getSystemPanelName()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;->strPanelName:Ljava/lang/String;

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->systemBoardInfo:Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v5

    const-string v6, "MBoot_VER"

    invoke-virtual {v5, v6}, Lcom/mstar/android/tvapi/common/TvManager;->getEnvironment(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;->strVersionMboot:Ljava/lang/String;

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getFactoryManager()Lcom/mstar/android/tvapi/factory/FactoryManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/factory/FactoryManager;->ursaGetVersionInfo()Lcom/mstar/android/tvapi/factory/vo/UrsaVersionInfo;

    move-result-object v3

    if-eqz v3, :cond_1

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->systemBoardInfo:Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "V"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v6, v3, Lcom/mstar/android/tvapi/factory/vo/UrsaVersionInfo;->u16Version:I

    invoke-static {v6}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-wide v6, v3, Lcom/mstar/android/tvapi/factory/vo/UrsaVersionInfo;->u32Changelist:J

    invoke-static {v6, v7}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;->strVersion6m30:Ljava/lang/String;

    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "fetch from sn:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->systemBoardInfo:Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;

    iget-object v6, v6, Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;->strVersion6m30:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const-string v4, "/customercfg/model/version.ini"

    invoke-virtual {v1, v4}, Lcom/konka/kkimplements/common/IniEditor;->loadFile(Ljava/lang/String;)Z

    new-instance v4, Ljava/lang/StringBuilder;

    iget-object v5, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->systemBoardInfo:Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;

    iget-object v5, v5, Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;->strPanelName:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, ":MODEL"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->systemBoardInfo:Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;

    const-string v5, "LED"

    invoke-virtual {v1, v2, v5}, Lcom/konka/kkimplements/common/IniEditor;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;->strSerial:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    iget-object v5, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->systemBoardInfo:Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;

    iget-object v5, v5, Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;->strPanelName:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, ":VERSION"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->systemBoardInfo:Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;

    const-string v5, "V1.0.00"

    invoke-virtual {v1, v2, v5}, Lcom/konka/kkimplements/common/IniEditor;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;->strVersion:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    iget-object v5, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->systemBoardInfo:Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;

    iget-object v5, v5, Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;->strPanelName:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, ":BUILD"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->systemBoardInfo:Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;

    const-string v5, "1234"

    invoke-virtual {v1, v2, v5}, Lcom/konka/kkimplements/common/IniEditor;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;->strBuild:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    iget-object v5, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->systemBoardInfo:Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;

    iget-object v5, v5, Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;->strPanelName:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, ":CODE"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->systemBoardInfo:Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;

    const-string v5, "99000000"

    invoke-virtual {v1, v2, v5}, Lcom/konka/kkimplements/common/IniEditor;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;->strCode:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    iget-object v5, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->systemBoardInfo:Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;

    iget-object v5, v5, Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;->strPanelName:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, ":DATE"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->systemBoardInfo:Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;

    const-string v5, "00:00:00 2012-02-25"

    invoke-virtual {v1, v2, v5}, Lcom/konka/kkimplements/common/IniEditor;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;->strDate:Ljava/lang/String;

    invoke-virtual {v1}, Lcom/konka/kkimplements/common/IniEditor;->unloadFile()V

    :cond_0
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    iget-object v5, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->systemBoardInfo:Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;

    iget-object v5, v5, Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;->strPlatform:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    iget-object v5, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->systemBoardInfo:Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;

    iget-object v5, v5, Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;->strSerial:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    iget-object v5, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->systemBoardInfo:Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;

    iget-object v5, v5, Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;->strPanelName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    iget-object v5, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->systemBoardInfo:Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;

    iget-object v5, v5, Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;->strVersion:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    iget-object v5, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->systemBoardInfo:Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;

    iget-object v5, v5, Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;->strBuild:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    iget-object v5, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->systemBoardInfo:Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;

    iget-object v5, v5, Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;->strCode:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    iget-object v5, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->systemBoardInfo:Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;

    iget-object v5, v5, Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;->strDate:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    iget-object v5, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->systemBoardInfo:Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;

    iget-object v5, v5, Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;->strVersion6m30:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->systemBoardInfo:Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;

    return-object v4

    :cond_1
    :try_start_1
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v5, "Can\'t fetch 6M30version!!!"

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->systemBoardInfo:Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;

    const-string v5, "No"

    iput-object v5, v4, Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;->strVersion6m30:Ljava/lang/String;
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v0

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->systemBoardInfo:Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;

    const-string v5, "panel"

    iput-object v5, v4, Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;->strPanelName:Ljava/lang/String;

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->systemBoardInfo:Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;

    const-string v5, "V1.0.00"

    iput-object v5, v4, Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;->strVersionMboot:Ljava/lang/String;

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->systemBoardInfo:Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;

    const-string v5, "No"

    iput-object v5, v4, Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;->strVersion6m30:Ljava/lang/String;

    goto/16 :goto_0

    :cond_2
    :try_start_2
    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->systemBoardInfo:Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;

    const-string v5, "panel"

    iput-object v5, v4, Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;->strPanelName:Ljava/lang/String;

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->systemBoardInfo:Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;

    const-string v5, "V1.0.00"

    iput-object v5, v4, Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;->strVersionMboot:Ljava/lang/String;

    iget-object v4, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->systemBoardInfo:Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;

    const-string v5, "No"

    iput-object v5, v4, Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;->strVersion6m30:Ljava/lang/String;
    :try_end_2
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_0
.end method

.method public getSystemBuildInfo()Lcom/konka/kkinterface/tv/CommonDesk$SystemBuildInfo;
    .locals 6

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->systemBuildInfo:Lcom/konka/kkinterface/tv/CommonDesk$SystemBuildInfo;

    if-nez v3, :cond_0

    new-instance v3, Lcom/konka/kkinterface/tv/CommonDesk$SystemBuildInfo;

    invoke-direct {v3}, Lcom/konka/kkinterface/tv/CommonDesk$SystemBuildInfo;-><init>()V

    iput-object v3, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->systemBuildInfo:Lcom/konka/kkinterface/tv/CommonDesk$SystemBuildInfo;

    const/4 v1, 0x0

    :try_start_0
    new-instance v2, Lcom/konka/kkimplements/common/IniReader;

    const-string v3, "/tvcustomer/Customer/sn_build.ini"

    invoke-direct {v2, v3}, Lcom/konka/kkimplements/common/IniReader;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v1, v2

    :goto_0
    if-eqz v1, :cond_0

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->systemBuildInfo:Lcom/konka/kkinterface/tv/CommonDesk$SystemBuildInfo;

    const-string v4, "SN_BUILD"

    const-string v5, "VERSION"

    invoke-virtual {v1, v4, v5}, Lcom/konka/kkimplements/common/IniReader;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/konka/kkinterface/tv/CommonDesk$SystemBuildInfo;->strTVVersion:Ljava/lang/String;

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->systemBuildInfo:Lcom/konka/kkinterface/tv/CommonDesk$SystemBuildInfo;

    const-string v4, "SN_BUILD"

    const-string v5, "BUILD"

    invoke-virtual {v1, v4, v5}, Lcom/konka/kkimplements/common/IniReader;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/konka/kkinterface/tv/CommonDesk$SystemBuildInfo;->strBuild:Ljava/lang/String;

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->systemBuildInfo:Lcom/konka/kkinterface/tv/CommonDesk$SystemBuildInfo;

    const-string v4, "SN_BUILD"

    const-string v5, "DATE"

    invoke-virtual {v1, v4, v5}, Lcom/konka/kkimplements/common/IniReader;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/konka/kkinterface/tv/CommonDesk$SystemBuildInfo;->strDate:Ljava/lang/String;

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->systemBuildInfo:Lcom/konka/kkinterface/tv/CommonDesk$SystemBuildInfo;

    const-string v4, "SN_BUILD"

    const-string v5, "DATE_UTC"

    invoke-virtual {v1, v4, v5}, Lcom/konka/kkimplements/common/IniReader;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/konka/kkinterface/tv/CommonDesk$SystemBuildInfo;->strDateUTC:Ljava/lang/String;

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->systemBuildInfo:Lcom/konka/kkinterface/tv/CommonDesk$SystemBuildInfo;

    const-string v4, "SN_BUILD"

    const-string v5, "HOST"

    invoke-virtual {v1, v4, v5}, Lcom/konka/kkimplements/common/IniReader;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/konka/kkinterface/tv/CommonDesk$SystemBuildInfo;->strHost:Ljava/lang/String;

    :cond_0
    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->systemBuildInfo:Lcom/konka/kkinterface/tv/CommonDesk$SystemBuildInfo;

    return-object v3

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method public getUrsaSelect()Lcom/konka/kkinterface/tv/CommonDesk$UrsaSelectEnumType;
    .locals 6

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->ursaSelectType:Lcom/konka/kkinterface/tv/CommonDesk$UrsaSelectEnumType;

    if-nez v3, :cond_0

    new-instance v1, Lcom/konka/kkimplements/common/IniEditor;

    invoke-direct {v1}, Lcom/konka/kkimplements/common/IniEditor;-><init>()V

    const-string v3, "/customercfg/model/Customer_1.ini"

    invoke-virtual {v1, v3}, Lcom/konka/kkimplements/common/IniEditor;->loadFile(Ljava/lang/String;)Z

    :try_start_0
    const-string v3, "Ursa:UrsaSelect"

    const-string v4, "0"

    invoke-virtual {v1, v3, v4}, Lcom/konka/kkimplements/common/IniEditor;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    :goto_0
    const-string v3, "CommonDeskImpl"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "UrsaSelect = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v3, "0x30"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    sget-object v3, Lcom/konka/kkinterface/tv/CommonDesk$UrsaSelectEnumType;->SIXM30_IC:Lcom/konka/kkinterface/tv/CommonDesk$UrsaSelectEnumType;

    iput-object v3, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->ursaSelectType:Lcom/konka/kkinterface/tv/CommonDesk$UrsaSelectEnumType;

    :cond_0
    :goto_1
    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->ursaSelectType:Lcom/konka/kkinterface/tv/CommonDesk$UrsaSelectEnumType;

    return-object v3

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->printStackTrace()V

    const-string v2, ""

    goto :goto_0

    :cond_1
    const-string v3, "7"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    sget-object v3, Lcom/konka/kkinterface/tv/CommonDesk$UrsaSelectEnumType;->SIXM40_IC:Lcom/konka/kkinterface/tv/CommonDesk$UrsaSelectEnumType;

    iput-object v3, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->ursaSelectType:Lcom/konka/kkinterface/tv/CommonDesk$UrsaSelectEnumType;

    goto :goto_1

    :cond_2
    sget-object v3, Lcom/konka/kkinterface/tv/CommonDesk$UrsaSelectEnumType;->OTHER_IC:Lcom/konka/kkinterface/tv/CommonDesk$UrsaSelectEnumType;

    iput-object v3, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->ursaSelectType:Lcom/konka/kkinterface/tv/CommonDesk$UrsaSelectEnumType;

    goto :goto_1
.end method

.method public getVideoInfo()Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;
    .locals 5

    const/4 v1, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPlayerManager()Lcom/mstar/android/tvapi/common/TvPlayer;

    move-result-object v2

    invoke-interface {v2}, Lcom/mstar/android/tvapi/common/TvPlayer;->getVideoInfo()Lcom/mstar/android/tvapi/common/vo/VideoInfo;

    move-result-object v1

    :cond_0
    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->videoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    iget v3, v1, Lcom/mstar/android/tvapi/common/vo/VideoInfo;->frameRate:I

    int-to-short v3, v3

    iput-short v3, v2, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16FrameRate:S

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->videoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    iget v3, v1, Lcom/mstar/android/tvapi/common/vo/VideoInfo;->hResolution:I

    int-to-short v3, v3

    iput-short v3, v2, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16HResolution:S

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->videoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    iget v3, v1, Lcom/mstar/android/tvapi/common/vo/VideoInfo;->vResolution:I

    int-to-short v3, v3

    iput-short v3, v2, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16VResolution:S

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->videoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    iget v3, v1, Lcom/mstar/android/tvapi/common/vo/VideoInfo;->modeIndex:I

    int-to-short v3, v3

    iput-short v3, v2, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16ModeIndex:S

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->videoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    invoke-static {}, Lcom/konka/kkinterface/tv/CommonDesk$EN_SCAN_TYPE;->values()[Lcom/konka/kkinterface/tv/CommonDesk$EN_SCAN_TYPE;

    move-result-object v3

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/VideoInfo;->getScanType()Lcom/mstar/android/tvapi/common/vo/VideoInfo$EnumScanType;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/vo/VideoInfo$EnumScanType;->ordinal()I

    move-result v4

    aget-object v3, v3, v4

    iput-object v3, v2, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->enScanType:Lcom/konka/kkinterface/tv/CommonDesk$EN_SCAN_TYPE;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->videoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    return-object v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public getWifiDeviceInfo()Lcom/konka/kkinterface/tv/CommonDesk$WifiDeviceInfo;
    .locals 4

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->wifiDeviceInfo:Lcom/konka/kkinterface/tv/CommonDesk$WifiDeviceInfo;

    if-nez v1, :cond_0

    new-instance v1, Lcom/konka/kkinterface/tv/CommonDesk$WifiDeviceInfo;

    invoke-direct {v1}, Lcom/konka/kkinterface/tv/CommonDesk$WifiDeviceInfo;-><init>()V

    iput-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->wifiDeviceInfo:Lcom/konka/kkinterface/tv/CommonDesk$WifiDeviceInfo;

    new-instance v0, Lcom/konka/kkimplements/common/IniEditor;

    invoke-direct {v0}, Lcom/konka/kkimplements/common/IniEditor;-><init>()V

    const-string v1, "customercfg/model/config.ini"

    invoke-virtual {v0, v1}, Lcom/konka/kkimplements/common/IniEditor;->loadFile(Ljava/lang/String;)Z

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->wifiDeviceInfo:Lcom/konka/kkinterface/tv/CommonDesk$WifiDeviceInfo;

    const-string v2, "MISC_SYSTEM_OPTIONS:WIFIDEVICE_INTERNAL"

    const-string v3, "1"

    invoke-virtual {v0, v2, v3}, Lcom/konka/kkimplements/common/IniEditor;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, v1, Lcom/konka/kkinterface/tv/CommonDesk$WifiDeviceInfo;->isDeviceInternal:I

    :cond_0
    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->wifiDeviceInfo:Lcom/konka/kkinterface/tv/CommonDesk$WifiDeviceInfo;

    return-object v1
.end method

.method public is1280x720(Landroid/content/Context;)Z
    .locals 4
    .param p1    # Landroid/content/Context;

    const-string v2, "window"

    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager;

    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    iget v2, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    const/16 v3, 0x500

    if-ne v2, v3, :cond_0

    iget v2, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    const/16 v3, 0x2d0

    if-ne v2, v3, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public isEnableAdvancedPipSyncview()Z
    .locals 6

    const/4 v2, 0x0

    new-instance v1, Lcom/konka/kkimplements/common/IniEditor;

    invoke-direct {v1}, Lcom/konka/kkimplements/common/IniEditor;-><init>()V

    const-string v3, "customercfg/model/config.ini"

    invoke-virtual {v1, v3}, Lcom/konka/kkimplements/common/IniEditor;->loadFile(Ljava/lang/String;)Z

    const-string v3, "MISC_SPECIAL_FUNCTION:ADVANCED_PIP_SYNCVIEW"

    const-string v4, "0"

    invoke-virtual {v1, v3, v4}, Lcom/konka/kkimplements/common/IniEditor;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const-string v3, "DEBUG"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "MISC_SPECIAL_FUNCTION:ADVANCED_PIP_SYNCVIEW["

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v3, 0x1

    if-ne v0, v3, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public isEnableAlTimeShift()Z
    .locals 6

    const/4 v2, 0x0

    new-instance v1, Lcom/konka/kkimplements/common/IniEditor;

    invoke-direct {v1}, Lcom/konka/kkimplements/common/IniEditor;-><init>()V

    const-string v3, "customercfg/model/config.ini"

    invoke-virtual {v1, v3}, Lcom/konka/kkimplements/common/IniEditor;->loadFile(Ljava/lang/String;)Z

    const-string v3, "MISC_SPECIAL_FUNCTION:FUNCTION"

    const-string v4, "1"

    invoke-virtual {v1, v3, v4}, Lcom/konka/kkimplements/common/IniEditor;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iget v4, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->AlwaysTimeShiftEnable:I

    and-int v0, v3, v4

    const-string v3, "DEBUG"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "MISC_SPECIAL_FUNCTION:FUNCTION["

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-lez v0, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public isEnableCEC()Z
    .locals 6

    const/4 v2, 0x0

    new-instance v1, Lcom/konka/kkimplements/common/IniEditor;

    invoke-direct {v1}, Lcom/konka/kkimplements/common/IniEditor;-><init>()V

    const-string v3, "customercfg/model/config.ini"

    invoke-virtual {v1, v3}, Lcom/konka/kkimplements/common/IniEditor;->loadFile(Ljava/lang/String;)Z

    const-string v3, "MISC_SPECIAL_FUNCTION:FUNCTION"

    const-string v4, "2"

    invoke-virtual {v1, v3, v4}, Lcom/konka/kkimplements/common/IniEditor;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iget v4, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->CECEnable:I

    and-int v0, v3, v4

    const-string v3, "DEBUG"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "MISC_SPECIAL_FUNCTION:FUNCTION["

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-lez v0, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public isEnableCI()Z
    .locals 6

    const/4 v2, 0x0

    new-instance v1, Lcom/konka/kkimplements/common/IniEditor;

    invoke-direct {v1}, Lcom/konka/kkimplements/common/IniEditor;-><init>()V

    const-string v3, "customercfg/model/config.ini"

    invoke-virtual {v1, v3}, Lcom/konka/kkimplements/common/IniEditor;->loadFile(Ljava/lang/String;)Z

    const-string v3, "MISC_SPECIAL_FUNCTION:FUNCTION"

    const-string v4, "3"

    invoke-virtual {v1, v3, v4}, Lcom/konka/kkimplements/common/IniEditor;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iget v4, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->CIInfo:I

    and-int v0, v3, v4

    const-string v3, "DEBUG"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "MISC_SPECIAL_FUNCTION:FUNCTION["

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-lez v0, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public isEnableDVBC()Z
    .locals 6

    const/4 v2, 0x0

    new-instance v1, Lcom/konka/kkimplements/common/IniEditor;

    invoke-direct {v1}, Lcom/konka/kkimplements/common/IniEditor;-><init>()V

    const-string v3, "customercfg/model/config.ini"

    invoke-virtual {v1, v3}, Lcom/konka/kkimplements/common/IniEditor;->loadFile(Ljava/lang/String;)Z

    const-string v3, "MISC_SPECIAL_FUNCTION:DVBC_ENABLE"

    const-string v4, "1"

    invoke-virtual {v1, v3, v4}, Lcom/konka/kkimplements/common/IniEditor;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const-string v3, "DEBUG"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "MISC_SPECIAL_FUNCTION:DVBC_ENABLE["

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-lez v0, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public isEnableStickerDemo()Z
    .locals 6

    const/4 v2, 0x0

    new-instance v1, Lcom/konka/kkimplements/common/IniEditor;

    invoke-direct {v1}, Lcom/konka/kkimplements/common/IniEditor;-><init>()V

    const-string v3, "customercfg/model/config.ini"

    invoke-virtual {v1, v3}, Lcom/konka/kkimplements/common/IniEditor;->loadFile(Ljava/lang/String;)Z

    const-string v3, "MISC_SPECIAL_FUNCTION:FUNCTION"

    const-string v4, "0"

    invoke-virtual {v1, v3, v4}, Lcom/konka/kkimplements/common/IniEditor;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iget v4, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->StickerDemo:I

    and-int v0, v3, v4

    const-string v3, "DEBUG"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "MISC_SPECIAL_FUNCTION:StickerDemo["

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-lez v0, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public isEnableUpgradeOnline()Z
    .locals 6

    const/4 v2, 0x0

    new-instance v1, Lcom/konka/kkimplements/common/IniEditor;

    invoke-direct {v1}, Lcom/konka/kkimplements/common/IniEditor;-><init>()V

    const-string v3, "customercfg/model/config.ini"

    invoke-virtual {v1, v3}, Lcom/konka/kkimplements/common/IniEditor;->loadFile(Ljava/lang/String;)Z

    const-string v3, "MISC_SPECIAL_FUNCTION:UPGRADE_ONLINE"

    const-string v4, "0"

    invoke-virtual {v1, v3, v4}, Lcom/konka/kkimplements/common/IniEditor;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const-string v3, "DEBUG"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "MISC_SPECIAL_FUNCTION:UPGRADE_ONLINE["

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v3, 0x1

    if-ne v0, v3, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public isHdmiSignalMode()Z
    .locals 1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPlayerManager()Lcom/mstar/android/tvapi/common/TvPlayer;

    move-result-object v0

    invoke-interface {v0}, Lcom/mstar/android/tvapi/common/TvPlayer;->isHdmiMode()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSignalStable()Z
    .locals 3

    const/4 v0, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPlayerManager()Lcom/mstar/android/tvapi/common/TvPlayer;

    move-result-object v2

    invoke-interface {v2}, Lcom/mstar/android/tvapi/common/TvPlayer;->isSignalStable()Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :cond_0
    :goto_0
    return v0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public isSupport3D()Z
    .locals 5

    const/4 v2, 0x1

    new-instance v1, Lcom/konka/kkimplements/common/IniEditor;

    invoke-direct {v1}, Lcom/konka/kkimplements/common/IniEditor;-><init>()V

    const-string v3, "/customercfg/model/config.ini"

    invoke-virtual {v1, v3}, Lcom/konka/kkimplements/common/IniEditor;->loadFile(Ljava/lang/String;)Z

    const-string v3, "MISC_SYSTEM_OPTIONS:IS_SUPPORT_3D"

    const-string v4, "1"

    invoke-virtual {v1, v3, v4}, Lcom/konka/kkimplements/common/IniEditor;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    const/4 v3, 0x1

    if-ne v0, v3, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public printfE(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "TvApp"

    invoke-static {v0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public printfE(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-static {p1, p2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public printfI(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "TvApp"

    invoke-static {v0, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public printfI(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-static {p1, p2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public printfV(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "TvApp"

    invoke-static {v0, p1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public printfV(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-static {p1, p2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public printfW(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "TvApp"

    invoke-static {v0, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public printfW(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-static {p1, p2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public releaseHandler(I)V
    .locals 3
    .param p1    # I

    const-string v0, "TvApp"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "releaseHandler:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    sget-boolean v0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->bThreadIsrunning:Z

    if-nez v0, :cond_1

    if-nez p1, :cond_2

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->deskTvLister:Lcom/konka/kkimplements/tv/mstar/DeskTvEventListener;

    invoke-virtual {v0}, Lcom/konka/kkimplements/tv/mstar/DeskTvEventListener;->releaseHandler()V

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->deskTvPlayerLister:Lcom/konka/kkimplements/tv/mstar/DeskTvPlayerEventListener;

    invoke-virtual {v0}, Lcom/konka/kkimplements/tv/mstar/DeskTvPlayerEventListener;->releaseHandler()V

    :cond_0
    :goto_1
    invoke-super {p0, p1}, Lcom/konka/kkimplements/tv/mstar/BaseDeskImpl;->releaseHandler(I)V

    return-void

    :cond_1
    const-string v0, "TvApp"

    const-string v1, "tv System not stable!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    const/4 v0, 0x1

    if-ne p1, v0, :cond_3

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->deskAtvPlayerLister:Lcom/konka/kkimplements/tv/mstar/DeskAtvPlayerEventListener;

    invoke-virtual {v0}, Lcom/konka/kkimplements/tv/mstar/DeskAtvPlayerEventListener;->releaseHandler()V

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->deskDtvPlayerLister:Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;

    invoke-virtual {v0}, Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;->releaseHandler()V

    goto :goto_1

    :cond_3
    const/4 v0, 0x2

    if-ne p1, v0, :cond_4

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->deskCiLister:Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener;

    invoke-virtual {v0}, Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener;->releaseHandler()V

    goto :goto_1

    :cond_4
    const/4 v0, 0x3

    if-ne p1, v0, :cond_5

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->deskTimerLister:Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener;

    invoke-virtual {v0}, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener;->releaseHandler()V

    goto :goto_1

    :cond_5
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->deskCaLister:Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener;

    invoke-virtual {v0}, Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener;->releaseHandler()V

    goto :goto_1
.end method

.method public setDisplayHolder(Landroid/view/SurfaceHolder;)Z
    .locals 2
    .param p1    # Landroid/view/SurfaceHolder;

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getPlayerManager()Lcom/mstar/android/tvapi/common/TvPlayer;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/mstar/android/tvapi/common/TvPlayer;->setDisplay(Landroid/view/SurfaceHolder;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    const/4 v1, 0x1

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setGpioDeviceStatus(IZ)Z
    .locals 2
    .param p1    # I
    .param p2    # Z

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Lcom/mstar/android/tvapi/common/TvManager;->setGpioDeviceStatus(IZ)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setHandler(Landroid/os/Handler;I)Z
    .locals 3
    .param p1    # Landroid/os/Handler;
    .param p2    # I

    const-string v0, "TvApp"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setHandler:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-nez p2, :cond_1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->deskTvLister:Lcom/konka/kkimplements/tv/mstar/DeskTvEventListener;

    invoke-virtual {v0, p1}, Lcom/konka/kkimplements/tv/mstar/DeskTvEventListener;->attachHandler(Landroid/os/Handler;)V

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->deskTvPlayerLister:Lcom/konka/kkimplements/tv/mstar/DeskTvPlayerEventListener;

    invoke-virtual {v0, p1}, Lcom/konka/kkimplements/tv/mstar/DeskTvPlayerEventListener;->attachHandler(Landroid/os/Handler;)V

    :cond_0
    :goto_0
    invoke-super {p0, p1, p2}, Lcom/konka/kkimplements/tv/mstar/BaseDeskImpl;->setHandler(Landroid/os/Handler;I)Z

    move-result v0

    return v0

    :cond_1
    const/4 v0, 0x1

    if-ne p2, v0, :cond_2

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->deskAtvPlayerLister:Lcom/konka/kkimplements/tv/mstar/DeskAtvPlayerEventListener;

    invoke-virtual {v0, p1}, Lcom/konka/kkimplements/tv/mstar/DeskAtvPlayerEventListener;->attachHandler(Landroid/os/Handler;)V

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->deskDtvPlayerLister:Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;

    invoke-virtual {v0, p1}, Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;->attachHandler(Landroid/os/Handler;)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x2

    if-ne p2, v0, :cond_3

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->deskCiLister:Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener;

    invoke-virtual {v0, p1}, Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener;->attachHandler(Landroid/os/Handler;)V

    goto :goto_0

    :cond_3
    const/4 v0, 0x3

    if-ne p2, v0, :cond_4

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->deskTimerLister:Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener;

    invoke-virtual {v0, p1}, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener;->attachHandler(Landroid/os/Handler;)V

    goto :goto_0

    :cond_4
    const/4 v0, 0x4

    if-ne p2, v0, :cond_0

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->deskCaLister:Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener;

    invoke-virtual {v0, p1}, Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener;->attachHandler(Landroid/os/Handler;)V

    goto :goto_0
.end method

.method public setTimeZone(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;Z)V
    .locals 2
    .param p1    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;
    .param p2    # Z

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getTimerManager()Lcom/mstar/android/tvapi/common/TimerManager;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Lcom/mstar/android/tvapi/common/TimerManager;->setTimeZone(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumTimeZone;Z)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public startMsrv()Z
    .locals 5

    invoke-virtual {p0}, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v0

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->context:Landroid/content/Context;

    invoke-static {v2}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getDataBaseMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    move-result-object v1

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v2

    invoke-virtual {v1}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryCurInputSrc()I

    move-result v3

    aget-object v0, v2, v3

    const-string v2, "TvApp"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "5.Start Msrv------------GetCurrentInputSource:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x1

    invoke-direct {p0, v0, v2}, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->execStartMsrv(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;Z)Z

    const/4 v2, 0x0

    return v2
.end method
