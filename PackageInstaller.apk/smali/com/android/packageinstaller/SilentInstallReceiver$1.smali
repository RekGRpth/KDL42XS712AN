.class Lcom/android/packageinstaller/SilentInstallReceiver$1;
.super Landroid/os/Handler;
.source "SilentInstallReceiver.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/packageinstaller/SilentInstallReceiver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/packageinstaller/SilentInstallReceiver;


# direct methods
.method constructor <init>(Lcom/android/packageinstaller/SilentInstallReceiver;)V
    .locals 0

    iput-object p1, p0, Lcom/android/packageinstaller/SilentInstallReceiver$1;->this$0:Lcom/android/packageinstaller/SilentInstallReceiver;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 8
    .param p1    # Landroid/os/Message;

    const-string v5, "ttttt"

    const-string v6, "SilentInstallReceiver : handleMessage()"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v5, p1, Landroid/os/Message;->what:I

    packed-switch v5, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    # getter for: Lcom/android/packageinstaller/SilentInstallReceiver;->appList:Ljava/util/List;
    invoke-static {}, Lcom/android/packageinstaller/SilentInstallReceiver;->access$0()Ljava/util/List;

    move-result-object v5

    if-eqz v5, :cond_0

    const/4 v3, 0x0

    :goto_1
    # getter for: Lcom/android/packageinstaller/SilentInstallReceiver;->appList:Ljava/util/List;
    invoke-static {}, Lcom/android/packageinstaller/SilentInstallReceiver;->access$0()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-lt v3, v5, :cond_3

    :cond_0
    :goto_2
    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    const-string v5, "com.konka.ACTION.SILENT_INSTALL_COMPLETE"

    invoke-virtual {v4, v5}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v5, "SILENT_INSTALL_RESULT"

    iget v6, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v5, "FILE_NAME"

    iget-object v6, p0, Lcom/android/packageinstaller/SilentInstallReceiver$1;->this$0:Lcom/android/packageinstaller/SilentInstallReceiver;

    # getter for: Lcom/android/packageinstaller/SilentInstallReceiver;->fileName:Ljava/lang/String;
    invoke-static {v6}, Lcom/android/packageinstaller/SilentInstallReceiver;->access$2(Lcom/android/packageinstaller/SilentInstallReceiver;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v5, "PACKAGE_NAME"

    iget-object v6, p0, Lcom/android/packageinstaller/SilentInstallReceiver$1;->this$0:Lcom/android/packageinstaller/SilentInstallReceiver;

    # getter for: Lcom/android/packageinstaller/SilentInstallReceiver;->mPkgInfo:Landroid/content/pm/PackageInfo;
    invoke-static {v6}, Lcom/android/packageinstaller/SilentInstallReceiver;->access$1(Lcom/android/packageinstaller/SilentInstallReceiver;)Landroid/content/pm/PackageInfo;

    move-result-object v6

    iget-object v6, v6, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget v5, p1, Landroid/os/Message;->arg1:I

    const/4 v6, 0x1

    if-ne v5, v6, :cond_5

    iget-object v5, p0, Lcom/android/packageinstaller/SilentInstallReceiver$1;->this$0:Lcom/android/packageinstaller/SilentInstallReceiver;

    # getter for: Lcom/android/packageinstaller/SilentInstallReceiver;->mIsToastShow:Ljava/lang/Boolean;
    invoke-static {v5}, Lcom/android/packageinstaller/SilentInstallReceiver;->access$3(Lcom/android/packageinstaller/SilentInstallReceiver;)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/android/packageinstaller/SilentInstallReceiver$1;->this$0:Lcom/android/packageinstaller/SilentInstallReceiver;

    # getter for: Lcom/android/packageinstaller/SilentInstallReceiver;->mToast:Landroid/widget/Toast;
    invoke-static {v5}, Lcom/android/packageinstaller/SilentInstallReceiver;->access$4(Lcom/android/packageinstaller/SilentInstallReceiver;)Landroid/widget/Toast;

    move-result-object v5

    const v6, 0x7f060007    # com.android.packageinstaller.R.string.install_done

    invoke-virtual {v5, v6}, Landroid/widget/Toast;->setText(I)V

    iget-object v5, p0, Lcom/android/packageinstaller/SilentInstallReceiver$1;->this$0:Lcom/android/packageinstaller/SilentInstallReceiver;

    # getter for: Lcom/android/packageinstaller/SilentInstallReceiver;->mToast:Landroid/widget/Toast;
    invoke-static {v5}, Lcom/android/packageinstaller/SilentInstallReceiver;->access$4(Lcom/android/packageinstaller/SilentInstallReceiver;)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    :cond_1
    :goto_3
    iget-object v5, p0, Lcom/android/packageinstaller/SilentInstallReceiver$1;->this$0:Lcom/android/packageinstaller/SilentInstallReceiver;

    # getter for: Lcom/android/packageinstaller/SilentInstallReceiver;->mRemoveFlag:Ljava/lang/Boolean;
    invoke-static {v5}, Lcom/android/packageinstaller/SilentInstallReceiver;->access$6(Lcom/android/packageinstaller/SilentInstallReceiver;)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_2

    new-instance v1, Ljava/io/File;

    iget-object v5, p0, Lcom/android/packageinstaller/SilentInstallReceiver$1;->this$0:Lcom/android/packageinstaller/SilentInstallReceiver;

    # getter for: Lcom/android/packageinstaller/SilentInstallReceiver;->fileName:Ljava/lang/String;
    invoke-static {v5}, Lcom/android/packageinstaller/SilentInstallReceiver;->access$2(Lcom/android/packageinstaller/SilentInstallReceiver;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v1, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_8

    invoke-virtual {v1}, Ljava/io/File;->isFile()Z

    move-result v5

    if-eqz v5, :cond_8

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    move-result v5

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-nez v5, :cond_2

    iget-object v5, p0, Lcom/android/packageinstaller/SilentInstallReceiver$1;->this$0:Lcom/android/packageinstaller/SilentInstallReceiver;

    # getter for: Lcom/android/packageinstaller/SilentInstallReceiver;->mIsToastShow:Ljava/lang/Boolean;
    invoke-static {v5}, Lcom/android/packageinstaller/SilentInstallReceiver;->access$3(Lcom/android/packageinstaller/SilentInstallReceiver;)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/android/packageinstaller/SilentInstallReceiver$1;->this$0:Lcom/android/packageinstaller/SilentInstallReceiver;

    # getter for: Lcom/android/packageinstaller/SilentInstallReceiver;->mToast:Landroid/widget/Toast;
    invoke-static {v5}, Lcom/android/packageinstaller/SilentInstallReceiver;->access$4(Lcom/android/packageinstaller/SilentInstallReceiver;)Landroid/widget/Toast;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    iget-object v7, p0, Lcom/android/packageinstaller/SilentInstallReceiver$1;->this$0:Lcom/android/packageinstaller/SilentInstallReceiver;

    # getter for: Lcom/android/packageinstaller/SilentInstallReceiver;->fileName:Ljava/lang/String;
    invoke-static {v7}, Lcom/android/packageinstaller/SilentInstallReceiver;->access$2(Lcom/android/packageinstaller/SilentInstallReceiver;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, " delete fail!!!"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    iget-object v5, p0, Lcom/android/packageinstaller/SilentInstallReceiver$1;->this$0:Lcom/android/packageinstaller/SilentInstallReceiver;

    # getter for: Lcom/android/packageinstaller/SilentInstallReceiver;->mToast:Landroid/widget/Toast;
    invoke-static {v5}, Lcom/android/packageinstaller/SilentInstallReceiver;->access$4(Lcom/android/packageinstaller/SilentInstallReceiver;)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    :cond_2
    :goto_4
    iget-object v5, p0, Lcom/android/packageinstaller/SilentInstallReceiver$1;->this$0:Lcom/android/packageinstaller/SilentInstallReceiver;

    # getter for: Lcom/android/packageinstaller/SilentInstallReceiver;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/android/packageinstaller/SilentInstallReceiver;->access$7(Lcom/android/packageinstaller/SilentInstallReceiver;)Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5, v4}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_3
    iget-object v5, p0, Lcom/android/packageinstaller/SilentInstallReceiver$1;->this$0:Lcom/android/packageinstaller/SilentInstallReceiver;

    # getter for: Lcom/android/packageinstaller/SilentInstallReceiver;->mPkgInfo:Landroid/content/pm/PackageInfo;
    invoke-static {v5}, Lcom/android/packageinstaller/SilentInstallReceiver;->access$1(Lcom/android/packageinstaller/SilentInstallReceiver;)Landroid/content/pm/PackageInfo;

    move-result-object v5

    iget-object v5, v5, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    if-eqz v5, :cond_4

    iget-object v5, p0, Lcom/android/packageinstaller/SilentInstallReceiver$1;->this$0:Lcom/android/packageinstaller/SilentInstallReceiver;

    # getter for: Lcom/android/packageinstaller/SilentInstallReceiver;->mPkgInfo:Landroid/content/pm/PackageInfo;
    invoke-static {v5}, Lcom/android/packageinstaller/SilentInstallReceiver;->access$1(Lcom/android/packageinstaller/SilentInstallReceiver;)Landroid/content/pm/PackageInfo;

    move-result-object v5

    iget-object v6, v5, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    # getter for: Lcom/android/packageinstaller/SilentInstallReceiver;->appList:Ljava/util/List;
    invoke-static {}, Lcom/android/packageinstaller/SilentInstallReceiver;->access$0()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/packageinstaller/SilentInstallReceiver$appInfo;

    iget-object v5, v5, Lcom/android/packageinstaller/SilentInstallReceiver$appInfo;->PackageName:Ljava/lang/String;

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    # getter for: Lcom/android/packageinstaller/SilentInstallReceiver;->appList:Ljava/util/List;
    invoke-static {}, Lcom/android/packageinstaller/SilentInstallReceiver;->access$0()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5, v3}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    goto/16 :goto_2

    :cond_4
    # getter for: Lcom/android/packageinstaller/SilentInstallReceiver;->appList:Ljava/util/List;
    invoke-static {}, Lcom/android/packageinstaller/SilentInstallReceiver;->access$0()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5, v3}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    goto/16 :goto_1

    :cond_5
    iget v5, p1, Landroid/os/Message;->arg1:I

    const/4 v6, -0x4

    if-ne v5, v6, :cond_6

    iget-object v5, p0, Lcom/android/packageinstaller/SilentInstallReceiver$1;->this$0:Lcom/android/packageinstaller/SilentInstallReceiver;

    # getter for: Lcom/android/packageinstaller/SilentInstallReceiver;->mIsToastShow:Ljava/lang/Boolean;
    invoke-static {v5}, Lcom/android/packageinstaller/SilentInstallReceiver;->access$3(Lcom/android/packageinstaller/SilentInstallReceiver;)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/android/packageinstaller/SilentInstallReceiver$1;->this$0:Lcom/android/packageinstaller/SilentInstallReceiver;

    # getter for: Lcom/android/packageinstaller/SilentInstallReceiver;->mToast:Landroid/widget/Toast;
    invoke-static {v5}, Lcom/android/packageinstaller/SilentInstallReceiver;->access$4(Lcom/android/packageinstaller/SilentInstallReceiver;)Landroid/widget/Toast;

    move-result-object v5

    const v6, 0x7f06003f    # com.android.packageinstaller.R.string.toast_out_of_space

    invoke-virtual {v5, v6}, Landroid/widget/Toast;->setText(I)V

    iget-object v5, p0, Lcom/android/packageinstaller/SilentInstallReceiver$1;->this$0:Lcom/android/packageinstaller/SilentInstallReceiver;

    # getter for: Lcom/android/packageinstaller/SilentInstallReceiver;->mToast:Landroid/widget/Toast;
    invoke-static {v5}, Lcom/android/packageinstaller/SilentInstallReceiver;->access$4(Lcom/android/packageinstaller/SilentInstallReceiver;)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    goto/16 :goto_3

    :cond_6
    iget-object v5, p0, Lcom/android/packageinstaller/SilentInstallReceiver$1;->this$0:Lcom/android/packageinstaller/SilentInstallReceiver;

    # getter for: Lcom/android/packageinstaller/SilentInstallReceiver;->mIsToastShow:Ljava/lang/Boolean;
    invoke-static {v5}, Lcom/android/packageinstaller/SilentInstallReceiver;->access$3(Lcom/android/packageinstaller/SilentInstallReceiver;)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/android/packageinstaller/SilentInstallReceiver$1;->this$0:Lcom/android/packageinstaller/SilentInstallReceiver;

    iget v6, p1, Landroid/os/Message;->arg1:I

    # invokes: Lcom/android/packageinstaller/SilentInstallReceiver;->getExplanationFromErrorCode(I)I
    invoke-static {v5, v6}, Lcom/android/packageinstaller/SilentInstallReceiver;->access$5(Lcom/android/packageinstaller/SilentInstallReceiver;I)I

    move-result v0

    const/4 v5, -0x1

    if-eq v0, v5, :cond_7

    iget-object v5, p0, Lcom/android/packageinstaller/SilentInstallReceiver$1;->this$0:Lcom/android/packageinstaller/SilentInstallReceiver;

    # getter for: Lcom/android/packageinstaller/SilentInstallReceiver;->mToast:Landroid/widget/Toast;
    invoke-static {v5}, Lcom/android/packageinstaller/SilentInstallReceiver;->access$4(Lcom/android/packageinstaller/SilentInstallReceiver;)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5, v0}, Landroid/widget/Toast;->setText(I)V

    iget-object v5, p0, Lcom/android/packageinstaller/SilentInstallReceiver$1;->this$0:Lcom/android/packageinstaller/SilentInstallReceiver;

    # getter for: Lcom/android/packageinstaller/SilentInstallReceiver;->mToast:Landroid/widget/Toast;
    invoke-static {v5}, Lcom/android/packageinstaller/SilentInstallReceiver;->access$4(Lcom/android/packageinstaller/SilentInstallReceiver;)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    goto/16 :goto_3

    :cond_7
    iget-object v5, p0, Lcom/android/packageinstaller/SilentInstallReceiver$1;->this$0:Lcom/android/packageinstaller/SilentInstallReceiver;

    # getter for: Lcom/android/packageinstaller/SilentInstallReceiver;->mToast:Landroid/widget/Toast;
    invoke-static {v5}, Lcom/android/packageinstaller/SilentInstallReceiver;->access$4(Lcom/android/packageinstaller/SilentInstallReceiver;)Landroid/widget/Toast;

    move-result-object v5

    const v6, 0x7f060009    # com.android.packageinstaller.R.string.install_failed

    invoke-virtual {v5, v6}, Landroid/widget/Toast;->setText(I)V

    iget-object v5, p0, Lcom/android/packageinstaller/SilentInstallReceiver$1;->this$0:Lcom/android/packageinstaller/SilentInstallReceiver;

    # getter for: Lcom/android/packageinstaller/SilentInstallReceiver;->mToast:Landroid/widget/Toast;
    invoke-static {v5}, Lcom/android/packageinstaller/SilentInstallReceiver;->access$4(Lcom/android/packageinstaller/SilentInstallReceiver;)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    goto/16 :goto_3

    :cond_8
    iget-object v5, p0, Lcom/android/packageinstaller/SilentInstallReceiver$1;->this$0:Lcom/android/packageinstaller/SilentInstallReceiver;

    # getter for: Lcom/android/packageinstaller/SilentInstallReceiver;->mIsToastShow:Ljava/lang/Boolean;
    invoke-static {v5}, Lcom/android/packageinstaller/SilentInstallReceiver;->access$3(Lcom/android/packageinstaller/SilentInstallReceiver;)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/android/packageinstaller/SilentInstallReceiver$1;->this$0:Lcom/android/packageinstaller/SilentInstallReceiver;

    # getter for: Lcom/android/packageinstaller/SilentInstallReceiver;->mToast:Landroid/widget/Toast;
    invoke-static {v5}, Lcom/android/packageinstaller/SilentInstallReceiver;->access$4(Lcom/android/packageinstaller/SilentInstallReceiver;)Landroid/widget/Toast;

    move-result-object v5

    const v6, 0x7f060040    # com.android.packageinstaller.R.string.toast_packageUI_error

    invoke-virtual {v5, v6}, Landroid/widget/Toast;->setText(I)V

    iget-object v5, p0, Lcom/android/packageinstaller/SilentInstallReceiver$1;->this$0:Lcom/android/packageinstaller/SilentInstallReceiver;

    # getter for: Lcom/android/packageinstaller/SilentInstallReceiver;->mToast:Landroid/widget/Toast;
    invoke-static {v5}, Lcom/android/packageinstaller/SilentInstallReceiver;->access$4(Lcom/android/packageinstaller/SilentInstallReceiver;)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    goto/16 :goto_4

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
