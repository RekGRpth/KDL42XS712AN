.class Lcom/android/packageinstaller/PackageInstallerActivity$14;
.super Ljava/lang/Object;
.source "PackageInstallerActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/packageinstaller/PackageInstallerActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/packageinstaller/PackageInstallerActivity;


# direct methods
.method constructor <init>(Lcom/android/packageinstaller/PackageInstallerActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/android/packageinstaller/PackageInstallerActivity$14;->this$0:Lcom/android/packageinstaller/PackageInstallerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1    # Landroid/view/View;

    const/4 v5, 0x2

    const/4 v4, 0x1

    iget-object v1, p0, Lcom/android/packageinstaller/PackageInstallerActivity$14;->this$0:Lcom/android/packageinstaller/PackageInstallerActivity;

    invoke-virtual {v1}, Lcom/android/packageinstaller/PackageInstallerActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "install_app_location_type"

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-ne v4, v0, :cond_2

    iget-object v1, p0, Lcom/android/packageinstaller/PackageInstallerActivity$14;->this$0:Lcom/android/packageinstaller/PackageInstallerActivity;

    invoke-virtual {v1}, Lcom/android/packageinstaller/PackageInstallerActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "install_app_location_type"

    invoke-static {v1, v2, v5}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/android/packageinstaller/PackageInstallerActivity$14;->this$0:Lcom/android/packageinstaller/PackageInstallerActivity;

    # getter for: Lcom/android/packageinstaller/PackageInstallerActivity;->mOk:Landroid/widget/Button;
    invoke-static {v1}, Lcom/android/packageinstaller/PackageInstallerActivity;->access$4(Lcom/android/packageinstaller/PackageInstallerActivity;)Landroid/widget/Button;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/packageinstaller/PackageInstallerActivity$14;->this$0:Lcom/android/packageinstaller/PackageInstallerActivity;

    # getter for: Lcom/android/packageinstaller/PackageInstallerActivity;->mOk:Landroid/widget/Button;
    invoke-static {v1}, Lcom/android/packageinstaller/PackageInstallerActivity;->access$4(Lcom/android/packageinstaller/PackageInstallerActivity;)Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Button;->isClickable()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/android/packageinstaller/PackageInstallerActivity$14;->this$0:Lcom/android/packageinstaller/PackageInstallerActivity;

    # getter for: Lcom/android/packageinstaller/PackageInstallerActivity;->mOk:Landroid/widget/Button;
    invoke-static {v1}, Lcom/android/packageinstaller/PackageInstallerActivity;->access$4(Lcom/android/packageinstaller/PackageInstallerActivity;)Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setClickable(Z)V

    iget-object v1, p0, Lcom/android/packageinstaller/PackageInstallerActivity$14;->this$0:Lcom/android/packageinstaller/PackageInstallerActivity;

    # getter for: Lcom/android/packageinstaller/PackageInstallerActivity;->mOk:Landroid/widget/Button;
    invoke-static {v1}, Lcom/android/packageinstaller/PackageInstallerActivity;->access$4(Lcom/android/packageinstaller/PackageInstallerActivity;)Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v1, p0, Lcom/android/packageinstaller/PackageInstallerActivity$14;->this$0:Lcom/android/packageinstaller/PackageInstallerActivity;

    # getter for: Lcom/android/packageinstaller/PackageInstallerActivity;->mOk:Landroid/widget/Button;
    invoke-static {v1}, Lcom/android/packageinstaller/PackageInstallerActivity;->access$4(Lcom/android/packageinstaller/PackageInstallerActivity;)Landroid/widget/Button;

    move-result-object v1

    const v2, 0x7f02000d    # com.android.packageinstaller.R.drawable.install_button

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setBackgroundResource(I)V

    iget-object v1, p0, Lcom/android/packageinstaller/PackageInstallerActivity$14;->this$0:Lcom/android/packageinstaller/PackageInstallerActivity;

    # getter for: Lcom/android/packageinstaller/PackageInstallerActivity;->mOk:Landroid/widget/Button;
    invoke-static {v1}, Lcom/android/packageinstaller/PackageInstallerActivity;->access$4(Lcom/android/packageinstaller/PackageInstallerActivity;)Landroid/widget/Button;

    move-result-object v1

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setTextColor(I)V

    iget-object v1, p0, Lcom/android/packageinstaller/PackageInstallerActivity$14;->this$0:Lcom/android/packageinstaller/PackageInstallerActivity;

    # getter for: Lcom/android/packageinstaller/PackageInstallerActivity;->mOk:Landroid/widget/Button;
    invoke-static {v1}, Lcom/android/packageinstaller/PackageInstallerActivity;->access$4(Lcom/android/packageinstaller/PackageInstallerActivity;)Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Button;->requestFocus()Z

    :cond_1
    iget-object v1, p0, Lcom/android/packageinstaller/PackageInstallerActivity$14;->this$0:Lcom/android/packageinstaller/PackageInstallerActivity;

    # invokes: Lcom/android/packageinstaller/PackageInstallerActivity;->initInstallLocation()V
    invoke-static {v1}, Lcom/android/packageinstaller/PackageInstallerActivity;->access$1(Lcom/android/packageinstaller/PackageInstallerActivity;)V

    iget-object v1, p0, Lcom/android/packageinstaller/PackageInstallerActivity$14;->this$0:Lcom/android/packageinstaller/PackageInstallerActivity;

    # invokes: Lcom/android/packageinstaller/PackageInstallerActivity;->initiateInstall()V
    invoke-static {v1}, Lcom/android/packageinstaller/PackageInstallerActivity;->access$5(Lcom/android/packageinstaller/PackageInstallerActivity;)V

    return-void

    :cond_2
    if-ne v5, v0, :cond_0

    iget-object v1, p0, Lcom/android/packageinstaller/PackageInstallerActivity$14;->this$0:Lcom/android/packageinstaller/PackageInstallerActivity;

    invoke-virtual {v1}, Lcom/android/packageinstaller/PackageInstallerActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "install_app_location_type"

    invoke-static {v1, v2, v4}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_0
.end method
