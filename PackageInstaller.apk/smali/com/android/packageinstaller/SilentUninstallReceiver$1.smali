.class Lcom/android/packageinstaller/SilentUninstallReceiver$1;
.super Landroid/os/Handler;
.source "SilentUninstallReceiver.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/packageinstaller/SilentUninstallReceiver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/packageinstaller/SilentUninstallReceiver;


# direct methods
.method constructor <init>(Lcom/android/packageinstaller/SilentUninstallReceiver;)V
    .locals 0

    iput-object p1, p0, Lcom/android/packageinstaller/SilentUninstallReceiver$1;->this$0:Lcom/android/packageinstaller/SilentUninstallReceiver;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1    # Landroid/os/Message;

    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v2, p0, Lcom/android/packageinstaller/SilentUninstallReceiver$1;->this$0:Lcom/android/packageinstaller/SilentUninstallReceiver;

    iget v3, p1, Landroid/os/Message;->arg1:I

    invoke-static {v2, v3}, Lcom/android/packageinstaller/SilentUninstallReceiver;->access$0(Lcom/android/packageinstaller/SilentUninstallReceiver;I)V

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    iget v2, p1, Landroid/os/Message;->arg1:I

    packed-switch v2, :pswitch_data_1

    :pswitch_1
    const-string v2, "SilentUninstallReceiver"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Uninstall failed for "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " with code "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const v1, 0x7f060029    # com.android.packageinstaller.R.string.uninstall_failed

    :goto_1
    iget-object v2, p0, Lcom/android/packageinstaller/SilentUninstallReceiver$1;->this$0:Lcom/android/packageinstaller/SilentUninstallReceiver;

    # getter for: Lcom/android/packageinstaller/SilentUninstallReceiver;->mIsToastShow:Ljava/lang/Boolean;
    invoke-static {v2}, Lcom/android/packageinstaller/SilentUninstallReceiver;->access$1(Lcom/android/packageinstaller/SilentUninstallReceiver;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/packageinstaller/SilentUninstallReceiver$1;->this$0:Lcom/android/packageinstaller/SilentUninstallReceiver;

    # getter for: Lcom/android/packageinstaller/SilentUninstallReceiver;->mToast:Landroid/widget/Toast;
    invoke-static {v2}, Lcom/android/packageinstaller/SilentUninstallReceiver;->access$2(Lcom/android/packageinstaller/SilentUninstallReceiver;)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/widget/Toast;->setText(I)V

    iget-object v2, p0, Lcom/android/packageinstaller/SilentUninstallReceiver$1;->this$0:Lcom/android/packageinstaller/SilentUninstallReceiver;

    # getter for: Lcom/android/packageinstaller/SilentUninstallReceiver;->mToast:Landroid/widget/Toast;
    invoke-static {v2}, Lcom/android/packageinstaller/SilentUninstallReceiver;->access$2(Lcom/android/packageinstaller/SilentUninstallReceiver;)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :pswitch_2
    const v1, 0x7f060028    # com.android.packageinstaller.R.string.uninstall_done

    goto :goto_1

    :pswitch_3
    const-string v2, "SilentUninstallReceiver"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Uninstall failed because "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " is a device admin"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const v1, 0x7f06002a    # com.android.packageinstaller.R.string.uninstall_failed_device_policy_manager

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch -0x2
        :pswitch_3
        :pswitch_1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
