.class public Lcom/android/packageinstaller/SilentInstallReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SilentInstallReceiver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/packageinstaller/SilentInstallReceiver$PackageInstallObserver;,
        Lcom/android/packageinstaller/SilentInstallReceiver$appInfo;
    }
.end annotation


# static fields
.field private static appList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/packageinstaller/SilentInstallReceiver$appInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final INSTALL_COMPLETE:I

.field private fileName:Ljava/lang/String;

.field private mAppInfo:Landroid/content/pm/ApplicationInfo;

.field private mAppInstallLocation:I

.field private mContext:Landroid/content/Context;

.field private mHandler:Landroid/os/Handler;

.field private mIsSystemReceived:Ljava/lang/Boolean;

.field private mIsToastShow:Ljava/lang/Boolean;

.field private mPackageURI:Landroid/net/Uri;

.field private mPkgInfo:Landroid/content/pm/PackageInfo;

.field private mRemoveFlag:Ljava/lang/Boolean;

.field private mToast:Landroid/widget/Toast;

.field private pm:Landroid/content/pm/PackageManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/packageinstaller/SilentInstallReceiver;->appList:Ljava/util/List;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    iput v2, p0, Lcom/android/packageinstaller/SilentInstallReceiver;->INSTALL_COMPLETE:I

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/android/packageinstaller/SilentInstallReceiver;->mRemoveFlag:Ljava/lang/Boolean;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/packageinstaller/SilentInstallReceiver;->fileName:Ljava/lang/String;

    iput v1, p0, Lcom/android/packageinstaller/SilentInstallReceiver;->mAppInstallLocation:I

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/android/packageinstaller/SilentInstallReceiver;->mIsToastShow:Ljava/lang/Boolean;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/android/packageinstaller/SilentInstallReceiver;->mIsSystemReceived:Ljava/lang/Boolean;

    new-instance v0, Lcom/android/packageinstaller/SilentInstallReceiver$1;

    invoke-direct {v0, p0}, Lcom/android/packageinstaller/SilentInstallReceiver$1;-><init>(Lcom/android/packageinstaller/SilentInstallReceiver;)V

    iput-object v0, p0, Lcom/android/packageinstaller/SilentInstallReceiver;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$0()Ljava/util/List;
    .locals 1

    sget-object v0, Lcom/android/packageinstaller/SilentInstallReceiver;->appList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$1(Lcom/android/packageinstaller/SilentInstallReceiver;)Landroid/content/pm/PackageInfo;
    .locals 1

    iget-object v0, p0, Lcom/android/packageinstaller/SilentInstallReceiver;->mPkgInfo:Landroid/content/pm/PackageInfo;

    return-object v0
.end method

.method static synthetic access$2(Lcom/android/packageinstaller/SilentInstallReceiver;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/packageinstaller/SilentInstallReceiver;->fileName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3(Lcom/android/packageinstaller/SilentInstallReceiver;)Ljava/lang/Boolean;
    .locals 1

    iget-object v0, p0, Lcom/android/packageinstaller/SilentInstallReceiver;->mIsToastShow:Ljava/lang/Boolean;

    return-object v0
.end method

.method static synthetic access$4(Lcom/android/packageinstaller/SilentInstallReceiver;)Landroid/widget/Toast;
    .locals 1

    iget-object v0, p0, Lcom/android/packageinstaller/SilentInstallReceiver;->mToast:Landroid/widget/Toast;

    return-object v0
.end method

.method static synthetic access$5(Lcom/android/packageinstaller/SilentInstallReceiver;I)I
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/packageinstaller/SilentInstallReceiver;->getExplanationFromErrorCode(I)I

    move-result v0

    return v0
.end method

.method static synthetic access$6(Lcom/android/packageinstaller/SilentInstallReceiver;)Ljava/lang/Boolean;
    .locals 1

    iget-object v0, p0, Lcom/android/packageinstaller/SilentInstallReceiver;->mRemoveFlag:Ljava/lang/Boolean;

    return-object v0
.end method

.method static synthetic access$7(Lcom/android/packageinstaller/SilentInstallReceiver;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/android/packageinstaller/SilentInstallReceiver;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$8(Lcom/android/packageinstaller/SilentInstallReceiver;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/android/packageinstaller/SilentInstallReceiver;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private getExplanationFromErrorCode(I)I
    .locals 1
    .param p1    # I

    sparse-switch p1, :sswitch_data_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :sswitch_0
    const v0, 0x7f06000a    # com.android.packageinstaller.R.string.install_failed_invalid_apk

    goto :goto_0

    :sswitch_1
    const v0, 0x7f06000b    # com.android.packageinstaller.R.string.install_failed_inconsistent_certificates

    goto :goto_0

    :sswitch_2
    const v0, 0x7f06000c    # com.android.packageinstaller.R.string.install_failed_older_sdk

    goto :goto_0

    :sswitch_3
    const v0, 0x7f06000d    # com.android.packageinstaller.R.string.install_failed_cpu_abi_incompatible

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x68 -> :sswitch_1
        -0x10 -> :sswitch_3
        -0xc -> :sswitch_2
        -0x2 -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 27
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const-string v4, ""

    const/4 v8, 0x1

    move-object/from16 v0, p1

    invoke-static {v0, v4, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/packageinstaller/SilentInstallReceiver;->mToast:Landroid/widget/Toast;

    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/packageinstaller/SilentInstallReceiver;->mContext:Landroid/content/Context;

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    const-string v8, "com.konka.ACTION.SILENT_INSTALL"

    invoke-virtual {v4, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    const-string v4, "FILE_NAME"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/packageinstaller/SilentInstallReceiver;->fileName:Ljava/lang/String;

    const-string v4, "REMOVE_APK_AFTER_INSTALL"

    const/4 v8, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v4, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/packageinstaller/SilentInstallReceiver;->mRemoveFlag:Ljava/lang/Boolean;

    new-instance v4, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/packageinstaller/SilentInstallReceiver;->fileName:Ljava/lang/String;

    invoke-direct {v4, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v4}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/packageinstaller/SilentInstallReceiver;->mPackageURI:Landroid/net/Uri;

    const-string v4, "INSTALL_LOCATION"

    const/4 v8, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v4, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    move-object/from16 v0, p0

    iput v4, v0, Lcom/android/packageinstaller/SilentInstallReceiver;->mAppInstallLocation:I

    const-string v4, "NEED_HINT_INSTALL_RESULT"

    const/4 v8, 0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v4, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/packageinstaller/SilentInstallReceiver;->mIsToastShow:Ljava/lang/Boolean;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/packageinstaller/SilentInstallReceiver;->mPackageURI:Landroid/net/Uri;

    if-nez v4, :cond_1

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/packageinstaller/SilentInstallReceiver;->mIsToastShow:Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_1

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/packageinstaller/SilentInstallReceiver;->mToast:Landroid/widget/Toast;

    const v8, 0x7f060040    # com.android.packageinstaller.R.string.toast_packageUI_error

    invoke-virtual {v4, v8}, Landroid/widget/Toast;->setText(I)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/packageinstaller/SilentInstallReceiver;->mToast:Landroid/widget/Toast;

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v24

    const-string v4, "package"

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/packageinstaller/SilentInstallReceiver;->mPackageURI:Landroid/net/Uri;

    invoke-virtual {v8}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    :try_start_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/packageinstaller/SilentInstallReceiver;->mPackageURI:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v4

    const/16 v8, 0x3000

    move-object/from16 v0, v24

    invoke-virtual {v0, v4, v8}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/packageinstaller/SilentInstallReceiver;->mPkgInfo:Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_2

    :goto_1
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v24

    const-string v4, "android.intent.extra.INSTALLER_PACKAGE_NAME"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/packageinstaller/SilentInstallReceiver;->mPkgInfo:Landroid/content/pm/PackageInfo;

    if-nez v4, :cond_3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/packageinstaller/SilentInstallReceiver;->mIsToastShow:Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/packageinstaller/SilentInstallReceiver;->mToast:Landroid/widget/Toast;

    const v8, 0x7f06002e    # com.android.packageinstaller.R.string.Parse_error_dlg_text

    invoke-virtual {v4, v8}, Landroid/widget/Toast;->setText(I)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/packageinstaller/SilentInstallReceiver;->mToast:Landroid/widget/Toast;

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_2
    new-instance v25, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/packageinstaller/SilentInstallReceiver;->mPackageURI:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v25

    invoke-direct {v0, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static/range {v25 .. v25}, Lcom/android/packageinstaller/PackageUtil;->getPackageInfo(Ljava/io/File;)Landroid/content/pm/PackageParser$Package;

    move-result-object v2

    const/4 v3, 0x0

    const/16 v4, 0x1000

    const-wide/16 v5, 0x0

    const-wide/16 v7, 0x0

    const/4 v9, 0x0

    new-instance v10, Landroid/content/pm/PackageUserState;

    invoke-direct {v10}, Landroid/content/pm/PackageUserState;-><init>()V

    invoke-static/range {v2 .. v10}, Landroid/content/pm/PackageParser;->generatePackageInfo(Landroid/content/pm/PackageParser$Package;[IIJJLjava/util/HashSet;Landroid/content/pm/PackageUserState;)Landroid/content/pm/PackageInfo;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/packageinstaller/SilentInstallReceiver;->mPkgInfo:Landroid/content/pm/PackageInfo;

    goto :goto_1

    :cond_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/packageinstaller/SilentInstallReceiver;->mPkgInfo:Landroid/content/pm/PackageInfo;

    iget-object v4, v4, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/packageinstaller/SilentInstallReceiver;->mAppInfo:Landroid/content/pm/ApplicationInfo;

    const/4 v11, 0x0

    new-instance v21, Lcom/android/packageinstaller/SilentInstallReceiver$appInfo;

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/packageinstaller/SilentInstallReceiver$appInfo;-><init>(Lcom/android/packageinstaller/SilentInstallReceiver;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/packageinstaller/SilentInstallReceiver;->fileName:Ljava/lang/String;

    move-object/from16 v0, v21

    iput-object v4, v0, Lcom/android/packageinstaller/SilentInstallReceiver$appInfo;->fileName:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/packageinstaller/SilentInstallReceiver;->mPkgInfo:Landroid/content/pm/PackageInfo;

    iget-object v4, v4, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    move-object/from16 v0, v21

    iput-object v4, v0, Lcom/android/packageinstaller/SilentInstallReceiver$appInfo;->PackageName:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/packageinstaller/SilentInstallReceiver;->mPkgInfo:Landroid/content/pm/PackageInfo;

    iget-object v4, v4, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/packageinstaller/SilentInstallReceiver;->mAppInfo:Landroid/content/pm/ApplicationInfo;

    sget-object v4, Lcom/android/packageinstaller/SilentInstallReceiver;->appList:Ljava/util/List;

    move-object/from16 v0, v21

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :try_start_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/packageinstaller/SilentInstallReceiver;->mAppInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v4, v4, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    const/16 v8, 0x2000

    move-object/from16 v0, v24

    invoke-virtual {v0, v4, v8}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v23

    if-eqz v23, :cond_4

    or-int/lit8 v11, v11, 0x2

    :cond_4
    :goto_2
    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/packageinstaller/SilentInstallReceiver;->mAppInstallLocation:I

    if-nez v4, :cond_7

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v8, "install_app_location_type"

    const/4 v9, 0x0

    invoke-static {v4, v8, v9}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v26

    :goto_3
    const/4 v4, 0x1

    move/from16 v0, v26

    if-ne v0, v4, :cond_5

    or-int/lit8 v11, v11, 0x10

    :cond_5
    const/4 v4, 0x2

    move/from16 v0, v26

    if-ne v0, v4, :cond_6

    or-int/lit8 v11, v11, 0x8

    :cond_6
    const-string v4, "android.intent.extra.ORIGINATING_URI"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v5

    check-cast v5, Landroid/net/Uri;

    const-string v4, "android.intent.extra.REFERRER"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v6

    check-cast v6, Landroid/net/Uri;

    const-string v4, "android.intent.extra.ORIGINATING_UID"

    const/4 v8, -0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v4, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v7

    new-instance v3, Landroid/content/pm/VerificationParams;

    const/4 v4, 0x0

    const/4 v8, 0x0

    invoke-direct/range {v3 .. v8}, Landroid/content/pm/VerificationParams;-><init>(Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;ILandroid/content/pm/ManifestDigest;)V

    new-instance v10, Lcom/android/packageinstaller/SilentInstallReceiver$PackageInstallObserver;

    move-object/from16 v0, p0

    invoke-direct {v10, v0}, Lcom/android/packageinstaller/SilentInstallReceiver$PackageInstallObserver;-><init>(Lcom/android/packageinstaller/SilentInstallReceiver;)V

    const-string v4, "package"

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/packageinstaller/SilentInstallReceiver;->mPackageURI:Landroid/net/Uri;

    invoke-virtual {v8}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    :try_start_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/packageinstaller/SilentInstallReceiver;->mAppInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v4, v4, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    move-object/from16 v0, v24

    invoke-virtual {v0, v4}, Landroid/content/pm/PackageManager;->installExistingPackage(Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/packageinstaller/SilentInstallReceiver;->mAppInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v4, v4, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    const/4 v8, 0x1

    invoke-virtual {v10, v4, v8}, Lcom/android/packageinstaller/SilentInstallReceiver$PackageInstallObserver;->packageInstalled(Ljava/lang/String;I)V
    :try_end_2
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v15

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/packageinstaller/SilentInstallReceiver;->mAppInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v4, v4, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    const/4 v8, -0x2

    invoke-virtual {v10, v4, v8}, Lcom/android/packageinstaller/SilentInstallReceiver$PackageInstallObserver;->packageInstalled(Ljava/lang/String;I)V

    goto/16 :goto_0

    :cond_7
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/packageinstaller/SilentInstallReceiver;->mAppInstallLocation:I

    move/from16 v26, v0

    goto :goto_3

    :cond_8
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/packageinstaller/SilentInstallReceiver;->mPackageURI:Landroid/net/Uri;

    const/4 v14, 0x0

    move-object/from16 v8, v24

    move-object v13, v3

    invoke-virtual/range {v8 .. v14}, Landroid/content/pm/PackageManager;->installPackageWithVerificationAndEncryption(Landroid/net/Uri;Landroid/content/pm/IPackageInstallObserver;ILjava/lang/String;Landroid/content/pm/VerificationParams;Landroid/content/pm/ContainerEncryptionParams;)V

    goto/16 :goto_0

    :cond_9
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    const-string v8, "com.konka.ACTION.INSTALL_SUCCEED"

    invoke-virtual {v4, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "PACKAGE_NAME"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    const/16 v16, 0x0

    sget-object v4, Lcom/android/packageinstaller/SilentInstallReceiver;->appList:Ljava/util/List;

    if-eqz v4, :cond_a

    const/16 v19, 0x0

    :goto_4
    sget-object v4, Lcom/android/packageinstaller/SilentInstallReceiver;->appList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    move/from16 v0, v19

    if-lt v0, v4, :cond_c

    :cond_a
    :goto_5
    new-instance v20, Landroid/content/Intent;

    invoke-direct/range {v20 .. v20}, Landroid/content/Intent;-><init>()V

    const-string v4, "com.konka.ACTION.SILENT_INSTALL_COMPLETE"

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v4, "SILENT_INSTALL_RESULT"

    const/4 v8, 0x1

    move-object/from16 v0, v20

    invoke-virtual {v0, v4, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v4, "FILE_NAME"

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    invoke-virtual {v0, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v4, "PACKAGE_NAME"

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-virtual {v0, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/packageinstaller/SilentInstallReceiver;->mRemoveFlag:Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_b

    new-instance v17, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/packageinstaller/SilentInstallReceiver;->fileName:Ljava/lang/String;

    move-object/from16 v0, v17

    invoke-direct {v0, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_e

    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->isFile()Z

    move-result v4

    if-eqz v4, :cond_e

    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->delete()Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-nez v4, :cond_b

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/packageinstaller/SilentInstallReceiver;->mIsToastShow:Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_b

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/packageinstaller/SilentInstallReceiver;->mToast:Landroid/widget/Toast;

    new-instance v8, Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/packageinstaller/SilentInstallReceiver;->fileName:Ljava/lang/String;

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v9, " delete fail!!!"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/packageinstaller/SilentInstallReceiver;->mToast:Landroid/widget/Toast;

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    :cond_b
    :goto_6
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/packageinstaller/SilentInstallReceiver;->mContext:Landroid/content/Context;

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_c
    if-eqz v22, :cond_d

    sget-object v4, Lcom/android/packageinstaller/SilentInstallReceiver;->appList:Ljava/util/List;

    move/from16 v0, v19

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/packageinstaller/SilentInstallReceiver$appInfo;

    iget-object v4, v4, Lcom/android/packageinstaller/SilentInstallReceiver$appInfo;->PackageName:Ljava/lang/String;

    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_d

    sget-object v4, Lcom/android/packageinstaller/SilentInstallReceiver;->appList:Ljava/util/List;

    move/from16 v0, v19

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/packageinstaller/SilentInstallReceiver$appInfo;

    iget-object v0, v4, Lcom/android/packageinstaller/SilentInstallReceiver$appInfo;->fileName:Ljava/lang/String;

    move-object/from16 v16, v0

    sget-object v4, Lcom/android/packageinstaller/SilentInstallReceiver;->appList:Ljava/util/List;

    move/from16 v0, v19

    invoke-interface {v4, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    goto/16 :goto_5

    :cond_d
    sget-object v4, Lcom/android/packageinstaller/SilentInstallReceiver;->appList:Ljava/util/List;

    move/from16 v0, v19

    invoke-interface {v4, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    goto/16 :goto_4

    :cond_e
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/packageinstaller/SilentInstallReceiver;->mIsToastShow:Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_b

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/packageinstaller/SilentInstallReceiver;->mToast:Landroid/widget/Toast;

    const v8, 0x7f060040    # com.android.packageinstaller.R.string.toast_packageUI_error

    invoke-virtual {v4, v8}, Landroid/widget/Toast;->setText(I)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/packageinstaller/SilentInstallReceiver;->mToast:Landroid/widget/Toast;

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    goto :goto_6

    :catch_1
    move-exception v4

    goto/16 :goto_2

    :catch_2
    move-exception v4

    goto/16 :goto_1
.end method
