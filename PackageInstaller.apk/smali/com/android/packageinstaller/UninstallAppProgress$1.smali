.class Lcom/android/packageinstaller/UninstallAppProgress$1;
.super Landroid/os/Handler;
.source "UninstallAppProgress.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/packageinstaller/UninstallAppProgress;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/packageinstaller/UninstallAppProgress;


# direct methods
.method constructor <init>(Lcom/android/packageinstaller/UninstallAppProgress;)V
    .locals 0

    iput-object p1, p0, Lcom/android/packageinstaller/UninstallAppProgress$1;->this$0:Lcom/android/packageinstaller/UninstallAppProgress;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 10
    .param p1    # Landroid/os/Message;

    const v9, 0x7f08003b    # com.android.packageinstaller.R.id.uninstall_title_text_three

    const/16 v8, 0x8

    const/4 v4, 0x1

    const/4 v7, 0x0

    iget v3, p1, Landroid/os/Message;->what:I

    packed-switch v3, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v3, p0, Lcom/android/packageinstaller/UninstallAppProgress$1;->this$0:Lcom/android/packageinstaller/UninstallAppProgress;

    # getter for: Lcom/android/packageinstaller/UninstallAppProgress;->mUninstallSdcardHint:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/android/packageinstaller/UninstallAppProgress;->access$0(Lcom/android/packageinstaller/UninstallAppProgress;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v3, p0, Lcom/android/packageinstaller/UninstallAppProgress$1;->this$0:Lcom/android/packageinstaller/UninstallAppProgress;

    invoke-virtual {v3, v9}, Lcom/android/packageinstaller/UninstallAppProgress;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iget-object v5, p0, Lcom/android/packageinstaller/UninstallAppProgress$1;->this$0:Lcom/android/packageinstaller/UninstallAppProgress;

    invoke-virtual {v5}, Lcom/android/packageinstaller/UninstallAppProgress;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f040006    # com.android.packageinstaller.R.color.install_title_color_s

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v3, p0, Lcom/android/packageinstaller/UninstallAppProgress$1;->this$0:Lcom/android/packageinstaller/UninstallAppProgress;

    invoke-virtual {v3, v9}, Lcom/android/packageinstaller/UninstallAppProgress;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iget-object v5, p0, Lcom/android/packageinstaller/UninstallAppProgress$1;->this$0:Lcom/android/packageinstaller/UninstallAppProgress;

    invoke-virtual {v5}, Lcom/android/packageinstaller/UninstallAppProgress;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f050012    # com.android.packageinstaller.R.dimen.install_title_size_s

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    iget-object v6, p0, Lcom/android/packageinstaller/UninstallAppProgress$1;->this$0:Lcom/android/packageinstaller/UninstallAppProgress;

    # getter for: Lcom/android/packageinstaller/UninstallAppProgress;->mEnvironment:Lcom/android/packageinstaller/Environment;
    invoke-static {v6}, Lcom/android/packageinstaller/UninstallAppProgress;->access$1(Lcom/android/packageinstaller/UninstallAppProgress;)Lcom/android/packageinstaller/Environment;

    move-result-object v6

    invoke-virtual {v6}, Lcom/android/packageinstaller/Environment;->getDestiny()F

    move-result v6

    div-float/2addr v5, v6

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setTextSize(F)V

    iget-object v3, p0, Lcom/android/packageinstaller/UninstallAppProgress$1;->this$0:Lcom/android/packageinstaller/UninstallAppProgress;

    const v5, 0x7f08003a    # com.android.packageinstaller.R.id.uninstall_title_text_two

    invoke-virtual {v3, v5}, Lcom/android/packageinstaller/UninstallAppProgress;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iget-object v5, p0, Lcom/android/packageinstaller/UninstallAppProgress$1;->this$0:Lcom/android/packageinstaller/UninstallAppProgress;

    invoke-virtual {v5}, Lcom/android/packageinstaller/UninstallAppProgress;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f050013    # com.android.packageinstaller.R.dimen.install_title_size_uns

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    iget-object v6, p0, Lcom/android/packageinstaller/UninstallAppProgress$1;->this$0:Lcom/android/packageinstaller/UninstallAppProgress;

    # getter for: Lcom/android/packageinstaller/UninstallAppProgress;->mEnvironment:Lcom/android/packageinstaller/Environment;
    invoke-static {v6}, Lcom/android/packageinstaller/UninstallAppProgress;->access$1(Lcom/android/packageinstaller/UninstallAppProgress;)Lcom/android/packageinstaller/Environment;

    move-result-object v6

    invoke-virtual {v6}, Lcom/android/packageinstaller/Environment;->getDestiny()F

    move-result v6

    div-float/2addr v5, v6

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setTextSize(F)V

    iget-object v3, p0, Lcom/android/packageinstaller/UninstallAppProgress$1;->this$0:Lcom/android/packageinstaller/UninstallAppProgress;

    invoke-virtual {v3}, Lcom/android/packageinstaller/UninstallAppProgress;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v5, "android.intent.extra.RETURN_RESULT"

    invoke-virtual {v3, v5, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_1

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v3, "android.intent.extra.INSTALL_RESULT"

    iget v5, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v1, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v5, p0, Lcom/android/packageinstaller/UninstallAppProgress$1;->this$0:Lcom/android/packageinstaller/UninstallAppProgress;

    iget v3, p1, Landroid/os/Message;->arg1:I

    if-ne v3, v4, :cond_0

    const/4 v3, -0x1

    :goto_1
    invoke-virtual {v5, v3, v1}, Lcom/android/packageinstaller/UninstallAppProgress;->setResult(ILandroid/content/Intent;)V

    iget-object v3, p0, Lcom/android/packageinstaller/UninstallAppProgress$1;->this$0:Lcom/android/packageinstaller/UninstallAppProgress;

    invoke-virtual {v3}, Lcom/android/packageinstaller/UninstallAppProgress;->finish()V

    goto/16 :goto_0

    :cond_0
    move v3, v4

    goto :goto_1

    :cond_1
    iget-object v3, p0, Lcom/android/packageinstaller/UninstallAppProgress$1;->this$0:Lcom/android/packageinstaller/UninstallAppProgress;

    iget v4, p1, Landroid/os/Message;->arg1:I

    invoke-static {v3, v4}, Lcom/android/packageinstaller/UninstallAppProgress;->access$2(Lcom/android/packageinstaller/UninstallAppProgress;I)V

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    iget v3, p1, Landroid/os/Message;->arg1:I

    packed-switch v3, :pswitch_data_1

    :pswitch_1
    const-string v3, "UninstallAppProgress"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Uninstall failed for "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " with code "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const v2, 0x7f060029    # com.android.packageinstaller.R.string.uninstall_failed

    :goto_2
    iget-object v3, p0, Lcom/android/packageinstaller/UninstallAppProgress$1;->this$0:Lcom/android/packageinstaller/UninstallAppProgress;

    # getter for: Lcom/android/packageinstaller/UninstallAppProgress;->mStatusTextView:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/android/packageinstaller/UninstallAppProgress;->access$3(Lcom/android/packageinstaller/UninstallAppProgress;)Landroid/widget/TextView;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/android/packageinstaller/UninstallAppProgress$1;->this$0:Lcom/android/packageinstaller/UninstallAppProgress;

    # getter for: Lcom/android/packageinstaller/UninstallAppProgress;->mLabel:Ljava/lang/CharSequence;
    invoke-static {v5}, Lcom/android/packageinstaller/UninstallAppProgress;->access$5(Lcom/android/packageinstaller/UninstallAppProgress;)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/packageinstaller/UninstallAppProgress$1;->this$0:Lcom/android/packageinstaller/UninstallAppProgress;

    invoke-virtual {v5}, Lcom/android/packageinstaller/UninstallAppProgress;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/android/packageinstaller/UninstallAppProgress$1;->this$0:Lcom/android/packageinstaller/UninstallAppProgress;

    # getter for: Lcom/android/packageinstaller/UninstallAppProgress;->mProgressBar:Landroid/widget/ProgressBar;
    invoke-static {v3}, Lcom/android/packageinstaller/UninstallAppProgress;->access$6(Lcom/android/packageinstaller/UninstallAppProgress;)Landroid/widget/ProgressBar;

    move-result-object v3

    invoke-virtual {v3, v8}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v3, p0, Lcom/android/packageinstaller/UninstallAppProgress$1;->this$0:Lcom/android/packageinstaller/UninstallAppProgress;

    # getter for: Lcom/android/packageinstaller/UninstallAppProgress;->mOkPanel:Landroid/view/View;
    invoke-static {v3}, Lcom/android/packageinstaller/UninstallAppProgress;->access$7(Lcom/android/packageinstaller/UninstallAppProgress;)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    iget-object v3, p0, Lcom/android/packageinstaller/UninstallAppProgress$1;->this$0:Lcom/android/packageinstaller/UninstallAppProgress;

    # getter for: Lcom/android/packageinstaller/UninstallAppProgress;->mOkPanel:Landroid/view/View;
    invoke-static {v3}, Lcom/android/packageinstaller/UninstallAppProgress;->access$7(Lcom/android/packageinstaller/UninstallAppProgress;)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->requestFocus()Z

    goto/16 :goto_0

    :pswitch_2
    const v2, 0x7f060028    # com.android.packageinstaller.R.string.uninstall_done

    iget-object v3, p0, Lcom/android/packageinstaller/UninstallAppProgress$1;->this$0:Lcom/android/packageinstaller/UninstallAppProgress;

    # getter for: Lcom/android/packageinstaller/UninstallAppProgress;->mStatusTextView:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/android/packageinstaller/UninstallAppProgress;->access$3(Lcom/android/packageinstaller/UninstallAppProgress;)Landroid/widget/TextView;

    move-result-object v3

    iget-object v4, p0, Lcom/android/packageinstaller/UninstallAppProgress$1;->this$0:Lcom/android/packageinstaller/UninstallAppProgress;

    invoke-virtual {v4}, Lcom/android/packageinstaller/UninstallAppProgress;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f04000c    # com.android.packageinstaller.R.color.install_succeed_color

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_2

    :pswitch_3
    const-string v3, "UninstallAppProgress"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Uninstall failed because "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " is a device admin"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/android/packageinstaller/UninstallAppProgress$1;->this$0:Lcom/android/packageinstaller/UninstallAppProgress;

    # getter for: Lcom/android/packageinstaller/UninstallAppProgress;->mDeviceManagerButton:Landroid/widget/Button;
    invoke-static {v3}, Lcom/android/packageinstaller/UninstallAppProgress;->access$4(Lcom/android/packageinstaller/UninstallAppProgress;)Landroid/widget/Button;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/widget/Button;->setVisibility(I)V

    const v2, 0x7f06002a    # com.android.packageinstaller.R.string.uninstall_failed_device_policy_manager

    goto/16 :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch -0x2
        :pswitch_3
        :pswitch_1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
