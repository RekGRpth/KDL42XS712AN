.class public final Lcom/android/packageinstaller/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/packageinstaller/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final Parse_error_dlg_text:I = 0x7f06002e

.field public static final Parse_error_dlg_title:I = 0x7f06002d

.field public static final allow_source_dlg_text:I = 0x7f060018

.field public static final allow_source_dlg_title:I = 0x7f060017

.field public static final app_name:I = 0x7f060000

.field public static final app_not_found_dlg_text:I = 0x7f060021

.field public static final app_not_found_dlg_title:I = 0x7f060020

.field public static final cancel:I = 0x7f060004

.field public static final change_install_location_hint:I = 0x7f060046

.field public static final change_loc_reinstall:I = 0x7f06003e

.field public static final change_location:I = 0x7f06003c

.field public static final default_install_location:I = 0x7f060031

.field public static final detail_app_size:I = 0x7f06004b

.field public static final dlg_app_replacement_statement:I = 0x7f06001b

.field public static final dlg_app_replacement_title:I = 0x7f06001a

.field public static final dlg_ok:I = 0x7f06001f

.field public static final dlg_sys_app_replacement_statement:I = 0x7f06001c

.field public static final done:I = 0x7f060002

.field public static final exteral_not_exist:I = 0x7f060044

.field public static final exteral_not_exist_sdcard:I = 0x7f060045

.field public static final install:I = 0x7f060001

.field public static final install_confirm_question:I = 0x7f060008

.field public static final install_cpspace_not_enough:I = 0x7f06004a

.field public static final install_done:I = 0x7f060007

.field public static final install_failed:I = 0x7f060009

.field public static final install_failed_cpu_abi_incompatible:I = 0x7f06000d

.field public static final install_failed_file_not_found:I = 0x7f06000e

.field public static final install_failed_inconsistent_certificates:I = 0x7f06000b

.field public static final install_failed_invalid_apk:I = 0x7f06000a

.field public static final install_failed_msg:I = 0x7f060011

.field public static final install_failed_older_sdk:I = 0x7f06000c

.field public static final install_failed_verify_failed:I = 0x7f06000f

.field public static final install_failed_verify_timeout:I = 0x7f060010

.field public static final install_free_size:I = 0x7f06002f

.field public static final install_installspace_not_enough:I = 0x7f060049

.field public static final install_location_auto:I = 0x7f060035

.field public static final install_location_flash:I = 0x7f060034

.field public static final install_location_sdcard:I = 0x7f060033

.field public static final install_location_usb:I = 0x7f060032

.field public static final install_not_remove_sdcard:I = 0x7f060047

.field public static final install_title_one:I = 0x7f060038

.field public static final install_title_three:I = 0x7f06003a

.field public static final install_title_two:I = 0x7f060039

.field public static final install_used_size:I = 0x7f060030

.field public static final installing:I = 0x7f060006

.field public static final launch:I = 0x7f060012

.field public static final manage_applications:I = 0x7f060019

.field public static final manage_device_administrators:I = 0x7f06002b

.field public static final no_sdcard:I = 0x7f060036

.field public static final ok:I = 0x7f060015

.field public static final out_of_space_dlg_text:I = 0x7f06001e

.field public static final out_of_space_dlg_title:I = 0x7f06001d

.field public static final security_settings_desc:I = 0x7f060003

.field public static final settings:I = 0x7f060016

.field public static final space_warning:I = 0x7f06003d

.field public static final still_install:I = 0x7f06003b

.field public static final there_is_no_sdcard:I = 0x7f060037

.field public static final toast_applicationInfo_error:I = 0x7f06004c

.field public static final toast_out_of_space:I = 0x7f06003f

.field public static final toast_packageUI_error:I = 0x7f060040

.field public static final toast_packagename_error:I = 0x7f06004d

.field public static final uninstall_activity_text:I = 0x7f060024

.field public static final uninstall_application_text:I = 0x7f060025

.field public static final uninstall_application_title:I = 0x7f060022

.field public static final uninstall_done:I = 0x7f060028

.field public static final uninstall_failed:I = 0x7f060029

.field public static final uninstall_failed_device_policy_manager:I = 0x7f06002a

.field public static final uninstall_failed_msg:I = 0x7f06002c

.field public static final uninstall_not_remove_sdcard:I = 0x7f060048

.field public static final uninstall_title_one:I = 0x7f060041

.field public static final uninstall_title_three:I = 0x7f060043

.field public static final uninstall_title_two:I = 0x7f060042

.field public static final uninstall_update_text:I = 0x7f060026

.field public static final uninstall_update_title:I = 0x7f060023

.field public static final uninstalling:I = 0x7f060027

.field public static final unknown:I = 0x7f060005

.field public static final unknown_apps_dlg_text:I = 0x7f060014

.field public static final unknown_apps_dlg_title:I = 0x7f060013


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
