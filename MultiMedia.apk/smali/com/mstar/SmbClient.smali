.class public Lcom/mstar/SmbClient;
.super Ljava/lang/Object;
.source "SmbClient.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mstar/SmbClient$scanHost;,
        Lcom/mstar/SmbClient$scanHostByPing;
    }
.end annotation


# static fields
.field private static final MAX_THREAD_COUNT:I = 0x10

.field private static final bit0:I = 0x1

.field private static final bit1:I = 0x2

.field private static final bit10:I = 0x400

.field private static final bit11:I = 0x800

.field private static final bit12:I = 0x1000

.field private static final bit13:I = 0x2000

.field private static final bit14:I = 0x4000

.field private static final bit15:I = 0x8000

.field private static final bit2:I = 0x4

.field private static final bit3:I = 0x8

.field private static final bit4:I = 0x10

.field private static final bit5:I = 0x20

.field private static final bit6:I = 0x40

.field private static final bit7:I = 0x80

.field private static final bit8:I = 0x100

.field private static final bit9:I = 0x200

.field public static logEnable:Z


# instance fields
.field HostTable:[Lcom/mstar/SambaFile;

.field PingTable:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field ScanCompleted:I

.field root:Ljcifs/smb/SmbFile;

.field private volatile stoprun:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x1

    sput-boolean v0, Lcom/mstar/SmbClient;->logEnable:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mstar/SmbClient;->HostTable:[Lcom/mstar/SambaFile;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/mstar/SmbClient;->PingTable:Ljava/util/ArrayList;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mstar/SmbClient;->stoprun:Z

    return-void
.end method

.method static synthetic access$0(Lcom/mstar/SmbClient;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/mstar/SmbClient;->stoprun:Z

    return v0
.end method

.method static convertToSambaFile(Lcom/mstar/SambaFile;Ljcifs/smb/SmbFile;)V
    .locals 0
    .param p0    # Lcom/mstar/SambaFile;
    .param p1    # Ljcifs/smb/SmbFile;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljcifs/smb/SmbException;
        }
    .end annotation

    iput-object p1, p0, Lcom/mstar/SambaFile;->file:Ljcifs/smb/SmbFile;

    return-void
.end method

.method public static initSamba()V
    .locals 2

    const-string v0, "jcifs.encoding"

    const-string v1, "UTF8"

    invoke-static {v0, v1}, Ljcifs/Config;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "jcifs.smb.lmCompatibility"

    const-string v1, "0"

    invoke-static {v0, v1}, Ljcifs/Config;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "jcifs.smb.client.responseTimeout"

    const-string v1, "10000"

    invoke-static {v0, v1}, Ljcifs/Config;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "jcifs.netbios.retryTimeout"

    const-string v1, "10000"

    invoke-static {v0, v1}, Ljcifs/Config;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "jcifs.smb.client.dfs.disabled"

    const-string v1, "true"

    invoke-static {v0, v1}, Ljcifs/Config;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    invoke-static {}, Ljcifs/Config;->registerSmbURLHandler()V

    return-void
.end method


# virtual methods
.method public findFile(Lcom/mstar/SambaFile;Ljava/lang/String;)Lcom/mstar/SambaFile;
    .locals 5
    .param p1    # Lcom/mstar/SambaFile;
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/MalformedURLException;
        }
    .end annotation

    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p1}, Lcom/mstar/SambaFile;->listFiles()[Lcom/mstar/SambaFile;

    move-result-object v2

    const/4 v3, 0x0

    :goto_0
    array-length v4, v2

    if-lt v3, v4, :cond_0

    move-object v4, v1

    :goto_1
    return-object v4

    :cond_0
    aget-object v4, v2, v3

    invoke-virtual {v4}, Lcom/mstar/SambaFile;->getFileName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    aget-object v4, v2, v3

    goto :goto_1

    :cond_1
    aget-object v4, v2, v3

    invoke-virtual {v4}, Lcom/mstar/SambaFile;->isDirectory()Z

    move-result v4

    if-eqz v4, :cond_2

    aget-object v4, v2, v3

    invoke-virtual {p0, v4, p2}, Lcom/mstar/SmbClient;->findFile(Lcom/mstar/SambaFile;Ljava/lang/String;)Lcom/mstar/SambaFile;
    :try_end_0
    .catch Lcom/mstar/SambaException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    const/4 v4, 0x0

    goto :goto_1
.end method

.method public getHostTable()[Lcom/mstar/SambaFile;
    .locals 1

    iget-object v0, p0, Lcom/mstar/SmbClient;->HostTable:[Lcom/mstar/SambaFile;

    return-object v0
.end method

.method public getHostTableByPing()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/mstar/SmbClient;->PingTable:Ljava/util/ArrayList;

    return-object v0
.end method

.method public isConnected()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/SambaException;
        }
    .end annotation

    :try_start_0
    iget-object v1, p0, Lcom/mstar/SmbClient;->root:Ljcifs/smb/SmbFile;

    invoke-virtual {v1}, Ljcifs/smb/SmbFile;->exists()Z
    :try_end_0
    .catch Ljcifs/smb/SmbException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    return v1

    :catch_0
    move-exception v0

    new-instance v1, Lcom/mstar/SambaException;

    invoke-direct {v1, v0}, Lcom/mstar/SambaException;-><init>(Ljcifs/smb/SmbException;)V

    throw v1
.end method

.method public isHost(Ljcifs/smb/SmbFile;)Z
    .locals 3
    .param p1    # Ljcifs/smb/SmbFile;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljcifs/smb/SmbException;
        }
    .end annotation

    const/4 v1, 0x1

    invoke-virtual {p1}, Ljcifs/smb/SmbFile;->getType()I

    move-result v0

    if-eq v0, v1, :cond_0

    const/16 v2, 0x8

    if-eq v0, v2, :cond_0

    const/4 v2, 0x4

    if-ne v0, v2, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public pingHost(Ljava/lang/String;I)Z
    .locals 11
    .param p1    # Ljava/lang/String;
    .param p2    # I

    const/4 v8, 0x0

    const/4 v2, 0x0

    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v7

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "ping -w "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " -c 1 "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    sget-boolean v9, Lcom/mstar/SmbClient;->logEnable:Z

    if-eqz v9, :cond_0

    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v9, v6}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    :try_start_0
    invoke-virtual {v7, v6}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v5

    if-nez v5, :cond_1

    :try_start_1
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    :goto_0
    return v8

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    :cond_1
    :try_start_2
    new-instance v3, Ljava/io/BufferedReader;

    new-instance v9, Ljava/io/InputStreamReader;

    invoke-virtual {v5}, Ljava/lang/Process;->getInputStream()Ljava/io/InputStream;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v3, v9}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    const/4 v4, 0x0

    :cond_2
    :try_start_3
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_6
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result-object v4

    if-nez v4, :cond_4

    :try_start_4
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_5

    :goto_1
    sget-boolean v9, Lcom/mstar/SmbClient;->logEnable:Z

    if-eqz v9, :cond_3

    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, " fail "

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_3
    move-object v2, v3

    goto :goto_0

    :cond_4
    :try_start_5
    sget-boolean v9, Lcom/mstar/SmbClient;->logEnable:Z

    if-eqz v9, :cond_5

    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v9, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_5
    const-string v9, "bytes from"

    invoke-virtual {v4, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_2

    sget-boolean v9, Lcom/mstar/SmbClient;->logEnable:Z

    if-eqz v9, :cond_6

    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, " ok "

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_6
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :cond_6
    :try_start_6
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1

    :goto_2
    const/4 v8, 0x1

    move-object v2, v3

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    :catch_2
    move-exception v1

    :goto_3
    :try_start_7
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3

    goto :goto_0

    :catch_3
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    :catchall_0
    move-exception v8

    :goto_4
    :try_start_8
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_4

    :goto_5
    throw v8

    :catch_4
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    :catch_5
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    :catchall_1
    move-exception v8

    move-object v2, v3

    goto :goto_4

    :catch_6
    move-exception v1

    move-object v2, v3

    goto :goto_3
.end method

.method public scanAvailableHosts(Ljava/lang/String;Lcom/mstar/CallBack;)V
    .locals 11
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/mstar/CallBack;

    const/4 v4, 0x0

    if-nez p1, :cond_1

    const/4 v0, 0x0

    const/4 v1, 0x7

    invoke-interface {p2, v0, v1}, Lcom/mstar/CallBack;->callback(Lcom/mstar/SambaFile;I)V

    :cond_0
    return-void

    :cond_1
    const/16 v0, 0x2e

    invoke-virtual {p1, v0}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v8

    add-int/lit8 v0, v8, 0x1

    invoke-virtual {p1, v4, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    sget-boolean v0, Lcom/mstar/SmbClient;->logEnable:Z

    if-eqz v0, :cond_2

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "start ip : "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_2
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    iput v4, p0, Lcom/mstar/SmbClient;->ScanCompleted:I

    const/4 v7, 0x0

    :goto_0
    const/16 v0, 0x10

    if-ge v7, v0, :cond_0

    new-instance v10, Ljava/lang/Thread;

    new-instance v0, Lcom/mstar/SmbClient$scanHostByPing;

    add-int/lit8 v3, v7, 0x1

    mul-int/lit8 v1, v7, 0x10

    add-int/lit8 v4, v1, 0x1

    mul-int/lit8 v1, v7, 0x10

    add-int/lit8 v5, v1, 0x10

    move-object v1, p0

    move-object v6, p2

    invoke-direct/range {v0 .. v6}, Lcom/mstar/SmbClient$scanHostByPing;-><init>(Lcom/mstar/SmbClient;Ljava/lang/String;IIILcom/mstar/CallBack;)V

    invoke-direct {v10, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v9, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v9, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    :cond_3
    add-int/lit8 v7, v7, 0x1

    goto :goto_0
.end method

.method public scanAvailableHosts(Ljava/lang/String;Lcom/mstar/NbtAuthority;Lcom/mstar/CallBack;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/mstar/NbtAuthority;
    .param p3    # Lcom/mstar/CallBack;

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/mstar/SmbClient$scanHost;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/mstar/SmbClient$scanHost;-><init>(Lcom/mstar/SmbClient;Ljava/lang/String;Lcom/mstar/NbtAuthority;Lcom/mstar/CallBack;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method public stopScan()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mstar/SmbClient;->stoprun:Z

    return-void
.end method
