.class public interface abstract Lcom/mstar/tv/service/interfaces/ITvServiceServerS3D;
.super Ljava/lang/Object;
.source "ITvServiceServerS3D.java"

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mstar/tv/service/interfaces/ITvServiceServerS3D$Stub;
    }
.end annotation


# virtual methods
.method public abstract get3DDepthMode()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract get3DOffsetMode()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract get3DOutputAspectMode()Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_3DOUTPUTASPECT;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract getAutoStartMode()Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_AUTOSTART;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract getDisplay3DTo2DMode()Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_3DTO2D;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract getDisplayFormat()Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_DISPLAYFORMAT;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract getLRViewSwitch()Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_LRVIEWSWITCH;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract getSelfAdaptiveDetect()Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_SELFADAPTIVE_DETECT;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract getSelfAdaptiveLevel()Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_SELFADAPTIVE_LEVEL;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract set3DDepthMode(I)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract set3DOffsetMode(I)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract set3DOutputAspectMode(Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_3DOUTPUTASPECT;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract set3DTo2D(Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_3DTO2D;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract setAutoStartMode(Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_AUTOSTART;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract setDisplayFormat(Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_DISPLAYFORMAT;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract setDisplayFormatForUI(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract setLRViewSwitch(Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_LRVIEWSWITCH;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract setSelfAdaptiveDetect(Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_SELFADAPTIVE_DETECT;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract setSelfAdaptiveLevel(Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_SELFADAPTIVE_LEVEL;)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
