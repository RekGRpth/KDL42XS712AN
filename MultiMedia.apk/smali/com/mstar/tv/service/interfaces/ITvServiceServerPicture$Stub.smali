.class public abstract Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture$Stub;
.super Landroid/os/Binder;
.source "ITvServiceServerPicture.java"

# interfaces
.implements Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.mstar.tv.service.interfaces.ITvServiceServerPicture"

.field static final TRANSACTION_ExecAutoPc:I = 0x1b

.field static final TRANSACTION_enableSetBacklight:I = 0x22

.field static final TRANSACTION_enter4K2KMode:I = 0x23

.field static final TRANSACTION_execVideoItem:I = 0x5

.field static final TRANSACTION_freezeImage:I = 0x1d

.field static final TRANSACTION_getBacklight:I = 0x8

.field static final TRANSACTION_getColorTempIdx:I = 0xa

.field static final TRANSACTION_getColorTempPara:I = 0xc

.field static final TRANSACTION_getDlcAverageLuma:I = 0x1c

.field static final TRANSACTION_getDlcHistogramMax:I = 0x20

.field static final TRANSACTION_getDlcHistogramMin:I = 0x21

.field static final TRANSACTION_getMWEStatus:I = 0x12

.field static final TRANSACTION_getMpegNR:I = 0xe

.field static final TRANSACTION_getNR:I = 0x10

.field static final TRANSACTION_getPCClock:I = 0x17

.field static final TRANSACTION_getPCHPos:I = 0x13

.field static final TRANSACTION_getPCPhase:I = 0x19

.field static final TRANSACTION_getPCVPos:I = 0x15

.field static final TRANSACTION_getPictureModeIdx:I = 0x2

.field static final TRANSACTION_getVideoArc:I = 0x4

.field static final TRANSACTION_getVideoItem:I = 0x6

.field static final TRANSACTION_is4K2KMode:I = 0x24

.field static final TRANSACTION_setBacklight:I = 0x7

.field static final TRANSACTION_setColorTempIdx:I = 0x9

.field static final TRANSACTION_setColorTempPara:I = 0xb

.field static final TRANSACTION_setMWEStatus:I = 0x11

.field static final TRANSACTION_setMpegNR:I = 0xd

.field static final TRANSACTION_setNR:I = 0xf

.field static final TRANSACTION_setPCClock:I = 0x18

.field static final TRANSACTION_setPCHPos:I = 0x14

.field static final TRANSACTION_setPCPhase:I = 0x1a

.field static final TRANSACTION_setPCVPos:I = 0x16

.field static final TRANSACTION_setPictureModeIdx:I = 0x1

.field static final TRANSACTION_setSwingLevel:I = 0x1f

.field static final TRANSACTION_setVideoArc:I = 0x3

.field static final TRANSACTION_unFreezeImage:I = 0x1e


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    const-string v0, "com.mstar.tv.service.interfaces.ITvServiceServerPicture"

    invoke-virtual {p0, p0, v0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture;
    .locals 2
    .param p0    # Landroid/os/IBinder;

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const-string v1, "com.mstar.tv.service.interfaces.ITvServiceServerPicture"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture;

    if-eqz v1, :cond_1

    check-cast v0, Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture;

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture$Stub$Proxy;

    invoke-direct {v0, p0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 6
    .param p1    # I
    .param p2    # Landroid/os/Parcel;
    .param p3    # Landroid/os/Parcel;
    .param p4    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v3, 0x0

    const/4 v4, 0x1

    sparse-switch p1, :sswitch_data_0

    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v4

    :goto_0
    return v4

    :sswitch_0
    const-string v3, "com.mstar.tv.service.interfaces.ITvServiceServerPicture"

    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    :sswitch_1
    const-string v5, "com.mstar.tv.service.interfaces.ITvServiceServerPicture"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    if-eqz v5, :cond_1

    sget-object v5, Lcom/mstar/tv/service/aidl/EN_MS_PICTURE;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v5, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/tv/service/aidl/EN_MS_PICTURE;

    :goto_1
    invoke-virtual {p0, v0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture$Stub;->setPictureModeIdx(Lcom/mstar/tv/service/aidl/EN_MS_PICTURE;)Z

    move-result v2

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v2, :cond_0

    move v3, v4

    :cond_0
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :sswitch_2
    const-string v5, "com.mstar.tv.service.interfaces.ITvServiceServerPicture"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture$Stub;->getPictureModeIdx()Lcom/mstar/tv/service/aidl/EN_MS_PICTURE;

    move-result-object v2

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v2, :cond_2

    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {v2, p3, v4}, Lcom/mstar/tv/service/aidl/EN_MS_PICTURE;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_0

    :cond_2
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    :sswitch_3
    const-string v5, "com.mstar.tv.service.interfaces.ITvServiceServerPicture"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    if-eqz v5, :cond_4

    sget-object v5, Lcom/mstar/tv/service/aidl/MAPI_VIDEO_ARC_Type;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v5, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/tv/service/aidl/MAPI_VIDEO_ARC_Type;

    :goto_2
    invoke-virtual {p0, v0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture$Stub;->setVideoArc(Lcom/mstar/tv/service/aidl/MAPI_VIDEO_ARC_Type;)Z

    move-result v2

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v2, :cond_3

    move v3, v4

    :cond_3
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    goto :goto_2

    :sswitch_4
    const-string v5, "com.mstar.tv.service.interfaces.ITvServiceServerPicture"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture$Stub;->getVideoArc()Lcom/mstar/tv/service/aidl/MAPI_VIDEO_ARC_Type;

    move-result-object v2

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v2, :cond_5

    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {v2, p3, v4}, Lcom/mstar/tv/service/aidl/MAPI_VIDEO_ARC_Type;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_0

    :cond_5
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    :sswitch_5
    const-string v5, "com.mstar.tv.service.interfaces.ITvServiceServerPicture"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    if-eqz v5, :cond_7

    sget-object v5, Lcom/mstar/tv/service/aidl/EN_MS_VIDEOITEM;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v5, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/tv/service/aidl/EN_MS_VIDEOITEM;

    :goto_3
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture$Stub;->execVideoItem(Lcom/mstar/tv/service/aidl/EN_MS_VIDEOITEM;I)Z

    move-result v2

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v2, :cond_6

    move v3, v4

    :cond_6
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :cond_7
    const/4 v0, 0x0

    goto :goto_3

    :sswitch_6
    const-string v3, "com.mstar.tv.service.interfaces.ITvServiceServerPicture"

    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_8

    sget-object v3, Lcom/mstar/tv/service/aidl/EN_MS_VIDEOITEM;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v3, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/tv/service/aidl/EN_MS_VIDEOITEM;

    :goto_4
    invoke-virtual {p0, v0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture$Stub;->getVideoItem(Lcom/mstar/tv/service/aidl/EN_MS_VIDEOITEM;)I

    move-result v2

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :cond_8
    const/4 v0, 0x0

    goto :goto_4

    :sswitch_7
    const-string v5, "com.mstar.tv.service.interfaces.ITvServiceServerPicture"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture$Stub;->setBacklight(I)Z

    move-result v2

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v2, :cond_9

    move v3, v4

    :cond_9
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :sswitch_8
    const-string v3, "com.mstar.tv.service.interfaces.ITvServiceServerPicture"

    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture$Stub;->getBacklight()I

    move-result v2

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :sswitch_9
    const-string v5, "com.mstar.tv.service.interfaces.ITvServiceServerPicture"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    if-eqz v5, :cond_b

    sget-object v5, Lcom/mstar/tv/service/aidl/EN_MS_COLOR_TEMP;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v5, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/tv/service/aidl/EN_MS_COLOR_TEMP;

    :goto_5
    invoke-virtual {p0, v0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture$Stub;->setColorTempIdx(Lcom/mstar/tv/service/aidl/EN_MS_COLOR_TEMP;)Z

    move-result v2

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v2, :cond_a

    move v3, v4

    :cond_a
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :cond_b
    const/4 v0, 0x0

    goto :goto_5

    :sswitch_a
    const-string v5, "com.mstar.tv.service.interfaces.ITvServiceServerPicture"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture$Stub;->getColorTempIdx()Lcom/mstar/tv/service/aidl/EN_MS_COLOR_TEMP;

    move-result-object v2

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v2, :cond_c

    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {v2, p3, v4}, Lcom/mstar/tv/service/aidl/EN_MS_COLOR_TEMP;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    :cond_c
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :sswitch_b
    const-string v5, "com.mstar.tv.service.interfaces.ITvServiceServerPicture"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    if-eqz v5, :cond_e

    sget-object v5, Lcom/mstar/tv/service/aidl/T_MS_COLOR_TEMPEX_DATA;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v5, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/tv/service/aidl/T_MS_COLOR_TEMPEX_DATA;

    :goto_6
    invoke-virtual {p0, v0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture$Stub;->setColorTempPara(Lcom/mstar/tv/service/aidl/T_MS_COLOR_TEMPEX_DATA;)Z

    move-result v2

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v2, :cond_d

    move v3, v4

    :cond_d
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :cond_e
    const/4 v0, 0x0

    goto :goto_6

    :sswitch_c
    const-string v5, "com.mstar.tv.service.interfaces.ITvServiceServerPicture"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture$Stub;->getColorTempPara()Lcom/mstar/tv/service/aidl/T_MS_COLOR_TEMPEX_DATA;

    move-result-object v2

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v2, :cond_f

    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {v2, p3, v4}, Lcom/mstar/tv/service/aidl/T_MS_COLOR_TEMPEX_DATA;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    :cond_f
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :sswitch_d
    const-string v5, "com.mstar.tv.service.interfaces.ITvServiceServerPicture"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    if-eqz v5, :cond_11

    sget-object v5, Lcom/mstar/tv/service/aidl/EN_MS_MPEG_NR;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v5, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/tv/service/aidl/EN_MS_MPEG_NR;

    :goto_7
    invoke-virtual {p0, v0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture$Stub;->setMpegNR(Lcom/mstar/tv/service/aidl/EN_MS_MPEG_NR;)Z

    move-result v2

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v2, :cond_10

    move v3, v4

    :cond_10
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :cond_11
    const/4 v0, 0x0

    goto :goto_7

    :sswitch_e
    const-string v5, "com.mstar.tv.service.interfaces.ITvServiceServerPicture"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture$Stub;->getMpegNR()Lcom/mstar/tv/service/aidl/EN_MS_MPEG_NR;

    move-result-object v2

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v2, :cond_12

    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {v2, p3, v4}, Lcom/mstar/tv/service/aidl/EN_MS_MPEG_NR;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    :cond_12
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :sswitch_f
    const-string v5, "com.mstar.tv.service.interfaces.ITvServiceServerPicture"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    if-eqz v5, :cond_14

    sget-object v5, Lcom/mstar/tv/service/aidl/EN_MS_NR;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v5, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/tv/service/aidl/EN_MS_NR;

    :goto_8
    invoke-virtual {p0, v0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture$Stub;->setNR(Lcom/mstar/tv/service/aidl/EN_MS_NR;)Z

    move-result v2

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v2, :cond_13

    move v3, v4

    :cond_13
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :cond_14
    const/4 v0, 0x0

    goto :goto_8

    :sswitch_10
    const-string v5, "com.mstar.tv.service.interfaces.ITvServiceServerPicture"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture$Stub;->getNR()Lcom/mstar/tv/service/aidl/EN_MS_NR;

    move-result-object v2

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v2, :cond_15

    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {v2, p3, v4}, Lcom/mstar/tv/service/aidl/EN_MS_NR;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    :cond_15
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :sswitch_11
    const-string v5, "com.mstar.tv.service.interfaces.ITvServiceServerPicture"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    if-eqz v5, :cond_17

    sget-object v5, Lcom/mstar/tv/service/aidl/EN_MWE_TYPE;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v5, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/tv/service/aidl/EN_MWE_TYPE;

    :goto_9
    invoke-virtual {p0, v0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture$Stub;->setMWEStatus(Lcom/mstar/tv/service/aidl/EN_MWE_TYPE;)Z

    move-result v2

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v2, :cond_16

    move v3, v4

    :cond_16
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :cond_17
    const/4 v0, 0x0

    goto :goto_9

    :sswitch_12
    const-string v5, "com.mstar.tv.service.interfaces.ITvServiceServerPicture"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture$Stub;->getMWEStatus()Lcom/mstar/tv/service/aidl/EN_MWE_TYPE;

    move-result-object v2

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v2, :cond_18

    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {v2, p3, v4}, Lcom/mstar/tv/service/aidl/EN_MWE_TYPE;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    :cond_18
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :sswitch_13
    const-string v3, "com.mstar.tv.service.interfaces.ITvServiceServerPicture"

    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture$Stub;->getPCHPos()I

    move-result v2

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :sswitch_14
    const-string v5, "com.mstar.tv.service.interfaces.ITvServiceServerPicture"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture$Stub;->setPCHPos(I)Z

    move-result v2

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v2, :cond_19

    move v3, v4

    :cond_19
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :sswitch_15
    const-string v3, "com.mstar.tv.service.interfaces.ITvServiceServerPicture"

    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture$Stub;->getPCVPos()I

    move-result v2

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :sswitch_16
    const-string v5, "com.mstar.tv.service.interfaces.ITvServiceServerPicture"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture$Stub;->setPCVPos(I)Z

    move-result v2

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v2, :cond_1a

    move v3, v4

    :cond_1a
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :sswitch_17
    const-string v3, "com.mstar.tv.service.interfaces.ITvServiceServerPicture"

    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture$Stub;->getPCClock()I

    move-result v2

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :sswitch_18
    const-string v5, "com.mstar.tv.service.interfaces.ITvServiceServerPicture"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture$Stub;->setPCClock(I)Z

    move-result v2

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v2, :cond_1b

    move v3, v4

    :cond_1b
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :sswitch_19
    const-string v3, "com.mstar.tv.service.interfaces.ITvServiceServerPicture"

    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture$Stub;->getPCPhase()I

    move-result v2

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :sswitch_1a
    const-string v5, "com.mstar.tv.service.interfaces.ITvServiceServerPicture"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture$Stub;->setPCPhase(I)Z

    move-result v2

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v2, :cond_1c

    move v3, v4

    :cond_1c
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :sswitch_1b
    const-string v5, "com.mstar.tv.service.interfaces.ITvServiceServerPicture"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture$Stub;->ExecAutoPc()Z

    move-result v2

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v2, :cond_1d

    move v3, v4

    :cond_1d
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :sswitch_1c
    const-string v3, "com.mstar.tv.service.interfaces.ITvServiceServerPicture"

    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture$Stub;->getDlcAverageLuma()I

    move-result v2

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :sswitch_1d
    const-string v5, "com.mstar.tv.service.interfaces.ITvServiceServerPicture"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture$Stub;->freezeImage()Z

    move-result v2

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v2, :cond_1e

    move v3, v4

    :cond_1e
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :sswitch_1e
    const-string v5, "com.mstar.tv.service.interfaces.ITvServiceServerPicture"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture$Stub;->unFreezeImage()Z

    move-result v2

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v2, :cond_1f

    move v3, v4

    :cond_1f
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :sswitch_1f
    const-string v5, "com.mstar.tv.service.interfaces.ITvServiceServerPicture"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture$Stub;->setSwingLevel(I)Z

    move-result v2

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v2, :cond_20

    move v3, v4

    :cond_20
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :sswitch_20
    const-string v3, "com.mstar.tv.service.interfaces.ITvServiceServerPicture"

    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture$Stub;->getDlcHistogramMax()I

    move-result v2

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :sswitch_21
    const-string v3, "com.mstar.tv.service.interfaces.ITvServiceServerPicture"

    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture$Stub;->getDlcHistogramMin()I

    move-result v2

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :sswitch_22
    const-string v5, "com.mstar.tv.service.interfaces.ITvServiceServerPicture"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    if-eqz v5, :cond_22

    move v0, v4

    :goto_a
    invoke-virtual {p0, v0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture$Stub;->enableSetBacklight(Z)Z

    move-result v2

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v2, :cond_21

    move v3, v4

    :cond_21
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :cond_22
    move v0, v3

    goto :goto_a

    :sswitch_23
    const-string v3, "com.mstar.tv.service.interfaces.ITvServiceServerPicture"

    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_23

    sget-object v3, Lcom/mstar/tv/service/aidl/EN_4K2K_MODE;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v3, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/tv/service/aidl/EN_4K2K_MODE;

    :goto_b
    invoke-virtual {p0, v0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture$Stub;->enter4K2KMode(Lcom/mstar/tv/service/aidl/EN_4K2K_MODE;)V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    :cond_23
    const/4 v0, 0x0

    goto :goto_b

    :sswitch_24
    const-string v5, "com.mstar.tv.service.interfaces.ITvServiceServerPicture"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture$Stub;->is4K2KMode()Z

    move-result v2

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v2, :cond_24

    move v3, v4

    :cond_24
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0xa -> :sswitch_a
        0xb -> :sswitch_b
        0xc -> :sswitch_c
        0xd -> :sswitch_d
        0xe -> :sswitch_e
        0xf -> :sswitch_f
        0x10 -> :sswitch_10
        0x11 -> :sswitch_11
        0x12 -> :sswitch_12
        0x13 -> :sswitch_13
        0x14 -> :sswitch_14
        0x15 -> :sswitch_15
        0x16 -> :sswitch_16
        0x17 -> :sswitch_17
        0x18 -> :sswitch_18
        0x19 -> :sswitch_19
        0x1a -> :sswitch_1a
        0x1b -> :sswitch_1b
        0x1c -> :sswitch_1c
        0x1d -> :sswitch_1d
        0x1e -> :sswitch_1e
        0x1f -> :sswitch_1f
        0x20 -> :sswitch_20
        0x21 -> :sswitch_21
        0x22 -> :sswitch_22
        0x23 -> :sswitch_23
        0x24 -> :sswitch_24
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
