.class public Lcom/mstar/tv/service/skin/PictureSkin;
.super Ljava/lang/Object;
.source "PictureSkin.java"


# instance fields
.field private handler:Landroid/os/Handler;

.field private iTvServicePicture:Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture;

.field private isBindOk:Z

.field private superContext:Landroid/content/Context;

.field protected tvServicePictureConnection:Landroid/content/ServiceConnection;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mstar/tv/service/skin/PictureSkin;->handler:Landroid/os/Handler;

    new-instance v0, Lcom/mstar/tv/service/skin/PictureSkin$1;

    invoke-direct {v0, p0}, Lcom/mstar/tv/service/skin/PictureSkin$1;-><init>(Lcom/mstar/tv/service/skin/PictureSkin;)V

    iput-object v0, p0, Lcom/mstar/tv/service/skin/PictureSkin;->tvServicePictureConnection:Landroid/content/ServiceConnection;

    iput-object p1, p0, Lcom/mstar/tv/service/skin/PictureSkin;->superContext:Landroid/content/Context;

    return-void
.end method

.method static synthetic access$0(Lcom/mstar/tv/service/skin/PictureSkin;Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture;)V
    .locals 0

    iput-object p1, p0, Lcom/mstar/tv/service/skin/PictureSkin;->iTvServicePicture:Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture;

    return-void
.end method

.method static synthetic access$1(Lcom/mstar/tv/service/skin/PictureSkin;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/mstar/tv/service/skin/PictureSkin;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$2(Lcom/mstar/tv/service/skin/PictureSkin;)Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture;
    .locals 1

    iget-object v0, p0, Lcom/mstar/tv/service/skin/PictureSkin;->iTvServicePicture:Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture;

    return-object v0
.end method


# virtual methods
.method public ExecAutoPc()Z
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/PictureSkin;->iTvServicePicture:Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture;

    invoke-interface {v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture;->ExecAutoPc()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public connect(Landroid/os/Handler;)Z
    .locals 5
    .param p1    # Landroid/os/Handler;

    const/4 v4, 0x0

    iput-object p1, p0, Lcom/mstar/tv/service/skin/PictureSkin;->handler:Landroid/os/Handler;

    const-string v3, "tv_services"

    invoke-static {v3}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServer$Stub;->asInterface(Landroid/os/IBinder;)Lcom/mstar/tv/service/interfaces/ITvServiceServer;

    move-result-object v2

    if-eqz v2, :cond_0

    :try_start_0
    invoke-interface {v2}, Lcom/mstar/tv/service/interfaces/ITvServiceServer;->getPictureManager()Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture;

    move-result-object v3

    iput-object v3, p0, Lcom/mstar/tv/service/skin/PictureSkin;->iTvServicePicture:Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture;

    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/mstar/tv/service/skin/PictureSkin;->isBindOk:Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-boolean v3, p0, Lcom/mstar/tv/service/skin/PictureSkin;->isBindOk:Z

    return v3

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    iput-boolean v4, p0, Lcom/mstar/tv/service/skin/PictureSkin;->isBindOk:Z

    goto :goto_0

    :cond_0
    iput-boolean v4, p0, Lcom/mstar/tv/service/skin/PictureSkin;->isBindOk:Z

    goto :goto_0
.end method

.method public disconnect()V
    .locals 0

    return-void
.end method

.method public enableSetBacklight(Z)Z
    .locals 2
    .param p1    # Z

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/PictureSkin;->iTvServicePicture:Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture;

    invoke-interface {v1, p1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture;->enableSetBacklight(Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public enter4K2KMode(Lcom/mstar/tv/service/aidl/EN_4K2K_MODE;)V
    .locals 2
    .param p1    # Lcom/mstar/tv/service/aidl/EN_4K2K_MODE;

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/PictureSkin;->iTvServicePicture:Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture;

    invoke-interface {v1, p1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture;->enter4K2KMode(Lcom/mstar/tv/service/aidl/EN_4K2K_MODE;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public freezeImage()Z
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/PictureSkin;->iTvServicePicture:Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture;

    invoke-interface {v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture;->freezeImage()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getBacklight()I
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/PictureSkin;->iTvServicePicture:Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture;

    invoke-interface {v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture;->getBacklight()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, -0x1

    goto :goto_0
.end method

.method public getColorTempIdx()Lcom/mstar/tv/service/aidl/EN_MS_COLOR_TEMP;
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/PictureSkin;->iTvServicePicture:Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture;

    invoke-interface {v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture;->getColorTempIdx()Lcom/mstar/tv/service/aidl/EN_MS_COLOR_TEMP;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getColorTempPara()Lcom/mstar/tv/service/aidl/T_MS_COLOR_TEMPEX_DATA;
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/PictureSkin;->iTvServicePicture:Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture;

    invoke-interface {v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture;->getColorTempPara()Lcom/mstar/tv/service/aidl/T_MS_COLOR_TEMPEX_DATA;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getDlcAverageLuma()I
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/PictureSkin;->iTvServicePicture:Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture;

    invoke-interface {v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture;->getDlcAverageLuma()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, -0x1

    goto :goto_0
.end method

.method public getMWEStatus()Lcom/mstar/tv/service/aidl/EN_MWE_TYPE;
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/PictureSkin;->iTvServicePicture:Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture;

    invoke-interface {v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture;->getMWEStatus()Lcom/mstar/tv/service/aidl/EN_MWE_TYPE;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getMpegNR()Lcom/mstar/tv/service/aidl/EN_MS_MPEG_NR;
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/PictureSkin;->iTvServicePicture:Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture;

    invoke-interface {v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture;->getMpegNR()Lcom/mstar/tv/service/aidl/EN_MS_MPEG_NR;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getNR()Lcom/mstar/tv/service/aidl/EN_MS_NR;
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/PictureSkin;->iTvServicePicture:Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture;

    invoke-interface {v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture;->getNR()Lcom/mstar/tv/service/aidl/EN_MS_NR;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getPCClock()I
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/PictureSkin;->iTvServicePicture:Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture;

    invoke-interface {v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture;->getPCClock()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, -0x1

    goto :goto_0
.end method

.method public getPCHPos()I
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/PictureSkin;->iTvServicePicture:Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture;

    invoke-interface {v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture;->getPCHPos()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, -0x1

    goto :goto_0
.end method

.method public getPCPhase()I
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/PictureSkin;->iTvServicePicture:Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture;

    invoke-interface {v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture;->getPCPhase()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, -0x1

    goto :goto_0
.end method

.method public getPCVPos()I
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/PictureSkin;->iTvServicePicture:Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture;

    invoke-interface {v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture;->getPCVPos()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, -0x1

    goto :goto_0
.end method

.method public getPictureModeIdx()Lcom/mstar/tv/service/aidl/EN_MS_PICTURE;
    .locals 6

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/mstar/tv/service/skin/PictureSkin;->iTvServicePicture:Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture;

    if-nez v2, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Picture service head is null!\t"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/Exception;

    invoke-direct {v4}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v4}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v4

    const/4 v5, 0x0

    aget-object v4, v4, v5

    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-object v1

    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/mstar/tv/service/skin/PictureSkin;->iTvServicePicture:Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture;

    invoke-interface {v2}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture;->getPictureModeIdx()Lcom/mstar/tv/service/aidl/EN_MS_PICTURE;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getVideoArc()Lcom/mstar/tv/service/aidl/MAPI_VIDEO_ARC_Type;
    .locals 6

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/mstar/tv/service/skin/PictureSkin;->iTvServicePicture:Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture;

    if-nez v2, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Picture service head is null!\t"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/Exception;

    invoke-direct {v4}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v4}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v4

    const/4 v5, 0x0

    aget-object v4, v4, v5

    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-object v1

    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/mstar/tv/service/skin/PictureSkin;->iTvServicePicture:Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture;

    invoke-interface {v2}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture;->getVideoArc()Lcom/mstar/tv/service/aidl/MAPI_VIDEO_ARC_Type;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getVideoItem(Lcom/mstar/tv/service/aidl/EN_MS_VIDEOITEM;)I
    .locals 2
    .param p1    # Lcom/mstar/tv/service/aidl/EN_MS_VIDEOITEM;

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/PictureSkin;->iTvServicePicture:Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture;

    invoke-interface {v1, p1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture;->getVideoItem(Lcom/mstar/tv/service/aidl/EN_MS_VIDEOITEM;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, -0x1

    goto :goto_0
.end method

.method public isConnectionOk()Z
    .locals 1

    iget-object v0, p0, Lcom/mstar/tv/service/skin/PictureSkin;->iTvServicePicture:Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setBacklight(I)Z
    .locals 2
    .param p1    # I

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/PictureSkin;->iTvServicePicture:Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture;

    invoke-interface {v1, p1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture;->setBacklight(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setColorTempIdx(Lcom/mstar/tv/service/aidl/EN_MS_COLOR_TEMP;)Z
    .locals 2
    .param p1    # Lcom/mstar/tv/service/aidl/EN_MS_COLOR_TEMP;

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/PictureSkin;->iTvServicePicture:Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture;

    invoke-interface {v1, p1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture;->setColorTempIdx(Lcom/mstar/tv/service/aidl/EN_MS_COLOR_TEMP;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setColorTempPara(Lcom/mstar/tv/service/aidl/T_MS_COLOR_TEMPEX_DATA;)Z
    .locals 2
    .param p1    # Lcom/mstar/tv/service/aidl/T_MS_COLOR_TEMPEX_DATA;

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/PictureSkin;->iTvServicePicture:Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture;

    invoke-interface {v1, p1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture;->setColorTempPara(Lcom/mstar/tv/service/aidl/T_MS_COLOR_TEMPEX_DATA;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setMWEStatus(Lcom/mstar/tv/service/aidl/EN_MWE_TYPE;)Z
    .locals 2
    .param p1    # Lcom/mstar/tv/service/aidl/EN_MWE_TYPE;

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/PictureSkin;->iTvServicePicture:Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture;

    invoke-interface {v1, p1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture;->setMWEStatus(Lcom/mstar/tv/service/aidl/EN_MWE_TYPE;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setMpegNR(Lcom/mstar/tv/service/aidl/EN_MS_MPEG_NR;)Z
    .locals 2
    .param p1    # Lcom/mstar/tv/service/aidl/EN_MS_MPEG_NR;

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/PictureSkin;->iTvServicePicture:Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture;

    invoke-interface {v1, p1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture;->setMpegNR(Lcom/mstar/tv/service/aidl/EN_MS_MPEG_NR;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setNR(Lcom/mstar/tv/service/aidl/EN_MS_NR;)Z
    .locals 2
    .param p1    # Lcom/mstar/tv/service/aidl/EN_MS_NR;

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/PictureSkin;->iTvServicePicture:Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture;

    invoke-interface {v1, p1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture;->setNR(Lcom/mstar/tv/service/aidl/EN_MS_NR;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setPCClock(I)Z
    .locals 2
    .param p1    # I

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/PictureSkin;->iTvServicePicture:Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture;

    invoke-interface {v1, p1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture;->setPCClock(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setPCHPos(I)Z
    .locals 2
    .param p1    # I

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/PictureSkin;->iTvServicePicture:Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture;

    invoke-interface {v1, p1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture;->setPCHPos(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setPCPhase(I)Z
    .locals 2
    .param p1    # I

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/PictureSkin;->iTvServicePicture:Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture;

    invoke-interface {v1, p1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture;->setPCClock(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setPCVPos(I)Z
    .locals 2
    .param p1    # I

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/PictureSkin;->iTvServicePicture:Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture;

    invoke-interface {v1, p1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture;->setPCVPos(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setPictureModeIdx(Lcom/mstar/tv/service/aidl/EN_MS_PICTURE;)Z
    .locals 5
    .param p1    # Lcom/mstar/tv/service/aidl/EN_MS_PICTURE;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/mstar/tv/service/skin/PictureSkin;->iTvServicePicture:Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture;

    if-nez v2, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Picture service head is null!\t"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/Exception;

    invoke-direct {v4}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v4}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v4

    aget-object v4, v4, v1

    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v1

    :cond_0
    invoke-virtual {p1}, Lcom/mstar/tv/service/aidl/EN_MS_PICTURE;->ordinal()I

    move-result v2

    sget-object v3, Lcom/mstar/tv/service/aidl/EN_MS_PICTURE;->PICTURE_NUMS:Lcom/mstar/tv/service/aidl/EN_MS_PICTURE;

    invoke-virtual {v3}, Lcom/mstar/tv/service/aidl/EN_MS_PICTURE;->ordinal()I

    move-result v3

    if-lt v2, v3, :cond_1

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Input params out of boundary!\t"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/Exception;

    invoke-direct {v4}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v4}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v4

    aget-object v4, v4, v1

    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    :try_start_0
    iget-object v2, p0, Lcom/mstar/tv/service/skin/PictureSkin;->iTvServicePicture:Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture;

    invoke-interface {v2, p1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture;->setPictureModeIdx(Lcom/mstar/tv/service/aidl/EN_MS_PICTURE;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public setVideoArc(Lcom/mstar/tv/service/aidl/MAPI_VIDEO_ARC_Type;)Z
    .locals 5
    .param p1    # Lcom/mstar/tv/service/aidl/MAPI_VIDEO_ARC_Type;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/mstar/tv/service/skin/PictureSkin;->iTvServicePicture:Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture;

    if-nez v2, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Picture service head is null!\t"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/Exception;

    invoke-direct {v4}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v4}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v4

    aget-object v4, v4, v1

    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v1

    :cond_0
    invoke-virtual {p1}, Lcom/mstar/tv/service/aidl/MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v2

    sget-object v3, Lcom/mstar/tv/service/aidl/MAPI_VIDEO_ARC_Type;->E_AR_MAX:Lcom/mstar/tv/service/aidl/MAPI_VIDEO_ARC_Type;

    invoke-virtual {v3}, Lcom/mstar/tv/service/aidl/MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v3

    if-lt v2, v3, :cond_1

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Input params out of boundary!\t"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/Exception;

    invoke-direct {v4}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v4}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v4

    aget-object v4, v4, v1

    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    :try_start_0
    iget-object v2, p0, Lcom/mstar/tv/service/skin/PictureSkin;->iTvServicePicture:Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture;

    invoke-interface {v2, p1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture;->setVideoArc(Lcom/mstar/tv/service/aidl/MAPI_VIDEO_ARC_Type;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public setVideoItem(Lcom/mstar/tv/service/aidl/EN_MS_VIDEOITEM;I)Z
    .locals 2
    .param p1    # Lcom/mstar/tv/service/aidl/EN_MS_VIDEOITEM;
    .param p2    # I

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/PictureSkin;->iTvServicePicture:Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture;

    invoke-interface {v1, p1, p2}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture;->execVideoItem(Lcom/mstar/tv/service/aidl/EN_MS_VIDEOITEM;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public unFreezeImage()Z
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/PictureSkin;->iTvServicePicture:Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture;

    invoke-interface {v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPicture;->unFreezeImage()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method
