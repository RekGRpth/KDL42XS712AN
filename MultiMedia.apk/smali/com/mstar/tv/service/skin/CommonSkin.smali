.class public Lcom/mstar/tv/service/skin/CommonSkin;
.super Ljava/lang/Object;
.source "CommonSkin.java"


# instance fields
.field private handler:Landroid/os/Handler;

.field private iTvServiceCommon:Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon;

.field private isBindOk:Z

.field private superContext:Landroid/content/Context;

.field protected tvServiceCommonConnection:Landroid/content/ServiceConnection;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mstar/tv/service/skin/CommonSkin;->handler:Landroid/os/Handler;

    new-instance v0, Lcom/mstar/tv/service/skin/CommonSkin$1;

    invoke-direct {v0, p0}, Lcom/mstar/tv/service/skin/CommonSkin$1;-><init>(Lcom/mstar/tv/service/skin/CommonSkin;)V

    iput-object v0, p0, Lcom/mstar/tv/service/skin/CommonSkin;->tvServiceCommonConnection:Landroid/content/ServiceConnection;

    iput-object p1, p0, Lcom/mstar/tv/service/skin/CommonSkin;->superContext:Landroid/content/Context;

    return-void
.end method

.method static synthetic access$0(Lcom/mstar/tv/service/skin/CommonSkin;Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon;)V
    .locals 0

    iput-object p1, p0, Lcom/mstar/tv/service/skin/CommonSkin;->iTvServiceCommon:Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon;

    return-void
.end method

.method static synthetic access$1(Lcom/mstar/tv/service/skin/CommonSkin;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/mstar/tv/service/skin/CommonSkin;->handler:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public AllowDetection()V
    .locals 5

    iget-object v1, p0, Lcom/mstar/tv/service/skin/CommonSkin;->iTvServiceCommon:Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon;

    if-nez v1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Common service head is null!\t"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v3, Ljava/lang/Exception;

    invoke-direct {v3}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v3}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v3

    const/4 v4, 0x0

    aget-object v3, v3, v4

    invoke-virtual {v3}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/CommonSkin;->iTvServiceCommon:Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon;

    invoke-interface {v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon;->AllowDetection()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public DisableAutoSourceSwitch()V
    .locals 5

    iget-object v1, p0, Lcom/mstar/tv/service/skin/CommonSkin;->iTvServiceCommon:Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon;

    if-nez v1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Common service head is null!\t"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v3, Ljava/lang/Exception;

    invoke-direct {v3}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v3}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v3

    const/4 v4, 0x0

    aget-object v3, v3, v4

    invoke-virtual {v3}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/CommonSkin;->iTvServiceCommon:Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon;

    invoke-interface {v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon;->DisableAutoSourceSwitch()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public EnableAutoSourceSwitch()V
    .locals 5

    iget-object v1, p0, Lcom/mstar/tv/service/skin/CommonSkin;->iTvServiceCommon:Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon;

    if-nez v1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Common service head is null!\t"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v3, Ljava/lang/Exception;

    invoke-direct {v3}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v3}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v3

    const/4 v4, 0x0

    aget-object v3, v3, v4

    invoke-virtual {v3}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/CommonSkin;->iTvServiceCommon:Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon;

    invoke-interface {v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon;->EnableAutoSourceSwitch()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public ForbidDetection()V
    .locals 5

    iget-object v1, p0, Lcom/mstar/tv/service/skin/CommonSkin;->iTvServiceCommon:Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon;

    if-nez v1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Common service head is null!\t"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v3, Ljava/lang/Exception;

    invoke-direct {v3}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v3}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v3

    const/4 v4, 0x0

    aget-object v3, v3, v4

    invoke-virtual {v3}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/CommonSkin;->iTvServiceCommon:Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon;

    invoke-interface {v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon;->ForbidDetection()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public GetCurrentInputSource()Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;
    .locals 5

    iget-object v1, p0, Lcom/mstar/tv/service/skin/CommonSkin;->iTvServiceCommon:Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon;

    if-nez v1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Common service head is null!\t"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v3, Ljava/lang/Exception;

    invoke-direct {v3}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v3}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v3

    const/4 v4, 0x0

    aget-object v3, v3, v4

    invoke-virtual {v3}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v1, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_ATV:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    :goto_0
    return-object v1

    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/CommonSkin;->iTvServiceCommon:Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon;

    invoke-interface {v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon;->GetCurrentInputSource()Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public GetInputSourceStatus()Lcom/mstar/tv/service/aidl/BoolArrayList;
    .locals 6

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/mstar/tv/service/skin/CommonSkin;->iTvServiceCommon:Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon;

    if-nez v2, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Common service head is null!\t"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/Exception;

    invoke-direct {v4}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v4}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v4

    const/4 v5, 0x0

    aget-object v4, v4, v5

    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-object v1

    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/mstar/tv/service/skin/CommonSkin;->iTvServiceCommon:Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon;

    invoke-interface {v2}, Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon;->GetInputSourceStatus()Lcom/mstar/tv/service/aidl/BoolArrayList;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public OSD_Set3Dformat(Lcom/mstar/tv/service/aidl/EN_ThreeD_OSD_TYPE;)V
    .locals 2
    .param p1    # Lcom/mstar/tv/service/aidl/EN_ThreeD_OSD_TYPE;

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/CommonSkin;->iTvServiceCommon:Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon;

    invoke-interface {v1, p1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon;->OSD_Set3Dformat(Lcom/mstar/tv/service/aidl/EN_ThreeD_OSD_TYPE;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public SetInputSource(Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;)V
    .locals 5
    .param p1    # Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    const/4 v4, 0x0

    iget-object v1, p0, Lcom/mstar/tv/service/skin/CommonSkin;->iTvServiceCommon:Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon;

    if-nez v1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Common service head is null!\t"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v3, Ljava/lang/Exception;

    invoke-direct {v3}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v3}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v3

    aget-object v3, v3, v4

    invoke-virtual {v3}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->ordinal()I

    move-result v1

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_NUM:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    invoke-virtual {v2}, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->ordinal()I

    move-result v2

    if-lt v1, v2, :cond_1

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Input params out of boundary!\t"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v3, Ljava/lang/Exception;

    invoke-direct {v3}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v3}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v3

    aget-object v3, v3, v4

    invoke-virtual {v3}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/CommonSkin;->iTvServiceCommon:Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon;

    invoke-interface {v1, p1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon;->SetInputSource(Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public StartSourceDetection()V
    .locals 5

    iget-object v1, p0, Lcom/mstar/tv/service/skin/CommonSkin;->iTvServiceCommon:Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon;

    if-nez v1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Common service head is null!\t"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v3, Ljava/lang/Exception;

    invoke-direct {v3}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v3}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v3

    const/4 v4, 0x0

    aget-object v3, v3, v4

    invoke-virtual {v3}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/CommonSkin;->iTvServiceCommon:Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon;

    invoke-interface {v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon;->StartSourceDetection()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public StopSourceDetection()V
    .locals 5

    iget-object v1, p0, Lcom/mstar/tv/service/skin/CommonSkin;->iTvServiceCommon:Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon;

    if-nez v1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Common service head is null!\t"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v3, Ljava/lang/Exception;

    invoke-direct {v3}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v3}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v3

    const/4 v4, 0x0

    aget-object v3, v3, v4

    invoke-virtual {v3}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/CommonSkin;->iTvServiceCommon:Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon;

    invoke-interface {v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon;->StopSourceDetection()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public closeSurfaceView()V
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/CommonSkin;->iTvServiceCommon:Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon;

    invoke-interface {v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon;->closeSurfaceView()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public connect(Landroid/os/Handler;)Z
    .locals 5
    .param p1    # Landroid/os/Handler;

    const/4 v4, 0x0

    iput-object p1, p0, Lcom/mstar/tv/service/skin/CommonSkin;->handler:Landroid/os/Handler;

    const-string v3, "tv_services"

    invoke-static {v3}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServer$Stub;->asInterface(Landroid/os/IBinder;)Lcom/mstar/tv/service/interfaces/ITvServiceServer;

    move-result-object v2

    if-eqz v2, :cond_0

    :try_start_0
    invoke-interface {v2}, Lcom/mstar/tv/service/interfaces/ITvServiceServer;->getCommonManager()Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon;

    move-result-object v3

    iput-object v3, p0, Lcom/mstar/tv/service/skin/CommonSkin;->iTvServiceCommon:Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon;

    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/mstar/tv/service/skin/CommonSkin;->isBindOk:Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-boolean v3, p0, Lcom/mstar/tv/service/skin/CommonSkin;->isBindOk:Z

    return v3

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    iput-boolean v4, p0, Lcom/mstar/tv/service/skin/CommonSkin;->isBindOk:Z

    goto :goto_0

    :cond_0
    iput-boolean v4, p0, Lcom/mstar/tv/service/skin/CommonSkin;->isBindOk:Z

    goto :goto_0
.end method

.method public disconnect()V
    .locals 0

    return-void
.end method

.method public enterSleepMode(ZZ)V
    .locals 2
    .param p1    # Z
    .param p2    # Z

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/CommonSkin;->iTvServiceCommon:Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon;

    invoke-interface {v1, p1, p2}, Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon;->enterSleepMode(ZZ)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getGpioDeviceStatus(I)I
    .locals 2
    .param p1    # I

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/CommonSkin;->iTvServiceCommon:Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon;

    invoke-interface {v1, p1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon;->getGpioDeviceStatus(I)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, -0x1

    goto :goto_0
.end method

.method public getHPPortStatus()Z
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/CommonSkin;->iTvServiceCommon:Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon;

    invoke-interface {v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon;->getHPPortStatus()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getOsdLanguage()Lcom/mstar/tv/service/aidl/EN_MEMBER_LANGUAGE;
    .locals 6

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/mstar/tv/service/skin/CommonSkin;->iTvServiceCommon:Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon;

    if-nez v2, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Common service head is null!\t"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/Exception;

    invoke-direct {v4}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v4}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v4

    const/4 v5, 0x0

    aget-object v4, v4, v5

    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-object v1

    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/mstar/tv/service/skin/CommonSkin;->iTvServiceCommon:Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon;

    invoke-interface {v2}, Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon;->getOsdLanguage()Lcom/mstar/tv/service/aidl/EN_MEMBER_LANGUAGE;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getPowerOnSource()Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;
    .locals 6

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/mstar/tv/service/skin/CommonSkin;->iTvServiceCommon:Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon;

    if-nez v2, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Common service head is null!\t"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/Exception;

    invoke-direct {v4}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v4}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v4

    const/4 v5, 0x0

    aget-object v4, v4, v5

    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-object v1

    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/mstar/tv/service/skin/CommonSkin;->iTvServiceCommon:Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon;

    invoke-interface {v2}, Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon;->getPowerOnSource()Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getPresentFollowingEventInfo(IIZI)Lcom/mstar/tv/service/aidl/PresentFollowingEventInfo;
    .locals 2
    .param p1    # I
    .param p2    # I
    .param p3    # Z
    .param p4    # I

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/CommonSkin;->iTvServiceCommon:Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon;

    invoke-interface {v1, p1, p2, p3, p4}, Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon;->getPresentFollowingEventInfo(IIZI)Lcom/mstar/tv/service/aidl/PresentFollowingEventInfo;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getSourceList()Lcom/mstar/tv/service/aidl/IntArrayList;
    .locals 6

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/mstar/tv/service/skin/CommonSkin;->iTvServiceCommon:Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon;

    if-nez v2, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Common service head is null!\t"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/Exception;

    invoke-direct {v4}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v4}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v4

    const/4 v5, 0x0

    aget-object v4, v4, v5

    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-object v1

    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/mstar/tv/service/skin/CommonSkin;->iTvServiceCommon:Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon;

    invoke-interface {v2}, Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon;->getSourceList()Lcom/mstar/tv/service/aidl/IntArrayList;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getVideoInfo()Lcom/mstar/tv/service/aidl/ST_VIDEO_INFO;
    .locals 6

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/mstar/tv/service/skin/CommonSkin;->iTvServiceCommon:Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon;

    if-nez v2, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Common service head is null!\t"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/Exception;

    invoke-direct {v4}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v4}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v4

    const/4 v5, 0x0

    aget-object v4, v4, v5

    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-object v1

    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/mstar/tv/service/skin/CommonSkin;->iTvServiceCommon:Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon;

    invoke-interface {v2}, Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon;->getVideoInfo()Lcom/mstar/tv/service/aidl/ST_VIDEO_INFO;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public isConnectionOk()Z
    .locals 1

    iget-object v0, p0, Lcom/mstar/tv/service/skin/CommonSkin;->iTvServiceCommon:Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isSignalStable()Z
    .locals 5

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/mstar/tv/service/skin/CommonSkin;->iTvServiceCommon:Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon;

    if-nez v2, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Common service head is null!\t"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/Exception;

    invoke-direct {v4}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v4}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v4

    aget-object v4, v4, v1

    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v1

    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/mstar/tv/service/skin/CommonSkin;->iTvServiceCommon:Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon;

    invoke-interface {v2}, Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon;->isSignalStable()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public openSurfaceView(IIII)V
    .locals 2
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/CommonSkin;->iTvServiceCommon:Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon;

    invoke-interface {v1, p1, p2, p3, p4}, Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon;->openSurfaceView(IIII)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public programDown()Z
    .locals 5

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/mstar/tv/service/skin/CommonSkin;->iTvServiceCommon:Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon;

    if-nez v2, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Common service head is null!\t"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/Exception;

    invoke-direct {v4}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v4}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v4

    aget-object v4, v4, v1

    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v1

    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/mstar/tv/service/skin/CommonSkin;->iTvServiceCommon:Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon;

    invoke-interface {v2}, Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon;->programDown()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public programUp()Z
    .locals 5

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/mstar/tv/service/skin/CommonSkin;->iTvServiceCommon:Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon;

    if-nez v2, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Common service head is null!\t"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/Exception;

    invoke-direct {v4}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v4}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v4

    aget-object v4, v4, v1

    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v1

    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/mstar/tv/service/skin/CommonSkin;->iTvServiceCommon:Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon;

    invoke-interface {v2}, Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon;->programUp()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public rebootSystem(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/CommonSkin;->iTvServiceCommon:Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon;

    invoke-interface {v1, p1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon;->rebootSystem(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public setGpioDeviceStatus(IZ)Z
    .locals 2
    .param p1    # I
    .param p2    # Z

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/CommonSkin;->iTvServiceCommon:Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon;

    invoke-interface {v1, p1, p2}, Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon;->setGpioDeviceStatus(IZ)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setHPPortStatus(Z)V
    .locals 2
    .param p1    # Z

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/CommonSkin;->iTvServiceCommon:Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon;

    invoke-interface {v1, p1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon;->setHPPortStatus(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public setOsdLanguage(Lcom/mstar/tv/service/aidl/EN_MEMBER_LANGUAGE;)Z
    .locals 2
    .param p1    # Lcom/mstar/tv/service/aidl/EN_MEMBER_LANGUAGE;

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/CommonSkin;->iTvServiceCommon:Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon;

    invoke-interface {v1, p1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon;->setOsdLanguage(Lcom/mstar/tv/service/aidl/EN_MEMBER_LANGUAGE;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public setPowerOnSource(Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;)Z
    .locals 2
    .param p1    # Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/CommonSkin;->iTvServiceCommon:Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon;

    invoke-interface {v1, p1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon;->setPowerOnSource(Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public setSurfaceView(IIII)V
    .locals 2
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/CommonSkin;->iTvServiceCommon:Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon;

    invoke-interface {v1, p1, p2, p3, p4}, Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon;->setSurfaceView(IIII)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public standbySystem(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/CommonSkin;->iTvServiceCommon:Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon;

    invoke-interface {v1, p1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon;->standbySystem(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public upgrade(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/CommonSkin;->iTvServiceCommon:Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon;

    invoke-interface {v1, p1, p2}, Lcom/mstar/tv/service/interfaces/ITvServiceServerCommon;->upgrade(Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x1

    goto :goto_0
.end method
