.class public Lcom/mstar/tv/service/skin/S3DSkin;
.super Ljava/lang/Object;
.source "S3DSkin.java"


# instance fields
.field private handler:Landroid/os/Handler;

.field private iTvServiceS3D:Lcom/mstar/tv/service/interfaces/ITvServiceServerS3D;

.field private isBindOk:Z

.field private superContext:Landroid/content/Context;

.field protected tvServiceS3DConnection:Landroid/content/ServiceConnection;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mstar/tv/service/skin/S3DSkin;->handler:Landroid/os/Handler;

    new-instance v0, Lcom/mstar/tv/service/skin/S3DSkin$1;

    invoke-direct {v0, p0}, Lcom/mstar/tv/service/skin/S3DSkin$1;-><init>(Lcom/mstar/tv/service/skin/S3DSkin;)V

    iput-object v0, p0, Lcom/mstar/tv/service/skin/S3DSkin;->tvServiceS3DConnection:Landroid/content/ServiceConnection;

    iput-object p1, p0, Lcom/mstar/tv/service/skin/S3DSkin;->superContext:Landroid/content/Context;

    return-void
.end method

.method static synthetic access$0(Lcom/mstar/tv/service/skin/S3DSkin;Lcom/mstar/tv/service/interfaces/ITvServiceServerS3D;)V
    .locals 0

    iput-object p1, p0, Lcom/mstar/tv/service/skin/S3DSkin;->iTvServiceS3D:Lcom/mstar/tv/service/interfaces/ITvServiceServerS3D;

    return-void
.end method

.method static synthetic access$1(Lcom/mstar/tv/service/skin/S3DSkin;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/mstar/tv/service/skin/S3DSkin;->handler:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public connect(Landroid/os/Handler;)Z
    .locals 5
    .param p1    # Landroid/os/Handler;

    const/4 v4, 0x0

    iput-object p1, p0, Lcom/mstar/tv/service/skin/S3DSkin;->handler:Landroid/os/Handler;

    const-string v3, "tv_services"

    invoke-static {v3}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServer$Stub;->asInterface(Landroid/os/IBinder;)Lcom/mstar/tv/service/interfaces/ITvServiceServer;

    move-result-object v2

    if-eqz v2, :cond_0

    :try_start_0
    invoke-interface {v2}, Lcom/mstar/tv/service/interfaces/ITvServiceServer;->getS3DManager()Lcom/mstar/tv/service/interfaces/ITvServiceServerS3D;

    move-result-object v3

    iput-object v3, p0, Lcom/mstar/tv/service/skin/S3DSkin;->iTvServiceS3D:Lcom/mstar/tv/service/interfaces/ITvServiceServerS3D;

    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/mstar/tv/service/skin/S3DSkin;->isBindOk:Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-boolean v3, p0, Lcom/mstar/tv/service/skin/S3DSkin;->isBindOk:Z

    return v3

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    iput-boolean v4, p0, Lcom/mstar/tv/service/skin/S3DSkin;->isBindOk:Z

    goto :goto_0

    :cond_0
    iput-boolean v4, p0, Lcom/mstar/tv/service/skin/S3DSkin;->isBindOk:Z

    goto :goto_0
.end method

.method public disconnect()V
    .locals 0

    return-void
.end method

.method public get3DDepthMode()I
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/S3DSkin;->iTvServiceS3D:Lcom/mstar/tv/service/interfaces/ITvServiceServerS3D;

    invoke-interface {v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerS3D;->get3DDepthMode()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, -0x1

    goto :goto_0
.end method

.method public get3DOffsetMode()I
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/S3DSkin;->iTvServiceS3D:Lcom/mstar/tv/service/interfaces/ITvServiceServerS3D;

    invoke-interface {v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerS3D;->get3DOffsetMode()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, -0x1

    goto :goto_0
.end method

.method public get3DOutputAspectMode()Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_3DOUTPUTASPECT;
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/S3DSkin;->iTvServiceS3D:Lcom/mstar/tv/service/interfaces/ITvServiceServerS3D;

    invoke-interface {v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerS3D;->get3DOutputAspectMode()Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_3DOUTPUTASPECT;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getAutoStartMode()Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_AUTOSTART;
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/S3DSkin;->iTvServiceS3D:Lcom/mstar/tv/service/interfaces/ITvServiceServerS3D;

    invoke-interface {v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerS3D;->getAutoStartMode()Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_AUTOSTART;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getDisplay3DTo2DMode()Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_3DTO2D;
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/S3DSkin;->iTvServiceS3D:Lcom/mstar/tv/service/interfaces/ITvServiceServerS3D;

    invoke-interface {v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerS3D;->getDisplay3DTo2DMode()Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_3DTO2D;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getDisplayFormat()Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_DISPLAYFORMAT;
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/S3DSkin;->iTvServiceS3D:Lcom/mstar/tv/service/interfaces/ITvServiceServerS3D;

    invoke-interface {v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerS3D;->getDisplayFormat()Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_DISPLAYFORMAT;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getLRViewSwitch()Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_LRVIEWSWITCH;
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/S3DSkin;->iTvServiceS3D:Lcom/mstar/tv/service/interfaces/ITvServiceServerS3D;

    invoke-interface {v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerS3D;->getLRViewSwitch()Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_LRVIEWSWITCH;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getSelfAdaptiveDetect()Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_SELFADAPTIVE_DETECT;
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/S3DSkin;->iTvServiceS3D:Lcom/mstar/tv/service/interfaces/ITvServiceServerS3D;

    invoke-interface {v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerS3D;->getSelfAdaptiveDetect()Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_SELFADAPTIVE_DETECT;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getSelfAdaptiveLevel()Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_SELFADAPTIVE_LEVEL;
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/S3DSkin;->iTvServiceS3D:Lcom/mstar/tv/service/interfaces/ITvServiceServerS3D;

    invoke-interface {v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerS3D;->getSelfAdaptiveLevel()Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_SELFADAPTIVE_LEVEL;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isConnectionOk()Z
    .locals 1

    iget-object v0, p0, Lcom/mstar/tv/service/skin/S3DSkin;->iTvServiceS3D:Lcom/mstar/tv/service/interfaces/ITvServiceServerS3D;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public set3DDepthMode(I)Z
    .locals 2
    .param p1    # I

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/S3DSkin;->iTvServiceS3D:Lcom/mstar/tv/service/interfaces/ITvServiceServerS3D;

    invoke-interface {v1, p1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerS3D;->set3DDepthMode(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public set3DOffsetMode(I)Z
    .locals 2
    .param p1    # I

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/S3DSkin;->iTvServiceS3D:Lcom/mstar/tv/service/interfaces/ITvServiceServerS3D;

    invoke-interface {v1, p1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerS3D;->set3DOffsetMode(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public set3DOutputAspectMode(Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_3DOUTPUTASPECT;)Z
    .locals 2
    .param p1    # Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_3DOUTPUTASPECT;

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/S3DSkin;->iTvServiceS3D:Lcom/mstar/tv/service/interfaces/ITvServiceServerS3D;

    invoke-interface {v1, p1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerS3D;->set3DOutputAspectMode(Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_3DOUTPUTASPECT;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public set3DTo2D(Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_3DTO2D;)Z
    .locals 2
    .param p1    # Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_3DTO2D;

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/S3DSkin;->iTvServiceS3D:Lcom/mstar/tv/service/interfaces/ITvServiceServerS3D;

    invoke-interface {v1, p1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerS3D;->set3DTo2D(Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_3DTO2D;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setAutoStartMode(Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_AUTOSTART;)Z
    .locals 2
    .param p1    # Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_AUTOSTART;

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/S3DSkin;->iTvServiceS3D:Lcom/mstar/tv/service/interfaces/ITvServiceServerS3D;

    invoke-interface {v1, p1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerS3D;->setAutoStartMode(Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_AUTOSTART;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setDisplayFormat(Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_DISPLAYFORMAT;)Z
    .locals 2
    .param p1    # Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_DISPLAYFORMAT;

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/S3DSkin;->iTvServiceS3D:Lcom/mstar/tv/service/interfaces/ITvServiceServerS3D;

    invoke-interface {v1, p1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerS3D;->setDisplayFormat(Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_DISPLAYFORMAT;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setDisplayFormatForUI(I)V
    .locals 2
    .param p1    # I

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/S3DSkin;->iTvServiceS3D:Lcom/mstar/tv/service/interfaces/ITvServiceServerS3D;

    invoke-interface {v1, p1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerS3D;->setDisplayFormatForUI(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public setLRViewSwitch(Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_LRVIEWSWITCH;)Z
    .locals 2
    .param p1    # Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_LRVIEWSWITCH;

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/S3DSkin;->iTvServiceS3D:Lcom/mstar/tv/service/interfaces/ITvServiceServerS3D;

    invoke-interface {v1, p1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerS3D;->setLRViewSwitch(Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_LRVIEWSWITCH;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setSelfAdaptiveDetect(Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_SELFADAPTIVE_DETECT;)Z
    .locals 2
    .param p1    # Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_SELFADAPTIVE_DETECT;

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/S3DSkin;->iTvServiceS3D:Lcom/mstar/tv/service/interfaces/ITvServiceServerS3D;

    invoke-interface {v1, p1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerS3D;->setSelfAdaptiveDetect(Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_SELFADAPTIVE_DETECT;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setSelfAdaptiveLevel(Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_SELFADAPTIVE_LEVEL;)Z
    .locals 2
    .param p1    # Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_SELFADAPTIVE_LEVEL;

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/S3DSkin;->iTvServiceS3D:Lcom/mstar/tv/service/interfaces/ITvServiceServerS3D;

    invoke-interface {v1, p1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerS3D;->setSelfAdaptiveLevel(Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_SELFADAPTIVE_LEVEL;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method
