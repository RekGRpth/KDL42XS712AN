.class public Lcom/mstar/tv/service/skin/TimerSkin;
.super Ljava/lang/Object;
.source "TimerSkin.java"


# instance fields
.field private handler:Landroid/os/Handler;

.field private iTvServiceTimer:Lcom/mstar/tv/service/interfaces/ITvServiceServerTimer;

.field private isBindOk:Z

.field private superContext:Landroid/content/Context;

.field protected tvServiceTimerConnection:Landroid/content/ServiceConnection;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mstar/tv/service/skin/TimerSkin;->handler:Landroid/os/Handler;

    new-instance v0, Lcom/mstar/tv/service/skin/TimerSkin$1;

    invoke-direct {v0, p0}, Lcom/mstar/tv/service/skin/TimerSkin$1;-><init>(Lcom/mstar/tv/service/skin/TimerSkin;)V

    iput-object v0, p0, Lcom/mstar/tv/service/skin/TimerSkin;->tvServiceTimerConnection:Landroid/content/ServiceConnection;

    iput-object p1, p0, Lcom/mstar/tv/service/skin/TimerSkin;->superContext:Landroid/content/Context;

    return-void
.end method

.method static synthetic access$0(Lcom/mstar/tv/service/skin/TimerSkin;Lcom/mstar/tv/service/interfaces/ITvServiceServerTimer;)V
    .locals 0

    iput-object p1, p0, Lcom/mstar/tv/service/skin/TimerSkin;->iTvServiceTimer:Lcom/mstar/tv/service/interfaces/ITvServiceServerTimer;

    return-void
.end method

.method static synthetic access$1(Lcom/mstar/tv/service/skin/TimerSkin;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/mstar/tv/service/skin/TimerSkin;->handler:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public connect(Landroid/os/Handler;)Z
    .locals 5
    .param p1    # Landroid/os/Handler;

    const/4 v4, 0x0

    iput-object p1, p0, Lcom/mstar/tv/service/skin/TimerSkin;->handler:Landroid/os/Handler;

    const-string v3, "tv_services"

    invoke-static {v3}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServer$Stub;->asInterface(Landroid/os/IBinder;)Lcom/mstar/tv/service/interfaces/ITvServiceServer;

    move-result-object v2

    if-eqz v2, :cond_0

    :try_start_0
    invoke-interface {v2}, Lcom/mstar/tv/service/interfaces/ITvServiceServer;->getTimerManager()Lcom/mstar/tv/service/interfaces/ITvServiceServerTimer;

    move-result-object v3

    iput-object v3, p0, Lcom/mstar/tv/service/skin/TimerSkin;->iTvServiceTimer:Lcom/mstar/tv/service/interfaces/ITvServiceServerTimer;

    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/mstar/tv/service/skin/TimerSkin;->isBindOk:Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-boolean v3, p0, Lcom/mstar/tv/service/skin/TimerSkin;->isBindOk:Z

    return v3

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    iput-boolean v4, p0, Lcom/mstar/tv/service/skin/TimerSkin;->isBindOk:Z

    goto :goto_0

    :cond_0
    iput-boolean v4, p0, Lcom/mstar/tv/service/skin/TimerSkin;->isBindOk:Z

    goto :goto_0
.end method

.method public disconnect()V
    .locals 0

    return-void
.end method

.method public getCurTimer()Lcom/mstar/tv/service/aidl/ST_Time;
    .locals 6

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/mstar/tv/service/skin/TimerSkin;->iTvServiceTimer:Lcom/mstar/tv/service/interfaces/ITvServiceServerTimer;

    if-nez v2, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Timer service head is null!\t"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/Exception;

    invoke-direct {v4}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v4}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v4

    const/4 v5, 0x0

    aget-object v4, v4, v5

    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-object v1

    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/mstar/tv/service/skin/TimerSkin;->iTvServiceTimer:Lcom/mstar/tv/service/interfaces/ITvServiceServerTimer;

    invoke-interface {v2}, Lcom/mstar/tv/service/interfaces/ITvServiceServerTimer;->getCurTimer()Lcom/mstar/tv/service/aidl/ST_Time;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getOffTimer()Lcom/mstar/tv/service/aidl/ST_Time;
    .locals 6

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/mstar/tv/service/skin/TimerSkin;->iTvServiceTimer:Lcom/mstar/tv/service/interfaces/ITvServiceServerTimer;

    if-nez v2, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Timer service head is null!\t"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/Exception;

    invoke-direct {v4}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v4}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v4

    const/4 v5, 0x0

    aget-object v4, v4, v5

    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-object v1

    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/mstar/tv/service/skin/TimerSkin;->iTvServiceTimer:Lcom/mstar/tv/service/interfaces/ITvServiceServerTimer;

    invoke-interface {v2}, Lcom/mstar/tv/service/interfaces/ITvServiceServerTimer;->getOffTimer()Lcom/mstar/tv/service/aidl/ST_Time;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getOnTimeEvent()Lcom/mstar/tv/service/aidl/ST_OnTime_TVDes;
    .locals 6

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/mstar/tv/service/skin/TimerSkin;->iTvServiceTimer:Lcom/mstar/tv/service/interfaces/ITvServiceServerTimer;

    if-nez v2, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Timer service head is null!\t"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/Exception;

    invoke-direct {v4}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v4}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v4

    const/4 v5, 0x0

    aget-object v4, v4, v5

    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-object v1

    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/mstar/tv/service/skin/TimerSkin;->iTvServiceTimer:Lcom/mstar/tv/service/interfaces/ITvServiceServerTimer;

    invoke-interface {v2}, Lcom/mstar/tv/service/interfaces/ITvServiceServerTimer;->getOnTimeEvent()Lcom/mstar/tv/service/aidl/ST_OnTime_TVDes;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getOnTimer()Lcom/mstar/tv/service/aidl/ST_Time;
    .locals 6

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/mstar/tv/service/skin/TimerSkin;->iTvServiceTimer:Lcom/mstar/tv/service/interfaces/ITvServiceServerTimer;

    if-nez v2, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Timer service head is null!\t"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/Exception;

    invoke-direct {v4}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v4}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v4

    const/4 v5, 0x0

    aget-object v4, v4, v5

    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-object v1

    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/mstar/tv/service/skin/TimerSkin;->iTvServiceTimer:Lcom/mstar/tv/service/interfaces/ITvServiceServerTimer;

    invoke-interface {v2}, Lcom/mstar/tv/service/interfaces/ITvServiceServerTimer;->getOnTimer()Lcom/mstar/tv/service/aidl/ST_Time;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getSleepMode()Lcom/mstar/tv/service/aidl/EN_SLEEP_TIME_STATE;
    .locals 6

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/mstar/tv/service/skin/TimerSkin;->iTvServiceTimer:Lcom/mstar/tv/service/interfaces/ITvServiceServerTimer;

    if-nez v2, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Timer service head is null!\t"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/Exception;

    invoke-direct {v4}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v4}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v4

    const/4 v5, 0x0

    aget-object v4, v4, v5

    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-object v1

    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/mstar/tv/service/skin/TimerSkin;->iTvServiceTimer:Lcom/mstar/tv/service/interfaces/ITvServiceServerTimer;

    invoke-interface {v2}, Lcom/mstar/tv/service/interfaces/ITvServiceServerTimer;->getSleepMode()Lcom/mstar/tv/service/aidl/EN_SLEEP_TIME_STATE;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v2, "timer skin"

    const-string v3, "Unexpected remote exception"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public isConnectionOk()Z
    .locals 1

    iget-object v0, p0, Lcom/mstar/tv/service/skin/TimerSkin;->iTvServiceTimer:Lcom/mstar/tv/service/interfaces/ITvServiceServerTimer;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isOffTimerEnable()Z
    .locals 5

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/mstar/tv/service/skin/TimerSkin;->iTvServiceTimer:Lcom/mstar/tv/service/interfaces/ITvServiceServerTimer;

    if-nez v2, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Timer service head is null!\t"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/Exception;

    invoke-direct {v4}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v4}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v4

    aget-object v4, v4, v1

    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v1

    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/mstar/tv/service/skin/TimerSkin;->iTvServiceTimer:Lcom/mstar/tv/service/interfaces/ITvServiceServerTimer;

    invoke-interface {v2}, Lcom/mstar/tv/service/interfaces/ITvServiceServerTimer;->isOffTimerEnable()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public isOnTimerEnable()Z
    .locals 5

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/mstar/tv/service/skin/TimerSkin;->iTvServiceTimer:Lcom/mstar/tv/service/interfaces/ITvServiceServerTimer;

    if-nez v2, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Timer service head is null!\t"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/Exception;

    invoke-direct {v4}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v4}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v4

    aget-object v4, v4, v1

    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v1

    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/mstar/tv/service/skin/TimerSkin;->iTvServiceTimer:Lcom/mstar/tv/service/interfaces/ITvServiceServerTimer;

    invoke-interface {v2}, Lcom/mstar/tv/service/interfaces/ITvServiceServerTimer;->isOnTimerEnable()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public setOffTimer(Lcom/mstar/tv/service/aidl/ST_Time;)Z
    .locals 5
    .param p1    # Lcom/mstar/tv/service/aidl/ST_Time;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/mstar/tv/service/skin/TimerSkin;->iTvServiceTimer:Lcom/mstar/tv/service/interfaces/ITvServiceServerTimer;

    if-nez v2, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Timer service head is null!\t"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/Exception;

    invoke-direct {v4}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v4}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v4

    aget-object v4, v4, v1

    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v1

    :cond_0
    if-nez p1, :cond_1

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Input pointer is null!\t"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/Exception;

    invoke-direct {v4}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v4}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v4

    aget-object v4, v4, v1

    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, Lcom/mstar/tv/service/aidl/ST_Time;->normalize(Z)J

    :try_start_0
    iget-object v2, p0, Lcom/mstar/tv/service/skin/TimerSkin;->iTvServiceTimer:Lcom/mstar/tv/service/interfaces/ITvServiceServerTimer;

    invoke-interface {v2, p1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerTimer;->setOffTimer(Lcom/mstar/tv/service/aidl/ST_Time;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public setOffTimerEnable(Z)Z
    .locals 5
    .param p1    # Z

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/mstar/tv/service/skin/TimerSkin;->iTvServiceTimer:Lcom/mstar/tv/service/interfaces/ITvServiceServerTimer;

    if-nez v2, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Timer service head is null!\t"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/Exception;

    invoke-direct {v4}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v4}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v4

    aget-object v4, v4, v1

    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v1

    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/mstar/tv/service/skin/TimerSkin;->iTvServiceTimer:Lcom/mstar/tv/service/interfaces/ITvServiceServerTimer;

    invoke-interface {v2, p1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerTimer;->setOffTimerEnable(Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public setOnTimeEvent(Lcom/mstar/tv/service/aidl/ST_OnTime_TVDes;)Z
    .locals 5
    .param p1    # Lcom/mstar/tv/service/aidl/ST_OnTime_TVDes;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/mstar/tv/service/skin/TimerSkin;->iTvServiceTimer:Lcom/mstar/tv/service/interfaces/ITvServiceServerTimer;

    if-nez v2, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Timer service head is null!\t"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/Exception;

    invoke-direct {v4}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v4}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v4

    aget-object v4, v4, v1

    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v1

    :cond_0
    if-nez p1, :cond_1

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Input pointer is null!\t"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/Exception;

    invoke-direct {v4}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v4}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v4

    aget-object v4, v4, v1

    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    iget-object v2, p1, Lcom/mstar/tv/service/aidl/ST_OnTime_TVDes;->enTVSrc:Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

    invoke-virtual {v2}, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;->ordinal()I

    move-result v2

    sget-object v3, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;->EN_Time_OnTimer_Source_NUM:Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;

    invoke-virtual {v3}, Lcom/mstar/tv/service/aidl/EN_TIME_ON_TIME_SOURCE;->ordinal()I

    move-result v3

    if-lt v2, v3, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Input params out of boundary!\t"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/Exception;

    invoke-direct {v4}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v4}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v4

    aget-object v4, v4, v1

    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    :try_start_0
    iget-object v2, p0, Lcom/mstar/tv/service/skin/TimerSkin;->iTvServiceTimer:Lcom/mstar/tv/service/interfaces/ITvServiceServerTimer;

    invoke-interface {v2, p1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerTimer;->setOnTimeEvent(Lcom/mstar/tv/service/aidl/ST_OnTime_TVDes;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public setOnTimer(Lcom/mstar/tv/service/aidl/ST_Time;)Z
    .locals 5
    .param p1    # Lcom/mstar/tv/service/aidl/ST_Time;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/mstar/tv/service/skin/TimerSkin;->iTvServiceTimer:Lcom/mstar/tv/service/interfaces/ITvServiceServerTimer;

    if-nez v2, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Timer service head is null!\t"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/Exception;

    invoke-direct {v4}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v4}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v4

    aget-object v4, v4, v1

    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v1

    :cond_0
    if-nez p1, :cond_1

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Input pointer is null!\t"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/Exception;

    invoke-direct {v4}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v4}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v4

    aget-object v4, v4, v1

    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, Lcom/mstar/tv/service/aidl/ST_Time;->normalize(Z)J

    :try_start_0
    iget-object v2, p0, Lcom/mstar/tv/service/skin/TimerSkin;->iTvServiceTimer:Lcom/mstar/tv/service/interfaces/ITvServiceServerTimer;

    invoke-interface {v2, p1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerTimer;->setOnTimer(Lcom/mstar/tv/service/aidl/ST_Time;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public setOnTimerEnable(Z)Z
    .locals 5
    .param p1    # Z

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/mstar/tv/service/skin/TimerSkin;->iTvServiceTimer:Lcom/mstar/tv/service/interfaces/ITvServiceServerTimer;

    if-nez v2, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Timer service head is null!\t"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/Exception;

    invoke-direct {v4}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v4}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v4

    aget-object v4, v4, v1

    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v1

    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/mstar/tv/service/skin/TimerSkin;->iTvServiceTimer:Lcom/mstar/tv/service/interfaces/ITvServiceServerTimer;

    invoke-interface {v2, p1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerTimer;->setOnTimerEnable(Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public setSleepMode(Lcom/mstar/tv/service/aidl/EN_SLEEP_TIME_STATE;)Z
    .locals 5
    .param p1    # Lcom/mstar/tv/service/aidl/EN_SLEEP_TIME_STATE;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/mstar/tv/service/skin/TimerSkin;->iTvServiceTimer:Lcom/mstar/tv/service/interfaces/ITvServiceServerTimer;

    if-nez v2, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Timer service head is null!\t"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/Exception;

    invoke-direct {v4}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v4}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v4

    aget-object v4, v4, v1

    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v1

    :cond_0
    invoke-virtual {p1}, Lcom/mstar/tv/service/aidl/EN_SLEEP_TIME_STATE;->ordinal()I

    move-result v2

    sget-object v3, Lcom/mstar/tv/service/aidl/EN_SLEEP_TIME_STATE;->STATE_SLEEP_TOTAL:Lcom/mstar/tv/service/aidl/EN_SLEEP_TIME_STATE;

    invoke-virtual {v3}, Lcom/mstar/tv/service/aidl/EN_SLEEP_TIME_STATE;->ordinal()I

    move-result v3

    if-lt v2, v3, :cond_1

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Input params out of boundary!\t"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/Exception;

    invoke-direct {v4}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v4}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v4

    aget-object v4, v4, v1

    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    :try_start_0
    iget-object v2, p0, Lcom/mstar/tv/service/skin/TimerSkin;->iTvServiceTimer:Lcom/mstar/tv/service/interfaces/ITvServiceServerTimer;

    invoke-interface {v2, p1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerTimer;->setSleepMode(Lcom/mstar/tv/service/aidl/EN_SLEEP_TIME_STATE;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method
