.class public Lcom/mstar/tv/service/aidl/ST_DTV_SPECIFIC_PROGINFO;
.super Ljava/lang/Object;
.source "ST_DTV_SPECIFIC_PROGINFO.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mstar/tv/service/aidl/ST_DTV_SPECIFIC_PROGINFO;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public m_bAD:Z

.field public m_bCCService:Z

.field public m_bHD:Z

.field public m_bInterlace:Z

.field public m_bMHEG5Service:Z

.field public m_bSubtitleService:Z

.field public m_bTTXService:Z

.field public m_eServiceType:I

.field public m_eVideoType:I

.field public m_sServiceName:Ljava/lang/String;

.field public m_stAudioInfo:Lcom/mstar/tv/service/aidl/AudioInfo;

.field public m_u16FrameRate:S

.field public m_u16Height:S

.field public m_u16Width:S

.field public m_u32Number:I

.field public m_u8AudioTrackNum:B

.field public m_u8SubtitleNum:B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/mstar/tv/service/aidl/ST_DTV_SPECIFIC_PROGINFO$1;

    invoke-direct {v0}, Lcom/mstar/tv/service/aidl/ST_DTV_SPECIFIC_PROGINFO$1;-><init>()V

    sput-object v0, Lcom/mstar/tv/service/aidl/ST_DTV_SPECIFIC_PROGINFO;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 3
    .param p1    # Landroid/os/Parcel;

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mstar/tv/service/aidl/ST_DTV_SPECIFIC_PROGINFO;->m_sServiceName:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/tv/service/aidl/ST_DTV_SPECIFIC_PROGINFO;->m_u32Number:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/tv/service/aidl/ST_DTV_SPECIFIC_PROGINFO;->m_u16Width:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/tv/service/aidl/ST_DTV_SPECIFIC_PROGINFO;->m_u16Height:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/tv/service/aidl/ST_DTV_SPECIFIC_PROGINFO;->m_u16FrameRate:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/mstar/tv/service/aidl/ST_DTV_SPECIFIC_PROGINFO;->m_bInterlace:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/tv/service/aidl/ST_DTV_SPECIFIC_PROGINFO;->m_eServiceType:I

    sget-object v0, Lcom/mstar/tv/service/aidl/AudioInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/tv/service/aidl/AudioInfo;

    iput-object v0, p0, Lcom/mstar/tv/service/aidl/ST_DTV_SPECIFIC_PROGINFO;->m_stAudioInfo:Lcom/mstar/tv/service/aidl/AudioInfo;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/tv/service/aidl/ST_DTV_SPECIFIC_PROGINFO;->m_eVideoType:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/mstar/tv/service/aidl/ST_DTV_SPECIFIC_PROGINFO;->m_bMHEG5Service:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/mstar/tv/service/aidl/ST_DTV_SPECIFIC_PROGINFO;->m_bSubtitleService:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_3

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lcom/mstar/tv/service/aidl/ST_DTV_SPECIFIC_PROGINFO;->m_bTTXService:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_4

    move v0, v1

    :goto_4
    iput-boolean v0, p0, Lcom/mstar/tv/service/aidl/ST_DTV_SPECIFIC_PROGINFO;->m_bCCService:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_5

    move v0, v1

    :goto_5
    iput-boolean v0, p0, Lcom/mstar/tv/service/aidl/ST_DTV_SPECIFIC_PROGINFO;->m_bHD:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_6

    :goto_6
    iput-boolean v1, p0, Lcom/mstar/tv/service/aidl/ST_DTV_SPECIFIC_PROGINFO;->m_bAD:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    iput-byte v0, p0, Lcom/mstar/tv/service/aidl/ST_DTV_SPECIFIC_PROGINFO;->m_u8AudioTrackNum:B

    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    iput-byte v0, p0, Lcom/mstar/tv/service/aidl/ST_DTV_SPECIFIC_PROGINFO;->m_u8SubtitleNum:B

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_2

    :cond_3
    move v0, v2

    goto :goto_3

    :cond_4
    move v0, v2

    goto :goto_4

    :cond_5
    move v0, v2

    goto :goto_5

    :cond_6
    move v1, v2

    goto :goto_6
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/mstar/tv/service/aidl/ST_DTV_SPECIFIC_PROGINFO;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/mstar/tv/service/aidl/ST_DTV_SPECIFIC_PROGINFO;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/mstar/tv/service/aidl/ST_DTV_SPECIFIC_PROGINFO;->m_sServiceName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget v0, p0, Lcom/mstar/tv/service/aidl/ST_DTV_SPECIFIC_PROGINFO;->m_u32Number:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/tv/service/aidl/ST_DTV_SPECIFIC_PROGINFO;->m_u16Width:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/tv/service/aidl/ST_DTV_SPECIFIC_PROGINFO;->m_u16Height:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/tv/service/aidl/ST_DTV_SPECIFIC_PROGINFO;->m_u16FrameRate:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/mstar/tv/service/aidl/ST_DTV_SPECIFIC_PROGINFO;->m_bInterlace:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/tv/service/aidl/ST_DTV_SPECIFIC_PROGINFO;->m_eServiceType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/mstar/tv/service/aidl/ST_DTV_SPECIFIC_PROGINFO;->m_stAudioInfo:Lcom/mstar/tv/service/aidl/AudioInfo;

    invoke-virtual {v0, p1, p2}, Lcom/mstar/tv/service/aidl/AudioInfo;->writeToParcel(Landroid/os/Parcel;I)V

    iget v0, p0, Lcom/mstar/tv/service/aidl/ST_DTV_SPECIFIC_PROGINFO;->m_eVideoType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/mstar/tv/service/aidl/ST_DTV_SPECIFIC_PROGINFO;->m_bMHEG5Service:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/mstar/tv/service/aidl/ST_DTV_SPECIFIC_PROGINFO;->m_bSubtitleService:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/mstar/tv/service/aidl/ST_DTV_SPECIFIC_PROGINFO;->m_bTTXService:Z

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/mstar/tv/service/aidl/ST_DTV_SPECIFIC_PROGINFO;->m_bCCService:Z

    if-eqz v0, :cond_4

    move v0, v1

    :goto_4
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/mstar/tv/service/aidl/ST_DTV_SPECIFIC_PROGINFO;->m_bHD:Z

    if-eqz v0, :cond_5

    move v0, v1

    :goto_5
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/mstar/tv/service/aidl/ST_DTV_SPECIFIC_PROGINFO;->m_bAD:Z

    if-eqz v0, :cond_6

    :goto_6
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    iget-byte v0, p0, Lcom/mstar/tv/service/aidl/ST_DTV_SPECIFIC_PROGINFO;->m_u8AudioTrackNum:B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    iget-byte v0, p0, Lcom/mstar/tv/service/aidl/ST_DTV_SPECIFIC_PROGINFO;->m_u8SubtitleNum:B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_2

    :cond_3
    move v0, v2

    goto :goto_3

    :cond_4
    move v0, v2

    goto :goto_4

    :cond_5
    move v0, v2

    goto :goto_5

    :cond_6
    move v1, v2

    goto :goto_6
.end method
