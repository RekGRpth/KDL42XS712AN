.class public final enum Lcom/mstar/tv/service/aidl/EnumInfoType;
.super Ljava/lang/Enum;
.source "EnumInfoType.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/tv/service/aidl/EnumInfoType;",
        ">;",
        "Landroid/os/Parcelable;"
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mstar/tv/service/aidl/EnumInfoType;",
            ">;"
        }
    .end annotation
.end field

.field private static final synthetic ENUM$VALUES:[Lcom/mstar/tv/service/aidl/EnumInfoType;

.field public static final enum E_FIRST_TO_SHOW_RF:Lcom/mstar/tv/service/aidl/EnumInfoType;

.field public static final enum E_NEXT_RF:Lcom/mstar/tv/service/aidl/EnumInfoType;

.field public static final enum E_PREV_RF:Lcom/mstar/tv/service/aidl/EnumInfoType;

.field public static final enum E_RF_INFO:Lcom/mstar/tv/service/aidl/EnumInfoType;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/mstar/tv/service/aidl/EnumInfoType;

    const-string v1, "E_FIRST_TO_SHOW_RF"

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EnumInfoType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EnumInfoType;->E_FIRST_TO_SHOW_RF:Lcom/mstar/tv/service/aidl/EnumInfoType;

    new-instance v0, Lcom/mstar/tv/service/aidl/EnumInfoType;

    const-string v1, "E_NEXT_RF"

    invoke-direct {v0, v1, v3}, Lcom/mstar/tv/service/aidl/EnumInfoType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EnumInfoType;->E_NEXT_RF:Lcom/mstar/tv/service/aidl/EnumInfoType;

    new-instance v0, Lcom/mstar/tv/service/aidl/EnumInfoType;

    const-string v1, "E_PREV_RF"

    invoke-direct {v0, v1, v4}, Lcom/mstar/tv/service/aidl/EnumInfoType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EnumInfoType;->E_PREV_RF:Lcom/mstar/tv/service/aidl/EnumInfoType;

    new-instance v0, Lcom/mstar/tv/service/aidl/EnumInfoType;

    const-string v1, "E_RF_INFO"

    invoke-direct {v0, v1, v5}, Lcom/mstar/tv/service/aidl/EnumInfoType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EnumInfoType;->E_RF_INFO:Lcom/mstar/tv/service/aidl/EnumInfoType;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/mstar/tv/service/aidl/EnumInfoType;

    sget-object v1, Lcom/mstar/tv/service/aidl/EnumInfoType;->E_FIRST_TO_SHOW_RF:Lcom/mstar/tv/service/aidl/EnumInfoType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/mstar/tv/service/aidl/EnumInfoType;->E_NEXT_RF:Lcom/mstar/tv/service/aidl/EnumInfoType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mstar/tv/service/aidl/EnumInfoType;->E_PREV_RF:Lcom/mstar/tv/service/aidl/EnumInfoType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/tv/service/aidl/EnumInfoType;->E_RF_INFO:Lcom/mstar/tv/service/aidl/EnumInfoType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/mstar/tv/service/aidl/EnumInfoType;->ENUM$VALUES:[Lcom/mstar/tv/service/aidl/EnumInfoType;

    new-instance v0, Lcom/mstar/tv/service/aidl/EnumInfoType$1;

    invoke-direct {v0}, Lcom/mstar/tv/service/aidl/EnumInfoType$1;-><init>()V

    sput-object v0, Lcom/mstar/tv/service/aidl/EnumInfoType;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/tv/service/aidl/EnumInfoType;
    .locals 1

    const-class v0, Lcom/mstar/tv/service/aidl/EnumInfoType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/tv/service/aidl/EnumInfoType;

    return-object v0
.end method

.method public static values()[Lcom/mstar/tv/service/aidl/EnumInfoType;
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lcom/mstar/tv/service/aidl/EnumInfoType;->ENUM$VALUES:[Lcom/mstar/tv/service/aidl/EnumInfoType;

    array-length v1, v0

    new-array v2, v1, [Lcom/mstar/tv/service/aidl/EnumInfoType;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    invoke-virtual {p0}, Lcom/mstar/tv/service/aidl/EnumInfoType;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
