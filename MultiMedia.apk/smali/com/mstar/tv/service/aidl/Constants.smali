.class public interface abstract Lcom/mstar/tv/service/aidl/Constants;
.super Ljava/lang/Object;
.source "Constants.java"


# static fields
.field public static final AUDIO_CONNECTION_OK:I = 0x7ffffffc

.field public static final CHANNEL_CONNECTION_OK:I = 0x7ffffffa

.field public static final CONNECTION_OK:I = 0x7fffffff

.field public static final CONNECTION_WRONG:I = -0x1

.field public static final PICTURE_CONNECTION_OK:I = 0x7ffffffd

.field public static final PIP_CONNECTION_OK:I = 0x7ffffffb

.field public static final REMOTE_EXEC_ERROR:I = -0x2

.field public static final S3D_CONNECTION_OK:I = 0x7ffffffa

.field public static final TIMER_CONNECTION_OK:I = 0x7ffffffe

.field public static final TIME_EVENT_SHOW_EXECUTE_TO_TARGET:I = 0x3

.field public static final TIME_EVENT_SHOW_OFF_COUNTER_DOWN:I = 0x2

.field public static final TIME_EVENT_SHOW_POWER_OFF:I = 0x4

.field public static final TIME_EVENT_SHOW_TARGET_COUNTER_DOWN:I = 0x1
