.class public final enum Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;
.super Ljava/lang/Enum;
.source "EN_MS_LANGUAGE.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;",
        ">;",
        "Landroid/os/Parcelable;"
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;",
            ">;"
        }
    .end annotation
.end field

.field private static final synthetic ENUM$VALUES:[Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_ABKHAZIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_ACHINESE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_ACOLI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_AD:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_ADANGME:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_AFAR:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_AFRIHILI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_AFRIKAANS:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_AFRO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_AKAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_AKKADIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_ALBANIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_ALEUT:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_ALGONQUIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_ALTAIC:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_AMHARIC:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_APACHE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_ARABIC:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_ARAMAIC:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_ARAPAHO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_ARAUCANIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_ARAWAK:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_ARMENIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_ARTIFICIAL:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_ASSAMESE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_ATHAPASCAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_AUSTRONESIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_AVARIC:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_AVESTAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_AWADHI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_AYMARA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_AZERBAIJANI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_AZTEC:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_BALINESE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_BALTIC:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_BALUCHI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_BAMBARA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_BAMILEKE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_BANDA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_BANTU:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_BASA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_BASHKIR:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_BASQUE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_BEJA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_BEMBA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_BENGALI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_BERBER:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_BHOJPURI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_BIHARI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_BIKOL:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_BINI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_BISLAMA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_BRAJ:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_BRETON:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_BUGINESE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_BULGARIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_BURIAT:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_BURMESE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_BYELORUSSIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_CADDO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_CANTONESE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_CARIB:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_CATALAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_CAUCASIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_CEBUANO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_CELTIC:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_CENTRALAMERICANINDIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_CHAGATAI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_CHAMORRO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_CHECHEN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_CHEROKEE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_CHEYENNE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_CHIBCHA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_CHINESE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_CHINOOKJARGON:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_CHOCTAW:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_CHURCHSLAVIC:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_CHUVASH:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_COPTIC:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_CORNISH:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_CORSICAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_CREE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_CREEK:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_CREOLESANDPIDGINS:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_CREOLESANDPIDGINSENGLISH:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_CREOLESANDPIDGINSFRENCH:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_CREOLESANDPIDGINSPORTUGUESE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_CROATIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_CUSHITIC:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_CZECH:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_DAKOTA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_DANISH:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_DELAWARE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_DINKA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_DIVEHI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_DOGRI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_DRAVIDIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_DUALA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_DUTCH:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_DUTCHMIDDLE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_DYULA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_DZONGKHA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_EFIK:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_EGYPTIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_EKAJUK:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_ELAMITE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_ENGLISH:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_ENGLISHMIDDLE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_ENGLISHOLD:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_ESKIMO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_ESPERANTO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_ESTONIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_EWE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_EWONDO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_FANG:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_FANTI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_FAROESE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_FIJIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_FINNISH:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_FINNOUGRIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_FON:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_FRENCH:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_FRENCHMIDDLE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_FRENCHOLD:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_FRISIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_FULAH:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_GA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_GAELIC:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_GALLEGAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_GANDA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_GAYO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_GEEZ:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_GEORGIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_GERMAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_GERMANIC:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_GERMANMIDDLEHIGH:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_GERMANOLDHIGH:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_GILBERTESE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_GONDI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_GOTHIC:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_GREBO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_GREEK:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_GREEKANCIENT:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_GREENLANDIC:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_GUARANI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_GUJARATI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_HAIDA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_HAUSA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_HAWAIIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_HEBREW:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_HERERO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_HILIGAYNON:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_HIMACHALI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_HINDI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_HIRIMOTU:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_HUNGARIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_HUPA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_IBAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_ICELANDIC:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_IGBO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_IJO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_ILOKO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_INDIC:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_INDOEUROPEAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_INDONESIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_INTERLINGUA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_INTERLINGUE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_INUKTITUT:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_INUPIAK:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_IRANIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_IRISH:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_IRISHMIDDLE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_IRISHOLD:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_IROQUOIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_ITALIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_JAPANESE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_JAVANESE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_JUDEOARABIC:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_JUDEOPERSIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_KABYLE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_KACHIN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_KAMBA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_KANNADA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_KANURI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_KARAKALPAK:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_KAREN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_KASHMIRI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_KAWI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_KAZAKH:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_KHASI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_KHMER:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_KHOISAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_KHOTANESE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_KIKUYU:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_KINYARWANDA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_KIRGHIZ:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_KOMI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_KONGO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_KONKANI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_KOREAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_KPELLE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_KRU:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_KUANYAMA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_KUMYK:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_KURDISH:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_KURUKH:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_KUSAIE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_KUTENAI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_LADINO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_LAHNDA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_LAMBA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_LANGUE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_LAO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_LATIN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_LATVIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_LETZEBURGESCH:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_LEZGHIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_LINGALA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_LITHUANIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_LOZI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_LUBAKATANGA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_LUISENO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_LUNDA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_LUO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_MACEDONIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_MADURESE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_MAGAHI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_MAITHILI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_MAKASAR:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_MALAGASY:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_MALAY:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_MALAYALAM:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_MALTESE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_MANDARIN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_MANDINGO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_MANIPURI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_MANOBO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_MANX:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_MAORI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_MARATHI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_MARI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_MARSHALL:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_MARWARI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_MASAI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_MAX:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_MAYAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_MENDE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_MICMAC:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_MINANGKABAU:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_MISCELLANEOUS:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_MOHAWK:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_MOLDAVIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_MONGO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_MONGOLIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_MONKMER:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_MOSSI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_MULTIPLE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_MUNDA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_NAURU:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_NAVAJO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_NDEBELENORTH:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_NDEBELESOUTH:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_NDONGO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_NEPALI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_NETHERLANDS:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_NEWARI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_NIGERKORDOFANIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_NILOSAHARAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_NIUEAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_NONE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_NORSEOLD:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_NORTHAMERICANINDIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_NORWEGIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_NORWEGIANNYNORSK:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_NUBIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_NUM:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_NYAMWEZI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_NYANJA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_NYANKOLE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_NYORO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_NZIMA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_OJIBWA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_ORIYA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_OROMO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_OSAGE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_OSSETIC:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_OTOMIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_PAHLAVI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_PALAUAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_PALI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_PAMPANGA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_PANGASINAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_PANJABI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_PAPIAMENTO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_PAPUANAUSTRALIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_PERSIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_PERSIANOLD:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_PHOENICIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_POLISH:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_PONAPE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_PORTUGUESE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_PRAKRIT:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_PROVENCALOLD:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_PUSHTO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_QAA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_QUECHUA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_RAJASTHANI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_RAROTONGAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_RHAETOROMANCE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_ROMANCE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_ROMANIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_ROMANY:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_RUNDI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_RUSSIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_SALISHAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_SAMARITANARAMAIC:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_SAMI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_SAMOAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_SANDAWE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_SANGO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_SANSKRIT:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_SARDINIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_SCOTS:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_SELKUP:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_SEMITIC:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_SERBIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_SERBOCROATIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_SERER:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_SHAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_SHONA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_SIDAMO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_SIKSIKA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_SINDHI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_SINGHALESE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_SINOTIBETAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_SIOUAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_SISWANT:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_SLAVIC:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_SLOVAK:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_SLOVENIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_SOGDIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_SOMALI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_SONGHAI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_SORBIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_SOTHONORTHERN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_SOTHOSOUTHERN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_SOUTHAMERICANINDIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_SPANISH:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_SUDANESE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_SUKUMA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_SUMERIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_SUSU:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_SWAHILI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_SWAZI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_SWEDISH:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_SWIZE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_SYRIAC:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_TAGALOG:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_TAHITIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_TAJIK:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_TAMASHEK:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_TAMIL:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_TATAR:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_TELETEXT:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_TELUGU:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_TERENO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_THAI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_TIBETAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_TIGRE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_TIGRINYA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_TIMNE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_TIVI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_TLINGIT:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_TONGAISLANDS:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_TONGANYASA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_TRUK:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_TSIMSHIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_TSONGA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_TSWANA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_TUMBUKA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_TURKISH:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_TURKISHOTTOMAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_TURKMEN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_TUVINIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_TWI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_UGARITIC:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_UIGHUR:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_UKRAINIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_UMBUNDU:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_UNDETERMINED:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_UNKNOWN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_URDU:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_UZBEK:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_VAI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_VALENCIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_VENDA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_VIETNAMESE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_VOLAPUK:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_VOTIC:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_WAKASHAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_WALAMO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_WARAY:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_WASHO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_WELSH:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_WOLOF:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_XHOSA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_YAKUT:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_YAO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_YAP:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_YIDDISH:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_YORUBA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_ZAPOTEC:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_ZENAGA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_ZHUANG:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_ZULU:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

.field public static final enum E_ZUNI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_CZECH"

    invoke-direct {v0, v1, v3}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_CZECH:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_DANISH"

    invoke-direct {v0, v1, v4}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_DANISH:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_GERMAN"

    invoke-direct {v0, v1, v5}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_GERMAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_ENGLISH"

    invoke-direct {v0, v1, v6}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_ENGLISH:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_SPANISH"

    invoke-direct {v0, v1, v7}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_SPANISH:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_GREEK"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_GREEK:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_FRENCH"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_FRENCH:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_CROATIAN"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_CROATIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_ITALIAN"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_ITALIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_HUNGARIAN"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_HUNGARIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_DUTCH"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_DUTCH:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_NORWEGIAN"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_NORWEGIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_POLISH"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_POLISH:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_PORTUGUESE"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_PORTUGUESE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_RUSSIAN"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_RUSSIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_ROMANIAN"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_ROMANIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_SLOVENIAN"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_SLOVENIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_SERBIAN"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_SERBIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_FINNISH"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_FINNISH:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_SWEDISH"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_SWEDISH:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_BULGARIAN"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_BULGARIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_SLOVAK"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_SLOVAK:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_CHINESE"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_CHINESE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_GAELIC"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_GAELIC:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_WELSH"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_WELSH:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_ARABIC"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_ARABIC:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_IRISH"

    const/16 v2, 0x1a

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_IRISH:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_LATVIAN"

    const/16 v2, 0x1b

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_LATVIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_HEBREW"

    const/16 v2, 0x1c

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_HEBREW:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_TURKISH"

    const/16 v2, 0x1d

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_TURKISH:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_ESTONIAN"

    const/16 v2, 0x1e

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_ESTONIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_NETHERLANDS"

    const/16 v2, 0x1f

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_NETHERLANDS:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_KOREAN"

    const/16 v2, 0x20

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_KOREAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_HINDI"

    const/16 v2, 0x21

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_HINDI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_MANDARIN"

    const/16 v2, 0x22

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_MANDARIN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_CANTONESE"

    const/16 v2, 0x23

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_CANTONESE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_MAORI"

    const/16 v2, 0x24

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_MAORI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_QAA"

    const/16 v2, 0x25

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_QAA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_UNDETERMINED"

    const/16 v2, 0x26

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_UNDETERMINED:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_AD"

    const/16 v2, 0x27

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_AD:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_UNKNOWN"

    const/16 v2, 0x28

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_UNKNOWN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_NONE"

    const/16 v2, 0x29

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_NONE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_ABKHAZIAN"

    const/16 v2, 0x2a

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_ABKHAZIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_ACHINESE"

    const/16 v2, 0x2b

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_ACHINESE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_ACOLI"

    const/16 v2, 0x2c

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_ACOLI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_ADANGME"

    const/16 v2, 0x2d

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_ADANGME:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_AFAR"

    const/16 v2, 0x2e

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_AFAR:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_AFRIHILI"

    const/16 v2, 0x2f

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_AFRIHILI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_AFRIKAANS"

    const/16 v2, 0x30

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_AFRIKAANS:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_AFRO"

    const/16 v2, 0x31

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_AFRO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_AKAN"

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_AKAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_AKKADIAN"

    const/16 v2, 0x33

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_AKKADIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_ALBANIAN"

    const/16 v2, 0x34

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_ALBANIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_ALEUT"

    const/16 v2, 0x35

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_ALEUT:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_ALGONQUIAN"

    const/16 v2, 0x36

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_ALGONQUIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_ALTAIC"

    const/16 v2, 0x37

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_ALTAIC:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_AMHARIC"

    const/16 v2, 0x38

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_AMHARIC:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_APACHE"

    const/16 v2, 0x39

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_APACHE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_ARAMAIC"

    const/16 v2, 0x3a

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_ARAMAIC:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_ARAPAHO"

    const/16 v2, 0x3b

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_ARAPAHO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_ARAUCANIAN"

    const/16 v2, 0x3c

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_ARAUCANIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_ARAWAK"

    const/16 v2, 0x3d

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_ARAWAK:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_ARMENIAN"

    const/16 v2, 0x3e

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_ARMENIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_ARTIFICIAL"

    const/16 v2, 0x3f

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_ARTIFICIAL:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_ASSAMESE"

    const/16 v2, 0x40

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_ASSAMESE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_ATHAPASCAN"

    const/16 v2, 0x41

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_ATHAPASCAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_AUSTRONESIAN"

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_AUSTRONESIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_AVARIC"

    const/16 v2, 0x43

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_AVARIC:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_AVESTAN"

    const/16 v2, 0x44

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_AVESTAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_AWADHI"

    const/16 v2, 0x45

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_AWADHI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_AYMARA"

    const/16 v2, 0x46

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_AYMARA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_AZERBAIJANI"

    const/16 v2, 0x47

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_AZERBAIJANI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_AZTEC"

    const/16 v2, 0x48

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_AZTEC:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_BALINESE"

    const/16 v2, 0x49

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_BALINESE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_BALTIC"

    const/16 v2, 0x4a

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_BALTIC:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_BALUCHI"

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_BALUCHI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_BAMBARA"

    const/16 v2, 0x4c

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_BAMBARA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_BAMILEKE"

    const/16 v2, 0x4d

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_BAMILEKE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_BANDA"

    const/16 v2, 0x4e

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_BANDA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_BANTU"

    const/16 v2, 0x4f

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_BANTU:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_BASA"

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_BASA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_BASHKIR"

    const/16 v2, 0x51

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_BASHKIR:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_BASQUE"

    const/16 v2, 0x52

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_BASQUE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_BEJA"

    const/16 v2, 0x53

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_BEJA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_BEMBA"

    const/16 v2, 0x54

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_BEMBA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_BENGALI"

    const/16 v2, 0x55

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_BENGALI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_BERBER"

    const/16 v2, 0x56

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_BERBER:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_BHOJPURI"

    const/16 v2, 0x57

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_BHOJPURI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_BIHARI"

    const/16 v2, 0x58

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_BIHARI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_BIKOL"

    const/16 v2, 0x59

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_BIKOL:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_BINI"

    const/16 v2, 0x5a

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_BINI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_BISLAMA"

    const/16 v2, 0x5b

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_BISLAMA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_BRAJ"

    const/16 v2, 0x5c

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_BRAJ:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_BRETON"

    const/16 v2, 0x5d

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_BRETON:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_BUGINESE"

    const/16 v2, 0x5e

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_BUGINESE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_BURIAT"

    const/16 v2, 0x5f

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_BURIAT:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_BURMESE"

    const/16 v2, 0x60

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_BURMESE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_BYELORUSSIAN"

    const/16 v2, 0x61

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_BYELORUSSIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_CADDO"

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_CADDO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_CARIB"

    const/16 v2, 0x63

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_CARIB:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_CATALAN"

    const/16 v2, 0x64

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_CATALAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_CAUCASIAN"

    const/16 v2, 0x65

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_CAUCASIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_CEBUANO"

    const/16 v2, 0x66

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_CEBUANO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_CELTIC"

    const/16 v2, 0x67

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_CELTIC:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_CENTRALAMERICANINDIAN"

    const/16 v2, 0x68

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_CENTRALAMERICANINDIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_CHAGATAI"

    const/16 v2, 0x69

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_CHAGATAI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_CHAMORRO"

    const/16 v2, 0x6a

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_CHAMORRO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_CHECHEN"

    const/16 v2, 0x6b

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_CHECHEN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_CHEROKEE"

    const/16 v2, 0x6c

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_CHEROKEE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_CHEYENNE"

    const/16 v2, 0x6d

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_CHEYENNE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_CHIBCHA"

    const/16 v2, 0x6e

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_CHIBCHA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_CHINOOKJARGON"

    const/16 v2, 0x6f

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_CHINOOKJARGON:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_CHOCTAW"

    const/16 v2, 0x70

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_CHOCTAW:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_CHURCHSLAVIC"

    const/16 v2, 0x71

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_CHURCHSLAVIC:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_CHUVASH"

    const/16 v2, 0x72

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_CHUVASH:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_COPTIC"

    const/16 v2, 0x73

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_COPTIC:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_CORNISH"

    const/16 v2, 0x74

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_CORNISH:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_CORSICAN"

    const/16 v2, 0x75

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_CORSICAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_CREE"

    const/16 v2, 0x76

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_CREE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_CREEK"

    const/16 v2, 0x77

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_CREEK:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_CREOLESANDPIDGINS"

    const/16 v2, 0x78

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_CREOLESANDPIDGINS:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_CREOLESANDPIDGINSENGLISH"

    const/16 v2, 0x79

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_CREOLESANDPIDGINSENGLISH:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_CREOLESANDPIDGINSFRENCH"

    const/16 v2, 0x7a

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_CREOLESANDPIDGINSFRENCH:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_CREOLESANDPIDGINSPORTUGUESE"

    const/16 v2, 0x7b

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_CREOLESANDPIDGINSPORTUGUESE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_CUSHITIC"

    const/16 v2, 0x7c

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_CUSHITIC:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_DAKOTA"

    const/16 v2, 0x7d

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_DAKOTA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_DELAWARE"

    const/16 v2, 0x7e

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_DELAWARE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_DINKA"

    const/16 v2, 0x7f

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_DINKA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_DIVEHI"

    const/16 v2, 0x80

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_DIVEHI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_DOGRI"

    const/16 v2, 0x81

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_DOGRI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_DRAVIDIAN"

    const/16 v2, 0x82

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_DRAVIDIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_DUALA"

    const/16 v2, 0x83

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_DUALA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_DUTCHMIDDLE"

    const/16 v2, 0x84

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_DUTCHMIDDLE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_DYULA"

    const/16 v2, 0x85

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_DYULA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_DZONGKHA"

    const/16 v2, 0x86

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_DZONGKHA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_EFIK"

    const/16 v2, 0x87

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_EFIK:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_EGYPTIAN"

    const/16 v2, 0x88

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_EGYPTIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_EKAJUK"

    const/16 v2, 0x89

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_EKAJUK:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_ELAMITE"

    const/16 v2, 0x8a

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_ELAMITE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_ENGLISHMIDDLE"

    const/16 v2, 0x8b

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_ENGLISHMIDDLE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_ENGLISHOLD"

    const/16 v2, 0x8c

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_ENGLISHOLD:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_ESKIMO"

    const/16 v2, 0x8d

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_ESKIMO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_ESPERANTO"

    const/16 v2, 0x8e

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_ESPERANTO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_EWE"

    const/16 v2, 0x8f

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_EWE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_EWONDO"

    const/16 v2, 0x90

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_EWONDO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_FANG"

    const/16 v2, 0x91

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_FANG:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_FANTI"

    const/16 v2, 0x92

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_FANTI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_FAROESE"

    const/16 v2, 0x93

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_FAROESE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_FIJIAN"

    const/16 v2, 0x94

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_FIJIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_FINNOUGRIAN"

    const/16 v2, 0x95

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_FINNOUGRIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_FON"

    const/16 v2, 0x96

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_FON:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_FRENCHMIDDLE"

    const/16 v2, 0x97

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_FRENCHMIDDLE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_FRENCHOLD"

    const/16 v2, 0x98

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_FRENCHOLD:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_FRISIAN"

    const/16 v2, 0x99

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_FRISIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_FULAH"

    const/16 v2, 0x9a

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_FULAH:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_GA"

    const/16 v2, 0x9b

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_GA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_GALLEGAN"

    const/16 v2, 0x9c

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_GALLEGAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_GANDA"

    const/16 v2, 0x9d

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_GANDA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_GAYO"

    const/16 v2, 0x9e

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_GAYO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_GEEZ"

    const/16 v2, 0x9f

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_GEEZ:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_GEORGIAN"

    const/16 v2, 0xa0

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_GEORGIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_GERMANMIDDLEHIGH"

    const/16 v2, 0xa1

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_GERMANMIDDLEHIGH:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_GERMANOLDHIGH"

    const/16 v2, 0xa2

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_GERMANOLDHIGH:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_GERMANIC"

    const/16 v2, 0xa3

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_GERMANIC:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_GILBERTESE"

    const/16 v2, 0xa4

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_GILBERTESE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_GONDI"

    const/16 v2, 0xa5

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_GONDI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_GOTHIC"

    const/16 v2, 0xa6

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_GOTHIC:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_GREBO"

    const/16 v2, 0xa7

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_GREBO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_GREEKANCIENT"

    const/16 v2, 0xa8

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_GREEKANCIENT:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_GREENLANDIC"

    const/16 v2, 0xa9

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_GREENLANDIC:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_GUARANI"

    const/16 v2, 0xaa

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_GUARANI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_GUJARATI"

    const/16 v2, 0xab

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_GUJARATI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_HAIDA"

    const/16 v2, 0xac

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_HAIDA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_HAUSA"

    const/16 v2, 0xad

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_HAUSA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_HAWAIIAN"

    const/16 v2, 0xae

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_HAWAIIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_HERERO"

    const/16 v2, 0xaf

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_HERERO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_HILIGAYNON"

    const/16 v2, 0xb0

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_HILIGAYNON:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_HIMACHALI"

    const/16 v2, 0xb1

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_HIMACHALI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_HIRIMOTU"

    const/16 v2, 0xb2

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_HIRIMOTU:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_HUPA"

    const/16 v2, 0xb3

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_HUPA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_IBAN"

    const/16 v2, 0xb4

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_IBAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_ICELANDIC"

    const/16 v2, 0xb5

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_ICELANDIC:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_IGBO"

    const/16 v2, 0xb6

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_IGBO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_IJO"

    const/16 v2, 0xb7

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_IJO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_ILOKO"

    const/16 v2, 0xb8

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_ILOKO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_INDIC"

    const/16 v2, 0xb9

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_INDIC:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_INDOEUROPEAN"

    const/16 v2, 0xba

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_INDOEUROPEAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_INDONESIAN"

    const/16 v2, 0xbb

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_INDONESIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_INTERLINGUA"

    const/16 v2, 0xbc

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_INTERLINGUA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_INTERLINGUE"

    const/16 v2, 0xbd

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_INTERLINGUE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_INUKTITUT"

    const/16 v2, 0xbe

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_INUKTITUT:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_INUPIAK"

    const/16 v2, 0xbf

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_INUPIAK:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_IRANIAN"

    const/16 v2, 0xc0

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_IRANIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_IRISHOLD"

    const/16 v2, 0xc1

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_IRISHOLD:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_IRISHMIDDLE"

    const/16 v2, 0xc2

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_IRISHMIDDLE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_IROQUOIAN"

    const/16 v2, 0xc3

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_IROQUOIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_JAPANESE"

    const/16 v2, 0xc4

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_JAPANESE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_JAVANESE"

    const/16 v2, 0xc5

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_JAVANESE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_JUDEOARABIC"

    const/16 v2, 0xc6

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_JUDEOARABIC:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_JUDEOPERSIAN"

    const/16 v2, 0xc7

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_JUDEOPERSIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_KABYLE"

    const/16 v2, 0xc8

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_KABYLE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_KACHIN"

    const/16 v2, 0xc9

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_KACHIN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_KAMBA"

    const/16 v2, 0xca

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_KAMBA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_KANNADA"

    const/16 v2, 0xcb

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_KANNADA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_KANURI"

    const/16 v2, 0xcc

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_KANURI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_KARAKALPAK"

    const/16 v2, 0xcd

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_KARAKALPAK:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_KAREN"

    const/16 v2, 0xce

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_KAREN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_KASHMIRI"

    const/16 v2, 0xcf

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_KASHMIRI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_KAWI"

    const/16 v2, 0xd0

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_KAWI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_KAZAKH"

    const/16 v2, 0xd1

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_KAZAKH:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_KHASI"

    const/16 v2, 0xd2

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_KHASI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_KHMER"

    const/16 v2, 0xd3

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_KHMER:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_KHOISAN"

    const/16 v2, 0xd4

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_KHOISAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_KHOTANESE"

    const/16 v2, 0xd5

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_KHOTANESE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_KIKUYU"

    const/16 v2, 0xd6

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_KIKUYU:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_KINYARWANDA"

    const/16 v2, 0xd7

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_KINYARWANDA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_KIRGHIZ"

    const/16 v2, 0xd8

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_KIRGHIZ:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_KOMI"

    const/16 v2, 0xd9

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_KOMI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_KONGO"

    const/16 v2, 0xda

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_KONGO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_KONKANI"

    const/16 v2, 0xdb

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_KONKANI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_KPELLE"

    const/16 v2, 0xdc

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_KPELLE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_KRU"

    const/16 v2, 0xdd

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_KRU:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_KUANYAMA"

    const/16 v2, 0xde

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_KUANYAMA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_KUMYK"

    const/16 v2, 0xdf

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_KUMYK:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_KURDISH"

    const/16 v2, 0xe0

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_KURDISH:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_KURUKH"

    const/16 v2, 0xe1

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_KURUKH:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_KUSAIE"

    const/16 v2, 0xe2

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_KUSAIE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_KUTENAI"

    const/16 v2, 0xe3

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_KUTENAI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_LADINO"

    const/16 v2, 0xe4

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_LADINO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_LAHNDA"

    const/16 v2, 0xe5

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_LAHNDA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_LAMBA"

    const/16 v2, 0xe6

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_LAMBA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_LANGUE"

    const/16 v2, 0xe7

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_LANGUE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_LAO"

    const/16 v2, 0xe8

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_LAO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_LATIN"

    const/16 v2, 0xe9

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_LATIN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_LETZEBURGESCH"

    const/16 v2, 0xea

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_LETZEBURGESCH:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_LEZGHIAN"

    const/16 v2, 0xeb

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_LEZGHIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_LINGALA"

    const/16 v2, 0xec

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_LINGALA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_LITHUANIAN"

    const/16 v2, 0xed

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_LITHUANIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_LOZI"

    const/16 v2, 0xee

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_LOZI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_LUBAKATANGA"

    const/16 v2, 0xef

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_LUBAKATANGA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_LUISENO"

    const/16 v2, 0xf0

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_LUISENO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_LUNDA"

    const/16 v2, 0xf1

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_LUNDA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_LUO"

    const/16 v2, 0xf2

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_LUO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_MACEDONIAN"

    const/16 v2, 0xf3

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_MACEDONIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_MADURESE"

    const/16 v2, 0xf4

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_MADURESE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_MAGAHI"

    const/16 v2, 0xf5

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_MAGAHI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_MAITHILI"

    const/16 v2, 0xf6

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_MAITHILI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_MAKASAR"

    const/16 v2, 0xf7

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_MAKASAR:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_MALAGASY"

    const/16 v2, 0xf8

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_MALAGASY:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_MALAY"

    const/16 v2, 0xf9

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_MALAY:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_MALAYALAM"

    const/16 v2, 0xfa

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_MALAYALAM:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_MALTESE"

    const/16 v2, 0xfb

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_MALTESE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_MANDINGO"

    const/16 v2, 0xfc

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_MANDINGO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_MANIPURI"

    const/16 v2, 0xfd

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_MANIPURI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_MANOBO"

    const/16 v2, 0xfe

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_MANOBO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_MANX"

    const/16 v2, 0xff

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_MANX:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_MARATHI"

    const/16 v2, 0x100

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_MARATHI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_MARI"

    const/16 v2, 0x101

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_MARI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_MARSHALL"

    const/16 v2, 0x102

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_MARSHALL:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_MARWARI"

    const/16 v2, 0x103

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_MARWARI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_MASAI"

    const/16 v2, 0x104

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_MASAI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_MAYAN"

    const/16 v2, 0x105

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_MAYAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_MENDE"

    const/16 v2, 0x106

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_MENDE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_MICMAC"

    const/16 v2, 0x107

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_MICMAC:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_MINANGKABAU"

    const/16 v2, 0x108

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_MINANGKABAU:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_MISCELLANEOUS"

    const/16 v2, 0x109

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_MISCELLANEOUS:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_MOHAWK"

    const/16 v2, 0x10a

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_MOHAWK:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_MOLDAVIAN"

    const/16 v2, 0x10b

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_MOLDAVIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_MONKMER"

    const/16 v2, 0x10c

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_MONKMER:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_MONGO"

    const/16 v2, 0x10d

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_MONGO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_MONGOLIAN"

    const/16 v2, 0x10e

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_MONGOLIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_MOSSI"

    const/16 v2, 0x10f

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_MOSSI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_MULTIPLE"

    const/16 v2, 0x110

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_MULTIPLE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_MUNDA"

    const/16 v2, 0x111

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_MUNDA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_NAURU"

    const/16 v2, 0x112

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_NAURU:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_NAVAJO"

    const/16 v2, 0x113

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_NAVAJO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_NDEBELENORTH"

    const/16 v2, 0x114

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_NDEBELENORTH:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_NDEBELESOUTH"

    const/16 v2, 0x115

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_NDEBELESOUTH:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_NDONGO"

    const/16 v2, 0x116

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_NDONGO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_NEPALI"

    const/16 v2, 0x117

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_NEPALI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_NEWARI"

    const/16 v2, 0x118

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_NEWARI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_NIGERKORDOFANIAN"

    const/16 v2, 0x119

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_NIGERKORDOFANIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_NILOSAHARAN"

    const/16 v2, 0x11a

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_NILOSAHARAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_NIUEAN"

    const/16 v2, 0x11b

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_NIUEAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_NORSEOLD"

    const/16 v2, 0x11c

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_NORSEOLD:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_NORTHAMERICANINDIAN"

    const/16 v2, 0x11d

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_NORTHAMERICANINDIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_NORWEGIANNYNORSK"

    const/16 v2, 0x11e

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_NORWEGIANNYNORSK:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_NUBIAN"

    const/16 v2, 0x11f

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_NUBIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_NYAMWEZI"

    const/16 v2, 0x120

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_NYAMWEZI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_NYANJA"

    const/16 v2, 0x121

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_NYANJA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_NYANKOLE"

    const/16 v2, 0x122

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_NYANKOLE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_NYORO"

    const/16 v2, 0x123

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_NYORO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_NZIMA"

    const/16 v2, 0x124

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_NZIMA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_OJIBWA"

    const/16 v2, 0x125

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_OJIBWA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_ORIYA"

    const/16 v2, 0x126

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_ORIYA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_OROMO"

    const/16 v2, 0x127

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_OROMO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_OSAGE"

    const/16 v2, 0x128

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_OSAGE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_OSSETIC"

    const/16 v2, 0x129

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_OSSETIC:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_OTOMIAN"

    const/16 v2, 0x12a

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_OTOMIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_PAHLAVI"

    const/16 v2, 0x12b

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_PAHLAVI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_PALAUAN"

    const/16 v2, 0x12c

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_PALAUAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_PALI"

    const/16 v2, 0x12d

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_PALI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_PAMPANGA"

    const/16 v2, 0x12e

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_PAMPANGA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_PANGASINAN"

    const/16 v2, 0x12f

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_PANGASINAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_PANJABI"

    const/16 v2, 0x130

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_PANJABI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_PAPIAMENTO"

    const/16 v2, 0x131

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_PAPIAMENTO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_PAPUANAUSTRALIAN"

    const/16 v2, 0x132

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_PAPUANAUSTRALIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_PERSIAN"

    const/16 v2, 0x133

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_PERSIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_PERSIANOLD"

    const/16 v2, 0x134

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_PERSIANOLD:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_PHOENICIAN"

    const/16 v2, 0x135

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_PHOENICIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_PONAPE"

    const/16 v2, 0x136

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_PONAPE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_PRAKRIT"

    const/16 v2, 0x137

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_PRAKRIT:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_PROVENCALOLD"

    const/16 v2, 0x138

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_PROVENCALOLD:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_PUSHTO"

    const/16 v2, 0x139

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_PUSHTO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_QUECHUA"

    const/16 v2, 0x13a

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_QUECHUA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_RHAETOROMANCE"

    const/16 v2, 0x13b

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_RHAETOROMANCE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_RAJASTHANI"

    const/16 v2, 0x13c

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_RAJASTHANI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_RAROTONGAN"

    const/16 v2, 0x13d

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_RAROTONGAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_ROMANCE"

    const/16 v2, 0x13e

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_ROMANCE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_ROMANY"

    const/16 v2, 0x13f

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_ROMANY:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_RUNDI"

    const/16 v2, 0x140

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_RUNDI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_SALISHAN"

    const/16 v2, 0x141

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_SALISHAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_SAMARITANARAMAIC"

    const/16 v2, 0x142

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_SAMARITANARAMAIC:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_SAMI"

    const/16 v2, 0x143

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_SAMI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_SAMOAN"

    const/16 v2, 0x144

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_SAMOAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_SANDAWE"

    const/16 v2, 0x145

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_SANDAWE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_SANGO"

    const/16 v2, 0x146

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_SANGO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_SANSKRIT"

    const/16 v2, 0x147

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_SANSKRIT:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_SARDINIAN"

    const/16 v2, 0x148

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_SARDINIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_SCOTS"

    const/16 v2, 0x149

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_SCOTS:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_SELKUP"

    const/16 v2, 0x14a

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_SELKUP:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_SEMITIC"

    const/16 v2, 0x14b

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_SEMITIC:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_SERBOCROATIAN"

    const/16 v2, 0x14c

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_SERBOCROATIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_SERER"

    const/16 v2, 0x14d

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_SERER:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_SHAN"

    const/16 v2, 0x14e

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_SHAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_SHONA"

    const/16 v2, 0x14f

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_SHONA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_SIDAMO"

    const/16 v2, 0x150

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_SIDAMO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_SIKSIKA"

    const/16 v2, 0x151

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_SIKSIKA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_SINDHI"

    const/16 v2, 0x152

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_SINDHI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_SINGHALESE"

    const/16 v2, 0x153

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_SINGHALESE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_SINOTIBETAN"

    const/16 v2, 0x154

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_SINOTIBETAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_SIOUAN"

    const/16 v2, 0x155

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_SIOUAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_SLAVIC"

    const/16 v2, 0x156

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_SLAVIC:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_SISWANT"

    const/16 v2, 0x157

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_SISWANT:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_SOGDIAN"

    const/16 v2, 0x158

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_SOGDIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_SOMALI"

    const/16 v2, 0x159

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_SOMALI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_SONGHAI"

    const/16 v2, 0x15a

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_SONGHAI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_SORBIAN"

    const/16 v2, 0x15b

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_SORBIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_SOTHONORTHERN"

    const/16 v2, 0x15c

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_SOTHONORTHERN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_SOTHOSOUTHERN"

    const/16 v2, 0x15d

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_SOTHOSOUTHERN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_SOUTHAMERICANINDIAN"

    const/16 v2, 0x15e

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_SOUTHAMERICANINDIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_SUKUMA"

    const/16 v2, 0x15f

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_SUKUMA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_SUMERIAN"

    const/16 v2, 0x160

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_SUMERIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_SUDANESE"

    const/16 v2, 0x161

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_SUDANESE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_SUSU"

    const/16 v2, 0x162

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_SUSU:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_SWAHILI"

    const/16 v2, 0x163

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_SWAHILI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_SWAZI"

    const/16 v2, 0x164

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_SWAZI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_SWIZE"

    const/16 v2, 0x165

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_SWIZE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_SYRIAC"

    const/16 v2, 0x166

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_SYRIAC:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_TAGALOG"

    const/16 v2, 0x167

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_TAGALOG:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_TAHITIAN"

    const/16 v2, 0x168

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_TAHITIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_TAJIK"

    const/16 v2, 0x169

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_TAJIK:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_TAMASHEK"

    const/16 v2, 0x16a

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_TAMASHEK:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_TAMIL"

    const/16 v2, 0x16b

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_TAMIL:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_TATAR"

    const/16 v2, 0x16c

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_TATAR:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_TELETEXT"

    const/16 v2, 0x16d

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_TELETEXT:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_TELUGU"

    const/16 v2, 0x16e

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_TELUGU:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_TERENO"

    const/16 v2, 0x16f

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_TERENO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_THAI"

    const/16 v2, 0x170

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_THAI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_TIBETAN"

    const/16 v2, 0x171

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_TIBETAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_TIGRE"

    const/16 v2, 0x172

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_TIGRE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_TIGRINYA"

    const/16 v2, 0x173

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_TIGRINYA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_TIMNE"

    const/16 v2, 0x174

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_TIMNE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_TIVI"

    const/16 v2, 0x175

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_TIVI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_TLINGIT"

    const/16 v2, 0x176

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_TLINGIT:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_TONGANYASA"

    const/16 v2, 0x177

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_TONGANYASA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_TONGAISLANDS"

    const/16 v2, 0x178

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_TONGAISLANDS:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_TRUK"

    const/16 v2, 0x179

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_TRUK:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_TSIMSHIAN"

    const/16 v2, 0x17a

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_TSIMSHIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_TSONGA"

    const/16 v2, 0x17b

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_TSONGA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_TSWANA"

    const/16 v2, 0x17c

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_TSWANA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_TUMBUKA"

    const/16 v2, 0x17d

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_TUMBUKA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_TURKISHOTTOMAN"

    const/16 v2, 0x17e

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_TURKISHOTTOMAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_TURKMEN"

    const/16 v2, 0x17f

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_TURKMEN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_TUVINIAN"

    const/16 v2, 0x180

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_TUVINIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_TWI"

    const/16 v2, 0x181

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_TWI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_UGARITIC"

    const/16 v2, 0x182

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_UGARITIC:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_UIGHUR"

    const/16 v2, 0x183

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_UIGHUR:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_UKRAINIAN"

    const/16 v2, 0x184

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_UKRAINIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_UMBUNDU"

    const/16 v2, 0x185

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_UMBUNDU:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_URDU"

    const/16 v2, 0x186

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_URDU:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_UZBEK"

    const/16 v2, 0x187

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_UZBEK:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_VAI"

    const/16 v2, 0x188

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_VAI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_VENDA"

    const/16 v2, 0x189

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_VENDA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_VIETNAMESE"

    const/16 v2, 0x18a

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_VIETNAMESE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_VOLAPUK"

    const/16 v2, 0x18b

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_VOLAPUK:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_VOTIC"

    const/16 v2, 0x18c

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_VOTIC:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_WAKASHAN"

    const/16 v2, 0x18d

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_WAKASHAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_WALAMO"

    const/16 v2, 0x18e

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_WALAMO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_WARAY"

    const/16 v2, 0x18f

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_WARAY:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_WASHO"

    const/16 v2, 0x190

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_WASHO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_WOLOF"

    const/16 v2, 0x191

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_WOLOF:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_XHOSA"

    const/16 v2, 0x192

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_XHOSA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_YAKUT"

    const/16 v2, 0x193

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_YAKUT:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_YAO"

    const/16 v2, 0x194

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_YAO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_YAP"

    const/16 v2, 0x195

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_YAP:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_YIDDISH"

    const/16 v2, 0x196

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_YIDDISH:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_YORUBA"

    const/16 v2, 0x197

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_YORUBA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_ZAPOTEC"

    const/16 v2, 0x198

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_ZAPOTEC:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_ZENAGA"

    const/16 v2, 0x199

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_ZENAGA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_ZHUANG"

    const/16 v2, 0x19a

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_ZHUANG:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_ZULU"

    const/16 v2, 0x19b

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_ZULU:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_ZUNI"

    const/16 v2, 0x19c

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_ZUNI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_VALENCIAN"

    const/16 v2, 0x19d

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_VALENCIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_NUM"

    const/16 v2, 0x19e

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_NUM:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const-string v1, "E_MAX"

    const/16 v2, 0x19f

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_MAX:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    const/16 v0, 0x1a0

    new-array v0, v0, [Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    sget-object v1, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_CZECH:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_DANISH:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_GERMAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_ENGLISH:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v1, v0, v6

    sget-object v1, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_SPANISH:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_GREEK:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_FRENCH:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_CROATIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_ITALIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_HUNGARIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_DUTCH:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_NORWEGIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_POLISH:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_PORTUGUESE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_RUSSIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_ROMANIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_SLOVENIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_SERBIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_FINNISH:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_SWEDISH:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_BULGARIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_SLOVAK:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_CHINESE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_GAELIC:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_WELSH:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_ARABIC:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_IRISH:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_LATVIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_HEBREW:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_TURKISH:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_ESTONIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_NETHERLANDS:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_KOREAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_HINDI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_MANDARIN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_CANTONESE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_MAORI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_QAA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_UNDETERMINED:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_AD:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_UNKNOWN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_NONE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_ABKHAZIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_ACHINESE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_ACOLI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_ADANGME:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_AFAR:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_AFRIHILI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x30

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_AFRIKAANS:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x31

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_AFRO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x32

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_AKAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x33

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_AKKADIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x34

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_ALBANIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x35

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_ALEUT:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x36

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_ALGONQUIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x37

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_ALTAIC:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x38

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_AMHARIC:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x39

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_APACHE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_ARAMAIC:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_ARAPAHO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_ARAUCANIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_ARAWAK:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_ARMENIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_ARTIFICIAL:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x40

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_ASSAMESE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x41

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_ATHAPASCAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x42

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_AUSTRONESIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x43

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_AVARIC:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x44

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_AVESTAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x45

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_AWADHI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x46

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_AYMARA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x47

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_AZERBAIJANI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x48

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_AZTEC:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x49

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_BALINESE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x4a

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_BALTIC:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x4b

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_BALUCHI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x4c

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_BAMBARA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x4d

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_BAMILEKE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x4e

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_BANDA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x4f

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_BANTU:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x50

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_BASA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x51

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_BASHKIR:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x52

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_BASQUE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x53

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_BEJA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x54

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_BEMBA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x55

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_BENGALI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x56

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_BERBER:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x57

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_BHOJPURI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x58

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_BIHARI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x59

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_BIKOL:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x5a

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_BINI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x5b

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_BISLAMA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x5c

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_BRAJ:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x5d

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_BRETON:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x5e

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_BUGINESE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x5f

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_BURIAT:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x60

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_BURMESE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x61

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_BYELORUSSIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x62

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_CADDO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x63

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_CARIB:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x64

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_CATALAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x65

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_CAUCASIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x66

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_CEBUANO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x67

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_CELTIC:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x68

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_CENTRALAMERICANINDIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x69

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_CHAGATAI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x6a

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_CHAMORRO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x6b

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_CHECHEN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x6c

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_CHEROKEE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x6d

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_CHEYENNE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x6e

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_CHIBCHA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x6f

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_CHINOOKJARGON:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x70

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_CHOCTAW:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x71

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_CHURCHSLAVIC:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x72

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_CHUVASH:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x73

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_COPTIC:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x74

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_CORNISH:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x75

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_CORSICAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x76

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_CREE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x77

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_CREEK:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x78

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_CREOLESANDPIDGINS:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x79

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_CREOLESANDPIDGINSENGLISH:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x7a

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_CREOLESANDPIDGINSFRENCH:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x7b

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_CREOLESANDPIDGINSPORTUGUESE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x7c

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_CUSHITIC:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x7d

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_DAKOTA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x7e

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_DELAWARE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x7f

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_DINKA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x80

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_DIVEHI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x81

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_DOGRI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x82

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_DRAVIDIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x83

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_DUALA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x84

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_DUTCHMIDDLE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x85

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_DYULA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x86

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_DZONGKHA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x87

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_EFIK:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x88

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_EGYPTIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x89

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_EKAJUK:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x8a

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_ELAMITE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x8b

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_ENGLISHMIDDLE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x8c

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_ENGLISHOLD:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x8d

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_ESKIMO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x8e

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_ESPERANTO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x8f

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_EWE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x90

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_EWONDO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x91

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_FANG:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x92

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_FANTI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x93

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_FAROESE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x94

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_FIJIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x95

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_FINNOUGRIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x96

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_FON:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x97

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_FRENCHMIDDLE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x98

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_FRENCHOLD:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x99

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_FRISIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x9a

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_FULAH:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x9b

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_GA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x9c

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_GALLEGAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x9d

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_GANDA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x9e

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_GAYO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x9f

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_GEEZ:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xa0

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_GEORGIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xa1

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_GERMANMIDDLEHIGH:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xa2

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_GERMANOLDHIGH:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xa3

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_GERMANIC:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xa4

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_GILBERTESE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xa5

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_GONDI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xa6

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_GOTHIC:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xa7

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_GREBO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xa8

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_GREEKANCIENT:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xa9

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_GREENLANDIC:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xaa

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_GUARANI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xab

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_GUJARATI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xac

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_HAIDA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xad

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_HAUSA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xae

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_HAWAIIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xaf

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_HERERO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xb0

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_HILIGAYNON:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xb1

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_HIMACHALI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xb2

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_HIRIMOTU:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xb3

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_HUPA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xb4

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_IBAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xb5

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_ICELANDIC:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xb6

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_IGBO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xb7

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_IJO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xb8

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_ILOKO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xb9

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_INDIC:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xba

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_INDOEUROPEAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xbb

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_INDONESIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xbc

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_INTERLINGUA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xbd

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_INTERLINGUE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xbe

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_INUKTITUT:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xbf

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_INUPIAK:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xc0

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_IRANIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xc1

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_IRISHOLD:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xc2

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_IRISHMIDDLE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xc3

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_IROQUOIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xc4

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_JAPANESE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xc5

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_JAVANESE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xc6

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_JUDEOARABIC:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xc7

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_JUDEOPERSIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xc8

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_KABYLE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xc9

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_KACHIN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xca

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_KAMBA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xcb

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_KANNADA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xcc

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_KANURI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xcd

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_KARAKALPAK:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xce

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_KAREN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xcf

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_KASHMIRI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xd0

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_KAWI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xd1

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_KAZAKH:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xd2

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_KHASI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xd3

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_KHMER:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xd4

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_KHOISAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xd5

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_KHOTANESE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xd6

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_KIKUYU:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xd7

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_KINYARWANDA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xd8

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_KIRGHIZ:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xd9

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_KOMI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xda

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_KONGO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xdb

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_KONKANI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xdc

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_KPELLE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xdd

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_KRU:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xde

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_KUANYAMA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xdf

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_KUMYK:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xe0

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_KURDISH:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xe1

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_KURUKH:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xe2

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_KUSAIE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xe3

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_KUTENAI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xe4

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_LADINO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xe5

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_LAHNDA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xe6

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_LAMBA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xe7

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_LANGUE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xe8

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_LAO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xe9

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_LATIN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xea

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_LETZEBURGESCH:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xeb

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_LEZGHIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xec

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_LINGALA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xed

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_LITHUANIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xee

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_LOZI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xef

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_LUBAKATANGA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xf0

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_LUISENO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xf1

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_LUNDA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xf2

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_LUO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xf3

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_MACEDONIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xf4

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_MADURESE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xf5

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_MAGAHI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xf6

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_MAITHILI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xf7

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_MAKASAR:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xf8

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_MALAGASY:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xf9

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_MALAY:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xfa

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_MALAYALAM:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xfb

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_MALTESE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xfc

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_MANDINGO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xfd

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_MANIPURI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xfe

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_MANOBO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0xff

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_MANX:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x100

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_MARATHI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x101

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_MARI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x102

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_MARSHALL:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x103

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_MARWARI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x104

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_MASAI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x105

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_MAYAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x106

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_MENDE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x107

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_MICMAC:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x108

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_MINANGKABAU:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x109

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_MISCELLANEOUS:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x10a

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_MOHAWK:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x10b

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_MOLDAVIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x10c

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_MONKMER:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x10d

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_MONGO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x10e

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_MONGOLIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x10f

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_MOSSI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x110

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_MULTIPLE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x111

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_MUNDA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x112

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_NAURU:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x113

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_NAVAJO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x114

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_NDEBELENORTH:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x115

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_NDEBELESOUTH:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x116

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_NDONGO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x117

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_NEPALI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x118

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_NEWARI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x119

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_NIGERKORDOFANIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x11a

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_NILOSAHARAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x11b

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_NIUEAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x11c

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_NORSEOLD:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x11d

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_NORTHAMERICANINDIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x11e

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_NORWEGIANNYNORSK:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x11f

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_NUBIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x120

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_NYAMWEZI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x121

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_NYANJA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x122

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_NYANKOLE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x123

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_NYORO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x124

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_NZIMA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x125

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_OJIBWA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x126

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_ORIYA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x127

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_OROMO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x128

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_OSAGE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x129

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_OSSETIC:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x12a

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_OTOMIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x12b

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_PAHLAVI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x12c

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_PALAUAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x12d

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_PALI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x12e

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_PAMPANGA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x12f

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_PANGASINAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x130

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_PANJABI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x131

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_PAPIAMENTO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x132

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_PAPUANAUSTRALIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x133

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_PERSIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x134

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_PERSIANOLD:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x135

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_PHOENICIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x136

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_PONAPE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x137

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_PRAKRIT:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x138

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_PROVENCALOLD:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x139

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_PUSHTO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x13a

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_QUECHUA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x13b

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_RHAETOROMANCE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x13c

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_RAJASTHANI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x13d

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_RAROTONGAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x13e

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_ROMANCE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x13f

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_ROMANY:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x140

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_RUNDI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x141

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_SALISHAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x142

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_SAMARITANARAMAIC:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x143

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_SAMI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x144

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_SAMOAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x145

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_SANDAWE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x146

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_SANGO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x147

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_SANSKRIT:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x148

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_SARDINIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x149

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_SCOTS:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x14a

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_SELKUP:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x14b

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_SEMITIC:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x14c

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_SERBOCROATIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x14d

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_SERER:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x14e

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_SHAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x14f

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_SHONA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x150

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_SIDAMO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x151

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_SIKSIKA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x152

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_SINDHI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x153

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_SINGHALESE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x154

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_SINOTIBETAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x155

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_SIOUAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x156

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_SLAVIC:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x157

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_SISWANT:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x158

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_SOGDIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x159

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_SOMALI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x15a

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_SONGHAI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x15b

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_SORBIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x15c

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_SOTHONORTHERN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x15d

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_SOTHOSOUTHERN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x15e

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_SOUTHAMERICANINDIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x15f

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_SUKUMA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x160

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_SUMERIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x161

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_SUDANESE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x162

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_SUSU:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x163

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_SWAHILI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x164

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_SWAZI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x165

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_SWIZE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x166

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_SYRIAC:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x167

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_TAGALOG:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x168

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_TAHITIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x169

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_TAJIK:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x16a

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_TAMASHEK:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x16b

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_TAMIL:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x16c

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_TATAR:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x16d

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_TELETEXT:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x16e

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_TELUGU:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x16f

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_TERENO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x170

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_THAI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x171

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_TIBETAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x172

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_TIGRE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x173

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_TIGRINYA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x174

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_TIMNE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x175

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_TIVI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x176

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_TLINGIT:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x177

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_TONGANYASA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x178

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_TONGAISLANDS:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x179

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_TRUK:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x17a

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_TSIMSHIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x17b

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_TSONGA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x17c

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_TSWANA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x17d

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_TUMBUKA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x17e

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_TURKISHOTTOMAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x17f

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_TURKMEN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x180

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_TUVINIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x181

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_TWI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x182

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_UGARITIC:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x183

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_UIGHUR:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x184

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_UKRAINIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x185

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_UMBUNDU:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x186

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_URDU:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x187

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_UZBEK:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x188

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_VAI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x189

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_VENDA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x18a

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_VIETNAMESE:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x18b

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_VOLAPUK:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x18c

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_VOTIC:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x18d

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_WAKASHAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x18e

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_WALAMO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x18f

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_WARAY:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x190

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_WASHO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x191

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_WOLOF:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x192

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_XHOSA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x193

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_YAKUT:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x194

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_YAO:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x195

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_YAP:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x196

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_YIDDISH:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x197

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_YORUBA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x198

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_ZAPOTEC:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x199

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_ZENAGA:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x19a

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_ZHUANG:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x19b

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_ZULU:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x19c

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_ZUNI:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x19d

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_VALENCIAN:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x19e

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_NUM:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    const/16 v1, 0x19f

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->E_MAX:Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    aput-object v2, v0, v1

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->ENUM$VALUES:[Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE$1;

    invoke-direct {v0}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE$1;-><init>()V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;
    .locals 1

    const-class v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    return-object v0
.end method

.method public static values()[Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->ENUM$VALUES:[Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    array-length v1, v0

    new-array v2, v1, [Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    invoke-virtual {p0}, Lcom/mstar/tv/service/aidl/EN_MS_LANGUAGE;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
