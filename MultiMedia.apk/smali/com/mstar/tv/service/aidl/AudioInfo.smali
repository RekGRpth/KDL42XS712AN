.class public Lcom/mstar/tv/service/aidl/AudioInfo;
.super Ljava/lang/Object;
.source "AudioInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mstar/tv/service/aidl/AudioInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public aacProfileAndLevel:S

.field public aacType:S

.field public audioPid:I

.field public audioType:S

.field public broadcastMixAd:Z

.field public isoLangInfo:Lcom/mstar/tv/service/aidl/LangIso639;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/mstar/tv/service/aidl/AudioInfo$1;

    invoke-direct {v0}, Lcom/mstar/tv/service/aidl/AudioInfo$1;-><init>()V

    sput-object v0, Lcom/mstar/tv/service/aidl/AudioInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/mstar/tv/service/aidl/LangIso639;

    invoke-direct {v0}, Lcom/mstar/tv/service/aidl/LangIso639;-><init>()V

    iput-object v0, p0, Lcom/mstar/tv/service/aidl/AudioInfo;->isoLangInfo:Lcom/mstar/tv/service/aidl/LangIso639;

    iput v1, p0, Lcom/mstar/tv/service/aidl/AudioInfo;->audioPid:I

    iput-short v1, p0, Lcom/mstar/tv/service/aidl/AudioInfo;->audioType:S

    iput-boolean v1, p0, Lcom/mstar/tv/service/aidl/AudioInfo;->broadcastMixAd:Z

    iput-short v1, p0, Lcom/mstar/tv/service/aidl/AudioInfo;->aacType:S

    iput-short v1, p0, Lcom/mstar/tv/service/aidl/AudioInfo;->aacProfileAndLevel:S

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1    # Landroid/os/Parcel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/mstar/tv/service/aidl/LangIso639;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/tv/service/aidl/LangIso639;

    iput-object v0, p0, Lcom/mstar/tv/service/aidl/AudioInfo;->isoLangInfo:Lcom/mstar/tv/service/aidl/LangIso639;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/tv/service/aidl/AudioInfo;->audioPid:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/tv/service/aidl/AudioInfo;->audioType:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, p0, Lcom/mstar/tv/service/aidl/AudioInfo;->broadcastMixAd:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/tv/service/aidl/AudioInfo;->aacType:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/tv/service/aidl/AudioInfo;->aacProfileAndLevel:S

    return-void

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/mstar/tv/service/aidl/AudioInfo;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/mstar/tv/service/aidl/AudioInfo;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/mstar/tv/service/aidl/AudioInfo;->isoLangInfo:Lcom/mstar/tv/service/aidl/LangIso639;

    invoke-virtual {v1, p1, v0}, Lcom/mstar/tv/service/aidl/LangIso639;->writeToParcel(Landroid/os/Parcel;I)V

    iget v1, p0, Lcom/mstar/tv/service/aidl/AudioInfo;->audioPid:I

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v1, p0, Lcom/mstar/tv/service/aidl/AudioInfo;->audioType:S

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v1, p0, Lcom/mstar/tv/service/aidl/AudioInfo;->broadcastMixAd:Z

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/tv/service/aidl/AudioInfo;->aacType:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/tv/service/aidl/AudioInfo;->aacProfileAndLevel:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
