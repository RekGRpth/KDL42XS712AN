.class public Lcom/mstar/tv/service/aidl/DvbMuxInfo;
.super Ljava/lang/Object;
.source "DvbMuxInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mstar/tv/service/aidl/DvbMuxInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public bandwidth:S

.field public cellID:I

.field public frequency:I

.field public lossSignalFrequency:I

.field public lossSignalStartTime:I

.field public lpCoding:Z

.field public modulationMode:S

.field public networkId:I

.field public networkTableID:I

.field public originalNetworkId:I

.field public plpID:I

.field public polarityPilotsReserved:I

.field public refCnt:I

.field public rfNumber:S

.field public satID:S

.field public satTableId:I

.field public symbRate:I

.field public transportStreamId:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/mstar/tv/service/aidl/DvbMuxInfo$1;

    invoke-direct {v0}, Lcom/mstar/tv/service/aidl/DvbMuxInfo$1;-><init>()V

    sput-object v0, Lcom/mstar/tv/service/aidl/DvbMuxInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/mstar/tv/service/aidl/DvbMuxInfo;->satTableId:I

    iput v0, p0, Lcom/mstar/tv/service/aidl/DvbMuxInfo;->networkTableID:I

    iput v0, p0, Lcom/mstar/tv/service/aidl/DvbMuxInfo;->refCnt:I

    iput v0, p0, Lcom/mstar/tv/service/aidl/DvbMuxInfo;->transportStreamId:I

    iput v0, p0, Lcom/mstar/tv/service/aidl/DvbMuxInfo;->originalNetworkId:I

    iput v0, p0, Lcom/mstar/tv/service/aidl/DvbMuxInfo;->networkId:I

    iput v0, p0, Lcom/mstar/tv/service/aidl/DvbMuxInfo;->cellID:I

    iput-short v0, p0, Lcom/mstar/tv/service/aidl/DvbMuxInfo;->rfNumber:S

    iput v0, p0, Lcom/mstar/tv/service/aidl/DvbMuxInfo;->frequency:I

    iput v0, p0, Lcom/mstar/tv/service/aidl/DvbMuxInfo;->lossSignalFrequency:I

    iput v0, p0, Lcom/mstar/tv/service/aidl/DvbMuxInfo;->lossSignalStartTime:I

    iput v0, p0, Lcom/mstar/tv/service/aidl/DvbMuxInfo;->symbRate:I

    iput-short v0, p0, Lcom/mstar/tv/service/aidl/DvbMuxInfo;->modulationMode:S

    iput v0, p0, Lcom/mstar/tv/service/aidl/DvbMuxInfo;->plpID:I

    iput-boolean v0, p0, Lcom/mstar/tv/service/aidl/DvbMuxInfo;->lpCoding:Z

    iput-short v0, p0, Lcom/mstar/tv/service/aidl/DvbMuxInfo;->satID:S

    iput-short v0, p0, Lcom/mstar/tv/service/aidl/DvbMuxInfo;->bandwidth:S

    iput v0, p0, Lcom/mstar/tv/service/aidl/DvbMuxInfo;->polarityPilotsReserved:I

    return-void
.end method

.method public constructor <init>(IIIIIIISIIIISIZSSI)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # I
    .param p7    # I
    .param p8    # S
    .param p9    # I
    .param p10    # I
    .param p11    # I
    .param p12    # I
    .param p13    # S
    .param p14    # I
    .param p15    # Z
    .param p16    # S
    .param p17    # S
    .param p18    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/mstar/tv/service/aidl/DvbMuxInfo;->satTableId:I

    iput p2, p0, Lcom/mstar/tv/service/aidl/DvbMuxInfo;->networkTableID:I

    iput p3, p0, Lcom/mstar/tv/service/aidl/DvbMuxInfo;->refCnt:I

    iput p4, p0, Lcom/mstar/tv/service/aidl/DvbMuxInfo;->transportStreamId:I

    iput p5, p0, Lcom/mstar/tv/service/aidl/DvbMuxInfo;->originalNetworkId:I

    iput p6, p0, Lcom/mstar/tv/service/aidl/DvbMuxInfo;->networkId:I

    iput p7, p0, Lcom/mstar/tv/service/aidl/DvbMuxInfo;->cellID:I

    iput-short p8, p0, Lcom/mstar/tv/service/aidl/DvbMuxInfo;->rfNumber:S

    iput p9, p0, Lcom/mstar/tv/service/aidl/DvbMuxInfo;->frequency:I

    iput p10, p0, Lcom/mstar/tv/service/aidl/DvbMuxInfo;->lossSignalFrequency:I

    iput p11, p0, Lcom/mstar/tv/service/aidl/DvbMuxInfo;->lossSignalStartTime:I

    iput p12, p0, Lcom/mstar/tv/service/aidl/DvbMuxInfo;->symbRate:I

    iput-short p13, p0, Lcom/mstar/tv/service/aidl/DvbMuxInfo;->modulationMode:S

    iput p14, p0, Lcom/mstar/tv/service/aidl/DvbMuxInfo;->plpID:I

    move/from16 v0, p15

    iput-boolean v0, p0, Lcom/mstar/tv/service/aidl/DvbMuxInfo;->lpCoding:Z

    move/from16 v0, p16

    iput-short v0, p0, Lcom/mstar/tv/service/aidl/DvbMuxInfo;->satID:S

    move/from16 v0, p17

    iput-short v0, p0, Lcom/mstar/tv/service/aidl/DvbMuxInfo;->bandwidth:S

    move/from16 v0, p18

    iput v0, p0, Lcom/mstar/tv/service/aidl/DvbMuxInfo;->polarityPilotsReserved:I

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .param p1    # Landroid/os/Parcel;

    const/4 v0, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/mstar/tv/service/aidl/DvbMuxInfo;->satTableId:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/mstar/tv/service/aidl/DvbMuxInfo;->networkTableID:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/mstar/tv/service/aidl/DvbMuxInfo;->refCnt:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/mstar/tv/service/aidl/DvbMuxInfo;->transportStreamId:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/mstar/tv/service/aidl/DvbMuxInfo;->originalNetworkId:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/mstar/tv/service/aidl/DvbMuxInfo;->networkId:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/mstar/tv/service/aidl/DvbMuxInfo;->cellID:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    int-to-short v1, v1

    iput-short v1, p0, Lcom/mstar/tv/service/aidl/DvbMuxInfo;->rfNumber:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/mstar/tv/service/aidl/DvbMuxInfo;->frequency:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/mstar/tv/service/aidl/DvbMuxInfo;->lossSignalFrequency:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/mstar/tv/service/aidl/DvbMuxInfo;->lossSignalStartTime:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/mstar/tv/service/aidl/DvbMuxInfo;->symbRate:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    int-to-short v1, v1

    iput-short v1, p0, Lcom/mstar/tv/service/aidl/DvbMuxInfo;->modulationMode:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/mstar/tv/service/aidl/DvbMuxInfo;->plpID:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/mstar/tv/service/aidl/DvbMuxInfo;->lpCoding:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/tv/service/aidl/DvbMuxInfo;->satID:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/tv/service/aidl/DvbMuxInfo;->bandwidth:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/tv/service/aidl/DvbMuxInfo;->polarityPilotsReserved:I

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/mstar/tv/service/aidl/DvbMuxInfo;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/mstar/tv/service/aidl/DvbMuxInfo;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget v0, p0, Lcom/mstar/tv/service/aidl/DvbMuxInfo;->satTableId:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/tv/service/aidl/DvbMuxInfo;->networkTableID:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/tv/service/aidl/DvbMuxInfo;->refCnt:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/tv/service/aidl/DvbMuxInfo;->transportStreamId:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/tv/service/aidl/DvbMuxInfo;->originalNetworkId:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/tv/service/aidl/DvbMuxInfo;->networkId:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/tv/service/aidl/DvbMuxInfo;->cellID:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/tv/service/aidl/DvbMuxInfo;->rfNumber:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/tv/service/aidl/DvbMuxInfo;->frequency:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/tv/service/aidl/DvbMuxInfo;->lossSignalFrequency:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/tv/service/aidl/DvbMuxInfo;->lossSignalStartTime:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/tv/service/aidl/DvbMuxInfo;->symbRate:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/tv/service/aidl/DvbMuxInfo;->modulationMode:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/tv/service/aidl/DvbMuxInfo;->plpID:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/mstar/tv/service/aidl/DvbMuxInfo;->lpCoding:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/tv/service/aidl/DvbMuxInfo;->satID:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/tv/service/aidl/DvbMuxInfo;->bandwidth:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/tv/service/aidl/DvbMuxInfo;->polarityPilotsReserved:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
