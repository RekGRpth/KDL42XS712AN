.class public Lcom/mstar/tv/service/aidl/T_MS_COLOR_TEMPEX_DATA;
.super Ljava/lang/Object;
.source "T_MS_COLOR_TEMPEX_DATA.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mstar/tv/service/aidl/T_MS_COLOR_TEMPEX_DATA;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public bluegain:I

.field public blueoffset:I

.field public greengain:I

.field public greenoffset:I

.field public redgain:I

.field public redoffset:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/mstar/tv/service/aidl/T_MS_COLOR_TEMPEX_DATA$1;

    invoke-direct {v0}, Lcom/mstar/tv/service/aidl/T_MS_COLOR_TEMPEX_DATA$1;-><init>()V

    sput-object v0, Lcom/mstar/tv/service/aidl/T_MS_COLOR_TEMPEX_DATA;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(IIIIII)V
    .locals 0
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/mstar/tv/service/aidl/T_MS_COLOR_TEMPEX_DATA;->redgain:I

    iput p2, p0, Lcom/mstar/tv/service/aidl/T_MS_COLOR_TEMPEX_DATA;->greengain:I

    iput p3, p0, Lcom/mstar/tv/service/aidl/T_MS_COLOR_TEMPEX_DATA;->bluegain:I

    iput p4, p0, Lcom/mstar/tv/service/aidl/T_MS_COLOR_TEMPEX_DATA;->redoffset:I

    iput p5, p0, Lcom/mstar/tv/service/aidl/T_MS_COLOR_TEMPEX_DATA;->greenoffset:I

    iput p6, p0, Lcom/mstar/tv/service/aidl/T_MS_COLOR_TEMPEX_DATA;->blueoffset:I

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget v0, p0, Lcom/mstar/tv/service/aidl/T_MS_COLOR_TEMPEX_DATA;->redgain:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/tv/service/aidl/T_MS_COLOR_TEMPEX_DATA;->greengain:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/tv/service/aidl/T_MS_COLOR_TEMPEX_DATA;->bluegain:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/tv/service/aidl/T_MS_COLOR_TEMPEX_DATA;->redoffset:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/tv/service/aidl/T_MS_COLOR_TEMPEX_DATA;->greenoffset:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/tv/service/aidl/T_MS_COLOR_TEMPEX_DATA;->blueoffset:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
