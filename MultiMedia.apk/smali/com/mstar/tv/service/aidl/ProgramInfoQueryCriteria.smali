.class public Lcom/mstar/tv/service/aidl/ProgramInfoQueryCriteria;
.super Ljava/lang/Object;
.source "ProgramInfoQueryCriteria.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mstar/tv/service/aidl/ProgramInfoQueryCriteria;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public number:I

.field public queryIndex:I

.field protected serviceType:S


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/mstar/tv/service/aidl/ProgramInfoQueryCriteria$1;

    invoke-direct {v0}, Lcom/mstar/tv/service/aidl/ProgramInfoQueryCriteria$1;-><init>()V

    sput-object v0, Lcom/mstar/tv/service/aidl/ProgramInfoQueryCriteria;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(IIS)V
    .locals 0
    .param p1    # I
    .param p2    # I
    .param p3    # S

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/mstar/tv/service/aidl/ProgramInfoQueryCriteria;->queryIndex:I

    iput p2, p0, Lcom/mstar/tv/service/aidl/ProgramInfoQueryCriteria;->number:I

    iput-short p3, p0, Lcom/mstar/tv/service/aidl/ProgramInfoQueryCriteria;->serviceType:S

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1    # Landroid/os/Parcel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/tv/service/aidl/ProgramInfoQueryCriteria;->queryIndex:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/tv/service/aidl/ProgramInfoQueryCriteria;->number:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/tv/service/aidl/ProgramInfoQueryCriteria;->serviceType:S

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/mstar/tv/service/aidl/ProgramInfoQueryCriteria;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/mstar/tv/service/aidl/ProgramInfoQueryCriteria;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getServiceType()Lcom/mstar/tv/service/aidl/EN_MEMBER_SERVICE_TYPE;
    .locals 2

    invoke-static {}, Lcom/mstar/tv/service/aidl/EN_MEMBER_SERVICE_TYPE;->values()[Lcom/mstar/tv/service/aidl/EN_MEMBER_SERVICE_TYPE;

    move-result-object v0

    iget-short v1, p0, Lcom/mstar/tv/service/aidl/ProgramInfoQueryCriteria;->serviceType:S

    aget-object v0, v0, v1

    return-object v0
.end method

.method public setServiceType(Lcom/mstar/tv/service/aidl/EN_MEMBER_SERVICE_TYPE;)V
    .locals 1
    .param p1    # Lcom/mstar/tv/service/aidl/EN_MEMBER_SERVICE_TYPE;

    invoke-virtual {p1}, Lcom/mstar/tv/service/aidl/EN_MEMBER_SERVICE_TYPE;->ordinal()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/tv/service/aidl/ProgramInfoQueryCriteria;->serviceType:S

    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget v0, p0, Lcom/mstar/tv/service/aidl/ProgramInfoQueryCriteria;->queryIndex:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/tv/service/aidl/ProgramInfoQueryCriteria;->number:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/tv/service/aidl/ProgramInfoQueryCriteria;->serviceType:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
