.class Lcom/mstar/SmbClient$scanHostByPing;
.super Ljava/lang/Object;
.source "SmbClient.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mstar/SmbClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "scanHostByPing"
.end annotation


# instance fields
.field callback:Lcom/mstar/CallBack;

.field endIndex:I

.field startIndex:I

.field startIp:Ljava/lang/String;

.field final synthetic this$0:Lcom/mstar/SmbClient;

.field threadid:I


# direct methods
.method constructor <init>(Lcom/mstar/SmbClient;Ljava/lang/String;IIILcom/mstar/CallBack;)V
    .locals 0
    .param p2    # Ljava/lang/String;
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # Lcom/mstar/CallBack;

    iput-object p1, p0, Lcom/mstar/SmbClient$scanHostByPing;->this$0:Lcom/mstar/SmbClient;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/mstar/SmbClient$scanHostByPing;->startIp:Ljava/lang/String;

    iput-object p6, p0, Lcom/mstar/SmbClient$scanHostByPing;->callback:Lcom/mstar/CallBack;

    iput p4, p0, Lcom/mstar/SmbClient$scanHostByPing;->startIndex:I

    iput p5, p0, Lcom/mstar/SmbClient$scanHostByPing;->endIndex:I

    iput p3, p0, Lcom/mstar/SmbClient$scanHostByPing;->threadid:I

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    const v6, 0xffff

    const/4 v4, 0x1

    const/4 v5, 0x0

    iget-object v2, p0, Lcom/mstar/SmbClient$scanHostByPing;->callback:Lcom/mstar/CallBack;

    invoke-interface {v2, v5, v4}, Lcom/mstar/CallBack;->callback(Lcom/mstar/SambaFile;I)V

    iget v0, p0, Lcom/mstar/SmbClient$scanHostByPing;->startIndex:I

    :goto_0
    iget v2, p0, Lcom/mstar/SmbClient$scanHostByPing;->endIndex:I

    if-le v0, v2, :cond_2

    :cond_0
    iget v2, p0, Lcom/mstar/SmbClient$scanHostByPing;->threadid:I

    packed-switch v2, :pswitch_data_0

    :goto_1
    iget-object v2, p0, Lcom/mstar/SmbClient$scanHostByPing;->this$0:Lcom/mstar/SmbClient;

    iget v2, v2, Lcom/mstar/SmbClient;->ScanCompleted:I

    and-int/2addr v2, v6

    if-ne v2, v6, :cond_1

    iget-object v2, p0, Lcom/mstar/SmbClient$scanHostByPing;->callback:Lcom/mstar/CallBack;

    const/4 v3, 0x3

    invoke-interface {v2, v5, v3}, Lcom/mstar/CallBack;->callback(Lcom/mstar/SambaFile;I)V

    :cond_1
    return-void

    :cond_2
    iget-object v2, p0, Lcom/mstar/SmbClient$scanHostByPing;->this$0:Lcom/mstar/SmbClient;

    # getter for: Lcom/mstar/SmbClient;->stoprun:Z
    invoke-static {v2}, Lcom/mstar/SmbClient;->access$0(Lcom/mstar/SmbClient;)Z

    move-result v2

    if-nez v2, :cond_0

    const/16 v2, 0xff

    if-gt v0, v2, :cond_0

    if-lt v0, v4, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/mstar/SmbClient$scanHostByPing;->startIp:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/mstar/SmbClient$scanHostByPing;->this$0:Lcom/mstar/SmbClient;

    const/16 v3, 0xa

    invoke-virtual {v2, v1, v3}, Lcom/mstar/SmbClient;->pingHost(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/mstar/SmbClient$scanHostByPing;->callback:Lcom/mstar/CallBack;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/mstar/SmbClient$scanHostByPing;->callback:Lcom/mstar/CallBack;

    const/4 v3, 0x2

    invoke-interface {v2, v5, v3}, Lcom/mstar/CallBack;->callback(Lcom/mstar/SambaFile;I)V

    :cond_3
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/mstar/SmbClient$scanHostByPing;->this$0:Lcom/mstar/SmbClient;

    iget-object v2, v2, Lcom/mstar/SmbClient;->PingTable:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    monitor-exit p0

    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    :pswitch_0
    iget-object v2, p0, Lcom/mstar/SmbClient$scanHostByPing;->this$0:Lcom/mstar/SmbClient;

    iget v3, v2, Lcom/mstar/SmbClient;->ScanCompleted:I

    or-int/lit8 v3, v3, 0x1

    iput v3, v2, Lcom/mstar/SmbClient;->ScanCompleted:I

    goto :goto_1

    :pswitch_1
    iget-object v2, p0, Lcom/mstar/SmbClient$scanHostByPing;->this$0:Lcom/mstar/SmbClient;

    iget v3, v2, Lcom/mstar/SmbClient;->ScanCompleted:I

    or-int/lit8 v3, v3, 0x2

    iput v3, v2, Lcom/mstar/SmbClient;->ScanCompleted:I

    goto :goto_1

    :pswitch_2
    iget-object v2, p0, Lcom/mstar/SmbClient$scanHostByPing;->this$0:Lcom/mstar/SmbClient;

    iget v3, v2, Lcom/mstar/SmbClient;->ScanCompleted:I

    or-int/lit8 v3, v3, 0x4

    iput v3, v2, Lcom/mstar/SmbClient;->ScanCompleted:I

    goto :goto_1

    :pswitch_3
    iget-object v2, p0, Lcom/mstar/SmbClient$scanHostByPing;->this$0:Lcom/mstar/SmbClient;

    iget v3, v2, Lcom/mstar/SmbClient;->ScanCompleted:I

    or-int/lit8 v3, v3, 0x8

    iput v3, v2, Lcom/mstar/SmbClient;->ScanCompleted:I

    goto :goto_1

    :pswitch_4
    iget-object v2, p0, Lcom/mstar/SmbClient$scanHostByPing;->this$0:Lcom/mstar/SmbClient;

    iget v3, v2, Lcom/mstar/SmbClient;->ScanCompleted:I

    or-int/lit8 v3, v3, 0x10

    iput v3, v2, Lcom/mstar/SmbClient;->ScanCompleted:I

    goto :goto_1

    :pswitch_5
    iget-object v2, p0, Lcom/mstar/SmbClient$scanHostByPing;->this$0:Lcom/mstar/SmbClient;

    iget v3, v2, Lcom/mstar/SmbClient;->ScanCompleted:I

    or-int/lit8 v3, v3, 0x20

    iput v3, v2, Lcom/mstar/SmbClient;->ScanCompleted:I

    goto/16 :goto_1

    :pswitch_6
    iget-object v2, p0, Lcom/mstar/SmbClient$scanHostByPing;->this$0:Lcom/mstar/SmbClient;

    iget v3, v2, Lcom/mstar/SmbClient;->ScanCompleted:I

    or-int/lit8 v3, v3, 0x40

    iput v3, v2, Lcom/mstar/SmbClient;->ScanCompleted:I

    goto/16 :goto_1

    :pswitch_7
    iget-object v2, p0, Lcom/mstar/SmbClient$scanHostByPing;->this$0:Lcom/mstar/SmbClient;

    iget v3, v2, Lcom/mstar/SmbClient;->ScanCompleted:I

    or-int/lit16 v3, v3, 0x80

    iput v3, v2, Lcom/mstar/SmbClient;->ScanCompleted:I

    goto/16 :goto_1

    :pswitch_8
    iget-object v2, p0, Lcom/mstar/SmbClient$scanHostByPing;->this$0:Lcom/mstar/SmbClient;

    iget v3, v2, Lcom/mstar/SmbClient;->ScanCompleted:I

    or-int/lit16 v3, v3, 0x100

    iput v3, v2, Lcom/mstar/SmbClient;->ScanCompleted:I

    goto/16 :goto_1

    :pswitch_9
    iget-object v2, p0, Lcom/mstar/SmbClient$scanHostByPing;->this$0:Lcom/mstar/SmbClient;

    iget v3, v2, Lcom/mstar/SmbClient;->ScanCompleted:I

    or-int/lit16 v3, v3, 0x200

    iput v3, v2, Lcom/mstar/SmbClient;->ScanCompleted:I

    goto/16 :goto_1

    :pswitch_a
    iget-object v2, p0, Lcom/mstar/SmbClient$scanHostByPing;->this$0:Lcom/mstar/SmbClient;

    iget v3, v2, Lcom/mstar/SmbClient;->ScanCompleted:I

    or-int/lit16 v3, v3, 0x400

    iput v3, v2, Lcom/mstar/SmbClient;->ScanCompleted:I

    goto/16 :goto_1

    :pswitch_b
    iget-object v2, p0, Lcom/mstar/SmbClient$scanHostByPing;->this$0:Lcom/mstar/SmbClient;

    iget v3, v2, Lcom/mstar/SmbClient;->ScanCompleted:I

    or-int/lit16 v3, v3, 0x800

    iput v3, v2, Lcom/mstar/SmbClient;->ScanCompleted:I

    goto/16 :goto_1

    :pswitch_c
    iget-object v2, p0, Lcom/mstar/SmbClient$scanHostByPing;->this$0:Lcom/mstar/SmbClient;

    iget v3, v2, Lcom/mstar/SmbClient;->ScanCompleted:I

    or-int/lit16 v3, v3, 0x1000

    iput v3, v2, Lcom/mstar/SmbClient;->ScanCompleted:I

    goto/16 :goto_1

    :pswitch_d
    iget-object v2, p0, Lcom/mstar/SmbClient$scanHostByPing;->this$0:Lcom/mstar/SmbClient;

    iget v3, v2, Lcom/mstar/SmbClient;->ScanCompleted:I

    or-int/lit16 v3, v3, 0x2000

    iput v3, v2, Lcom/mstar/SmbClient;->ScanCompleted:I

    goto/16 :goto_1

    :pswitch_e
    iget-object v2, p0, Lcom/mstar/SmbClient$scanHostByPing;->this$0:Lcom/mstar/SmbClient;

    iget v3, v2, Lcom/mstar/SmbClient;->ScanCompleted:I

    or-int/lit16 v3, v3, 0x4000

    iput v3, v2, Lcom/mstar/SmbClient;->ScanCompleted:I

    goto/16 :goto_1

    :pswitch_f
    iget-object v2, p0, Lcom/mstar/SmbClient$scanHostByPing;->this$0:Lcom/mstar/SmbClient;

    iget v3, v2, Lcom/mstar/SmbClient;->ScanCompleted:I

    const v4, 0x8000

    or-int/2addr v3, v4

    iput v3, v2, Lcom/mstar/SmbClient;->ScanCompleted:I

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
    .end packed-switch
.end method
