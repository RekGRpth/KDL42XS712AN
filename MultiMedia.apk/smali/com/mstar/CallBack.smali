.class public interface abstract Lcom/mstar/CallBack;
.super Ljava/lang/Object;
.source "CallBack.java"


# static fields
.field public static final FLAG_COMPLETED:I = 0x3

.field public static final FLAG_DUPLICATE_NAME:I = 0x6

.field public static final FLAG_INVALID_COMPUTER_NAME:I = 0x7

.field public static final FLAG_INVALID_LOGON_HOURS:I = 0x8

.field public static final FLAG_LOGINFAIL:I = 0x4

.field public static final FLAG_LOGON_FAILURE:I = 0x9

.field public static final FLAG_LOGON_TYPE_NOT_GRANTED:I = 0xa

.field public static final FLAG_NOT_FOUND:I = 0xe

.field public static final FLAG_NO_LOGON_SERVERS:I = 0xb

.field public static final FLAG_NO_SUCH_DOMAIN:I = 0xc

.field public static final FLAG_NO_SUCH_USER:I = 0xd

.field public static final FLAG_PASSWORD_EXPIRED:I = 0xf

.field public static final FLAG_PASSWORD_MUST_CHANGE:I = 0x10

.field public static final FLAG_REQUEST_NOT_ACCEPTED:I = 0x11

.field public static final FLAG_SCANING:I = 0x2

.field public static final FLAG_STARTING:I = 0x1

.field public static final FLAG_UNKNOWHOST:I = 0x5

.field public static final FLAG_WRONG_PASSWORD:I = 0x12


# virtual methods
.method public abstract callback(Lcom/mstar/SambaFile;I)V
.end method
