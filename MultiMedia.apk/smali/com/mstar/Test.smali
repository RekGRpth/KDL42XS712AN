.class public Lcom/mstar/Test;
.super Ljava/lang/Object;
.source "Test.java"


# static fields
.field static bEnd:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/mstar/Test;->bEnd:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static main([Ljava/lang/String;)V
    .locals 23
    .param p0    # [Ljava/lang/String;

    invoke-static {}, Lcom/mstar/SmbClient;->initSamba()V

    new-instance v8, Lcom/mstar/SmbClient;

    invoke-direct {v8}, Lcom/mstar/SmbClient;-><init>()V

    const-string v19, "192.168.1.124"

    new-instance v20, Lcom/mstar/Test$1;

    invoke-direct/range {v20 .. v20}, Lcom/mstar/Test$1;-><init>()V

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v8, v0, v1}, Lcom/mstar/SmbClient;->scanAvailableHosts(Ljava/lang/String;Lcom/mstar/CallBack;)V

    :goto_0
    sget-object v19, Lcom/mstar/Test;->bEnd:Ljava/lang/Boolean;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v19

    if-eqz v19, :cond_1

    const/4 v9, 0x0

    sget-object v19, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v20, "get host table "

    invoke-virtual/range {v19 .. v20}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    invoke-virtual {v8}, Lcom/mstar/SmbClient;->getHostTableByPing()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v19

    if-lez v19, :cond_0

    sget-object v19, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v20, Ljava/lang/StringBuilder;

    const-string v21, " size :  "

    invoke-direct/range {v20 .. v21}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    const/4 v10, 0x0

    :goto_1
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v19

    move/from16 v0, v19

    if-lt v10, v0, :cond_2

    :cond_0
    new-instance v15, Lcom/mstar/SmbClient;

    invoke-direct {v15}, Lcom/mstar/SmbClient;-><init>()V

    new-instance v3, Lcom/mstar/NbtAuthority;

    const-string v19, "MSTARSEMI"

    const-string v20, "root"

    const-string v21, "888"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    invoke-direct {v3, v0, v1, v2}, Lcom/mstar/NbtAuthority;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string v19, "smb://UBUNTU/"

    new-instance v20, Lcom/mstar/Test$2;

    invoke-direct/range {v20 .. v20}, Lcom/mstar/Test$2;-><init>()V

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v15, v0, v3, v1}, Lcom/mstar/SmbClient;->scanAvailableHosts(Ljava/lang/String;Lcom/mstar/NbtAuthority;Lcom/mstar/CallBack;)V

    sget-object v19, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v20, "scan completed! go to next"

    invoke-virtual/range {v19 .. v20}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    const/16 v16, 0x0

    check-cast v16, [Lcom/mstar/SambaFile;

    :goto_2
    if-eqz v16, :cond_3

    const/4 v12, 0x0

    check-cast v12, [Lcom/mstar/SambaFile;

    const/16 v19, 0x0

    :try_start_0
    aget-object v19, v16, v19

    invoke-virtual/range {v19 .. v19}, Lcom/mstar/SambaFile;->listFiles()[Lcom/mstar/SambaFile;

    move-result-object v12

    const/4 v13, 0x0

    :goto_3
    array-length v0, v12

    move/from16 v19, v0
    :try_end_0
    .catch Lcom/mstar/SambaException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    move/from16 v0, v19

    if-lt v13, v0, :cond_4

    :goto_4
    const/4 v5, 0x5

    :try_start_1
    sget-object v19, Ljava/lang/System;->out:Ljava/io/PrintStream;

    aget-object v20, v12, v5

    invoke-virtual/range {v20 .. v20}, Lcom/mstar/SambaFile;->getFileName()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    aget-object v19, v12, v5

    invoke-virtual/range {v19 .. v19}, Lcom/mstar/SambaFile;->openFile()Ljava/io/InputStream;

    move-result-object v11

    const/high16 v19, 0x10000

    move/from16 v0, v19

    new-array v4, v0, [B

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v17

    new-instance v7, Ljava/io/FileOutputStream;

    new-instance v19, Ljava/lang/StringBuilder;

    const-string v20, "/home/administrator/workspace/"

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v20, v12, v5

    invoke-virtual/range {v20 .. v20}, Lcom/mstar/SambaFile;->getFileName()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-direct {v7, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    :goto_5
    invoke-virtual {v11, v4}, Ljava/io/InputStream;->read([B)I

    move-result v14

    if-gtz v14, :cond_5

    sget-object v19, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v20, Ljava/lang/StringBuilder;

    const-string v21, "\u8017\u65f6\uff1a"

    invoke-direct/range {v20 .. v21}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v21

    sub-long v21, v21, v17

    invoke-virtual/range {v20 .. v22}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "ms"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    invoke-virtual {v7}, Ljava/io/FileOutputStream;->close()V

    invoke-virtual {v11}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4

    :goto_6
    return-void

    :cond_1
    const-wide/16 v19, 0x3e8

    :try_start_2
    invoke-static/range {v19 .. v20}, Ljava/lang/Thread;->sleep(J)V

    sget-object v19, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v20, "wait..."

    invoke-virtual/range {v19 .. v20}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v6

    invoke-virtual {v6}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto/16 :goto_0

    :cond_2
    sget-object v20, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/String;

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_1

    :cond_3
    const-wide/16 v19, 0x3e8

    :try_start_3
    invoke-static/range {v19 .. v20}, Ljava/lang/Thread;->sleep(J)V

    sget-object v19, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v20, "wait..."

    invoke-virtual/range {v19 .. v20}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_1

    :goto_7
    invoke-virtual {v15}, Lcom/mstar/SmbClient;->getHostTable()[Lcom/mstar/SambaFile;

    move-result-object v16

    sget-object v19, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v20, "get share table "

    invoke-virtual/range {v19 .. v20}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_2

    :catch_1
    move-exception v6

    invoke-virtual {v6}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_7

    :cond_4
    :try_start_4
    sget-object v19, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v20, Ljava/lang/StringBuilder;

    const-string v21, " "

    invoke-direct/range {v20 .. v21}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v21, v12, v13

    invoke-virtual/range {v21 .. v21}, Lcom/mstar/SambaFile;->getPath()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_4
    .catch Lcom/mstar/SambaException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    add-int/lit8 v13, v13, 0x1

    goto/16 :goto_3

    :catch_2
    move-exception v6

    invoke-virtual {v6}, Lcom/mstar/SambaException;->printStackTrace()V

    goto/16 :goto_4

    :catch_3
    move-exception v6

    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_4

    :cond_5
    const/16 v19, 0x0

    :try_start_5
    move/from16 v0, v19

    invoke-virtual {v7, v4, v0, v14}, Ljava/io/FileOutputStream;->write([BII)V

    invoke-virtual {v7}, Ljava/io/FileOutputStream;->flush()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_4

    goto/16 :goto_5

    :catch_4
    move-exception v6

    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_6
.end method

.method public static setFlag(Ljava/lang/Boolean;)V
    .locals 0
    .param p0    # Ljava/lang/Boolean;

    sput-object p0, Lcom/mstar/Test;->bEnd:Ljava/lang/Boolean;

    return-void
.end method
