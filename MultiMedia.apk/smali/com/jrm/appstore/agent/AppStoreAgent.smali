.class public Lcom/jrm/appstore/agent/AppStoreAgent;
.super Ljava/lang/Object;
.source "AppStoreAgent.java"


# static fields
.field public static final NEED_INSTALL:I = 0x1

.field public static final NEED_UN_INSTALL:I = 0x3

.field public static final NEED_UPDATE:I = 0x2

.field public static final TAG:Ljava/lang/String;

.field private static final TIMTOUT:I = 0x5


# instance fields
.field private binder:Lcom/jrm/appstore/service/IAppStoreService;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/jrm/appstore/agent/AppStoreAgent;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jrm/appstore/agent/AppStoreAgent;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/jrm/appstore/service/IAppStoreService;)V
    .locals 0
    .param p1    # Lcom/jrm/appstore/service/IAppStoreService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/jrm/appstore/agent/AppStoreAgent;->binder:Lcom/jrm/appstore/service/IAppStoreService;

    return-void
.end method

.method private json2CompType(Ljava/lang/String;)Ljava/util/List;
    .locals 12
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/jrm/appstore/domain/ComponentSort;",
            ">;"
        }
    .end annotation

    const/4 v8, 0x0

    if-nez p1, :cond_1

    move-object v7, v8

    :cond_0
    :goto_0
    return-object v7

    :cond_1
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    :try_start_0
    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    sget-object v9, Lcom/jrm/appstore/agent/AppStoreAgent;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "err:"

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v11, "err"

    invoke-virtual {v5, v11}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v9, "err"

    invoke-virtual {v5, v9}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v9

    if-nez v9, :cond_0

    const-string v9, "bd"

    invoke-virtual {v5, v9}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    const-string v9, "tlist"

    invoke-virtual {v2, v9}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v9

    invoke-virtual {v9}, Lorg/json/JSONArray;->length()I

    move-result v4

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v4, :cond_0

    new-instance v6, Lcom/jrm/appstore/domain/ComponentSort;

    invoke-direct {v6}, Lcom/jrm/appstore/domain/ComponentSort;-><init>()V

    const-string v9, "tlist"

    invoke-virtual {v2, v9}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v9

    invoke-virtual {v9, v1}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/json/JSONObject;

    const-string v9, "Id"

    invoke-virtual {v3, v9}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9}, Lcom/jrm/appstore/domain/ComponentSort;->setTypeId(Ljava/lang/String;)V

    const-string v9, "Name"

    invoke-virtual {v3, v9}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9}, Lcom/jrm/appstore/domain/ComponentSort;->setTypeName(Ljava/lang/String;)V

    const-string v9, "Logo"

    invoke-virtual {v3, v9}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9}, Lcom/jrm/appstore/domain/ComponentSort;->setOther(Ljava/lang/String;)V

    sget-object v9, Lcom/jrm/appstore/agent/AppStoreAgent;->TAG:Ljava/lang/String;

    invoke-virtual {v6}, Lcom/jrm/appstore/domain/ComponentSort;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-interface {v7, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :catch_0
    move-exception v0

    sget-object v9, Lcom/jrm/appstore/agent/AppStoreAgent;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "AGENT_GET_TYPES_ERROR:"

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v7, v8

    goto/16 :goto_0
.end method

.method private json2Components(Ljava/lang/String;)Ljava/util/List;
    .locals 21
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/jrm/appstore/domain/Component;",
            ">;"
        }
    .end annotation

    if-nez p1, :cond_1

    const/4 v4, 0x0

    :cond_0
    :goto_0
    return-object v4

    :cond_1
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    :try_start_0
    new-instance v15, Lorg/json/JSONObject;

    move-object/from16 v0, p1

    invoke-direct {v15, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v18, "err"

    move-object/from16 v0, v18

    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v18

    if-nez v18, :cond_8

    const-string v18, "bd"

    move-object/from16 v0, v18

    invoke-virtual {v15, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v13

    const-string v18, "apkList"

    move-object/from16 v0, v18

    invoke-virtual {v13, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lorg/json/JSONArray;->length()I

    move-result v14

    const/4 v9, 0x0

    :goto_1
    if-ge v9, v14, :cond_0

    new-instance v3, Lcom/jrm/appstore/domain/Component;

    invoke-direct {v3}, Lcom/jrm/appstore/domain/Component;-><init>()V

    const-string v18, "apkList"

    move-object/from16 v0, v18

    invoke-virtual {v13, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v9}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/json/JSONObject;

    const-string v18, "id"

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Lcom/jrm/appstore/domain/Component;->setComId(Ljava/lang/String;)V

    const-string v18, "na"

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Lcom/jrm/appstore/domain/Component;->setCompName(Ljava/lang/String;)V

    const-string v18, "vershow"

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Lcom/jrm/appstore/domain/Component;->setVersionName(Ljava/lang/String;)V

    const-string v18, "ver"

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v18

    move/from16 v0, v18

    invoke-virtual {v3, v0}, Lcom/jrm/appstore/domain/Component;->setVersionCode(I)V

    const-string v18, "des"

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Lcom/jrm/appstore/domain/Component;->setCompDescribe(Ljava/lang/String;)V

    const-string v18, "pro"

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Lcom/jrm/appstore/domain/Component;->setCompDeveloper(Ljava/lang/String;)V

    const-string v18, "ico"

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Lcom/jrm/appstore/domain/Component;->setCompLogoURL(Ljava/lang/String;)V

    const-string v18, "Imgs"

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v6

    invoke-virtual {v6}, Lorg/json/JSONArray;->length()I

    move-result v10

    new-array v11, v10, [Ljava/lang/String;

    const/4 v12, 0x0

    :goto_2
    invoke-virtual {v6}, Lorg/json/JSONArray;->length()I

    move-result v18

    move/from16 v0, v18

    if-lt v12, v0, :cond_3

    invoke-virtual {v3, v11}, Lcom/jrm/appstore/domain/Component;->setCompPrintScreenURL([Ljava/lang/String;)V

    const-string v18, "Price"

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v18

    if-lez v18, :cond_4

    const/16 v18, 0x1

    :goto_3
    move/from16 v0, v18

    invoke-virtual {v3, v0}, Lcom/jrm/appstore/domain/Component;->setCompCharge(Z)V

    const-string v18, "des"

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Lcom/jrm/appstore/domain/Component;->setCompDescribe(Ljava/lang/String;)V

    const-string v18, "url"

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Lcom/jrm/appstore/domain/Component;->setCompDownloadURL(Ljava/lang/String;)V

    const-string v18, "md5"

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Lcom/jrm/appstore/domain/Component;->setMd5(Ljava/lang/String;)V

    const-string v18, "typ"

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v8

    invoke-virtual {v8}, Lorg/json/JSONArray;->length()I

    move-result v17

    if-lez v17, :cond_9

    new-instance v16, Ljava/lang/StringBuffer;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuffer;-><init>()V

    const/4 v12, 0x0

    :goto_4
    move/from16 v0, v17

    if-lt v12, v0, :cond_5

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->length()I

    move-result v18

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-le v0, v1, :cond_2

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v18

    const/16 v19, 0x0

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->length()I

    move-result v20

    add-int/lit8 v20, v20, -0x1

    invoke-virtual/range {v18 .. v20}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Lcom/jrm/appstore/domain/Component;->setCompType(Ljava/lang/String;)V

    :cond_2
    :goto_5
    const-string v18, "grd"

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v18

    move/from16 v0, v18

    invoke-virtual {v3, v0}, Lcom/jrm/appstore/domain/Component;->setCompStar(I)V

    const-string v18, "size"

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v18

    move/from16 v0, v18

    invoke-virtual {v3, v0}, Lcom/jrm/appstore/domain/Component;->setCompSize(I)V

    const-string v18, "lagh"

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Lcom/jrm/appstore/domain/Component;->setCompActClass(Ljava/lang/String;)V

    const-string v18, "pkg"

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Lcom/jrm/appstore/domain/Component;->setCompPackage(Ljava/lang/String;)V

    sget-object v18, Lcom/jrm/appstore/agent/AppStoreAgent;->TAG:Ljava/lang/String;

    invoke-virtual {v3}, Lcom/jrm/appstore/domain/Component;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_1

    :cond_3
    invoke-virtual {v6, v12}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/String;

    aput-object v18, v11, v12

    add-int/lit8 v12, v12, 0x1

    goto/16 :goto_2

    :cond_4
    const/16 v18, 0x0

    goto/16 :goto_3

    :cond_5
    invoke-virtual {v8, v12}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v7

    const-string v18, "NM"

    move-object/from16 v0, v18

    invoke-virtual {v7, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_7

    new-instance v18, Ljava/lang/StringBuilder;

    const-string v19, "NM"

    move-object/from16 v0, v19

    invoke-virtual {v7, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v19

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v19, "|"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_6
    :goto_6
    add-int/lit8 v12, v12, 0x1

    goto/16 :goto_4

    :cond_7
    const-string v18, "DEVEL_NAME"

    move-object/from16 v0, v18

    invoke-virtual {v7, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_6

    new-instance v18, Ljava/lang/StringBuilder;

    const-string v19, "DEVEL_NAME"

    move-object/from16 v0, v19

    invoke-virtual {v7, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v19

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v19, "|"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_6

    :catch_0
    move-exception v5

    invoke-virtual {v5}, Lorg/json/JSONException;->printStackTrace()V

    sget-object v18, Lcom/jrm/appstore/agent/AppStoreAgent;->TAG:Ljava/lang/String;

    new-instance v19, Ljava/lang/StringBuilder;

    const-string v20, "ERROR_JSON_COMPONENTS:"

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_8
    const/4 v4, 0x0

    goto/16 :goto_0

    :cond_9
    :try_start_1
    const-string v18, "unknow"

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Lcom/jrm/appstore/domain/Component;->setCompType(Ljava/lang/String;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_5
.end method


# virtual methods
.method public getAdInfo(Ljava/lang/String;)Ljava/util/List;
    .locals 13
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/jrm/appstore/domain/AD;",
            ">;"
        }
    .end annotation

    const/4 v9, 0x0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    :try_start_0
    iget-object v10, p0, Lcom/jrm/appstore/agent/AppStoreAgent;->binder:Lcom/jrm/appstore/service/IAppStoreService;

    if-nez v10, :cond_1

    move-object v1, v9

    :cond_0
    :goto_0
    return-object v1

    :cond_1
    iget-object v10, p0, Lcom/jrm/appstore/agent/AppStoreAgent;->binder:Lcom/jrm/appstore/service/IAppStoreService;

    const/4 v11, 0x5

    invoke-interface {v10, p1, v11}, Lcom/jrm/appstore/service/IAppStoreService;->getAdInfo(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v6

    if-nez v6, :cond_2

    move-object v1, v9

    goto :goto_0

    :cond_2
    new-instance v8, Lorg/json/JSONObject;

    invoke-direct {v8, v6}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v10, "err"

    invoke-virtual {v8, v10}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v10

    if-nez v10, :cond_0

    const-string v10, "bd"

    invoke-virtual {v8, v10}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v10, "ads"

    invoke-virtual {v4, v10}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v10

    invoke-virtual {v10}, Lorg/json/JSONArray;->length()I

    move-result v7

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v7, :cond_0

    const-string v10, "ads"

    invoke-virtual {v4, v10}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v10

    invoke-virtual {v10, v3}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/json/JSONObject;

    new-instance v0, Lcom/jrm/appstore/domain/AD;

    invoke-direct {v0}, Lcom/jrm/appstore/domain/AD;-><init>()V

    const-string v10, "id"

    invoke-virtual {v5, v10}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v10

    invoke-virtual {v0, v10}, Lcom/jrm/appstore/domain/AD;->setAdId(I)V

    const-string v10, "nm"

    invoke-virtual {v5, v10}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v10}, Lcom/jrm/appstore/domain/AD;->setAdName(Ljava/lang/String;)V

    const-string v10, "url"

    invoke-virtual {v5, v10}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v10}, Lcom/jrm/appstore/domain/AD;->setLogoURL(Ljava/lang/String;)V

    const-string v10, "ac"

    invoke-virtual {v5, v10}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v10}, Lcom/jrm/appstore/domain/AD;->setAdLocation(Ljava/lang/String;)V

    const-string v10, "ds"

    invoke-virtual {v5, v10}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v10}, Lcom/jrm/appstore/domain/AD;->setAdDescribe(Ljava/lang/String;)V

    const-string v10, "pkg"

    invoke-virtual {v5, v10}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_3

    const-string v10, "pkg"

    invoke-virtual {v5, v10}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v10}, Lcom/jrm/appstore/domain/AD;->setPkgName(Ljava/lang/String;)V

    :cond_3
    sget-object v10, Lcom/jrm/appstore/agent/AppStoreAgent;->TAG:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/jrm/appstore/domain/AD;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Landroid/os/RemoteException;->printStackTrace()V

    sget-object v10, Lcom/jrm/appstore/agent/AppStoreAgent;->TAG:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "AGENT_AD_ERROR:"

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v9

    goto/16 :goto_0

    :catch_1
    move-exception v2

    invoke-virtual {v2}, Lorg/json/JSONException;->printStackTrace()V

    sget-object v10, Lcom/jrm/appstore/agent/AppStoreAgent;->TAG:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "AGENT_AD_ERROR:"

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v9

    goto/16 :goto_0
.end method

.method public getAppByPackage(Ljava/lang/String;)Lcom/jrm/appstore/domain/Component;
    .locals 9
    .param p1    # Ljava/lang/String;

    const/4 v4, 0x0

    :try_start_0
    iget-object v3, p0, Lcom/jrm/appstore/agent/AppStoreAgent;->binder:Lcom/jrm/appstore/service/IAppStoreService;

    if-nez v3, :cond_0

    move-object v3, v4

    :goto_0
    return-object v3

    :cond_0
    iget-object v3, p0, Lcom/jrm/appstore/agent/AppStoreAgent;->binder:Lcom/jrm/appstore/service/IAppStoreService;

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object p1, v5, v6

    const-string v6, "dnc"

    const/4 v7, 0x1

    const/4 v8, 0x5

    invoke-interface {v3, v5, v6, v7, v8}, Lcom/jrm/appstore/service/IAppStoreService;->getAppsByPackageNames([Ljava/lang/String;Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/jrm/appstore/agent/AppStoreAgent;->json2Components(Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_1

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/jrm/appstore/domain/Component;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :cond_1
    move-object v3, v4

    goto :goto_0

    :catch_0
    move-exception v0

    sget-object v3, Lcom/jrm/appstore/agent/AppStoreAgent;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "AGENT_GET_APP_BY_PKG_ERROR:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v3, v4

    goto :goto_0
.end method

.method public getAppStatus(Lcom/jrm/appstore/domain/Component;)I
    .locals 5
    .param p1    # Lcom/jrm/appstore/domain/Component;

    :try_start_0
    sget-object v2, Lcom/jrm/appstore/agent/AppStoreAgent;->TAG:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/jrm/appstore/domain/Component;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/jrm/appstore/agent/AppStoreAgent;->binder:Lcom/jrm/appstore/service/IAppStoreService;

    invoke-virtual {p1}, Lcom/jrm/appstore/domain/Component;->getCompPackage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/jrm/appstore/domain/Component;->getVersionCode()I

    move-result v4

    invoke-interface {v2, v3, v4}, Lcom/jrm/appstore/service/IAppStoreService;->getAppStatus(Ljava/lang/String;I)I

    move-result v1

    sget-object v2, Lcom/jrm/appstore/agent/AppStoreAgent;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "iiiiiiiiiiiiiii:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return v1

    :catch_0
    move-exception v0

    sget-object v2, Lcom/jrm/appstore/agent/AppStoreAgent;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "AGENT_GET_APP_STATUS_ERROR:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getAppTypes()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/jrm/appstore/domain/ComponentSort;",
            ">;"
        }
    .end annotation

    const/4 v2, 0x0

    :try_start_0
    iget-object v3, p0, Lcom/jrm/appstore/agent/AppStoreAgent;->binder:Lcom/jrm/appstore/service/IAppStoreService;

    if-nez v3, :cond_0

    :goto_0
    return-object v2

    :cond_0
    iget-object v3, p0, Lcom/jrm/appstore/agent/AppStoreAgent;->binder:Lcom/jrm/appstore/service/IAppStoreService;

    const/4 v4, 0x5

    invoke-interface {v3, v4}, Lcom/jrm/appstore/service/IAppStoreService;->getAppTypes(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/jrm/appstore/agent/AppStoreAgent;->json2CompType(Ljava/lang/String;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    goto :goto_0

    :catch_0
    move-exception v0

    sget-object v3, Lcom/jrm/appstore/agent/AppStoreAgent;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "AGENT_GET_APP_TYPES_ERROR:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getApps([Ljava/lang/String;III)Ljava/util/List;
    .locals 10
    .param p1    # [Ljava/lang/String;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            "III)",
            "Ljava/util/List",
            "<",
            "Lcom/jrm/appstore/domain/Component;",
            ">;"
        }
    .end annotation

    const/4 v9, 0x0

    :try_start_0
    iget-object v0, p0, Lcom/jrm/appstore/agent/AppStoreAgent;->binder:Lcom/jrm/appstore/service/IAppStoreService;

    if-nez v0, :cond_0

    move-object v0, v9

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/jrm/appstore/agent/AppStoreAgent;->binder:Lcom/jrm/appstore/service/IAppStoreService;

    const-string v4, "dnc"

    const/4 v6, 0x5

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v5, p4

    invoke-interface/range {v0 .. v6}, Lcom/jrm/appstore/service/IAppStoreService;->getApps([Ljava/lang/String;IILjava/lang/String;II)Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v8}, Lcom/jrm/appstore/agent/AppStoreAgent;->json2Components(Ljava/lang/String;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v7

    sget-object v0, Lcom/jrm/appstore/agent/AppStoreAgent;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AGENT_GET_APPS_ERROR:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v9

    goto :goto_0
.end method

.method public getAppsByPackageNames([Ljava/lang/String;I)Ljava/util/List;
    .locals 6
    .param p1    # [Ljava/lang/String;
    .param p2    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/List",
            "<",
            "Lcom/jrm/appstore/domain/Component;",
            ">;"
        }
    .end annotation

    const/4 v2, 0x0

    :try_start_0
    iget-object v3, p0, Lcom/jrm/appstore/agent/AppStoreAgent;->binder:Lcom/jrm/appstore/service/IAppStoreService;

    if-nez v3, :cond_0

    :goto_0
    return-object v2

    :cond_0
    iget-object v3, p0, Lcom/jrm/appstore/agent/AppStoreAgent;->binder:Lcom/jrm/appstore/service/IAppStoreService;

    const-string v4, "dnc"

    const/4 v5, 0x5

    invoke-interface {v3, p1, v4, p2, v5}, Lcom/jrm/appstore/service/IAppStoreService;->getAppsByPackageNames([Ljava/lang/String;Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/jrm/appstore/agent/AppStoreAgent;->json2Components(Ljava/lang/String;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    goto :goto_0

    :catch_0
    move-exception v0

    sget-object v3, Lcom/jrm/appstore/agent/AppStoreAgent;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "AGENT_GET_APPS_BY_PKG_ERROR:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getAppsByType(Ljava/lang/String;[Ljava/lang/String;III)Ljava/util/List;
    .locals 10
    .param p1    # Ljava/lang/String;
    .param p2    # [Ljava/lang/String;
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            "III)",
            "Ljava/util/List",
            "<",
            "Lcom/jrm/appstore/domain/Component;",
            ">;"
        }
    .end annotation

    :try_start_0
    iget-object v0, p0, Lcom/jrm/appstore/agent/AppStoreAgent;->binder:Lcom/jrm/appstore/service/IAppStoreService;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/jrm/appstore/agent/AppStoreAgent;->binder:Lcom/jrm/appstore/service/IAppStoreService;

    const-string v5, "dnc"

    const/4 v7, 0x5

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v6, p5

    invoke-interface/range {v0 .. v7}, Lcom/jrm/appstore/service/IAppStoreService;->getAppsByType(Ljava/lang/String;[Ljava/lang/String;IILjava/lang/String;II)Ljava/lang/String;

    move-result-object v9

    invoke-direct {p0, v9}, Lcom/jrm/appstore/agent/AppStoreAgent;->json2Components(Ljava/lang/String;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v8

    sget-object v0, Lcom/jrm/appstore/agent/AppStoreAgent;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AGENT_GET_APP_BY_TYPE_ERROR:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getInstallDeal()Ljava/lang/String;
    .locals 7

    const/4 v4, 0x0

    :try_start_0
    iget-object v5, p0, Lcom/jrm/appstore/agent/AppStoreAgent;->binder:Lcom/jrm/appstore/service/IAppStoreService;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    if-nez v5, :cond_1

    const-string v4, "\u83b7\u53d6\u5b89\u88c5\u534f\u8bae\u5931\u8d25."

    :cond_0
    :goto_0
    return-object v4

    :cond_1
    :try_start_1
    iget-object v5, p0, Lcom/jrm/appstore/agent/AppStoreAgent;->binder:Lcom/jrm/appstore/service/IAppStoreService;

    const/4 v6, 0x5

    invoke-interface {v5, v6}, Lcom/jrm/appstore/service/IAppStoreService;->getInstallDeal(I)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, v3}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v5, "bd"

    invoke-virtual {v1, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    const-string v5, "value"

    invoke-virtual {v2, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v4

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    const-string v4, "\u89e3\u6790\u51fa\u9519."

    goto :goto_0
.end method

.method public getMyApps(I)Ljava/util/List;
    .locals 9
    .param p1    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lcom/jrm/appstore/domain/Component;",
            ">;"
        }
    .end annotation

    const/4 v8, 0x0

    :try_start_0
    iget-object v0, p0, Lcom/jrm/appstore/agent/AppStoreAgent;->binder:Lcom/jrm/appstore/service/IAppStoreService;

    if-nez v0, :cond_0

    move-object v0, v8

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/jrm/appstore/agent/AppStoreAgent;->binder:Lcom/jrm/appstore/service/IAppStoreService;

    const/16 v1, 0x9

    const/4 v2, 0x1

    const-string v3, "dnc"

    const/4 v5, 0x5

    move v4, p1

    invoke-interface/range {v0 .. v5}, Lcom/jrm/appstore/service/IAppStoreService;->getMyApps(IILjava/lang/String;II)Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/jrm/appstore/agent/AppStoreAgent;->json2Components(Ljava/lang/String;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v6

    invoke-virtual {v6}, Landroid/os/RemoteException;->printStackTrace()V

    sget-object v0, Lcom/jrm/appstore/agent/AppStoreAgent;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AGENT_GET_MY_APP:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v8

    goto :goto_0
.end method

.method public getRecommendApps()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/jrm/appstore/domain/Component;",
            ">;"
        }
    .end annotation

    const/4 v2, 0x0

    :try_start_0
    iget-object v3, p0, Lcom/jrm/appstore/agent/AppStoreAgent;->binder:Lcom/jrm/appstore/service/IAppStoreService;

    if-nez v3, :cond_0

    :goto_0
    return-object v2

    :cond_0
    iget-object v3, p0, Lcom/jrm/appstore/agent/AppStoreAgent;->binder:Lcom/jrm/appstore/service/IAppStoreService;

    const/4 v4, 0x5

    invoke-interface {v3, v4}, Lcom/jrm/appstore/service/IAppStoreService;->getRecommendApps(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/jrm/appstore/agent/AppStoreAgent;->json2Components(Ljava/lang/String;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    sget-object v3, Lcom/jrm/appstore/agent/AppStoreAgent;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "AGENT_RECOMMAND_ERROR:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public installORupdateAPK(Lcom/jrm/appstore/domain/Component;Lcom/jrm/appstore/service/IInstallStatusListener;)V
    .locals 5
    .param p1    # Lcom/jrm/appstore/domain/Component;
    .param p2    # Lcom/jrm/appstore/service/IInstallStatusListener;

    :try_start_0
    iget-object v1, p0, Lcom/jrm/appstore/agent/AppStoreAgent;->binder:Lcom/jrm/appstore/service/IAppStoreService;

    invoke-virtual {p1}, Lcom/jrm/appstore/domain/Component;->getCompName()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/jrm/appstore/domain/Component;->getVersionName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3, p2}, Lcom/jrm/appstore/service/IAppStoreService;->installORupdateAPK(Ljava/lang/String;Ljava/lang/String;Lcom/jrm/appstore/service/IInstallStatusListener;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    sget-object v1, Lcom/jrm/appstore/agent/AppStoreAgent;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "AGENT_INSTALL_OR_UPDATE_ERROR:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public isCompLegal(Lcom/jrm/appstore/domain/Component;)Z
    .locals 4
    .param p1    # Lcom/jrm/appstore/domain/Component;

    :try_start_0
    iget-object v1, p0, Lcom/jrm/appstore/agent/AppStoreAgent;->binder:Lcom/jrm/appstore/service/IAppStoreService;

    invoke-virtual {p1}, Lcom/jrm/appstore/domain/Component;->getCompName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/jrm/appstore/service/IAppStoreService;->isCompLegal(Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    sget-object v1, Lcom/jrm/appstore/agent/AppStoreAgent;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "AGENT_COMP_LEGAL_ERROR:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public unInstall(Lcom/jrm/appstore/domain/Component;Lcom/jrm/appstore/service/IInstallStatusListener;)V
    .locals 4
    .param p1    # Lcom/jrm/appstore/domain/Component;
    .param p2    # Lcom/jrm/appstore/service/IInstallStatusListener;

    :try_start_0
    iget-object v1, p0, Lcom/jrm/appstore/agent/AppStoreAgent;->binder:Lcom/jrm/appstore/service/IAppStoreService;

    invoke-virtual {p1}, Lcom/jrm/appstore/domain/Component;->getCompName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, p2}, Lcom/jrm/appstore/service/IAppStoreService;->unInstall(Ljava/lang/String;Lcom/jrm/appstore/service/IInstallStatusListener;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    sget-object v1, Lcom/jrm/appstore/agent/AppStoreAgent;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "AGENT_UNINSTALL_ERROR:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
