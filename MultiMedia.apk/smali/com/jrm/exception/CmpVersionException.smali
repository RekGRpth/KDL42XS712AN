.class public Lcom/jrm/exception/CmpVersionException;
.super Lcom/jrm/exception/JRMException;
.source "CmpVersionException.java"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private state:I


# direct methods
.method public constructor <init>(I)V
    .locals 0
    .param p1    # I

    invoke-direct {p0}, Lcom/jrm/exception/JRMException;-><init>()V

    iput p1, p0, Lcom/jrm/exception/CmpVersionException;->state:I

    return-void
.end method


# virtual methods
.method public getState()I
    .locals 1

    iget v0, p0, Lcom/jrm/exception/CmpVersionException;->state:I

    return v0
.end method
