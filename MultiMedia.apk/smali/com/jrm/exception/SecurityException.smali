.class public Lcom/jrm/exception/SecurityException;
.super Lcom/jrm/exception/JRMException;
.source "SecurityException.java"


# static fields
.field public static final ACCESS_FORBID:I = 0x4

.field public static final MAC_NOT_AUTHORIZE:I = 0x1

.field public static final PRODUCT_EXPIRED:I = 0x3

.field public static final PRODUCT_NOT_AUTHORIZE:I = 0x2

.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private authExceptionCode:I


# direct methods
.method public constructor <init>(I)V
    .locals 0
    .param p1    # I

    invoke-direct {p0}, Lcom/jrm/exception/JRMException;-><init>()V

    iput p1, p0, Lcom/jrm/exception/SecurityException;->authExceptionCode:I

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;)V
    .locals 0
    .param p1    # I
    .param p2    # Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/jrm/exception/JRMException;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public getAuthExceptionCode()I
    .locals 1

    iget v0, p0, Lcom/jrm/exception/SecurityException;->authExceptionCode:I

    return v0
.end method
