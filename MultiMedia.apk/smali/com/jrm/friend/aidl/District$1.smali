.class Lcom/jrm/friend/aidl/District$1;
.super Ljava/lang/Object;
.source "District.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jrm/friend/aidl/District;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/jrm/friend/aidl/District;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/jrm/friend/aidl/District;
    .locals 4
    .param p1    # Landroid/os/Parcel;

    new-instance v0, Lcom/jrm/friend/aidl/District;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/jrm/friend/aidl/District;-><init>(IILjava/lang/String;)V

    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/jrm/friend/aidl/District$1;->createFromParcel(Landroid/os/Parcel;)Lcom/jrm/friend/aidl/District;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/jrm/friend/aidl/District;
    .locals 1
    .param p1    # I

    new-array v0, p1, [Lcom/jrm/friend/aidl/District;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/jrm/friend/aidl/District$1;->newArray(I)[Lcom/jrm/friend/aidl/District;

    move-result-object v0

    return-object v0
.end method
