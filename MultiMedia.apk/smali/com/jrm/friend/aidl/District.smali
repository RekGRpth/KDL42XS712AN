.class public Lcom/jrm/friend/aidl/District;
.super Ljava/lang/Object;
.source "District.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/jrm/friend/aidl/District;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mDistrictId:I

.field private mDistrictName:Ljava/lang/String;

.field private mProvinceId:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/jrm/friend/aidl/District$1;

    invoke-direct {v0}, Lcom/jrm/friend/aidl/District$1;-><init>()V

    sput-object v0, Lcom/jrm/friend/aidl/District;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(IILjava/lang/String;)V
    .locals 0
    .param p1    # I
    .param p2    # I
    .param p3    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/jrm/friend/aidl/District;->mProvinceId:I

    iput p2, p0, Lcom/jrm/friend/aidl/District;->mDistrictId:I

    iput-object p3, p0, Lcom/jrm/friend/aidl/District;->mDistrictName:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getDistrictId()I
    .locals 1

    iget v0, p0, Lcom/jrm/friend/aidl/District;->mDistrictId:I

    return v0
.end method

.method public getDistrictName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jrm/friend/aidl/District;->mDistrictName:Ljava/lang/String;

    return-object v0
.end method

.method public getProvinceId()I
    .locals 1

    iget v0, p0, Lcom/jrm/friend/aidl/District;->mProvinceId:I

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget v0, p0, Lcom/jrm/friend/aidl/District;->mProvinceId:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/jrm/friend/aidl/District;->mDistrictId:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/jrm/friend/aidl/District;->mDistrictName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method
