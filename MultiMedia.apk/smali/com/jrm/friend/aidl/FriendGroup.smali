.class public Lcom/jrm/friend/aidl/FriendGroup;
.super Ljava/lang/Object;
.source "FriendGroup.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/jrm/friend/aidl/FriendGroup;",
            ">;"
        }
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String; = "FriendGroup"


# instance fields
.field private mFriendGroupCreateTime:J

.field private mFriendGroupModifyTime:J

.field private mFriendGroupName:Ljava/lang/String;

.field private mFriengGroupId:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/jrm/friend/aidl/FriendGroup$1;

    invoke-direct {v0}, Lcom/jrm/friend/aidl/FriendGroup$1;-><init>()V

    sput-object v0, Lcom/jrm/friend/aidl/FriendGroup;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;JJ)V
    .locals 0
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # J
    .param p5    # J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/jrm/friend/aidl/FriendGroup;->mFriengGroupId:I

    iput-object p2, p0, Lcom/jrm/friend/aidl/FriendGroup;->mFriendGroupName:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getGroupCreateTime()J
    .locals 2

    iget-wide v0, p0, Lcom/jrm/friend/aidl/FriendGroup;->mFriendGroupCreateTime:J

    return-wide v0
.end method

.method public getGroupId()I
    .locals 1

    iget v0, p0, Lcom/jrm/friend/aidl/FriendGroup;->mFriengGroupId:I

    return v0
.end method

.method public getGroupModifyTime()J
    .locals 2

    iget-wide v0, p0, Lcom/jrm/friend/aidl/FriendGroup;->mFriendGroupModifyTime:J

    return-wide v0
.end method

.method public getGroupName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jrm/friend/aidl/FriendGroup;->mFriendGroupName:Ljava/lang/String;

    return-object v0
.end method

.method public setGroupModifyTime(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/jrm/friend/aidl/FriendGroup;->mFriendGroupModifyTime:J

    return-void
.end method

.method public setGroupName(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/jrm/friend/aidl/FriendGroup;->mFriendGroupName:Ljava/lang/String;

    if-eqz p1, :cond_0

    iput-object p1, p0, Lcom/jrm/friend/aidl/FriendGroup;->mFriendGroupName:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "FriendGroup:  setGroupNameFailed, since the group name is null!"

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget v0, p0, Lcom/jrm/friend/aidl/FriendGroup;->mFriengGroupId:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/jrm/friend/aidl/FriendGroup;->mFriendGroupName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-wide v0, p0, Lcom/jrm/friend/aidl/FriendGroup;->mFriendGroupModifyTime:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    iget-wide v0, p0, Lcom/jrm/friend/aidl/FriendGroup;->mFriendGroupCreateTime:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    return-void
.end method
