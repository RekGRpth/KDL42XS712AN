.class public interface abstract Lcom/jrm/friend/aidl/IFriendDataFreshListener;
.super Ljava/lang/Object;
.source "IFriendDataFreshListener.java"

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/jrm/friend/aidl/IFriendDataFreshListener$Stub;
    }
.end annotation


# virtual methods
.method public abstract refreshBegine()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract refreshFaild(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract refreshFinish()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
