.class Lcom/jrm/friend/binder/JRMDistrictService$DistrinctDataFreshListener;
.super Lcom/jrm/friend/aidl/IDistrictDataRefreshListener$Stub;
.source "JRMDistrictService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jrm/friend/binder/JRMDistrictService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DistrinctDataFreshListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/friend/binder/JRMDistrictService;


# direct methods
.method private constructor <init>(Lcom/jrm/friend/binder/JRMDistrictService;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/friend/binder/JRMDistrictService$DistrinctDataFreshListener;->this$0:Lcom/jrm/friend/binder/JRMDistrictService;

    invoke-direct {p0}, Lcom/jrm/friend/aidl/IDistrictDataRefreshListener$Stub;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/jrm/friend/binder/JRMDistrictService;Lcom/jrm/friend/binder/JRMDistrictService$DistrinctDataFreshListener;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/jrm/friend/binder/JRMDistrictService$DistrinctDataFreshListener;-><init>(Lcom/jrm/friend/binder/JRMDistrictService;)V

    return-void
.end method


# virtual methods
.method public districtDataRefreshBegine()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jrm/friend/binder/JRMDistrictService$DistrinctDataFreshListener;->this$0:Lcom/jrm/friend/binder/JRMDistrictService;

    # getter for: Lcom/jrm/friend/binder/JRMDistrictService;->handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/jrm/friend/binder/JRMDistrictService;->access$0(Lcom/jrm/friend/binder/JRMDistrictService;)Landroid/os/Handler;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/friend/binder/JRMDistrictService$DistrinctDataFreshListener;->this$0:Lcom/jrm/friend/binder/JRMDistrictService;

    # getter for: Lcom/jrm/friend/binder/JRMDistrictService;->handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/jrm/friend/binder/JRMDistrictService;->access$0(Lcom/jrm/friend/binder/JRMDistrictService;)Landroid/os/Handler;

    move-result-object v0

    const v1, 0x20000001

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_0
    return-void
.end method

.method public districtDataRefreshEnd()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jrm/friend/binder/JRMDistrictService$DistrinctDataFreshListener;->this$0:Lcom/jrm/friend/binder/JRMDistrictService;

    # getter for: Lcom/jrm/friend/binder/JRMDistrictService;->handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/jrm/friend/binder/JRMDistrictService;->access$0(Lcom/jrm/friend/binder/JRMDistrictService;)Landroid/os/Handler;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/friend/binder/JRMDistrictService$DistrinctDataFreshListener;->this$0:Lcom/jrm/friend/binder/JRMDistrictService;

    # getter for: Lcom/jrm/friend/binder/JRMDistrictService;->handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/jrm/friend/binder/JRMDistrictService;->access$0(Lcom/jrm/friend/binder/JRMDistrictService;)Landroid/os/Handler;

    move-result-object v0

    const v1, 0x20000002

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_0
    return-void
.end method
