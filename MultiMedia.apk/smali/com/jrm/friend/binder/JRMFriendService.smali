.class public Lcom/jrm/friend/binder/JRMFriendService;
.super Ljava/lang/Object;
.source "JRMFriendService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/jrm/friend/binder/JRMFriendService$FriendFreshDataListener;
    }
.end annotation


# static fields
.field private static final ADDFRIEND:I = 0x1000003

.field private static final DELETEFRIEND:I = 0x1000001

.field private static final MODIFYFRIEND:I = 0x1000005

.field private static final SEARCHFRIEND:I = 0x1000007


# instance fields
.field private handler:Landroid/os/Handler;

.field private mAddFriendListener:Lcom/jrm/friend/listener/OnAddFriendListener;

.field private mDeleteFriendListener:Lcom/jrm/friend/listener/OnDeleteFriendListener;

.field private mFriendManager:Lcom/jrm/friend/aidl/IFriendManagerService;

.field private mHandler:Landroid/os/Handler;

.field private mModifyFriendListener:Lcom/jrm/friend/listener/OnModifyFriendListener;


# direct methods
.method public constructor <init>(Lcom/jrm/friend/aidl/IFriendManagerService;)V
    .locals 4
    .param p1    # Lcom/jrm/friend/aidl/IFriendManagerService;

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/jrm/friend/binder/JRMFriendService;->mFriendManager:Lcom/jrm/friend/aidl/IFriendManagerService;

    iput-object v1, p0, Lcom/jrm/friend/binder/JRMFriendService;->mDeleteFriendListener:Lcom/jrm/friend/listener/OnDeleteFriendListener;

    iput-object v1, p0, Lcom/jrm/friend/binder/JRMFriendService;->mAddFriendListener:Lcom/jrm/friend/listener/OnAddFriendListener;

    iput-object v1, p0, Lcom/jrm/friend/binder/JRMFriendService;->mModifyFriendListener:Lcom/jrm/friend/listener/OnModifyFriendListener;

    new-instance v1, Lcom/jrm/friend/binder/JRMFriendService$1;

    invoke-direct {v1, p0}, Lcom/jrm/friend/binder/JRMFriendService$1;-><init>(Lcom/jrm/friend/binder/JRMFriendService;)V

    iput-object v1, p0, Lcom/jrm/friend/binder/JRMFriendService;->mHandler:Landroid/os/Handler;

    iput-object p1, p0, Lcom/jrm/friend/binder/JRMFriendService;->mFriendManager:Lcom/jrm/friend/aidl/IFriendManagerService;

    :try_start_0
    iget-object v1, p0, Lcom/jrm/friend/binder/JRMFriendService;->mFriendManager:Lcom/jrm/friend/aidl/IFriendManagerService;

    new-instance v2, Lcom/jrm/friend/binder/JRMFriendService$FriendFreshDataListener;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/jrm/friend/binder/JRMFriendService$FriendFreshDataListener;-><init>(Lcom/jrm/friend/binder/JRMFriendService;Lcom/jrm/friend/binder/JRMFriendService$FriendFreshDataListener;)V

    invoke-interface {v1, v2}, Lcom/jrm/friend/aidl/IFriendManagerService;->addDataRefreshListener(Lcom/jrm/friend/aidl/IFriendDataFreshListener;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method static synthetic access$0(Lcom/jrm/friend/binder/JRMFriendService;)Lcom/jrm/friend/listener/OnDeleteFriendListener;
    .locals 1

    iget-object v0, p0, Lcom/jrm/friend/binder/JRMFriendService;->mDeleteFriendListener:Lcom/jrm/friend/listener/OnDeleteFriendListener;

    return-object v0
.end method

.method static synthetic access$1(Lcom/jrm/friend/binder/JRMFriendService;)Lcom/jrm/friend/listener/OnAddFriendListener;
    .locals 1

    iget-object v0, p0, Lcom/jrm/friend/binder/JRMFriendService;->mAddFriendListener:Lcom/jrm/friend/listener/OnAddFriendListener;

    return-object v0
.end method

.method static synthetic access$2(Lcom/jrm/friend/binder/JRMFriendService;)Lcom/jrm/friend/listener/OnModifyFriendListener;
    .locals 1

    iget-object v0, p0, Lcom/jrm/friend/binder/JRMFriendService;->mModifyFriendListener:Lcom/jrm/friend/listener/OnModifyFriendListener;

    return-object v0
.end method

.method static synthetic access$3(Lcom/jrm/friend/binder/JRMFriendService;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/jrm/friend/binder/JRMFriendService;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$4(Lcom/jrm/friend/binder/JRMFriendService;)Lcom/jrm/friend/aidl/IFriendManagerService;
    .locals 1

    iget-object v0, p0, Lcom/jrm/friend/binder/JRMFriendService;->mFriendManager:Lcom/jrm/friend/aidl/IFriendManagerService;

    return-object v0
.end method

.method static synthetic access$5(Lcom/jrm/friend/binder/JRMFriendService;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/jrm/friend/binder/JRMFriendService;->mHandler:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public addFriend(II)Z
    .locals 4
    .param p1    # I
    .param p2    # I

    iget-object v2, p0, Lcom/jrm/friend/binder/JRMFriendService;->mFriendManager:Lcom/jrm/friend/aidl/IFriendManagerService;

    if-eqz v2, :cond_0

    new-instance v2, Ljava/lang/Thread;

    new-instance v3, Lcom/jrm/friend/binder/JRMFriendService$2;

    invoke-direct {v3, p0, p1, p2}, Lcom/jrm/friend/binder/JRMFriendService$2;-><init>(Lcom/jrm/friend/binder/JRMFriendService;II)V

    invoke-direct {v2, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    :goto_0
    const/4 v2, 0x1

    return v2

    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "result"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    new-instance v1, Landroid/os/Message;

    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    const v2, 0x1000003

    iput v2, v1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v2, p0, Lcom/jrm/friend/binder/JRMFriendService;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public addFriendGroup([Ljava/lang/String;[Ljava/lang/String;)Z
    .locals 2
    .param p1    # [Ljava/lang/String;
    .param p2    # [Ljava/lang/String;

    iget-object v1, p0, Lcom/jrm/friend/binder/JRMFriendService;->mFriendManager:Lcom/jrm/friend/aidl/IFriendManagerService;

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/jrm/friend/binder/JRMFriendService;->mFriendManager:Lcom/jrm/friend/aidl/IFriendManagerService;

    invoke-interface {v1, p1, p2}, Lcom/jrm/friend/aidl/IFriendManagerService;->addFriendGroup([Ljava/lang/String;[Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public bindHandler(Landroid/os/Handler;)V
    .locals 0
    .param p1    # Landroid/os/Handler;

    iput-object p1, p0, Lcom/jrm/friend/binder/JRMFriendService;->handler:Landroid/os/Handler;

    return-void
.end method

.method public bindUser(I)Z
    .locals 3
    .param p1    # I

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/jrm/friend/binder/JRMFriendService;->mFriendManager:Lcom/jrm/friend/aidl/IFriendManagerService;

    if-eqz v2, :cond_0

    :try_start_0
    iget-object v2, p0, Lcom/jrm/friend/binder/JRMFriendService;->mFriendManager:Lcom/jrm/friend/aidl/IFriendManagerService;

    invoke-interface {v2, p1}, Lcom/jrm/friend/aidl/IFriendManagerService;->bindUser(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v1, 0x1

    :cond_0
    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public deleteFriend([Ljava/lang/String;[Ljava/lang/String;)Z
    .locals 4
    .param p1    # [Ljava/lang/String;
    .param p2    # [Ljava/lang/String;

    iget-object v2, p0, Lcom/jrm/friend/binder/JRMFriendService;->mFriendManager:Lcom/jrm/friend/aidl/IFriendManagerService;

    if-eqz v2, :cond_0

    new-instance v2, Ljava/lang/Thread;

    new-instance v3, Lcom/jrm/friend/binder/JRMFriendService$3;

    invoke-direct {v3, p0, p1, p2}, Lcom/jrm/friend/binder/JRMFriendService$3;-><init>(Lcom/jrm/friend/binder/JRMFriendService;[Ljava/lang/String;[Ljava/lang/String;)V

    invoke-direct {v2, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    :goto_0
    const/4 v2, 0x1

    return v2

    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "result"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    new-instance v1, Landroid/os/Message;

    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    const v2, 0x1000001

    iput v2, v1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v2, p0, Lcom/jrm/friend/binder/JRMFriendService;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public deleteFriendGroup([Ljava/lang/String;[Ljava/lang/String;Z)Z
    .locals 2
    .param p1    # [Ljava/lang/String;
    .param p2    # [Ljava/lang/String;
    .param p3    # Z

    iget-object v1, p0, Lcom/jrm/friend/binder/JRMFriendService;->mFriendManager:Lcom/jrm/friend/aidl/IFriendManagerService;

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/jrm/friend/binder/JRMFriendService;->mFriendManager:Lcom/jrm/friend/aidl/IFriendManagerService;

    invoke-interface {v1, p1, p2, p3}, Lcom/jrm/friend/aidl/IFriendManagerService;->deleteFriendGroup([Ljava/lang/String;[Ljava/lang/String;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public destroyService()V
    .locals 2

    iget-object v1, p0, Lcom/jrm/friend/binder/JRMFriendService;->mFriendManager:Lcom/jrm/friend/aidl/IFriendManagerService;

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/jrm/friend/binder/JRMFriendService;->mFriendManager:Lcom/jrm/friend/aidl/IFriendManagerService;

    invoke-interface {v1}, Lcom/jrm/friend/aidl/IFriendManagerService;->destroyManager()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getFriendGroup([Ljava/lang/String;[Ljava/lang/String;)Ljava/util/List;
    .locals 2
    .param p1    # [Ljava/lang/String;
    .param p2    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/jrm/friend/aidl/FriendGroup;",
            ">;"
        }
    .end annotation

    iget-object v1, p0, Lcom/jrm/friend/binder/JRMFriendService;->mFriendManager:Lcom/jrm/friend/aidl/IFriendManagerService;

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/jrm/friend/binder/JRMFriendService;->mFriendManager:Lcom/jrm/friend/aidl/IFriendManagerService;

    invoke-interface {v1, p1, p2}, Lcom/jrm/friend/aidl/IFriendManagerService;->getFriendGroup([Ljava/lang/String;[Ljava/lang/String;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getFriends([Ljava/lang/String;[Ljava/lang/String;)Ljava/util/List;
    .locals 2
    .param p1    # [Ljava/lang/String;
    .param p2    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/jrm/friend/aidl/Friend;",
            ">;"
        }
    .end annotation

    iget-object v1, p0, Lcom/jrm/friend/binder/JRMFriendService;->mFriendManager:Lcom/jrm/friend/aidl/IFriendManagerService;

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/jrm/friend/binder/JRMFriendService;->mFriendManager:Lcom/jrm/friend/aidl/IFriendManagerService;

    invoke-interface {v1, p1, p2}, Lcom/jrm/friend/aidl/IFriendManagerService;->getFriendList([Ljava/lang/String;[Ljava/lang/String;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public modifyFriendGroup([Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)Z
    .locals 2
    .param p1    # [Ljava/lang/String;
    .param p2    # [Ljava/lang/String;
    .param p3    # [Ljava/lang/String;
    .param p4    # [Ljava/lang/String;

    iget-object v1, p0, Lcom/jrm/friend/binder/JRMFriendService;->mFriendManager:Lcom/jrm/friend/aidl/IFriendManagerService;

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/jrm/friend/binder/JRMFriendService;->mFriendManager:Lcom/jrm/friend/aidl/IFriendManagerService;

    invoke-interface {v1, p2, p2, p3, p4}, Lcom/jrm/friend/aidl/IFriendManagerService;->modifyFriendGroup([Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public removeFriendToGroup([Ljava/lang/String;[Ljava/lang/String;I)Z
    .locals 2
    .param p1    # [Ljava/lang/String;
    .param p2    # [Ljava/lang/String;
    .param p3    # I

    iget-object v1, p0, Lcom/jrm/friend/binder/JRMFriendService;->mFriendManager:Lcom/jrm/friend/aidl/IFriendManagerService;

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/jrm/friend/binder/JRMFriendService;->mFriendManager:Lcom/jrm/friend/aidl/IFriendManagerService;

    invoke-interface {v1, p1, p2, p3}, Lcom/jrm/friend/aidl/IFriendManagerService;->removeFriendToGroup([Ljava/lang/String;[Ljava/lang/String;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public searchFriends(ILjava/lang/String;)Ljava/util/List;
    .locals 2
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/jrm/friend/aidl/Friend;",
            ">;"
        }
    .end annotation

    iget-object v1, p0, Lcom/jrm/friend/binder/JRMFriendService;->mFriendManager:Lcom/jrm/friend/aidl/IFriendManagerService;

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/jrm/friend/binder/JRMFriendService;->mFriendManager:Lcom/jrm/friend/aidl/IFriendManagerService;

    invoke-interface {v1, p1, p2}, Lcom/jrm/friend/aidl/IFriendManagerService;->searchFriend(ILjava/lang/String;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setAddFriendListener(Lcom/jrm/friend/listener/OnAddFriendListener;)V
    .locals 0
    .param p1    # Lcom/jrm/friend/listener/OnAddFriendListener;

    if-eqz p1, :cond_0

    iput-object p1, p0, Lcom/jrm/friend/binder/JRMFriendService;->mAddFriendListener:Lcom/jrm/friend/listener/OnAddFriendListener;

    :cond_0
    return-void
.end method

.method public setDeleteFriendListener(Lcom/jrm/friend/listener/OnDeleteFriendListener;)V
    .locals 0
    .param p1    # Lcom/jrm/friend/listener/OnDeleteFriendListener;

    if-eqz p1, :cond_0

    iput-object p1, p0, Lcom/jrm/friend/binder/JRMFriendService;->mDeleteFriendListener:Lcom/jrm/friend/listener/OnDeleteFriendListener;

    :cond_0
    return-void
.end method

.method public setURL(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    if-eqz p1, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/jrm/friend/binder/JRMFriendService;->mFriendManager:Lcom/jrm/friend/aidl/IFriendManagerService;

    invoke-interface {v1, p1}, Lcom/jrm/friend/aidl/IFriendManagerService;->setRequestUrl(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method
