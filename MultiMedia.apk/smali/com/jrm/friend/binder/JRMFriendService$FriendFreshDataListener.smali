.class Lcom/jrm/friend/binder/JRMFriendService$FriendFreshDataListener;
.super Lcom/jrm/friend/aidl/IFriendDataFreshListener$Stub;
.source "JRMFriendService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jrm/friend/binder/JRMFriendService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FriendFreshDataListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/friend/binder/JRMFriendService;


# direct methods
.method private constructor <init>(Lcom/jrm/friend/binder/JRMFriendService;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/friend/binder/JRMFriendService$FriendFreshDataListener;->this$0:Lcom/jrm/friend/binder/JRMFriendService;

    invoke-direct {p0}, Lcom/jrm/friend/aidl/IFriendDataFreshListener$Stub;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/jrm/friend/binder/JRMFriendService;Lcom/jrm/friend/binder/JRMFriendService$FriendFreshDataListener;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/jrm/friend/binder/JRMFriendService$FriendFreshDataListener;-><init>(Lcom/jrm/friend/binder/JRMFriendService;)V

    return-void
.end method


# virtual methods
.method public refreshBegine()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jrm/friend/binder/JRMFriendService$FriendFreshDataListener;->this$0:Lcom/jrm/friend/binder/JRMFriendService;

    # getter for: Lcom/jrm/friend/binder/JRMFriendService;->handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/jrm/friend/binder/JRMFriendService;->access$3(Lcom/jrm/friend/binder/JRMFriendService;)Landroid/os/Handler;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/friend/binder/JRMFriendService$FriendFreshDataListener;->this$0:Lcom/jrm/friend/binder/JRMFriendService;

    # getter for: Lcom/jrm/friend/binder/JRMFriendService;->handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/jrm/friend/binder/JRMFriendService;->access$3(Lcom/jrm/friend/binder/JRMFriendService;)Landroid/os/Handler;

    move-result-object v0

    const v1, 0x10000001

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_0
    return-void
.end method

.method public refreshFaild(I)V
    .locals 2
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jrm/friend/binder/JRMFriendService$FriendFreshDataListener;->this$0:Lcom/jrm/friend/binder/JRMFriendService;

    # getter for: Lcom/jrm/friend/binder/JRMFriendService;->handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/jrm/friend/binder/JRMFriendService;->access$3(Lcom/jrm/friend/binder/JRMFriendService;)Landroid/os/Handler;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/friend/binder/JRMFriendService$FriendFreshDataListener;->this$0:Lcom/jrm/friend/binder/JRMFriendService;

    # getter for: Lcom/jrm/friend/binder/JRMFriendService;->handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/jrm/friend/binder/JRMFriendService;->access$3(Lcom/jrm/friend/binder/JRMFriendService;)Landroid/os/Handler;

    move-result-object v0

    const v1, 0x10000002

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_0
    return-void
.end method

.method public refreshFinish()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jrm/friend/binder/JRMFriendService$FriendFreshDataListener;->this$0:Lcom/jrm/friend/binder/JRMFriendService;

    # getter for: Lcom/jrm/friend/binder/JRMFriendService;->handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/jrm/friend/binder/JRMFriendService;->access$3(Lcom/jrm/friend/binder/JRMFriendService;)Landroid/os/Handler;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/friend/binder/JRMFriendService$FriendFreshDataListener;->this$0:Lcom/jrm/friend/binder/JRMFriendService;

    # getter for: Lcom/jrm/friend/binder/JRMFriendService;->handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/jrm/friend/binder/JRMFriendService;->access$3(Lcom/jrm/friend/binder/JRMFriendService;)Landroid/os/Handler;

    move-result-object v0

    const v1, 0x10000003

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_0
    return-void
.end method
