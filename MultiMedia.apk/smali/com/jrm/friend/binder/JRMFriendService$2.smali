.class Lcom/jrm/friend/binder/JRMFriendService$2;
.super Ljava/lang/Object;
.source "JRMFriendService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/jrm/friend/binder/JRMFriendService;->addFriend(II)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/friend/binder/JRMFriendService;

.field private final synthetic val$bbNo:I

.field private final synthetic val$groupId:I


# direct methods
.method constructor <init>(Lcom/jrm/friend/binder/JRMFriendService;II)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/friend/binder/JRMFriendService$2;->this$0:Lcom/jrm/friend/binder/JRMFriendService;

    iput p2, p0, Lcom/jrm/friend/binder/JRMFriendService$2;->val$bbNo:I

    iput p3, p0, Lcom/jrm/friend/binder/JRMFriendService$2;->val$groupId:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    :try_start_0
    iget-object v4, p0, Lcom/jrm/friend/binder/JRMFriendService$2;->this$0:Lcom/jrm/friend/binder/JRMFriendService;

    # getter for: Lcom/jrm/friend/binder/JRMFriendService;->mFriendManager:Lcom/jrm/friend/aidl/IFriendManagerService;
    invoke-static {v4}, Lcom/jrm/friend/binder/JRMFriendService;->access$4(Lcom/jrm/friend/binder/JRMFriendService;)Lcom/jrm/friend/aidl/IFriendManagerService;

    move-result-object v4

    iget v5, p0, Lcom/jrm/friend/binder/JRMFriendService$2;->val$bbNo:I

    iget v6, p0, Lcom/jrm/friend/binder/JRMFriendService$2;->val$groupId:I

    invoke-interface {v4, v5, v6}, Lcom/jrm/friend/aidl/IFriendManagerService;->addFriend(II)Lcom/jrm/friend/aidl/Friend;

    move-result-object v2

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    if-eqz v2, :cond_0

    const-string v4, "result"

    const/4 v5, 0x1

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v4, "friend"

    invoke-virtual {v0, v4, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :goto_0
    new-instance v3, Landroid/os/Message;

    invoke-direct {v3}, Landroid/os/Message;-><init>()V

    const v4, 0x1000003

    iput v4, v3, Landroid/os/Message;->what:I

    invoke-virtual {v3, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v4, p0, Lcom/jrm/friend/binder/JRMFriendService$2;->this$0:Lcom/jrm/friend/binder/JRMFriendService;

    # getter for: Lcom/jrm/friend/binder/JRMFriendService;->mHandler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/jrm/friend/binder/JRMFriendService;->access$5(Lcom/jrm/friend/binder/JRMFriendService;)Landroid/os/Handler;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :goto_1
    return-void

    :cond_0
    const-string v4, "result"

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1
.end method
