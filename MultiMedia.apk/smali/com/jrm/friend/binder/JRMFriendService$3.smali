.class Lcom/jrm/friend/binder/JRMFriendService$3;
.super Ljava/lang/Object;
.source "JRMFriendService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/jrm/friend/binder/JRMFriendService;->deleteFriend([Ljava/lang/String;[Ljava/lang/String;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/friend/binder/JRMFriendService;

.field private final synthetic val$key:[Ljava/lang/String;

.field private final synthetic val$keydata:[Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/jrm/friend/binder/JRMFriendService;[Ljava/lang/String;[Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/friend/binder/JRMFriendService$3;->this$0:Lcom/jrm/friend/binder/JRMFriendService;

    iput-object p2, p0, Lcom/jrm/friend/binder/JRMFriendService$3;->val$key:[Ljava/lang/String;

    iput-object p3, p0, Lcom/jrm/friend/binder/JRMFriendService$3;->val$keydata:[Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    :try_start_0
    iget-object v4, p0, Lcom/jrm/friend/binder/JRMFriendService$3;->this$0:Lcom/jrm/friend/binder/JRMFriendService;

    # getter for: Lcom/jrm/friend/binder/JRMFriendService;->mFriendManager:Lcom/jrm/friend/aidl/IFriendManagerService;
    invoke-static {v4}, Lcom/jrm/friend/binder/JRMFriendService;->access$4(Lcom/jrm/friend/binder/JRMFriendService;)Lcom/jrm/friend/aidl/IFriendManagerService;

    move-result-object v4

    iget-object v5, p0, Lcom/jrm/friend/binder/JRMFriendService$3;->val$key:[Ljava/lang/String;

    iget-object v6, p0, Lcom/jrm/friend/binder/JRMFriendService$3;->val$keydata:[Ljava/lang/String;

    invoke-interface {v4, v5, v6}, Lcom/jrm/friend/aidl/IFriendManagerService;->deleteFriend([Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v1

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v4, "result"

    invoke-virtual {v0, v4, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    new-instance v3, Landroid/os/Message;

    invoke-direct {v3}, Landroid/os/Message;-><init>()V

    const v4, 0x1000001

    iput v4, v3, Landroid/os/Message;->what:I

    invoke-virtual {v3, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v4, p0, Lcom/jrm/friend/binder/JRMFriendService$3;->this$0:Lcom/jrm/friend/binder/JRMFriendService;

    # getter for: Lcom/jrm/friend/binder/JRMFriendService;->mHandler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/jrm/friend/binder/JRMFriendService;->access$5(Lcom/jrm/friend/binder/JRMFriendService;)Landroid/os/Handler;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method
