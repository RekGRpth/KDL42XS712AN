.class Lcom/jrm/friend/binder/JRMFriendService$1;
.super Landroid/os/Handler;
.source "JRMFriendService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jrm/friend/binder/JRMFriendService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/friend/binder/JRMFriendService;


# direct methods
.method constructor <init>(Lcom/jrm/friend/binder/JRMFriendService;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/friend/binder/JRMFriendService$1;->this$0:Lcom/jrm/friend/binder/JRMFriendService;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1    # Landroid/os/Message;

    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    iget-object v0, p0, Lcom/jrm/friend/binder/JRMFriendService$1;->this$0:Lcom/jrm/friend/binder/JRMFriendService;

    # getter for: Lcom/jrm/friend/binder/JRMFriendService;->mDeleteFriendListener:Lcom/jrm/friend/listener/OnDeleteFriendListener;
    invoke-static {v0}, Lcom/jrm/friend/binder/JRMFriendService;->access$0(Lcom/jrm/friend/binder/JRMFriendService;)Lcom/jrm/friend/listener/OnDeleteFriendListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/friend/binder/JRMFriendService$1;->this$0:Lcom/jrm/friend/binder/JRMFriendService;

    # getter for: Lcom/jrm/friend/binder/JRMFriendService;->mDeleteFriendListener:Lcom/jrm/friend/listener/OnDeleteFriendListener;
    invoke-static {v0}, Lcom/jrm/friend/binder/JRMFriendService;->access$0(Lcom/jrm/friend/binder/JRMFriendService;)Lcom/jrm/friend/listener/OnDeleteFriendListener;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "result"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Lcom/jrm/friend/listener/OnDeleteFriendListener;->onDeleteFriend(ZLjava/lang/String;)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/jrm/friend/binder/JRMFriendService$1;->this$0:Lcom/jrm/friend/binder/JRMFriendService;

    # getter for: Lcom/jrm/friend/binder/JRMFriendService;->mAddFriendListener:Lcom/jrm/friend/listener/OnAddFriendListener;
    invoke-static {v0}, Lcom/jrm/friend/binder/JRMFriendService;->access$1(Lcom/jrm/friend/binder/JRMFriendService;)Lcom/jrm/friend/listener/OnAddFriendListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/friend/binder/JRMFriendService$1;->this$0:Lcom/jrm/friend/binder/JRMFriendService;

    # getter for: Lcom/jrm/friend/binder/JRMFriendService;->mAddFriendListener:Lcom/jrm/friend/listener/OnAddFriendListener;
    invoke-static {v0}, Lcom/jrm/friend/binder/JRMFriendService;->access$1(Lcom/jrm/friend/binder/JRMFriendService;)Lcom/jrm/friend/listener/OnAddFriendListener;

    move-result-object v1

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "result"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string v3, "friend"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jrm/friend/aidl/Friend;

    invoke-interface {v1, v2, v0}, Lcom/jrm/friend/listener/OnAddFriendListener;->onAddFriend(ZLcom/jrm/friend/aidl/Friend;)V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/jrm/friend/binder/JRMFriendService$1;->this$0:Lcom/jrm/friend/binder/JRMFriendService;

    # getter for: Lcom/jrm/friend/binder/JRMFriendService;->mModifyFriendListener:Lcom/jrm/friend/listener/OnModifyFriendListener;
    invoke-static {v0}, Lcom/jrm/friend/binder/JRMFriendService;->access$2(Lcom/jrm/friend/binder/JRMFriendService;)Lcom/jrm/friend/listener/OnModifyFriendListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/friend/binder/JRMFriendService$1;->this$0:Lcom/jrm/friend/binder/JRMFriendService;

    # getter for: Lcom/jrm/friend/binder/JRMFriendService;->mModifyFriendListener:Lcom/jrm/friend/listener/OnModifyFriendListener;
    invoke-static {v0}, Lcom/jrm/friend/binder/JRMFriendService;->access$2(Lcom/jrm/friend/binder/JRMFriendService;)Lcom/jrm/friend/listener/OnModifyFriendListener;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "result"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Lcom/jrm/friend/listener/OnModifyFriendListener;->onModifyFriend(ZLjava/lang/String;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1000001
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method
