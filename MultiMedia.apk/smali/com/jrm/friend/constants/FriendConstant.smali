.class public Lcom/jrm/friend/constants/FriendConstant;
.super Ljava/lang/Object;
.source "FriendConstant.java"


# static fields
.field public static final FRIENDGROUP_ID:Ljava/lang/String; = "group_id"

.field public static final FRIENDGROUP_NAME:Ljava/lang/String; = "group_name"

.field public static final FRIEND_DATA_REFRESH_FAILED:I = 0x10000002

.field public static final FRIEND_DATA_REFRESH_START:I = 0x10000001

.field public static final FRIEND_DATA_REFRESH_SUCCESS:I = 0x10000003

.field public static final FRIEND_FRIEND_ADDRESS:Ljava/lang/String; = "friend_address"

.field public static final FRIEND_FRIEND_BBNO:Ljava/lang/String; = "friend_BBNo"

.field public static final FRIEND_FRIEND_BIRTHDAY:Ljava/lang/String; = "friend_birthday"

.field public static final FRIEND_FRIEND_DEVICE:Ljava/lang/String; = "friend_dv"

.field public static final FRIEND_FRIEND_EMAIL:Ljava/lang/String; = "friend_email"

.field public static final FRIEND_FRIEND_LOGO:Ljava/lang/String; = "friend_logo"

.field public static final FRIEND_FRIEND_MOBILE:Ljava/lang/String; = "friend_mobile"

.field public static final FRIEND_FRIEND_NAME:Ljava/lang/String; = "firend_name"

.field public static final FRIEND_FRIEND_NICK_NAME:Ljava/lang/String; = "friend_nick_name"

.field public static final FRIEND_FRIEND_SEX:Ljava/lang/String; = "friend_sex"

.field public static final FRIEND_FRIEND_SIGNATURE:Ljava/lang/String; = "friend_signature"

.field public static final FRIEND_GROUP_ID:Ljava/lang/String; = "friend_group_id"

.field public static final FRIEND_REMARK_NAME:Ljava/lang/String; = "remark_name"

.field public static REQUEST_URL:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "http://transaction.88popo.com/interface/babaoService.jsp"

    sput-object v0, Lcom/jrm/friend/constants/FriendConstant;->REQUEST_URL:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
