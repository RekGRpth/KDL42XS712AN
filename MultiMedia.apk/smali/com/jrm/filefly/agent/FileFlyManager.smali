.class public Lcom/jrm/filefly/agent/FileFlyManager;
.super Ljava/lang/Object;
.source "FileFlyManager.java"


# instance fields
.field private final TAG:Ljava/lang/String;

.field private mIFileFlyManagerService:Lcom/jrm/filefly/service/IFileFlyManagerService;


# direct methods
.method public constructor <init>(Lcom/jrm/filefly/service/IFileFlyManagerService;)V
    .locals 3
    .param p1    # Lcom/jrm/filefly/service/IFileFlyManagerService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "FileFlyManager"

    iput-object v0, p0, Lcom/jrm/filefly/agent/FileFlyManager;->TAG:Ljava/lang/String;

    iput-object p1, p0, Lcom/jrm/filefly/agent/FileFlyManager;->mIFileFlyManagerService:Lcom/jrm/filefly/service/IFileFlyManagerService;

    const-string v0, "FileFlyManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mIFileFlyManagerService:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/jrm/filefly/agent/FileFlyManager;->mIFileFlyManagerService:Lcom/jrm/filefly/service/IFileFlyManagerService;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method


# virtual methods
.method public setHandlerMessageListener(Lcom/jrm/filefly/service/IHandlerMessageListener;)V
    .locals 2
    .param p1    # Lcom/jrm/filefly/service/IHandlerMessageListener;

    :try_start_0
    iget-object v1, p0, Lcom/jrm/filefly/agent/FileFlyManager;->mIFileFlyManagerService:Lcom/jrm/filefly/service/IFileFlyManagerService;

    invoke-interface {v1, p1}, Lcom/jrm/filefly/service/IFileFlyManagerService;->setServiceListener(Lcom/jrm/filefly/service/IHandlerMessageListener;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method
