.class public Lcom/jrm/vvoip/agent/VVoipAgent;
.super Ljava/lang/Object;
.source "VVoipAgent.java"


# static fields
.field public static final BLACK_FRIEND:I = 0x1

.field public static final DEVICE_UNKNOW:I = 0x3

.field public static final DEVICE_UNUSABLE:I = 0x2

.field public static final DEVICE_USABLE:I = 0x1

.field public static final RECORDS_TYPE_INCOMING:I = 0x2

.field public static final RECORDS_TYPE_MISSING:I = 0x3

.field public static final RECORDS_TYPE_OUTGOING:I = 0x1

.field public static final TAG:Ljava/lang/String;

.field public static final WHITE_FRIEND:I = 0x2


# instance fields
.field private binder:Lcom/jrm/vvoip/service/IVVoipService;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/jrm/vvoip/agent/VVoipAgent;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jrm/vvoip/agent/VVoipAgent;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/jrm/vvoip/service/IVVoipService;)V
    .locals 0
    .param p1    # Lcom/jrm/vvoip/service/IVVoipService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/jrm/vvoip/agent/VVoipAgent;->binder:Lcom/jrm/vvoip/service/IVVoipService;

    return-void
.end method

.method private sendMsg2UI()V
    .locals 0

    return-void
.end method


# virtual methods
.method public add2BlackList(Ljava/lang/String;Lcom/jrm/vvoip/service/DBListener;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/jrm/vvoip/service/DBListener;

    :try_start_0
    iget-object v1, p0, Lcom/jrm/vvoip/agent/VVoipAgent;->binder:Lcom/jrm/vvoip/service/IVVoipService;

    const/4 v2, 0x1

    invoke-interface {v1, p1, v2, p2}, Lcom/jrm/vvoip/service/IVVoipService;->add2FriendList(Ljava/lang/String;ILcom/jrm/vvoip/service/IDBListener;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public add2WhiteList(Ljava/lang/String;Lcom/jrm/vvoip/service/DBListener;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/jrm/vvoip/service/DBListener;

    :try_start_0
    iget-object v1, p0, Lcom/jrm/vvoip/agent/VVoipAgent;->binder:Lcom/jrm/vvoip/service/IVVoipService;

    const/4 v2, 0x2

    invoke-interface {v1, p1, v2, p2}, Lcom/jrm/vvoip/service/IVVoipService;->add2FriendList(Ljava/lang/String;ILcom/jrm/vvoip/service/IDBListener;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public addRecords(Ljava/lang/String;I)V
    .locals 8
    .param p1    # Ljava/lang/String;
    .param p2    # I

    :try_start_0
    new-instance v6, Lcom/jrm/vvoip/agent/VVoipAgent$1;

    invoke-direct {v6, p0}, Lcom/jrm/vvoip/agent/VVoipAgent$1;-><init>(Lcom/jrm/vvoip/agent/VVoipAgent;)V

    iget-object v0, p0, Lcom/jrm/vvoip/agent/VVoipAgent;->binder:Lcom/jrm/vvoip/service/IVVoipService;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const/4 v4, 0x0

    move-object v1, p1

    move v5, p2

    invoke-interface/range {v0 .. v6}, Lcom/jrm/vvoip/service/IVVoipService;->add2RecordList(Ljava/lang/String;JIILcom/jrm/vvoip/service/IDBListener;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v7

    invoke-virtual {v7}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public checkDevice(Lcom/jrm/vvoip/service/DeviceCheckListener;)V
    .locals 2
    .param p1    # Lcom/jrm/vvoip/service/DeviceCheckListener;

    :try_start_0
    iget-object v1, p0, Lcom/jrm/vvoip/agent/VVoipAgent;->binder:Lcom/jrm/vvoip/service/IVVoipService;

    invoke-interface {v1, p1}, Lcom/jrm/vvoip/service/IVVoipService;->checkDevice(Lcom/jrm/vvoip/service/IDeviceCheckListener;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public delAllRecords(Lcom/jrm/vvoip/service/DBListener;)V
    .locals 2
    .param p1    # Lcom/jrm/vvoip/service/DBListener;

    :try_start_0
    iget-object v1, p0, Lcom/jrm/vvoip/agent/VVoipAgent;->binder:Lcom/jrm/vvoip/service/IVVoipService;

    invoke-interface {v1, p1}, Lcom/jrm/vvoip/service/IVVoipService;->delAllRecords(Lcom/jrm/vvoip/service/IDBListener;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public delName(Ljava/lang/String;Lcom/jrm/vvoip/service/DBListener;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/jrm/vvoip/service/DBListener;

    :try_start_0
    iget-object v1, p0, Lcom/jrm/vvoip/agent/VVoipAgent;->binder:Lcom/jrm/vvoip/service/IVVoipService;

    invoke-interface {v1, p1, p2}, Lcom/jrm/vvoip/service/IVVoipService;->delFriend(Ljava/lang/String;Lcom/jrm/vvoip/service/IDBListener;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public delRecords(ILcom/jrm/vvoip/service/DBListener;)V
    .locals 2
    .param p1    # I
    .param p2    # Lcom/jrm/vvoip/service/DBListener;

    :try_start_0
    iget-object v1, p0, Lcom/jrm/vvoip/agent/VVoipAgent;->binder:Lcom/jrm/vvoip/service/IVVoipService;

    invoke-interface {v1, p1, p2}, Lcom/jrm/vvoip/service/IVVoipService;->delRecordsById(ILcom/jrm/vvoip/service/IDBListener;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public delRecordsByNumber(Ljava/lang/String;Lcom/jrm/vvoip/service/DBListener;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/jrm/vvoip/service/DBListener;

    :try_start_0
    iget-object v1, p0, Lcom/jrm/vvoip/agent/VVoipAgent;->binder:Lcom/jrm/vvoip/service/IVVoipService;

    invoke-interface {v1, p1, p2}, Lcom/jrm/vvoip/service/IVVoipService;->delRecordsByNumber(Ljava/lang/String;Lcom/jrm/vvoip/service/IDBListener;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getAllRecords()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/jrm/vvoip/service/Records;",
            ">;"
        }
    .end annotation

    :try_start_0
    iget-object v1, p0, Lcom/jrm/vvoip/agent/VVoipAgent;->binder:Lcom/jrm/vvoip/service/IVVoipService;

    invoke-interface {v1}, Lcom/jrm/vvoip/service/IVVoipService;->getAllRecords()Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0
.end method

.method public getBlackNames()[Ljava/lang/String;
    .locals 3

    :try_start_0
    iget-object v1, p0, Lcom/jrm/vvoip/agent/VVoipAgent;->binder:Lcom/jrm/vvoip/service/IVVoipService;

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Lcom/jrm/vvoip/service/IVVoipService;->getAllFriends(I)[Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getLocalProflie()Landroid/net/sip/SipProfile;
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/jrm/vvoip/agent/VVoipAgent;->binder:Lcom/jrm/vvoip/service/IVVoipService;

    invoke-interface {v1}, Lcom/jrm/vvoip/service/IVVoipService;->getLocalProfile()Landroid/net/sip/SipProfile;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getRecordsByNumber(Ljava/lang/String;)Ljava/util/List;
    .locals 2
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/jrm/vvoip/service/Records;",
            ">;"
        }
    .end annotation

    :try_start_0
    iget-object v1, p0, Lcom/jrm/vvoip/agent/VVoipAgent;->binder:Lcom/jrm/vvoip/service/IVVoipService;

    invoke-interface {v1, p1}, Lcom/jrm/vvoip/service/IVVoipService;->getRecordsByNumber(Ljava/lang/String;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0
.end method

.method public getWhiteNames()[Ljava/lang/String;
    .locals 3

    :try_start_0
    iget-object v1, p0, Lcom/jrm/vvoip/agent/VVoipAgent;->binder:Lcom/jrm/vvoip/service/IVVoipService;

    const/4 v2, 0x2

    invoke-interface {v1, v2}, Lcom/jrm/vvoip/service/IVVoipService;->getAllFriends(I)[Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    goto :goto_0
.end method

.method public modifyCallType(Ljava/lang/String;I)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # I

    :try_start_0
    new-instance v1, Lcom/jrm/vvoip/agent/VVoipAgent$3;

    invoke-direct {v1, p0}, Lcom/jrm/vvoip/agent/VVoipAgent$3;-><init>(Lcom/jrm/vvoip/agent/VVoipAgent;)V

    iget-object v2, p0, Lcom/jrm/vvoip/agent/VVoipAgent;->binder:Lcom/jrm/vvoip/service/IVVoipService;

    invoke-interface {v2, p1, p2, v1}, Lcom/jrm/vvoip/service/IVVoipService;->modifyCallType(Ljava/lang/String;ILcom/jrm/vvoip/service/IDBListener;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public modifyDuration(Ljava/lang/String;I)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # I

    :try_start_0
    new-instance v1, Lcom/jrm/vvoip/agent/VVoipAgent$2;

    invoke-direct {v1, p0}, Lcom/jrm/vvoip/agent/VVoipAgent$2;-><init>(Lcom/jrm/vvoip/agent/VVoipAgent;)V

    iget-object v2, p0, Lcom/jrm/vvoip/agent/VVoipAgent;->binder:Lcom/jrm/vvoip/service/IVVoipService;

    invoke-interface {v2, p1, p2, v1}, Lcom/jrm/vvoip/service/IVVoipService;->modifyDuration(Ljava/lang/String;ILcom/jrm/vvoip/service/IDBListener;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public registerVVoip(Lcom/jrm/vvoip/service/VVoipRegisterListener;)V
    .locals 3
    .param p1    # Lcom/jrm/vvoip/service/VVoipRegisterListener;

    :try_start_0
    sget-object v1, Lcom/jrm/vvoip/agent/VVoipAgent;->TAG:Ljava/lang/String;

    const-string v2, "agent.register..."

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/jrm/vvoip/agent/VVoipAgent;->binder:Lcom/jrm/vvoip/service/IVVoipService;

    invoke-interface {v1, p1}, Lcom/jrm/vvoip/service/IVVoipService;->registerVVoip(Lcom/jrm/vvoip/service/IVVoipRegisterListener;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public test()V
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/jrm/vvoip/agent/VVoipAgent;->binder:Lcom/jrm/vvoip/service/IVVoipService;

    invoke-interface {v1}, Lcom/jrm/vvoip/service/IVVoipService;->test()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public unRegistSIPServer(Lcom/jrm/vvoip/service/VVoipRegisterListener;)V
    .locals 2
    .param p1    # Lcom/jrm/vvoip/service/VVoipRegisterListener;

    :try_start_0
    iget-object v1, p0, Lcom/jrm/vvoip/agent/VVoipAgent;->binder:Lcom/jrm/vvoip/service/IVVoipService;

    invoke-interface {v1, p1}, Lcom/jrm/vvoip/service/IVVoipService;->unRegistSIPServer(Lcom/jrm/vvoip/service/IVVoipRegisterListener;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method
