.class public interface abstract Lcom/jrm/vvoip/service/IDeviceCheckListener;
.super Ljava/lang/Object;
.source "IDeviceCheckListener.java"

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/jrm/vvoip/service/IDeviceCheckListener$Stub;
    }
.end annotation


# virtual methods
.method public abstract checkCamera(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract checkMicphone(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
