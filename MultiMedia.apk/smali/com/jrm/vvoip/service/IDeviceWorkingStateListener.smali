.class public interface abstract Lcom/jrm/vvoip/service/IDeviceWorkingStateListener;
.super Ljava/lang/Object;
.source "IDeviceWorkingStateListener.java"

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/jrm/vvoip/service/IDeviceWorkingStateListener$Stub;
    }
.end annotation


# virtual methods
.method public abstract cameraStatus(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract localVideoStatus(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract micphoneStatus(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
