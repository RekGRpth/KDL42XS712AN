.class public abstract Lcom/jrm/vvoip/service/VVoipRegisterListener;
.super Lcom/jrm/vvoip/service/IVVoipRegisterListener$Stub;
.source "VVoipRegisterListener.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/jrm/vvoip/service/IVVoipRegisterListener$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract onRegistering(Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract onRegistrationDone(Ljava/lang/String;J)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract onRegistrationFailed(Ljava/lang/String;ILjava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
