.class public interface abstract Lcom/jrm/core/container/cmps/management/ICmpUpgradeListener;
.super Ljava/lang/Object;
.source "ICmpUpgradeListener.java"

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/jrm/core/container/cmps/management/ICmpUpgradeListener$Stub;
    }
.end annotation


# virtual methods
.method public abstract onUpgradeFailed(ILjava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract onUpgradeSuccess()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
