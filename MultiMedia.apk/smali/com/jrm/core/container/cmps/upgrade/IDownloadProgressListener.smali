.class public interface abstract Lcom/jrm/core/container/cmps/upgrade/IDownloadProgressListener;
.super Ljava/lang/Object;
.source "IDownloadProgressListener.java"

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/jrm/core/container/cmps/upgrade/IDownloadProgressListener$Stub;
    }
.end annotation


# virtual methods
.method public abstract onDownloadSizeChange(II)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
