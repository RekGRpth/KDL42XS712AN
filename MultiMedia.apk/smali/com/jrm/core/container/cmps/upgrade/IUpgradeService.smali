.class public interface abstract Lcom/jrm/core/container/cmps/upgrade/IUpgradeService;
.super Ljava/lang/Object;
.source "IUpgradeService.java"

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/jrm/core/container/cmps/upgrade/IUpgradeService$Stub;
    }
.end annotation


# virtual methods
.method public abstract cancelUpgradeTask(J)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract createUpgradeTask(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract getAllUpgradeTask(I)[Lcom/jrm/core/container/cmps/upgrade/TaskModel;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract getUpgradeTask(J)Lcom/jrm/core/container/cmps/upgrade/TaskModel;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract startUpgradeTask(J)Lcom/jrm/core/container/cmps/upgrade/IUpgradeTask;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
