.class public interface abstract Lcom/jrm/core/container/cmps/upgrade/IUpgradeTask;
.super Ljava/lang/Object;
.source "IUpgradeTask.java"

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/jrm/core/container/cmps/upgrade/IUpgradeTask$Stub;
    }
.end annotation


# virtual methods
.method public abstract commitInstall(I)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract getModel()Lcom/jrm/core/container/cmps/upgrade/TaskModel;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract pause()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract registUpgradeStateListener(Lcom/jrm/core/container/cmps/upgrade/IUpgradeStateListener;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract resume()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract setDownloadSizeChangeListener(Lcom/jrm/core/container/cmps/upgrade/IDownloadProgressListener;I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
