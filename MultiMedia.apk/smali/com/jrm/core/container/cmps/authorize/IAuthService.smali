.class public interface abstract Lcom/jrm/core/container/cmps/authorize/IAuthService;
.super Ljava/lang/Object;
.source "IAuthService.java"

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/jrm/core/container/cmps/authorize/IAuthService$Stub;
    }
.end annotation


# virtual methods
.method public abstract authMachineAuthorize()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract authUserCertificate(Ljava/lang/String;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
