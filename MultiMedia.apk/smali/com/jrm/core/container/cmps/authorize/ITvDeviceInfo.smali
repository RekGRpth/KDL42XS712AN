.class public interface abstract Lcom/jrm/core/container/cmps/authorize/ITvDeviceInfo;
.super Ljava/lang/Object;
.source "ITvDeviceInfo.java"

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/jrm/core/container/cmps/authorize/ITvDeviceInfo$Stub;
    }
.end annotation


# virtual methods
.method public abstract editTvprofile(Lcom/jrm/core/container/cmps/authorize/TvDeviceProfile;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract getBBNumer()Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract getClientId()Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract getMac()Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract getTvProfile()Lcom/jrm/core/container/cmps/authorize/TvDeviceProfile;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
