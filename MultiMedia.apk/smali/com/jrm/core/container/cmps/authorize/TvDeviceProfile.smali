.class public Lcom/jrm/core/container/cmps/authorize/TvDeviceProfile;
.super Ljava/lang/Object;
.source "TvDeviceProfile.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/jrm/core/container/cmps/authorize/TvDeviceProfile;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private address:Ljava/lang/String;

.field private birthday:Ljava/lang/String;

.field private email:Ljava/lang/String;

.field private gender:I

.field private logo:Ljava/lang/String;

.field private nickName:Ljava/lang/String;

.field private singure:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/jrm/core/container/cmps/authorize/TvDeviceProfile$1;

    invoke-direct {v0}, Lcom/jrm/core/container/cmps/authorize/TvDeviceProfile$1;-><init>()V

    sput-object v0, Lcom/jrm/core/container/cmps/authorize/TvDeviceProfile;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getAddress()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jrm/core/container/cmps/authorize/TvDeviceProfile;->address:Ljava/lang/String;

    return-object v0
.end method

.method public getBirthday()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jrm/core/container/cmps/authorize/TvDeviceProfile;->birthday:Ljava/lang/String;

    return-object v0
.end method

.method public getEmail()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jrm/core/container/cmps/authorize/TvDeviceProfile;->email:Ljava/lang/String;

    return-object v0
.end method

.method public getGender()I
    .locals 1

    iget v0, p0, Lcom/jrm/core/container/cmps/authorize/TvDeviceProfile;->gender:I

    return v0
.end method

.method public getLogo()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jrm/core/container/cmps/authorize/TvDeviceProfile;->logo:Ljava/lang/String;

    return-object v0
.end method

.method public getNickName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jrm/core/container/cmps/authorize/TvDeviceProfile;->nickName:Ljava/lang/String;

    return-object v0
.end method

.method public getSingure()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jrm/core/container/cmps/authorize/TvDeviceProfile;->singure:Ljava/lang/String;

    return-object v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 0
    .param p1    # Landroid/os/Parcel;

    return-void
.end method

.method public setAddress(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/jrm/core/container/cmps/authorize/TvDeviceProfile;->address:Ljava/lang/String;

    return-void
.end method

.method public setBirthday(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/jrm/core/container/cmps/authorize/TvDeviceProfile;->birthday:Ljava/lang/String;

    return-void
.end method

.method public setEmail(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/jrm/core/container/cmps/authorize/TvDeviceProfile;->email:Ljava/lang/String;

    return-void
.end method

.method public setGender(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/jrm/core/container/cmps/authorize/TvDeviceProfile;->gender:I

    return-void
.end method

.method public setLogo(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/jrm/core/container/cmps/authorize/TvDeviceProfile;->logo:Ljava/lang/String;

    return-void
.end method

.method public setNickName(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/jrm/core/container/cmps/authorize/TvDeviceProfile;->nickName:Ljava/lang/String;

    return-void
.end method

.method public setSingure(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/jrm/core/container/cmps/authorize/TvDeviceProfile;->singure:Ljava/lang/String;

    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    return-void
.end method
