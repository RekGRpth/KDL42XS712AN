.class Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl$1;
.super Ljava/lang/Object;
.source "JRMServiceManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;->onServiceBindException(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;

.field private final synthetic val$exceptionCode:I


# direct methods
.method constructor <init>(Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;I)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl$1;->this$1:Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;

    iput p2, p0, Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl$1;->val$exceptionCode:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    const/4 v4, 0x1

    const-string v0, "\u4f9d\u8d56\u7ec4\u4ef6\u4e0d\u5b58\u5728\uff0c\u66f4\u65b0\u4e2d..."

    iget v1, p0, Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl$1;->val$exceptionCode:I

    const/16 v2, 0xc

    if-ne v1, v2, :cond_0

    const-string v0, "\u4f9d\u8d56\u7684\u540e\u53f0\u7ec4\u4ef6\u7248\u672c\u8f83\u4f4e\uff0c\u66f4\u65b0\u4e2d..."

    :cond_0
    iget-object v1, p0, Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl$1;->this$1:Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;

    new-instance v2, Landroid/app/ProgressDialog;

    iget-object v3, p0, Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl$1;->this$1:Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;

    # getter for: Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;->access$0(Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;)Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    invoke-static {v1, v2}, Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;->access$1(Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;Landroid/app/ProgressDialog;)V

    iget-object v1, p0, Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl$1;->this$1:Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;

    # getter for: Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;->dialog:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;->access$2(Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl$1;->this$1:Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;

    # getter for: Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;->dialog:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;->access$2(Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    iget-object v1, p0, Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl$1;->this$1:Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;

    # getter for: Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;->dialog:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;->access$2(Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    iget-object v1, p0, Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl$1;->this$1:Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;

    # getter for: Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;->dialog:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;->access$2(Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->show()V

    return-void
.end method
