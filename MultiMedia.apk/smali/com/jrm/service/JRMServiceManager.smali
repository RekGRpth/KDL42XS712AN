.class public Lcom/jrm/service/JRMServiceManager;
.super Ljava/lang/Object;
.source "JRMServiceManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;
    }
.end annotation


# static fields
.field private static final DEVELOPING:Z = false

.field private static final TAG:Ljava/lang/String; = "JRM"

.field private static agent:Lcom/jrm/service/JRMServiceAgent;

.field private static debugserviceConnection:Landroid/content/ServiceConnection;

.field private static jrmconnector:Lcom/jrm/service/IJRMConnectListener;

.field private static jrmexceptionListener:Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;

.field private static mContext:Landroid/content/Context;

.field private static pkgName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/jrm/service/JRMServiceManager$1;

    invoke-direct {v0}, Lcom/jrm/service/JRMServiceManager$1;-><init>()V

    sput-object v0, Lcom/jrm/service/JRMServiceManager;->debugserviceConnection:Landroid/content/ServiceConnection;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0()Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;
    .locals 1

    sget-object v0, Lcom/jrm/service/JRMServiceManager;->jrmexceptionListener:Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;

    return-object v0
.end method

.method static synthetic access$1()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/jrm/service/JRMServiceManager;->pkgName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2(Lcom/jrm/service/JRMServiceAgent;)V
    .locals 0

    sput-object p0, Lcom/jrm/service/JRMServiceManager;->agent:Lcom/jrm/service/JRMServiceAgent;

    return-void
.end method

.method static synthetic access$3()Lcom/jrm/service/IJRMConnectListener;
    .locals 1

    sget-object v0, Lcom/jrm/service/JRMServiceManager;->jrmconnector:Lcom/jrm/service/IJRMConnectListener;

    return-object v0
.end method

.method static synthetic access$4()Lcom/jrm/service/JRMServiceAgent;
    .locals 1

    sget-object v0, Lcom/jrm/service/JRMServiceManager;->agent:Lcom/jrm/service/JRMServiceAgent;

    return-object v0
.end method

.method static synthetic access$5(Lcom/jrm/service/IJRMConnectListener;)V
    .locals 0

    sput-object p0, Lcom/jrm/service/JRMServiceManager;->jrmconnector:Lcom/jrm/service/IJRMConnectListener;

    return-void
.end method

.method private static aysnConnectToServiceContainer()V
    .locals 2

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/jrm/service/JRMServiceManager$2;

    invoke-direct {v1}, Lcom/jrm/service/JRMServiceManager$2;-><init>()V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method public static connectToJRM(Landroid/app/Activity;Lcom/jrm/service/IJRMConnectListener;)V
    .locals 3
    .param p0    # Landroid/app/Activity;
    .param p1    # Lcom/jrm/service/IJRMConnectListener;

    invoke-virtual {p0}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/jrm/service/JRMServiceManager;->pkgName:Ljava/lang/String;

    sput-object p1, Lcom/jrm/service/JRMServiceManager;->jrmconnector:Lcom/jrm/service/IJRMConnectListener;

    new-instance v1, Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;

    invoke-direct {v1, p0}, Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;-><init>(Landroid/app/Activity;)V

    sput-object v1, Lcom/jrm/service/JRMServiceManager;->jrmexceptionListener:Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;

    sput-object p0, Lcom/jrm/service/JRMServiceManager;->mContext:Landroid/content/Context;

    sget-object v1, Lcom/jrm/service/JRMServiceManager;->agent:Lcom/jrm/service/JRMServiceAgent;

    if-nez v1, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.jrm.core.container.ServiceContainerService"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    sget-object v1, Lcom/jrm/service/JRMServiceManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    invoke-static {}, Lcom/jrm/service/JRMServiceManager;->aysnConnectToServiceContainer()V

    :goto_0
    return-void

    :cond_0
    sget-object v1, Lcom/jrm/service/JRMServiceManager;->agent:Lcom/jrm/service/JRMServiceAgent;

    sget-object v2, Lcom/jrm/service/JRMServiceManager;->pkgName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/jrm/service/JRMServiceAgent;->setPkgName(Ljava/lang/String;)V

    sget-object v1, Lcom/jrm/service/JRMServiceManager;->agent:Lcom/jrm/service/JRMServiceAgent;

    sget-object v2, Lcom/jrm/service/JRMServiceManager;->jrmexceptionListener:Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;

    invoke-virtual {v1, v2}, Lcom/jrm/service/JRMServiceAgent;->setConnection(Lcom/jrm/service/JRMExceptionListener;)V

    sget-object v1, Lcom/jrm/service/JRMServiceManager;->agent:Lcom/jrm/service/JRMServiceAgent;

    invoke-interface {p1, v1}, Lcom/jrm/service/IJRMConnectListener;->onConnectedSuccess(Lcom/jrm/service/JRMServiceAgent;)V

    goto :goto_0
.end method

.method public static connectToJRM(Landroid/content/Context;Lcom/jrm/service/IJRMConnectListener;Lcom/jrm/service/JRMExceptionListener;)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/jrm/service/IJRMConnectListener;
    .param p2    # Lcom/jrm/service/JRMExceptionListener;

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/jrm/service/JRMServiceManager;->pkgName:Ljava/lang/String;

    sput-object p1, Lcom/jrm/service/JRMServiceManager;->jrmconnector:Lcom/jrm/service/IJRMConnectListener;

    new-instance v1, Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;

    invoke-direct {v1, p2}, Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;-><init>(Lcom/jrm/service/JRMExceptionListener;)V

    sput-object v1, Lcom/jrm/service/JRMServiceManager;->jrmexceptionListener:Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;

    sput-object p0, Lcom/jrm/service/JRMServiceManager;->mContext:Landroid/content/Context;

    sget-object v1, Lcom/jrm/service/JRMServiceManager;->agent:Lcom/jrm/service/JRMServiceAgent;

    if-nez v1, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.jrm.core.container.ServiceContainerService"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    sget-object v1, Lcom/jrm/service/JRMServiceManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    invoke-static {}, Lcom/jrm/service/JRMServiceManager;->aysnConnectToServiceContainer()V

    :goto_0
    return-void

    :cond_0
    sget-object v1, Lcom/jrm/service/JRMServiceManager;->agent:Lcom/jrm/service/JRMServiceAgent;

    sget-object v2, Lcom/jrm/service/JRMServiceManager;->pkgName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/jrm/service/JRMServiceAgent;->setPkgName(Ljava/lang/String;)V

    sget-object v1, Lcom/jrm/service/JRMServiceManager;->agent:Lcom/jrm/service/JRMServiceAgent;

    sget-object v2, Lcom/jrm/service/JRMServiceManager;->jrmexceptionListener:Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;

    invoke-virtual {v1, v2}, Lcom/jrm/service/JRMServiceAgent;->setConnection(Lcom/jrm/service/JRMExceptionListener;)V

    sget-object v1, Lcom/jrm/service/JRMServiceManager;->agent:Lcom/jrm/service/JRMServiceAgent;

    invoke-interface {p1, v1}, Lcom/jrm/service/IJRMConnectListener;->onConnectedSuccess(Lcom/jrm/service/JRMServiceAgent;)V

    goto :goto_0
.end method

.method public static disConnect(Landroid/content/Context;)V
    .locals 0
    .param p0    # Landroid/content/Context;

    return-void
.end method

.method public static reconnectToJRM(Landroid/app/Activity;)Lcom/jrm/service/JRMServiceAgent;
    .locals 2
    .param p0    # Landroid/app/Activity;

    sput-object p0, Lcom/jrm/service/JRMServiceManager;->mContext:Landroid/content/Context;

    sget-object v0, Lcom/jrm/service/JRMServiceManager;->agent:Lcom/jrm/service/JRMServiceAgent;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;

    invoke-direct {v0, p0}, Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;-><init>(Landroid/app/Activity;)V

    sput-object v0, Lcom/jrm/service/JRMServiceManager;->jrmexceptionListener:Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;

    sget-object v0, Lcom/jrm/service/JRMServiceManager;->agent:Lcom/jrm/service/JRMServiceAgent;

    sget-object v1, Lcom/jrm/service/JRMServiceManager;->pkgName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/jrm/service/JRMServiceAgent;->setPkgName(Ljava/lang/String;)V

    sget-object v0, Lcom/jrm/service/JRMServiceManager;->agent:Lcom/jrm/service/JRMServiceAgent;

    sget-object v1, Lcom/jrm/service/JRMServiceManager;->jrmexceptionListener:Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;

    invoke-virtual {v0, v1}, Lcom/jrm/service/JRMServiceAgent;->setConnection(Lcom/jrm/service/JRMExceptionListener;)V

    sget-object v0, Lcom/jrm/service/JRMServiceManager;->agent:Lcom/jrm/service/JRMServiceAgent;

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "JRM"

    const-string v1, "jrm agent is null. pls connectToJrmFramework() first."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static reconnectToJRM(Landroid/app/Activity;Lcom/jrm/service/JRMExceptionListener;)Lcom/jrm/service/JRMServiceAgent;
    .locals 2
    .param p0    # Landroid/app/Activity;
    .param p1    # Lcom/jrm/service/JRMExceptionListener;

    sput-object p0, Lcom/jrm/service/JRMServiceManager;->mContext:Landroid/content/Context;

    sget-object v0, Lcom/jrm/service/JRMServiceManager;->agent:Lcom/jrm/service/JRMServiceAgent;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;

    invoke-direct {v0, p1}, Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;-><init>(Lcom/jrm/service/JRMExceptionListener;)V

    sput-object v0, Lcom/jrm/service/JRMServiceManager;->jrmexceptionListener:Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;

    sget-object v0, Lcom/jrm/service/JRMServiceManager;->agent:Lcom/jrm/service/JRMServiceAgent;

    sget-object v1, Lcom/jrm/service/JRMServiceManager;->pkgName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/jrm/service/JRMServiceAgent;->setPkgName(Ljava/lang/String;)V

    sget-object v0, Lcom/jrm/service/JRMServiceManager;->agent:Lcom/jrm/service/JRMServiceAgent;

    sget-object v1, Lcom/jrm/service/JRMServiceManager;->jrmexceptionListener:Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;

    invoke-virtual {v0, v1}, Lcom/jrm/service/JRMServiceAgent;->setConnection(Lcom/jrm/service/JRMExceptionListener;)V

    sget-object v0, Lcom/jrm/service/JRMServiceManager;->agent:Lcom/jrm/service/JRMServiceAgent;

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "JRM"

    const-string v1, "jrm agent is null. pls connectToJrmFramework() first."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static setJrmexceptionListener(Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;)V
    .locals 0
    .param p0    # Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;

    sput-object p0, Lcom/jrm/service/JRMServiceManager;->jrmexceptionListener:Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;

    return-void
.end method
