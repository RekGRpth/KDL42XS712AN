.class Lcom/jrm/service/JRMServiceManager$1;
.super Ljava/lang/Object;
.source "JRMServiceManager.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jrm/service/JRMServiceManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 4
    .param p1    # Landroid/content/ComponentName;
    .param p2    # Landroid/os/IBinder;

    invoke-static {p2}, Lcom/jrm/core/container/IServiceContainer$Stub;->asInterface(Landroid/os/IBinder;)Lcom/jrm/core/container/IServiceContainer;

    move-result-object v0

    new-instance v1, Lcom/jrm/service/JRMServiceAgent;

    # getter for: Lcom/jrm/service/JRMServiceManager;->jrmexceptionListener:Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;
    invoke-static {}, Lcom/jrm/service/JRMServiceManager;->access$0()Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;

    move-result-object v2

    # getter for: Lcom/jrm/service/JRMServiceManager;->pkgName:Ljava/lang/String;
    invoke-static {}, Lcom/jrm/service/JRMServiceManager;->access$1()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v0, v2, v3}, Lcom/jrm/service/JRMServiceAgent;-><init>(Lcom/jrm/core/container/IServiceContainer;Lcom/jrm/service/JRMExceptionListener;Ljava/lang/String;)V

    invoke-static {v1}, Lcom/jrm/service/JRMServiceManager;->access$2(Lcom/jrm/service/JRMServiceAgent;)V

    # getter for: Lcom/jrm/service/JRMServiceManager;->jrmconnector:Lcom/jrm/service/IJRMConnectListener;
    invoke-static {}, Lcom/jrm/service/JRMServiceManager;->access$3()Lcom/jrm/service/IJRMConnectListener;

    move-result-object v1

    # getter for: Lcom/jrm/service/JRMServiceManager;->agent:Lcom/jrm/service/JRMServiceAgent;
    invoke-static {}, Lcom/jrm/service/JRMServiceManager;->access$4()Lcom/jrm/service/JRMServiceAgent;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/jrm/service/IJRMConnectListener;->onConnectedSuccess(Lcom/jrm/service/JRMServiceAgent;)V

    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 1
    .param p1    # Landroid/content/ComponentName;

    const/4 v0, 0x0

    invoke-static {v0}, Lcom/jrm/service/JRMServiceManager;->access$2(Lcom/jrm/service/JRMServiceAgent;)V

    invoke-static {v0}, Lcom/jrm/service/JRMServiceManager;->access$5(Lcom/jrm/service/IJRMConnectListener;)V

    return-void
.end method
