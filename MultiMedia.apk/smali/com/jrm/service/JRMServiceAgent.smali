.class public Lcom/jrm/service/JRMServiceAgent;
.super Lcom/jrm/service/AbstractJRMServiceAgent;
.source "JRMServiceAgent.java"


# instance fields
.field private final VERSION:I

.field private mDistrictService:Lcom/jrm/friend/binder/JRMDistrictService;

.field private mFriendService:Lcom/jrm/friend/binder/JRMFriendService;

.field private mIMService:Lcom/jrm/im/binder/JRMIMService;


# direct methods
.method public constructor <init>(Lcom/jrm/core/container/IServiceContainer;Lcom/jrm/service/JRMExceptionListener;Ljava/lang/String;)V
    .locals 2
    .param p1    # Lcom/jrm/core/container/IServiceContainer;
    .param p2    # Lcom/jrm/service/JRMExceptionListener;
    .param p3    # Ljava/lang/String;

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, p3}, Lcom/jrm/service/AbstractJRMServiceAgent;-><init>(Lcom/jrm/core/container/IServiceContainer;Lcom/jrm/service/JRMExceptionListener;Ljava/lang/String;)V

    const/4 v0, 0x1

    iput v0, p0, Lcom/jrm/service/JRMServiceAgent;->VERSION:I

    iput-object v1, p0, Lcom/jrm/service/JRMServiceAgent;->mFriendService:Lcom/jrm/friend/binder/JRMFriendService;

    iput-object v1, p0, Lcom/jrm/service/JRMServiceAgent;->mIMService:Lcom/jrm/im/binder/JRMIMService;

    iput-object v1, p0, Lcom/jrm/service/JRMServiceAgent;->mDistrictService:Lcom/jrm/friend/binder/JRMDistrictService;

    return-void
.end method


# virtual methods
.method public getAppStoreService()Lcom/jrm/appstore/agent/AppStoreAgent;
    .locals 5

    const-string v1, "com.jrm.appstore.framework"

    const-string v0, "1.0.0"

    const-string v2, "com.jrm.appstore.service.AppStoreService"

    new-instance v3, Lcom/jrm/appstore/agent/AppStoreAgent;

    invoke-virtual {p0, v1, v0, v2}, Lcom/jrm/service/JRMServiceAgent;->getBpBinder(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v4

    invoke-static {v4}, Lcom/jrm/appstore/service/IAppStoreService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/jrm/appstore/service/IAppStoreService;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/jrm/appstore/agent/AppStoreAgent;-><init>(Lcom/jrm/appstore/service/IAppStoreService;)V

    return-object v3
.end method

.method public getCmpManagementService()Lcom/jrm/core/container/cmps/management/ICmpManagerService;
    .locals 7

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/jrm/service/JRMServiceAgent;->mServiceContainer:Lcom/jrm/core/container/IServiceContainer;

    if-eqz v2, :cond_0

    :try_start_0
    iget-object v2, p0, Lcom/jrm/service/JRMServiceAgent;->mServiceContainer:Lcom/jrm/core/container/IServiceContainer;

    const/4 v3, 0x0

    const-string v4, "jrmsys_mgt_upgrade"

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-interface {v2, v3, v4, v5, v6}, Lcom/jrm/core/container/IServiceContainer;->getService(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Lcom/jrm/core/container/cmps/management/ICmpManagerService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/jrm/core/container/cmps/management/ICmpManagerService;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :cond_0
    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getDevService()Lcom/jrm/core/container/cmps/dev/Idev;
    .locals 7

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/jrm/service/JRMServiceAgent;->mServiceContainer:Lcom/jrm/core/container/IServiceContainer;

    if-eqz v2, :cond_0

    :try_start_0
    iget-object v2, p0, Lcom/jrm/service/JRMServiceAgent;->mServiceContainer:Lcom/jrm/core/container/IServiceContainer;

    const/4 v3, 0x0

    const-string v4, "jrmsys_devService"

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-interface {v2, v3, v4, v5, v6}, Lcom/jrm/core/container/IServiceContainer;->getService(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Lcom/jrm/core/container/cmps/dev/Idev$Stub;->asInterface(Landroid/os/IBinder;)Lcom/jrm/core/container/cmps/dev/Idev;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :cond_0
    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getDistrictService()Lcom/jrm/friend/binder/JRMDistrictService;
    .locals 5

    iget-object v4, p0, Lcom/jrm/service/JRMServiceAgent;->mDistrictService:Lcom/jrm/friend/binder/JRMDistrictService;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/jrm/service/JRMServiceAgent;->mDistrictService:Lcom/jrm/friend/binder/JRMDistrictService;

    :goto_0
    return-object v4

    :cond_0
    const-string v1, "com.jrm.im"

    const-string v0, "1.0.0"

    const-string v2, "com.jrm.friend.binder.JRMDistrictManagerService"

    invoke-virtual {p0, v1, v0, v2}, Lcom/jrm/service/JRMServiceAgent;->getBpBinder(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v4

    invoke-static {v4}, Lcom/jrm/friend/aidl/IDistrictManager$Stub;->asInterface(Landroid/os/IBinder;)Lcom/jrm/friend/aidl/IDistrictManager;

    move-result-object v3

    if-nez v3, :cond_1

    const/4 v4, 0x0

    goto :goto_0

    :cond_1
    new-instance v4, Lcom/jrm/friend/binder/JRMDistrictService;

    invoke-direct {v4, v3}, Lcom/jrm/friend/binder/JRMDistrictService;-><init>(Lcom/jrm/friend/aidl/IDistrictManager;)V

    iput-object v4, p0, Lcom/jrm/service/JRMServiceAgent;->mDistrictService:Lcom/jrm/friend/binder/JRMDistrictService;

    iget-object v4, p0, Lcom/jrm/service/JRMServiceAgent;->mDistrictService:Lcom/jrm/friend/binder/JRMDistrictService;

    goto :goto_0
.end method

.method public getFileFlyManager()Lcom/jrm/filefly/agent/FileFlyManager;
    .locals 7

    const-string v1, "com.jrm.filefly.framework"

    const-string v0, "1.0.0"

    const-string v2, "com.jrm.filefly.service.FileFlyManagerService"

    invoke-virtual {p0, v1, v0, v2}, Lcom/jrm/service/JRMServiceAgent;->getBpBinder(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v4

    invoke-static {v4}, Lcom/jrm/filefly/service/IFileFlyManagerService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/jrm/filefly/service/IFileFlyManagerService;

    move-result-object v3

    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "service:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    if-eqz v3, :cond_0

    new-instance v4, Lcom/jrm/filefly/agent/FileFlyManager;

    invoke-direct {v4, v3}, Lcom/jrm/filefly/agent/FileFlyManager;-><init>(Lcom/jrm/filefly/service/IFileFlyManagerService;)V

    :goto_0
    return-object v4

    :cond_0
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public getFriendService()Lcom/jrm/friend/binder/JRMFriendService;
    .locals 5

    iget-object v4, p0, Lcom/jrm/service/JRMServiceAgent;->mFriendService:Lcom/jrm/friend/binder/JRMFriendService;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/jrm/service/JRMServiceAgent;->mFriendService:Lcom/jrm/friend/binder/JRMFriendService;

    :goto_0
    return-object v4

    :cond_0
    const-string v1, "com.jrm.im"

    const-string v0, "1.0.0"

    const-string v2, "com.jrm.friend.binder.JRMFriendManagerService"

    invoke-virtual {p0, v1, v0, v2}, Lcom/jrm/service/JRMServiceAgent;->getBpBinder(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v4

    invoke-static {v4}, Lcom/jrm/friend/aidl/IFriendManagerService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/jrm/friend/aidl/IFriendManagerService;

    move-result-object v3

    if-nez v3, :cond_1

    const/4 v4, 0x0

    goto :goto_0

    :cond_1
    new-instance v4, Lcom/jrm/friend/binder/JRMFriendService;

    invoke-direct {v4, v3}, Lcom/jrm/friend/binder/JRMFriendService;-><init>(Lcom/jrm/friend/aidl/IFriendManagerService;)V

    iput-object v4, p0, Lcom/jrm/service/JRMServiceAgent;->mFriendService:Lcom/jrm/friend/binder/JRMFriendService;

    iget-object v4, p0, Lcom/jrm/service/JRMServiceAgent;->mFriendService:Lcom/jrm/friend/binder/JRMFriendService;

    goto :goto_0
.end method

.method public getIMService()Lcom/jrm/im/binder/JRMIMService;
    .locals 5

    iget-object v4, p0, Lcom/jrm/service/JRMServiceAgent;->mIMService:Lcom/jrm/im/binder/JRMIMService;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/jrm/service/JRMServiceAgent;->mIMService:Lcom/jrm/im/binder/JRMIMService;

    :goto_0
    return-object v4

    :cond_0
    const-string v1, "com.jrm.im"

    const-string v0, "1.0.0"

    const-string v2, "com.jrm.im.binder.JRMIMManagerService"

    invoke-virtual {p0, v1, v0, v2}, Lcom/jrm/service/JRMServiceAgent;->getBpBinder(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v4

    invoke-static {v4}, Lcom/jrm/im/aidl/IMManagerService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/jrm/im/aidl/IMManagerService;

    move-result-object v3

    if-nez v3, :cond_1

    const/4 v4, 0x0

    goto :goto_0

    :cond_1
    new-instance v4, Lcom/jrm/im/binder/JRMIMService;

    invoke-direct {v4, v3}, Lcom/jrm/im/binder/JRMIMService;-><init>(Lcom/jrm/im/aidl/IMManagerService;)V

    iput-object v4, p0, Lcom/jrm/service/JRMServiceAgent;->mIMService:Lcom/jrm/im/binder/JRMIMService;

    iget-object v4, p0, Lcom/jrm/service/JRMServiceAgent;->mIMService:Lcom/jrm/im/binder/JRMIMService;

    goto :goto_0
.end method

.method public getProfileManager()Lcom/jrm/profile/TvDeviceProfileManager;
    .locals 8

    const/4 v2, 0x0

    iget-object v1, p0, Lcom/jrm/service/JRMServiceAgent;->mServiceContainer:Lcom/jrm/core/container/IServiceContainer;

    if-eqz v1, :cond_0

    :try_start_0
    new-instance v1, Lcom/jrm/profile/TvDeviceProfileManager;

    iget-object v3, p0, Lcom/jrm/service/JRMServiceAgent;->mServiceContainer:Lcom/jrm/core/container/IServiceContainer;

    const/4 v4, 0x0

    const-string v5, "jrmsys_profileService"

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-interface {v3, v4, v5, v6, v7}, Lcom/jrm/core/container/IServiceContainer;->getService(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v3

    invoke-static {v3}, Lcom/jrm/core/container/cmps/authorize/ITvDeviceInfo$Stub;->asInterface(Landroid/os/IBinder;)Lcom/jrm/core/container/cmps/authorize/ITvDeviceInfo;

    move-result-object v3

    invoke-direct {v1, v3}, Lcom/jrm/profile/TvDeviceProfileManager;-><init>(Lcom/jrm/core/container/cmps/authorize/ITvDeviceInfo;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    :cond_0
    move-object v1, v2

    goto :goto_0
.end method

.method public getTestService()Lcom/test/demo1/test;
    .locals 4

    const-string v1, "com.test.demo1"

    const-string v0, "1.2.0"

    const-string v2, "com.test.demo1.test"

    invoke-virtual {p0, v1, v0, v2}, Lcom/jrm/service/JRMServiceAgent;->getBpBinder(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v3

    invoke-static {v3}, Lcom/test/demo1/test$Stub;->asInterface(Landroid/os/IBinder;)Lcom/test/demo1/test;

    move-result-object v3

    return-object v3
.end method

.method public getUpgradeService()Lcom/jrm/core/container/cmps/upgrade/IUpgradeService;
    .locals 7

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/jrm/service/JRMServiceAgent;->mServiceContainer:Lcom/jrm/core/container/IServiceContainer;

    if-eqz v2, :cond_0

    :try_start_0
    iget-object v2, p0, Lcom/jrm/service/JRMServiceAgent;->mServiceContainer:Lcom/jrm/core/container/IServiceContainer;

    const/4 v3, 0x0

    const-string v4, "jrmsys_IUpgradeService"

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-interface {v2, v3, v4, v5, v6}, Lcom/jrm/core/container/IServiceContainer;->getService(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Lcom/jrm/core/container/cmps/upgrade/IUpgradeService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/jrm/core/container/cmps/upgrade/IUpgradeService;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :cond_0
    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getVVoipService()Lcom/jrm/vvoip/agent/VVoipAgent;
    .locals 5

    const-string v1, "com.jrm.vvoip.framework"

    const-string v0, "1.0.0"

    const-string v2, "com.jrm.vvoip.service.VVoipService"

    new-instance v3, Lcom/jrm/vvoip/agent/VVoipAgent;

    invoke-virtual {p0, v1, v0, v2}, Lcom/jrm/service/JRMServiceAgent;->getBpBinder(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v4

    invoke-static {v4}, Lcom/jrm/vvoip/service/IVVoipService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/jrm/vvoip/service/IVVoipService;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/jrm/vvoip/agent/VVoipAgent;-><init>(Lcom/jrm/vvoip/service/IVVoipService;)V

    return-object v3
.end method
