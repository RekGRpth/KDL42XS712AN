.class Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;
.super Lcom/jrm/core/container/IServiceContainerListener$Stub;
.source "JRMServiceManager.java"

# interfaces
.implements Lcom/jrm/service/JRMExceptionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jrm/service/JRMServiceManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "JRMExceptionListenerImpl"
.end annotation


# instance fields
.field private dialog:Landroid/app/ProgressDialog;

.field private mContext:Landroid/content/Context;

.field private mExceptionListener:Lcom/jrm/service/JRMExceptionListener;


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 0
    .param p1    # Landroid/app/Activity;

    invoke-direct {p0}, Lcom/jrm/core/container/IServiceContainerListener$Stub;-><init>()V

    iput-object p1, p0, Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;->mContext:Landroid/content/Context;

    return-void
.end method

.method public constructor <init>(Lcom/jrm/service/JRMExceptionListener;)V
    .locals 0
    .param p1    # Lcom/jrm/service/JRMExceptionListener;

    invoke-direct {p0}, Lcom/jrm/core/container/IServiceContainerListener$Stub;-><init>()V

    iput-object p1, p0, Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;->mExceptionListener:Lcom/jrm/service/JRMExceptionListener;

    return-void
.end method

.method static synthetic access$0(Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1(Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;Landroid/app/ProgressDialog;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;->dialog:Landroid/app/ProgressDialog;

    return-void
.end method

.method static synthetic access$2(Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;)Landroid/app/ProgressDialog;
    .locals 1

    iget-object v0, p0, Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;->dialog:Landroid/app/ProgressDialog;

    return-object v0
.end method


# virtual methods
.method public onServiceBindException(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;

    iget-object v4, p0, Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;->mExceptionListener:Lcom/jrm/service/JRMExceptionListener;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;->mExceptionListener:Lcom/jrm/service/JRMExceptionListener;

    invoke-interface {v4, p1, p2, p3, p4}, Lcom/jrm/service/JRMExceptionListener;->onServiceBindException(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    const-string v4, "JRM"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "JRMExceptionListenerImpl:onServiceBindException:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_1
    iget-object v4, p0, Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;->mContext:Landroid/content/Context;

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;->mContext:Landroid/content/Context;

    instance-of v4, v4, Landroid/app/Activity;

    if-eqz v4, :cond_3

    iget-object v0, p0, Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    const/16 v4, 0xb

    if-eq p1, v4, :cond_2

    const/16 v4, 0xc

    if-ne p1, v4, :cond_0

    :cond_2
    :try_start_0
    new-instance v4, Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl$1;

    invoke-direct {v4, p0, p1}, Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl$1;-><init>(Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;I)V

    invoke-virtual {v0, v4}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    # getter for: Lcom/jrm/service/JRMServiceManager;->agent:Lcom/jrm/service/JRMServiceAgent;
    invoke-static {}, Lcom/jrm/service/JRMServiceManager;->access$4()Lcom/jrm/service/JRMServiceAgent;

    move-result-object v4

    invoke-virtual {v4}, Lcom/jrm/service/JRMServiceAgent;->getCmpManagementService()Lcom/jrm/core/container/cmps/management/ICmpManagerService;

    move-result-object v1

    new-instance v4, Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl$2;

    invoke-direct {v4, p0, v0}, Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl$2;-><init>(Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;Landroid/app/Activity;)V

    invoke-interface {v1, p2, p3, v4}, Lcom/jrm/core/container/cmps/management/ICmpManagerService;->upgradecmp(Ljava/lang/String;Ljava/lang/String;Lcom/jrm/core/container/cmps/management/ICmpUpgradeListener;)Z

    move-result v3

    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "updateCmp:::"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    :cond_3
    const-string v4, "JRM"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "EXCEPTION:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "...ERRORCODE:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onServiceContainerPreRestart()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v1, p0, Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;->mContext:Landroid/content/Context;

    instance-of v1, v1, Landroid/app/Activity;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    new-instance v1, Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl$3;

    invoke-direct {v1, p0}, Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl$3;-><init>(Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    :goto_0
    return-void

    :cond_0
    const-string v1, "JRM"

    const-string v2, "\u5347\u7ea7\u4e86\u540e\u53f0\u7ec4\u4ef6\uff0c\u9700\u91cd\u542f\u5e94\u7528"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v1

    invoke-static {v1}, Landroid/os/Process;->killProcess(I)V

    goto :goto_0
.end method
