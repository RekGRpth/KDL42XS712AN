.class public interface abstract Lcom/jrm/service/JRMExceptionListener;
.super Ljava/lang/Object;
.source "JRMExceptionListener.java"


# static fields
.field public static final CMP_NOTFOUND:I = 0xb

.field public static final CMP_NOT_STATED:I = 0x16

.field public static final CMP_START_FAILED:I = 0x15

.field public static final CMP_VERSION_TOO_LARGE:I = 0xd

.field public static final CMP_VERSION_TOO_SMAILL:I = 0xc

.field public static final SECURITY_ACCESS_DENIED:I = 0x2

.field public static final SECURITY_CERTIFICATE_EXPIRED:I = 0x5

.field public static final SECURITY_CERTIFICATE_NOTFOUND:I = 0x6

.field public static final SECURITY_MACHINE_AUTHORIZE_FAILED:I = 0x3

.field public static final UNKNOWN_EXCEPTION:I = 0x5a


# virtual methods
.method public abstract onServiceBindException(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end method
