.class Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl$2$2;
.super Ljava/lang/Object;
.source "JRMServiceManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl$2;->onUpgradeFailed(ILjava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$2:Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl$2;


# direct methods
.method constructor <init>(Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl$2;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl$2$2;->this$2:Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl$2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    iget-object v3, p0, Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl$2$2;->this$2:Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl$2;

    # getter for: Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl$2;->this$1:Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;
    invoke-static {v3}, Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl$2;->access$0(Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl$2;)Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;

    move-result-object v3

    # getter for: Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;->dialog:Landroid/app/ProgressDialog;
    invoke-static {v3}, Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;->access$2(Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;)Landroid/app/ProgressDialog;

    move-result-object v3

    if-eqz v3, :cond_0

    :try_start_0
    iget-object v3, p0, Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl$2$2;->this$2:Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl$2;

    # getter for: Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl$2;->this$1:Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;
    invoke-static {v3}, Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl$2;->access$0(Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl$2;)Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;

    move-result-object v3

    # getter for: Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;->dialog:Landroid/app/ProgressDialog;
    invoke-static {v3}, Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;->access$2(Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;)Landroid/app/ProgressDialog;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/ProgressDialog;->dismiss()V

    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v3, p0, Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl$2$2;->this$2:Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl$2;

    # getter for: Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl$2;->this$1:Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;
    invoke-static {v3}, Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl$2;->access$0(Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl$2;)Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;

    move-result-object v3

    # getter for: Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;->access$0(Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl;)Landroid/content/Context;

    move-result-object v3

    invoke-direct {v1, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const-string v3, "\u540e\u53f0\u7ec4\u4ef6\u66f4\u65b0\u5931\u8d25\uff0c\u5c06\u5f71\u54cd\u529f\u80fd\u7684\u4f7f\u7528!"

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const-string v4, "\u786e\u5b9a"

    new-instance v5, Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl$2$2$1;

    invoke-direct {v5, p0}, Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl$2$2$1;-><init>(Lcom/jrm/service/JRMServiceManager$JRMExceptionListenerImpl$2$2;)V

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
