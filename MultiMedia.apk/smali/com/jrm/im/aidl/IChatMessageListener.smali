.class public interface abstract Lcom/jrm/im/aidl/IChatMessageListener;
.super Ljava/lang/Object;
.source "IChatMessageListener.java"

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/jrm/im/aidl/IChatMessageListener$Stub;
    }
.end annotation


# virtual methods
.method public abstract openedChatMessage(ILjava/lang/String;J)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract unOpenedChatMessage(ILjava/lang/String;J)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
