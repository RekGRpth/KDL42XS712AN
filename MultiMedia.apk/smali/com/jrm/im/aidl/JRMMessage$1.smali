.class Lcom/jrm/im/aidl/JRMMessage$1;
.super Ljava/lang/Object;
.source "JRMMessage.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jrm/im/aidl/JRMMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/jrm/im/aidl/JRMMessage;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/jrm/im/aidl/JRMMessage;
    .locals 3
    .param p1    # Landroid/os/Parcel;

    new-instance v0, Lcom/jrm/im/aidl/JRMMessage;

    invoke-direct {v0}, Lcom/jrm/im/aidl/JRMMessage;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, v0, Lcom/jrm/im/aidl/JRMMessage;->sender:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/jrm/im/aidl/JRMMessage;->messageBody:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, v0, Lcom/jrm/im/aidl/JRMMessage;->to:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, v0, Lcom/jrm/im/aidl/JRMMessage;->bbNO:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v1

    iput-wide v1, v0, Lcom/jrm/im/aidl/JRMMessage;->timeStamp:J

    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/jrm/im/aidl/JRMMessage$1;->createFromParcel(Landroid/os/Parcel;)Lcom/jrm/im/aidl/JRMMessage;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/jrm/im/aidl/JRMMessage;
    .locals 1
    .param p1    # I

    new-array v0, p1, [Lcom/jrm/im/aidl/JRMMessage;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/jrm/im/aidl/JRMMessage$1;->newArray(I)[Lcom/jrm/im/aidl/JRMMessage;

    move-result-object v0

    return-object v0
.end method
