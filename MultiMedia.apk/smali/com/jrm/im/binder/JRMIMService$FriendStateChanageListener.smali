.class Lcom/jrm/im/binder/JRMIMService$FriendStateChanageListener;
.super Lcom/jrm/im/aidl/IFriendStateChangeListener$Stub;
.source "JRMIMService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jrm/im/binder/JRMIMService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FriendStateChanageListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/im/binder/JRMIMService;


# direct methods
.method private constructor <init>(Lcom/jrm/im/binder/JRMIMService;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/im/binder/JRMIMService$FriendStateChanageListener;->this$0:Lcom/jrm/im/binder/JRMIMService;

    invoke-direct {p0}, Lcom/jrm/im/aidl/IFriendStateChangeListener$Stub;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/jrm/im/binder/JRMIMService;Lcom/jrm/im/binder/JRMIMService$FriendStateChanageListener;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/jrm/im/binder/JRMIMService$FriendStateChanageListener;-><init>(Lcom/jrm/im/binder/JRMIMService;)V

    return-void
.end method


# virtual methods
.method public friendStateChanged(II)V
    .locals 5
    .param p1    # I
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v3, p0, Lcom/jrm/im/binder/JRMIMService$FriendStateChanageListener;->this$0:Lcom/jrm/im/binder/JRMIMService;

    # getter for: Lcom/jrm/im/binder/JRMIMService;->mFriendStateListenerList:Ljava/util/List;
    invoke-static {v3}, Lcom/jrm/im/binder/JRMIMService;->access$1(Lcom/jrm/im/binder/JRMIMService;)Ljava/util/List;

    move-result-object v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/jrm/im/binder/JRMIMService$FriendStateChanageListener;->this$0:Lcom/jrm/im/binder/JRMIMService;

    # getter for: Lcom/jrm/im/binder/JRMIMService;->mFriendStateList:Ljava/util/List;
    invoke-static {v3}, Lcom/jrm/im/binder/JRMIMService;->access$2(Lcom/jrm/im/binder/JRMIMService;)Ljava/util/List;

    move-result-object v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/jrm/im/binder/JRMIMService$FriendStateChanageListener;->this$0:Lcom/jrm/im/binder/JRMIMService;

    # getter for: Lcom/jrm/im/binder/JRMIMService;->mFriendStateList:Ljava/util/List;
    invoke-static {v3}, Lcom/jrm/im/binder/JRMIMService;->access$2(Lcom/jrm/im/binder/JRMIMService;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_2

    new-instance v1, Lcom/jrm/im/binder/JRMIMService$FriendStatus;

    iget-object v3, p0, Lcom/jrm/im/binder/JRMIMService$FriendStateChanageListener;->this$0:Lcom/jrm/im/binder/JRMIMService;

    invoke-direct {v1, v3, p1, p2}, Lcom/jrm/im/binder/JRMIMService$FriendStatus;-><init>(Lcom/jrm/im/binder/JRMIMService;II)V

    iget-object v3, p0, Lcom/jrm/im/binder/JRMIMService$FriendStateChanageListener;->this$0:Lcom/jrm/im/binder/JRMIMService;

    # getter for: Lcom/jrm/im/binder/JRMIMService;->mFriendStateList:Ljava/util/List;
    invoke-static {v3}, Lcom/jrm/im/binder/JRMIMService;->access$2(Lcom/jrm/im/binder/JRMIMService;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    return-void

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jrm/im/binder/JRMIMService$FriendStatus;

    iget v4, v0, Lcom/jrm/im/binder/JRMIMService$FriendStatus;->friendBBNo:I

    if-ne v4, p1, :cond_0

    iget v4, v0, Lcom/jrm/im/binder/JRMIMService$FriendStatus;->friendStatus:I

    if-eq v4, p2, :cond_0

    iput p2, v0, Lcom/jrm/im/binder/JRMIMService$FriendStatus;->friendStatus:I

    iget-object v3, p0, Lcom/jrm/im/binder/JRMIMService$FriendStateChanageListener;->this$0:Lcom/jrm/im/binder/JRMIMService;

    # getter for: Lcom/jrm/im/binder/JRMIMService;->mFriendStateListenerList:Ljava/util/List;
    invoke-static {v3}, Lcom/jrm/im/binder/JRMIMService;->access$1(Lcom/jrm/im/binder/JRMIMService;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/jrm/im/listener/OnFriendStateChangeListener;

    invoke-interface {v2, p1, p2}, Lcom/jrm/im/listener/OnFriendStateChangeListener;->onFriendStateChange(II)V

    goto :goto_0
.end method
