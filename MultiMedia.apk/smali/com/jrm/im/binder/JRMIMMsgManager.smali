.class public Lcom/jrm/im/binder/JRMIMMsgManager;
.super Ljava/lang/Object;
.source "JRMIMMsgManager.java"


# static fields
.field private static final SERVICE_PACKAGE_NAME:Ljava/lang/String; = "com.jrm.im.binder.JRMIMMessageService"

.field private static final SP_PACKAGE_NAME:Ljava/lang/String; = "com.jrm"

.field private static final VERSION:Ljava/lang/String; = "1.0.0"


# instance fields
.field private mIMMsgManagerService:Lcom/jrm/im/aidl/IMessageService;


# direct methods
.method public constructor <init>(Lcom/jrm/im/aidl/IMessageService;)V
    .locals 1
    .param p1    # Lcom/jrm/im/aidl/IMessageService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/jrm/im/binder/JRMIMMsgManager;->mIMMsgManagerService:Lcom/jrm/im/aidl/IMessageService;

    iput-object p1, p0, Lcom/jrm/im/binder/JRMIMMsgManager;->mIMMsgManagerService:Lcom/jrm/im/aidl/IMessageService;

    return-void
.end method


# virtual methods
.method public JRMIMMsgManagerInit()Z
    .locals 1

    iget-object v0, p0, Lcom/jrm/im/binder/JRMIMMsgManager;->mIMMsgManagerService:Lcom/jrm/im/aidl/IMessageService;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public addMessage(IIIJLjava/lang/String;)V
    .locals 8
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # J
    .param p6    # Ljava/lang/String;

    iget-object v0, p0, Lcom/jrm/im/binder/JRMIMMsgManager;->mIMMsgManagerService:Lcom/jrm/im/aidl/IMessageService;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/jrm/im/binder/JRMIMMsgManager;->mIMMsgManagerService:Lcom/jrm/im/aidl/IMessageService;

    move v1, p1

    move v2, p2

    move v3, p3

    move-wide v4, p4

    move-object v6, p6

    invoke-interface/range {v0 .. v6}, Lcom/jrm/im/aidl/IMessageService;->addMessage(IIIJLjava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v7

    invoke-virtual {v7}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public deleteMessages(IJJ)V
    .locals 7
    .param p1    # I
    .param p2    # J
    .param p4    # J

    iget-object v0, p0, Lcom/jrm/im/binder/JRMIMMsgManager;->mIMMsgManagerService:Lcom/jrm/im/aidl/IMessageService;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/jrm/im/binder/JRMIMMsgManager;->mIMMsgManagerService:Lcom/jrm/im/aidl/IMessageService;

    move v1, p1

    move-wide v2, p2

    move-wide v4, p4

    invoke-interface/range {v0 .. v5}, Lcom/jrm/im/aidl/IMessageService;->deleteMessage(IJJ)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v6

    invoke-virtual {v6}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public destroyManager()V
    .locals 2

    iget-object v1, p0, Lcom/jrm/im/binder/JRMIMMsgManager;->mIMMsgManagerService:Lcom/jrm/im/aidl/IMessageService;

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/jrm/im/binder/JRMIMMsgManager;->mIMMsgManagerService:Lcom/jrm/im/aidl/IMessageService;

    invoke-interface {v1}, Lcom/jrm/im/aidl/IMessageService;->destroyService()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public exportMessages(ILjava/lang/String;)Z
    .locals 2
    .param p1    # I
    .param p2    # Ljava/lang/String;

    iget-object v1, p0, Lcom/jrm/im/binder/JRMIMMsgManager;->mIMMsgManagerService:Lcom/jrm/im/aidl/IMessageService;

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/jrm/im/binder/JRMIMMsgManager;->mIMMsgManagerService:Lcom/jrm/im/aidl/IMessageService;

    invoke-interface {v1, p1, p2}, Lcom/jrm/im/aidl/IMessageService;->exportMessage(ILjava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getMessages(I)Ljava/util/List;
    .locals 2
    .param p1    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lcom/jrm/im/aidl/JRMMessage;",
            ">;"
        }
    .end annotation

    iget-object v1, p0, Lcom/jrm/im/binder/JRMIMMsgManager;->mIMMsgManagerService:Lcom/jrm/im/aidl/IMessageService;

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/jrm/im/binder/JRMIMMsgManager;->mIMMsgManagerService:Lcom/jrm/im/aidl/IMessageService;

    invoke-interface {v1, p1}, Lcom/jrm/im/aidl/IMessageService;->getMessage(I)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
