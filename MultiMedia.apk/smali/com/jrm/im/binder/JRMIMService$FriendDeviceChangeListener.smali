.class Lcom/jrm/im/binder/JRMIMService$FriendDeviceChangeListener;
.super Lcom/jrm/im/aidl/IFriendDeviceChangeListener$Stub;
.source "JRMIMService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jrm/im/binder/JRMIMService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FriendDeviceChangeListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/im/binder/JRMIMService;


# direct methods
.method private constructor <init>(Lcom/jrm/im/binder/JRMIMService;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/im/binder/JRMIMService$FriendDeviceChangeListener;->this$0:Lcom/jrm/im/binder/JRMIMService;

    invoke-direct {p0}, Lcom/jrm/im/aidl/IFriendDeviceChangeListener$Stub;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/jrm/im/binder/JRMIMService;Lcom/jrm/im/binder/JRMIMService$FriendDeviceChangeListener;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/jrm/im/binder/JRMIMService$FriendDeviceChangeListener;-><init>(Lcom/jrm/im/binder/JRMIMService;)V

    return-void
.end method


# virtual methods
.method public onFriendDeviceChange(IIZ)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jrm/im/binder/JRMIMService$FriendDeviceChangeListener;->this$0:Lcom/jrm/im/binder/JRMIMService;

    # getter for: Lcom/jrm/im/binder/JRMIMService;->mFriendDeviceStateListener:Lcom/jrm/im/listener/OnFriendDeviceStateListener;
    invoke-static {v0}, Lcom/jrm/im/binder/JRMIMService;->access$4(Lcom/jrm/im/binder/JRMIMService;)Lcom/jrm/im/listener/OnFriendDeviceStateListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/im/binder/JRMIMService$FriendDeviceChangeListener;->this$0:Lcom/jrm/im/binder/JRMIMService;

    # getter for: Lcom/jrm/im/binder/JRMIMService;->mFriendDeviceStateListener:Lcom/jrm/im/listener/OnFriendDeviceStateListener;
    invoke-static {v0}, Lcom/jrm/im/binder/JRMIMService;->access$4(Lcom/jrm/im/binder/JRMIMService;)Lcom/jrm/im/listener/OnFriendDeviceStateListener;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Lcom/jrm/im/listener/OnFriendDeviceStateListener;->onDeviceStateChange(IIZ)V

    :cond_0
    return-void
.end method
