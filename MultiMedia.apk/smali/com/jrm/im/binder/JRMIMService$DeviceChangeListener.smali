.class Lcom/jrm/im/binder/JRMIMService$DeviceChangeListener;
.super Lcom/jrm/im/aidl/IDeviceChangeListener$Stub;
.source "JRMIMService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jrm/im/binder/JRMIMService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DeviceChangeListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/im/binder/JRMIMService;


# direct methods
.method private constructor <init>(Lcom/jrm/im/binder/JRMIMService;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/im/binder/JRMIMService$DeviceChangeListener;->this$0:Lcom/jrm/im/binder/JRMIMService;

    invoke-direct {p0}, Lcom/jrm/im/aidl/IDeviceChangeListener$Stub;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/jrm/im/binder/JRMIMService;Lcom/jrm/im/binder/JRMIMService$DeviceChangeListener;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/jrm/im/binder/JRMIMService$DeviceChangeListener;-><init>(Lcom/jrm/im/binder/JRMIMService;)V

    return-void
.end method


# virtual methods
.method public onCameraDeviceChange(Z)V
    .locals 1
    .param p1    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jrm/im/binder/JRMIMService$DeviceChangeListener;->this$0:Lcom/jrm/im/binder/JRMIMService;

    # getter for: Lcom/jrm/im/binder/JRMIMService;->mDeviceStateListener:Lcom/jrm/im/listener/OnDeviceChangeListener;
    invoke-static {v0}, Lcom/jrm/im/binder/JRMIMService;->access$3(Lcom/jrm/im/binder/JRMIMService;)Lcom/jrm/im/listener/OnDeviceChangeListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/im/binder/JRMIMService$DeviceChangeListener;->this$0:Lcom/jrm/im/binder/JRMIMService;

    # getter for: Lcom/jrm/im/binder/JRMIMService;->mDeviceStateListener:Lcom/jrm/im/listener/OnDeviceChangeListener;
    invoke-static {v0}, Lcom/jrm/im/binder/JRMIMService;->access$3(Lcom/jrm/im/binder/JRMIMService;)Lcom/jrm/im/listener/OnDeviceChangeListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/jrm/im/listener/OnDeviceChangeListener;->onCameraDeviceChange(Z)V

    :cond_0
    return-void
.end method

.method public onMicDeviceChange(Z)V
    .locals 1
    .param p1    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jrm/im/binder/JRMIMService$DeviceChangeListener;->this$0:Lcom/jrm/im/binder/JRMIMService;

    # getter for: Lcom/jrm/im/binder/JRMIMService;->mDeviceStateListener:Lcom/jrm/im/listener/OnDeviceChangeListener;
    invoke-static {v0}, Lcom/jrm/im/binder/JRMIMService;->access$3(Lcom/jrm/im/binder/JRMIMService;)Lcom/jrm/im/listener/OnDeviceChangeListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/im/binder/JRMIMService$DeviceChangeListener;->this$0:Lcom/jrm/im/binder/JRMIMService;

    # getter for: Lcom/jrm/im/binder/JRMIMService;->mDeviceStateListener:Lcom/jrm/im/listener/OnDeviceChangeListener;
    invoke-static {v0}, Lcom/jrm/im/binder/JRMIMService;->access$3(Lcom/jrm/im/binder/JRMIMService;)Lcom/jrm/im/listener/OnDeviceChangeListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/jrm/im/listener/OnDeviceChangeListener;->onMicDeviceChange(Z)V

    :cond_0
    return-void
.end method
