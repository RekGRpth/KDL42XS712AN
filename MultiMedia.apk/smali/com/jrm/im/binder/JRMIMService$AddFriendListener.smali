.class Lcom/jrm/im/binder/JRMIMService$AddFriendListener;
.super Lcom/jrm/im/aidl/IAddFriendRequestListener$Stub;
.source "JRMIMService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jrm/im/binder/JRMIMService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AddFriendListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/im/binder/JRMIMService;


# direct methods
.method public constructor <init>(Lcom/jrm/im/binder/JRMIMService;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/im/binder/JRMIMService$AddFriendListener;->this$0:Lcom/jrm/im/binder/JRMIMService;

    invoke-direct {p0}, Lcom/jrm/im/aidl/IAddFriendRequestListener$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public requestAgree(I)V
    .locals 1
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jrm/im/binder/JRMIMService$AddFriendListener;->this$0:Lcom/jrm/im/binder/JRMIMService;

    # getter for: Lcom/jrm/im/binder/JRMIMService;->mAddFriendListener:Lcom/jrm/im/listener/OnAddFriendListener;
    invoke-static {v0}, Lcom/jrm/im/binder/JRMIMService;->access$5(Lcom/jrm/im/binder/JRMIMService;)Lcom/jrm/im/listener/OnAddFriendListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/im/binder/JRMIMService$AddFriendListener;->this$0:Lcom/jrm/im/binder/JRMIMService;

    # getter for: Lcom/jrm/im/binder/JRMIMService;->mAddFriendListener:Lcom/jrm/im/listener/OnAddFriendListener;
    invoke-static {v0}, Lcom/jrm/im/binder/JRMIMService;->access$5(Lcom/jrm/im/binder/JRMIMService;)Lcom/jrm/im/listener/OnAddFriendListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/jrm/im/listener/OnAddFriendListener;->requestAgree(I)V

    :cond_0
    return-void
.end method

.method public requestComing(ILjava/lang/String;)V
    .locals 1
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jrm/im/binder/JRMIMService$AddFriendListener;->this$0:Lcom/jrm/im/binder/JRMIMService;

    # getter for: Lcom/jrm/im/binder/JRMIMService;->mAddFriendListener:Lcom/jrm/im/listener/OnAddFriendListener;
    invoke-static {v0}, Lcom/jrm/im/binder/JRMIMService;->access$5(Lcom/jrm/im/binder/JRMIMService;)Lcom/jrm/im/listener/OnAddFriendListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/im/binder/JRMIMService$AddFriendListener;->this$0:Lcom/jrm/im/binder/JRMIMService;

    # getter for: Lcom/jrm/im/binder/JRMIMService;->mAddFriendListener:Lcom/jrm/im/listener/OnAddFriendListener;
    invoke-static {v0}, Lcom/jrm/im/binder/JRMIMService;->access$5(Lcom/jrm/im/binder/JRMIMService;)Lcom/jrm/im/listener/OnAddFriendListener;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/jrm/im/listener/OnAddFriendListener;->requestComing(ILjava/lang/String;)V

    :cond_0
    return-void
.end method

.method public requestRefuse(I)V
    .locals 1
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jrm/im/binder/JRMIMService$AddFriendListener;->this$0:Lcom/jrm/im/binder/JRMIMService;

    # getter for: Lcom/jrm/im/binder/JRMIMService;->mAddFriendListener:Lcom/jrm/im/listener/OnAddFriendListener;
    invoke-static {v0}, Lcom/jrm/im/binder/JRMIMService;->access$5(Lcom/jrm/im/binder/JRMIMService;)Lcom/jrm/im/listener/OnAddFriendListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/im/binder/JRMIMService$AddFriendListener;->this$0:Lcom/jrm/im/binder/JRMIMService;

    # getter for: Lcom/jrm/im/binder/JRMIMService;->mAddFriendListener:Lcom/jrm/im/listener/OnAddFriendListener;
    invoke-static {v0}, Lcom/jrm/im/binder/JRMIMService;->access$5(Lcom/jrm/im/binder/JRMIMService;)Lcom/jrm/im/listener/OnAddFriendListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/jrm/im/listener/OnAddFriendListener;->requestRefuse(I)V

    :cond_0
    return-void
.end method
