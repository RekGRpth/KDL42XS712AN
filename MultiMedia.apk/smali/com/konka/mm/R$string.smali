.class public final Lcom/konka/mm/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/mm/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final Build:I = 0x7f09000f

.field public static final DELETE_ALL_FILE:I = 0x7f090082

.field public static final DELETE_FILE:I = 0x7f090087

.field public static final FAILE:I = 0x7f09008b

.field public static final FILE:I = 0x7f090080

.field public static final FILE_DELETE:I = 0x7f090081

.field public static final FILE_DELETE_SUCCESS:I = 0x7f0900af

.field public static final FILE_LOADING:I = 0x7f09008d

.field public static final FILE_MENU_CANCEL:I = 0x7f090085

.field public static final FILE_MENU_SURE:I = 0x7f090084

.field public static final FILE_TITLE:I = 0x7f09008c

.field public static final Format_not_support:I = 0x7f09005f

.field public static final LaunchAudio:I = 0x7f090003

.field public static final LaunchFileManager:I = 0x7f090006

.field public static final LaunchImage:I = 0x7f090004

.field public static final LaunchShare:I = 0x7f090005

.field public static final LaunchVideo:I = 0x7f090002

.field public static final LaunchWidget:I = 0x7f090007

.field public static final MEDIA_LOADING:I = 0x7f090061

.field public static final MM_APK:I = 0x7f09001e

.field public static final MM_AUDIO:I = 0x7f090017

.field public static final MM_CANCLE:I = 0x7f09000a

.field public static final MM_CONPUTER_UNCONNECT:I = 0x7f090032

.field public static final MM_DISK_TXT:I = 0x7f090044

.field public static final MM_EDIT:I = 0x7f090008

.field public static final MM_ERROR_PASSWORD:I = 0x7f09002f

.field public static final MM_FILE_MANAGER:I = 0x7f09001a

.field public static final MM_FSM_CANCEL:I = 0x7f090021

.field public static final MM_FSM_DEFAULT_HOST_NAME:I = 0x7f090023

.field public static final MM_FSM_LOGIN_PASSWORD:I = 0x7f090027

.field public static final MM_FSM_LOGIN_TITLE:I = 0x7f090025

.field public static final MM_FSM_LOGIN_USER_NAME:I = 0x7f090026

.field public static final MM_FSM_MODULE_NAME:I = 0x7f090022

.field public static final MM_FSM_SEARCH:I = 0x7f090020

.field public static final MM_IMAGE:I = 0x7f090019

.field public static final MM_IS_QUIT_APP:I = 0x7f09000c

.field public static final MM_LOADING:I = 0x7f090036

.field public static final MM_LOADING_FILE:I = 0x7f090038

.field public static final MM_NET_EXCEPTION:I = 0x7f09002c

.field public static final MM_NET_UNCONNECT:I = 0x7f09002d

.field public static final MM_NOFOUND_PHOTO:I = 0x7f090015

.field public static final MM_NOFOUND_SHARE:I = 0x7f09002b

.field public static final MM_NOFOUND_SHARE_HOST:I = 0x7f090024

.field public static final MM_NO_FILE:I = 0x7f090031

.field public static final MM_NO_SHARE:I = 0x7f090030

.field public static final MM_NULL_PASSWORD:I = 0x7f09002e

.field public static final MM_QUIE:I = 0x7f090009

.field public static final MM_QUIT_APP:I = 0x7f09000b

.field public static final MM_QUIT_FILE_MANAGER:I = 0x7f0900aa

.field public static final MM_SCAN_FILE:I = 0x7f090033

.field public static final MM_SCAN_SAMBA:I = 0x7f090034

.field public static final MM_SDCARD_NOT_FOUND:I = 0x7f09000d

.field public static final MM_SELECT_DISK_C:I = 0x7f09003d

.field public static final MM_SELECT_DISK_D:I = 0x7f09003e

.field public static final MM_SELECT_DISK_DEFAULT_STATUS:I = 0x7f090042

.field public static final MM_SELECT_DISK_E:I = 0x7f09003f

.field public static final MM_SELECT_DISK_F:I = 0x7f090040

.field public static final MM_SELECT_DISK_G:I = 0x7f090041

.field public static final MM_SELECT_DISK_PATH:I = 0x7f09003c

.field public static final MM_SHARE:I = 0x7f09001b

.field public static final MM_TXT:I = 0x7f09001d

.field public static final MM_UNMOUNT:I = 0x7f090037

.field public static final MM_UPDATE:I = 0x7f090035

.field public static final MM_VIDEO:I = 0x7f090018

.field public static final MM_WIDGET:I = 0x7f09001c

.field public static final MOUNT_FAILED:I = 0x7f090039

.field public static final NOLRC:I = 0x7f09005c

.field public static final NOT_MM_FILE_FOUND:I = 0x7f090092

.field public static final NOT_STORAGE:I = 0x7f090091

.field public static final QUIT_Audio:I = 0x7f0900ad

.field public static final QUIT_LaunchAudio:I = 0x7f0900da

.field public static final QUIT_LaunchImage:I = 0x7f0900ab

.field public static final QUIT_LaunchVideo:I = 0x7f0900d9

.field public static final QUIT_Share:I = 0x7f0900ae

.field public static final QUIT_Video:I = 0x7f0900ac

.field public static final RENAME_FAILED:I = 0x7f0900a7

.field public static final RENAME_FILE:I = 0x7f090083

.field public static final SELECT_ONE:I = 0x7f090086

.field public static final SORT:I = 0x7f09005e

.field public static final SORT_TITLE:I = 0x7f09005d

.field public static final SUCCESS:I = 0x7f09008a

.field public static final SURE_DELETE:I = 0x7f090088

.field public static final SURE_DELETE_ALL:I = 0x7f090089

.field public static final VIDEO:I = 0x7f090001

.field public static final Version:I = 0x7f09000e

.field public static final Your_File_Format_not_support:I = 0x7f090060

.field public static final all:I = 0x7f090014

.field public static final app_name:I = 0x7f090016

.field public static final back:I = 0x7f0900d7

.field public static final buffering:I = 0x7f0900d2

.field public static final can_not_find_a_suitable_program_to_open_this_file:I = 0x7f0900c5

.field public static final can_not_open_file:I = 0x7f0900c4

.field public static final cancel:I = 0x7f090029

.field public static final cancel_copy:I = 0x7f0900bf

.field public static final choice_program:I = 0x7f0900c7

.field public static final copyFile:I = 0x7f0900b7

.field public static final copying:I = 0x7f0900b9

.field public static final cycleTxt:I = 0x7f09006a

.field public static final dele_file_error:I = 0x7f0900be

.field public static final delete_info:I = 0x7f090096

.field public static final descale:I = 0x7f09004f

.field public static final directory_empty:I = 0x7f09008e

.field public static final disk_eject:I = 0x7f0900c8

.field public static final disk_path_null:I = 0x7f09009c

.field public static final done_copy:I = 0x7f0900c1

.field public static final empty_file_name:I = 0x7f0900a6

.field public static final enlarge:I = 0x7f09004e

.field public static final existed:I = 0x7f0900b8

.field public static final fail_to_copy:I = 0x7f0900c0

.field public static final fail_to_modify:I = 0x7f0900c6

.field public static final file_has_change:I = 0x7f090013

.field public static final file_no_exist:I = 0x7f090056

.field public static final file_not_exist:I = 0x7f090010

.field public static final file_not_exist_promte:I = 0x7f090012

.field public static final first_photo:I = 0x7f090052

.field public static final frist_song:I = 0x7f09007e

.field public static final fullScreen:I = 0x7f090050

.field public static final has_same_name:I = 0x7f0900a5

.field public static final hide:I = 0x7f0900c3

.field public static final iamge_too_large:I = 0x7f09001f

.field public static final into:I = 0x7f09009b

.field public static final last_photo:I = 0x7f090053

.field public static final last_song:I = 0x7f09007d

.field public static final leftrotion:I = 0x7f09004c

.field public static final listPic:I = 0x7f090046

.field public static final loader:I = 0x7f090051

.field public static final login:I = 0x7f090028

.field public static final lrcTxt:I = 0x7f090063

.field public static final lrc_load_success:I = 0x7f090078

.field public static final lrc_no_found1:I = 0x7f090079

.field public static final lrc_no_found2:I = 0x7f09007a

.field public static final lrc_searching:I = 0x7f09007c

.field public static final lrc_write_failed:I = 0x7f09007f

.field public static final mention_info:I = 0x7f090097

.field public static final moving_files:I = 0x7f0900c2

.field public static final musicListTxt:I = 0x7f090062

.field public static final music_running:I = 0x7f090073

.field public static final network_connected_failed:I = 0x7f09007b

.field public static final new_directory:I = 0x7f0900bc

.field public static final new_file:I = 0x7f0900bb

.field public static final new_usb_mount:I = 0x7f090094

.field public static final nextPic:I = 0x7f09004a

.field public static final nextTxt:I = 0x7f090067

.field public static final no:I = 0x7f090099

.field public static final no_multimedia_file:I = 0x7f0900d3

.field public static final no_music_file:I = 0x7f090055

.field public static final not_available_net:I = 0x7f09003b

.field public static final not_support_song_format:I = 0x7f090011

.field public static final ok:I = 0x7f09002a

.field public static final page:I = 0x7f09009a

.field public static final pause:I = 0x7f0900cf

.field public static final pausePic:I = 0x7f090049

.field public static final pauseTxt:I = 0x7f090066

.field public static final per_null:I = 0x7f0900b0

.field public static final per_only_exec:I = 0x7f0900b2

.field public static final per_only_read:I = 0x7f0900b1

.field public static final per_read_exec:I = 0x7f0900b4

.field public static final per_read_write:I = 0x7f0900b3

.field public static final per_write_exec:I = 0x7f0900b5

.field public static final permission:I = 0x7f0900b6

.field public static final pic_setting_defaule_sort:I = 0x7f0900d4

.field public static final pic_setting_name_sort:I = 0x7f0900d5

.field public static final pic_setting_size_sort:I = 0x7f0900d6

.field public static final playPic:I = 0x7f090048

.field public static final playTxt:I = 0x7f090065

.field public static final playing:I = 0x7f0900ce

.field public static final prePic:I = 0x7f090047

.field public static final preTxt:I = 0x7f090064

.field public static final remove:I = 0x7f090090

.field public static final rename:I = 0x7f0900bd

.field public static final repeatTxt:I = 0x7f090069

.field public static final request_ip:I = 0x7f09003a

.field public static final rightrotion:I = 0x7f09004d

.field public static final scan_disk_file:I = 0x7f0900a8

.field public static final scan_disk_music:I = 0x7f090074

.field public static final scan_disk_picture:I = 0x7f09009d

.field public static final sdcarderror:I = 0x7f090043

.field public static final searchLrc:I = 0x7f09006c

.field public static final sequenceTxt:I = 0x7f090068

.field public static final show_by_big:I = 0x7f0900a4

.field public static final show_by_list:I = 0x7f0900a3

.field public static final show_info:I = 0x7f0900d1

.field public static final shuffleTxt:I = 0x7f09006b

.field public static final singer:I = 0x7f090076

.field public static final size:I = 0x7f090054

.field public static final song_name:I = 0x7f090077

.field public static final sort_by_def:I = 0x7f0900a2

.field public static final sort_by_name:I = 0x7f09009f

.field public static final sort_by_size:I = 0x7f0900a0

.field public static final sort_by_type:I = 0x7f0900a1

.field public static final stop:I = 0x7f09005b

.field public static final stopPic:I = 0x7f09004b

.field public static final storage:I = 0x7f09008f

.field public static final str_cirmode:I = 0x7f0900cc

.field public static final str_lanmode:I = 0x7f0900cd

.field public static final str_picmode:I = 0x7f0900cb

.field public static final str_showmode:I = 0x7f0900ca

.field public static final str_soundmod:I = 0x7f0900c9

.field public static final str_sys_language:I = 0x7f090000

.field public static final sure_delete_files:I = 0x7f0900ba

.field public static final uac_disable:I = 0x7f090072

.field public static final uac_enable:I = 0x7f090071

.field public static final update_disk_file:I = 0x7f0900a9

.field public static final update_disk_music:I = 0x7f090075

.field public static final update_disk_picture:I = 0x7f09009e

.field public static final usb_remove:I = 0x7f090095

.field public static final versionFormatter:I = 0x7f0900d8

.field public static final version_no:I = 0x7f090093

.field public static final videoformaterror:I = 0x7f0900d0

.field public static final voiceDown:I = 0x7f09006f

.field public static final voiceNoTxt:I = 0x7f09006e

.field public static final voiceTxt:I = 0x7f09006d

.field public static final voiceUp:I = 0x7f090070

.field public static final wallpaper:I = 0x7f090045

.field public static final wallpaper_cannt_set:I = 0x7f09005a

.field public static final wallpaper_failed:I = 0x7f090058

.field public static final wallpaper_success:I = 0x7f090057

.field public static final wallpaper_waitting:I = 0x7f090059

.field public static final yes:I = 0x7f090098


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
