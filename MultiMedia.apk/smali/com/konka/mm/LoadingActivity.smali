.class public Lcom/konka/mm/LoadingActivity;
.super Landroid/app/Activity;
.source "LoadingActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/mm/LoadingActivity$Listener4ibComAudio;,
        Lcom/konka/mm/LoadingActivity$Listener4ibComFileManager;,
        Lcom/konka/mm/LoadingActivity$Listener4ibComImage;,
        Lcom/konka/mm/LoadingActivity$Listener4ibComShare;,
        Lcom/konka/mm/LoadingActivity$Listener4ibComVideo;,
        Lcom/konka/mm/LoadingActivity$Listener4ibComWidget;
    }
.end annotation


# instance fields
.field ibComAudio:Landroid/widget/Button;

.field ibComFileManager:Landroid/widget/Button;

.field ibComImage:Landroid/widget/Button;

.field ibComShare:Landroid/widget/Button;

.field ibComVideo:Landroid/widget/Button;

.field ibComWidget:Landroid/widget/Button;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v3, 0x7f030016    # com.konka.mm.R.layout.main

    invoke-virtual {p0, v3}, Lcom/konka/mm/LoadingActivity;->setContentView(I)V

    const v3, 0x7f0b0054    # com.konka.mm.R.id.ibFileManager

    invoke-virtual {p0, v3}, Lcom/konka/mm/LoadingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/konka/mm/LoadingActivity;->ibComFileManager:Landroid/widget/Button;

    const v3, 0x7f0b0055    # com.konka.mm.R.id.ibShare

    invoke-virtual {p0, v3}, Lcom/konka/mm/LoadingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/konka/mm/LoadingActivity;->ibComShare:Landroid/widget/Button;

    const v3, 0x7f0b0050    # com.konka.mm.R.id.ibVideo

    invoke-virtual {p0, v3}, Lcom/konka/mm/LoadingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/konka/mm/LoadingActivity;->ibComVideo:Landroid/widget/Button;

    const v3, 0x7f0b004f    # com.konka.mm.R.id.ibAudio

    invoke-virtual {p0, v3}, Lcom/konka/mm/LoadingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/konka/mm/LoadingActivity;->ibComAudio:Landroid/widget/Button;

    const v3, 0x7f0b0053    # com.konka.mm.R.id.ibImage

    invoke-virtual {p0, v3}, Lcom/konka/mm/LoadingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/konka/mm/LoadingActivity;->ibComImage:Landroid/widget/Button;

    const v3, 0x7f0b0056    # com.konka.mm.R.id.ibWidget

    invoke-virtual {p0, v3}, Lcom/konka/mm/LoadingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/konka/mm/LoadingActivity;->ibComWidget:Landroid/widget/Button;

    iget-object v3, p0, Lcom/konka/mm/LoadingActivity;->ibComFileManager:Landroid/widget/Button;

    new-instance v4, Lcom/konka/mm/LoadingActivity$Listener4ibComFileManager;

    invoke-direct {v4, p0}, Lcom/konka/mm/LoadingActivity$Listener4ibComFileManager;-><init>(Lcom/konka/mm/LoadingActivity;)V

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lcom/konka/mm/LoadingActivity;->ibComShare:Landroid/widget/Button;

    new-instance v4, Lcom/konka/mm/LoadingActivity$Listener4ibComShare;

    invoke-direct {v4, p0}, Lcom/konka/mm/LoadingActivity$Listener4ibComShare;-><init>(Lcom/konka/mm/LoadingActivity;)V

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lcom/konka/mm/LoadingActivity;->ibComVideo:Landroid/widget/Button;

    new-instance v4, Lcom/konka/mm/LoadingActivity$Listener4ibComVideo;

    invoke-direct {v4, p0}, Lcom/konka/mm/LoadingActivity$Listener4ibComVideo;-><init>(Lcom/konka/mm/LoadingActivity;)V

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lcom/konka/mm/LoadingActivity;->ibComAudio:Landroid/widget/Button;

    new-instance v4, Lcom/konka/mm/LoadingActivity$Listener4ibComAudio;

    invoke-direct {v4, p0}, Lcom/konka/mm/LoadingActivity$Listener4ibComAudio;-><init>(Lcom/konka/mm/LoadingActivity;)V

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lcom/konka/mm/LoadingActivity;->ibComImage:Landroid/widget/Button;

    new-instance v4, Lcom/konka/mm/LoadingActivity$Listener4ibComImage;

    invoke-direct {v4, p0}, Lcom/konka/mm/LoadingActivity$Listener4ibComImage;-><init>(Lcom/konka/mm/LoadingActivity;)V

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lcom/konka/mm/LoadingActivity;->ibComWidget:Landroid/widget/Button;

    new-instance v4, Lcom/konka/mm/LoadingActivity$Listener4ibComWidget;

    invoke-direct {v4, p0}, Lcom/konka/mm/LoadingActivity$Listener4ibComWidget;-><init>(Lcom/konka/mm/LoadingActivity;)V

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0}, Lcom/konka/mm/LoadingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    sget-object v3, Ljava/util/Locale;->SIMPLIFIED_CHINESE:Ljava/util/Locale;

    iput-object v3, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/content/res/Resources;->updateConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;)V

    invoke-static {v1}, Lcom/konka/mm/model/MiscModel;->setResolution(Landroid/util/DisplayMetrics;)V

    iget-object v3, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-static {v3}, Lcom/konka/mm/model/MiscModel;->setLanguage(Ljava/util/Locale;)V

    const-string v3, "LoadingActivity"

    invoke-virtual {v1}, Landroid/util/DisplayMetrics;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.intent.action.MEDIA_MOUNTED"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "file://"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v6

    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p0, v3}, Lcom/konka/mm/LoadingActivity;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method
