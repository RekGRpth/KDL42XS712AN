.class public Lcom/konka/mm/modules/ModulesActivity;
.super Landroid/app/Activity;
.source "ModulesActivity.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;
.implements Lcom/konka/mm/IActivity;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/mm/modules/ModulesActivity$BtnScanDialogListener;,
        Lcom/konka/mm/modules/ModulesActivity$ScanSdReceiver;,
        Lcom/konka/mm/modules/ModulesActivity$SortThread;
    }
.end annotation


# static fields
.field public static IMAGE_MAX:I = 0x0

.field public static final SCAN_DISK_FILE_DIALOG:I = 0x1

.field public static final SCREEN_1080P:I = 0x1

.field public static final SCREEN_720P:I = 0x0

.field private static final TAG:Ljava/lang/String; = "MusicListActivity"

.field public static final UPDATA_DISK_FILE_DIALOG:I = 0x2

.field public static instance:Lcom/konka/mm/modules/ModulesActivity;


# instance fields
.field public Item_Show_Mode:I

.field private ListSize:I

.field private bNextPageKey:Z

.field private broadcastReceiver:Landroid/content/BroadcastReceiver;

.field private changeListener:Landroid/widget/RadioGroup$OnCheckedChangeListener;

.field private clickListener:Landroid/view/View$OnClickListener;

.field private curPage:I

.field private curScreen:I

.field private currentPage:I

.field private gridOnItemClick:Landroid/widget/AdapterView$OnItemClickListener;

.field private gridOnKey:Landroid/view/View$OnKeyListener;

.field private gridhandler:Landroid/os/Handler;

.field private handler:Landroid/os/Handler;

.field public indexDisk:I

.field private indexPage:I

.field private infoText:Landroid/widget/TextView;

.field public infos:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field private isAddViewRun:Z

.field private isPopup:Z

.field public isStopScanRun:Z

.field public isUsbStateChange:Z

.field protected mCountPerPage:I

.field public mCurrentRootPath:Ljava/lang/String;

.field private mDialogScan:Landroid/app/ProgressDialog;

.field public mListItemPaths:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public mModuleCallWho:Ljava/lang/String;

.field private mPushLeftInAnim:Landroid/view/animation/Animation;

.field private mPushLeftOutAnim:Landroid/view/animation/Animation;

.field private mPustRightInAnim:Landroid/view/animation/Animation;

.field private mPustRightOutAnim:Landroid/view/animation/Animation;

.field private mReceiver:Landroid/content/BroadcastReceiver;

.field protected mScreen_mode:I

.field public mSourceType:Lcom/konka/mm/finals/CommonFinals$MMSourceType;

.field private mTitle:Landroid/widget/TextView;

.field private mUsbList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private notifyPageChange:Ljava/lang/Runnable;

.field private numColumns:I

.field private pageCount:I

.field private pageGridView:[Landroid/widget/GridView;

.field private pageInfomation:Landroid/widget/TextView;

.field private pageLeft:Landroid/widget/Button;

.field private pageRight:Landroid/widget/Button;

.field private page_linearLayout:Landroid/widget/LinearLayout;

.field private perColumnWidth:I

.field private popupView:Landroid/view/View;

.field private popupWindow:Landroid/widget/PopupWindow;

.field private proDlg:Landroid/app/ProgressDialog;

.field private screenHeight:I

.field private screenHeight_720p:I

.field private screenWidth:I

.field private screenWidth_720p:I

.field private sortBtns:Landroid/widget/RadioGroup;

.field private sortType:I

.field private sort_btn:Landroid/widget/ImageView;

.field startX:F

.field private viewFilpper:Landroid/widget/ViewFlipper;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/konka/mm/modules/ModulesActivity;->instance:Lcom/konka/mm/modules/ModulesActivity;

    const v0, 0x9d6b

    sput v0, Lcom/konka/mm/modules/ModulesActivity;->IMAGE_MAX:I

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput-object v2, p0, Lcom/konka/mm/modules/ModulesActivity;->pageGridView:[Landroid/widget/GridView;

    iput-object v2, p0, Lcom/konka/mm/modules/ModulesActivity;->mUsbList:Ljava/util/List;

    const/16 v0, 0x500

    iput v0, p0, Lcom/konka/mm/modules/ModulesActivity;->screenWidth_720p:I

    const/16 v0, 0x2d0

    iput v0, p0, Lcom/konka/mm/modules/ModulesActivity;->screenHeight_720p:I

    iput v3, p0, Lcom/konka/mm/modules/ModulesActivity;->mScreen_mode:I

    const/4 v0, 0x2

    iput v0, p0, Lcom/konka/mm/modules/ModulesActivity;->Item_Show_Mode:I

    iput-object v2, p0, Lcom/konka/mm/modules/ModulesActivity;->infos:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/konka/mm/modules/ModulesActivity;->mListItemPaths:Ljava/util/ArrayList;

    const/4 v0, -0x1

    iput v0, p0, Lcom/konka/mm/modules/ModulesActivity;->indexDisk:I

    sget-object v0, Lcom/konka/mm/finals/CommonFinals$MMSourceType;->KONKA_MM_SOURCE_VIDEO:Lcom/konka/mm/finals/CommonFinals$MMSourceType;

    iput-object v0, p0, Lcom/konka/mm/modules/ModulesActivity;->mSourceType:Lcom/konka/mm/finals/CommonFinals$MMSourceType;

    iput-boolean v1, p0, Lcom/konka/mm/modules/ModulesActivity;->isUsbStateChange:Z

    iput-boolean v1, p0, Lcom/konka/mm/modules/ModulesActivity;->isStopScanRun:Z

    iput-object v2, p0, Lcom/konka/mm/modules/ModulesActivity;->mDialogScan:Landroid/app/ProgressDialog;

    new-instance v0, Lcom/konka/mm/modules/ModulesActivity$1;

    invoke-direct {v0, p0}, Lcom/konka/mm/modules/ModulesActivity$1;-><init>(Lcom/konka/mm/modules/ModulesActivity;)V

    iput-object v0, p0, Lcom/konka/mm/modules/ModulesActivity;->broadcastReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/konka/mm/modules/ModulesActivity$2;

    invoke-direct {v0, p0}, Lcom/konka/mm/modules/ModulesActivity$2;-><init>(Lcom/konka/mm/modules/ModulesActivity;)V

    iput-object v0, p0, Lcom/konka/mm/modules/ModulesActivity;->handler:Landroid/os/Handler;

    iput-boolean v1, p0, Lcom/konka/mm/modules/ModulesActivity;->isPopup:Z

    const/16 v0, 0x14a

    iput v0, p0, Lcom/konka/mm/modules/ModulesActivity;->perColumnWidth:I

    iput v1, p0, Lcom/konka/mm/modules/ModulesActivity;->ListSize:I

    iput v1, p0, Lcom/konka/mm/modules/ModulesActivity;->pageCount:I

    iput v1, p0, Lcom/konka/mm/modules/ModulesActivity;->currentPage:I

    iput v1, p0, Lcom/konka/mm/modules/ModulesActivity;->curScreen:I

    const/4 v0, 0x6

    iput v0, p0, Lcom/konka/mm/modules/ModulesActivity;->numColumns:I

    iput v1, p0, Lcom/konka/mm/modules/ModulesActivity;->curPage:I

    iput v1, p0, Lcom/konka/mm/modules/ModulesActivity;->sortType:I

    iput-boolean v1, p0, Lcom/konka/mm/modules/ModulesActivity;->bNextPageKey:Z

    iput-boolean v3, p0, Lcom/konka/mm/modules/ModulesActivity;->isAddViewRun:Z

    iput v3, p0, Lcom/konka/mm/modules/ModulesActivity;->indexPage:I

    new-instance v0, Lcom/konka/mm/modules/ModulesActivity$3;

    invoke-direct {v0, p0}, Lcom/konka/mm/modules/ModulesActivity$3;-><init>(Lcom/konka/mm/modules/ModulesActivity;)V

    iput-object v0, p0, Lcom/konka/mm/modules/ModulesActivity;->gridhandler:Landroid/os/Handler;

    new-instance v0, Lcom/konka/mm/modules/ModulesActivity$4;

    invoke-direct {v0, p0}, Lcom/konka/mm/modules/ModulesActivity$4;-><init>(Lcom/konka/mm/modules/ModulesActivity;)V

    iput-object v0, p0, Lcom/konka/mm/modules/ModulesActivity;->clickListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/konka/mm/modules/ModulesActivity$5;

    invoke-direct {v0, p0}, Lcom/konka/mm/modules/ModulesActivity$5;-><init>(Lcom/konka/mm/modules/ModulesActivity;)V

    iput-object v0, p0, Lcom/konka/mm/modules/ModulesActivity;->changeListener:Landroid/widget/RadioGroup$OnCheckedChangeListener;

    new-instance v0, Lcom/konka/mm/modules/ModulesActivity$6;

    invoke-direct {v0, p0}, Lcom/konka/mm/modules/ModulesActivity$6;-><init>(Lcom/konka/mm/modules/ModulesActivity;)V

    iput-object v0, p0, Lcom/konka/mm/modules/ModulesActivity;->gridOnKey:Landroid/view/View$OnKeyListener;

    new-instance v0, Lcom/konka/mm/modules/ModulesActivity$7;

    invoke-direct {v0, p0}, Lcom/konka/mm/modules/ModulesActivity$7;-><init>(Lcom/konka/mm/modules/ModulesActivity;)V

    iput-object v0, p0, Lcom/konka/mm/modules/ModulesActivity;->gridOnItemClick:Landroid/widget/AdapterView$OnItemClickListener;

    new-instance v0, Lcom/konka/mm/modules/ModulesActivity$8;

    invoke-direct {v0, p0}, Lcom/konka/mm/modules/ModulesActivity$8;-><init>(Lcom/konka/mm/modules/ModulesActivity;)V

    iput-object v0, p0, Lcom/konka/mm/modules/ModulesActivity;->notifyPageChange:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic access$0(Lcom/konka/mm/modules/ModulesActivity;)Landroid/app/ProgressDialog;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity;->proDlg:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$1(Lcom/konka/mm/modules/ModulesActivity;Landroid/app/ProgressDialog;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/modules/ModulesActivity;->proDlg:Landroid/app/ProgressDialog;

    return-void
.end method

.method static synthetic access$10(Lcom/konka/mm/modules/ModulesActivity;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$11(Lcom/konka/mm/modules/ModulesActivity;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity;->mUsbList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$12(Lcom/konka/mm/modules/ModulesActivity;[Ljava/io/File;I)[Ljava/io/File;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/konka/mm/modules/ModulesActivity;->sortFiles([Ljava/io/File;I)[Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$13(Lcom/konka/mm/modules/ModulesActivity;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity;->gridhandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$14(Lcom/konka/mm/modules/ModulesActivity;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/konka/mm/modules/ModulesActivity;->isAddViewRun:Z

    return-void
.end method

.method static synthetic access$15(Lcom/konka/mm/modules/ModulesActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/mm/modules/ModulesActivity;->dismissPopupWindow()V

    return-void
.end method

.method static synthetic access$16(Lcom/konka/mm/modules/ModulesActivity;Landroid/content/Intent;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/mm/modules/ModulesActivity;->onReceiveSdCardBroadCast(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic access$17(Lcom/konka/mm/modules/ModulesActivity;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/mm/modules/ModulesActivity;->initItemInfo(I)V

    return-void
.end method

.method static synthetic access$2(Lcom/konka/mm/modules/ModulesActivity;Ljava/util/List;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/mm/modules/ModulesActivity;->initGridView(Ljava/util/List;)V

    return-void
.end method

.method static synthetic access$3(Lcom/konka/mm/modules/ModulesActivity;)Landroid/app/ProgressDialog;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity;->mDialogScan:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$4(Lcom/konka/mm/modules/ModulesActivity;I)V
    .locals 0

    iput p1, p0, Lcom/konka/mm/modules/ModulesActivity;->sortType:I

    return-void
.end method

.method static synthetic access$5(Lcom/konka/mm/modules/ModulesActivity;)Landroid/widget/PopupWindow;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity;->popupWindow:Landroid/widget/PopupWindow;

    return-object v0
.end method

.method static synthetic access$6(Lcom/konka/mm/modules/ModulesActivity;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/konka/mm/modules/ModulesActivity;->isPopup:Z

    return-void
.end method

.method static synthetic access$7(Lcom/konka/mm/modules/ModulesActivity;)I
    .locals 1

    iget v0, p0, Lcom/konka/mm/modules/ModulesActivity;->currentPage:I

    return v0
.end method

.method static synthetic access$8(Lcom/konka/mm/modules/ModulesActivity;)I
    .locals 1

    iget v0, p0, Lcom/konka/mm/modules/ModulesActivity;->pageCount:I

    return v0
.end method

.method static synthetic access$9(Lcom/konka/mm/modules/ModulesActivity;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/konka/mm/modules/ModulesActivity;->bNextPageKey:Z

    return-void
.end method

.method private dismissPopupWindow()V
    .locals 2

    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity;->popupWindow:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/konka/mm/modules/ModulesActivity;->isPopup:Z

    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity;->sort_btn:Landroid/widget/ImageView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity;->sort_btn:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->requestFocus()Z

    return-void
.end method

.method private init()V
    .locals 8

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v4, -0x1

    const/4 v5, 0x0

    const v2, 0x7f0b005a    # com.konka.mm.R.id.which_module_txt

    invoke-virtual {p0, v2}, Lcom/konka/mm/modules/ModulesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/konka/mm/modules/ModulesActivity;->mTitle:Landroid/widget/TextView;

    const v2, 0x7f0b005d    # com.konka.mm.R.id.modules_list_item_info

    invoke-virtual {p0, v2}, Lcom/konka/mm/modules/ModulesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/konka/mm/modules/ModulesActivity;->infoText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/konka/mm/modules/ModulesActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/konka/mm/modules/ModulesActivity;->mUsbList:Ljava/util/List;

    invoke-static {p0}, Lcom/konka/mm/tools/FileTool;->getUsbs(Landroid/content/Context;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/konka/mm/modules/ModulesActivity;->mUsbList:Ljava/util/List;

    new-instance v2, Landroid/app/ProgressDialog;

    invoke-direct {v2, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/konka/mm/modules/ModulesActivity;->mDialogScan:Landroid/app/ProgressDialog;

    const-string v2, "com.konka.mm.module.called.who"

    const-string v3, "com.konka.mm.module.called.video"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/konka/mm/modules/ModulesActivity;->mModuleCallWho:Ljava/lang/String;

    const-string v2, "com.konka.mm.module.called.index.disk"

    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/konka/mm/modules/ModulesActivity;->indexDisk:I

    iget v2, p0, Lcom/konka/mm/modules/ModulesActivity;->indexDisk:I

    if-eq v2, v4, :cond_0

    iget-object v2, p0, Lcom/konka/mm/modules/ModulesActivity;->mUsbList:Ljava/util/List;

    iget v3, p0, Lcom/konka/mm/modules/ModulesActivity;->indexDisk:I

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iput-object v2, p0, Lcom/konka/mm/modules/ModulesActivity;->mCurrentRootPath:Ljava/lang/String;

    :cond_0
    iput v5, p0, Lcom/konka/mm/modules/ModulesActivity;->sortType:I

    iget-object v2, p0, Lcom/konka/mm/modules/ModulesActivity;->mModuleCallWho:Ljava/lang/String;

    const-string v3, "com.konka.mm.module.called.video"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/konka/mm/modules/ModulesActivity;->mTitle:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/konka/mm/modules/ModulesActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090018    # com.konka.mm.R.string.MM_VIDEO

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    sget-object v2, Lcom/konka/mm/finals/CommonFinals$MMSourceType;->KONKA_MM_SOURCE_VIDEO:Lcom/konka/mm/finals/CommonFinals$MMSourceType;

    iput-object v2, p0, Lcom/konka/mm/modules/ModulesActivity;->mSourceType:Lcom/konka/mm/finals/CommonFinals$MMSourceType;

    :cond_1
    :goto_0
    const v2, 0x7f0b0030    # com.konka.mm.R.id.viewFlipper

    invoke-virtual {p0, v2}, Lcom/konka/mm/modules/ModulesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ViewFlipper;

    iput-object v2, p0, Lcom/konka/mm/modules/ModulesActivity;->viewFilpper:Landroid/widget/ViewFlipper;

    const/4 v2, 0x3

    new-array v2, v2, [Landroid/widget/GridView;

    iput-object v2, p0, Lcom/konka/mm/modules/ModulesActivity;->pageGridView:[Landroid/widget/GridView;

    iget-object v3, p0, Lcom/konka/mm/modules/ModulesActivity;->pageGridView:[Landroid/widget/GridView;

    iget-object v2, p0, Lcom/konka/mm/modules/ModulesActivity;->viewFilpper:Landroid/widget/ViewFlipper;

    const v4, 0x7f0b0031    # com.konka.mm.R.id.page1

    invoke-virtual {v2, v4}, Landroid/widget/ViewFlipper;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/GridView;

    aput-object v2, v3, v5

    iget-object v3, p0, Lcom/konka/mm/modules/ModulesActivity;->pageGridView:[Landroid/widget/GridView;

    iget-object v2, p0, Lcom/konka/mm/modules/ModulesActivity;->viewFilpper:Landroid/widget/ViewFlipper;

    const v4, 0x7f0b0032    # com.konka.mm.R.id.page2

    invoke-virtual {v2, v4}, Landroid/widget/ViewFlipper;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/GridView;

    aput-object v2, v3, v6

    iget-object v3, p0, Lcom/konka/mm/modules/ModulesActivity;->pageGridView:[Landroid/widget/GridView;

    iget-object v2, p0, Lcom/konka/mm/modules/ModulesActivity;->viewFilpper:Landroid/widget/ViewFlipper;

    const v4, 0x7f0b0033    # com.konka.mm.R.id.page3

    invoke-virtual {v2, v4}, Landroid/widget/ViewFlipper;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/GridView;

    aput-object v2, v3, v7

    iget-object v2, p0, Lcom/konka/mm/modules/ModulesActivity;->pageGridView:[Landroid/widget/GridView;

    aget-object v2, v2, v5

    invoke-virtual {v2, p0}, Landroid/widget/GridView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v2, p0, Lcom/konka/mm/modules/ModulesActivity;->pageGridView:[Landroid/widget/GridView;

    aget-object v2, v2, v6

    invoke-virtual {v2, p0}, Landroid/widget/GridView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v2, p0, Lcom/konka/mm/modules/ModulesActivity;->pageGridView:[Landroid/widget/GridView;

    aget-object v2, v2, v7

    invoke-virtual {v2, p0}, Landroid/widget/GridView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    const v2, 0x7f0b002e    # com.konka.mm.R.id.page_linearlayout

    invoke-virtual {p0, v2}, Lcom/konka/mm/modules/ModulesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/konka/mm/modules/ModulesActivity;->page_linearLayout:Landroid/widget/LinearLayout;

    const v2, 0x7f040005    # com.konka.mm.R.anim.photo_push_left_in

    invoke-static {p0, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v2

    iput-object v2, p0, Lcom/konka/mm/modules/ModulesActivity;->mPushLeftInAnim:Landroid/view/animation/Animation;

    const v2, 0x7f040009    # com.konka.mm.R.anim.push_left_out

    invoke-static {p0, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v2

    iput-object v2, p0, Lcom/konka/mm/modules/ModulesActivity;->mPushLeftOutAnim:Landroid/view/animation/Animation;

    const v2, 0x7f040006    # com.konka.mm.R.anim.photo_push_right_in

    invoke-static {p0, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v2

    iput-object v2, p0, Lcom/konka/mm/modules/ModulesActivity;->mPustRightInAnim:Landroid/view/animation/Animation;

    const v2, 0x7f04000a    # com.konka.mm.R.anim.push_right_out

    invoke-static {p0, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v2

    iput-object v2, p0, Lcom/konka/mm/modules/ModulesActivity;->mPustRightOutAnim:Landroid/view/animation/Animation;

    const v2, 0x7f0b002f    # com.konka.mm.R.id.btn_list_left

    invoke-virtual {p0, v2}, Lcom/konka/mm/modules/ModulesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/konka/mm/modules/ModulesActivity;->pageLeft:Landroid/widget/Button;

    const v2, 0x7f0b0034    # com.konka.mm.R.id.btn_list_right

    invoke-virtual {p0, v2}, Lcom/konka/mm/modules/ModulesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/konka/mm/modules/ModulesActivity;->pageRight:Landroid/widget/Button;

    const v2, 0x7f0b0035    # com.konka.mm.R.id.tv_page_info

    invoke-virtual {p0, v2}, Lcom/konka/mm/modules/ModulesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/konka/mm/modules/ModulesActivity;->pageInfomation:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/konka/mm/modules/ModulesActivity;->pageLeft:Landroid/widget/Button;

    iget-object v3, p0, Lcom/konka/mm/modules/ModulesActivity;->clickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, Lcom/konka/mm/modules/ModulesActivity;->pageRight:Landroid/widget/Button;

    iget-object v3, p0, Lcom/konka/mm/modules/ModulesActivity;->clickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v2, 0x7f0b005c    # com.konka.mm.R.id.img_sort

    invoke-virtual {p0, v2}, Lcom/konka/mm/modules/ModulesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/konka/mm/modules/ModulesActivity;->sort_btn:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/konka/mm/modules/ModulesActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f030037    # com.konka.mm.R.layout.sort_popup_window_template

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/konka/mm/modules/ModulesActivity;->popupView:Landroid/view/View;

    iget-object v2, p0, Lcom/konka/mm/modules/ModulesActivity;->popupView:Landroid/view/View;

    const v3, 0x7f0b00b3    # com.konka.mm.R.id.rg_sort_btns

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RadioGroup;

    iput-object v2, p0, Lcom/konka/mm/modules/ModulesActivity;->sortBtns:Landroid/widget/RadioGroup;

    sput-object p0, Lcom/konka/mm/modules/ModulesActivity;->instance:Lcom/konka/mm/modules/ModulesActivity;

    invoke-virtual {p0}, Lcom/konka/mm/modules/ModulesActivity;->broweToRoot()V

    iget-object v2, p0, Lcom/konka/mm/modules/ModulesActivity;->popupView:Landroid/view/View;

    new-instance v3, Lcom/konka/mm/modules/ModulesActivity$9;

    invoke-direct {v3, p0}, Lcom/konka/mm/modules/ModulesActivity$9;-><init>(Lcom/konka/mm/modules/ModulesActivity;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    const-string v2, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v2, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v2, "file"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    new-instance v2, Lcom/konka/mm/modules/ModulesActivity$10;

    invoke-direct {v2, p0}, Lcom/konka/mm/modules/ModulesActivity$10;-><init>(Lcom/konka/mm/modules/ModulesActivity;)V

    iput-object v2, p0, Lcom/konka/mm/modules/ModulesActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    iget-object v2, p0, Lcom/konka/mm/modules/ModulesActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v2, v1}, Lcom/konka/mm/modules/ModulesActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void

    :cond_2
    iget-object v2, p0, Lcom/konka/mm/modules/ModulesActivity;->mModuleCallWho:Ljava/lang/String;

    const-string v3, "com.konka.mm.module.called.txt"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/konka/mm/modules/ModulesActivity;->mTitle:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/konka/mm/modules/ModulesActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f09001d    # com.konka.mm.R.string.MM_TXT

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    sget-object v2, Lcom/konka/mm/finals/CommonFinals$MMSourceType;->KONKA_MM_SOURCE_TXT:Lcom/konka/mm/finals/CommonFinals$MMSourceType;

    iput-object v2, p0, Lcom/konka/mm/modules/ModulesActivity;->mSourceType:Lcom/konka/mm/finals/CommonFinals$MMSourceType;

    goto/16 :goto_0

    :cond_3
    iget-object v2, p0, Lcom/konka/mm/modules/ModulesActivity;->mModuleCallWho:Ljava/lang/String;

    const-string v3, "com.konka.mm.module.called.apk"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/konka/mm/modules/ModulesActivity;->mTitle:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/konka/mm/modules/ModulesActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f09001e    # com.konka.mm.R.string.MM_APK

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    sget-object v2, Lcom/konka/mm/finals/CommonFinals$MMSourceType;->KONKA_MM_SOURCE_APK:Lcom/konka/mm/finals/CommonFinals$MMSourceType;

    iput-object v2, p0, Lcom/konka/mm/modules/ModulesActivity;->mSourceType:Lcom/konka/mm/finals/CommonFinals$MMSourceType;

    goto/16 :goto_0

    :cond_4
    iget-object v2, p0, Lcom/konka/mm/modules/ModulesActivity;->mModuleCallWho:Ljava/lang/String;

    const-string v3, "com.konka.mm.module.called.audio"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/konka/mm/modules/ModulesActivity;->mTitle:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/konka/mm/modules/ModulesActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090017    # com.konka.mm.R.string.MM_AUDIO

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    sget-object v2, Lcom/konka/mm/finals/CommonFinals$MMSourceType;->KONKA_MM_SOURCE_AUDIO:Lcom/konka/mm/finals/CommonFinals$MMSourceType;

    iput-object v2, p0, Lcom/konka/mm/modules/ModulesActivity;->mSourceType:Lcom/konka/mm/finals/CommonFinals$MMSourceType;

    goto/16 :goto_0

    :cond_5
    iget-object v2, p0, Lcom/konka/mm/modules/ModulesActivity;->mModuleCallWho:Ljava/lang/String;

    const-string v3, "com.konka.mm.module.called.image"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/konka/mm/modules/ModulesActivity;->mTitle:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/konka/mm/modules/ModulesActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090019    # com.konka.mm.R.string.MM_IMAGE

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    sget-object v2, Lcom/konka/mm/finals/CommonFinals$MMSourceType;->KONKA_MM_SOURCE_IMAGE:Lcom/konka/mm/finals/CommonFinals$MMSourceType;

    iput-object v2, p0, Lcom/konka/mm/modules/ModulesActivity;->mSourceType:Lcom/konka/mm/finals/CommonFinals$MMSourceType;

    goto/16 :goto_0
.end method

.method private initGridView(Ljava/util/List;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/io/File;",
            ">;)V"
        }
    .end annotation

    const/16 v10, 0x12

    const/4 v5, 0x3

    const/16 v9, 0x14

    const/4 v8, 0x1

    const/4 v7, 0x0

    iput v7, p0, Lcom/konka/mm/modules/ModulesActivity;->currentPage:I

    iput v7, p0, Lcom/konka/mm/modules/ModulesActivity;->pageCount:I

    iget v3, p0, Lcom/konka/mm/modules/ModulesActivity;->Item_Show_Mode:I

    if-ne v3, v8, :cond_0

    const/16 v3, 0x21

    iput v3, p0, Lcom/konka/mm/modules/ModulesActivity;->mCountPerPage:I

    iput v5, p0, Lcom/konka/mm/modules/ModulesActivity;->numColumns:I

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    iput v3, p0, Lcom/konka/mm/modules/ModulesActivity;->ListSize:I

    iget v3, p0, Lcom/konka/mm/modules/ModulesActivity;->ListSize:I

    int-to-float v3, v3

    iget v4, p0, Lcom/konka/mm/modules/ModulesActivity;->mCountPerPage:I

    int-to-float v4, v4

    div-float/2addr v3, v4

    float-to-double v3, v3

    invoke-static {v3, v4}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v3

    double-to-int v3, v3

    iput v3, p0, Lcom/konka/mm/modules/ModulesActivity;->pageCount:I

    invoke-virtual {p0}, Lcom/konka/mm/modules/ModulesActivity;->setPageInfo()V

    iget v3, p0, Lcom/konka/mm/modules/ModulesActivity;->pageCount:I

    if-gt v3, v5, :cond_1

    iget v0, p0, Lcom/konka/mm/modules/ModulesActivity;->pageCount:I

    :goto_1
    iget v1, p0, Lcom/konka/mm/modules/ModulesActivity;->curScreen:I

    :goto_2
    iget v3, p0, Lcom/konka/mm/modules/ModulesActivity;->curScreen:I

    add-int/2addr v3, v0

    if-lt v1, v3, :cond_2

    invoke-direct {p0, v7}, Lcom/konka/mm/modules/ModulesActivity;->initItemInfo(I)V

    return-void

    :cond_0
    iput v10, p0, Lcom/konka/mm/modules/ModulesActivity;->mCountPerPage:I

    const/4 v3, 0x6

    iput v3, p0, Lcom/konka/mm/modules/ModulesActivity;->numColumns:I

    goto :goto_0

    :cond_1
    const/4 v0, 0x3

    goto :goto_1

    :cond_2
    iget-object v3, p0, Lcom/konka/mm/modules/ModulesActivity;->pageGridView:[Landroid/widget/GridView;

    rem-int/lit8 v4, v1, 0x3

    aget-object v3, v3, v4

    iget v4, p0, Lcom/konka/mm/modules/ModulesActivity;->numColumns:I

    invoke-virtual {v3, v4}, Landroid/widget/GridView;->setNumColumns(I)V

    iget v3, p0, Lcom/konka/mm/modules/ModulesActivity;->curScreen:I

    sub-int v3, v1, v3

    iget v4, p0, Lcom/konka/mm/modules/ModulesActivity;->pageCount:I

    add-int/lit8 v4, v4, -0x1

    if-ge v3, v4, :cond_4

    iget v2, p0, Lcom/konka/mm/modules/ModulesActivity;->mCountPerPage:I

    :cond_3
    :goto_3
    iget-object v3, p0, Lcom/konka/mm/modules/ModulesActivity;->pageGridView:[Landroid/widget/GridView;

    rem-int/lit8 v4, v1, 0x3

    aget-object v3, v3, v4

    invoke-virtual {v3, v9, v7, v9, v7}, Landroid/widget/GridView;->setPadding(IIII)V

    iget v3, p0, Lcom/konka/mm/modules/ModulesActivity;->Item_Show_Mode:I

    if-ne v3, v8, :cond_6

    iget v3, p0, Lcom/konka/mm/modules/ModulesActivity;->mScreen_mode:I

    if-nez v3, :cond_5

    iget-object v3, p0, Lcom/konka/mm/modules/ModulesActivity;->pageGridView:[Landroid/widget/GridView;

    rem-int/lit8 v4, v1, 0x3

    aget-object v3, v3, v4

    const/16 v4, -0x14

    invoke-virtual {v3, v4}, Landroid/widget/GridView;->setVerticalSpacing(I)V

    :goto_4
    iget-object v3, p0, Lcom/konka/mm/modules/ModulesActivity;->page_linearLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v7, v7, v7, v7}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    :goto_5
    iget-object v3, p0, Lcom/konka/mm/modules/ModulesActivity;->pageGridView:[Landroid/widget/GridView;

    rem-int/lit8 v4, v1, 0x3

    aget-object v3, v3, v4

    new-instance v4, Lcom/konka/mm/adapters/ModulesListAdapter;

    iget v5, p0, Lcom/konka/mm/modules/ModulesActivity;->curScreen:I

    sub-int v5, v1, v5

    iget v6, p0, Lcom/konka/mm/modules/ModulesActivity;->mCountPerPage:I

    invoke-direct {v4, p0, v5, v2, v6}, Lcom/konka/mm/adapters/ModulesListAdapter;-><init>(Lcom/konka/mm/modules/ModulesActivity;III)V

    invoke-virtual {v3, v4}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v3, p0, Lcom/konka/mm/modules/ModulesActivity;->pageGridView:[Landroid/widget/GridView;

    rem-int/lit8 v4, v1, 0x3

    aget-object v3, v3, v4

    new-instance v4, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v4, v7}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v3, v4}, Landroid/widget/GridView;->setSelector(Landroid/graphics/drawable/Drawable;)V

    iget-object v3, p0, Lcom/konka/mm/modules/ModulesActivity;->pageGridView:[Landroid/widget/GridView;

    rem-int/lit8 v4, v1, 0x3

    aget-object v3, v3, v4

    iget-object v4, p0, Lcom/konka/mm/modules/ModulesActivity;->gridOnKey:Landroid/view/View$OnKeyListener;

    invoke-virtual {v3, v4}, Landroid/widget/GridView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v3, p0, Lcom/konka/mm/modules/ModulesActivity;->pageGridView:[Landroid/widget/GridView;

    rem-int/lit8 v4, v1, 0x3

    aget-object v3, v3, v4

    iget-object v4, p0, Lcom/konka/mm/modules/ModulesActivity;->gridOnItemClick:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v3, v4}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v3, p0, Lcom/konka/mm/modules/ModulesActivity;->pageGridView:[Landroid/widget/GridView;

    rem-int/lit8 v4, v1, 0x3

    aget-object v3, v3, v4

    new-instance v4, Lcom/konka/mm/modules/ModulesActivity$13;

    invoke-direct {v4, p0}, Lcom/konka/mm/modules/ModulesActivity$13;-><init>(Lcom/konka/mm/modules/ModulesActivity;)V

    invoke-virtual {v3, v4}, Landroid/widget/GridView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    iget-object v3, p0, Lcom/konka/mm/modules/ModulesActivity;->viewFilpper:Landroid/widget/ViewFlipper;

    invoke-virtual {v3}, Landroid/widget/ViewFlipper;->getCurrentView()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v8}, Landroid/view/View;->setFocusable(Z)V

    iget-object v3, p0, Lcom/konka/mm/modules/ModulesActivity;->viewFilpper:Landroid/widget/ViewFlipper;

    invoke-virtual {v3}, Landroid/widget/ViewFlipper;->getCurrentView()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->requestFocus()Z

    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_2

    :cond_4
    iget v3, p0, Lcom/konka/mm/modules/ModulesActivity;->ListSize:I

    iget v4, p0, Lcom/konka/mm/modules/ModulesActivity;->mCountPerPage:I

    iget v5, p0, Lcom/konka/mm/modules/ModulesActivity;->pageCount:I

    add-int/lit8 v5, v5, -0x1

    mul-int/2addr v4, v5

    sub-int v2, v3, v4

    iget v3, p0, Lcom/konka/mm/modules/ModulesActivity;->mCountPerPage:I

    if-lt v2, v3, :cond_3

    iget v2, p0, Lcom/konka/mm/modules/ModulesActivity;->mCountPerPage:I

    goto/16 :goto_3

    :cond_5
    iget-object v3, p0, Lcom/konka/mm/modules/ModulesActivity;->pageGridView:[Landroid/widget/GridView;

    rem-int/lit8 v4, v1, 0x3

    aget-object v3, v3, v4

    const/16 v4, -0x1e

    invoke-virtual {v3, v4}, Landroid/widget/GridView;->setVerticalSpacing(I)V

    goto :goto_4

    :cond_6
    iget-object v3, p0, Lcom/konka/mm/modules/ModulesActivity;->pageGridView:[Landroid/widget/GridView;

    rem-int/lit8 v4, v1, 0x3

    aget-object v3, v3, v4

    invoke-virtual {v3, v10}, Landroid/widget/GridView;->setVerticalSpacing(I)V

    iget-object v3, p0, Lcom/konka/mm/modules/ModulesActivity;->page_linearLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v7, v9, v7, v7}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    goto/16 :goto_5
.end method

.method private initItemInfo(I)V
    .locals 5
    .param p1    # I

    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity;->infoText:Landroid/widget/TextView;

    const-string v1, " "

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lcom/konka/mm/modules/ModulesActivity;->currentPage:I

    iget v1, p0, Lcom/konka/mm/modules/ModulesActivity;->mCountPerPage:I

    mul-int/2addr v0, v1

    add-int/2addr v0, p1

    iget-object v1, p0, Lcom/konka/mm/modules/ModulesActivity;->infos:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    iget-boolean v0, p0, Lcom/konka/mm/modules/ModulesActivity;->bNextPageKey:Z

    if-eqz v0, :cond_2

    if-eqz p1, :cond_2

    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/konka/mm/modules/ModulesActivity;->bNextPageKey:Z

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/konka/mm/modules/ModulesActivity;->infoText:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity;->infos:Ljava/util/ArrayList;

    iget v3, p0, Lcom/konka/mm/modules/ModulesActivity;->currentPage:I

    iget v4, p0, Lcom/konka/mm/modules/ModulesActivity;->mCountPerPage:I

    mul-int/2addr v3, v4

    add-int/2addr v3, p1

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v0, ", "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity;->infos:Ljava/util/ArrayList;

    iget v3, p0, Lcom/konka/mm/modules/ModulesActivity;->currentPage:I

    iget v4, p0, Lcom/konka/mm/modules/ModulesActivity;->mCountPerPage:I

    mul-int/2addr v3, v4

    add-int/2addr v3, p1

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v3

    long-to-double v3, v3

    invoke-static {v3, v4}, Lcom/konka/mm/tools/MusicTool;->byte2M(D)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "M"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private onReceiveSdCardBroadCast(Landroid/content/Intent;)V
    .locals 4
    .param p1    # Landroid/content/Intent;

    const/4 v3, 0x1

    const/4 v2, -0x1

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget v1, p0, Lcom/konka/mm/modules/ModulesActivity;->indexDisk:I

    if-ne v1, v2, :cond_0

    iput-boolean v3, p0, Lcom/konka/mm/modules/ModulesActivity;->isUsbStateChange:Z

    invoke-virtual {p0}, Lcom/konka/mm/modules/ModulesActivity;->broweToRoot()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v1, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/konka/mm/modules/ModulesActivity;->indexDisk:I

    if-eq v1, v2, :cond_2

    iget-object v1, p0, Lcom/konka/mm/modules/ModulesActivity;->mCurrentRootPath:Ljava/lang/String;

    invoke-static {p0, v1}, Lcom/konka/mm/tools/FileTool;->checkUsbExist(Landroid/app/Activity;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/konka/mm/modules/ModulesActivity;->finish()V

    goto :goto_0

    :cond_2
    iput-boolean v3, p0, Lcom/konka/mm/modules/ModulesActivity;->isUsbStateChange:Z

    invoke-static {p0}, Lcom/konka/mm/tools/FileTool;->usbIsExists(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p0}, Lcom/konka/mm/modules/ModulesActivity;->broweToRoot()V

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/konka/mm/modules/ModulesActivity;->finish()V

    goto :goto_0
.end method

.method private registerBroacast()V
    .locals 2

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.intent.action.MEDIA_SCANNER_FINISHED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.MEDIA_SCANNER_STARTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    return-void
.end method

.method private sortFiles([Ljava/io/File;I)[Ljava/io/File;
    .locals 1
    .param p1    # [Ljava/io/File;
    .param p2    # I

    const/4 v0, 0x0

    packed-switch p2, :pswitch_data_0

    :goto_0
    return-object v0

    :pswitch_0
    move-object v0, p1

    goto :goto_0

    :pswitch_1
    invoke-static {p1}, Lcom/konka/mm/tools/FileTool;->getSortFilesByName([Ljava/io/File;)[Ljava/io/File;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    invoke-static {p1}, Lcom/konka/mm/tools/FileTool;->getSortFilesBySize([Ljava/io/File;)[Ljava/io/File;

    move-result-object v0

    goto :goto_0

    :pswitch_3
    invoke-static {p1}, Lcom/konka/mm/tools/FileTool;->getSortFilesByType([Ljava/io/File;)[Ljava/io/File;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public broweToRoot()V
    .locals 3

    const/4 v2, 0x0

    invoke-static {p0}, Lcom/konka/mm/tools/FileTool;->usbIsExists(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {p0}, Lcom/konka/mm/tools/FileTool;->getUsbs(Landroid/content/Context;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/mm/modules/ModulesActivity;->mUsbList:Ljava/util/List;

    iput-boolean v2, p0, Lcom/konka/mm/modules/ModulesActivity;->isStopScanRun:Z

    iput v2, p0, Lcom/konka/mm/modules/ModulesActivity;->sortType:I

    iget-object v1, p0, Lcom/konka/mm/modules/ModulesActivity;->mDialogScan:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/konka/mm/modules/ModulesActivity;->showDialog(I)V

    :cond_0
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/konka/mm/modules/ModulesActivity$SortThread;

    iget v2, p0, Lcom/konka/mm/modules/ModulesActivity;->sortType:I

    invoke-direct {v1, p0, v2}, Lcom/konka/mm/modules/ModulesActivity$SortThread;-><init>(Lcom/konka/mm/modules/ModulesActivity;I)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    iget-object v1, p0, Lcom/konka/mm/modules/ModulesActivity;->sort_btn:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/konka/mm/modules/ModulesActivity;->clickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/konka/mm/modules/ModulesActivity;->sort_btn:Landroid/widget/ImageView;

    new-instance v2, Lcom/konka/mm/modules/ModulesActivity$11;

    invoke-direct {v2, p0}, Lcom/konka/mm/modules/ModulesActivity$11;-><init>(Lcom/konka/mm/modules/ModulesActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v1, p0, Lcom/konka/mm/modules/ModulesActivity;->sortBtns:Landroid/widget/RadioGroup;

    iget-object v2, p0, Lcom/konka/mm/modules/ModulesActivity;->changeListener:Landroid/widget/RadioGroup$OnCheckedChangeListener;

    invoke-virtual {v1, v2}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    :goto_0
    return-void

    :cond_1
    invoke-static {p0}, Lcom/konka/mm/finals/CommonFinals;->sdCardNoFound(Landroid/app/Activity;)V

    goto :goto_0
.end method

.method public cancelProgressDlg()V
    .locals 2

    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity;->proDlg:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity;->handler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_0
    return-void
.end method

.method public checkScreenMode()V
    .locals 3

    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    invoke-virtual {p0}, Lcom/konka/mm/modules/ModulesActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    iget v1, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v1, p0, Lcom/konka/mm/modules/ModulesActivity;->screenWidth:I

    iget v1, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    iput v1, p0, Lcom/konka/mm/modules/ModulesActivity;->screenHeight:I

    iget v1, p0, Lcom/konka/mm/modules/ModulesActivity;->screenWidth:I

    iget v2, p0, Lcom/konka/mm/modules/ModulesActivity;->screenWidth_720p:I

    if-ne v1, v2, :cond_0

    iget v1, p0, Lcom/konka/mm/modules/ModulesActivity;->screenHeight:I

    iget v2, p0, Lcom/konka/mm/modules/ModulesActivity;->screenHeight_720p:I

    if-ne v1, v2, :cond_0

    const/4 v1, 0x0

    iput v1, p0, Lcom/konka/mm/modules/ModulesActivity;->mScreen_mode:I

    :goto_0
    return-void

    :cond_0
    const/4 v1, 0x1

    iput v1, p0, Lcom/konka/mm/modules/ModulesActivity;->mScreen_mode:I

    goto :goto_0
.end method

.method public isBtnFocuse()Z
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity;->sort_btn:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->isFocusable()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f030018    # com.konka.mm.R.layout.modules_list

    invoke-virtual {p0, v0}, Lcom/konka/mm/modules/ModulesActivity;->setContentView(I)V

    invoke-direct {p0}, Lcom/konka/mm/modules/ModulesActivity;->init()V

    invoke-virtual {p0}, Lcom/konka/mm/modules/ModulesActivity;->checkScreenMode()V

    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 5
    .param p1    # I

    const v4, 0x7f090009    # com.konka.mm.R.string.MM_QUIE

    const/4 v3, 0x1

    packed-switch p1, :pswitch_data_0

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    :pswitch_0
    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity;->mDialogScan:Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/konka/mm/modules/ModulesActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0900a8    # com.konka.mm.R.string.scan_disk_file

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity;->mDialogScan:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity;->mDialogScan:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity;->mDialogScan:Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/konka/mm/modules/ModulesActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/konka/mm/modules/ModulesActivity$BtnScanDialogListener;

    invoke-direct {v2, p0}, Lcom/konka/mm/modules/ModulesActivity$BtnScanDialogListener;-><init>(Lcom/konka/mm/modules/ModulesActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/ProgressDialog;->setButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity;->mDialogScan:Landroid/app/ProgressDialog;

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity;->mDialogScan:Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/konka/mm/modules/ModulesActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0900a9    # com.konka.mm.R.string.update_disk_file

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity;->mDialogScan:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity;->mDialogScan:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity;->mDialogScan:Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/konka/mm/modules/ModulesActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/konka/mm/modules/ModulesActivity$BtnScanDialogListener;

    invoke-direct {v2, p0}, Lcom/konka/mm/modules/ModulesActivity$BtnScanDialogListener;-><init>(Lcom/konka/mm/modules/ModulesActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/ProgressDialog;->setButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity;->mDialogScan:Landroid/app/ProgressDialog;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onDestroy()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/konka/mm/modules/ModulesActivity;->isStopScanRun:Z

    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/konka/mm/modules/ModulesActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    :cond_0
    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity;->broadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/konka/mm/modules/ModulesActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/konka/mm/modules/ModulesActivity;->infos:Ljava/util/ArrayList;

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    :try_start_0
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    packed-switch v1, :pswitch_data_0

    :goto_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    return v1

    :pswitch_0
    const/4 v1, -0x1

    :try_start_1
    invoke-virtual {p0, v1}, Lcom/konka/mm/modules/ModulesActivity;->snapScreen(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    :pswitch_1
    const/4 v1, 0x1

    :try_start_2
    invoke-virtual {p0, v1}, Lcom/konka/mm/modules/ModulesActivity;->snapScreen(I)V

    goto :goto_0

    :pswitch_2
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/konka/mm/modules/ModulesActivity;->setBtnFocuseFlag(Z)V

    goto :goto_0

    :pswitch_3
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/konka/mm/modules/ModulesActivity;->setBtnFocuseFlag(Z)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x13
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onPause()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    return-void
.end method

.method protected onResume()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "konka.voice.control.action.USB_CONTEXT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/mm/modules/ModulesActivity;->broadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/konka/mm/modules/ModulesActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method protected onStop()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/konka/mm/modules/ModulesActivity;->isStopScanRun:Z

    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/view/MotionEvent;

    const/high16 v3, 0x42480000    # 50.0f

    const/4 v0, 0x1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_0
    invoke-super {p0, p2}, Landroid/app/Activity;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    :goto_1
    return v0

    :pswitch_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/konka/mm/modules/ModulesActivity;->startX:F

    goto :goto_0

    :pswitch_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    iget v2, p0, Lcom/konka/mm/modules/ModulesActivity;->startX:F

    cmpl-float v1, v1, v2

    if-lez v1, :cond_1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    iget v2, p0, Lcom/konka/mm/modules/ModulesActivity;->startX:F

    sub-float/2addr v1, v2

    cmpl-float v1, v1, v3

    if-lez v1, :cond_1

    const/4 v1, -0x1

    invoke-virtual {p0, v1}, Lcom/konka/mm/modules/ModulesActivity;->snapScreen(I)V

    goto :goto_1

    :cond_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    iget v2, p0, Lcom/konka/mm/modules/ModulesActivity;->startX:F

    cmpg-float v1, v1, v2

    if-gez v1, :cond_0

    iget v1, p0, Lcom/konka/mm/modules/ModulesActivity;->startX:F

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    sub-float/2addr v1, v2

    cmpl-float v1, v1, v3

    if-lez v1, :cond_0

    invoke-virtual {p0, v0}, Lcom/konka/mm/modules/ModulesActivity;->snapScreen(I)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setBtnFocuseFlag(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity;->sort_btn:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setFocusable(Z)V

    return-void
.end method

.method public setPageInfo()V
    .locals 6

    const/4 v5, 0x4

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity;->pageInfomation:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/konka/mm/modules/ModulesActivity;->currentPage:I

    add-int/lit8 v2, v2, 0x1

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/konka/mm/modules/ModulesActivity;->pageCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/konka/mm/modules/ModulesActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09009a    # com.konka.mm.R.string.page

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget v0, p0, Lcom/konka/mm/modules/ModulesActivity;->pageCount:I

    const/4 v1, 0x1

    if-gt v0, v1, :cond_1

    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity;->pageLeft:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity;->pageRight:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setVisibility(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/konka/mm/modules/ModulesActivity;->currentPage:I

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity;->pageLeft:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity;->pageRight:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0

    :cond_2
    iget v0, p0, Lcom/konka/mm/modules/ModulesActivity;->currentPage:I

    if-eqz v0, :cond_3

    iget v0, p0, Lcom/konka/mm/modules/ModulesActivity;->currentPage:I

    iget v1, p0, Lcom/konka/mm/modules/ModulesActivity;->pageCount:I

    add-int/lit8 v1, v1, -0x1

    if-eq v0, v1, :cond_3

    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity;->pageLeft:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity;->pageRight:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0

    :cond_3
    iget v0, p0, Lcom/konka/mm/modules/ModulesActivity;->currentPage:I

    iget v1, p0, Lcom/konka/mm/modules/ModulesActivity;->pageCount:I

    add-int/lit8 v1, v1, -0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity;->pageLeft:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity;->pageRight:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0
.end method

.method public setPreOrNextScreen(I)V
    .locals 6
    .param p1    # I

    iget v2, p0, Lcom/konka/mm/modules/ModulesActivity;->currentPage:I

    add-int/2addr v2, p1

    iput v2, p0, Lcom/konka/mm/modules/ModulesActivity;->currentPage:I

    iget v2, p0, Lcom/konka/mm/modules/ModulesActivity;->currentPage:I

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v2, p0, Lcom/konka/mm/modules/ModulesActivity;->currentPage:I

    iget v3, p0, Lcom/konka/mm/modules/ModulesActivity;->pageCount:I

    add-int/lit8 v3, v3, -0x1

    if-ge v2, v3, :cond_0

    iget v2, p0, Lcom/konka/mm/modules/ModulesActivity;->curScreen:I

    add-int/2addr v2, p1

    add-int/lit8 v2, v2, 0x3

    rem-int/lit8 v0, v2, 0x3

    iget-object v2, p0, Lcom/konka/mm/modules/ModulesActivity;->pageGridView:[Landroid/widget/GridView;

    aget-object v2, v2, v0

    iget v3, p0, Lcom/konka/mm/modules/ModulesActivity;->numColumns:I

    invoke-virtual {v2, v3}, Landroid/widget/GridView;->setNumColumns(I)V

    const/4 v1, 0x0

    iget v2, p0, Lcom/konka/mm/modules/ModulesActivity;->currentPage:I

    add-int/2addr v2, p1

    iget v3, p0, Lcom/konka/mm/modules/ModulesActivity;->pageCount:I

    add-int/lit8 v3, v3, -0x1

    if-ge v2, v3, :cond_3

    iget v1, p0, Lcom/konka/mm/modules/ModulesActivity;->mCountPerPage:I

    :cond_2
    :goto_1
    iget-object v2, p0, Lcom/konka/mm/modules/ModulesActivity;->pageGridView:[Landroid/widget/GridView;

    aget-object v2, v2, v0

    new-instance v3, Lcom/konka/mm/adapters/ModulesListAdapter;

    iget v4, p0, Lcom/konka/mm/modules/ModulesActivity;->currentPage:I

    add-int/2addr v4, p1

    iget v5, p0, Lcom/konka/mm/modules/ModulesActivity;->mCountPerPage:I

    invoke-direct {v3, p0, v4, v1, v5}, Lcom/konka/mm/adapters/ModulesListAdapter;-><init>(Lcom/konka/mm/modules/ModulesActivity;III)V

    invoke-virtual {v2, v3}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v2, p0, Lcom/konka/mm/modules/ModulesActivity;->pageGridView:[Landroid/widget/GridView;

    aget-object v2, v2, v0

    iget-object v3, p0, Lcom/konka/mm/modules/ModulesActivity;->gridOnItemClick:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    goto :goto_0

    :cond_3
    iget v2, p0, Lcom/konka/mm/modules/ModulesActivity;->ListSize:I

    iget v3, p0, Lcom/konka/mm/modules/ModulesActivity;->mCountPerPage:I

    iget v4, p0, Lcom/konka/mm/modules/ModulesActivity;->pageCount:I

    add-int/lit8 v4, v4, -0x1

    mul-int/2addr v3, v4

    sub-int v1, v2, v3

    iget v2, p0, Lcom/konka/mm/modules/ModulesActivity;->mCountPerPage:I

    if-lt v1, v2, :cond_2

    iget v1, p0, Lcom/konka/mm/modules/ModulesActivity;->mCountPerPage:I

    goto :goto_1
.end method

.method public showProgressDialog(II)V
    .locals 3
    .param p1    # I
    .param p2    # I

    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity;->proDlg:Landroid/app/ProgressDialog;

    if-nez v0, :cond_0

    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/konka/mm/modules/ModulesActivity;->proDlg:Landroid/app/ProgressDialog;

    :cond_0
    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity;->proDlg:Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/konka/mm/modules/ModulesActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity;->proDlg:Landroid/app/ProgressDialog;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/konka/mm/modules/ModulesActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "..."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity;->proDlg:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    return-void
.end method

.method public snapScreen(I)V
    .locals 5
    .param p1    # I

    const/4 v4, 0x0

    const/4 v3, -0x1

    const/4 v2, 0x1

    invoke-virtual {p0, v4}, Lcom/konka/mm/modules/ModulesActivity;->setBtnFocuseFlag(Z)V

    if-ne p1, v2, :cond_4

    iget v0, p0, Lcom/konka/mm/modules/ModulesActivity;->currentPage:I

    iget v1, p0, Lcom/konka/mm/modules/ModulesActivity;->pageCount:I

    add-int/lit8 v1, v1, -0x1

    if-lt v0, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity;->viewFilpper:Landroid/widget/ViewFlipper;

    iget-object v1, p0, Lcom/konka/mm/modules/ModulesActivity;->mPushLeftInAnim:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->setInAnimation(Landroid/view/animation/Animation;)V

    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity;->viewFilpper:Landroid/widget/ViewFlipper;

    iget-object v1, p0, Lcom/konka/mm/modules/ModulesActivity;->mPustRightOutAnim:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->setOutAnimation(Landroid/view/animation/Animation;)V

    iget v0, p0, Lcom/konka/mm/modules/ModulesActivity;->curScreen:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/konka/mm/modules/ModulesActivity;->curScreen:I

    iget v0, p0, Lcom/konka/mm/modules/ModulesActivity;->curScreen:I

    rem-int/lit8 v0, v0, 0x3

    iput v0, p0, Lcom/konka/mm/modules/ModulesActivity;->curScreen:I

    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity;->viewFilpper:Landroid/widget/ViewFlipper;

    invoke-virtual {v0}, Landroid/widget/ViewFlipper;->showNext()V

    invoke-virtual {p0, v2}, Lcom/konka/mm/modules/ModulesActivity;->setPreOrNextScreen(I)V

    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity;->pageGridView:[Landroid/widget/GridView;

    iget v1, p0, Lcom/konka/mm/modules/ModulesActivity;->curScreen:I

    aget-object v0, v0, v1

    invoke-virtual {v0, v2}, Landroid/widget/GridView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity;->pageGridView:[Landroid/widget/GridView;

    iget v1, p0, Lcom/konka/mm/modules/ModulesActivity;->curScreen:I

    aget-object v0, v0, v1

    invoke-virtual {v0}, Landroid/widget/GridView;->requestFocus()Z

    if-ne p1, v2, :cond_5

    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity;->pageGridView:[Landroid/widget/GridView;

    iget v1, p0, Lcom/konka/mm/modules/ModulesActivity;->curScreen:I

    aget-object v0, v0, v1

    invoke-virtual {v0, v4}, Landroid/widget/GridView;->setSelection(I)V

    :goto_2
    if-ne p1, v2, :cond_6

    invoke-direct {p0, v4}, Lcom/konka/mm/modules/ModulesActivity;->initItemInfo(I)V

    :cond_3
    :goto_3
    invoke-virtual {p0}, Lcom/konka/mm/modules/ModulesActivity;->setPageInfo()V

    goto :goto_0

    :cond_4
    if-ne p1, v3, :cond_2

    iget v0, p0, Lcom/konka/mm/modules/ModulesActivity;->currentPage:I

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity;->viewFilpper:Landroid/widget/ViewFlipper;

    iget-object v1, p0, Lcom/konka/mm/modules/ModulesActivity;->mPustRightInAnim:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->setInAnimation(Landroid/view/animation/Animation;)V

    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity;->viewFilpper:Landroid/widget/ViewFlipper;

    iget-object v1, p0, Lcom/konka/mm/modules/ModulesActivity;->mPushLeftOutAnim:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->setOutAnimation(Landroid/view/animation/Animation;)V

    iget v0, p0, Lcom/konka/mm/modules/ModulesActivity;->curScreen:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/konka/mm/modules/ModulesActivity;->curScreen:I

    iget v0, p0, Lcom/konka/mm/modules/ModulesActivity;->curScreen:I

    add-int/lit8 v0, v0, 0x3

    rem-int/lit8 v0, v0, 0x3

    iput v0, p0, Lcom/konka/mm/modules/ModulesActivity;->curScreen:I

    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity;->viewFilpper:Landroid/widget/ViewFlipper;

    invoke-virtual {v0}, Landroid/widget/ViewFlipper;->showPrevious()V

    invoke-virtual {p0, v3}, Lcom/konka/mm/modules/ModulesActivity;->setPreOrNextScreen(I)V

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity;->pageGridView:[Landroid/widget/GridView;

    iget v1, p0, Lcom/konka/mm/modules/ModulesActivity;->curScreen:I

    aget-object v0, v0, v1

    iget v1, p0, Lcom/konka/mm/modules/ModulesActivity;->numColumns:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setSelection(I)V

    goto :goto_2

    :cond_6
    if-ne p1, v3, :cond_3

    iget v0, p0, Lcom/konka/mm/modules/ModulesActivity;->numColumns:I

    add-int/lit8 v0, v0, -0x1

    invoke-direct {p0, v0}, Lcom/konka/mm/modules/ModulesActivity;->initItemInfo(I)V

    goto :goto_3
.end method

.method public sortBtnOnKeyOrOnClickEvent()V
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity;->popupWindow:Landroid/widget/PopupWindow;

    if-nez v0, :cond_0

    iget v0, p0, Lcom/konka/mm/modules/ModulesActivity;->mScreen_mode:I

    if-nez v0, :cond_1

    new-instance v0, Landroid/widget/PopupWindow;

    iget-object v1, p0, Lcom/konka/mm/modules/ModulesActivity;->popupView:Landroid/view/View;

    const/16 v2, 0x96

    const/16 v3, 0xb6

    invoke-direct {v0, v1, v2, v3, v5}, Landroid/widget/PopupWindow;-><init>(Landroid/view/View;IIZ)V

    iput-object v0, p0, Lcom/konka/mm/modules/ModulesActivity;->popupWindow:Landroid/widget/PopupWindow;

    :goto_0
    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity;->popupWindow:Landroid/widget/PopupWindow;

    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v1}, Landroid/graphics/drawable/BitmapDrawable;-><init>()V

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity;->popupWindow:Landroid/widget/PopupWindow;

    new-instance v1, Lcom/konka/mm/modules/ModulesActivity$12;

    invoke-direct {v1, p0}, Lcom/konka/mm/modules/ModulesActivity$12;-><init>(Lcom/konka/mm/modules/ModulesActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V

    :cond_0
    iget-boolean v0, p0, Lcom/konka/mm/modules/ModulesActivity;->isPopup:Z

    if-nez v0, :cond_3

    iget v0, p0, Lcom/konka/mm/modules/ModulesActivity;->mScreen_mode:I

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity;->popupWindow:Landroid/widget/PopupWindow;

    iget-object v1, p0, Lcom/konka/mm/modules/ModulesActivity;->sort_btn:Landroid/widget/ImageView;

    const/16 v2, -0x3d

    invoke-virtual {v0, v1, v2, v4}, Landroid/widget/PopupWindow;->showAsDropDown(Landroid/view/View;II)V

    :goto_1
    iput-boolean v5, p0, Lcom/konka/mm/modules/ModulesActivity;->isPopup:Z

    :goto_2
    return-void

    :cond_1
    new-instance v0, Landroid/widget/PopupWindow;

    iget-object v1, p0, Lcom/konka/mm/modules/ModulesActivity;->popupView:Landroid/view/View;

    const/16 v2, 0x140

    const/16 v3, 0x113

    invoke-direct {v0, v1, v2, v3, v5}, Landroid/widget/PopupWindow;-><init>(Landroid/view/View;IIZ)V

    iput-object v0, p0, Lcom/konka/mm/modules/ModulesActivity;->popupWindow:Landroid/widget/PopupWindow;

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity;->popupWindow:Landroid/widget/PopupWindow;

    iget-object v1, p0, Lcom/konka/mm/modules/ModulesActivity;->sort_btn:Landroid/widget/ImageView;

    const/16 v2, -0xbe

    invoke-virtual {v0, v1, v2, v4}, Landroid/widget/PopupWindow;->showAsDropDown(Landroid/view/View;II)V

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity;->popupWindow:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    iput-boolean v4, p0, Lcom/konka/mm/modules/ModulesActivity;->isPopup:Z

    goto :goto_2
.end method

.method public startSortThread()V
    .locals 3

    const v2, 0x7f09005e    # com.konka.mm.R.string.SORT

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/konka/mm/modules/ModulesActivity;->isStopScanRun:Z

    invoke-virtual {p0, v2, v2}, Lcom/konka/mm/modules/ModulesActivity;->showProgressDialog(II)V

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/konka/mm/modules/ModulesActivity$SortThread;

    iget v2, p0, Lcom/konka/mm/modules/ModulesActivity;->sortType:I

    invoke-direct {v1, p0, v2}, Lcom/konka/mm/modules/ModulesActivity$SortThread;-><init>(Lcom/konka/mm/modules/ModulesActivity;I)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method
