.class Lcom/konka/mm/modules/ModulesActivity$7;
.super Ljava/lang/Object;
.source "ModulesActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/mm/modules/ModulesActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/mm/modules/ModulesActivity;


# direct methods
.method constructor <init>(Lcom/konka/mm/modules/ModulesActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/modules/ModulesActivity$7;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 13
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    iget-object v10, p0, Lcom/konka/mm/modules/ModulesActivity$7;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    # getter for: Lcom/konka/mm/modules/ModulesActivity;->currentPage:I
    invoke-static {v10}, Lcom/konka/mm/modules/ModulesActivity;->access$7(Lcom/konka/mm/modules/ModulesActivity;)I

    move-result v10

    iget-object v11, p0, Lcom/konka/mm/modules/ModulesActivity$7;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget v11, v11, Lcom/konka/mm/modules/ModulesActivity;->mCountPerPage:I

    mul-int/2addr v10, v11

    add-int v1, v10, p3

    iget-object v10, p0, Lcom/konka/mm/modules/ModulesActivity$7;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v10, v10, Lcom/konka/mm/modules/ModulesActivity;->mListItemPaths:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v10

    if-le v1, v10, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v10, p0, Lcom/konka/mm/modules/ModulesActivity$7;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v10, v10, Lcom/konka/mm/modules/ModulesActivity;->mSourceType:Lcom/konka/mm/finals/CommonFinals$MMSourceType;

    sget-object v11, Lcom/konka/mm/finals/CommonFinals$MMSourceType;->KONKA_MM_SOURCE_VIDEO:Lcom/konka/mm/finals/CommonFinals$MMSourceType;

    if-ne v10, v11, :cond_4

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    iget-object v10, p0, Lcom/konka/mm/modules/ModulesActivity$7;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v10, v10, Lcom/konka/mm/modules/ModulesActivity;->infos:Ljava/util/ArrayList;

    invoke-virtual {v10, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/io/File;

    invoke-virtual {v10}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v9

    const/4 v4, 0x0

    :goto_1
    iget-object v10, p0, Lcom/konka/mm/modules/ModulesActivity$7;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v10, v10, Lcom/konka/mm/modules/ModulesActivity;->infos:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v10

    if-lt v4, v10, :cond_2

    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v11, "the movie play list:"

    invoke-virtual {v10, v11}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    const/4 v6, 0x0

    :goto_2
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v10

    if-lt v6, v10, :cond_3

    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "current playing movie path: "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :try_start_0
    new-instance v5, Landroid/content/Intent;

    invoke-direct {v5}, Landroid/content/Intent;-><init>()V

    const-string v10, "com.konka.kkvideoplayer"

    const-string v11, "com.konka.kkvideoplayer.VideoPlayerMain"

    invoke-virtual {v5, v10, v11}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v10, "videofile"

    invoke-virtual {v0, v10, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v10, "com.konka.mm.movie.list"

    invoke-virtual {v0, v10, v8}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    invoke-virtual {v5, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "------>goto movie theater["

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "]"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Liapp/eric/utils/base/Trace;->Info(Ljava/lang/String;)V

    iget-object v10, p0, Lcom/konka/mm/modules/ModulesActivity$7;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    invoke-virtual {v10, v5}, Lcom/konka/mm/modules/ModulesActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v2

    const-string v10, "KONKA_MM"

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_2
    iget-object v10, p0, Lcom/konka/mm/modules/ModulesActivity$7;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v10, v10, Lcom/konka/mm/modules/ModulesActivity;->infos:Ljava/util/ArrayList;

    invoke-virtual {v10, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/io/File;

    invoke-virtual {v10}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_1

    :cond_3
    sget-object v11, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuilder;

    add-int/lit8 v12, v6, 0x1

    invoke-static {v12}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v10, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v12, ":  "

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v8, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v11, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_2

    :cond_4
    iget-object v10, p0, Lcom/konka/mm/modules/ModulesActivity$7;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v10, v10, Lcom/konka/mm/modules/ModulesActivity;->mSourceType:Lcom/konka/mm/finals/CommonFinals$MMSourceType;

    sget-object v11, Lcom/konka/mm/finals/CommonFinals$MMSourceType;->KONKA_MM_SOURCE_TXT:Lcom/konka/mm/finals/CommonFinals$MMSourceType;

    if-ne v10, v11, :cond_7

    iget-object v10, p0, Lcom/konka/mm/modules/ModulesActivity$7;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v10, v10, Lcom/konka/mm/modules/ModulesActivity;->infos:Ljava/util/ArrayList;

    invoke-virtual {v10, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/io/File;

    invoke-virtual {v10}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v9

    const-string v10, ".txt"

    invoke-virtual {v9, v10}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_5

    new-instance v5, Landroid/content/Intent;

    const-string v10, "com.konka.kkreader.remote.txtactivity"

    invoke-direct {v5, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v10, "filePath"

    invoke-virtual {v5, v10, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v10, 0x10000000

    invoke-virtual {v5, v10}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "strPath"

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Liapp/eric/utils/base/Trace;->Debug(Ljava/lang/String;)V

    iget-object v10, p0, Lcom/konka/mm/modules/ModulesActivity$7;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    invoke-virtual {v10, v5}, Lcom/konka/mm/modules/ModulesActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_5
    const-string v10, ".pdf"

    invoke-virtual {v9, v10}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_6

    new-instance v5, Landroid/content/Intent;

    invoke-direct {v5}, Landroid/content/Intent;-><init>()V

    const-string v10, "android.intent.action.VIEW"

    invoke-virtual {v5, v10}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "file://"

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v10

    const-string v11, "application/pdf"

    invoke-virtual {v5, v10, v11}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v10, 0x10000000

    invoke-virtual {v5, v10}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    iget-object v10, p0, Lcom/konka/mm/modules/ModulesActivity$7;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    invoke-virtual {v10, v5}, Lcom/konka/mm/modules/ModulesActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_6
    const-string v10, ".doc"

    invoke-virtual {v9, v10}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_0

    new-instance v5, Landroid/content/Intent;

    invoke-direct {v5}, Landroid/content/Intent;-><init>()V

    const-string v10, "android.intent.action.VIEW"

    invoke-virtual {v5, v10}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "file://"

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v10

    const-string v11, "application/msword"

    invoke-virtual {v5, v10, v11}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v10, 0x10000000

    invoke-virtual {v5, v10}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    iget-object v10, p0, Lcom/konka/mm/modules/ModulesActivity$7;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    invoke-virtual {v10, v5}, Lcom/konka/mm/modules/ModulesActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_7
    iget-object v10, p0, Lcom/konka/mm/modules/ModulesActivity$7;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v10, v10, Lcom/konka/mm/modules/ModulesActivity;->mSourceType:Lcom/konka/mm/finals/CommonFinals$MMSourceType;

    sget-object v11, Lcom/konka/mm/finals/CommonFinals$MMSourceType;->KONKA_MM_SOURCE_APK:Lcom/konka/mm/finals/CommonFinals$MMSourceType;

    if-ne v10, v11, :cond_8

    iget-object v10, p0, Lcom/konka/mm/modules/ModulesActivity$7;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v10, v10, Lcom/konka/mm/modules/ModulesActivity;->infos:Ljava/util/ArrayList;

    invoke-virtual {v10, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/io/File;

    invoke-virtual {v10}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v9

    new-instance v5, Landroid/content/Intent;

    invoke-direct {v5}, Landroid/content/Intent;-><init>()V

    const-string v10, "android.intent.action.VIEW"

    invoke-virtual {v5, v10}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "file://"

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v10

    const-string v11, "application/vnd.android.package-archive"

    invoke-virtual {v5, v10, v11}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v10, 0x10000000

    invoke-virtual {v5, v10}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    iget-object v10, p0, Lcom/konka/mm/modules/ModulesActivity$7;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    invoke-virtual {v10, v5}, Lcom/konka/mm/modules/ModulesActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_8
    iget-object v10, p0, Lcom/konka/mm/modules/ModulesActivity$7;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v10, v10, Lcom/konka/mm/modules/ModulesActivity;->mSourceType:Lcom/konka/mm/finals/CommonFinals$MMSourceType;

    sget-object v11, Lcom/konka/mm/finals/CommonFinals$MMSourceType;->KONKA_MM_SOURCE_IMAGE:Lcom/konka/mm/finals/CommonFinals$MMSourceType;

    if-ne v10, v11, :cond_a

    new-instance v3, Ljava/io/File;

    iget-object v10, p0, Lcom/konka/mm/modules/ModulesActivity$7;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v10, v10, Lcom/konka/mm/modules/ModulesActivity;->mListItemPaths:Ljava/util/ArrayList;

    iget-object v11, p0, Lcom/konka/mm/modules/ModulesActivity$7;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    # getter for: Lcom/konka/mm/modules/ModulesActivity;->currentPage:I
    invoke-static {v11}, Lcom/konka/mm/modules/ModulesActivity;->access$7(Lcom/konka/mm/modules/ModulesActivity;)I

    move-result v11

    iget-object v12, p0, Lcom/konka/mm/modules/ModulesActivity$7;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget v12, v12, Lcom/konka/mm/modules/ModulesActivity;->mCountPerPage:I

    mul-int/2addr v11, v12

    add-int v11, v11, p3

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    invoke-direct {v3, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v10

    if-nez v10, :cond_9

    const-string v10, "file not exists!"

    invoke-static {v10}, Liapp/eric/utils/base/Trace;->Info(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_9
    iget-object v10, p0, Lcom/konka/mm/modules/ModulesActivity$7;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    invoke-virtual {v10}, Lcom/konka/mm/modules/ModulesActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    check-cast v7, Lcom/konka/mm/GlobalData;

    iget-object v10, p0, Lcom/konka/mm/modules/ModulesActivity$7;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v10, v10, Lcom/konka/mm/modules/ModulesActivity;->mListItemPaths:Ljava/util/ArrayList;

    invoke-virtual {v7, v10}, Lcom/konka/mm/GlobalData;->setmMMFileList(Ljava/util/ArrayList;)V

    new-instance v5, Landroid/content/Intent;

    iget-object v10, p0, Lcom/konka/mm/modules/ModulesActivity$7;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    const-class v11, Lcom/konka/mm/photo/AutoShowPicActivity;

    invoke-direct {v5, v10, v11}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v10, "com.konka.mm.file.where.come.from"

    const-string v11, "com.konka.mm.file.come.from.one.disk"

    invoke-virtual {v0, v10, v11}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v10, "com.konka.mm.file.index.posstion"

    iget-object v11, p0, Lcom/konka/mm/modules/ModulesActivity$7;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    # getter for: Lcom/konka/mm/modules/ModulesActivity;->currentPage:I
    invoke-static {v11}, Lcom/konka/mm/modules/ModulesActivity;->access$7(Lcom/konka/mm/modules/ModulesActivity;)I

    move-result v11

    iget-object v12, p0, Lcom/konka/mm/modules/ModulesActivity$7;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget v12, v12, Lcom/konka/mm/modules/ModulesActivity;->mCountPerPage:I

    mul-int/2addr v11, v12

    add-int v11, v11, p3

    invoke-virtual {v0, v10, v11}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v5, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    iget-object v10, p0, Lcom/konka/mm/modules/ModulesActivity$7;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    invoke-virtual {v10, v5}, Lcom/konka/mm/modules/ModulesActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_a
    iget-object v10, p0, Lcom/konka/mm/modules/ModulesActivity$7;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v10, v10, Lcom/konka/mm/modules/ModulesActivity;->mSourceType:Lcom/konka/mm/finals/CommonFinals$MMSourceType;

    sget-object v11, Lcom/konka/mm/finals/CommonFinals$MMSourceType;->KONKA_MM_SOURCE_AUDIO:Lcom/konka/mm/finals/CommonFinals$MMSourceType;

    if-ne v10, v11, :cond_0

    new-instance v3, Ljava/io/File;

    iget-object v10, p0, Lcom/konka/mm/modules/ModulesActivity$7;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v10, v10, Lcom/konka/mm/modules/ModulesActivity;->mListItemPaths:Ljava/util/ArrayList;

    invoke-virtual {v10, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    invoke-direct {v3, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v10

    if-nez v10, :cond_b

    const-string v10, "file not exists!"

    invoke-static {v10}, Liapp/eric/utils/base/Trace;->Info(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_b
    iget-object v10, p0, Lcom/konka/mm/modules/ModulesActivity$7;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    invoke-virtual {v10}, Lcom/konka/mm/modules/ModulesActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    check-cast v7, Lcom/konka/mm/GlobalData;

    iget-object v10, p0, Lcom/konka/mm/modules/ModulesActivity$7;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v10, v10, Lcom/konka/mm/modules/ModulesActivity;->mListItemPaths:Ljava/util/ArrayList;

    invoke-virtual {v7, v10}, Lcom/konka/mm/GlobalData;->setmMMFileList(Ljava/util/ArrayList;)V

    new-instance v5, Landroid/content/Intent;

    iget-object v10, p0, Lcom/konka/mm/modules/ModulesActivity$7;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    const-class v11, Lcom/konka/mm/music/MusicActivity;

    invoke-direct {v5, v10, v11}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v10, "com.konka.mm.file.index.posstion"

    invoke-virtual {v0, v10, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v10, "com.konka.mm.file.where.come.from"

    const-string v11, "com.konka.mm.file.come.from.one.disk"

    invoke-virtual {v0, v10, v11}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    iget-object v10, p0, Lcom/konka/mm/modules/ModulesActivity$7;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    invoke-virtual {v10, v5}, Lcom/konka/mm/modules/ModulesActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0
.end method
