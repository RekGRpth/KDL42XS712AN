.class public Lcom/konka/mm/modules/ModulesActivity$ScanSdReceiver;
.super Landroid/content/BroadcastReceiver;
.source "ModulesActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/mm/modules/ModulesActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ScanSdReceiver"
.end annotation


# instance fields
.field private activity:Lcom/konka/mm/IActivity;

.field final synthetic this$0:Lcom/konka/mm/modules/ModulesActivity;


# direct methods
.method public constructor <init>(Lcom/konka/mm/modules/ModulesActivity;Lcom/konka/mm/IActivity;)V
    .locals 0
    .param p2    # Lcom/konka/mm/IActivity;

    iput-object p1, p0, Lcom/konka/mm/modules/ModulesActivity$ScanSdReceiver;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    iput-object p2, p0, Lcom/konka/mm/modules/ModulesActivity$ScanSdReceiver;->activity:Lcom/konka/mm/IActivity;

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.MEDIA_SCANNER_STARTED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "android.intent.action.MEDIA_SCANNER_FINISHED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "onReceive....."

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/mm/modules/ModulesActivity$ScanSdReceiver;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/konka/mm/modules/ModulesActivity;->access$14(Lcom/konka/mm/modules/ModulesActivity;Z)V

    :cond_0
    return-void
.end method
