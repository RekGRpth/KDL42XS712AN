.class Lcom/konka/mm/modules/ModulesActivity$5;
.super Ljava/lang/Object;
.source "ModulesActivity.java"

# interfaces
.implements Landroid/widget/RadioGroup$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/mm/modules/ModulesActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/mm/modules/ModulesActivity;


# direct methods
.method constructor <init>(Lcom/konka/mm/modules/ModulesActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/modules/ModulesActivity$5;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/RadioGroup;I)V
    .locals 4
    .param p1    # Landroid/widget/RadioGroup;
    .param p2    # I

    const/4 v3, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity$5;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    invoke-static {v0}, Lcom/konka/mm/tools/FileTool;->usbIsExists(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    sparse-switch p2, :sswitch_data_0

    :goto_0
    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity$5;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    # getter for: Lcom/konka/mm/modules/ModulesActivity;->popupWindow:Landroid/widget/PopupWindow;
    invoke-static {v0}, Lcom/konka/mm/modules/ModulesActivity;->access$5(Lcom/konka/mm/modules/ModulesActivity;)Landroid/widget/PopupWindow;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity$5;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    # getter for: Lcom/konka/mm/modules/ModulesActivity;->popupWindow:Landroid/widget/PopupWindow;
    invoke-static {v0}, Lcom/konka/mm/modules/ModulesActivity;->access$5(Lcom/konka/mm/modules/ModulesActivity;)Landroid/widget/PopupWindow;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity$5;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    invoke-static {v0, v2}, Lcom/konka/mm/modules/ModulesActivity;->access$6(Lcom/konka/mm/modules/ModulesActivity;Z)V

    :cond_0
    :goto_1
    return-void

    :sswitch_0
    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity$5;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    invoke-static {v0, v2}, Lcom/konka/mm/modules/ModulesActivity;->access$4(Lcom/konka/mm/modules/ModulesActivity;I)V

    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity$5;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    invoke-virtual {v0}, Lcom/konka/mm/modules/ModulesActivity;->startSortThread()V

    goto :goto_0

    :sswitch_1
    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity$5;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    invoke-static {v0, v1}, Lcom/konka/mm/modules/ModulesActivity;->access$4(Lcom/konka/mm/modules/ModulesActivity;I)V

    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity$5;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    invoke-virtual {v0}, Lcom/konka/mm/modules/ModulesActivity;->startSortThread()V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity$5;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    invoke-static {v0, v3}, Lcom/konka/mm/modules/ModulesActivity;->access$4(Lcom/konka/mm/modules/ModulesActivity;I)V

    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity$5;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    invoke-virtual {v0}, Lcom/konka/mm/modules/ModulesActivity;->startSortThread()V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity$5;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/konka/mm/modules/ModulesActivity;->access$4(Lcom/konka/mm/modules/ModulesActivity;I)V

    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity$5;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    invoke-virtual {v0}, Lcom/konka/mm/modules/ModulesActivity;->startSortThread()V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity$5;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v0, v0, Lcom/konka/mm/modules/ModulesActivity;->infos:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity$5;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v0, v0, Lcom/konka/mm/modules/ModulesActivity;->infos:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity$5;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iput v1, v0, Lcom/konka/mm/modules/ModulesActivity;->Item_Show_Mode:I

    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity$5;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v1, p0, Lcom/konka/mm/modules/ModulesActivity$5;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v1, v1, Lcom/konka/mm/modules/ModulesActivity;->infos:Ljava/util/ArrayList;

    # invokes: Lcom/konka/mm/modules/ModulesActivity;->initGridView(Ljava/util/List;)V
    invoke-static {v0, v1}, Lcom/konka/mm/modules/ModulesActivity;->access$2(Lcom/konka/mm/modules/ModulesActivity;Ljava/util/List;)V

    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity$5;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    invoke-virtual {v0, v2}, Lcom/konka/mm/modules/ModulesActivity;->setBtnFocuseFlag(Z)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity$5;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v0, v0, Lcom/konka/mm/modules/ModulesActivity;->infos:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity$5;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v0, v0, Lcom/konka/mm/modules/ModulesActivity;->infos:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity$5;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iput v3, v0, Lcom/konka/mm/modules/ModulesActivity;->Item_Show_Mode:I

    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity$5;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v1, p0, Lcom/konka/mm/modules/ModulesActivity$5;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v1, v1, Lcom/konka/mm/modules/ModulesActivity;->infos:Ljava/util/ArrayList;

    # invokes: Lcom/konka/mm/modules/ModulesActivity;->initGridView(Ljava/util/List;)V
    invoke-static {v0, v1}, Lcom/konka/mm/modules/ModulesActivity;->access$2(Lcom/konka/mm/modules/ModulesActivity;Ljava/util/List;)V

    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity$5;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    invoke-virtual {v0, v2}, Lcom/konka/mm/modules/ModulesActivity;->setBtnFocuseFlag(Z)V

    goto/16 :goto_0

    :cond_1
    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity$5;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    invoke-static {v0}, Lcom/konka/mm/finals/CommonFinals;->sdCardNoFound(Landroid/app/Activity;)V

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0b00b4 -> :sswitch_4    # com.konka.mm.R.id.list_small_style
        0x7f0b00b5 -> :sswitch_5    # com.konka.mm.R.id.list_big_style
        0x7f0b00fe -> :sswitch_1    # com.konka.mm.R.id.rb_sort_by_filename
        0x7f0b00ff -> :sswitch_2    # com.konka.mm.R.id.rb_sort_by_filesize
        0x7f0b0100 -> :sswitch_3    # com.konka.mm.R.id.rb_sort_by_filetype
        0x7f0b0101 -> :sswitch_0    # com.konka.mm.R.id.rb_sort_by_default
    .end sparse-switch
.end method
