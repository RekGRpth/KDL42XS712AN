.class Lcom/konka/mm/modules/ModulesActivity$3;
.super Landroid/os/Handler;
.source "ModulesActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/mm/modules/ModulesActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/mm/modules/ModulesActivity;


# direct methods
.method constructor <init>(Lcom/konka/mm/modules/ModulesActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/modules/ModulesActivity$3;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1    # Landroid/os/Message;

    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity$3;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v0, v0, Lcom/konka/mm/modules/ModulesActivity;->infos:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity$3;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v0, v0, Lcom/konka/mm/modules/ModulesActivity;->infos:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_3

    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity$3;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v1, p0, Lcom/konka/mm/modules/ModulesActivity$3;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    invoke-virtual {v1}, Lcom/konka/mm/modules/ModulesActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090097    # com.konka.mm.R.string.mention_info

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/mm/modules/ModulesActivity$3;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    invoke-virtual {v2}, Lcom/konka/mm/modules/ModulesActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090056    # com.konka.mm.R.string.file_no_exist

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/konka/mm/finals/CommonFinals;->quitNoFileDialog(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity$3;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    # getter for: Lcom/konka/mm/modules/ModulesActivity;->mDialogScan:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/konka/mm/modules/ModulesActivity;->access$3(Lcom/konka/mm/modules/ModulesActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity$3;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    # getter for: Lcom/konka/mm/modules/ModulesActivity;->mDialogScan:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/konka/mm/modules/ModulesActivity;->access$3(Lcom/konka/mm/modules/ModulesActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    :cond_1
    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity$3;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    # getter for: Lcom/konka/mm/modules/ModulesActivity;->proDlg:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/konka/mm/modules/ModulesActivity;->access$0(Lcom/konka/mm/modules/ModulesActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity$3;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    # getter for: Lcom/konka/mm/modules/ModulesActivity;->proDlg:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/konka/mm/modules/ModulesActivity;->access$0(Lcom/konka/mm/modules/ModulesActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity$3;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/konka/mm/modules/ModulesActivity;->access$1(Lcom/konka/mm/modules/ModulesActivity;Landroid/app/ProgressDialog;)V

    :cond_2
    return-void

    :cond_3
    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity$3;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v1, p0, Lcom/konka/mm/modules/ModulesActivity$3;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v1, v1, Lcom/konka/mm/modules/ModulesActivity;->infos:Ljava/util/ArrayList;

    # invokes: Lcom/konka/mm/modules/ModulesActivity;->initGridView(Ljava/util/List;)V
    invoke-static {v0, v1}, Lcom/konka/mm/modules/ModulesActivity;->access$2(Lcom/konka/mm/modules/ModulesActivity;Ljava/util/List;)V

    goto :goto_0
.end method
