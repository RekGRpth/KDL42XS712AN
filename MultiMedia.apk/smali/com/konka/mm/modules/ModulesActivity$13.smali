.class Lcom/konka/mm/modules/ModulesActivity$13;
.super Ljava/lang/Object;
.source "ModulesActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/mm/modules/ModulesActivity;->initGridView(Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemSelectedListener;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/mm/modules/ModulesActivity;


# direct methods
.method constructor <init>(Lcom/konka/mm/modules/ModulesActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/modules/ModulesActivity$13;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity$13;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    # invokes: Lcom/konka/mm/modules/ModulesActivity;->initItemInfo(I)V
    invoke-static {v0, p3}, Lcom/konka/mm/modules/ModulesActivity;->access$17(Lcom/konka/mm/modules/ModulesActivity;I)V

    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity$13;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v0, v0, Lcom/konka/mm/modules/ModulesActivity;->mSourceType:Lcom/konka/mm/finals/CommonFinals$MMSourceType;

    sget-object v1, Lcom/konka/mm/finals/CommonFinals$MMSourceType;->KONKA_MM_SOURCE_VIDEO:Lcom/konka/mm/finals/CommonFinals$MMSourceType;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity$13;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v0, v0, Lcom/konka/mm/modules/ModulesActivity;->mSourceType:Lcom/konka/mm/finals/CommonFinals$MMSourceType;

    sget-object v1, Lcom/konka/mm/finals/CommonFinals$MMSourceType;->KONKA_MM_SOURCE_APK:Lcom/konka/mm/finals/CommonFinals$MMSourceType;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity$13;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v0, v0, Lcom/konka/mm/modules/ModulesActivity;->mSourceType:Lcom/konka/mm/finals/CommonFinals$MMSourceType;

    sget-object v1, Lcom/konka/mm/finals/CommonFinals$MMSourceType;->KONKA_MM_SOURCE_TXT:Lcom/konka/mm/finals/CommonFinals$MMSourceType;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/konka/mm/modules/ModulesActivity$13;->this$0:Lcom/konka/mm/modules/ModulesActivity;

    iget-object v0, v0, Lcom/konka/mm/modules/ModulesActivity;->mSourceType:Lcom/konka/mm/finals/CommonFinals$MMSourceType;

    sget-object v1, Lcom/konka/mm/finals/CommonFinals$MMSourceType;->KONKA_MM_SOURCE_IMAGE:Lcom/konka/mm/finals/CommonFinals$MMSourceType;

    if-eq v0, v1, :cond_0

    sget-object v0, Lcom/konka/mm/finals/CommonFinals$MMSourceType;->KONKA_MM_SOURCE_AUDIO:Lcom/konka/mm/finals/CommonFinals$MMSourceType;

    :cond_0
    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    return-void
.end method
