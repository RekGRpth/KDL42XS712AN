.class Lcom/konka/mm/filemanager/FileListActivity$PathBtnClickListener;
.super Ljava/lang/Object;
.source "FileListActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/mm/filemanager/FileListActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PathBtnClickListener"
.end annotation


# instance fields
.field private pathItem:Lcom/konka/mm/model/PathItem;

.field final synthetic this$0:Lcom/konka/mm/filemanager/FileListActivity;


# direct methods
.method public constructor <init>(Lcom/konka/mm/filemanager/FileListActivity;Lcom/konka/mm/model/PathItem;)V
    .locals 0
    .param p2    # Lcom/konka/mm/model/PathItem;

    iput-object p1, p0, Lcom/konka/mm/filemanager/FileListActivity$PathBtnClickListener;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/konka/mm/filemanager/FileListActivity$PathBtnClickListener;->pathItem:Lcom/konka/mm/model/PathItem;

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity$PathBtnClickListener;->pathItem:Lcom/konka/mm/model/PathItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity$PathBtnClickListener;->pathItem:Lcom/konka/mm/model/PathItem;

    invoke-virtual {v0}, Lcom/konka/mm/model/PathItem;->getPath()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity$PathBtnClickListener;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    # getter for: Lcom/konka/mm/filemanager/FileListActivity;->curFile:Ljava/io/File;
    invoke-static {v1}, Lcom/konka/mm/filemanager/FileListActivity;->access$13(Lcom/konka/mm/filemanager/FileListActivity;)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity$PathBtnClickListener;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    iget-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity$PathBtnClickListener;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    iget-object v2, p0, Lcom/konka/mm/filemanager/FileListActivity$PathBtnClickListener;->pathItem:Lcom/konka/mm/model/PathItem;

    # invokes: Lcom/konka/mm/filemanager/FileListActivity;->getPathIndexInList(Lcom/konka/mm/model/PathItem;)I
    invoke-static {v1, v2}, Lcom/konka/mm/filemanager/FileListActivity;->access$27(Lcom/konka/mm/filemanager/FileListActivity;Lcom/konka/mm/model/PathItem;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/konka/mm/filemanager/FileListActivity;->access$28(Lcom/konka/mm/filemanager/FileListActivity;I)V

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity$PathBtnClickListener;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    iget-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity$PathBtnClickListener;->pathItem:Lcom/konka/mm/model/PathItem;

    invoke-virtual {v1}, Lcom/konka/mm/model/PathItem;->getPath()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Lcom/konka/mm/filemanager/FileListActivity;->broweTo(Ljava/lang/String;I)V

    :cond_0
    return-void
.end method
