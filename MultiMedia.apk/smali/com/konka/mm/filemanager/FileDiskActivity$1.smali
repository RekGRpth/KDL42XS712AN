.class Lcom/konka/mm/filemanager/FileDiskActivity$1;
.super Ljava/lang/Object;
.source "FileDiskActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/mm/filemanager/FileDiskActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/mm/filemanager/FileDiskActivity;


# direct methods
.method constructor <init>(Lcom/konka/mm/filemanager/FileDiskActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/filemanager/FileDiskActivity$1;->this$0:Lcom/konka/mm/filemanager/FileDiskActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    const/4 v5, 0x0

    iget-object v3, p0, Lcom/konka/mm/filemanager/FileDiskActivity$1;->this$0:Lcom/konka/mm/filemanager/FileDiskActivity;

    invoke-static {v3, v5}, Lcom/konka/mm/filemanager/FileDiskActivity;->access$0(Lcom/konka/mm/filemanager/FileDiskActivity;Z)V

    iget-object v3, p0, Lcom/konka/mm/filemanager/FileDiskActivity$1;->this$0:Lcom/konka/mm/filemanager/FileDiskActivity;

    # getter for: Lcom/konka/mm/filemanager/FileDiskActivity;->usbs:Ljava/util/List;
    invoke-static {v3}, Lcom/konka/mm/filemanager/FileDiskActivity;->access$1(Lcom/konka/mm/filemanager/FileDiskActivity;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    # getter for: Lcom/konka/mm/filemanager/FileDiskActivity;->diskRoot:Ljava/lang/String;
    invoke-static {}, Lcom/konka/mm/filemanager/FileDiskActivity;->access$2()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    sget-object v3, Lcom/konka/mm/filemanager/FileListActivity;->instance:Lcom/konka/mm/filemanager/FileListActivity;

    if-eqz v3, :cond_0

    sget-object v3, Lcom/konka/mm/filemanager/FileListActivity;->instance:Lcom/konka/mm/filemanager/FileListActivity;

    invoke-virtual {v3}, Lcom/konka/mm/filemanager/FileListActivity;->finish()V

    :cond_0
    iget-object v3, p0, Lcom/konka/mm/filemanager/FileDiskActivity$1;->this$0:Lcom/konka/mm/filemanager/FileDiskActivity;

    # getter for: Lcom/konka/mm/filemanager/FileDiskActivity;->usbs:Ljava/util/List;
    invoke-static {v3}, Lcom/konka/mm/filemanager/FileDiskActivity;->access$1(Lcom/konka/mm/filemanager/FileDiskActivity;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v3}, Lcom/konka/mm/filemanager/FileDiskActivity;->access$3(Ljava/lang/String;)V

    if-nez v2, :cond_1

    :goto_0
    return-void

    :cond_1
    iget-object v4, p0, Lcom/konka/mm/filemanager/FileDiskActivity$1;->this$0:Lcom/konka/mm/filemanager/FileDiskActivity;

    iget-object v3, p0, Lcom/konka/mm/filemanager/FileDiskActivity$1;->this$0:Lcom/konka/mm/filemanager/FileDiskActivity;

    # getter for: Lcom/konka/mm/filemanager/FileDiskActivity;->diskName:Ljava/util/List;
    invoke-static {v3}, Lcom/konka/mm/filemanager/FileDiskActivity;->access$4(Lcom/konka/mm/filemanager/FileDiskActivity;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v4, v3}, Lcom/konka/mm/filemanager/FileDiskActivity;->access$5(Lcom/konka/mm/filemanager/FileDiskActivity;Ljava/lang/String;)V

    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v3

    array-length v3, v3

    if-gtz v3, :cond_2

    iget-object v3, p0, Lcom/konka/mm/filemanager/FileDiskActivity$1;->this$0:Lcom/konka/mm/filemanager/FileDiskActivity;

    const v4, 0x7f09009c    # com.konka.mm.R.string.disk_path_null

    invoke-static {v3, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_2
    iget-object v3, p0, Lcom/konka/mm/filemanager/FileDiskActivity$1;->this$0:Lcom/konka/mm/filemanager/FileDiskActivity;

    iget-object v4, p0, Lcom/konka/mm/filemanager/FileDiskActivity$1;->this$0:Lcom/konka/mm/filemanager/FileDiskActivity;

    # getter for: Lcom/konka/mm/filemanager/FileDiskActivity;->curDiskName:Ljava/lang/String;
    invoke-static {v4}, Lcom/konka/mm/filemanager/FileDiskActivity;->access$6(Lcom/konka/mm/filemanager/FileDiskActivity;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/konka/mm/filemanager/FileDiskActivity;->initDiskInfo(Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    iget-object v3, p0, Lcom/konka/mm/filemanager/FileDiskActivity$1;->this$0:Lcom/konka/mm/filemanager/FileDiskActivity;

    const-class v4, Lcom/konka/mm/filemanager/FileListActivity;

    invoke-direct {v0, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v3, "root_path"

    invoke-virtual {v0, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v3, "diskName"

    iget-object v4, p0, Lcom/konka/mm/filemanager/FileDiskActivity$1;->this$0:Lcom/konka/mm/filemanager/FileDiskActivity;

    # getter for: Lcom/konka/mm/filemanager/FileDiskActivity;->curDiskName:Ljava/lang/String;
    invoke-static {v4}, Lcom/konka/mm/filemanager/FileDiskActivity;->access$6(Lcom/konka/mm/filemanager/FileDiskActivity;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v3, p0, Lcom/konka/mm/filemanager/FileDiskActivity$1;->this$0:Lcom/konka/mm/filemanager/FileDiskActivity;

    invoke-virtual {v3, v0}, Lcom/konka/mm/filemanager/FileDiskActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
