.class Lcom/konka/mm/filemanager/FileItemClickListener$4;
.super Ljava/lang/Object;
.source "FileItemClickListener.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/mm/filemanager/FileItemClickListener;->init()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/mm/filemanager/FileItemClickListener;


# direct methods
.method constructor <init>(Lcom/konka/mm/filemanager/FileItemClickListener;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/filemanager/FileItemClickListener$4;->this$0:Lcom/konka/mm/filemanager/FileItemClickListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 7
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    const/16 v6, 0xd

    iget-object v3, p0, Lcom/konka/mm/filemanager/FileItemClickListener$4;->this$0:Lcom/konka/mm/filemanager/FileItemClickListener;

    iget-object v3, v3, Lcom/konka/mm/filemanager/FileItemClickListener;->inputEdit:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-interface {v3}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/konka/mm/filemanager/FileItemClickListener$4;->this$0:Lcom/konka/mm/filemanager/FileItemClickListener;

    iget-object v3, v3, Lcom/konka/mm/filemanager/FileItemClickListener;->type:Ljava/lang/String;

    const-string v4, "dir"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/konka/mm/filemanager/FileItemClickListener$4;->this$0:Lcom/konka/mm/filemanager/FileItemClickListener;

    new-instance v4, Ljava/lang/StringBuilder;

    iget-object v5, p0, Lcom/konka/mm/filemanager/FileItemClickListener$4;->this$0:Lcom/konka/mm/filemanager/FileItemClickListener;

    iget-object v5, v5, Lcom/konka/mm/filemanager/FileItemClickListener;->fileListActivity:Lcom/konka/mm/filemanager/FileListActivity;

    iget-object v5, v5, Lcom/konka/mm/filemanager/FileListActivity;->parentFile:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/konka/mm/filemanager/FileItemClickListener;->renamePath:Ljava/lang/String;

    :goto_0
    iget-object v3, p0, Lcom/konka/mm/filemanager/FileItemClickListener$4;->this$0:Lcom/konka/mm/filemanager/FileItemClickListener;

    iget-object v3, v3, Lcom/konka/mm/filemanager/FileItemClickListener;->renamePath:Ljava/lang/String;

    iget-object v4, p0, Lcom/konka/mm/filemanager/FileItemClickListener$4;->this$0:Lcom/konka/mm/filemanager/FileItemClickListener;

    iget-object v4, v4, Lcom/konka/mm/filemanager/FileItemClickListener;->absPath:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    :goto_1
    return-void

    :cond_0
    iget-object v3, p0, Lcom/konka/mm/filemanager/FileItemClickListener$4;->this$0:Lcom/konka/mm/filemanager/FileItemClickListener;

    new-instance v4, Ljava/lang/StringBuilder;

    iget-object v5, p0, Lcom/konka/mm/filemanager/FileItemClickListener$4;->this$0:Lcom/konka/mm/filemanager/FileItemClickListener;

    iget-object v5, v5, Lcom/konka/mm/filemanager/FileItemClickListener;->fileListActivity:Lcom/konka/mm/filemanager/FileListActivity;

    iget-object v5, v5, Lcom/konka/mm/filemanager/FileListActivity;->parentFile:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/konka/mm/filemanager/FileItemClickListener$4;->this$0:Lcom/konka/mm/filemanager/FileItemClickListener;

    iget-object v5, v5, Lcom/konka/mm/filemanager/FileItemClickListener;->type:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/konka/mm/filemanager/FileItemClickListener;->renamePath:Ljava/lang/String;

    goto :goto_0

    :cond_1
    new-instance v1, Ljava/io/File;

    iget-object v3, p0, Lcom/konka/mm/filemanager/FileItemClickListener$4;->this$0:Lcom/konka/mm/filemanager/FileItemClickListener;

    iget-object v3, v3, Lcom/konka/mm/filemanager/FileItemClickListener;->renamePath:Ljava/lang/String;

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/konka/mm/filemanager/FileItemClickListener$4;->this$0:Lcom/konka/mm/filemanager/FileItemClickListener;

    iget-object v3, v3, Lcom/konka/mm/filemanager/FileItemClickListener;->fileListActivity:Lcom/konka/mm/filemanager/FileListActivity;

    iget-object v3, v3, Lcom/konka/mm/filemanager/FileListActivity;->listViewHandler:Landroid/os/Handler;

    invoke-virtual {v3}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    iput v6, v0, Landroid/os/Message;->what:I

    const-string v3, "enpty_file_name"

    iput-object v3, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    iget-object v3, p0, Lcom/konka/mm/filemanager/FileItemClickListener$4;->this$0:Lcom/konka/mm/filemanager/FileItemClickListener;

    iget-object v3, v3, Lcom/konka/mm/filemanager/FileItemClickListener;->fileListActivity:Lcom/konka/mm/filemanager/FileListActivity;

    iget-object v3, v3, Lcom/konka/mm/filemanager/FileListActivity;->listViewHandler:Landroid/os/Handler;

    invoke-virtual {v3, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_1

    :cond_2
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/konka/mm/filemanager/FileItemClickListener$4;->this$0:Lcom/konka/mm/filemanager/FileItemClickListener;

    iget-object v3, v3, Lcom/konka/mm/filemanager/FileItemClickListener;->fileListActivity:Lcom/konka/mm/filemanager/FileListActivity;

    iget-object v3, v3, Lcom/konka/mm/filemanager/FileListActivity;->listViewHandler:Landroid/os/Handler;

    invoke-virtual {v3}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    iput v6, v0, Landroid/os/Message;->what:I

    const-string v3, "has_same_name"

    iput-object v3, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    iget-object v3, p0, Lcom/konka/mm/filemanager/FileItemClickListener$4;->this$0:Lcom/konka/mm/filemanager/FileItemClickListener;

    iget-object v3, v3, Lcom/konka/mm/filemanager/FileItemClickListener;->fileListActivity:Lcom/konka/mm/filemanager/FileListActivity;

    iget-object v3, v3, Lcom/konka/mm/filemanager/FileListActivity;->listViewHandler:Landroid/os/Handler;

    invoke-virtual {v3, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_1

    :cond_3
    iget-object v3, p0, Lcom/konka/mm/filemanager/FileItemClickListener$4;->this$0:Lcom/konka/mm/filemanager/FileItemClickListener;

    # invokes: Lcom/konka/mm/filemanager/FileItemClickListener;->doRename()V
    invoke-static {v3}, Lcom/konka/mm/filemanager/FileItemClickListener;->access$6(Lcom/konka/mm/filemanager/FileItemClickListener;)V

    goto/16 :goto_1
.end method
