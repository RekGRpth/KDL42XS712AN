.class Lcom/konka/mm/filemanager/FileItemClickListener$3;
.super Ljava/lang/Object;
.source "FileItemClickListener.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/mm/filemanager/FileItemClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/mm/filemanager/FileItemClickListener;


# direct methods
.method constructor <init>(Lcom/konka/mm/filemanager/FileItemClickListener;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/filemanager/FileItemClickListener$3;->this$0:Lcom/konka/mm/filemanager/FileItemClickListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    const/4 v2, 0x0

    packed-switch p2, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    check-cast p1, Landroid/app/Dialog;

    invoke-virtual {p1}, Landroid/app/Dialog;->hide()V

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileItemClickListener$3;->this$0:Lcom/konka/mm/filemanager/FileItemClickListener;

    # getter for: Lcom/konka/mm/filemanager/FileItemClickListener;->copyFileService:Lcom/konka/mm/services/CopyFileService;
    invoke-static {v0}, Lcom/konka/mm/filemanager/FileItemClickListener;->access$1(Lcom/konka/mm/filemanager/FileItemClickListener;)Lcom/konka/mm/services/CopyFileService;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/konka/mm/services/CopyFileService;->setHidden(Z)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/konka/mm/filemanager/FileItemClickListener$3;->this$0:Lcom/konka/mm/filemanager/FileItemClickListener;

    # getter for: Lcom/konka/mm/filemanager/FileItemClickListener;->copyFileService:Lcom/konka/mm/services/CopyFileService;
    invoke-static {v0}, Lcom/konka/mm/filemanager/FileItemClickListener;->access$1(Lcom/konka/mm/filemanager/FileItemClickListener;)Lcom/konka/mm/services/CopyFileService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/mm/services/CopyFileService;->cancelCopy()V

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileItemClickListener$3;->this$0:Lcom/konka/mm/filemanager/FileItemClickListener;

    iget-object v0, v0, Lcom/konka/mm/filemanager/FileItemClickListener;->fileListActivity:Lcom/konka/mm/filemanager/FileListActivity;

    const-string v1, "\u6b63\u5728\u53d6\u6d88\u590d\u5236"

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileItemClickListener$3;->this$0:Lcom/konka/mm/filemanager/FileItemClickListener;

    invoke-static {v0, v2}, Lcom/konka/mm/filemanager/FileItemClickListener;->access$5(Lcom/konka/mm/filemanager/FileItemClickListener;Z)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method
