.class public Lcom/konka/mm/filemanager/FileOperation;
.super Ljava/lang/Object;
.source "FileOperation.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getDirectorySize(Ljava/io/File;)J
    .locals 7
    .param p0    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-wide/16 v3, 0x0

    invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Ljava/io/File;->length()J

    move-result-wide v5

    :goto_0
    return-wide v5

    :cond_0
    array-length v2, v0

    const/4 v1, 0x0

    :goto_1
    if-lt v1, v2, :cond_1

    move-wide v5, v3

    goto :goto_0

    :cond_1
    aget-object v5, v0, v1

    invoke-virtual {v5}, Ljava/io/File;->isDirectory()Z

    move-result v5

    if-eqz v5, :cond_2

    aget-object v5, v0, v1

    invoke-static {v5}, Lcom/konka/mm/filemanager/FileOperation;->getDirectorySize(Ljava/io/File;)J

    move-result-wide v5

    add-long/2addr v3, v5

    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    aget-object v5, v0, v1

    invoke-virtual {v5}, Ljava/io/File;->length()J

    move-result-wide v5

    add-long/2addr v3, v5

    goto :goto_2
.end method

.method public static getDirectorySize(Ljava/lang/String;)J
    .locals 8
    .param p0    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-wide/16 v4, 0x0

    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v6

    :goto_0
    return-wide v6

    :cond_0
    array-length v3, v1

    const/4 v2, 0x0

    :goto_1
    if-lt v2, v3, :cond_1

    move-wide v6, v4

    goto :goto_0

    :cond_1
    aget-object v6, v1, v2

    invoke-virtual {v6}, Ljava/io/File;->isDirectory()Z

    move-result v6

    if-eqz v6, :cond_2

    aget-object v6, v1, v2

    invoke-static {v6}, Lcom/konka/mm/filemanager/FileOperation;->getDirectorySize(Ljava/io/File;)J

    move-result-wide v6

    add-long/2addr v4, v6

    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    aget-object v6, v1, v2

    invoke-virtual {v6}, Ljava/io/File;->length()J

    move-result-wide v6

    add-long/2addr v4, v6

    goto :goto_2
.end method
