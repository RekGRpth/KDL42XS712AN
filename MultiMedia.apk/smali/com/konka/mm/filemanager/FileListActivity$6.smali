.class Lcom/konka/mm/filemanager/FileListActivity$6;
.super Ljava/lang/Object;
.source "FileListActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/mm/filemanager/FileListActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/mm/filemanager/FileListActivity;


# direct methods
.method constructor <init>(Lcom/konka/mm/filemanager/FileListActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/filemanager/FileListActivity$6;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/konka/mm/filemanager/FileListActivity$6;)Lcom/konka/mm/filemanager/FileListActivity;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity$6;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    return-object v0
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 26
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/mm/filemanager/FileListActivity$6;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    move/from16 v0, p3

    iput v0, v2, Lcom/konka/mm/filemanager/FileListActivity;->curSelectedPos:I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/mm/filemanager/FileListActivity$6;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/konka/mm/filemanager/FileListActivity$6;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    move/from16 v0, p3

    # invokes: Lcom/konka/mm/filemanager/FileListActivity;->getCurFile(I)Ljava/io/File;
    invoke-static {v3, v0}, Lcom/konka/mm/filemanager/FileListActivity;->access$18(Lcom/konka/mm/filemanager/FileListActivity;I)Ljava/io/File;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/konka/mm/filemanager/FileListActivity;->access$19(Lcom/konka/mm/filemanager/FileListActivity;Ljava/io/File;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/mm/filemanager/FileListActivity$6;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    # getter for: Lcom/konka/mm/filemanager/FileListActivity;->curFile:Ljava/io/File;
    invoke-static {v2}, Lcom/konka/mm/filemanager/FileListActivity;->access$13(Lcom/konka/mm/filemanager/FileListActivity;)Ljava/io/File;

    move-result-object v2

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/mm/filemanager/FileListActivity$6;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    iget-boolean v2, v2, Lcom/konka/mm/filemanager/FileListActivity;->isManager:Z

    if-nez v2, :cond_c

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/mm/filemanager/FileListActivity$6;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    iget-boolean v2, v2, Lcom/konka/mm/filemanager/FileListActivity;->isRenameManager:Z

    if-eqz v2, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/mm/filemanager/FileListActivity$6;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/konka/mm/filemanager/FileListActivity$6;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    # getter for: Lcom/konka/mm/filemanager/FileListActivity;->curFile:Ljava/io/File;
    invoke-static {v3}, Lcom/konka/mm/filemanager/FileListActivity;->access$13(Lcom/konka/mm/filemanager/FileListActivity;)Ljava/io/File;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/konka/mm/filemanager/FileListActivity;->isCanOperate(Ljava/io/File;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v2, "FileListActivity"

    const-string v3, "\u8fdb\u5165\u4e86\u91cd\u547d\u540d"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/mm/filemanager/FileListActivity$6;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/konka/mm/filemanager/FileListActivity$6;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    # getter for: Lcom/konka/mm/filemanager/FileListActivity;->curFile:Ljava/io/File;
    invoke-static {v3}, Lcom/konka/mm/filemanager/FileListActivity;->access$13(Lcom/konka/mm/filemanager/FileListActivity;)Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    const/4 v5, 0x5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/konka/mm/filemanager/FileListActivity$6;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    # getter for: Lcom/konka/mm/filemanager/FileListActivity;->curFile:Ljava/io/File;
    invoke-static {v6}, Lcom/konka/mm/filemanager/FileListActivity;->access$13(Lcom/konka/mm/filemanager/FileListActivity;)Ljava/io/File;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/konka/mm/filemanager/FileListActivity$6;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    iget v7, v7, Lcom/konka/mm/filemanager/FileListActivity;->mScreen_mode:I

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/konka/mm/filemanager/FileListActivity$6;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    iget v8, v8, Lcom/konka/mm/filemanager/FileListActivity;->currentPage:I

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/konka/mm/filemanager/FileListActivity$6;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    iget v9, v9, Lcom/konka/mm/filemanager/FileListActivity;->curSelectedPos:I

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/konka/mm/filemanager/FileListActivity$6;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    iget v10, v10, Lcom/konka/mm/filemanager/FileListActivity;->mCountPerPage:I

    invoke-static/range {v2 .. v10}, Lcom/konka/mm/finals/CommonFinals;->quitActivityByPopup(Landroid/app/Activity;Ljava/lang/String;IILjava/io/File;IIII)V

    goto :goto_0

    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/mm/filemanager/FileListActivity$6;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    invoke-virtual {v2}, Lcom/konka/mm/filemanager/FileListActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v17

    check-cast v17, Lcom/konka/mm/GlobalData;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/mm/filemanager/FileListActivity$6;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    # getter for: Lcom/konka/mm/filemanager/FileListActivity;->curFile:Ljava/io/File;
    invoke-static {v2}, Lcom/konka/mm/filemanager/FileListActivity;->access$13(Lcom/konka/mm/filemanager/FileListActivity;)Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->isDirectory()Z

    move-result v2

    if-eqz v2, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/mm/filemanager/FileListActivity$6;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    # getter for: Lcom/konka/mm/filemanager/FileListActivity;->curFile:Ljava/io/File;
    invoke-static {v2}, Lcom/konka/mm/filemanager/FileListActivity;->access$13(Lcom/konka/mm/filemanager/FileListActivity;)Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/mm/filemanager/FileListActivity$6;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    const/4 v3, 0x1

    move-object/from16 v0, v22

    invoke-virtual {v2, v0, v3}, Lcom/konka/mm/filemanager/FileListActivity;->broweTo(Ljava/lang/String;I)V

    goto :goto_0

    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/mm/filemanager/FileListActivity$6;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    # getter for: Lcom/konka/mm/filemanager/FileListActivity;->curFile:Ljava/io/File;
    invoke-static {v2}, Lcom/konka/mm/filemanager/FileListActivity;->access$13(Lcom/konka/mm/filemanager/FileListActivity;)Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/konka/mm/filemanager/FileListActivity$6;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    invoke-virtual {v3}, Lcom/konka/mm/filemanager/FileListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f070001    # com.konka.mm.R.array.fileEndingAudio

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/konka/mm/tools/FileTool;->checkEndsWithInStringArray(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/mm/filemanager/FileListActivity$6;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    # getter for: Lcom/konka/mm/filemanager/FileListActivity;->curFile:Ljava/io/File;
    invoke-static {v2}, Lcom/konka/mm/filemanager/FileListActivity;->access$13(Lcom/konka/mm/filemanager/FileListActivity;)Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v2

    new-instance v3, Lcom/konka/mm/filemanager/FileListActivity$6$1;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/konka/mm/filemanager/FileListActivity$6$1;-><init>(Lcom/konka/mm/filemanager/FileListActivity$6;)V

    invoke-virtual {v2, v3}, Ljava/io/File;->listFiles(Ljava/io/FileFilter;)[Ljava/io/File;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/mm/filemanager/FileListActivity$6;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    # invokes: Lcom/konka/mm/filemanager/FileListActivity;->sortFiles([Ljava/io/File;)[Ljava/io/File;
    invoke-static {v2, v13}, Lcom/konka/mm/filemanager/FileListActivity;->access$20(Lcom/konka/mm/filemanager/FileListActivity;[Ljava/io/File;)[Ljava/io/File;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/mm/filemanager/FileListActivity$6;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    # getter for: Lcom/konka/mm/filemanager/FileListActivity;->curFile:Ljava/io/File;
    invoke-static {v2}, Lcom/konka/mm/filemanager/FileListActivity;->access$13(Lcom/konka/mm/filemanager/FileListActivity;)Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/konka/mm/filemanager/FileListActivity;->PLAYING_MUSIC_PATH:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/mm/filemanager/FileListActivity$6;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    # invokes: Lcom/konka/mm/filemanager/FileListActivity;->files2Strings([Ljava/io/File;)Ljava/util/ArrayList;
    invoke-static {v2, v13}, Lcom/konka/mm/filemanager/FileListActivity;->access$21(Lcom/konka/mm/filemanager/FileListActivity;[Ljava/io/File;)Ljava/util/ArrayList;

    move-result-object v20

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/konka/mm/GlobalData;->setmMMFileList(Ljava/util/ArrayList;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/mm/filemanager/FileListActivity$6;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    sget-object v3, Lcom/konka/mm/filemanager/FileListActivity;->PLAYING_MUSIC_PATH:Ljava/lang/String;

    move-object/from16 v0, v20

    invoke-virtual {v2, v3, v0}, Lcom/konka/mm/filemanager/FileListActivity;->getListCurPos(Ljava/lang/String;Ljava/util/ArrayList;)I

    move-result v19

    new-instance v15, Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/mm/filemanager/FileListActivity$6;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    const-class v3, Lcom/konka/mm/music/MusicActivity;

    invoke-direct {v15, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    new-instance v11, Landroid/os/Bundle;

    invoke-direct {v11}, Landroid/os/Bundle;-><init>()V

    const-string v2, "com.konka.mm.file.root.path"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/konka/mm/filemanager/FileListActivity$6;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    # getter for: Lcom/konka/mm/filemanager/FileListActivity;->root_path:Ljava/lang/String;
    invoke-static {v3}, Lcom/konka/mm/filemanager/FileListActivity;->access$22(Lcom/konka/mm/filemanager/FileListActivity;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v11, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "com.konka.mm.file.where.come.from"

    const-string v3, "com.konka.mm.file.come.from.filemanager"

    invoke-virtual {v11, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "com.konka.mm.file.index.posstion"

    move/from16 v0, v19

    invoke-virtual {v11, v2, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v15, v11}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/mm/filemanager/FileListActivity$6;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    invoke-virtual {v2, v15}, Lcom/konka/mm/filemanager/FileListActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/mm/filemanager/FileListActivity$6;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    # getter for: Lcom/konka/mm/filemanager/FileListActivity;->curFile:Ljava/io/File;
    invoke-static {v2}, Lcom/konka/mm/filemanager/FileListActivity;->access$13(Lcom/konka/mm/filemanager/FileListActivity;)Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/konka/mm/filemanager/FileListActivity$6;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    invoke-virtual {v3}, Lcom/konka/mm/filemanager/FileListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f070002    # com.konka.mm.R.array.fileEndingVedio

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/konka/mm/tools/FileTool;->checkEndsWithInStringArray(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/mm/filemanager/FileListActivity$6;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    invoke-static {v2}, Lcom/konka/mm/tools/FileTool;->killRunningServiceInfo(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/mm/filemanager/FileListActivity$6;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    # getter for: Lcom/konka/mm/filemanager/FileListActivity;->curFile:Ljava/io/File;
    invoke-static {v2}, Lcom/konka/mm/filemanager/FileListActivity;->access$13(Lcom/konka/mm/filemanager/FileListActivity;)Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/mm/filemanager/FileListActivity$6;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    # getter for: Lcom/konka/mm/filemanager/FileListActivity;->curFile:Ljava/io/File;
    invoke-static {v2}, Lcom/konka/mm/filemanager/FileListActivity;->access$13(Lcom/konka/mm/filemanager/FileListActivity;)Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v25

    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    const/4 v14, 0x0

    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/mm/filemanager/FileListActivity$6;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    iget-object v2, v2, Lcom/konka/mm/filemanager/FileListActivity;->infos:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lt v14, v2, :cond_5

    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "the movie play list:"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    const/16 v16, 0x0

    :goto_2
    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->size()I

    move-result v2

    move/from16 v0, v16

    if-lt v0, v2, :cond_7

    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "current playing movie path: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v25

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :try_start_0
    new-instance v15, Landroid/content/Intent;

    invoke-direct {v15}, Landroid/content/Intent;-><init>()V

    const-string v2, "com.konka.kkvideoplayer"

    const-string v3, "com.konka.kkvideoplayer.VideoPlayerMain"

    invoke-virtual {v15, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    new-instance v11, Landroid/os/Bundle;

    invoke-direct {v11}, Landroid/os/Bundle;-><init>()V

    const-string v2, "videofile"

    move-object/from16 v0, v25

    invoke-virtual {v11, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "com.konka.mm.movie.list"

    move-object/from16 v0, v18

    invoke-virtual {v11, v2, v0}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    invoke-virtual {v15, v11}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    const-string v2, "KONKA_MM"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "------>goto movie theater["

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v25

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/mm/filemanager/FileListActivity$6;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    invoke-virtual {v2, v15}, Lcom/konka/mm/filemanager/FileListActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v12

    const-string v2, "KONKA_MM"

    invoke-virtual {v12}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/mm/filemanager/FileListActivity$6;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    iget-object v2, v2, Lcom/konka/mm/filemanager/FileListActivity;->infos:Ljava/util/List;

    invoke-interface {v2, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/konka/mm/filemanager/FileListActivity$6;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    invoke-virtual {v3}, Lcom/konka/mm/filemanager/FileListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f070002    # com.konka.mm.R.array.fileEndingVedio

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/konka/mm/tools/FileTool;->checkEndsWithInStringArray(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/mm/filemanager/FileListActivity$6;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    iget-object v2, v2, Lcom/konka/mm/filemanager/FileListActivity;->infos:Ljava/util/List;

    invoke-interface {v2, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_6
    add-int/lit8 v14, v14, 0x1

    goto/16 :goto_1

    :cond_7
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    add-int/lit8 v4, v16, 0x1

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, ":  "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v18

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    add-int/lit8 v16, v16, 0x1

    goto/16 :goto_2

    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/mm/filemanager/FileListActivity$6;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    # getter for: Lcom/konka/mm/filemanager/FileListActivity;->curFile:Ljava/io/File;
    invoke-static {v2}, Lcom/konka/mm/filemanager/FileListActivity;->access$13(Lcom/konka/mm/filemanager/FileListActivity;)Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/konka/mm/filemanager/FileListActivity$6;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    invoke-virtual {v3}, Lcom/konka/mm/filemanager/FileListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const/high16 v4, 0x7f070000    # com.konka.mm.R.array.fileEndingImage

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/konka/mm/tools/FileTool;->checkEndsWithInStringArray(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_b

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/mm/filemanager/FileListActivity$6;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/konka/mm/filemanager/FileListActivity;->access$23(Lcom/konka/mm/filemanager/FileListActivity;Z)V

    new-instance v24, Ljava/util/ArrayList;

    invoke-direct/range {v24 .. v24}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, v17

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/konka/mm/GlobalData;->setmMMFileList(Ljava/util/ArrayList;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/mm/filemanager/FileListActivity$6;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    # getter for: Lcom/konka/mm/filemanager/FileListActivity;->curFile:Ljava/io/File;
    invoke-static {v2}, Lcom/konka/mm/filemanager/FileListActivity;->access$13(Lcom/konka/mm/filemanager/FileListActivity;)Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/mm/filemanager/FileListActivity$6;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    # getter for: Lcom/konka/mm/filemanager/FileListActivity;->curFile:Ljava/io/File;
    invoke-static {v2}, Lcom/konka/mm/filemanager/FileListActivity;->access$13(Lcom/konka/mm/filemanager/FileListActivity;)Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v25

    const/4 v14, 0x0

    :goto_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/mm/filemanager/FileListActivity$6;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    iget-object v2, v2, Lcom/konka/mm/filemanager/FileListActivity;->infos:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lt v14, v2, :cond_9

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/mm/filemanager/FileListActivity$6;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/konka/mm/filemanager/FileListActivity$6;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    # getter for: Lcom/konka/mm/filemanager/FileListActivity;->curFile:Ljava/io/File;
    invoke-static {v3}, Lcom/konka/mm/filemanager/FileListActivity;->access$13(Lcom/konka/mm/filemanager/FileListActivity;)Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v24

    invoke-virtual {v2, v3, v0}, Lcom/konka/mm/filemanager/FileListActivity;->getListCurPos(Ljava/lang/String;Ljava/util/ArrayList;)I

    move-result v23

    new-instance v15, Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/mm/filemanager/FileListActivity$6;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    const-class v3, Lcom/konka/mm/photo/AutoShowPicActivity;

    invoke-direct {v15, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    new-instance v11, Landroid/os/Bundle;

    invoke-direct {v11}, Landroid/os/Bundle;-><init>()V

    const-string v2, "com.konka.mm.file.parent.path"

    move-object/from16 v0, v21

    invoke-virtual {v11, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "com.konka.mm.file.index.posstion"

    move/from16 v0, v23

    invoke-virtual {v11, v2, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "com.konka.mm.file.root.path"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/konka/mm/filemanager/FileListActivity$6;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    # getter for: Lcom/konka/mm/filemanager/FileListActivity;->root_path:Ljava/lang/String;
    invoke-static {v3}, Lcom/konka/mm/filemanager/FileListActivity;->access$22(Lcom/konka/mm/filemanager/FileListActivity;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v11, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "com.konka.mm.file.where.come.from"

    const-string v3, "com.konka.mm.file.come.from.filemanager"

    invoke-virtual {v11, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v15, v11}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/mm/filemanager/FileListActivity$6;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    invoke-virtual {v2, v15}, Lcom/konka/mm/filemanager/FileListActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/mm/filemanager/FileListActivity$6;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    iget-object v2, v2, Lcom/konka/mm/filemanager/FileListActivity;->infos:Ljava/util/List;

    invoke-interface {v2, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/konka/mm/filemanager/FileListActivity$6;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    invoke-virtual {v3}, Lcom/konka/mm/filemanager/FileListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const/high16 v4, 0x7f070000    # com.konka.mm.R.array.fileEndingImage

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/konka/mm/tools/FileTool;->checkEndsWithInStringArray(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_a

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/mm/filemanager/FileListActivity$6;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    iget-object v2, v2, Lcom/konka/mm/filemanager/FileListActivity;->infos:Ljava/util/List;

    invoke-interface {v2, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v24

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_a
    add-int/lit8 v14, v14, 0x1

    goto/16 :goto_3

    :cond_b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/mm/filemanager/FileListActivity$6;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    # getter for: Lcom/konka/mm/filemanager/FileListActivity;->curFile:Ljava/io/File;
    invoke-static {v2}, Lcom/konka/mm/filemanager/FileListActivity;->access$13(Lcom/konka/mm/filemanager/FileListActivity;)Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/konka/mm/filemanager/FileListActivity$6;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    invoke-virtual {v3}, Lcom/konka/mm/filemanager/FileListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f070003    # com.konka.mm.R.array.fileEndingApk

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/konka/mm/tools/FileTool;->checkEndsWithInStringArray(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    new-instance v15, Landroid/content/Intent;

    invoke-direct {v15}, Landroid/content/Intent;-><init>()V

    const-string v2, "android.intent.action.VIEW"

    invoke-virtual {v15, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "file://"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/konka/mm/filemanager/FileListActivity$6;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    # getter for: Lcom/konka/mm/filemanager/FileListActivity;->curFile:Ljava/io/File;
    invoke-static {v3}, Lcom/konka/mm/filemanager/FileListActivity;->access$13(Lcom/konka/mm/filemanager/FileListActivity;)Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const-string v3, "application/vnd.android.package-archive"

    invoke-virtual {v15, v2, v3}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v2, 0x10000000

    invoke-virtual {v15, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/mm/filemanager/FileListActivity$6;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    invoke-virtual {v2, v15}, Lcom/konka/mm/filemanager/FileListActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/mm/filemanager/FileListActivity$6;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    iget-boolean v2, v2, Lcom/konka/mm/filemanager/FileListActivity;->isManager:Z

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/mm/filemanager/FileListActivity$6;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/konka/mm/filemanager/FileListActivity$6;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    # getter for: Lcom/konka/mm/filemanager/FileListActivity;->curFile:Ljava/io/File;
    invoke-static {v3}, Lcom/konka/mm/filemanager/FileListActivity;->access$13(Lcom/konka/mm/filemanager/FileListActivity;)Ljava/io/File;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/konka/mm/filemanager/FileListActivity;->isCanOperate(Ljava/io/File;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "FileListActivity"

    const-string v3, "\u8fdb\u5165\u4e86\u7f16\u8f91\u72b6\u6001"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/mm/filemanager/FileListActivity$6;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/konka/mm/filemanager/FileListActivity$6;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    # getter for: Lcom/konka/mm/filemanager/FileListActivity;->curFile:Ljava/io/File;
    invoke-static {v3}, Lcom/konka/mm/filemanager/FileListActivity;->access$13(Lcom/konka/mm/filemanager/FileListActivity;)Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    const/4 v5, 0x1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/konka/mm/filemanager/FileListActivity$6;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    # getter for: Lcom/konka/mm/filemanager/FileListActivity;->curFile:Ljava/io/File;
    invoke-static {v6}, Lcom/konka/mm/filemanager/FileListActivity;->access$13(Lcom/konka/mm/filemanager/FileListActivity;)Ljava/io/File;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/konka/mm/filemanager/FileListActivity$6;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    iget v7, v7, Lcom/konka/mm/filemanager/FileListActivity;->mScreen_mode:I

    invoke-static/range {v2 .. v7}, Lcom/konka/mm/finals/CommonFinals;->quitActivityByPopup(Landroid/app/Activity;Ljava/lang/String;IILjava/io/File;I)V

    goto/16 :goto_0
.end method
