.class Lcom/konka/mm/filemanager/FileListActivity$9;
.super Ljava/lang/Object;
.source "FileListActivity.java"

# interfaces
.implements Ljava/util/Observer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/mm/filemanager/FileListActivity;->setListeners()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/mm/filemanager/FileListActivity;


# direct methods
.method constructor <init>(Lcom/konka/mm/filemanager/FileListActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/filemanager/FileListActivity$9;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 11
    .param p1    # Ljava/util/Observable;
    .param p2    # Ljava/lang/Object;

    const v10, 0x7f0b0043    # com.konka.mm.R.id.img_item_rename_icon

    const v9, 0x7f0b0027    # com.konka.mm.R.id.img_item_delete_icon

    const/4 v8, 0x4

    const/4 v7, 0x0

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iget-object v6, p0, Lcom/konka/mm/filemanager/FileListActivity$9;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    iget-object v6, v6, Lcom/konka/mm/filemanager/FileListActivity;->viewFilpper:Landroid/widget/ViewFlipper;

    if-eqz v6, :cond_0

    const/4 v4, 0x0

    :goto_0
    const/4 v6, 0x3

    if-lt v4, v6, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v6, p0, Lcom/konka/mm/filemanager/FileListActivity$9;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    iget-object v6, v6, Lcom/konka/mm/filemanager/FileListActivity;->pageGridView:[Landroid/widget/GridView;

    aget-object v6, v6, v4

    if-eqz v6, :cond_0

    const/4 v1, 0x0

    :goto_1
    iget-object v6, p0, Lcom/konka/mm/filemanager/FileListActivity$9;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    iget-object v6, v6, Lcom/konka/mm/filemanager/FileListActivity;->pageGridView:[Landroid/widget/GridView;

    aget-object v6, v6, v4

    invoke-virtual {v6}, Landroid/widget/GridView;->getChildCount()I

    move-result v6

    if-lt v1, v6, :cond_3

    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_3
    iget-object v6, p0, Lcom/konka/mm/filemanager/FileListActivity$9;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    iget-object v6, v6, Lcom/konka/mm/filemanager/FileListActivity;->pageGridView:[Landroid/widget/GridView;

    aget-object v6, v6, v4

    invoke-virtual {v6, v1}, Landroid/widget/GridView;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    if-eqz v5, :cond_2

    iget-object v6, p0, Lcom/konka/mm/filemanager/FileListActivity$9;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    iget-boolean v6, v6, Lcom/konka/mm/filemanager/FileListActivity;->isManager:Z

    if-eqz v6, :cond_5

    invoke-virtual {v5, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    if-eqz v0, :cond_4

    move v6, v7

    :goto_2
    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_4
    move v6, v8

    goto :goto_2

    :cond_5
    iget-object v6, p0, Lcom/konka/mm/filemanager/FileListActivity$9;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    iget-boolean v6, v6, Lcom/konka/mm/filemanager/FileListActivity;->isRenameManager:Z

    if-eqz v6, :cond_7

    invoke-virtual {v5, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    if-eqz v0, :cond_6

    move v6, v7

    :goto_4
    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_3

    :cond_6
    move v6, v8

    goto :goto_4

    :cond_7
    invoke-virtual {v5, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    if-eqz v0, :cond_8

    move v6, v7

    :goto_5
    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-virtual {v5, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    if-eqz v0, :cond_9

    move v6, v7

    :goto_6
    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_3

    :cond_8
    move v6, v8

    goto :goto_5

    :cond_9
    move v6, v8

    goto :goto_6
.end method
