.class Lcom/konka/mm/filemanager/FileListActivity$7;
.super Ljava/lang/Object;
.source "FileListActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/mm/filemanager/FileListActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemSelectedListener;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/mm/filemanager/FileListActivity;


# direct methods
.method constructor <init>(Lcom/konka/mm/filemanager/FileListActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/filemanager/FileListActivity$7;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    iget-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity$7;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    iput p3, v1, Lcom/konka/mm/filemanager/FileListActivity;->curSelectedPos:I

    iget-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity$7;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    iget-boolean v1, v1, Lcom/konka/mm/filemanager/FileListActivity;->isManager:Z

    if-eqz v1, :cond_0

    const-string v1, "FileListActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "OnItemSelectedListener ------>"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/mm/filemanager/FileListActivity$7;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    iget v3, v3, Lcom/konka/mm/filemanager/FileListActivity;->curSelectedPos:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity$7;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    # invokes: Lcom/konka/mm/filemanager/FileListActivity;->getCurFile(I)Ljava/io/File;
    invoke-static {v1, p3}, Lcom/konka/mm/filemanager/FileListActivity;->access$18(Lcom/konka/mm/filemanager/FileListActivity;I)Ljava/io/File;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity$7;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    invoke-virtual {v1, v0}, Lcom/konka/mm/filemanager/FileListActivity;->initFileInfo(Ljava/io/File;)V

    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity$7;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    const/4 v2, 0x0

    iput v2, v1, Lcom/konka/mm/filemanager/FileListActivity;->curSelectedPos:I

    goto :goto_0
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    return-void
.end method
