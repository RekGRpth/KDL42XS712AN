.class Lcom/konka/mm/filemanager/FileListActivity$FileInfoTask;
.super Landroid/os/AsyncTask;
.source "FileListActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/mm/filemanager/FileListActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "FileInfoTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Object;",
        "Ljava/lang/Object;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private file:Ljava/io/File;

.field fileCount:Ljava/lang/String;

.field size:J

.field private textView:Landroid/widget/TextView;

.field final synthetic this$0:Lcom/konka/mm/filemanager/FileListActivity;


# direct methods
.method public constructor <init>(Lcom/konka/mm/filemanager/FileListActivity;Landroid/widget/TextView;Ljava/io/File;)V
    .locals 2
    .param p2    # Landroid/widget/TextView;
    .param p3    # Ljava/io/File;

    iput-object p1, p0, Lcom/konka/mm/filemanager/FileListActivity$FileInfoTask;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/konka/mm/filemanager/FileListActivity$FileInfoTask;->size:J

    iput-object p2, p0, Lcom/konka/mm/filemanager/FileListActivity$FileInfoTask;->textView:Landroid/widget/TextView;

    iput-object p3, p0, Lcom/konka/mm/filemanager/FileListActivity$FileInfoTask;->file:Ljava/io/File;

    return-void
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Object;

    invoke-virtual {p0, p1}, Lcom/konka/mm/filemanager/FileListActivity$FileInfoTask;->doInBackground([Ljava/lang/Object;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Object;)Ljava/lang/Void;
    .locals 4
    .param p1    # [Ljava/lang/Object;

    :try_start_0
    iget-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity$FileInfoTask;->file:Ljava/io/File;

    invoke-static {v1}, Lcom/konka/mm/tools/FileTool;->getFileSize(Ljava/io/File;)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/konka/mm/filemanager/FileListActivity$FileInfoTask;->size:J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/konka/mm/filemanager/FileListActivity$FileInfoTask;->file:Ljava/io/File;

    invoke-static {v2}, Lcom/konka/mm/tools/FileTool;->getFileCount(Ljava/io/File;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/mm/filemanager/FileListActivity$FileInfoTask;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    invoke-virtual {v2}, Lcom/konka/mm/filemanager/FileListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09008c    # com.konka.mm.R.string.FILE_TITLE

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity$FileInfoTask;->fileCount:Ljava/lang/String;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p0, v1}, Lcom/konka/mm/filemanager/FileListActivity$FileInfoTask;->publishProgress([Ljava/lang/Object;)V

    const/4 v1, 0x0

    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method protected varargs onProgressUpdate([Ljava/lang/Object;)V
    .locals 4
    .param p1    # [Ljava/lang/Object;

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity$FileInfoTask;->textView:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/konka/mm/filemanager/FileListActivity$FileInfoTask;->file:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, ",  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/mm/filemanager/FileListActivity$FileInfoTask;->fileCount:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/konka/mm/filemanager/FileListActivity$FileInfoTask;->size:J

    invoke-static {v2, v3}, Lcom/konka/mm/tools/FileTool;->FormetFileSize(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onProgressUpdate([Ljava/lang/Object;)V

    return-void
.end method
