.class Lcom/konka/mm/filemanager/FileItemClickListener$2;
.super Landroid/os/Handler;
.source "FileItemClickListener.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/mm/filemanager/FileItemClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/mm/filemanager/FileItemClickListener;


# direct methods
.method constructor <init>(Lcom/konka/mm/filemanager/FileItemClickListener;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/filemanager/FileItemClickListener$2;->this$0:Lcom/konka/mm/filemanager/FileItemClickListener;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1    # Landroid/os/Message;

    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    iget-object v2, p0, Lcom/konka/mm/filemanager/FileItemClickListener$2;->this$0:Lcom/konka/mm/filemanager/FileItemClickListener;

    # invokes: Lcom/konka/mm/filemanager/FileItemClickListener;->initCopyProgressDialog()V
    invoke-static {v2}, Lcom/konka/mm/filemanager/FileItemClickListener;->access$3(Lcom/konka/mm/filemanager/FileItemClickListener;)V

    iget-object v2, p0, Lcom/konka/mm/filemanager/FileItemClickListener$2;->this$0:Lcom/konka/mm/filemanager/FileItemClickListener;

    iget-object v2, v2, Lcom/konka/mm/filemanager/FileItemClickListener;->copyProgressDialog:Landroid/app/ProgressDialog;

    iget v3, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v2, v3}, Landroid/app/ProgressDialog;->setMax(I)V

    iget-object v2, p0, Lcom/konka/mm/filemanager/FileItemClickListener$2;->this$0:Lcom/konka/mm/filemanager/FileItemClickListener;

    iget-object v2, v2, Lcom/konka/mm/filemanager/FileItemClickListener;->copyProgressDialog:Landroid/app/ProgressDialog;

    const-string v3, "---"

    invoke-virtual {v2, v3}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/konka/mm/filemanager/FileItemClickListener$2;->this$0:Lcom/konka/mm/filemanager/FileItemClickListener;

    iget-object v2, v2, Lcom/konka/mm/filemanager/FileItemClickListener;->copyProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v2}, Landroid/app/ProgressDialog;->show()V

    goto :goto_0

    :pswitch_2
    iget-object v2, p0, Lcom/konka/mm/filemanager/FileItemClickListener$2;->this$0:Lcom/konka/mm/filemanager/FileItemClickListener;

    iget-object v2, v2, Lcom/konka/mm/filemanager/FileItemClickListener;->fileListActivity:Lcom/konka/mm/filemanager/FileListActivity;

    iget-object v2, v2, Lcom/konka/mm/filemanager/FileListActivity;->listViewHandler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    const/16 v2, 0xc

    iput v2, v1, Landroid/os/Message;->what:I

    iget-object v2, p0, Lcom/konka/mm/filemanager/FileItemClickListener$2;->this$0:Lcom/konka/mm/filemanager/FileItemClickListener;

    iget v2, v2, Lcom/konka/mm/filemanager/FileItemClickListener;->position:I

    add-int/lit8 v2, v2, -0x1

    iput v2, v1, Landroid/os/Message;->arg1:I

    const/16 v2, 0x17

    iput v2, v1, Landroid/os/Message;->arg2:I

    iget-object v2, p0, Lcom/konka/mm/filemanager/FileItemClickListener$2;->this$0:Lcom/konka/mm/filemanager/FileItemClickListener;

    iget-object v2, v2, Lcom/konka/mm/filemanager/FileItemClickListener;->fileListActivity:Lcom/konka/mm/filemanager/FileListActivity;

    iget-object v2, v2, Lcom/konka/mm/filemanager/FileListActivity;->listViewHandler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    :pswitch_3
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/konka/mm/filemanager/FileItemClickListener$2;->this$0:Lcom/konka/mm/filemanager/FileItemClickListener;

    iget-object v2, v2, Lcom/konka/mm/filemanager/FileItemClickListener;->copyProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/konka/mm/filemanager/FileItemClickListener$2;->this$0:Lcom/konka/mm/filemanager/FileItemClickListener;

    iget-object v2, v2, Lcom/konka/mm/filemanager/FileItemClickListener;->copyProgressDialog:Landroid/app/ProgressDialog;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "\u4ece\t"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "fp"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\t\u5230\t"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "tp"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n\u5927\u5c0f: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/mm/filemanager/FileItemClickListener$2;->this$0:Lcom/konka/mm/filemanager/FileItemClickListener;

    iget-object v4, v4, Lcom/konka/mm/filemanager/FileItemClickListener;->copyProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v4}, Landroid/app/ProgressDialog;->getMax()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " KB"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :pswitch_4
    iget-object v2, p0, Lcom/konka/mm/filemanager/FileItemClickListener$2;->this$0:Lcom/konka/mm/filemanager/FileItemClickListener;

    iget-object v2, v2, Lcom/konka/mm/filemanager/FileItemClickListener;->copyProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/konka/mm/filemanager/FileItemClickListener$2;->this$0:Lcom/konka/mm/filemanager/FileItemClickListener;

    iget-object v2, v2, Lcom/konka/mm/filemanager/FileItemClickListener;->copyProgressDialog:Landroid/app/ProgressDialog;

    iget v3, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v2, v3}, Landroid/app/ProgressDialog;->incrementProgressBy(I)V

    goto/16 :goto_0

    :pswitch_5
    iget-object v2, p0, Lcom/konka/mm/filemanager/FileItemClickListener$2;->this$0:Lcom/konka/mm/filemanager/FileItemClickListener;

    const/16 v3, 0x1d

    # invokes: Lcom/konka/mm/filemanager/FileItemClickListener;->dealCopyBack(I)V
    invoke-static {v2, v3}, Lcom/konka/mm/filemanager/FileItemClickListener;->access$4(Lcom/konka/mm/filemanager/FileItemClickListener;I)V

    iget-object v2, p0, Lcom/konka/mm/filemanager/FileItemClickListener$2;->this$0:Lcom/konka/mm/filemanager/FileItemClickListener;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/konka/mm/filemanager/FileItemClickListener;->access$5(Lcom/konka/mm/filemanager/FileItemClickListener;Z)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x14
        :pswitch_1
        :pswitch_4
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_5
    .end packed-switch
.end method
