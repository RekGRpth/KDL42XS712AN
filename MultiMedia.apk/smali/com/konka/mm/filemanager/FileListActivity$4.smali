.class Lcom/konka/mm/filemanager/FileListActivity$4;
.super Ljava/lang/Object;
.source "FileListActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/mm/filemanager/FileListActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/mm/filemanager/FileListActivity;


# direct methods
.method constructor <init>(Lcom/konka/mm/filemanager/FileListActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/filemanager/FileListActivity$4;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1    # Landroid/view/View;

    const/16 v5, 0x3e8

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :goto_0
    :pswitch_0
    iget-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity$4;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    # getter for: Lcom/konka/mm/filemanager/FileListActivity;->menuPopupWindow:Landroid/widget/PopupWindow;
    invoke-static {v1}, Lcom/konka/mm/filemanager/FileListActivity;->access$17(Lcom/konka/mm/filemanager/FileListActivity;)Landroid/widget/PopupWindow;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity$4;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    # getter for: Lcom/konka/mm/filemanager/FileListActivity;->menuPopupWindow:Landroid/widget/PopupWindow;
    invoke-static {v1}, Lcom/konka/mm/filemanager/FileListActivity;->access$17(Lcom/konka/mm/filemanager/FileListActivity;)Landroid/widget/PopupWindow;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/PopupWindow;->dismiss()V

    :cond_0
    return-void

    :pswitch_1
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity$4;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    # getter for: Lcom/konka/mm/filemanager/FileListActivity;->curFile:Ljava/io/File;
    invoke-static {v1}, Lcom/konka/mm/filemanager/FileListActivity;->access$13(Lcom/konka/mm/filemanager/FileListActivity;)Ljava/io/File;

    move-result-object v1

    invoke-static {v1}, Lcom/konka/mm/tools/FileTool;->deleteFileOrDirectory(Ljava/io/File;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity$4;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    new-instance v2, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/konka/mm/filemanager/FileListActivity$4;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    invoke-virtual {v3}, Lcom/konka/mm/filemanager/FileListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0900af    # com.konka.mm.R.string.FILE_DELETE_SUCCESS

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "!"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    :goto_1
    iget-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity$4;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    iget-object v2, p0, Lcom/konka/mm/filemanager/FileListActivity$4;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    iget-object v2, v2, Lcom/konka/mm/filemanager/FileListActivity;->parentFile:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x64

    invoke-virtual {v1, v2, v3}, Lcom/konka/mm/filemanager/FileListActivity;->broweTo(Ljava/lang/String;I)V

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity$4;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    new-instance v2, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/konka/mm/filemanager/FileListActivity$4;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    invoke-virtual {v3}, Lcom/konka/mm/filemanager/FileListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090081    # com.konka.mm.R.string.FILE_DELETE

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/mm/filemanager/FileListActivity$4;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    invoke-virtual {v3}, Lcom/konka/mm/filemanager/FileListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f09008b    # com.konka.mm.R.string.FAILE

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "!"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x7f0b0058
        :pswitch_1    # com.konka.mm.R.id.btn_menu_delete_sure
        :pswitch_0    # com.konka.mm.R.id.btn_menu_delete_cancle
    .end packed-switch
.end method
