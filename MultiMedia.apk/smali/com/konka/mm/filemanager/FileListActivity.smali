.class public Lcom/konka/mm/filemanager/FileListActivity;
.super Landroid/app/Activity;
.source "FileListActivity.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;
.implements Lcom/konka/mm/IActivity;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/mm/filemanager/FileListActivity$FileInfoTask;,
        Lcom/konka/mm/filemanager/FileListActivity$MyObservable;,
        Lcom/konka/mm/filemanager/FileListActivity$PathBtnClickListener;,
        Lcom/konka/mm/filemanager/FileListActivity$SortSizeThread;
    }
.end annotation


# static fields
.field public static final BROWE_DELETE:I = 0x4

.field public static final BROWE_DOWN:I = 0x1

.field public static final BROWE_OTHER:I = 0x3

.field public static final BROWE_ROOT:I = 0x0

.field public static final BROWE_SORT:I = 0x5

.field public static final BROWE_UP:I = 0x2

.field public static final EMPTY_FILE_NAME:Ljava/lang/String; = "enpty_file_name"

.field public static final HANDLER_ADD_LIST_ITEM:I = 0x16

.field public static final HANDLER_CLIP_BOARD_EMPTY:I = 0xe

.field public static final HANDLER_COPY_FILE_ERROR:I = 0xf

.field public static final HANDLER_FILE_CLICK:I = 0x10

.field public static final HANDLER_LIST_ADPATER_CHANGED:I = 0x11

.field public static final HANDLER_REFRESH_LISTVIEW:I = 0x17

.field public static final HANDLER_RENAME_LISTVIEW:I = 0xd

.field public static final HANDLER_SEARCHBAR_HIDE:I = 0x12

.field public static final HANDLER_SEARCHSTOP:I = 0x13

.field public static final HANDLER_SET_LISTVIEW_SELECTED:I = 0xc

.field public static final HANDLER_SET_SEARCHDIR:I = 0x14

.field public static final HANDLER_SET_SEARCH_VISIBLE:I = 0x15

.field public static final HANDLER_SHOW_COPY_WARNING_DIALOG:I = 0xb

.field public static final HAS_SAME_NAME:Ljava/lang/String; = "has_same_name"

.field public static IsRussian:Z = false

.field public static final LANGUAGE_EN:Ljava/lang/String; = "en"

.field public static final LANGUAGE_ZH:Ljava/lang/String; = "zh"

.field private static final MENU_APK_MANAGER:I = 0x5

.field private static final MENU_CREATE_DIRECTORY:I = 0x0

.field private static final MENU_CREATE_FILE:I = 0x1

.field private static final MENU_FILE_LIB:I = 0x8

.field private static final MENU_FINISH_ACTIVITY:I = 0x9

.field static final MENU_ITEM_ADD_LIB:I = 0xa

.field static final MENU_ITEM_COPY:I = 0x2

.field static final MENU_ITEM_COPY_SELECTED:I = 0xc

.field static final MENU_ITEM_CREATE_DIRECTORY:I = 0xf

.field static final MENU_ITEM_CREATE_FILE:I = 0x10

.field static final MENU_ITEM_CUT:I = 0x3

.field static final MENU_ITEM_CUT_SELECTED:I = 0xd

.field static final MENU_ITEM_DELETE:I = 0x5

.field static final MENU_ITEM_DELETE_SELECTED:I = 0xe

.field static final MENU_ITEM_DRABLE:I = 0x9

.field static final MENU_ITEM_OPEN:I = 0x0

.field static final MENU_ITEM_OPEN_IN_OTHER:I = 0x1

.field static final MENU_ITEM_PASTE:I = 0x4

.field static final MENU_ITEM_PROPERTIES:I = 0xb

.field static final MENU_ITEM_RENAME:I = 0x6

.field static final MENU_ITEM_SELECT_ALL:I = 0x7

.field static final MENU_ITEM_UNDRABLE:I = 0x8

.field private static final MENU_PASTE:I = 0x2

.field public static final MENU_SEARCH:I = 0x3

.field private static final MENU_SETTING:I = 0x6

.field private static final MENU_SET_VIEW_STYLE:I = 0x7

.field private static final MENU_SHOW_COPY_DIALOG:I = 0x4

.field public static PLAYING_MUSIC_PATH:Ljava/lang/String; = null

.field public static final SCREEN_1080P:I = 0x1

.field public static final SCREEN_720P:I = 0x0

.field private static final TAG:Ljava/lang/String; = "FileListActivity"

.field private static executor:Ljava/util/concurrent/ThreadPoolExecutor;

.field public static instance:Lcom/konka/mm/filemanager/FileListActivity;


# instance fields
.field private CURRENT_LANGUAGE:Ljava/lang/String;

.field private PageCount:I

.field private adapter:Lcom/konka/mm/adapters/FileListAdapter;

.field private appMenu:Landroid/widget/LinearLayout;

.field private bNextPageKey:Z

.field private cancleBtnMenu:Landroid/widget/Button;

.field private changeListener:Landroid/widget/RadioGroup$OnCheckedChangeListener;

.field private clickListener:Landroid/view/View$OnClickListener;

.field private curFile:Ljava/io/File;

.field protected curScreen:I

.field public curSelectedPos:I

.field protected currentPage:I

.field private delete:Landroid/widget/LinearLayout;

.field private deleteAll:Landroid/widget/LinearLayout;

.field private diskName:Ljava/lang/String;

.field private diskPath:Landroid/widget/LinearLayout;

.field private executorService:Ljava/util/concurrent/ExecutorService;

.field private fileArr:[Ljava/io/File;

.field private fileInfo:Landroid/widget/TextView;

.field private fileInfoTask:Lcom/konka/mm/filemanager/FileListActivity$FileInfoTask;

.field private fileSize:I

.field private fileViewLongClickListener:Landroid/widget/AdapterView$OnItemLongClickListener;

.field private gridColumnWidth:I

.field private gridOnKey:Landroid/view/View$OnKeyListener;

.field private gridViewOnItemClick:Landroid/widget/AdapterView$OnItemClickListener;

.field private gridViewOnItemSelected:Landroid/widget/AdapterView$OnItemSelectedListener;

.field private infoMenu:Landroid/widget/TextView;

.field public infos:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field private is3dKeyDown:Z

.field private isEnterOtherAct:Z

.field private isFirst:I

.field private isFocusInGrid:Z

.field public isManager:Z

.field private isPopup:Z

.field public isRenameManager:Z

.field private isRun:Z

.field public linux:Lcom/konka/mm/utils/LinuxFileCommand;

.field listListener:Lcom/konka/mm/filemanager/FileItemClickListener;

.field listViewHandler:Landroid/os/Handler;

.field private list_btn:Landroid/widget/Button;

.field protected mCountPerPage:I

.field private mPushLeftInAnim:Landroid/view/animation/Animation;

.field private mPushLeftOutAnim:Landroid/view/animation/Animation;

.field private mPustRightInAnim:Landroid/view/animation/Animation;

.field private mPustRightOutAnim:Landroid/view/animation/Animation;

.field private mReceiver:Landroid/content/BroadcastReceiver;

.field protected mScreen_mode:I

.field public manager:Landroid/widget/Button;

.field private menuClickListener:Landroid/view/View$OnClickListener;

.field private menuPopupView:Landroid/view/View;

.field private menuPopupWindow:Landroid/widget/PopupWindow;

.field private move:I

.field protected multFile:Z

.field private numColumns:I

.field public observable:Lcom/konka/mm/filemanager/FileListActivity$MyObservable;

.field protected pageGridView:[Landroid/widget/GridView;

.field private pageInfo:Lcom/konka/mm/model/PageInfo;

.field private pageInfomation:Landroid/widget/TextView;

.field private pageInfos:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/konka/mm/model/PageInfo;",
            ">;"
        }
    .end annotation
.end field

.field private pageLeft:Landroid/widget/Button;

.field private pageRight:Landroid/widget/Button;

.field private page_linearLayout:Landroid/widget/LinearLayout;

.field protected parentFile:Ljava/io/File;

.field private pathIndex:I

.field private pathList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/konka/mm/model/PathItem;",
            ">;"
        }
    .end annotation
.end field

.field private perColumnWidth:I

.field private popupView:Landroid/view/View;

.field private popupWindow:Landroid/widget/PopupWindow;

.field private proDlg:Landroid/app/ProgressDialog;

.field private rename:Landroid/widget/LinearLayout;

.field private root_path:Ljava/lang/String;

.field private screenCount:I

.field private screenHeight:I

.field private screenHeight_720p:I

.field private screenWidth:I

.field private screenWidth_720p:I

.field private showPage:I

.field private sort:Landroid/widget/Button;

.field private sortBtns:Landroid/widget/RadioGroup;

.field private sortHandler:Landroid/os/Handler;

.field private sortType:I

.field startX:F

.field private sureBtnMenu:Landroid/widget/Button;

.field protected viewFilpper:Landroid/widget/ViewFlipper;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x5

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ThreadPoolExecutor;

    sput-object v0, Lcom/konka/mm/filemanager/FileListActivity;->executor:Ljava/util/concurrent/ThreadPoolExecutor;

    const-string v0, ""

    sput-object v0, Lcom/konka/mm/filemanager/FileListActivity;->PLAYING_MUSIC_PATH:Ljava/lang/String;

    const/4 v0, 0x0

    sput-boolean v0, Lcom/konka/mm/filemanager/FileListActivity;->IsRussian:Z

    const/4 v0, 0x0

    sput-object v0, Lcom/konka/mm/filemanager/FileListActivity;->instance:Lcom/konka/mm/filemanager/FileListActivity;

    return-void
.end method

.method public constructor <init>()V
    .locals 5

    const/4 v4, 0x5

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput-boolean v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->bNextPageKey:Z

    const/4 v0, 0x3

    new-array v0, v0, [Landroid/widget/GridView;

    iput-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->pageGridView:[Landroid/widget/GridView;

    iput-object v2, p0, Lcom/konka/mm/filemanager/FileListActivity;->pathList:Ljava/util/List;

    iput-object v2, p0, Lcom/konka/mm/filemanager/FileListActivity;->infos:Ljava/util/List;

    iput-object v2, p0, Lcom/konka/mm/filemanager/FileListActivity;->fileArr:[Ljava/io/File;

    iput-object v2, p0, Lcom/konka/mm/filemanager/FileListActivity;->curFile:Ljava/io/File;

    iput-object v2, p0, Lcom/konka/mm/filemanager/FileListActivity;->parentFile:Ljava/io/File;

    iput-object v2, p0, Lcom/konka/mm/filemanager/FileListActivity;->adapter:Lcom/konka/mm/adapters/FileListAdapter;

    iput-object v2, p0, Lcom/konka/mm/filemanager/FileListActivity;->root_path:Ljava/lang/String;

    const/16 v0, 0x14a

    iput v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->gridColumnWidth:I

    iput-boolean v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->isPopup:Z

    iput-boolean v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->isManager:Z

    iput-boolean v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->isRenameManager:Z

    iput v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->fileSize:I

    iput v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->PageCount:I

    const/16 v0, 0x500

    iput v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->screenWidth_720p:I

    const/16 v0, 0x2d0

    iput v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->screenHeight_720p:I

    iput v3, p0, Lcom/konka/mm/filemanager/FileListActivity;->mScreen_mode:I

    iput v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->currentPage:I

    iput v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->curScreen:I

    iput-boolean v3, p0, Lcom/konka/mm/filemanager/FileListActivity;->isEnterOtherAct:Z

    iput-boolean v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->is3dKeyDown:Z

    invoke-static {v4}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->executorService:Ljava/util/concurrent/ExecutorService;

    iput v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->curSelectedPos:I

    iput v4, p0, Lcom/konka/mm/filemanager/FileListActivity;->showPage:I

    iput v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->sortType:I

    iput-boolean v3, p0, Lcom/konka/mm/filemanager/FileListActivity;->isRun:Z

    iput v3, p0, Lcom/konka/mm/filemanager/FileListActivity;->isFirst:I

    iput v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->screenCount:I

    const/16 v0, 0xa

    iput v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->perColumnWidth:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->pageInfos:Ljava/util/List;

    iput-object v2, p0, Lcom/konka/mm/filemanager/FileListActivity;->pageInfo:Lcom/konka/mm/model/PageInfo;

    iput-boolean v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->multFile:Z

    iput v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->pathIndex:I

    iput-boolean v3, p0, Lcom/konka/mm/filemanager/FileListActivity;->isFocusInGrid:Z

    new-instance v0, Lcom/konka/mm/filemanager/FileListActivity$1;

    invoke-direct {v0, p0}, Lcom/konka/mm/filemanager/FileListActivity$1;-><init>(Lcom/konka/mm/filemanager/FileListActivity;)V

    iput-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->sortHandler:Landroid/os/Handler;

    new-instance v0, Lcom/konka/mm/filemanager/FileListActivity$2;

    invoke-direct {v0, p0}, Lcom/konka/mm/filemanager/FileListActivity$2;-><init>(Lcom/konka/mm/filemanager/FileListActivity;)V

    iput-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->clickListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/konka/mm/filemanager/FileListActivity$3;

    invoke-direct {v0, p0}, Lcom/konka/mm/filemanager/FileListActivity$3;-><init>(Lcom/konka/mm/filemanager/FileListActivity;)V

    iput-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->changeListener:Landroid/widget/RadioGroup$OnCheckedChangeListener;

    new-instance v0, Lcom/konka/mm/filemanager/FileListActivity$4;

    invoke-direct {v0, p0}, Lcom/konka/mm/filemanager/FileListActivity$4;-><init>(Lcom/konka/mm/filemanager/FileListActivity;)V

    iput-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->menuClickListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/konka/mm/filemanager/FileListActivity$5;

    invoke-direct {v0, p0}, Lcom/konka/mm/filemanager/FileListActivity$5;-><init>(Lcom/konka/mm/filemanager/FileListActivity;)V

    iput-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->fileViewLongClickListener:Landroid/widget/AdapterView$OnItemLongClickListener;

    new-instance v0, Lcom/konka/mm/filemanager/FileListActivity$6;

    invoke-direct {v0, p0}, Lcom/konka/mm/filemanager/FileListActivity$6;-><init>(Lcom/konka/mm/filemanager/FileListActivity;)V

    iput-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->gridViewOnItemClick:Landroid/widget/AdapterView$OnItemClickListener;

    new-instance v0, Lcom/konka/mm/filemanager/FileListActivity$7;

    invoke-direct {v0, p0}, Lcom/konka/mm/filemanager/FileListActivity$7;-><init>(Lcom/konka/mm/filemanager/FileListActivity;)V

    iput-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->gridViewOnItemSelected:Landroid/widget/AdapterView$OnItemSelectedListener;

    new-instance v0, Lcom/konka/mm/filemanager/FileListActivity$8;

    invoke-direct {v0, p0}, Lcom/konka/mm/filemanager/FileListActivity$8;-><init>(Lcom/konka/mm/filemanager/FileListActivity;)V

    iput-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->gridOnKey:Landroid/view/View$OnKeyListener;

    return-void
.end method

.method public static FormetFileSize(J)Ljava/lang/String;
    .locals 7
    .param p0    # J

    new-instance v0, Ljava/text/DecimalFormat;

    const-string v2, "#.00"

    invoke-direct {v0, v2}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    const-string v1, ""

    const-wide/16 v2, 0x400

    cmp-long v2, p0, v2

    if-gez v2, :cond_1

    const-wide/16 v2, 0x0

    cmp-long v2, v2, p0

    if-nez v2, :cond_0

    const-string v1, "0.00B"

    :goto_0
    return-object v1

    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    long-to-double v3, p0

    invoke-virtual {v0, v3, v4}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "B"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_1
    const-wide/32 v2, 0x100000

    cmp-long v2, p0, v2

    if-gez v2, :cond_2

    new-instance v2, Ljava/lang/StringBuilder;

    long-to-double v3, p0

    const-wide/high16 v5, 0x4090000000000000L    # 1024.0

    div-double/2addr v3, v5

    invoke-virtual {v0, v3, v4}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "K"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_2
    const-wide/32 v2, 0x40000000

    cmp-long v2, p0, v2

    if-gez v2, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    long-to-double v3, p0

    const-wide/high16 v5, 0x4130000000000000L    # 1048576.0

    div-double/2addr v3, v5

    invoke-virtual {v0, v3, v4}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "M"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_3
    new-instance v2, Ljava/lang/StringBuilder;

    long-to-double v3, p0

    const-wide/high16 v5, 0x41d0000000000000L    # 1.073741824E9

    div-double/2addr v3, v5

    invoke-virtual {v0, v3, v4}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "G"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method static synthetic access$0(Lcom/konka/mm/filemanager/FileListActivity;[Ljava/io/File;)Ljava/util/List;
    .locals 1

    invoke-direct {p0, p1}, Lcom/konka/mm/filemanager/FileListActivity;->getListFromFiles([Ljava/io/File;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1(Lcom/konka/mm/filemanager/FileListActivity;)Landroid/app/ProgressDialog;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->proDlg:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$10(Lcom/konka/mm/filemanager/FileListActivity;)Landroid/widget/Button;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->sort:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$11(Lcom/konka/mm/filemanager/FileListActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/mm/filemanager/FileListActivity;->deleteFile()V

    return-void
.end method

.method static synthetic access$12(Lcom/konka/mm/filemanager/FileListActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/mm/filemanager/FileListActivity;->deleteAllFile()V

    return-void
.end method

.method static synthetic access$13(Lcom/konka/mm/filemanager/FileListActivity;)Ljava/io/File;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->curFile:Ljava/io/File;

    return-object v0
.end method

.method static synthetic access$14(Lcom/konka/mm/filemanager/FileListActivity;I)V
    .locals 0

    iput p1, p0, Lcom/konka/mm/filemanager/FileListActivity;->sortType:I

    return-void
.end method

.method static synthetic access$15(Lcom/konka/mm/filemanager/FileListActivity;I)V
    .locals 0

    iput p1, p0, Lcom/konka/mm/filemanager/FileListActivity;->isFirst:I

    return-void
.end method

.method static synthetic access$16(Lcom/konka/mm/filemanager/FileListActivity;)I
    .locals 1

    iget v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->isFirst:I

    return v0
.end method

.method static synthetic access$17(Lcom/konka/mm/filemanager/FileListActivity;)Landroid/widget/PopupWindow;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->menuPopupWindow:Landroid/widget/PopupWindow;

    return-object v0
.end method

.method static synthetic access$18(Lcom/konka/mm/filemanager/FileListActivity;I)Ljava/io/File;
    .locals 1

    invoke-direct {p0, p1}, Lcom/konka/mm/filemanager/FileListActivity;->getCurFile(I)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$19(Lcom/konka/mm/filemanager/FileListActivity;Ljava/io/File;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/filemanager/FileListActivity;->curFile:Ljava/io/File;

    return-void
.end method

.method static synthetic access$2(Lcom/konka/mm/filemanager/FileListActivity;)Landroid/widget/PopupWindow;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->popupWindow:Landroid/widget/PopupWindow;

    return-object v0
.end method

.method static synthetic access$20(Lcom/konka/mm/filemanager/FileListActivity;[Ljava/io/File;)[Ljava/io/File;
    .locals 1

    invoke-direct {p0, p1}, Lcom/konka/mm/filemanager/FileListActivity;->sortFiles([Ljava/io/File;)[Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$21(Lcom/konka/mm/filemanager/FileListActivity;[Ljava/io/File;)Ljava/util/ArrayList;
    .locals 1

    invoke-direct {p0, p1}, Lcom/konka/mm/filemanager/FileListActivity;->files2Strings([Ljava/io/File;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$22(Lcom/konka/mm/filemanager/FileListActivity;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->root_path:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$23(Lcom/konka/mm/filemanager/FileListActivity;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/konka/mm/filemanager/FileListActivity;->isEnterOtherAct:Z

    return-void
.end method

.method static synthetic access$24(Lcom/konka/mm/filemanager/FileListActivity;)I
    .locals 1

    iget v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->PageCount:I

    return v0
.end method

.method static synthetic access$25(Lcom/konka/mm/filemanager/FileListActivity;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/konka/mm/filemanager/FileListActivity;->bNextPageKey:Z

    return-void
.end method

.method static synthetic access$26(Lcom/konka/mm/filemanager/FileListActivity;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->sortHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$27(Lcom/konka/mm/filemanager/FileListActivity;Lcom/konka/mm/model/PathItem;)I
    .locals 1

    invoke-direct {p0, p1}, Lcom/konka/mm/filemanager/FileListActivity;->getPathIndexInList(Lcom/konka/mm/model/PathItem;)I

    move-result v0

    return v0
.end method

.method static synthetic access$28(Lcom/konka/mm/filemanager/FileListActivity;I)V
    .locals 0

    iput p1, p0, Lcom/konka/mm/filemanager/FileListActivity;->pathIndex:I

    return-void
.end method

.method static synthetic access$29(Lcom/konka/mm/filemanager/FileListActivity;Landroid/content/Intent;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/mm/filemanager/FileListActivity;->onReceiveSdCardBroadCast(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic access$3(Lcom/konka/mm/filemanager/FileListActivity;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->popupView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$4(Lcom/konka/mm/filemanager/FileListActivity;Landroid/widget/PopupWindow;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/filemanager/FileListActivity;->popupWindow:Landroid/widget/PopupWindow;

    return-void
.end method

.method static synthetic access$5(Lcom/konka/mm/filemanager/FileListActivity;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/filemanager/FileListActivity;->CURRENT_LANGUAGE:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$6(Lcom/konka/mm/filemanager/FileListActivity;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->CURRENT_LANGUAGE:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$7(Lcom/konka/mm/filemanager/FileListActivity;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/konka/mm/filemanager/FileListActivity;->isPopup:Z

    return-void
.end method

.method static synthetic access$8(Lcom/konka/mm/filemanager/FileListActivity;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/konka/mm/filemanager/FileListActivity;->isFocusInGrid:Z

    return-void
.end method

.method static synthetic access$9(Lcom/konka/mm/filemanager/FileListActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->isPopup:Z

    return v0
.end method

.method private checkIsRussian()V
    .locals 2

    invoke-virtual {p0}, Lcom/konka/mm/filemanager/FileListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f090000    # com.konka.mm.R.string.str_sys_language

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->CURRENT_LANGUAGE:Ljava/lang/String;

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->CURRENT_LANGUAGE:Ljava/lang/String;

    const-string v1, "ru"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    sput-boolean v0, Lcom/konka/mm/filemanager/FileListActivity;->IsRussian:Z

    :cond_0
    return-void
.end method

.method private cleanPath()V
    .locals 4

    iget-object v2, p0, Lcom/konka/mm/filemanager/FileListActivity;->pathList:Ljava/util/List;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/konka/mm/filemanager/FileListActivity;->pathList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/konka/mm/filemanager/FileListActivity;->pathList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-gtz v2, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/konka/mm/filemanager/FileListActivity;->pathList:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/konka/mm/model/PathItem;

    iget-object v2, p0, Lcom/konka/mm/filemanager/FileListActivity;->diskPath:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Lcom/konka/mm/model/PathItem;->getBtn()Landroid/widget/Button;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    iget-object v2, p0, Lcom/konka/mm/filemanager/FileListActivity;->diskPath:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Lcom/konka/mm/model/PathItem;->getTextview()Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    add-int/lit8 v0, v0, -0x1

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private deleteAllFile()V
    .locals 6

    const/4 v3, 0x2

    const/4 v2, 0x0

    iget-boolean v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->isManager:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->curSelectedPos:I

    invoke-direct {p0, v0}, Lcom/konka/mm/filemanager/FileListActivity;->getCurFile(I)Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->root_path:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->root_path:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->root_path:Ljava/lang/String;

    iget-object v2, p0, Lcom/konka/mm/filemanager/FileListActivity;->diskName:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    :cond_0
    invoke-static {p0}, Lcom/konka/mm/tools/FileTool;->killRunningServiceInfo(Landroid/content/Context;)V

    const/4 v2, 0x1

    iget v5, p0, Lcom/konka/mm/filemanager/FileListActivity;->mScreen_mode:I

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lcom/konka/mm/finals/CommonFinals;->quitActivityByPopup(Landroid/app/Activity;Ljava/lang/String;IILjava/io/File;I)V

    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->isRenameManager:Z

    if-eqz v0, :cond_2

    iput-boolean v2, p0, Lcom/konka/mm/filemanager/FileListActivity;->isRenameManager:Z

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->observable:Lcom/konka/mm/filemanager/FileListActivity$MyObservable;

    invoke-virtual {v0, v2}, Lcom/konka/mm/filemanager/FileListActivity$MyObservable;->toobarStatusChanged(Z)V

    :cond_2
    invoke-virtual {p0}, Lcom/konka/mm/filemanager/FileListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f090096    # com.konka.mm.R.string.delete_info

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x3

    iget v5, p0, Lcom/konka/mm/filemanager/FileListActivity;->mScreen_mode:I

    invoke-static {p0, v0, v3, v2, v5}, Lcom/konka/mm/finals/CommonFinals;->quitActivityByPopup(Landroid/app/Activity;Ljava/lang/String;III)V

    goto :goto_0
.end method

.method private deleteFile()V
    .locals 4

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->isManager:Z

    if-eqz v0, :cond_0

    iput-boolean v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->isManager:Z

    iput-boolean v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->isRenameManager:Z

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->observable:Lcom/konka/mm/filemanager/FileListActivity$MyObservable;

    invoke-virtual {v0, v1}, Lcom/konka/mm/filemanager/FileListActivity$MyObservable;->toobarStatusChanged(Z)V

    :goto_0
    return-void

    :cond_0
    iget-boolean v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->isRenameManager:Z

    if-eqz v0, :cond_1

    iput-boolean v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->isManager:Z

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->observable:Lcom/konka/mm/filemanager/FileListActivity$MyObservable;

    invoke-virtual {v0, v1}, Lcom/konka/mm/filemanager/FileListActivity$MyObservable;->toobarStatusChanged(Z)V

    :cond_1
    invoke-virtual {p0}, Lcom/konka/mm/filemanager/FileListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090096    # com.konka.mm.R.string.delete_info

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x3

    iget v3, p0, Lcom/konka/mm/filemanager/FileListActivity;->mScreen_mode:I

    invoke-static {p0, v0, v1, v2, v3}, Lcom/konka/mm/finals/CommonFinals;->quitActivityByPopup(Landroid/app/Activity;Ljava/lang/String;III)V

    goto :goto_0
.end method

.method private files2Strings([Ljava/io/File;)Ljava/util/ArrayList;
    .locals 3
    .param p1    # [Ljava/io/File;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/io/File;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    array-length v2, p1

    if-lez v2, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    :goto_0
    array-length v2, p1

    if-lt v1, v2, :cond_1

    :cond_0
    return-object v0

    :cond_1
    aget-object v2, p1, v1

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private findViews()V
    .locals 9

    const/4 v8, 0x1

    const/4 v7, 0x0

    const v6, 0x7f0b0023    # com.konka.mm.R.id.img_file_rename

    const v5, 0x7f0b0020    # com.konka.mm.R.id.img_file_delete_all

    const v4, 0x7f0b001d    # com.konka.mm.R.id.img_file_delete

    const v0, 0x7f0b0030    # com.konka.mm.R.id.viewFlipper

    invoke-virtual {p0, v0}, Lcom/konka/mm/filemanager/FileListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ViewFlipper;

    iput-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->viewFilpper:Landroid/widget/ViewFlipper;

    iget-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->pageGridView:[Landroid/widget/GridView;

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->viewFilpper:Landroid/widget/ViewFlipper;

    const v2, 0x7f0b0031    # com.konka.mm.R.id.page1

    invoke-virtual {v0, v2}, Landroid/widget/ViewFlipper;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    aput-object v0, v1, v7

    iget-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->pageGridView:[Landroid/widget/GridView;

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->viewFilpper:Landroid/widget/ViewFlipper;

    const v2, 0x7f0b0032    # com.konka.mm.R.id.page2

    invoke-virtual {v0, v2}, Landroid/widget/ViewFlipper;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    aput-object v0, v1, v8

    iget-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->pageGridView:[Landroid/widget/GridView;

    const/4 v2, 0x2

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->viewFilpper:Landroid/widget/ViewFlipper;

    const v3, 0x7f0b0033    # com.konka.mm.R.id.page3

    invoke-virtual {v0, v3}, Landroid/widget/ViewFlipper;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    aput-object v0, v1, v2

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->pageGridView:[Landroid/widget/GridView;

    aget-object v0, v0, v7

    invoke-virtual {v0, p0}, Landroid/widget/GridView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->pageGridView:[Landroid/widget/GridView;

    aget-object v0, v0, v8

    invoke-virtual {v0, p0}, Landroid/widget/GridView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->pageGridView:[Landroid/widget/GridView;

    const/4 v1, 0x2

    aget-object v0, v0, v1

    invoke-virtual {v0, p0}, Landroid/widget/GridView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    const v0, 0x7f040005    # com.konka.mm.R.anim.photo_push_left_in

    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->mPushLeftInAnim:Landroid/view/animation/Animation;

    const v0, 0x7f040009    # com.konka.mm.R.anim.push_left_out

    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->mPushLeftOutAnim:Landroid/view/animation/Animation;

    const v0, 0x7f040006    # com.konka.mm.R.anim.photo_push_right_in

    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->mPustRightInAnim:Landroid/view/animation/Animation;

    const v0, 0x7f04000a    # com.konka.mm.R.anim.push_right_out

    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->mPustRightOutAnim:Landroid/view/animation/Animation;

    const v0, 0x7f0b0018    # com.konka.mm.R.id.file_list_path

    invoke-virtual {p0, v0}, Lcom/konka/mm/filemanager/FileListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->diskPath:Landroid/widget/LinearLayout;

    const v0, 0x7f0b002e    # com.konka.mm.R.id.page_linearlayout

    invoke-virtual {p0, v0}, Lcom/konka/mm/filemanager/FileListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->page_linearLayout:Landroid/widget/LinearLayout;

    const v0, 0x7f0b0013    # com.konka.mm.R.id.img_file_sort

    invoke-virtual {p0, v0}, Lcom/konka/mm/filemanager/FileListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->sort:Landroid/widget/Button;

    const v0, 0x7f0b0019    # com.konka.mm.R.id.list_btn

    invoke-virtual {p0, v0}, Lcom/konka/mm/filemanager/FileListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->list_btn:Landroid/widget/Button;

    const v0, 0x7f0b001a    # com.konka.mm.R.id.img_file_manager

    invoke-virtual {p0, v0}, Lcom/konka/mm/filemanager/FileListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->manager:Landroid/widget/Button;

    invoke-virtual {p0}, Lcom/konka/mm/filemanager/FileListActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030037    # com.konka.mm.R.layout.sort_popup_window_template

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->popupView:Landroid/view/View;

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->popupView:Landroid/view/View;

    const v1, 0x7f0b00b3    # com.konka.mm.R.id.rg_sort_btns

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    iput-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->sortBtns:Landroid/widget/RadioGroup;

    const v0, 0x7f0b001b    # com.konka.mm.R.id.tv_file_info

    invoke-virtual {p0, v0}, Lcom/konka/mm/filemanager/FileListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->fileInfo:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/konka/mm/filemanager/FileListActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030017    # com.konka.mm.R.layout.menu_delete_item

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->menuPopupView:Landroid/view/View;

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->menuPopupView:Landroid/view/View;

    const v1, 0x7f0b0058    # com.konka.mm.R.id.btn_menu_delete_sure

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->sureBtnMenu:Landroid/widget/Button;

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->menuPopupView:Landroid/view/View;

    const v1, 0x7f0b0059    # com.konka.mm.R.id.btn_menu_delete_cancle

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->cancleBtnMenu:Landroid/widget/Button;

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->menuPopupView:Landroid/view/View;

    const v1, 0x7f0b0057    # com.konka.mm.R.id.tv_menu_delete_info

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->infoMenu:Landroid/widget/TextView;

    invoke-virtual {p0, v4}, Lcom/konka/mm/filemanager/FileListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->delete:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v5}, Lcom/konka/mm/filemanager/FileListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->deleteAll:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v6}, Lcom/konka/mm/filemanager/FileListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->rename:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->delete:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setNextFocusRightId(I)V

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->deleteAll:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setNextFocusRightId(I)V

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->deleteAll:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setNextFocusLeftId(I)V

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->rename:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setNextFocusLeftId(I)V

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->rename:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setNextFocusRightId(I)V

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->delete:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setNextFocusLeftId(I)V

    const v0, 0x7f0b002f    # com.konka.mm.R.id.btn_list_left

    invoke-virtual {p0, v0}, Lcom/konka/mm/filemanager/FileListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->pageLeft:Landroid/widget/Button;

    const v0, 0x7f0b0034    # com.konka.mm.R.id.btn_list_right

    invoke-virtual {p0, v0}, Lcom/konka/mm/filemanager/FileListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->pageRight:Landroid/widget/Button;

    const v0, 0x7f0b0035    # com.konka.mm.R.id.tv_page_info

    invoke-virtual {p0, v0}, Lcom/konka/mm/filemanager/FileListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->pageInfomation:Landroid/widget/TextView;

    return-void
.end method

.method private getCurFile(I)Ljava/io/File;
    .locals 6
    .param p1    # I

    iget-boolean v3, p0, Lcom/konka/mm/filemanager/FileListActivity;->bNextPageKey:Z

    if-eqz v3, :cond_0

    if-eqz p1, :cond_0

    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/konka/mm/filemanager/FileListActivity;->bNextPageKey:Z

    const/4 v2, 0x0

    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    :try_start_0
    iget-object v3, p0, Lcom/konka/mm/filemanager/FileListActivity;->infos:Ljava/util/List;

    iget v4, p0, Lcom/konka/mm/filemanager/FileListActivity;->currentPage:I

    iget v5, p0, Lcom/konka/mm/filemanager/FileListActivity;->mCountPerPage:I

    mul-int/2addr v4, v5

    add-int/2addr v4, p1

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/io/File;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v3, "FileListActivity"

    const-string v4, "getCurFile()"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private getListFromFiles([Ljava/io/File;)Ljava/util/List;
    .locals 7
    .param p1    # [Ljava/io/File;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/io/File;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    const/4 v2, 0x0

    if-eqz p1, :cond_0

    array-length v4, p1

    if-lez v4, :cond_0

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    array-length v3, p1

    const/4 v1, 0x0

    :goto_0
    if-lt v1, v3, :cond_1

    :cond_0
    return-object v2

    :cond_1
    aget-object v0, p1, v1

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    iget-object v6, p0, Lcom/konka/mm/filemanager/FileListActivity;->root_path:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, "/.multimediaThumbs"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    iget-object v6, p0, Lcom/konka/mm/filemanager/FileListActivity;->root_path:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "kk.com.konka.mm.ImgCach"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    iget-object v6, p0, Lcom/konka/mm/filemanager/FileListActivity;->root_path:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "$RECYCLE.BIN"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private getPathIndexInList(Lcom/konka/mm/model/PathItem;)I
    .locals 3
    .param p1    # Lcom/konka/mm/model/PathItem;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/konka/mm/filemanager/FileListActivity;->pathList:Ljava/util/List;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/konka/mm/filemanager/FileListActivity;->pathList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/konka/mm/filemanager/FileListActivity;->pathList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lt v0, v2, :cond_1

    :cond_0
    return v1

    :cond_1
    iget-object v2, p0, Lcom/konka/mm/filemanager/FileListActivity;->pathList:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    move v1, v0

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private init()V
    .locals 2

    invoke-direct {p0}, Lcom/konka/mm/filemanager/FileListActivity;->findViews()V

    invoke-direct {p0}, Lcom/konka/mm/filemanager/FileListActivity;->setListeners()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->pathList:Ljava/util/List;

    new-instance v0, Lcom/konka/mm/utils/LinuxFileCommand;

    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/konka/mm/utils/LinuxFileCommand;-><init>(Ljava/lang/Runtime;)V

    iput-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->linux:Lcom/konka/mm/utils/LinuxFileCommand;

    new-instance v0, Lcom/konka/mm/filemanager/FileItemClickListener;

    invoke-direct {v0, p0}, Lcom/konka/mm/filemanager/FileItemClickListener;-><init>(Lcom/konka/mm/filemanager/FileListActivity;)V

    iput-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->listListener:Lcom/konka/mm/filemanager/FileItemClickListener;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->isEnterOtherAct:Z

    const/4 v0, 0x0

    iput v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->sortType:I

    invoke-virtual {p0}, Lcom/konka/mm/filemanager/FileListActivity;->broweToRoot()V

    return-void
.end method

.method private initPath(Ljava/io/File;I)V
    .locals 13
    .param p1    # Ljava/io/File;
    .param p2    # I

    const/4 v11, 0x2

    if-eqz p1, :cond_0

    :try_start_0
    iget-object v10, p0, Lcom/konka/mm/filemanager/FileListActivity;->diskPath:Landroid/widget/LinearLayout;

    if-eqz v10, :cond_0

    packed-switch p2, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    new-instance v9, Landroid/widget/TextView;

    invoke-direct {v9, p0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    const/4 v10, 0x1

    const/high16 v11, 0x41d80000    # 27.0f

    invoke-virtual {v9, v10, v11}, Landroid/widget/TextView;->setTextSize(IF)V

    invoke-virtual {p0}, Lcom/konka/mm/filemanager/FileListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f06000d    # com.konka.mm.R.color.file_manager_title_white

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getColor(I)I

    move-result v10

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setTextColor(I)V

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setFocusable(Z)V

    const-string v10, " > "

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v10, p0, Lcom/konka/mm/filemanager/FileListActivity;->diskPath:Landroid/widget/LinearLayout;

    invoke-virtual {v10, v9}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v10

    const v11, 0x7f030023    # com.konka.mm.R.layout.path_btn

    const/4 v12, 0x0

    invoke-virtual {v10, v11, v12}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/Button;

    const/4 v10, 0x1

    const/high16 v11, 0x41d80000    # 27.0f

    invoke-virtual {v6, v10, v11}, Landroid/widget/Button;->setTextSize(IF)V

    const/4 v10, 0x0

    invoke-virtual {v6, v10}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v4, p0, Lcom/konka/mm/filemanager/FileListActivity;->diskName:Ljava/lang/String;

    iget-object v10, p0, Lcom/konka/mm/filemanager/FileListActivity;->root_path:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v11

    invoke-virtual {v11}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_1

    invoke-virtual {p1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v10

    invoke-virtual {v10}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v3

    const/16 v5, 0xf

    sub-int v10, v3, v5

    const/4 v11, 0x5

    if-le v10, v11, :cond_1

    new-instance v10, Ljava/lang/StringBuilder;

    const/4 v11, 0x0

    const/16 v12, 0xa

    invoke-virtual {v4, v11, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v11, "..."

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    :cond_1
    invoke-virtual {v6, v4}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    new-instance v2, Lcom/konka/mm/model/PathItem;

    invoke-virtual {p1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v10

    invoke-virtual {v10}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v2, v10, v6, v9}, Lcom/konka/mm/model/PathItem;-><init>(Ljava/lang/String;Landroid/widget/Button;Landroid/widget/TextView;)V

    new-instance v10, Lcom/konka/mm/filemanager/FileListActivity$PathBtnClickListener;

    invoke-direct {v10, p0, v2}, Lcom/konka/mm/filemanager/FileListActivity$PathBtnClickListener;-><init>(Lcom/konka/mm/filemanager/FileListActivity;Lcom/konka/mm/model/PathItem;)V

    invoke-virtual {v6, v10}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v10, p0, Lcom/konka/mm/filemanager/FileListActivity;->diskPath:Landroid/widget/LinearLayout;

    invoke-virtual {v10, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    iget-object v10, p0, Lcom/konka/mm/filemanager/FileListActivity;->pathList:Ljava/util/List;

    if-eqz v10, :cond_2

    iget-object v10, p0, Lcom/konka/mm/filemanager/FileListActivity;->pathList:Ljava/util/List;

    invoke-interface {v10, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    iget-object v10, p0, Lcom/konka/mm/filemanager/FileListActivity;->diskPath:Landroid/widget/LinearLayout;

    const/4 v11, 0x2

    invoke-virtual {v10, v11}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v10

    new-instance v11, Lcom/konka/mm/filemanager/FileListActivity$12;

    invoke-direct {v11, p0}, Lcom/konka/mm/filemanager/FileListActivity$12;-><init>(Lcom/konka/mm/filemanager/FileListActivity;)V

    invoke-virtual {v10, v11}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    :pswitch_1
    :try_start_1
    iget-object v10, p0, Lcom/konka/mm/filemanager/FileListActivity;->diskPath:Landroid/widget/LinearLayout;

    if-eqz v10, :cond_0

    iget-object v10, p0, Lcom/konka/mm/filemanager/FileListActivity;->diskPath:Landroid/widget/LinearLayout;

    invoke-virtual {v10}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v10

    if-le v10, v11, :cond_0

    iget-object v10, p0, Lcom/konka/mm/filemanager/FileListActivity;->pathList:Ljava/util/List;

    if-eqz v10, :cond_0

    iget-object v10, p0, Lcom/konka/mm/filemanager/FileListActivity;->pathList:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v10

    if-lez v10, :cond_0

    iget-object v10, p0, Lcom/konka/mm/filemanager/FileListActivity;->pathList:Ljava/util/List;

    iget-object v11, p0, Lcom/konka/mm/filemanager/FileListActivity;->pathList:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v11

    add-int/lit8 v11, v11, -0x1

    invoke-interface {v10, v11}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/konka/mm/model/PathItem;

    iget-object v10, p0, Lcom/konka/mm/filemanager/FileListActivity;->diskPath:Landroid/widget/LinearLayout;

    invoke-virtual {v7}, Lcom/konka/mm/model/PathItem;->getBtn()Landroid/widget/Button;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    iget-object v10, p0, Lcom/konka/mm/filemanager/FileListActivity;->diskPath:Landroid/widget/LinearLayout;

    invoke-virtual {v7}, Lcom/konka/mm/model/PathItem;->getTextview()Landroid/widget/TextView;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    goto/16 :goto_0

    :pswitch_2
    iget-object v10, p0, Lcom/konka/mm/filemanager/FileListActivity;->diskPath:Landroid/widget/LinearLayout;

    if-eqz v10, :cond_0

    iget-object v10, p0, Lcom/konka/mm/filemanager/FileListActivity;->diskPath:Landroid/widget/LinearLayout;

    invoke-virtual {v10}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v10

    if-le v10, v11, :cond_0

    iget-object v10, p0, Lcom/konka/mm/filemanager/FileListActivity;->pathList:Ljava/util/List;

    if-eqz v10, :cond_0

    iget-object v10, p0, Lcom/konka/mm/filemanager/FileListActivity;->pathList:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v8

    if-lez v8, :cond_0

    iget v10, p0, Lcom/konka/mm/filemanager/FileListActivity;->pathIndex:I

    add-int/lit8 v1, v10, 0x1

    :goto_1
    if-ge v1, v8, :cond_0

    iget-object v10, p0, Lcom/konka/mm/filemanager/FileListActivity;->pathList:Ljava/util/List;

    invoke-interface {v10, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/konka/mm/model/PathItem;

    iget-object v10, p0, Lcom/konka/mm/filemanager/FileListActivity;->diskPath:Landroid/widget/LinearLayout;

    invoke-virtual {v7}, Lcom/konka/mm/model/PathItem;->getBtn()Landroid/widget/Button;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    iget-object v10, p0, Lcom/konka/mm/filemanager/FileListActivity;->diskPath:Landroid/widget/LinearLayout;

    invoke-virtual {v7}, Lcom/konka/mm/model/PathItem;->getTextview()Landroid/widget/TextView;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    add-int/lit8 v1, v1, -0x1

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private onReceiveSdCardBroadCast(Landroid/content/Intent;)V
    .locals 2
    .param p1    # Landroid/content/Intent;

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->root_path:Ljava/lang/String;

    invoke-static {p0, v1}, Lcom/konka/mm/tools/FileTool;->checkUsbExist(Landroid/app/Activity;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/konka/mm/filemanager/FileListActivity;->finish()V

    :cond_0
    return-void
.end method

.method private setBtnFocusable(Z)V
    .locals 3
    .param p1    # Z

    iget-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->list_btn:Landroid/widget/Button;

    invoke-virtual {v1, p1}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->manager:Landroid/widget/Button;

    invoke-virtual {v1, p1}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->sort:Landroid/widget/Button;

    invoke-virtual {v1, p1}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->pathList:Ljava/util/List;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->pathList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    iget-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->pathList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    if-eqz p1, :cond_0

    iget-boolean v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->isFocusInGrid:Z

    if-eqz v1, :cond_2

    :cond_0
    :goto_1
    return-void

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/konka/mm/model/PathItem;

    invoke-virtual {v0}, Lcom/konka/mm/model/PathItem;->getBtn()Landroid/widget/Button;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/widget/Button;->setFocusable(Z)V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->pathList:Ljava/util/List;

    iget-object v2, p0, Lcom/konka/mm/filemanager/FileListActivity;->pathList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/konka/mm/model/PathItem;

    invoke-virtual {v1}, Lcom/konka/mm/model/PathItem;->getBtn()Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Button;->requestFocus()Z

    goto :goto_1
.end method

.method private setLeftBtnFocuse()Z
    .locals 4

    const/4 v2, 0x1

    iget-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->pathList:Ljava/util/List;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->pathList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_1

    iget-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->manager:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->isFocused()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->pathList:Ljava/util/List;

    iget-object v3, p0, Lcom/konka/mm/filemanager/FileListActivity;->pathList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/konka/mm/model/PathItem;

    invoke-virtual {v1}, Lcom/konka/mm/model/PathItem;->getBtn()Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Button;->requestFocus()Z

    move v1, v2

    :goto_0
    return v1

    :cond_0
    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->pathList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-le v0, v1, :cond_2

    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->pathList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/konka/mm/model/PathItem;

    invoke-virtual {v1}, Lcom/konka/mm/model/PathItem;->getBtn()Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Button;->isFocused()Z

    move-result v1

    if-eqz v1, :cond_3

    if-eqz v0, :cond_3

    iget-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->pathList:Ljava/util/List;

    add-int/lit8 v3, v0, -0x1

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/konka/mm/model/PathItem;

    invoke-virtual {v1}, Lcom/konka/mm/model/PathItem;->getBtn()Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Button;->requestFocus()Z

    move v1, v2

    goto :goto_0

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method private setListeners()V
    .locals 3

    iget-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->sureBtnMenu:Landroid/widget/Button;

    iget-object v2, p0, Lcom/konka/mm/filemanager/FileListActivity;->menuClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->cancleBtnMenu:Landroid/widget/Button;

    iget-object v2, p0, Lcom/konka/mm/filemanager/FileListActivity;->menuClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->sort:Landroid/widget/Button;

    iget-object v2, p0, Lcom/konka/mm/filemanager/FileListActivity;->clickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->list_btn:Landroid/widget/Button;

    iget-object v2, p0, Lcom/konka/mm/filemanager/FileListActivity;->clickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->manager:Landroid/widget/Button;

    iget-object v2, p0, Lcom/konka/mm/filemanager/FileListActivity;->clickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->sortBtns:Landroid/widget/RadioGroup;

    iget-object v2, p0, Lcom/konka/mm/filemanager/FileListActivity;->changeListener:Landroid/widget/RadioGroup$OnCheckedChangeListener;

    invoke-virtual {v1, v2}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    iget-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->delete:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/konka/mm/filemanager/FileListActivity;->clickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->rename:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/konka/mm/filemanager/FileListActivity;->clickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->deleteAll:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/konka/mm/filemanager/FileListActivity;->clickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->pageLeft:Landroid/widget/Button;

    iget-object v2, p0, Lcom/konka/mm/filemanager/FileListActivity;->clickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->pageRight:Landroid/widget/Button;

    iget-object v2, p0, Lcom/konka/mm/filemanager/FileListActivity;->clickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v1, Lcom/konka/mm/filemanager/FileListActivity$MyObservable;

    invoke-direct {v1, p0}, Lcom/konka/mm/filemanager/FileListActivity$MyObservable;-><init>(Lcom/konka/mm/filemanager/FileListActivity;)V

    iput-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->observable:Lcom/konka/mm/filemanager/FileListActivity$MyObservable;

    iget-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->observable:Lcom/konka/mm/filemanager/FileListActivity$MyObservable;

    new-instance v2, Lcom/konka/mm/filemanager/FileListActivity$9;

    invoke-direct {v2, p0}, Lcom/konka/mm/filemanager/FileListActivity$9;-><init>(Lcom/konka/mm/filemanager/FileListActivity;)V

    invoke-virtual {v1, v2}, Lcom/konka/mm/filemanager/FileListActivity$MyObservable;->addObserver(Ljava/util/Observer;)V

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    new-instance v1, Lcom/konka/mm/filemanager/FileListActivity$10;

    invoke-direct {v1, p0}, Lcom/konka/mm/filemanager/FileListActivity$10;-><init>(Lcom/konka/mm/filemanager/FileListActivity;)V

    iput-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    iget-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/konka/mm/filemanager/FileListActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method private setRightBtnFocuse()Z
    .locals 4

    const/4 v2, 0x1

    iget-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->pathList:Ljava/util/List;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->pathList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->pathList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-le v0, v1, :cond_1

    :cond_0
    const/4 v1, 0x0

    :goto_1
    return v1

    :cond_1
    iget-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->pathList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/konka/mm/model/PathItem;

    invoke-virtual {v1}, Lcom/konka/mm/model/PathItem;->getBtn()Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Button;->isFocused()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->pathList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    iget-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->pathList:Ljava/util/List;

    add-int/lit8 v3, v0, 0x1

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/konka/mm/model/PathItem;

    invoke-virtual {v1}, Lcom/konka/mm/model/PathItem;->getBtn()Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Button;->requestFocus()Z

    move v1, v2

    goto :goto_1

    :cond_2
    iget-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->pathList:Ljava/util/List;

    iget-object v3, p0, Lcom/konka/mm/filemanager/FileListActivity;->pathList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/konka/mm/model/PathItem;

    invoke-virtual {v1}, Lcom/konka/mm/model/PathItem;->getBtn()Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Button;->isFocused()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->manager:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->requestFocus()Z

    move v1, v2

    goto :goto_1

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private setThreeButtonFocusable(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->delete:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->deleteAll:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->rename:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    return-void
.end method

.method private showDeleteDialog()V
    .locals 3

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f090097    # com.konka.mm.R.string.mention_info

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x108009b    # android.R.drawable.ic_dialog_info

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090096    # com.konka.mm.R.string.delete_info

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090084    # com.konka.mm.R.string.FILE_MENU_SURE

    new-instance v2, Lcom/konka/mm/filemanager/FileListActivity$13;

    invoke-direct {v2, p0}, Lcom/konka/mm/filemanager/FileListActivity$13;-><init>(Lcom/konka/mm/filemanager/FileListActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090085    # com.konka.mm.R.string.FILE_MENU_CANCEL

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    return-void
.end method

.method private sortFiles([Ljava/io/File;)[Ljava/io/File;
    .locals 2
    .param p1    # [Ljava/io/File;

    const/4 v0, 0x0

    iget v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->sortType:I

    packed-switch v1, :pswitch_data_0

    :goto_0
    return-object v0

    :pswitch_0
    move-object v0, p1

    goto :goto_0

    :pswitch_1
    invoke-static {p1}, Lcom/konka/mm/tools/FileTool;->getSortFilesByName([Ljava/io/File;)[Ljava/io/File;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    invoke-static {p1}, Lcom/konka/mm/tools/FileTool;->getSortFilesBySize([Ljava/io/File;)[Ljava/io/File;

    move-result-object v0

    goto :goto_0

    :pswitch_3
    invoke-static {p1}, Lcom/konka/mm/tools/FileTool;->getSortFilesByType([Ljava/io/File;)[Ljava/io/File;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public broweTo(Ljava/lang/String;I)V
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # I

    const v3, 0x7f09008d    # com.konka.mm.R.string.FILE_LOADING

    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->parentFile:Ljava/io/File;

    iget-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->parentFile:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->parentFile:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->mkdir()Z

    :cond_0
    iget-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->root_path:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    iget-object v2, p0, Lcom/konka/mm/filemanager/FileListActivity;->parentFile:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-le v1, v2, :cond_1

    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/konka/mm/filemanager/FileListActivity;->root_path:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->parentFile:Ljava/io/File;

    :cond_1
    iget-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->parentFile:Ljava/io/File;

    new-instance v2, Lcom/konka/mm/filemanager/FileManagerFilter;

    invoke-direct {v2, p0}, Lcom/konka/mm/filemanager/FileManagerFilter;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v2}, Ljava/io/File;->listFiles(Ljava/io/FileFilter;)[Ljava/io/File;

    move-result-object v0

    if-eqz v0, :cond_3

    array-length v1, v0

    if-lez v1, :cond_3

    const/4 v1, 0x0

    aget-object v1, v0, v1

    iput-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->curFile:Ljava/io/File;

    iget-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->curFile:Ljava/io/File;

    invoke-direct {p0, v1, p2}, Lcom/konka/mm/filemanager/FileListActivity;->initPath(Ljava/io/File;I)V

    invoke-virtual {p0, v3, v3}, Lcom/konka/mm/filemanager/FileListActivity;->showProgressDialog(II)V

    const/4 v1, 0x1

    iput v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->isFirst:I

    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/konka/mm/filemanager/FileListActivity$SortSizeThread;

    iget v3, p0, Lcom/konka/mm/filemanager/FileListActivity;->isFirst:I

    invoke-direct {v2, p0, v0, v3, p2}, Lcom/konka/mm/filemanager/FileListActivity$SortSizeThread;-><init>(Lcom/konka/mm/filemanager/FileListActivity;[Ljava/io/File;II)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    :cond_2
    :goto_0
    return-void

    :cond_3
    invoke-virtual {p0}, Lcom/konka/mm/filemanager/FileListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09008e    # com.konka.mm.R.string.directory_empty

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x3e8

    invoke-static {p0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    iget-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->root_path:Ljava/lang/String;

    iget-object v2, p0, Lcom/konka/mm/filemanager/FileListActivity;->parentFile:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->root_path:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    iget-object v2, p0, Lcom/konka/mm/filemanager/FileListActivity;->parentFile:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-le v1, v2, :cond_6

    :cond_4
    iget-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->viewFilpper:Landroid/widget/ViewFlipper;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->viewFilpper:Landroid/widget/ViewFlipper;

    invoke-virtual {v1}, Landroid/widget/ViewFlipper;->removeAllViews()V

    :cond_5
    iget-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->pageInfomation:Landroid/widget/TextView;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->pageInfomation:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "0/0 "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/konka/mm/filemanager/FileListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f09009a    # com.konka.mm.R.string.page

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_6
    iget-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->parentFile:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->parentFile:Ljava/io/File;

    goto :goto_0
.end method

.method public broweToRoot()V
    .locals 3

    const/4 v2, 0x0

    invoke-static {p0}, Lcom/konka/mm/tools/FileTool;->usbIsExists(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/konka/mm/filemanager/FileListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "sortType"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->sortType:I

    invoke-virtual {p0}, Lcom/konka/mm/filemanager/FileListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "root_path"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->root_path:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/konka/mm/filemanager/FileListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "diskName"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->diskName:Ljava/lang/String;

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->root_path:Ljava/lang/String;

    invoke-virtual {p0, v0, v2}, Lcom/konka/mm/filemanager/FileListActivity;->broweTo(Ljava/lang/String;I)V

    :goto_0
    return-void

    :cond_0
    invoke-static {p0}, Lcom/konka/mm/finals/CommonFinals;->sdCardNoFound(Landroid/app/Activity;)V

    goto :goto_0
.end method

.method public cancelProgressDlg()V
    .locals 0

    return-void
.end method

.method public checkScreenMode()V
    .locals 3

    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    invoke-virtual {p0}, Lcom/konka/mm/filemanager/FileListActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    iget v1, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->screenWidth:I

    iget v1, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    iput v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->screenHeight:I

    iget v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->screenWidth:I

    iget v2, p0, Lcom/konka/mm/filemanager/FileListActivity;->screenWidth_720p:I

    if-ne v1, v2, :cond_0

    iget v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->screenHeight:I

    iget v2, p0, Lcom/konka/mm/filemanager/FileListActivity;->screenHeight_720p:I

    if-ne v1, v2, :cond_0

    const/4 v1, 0x0

    iput v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->mScreen_mode:I

    :goto_0
    return-void

    :cond_0
    const/4 v1, 0x1

    iput v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->mScreen_mode:I

    goto :goto_0
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # Landroid/view/KeyEvent;

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    invoke-super {p0, p1}, Landroid/app/Activity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    return v0

    :pswitch_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->isEnterOtherAct:Z

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method

.method public getListCurPos(Ljava/lang/String;Ljava/util/ArrayList;)I
    .locals 4
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)I"
        }
    .end annotation

    const/4 v0, 0x0

    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    return v0

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public initFileInfo(Ljava/io/File;)V
    .locals 7
    .param p1    # Ljava/io/File;

    const-wide/16 v2, 0x0

    const-string v1, ""

    if-eqz p1, :cond_3

    :try_start_0
    invoke-virtual {p1}, Ljava/io/File;->isFile()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-static {p1}, Lcom/konka/mm/tools/FileTool;->getFileSizes(Ljava/io/File;)J

    move-result-wide v2

    iget-object v4, p0, Lcom/konka/mm/filemanager/FileListActivity;->fileInfo:Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, "   "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v2, v3}, Lcom/konka/mm/tools/FileTool;->FormetFileSize(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/konka/mm/filemanager/FileListActivity;->fileInfo:Landroid/widget/TextView;

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/konka/mm/filemanager/FileListActivity;->fileInfoTask:Lcom/konka/mm/filemanager/FileListActivity$FileInfoTask;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/konka/mm/filemanager/FileListActivity;->fileInfoTask:Lcom/konka/mm/filemanager/FileListActivity$FileInfoTask;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/konka/mm/filemanager/FileListActivity$FileInfoTask;->cancel(Z)Z

    :cond_2
    new-instance v4, Lcom/konka/mm/filemanager/FileListActivity$FileInfoTask;

    iget-object v5, p0, Lcom/konka/mm/filemanager/FileListActivity;->fileInfo:Landroid/widget/TextView;

    invoke-direct {v4, p0, v5, p1}, Lcom/konka/mm/filemanager/FileListActivity$FileInfoTask;-><init>(Lcom/konka/mm/filemanager/FileListActivity;Landroid/widget/TextView;Ljava/io/File;)V

    iput-object v4, p0, Lcom/konka/mm/filemanager/FileListActivity;->fileInfoTask:Lcom/konka/mm/filemanager/FileListActivity$FileInfoTask;

    iget-object v4, p0, Lcom/konka/mm/filemanager/FileListActivity;->fileInfoTask:Lcom/konka/mm/filemanager/FileListActivity$FileInfoTask;

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v4, v5}, Lcom/konka/mm/filemanager/FileListActivity$FileInfoTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    :cond_3
    :try_start_1
    iget-object v4, p0, Lcom/konka/mm/filemanager/FileListActivity;->fileInfo:Landroid/widget/TextView;

    const-string v5, ""

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public initViewHandler()V
    .locals 1

    new-instance v0, Lcom/konka/mm/filemanager/FileListActivity$11;

    invoke-direct {v0, p0}, Lcom/konka/mm/filemanager/FileListActivity$11;-><init>(Lcom/konka/mm/filemanager/FileListActivity;)V

    iput-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->listViewHandler:Landroid/os/Handler;

    return-void
.end method

.method public intiGridView(Ljava/util/List;I)V
    .locals 18
    .param p2    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/io/File;",
            ">;I)V"
        }
    .end annotation

    const/4 v13, 0x1

    move/from16 v0, p2

    if-ne v0, v13, :cond_0

    :try_start_0
    move-object/from16 v0, p0

    iget v13, v0, Lcom/konka/mm/filemanager/FileListActivity;->currentPage:I

    move-object/from16 v0, p0

    iget v14, v0, Lcom/konka/mm/filemanager/FileListActivity;->mCountPerPage:I

    mul-int/2addr v13, v14

    move-object/from16 v0, p0

    iget v14, v0, Lcom/konka/mm/filemanager/FileListActivity;->curSelectedPos:I

    add-int v12, v13, v14

    new-instance v13, Lcom/konka/mm/model/PageInfo;

    invoke-direct {v13, v12}, Lcom/konka/mm/model/PageInfo;-><init>(I)V

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/konka/mm/filemanager/FileListActivity;->pageInfo:Lcom/konka/mm/model/PageInfo;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/konka/mm/filemanager/FileListActivity;->pageInfos:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/konka/mm/filemanager/FileListActivity;->pageInfo:Lcom/konka/mm/model/PageInfo;

    invoke-interface {v13, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput v13, v0, Lcom/konka/mm/filemanager/FileListActivity;->curSelectedPos:I

    sget v13, Lcom/konka/mm/filemanager/FileDiskActivity;->List_Mode:I

    const/4 v14, 0x1

    if-ne v13, v14, :cond_3

    const/16 v13, 0x21

    move-object/from16 v0, p0

    iput v13, v0, Lcom/konka/mm/filemanager/FileListActivity;->mCountPerPage:I

    const/4 v13, 0x3

    move-object/from16 v0, p0

    iput v13, v0, Lcom/konka/mm/filemanager/FileListActivity;->numColumns:I

    :goto_0
    const/4 v13, 0x1

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/konka/mm/filemanager/FileListActivity;->isFocusInGrid:Z

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput v13, v0, Lcom/konka/mm/filemanager/FileListActivity;->currentPage:I

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v13

    move-object/from16 v0, p0

    iput v13, v0, Lcom/konka/mm/filemanager/FileListActivity;->fileSize:I

    move-object/from16 v0, p0

    iget v13, v0, Lcom/konka/mm/filemanager/FileListActivity;->fileSize:I

    int-to-float v13, v13

    move-object/from16 v0, p0

    iget v14, v0, Lcom/konka/mm/filemanager/FileListActivity;->mCountPerPage:I

    int-to-float v14, v14

    div-float/2addr v13, v14

    float-to-double v13, v13

    invoke-static {v13, v14}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v13

    double-to-int v13, v13

    move-object/from16 v0, p0

    iput v13, v0, Lcom/konka/mm/filemanager/FileListActivity;->PageCount:I

    const/4 v1, 0x0

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget v13, v0, Lcom/konka/mm/filemanager/FileListActivity;->PageCount:I

    const/4 v14, 0x3

    if-lt v13, v14, :cond_4

    const/4 v3, 0x3

    :goto_1
    const/4 v5, 0x0

    const/4 v11, 0x0

    const/4 v13, 0x2

    move/from16 v0, p2

    if-ne v0, v13, :cond_2

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/konka/mm/filemanager/FileListActivity;->pageInfos:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/konka/mm/filemanager/FileListActivity;->pageInfos:Ljava/util/List;

    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v14

    add-int/lit8 v14, v14, -0x1

    invoke-interface {v13, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/konka/mm/model/PageInfo;

    invoke-virtual {v10}, Lcom/konka/mm/model/PageInfo;->getPos()I

    move-result v13

    move-object/from16 v0, p0

    iget v14, v0, Lcom/konka/mm/filemanager/FileListActivity;->mCountPerPage:I

    div-int v5, v13, v14

    move-object/from16 v0, p0

    iget v13, v0, Lcom/konka/mm/filemanager/FileListActivity;->mCountPerPage:I

    div-int v13, v5, v13

    move-object/from16 v0, p0

    iput v13, v0, Lcom/konka/mm/filemanager/FileListActivity;->currentPage:I

    if-nez v5, :cond_1

    const/4 v1, 0x0

    move-object/from16 v0, p0

    iget v13, v0, Lcom/konka/mm/filemanager/FileListActivity;->PageCount:I

    const/4 v14, 0x3

    if-lt v13, v14, :cond_5

    const/4 v3, 0x3

    :cond_1
    :goto_2
    if-lez v5, :cond_2

    const/4 v1, -0x1

    move-object/from16 v0, p0

    iget v13, v0, Lcom/konka/mm/filemanager/FileListActivity;->PageCount:I

    add-int/lit8 v13, v13, -0x1

    if-ne v13, v5, :cond_6

    const/4 v3, 0x2

    :cond_2
    :goto_3
    move-object/from16 v0, p0

    iget v4, v0, Lcom/konka/mm/filemanager/FileListActivity;->curScreen:I

    :goto_4
    move-object/from16 v0, p0

    iget v13, v0, Lcom/konka/mm/filemanager/FileListActivity;->curScreen:I

    add-int/2addr v13, v3

    if-lt v4, v13, :cond_7

    const/4 v13, 0x2

    move/from16 v0, p2

    if-ne v0, v13, :cond_c

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/konka/mm/filemanager/FileListActivity;->pageInfos:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/konka/mm/filemanager/FileListActivity;->pageInfos:Ljava/util/List;

    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v14

    add-int/lit8 v14, v14, -0x1

    invoke-interface {v13, v14}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/konka/mm/model/PageInfo;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/konka/mm/filemanager/FileListActivity;->viewFilpper:Landroid/widget/ViewFlipper;

    invoke-virtual {v13}, Landroid/widget/ViewFlipper;->getCurrentView()Landroid/view/View;

    move-result-object v13

    const/4 v14, 0x1

    invoke-virtual {v13, v14}, Landroid/view/View;->setFocusable(Z)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/konka/mm/filemanager/FileListActivity;->viewFilpper:Landroid/widget/ViewFlipper;

    invoke-virtual {v13}, Landroid/widget/ViewFlipper;->getCurrentView()Landroid/view/View;

    move-result-object v13

    invoke-virtual {v13}, Landroid/view/View;->requestFocus()Z

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/konka/mm/filemanager/FileListActivity;->pageGridView:[Landroid/widget/GridView;

    move-object/from16 v0, p0

    iget v14, v0, Lcom/konka/mm/filemanager/FileListActivity;->curScreen:I

    aget-object v13, v13, v14

    invoke-virtual {v13, v11}, Landroid/widget/GridView;->setSelection(I)V

    invoke-virtual {v8}, Lcom/konka/mm/model/PageInfo;->getPos()I

    move-result v13

    move-object/from16 v0, p0

    iget v14, v0, Lcom/konka/mm/filemanager/FileListActivity;->mCountPerPage:I

    div-int v6, v13, v14

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/konka/mm/filemanager/FileListActivity;->pageGridView:[Landroid/widget/GridView;

    move-object/from16 v0, p0

    iget v14, v0, Lcom/konka/mm/filemanager/FileListActivity;->curScreen:I

    aget-object v13, v13, v14

    invoke-virtual {v8}, Lcom/konka/mm/model/PageInfo;->getPos()I

    move-result v14

    move-object/from16 v0, p0

    iget v15, v0, Lcom/konka/mm/filemanager/FileListActivity;->mCountPerPage:I

    rem-int/2addr v14, v15

    invoke-virtual {v13, v14}, Landroid/widget/GridView;->setSelection(I)V

    invoke-virtual {v8}, Lcom/konka/mm/model/PageInfo;->getPos()I

    move-result v13

    move-object/from16 v0, p0

    iget v14, v0, Lcom/konka/mm/filemanager/FileListActivity;->mCountPerPage:I

    div-int/2addr v13, v14

    move-object/from16 v0, p0

    iput v13, v0, Lcom/konka/mm/filemanager/FileListActivity;->currentPage:I

    :goto_5
    const/4 v13, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/konka/mm/filemanager/FileListActivity;->getCurFile(I)Ljava/io/File;

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/konka/mm/filemanager/FileListActivity;->curFile:Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/konka/mm/filemanager/FileListActivity;->curFile:Ljava/io/File;

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/konka/mm/filemanager/FileListActivity;->initFileInfo(Ljava/io/File;)V

    invoke-virtual/range {p0 .. p0}, Lcom/konka/mm/filemanager/FileListActivity;->setPageInfo()V

    :goto_6
    return-void

    :cond_3
    const/16 v13, 0x12

    move-object/from16 v0, p0

    iput v13, v0, Lcom/konka/mm/filemanager/FileListActivity;->mCountPerPage:I

    const/4 v13, 0x6

    move-object/from16 v0, p0

    iput v13, v0, Lcom/konka/mm/filemanager/FileListActivity;->numColumns:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_6

    :cond_4
    :try_start_1
    move-object/from16 v0, p0

    iget v3, v0, Lcom/konka/mm/filemanager/FileListActivity;->PageCount:I

    goto/16 :goto_1

    :cond_5
    move-object/from16 v0, p0

    iget v3, v0, Lcom/konka/mm/filemanager/FileListActivity;->PageCount:I

    goto/16 :goto_2

    :cond_6
    const/4 v3, 0x3

    goto/16 :goto_3

    :cond_7
    add-int v13, v5, v4

    move-object/from16 v0, p0

    iget v14, v0, Lcom/konka/mm/filemanager/FileListActivity;->curScreen:I

    sub-int/2addr v13, v14

    add-int v7, v13, v1

    move-object/from16 v0, p0

    iget v13, v0, Lcom/konka/mm/filemanager/FileListActivity;->PageCount:I

    add-int/lit8 v13, v13, -0x1

    if-ge v7, v13, :cond_9

    move-object/from16 v0, p0

    iget v9, v0, Lcom/konka/mm/filemanager/FileListActivity;->mCountPerPage:I

    :cond_8
    :goto_7
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/konka/mm/filemanager/FileListActivity;->pageGridView:[Landroid/widget/GridView;

    add-int v14, v4, v1

    add-int/lit8 v14, v14, 0x3

    rem-int/lit8 v14, v14, 0x3

    aget-object v13, v13, v14

    const/16 v14, 0x14

    const/4 v15, 0x0

    const/16 v16, 0x14

    const/16 v17, 0x0

    invoke-virtual/range {v13 .. v17}, Landroid/widget/GridView;->setPadding(IIII)V

    sget v13, Lcom/konka/mm/filemanager/FileDiskActivity;->List_Mode:I

    const/4 v14, 0x1

    if-ne v13, v14, :cond_b

    move-object/from16 v0, p0

    iget v13, v0, Lcom/konka/mm/filemanager/FileListActivity;->mScreen_mode:I

    if-nez v13, :cond_a

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/konka/mm/filemanager/FileListActivity;->pageGridView:[Landroid/widget/GridView;

    add-int v14, v4, v1

    add-int/lit8 v14, v14, 0x3

    rem-int/lit8 v14, v14, 0x3

    aget-object v13, v13, v14

    const/16 v14, -0x14

    invoke-virtual {v13, v14}, Landroid/widget/GridView;->setVerticalSpacing(I)V

    :goto_8
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/konka/mm/filemanager/FileListActivity;->page_linearLayout:Landroid/widget/LinearLayout;

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    invoke-virtual/range {v13 .. v17}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    :goto_9
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/konka/mm/filemanager/FileListActivity;->pageGridView:[Landroid/widget/GridView;

    add-int v14, v4, v1

    add-int/lit8 v14, v14, 0x3

    rem-int/lit8 v14, v14, 0x3

    aget-object v13, v13, v14

    move-object/from16 v0, p0

    iget v14, v0, Lcom/konka/mm/filemanager/FileListActivity;->numColumns:I

    invoke-virtual {v13, v14}, Landroid/widget/GridView;->setNumColumns(I)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/konka/mm/filemanager/FileListActivity;->pageGridView:[Landroid/widget/GridView;

    add-int v14, v4, v1

    add-int/lit8 v14, v14, 0x3

    rem-int/lit8 v14, v14, 0x3

    aget-object v13, v13, v14

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/konka/mm/filemanager/FileListActivity;->gridOnKey:Landroid/view/View$OnKeyListener;

    invoke-virtual {v13, v14}, Landroid/widget/GridView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/konka/mm/filemanager/FileListActivity;->pageGridView:[Landroid/widget/GridView;

    add-int v14, v4, v1

    add-int/lit8 v14, v14, 0x3

    rem-int/lit8 v14, v14, 0x3

    aget-object v13, v13, v14

    new-instance v14, Lcom/konka/mm/adapters/FileListAdapter;

    move-object/from16 v0, p0

    iget v15, v0, Lcom/konka/mm/filemanager/FileListActivity;->mCountPerPage:I

    move-object/from16 v0, p0

    invoke-direct {v14, v0, v7, v9, v15}, Lcom/konka/mm/adapters/FileListAdapter;-><init>(Lcom/konka/mm/filemanager/FileListActivity;III)V

    invoke-virtual {v13, v14}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/konka/mm/filemanager/FileListActivity;->pageGridView:[Landroid/widget/GridView;

    add-int v14, v4, v1

    add-int/lit8 v14, v14, 0x3

    rem-int/lit8 v14, v14, 0x3

    aget-object v13, v13, v14

    new-instance v14, Landroid/graphics/drawable/ColorDrawable;

    const/4 v15, 0x0

    invoke-direct {v14, v15}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v13, v14}, Landroid/widget/GridView;->setSelector(Landroid/graphics/drawable/Drawable;)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/konka/mm/filemanager/FileListActivity;->pageGridView:[Landroid/widget/GridView;

    add-int v14, v4, v1

    add-int/lit8 v14, v14, 0x3

    rem-int/lit8 v14, v14, 0x3

    aget-object v13, v13, v14

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/konka/mm/filemanager/FileListActivity;->gridViewOnItemClick:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v13, v14}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/konka/mm/filemanager/FileListActivity;->pageGridView:[Landroid/widget/GridView;

    add-int v14, v4, v1

    add-int/lit8 v14, v14, 0x3

    rem-int/lit8 v14, v14, 0x3

    aget-object v13, v13, v14

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/widget/GridView;->setFocusable(Z)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/konka/mm/filemanager/FileListActivity;->pageGridView:[Landroid/widget/GridView;

    add-int v14, v4, v1

    add-int/lit8 v14, v14, 0x3

    rem-int/lit8 v14, v14, 0x3

    aget-object v13, v13, v14

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/konka/mm/filemanager/FileListActivity;->gridViewOnItemSelected:Landroid/widget/AdapterView$OnItemSelectedListener;

    invoke-virtual {v13, v14}, Landroid/widget/GridView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_4

    :cond_9
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v13

    move-object/from16 v0, p0

    iget v14, v0, Lcom/konka/mm/filemanager/FileListActivity;->mCountPerPage:I

    move-object/from16 v0, p0

    iget v15, v0, Lcom/konka/mm/filemanager/FileListActivity;->PageCount:I

    add-int/lit8 v15, v15, -0x1

    mul-int/2addr v14, v15

    sub-int v9, v13, v14

    move-object/from16 v0, p0

    iget v13, v0, Lcom/konka/mm/filemanager/FileListActivity;->mCountPerPage:I

    if-lt v9, v13, :cond_8

    move-object/from16 v0, p0

    iget v9, v0, Lcom/konka/mm/filemanager/FileListActivity;->mCountPerPage:I

    goto/16 :goto_7

    :cond_a
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/konka/mm/filemanager/FileListActivity;->pageGridView:[Landroid/widget/GridView;

    add-int v14, v4, v1

    add-int/lit8 v14, v14, 0x3

    rem-int/lit8 v14, v14, 0x3

    aget-object v13, v13, v14

    const/16 v14, -0x1e

    invoke-virtual {v13, v14}, Landroid/widget/GridView;->setVerticalSpacing(I)V

    goto/16 :goto_8

    :cond_b
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/konka/mm/filemanager/FileListActivity;->pageGridView:[Landroid/widget/GridView;

    add-int v14, v4, v1

    add-int/lit8 v14, v14, 0x3

    rem-int/lit8 v14, v14, 0x3

    aget-object v13, v13, v14

    const/16 v14, 0x12

    invoke-virtual {v13, v14}, Landroid/widget/GridView;->setVerticalSpacing(I)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/konka/mm/filemanager/FileListActivity;->page_linearLayout:Landroid/widget/LinearLayout;

    const/4 v14, 0x0

    const/16 v15, 0x14

    const/16 v16, 0x0

    const/16 v17, 0x0

    invoke-virtual/range {v13 .. v17}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    goto/16 :goto_9

    :cond_c
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/konka/mm/filemanager/FileListActivity;->viewFilpper:Landroid/widget/ViewFlipper;

    invoke-virtual {v13}, Landroid/widget/ViewFlipper;->getCurrentView()Landroid/view/View;

    move-result-object v13

    const/4 v14, 0x1

    invoke-virtual {v13, v14}, Landroid/view/View;->setFocusable(Z)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/konka/mm/filemanager/FileListActivity;->viewFilpper:Landroid/widget/ViewFlipper;

    invoke-virtual {v13}, Landroid/widget/ViewFlipper;->getCurrentView()Landroid/view/View;

    move-result-object v13

    invoke-virtual {v13}, Landroid/view/View;->requestFocus()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_5
.end method

.method public isCanOperate(Ljava/io/File;)Z
    .locals 3
    .param p1    # Ljava/io/File;

    const/4 v0, 0x0

    invoke-static {p0}, Lcom/konka/mm/tools/FileTool;->isRunningMusicService(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v1, Lcom/konka/mm/filemanager/FileListActivity;->PLAYING_MUSIC_PATH:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/konka/mm/filemanager/FileListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090073    # com.konka.mm.R.string.music_running

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    :goto_0
    return v0

    :cond_0
    sget-object v0, Lcom/konka/mm/filemanager/FileListActivity;->PLAYING_MUSIC_PATH:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p0}, Lcom/konka/mm/tools/FileTool;->killRunningServiceInfo(Landroid/content/Context;)V

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    const-string v0, "FileListActivity"

    const-string v1, "------------>onCreate()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f030006    # com.konka.mm.R.layout.file_list_1280x720

    invoke-virtual {p0, v0}, Lcom/konka/mm/filemanager/FileListActivity;->setContentView(I)V

    invoke-direct {p0}, Lcom/konka/mm/filemanager/FileListActivity;->checkIsRussian()V

    invoke-direct {p0}, Lcom/konka/mm/filemanager/FileListActivity;->init()V

    invoke-virtual {p0}, Lcom/konka/mm/filemanager/FileListActivity;->checkScreenMode()V

    invoke-virtual {p0}, Lcom/konka/mm/filemanager/FileListActivity;->initViewHandler()V

    sput-object p0, Lcom/konka/mm/filemanager/FileListActivity;->instance:Lcom/konka/mm/filemanager/FileListActivity;

    return-void
.end method

.method protected onDestroy()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/konka/mm/filemanager/FileListActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    :cond_0
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 5
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v4, -0x1

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_0
    :goto_0
    :sswitch_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    :cond_1
    :goto_1
    return v1

    :sswitch_1
    iput-boolean v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->isEnterOtherAct:Z

    iget-object v2, p0, Lcom/konka/mm/filemanager/FileListActivity;->root_path:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/konka/mm/filemanager/FileListActivity;->parentFile:Ljava/io/File;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/konka/mm/filemanager/FileListActivity;->root_path:Ljava/lang/String;

    iget-object v3, p0, Lcom/konka/mm/filemanager/FileListActivity;->parentFile:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    iput-boolean v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->isManager:Z

    iput v4, p0, Lcom/konka/mm/filemanager/FileListActivity;->curSelectedPos:I

    invoke-virtual {p0}, Lcom/konka/mm/filemanager/FileListActivity;->up2One()V

    iput-boolean v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->isEnterOtherAct:Z

    iget-boolean v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->isManager:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->popupWindow:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->popupWindow:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    goto :goto_1

    :sswitch_2
    iget-boolean v2, p0, Lcom/konka/mm/filemanager/FileListActivity;->isFocusInGrid:Z

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/konka/mm/filemanager/FileListActivity;->delete:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->isFocused()Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/konka/mm/filemanager/FileListActivity;->deleteAll:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->isFocused()Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/konka/mm/filemanager/FileListActivity;->rename:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->isFocused()Z

    move-result v2

    if-nez v2, :cond_3

    invoke-virtual {p0, v4}, Lcom/konka/mm/filemanager/FileListActivity;->snapScreen(I)V

    iget-boolean v2, p0, Lcom/konka/mm/filemanager/FileListActivity;->isFocusInGrid:Z

    if-eqz v2, :cond_2

    :goto_2
    invoke-direct {p0, v0}, Lcom/konka/mm/filemanager/FileListActivity;->setThreeButtonFocusable(Z)V

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_2

    :cond_3
    iget-boolean v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->isFocusInGrid:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/konka/mm/filemanager/FileListActivity;->setLeftBtnFocuse()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_1

    :sswitch_3
    iget-boolean v2, p0, Lcom/konka/mm/filemanager/FileListActivity;->isFocusInGrid:Z

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/konka/mm/filemanager/FileListActivity;->delete:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->isFocused()Z

    move-result v2

    if-nez v2, :cond_5

    iget-object v2, p0, Lcom/konka/mm/filemanager/FileListActivity;->deleteAll:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->isFocused()Z

    move-result v2

    if-nez v2, :cond_5

    iget-object v2, p0, Lcom/konka/mm/filemanager/FileListActivity;->rename:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->isFocused()Z

    move-result v2

    if-nez v2, :cond_5

    invoke-virtual {p0, v1}, Lcom/konka/mm/filemanager/FileListActivity;->snapScreen(I)V

    iget-boolean v2, p0, Lcom/konka/mm/filemanager/FileListActivity;->isFocusInGrid:Z

    if-eqz v2, :cond_4

    :goto_3
    invoke-direct {p0, v0}, Lcom/konka/mm/filemanager/FileListActivity;->setThreeButtonFocusable(Z)V

    goto/16 :goto_0

    :cond_4
    move v0, v1

    goto :goto_3

    :cond_5
    iget-boolean v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->isFocusInGrid:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/konka/mm/filemanager/FileListActivity;->setRightBtnFocuse()Z

    move-result v0

    if-eqz v0, :cond_0

    goto/16 :goto_1

    :sswitch_4
    iput-boolean v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->is3dKeyDown:Z

    goto/16 :goto_0

    :sswitch_5
    iget-object v2, p0, Lcom/konka/mm/filemanager/FileListActivity;->delete:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->isFocused()Z

    move-result v2

    if-nez v2, :cond_7

    iget-object v2, p0, Lcom/konka/mm/filemanager/FileListActivity;->deleteAll:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->isFocused()Z

    move-result v2

    if-nez v2, :cond_7

    iget-object v2, p0, Lcom/konka/mm/filemanager/FileListActivity;->rename:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->isFocused()Z

    move-result v2

    if-nez v2, :cond_7

    iput-boolean v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->isFocusInGrid:Z

    iget-boolean v2, p0, Lcom/konka/mm/filemanager/FileListActivity;->isFocusInGrid:Z

    if-eqz v2, :cond_6

    :goto_4
    invoke-direct {p0, v0}, Lcom/konka/mm/filemanager/FileListActivity;->setBtnFocusable(Z)V

    goto/16 :goto_0

    :cond_6
    move v0, v1

    goto :goto_4

    :cond_7
    iput-boolean v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->isFocusInGrid:Z

    iget-boolean v2, p0, Lcom/konka/mm/filemanager/FileListActivity;->isFocusInGrid:Z

    if-eqz v2, :cond_8

    :goto_5
    invoke-direct {p0, v0}, Lcom/konka/mm/filemanager/FileListActivity;->setBtnFocusable(Z)V

    goto/16 :goto_0

    :cond_8
    move v0, v1

    goto :goto_5

    :sswitch_6
    iput-boolean v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->isFocusInGrid:Z

    invoke-direct {p0, v1}, Lcom/konka/mm/filemanager/FileListActivity;->setThreeButtonFocusable(Z)V

    goto/16 :goto_0

    :sswitch_7
    invoke-direct {p0}, Lcom/konka/mm/filemanager/FileListActivity;->deleteFile()V

    goto/16 :goto_0

    :sswitch_8
    invoke-direct {p0}, Lcom/konka/mm/filemanager/FileListActivity;->deleteAllFile()V

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p0}, Lcom/konka/mm/filemanager/FileListActivity;->setRename()V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_1
        0x13 -> :sswitch_5
        0x14 -> :sswitch_6
        0x15 -> :sswitch_2
        0x16 -> :sswitch_3
        0x17 -> :sswitch_0
        0xb7 -> :sswitch_7
        0xb8 -> :sswitch_8
        0xb9 -> :sswitch_9
        0xce -> :sswitch_4
        0x205 -> :sswitch_4
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    iget-boolean v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->isEnterOtherAct:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->is3dKeyDown:Z

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/konka/mm/tools/FileTool;->killRunningServiceInfo(Landroid/content/Context;)V

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->is3dKeyDown:Z

    return-void
.end method

.method protected onResume()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->isEnterOtherAct:Z

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    return-void
.end method

.method protected onStop()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/view/MotionEvent;

    const/high16 v3, 0x42480000    # 50.0f

    const/4 v0, 0x1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_0
    invoke-super {p0, p2}, Landroid/app/Activity;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    :goto_1
    return v0

    :pswitch_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->startX:F

    goto :goto_0

    :pswitch_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    iget v2, p0, Lcom/konka/mm/filemanager/FileListActivity;->startX:F

    cmpl-float v1, v1, v2

    if-lez v1, :cond_1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    iget v2, p0, Lcom/konka/mm/filemanager/FileListActivity;->startX:F

    sub-float/2addr v1, v2

    cmpl-float v1, v1, v3

    if-lez v1, :cond_1

    const/4 v1, -0x1

    invoke-virtual {p0, v1}, Lcom/konka/mm/filemanager/FileListActivity;->snapScreen(I)V

    goto :goto_1

    :cond_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    iget v2, p0, Lcom/konka/mm/filemanager/FileListActivity;->startX:F

    cmpg-float v1, v1, v2

    if-gez v1, :cond_0

    iget v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->startX:F

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    sub-float/2addr v1, v2

    cmpl-float v1, v1, v3

    if-lez v1, :cond_0

    invoke-virtual {p0, v0}, Lcom/konka/mm/filemanager/FileListActivity;->snapScreen(I)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public rename(Ljava/lang/String;)V
    .locals 7
    .param p1    # Ljava/lang/String;

    iget-object v5, p0, Lcom/konka/mm/filemanager/FileListActivity;->pageGridView:[Landroid/widget/GridView;

    iget v6, p0, Lcom/konka/mm/filemanager/FileListActivity;->curScreen:I

    aget-object v5, v5, v6

    iget v6, p0, Lcom/konka/mm/filemanager/FileListActivity;->curSelectedPos:I

    invoke-virtual {v5, v6}, Landroid/widget/GridView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    const/4 v4, 0x0

    sget v5, Lcom/konka/mm/filemanager/FileDiskActivity;->List_Mode:I

    const/4 v6, 0x1

    if-ne v5, v6, :cond_0

    const v5, 0x7f0b0028    # com.konka.mm.R.id.textitem

    invoke-virtual {v1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    :goto_0
    const-string v5, "/"

    invoke-virtual {p1, v5}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {p1, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    if-nez v0, :cond_1

    :goto_1
    return-void

    :cond_0
    const v5, 0x7f0b0044    # com.konka.mm.R.id.tv_item_name

    invoke-virtual {v1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    goto :goto_0

    :cond_1
    iget v5, p0, Lcom/konka/mm/filemanager/FileListActivity;->currentPage:I

    iget v6, p0, Lcom/konka/mm/filemanager/FileListActivity;->mCountPerPage:I

    mul-int/2addr v5, v6

    iget v6, p0, Lcom/konka/mm/filemanager/FileListActivity;->curSelectedPos:I

    add-int v3, v5, v6

    iget-object v5, p0, Lcom/konka/mm/filemanager/FileListActivity;->infos:Ljava/util/List;

    invoke-interface {v5, v3, v0}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0, v0}, Lcom/konka/mm/filemanager/FileListActivity;->initFileInfo(Ljava/io/File;)V

    goto :goto_1
.end method

.method public renameFile()V
    .locals 4

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->isRenameManager:Z

    if-eqz v0, :cond_0

    iput-boolean v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->isRenameManager:Z

    iput-boolean v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->isManager:Z

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->observable:Lcom/konka/mm/filemanager/FileListActivity$MyObservable;

    invoke-virtual {v0, v1}, Lcom/konka/mm/filemanager/FileListActivity$MyObservable;->toobarStatusChanged(Z)V

    :goto_0
    return-void

    :cond_0
    iget-boolean v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->isManager:Z

    if-eqz v0, :cond_1

    iput-boolean v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->isManager:Z

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->observable:Lcom/konka/mm/filemanager/FileListActivity$MyObservable;

    invoke-virtual {v0, v1}, Lcom/konka/mm/filemanager/FileListActivity$MyObservable;->toobarStatusChanged(Z)V

    :cond_1
    invoke-virtual {p0}, Lcom/konka/mm/filemanager/FileListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090096    # com.konka.mm.R.string.delete_info

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x4

    iget v3, p0, Lcom/konka/mm/filemanager/FileListActivity;->mScreen_mode:I

    invoke-static {p0, v0, v1, v2, v3}, Lcom/konka/mm/finals/CommonFinals;->quitActivityByPopup(Landroid/app/Activity;Ljava/lang/String;III)V

    goto :goto_0
.end method

.method public setPageInfo()V
    .locals 6

    const/4 v5, 0x4

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->pageInfomation:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/konka/mm/filemanager/FileListActivity;->currentPage:I

    add-int/lit8 v2, v2, 0x1

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/konka/mm/filemanager/FileListActivity;->PageCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/konka/mm/filemanager/FileListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09009a    # com.konka.mm.R.string.page

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->PageCount:I

    const/4 v1, 0x1

    if-gt v0, v1, :cond_1

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->pageLeft:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->pageRight:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setVisibility(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->currentPage:I

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->pageLeft:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->pageRight:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0

    :cond_2
    iget v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->currentPage:I

    if-eqz v0, :cond_3

    iget v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->currentPage:I

    iget v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->PageCount:I

    add-int/lit8 v1, v1, -0x1

    if-eq v0, v1, :cond_3

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->pageLeft:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->pageRight:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0

    :cond_3
    iget v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->currentPage:I

    iget v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->PageCount:I

    add-int/lit8 v1, v1, -0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->pageLeft:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->pageRight:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0
.end method

.method public setPreOrNextScreen(I)V
    .locals 6
    .param p1    # I

    iget v2, p0, Lcom/konka/mm/filemanager/FileListActivity;->currentPage:I

    add-int/2addr v2, p1

    iput v2, p0, Lcom/konka/mm/filemanager/FileListActivity;->currentPage:I

    iget v2, p0, Lcom/konka/mm/filemanager/FileListActivity;->currentPage:I

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v2, p0, Lcom/konka/mm/filemanager/FileListActivity;->currentPage:I

    iget v3, p0, Lcom/konka/mm/filemanager/FileListActivity;->PageCount:I

    add-int/lit8 v3, v3, -0x1

    if-ge v2, v3, :cond_0

    iget v2, p0, Lcom/konka/mm/filemanager/FileListActivity;->curScreen:I

    add-int/2addr v2, p1

    add-int/lit8 v2, v2, 0x3

    rem-int/lit8 v0, v2, 0x3

    iget-object v2, p0, Lcom/konka/mm/filemanager/FileListActivity;->pageGridView:[Landroid/widget/GridView;

    aget-object v2, v2, v0

    iget v3, p0, Lcom/konka/mm/filemanager/FileListActivity;->numColumns:I

    invoke-virtual {v2, v3}, Landroid/widget/GridView;->setNumColumns(I)V

    iget v2, p0, Lcom/konka/mm/filemanager/FileListActivity;->currentPage:I

    add-int/2addr v2, p1

    iget v3, p0, Lcom/konka/mm/filemanager/FileListActivity;->PageCount:I

    add-int/lit8 v3, v3, -0x1

    if-ge v2, v3, :cond_3

    iget v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->mCountPerPage:I

    :cond_2
    :goto_1
    iget-object v2, p0, Lcom/konka/mm/filemanager/FileListActivity;->pageGridView:[Landroid/widget/GridView;

    aget-object v2, v2, v0

    new-instance v3, Lcom/konka/mm/adapters/FileListAdapter;

    iget v4, p0, Lcom/konka/mm/filemanager/FileListActivity;->currentPage:I

    add-int/2addr v4, p1

    iget v5, p0, Lcom/konka/mm/filemanager/FileListActivity;->mCountPerPage:I

    invoke-direct {v3, p0, v4, v1, v5}, Lcom/konka/mm/adapters/FileListAdapter;-><init>(Lcom/konka/mm/filemanager/FileListActivity;III)V

    invoke-virtual {v2, v3}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    goto :goto_0

    :cond_3
    iget v2, p0, Lcom/konka/mm/filemanager/FileListActivity;->fileSize:I

    iget v3, p0, Lcom/konka/mm/filemanager/FileListActivity;->mCountPerPage:I

    iget v4, p0, Lcom/konka/mm/filemanager/FileListActivity;->PageCount:I

    add-int/lit8 v4, v4, -0x1

    mul-int/2addr v3, v4

    sub-int v1, v2, v3

    iget v2, p0, Lcom/konka/mm/filemanager/FileListActivity;->mCountPerPage:I

    if-lt v1, v2, :cond_2

    iget v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->mCountPerPage:I

    goto :goto_1
.end method

.method public setRename()V
    .locals 5

    const/4 v3, -0x1

    iget-boolean v2, p0, Lcom/konka/mm/filemanager/FileListActivity;->isFocusInGrid:Z

    if-nez v2, :cond_0

    iput v3, p0, Lcom/konka/mm/filemanager/FileListActivity;->curSelectedPos:I

    :cond_0
    iget v2, p0, Lcom/konka/mm/filemanager/FileListActivity;->curSelectedPos:I

    if-eq v2, v3, :cond_2

    iget v2, p0, Lcom/konka/mm/filemanager/FileListActivity;->curSelectedPos:I

    invoke-direct {p0, v2}, Lcom/konka/mm/filemanager/FileListActivity;->getCurFile(I)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0}, Lcom/konka/mm/filemanager/FileListActivity;->isCanOperate(Ljava/io/File;)Z

    move-result v2

    if-nez v2, :cond_1

    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/konka/mm/filemanager/FileListActivity;->listListener:Lcom/konka/mm/filemanager/FileItemClickListener;

    invoke-virtual {v2, v1}, Lcom/konka/mm/filemanager/FileItemClickListener;->setFile(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/mm/filemanager/FileListActivity;->listListener:Lcom/konka/mm/filemanager/FileItemClickListener;

    iget v3, p0, Lcom/konka/mm/filemanager/FileListActivity;->currentPage:I

    iget v4, p0, Lcom/konka/mm/filemanager/FileListActivity;->mCountPerPage:I

    mul-int/2addr v3, v4

    iget v4, p0, Lcom/konka/mm/filemanager/FileListActivity;->curSelectedPos:I

    add-int/2addr v3, v4

    invoke-virtual {v2, v3}, Lcom/konka/mm/filemanager/FileItemClickListener;->setPosition(I)V

    iget-object v2, p0, Lcom/konka/mm/filemanager/FileListActivity;->listListener:Lcom/konka/mm/filemanager/FileItemClickListener;

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/konka/mm/filemanager/FileItemClickListener;->setName(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/mm/filemanager/FileListActivity;->listListener:Lcom/konka/mm/filemanager/FileItemClickListener;

    const/4 v3, 0x0

    const/4 v4, 0x6

    invoke-virtual {v2, v3, v4}, Lcom/konka/mm/filemanager/FileItemClickListener;->onClick(Landroid/view/View;I)V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/konka/mm/filemanager/FileListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090086    # com.konka.mm.R.string.SELECT_ONE

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x64

    invoke-static {p0, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public showProgressDialog(II)V
    .locals 3
    .param p1    # I
    .param p2    # I

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->proDlg:Landroid/app/ProgressDialog;

    if-nez v0, :cond_0

    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->proDlg:Landroid/app/ProgressDialog;

    :cond_0
    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->proDlg:Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/konka/mm/filemanager/FileListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->proDlg:Landroid/app/ProgressDialog;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/konka/mm/filemanager/FileListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "..."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->proDlg:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    return-void
.end method

.method public snapScreen(I)V
    .locals 5
    .param p1    # I

    const/4 v4, -0x1

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/konka/mm/filemanager/FileListActivity;->setBtnFocusable(Z)V

    if-ne p1, v3, :cond_4

    iget v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->currentPage:I

    iget v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->PageCount:I

    add-int/lit8 v1, v1, -0x1

    if-lt v0, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->viewFilpper:Landroid/widget/ViewFlipper;

    iget-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->mPushLeftInAnim:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->setInAnimation(Landroid/view/animation/Animation;)V

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->viewFilpper:Landroid/widget/ViewFlipper;

    iget-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->mPustRightOutAnim:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->setOutAnimation(Landroid/view/animation/Animation;)V

    iget v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->curScreen:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->curScreen:I

    iget v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->curScreen:I

    rem-int/lit8 v0, v0, 0x3

    iput v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->curScreen:I

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->viewFilpper:Landroid/widget/ViewFlipper;

    invoke-virtual {v0}, Landroid/widget/ViewFlipper;->showNext()V

    invoke-virtual {p0, v3}, Lcom/konka/mm/filemanager/FileListActivity;->setPreOrNextScreen(I)V

    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->pageGridView:[Landroid/widget/GridView;

    iget v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->curScreen:I

    aget-object v0, v0, v1

    invoke-virtual {v0, v3}, Landroid/widget/GridView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->pageGridView:[Landroid/widget/GridView;

    iget v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->curScreen:I

    aget-object v0, v0, v1

    invoke-virtual {v0}, Landroid/widget/GridView;->requestFocus()Z

    if-ne p1, v3, :cond_5

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->pageGridView:[Landroid/widget/GridView;

    iget v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->curScreen:I

    aget-object v0, v0, v1

    invoke-virtual {v0, v2}, Landroid/widget/GridView;->setSelection(I)V

    iput v2, p0, Lcom/konka/mm/filemanager/FileListActivity;->curSelectedPos:I

    :goto_2
    if-ne p1, v3, :cond_6

    invoke-direct {p0, v2}, Lcom/konka/mm/filemanager/FileListActivity;->getCurFile(I)Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->curFile:Ljava/io/File;

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->curFile:Ljava/io/File;

    invoke-virtual {p0, v0}, Lcom/konka/mm/filemanager/FileListActivity;->initFileInfo(Ljava/io/File;)V

    :cond_3
    :goto_3
    invoke-virtual {p0}, Lcom/konka/mm/filemanager/FileListActivity;->setPageInfo()V

    iget-boolean v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->isManager:Z

    if-eqz v0, :cond_0

    iput-boolean v2, p0, Lcom/konka/mm/filemanager/FileListActivity;->isManager:Z

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->observable:Lcom/konka/mm/filemanager/FileListActivity$MyObservable;

    invoke-virtual {v0, v2}, Lcom/konka/mm/filemanager/FileListActivity$MyObservable;->toobarStatusChanged(Z)V

    goto :goto_0

    :cond_4
    if-ne p1, v4, :cond_2

    iget v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->currentPage:I

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->viewFilpper:Landroid/widget/ViewFlipper;

    iget-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->mPustRightInAnim:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->setInAnimation(Landroid/view/animation/Animation;)V

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->viewFilpper:Landroid/widget/ViewFlipper;

    iget-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->mPushLeftOutAnim:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->setOutAnimation(Landroid/view/animation/Animation;)V

    iget v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->curScreen:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->curScreen:I

    iget v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->curScreen:I

    add-int/lit8 v0, v0, 0x3

    rem-int/lit8 v0, v0, 0x3

    iput v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->curScreen:I

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->viewFilpper:Landroid/widget/ViewFlipper;

    invoke-virtual {v0}, Landroid/widget/ViewFlipper;->showPrevious()V

    invoke-virtual {p0, v4}, Lcom/konka/mm/filemanager/FileListActivity;->setPreOrNextScreen(I)V

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->pageGridView:[Landroid/widget/GridView;

    iget v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->curScreen:I

    aget-object v0, v0, v1

    iget v1, p0, Lcom/konka/mm/filemanager/FileListActivity;->numColumns:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setSelection(I)V

    iget v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->numColumns:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->curSelectedPos:I

    goto :goto_2

    :cond_6
    if-ne p1, v4, :cond_3

    iget v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->numColumns:I

    add-int/lit8 v0, v0, -0x1

    invoke-direct {p0, v0}, Lcom/konka/mm/filemanager/FileListActivity;->getCurFile(I)Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->curFile:Ljava/io/File;

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->curFile:Ljava/io/File;

    invoke-virtual {p0, v0}, Lcom/konka/mm/filemanager/FileListActivity;->initFileInfo(Ljava/io/File;)V

    goto :goto_3
.end method

.method public up2One()V
    .locals 2

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->parentFile:Ljava/io/File;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity;->parentFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/konka/mm/filemanager/FileListActivity;->broweTo(Ljava/lang/String;I)V

    :cond_0
    return-void
.end method
