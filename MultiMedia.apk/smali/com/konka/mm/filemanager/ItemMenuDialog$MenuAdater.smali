.class Lcom/konka/mm/filemanager/ItemMenuDialog$MenuAdater;
.super Landroid/widget/BaseAdapter;
.source "ItemMenuDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/mm/filemanager/ItemMenuDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "MenuAdater"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/mm/filemanager/ItemMenuDialog;


# direct methods
.method constructor <init>(Lcom/konka/mm/filemanager/ItemMenuDialog;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/filemanager/ItemMenuDialog$MenuAdater;->this$0:Lcom/konka/mm/filemanager/ItemMenuDialog;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/filemanager/ItemMenuDialog$MenuAdater;->this$0:Lcom/konka/mm/filemanager/ItemMenuDialog;

    iget-object v0, v0, Lcom/konka/mm/filemanager/ItemMenuDialog;->menuNames:[Ljava/lang/String;

    array-length v0, v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1    # I

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1    # I

    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    new-instance v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/mm/filemanager/ItemMenuDialog$MenuAdater;->this$0:Lcom/konka/mm/filemanager/ItemMenuDialog;

    iget-object v1, v1, Lcom/konka/mm/filemanager/ItemMenuDialog;->context:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->MIDDLE:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    iget-object v1, p0, Lcom/konka/mm/filemanager/ItemMenuDialog$MenuAdater;->this$0:Lcom/konka/mm/filemanager/ItemMenuDialog;

    iget-object v1, v1, Lcom/konka/mm/filemanager/ItemMenuDialog;->menuNames:[Ljava/lang/String;

    aget-object v1, v1, p1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-object v0
.end method
