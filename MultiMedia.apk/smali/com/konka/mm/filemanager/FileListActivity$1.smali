.class Lcom/konka/mm/filemanager/FileListActivity$1;
.super Landroid/os/Handler;
.source "FileListActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/mm/filemanager/FileListActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/mm/filemanager/FileListActivity;


# direct methods
.method constructor <init>(Lcom/konka/mm/filemanager/FileListActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/filemanager/FileListActivity$1;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1    # Landroid/os/Message;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, [Ljava/io/File;

    iget-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity$1;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    iget-object v2, p0, Lcom/konka/mm/filemanager/FileListActivity$1;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    # invokes: Lcom/konka/mm/filemanager/FileListActivity;->getListFromFiles([Ljava/io/File;)Ljava/util/List;
    invoke-static {v2, v0}, Lcom/konka/mm/filemanager/FileListActivity;->access$0(Lcom/konka/mm/filemanager/FileListActivity;[Ljava/io/File;)Ljava/util/List;

    move-result-object v2

    iput-object v2, v1, Lcom/konka/mm/filemanager/FileListActivity;->infos:Ljava/util/List;

    iget-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity$1;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    iget-object v1, v1, Lcom/konka/mm/filemanager/FileListActivity;->infos:Ljava/util/List;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity$1;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    iget-object v1, v1, Lcom/konka/mm/filemanager/FileListActivity;->infos:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    iget-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity$1;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    iget-object v2, p0, Lcom/konka/mm/filemanager/FileListActivity$1;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    iget-object v2, v2, Lcom/konka/mm/filemanager/FileListActivity;->infos:Ljava/util/List;

    iget v3, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v1, v2, v3}, Lcom/konka/mm/filemanager/FileListActivity;->intiGridView(Ljava/util/List;I)V

    :cond_0
    iget-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity$1;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    # getter for: Lcom/konka/mm/filemanager/FileListActivity;->proDlg:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/konka/mm/filemanager/FileListActivity;->access$1(Lcom/konka/mm/filemanager/FileListActivity;)Landroid/app/ProgressDialog;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity$1;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    # getter for: Lcom/konka/mm/filemanager/FileListActivity;->proDlg:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/konka/mm/filemanager/FileListActivity;->access$1(Lcom/konka/mm/filemanager/FileListActivity;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    :cond_1
    iget-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity$1;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/konka/mm/filemanager/FileListActivity;->isManager:Z

    iget-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity$1;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    iget-object v1, v1, Lcom/konka/mm/filemanager/FileListActivity;->observable:Lcom/konka/mm/filemanager/FileListActivity$MyObservable;

    iget-object v2, p0, Lcom/konka/mm/filemanager/FileListActivity$1;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    iget-boolean v2, v2, Lcom/konka/mm/filemanager/FileListActivity;->isManager:Z

    invoke-virtual {v1, v2}, Lcom/konka/mm/filemanager/FileListActivity$MyObservable;->toobarStatusChanged(Z)V

    return-void
.end method
