.class public Lcom/konka/mm/filemanager/AsycLoadImage;
.super Ljava/lang/Object;
.source "AsycLoadImage.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/mm/filemanager/AsycLoadImage$MyHandler;
    }
.end annotation


# instance fields
.field private context:Landroid/content/Context;

.field private handlerThread:Landroid/os/HandlerThread;

.field private myHandler:Lcom/konka/mm/filemanager/AsycLoadImage$MyHandler;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/konka/mm/filemanager/AsycLoadImage;->myHandler:Lcom/konka/mm/filemanager/AsycLoadImage$MyHandler;

    iput-object v0, p0, Lcom/konka/mm/filemanager/AsycLoadImage;->handlerThread:Landroid/os/HandlerThread;

    iput-object p1, p0, Lcom/konka/mm/filemanager/AsycLoadImage;->context:Landroid/content/Context;

    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "handler_thread"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/konka/mm/filemanager/AsycLoadImage;->handlerThread:Landroid/os/HandlerThread;

    iget-object v0, p0, Lcom/konka/mm/filemanager/AsycLoadImage;->handlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    new-instance v0, Lcom/konka/mm/filemanager/AsycLoadImage$MyHandler;

    iget-object v1, p0, Lcom/konka/mm/filemanager/AsycLoadImage;->handlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/konka/mm/filemanager/AsycLoadImage$MyHandler;-><init>(Lcom/konka/mm/filemanager/AsycLoadImage;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/konka/mm/filemanager/AsycLoadImage;->myHandler:Lcom/konka/mm/filemanager/AsycLoadImage$MyHandler;

    return-void
.end method

.method static synthetic access$0(Lcom/konka/mm/filemanager/AsycLoadImage;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/filemanager/AsycLoadImage;->context:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public loadImage(Landroid/widget/ImageView;Ljava/lang/String;)V
    .locals 3
    .param p1    # Landroid/widget/ImageView;
    .param p2    # Ljava/lang/String;

    iget-object v2, p0, Lcom/konka/mm/filemanager/AsycLoadImage;->myHandler:Lcom/konka/mm/filemanager/AsycLoadImage$MyHandler;

    invoke-virtual {v2}, Lcom/konka/mm/filemanager/AsycLoadImage$MyHandler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string v2, "path"

    invoke-virtual {v0, v2, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "icon"

    invoke-virtual {v0, v2, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iput-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    return-void
.end method
