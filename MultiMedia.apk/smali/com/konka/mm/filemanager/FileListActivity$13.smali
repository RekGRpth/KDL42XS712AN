.class Lcom/konka/mm/filemanager/FileListActivity$13;
.super Ljava/lang/Object;
.source "FileListActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/mm/filemanager/FileListActivity;->showDeleteDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/mm/filemanager/FileListActivity;


# direct methods
.method constructor <init>(Lcom/konka/mm/filemanager/FileListActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/filemanager/FileListActivity$13;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity$13;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    iget-object v0, v0, Lcom/konka/mm/filemanager/FileListActivity;->manager:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity$13;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    iget-object v0, v0, Lcom/konka/mm/filemanager/FileListActivity;->manager:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity$13;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    iput-boolean v2, v0, Lcom/konka/mm/filemanager/FileListActivity;->isManager:Z

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity$13;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    const/4 v1, 0x0

    iput v1, v0, Lcom/konka/mm/filemanager/FileListActivity;->curSelectedPos:I

    iget-object v0, p0, Lcom/konka/mm/filemanager/FileListActivity$13;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    iget-object v0, v0, Lcom/konka/mm/filemanager/FileListActivity;->observable:Lcom/konka/mm/filemanager/FileListActivity$MyObservable;

    invoke-virtual {v0, v2}, Lcom/konka/mm/filemanager/FileListActivity$MyObservable;->toobarStatusChanged(Z)V

    return-void
.end method
