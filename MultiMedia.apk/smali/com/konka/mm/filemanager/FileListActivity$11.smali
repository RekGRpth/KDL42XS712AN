.class Lcom/konka/mm/filemanager/FileListActivity$11;
.super Landroid/os/Handler;
.source "FileListActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/mm/filemanager/FileListActivity;->initViewHandler()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/mm/filemanager/FileListActivity;


# direct methods
.method constructor <init>(Lcom/konka/mm/filemanager/FileListActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/filemanager/FileListActivity$11;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1    # Landroid/os/Message;

    const/4 v4, 0x0

    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_0
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    return-void

    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    const-string v1, "enpty_file_name"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity$11;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    iget-object v2, p0, Lcom/konka/mm/filemanager/FileListActivity$11;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    invoke-virtual {v2}, Lcom/konka/mm/filemanager/FileListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0900a6    # com.konka.mm.R.string.empty_file_name

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_1
    const-string v1, "has_same_name"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity$11;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    iget-object v2, p0, Lcom/konka/mm/filemanager/FileListActivity$11;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    invoke-virtual {v2}, Lcom/konka/mm/filemanager/FileListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0900a5    # com.konka.mm.R.string.has_same_name

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity$11;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    invoke-virtual {v1, v0}, Lcom/konka/mm/filemanager/FileListActivity;->rename(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_1
    iget v1, p1, Landroid/os/Message;->arg2:I

    const/16 v2, 0x17

    if-ne v1, v2, :cond_0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0xc
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
