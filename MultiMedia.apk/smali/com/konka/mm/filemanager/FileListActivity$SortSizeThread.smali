.class Lcom/konka/mm/filemanager/FileListActivity$SortSizeThread;
.super Ljava/lang/Object;
.source "FileListActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/mm/filemanager/FileListActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SortSizeThread"
.end annotation


# instance fields
.field private browTo:I

.field private files:[Ljava/io/File;

.field private flag:I

.field final synthetic this$0:Lcom/konka/mm/filemanager/FileListActivity;


# direct methods
.method public constructor <init>(Lcom/konka/mm/filemanager/FileListActivity;[Ljava/io/File;II)V
    .locals 1
    .param p2    # [Ljava/io/File;
    .param p3    # I
    .param p4    # I

    const/4 v0, 0x0

    iput-object p1, p0, Lcom/konka/mm/filemanager/FileListActivity$SortSizeThread;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/konka/mm/filemanager/FileListActivity$SortSizeThread;->flag:I

    iput v0, p0, Lcom/konka/mm/filemanager/FileListActivity$SortSizeThread;->browTo:I

    iput-object p2, p0, Lcom/konka/mm/filemanager/FileListActivity$SortSizeThread;->files:[Ljava/io/File;

    iput p3, p0, Lcom/konka/mm/filemanager/FileListActivity$SortSizeThread;->flag:I

    iput p4, p0, Lcom/konka/mm/filemanager/FileListActivity$SortSizeThread;->browTo:I

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/konka/mm/filemanager/FileListActivity$SortSizeThread;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    iget-object v3, p0, Lcom/konka/mm/filemanager/FileListActivity$SortSizeThread;->files:[Ljava/io/File;

    # invokes: Lcom/konka/mm/filemanager/FileListActivity;->sortFiles([Ljava/io/File;)[Ljava/io/File;
    invoke-static {v2, v3}, Lcom/konka/mm/filemanager/FileListActivity;->access$20(Lcom/konka/mm/filemanager/FileListActivity;[Ljava/io/File;)[Ljava/io/File;

    move-result-object v0

    iget-object v2, p0, Lcom/konka/mm/filemanager/FileListActivity$SortSizeThread;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    # getter for: Lcom/konka/mm/filemanager/FileListActivity;->sortHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/konka/mm/filemanager/FileListActivity;->access$26(Lcom/konka/mm/filemanager/FileListActivity;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    iget v2, p0, Lcom/konka/mm/filemanager/FileListActivity$SortSizeThread;->flag:I

    iput v2, v1, Landroid/os/Message;->what:I

    iget v2, p0, Lcom/konka/mm/filemanager/FileListActivity$SortSizeThread;->browTo:I

    iput v2, v1, Landroid/os/Message;->arg1:I

    iput-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    iget-object v2, p0, Lcom/konka/mm/filemanager/FileListActivity$SortSizeThread;->this$0:Lcom/konka/mm/filemanager/FileListActivity;

    # getter for: Lcom/konka/mm/filemanager/FileListActivity;->sortHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/konka/mm/filemanager/FileListActivity;->access$26(Lcom/konka/mm/filemanager/FileListActivity;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method
