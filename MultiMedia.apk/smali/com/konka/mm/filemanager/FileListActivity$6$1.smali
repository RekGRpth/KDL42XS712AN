.class Lcom/konka/mm/filemanager/FileListActivity$6$1;
.super Ljava/lang/Object;
.source "FileListActivity.java"

# interfaces
.implements Ljava/io/FileFilter;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/mm/filemanager/FileListActivity$6;->onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/konka/mm/filemanager/FileListActivity$6;


# direct methods
.method constructor <init>(Lcom/konka/mm/filemanager/FileListActivity$6;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/filemanager/FileListActivity$6$1;->this$1:Lcom/konka/mm/filemanager/FileListActivity$6;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public accept(Ljava/io/File;)Z
    .locals 3
    .param p1    # Ljava/io/File;

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/mm/filemanager/FileListActivity$6$1;->this$1:Lcom/konka/mm/filemanager/FileListActivity$6;

    # getter for: Lcom/konka/mm/filemanager/FileListActivity$6;->this$0:Lcom/konka/mm/filemanager/FileListActivity;
    invoke-static {v1}, Lcom/konka/mm/filemanager/FileListActivity$6;->access$0(Lcom/konka/mm/filemanager/FileListActivity$6;)Lcom/konka/mm/filemanager/FileListActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/konka/mm/filemanager/FileListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070001    # com.konka.mm.R.array.fileEndingAudio

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/konka/mm/tools/FileTool;->checkEndsWithInStringArray(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
