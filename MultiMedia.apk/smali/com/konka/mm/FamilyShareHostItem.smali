.class public Lcom/konka/mm/FamilyShareHostItem;
.super Ljava/lang/Object;
.source "FamilyShareHostItem.java"


# instance fields
.field private HostIP:Ljava/lang/String;

.field private HostName:Ljava/lang/String;

.field public Icon:I


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-nez p1, :cond_0

    iput-object p2, p0, Lcom/konka/mm/FamilyShareHostItem;->HostName:Ljava/lang/String;

    :cond_0
    iput-object p2, p0, Lcom/konka/mm/FamilyShareHostItem;->HostIP:Ljava/lang/String;

    iput p3, p0, Lcom/konka/mm/FamilyShareHostItem;->Icon:I

    return-void
.end method


# virtual methods
.method public getHostIP()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/FamilyShareHostItem;->HostIP:Ljava/lang/String;

    return-object v0
.end method

.method public getHostName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/FamilyShareHostItem;->HostName:Ljava/lang/String;

    return-object v0
.end method

.method public setHostIP(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/mm/FamilyShareHostItem;->HostIP:Ljava/lang/String;

    return-void
.end method

.method public setHostName(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/mm/FamilyShareHostItem;->HostName:Ljava/lang/String;

    return-void
.end method
