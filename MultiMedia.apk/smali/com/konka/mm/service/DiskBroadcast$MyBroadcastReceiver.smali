.class Lcom/konka/mm/service/DiskBroadcast$MyBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "DiskBroadcast.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/mm/service/DiskBroadcast;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyBroadcastReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/mm/service/DiskBroadcast;


# direct methods
.method private constructor <init>(Lcom/konka/mm/service/DiskBroadcast;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/service/DiskBroadcast$MyBroadcastReceiver;->this$0:Lcom/konka/mm/service/DiskBroadcast;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/konka/mm/service/DiskBroadcast;Lcom/konka/mm/service/DiskBroadcast$MyBroadcastReceiver;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/mm/service/DiskBroadcast$MyBroadcastReceiver;-><init>(Lcom/konka/mm/service/DiskBroadcast;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    iget-object v2, p0, Lcom/konka/mm/service/DiskBroadcast$MyBroadcastReceiver;->this$0:Lcom/konka/mm/service/DiskBroadcast;

    # getter for: Lcom/konka/mm/service/DiskBroadcast;->listen:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/konka/mm/service/DiskBroadcast;->access$0(Lcom/konka/mm/service/DiskBroadcast;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v3, "android.intent.action.MEDIA_SCANNER_STARTED"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "--------------------------------ACTION_MEDIA_SCANNER_STARTED."

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    const/4 v0, 0x0

    :goto_0
    if-lt v0, v1, :cond_2

    :cond_0
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v3, "android.intent.action.MEDIA_SCANNER_FINISHED"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "--------------------------------ACTION_MEDIA_SCANNER_FINISHED."

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    const/4 v0, 0x0

    :goto_1
    if-lt v0, v1, :cond_3

    :cond_1
    return-void

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v3, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "--------------------------------ACTION_MEDIA_EJECT."

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    const/4 v0, 0x0

    :goto_2
    if-ge v0, v1, :cond_1

    add-int/lit8 v0, v0, 0x1

    goto :goto_2
.end method
