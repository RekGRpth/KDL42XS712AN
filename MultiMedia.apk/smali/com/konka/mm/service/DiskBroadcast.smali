.class public Lcom/konka/mm/service/DiskBroadcast;
.super Ljava/lang/Object;
.source "DiskBroadcast.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/mm/service/DiskBroadcast$MyBroadcastReceiver;
    }
.end annotation


# static fields
.field private static final single:Lcom/konka/mm/service/DiskBroadcast;


# instance fields
.field private broadcastReceiver:Lcom/konka/mm/service/DiskBroadcast$MyBroadcastReceiver;

.field private listen:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/konka/mm/service/IDiskChange;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/konka/mm/service/DiskBroadcast;

    invoke-direct {v0}, Lcom/konka/mm/service/DiskBroadcast;-><init>()V

    sput-object v0, Lcom/konka/mm/service/DiskBroadcast;->single:Lcom/konka/mm/service/DiskBroadcast;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/konka/mm/service/DiskBroadcast;->listen:Ljava/util/ArrayList;

    new-instance v0, Lcom/konka/mm/service/DiskBroadcast$MyBroadcastReceiver;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/konka/mm/service/DiskBroadcast$MyBroadcastReceiver;-><init>(Lcom/konka/mm/service/DiskBroadcast;Lcom/konka/mm/service/DiskBroadcast$MyBroadcastReceiver;)V

    iput-object v0, p0, Lcom/konka/mm/service/DiskBroadcast;->broadcastReceiver:Lcom/konka/mm/service/DiskBroadcast$MyBroadcastReceiver;

    return-void
.end method

.method static synthetic access$0(Lcom/konka/mm/service/DiskBroadcast;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/service/DiskBroadcast;->listen:Ljava/util/ArrayList;

    return-object v0
.end method

.method public static getInstance()Lcom/konka/mm/service/DiskBroadcast;
    .locals 1

    sget-object v0, Lcom/konka/mm/service/DiskBroadcast;->single:Lcom/konka/mm/service/DiskBroadcast;

    return-object v0
.end method


# virtual methods
.method public addReceiver(Lcom/konka/mm/service/IDiskChange;)V
    .locals 1
    .param p1    # Lcom/konka/mm/service/IDiskChange;

    iget-object v0, p0, Lcom/konka/mm/service/DiskBroadcast;->listen:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public registerBroadcast(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.MEDIA_SCANNER_STARTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.MEDIA_SCANNER_FINISHED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.MEDIA_SHARED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "AACC"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/mm/service/DiskBroadcast;->broadcastReceiver:Lcom/konka/mm/service/DiskBroadcast$MyBroadcastReceiver;

    invoke-virtual {p1, v1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method public removeAllReceiver(Lcom/konka/mm/service/IDiskChange;)V
    .locals 1
    .param p1    # Lcom/konka/mm/service/IDiskChange;

    iget-object v0, p0, Lcom/konka/mm/service/DiskBroadcast;->listen:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    return-void
.end method

.method public removeReceiver(Lcom/konka/mm/service/IDiskChange;)V
    .locals 1
    .param p1    # Lcom/konka/mm/service/IDiskChange;

    iget-object v0, p0, Lcom/konka/mm/service/DiskBroadcast;->listen:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    return-void
.end method
