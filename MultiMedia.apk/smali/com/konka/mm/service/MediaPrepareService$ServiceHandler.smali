.class final Lcom/konka/mm/service/MediaPrepareService$ServiceHandler;
.super Landroid/os/Handler;
.source "MediaPrepareService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/mm/service/MediaPrepareService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "ServiceHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/mm/service/MediaPrepareService;


# direct methods
.method private constructor <init>(Lcom/konka/mm/service/MediaPrepareService;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/service/MediaPrepareService$ServiceHandler;->this$0:Lcom/konka/mm/service/MediaPrepareService;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1    # Landroid/os/Message;

    invoke-static {}, Lcom/konka/mm/data/DataGB;->getInstance()Lcom/konka/mm/data/DataGB;

    move-result-object v1

    invoke-virtual {v1}, Lcom/konka/mm/data/DataGB;->create()V

    iget-object v1, p0, Lcom/konka/mm/service/MediaPrepareService$ServiceHandler;->this$0:Lcom/konka/mm/service/MediaPrepareService;

    # getter for: Lcom/konka/mm/service/MediaPrepareService;->imageChanged:Ljava/lang/Boolean;
    invoke-static {v1}, Lcom/konka/mm/service/MediaPrepareService;->access$1(Lcom/konka/mm/service/MediaPrepareService;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/konka/mm/service/MediaPrepareService$ServiceHandler;->this$0:Lcom/konka/mm/service/MediaPrepareService;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/konka/mm/service/MediaPrepareService;->access$0(Lcom/konka/mm/service/MediaPrepareService;Ljava/lang/Boolean;)V

    invoke-static {}, Lcom/konka/mm/data/DataGB;->getInstance()Lcom/konka/mm/data/DataGB;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/mm/service/MediaPrepareService$ServiceHandler;->this$0:Lcom/konka/mm/service/MediaPrepareService;

    invoke-virtual {v2}, Lcom/konka/mm/service/MediaPrepareService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/konka/mm/data/DataGB;->getAllImageFolder(Landroid/content/Context;)V

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.mstar.mediaprepare.prepareready"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "localmm"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v1, p0, Lcom/konka/mm/service/MediaPrepareService$ServiceHandler;->this$0:Lcom/konka/mm/service/MediaPrepareService;

    invoke-virtual {v1, v0}, Lcom/konka/mm/service/MediaPrepareService;->sendBroadcast(Landroid/content/Intent;)V

    :cond_0
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    return-void
.end method
