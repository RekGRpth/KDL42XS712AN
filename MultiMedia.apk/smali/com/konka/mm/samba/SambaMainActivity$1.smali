.class Lcom/konka/mm/samba/SambaMainActivity$1;
.super Ljava/lang/Object;
.source "SambaMainActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/mm/samba/SambaMainActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/mm/samba/SambaMainActivity;


# direct methods
.method constructor <init>(Lcom/konka/mm/samba/SambaMainActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/samba/SambaMainActivity$1;->this$0:Lcom/konka/mm/samba/SambaMainActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    iget-object v1, p0, Lcom/konka/mm/samba/SambaMainActivity$1;->this$0:Lcom/konka/mm/samba/SambaMainActivity;

    invoke-static {v1}, Lcom/konka/mm/model/FamilyShareModel;->isConneted2Network(Landroid/app/Activity;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/konka/mm/samba/SambaMainActivity$1;->this$0:Lcom/konka/mm/samba/SambaMainActivity;

    iget-object v2, p0, Lcom/konka/mm/samba/SambaMainActivity$1;->this$0:Lcom/konka/mm/samba/SambaMainActivity;

    invoke-virtual {v2}, Lcom/konka/mm/samba/SambaMainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09003b    # com.konka.mm.R.string.not_available_net

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    :goto_0
    return-void

    :cond_0
    sget-object v1, Lcom/konka/mm/samba/SambaMainActivity;->TAG:Ljava/lang/String;

    const-string v2, "the network is available!"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/konka/mm/samba/SambaMainActivity$1;->this$0:Lcom/konka/mm/samba/SambaMainActivity;

    const-class v2, Lcom/konka/mm/samba/SambaBrowserActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v1, p0, Lcom/konka/mm/samba/SambaMainActivity$1;->this$0:Lcom/konka/mm/samba/SambaMainActivity;

    invoke-virtual {v1, v0}, Lcom/konka/mm/samba/SambaMainActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
