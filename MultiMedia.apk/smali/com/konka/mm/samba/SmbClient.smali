.class public Lcom/konka/mm/samba/SmbClient;
.super Ljava/lang/Object;
.source "SmbClient.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/mm/samba/SmbClient$scanHostByPing;
    }
.end annotation


# static fields
.field private static final MAX_THREAD_COUNT:I = 0x10

.field private static final bit0:I = 0x1

.field private static final bit1:I = 0x2

.field private static final bit10:I = 0x400

.field private static final bit11:I = 0x800

.field private static final bit12:I = 0x1000

.field private static final bit13:I = 0x2000

.field private static final bit14:I = 0x4000

.field private static final bit15:I = 0x8000

.field private static final bit2:I = 0x4

.field private static final bit3:I = 0x8

.field private static final bit4:I = 0x10

.field private static final bit5:I = 0x20

.field private static final bit6:I = 0x40

.field private static final bit7:I = 0x80

.field private static final bit8:I = 0x100

.field private static final bit9:I = 0x200

.field private static logEnable:Z = false

.field private static final smbclient_version:Ljava/lang/String; = "00.05"


# instance fields
.field private ScanCompleted:I

.field public deviceList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/konka/mm/samba/SmbDevice;",
            ">;"
        }
    .end annotation
.end field

.field private isScanning:Z

.field private onRecvMsglistener:Lcom/konka/mm/samba/OnRecvMsgListener;

.field private volatile stoprun:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x1

    sput-boolean v0, Lcom/konka/mm/samba/SmbClient;->logEnable:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/konka/mm/samba/SmbClient;->deviceList:Ljava/util/ArrayList;

    iput-boolean v1, p0, Lcom/konka/mm/samba/SmbClient;->isScanning:Z

    iput-boolean v1, p0, Lcom/konka/mm/samba/SmbClient;->stoprun:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/konka/mm/samba/SmbClient;->onRecvMsglistener:Lcom/konka/mm/samba/OnRecvMsgListener;

    return-void
.end method

.method static synthetic access$0()Z
    .locals 1

    sget-boolean v0, Lcom/konka/mm/samba/SmbClient;->logEnable:Z

    return v0
.end method

.method static synthetic access$1(Lcom/konka/mm/samba/SmbClient;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/mm/samba/SmbClient;->stoprun:Z

    return v0
.end method

.method static synthetic access$2(Lcom/konka/mm/samba/SmbClient;)I
    .locals 1

    iget v0, p0, Lcom/konka/mm/samba/SmbClient;->ScanCompleted:I

    return v0
.end method

.method static synthetic access$3(Lcom/konka/mm/samba/SmbClient;I)V
    .locals 0

    iput p1, p0, Lcom/konka/mm/samba/SmbClient;->ScanCompleted:I

    return-void
.end method

.method static synthetic access$4(Lcom/konka/mm/samba/SmbClient;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/konka/mm/samba/SmbClient;->isScanning:Z

    return-void
.end method

.method private getLocalIP()Ljava/lang/String;
    .locals 10

    const/4 v5, 0x0

    :try_start_0
    invoke-static {}, Ljava/net/NetworkInterface;->getNetworkInterfaces()Ljava/util/Enumeration;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z
    :try_end_0
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v7

    if-nez v7, :cond_1

    :goto_0
    sget-object v7, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "ip addr :"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    return-object v5

    :cond_1
    :try_start_1
    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/net/NetworkInterface;

    invoke-virtual {v4}, Ljava/net/NetworkInterface;->getInetAddresses()Ljava/util/Enumeration;

    move-result-object v1

    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/net/InetAddress;

    invoke-virtual {v3}, Ljava/net/InetAddress;->isLoopbackAddress()Z

    move-result v7

    if-nez v7, :cond_2

    invoke-virtual {v3}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v6

    const-string v7, "::"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_2

    const-string v7, "%"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_2

    const-string v7, "."

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-virtual {v3}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->toString()Ljava/lang/String;
    :try_end_1
    .catch Ljava/net/SocketException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v5

    goto :goto_1

    :catch_0
    move-exception v2

    sget-object v7, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "WifiPreference IpAddress"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/net/SocketException;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static initSamba()V
    .locals 2

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "  smbclient_version : 00.05"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    const-string v0, "jcifs.encoding"

    const-string v1, "UTF8"

    invoke-static {v0, v1}, Ljcifs/Config;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "jcifs.smb.lmCompatibility"

    const-string v1, "0"

    invoke-static {v0, v1}, Ljcifs/Config;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "jcifs.smb.client.responseTimeout"

    const-string v1, "10000"

    invoke-static {v0, v1}, Ljcifs/Config;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "jcifs.netbios.retryTimeout"

    const-string v1, "10000"

    invoke-static {v0, v1}, Ljcifs/Config;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "jcifs.smb.client.dfs.disabled"

    const-string v1, "true"

    invoke-static {v0, v1}, Ljcifs/Config;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    invoke-static {}, Ljcifs/Config;->registerSmbURLHandler()V

    return-void
.end method

.method public static pingHost(Ljava/lang/String;I)Z
    .locals 11
    .param p0    # Ljava/lang/String;
    .param p1    # I

    const/4 v8, 0x0

    const/4 v2, 0x0

    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v7

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "ping -w "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " -c 1 "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    sget-boolean v9, Lcom/konka/mm/samba/SmbClient;->logEnable:Z

    if-eqz v9, :cond_0

    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v9, v6}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    :try_start_0
    invoke-virtual {v7, v6}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v5

    if-nez v5, :cond_1

    :try_start_1
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    :goto_0
    return v8

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    :cond_1
    :try_start_2
    new-instance v3, Ljava/io/BufferedReader;

    new-instance v9, Ljava/io/InputStreamReader;

    invoke-virtual {v5}, Ljava/lang/Process;->getInputStream()Ljava/io/InputStream;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v3, v9}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    const/4 v4, 0x0

    :cond_2
    :try_start_3
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_7
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result-object v4

    if-nez v4, :cond_4

    :try_start_4
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_5

    :goto_1
    :try_start_5
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_6

    :goto_2
    sget-boolean v9, Lcom/konka/mm/samba/SmbClient;->logEnable:Z

    if-eqz v9, :cond_3

    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, " fail "

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_3
    move-object v2, v3

    goto :goto_0

    :cond_4
    :try_start_6
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v4

    const-string v9, "bytes from"

    invoke-virtual {v4, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_2

    sget-boolean v9, Lcom/konka/mm/samba/SmbClient;->logEnable:Z

    if-eqz v9, :cond_5

    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v10, " ok"

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_7
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :cond_5
    :try_start_7
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1

    :goto_3
    const/4 v8, 0x1

    move-object v2, v3

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    :catch_2
    move-exception v1

    :goto_4
    :try_start_8
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3

    goto :goto_0

    :catch_3
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    :catchall_0
    move-exception v8

    :goto_5
    :try_start_9
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_4

    :goto_6
    throw v8

    :catch_4
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6

    :catch_5
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    :catch_6
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    :catchall_1
    move-exception v8

    move-object v2, v3

    goto :goto_5

    :catch_7
    move-exception v1

    move-object v2, v3

    goto :goto_4
.end method


# virtual methods
.method public StopUpdate()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/konka/mm/samba/SmbClient;->stoprun:Z

    return-void
.end method

.method public getSmbDeviceList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/konka/mm/samba/SmbDevice;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/konka/mm/samba/SmbClient;->deviceList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public isUpdating()Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/mm/samba/SmbClient;->isScanning:Z

    return v0
.end method

.method public setOnRecvMsgListener(Lcom/konka/mm/samba/OnRecvMsgListener;)V
    .locals 0
    .param p1    # Lcom/konka/mm/samba/OnRecvMsgListener;

    iput-object p1, p0, Lcom/konka/mm/samba/SmbClient;->onRecvMsglistener:Lcom/konka/mm/samba/OnRecvMsgListener;

    return-void
.end method

.method public setStoprun(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/konka/mm/samba/SmbClient;->stoprun:Z

    return-void
.end method

.method public updateSmbDeviceList()V
    .locals 10

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/konka/mm/samba/SmbClient;->getLocalIP()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "no ip address or recv listener! "

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "127.0.0.1"

    invoke-virtual {v2, v0}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    iput-boolean v1, p0, Lcom/konka/mm/samba/SmbClient;->isScanning:Z

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "no ip address or recv listener! "

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    iput v1, p0, Lcom/konka/mm/samba/SmbClient;->ScanCompleted:I

    const/4 v7, 0x0

    :goto_1
    const/16 v0, 0x10

    if-ge v7, v0, :cond_0

    new-instance v9, Ljava/lang/Thread;

    new-instance v0, Lcom/konka/mm/samba/SmbClient$scanHostByPing;

    add-int/lit8 v3, v7, 0x1

    mul-int/lit16 v1, v7, 0x100

    div-int/lit8 v1, v1, 0x10

    add-int/lit8 v4, v1, 0x1

    mul-int/lit16 v1, v7, 0x100

    div-int/lit8 v1, v1, 0x10

    add-int/lit8 v5, v1, 0x10

    iget-object v6, p0, Lcom/konka/mm/samba/SmbClient;->onRecvMsglistener:Lcom/konka/mm/samba/OnRecvMsgListener;

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/konka/mm/samba/SmbClient$scanHostByPing;-><init>(Lcom/konka/mm/samba/SmbClient;Ljava/lang/String;IIILcom/konka/mm/samba/OnRecvMsgListener;)V

    invoke-direct {v9, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v8, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_3

    :goto_2
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    :cond_3
    invoke-virtual {v8, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/konka/mm/samba/SmbClient;->isScanning:Z

    goto :goto_2
.end method
