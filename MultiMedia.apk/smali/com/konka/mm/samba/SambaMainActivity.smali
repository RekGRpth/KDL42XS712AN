.class public Lcom/konka/mm/samba/SambaMainActivity;
.super Landroid/app/Activity;
.source "SambaMainActivity.java"


# static fields
.field public static TAG:Ljava/lang/String;


# instance fields
.field protected cancle_btn:Landroid/widget/Button;

.field protected search_btn:Landroid/widget/Button;

.field protected version_text:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "SambaMainActivity"

    sput-object v0, Lcom/konka/mm/samba/SambaMainActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # Landroid/view/KeyEvent;

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    invoke-super {p0, p1}, Landroid/app/Activity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    return v0

    :pswitch_0
    invoke-virtual {p0}, Lcom/konka/mm/samba/SambaMainActivity;->finish()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method

.method public findView()V
    .locals 1

    const v0, 0x7f0b00b0    # com.konka.mm.R.id.version_text

    invoke-virtual {p0, v0}, Lcom/konka/mm/samba/SambaMainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/mm/samba/SambaMainActivity;->version_text:Landroid/widget/TextView;

    const v0, 0x7f0b00b1    # com.konka.mm.R.id.search_btn

    invoke-virtual {p0, v0}, Lcom/konka/mm/samba/SambaMainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/mm/samba/SambaMainActivity;->search_btn:Landroid/widget/Button;

    const v0, 0x7f0b00b2    # com.konka.mm.R.id.cancle_btn

    invoke-virtual {p0, v0}, Lcom/konka/mm/samba/SambaMainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/mm/samba/SambaMainActivity;->cancle_btn:Landroid/widget/Button;

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    sget-object v1, Lcom/konka/mm/finals/CommonFinals;->MM_LOG_TAG:Ljava/lang/String;

    invoke-static {v1}, Liapp/eric/utils/base/Trace;->setTag(Ljava/lang/String;)V

    const v1, 0x7f03002c    # com.konka.mm.R.layout.samba_main

    invoke-virtual {p0, v1}, Lcom/konka/mm/samba/SambaMainActivity;->setContentView(I)V

    invoke-virtual {p0}, Lcom/konka/mm/samba/SambaMainActivity;->findView()V

    :try_start_0
    iget-object v1, p0, Lcom/konka/mm/samba/SambaMainActivity;->version_text:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    const v3, 0x7f09000e    # com.konka.mm.R.string.Version

    invoke-virtual {p0, v3}, Lcom/konka/mm/samba/SambaMainActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/konka/mm/samba/SambaMainActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {p0}, Lcom/konka/mm/samba/SambaMainActivity;->getPackageName()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v3

    iget-object v3, v3, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".00 "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x7f09000f    # com.konka.mm.R.string.Build

    invoke-virtual {p0, v3}, Lcom/konka/mm/samba/SambaMainActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/konka/mm/samba/SambaMainActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {p0}, Lcom/konka/mm/samba/SambaMainActivity;->getPackageName()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v3

    iget v3, v3, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v1, p0, Lcom/konka/mm/samba/SambaMainActivity;->search_btn:Landroid/widget/Button;

    new-instance v2, Lcom/konka/mm/samba/SambaMainActivity$1;

    invoke-direct {v2, p0}, Lcom/konka/mm/samba/SambaMainActivity$1;-><init>(Lcom/konka/mm/samba/SambaMainActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/konka/mm/samba/SambaMainActivity;->cancle_btn:Landroid/widget/Button;

    new-instance v2, Lcom/konka/mm/samba/SambaMainActivity$2;

    invoke-direct {v2, p0}, Lcom/konka/mm/samba/SambaMainActivity$2;-><init>(Lcom/konka/mm/samba/SambaMainActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 4
    .param p1    # Landroid/view/Menu;

    const/4 v0, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    const-string v3, "\u5173\u4e8e"

    invoke-interface {p1, v0, v1, v2, v3}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v3

    return v3
.end method

.method protected onDestroy()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/System;->exit(I)V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1    # Landroid/view/MenuItem;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    const/4 v0, 0x0

    return v0

    :pswitch_0
    invoke-static {p0}, Lcom/konka/mm/tools/VersionTool;->VersionShow(Landroid/content/Context;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
