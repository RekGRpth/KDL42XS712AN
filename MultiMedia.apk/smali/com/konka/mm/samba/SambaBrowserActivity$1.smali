.class Lcom/konka/mm/samba/SambaBrowserActivity$1;
.super Landroid/os/Handler;
.source "SambaBrowserActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/mm/samba/SambaBrowserActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/mm/samba/SambaBrowserActivity;


# direct methods
.method constructor <init>(Lcom/konka/mm/samba/SambaBrowserActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/samba/SambaBrowserActivity$1;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 8
    .param p1    # Landroid/os/Message;

    const/16 v7, 0x11

    const/16 v6, 0xb

    const/4 v5, 0x2

    const/4 v3, 0x1

    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    iget-object v2, p0, Lcom/konka/mm/samba/SambaBrowserActivity$1;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    invoke-virtual {v2, v3}, Lcom/konka/mm/samba/SambaBrowserActivity;->hideDialog(I)V

    iget v2, p1, Landroid/os/Message;->what:I

    sparse-switch v2, :sswitch_data_0

    :cond_0
    :goto_0
    return-void

    :sswitch_0
    iget-object v2, p0, Lcom/konka/mm/samba/SambaBrowserActivity$1;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    invoke-virtual {v2, v5}, Lcom/konka/mm/samba/SambaBrowserActivity;->hideDialog(I)V

    goto :goto_0

    :sswitch_1
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "scan completed scanHandler message!!"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/mm/samba/SambaBrowserActivity$1;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Lcom/konka/mm/samba/SambaBrowserActivity;->hideDialog(I)V

    iget-object v2, p0, Lcom/konka/mm/samba/SambaBrowserActivity$1;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    # getter for: Lcom/konka/mm/samba/SambaBrowserActivity;->viewHolder:Lcom/konka/mm/samba/SambaBrowserViewHolder;
    invoke-static {v2}, Lcom/konka/mm/samba/SambaBrowserActivity;->access$0(Lcom/konka/mm/samba/SambaBrowserActivity;)Lcom/konka/mm/samba/SambaBrowserViewHolder;

    move-result-object v2

    invoke-virtual {v2, v6}, Lcom/konka/mm/samba/SambaBrowserViewHolder;->FillHostDataView(I)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/konka/mm/samba/SambaBrowserActivity$1;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Lcom/konka/mm/samba/SambaBrowserActivity;->messageShow(I)V

    goto :goto_0

    :sswitch_2
    iget-object v2, p0, Lcom/konka/mm/samba/SambaBrowserActivity$1;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    iget v2, v2, Lcom/konka/mm/samba/SambaBrowserActivity;->ThreadState:I

    if-ne v3, v2, :cond_1

    iget-object v2, p0, Lcom/konka/mm/samba/SambaBrowserActivity$1;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    iget-boolean v2, v2, Lcom/konka/mm/samba/SambaBrowserActivity;->keyResponse:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/konka/mm/samba/SambaBrowserActivity$1;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    # getter for: Lcom/konka/mm/samba/SambaBrowserActivity;->viewHolder:Lcom/konka/mm/samba/SambaBrowserViewHolder;
    invoke-static {v2}, Lcom/konka/mm/samba/SambaBrowserActivity;->access$0(Lcom/konka/mm/samba/SambaBrowserActivity;)Lcom/konka/mm/samba/SambaBrowserViewHolder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/konka/mm/samba/SambaBrowserViewHolder;->returnToPreviousFolder()V

    :cond_1
    iget-object v2, p0, Lcom/konka/mm/samba/SambaBrowserActivity$1;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    # getter for: Lcom/konka/mm/samba/SambaBrowserActivity;->viewHolder:Lcom/konka/mm/samba/SambaBrowserViewHolder;
    invoke-static {v2}, Lcom/konka/mm/samba/SambaBrowserActivity;->access$0(Lcom/konka/mm/samba/SambaBrowserActivity;)Lcom/konka/mm/samba/SambaBrowserViewHolder;

    move-result-object v2

    iget-object v2, v2, Lcom/konka/mm/samba/SambaBrowserViewHolder;->toast_noshare_file:Landroid/widget/Toast;

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    iget-object v2, p0, Lcom/konka/mm/samba/SambaBrowserActivity$1;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    invoke-virtual {v2}, Lcom/konka/mm/samba/SambaBrowserActivity;->hideInputDlg()V

    goto :goto_0

    :sswitch_3
    const-string v2, "SambaBrowserActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "host login ok! useName:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/konka/mm/samba/SambaBrowserActivity$1;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    # getter for: Lcom/konka/mm/samba/SambaBrowserActivity;->userName:Ljava/lang/String;
    invoke-static {v4}, Lcom/konka/mm/samba/SambaBrowserActivity;->access$1(Lcom/konka/mm/samba/SambaBrowserActivity;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " password:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/mm/samba/SambaBrowserActivity$1;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    # getter for: Lcom/konka/mm/samba/SambaBrowserActivity;->passWord:Ljava/lang/String;
    invoke-static {v4}, Lcom/konka/mm/samba/SambaBrowserActivity;->access$2(Lcom/konka/mm/samba/SambaBrowserActivity;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/konka/mm/samba/SambaBrowserActivity$1;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    # getter for: Lcom/konka/mm/samba/SambaBrowserActivity;->userName:Ljava/lang/String;
    invoke-static {v2}, Lcom/konka/mm/samba/SambaBrowserActivity;->access$1(Lcom/konka/mm/samba/SambaBrowserActivity;)Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/konka/mm/samba/SambaBrowserActivity$1;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    # getter for: Lcom/konka/mm/samba/SambaBrowserActivity;->passWord:Ljava/lang/String;
    invoke-static {v2}, Lcom/konka/mm/samba/SambaBrowserActivity;->access$2(Lcom/konka/mm/samba/SambaBrowserActivity;)Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/konka/mm/samba/SambaBrowserActivity$1;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    iget-object v2, v2, Lcom/konka/mm/samba/SambaBrowserActivity;->gxdbhelper:Lcom/konka/mm/samba/GXdbHelper;

    iget-object v3, p0, Lcom/konka/mm/samba/SambaBrowserActivity$1;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    # getter for: Lcom/konka/mm/samba/SambaBrowserActivity;->viewHolder:Lcom/konka/mm/samba/SambaBrowserViewHolder;
    invoke-static {v3}, Lcom/konka/mm/samba/SambaBrowserActivity;->access$0(Lcom/konka/mm/samba/SambaBrowserActivity;)Lcom/konka/mm/samba/SambaBrowserViewHolder;

    move-result-object v3

    iget-object v3, v3, Lcom/konka/mm/samba/SambaBrowserViewHolder;->selectDevice:Lcom/konka/mm/samba/SmbDevice;

    invoke-virtual {v3}, Lcom/konka/mm/samba/SmbDevice;->getAddress()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/konka/mm/samba/GXdbHelper;->delectHomeShareDB(Ljava/lang/String;)V

    new-instance v1, Lcom/konka/mm/data/HomeShareData;

    iget-object v2, p0, Lcom/konka/mm/samba/SambaBrowserActivity$1;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    # getter for: Lcom/konka/mm/samba/SambaBrowserActivity;->viewHolder:Lcom/konka/mm/samba/SambaBrowserViewHolder;
    invoke-static {v2}, Lcom/konka/mm/samba/SambaBrowserActivity;->access$0(Lcom/konka/mm/samba/SambaBrowserActivity;)Lcom/konka/mm/samba/SambaBrowserViewHolder;

    move-result-object v2

    iget-object v2, v2, Lcom/konka/mm/samba/SambaBrowserViewHolder;->selectDevice:Lcom/konka/mm/samba/SmbDevice;

    invoke-virtual {v2}, Lcom/konka/mm/samba/SmbDevice;->getAddress()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/konka/mm/samba/SambaBrowserActivity$1;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    # getter for: Lcom/konka/mm/samba/SambaBrowserActivity;->userName:Ljava/lang/String;
    invoke-static {v3}, Lcom/konka/mm/samba/SambaBrowserActivity;->access$1(Lcom/konka/mm/samba/SambaBrowserActivity;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/mm/samba/SambaBrowserActivity$1;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    # getter for: Lcom/konka/mm/samba/SambaBrowserActivity;->passWord:Ljava/lang/String;
    invoke-static {v4}, Lcom/konka/mm/samba/SambaBrowserActivity;->access$2(Lcom/konka/mm/samba/SambaBrowserActivity;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/konka/mm/data/HomeShareData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/mm/samba/SambaBrowserActivity$1;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    iget-object v2, v2, Lcom/konka/mm/samba/SambaBrowserActivity;->gxdbhelper:Lcom/konka/mm/samba/GXdbHelper;

    invoke-virtual {v2, v1}, Lcom/konka/mm/samba/GXdbHelper;->insertHomeShareData(Lcom/konka/mm/data/HomeShareData;)V

    :cond_2
    iget-object v2, p0, Lcom/konka/mm/samba/SambaBrowserActivity$1;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    # getter for: Lcom/konka/mm/samba/SambaBrowserActivity;->viewHolder:Lcom/konka/mm/samba/SambaBrowserViewHolder;
    invoke-static {v2}, Lcom/konka/mm/samba/SambaBrowserActivity;->access$0(Lcom/konka/mm/samba/SambaBrowserActivity;)Lcom/konka/mm/samba/SambaBrowserViewHolder;

    move-result-object v2

    iget-object v2, v2, Lcom/konka/mm/samba/SambaBrowserViewHolder;->currentFolderList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_3

    iget-object v2, p0, Lcom/konka/mm/samba/SambaBrowserActivity$1;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    # getter for: Lcom/konka/mm/samba/SambaBrowserActivity;->isPopup:Z
    invoke-static {v2}, Lcom/konka/mm/samba/SambaBrowserActivity;->access$3(Lcom/konka/mm/samba/SambaBrowserActivity;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/konka/mm/samba/SambaBrowserActivity$1;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    invoke-virtual {v2}, Lcom/konka/mm/samba/SambaBrowserActivity;->hideInputDlg()V

    :cond_3
    iget-object v2, p0, Lcom/konka/mm/samba/SambaBrowserActivity$1;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    # getter for: Lcom/konka/mm/samba/SambaBrowserActivity;->viewHolder:Lcom/konka/mm/samba/SambaBrowserViewHolder;
    invoke-static {v2}, Lcom/konka/mm/samba/SambaBrowserActivity;->access$0(Lcom/konka/mm/samba/SambaBrowserActivity;)Lcom/konka/mm/samba/SambaBrowserViewHolder;

    move-result-object v2

    invoke-virtual {v2, v6}, Lcom/konka/mm/samba/SambaBrowserViewHolder;->FillFolderDataView(I)Z

    iget-object v2, p0, Lcom/konka/mm/samba/SambaBrowserActivity$1;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    invoke-virtual {v2, v7}, Lcom/konka/mm/samba/SambaBrowserActivity;->hideDialog(I)V

    iget-object v2, p0, Lcom/konka/mm/samba/SambaBrowserActivity$1;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    invoke-virtual {v2, v5}, Lcom/konka/mm/samba/SambaBrowserActivity;->hideDialog(I)V

    goto/16 :goto_0

    :sswitch_4
    const-string v2, "SambaBrowserActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "use userName:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/konka/mm/samba/SambaBrowserActivity$1;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    # getter for: Lcom/konka/mm/samba/SambaBrowserActivity;->userName:Ljava/lang/String;
    invoke-static {v4}, Lcom/konka/mm/samba/SambaBrowserActivity;->access$1(Lcom/konka/mm/samba/SambaBrowserActivity;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " passWord:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/mm/samba/SambaBrowserActivity$1;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    # getter for: Lcom/konka/mm/samba/SambaBrowserActivity;->passWord:Ljava/lang/String;
    invoke-static {v4}, Lcom/konka/mm/samba/SambaBrowserActivity;->access$2(Lcom/konka/mm/samba/SambaBrowserActivity;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "  login failed"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/konka/mm/samba/SambaBrowserActivity$1;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    # getter for: Lcom/konka/mm/samba/SambaBrowserActivity;->userName:Ljava/lang/String;
    invoke-static {v2}, Lcom/konka/mm/samba/SambaBrowserActivity;->access$1(Lcom/konka/mm/samba/SambaBrowserActivity;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/konka/mm/samba/SambaBrowserActivity$1;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    # getter for: Lcom/konka/mm/samba/SambaBrowserActivity;->userName:Ljava/lang/String;
    invoke-static {v2}, Lcom/konka/mm/samba/SambaBrowserActivity;->access$1(Lcom/konka/mm/samba/SambaBrowserActivity;)Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/konka/mm/samba/SambaBrowserActivity$1;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    iget-object v2, v2, Lcom/konka/mm/samba/SambaBrowserActivity;->gxdbhelper:Lcom/konka/mm/samba/GXdbHelper;

    iget-object v3, p0, Lcom/konka/mm/samba/SambaBrowserActivity$1;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    # getter for: Lcom/konka/mm/samba/SambaBrowserActivity;->viewHolder:Lcom/konka/mm/samba/SambaBrowserViewHolder;
    invoke-static {v3}, Lcom/konka/mm/samba/SambaBrowserActivity;->access$0(Lcom/konka/mm/samba/SambaBrowserActivity;)Lcom/konka/mm/samba/SambaBrowserViewHolder;

    move-result-object v3

    iget-object v3, v3, Lcom/konka/mm/samba/SambaBrowserViewHolder;->selectDevice:Lcom/konka/mm/samba/SmbDevice;

    invoke-virtual {v3}, Lcom/konka/mm/samba/SmbDevice;->getAddress()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/konka/mm/samba/GXdbHelper;->getHomeShareDB(Ljava/lang/String;)Lcom/konka/mm/data/HomeShareData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/mm/data/HomeShareData;->getUsename()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_4

    invoke-virtual {v0}, Lcom/konka/mm/data/HomeShareData;->getUsename()Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/konka/mm/samba/SambaBrowserActivity$1;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    invoke-virtual {v0}, Lcom/konka/mm/data/HomeShareData;->getUsename()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/konka/mm/samba/SambaBrowserActivity;->access$4(Lcom/konka/mm/samba/SambaBrowserActivity;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/mm/samba/SambaBrowserActivity$1;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    invoke-virtual {v0}, Lcom/konka/mm/data/HomeShareData;->getPassword()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/konka/mm/samba/SambaBrowserActivity;->access$5(Lcom/konka/mm/samba/SambaBrowserActivity;Ljava/lang/String;)V

    const-string v2, "SambaBrowserActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "there is a account of the host:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/konka/mm/samba/SambaBrowserActivity$1;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    # getter for: Lcom/konka/mm/samba/SambaBrowserActivity;->viewHolder:Lcom/konka/mm/samba/SambaBrowserViewHolder;
    invoke-static {v4}, Lcom/konka/mm/samba/SambaBrowserActivity;->access$0(Lcom/konka/mm/samba/SambaBrowserActivity;)Lcom/konka/mm/samba/SambaBrowserViewHolder;

    move-result-object v4

    iget-object v4, v4, Lcom/konka/mm/samba/SambaBrowserViewHolder;->selectDevice:Lcom/konka/mm/samba/SmbDevice;

    invoke-virtual {v4}, Lcom/konka/mm/samba/SmbDevice;->getAddress()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " in the databace?\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", the userName:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/mm/samba/SambaBrowserActivity$1;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    # getter for: Lcom/konka/mm/samba/SambaBrowserActivity;->userName:Ljava/lang/String;
    invoke-static {v4}, Lcom/konka/mm/samba/SambaBrowserActivity;->access$1(Lcom/konka/mm/samba/SambaBrowserActivity;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " passWord:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/mm/samba/SambaBrowserActivity$1;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    # getter for: Lcom/konka/mm/samba/SambaBrowserActivity;->passWord:Ljava/lang/String;
    invoke-static {v4}, Lcom/konka/mm/samba/SambaBrowserActivity;->access$2(Lcom/konka/mm/samba/SambaBrowserActivity;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v2, Ljava/lang/Thread;

    new-instance v3, Lcom/konka/mm/samba/SambaBrowserActivity$LoginRunnable;

    iget-object v4, p0, Lcom/konka/mm/samba/SambaBrowserActivity$1;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    invoke-direct {v3, v4}, Lcom/konka/mm/samba/SambaBrowserActivity$LoginRunnable;-><init>(Lcom/konka/mm/samba/SambaBrowserActivity;)V

    invoke-direct {v2, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    goto/16 :goto_0

    :cond_4
    iget-object v2, p0, Lcom/konka/mm/samba/SambaBrowserActivity$1;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    const-string v3, "test"

    invoke-static {v2, v3}, Lcom/konka/mm/samba/SambaBrowserActivity;->access$4(Lcom/konka/mm/samba/SambaBrowserActivity;Ljava/lang/String;)V

    const-string v2, "SambaBrowserActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "there isnt host:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/konka/mm/samba/SambaBrowserActivity$1;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    # getter for: Lcom/konka/mm/samba/SambaBrowserActivity;->viewHolder:Lcom/konka/mm/samba/SambaBrowserViewHolder;
    invoke-static {v4}, Lcom/konka/mm/samba/SambaBrowserActivity;->access$0(Lcom/konka/mm/samba/SambaBrowserActivity;)Lcom/konka/mm/samba/SambaBrowserViewHolder;

    move-result-object v4

    iget-object v4, v4, Lcom/konka/mm/samba/SambaBrowserViewHolder;->selectDevice:Lcom/konka/mm/samba/SmbDevice;

    invoke-virtual {v4}, Lcom/konka/mm/samba/SmbDevice;->getAddress()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " account in the databace!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    iget-object v2, p0, Lcom/konka/mm/samba/SambaBrowserActivity$1;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    # getter for: Lcom/konka/mm/samba/SambaBrowserActivity;->isPopup:Z
    invoke-static {v2}, Lcom/konka/mm/samba/SambaBrowserActivity;->access$3(Lcom/konka/mm/samba/SambaBrowserActivity;)Z

    move-result v2

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/konka/mm/samba/SambaBrowserActivity$1;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    # getter for: Lcom/konka/mm/samba/SambaBrowserActivity;->viewHolder:Lcom/konka/mm/samba/SambaBrowserViewHolder;
    invoke-static {v2}, Lcom/konka/mm/samba/SambaBrowserActivity;->access$0(Lcom/konka/mm/samba/SambaBrowserActivity;)Lcom/konka/mm/samba/SambaBrowserViewHolder;

    move-result-object v2

    iget-object v2, v2, Lcom/konka/mm/samba/SambaBrowserViewHolder;->toast_login_error:Landroid/widget/Toast;

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    :cond_6
    iget-object v2, p0, Lcom/konka/mm/samba/SambaBrowserActivity$1;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    invoke-virtual {v2}, Lcom/konka/mm/samba/SambaBrowserActivity;->showInputDlg()V

    goto/16 :goto_0

    :sswitch_5
    iget-object v2, p0, Lcom/konka/mm/samba/SambaBrowserActivity$1;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    invoke-virtual {v2, v7}, Lcom/konka/mm/samba/SambaBrowserActivity;->hideDialog(I)V

    new-instance v2, Ljava/lang/Thread;

    new-instance v3, Lcom/konka/mm/samba/SambaBrowserActivity$SambaRunnable;

    iget-object v4, p0, Lcom/konka/mm/samba/SambaBrowserActivity$1;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    invoke-direct {v3, v4}, Lcom/konka/mm/samba/SambaBrowserActivity$SambaRunnable;-><init>(Lcom/konka/mm/samba/SambaBrowserActivity;)V

    invoke-direct {v2, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_1
        0x31 -> :sswitch_0
        0x32 -> :sswitch_5
        0x33 -> :sswitch_4
        0x34 -> :sswitch_3
        0x36 -> :sswitch_2
    .end sparse-switch
.end method
