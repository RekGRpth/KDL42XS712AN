.class public Lcom/konka/mm/samba/GXdbHelper;
.super Ljava/lang/Object;
.source "GXdbHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/mm/samba/GXdbHelper$MyDBHelper;
    }
.end annotation


# static fields
.field public static final DB_HOMESHARE:Ljava/lang/String; = "homeshare"

.field public static final DB_TABLENAME:Ljava/lang/String; = "gongxiangdb.db"

.field public static final TAG:Ljava/lang/String; = "GXdbHelper"

.field public static final VERSION:I = 0x6


# instance fields
.field private mContext:Landroid/content/Context;

.field private mMyDBHelper:Lcom/konka/mm/samba/GXdbHelper$MyDBHelper;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/konka/mm/samba/GXdbHelper;->mContext:Landroid/content/Context;

    new-instance v0, Lcom/konka/mm/samba/GXdbHelper$MyDBHelper;

    iget-object v2, p0, Lcom/konka/mm/samba/GXdbHelper;->mContext:Landroid/content/Context;

    const-string v3, "gongxiangdb.db"

    const/4 v4, 0x0

    const/4 v5, 0x6

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/konka/mm/samba/GXdbHelper$MyDBHelper;-><init>(Lcom/konka/mm/samba/GXdbHelper;Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    iput-object v0, p0, Lcom/konka/mm/samba/GXdbHelper;->mMyDBHelper:Lcom/konka/mm/samba/GXdbHelper$MyDBHelper;

    return-void
.end method


# virtual methods
.method public delectHomeShareDB(Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;

    iget-object v1, p0, Lcom/konka/mm/samba/GXdbHelper;->mMyDBHelper:Lcom/konka/mm/samba/GXdbHelper$MyDBHelper;

    invoke-virtual {v1}, Lcom/konka/mm/samba/GXdbHelper$MyDBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    :try_start_0
    const-string v1, "homeshare"

    const-string v2, "hostip=?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    return-void

    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v1
.end method

.method public deleteAllDate()V
    .locals 4

    iget-object v1, p0, Lcom/konka/mm/samba/GXdbHelper;->mMyDBHelper:Lcom/konka/mm/samba/GXdbHelper$MyDBHelper;

    invoke-virtual {v1}, Lcom/konka/mm/samba/GXdbHelper$MyDBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    :try_start_0
    const-string v1, "homeshare"

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    return-void

    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v1
.end method

.method public getHomeShareDB(Ljava/lang/String;)Lcom/konka/mm/data/HomeShareData;
    .locals 10
    .param p1    # Ljava/lang/String;

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    iget-object v1, p0, Lcom/konka/mm/samba/GXdbHelper;->mMyDBHelper:Lcom/konka/mm/samba/GXdbHelper$MyDBHelper;

    invoke-virtual {v1}, Lcom/konka/mm/samba/GXdbHelper$MyDBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    new-instance v9, Lcom/konka/mm/data/HomeShareData;

    invoke-direct {v9}, Lcom/konka/mm/data/HomeShareData;-><init>()V

    const-string v1, "homeshare"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "hostip"

    aput-object v3, v2, v6

    const-string v3, "usename"

    aput-object v3, v2, v7

    const/4 v3, 0x2

    const-string v4, "password"

    aput-object v4, v2, v3

    const-string v3, "hostip=?"

    new-array v4, v7, [Ljava/lang/String;

    aput-object p1, v4, v6

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    return-object v9

    :cond_0
    const-string v1, "hostip"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v1}, Lcom/konka/mm/data/HomeShareData;->setHostip(Ljava/lang/String;)V

    const-string v1, "usename"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v1}, Lcom/konka/mm/data/HomeShareData;->setUsename(Ljava/lang/String;)V

    const-string v1, "password"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v1}, Lcom/konka/mm/data/HomeShareData;->setPassword(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getHomeShareDatas()Ljava/util/ArrayList;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/konka/mm/data/HomeShareData;",
            ">;"
        }
    .end annotation

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/konka/mm/samba/GXdbHelper;->mMyDBHelper:Lcom/konka/mm/samba/GXdbHelper$MyDBHelper;

    invoke-virtual {v1}, Lcom/konka/mm/samba/GXdbHelper$MyDBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    const-string v1, "homeshare"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "hostip"

    aput-object v5, v2, v4

    const/4 v4, 0x1

    const-string v5, "usename"

    aput-object v5, v2, v4

    const/4 v4, 0x2

    const-string v5, "password"

    aput-object v5, v2, v4

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    move-object v7, v3

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    return-object v10

    :cond_0
    new-instance v9, Lcom/konka/mm/data/HomeShareData;

    invoke-direct {v9}, Lcom/konka/mm/data/HomeShareData;-><init>()V

    const-string v1, "hostip"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v1}, Lcom/konka/mm/data/HomeShareData;->setHostip(Ljava/lang/String;)V

    const-string v1, "usename"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v1}, Lcom/konka/mm/data/HomeShareData;->setUsename(Ljava/lang/String;)V

    const-string v1, "password"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v1}, Lcom/konka/mm/data/HomeShareData;->setPassword(Ljava/lang/String;)V

    invoke-virtual {v10, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public getTotalCount()I
    .locals 11

    const/4 v10, 0x0

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/konka/mm/samba/GXdbHelper;->mMyDBHelper:Lcom/konka/mm/samba/GXdbHelper$MyDBHelper;

    invoke-virtual {v1}, Lcom/konka/mm/samba/GXdbHelper$MyDBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    const-string v1, "homeshare"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const-string v4, "count(*)"

    aput-object v4, v2, v10

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    move-object v7, v3

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    invoke-interface {v8, v10}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    return v9
.end method

.method public insertHomeShareData(Lcom/konka/mm/data/HomeShareData;)V
    .locals 5
    .param p1    # Lcom/konka/mm/data/HomeShareData;

    iget-object v2, p0, Lcom/konka/mm/samba/GXdbHelper;->mMyDBHelper:Lcom/konka/mm/samba/GXdbHelper$MyDBHelper;

    invoke-virtual {v2}, Lcom/konka/mm/samba/GXdbHelper$MyDBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    const-string v2, "hostip"

    invoke-virtual {p1}, Lcom/konka/mm/data/HomeShareData;->getHostip()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "usename"

    invoke-virtual {p1}, Lcom/konka/mm/data/HomeShareData;->getUsename()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "password"

    invoke-virtual {p1}, Lcom/konka/mm/data/HomeShareData;->getPassword()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    const-string v2, "homeshare"

    const/4 v3, 0x0

    const/4 v4, 0x5

    invoke-virtual {v0, v2, v3, v1, v4}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    return-void

    :catchall_0
    move-exception v2

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v2
.end method

.method public updateHomeShareDB(Ljava/lang/String;Lcom/konka/mm/data/HomeShareData;)V
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/konka/mm/data/HomeShareData;

    iget-object v2, p0, Lcom/konka/mm/samba/GXdbHelper;->mMyDBHelper:Lcom/konka/mm/samba/GXdbHelper$MyDBHelper;

    invoke-virtual {v2}, Lcom/konka/mm/samba/GXdbHelper$MyDBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    const-string v2, "hostip"

    invoke-virtual {p2}, Lcom/konka/mm/data/HomeShareData;->getHostip()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "usename"

    invoke-virtual {p2}, Lcom/konka/mm/data/HomeShareData;->getUsename()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "password"

    invoke-virtual {p2}, Lcom/konka/mm/data/HomeShareData;->getPassword()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    const-string v2, "homeshare"

    const-string v3, "hostip=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-virtual {v0, v2, v1, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    return-void

    :catchall_0
    move-exception v2

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v2
.end method
