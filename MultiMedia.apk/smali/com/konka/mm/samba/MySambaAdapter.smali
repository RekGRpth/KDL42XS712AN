.class public Lcom/konka/mm/samba/MySambaAdapter;
.super Landroid/widget/BaseAdapter;
.source "MySambaAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/mm/samba/MySambaAdapter$ListItemViewHolder;
    }
.end annotation


# instance fields
.field private context:Lcom/konka/mm/samba/SambaBrowserActivity;

.field private indexPage:I

.field private layoutInflater:Landroid/view/LayoutInflater;

.field private list:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/konka/mm/samba/SambaListItem;",
            ">;"
        }
    .end annotation
.end field

.field private pageCount:I

.field private pageNum:I


# direct methods
.method public constructor <init>(Lcom/konka/mm/samba/SambaBrowserActivity;Ljava/util/List;)V
    .locals 1
    .param p1    # Lcom/konka/mm/samba/SambaBrowserActivity;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/konka/mm/samba/SambaBrowserActivity;",
            "Ljava/util/List",
            "<",
            "Lcom/konka/mm/samba/SambaListItem;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    iput-object p1, p0, Lcom/konka/mm/samba/MySambaAdapter;->context:Lcom/konka/mm/samba/SambaBrowserActivity;

    iput-object p2, p0, Lcom/konka/mm/samba/MySambaAdapter;->list:Ljava/util/List;

    iget-object v0, p0, Lcom/konka/mm/samba/MySambaAdapter;->context:Lcom/konka/mm/samba/SambaBrowserActivity;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/mm/samba/MySambaAdapter;->layoutInflater:Landroid/view/LayoutInflater;

    return-void
.end method

.method public constructor <init>(Lcom/konka/mm/samba/SambaBrowserActivity;Ljava/util/List;III)V
    .locals 1
    .param p1    # Lcom/konka/mm/samba/SambaBrowserActivity;
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/konka/mm/samba/SambaBrowserActivity;",
            "Ljava/util/List",
            "<",
            "Lcom/konka/mm/samba/SambaListItem;",
            ">;III)V"
        }
    .end annotation

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    iput-object p1, p0, Lcom/konka/mm/samba/MySambaAdapter;->context:Lcom/konka/mm/samba/SambaBrowserActivity;

    iput-object p2, p0, Lcom/konka/mm/samba/MySambaAdapter;->list:Ljava/util/List;

    iput p3, p0, Lcom/konka/mm/samba/MySambaAdapter;->indexPage:I

    iput p4, p0, Lcom/konka/mm/samba/MySambaAdapter;->pageNum:I

    iput p5, p0, Lcom/konka/mm/samba/MySambaAdapter;->pageCount:I

    iget-object v0, p0, Lcom/konka/mm/samba/MySambaAdapter;->context:Lcom/konka/mm/samba/SambaBrowserActivity;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/mm/samba/MySambaAdapter;->layoutInflater:Landroid/view/LayoutInflater;

    return-void
.end method

.method private setIcon(Landroid/widget/ImageView;Ljava/lang/String;)V
    .locals 0
    .param p1    # Landroid/widget/ImageView;
    .param p2    # Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    iget v0, p0, Lcom/konka/mm/samba/MySambaAdapter;->pageNum:I

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1    # I

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1    # I

    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    const/4 v5, 0x0

    const v8, 0x7f020084    # com.konka.mm.R.drawable.music_listadapter

    const v7, 0x7f02006b    # com.konka.mm.R.drawable.media_default_music_uns

    const/4 v6, 0x1

    const/4 v1, 0x0

    if-nez p2, :cond_4

    new-instance v1, Lcom/konka/mm/samba/MySambaAdapter$ListItemViewHolder;

    invoke-direct {v1}, Lcom/konka/mm/samba/MySambaAdapter$ListItemViewHolder;-><init>()V

    iget-object v3, p0, Lcom/konka/mm/samba/MySambaAdapter;->context:Lcom/konka/mm/samba/SambaBrowserActivity;

    iget v3, v3, Lcom/konka/mm/samba/SambaBrowserActivity;->List_Mode:I

    if-ne v3, v6, :cond_3

    iget-object v3, p0, Lcom/konka/mm/samba/MySambaAdapter;->layoutInflater:Landroid/view/LayoutInflater;

    const v4, 0x7f030012    # com.konka.mm.R.layout.listadapter

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    const v3, 0x7f0b0026    # com.konka.mm.R.id.picitem

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, v1, Lcom/konka/mm/samba/MySambaAdapter$ListItemViewHolder;->iconImage:Landroid/widget/ImageView;

    const v3, 0x7f0b0028    # com.konka.mm.R.id.textitem

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v1, Lcom/konka/mm/samba/MySambaAdapter$ListItemViewHolder;->nameText:Landroid/widget/TextView;

    :goto_0
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :goto_1
    :try_start_0
    iget-object v3, p0, Lcom/konka/mm/samba/MySambaAdapter;->list:Ljava/util/List;

    iget v4, p0, Lcom/konka/mm/samba/MySambaAdapter;->indexPage:I

    iget v5, p0, Lcom/konka/mm/samba/MySambaAdapter;->pageCount:I

    mul-int/2addr v4, v5

    add-int/2addr v4, p1

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/konka/mm/samba/SambaListItem;

    if-eqz v2, :cond_2

    iget v3, v2, Lcom/konka/mm/samba/SambaListItem;->icon:I

    const v4, 0x7f02006a    # com.konka.mm.R.drawable.media_default_images_uns

    if-ne v3, v4, :cond_5

    iget-object v3, v1, Lcom/konka/mm/samba/MySambaAdapter$ListItemViewHolder;->iconImage:Landroid/widget/ImageView;

    iget v4, v2, Lcom/konka/mm/samba/SambaListItem;->icon:I

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v3, p0, Lcom/konka/mm/samba/MySambaAdapter;->context:Lcom/konka/mm/samba/SambaBrowserActivity;

    iget v3, v3, Lcom/konka/mm/samba/SambaBrowserActivity;->List_Mode:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    iget-object v3, v1, Lcom/konka/mm/samba/MySambaAdapter$ListItemViewHolder;->handler:Lcom/konka/mm/photo/ImageThumbHandler;

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Lcom/konka/mm/samba/SambaListItem;->getPath()Ljava/lang/String;

    move-result-object v3

    iget-object v4, v1, Lcom/konka/mm/samba/MySambaAdapter$ListItemViewHolder;->handler:Lcom/konka/mm/photo/ImageThumbHandler;

    invoke-static {v3, v4}, Lcom/konka/mm/photo/PhotoThumbLoader;->remove(Ljava/lang/String;Lcom/konka/mm/photo/ImageThumbHandler;)Z

    :cond_0
    new-instance v3, Lcom/konka/mm/photo/ThumbFileDownloadHandler;

    iget-object v4, v1, Lcom/konka/mm/samba/MySambaAdapter$ListItemViewHolder;->iconImage:Landroid/widget/ImageView;

    invoke-direct {v3, v4}, Lcom/konka/mm/photo/ThumbFileDownloadHandler;-><init>(Landroid/widget/ImageView;)V

    iput-object v3, v1, Lcom/konka/mm/samba/MySambaAdapter$ListItemViewHolder;->handler:Lcom/konka/mm/photo/ImageThumbHandler;

    invoke-virtual {v2}, Lcom/konka/mm/samba/SambaListItem;->getPath()Ljava/lang/String;

    move-result-object v3

    iget-object v4, v1, Lcom/konka/mm/samba/MySambaAdapter$ListItemViewHolder;->handler:Lcom/konka/mm/photo/ImageThumbHandler;

    invoke-static {v3, v4}, Lcom/konka/mm/photo/PhotoThumbLoader;->start(Ljava/lang/String;Lcom/konka/mm/photo/ImageThumbHandler;)Z

    :cond_1
    :goto_2
    iget-object v3, v1, Lcom/konka/mm/samba/MySambaAdapter$ListItemViewHolder;->nameText:Landroid/widget/TextView;

    invoke-virtual {v2}, Lcom/konka/mm/samba/SambaListItem;->getHostName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    :goto_3
    return-object p2

    :cond_3
    iget-object v3, p0, Lcom/konka/mm/samba/MySambaAdapter;->layoutInflater:Landroid/view/LayoutInflater;

    const v4, 0x7f03002b    # com.konka.mm.R.layout.samba_list_adapter

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    const v3, 0x7f0b00ae    # com.konka.mm.R.id.samba_icon

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, v1, Lcom/konka/mm/samba/MySambaAdapter$ListItemViewHolder;->iconImage:Landroid/widget/ImageView;

    const v3, 0x7f0b00af    # com.konka.mm.R.id.samba_ip

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v1, Lcom/konka/mm/samba/MySambaAdapter$ListItemViewHolder;->nameText:Landroid/widget/TextView;

    goto :goto_0

    :cond_4
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/konka/mm/samba/MySambaAdapter$ListItemViewHolder;

    goto :goto_1

    :cond_5
    :try_start_1
    iget v3, v2, Lcom/konka/mm/samba/SambaListItem;->icon:I

    if-eq v3, v7, :cond_6

    iget v3, v2, Lcom/konka/mm/samba/SambaListItem;->icon:I

    if-ne v3, v8, :cond_7

    :cond_6
    iget-object v3, p0, Lcom/konka/mm/samba/MySambaAdapter;->context:Lcom/konka/mm/samba/SambaBrowserActivity;

    iget v3, v3, Lcom/konka/mm/samba/SambaBrowserActivity;->List_Mode:I

    if-ne v3, v6, :cond_8

    const v3, 0x7f020084    # com.konka.mm.R.drawable.music_listadapter

    iput v3, v2, Lcom/konka/mm/samba/SambaListItem;->icon:I

    :cond_7
    :goto_4
    iget-object v3, v1, Lcom/konka/mm/samba/MySambaAdapter$ListItemViewHolder;->iconImage:Landroid/widget/ImageView;

    iget v4, v2, Lcom/konka/mm/samba/SambaListItem;->icon:I

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageResource(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_3

    :cond_8
    const v3, 0x7f02006b    # com.konka.mm.R.drawable.media_default_music_uns

    :try_start_2
    iput v3, v2, Lcom/konka/mm/samba/SambaListItem;->icon:I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_4
.end method
