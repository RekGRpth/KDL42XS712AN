.class Lcom/konka/mm/samba/SambaBrowserActivity$14;
.super Ljava/lang/Object;
.source "SambaBrowserActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/mm/samba/SambaBrowserActivity;->findInputDlg()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/mm/samba/SambaBrowserActivity;


# direct methods
.method constructor <init>(Lcom/konka/mm/samba/SambaBrowserActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/samba/SambaBrowserActivity$14;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1    # Landroid/view/View;

    const/4 v4, 0x1

    iget-object v2, p0, Lcom/konka/mm/samba/SambaBrowserActivity$14;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    iget-object v3, p0, Lcom/konka/mm/samba/SambaBrowserActivity$14;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    # getter for: Lcom/konka/mm/samba/SambaBrowserActivity;->edit_user:Landroid/widget/EditText;
    invoke-static {v3}, Lcom/konka/mm/samba/SambaBrowserActivity;->access$16(Lcom/konka/mm/samba/SambaBrowserActivity;)Landroid/widget/EditText;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-interface {v3}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/konka/mm/samba/SambaBrowserActivity;->access$4(Lcom/konka/mm/samba/SambaBrowserActivity;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/mm/samba/SambaBrowserActivity$14;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    iget-object v3, p0, Lcom/konka/mm/samba/SambaBrowserActivity$14;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    # getter for: Lcom/konka/mm/samba/SambaBrowserActivity;->edit_pass:Landroid/widget/EditText;
    invoke-static {v3}, Lcom/konka/mm/samba/SambaBrowserActivity;->access$17(Lcom/konka/mm/samba/SambaBrowserActivity;)Landroid/widget/EditText;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-interface {v3}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/konka/mm/samba/SambaBrowserActivity;->access$5(Lcom/konka/mm/samba/SambaBrowserActivity;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/mm/samba/SambaBrowserActivity$14;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    iput v4, v2, Lcom/konka/mm/samba/SambaBrowserActivity;->ThreadState:I

    new-instance v0, Lcom/konka/mm/samba/SambaBrowserActivity$SambaRunnable;

    iget-object v2, p0, Lcom/konka/mm/samba/SambaBrowserActivity$14;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    invoke-direct {v0, v2}, Lcom/konka/mm/samba/SambaBrowserActivity$SambaRunnable;-><init>(Lcom/konka/mm/samba/SambaBrowserActivity;)V

    new-instance v1, Ljava/lang/Thread;

    invoke-direct {v1, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    iget-object v2, p0, Lcom/konka/mm/samba/SambaBrowserActivity$14;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    invoke-virtual {v2, v4}, Lcom/konka/mm/samba/SambaBrowserActivity;->showDialog(I)V

    return-void
.end method
