.class public Lcom/konka/mm/samba/SmbShareFolder;
.super Lcom/konka/mm/samba/SambaFile;
.source "SmbShareFolder.java"


# instance fields
.field private device:Lcom/konka/mm/samba/SmbDevice;


# direct methods
.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/konka/mm/samba/SambaFile;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/konka/mm/samba/SmbShareFolder;->device:Lcom/konka/mm/samba/SmbDevice;

    return-void
.end method


# virtual methods
.method public canRead()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public canWrite()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getAuth()Lcom/konka/mm/samba/SmbAuthentication;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/samba/SmbShareFolder;->device:Lcom/konka/mm/samba/SmbDevice;

    invoke-virtual {v0}, Lcom/konka/mm/samba/SmbDevice;->getAuth()Lcom/konka/mm/samba/SmbAuthentication;

    move-result-object v0

    return-object v0
.end method

.method public getDevice()Lcom/konka/mm/samba/SmbDevice;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/samba/SmbShareFolder;->device:Lcom/konka/mm/samba/SmbDevice;

    return-object v0
.end method

.method public getSmbDevice()Lcom/konka/mm/samba/SmbDevice;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/samba/SmbShareFolder;->device:Lcom/konka/mm/samba/SmbDevice;

    return-object v0
.end method

.method public localPath()Ljava/lang/String;
    .locals 4

    invoke-virtual {p0}, Lcom/konka/mm/samba/SmbShareFolder;->getSmbDevice()Lcom/konka/mm/samba/SmbDevice;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lcom/konka/mm/samba/SmbDevice;->localPath()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/konka/mm/samba/SmbShareFolder;->getFileName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public remotePath()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/konka/mm/samba/SmbShareFolder;->getPath()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method setDevice(Lcom/konka/mm/samba/SmbDevice;)V
    .locals 0
    .param p1    # Lcom/konka/mm/samba/SmbDevice;

    iput-object p1, p0, Lcom/konka/mm/samba/SmbShareFolder;->device:Lcom/konka/mm/samba/SmbDevice;

    return-void
.end method
