.class Lcom/konka/mm/samba/SambaBrowserActivity$6;
.super Landroid/os/Handler;
.source "SambaBrowserActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/mm/samba/SambaBrowserActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/mm/samba/SambaBrowserActivity;


# direct methods
.method constructor <init>(Lcom/konka/mm/samba/SambaBrowserActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/samba/SambaBrowserActivity$6;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1    # Landroid/os/Message;

    const/4 v5, 0x4

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity$6;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    # getter for: Lcom/konka/mm/samba/SambaBrowserActivity;->pageInfomation:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/mm/samba/SambaBrowserActivity;->access$8(Lcom/konka/mm/samba/SambaBrowserActivity;)Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    iget v2, p1, Landroid/os/Message;->arg1:I

    add-int/lit8 v2, v2, 0x1

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->arg2:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/mm/samba/SambaBrowserActivity$6;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    invoke-virtual {v2}, Lcom/konka/mm/samba/SambaBrowserActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09009a    # com.konka.mm.R.string.page

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity$6;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    # getter for: Lcom/konka/mm/samba/SambaBrowserActivity;->pageLeft:Landroid/widget/Button;
    invoke-static {v0}, Lcom/konka/mm/samba/SambaBrowserActivity;->access$9(Lcom/konka/mm/samba/SambaBrowserActivity;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity$6;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    # getter for: Lcom/konka/mm/samba/SambaBrowserActivity;->pageRight:Landroid/widget/Button;
    invoke-static {v0}, Lcom/konka/mm/samba/SambaBrowserActivity;->access$10(Lcom/konka/mm/samba/SambaBrowserActivity;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity$6;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    # getter for: Lcom/konka/mm/samba/SambaBrowserActivity;->pageLeft:Landroid/widget/Button;
    invoke-static {v0}, Lcom/konka/mm/samba/SambaBrowserActivity;->access$9(Lcom/konka/mm/samba/SambaBrowserActivity;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity$6;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    # getter for: Lcom/konka/mm/samba/SambaBrowserActivity;->pageRight:Landroid/widget/Button;
    invoke-static {v0}, Lcom/konka/mm/samba/SambaBrowserActivity;->access$10(Lcom/konka/mm/samba/SambaBrowserActivity;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity$6;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    # getter for: Lcom/konka/mm/samba/SambaBrowserActivity;->pageLeft:Landroid/widget/Button;
    invoke-static {v0}, Lcom/konka/mm/samba/SambaBrowserActivity;->access$9(Lcom/konka/mm/samba/SambaBrowserActivity;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity$6;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    # getter for: Lcom/konka/mm/samba/SambaBrowserActivity;->pageRight:Landroid/widget/Button;
    invoke-static {v0}, Lcom/konka/mm/samba/SambaBrowserActivity;->access$10(Lcom/konka/mm/samba/SambaBrowserActivity;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity$6;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    # getter for: Lcom/konka/mm/samba/SambaBrowserActivity;->pageLeft:Landroid/widget/Button;
    invoke-static {v0}, Lcom/konka/mm/samba/SambaBrowserActivity;->access$9(Lcom/konka/mm/samba/SambaBrowserActivity;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity$6;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    # getter for: Lcom/konka/mm/samba/SambaBrowserActivity;->pageRight:Landroid/widget/Button;
    invoke-static {v0}, Lcom/konka/mm/samba/SambaBrowserActivity;->access$10(Lcom/konka/mm/samba/SambaBrowserActivity;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
