.class Lcom/konka/mm/samba/SambaBrowserActivity$5;
.super Ljava/lang/Object;
.source "SambaBrowserActivity.java"

# interfaces
.implements Landroid/widget/RadioGroup$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/mm/samba/SambaBrowserActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/mm/samba/SambaBrowserActivity;


# direct methods
.method constructor <init>(Lcom/konka/mm/samba/SambaBrowserActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/samba/SambaBrowserActivity$5;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/RadioGroup;I)V
    .locals 3
    .param p1    # Landroid/widget/RadioGroup;
    .param p2    # I

    const/16 v2, 0x21

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity$5;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    # getter for: Lcom/konka/mm/samba/SambaBrowserActivity;->sortPopupWindow:Landroid/widget/PopupWindow;
    invoke-static {v0}, Lcom/konka/mm/samba/SambaBrowserActivity;->access$6(Lcom/konka/mm/samba/SambaBrowserActivity;)Landroid/widget/PopupWindow;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity$5;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    # getter for: Lcom/konka/mm/samba/SambaBrowserActivity;->sortPopupWindow:Landroid/widget/PopupWindow;
    invoke-static {v0}, Lcom/konka/mm/samba/SambaBrowserActivity;->access$6(Lcom/konka/mm/samba/SambaBrowserActivity;)Landroid/widget/PopupWindow;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity$5;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    invoke-static {v0, v1}, Lcom/konka/mm/samba/SambaBrowserActivity;->access$7(Lcom/konka/mm/samba/SambaBrowserActivity;Z)V

    :cond_0
    packed-switch p2, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity$5;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    invoke-virtual {v0, v1}, Lcom/konka/mm/samba/SambaBrowserActivity;->setBtnFocuseFlag(Z)V

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity$5;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    const/4 v1, 0x1

    iput v1, v0, Lcom/konka/mm/samba/SambaBrowserActivity;->List_Mode:I

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity$5;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    # getter for: Lcom/konka/mm/samba/SambaBrowserActivity;->viewHolder:Lcom/konka/mm/samba/SambaBrowserViewHolder;
    invoke-static {v0}, Lcom/konka/mm/samba/SambaBrowserActivity;->access$0(Lcom/konka/mm/samba/SambaBrowserActivity;)Lcom/konka/mm/samba/SambaBrowserViewHolder;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/konka/mm/samba/SambaBrowserViewHolder;->setAdapter(I)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity$5;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    invoke-virtual {v0, v1}, Lcom/konka/mm/samba/SambaBrowserActivity;->setBtnFocuseFlag(Z)V

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity$5;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    const/4 v1, 0x2

    iput v1, v0, Lcom/konka/mm/samba/SambaBrowserActivity;->List_Mode:I

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity$5;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    # getter for: Lcom/konka/mm/samba/SambaBrowserActivity;->viewHolder:Lcom/konka/mm/samba/SambaBrowserViewHolder;
    invoke-static {v0}, Lcom/konka/mm/samba/SambaBrowserActivity;->access$0(Lcom/konka/mm/samba/SambaBrowserActivity;)Lcom/konka/mm/samba/SambaBrowserViewHolder;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/konka/mm/samba/SambaBrowserViewHolder;->setAdapter(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7f0b00b4
        :pswitch_0    # com.konka.mm.R.id.list_small_style
        :pswitch_1    # com.konka.mm.R.id.list_big_style
    .end packed-switch
.end method
