.class Lcom/konka/mm/samba/SambaBrowserActivity$sortThread;
.super Ljava/lang/Object;
.source "SambaBrowserActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/mm/samba/SambaBrowserActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "sortThread"
.end annotation


# instance fields
.field private paths:[Ljava/lang/String;

.field final synthetic this$0:Lcom/konka/mm/samba/SambaBrowserActivity;


# direct methods
.method public constructor <init>(Lcom/konka/mm/samba/SambaBrowserActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/samba/SambaBrowserActivity$sortThread;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    iget-object v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity$sortThread;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    # getter for: Lcom/konka/mm/samba/SambaBrowserActivity;->sortType:I
    invoke-static {v1}, Lcom/konka/mm/samba/SambaBrowserActivity;->access$13(Lcom/konka/mm/samba/SambaBrowserActivity;)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :goto_0
    iget-object v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity$sortThread;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    # getter for: Lcom/konka/mm/samba/SambaBrowserActivity;->sortHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/konka/mm/samba/SambaBrowserActivity;->access$14(Lcom/konka/mm/samba/SambaBrowserActivity;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    const/16 v1, 0xa

    iput v1, v0, Landroid/os/Message;->what:I

    iget-object v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity$sortThread;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    # getter for: Lcom/konka/mm/samba/SambaBrowserActivity;->sortHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/konka/mm/samba/SambaBrowserActivity;->access$14(Lcom/konka/mm/samba/SambaBrowserActivity;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void

    :pswitch_0
    iget-object v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity$sortThread;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    # getter for: Lcom/konka/mm/samba/SambaBrowserActivity;->viewHolder:Lcom/konka/mm/samba/SambaBrowserViewHolder;
    invoke-static {v1}, Lcom/konka/mm/samba/SambaBrowserActivity;->access$0(Lcom/konka/mm/samba/SambaBrowserActivity;)Lcom/konka/mm/samba/SambaBrowserViewHolder;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/mm/samba/SambaBrowserActivity$sortThread;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    # getter for: Lcom/konka/mm/samba/SambaBrowserActivity;->viewHolder:Lcom/konka/mm/samba/SambaBrowserViewHolder;
    invoke-static {v2}, Lcom/konka/mm/samba/SambaBrowserActivity;->access$0(Lcom/konka/mm/samba/SambaBrowserActivity;)Lcom/konka/mm/samba/SambaBrowserViewHolder;

    move-result-object v2

    iget-object v2, v2, Lcom/konka/mm/samba/SambaBrowserViewHolder;->list:Ljava/util/List;

    invoke-static {v2}, Lcom/konka/mm/tools/FileTool;->getSortByType(Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, v1, Lcom/konka/mm/samba/SambaBrowserViewHolder;->list:Ljava/util/List;

    goto :goto_0

    :pswitch_1
    iget-object v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity$sortThread;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    # getter for: Lcom/konka/mm/samba/SambaBrowserActivity;->viewHolder:Lcom/konka/mm/samba/SambaBrowserViewHolder;
    invoke-static {v1}, Lcom/konka/mm/samba/SambaBrowserActivity;->access$0(Lcom/konka/mm/samba/SambaBrowserActivity;)Lcom/konka/mm/samba/SambaBrowserViewHolder;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/mm/samba/SambaBrowserActivity$sortThread;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    # getter for: Lcom/konka/mm/samba/SambaBrowserActivity;->viewHolder:Lcom/konka/mm/samba/SambaBrowserViewHolder;
    invoke-static {v2}, Lcom/konka/mm/samba/SambaBrowserActivity;->access$0(Lcom/konka/mm/samba/SambaBrowserActivity;)Lcom/konka/mm/samba/SambaBrowserViewHolder;

    move-result-object v2

    iget-object v2, v2, Lcom/konka/mm/samba/SambaBrowserViewHolder;->list:Ljava/util/List;

    invoke-static {v2}, Lcom/konka/mm/tools/FileTool;->getSortByName(Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, v1, Lcom/konka/mm/samba/SambaBrowserViewHolder;->list:Ljava/util/List;

    goto :goto_0

    :pswitch_2
    iget-object v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity$sortThread;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    # getter for: Lcom/konka/mm/samba/SambaBrowserActivity;->viewHolder:Lcom/konka/mm/samba/SambaBrowserViewHolder;
    invoke-static {v1}, Lcom/konka/mm/samba/SambaBrowserActivity;->access$0(Lcom/konka/mm/samba/SambaBrowserActivity;)Lcom/konka/mm/samba/SambaBrowserViewHolder;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/mm/samba/SambaBrowserActivity$sortThread;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    # getter for: Lcom/konka/mm/samba/SambaBrowserActivity;->viewHolder:Lcom/konka/mm/samba/SambaBrowserViewHolder;
    invoke-static {v2}, Lcom/konka/mm/samba/SambaBrowserActivity;->access$0(Lcom/konka/mm/samba/SambaBrowserActivity;)Lcom/konka/mm/samba/SambaBrowserViewHolder;

    move-result-object v2

    iget-object v2, v2, Lcom/konka/mm/samba/SambaBrowserViewHolder;->list:Ljava/util/List;

    invoke-static {v2}, Lcom/konka/mm/tools/FileTool;->getSortBySize(Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, v1, Lcom/konka/mm/samba/SambaBrowserViewHolder;->list:Ljava/util/List;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
