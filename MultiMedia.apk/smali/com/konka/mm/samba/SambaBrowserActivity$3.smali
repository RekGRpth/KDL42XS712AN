.class Lcom/konka/mm/samba/SambaBrowserActivity$3;
.super Ljava/lang/Object;
.source "SambaBrowserActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/mm/samba/SambaBrowserActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/mm/samba/SambaBrowserActivity;


# direct methods
.method constructor <init>(Lcom/konka/mm/samba/SambaBrowserActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/samba/SambaBrowserActivity$3;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    :cond_0
    :goto_0
    return-void

    :sswitch_0
    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity$3;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    invoke-virtual {v0}, Lcom/konka/mm/samba/SambaBrowserActivity;->sortBtnOnKeyOrOnClickEvent()V

    goto :goto_0

    :sswitch_1
    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity$3;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    invoke-virtual {v0}, Lcom/konka/mm/samba/SambaBrowserActivity;->updataBtnOnKeyOrOnClickEvent()V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity$3;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    iget v0, v0, Lcom/konka/mm/samba/SambaBrowserActivity;->currentPage:I

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity$3;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    invoke-virtual {v0, v2}, Lcom/konka/mm/samba/SambaBrowserActivity;->setBtnFocuseFlag(Z)V

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity$3;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/konka/mm/samba/SambaBrowserActivity;->snapScreen(I)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity$3;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    iget v0, v0, Lcom/konka/mm/samba/SambaBrowserActivity;->currentPage:I

    iget-object v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity$3;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    iget v1, v1, Lcom/konka/mm/samba/SambaBrowserActivity;->PageCount:I

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity$3;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    invoke-virtual {v0, v2}, Lcom/konka/mm/samba/SambaBrowserActivity;->setBtnFocuseFlag(Z)V

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity$3;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/konka/mm/samba/SambaBrowserActivity;->snapScreen(I)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0b0019 -> :sswitch_0    # com.konka.mm.R.id.list_btn
        0x7f0b002f -> :sswitch_2    # com.konka.mm.R.id.btn_list_left
        0x7f0b0034 -> :sswitch_3    # com.konka.mm.R.id.btn_list_right
        0x7f0b00a8 -> :sswitch_1    # com.konka.mm.R.id.update_btn
    .end sparse-switch
.end method
