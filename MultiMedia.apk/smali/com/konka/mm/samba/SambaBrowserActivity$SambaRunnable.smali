.class public Lcom/konka/mm/samba/SambaBrowserActivity$SambaRunnable;
.super Ljava/lang/Object;
.source "SambaBrowserActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/mm/samba/SambaBrowserActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SambaRunnable"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/mm/samba/SambaBrowserActivity;


# direct methods
.method public constructor <init>(Lcom/konka/mm/samba/SambaBrowserActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/samba/SambaBrowserActivity$SambaRunnable;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity$SambaRunnable;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    iget v0, v0, Lcom/konka/mm/samba/SambaBrowserActivity;->ThreadState:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    const-string v0, "Scanning the LAN ip..."

    invoke-static {v0}, Liapp/eric/utils/base/Trace;->Info(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity$SambaRunnable;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    # invokes: Lcom/konka/mm/samba/SambaBrowserActivity;->findHosts()V
    invoke-static {v0}, Lcom/konka/mm/samba/SambaBrowserActivity;->access$15(Lcom/konka/mm/samba/SambaBrowserActivity;)V

    goto :goto_0

    :pswitch_1
    const-string v0, "Scanning the share files..."

    invoke-static {v0}, Liapp/eric/utils/base/Trace;->Info(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/mm/samba/SambaBrowserActivity$SambaRunnable;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    iget-object v1, p0, Lcom/konka/mm/samba/SambaBrowserActivity$SambaRunnable;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    # getter for: Lcom/konka/mm/samba/SambaBrowserActivity;->userName:Ljava/lang/String;
    invoke-static {v1}, Lcom/konka/mm/samba/SambaBrowserActivity;->access$1(Lcom/konka/mm/samba/SambaBrowserActivity;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/mm/samba/SambaBrowserActivity$SambaRunnable;->this$0:Lcom/konka/mm/samba/SambaBrowserActivity;

    # getter for: Lcom/konka/mm/samba/SambaBrowserActivity;->passWord:Ljava/lang/String;
    invoke-static {v2}, Lcom/konka/mm/samba/SambaBrowserActivity;->access$2(Lcom/konka/mm/samba/SambaBrowserActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/konka/mm/samba/SambaBrowserActivity;->sambaLogin(Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
