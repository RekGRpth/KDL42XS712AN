.class public Lcom/konka/mm/samba/SambaException;
.super Ljava/lang/Exception;
.source "SambaException.java"


# static fields
.field public static final ERR_NULLPOINTER:I = 0x2

.field public static final ERR_UNSUPPORTTYPE:I = 0x2

.field public static final SMBERR_UNKNOWN:I = 0x1

.field private static final serialVersionUID:J = 0x1L


# instance fields
.field errcode:I


# direct methods
.method constructor <init>(I)V
    .locals 0
    .param p1    # I

    invoke-direct {p0}, Ljava/lang/Exception;-><init>()V

    iput p1, p0, Lcom/konka/mm/samba/SambaException;->errcode:I

    return-void
.end method

.method constructor <init>(Ljcifs/smb/SmbException;)V
    .locals 1
    .param p1    # Ljcifs/smb/SmbException;

    invoke-direct {p0}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {p1}, Ljcifs/smb/SmbException;->getNtStatus()I

    move-result v0

    iput v0, p0, Lcom/konka/mm/samba/SambaException;->errcode:I

    invoke-virtual {p1}, Ljcifs/smb/SmbException;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/konka/mm/samba/SambaException;->setStackTrace([Ljava/lang/StackTraceElement;)V

    return-void
.end method


# virtual methods
.method public getErr()I
    .locals 1

    iget v0, p0, Lcom/konka/mm/samba/SambaException;->errcode:I

    return v0
.end method
