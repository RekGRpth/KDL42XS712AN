.class Lcom/konka/mm/samba/SambaBrowserViewHolder$hostnameRun;
.super Ljava/lang/Object;
.source "SambaBrowserViewHolder.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/mm/samba/SambaBrowserViewHolder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "hostnameRun"
.end annotation


# instance fields
.field devicelist:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/konka/mm/samba/SmbDevice;",
            ">;"
        }
    .end annotation
.end field

.field hostNumber:I

.field final synthetic this$0:Lcom/konka/mm/samba/SambaBrowserViewHolder;


# direct methods
.method constructor <init>(Lcom/konka/mm/samba/SambaBrowserViewHolder;Ljava/util/ArrayList;I)V
    .locals 0
    .param p3    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/konka/mm/samba/SmbDevice;",
            ">;I)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder$hostnameRun;->this$0:Lcom/konka/mm/samba/SambaBrowserViewHolder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder$hostnameRun;->devicelist:Ljava/util/ArrayList;

    iput p3, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder$hostnameRun;->hostNumber:I

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    const-string v4, "SambaBrowserViewHolder"

    const-string v5, "find the computer name start..."

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x0

    :goto_0
    iget v4, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder$hostnameRun;->hostNumber:I

    if-lt v2, v4, :cond_0

    const-string v4, "SambaBrowserViewHolder"

    const-string v5, "find the computer name is ok"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder$hostnameRun;->this$0:Lcom/konka/mm/samba/SambaBrowserViewHolder;

    const/4 v5, 0x1

    invoke-static {v4, v5}, Lcom/konka/mm/samba/SambaBrowserViewHolder;->access$1(Lcom/konka/mm/samba/SambaBrowserViewHolder;Z)V

    iget-object v4, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder$hostnameRun;->this$0:Lcom/konka/mm/samba/SambaBrowserViewHolder;

    iget-object v4, v4, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    iget v4, v4, Lcom/konka/mm/samba/SambaBrowserActivity;->ThreadState:I

    if-nez v4, :cond_2

    iget-object v4, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder$hostnameRun;->this$0:Lcom/konka/mm/samba/SambaBrowserViewHolder;

    # getter for: Lcom/konka/mm/samba/SambaBrowserViewHolder;->isfristSearch:Z
    invoke-static {v4}, Lcom/konka/mm/samba/SambaBrowserViewHolder;->access$2(Lcom/konka/mm/samba/SambaBrowserViewHolder;)Z

    move-result v4

    if-eqz v4, :cond_2

    const-string v4, "SambaBrowserViewHolder"

    const-string v5, "current level is SCAN_HOST & it is frist Search ,sendMessage"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder$hostnameRun;->this$0:Lcom/konka/mm/samba/SambaBrowserViewHolder;

    iget-object v4, v4, Lcom/konka/mm/samba/SambaBrowserViewHolder;->fileBrowserActivity:Lcom/konka/mm/samba/SambaBrowserActivity;

    iget-object v4, v4, Lcom/konka/mm/samba/SambaBrowserActivity;->scanHandler:Landroid/os/Handler;

    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    iget-object v4, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder$hostnameRun;->this$0:Lcom/konka/mm/samba/SambaBrowserViewHolder;

    const/4 v5, 0x0

    invoke-static {v4, v5}, Lcom/konka/mm/samba/SambaBrowserViewHolder;->access$3(Lcom/konka/mm/samba/SambaBrowserViewHolder;Z)V

    :goto_1
    return-void

    :cond_0
    const-string v1, ""

    const-string v3, ""

    :try_start_0
    iget-object v4, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder$hostnameRun;->devicelist:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/konka/mm/samba/SmbDevice;

    invoke-virtual {v4}, Lcom/konka/mm/samba/SmbDevice;->getAddress()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder$hostnameRun;->this$0:Lcom/konka/mm/samba/SambaBrowserViewHolder;

    # getter for: Lcom/konka/mm/samba/SambaBrowserViewHolder;->hostNameMap:Ljava/util/Map;
    invoke-static {v4}, Lcom/konka/mm/samba/SambaBrowserViewHolder;->access$0(Lcom/konka/mm/samba/SambaBrowserViewHolder;)Ljava/util/Map;

    move-result-object v4

    invoke-interface {v4, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder$hostnameRun;->devicelist:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/konka/mm/samba/SmbDevice;

    invoke-virtual {v4}, Lcom/konka/mm/samba/SmbDevice;->getHostName()Ljava/lang/String;

    move-result-object v1

    iget-object v4, p0, Lcom/konka/mm/samba/SambaBrowserViewHolder$hostnameRun;->this$0:Lcom/konka/mm/samba/SambaBrowserViewHolder;

    # getter for: Lcom/konka/mm/samba/SambaBrowserViewHolder;->hostNameMap:Ljava/util/Map;
    invoke-static {v4}, Lcom/konka/mm/samba/SambaBrowserViewHolder;->access$0(Lcom/konka/mm/samba/SambaBrowserViewHolder;)Ljava/util/Map;

    move-result-object v4

    invoke-interface {v4, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/net/UnknownHostException;->printStackTrace()V

    goto :goto_2

    :cond_2
    const-string v4, "SambaBrowserViewHolder"

    const-string v5, "current level isn\'t SCAN_HOST or isn\'t frist search,need\'t send Message"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method
