.class Lcom/konka/mm/services/CopyFileService$CopyThread;
.super Ljava/lang/Object;
.source "CopyFileService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/mm/services/CopyFileService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "CopyThread"
.end annotation


# instance fields
.field from:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/konka/mm/services/CopyFileService;

.field toPath:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/konka/mm/services/CopyFileService;Ljava/util/ArrayList;Ljava/lang/String;)V
    .locals 0
    .param p3    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    iput-object p1, p0, Lcom/konka/mm/services/CopyFileService$CopyThread;->this$0:Lcom/konka/mm/services/CopyFileService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/konka/mm/services/CopyFileService$CopyThread;->from:Ljava/util/ArrayList;

    iput-object p3, p0, Lcom/konka/mm/services/CopyFileService$CopyThread;->toPath:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    iget-object v0, p0, Lcom/konka/mm/services/CopyFileService$CopyThread;->this$0:Lcom/konka/mm/services/CopyFileService;

    iget-object v1, p0, Lcom/konka/mm/services/CopyFileService$CopyThread;->from:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/konka/mm/services/CopyFileService$CopyThread;->toPath:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/konka/mm/services/CopyFileService;->startCopy(Ljava/util/ArrayList;Ljava/lang/String;)V

    return-void
.end method
