.class public Lcom/konka/mm/services/MusicPlayService;
.super Landroid/app/Service;
.source "MusicPlayService.java"

# interfaces
.implements Ljava/lang/Runnable;


# static fields
.field private static activity:Lcom/konka/mm/music/MusicActivity;

.field public static lyricList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/konka/mm/music/LyricContent;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private countTime:I

.field private currentTime:I

.field private executorService:Ljava/util/concurrent/ExecutorService;

.field private handler:Landroid/os/Handler;

.field private index:I

.field private isPause:Z

.field private isPlay:Z

.field private isRepeat:Z

.field private isRun:Z

.field private lrcHandler:Landroid/os/Handler;

.field public lrcRead:Lcom/konka/mm/music/LrcRead;

.field mRunnable:Ljava/lang/Runnable;

.field private musicPath:Ljava/lang/String;

.field private player:Landroid/media/MediaPlayer;

.field private thread:Ljava/lang/Thread;

.field private timer:Ljava/util/Timer;

.field private timerTast:Ljava/util/TimerTask;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/konka/mm/services/MusicPlayService;->activity:Lcom/konka/mm/music/MusicActivity;

    sput-object v0, Lcom/konka/mm/services/MusicPlayService;->lyricList:Ljava/util/List;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    iput-object v1, p0, Lcom/konka/mm/services/MusicPlayService;->player:Landroid/media/MediaPlayer;

    iput-boolean v0, p0, Lcom/konka/mm/services/MusicPlayService;->isPlay:Z

    iput-boolean v0, p0, Lcom/konka/mm/services/MusicPlayService;->isPause:Z

    iput-boolean v0, p0, Lcom/konka/mm/services/MusicPlayService;->isRepeat:Z

    iput-object v1, p0, Lcom/konka/mm/services/MusicPlayService;->lrcRead:Lcom/konka/mm/music/LrcRead;

    iput v0, p0, Lcom/konka/mm/services/MusicPlayService;->index:I

    iput v0, p0, Lcom/konka/mm/services/MusicPlayService;->currentTime:I

    iput v0, p0, Lcom/konka/mm/services/MusicPlayService;->countTime:I

    iput-boolean v0, p0, Lcom/konka/mm/services/MusicPlayService;->isRun:Z

    iput-object v1, p0, Lcom/konka/mm/services/MusicPlayService;->thread:Ljava/lang/Thread;

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/mm/services/MusicPlayService;->executorService:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Lcom/konka/mm/services/MusicPlayService$1;

    invoke-direct {v0, p0}, Lcom/konka/mm/services/MusicPlayService$1;-><init>(Lcom/konka/mm/services/MusicPlayService;)V

    iput-object v0, p0, Lcom/konka/mm/services/MusicPlayService;->handler:Landroid/os/Handler;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/konka/mm/services/MusicPlayService;->lrcHandler:Landroid/os/Handler;

    new-instance v0, Lcom/konka/mm/services/MusicPlayService$2;

    invoke-direct {v0, p0}, Lcom/konka/mm/services/MusicPlayService$2;-><init>(Lcom/konka/mm/services/MusicPlayService;)V

    iput-object v0, p0, Lcom/konka/mm/services/MusicPlayService;->mRunnable:Ljava/lang/Runnable;

    new-instance v0, Landroid/media/MediaPlayer;

    invoke-direct {v0}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v0, p0, Lcom/konka/mm/services/MusicPlayService;->player:Landroid/media/MediaPlayer;

    return-void
.end method

.method public static RefreshLRC(I)V
    .locals 6
    .param p0    # I

    const/4 v3, 0x1

    const/4 v1, 0x0

    const/4 v0, 0x1

    :goto_0
    sget-object v2, Lcom/konka/mm/services/MusicPlayService;->lyricList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lt v0, v2, :cond_0

    return-void

    :cond_0
    sget-object v2, Lcom/konka/mm/services/MusicPlayService;->lyricList:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/konka/mm/music/LyricContent;

    invoke-virtual {v2}, Lcom/konka/mm/music/LyricContent;->getLyricTime()I

    move-result v2

    if-ge p0, v2, :cond_2

    if-eq v0, v3, :cond_1

    sget-object v2, Lcom/konka/mm/services/MusicPlayService;->lyricList:Ljava/util/List;

    add-int/lit8 v4, v0, -0x1

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/konka/mm/music/LyricContent;

    invoke-virtual {v2}, Lcom/konka/mm/music/LyricContent;->getLyricTime()I

    move-result v2

    if-lt p0, v2, :cond_2

    :cond_1
    sget-object v4, Lcom/konka/mm/services/MusicPlayService;->activity:Lcom/konka/mm/music/MusicActivity;

    sget-object v2, Lcom/konka/mm/services/MusicPlayService;->lyricList:Ljava/util/List;

    add-int/lit8 v5, v0, -0x1

    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/konka/mm/music/LyricContent;

    invoke-virtual {v2}, Lcom/konka/mm/music/LyricContent;->getLyric()Ljava/lang/String;

    move-result-object v5

    add-int/lit8 v2, v0, -0x1

    if-eq v1, v2, :cond_3

    move v2, v3

    :goto_1
    invoke-virtual {v4, v5, v2}, Lcom/konka/mm/music/MusicActivity;->setMiniLRCText(Ljava/lang/String;Z)V

    add-int/lit8 v1, v0, -0x1

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    const/4 v2, 0x0

    goto :goto_1
.end method

.method static synthetic access$0()Lcom/konka/mm/music/MusicActivity;
    .locals 1

    sget-object v0, Lcom/konka/mm/services/MusicPlayService;->activity:Lcom/konka/mm/music/MusicActivity;

    return-object v0
.end method

.method static synthetic access$1(Lcom/konka/mm/services/MusicPlayService;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/mm/services/MusicPlayService;->isRun:Z

    return v0
.end method

.method static synthetic access$2(Lcom/konka/mm/services/MusicPlayService;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/services/MusicPlayService;->lrcHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$3(Lcom/konka/mm/services/MusicPlayService;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/services/MusicPlayService;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$4(Lcom/konka/mm/services/MusicPlayService;)Landroid/media/MediaPlayer;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/services/MusicPlayService;->player:Landroid/media/MediaPlayer;

    return-object v0
.end method

.method public static addActivity(Lcom/konka/mm/music/MusicActivity;)V
    .locals 0
    .param p0    # Lcom/konka/mm/music/MusicActivity;

    sput-object p0, Lcom/konka/mm/services/MusicPlayService;->activity:Lcom/konka/mm/music/MusicActivity;

    return-void
.end method

.method private initPlay()V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lcom/konka/mm/services/MusicPlayService;->index:I

    iput-boolean v0, p0, Lcom/konka/mm/services/MusicPlayService;->isPlay:Z

    iput-boolean v0, p0, Lcom/konka/mm/services/MusicPlayService;->isPause:Z

    invoke-virtual {p0}, Lcom/konka/mm/services/MusicPlayService;->stopThread()V

    return-void
.end method

.method private playMusic(Ljava/lang/String;)V
    .locals 9
    .param p1    # Ljava/lang/String;

    const/4 v8, 0x1

    const/4 v7, 0x0

    invoke-virtual {p0}, Lcom/konka/mm/services/MusicPlayService;->stopThread()V

    iget-boolean v4, p0, Lcom/konka/mm/services/MusicPlayService;->isPlay:Z

    if-nez v4, :cond_2

    iget-boolean v4, p0, Lcom/konka/mm/services/MusicPlayService;->isPause:Z

    if-nez v4, :cond_1

    move-object v0, p1

    sput-object v0, Lcom/konka/mm/filemanager/FileListActivity;->PLAYING_MUSIC_PATH:Ljava/lang/String;

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    sput-object v4, Lcom/konka/mm/services/MusicPlayService;->lyricList:Ljava/util/List;

    iput v7, p0, Lcom/konka/mm/services/MusicPlayService;->index:I

    if-eqz v0, :cond_0

    :try_start_0
    const-string v4, ""

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    new-instance v2, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    const/4 v5, 0x0

    const-string v6, "."

    invoke-virtual {v0, v6}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v0, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, ".lrc"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_0

    new-instance v4, Lcom/konka/mm/music/LrcRead;

    invoke-direct {v4}, Lcom/konka/mm/music/LrcRead;-><init>()V

    iput-object v4, p0, Lcom/konka/mm/services/MusicPlayService;->lrcRead:Lcom/konka/mm/music/LrcRead;

    iget-object v4, p0, Lcom/konka/mm/services/MusicPlayService;->lrcRead:Lcom/konka/mm/music/LrcRead;

    invoke-virtual {v4, v2}, Lcom/konka/mm/music/LrcRead;->Read(Ljava/io/File;)V

    iget-object v4, p0, Lcom/konka/mm/services/MusicPlayService;->lrcRead:Lcom/konka/mm/music/LrcRead;

    invoke-virtual {v4}, Lcom/konka/mm/music/LrcRead;->GetLyricContent()Ljava/util/List;

    move-result-object v4

    sput-object v4, Lcom/konka/mm/services/MusicPlayService;->lyricList:Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    sget-object v4, Lcom/konka/mm/services/MusicPlayService;->activity:Lcom/konka/mm/music/MusicActivity;

    iget-object v4, v4, Lcom/konka/mm/music/MusicActivity;->lyricView:Lcom/konka/mm/music/LyricView;

    sget-object v5, Lcom/konka/mm/services/MusicPlayService;->lyricList:Ljava/util/List;

    invoke-virtual {v4, v5}, Lcom/konka/mm/music/LyricView;->setSentenceEntities(Ljava/util/List;)V

    iget-object v4, p0, Lcom/konka/mm/services/MusicPlayService;->lrcHandler:Landroid/os/Handler;

    iget-object v5, p0, Lcom/konka/mm/services/MusicPlayService;->mRunnable:Ljava/lang/Runnable;

    invoke-virtual {v4, v5}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v4, p0, Lcom/konka/mm/services/MusicPlayService;->lrcHandler:Landroid/os/Handler;

    iget-object v5, p0, Lcom/konka/mm/services/MusicPlayService;->mRunnable:Ljava/lang/Runnable;

    invoke-virtual {v4, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    iget-object v4, p0, Lcom/konka/mm/services/MusicPlayService;->player:Landroid/media/MediaPlayer;

    invoke-static {v4, p1}, Lcom/konka/mm/tools/MusicTool;->playMusic(Landroid/media/MediaPlayer;Ljava/lang/String;)Z

    invoke-virtual {p0}, Lcom/konka/mm/services/MusicPlayService;->startThread()V

    iput-boolean v8, p0, Lcom/konka/mm/services/MusicPlayService;->isPlay:Z

    iput-boolean v7, p0, Lcom/konka/mm/services/MusicPlayService;->isPause:Z

    :goto_1
    iget-object v4, p0, Lcom/konka/mm/services/MusicPlayService;->player:Landroid/media/MediaPlayer;

    invoke-virtual {v4}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v4

    iput v4, p0, Lcom/konka/mm/services/MusicPlayService;->countTime:I

    new-instance v3, Landroid/os/Message;

    invoke-direct {v3}, Landroid/os/Message;-><init>()V

    const/16 v4, 0x21

    iput v4, v3, Landroid/os/Message;->what:I

    iget-object v4, p0, Lcom/konka/mm/services/MusicPlayService;->player:Landroid/media/MediaPlayer;

    iput-object v4, v3, Landroid/os/Message;->obj:Ljava/lang/Object;

    sget-object v4, Lcom/konka/mm/services/MusicPlayService;->activity:Lcom/konka/mm/music/MusicActivity;

    iget-object v4, v4, Lcom/konka/mm/music/MusicActivity;->handler:Landroid/os/Handler;

    invoke-virtual {v4, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    :cond_1
    iget-object v4, p0, Lcom/konka/mm/services/MusicPlayService;->lrcHandler:Landroid/os/Handler;

    iget-object v5, p0, Lcom/konka/mm/services/MusicPlayService;->mRunnable:Ljava/lang/Runnable;

    invoke-virtual {v4, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    iput-boolean v8, p0, Lcom/konka/mm/services/MusicPlayService;->isPlay:Z

    iput-boolean v7, p0, Lcom/konka/mm/services/MusicPlayService;->isPause:Z

    invoke-virtual {p0}, Lcom/konka/mm/services/MusicPlayService;->startThread()V

    iget-object v4, p0, Lcom/konka/mm/services/MusicPlayService;->player:Landroid/media/MediaPlayer;

    invoke-virtual {v4}, Landroid/media/MediaPlayer;->start()V

    goto :goto_1

    :cond_2
    iget-object v4, p0, Lcom/konka/mm/services/MusicPlayService;->lrcHandler:Landroid/os/Handler;

    iget-object v5, p0, Lcom/konka/mm/services/MusicPlayService;->mRunnable:Ljava/lang/Runnable;

    invoke-virtual {v4, v5}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iput-boolean v7, p0, Lcom/konka/mm/services/MusicPlayService;->isPlay:Z

    iput-boolean v8, p0, Lcom/konka/mm/services/MusicPlayService;->isPause:Z

    invoke-virtual {p0}, Lcom/konka/mm/services/MusicPlayService;->stopThread()V

    iget-object v4, p0, Lcom/konka/mm/services/MusicPlayService;->player:Landroid/media/MediaPlayer;

    invoke-virtual {v4}, Landroid/media/MediaPlayer;->pause()V

    goto :goto_1
.end method

.method private sendLrcBroadcast(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "konka.mm.lrc.action"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "message"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/konka/mm/services/MusicPlayService;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method


# virtual methods
.method public Index()I
    .locals 5

    :try_start_0
    iget-object v2, p0, Lcom/konka/mm/services/MusicPlayService;->player:Landroid/media/MediaPlayer;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/konka/mm/services/MusicPlayService;->player:Landroid/media/MediaPlayer;

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/konka/mm/services/MusicPlayService;->player:Landroid/media/MediaPlayer;

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v2

    iput v2, p0, Lcom/konka/mm/services/MusicPlayService;->currentTime:I

    :cond_0
    iget v2, p0, Lcom/konka/mm/services/MusicPlayService;->currentTime:I

    iget v3, p0, Lcom/konka/mm/services/MusicPlayService;->countTime:I

    if-ge v2, v3, :cond_1

    const/4 v1, 0x0

    :goto_0
    sget-object v2, Lcom/konka/mm/services/MusicPlayService;->lyricList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-lt v1, v2, :cond_2

    :cond_1
    :goto_1
    iget v2, p0, Lcom/konka/mm/services/MusicPlayService;->index:I

    return v2

    :cond_2
    :try_start_1
    sget-object v2, Lcom/konka/mm/services/MusicPlayService;->lyricList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ge v1, v2, :cond_4

    iget v3, p0, Lcom/konka/mm/services/MusicPlayService;->currentTime:I

    sget-object v2, Lcom/konka/mm/services/MusicPlayService;->lyricList:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/konka/mm/music/LyricContent;

    invoke-virtual {v2}, Lcom/konka/mm/music/LyricContent;->getLyricTime()I

    move-result v2

    if-ge v3, v2, :cond_3

    if-nez v1, :cond_3

    iput v1, p0, Lcom/konka/mm/services/MusicPlayService;->index:I

    :cond_3
    iget v3, p0, Lcom/konka/mm/services/MusicPlayService;->currentTime:I

    sget-object v2, Lcom/konka/mm/services/MusicPlayService;->lyricList:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/konka/mm/music/LyricContent;

    invoke-virtual {v2}, Lcom/konka/mm/music/LyricContent;->getLyricTime()I

    move-result v2

    if-le v3, v2, :cond_4

    iget v3, p0, Lcom/konka/mm/services/MusicPlayService;->currentTime:I

    sget-object v2, Lcom/konka/mm/services/MusicPlayService;->lyricList:Ljava/util/List;

    add-int/lit8 v4, v1, 0x1

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/konka/mm/music/LyricContent;

    invoke-virtual {v2}, Lcom/konka/mm/music/LyricContent;->getLyricTime()I

    move-result v2

    if-ge v3, v2, :cond_4

    iput v1, p0, Lcom/konka/mm/services/MusicPlayService;->index:I

    :cond_4
    sget-object v2, Lcom/konka/mm/services/MusicPlayService;->lyricList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ne v1, v2, :cond_5

    iget v3, p0, Lcom/konka/mm/services/MusicPlayService;->currentTime:I

    sget-object v2, Lcom/konka/mm/services/MusicPlayService;->lyricList:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/konka/mm/music/LyricContent;

    invoke-virtual {v2}, Lcom/konka/mm/music/LyricContent;->getLyricTime()I

    move-result v2

    if-le v3, v2, :cond_5

    iput v1, p0, Lcom/konka/mm/services/MusicPlayService;->index:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public getMediaPlayer()Landroid/media/MediaPlayer;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/services/MusicPlayService;->player:Landroid/media/MediaPlayer;

    return-object v0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1    # Landroid/content/Intent;

    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 0

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    return-void
.end method

.method public onDestroy()V
    .locals 2

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    iget-object v0, p0, Lcom/konka/mm/services/MusicPlayService;->player:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/konka/mm/services/MusicPlayService;->player:Landroid/media/MediaPlayer;

    invoke-virtual {p0}, Lcom/konka/mm/services/MusicPlayService;->stopThread()V

    iget-object v0, p0, Lcom/konka/mm/services/MusicPlayService;->lrcHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/mm/services/MusicPlayService;->lrcHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/konka/mm/services/MusicPlayService;->mRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    :cond_0
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 8
    .param p1    # Landroid/content/Intent;
    .param p2    # I
    .param p3    # I

    const/4 v4, 0x1

    const/4 v5, 0x0

    sget-object v6, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v3, "intent: "

    invoke-direct {v7, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-nez p1, :cond_1

    move v3, v4

    :goto_0
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    if-eqz p1, :cond_0

    const-string v3, "MSG"

    invoke-virtual {p1, v3, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_1
    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    move-result v3

    return v3

    :cond_1
    move v3, v5

    goto :goto_0

    :pswitch_0
    iget-object v3, p0, Lcom/konka/mm/services/MusicPlayService;->player:Landroid/media/MediaPlayer;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/konka/mm/services/MusicPlayService;->player:Landroid/media/MediaPlayer;

    invoke-virtual {v3}, Landroid/media/MediaPlayer;->isLooping()Z

    move-result v3

    if-nez v3, :cond_2

    iput-boolean v4, p0, Lcom/konka/mm/services/MusicPlayService;->isRepeat:Z

    iget-object v3, p0, Lcom/konka/mm/services/MusicPlayService;->player:Landroid/media/MediaPlayer;

    iget-boolean v4, p0, Lcom/konka/mm/services/MusicPlayService;->isRepeat:Z

    invoke-virtual {v3, v4}, Landroid/media/MediaPlayer;->setLooping(Z)V

    goto :goto_1

    :cond_2
    iput-boolean v5, p0, Lcom/konka/mm/services/MusicPlayService;->isRepeat:Z

    iget-object v3, p0, Lcom/konka/mm/services/MusicPlayService;->player:Landroid/media/MediaPlayer;

    iget-boolean v4, p0, Lcom/konka/mm/services/MusicPlayService;->isRepeat:Z

    invoke-virtual {v3, v4}, Landroid/media/MediaPlayer;->setLooping(Z)V

    goto :goto_1

    :pswitch_1
    const-string v3, "MUSIC_PATH"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/konka/mm/services/MusicPlayService;->musicPath:Ljava/lang/String;

    invoke-direct {p0}, Lcom/konka/mm/services/MusicPlayService;->initPlay()V

    iget-object v3, p0, Lcom/konka/mm/services/MusicPlayService;->musicPath:Ljava/lang/String;

    invoke-direct {p0, v3}, Lcom/konka/mm/services/MusicPlayService;->playMusic(Ljava/lang/String;)V

    goto :goto_1

    :pswitch_2
    const-string v3, "MUSIC_PATH"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/konka/mm/services/MusicPlayService;->musicPath:Ljava/lang/String;

    invoke-direct {p0}, Lcom/konka/mm/services/MusicPlayService;->initPlay()V

    iget-object v3, p0, Lcom/konka/mm/services/MusicPlayService;->musicPath:Ljava/lang/String;

    invoke-direct {p0, v3}, Lcom/konka/mm/services/MusicPlayService;->playMusic(Ljava/lang/String;)V

    goto :goto_1

    :pswitch_3
    const-string v3, "MUSIC_PATH"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/konka/mm/services/MusicPlayService;->musicPath:Ljava/lang/String;

    iget-object v3, p0, Lcom/konka/mm/services/MusicPlayService;->musicPath:Ljava/lang/String;

    invoke-direct {p0, v3}, Lcom/konka/mm/services/MusicPlayService;->playMusic(Ljava/lang/String;)V

    goto :goto_1

    :pswitch_4
    const-string v3, "MUSIC_PATH"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/konka/mm/services/MusicPlayService;->musicPath:Ljava/lang/String;

    iget-object v3, p0, Lcom/konka/mm/services/MusicPlayService;->musicPath:Ljava/lang/String;

    invoke-direct {p0, v3}, Lcom/konka/mm/services/MusicPlayService;->playMusic(Ljava/lang/String;)V

    goto :goto_1

    :pswitch_5
    const-string v3, "MUSIC_PATH"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/konka/mm/services/MusicPlayService;->musicPath:Ljava/lang/String;

    invoke-direct {p0}, Lcom/konka/mm/services/MusicPlayService;->initPlay()V

    iget-object v3, p0, Lcom/konka/mm/services/MusicPlayService;->musicPath:Ljava/lang/String;

    invoke-direct {p0, v3}, Lcom/konka/mm/services/MusicPlayService;->playMusic(Ljava/lang/String;)V

    goto :goto_1

    :pswitch_6
    const-string v3, "progress"

    invoke-virtual {p1, v3, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iget-object v3, p0, Lcom/konka/mm/services/MusicPlayService;->player:Landroid/media/MediaPlayer;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/konka/mm/services/MusicPlayService;->player:Landroid/media/MediaPlayer;

    invoke-virtual {v3, v2}, Landroid/media/MediaPlayer;->seekTo(I)V

    goto/16 :goto_1

    :pswitch_7
    invoke-direct {p0}, Lcom/konka/mm/services/MusicPlayService;->initPlay()V

    :try_start_0
    iget-object v3, p0, Lcom/konka/mm/services/MusicPlayService;->player:Landroid/media/MediaPlayer;

    invoke-virtual {v3}, Landroid/media/MediaPlayer;->prepare()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_2
    iget-object v3, p0, Lcom/konka/mm/services/MusicPlayService;->player:Landroid/media/MediaPlayer;

    invoke-virtual {v3, v5}, Landroid/media/MediaPlayer;->seekTo(I)V

    goto/16 :goto_1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_2

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public run()V
    .locals 1

    :cond_0
    iget-boolean v0, p0, Lcom/konka/mm/services/MusicPlayService;->isRun:Z

    if-nez v0, :cond_0

    return-void
.end method

.method public startThread()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/konka/mm/services/MusicPlayService;->isRun:Z

    invoke-virtual {p0}, Lcom/konka/mm/services/MusicPlayService;->stopTimer()V

    invoke-virtual {p0}, Lcom/konka/mm/services/MusicPlayService;->startTimer()V

    return-void
.end method

.method public startTimer()V
    .locals 6

    iget-object v0, p0, Lcom/konka/mm/services/MusicPlayService;->timer:Ljava/util/Timer;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/konka/mm/services/MusicPlayService;->timer:Ljava/util/Timer;

    :cond_0
    iget-object v0, p0, Lcom/konka/mm/services/MusicPlayService;->timerTast:Ljava/util/TimerTask;

    if-nez v0, :cond_1

    new-instance v0, Lcom/konka/mm/services/MusicPlayService$3;

    invoke-direct {v0, p0}, Lcom/konka/mm/services/MusicPlayService$3;-><init>(Lcom/konka/mm/services/MusicPlayService;)V

    iput-object v0, p0, Lcom/konka/mm/services/MusicPlayService;->timerTast:Ljava/util/TimerTask;

    :cond_1
    iget-object v0, p0, Lcom/konka/mm/services/MusicPlayService;->timer:Ljava/util/Timer;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/konka/mm/services/MusicPlayService;->timerTast:Ljava/util/TimerTask;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/konka/mm/services/MusicPlayService;->timer:Ljava/util/Timer;

    iget-object v1, p0, Lcom/konka/mm/services/MusicPlayService;->timerTast:Ljava/util/TimerTask;

    const-wide/16 v2, 0x1

    const-wide/16 v4, 0x3e8

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    :cond_2
    return-void
.end method

.method public stopThread()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/konka/mm/services/MusicPlayService;->isRun:Z

    return-void
.end method

.method public stopTimer()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/konka/mm/services/MusicPlayService;->timer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/mm/services/MusicPlayService;->timer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    iput-object v1, p0, Lcom/konka/mm/services/MusicPlayService;->timer:Ljava/util/Timer;

    :cond_0
    iget-object v0, p0, Lcom/konka/mm/services/MusicPlayService;->timerTast:Ljava/util/TimerTask;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/konka/mm/services/MusicPlayService;->timerTast:Ljava/util/TimerTask;

    invoke-virtual {v0}, Ljava/util/TimerTask;->cancel()Z

    iput-object v1, p0, Lcom/konka/mm/services/MusicPlayService;->timerTast:Ljava/util/TimerTask;

    :cond_1
    return-void
.end method
