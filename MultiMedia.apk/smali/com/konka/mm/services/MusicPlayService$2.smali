.class Lcom/konka/mm/services/MusicPlayService$2;
.super Ljava/lang/Object;
.source "MusicPlayService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/mm/services/MusicPlayService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/mm/services/MusicPlayService;


# direct methods
.method constructor <init>(Lcom/konka/mm/services/MusicPlayService;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/services/MusicPlayService$2;->this$0:Lcom/konka/mm/services/MusicPlayService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    # getter for: Lcom/konka/mm/services/MusicPlayService;->activity:Lcom/konka/mm/music/MusicActivity;
    invoke-static {}, Lcom/konka/mm/services/MusicPlayService;->access$0()Lcom/konka/mm/music/MusicActivity;

    move-result-object v0

    iget-object v0, v0, Lcom/konka/mm/music/MusicActivity;->lyricView:Lcom/konka/mm/music/LyricView;

    iget-object v1, p0, Lcom/konka/mm/services/MusicPlayService$2;->this$0:Lcom/konka/mm/services/MusicPlayService;

    invoke-virtual {v1}, Lcom/konka/mm/services/MusicPlayService;->Index()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/konka/mm/music/LyricView;->SetIndex(I)V

    # getter for: Lcom/konka/mm/services/MusicPlayService;->activity:Lcom/konka/mm/music/MusicActivity;
    invoke-static {}, Lcom/konka/mm/services/MusicPlayService;->access$0()Lcom/konka/mm/music/MusicActivity;

    move-result-object v0

    iget-object v0, v0, Lcom/konka/mm/music/MusicActivity;->lyricView:Lcom/konka/mm/music/LyricView;

    invoke-virtual {v0}, Lcom/konka/mm/music/LyricView;->invalidate()V

    iget-object v0, p0, Lcom/konka/mm/services/MusicPlayService$2;->this$0:Lcom/konka/mm/services/MusicPlayService;

    # getter for: Lcom/konka/mm/services/MusicPlayService;->isRun:Z
    invoke-static {v0}, Lcom/konka/mm/services/MusicPlayService;->access$1(Lcom/konka/mm/services/MusicPlayService;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/mm/services/MusicPlayService$2;->this$0:Lcom/konka/mm/services/MusicPlayService;

    # getter for: Lcom/konka/mm/services/MusicPlayService;->lrcHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/konka/mm/services/MusicPlayService;->access$2(Lcom/konka/mm/services/MusicPlayService;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/mm/services/MusicPlayService$2;->this$0:Lcom/konka/mm/services/MusicPlayService;

    iget-object v1, v1, Lcom/konka/mm/services/MusicPlayService;->mRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x190

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_0
    return-void
.end method
