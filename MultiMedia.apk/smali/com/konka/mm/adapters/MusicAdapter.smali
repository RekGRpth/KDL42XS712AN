.class public Lcom/konka/mm/adapters/MusicAdapter;
.super Landroid/widget/BaseAdapter;
.source "MusicAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/mm/adapters/MusicAdapter$AlbumThumbHandler;,
        Lcom/konka/mm/adapters/MusicAdapter$AlbumThumbRunnable;,
        Lcom/konka/mm/adapters/MusicAdapter$ViewHolder;
    }
.end annotation


# static fields
.field public static PAGE_NUM:I = 0x0

.field private static final TAG:Ljava/lang/String; = "MusicAdapter"


# instance fields
.field private executorService:Ljava/util/concurrent/ExecutorService;

.field private flag:Z

.field private inflate:Landroid/view/LayoutInflater;

.field private mActivit:Lcom/konka/mm/music/MusicActivity;

.field private pageIndex:I

.field private perPageCount:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0xc

    sput v0, Lcom/konka/mm/adapters/MusicAdapter;->PAGE_NUM:I

    return-void
.end method

.method public constructor <init>(Lcom/konka/mm/music/MusicActivity;II)V
    .locals 2
    .param p1    # Lcom/konka/mm/music/MusicActivity;
    .param p2    # I
    .param p3    # I

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    iput-object v1, p0, Lcom/konka/mm/adapters/MusicAdapter;->inflate:Landroid/view/LayoutInflater;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/konka/mm/adapters/MusicAdapter;->flag:Z

    const/4 v0, 0x0

    iput v0, p0, Lcom/konka/mm/adapters/MusicAdapter;->perPageCount:I

    iput-object v1, p0, Lcom/konka/mm/adapters/MusicAdapter;->executorService:Ljava/util/concurrent/ExecutorService;

    iput-object p1, p0, Lcom/konka/mm/adapters/MusicAdapter;->mActivit:Lcom/konka/mm/music/MusicActivity;

    const/4 v0, 0x5

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/mm/adapters/MusicAdapter;->executorService:Ljava/util/concurrent/ExecutorService;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/mm/adapters/MusicAdapter;->inflate:Landroid/view/LayoutInflater;

    iput p3, p0, Lcom/konka/mm/adapters/MusicAdapter;->perPageCount:I

    iput p2, p0, Lcom/konka/mm/adapters/MusicAdapter;->pageIndex:I

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    iget v0, p0, Lcom/konka/mm/adapters/MusicAdapter;->perPageCount:I

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/konka/mm/adapters/MusicAdapter;->mActivit:Lcom/konka/mm/music/MusicActivity;

    iget-object v0, v0, Lcom/konka/mm/music/MusicActivity;->musicPaths:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/konka/mm/adapters/MusicAdapter;->mActivit:Lcom/konka/mm/music/MusicActivity;

    iget-object v0, v0, Lcom/konka/mm/music/MusicActivity;->musicPaths:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/konka/mm/adapters/MusicAdapter;->mActivit:Lcom/konka/mm/music/MusicActivity;

    iget-object v0, v0, Lcom/konka/mm/music/MusicActivity;->musicPaths:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    const/4 p1, 0x0

    :cond_0
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    const/4 v5, 0x0

    const/4 v1, 0x0

    if-nez p2, :cond_2

    iget-object v3, p0, Lcom/konka/mm/adapters/MusicAdapter;->inflate:Landroid/view/LayoutInflater;

    const v4, 0x7f030027    # com.konka.mm.R.layout.play_item_template

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    new-instance v1, Lcom/konka/mm/adapters/MusicAdapter$ViewHolder;

    invoke-direct {v1, v5}, Lcom/konka/mm/adapters/MusicAdapter$ViewHolder;-><init>(Lcom/konka/mm/adapters/MusicAdapter$ViewHolder;)V

    const v3, 0x7f0b00a0    # com.konka.mm.R.id.img_play_item_icon

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, v1, Lcom/konka/mm/adapters/MusicAdapter$ViewHolder;->icon:Landroid/widget/ImageView;

    const v3, 0x7f0b00a1    # com.konka.mm.R.id.tv_play_item_name

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v1, Lcom/konka/mm/adapters/MusicAdapter$ViewHolder;->name:Landroid/widget/TextView;

    const v3, 0x7f0b00a2    # com.konka.mm.R.id.tv_play_item_artist

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v1, Lcom/konka/mm/adapters/MusicAdapter$ViewHolder;->artist:Landroid/widget/TextView;

    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :goto_0
    iget-object v3, p0, Lcom/konka/mm/adapters/MusicAdapter;->mActivit:Lcom/konka/mm/music/MusicActivity;

    iget-object v3, v3, Lcom/konka/mm/music/MusicActivity;->musicPaths:Ljava/util/ArrayList;

    iget v4, p0, Lcom/konka/mm/adapters/MusicAdapter;->pageIndex:I

    sget v5, Lcom/konka/mm/adapters/MusicAdapter;->PAGE_NUM:I

    mul-int/2addr v4, v5

    add-int/2addr v4, p1

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    new-instance v0, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v4, 0x0

    const-string v5, "."

    invoke-virtual {v2, v5}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".png"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, v1, Lcom/konka/mm/adapters/MusicAdapter$ViewHolder;->handler:Lcom/konka/mm/adapters/MusicAdapter$AlbumThumbHandler;

    if-nez v3, :cond_0

    new-instance v3, Lcom/konka/mm/adapters/MusicAdapter$AlbumThumbHandler;

    iget-object v4, v1, Lcom/konka/mm/adapters/MusicAdapter$ViewHolder;->icon:Landroid/widget/ImageView;

    invoke-direct {v3, p0, v4}, Lcom/konka/mm/adapters/MusicAdapter$AlbumThumbHandler;-><init>(Lcom/konka/mm/adapters/MusicAdapter;Landroid/widget/ImageView;)V

    iput-object v3, v1, Lcom/konka/mm/adapters/MusicAdapter$ViewHolder;->handler:Lcom/konka/mm/adapters/MusicAdapter$AlbumThumbHandler;

    :cond_0
    new-instance v3, Ljava/lang/Thread;

    new-instance v4, Lcom/konka/mm/adapters/MusicAdapter$AlbumThumbRunnable;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    iget-object v6, v1, Lcom/konka/mm/adapters/MusicAdapter$ViewHolder;->handler:Lcom/konka/mm/adapters/MusicAdapter$AlbumThumbHandler;

    invoke-direct {v4, p0, v5, v6}, Lcom/konka/mm/adapters/MusicAdapter$AlbumThumbRunnable;-><init>(Lcom/konka/mm/adapters/MusicAdapter;Ljava/lang/String;Lcom/konka/mm/adapters/MusicAdapter$AlbumThumbHandler;)V

    invoke-direct {v3, v4}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v3}, Ljava/lang/Thread;->start()V

    :cond_1
    iget-object v3, v1, Lcom/konka/mm/adapters/MusicAdapter$ViewHolder;->icon:Landroid/widget/ImageView;

    const v4, 0x7f020075    # com.konka.mm.R.drawable.music_icon_big

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v3, v1, Lcom/konka/mm/adapters/MusicAdapter$ViewHolder;->name:Landroid/widget/TextView;

    const-string v4, "/"

    invoke-virtual {v2, v4}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v2, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, v1, Lcom/konka/mm/adapters/MusicAdapter$ViewHolder;->artist:Landroid/widget/TextView;

    const-string v4, "unknown"

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-object p2

    :cond_2
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/konka/mm/adapters/MusicAdapter$ViewHolder;

    goto :goto_0
.end method
