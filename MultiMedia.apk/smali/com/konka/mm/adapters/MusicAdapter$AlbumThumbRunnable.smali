.class Lcom/konka/mm/adapters/MusicAdapter$AlbumThumbRunnable;
.super Ljava/lang/Object;
.source "MusicAdapter.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/mm/adapters/MusicAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "AlbumThumbRunnable"
.end annotation


# instance fields
.field albumthumbPath:Ljava/lang/String;

.field mHandler:Lcom/konka/mm/adapters/MusicAdapter$AlbumThumbHandler;

.field final synthetic this$0:Lcom/konka/mm/adapters/MusicAdapter;


# direct methods
.method public constructor <init>(Lcom/konka/mm/adapters/MusicAdapter;Ljava/lang/String;Lcom/konka/mm/adapters/MusicAdapter$AlbumThumbHandler;)V
    .locals 0
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/konka/mm/adapters/MusicAdapter$AlbumThumbHandler;

    iput-object p1, p0, Lcom/konka/mm/adapters/MusicAdapter$AlbumThumbRunnable;->this$0:Lcom/konka/mm/adapters/MusicAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/konka/mm/adapters/MusicAdapter$AlbumThumbRunnable;->albumthumbPath:Ljava/lang/String;

    iput-object p3, p0, Lcom/konka/mm/adapters/MusicAdapter$AlbumThumbRunnable;->mHandler:Lcom/konka/mm/adapters/MusicAdapter$AlbumThumbHandler;

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    iget-object v2, p0, Lcom/konka/mm/adapters/MusicAdapter$AlbumThumbRunnable;->albumthumbPath:Ljava/lang/String;

    const/high16 v3, 0x10000

    invoke-static {v2, v3}, Lcom/konka/mm/tools/PicTool;->decodeFileDescriptor(Ljava/lang/String;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iget-object v2, p0, Lcom/konka/mm/adapters/MusicAdapter$AlbumThumbRunnable;->mHandler:Lcom/konka/mm/adapters/MusicAdapter$AlbumThumbHandler;

    invoke-virtual {v2}, Lcom/konka/mm/adapters/MusicAdapter$AlbumThumbHandler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    iput-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/konka/mm/adapters/MusicAdapter$AlbumThumbRunnable;->mHandler:Lcom/konka/mm/adapters/MusicAdapter$AlbumThumbHandler;

    invoke-virtual {v2, v1}, Lcom/konka/mm/adapters/MusicAdapter$AlbumThumbHandler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method
