.class Lcom/konka/mm/adapters/MusicAdapter$AlbumThumbHandler;
.super Landroid/os/Handler;
.source "MusicAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/mm/adapters/MusicAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "AlbumThumbHandler"
.end annotation


# instance fields
.field public mImagerView:Landroid/widget/ImageView;

.field final synthetic this$0:Lcom/konka/mm/adapters/MusicAdapter;


# direct methods
.method public constructor <init>(Lcom/konka/mm/adapters/MusicAdapter;Landroid/widget/ImageView;)V
    .locals 0
    .param p2    # Landroid/widget/ImageView;

    iput-object p1, p0, Lcom/konka/mm/adapters/MusicAdapter$AlbumThumbHandler;->this$0:Lcom/konka/mm/adapters/MusicAdapter;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    iput-object p2, p0, Lcom/konka/mm/adapters/MusicAdapter$AlbumThumbHandler;->mImagerView:Landroid/widget/ImageView;

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1    # Landroid/os/Message;

    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/konka/mm/adapters/MusicAdapter$AlbumThumbHandler;->mImagerView:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    return-void
.end method
