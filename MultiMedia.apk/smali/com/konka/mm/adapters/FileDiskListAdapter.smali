.class public Lcom/konka/mm/adapters/FileDiskListAdapter;
.super Landroid/widget/BaseAdapter;
.source "FileDiskListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/mm/adapters/FileDiskListAdapter$ViewHolder;
    }
.end annotation


# instance fields
.field private context:Landroid/content/Context;

.field private inflate:Landroid/view/LayoutInflater;

.field private infos:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/konka/mm/adapters/FileDiskListAdapter;->inflate:Landroid/view/LayoutInflater;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    iput-object p2, p0, Lcom/konka/mm/adapters/FileDiskListAdapter;->infos:Ljava/util/List;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/mm/adapters/FileDiskListAdapter;->inflate:Landroid/view/LayoutInflater;

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/adapters/FileDiskListAdapter;->infos:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/konka/mm/adapters/FileDiskListAdapter;->infos:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getInfos()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/konka/mm/adapters/FileDiskListAdapter;->infos:Ljava/util/List;

    return-object v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/konka/mm/adapters/FileDiskListAdapter;->infos:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/konka/mm/adapters/FileDiskListAdapter;->infos:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/konka/mm/adapters/FileDiskListAdapter;->infos:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 p1, 0x0

    :cond_0
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    const/4 v4, 0x0

    const/4 v0, 0x0

    if-nez p2, :cond_1

    iget-object v2, p0, Lcom/konka/mm/adapters/FileDiskListAdapter;->inflate:Landroid/view/LayoutInflater;

    const v3, 0x7f030011    # com.konka.mm.R.layout.item_template

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    new-instance v0, Lcom/konka/mm/adapters/FileDiskListAdapter$ViewHolder;

    invoke-direct {v0, v4}, Lcom/konka/mm/adapters/FileDiskListAdapter$ViewHolder;-><init>(Lcom/konka/mm/adapters/FileDiskListAdapter$ViewHolder;)V

    const v2, 0x7f0b0042    # com.konka.mm.R.id.img_item_icon

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, v0, Lcom/konka/mm/adapters/FileDiskListAdapter$ViewHolder;->icon:Landroid/widget/ImageView;

    const v2, 0x7f0b0044    # com.konka.mm.R.id.tv_item_name

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v0, Lcom/konka/mm/adapters/FileDiskListAdapter$ViewHolder;->name:Landroid/widget/TextView;

    iget-object v2, v0, Lcom/konka/mm/adapters/FileDiskListAdapter$ViewHolder;->name:Landroid/widget/TextView;

    const/4 v3, 0x1

    const/high16 v4, 0x41c80000    # 25.0f

    invoke-virtual {v2, v3, v4}, Landroid/widget/TextView;->setTextSize(IF)V

    iget-object v2, v0, Lcom/konka/mm/adapters/FileDiskListAdapter$ViewHolder;->name:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->invalidate()V

    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :goto_0
    iget-object v2, p0, Lcom/konka/mm/adapters/FileDiskListAdapter;->infos:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v2, v0, Lcom/konka/mm/adapters/FileDiskListAdapter$ViewHolder;->name:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, v0, Lcom/konka/mm/adapters/FileDiskListAdapter$ViewHolder;->icon:Landroid/widget/ImageView;

    const v3, 0x7f02006f    # com.konka.mm.R.drawable.media_disk_uns

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_0
    return-object p2

    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/konka/mm/adapters/FileDiskListAdapter$ViewHolder;

    goto :goto_0
.end method

.method public setInfos(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/konka/mm/adapters/FileDiskListAdapter;->infos:Ljava/util/List;

    return-void
.end method
