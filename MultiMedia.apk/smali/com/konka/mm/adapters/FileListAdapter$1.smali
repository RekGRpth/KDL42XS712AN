.class Lcom/konka/mm/adapters/FileListAdapter$1;
.super Landroid/os/Handler;
.source "FileListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/mm/adapters/FileListAdapter;->setItemIcon(Ljava/io/File;Landroid/widget/ImageView;Lcom/konka/mm/photo/ImageThumbHandler;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/mm/adapters/FileListAdapter;

.field private final synthetic val$icon:Landroid/widget/ImageView;


# direct methods
.method constructor <init>(Lcom/konka/mm/adapters/FileListAdapter;Landroid/widget/ImageView;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/adapters/FileListAdapter$1;->this$0:Lcom/konka/mm/adapters/FileListAdapter;

    iput-object p2, p0, Lcom/konka/mm/adapters/FileListAdapter$1;->val$icon:Landroid/widget/ImageView;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1    # Landroid/os/Message;

    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    iget-object v1, p0, Lcom/konka/mm/adapters/FileListAdapter$1;->val$icon:Landroid/widget/ImageView;

    const v2, 0x7f020069    # com.konka.mm.R.drawable.media_default_file_uns

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/konka/mm/adapters/FileListAdapter$1;->val$icon:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method
