.class public Lcom/konka/mm/adapters/FileListAdapter;
.super Landroid/widget/BaseAdapter;
.source "FileListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/mm/adapters/FileListAdapter$Mrunnable;,
        Lcom/konka/mm/adapters/FileListAdapter$ViewHolder;
    }
.end annotation


# instance fields
.field private activity:Lcom/konka/mm/filemanager/FileListActivity;

.field private countPerPage:I

.field private executorService:Ljava/util/concurrent/ExecutorService;

.field private handler:Landroid/os/Handler;

.field private indexPage:I

.field private inflate:Landroid/view/LayoutInflater;

.field public mPosition:I

.field private pageNum:I

.field private r:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Lcom/konka/mm/filemanager/FileListActivity;III)V
    .locals 1
    .param p1    # Lcom/konka/mm/filemanager/FileListActivity;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/konka/mm/adapters/FileListAdapter;->inflate:Landroid/view/LayoutInflater;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/konka/mm/adapters/FileListAdapter;->handler:Landroid/os/Handler;

    const/16 v0, 0x14

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/mm/adapters/FileListAdapter;->executorService:Ljava/util/concurrent/ExecutorService;

    iput-object p1, p0, Lcom/konka/mm/adapters/FileListAdapter;->activity:Lcom/konka/mm/filemanager/FileListActivity;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/mm/adapters/FileListAdapter;->inflate:Landroid/view/LayoutInflater;

    iput p2, p0, Lcom/konka/mm/adapters/FileListAdapter;->indexPage:I

    iput p3, p0, Lcom/konka/mm/adapters/FileListAdapter;->pageNum:I

    iput p4, p0, Lcom/konka/mm/adapters/FileListAdapter;->countPerPage:I

    invoke-virtual {p1}, Lcom/konka/mm/filemanager/FileListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/mm/adapters/FileListAdapter;->r:Landroid/content/res/Resources;

    return-void
.end method

.method static synthetic access$0(Lcom/konka/mm/adapters/FileListAdapter;)Lcom/konka/mm/filemanager/FileListActivity;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/adapters/FileListAdapter;->activity:Lcom/konka/mm/filemanager/FileListActivity;

    return-object v0
.end method

.method static synthetic access$1(Lcom/konka/mm/adapters/FileListAdapter;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/adapters/FileListAdapter;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method private loadImage(Landroid/widget/ImageView;Ljava/lang/String;)V
    .locals 2
    .param p1    # Landroid/widget/ImageView;
    .param p2    # Ljava/lang/String;

    iget-object v0, p0, Lcom/konka/mm/adapters/FileListAdapter;->executorService:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/konka/mm/adapters/FileListAdapter$2;

    invoke-direct {v1, p0, p2, p1}, Lcom/konka/mm/adapters/FileListAdapter$2;-><init>(Lcom/konka/mm/adapters/FileListAdapter;Ljava/lang/String;Landroid/widget/ImageView;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    return-void
.end method

.method private setItemIcon(Ljava/io/File;Landroid/widget/ImageView;Lcom/konka/mm/photo/ImageThumbHandler;)V
    .locals 7
    .param p1    # Ljava/io/File;
    .param p2    # Landroid/widget/ImageView;
    .param p3    # Lcom/konka/mm/photo/ImageThumbHandler;

    const v6, 0x7f020069    # com.konka.mm.R.drawable.media_default_file_uns

    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/konka/mm/adapters/FileListAdapter;->getPicName([Ljava/io/File;)Ljava/util/List;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    if-nez v4, :cond_2

    :cond_0
    invoke-virtual {p2, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-virtual {p2, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    new-instance v1, Lcom/konka/mm/adapters/FileListAdapter$1;

    invoke-direct {v1, p0, p2}, Lcom/konka/mm/adapters/FileListAdapter$1;-><init>(Lcom/konka/mm/adapters/FileListAdapter;Landroid/widget/ImageView;)V

    iget-object v4, p0, Lcom/konka/mm/adapters/FileListAdapter;->executorService:Ljava/util/concurrent/ExecutorService;

    new-instance v5, Ljava/lang/Thread;

    new-instance v6, Lcom/konka/mm/adapters/FileListAdapter$Mrunnable;

    invoke-direct {v6, p0, v3, p1, v1}, Lcom/konka/mm/adapters/FileListAdapter$Mrunnable;-><init>(Lcom/konka/mm/adapters/FileListAdapter;Ljava/util/List;Ljava/io/File;Landroid/os/Handler;)V

    invoke-direct {v5, v6}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-interface {v4, v5}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    goto :goto_0

    :cond_3
    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcom/konka/mm/adapters/FileListAdapter;->activity:Lcom/konka/mm/filemanager/FileListActivity;

    invoke-virtual {v4}, Lcom/konka/mm/filemanager/FileListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const/high16 v5, 0x7f070000    # com.konka.mm.R.array.fileEndingImage

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/konka/mm/tools/FileTool;->checkEndsWithInStringArray(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    const v4, 0x7f02006a    # com.konka.mm.R.drawable.media_default_images_uns

    invoke-virtual {p2, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    sget v4, Lcom/konka/mm/filemanager/FileDiskActivity;->List_Mode:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_1

    if-eqz p3, :cond_4

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, p3}, Lcom/konka/mm/photo/PhotoThumbLoader;->remove(Ljava/lang/String;Lcom/konka/mm/photo/ImageThumbHandler;)Z

    :cond_4
    new-instance p3, Lcom/konka/mm/photo/ThumbFileDownloadHandler;

    invoke-direct {p3, p2}, Lcom/konka/mm/photo/ThumbFileDownloadHandler;-><init>(Landroid/widget/ImageView;)V

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, p3}, Lcom/konka/mm/photo/PhotoThumbLoader;->start(Ljava/lang/String;Lcom/konka/mm/photo/ImageThumbHandler;)Z

    goto :goto_0

    :cond_5
    iget-object v4, p0, Lcom/konka/mm/adapters/FileListAdapter;->activity:Lcom/konka/mm/filemanager/FileListActivity;

    invoke-virtual {v4}, Lcom/konka/mm/filemanager/FileListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f070006    # com.konka.mm.R.array.fileEndingWebText

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/konka/mm/tools/FileTool;->checkEndsWithInStringArray(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, p0, Lcom/konka/mm/adapters/FileListAdapter;->activity:Lcom/konka/mm/filemanager/FileListActivity;

    invoke-virtual {v4}, Lcom/konka/mm/filemanager/FileListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f070005    # com.konka.mm.R.array.fileEndingPackage

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/konka/mm/tools/FileTool;->checkEndsWithInStringArray(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, p0, Lcom/konka/mm/adapters/FileListAdapter;->activity:Lcom/konka/mm/filemanager/FileListActivity;

    invoke-virtual {v4}, Lcom/konka/mm/filemanager/FileListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f070001    # com.konka.mm.R.array.fileEndingAudio

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/konka/mm/tools/FileTool;->checkEndsWithInStringArray(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_7

    sget v4, Lcom/konka/mm/filemanager/FileDiskActivity;->List_Mode:I

    const/4 v5, 0x1

    if-ne v4, v5, :cond_6

    const v4, 0x7f020084    # com.konka.mm.R.drawable.music_listadapter

    invoke-virtual {p2, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0

    :cond_6
    const v4, 0x7f02006b    # com.konka.mm.R.drawable.media_default_music_uns

    invoke-virtual {p2, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0

    :cond_7
    iget-object v4, p0, Lcom/konka/mm/adapters/FileListAdapter;->activity:Lcom/konka/mm/filemanager/FileListActivity;

    invoke-virtual {v4}, Lcom/konka/mm/filemanager/FileListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f070002    # com.konka.mm.R.array.fileEndingVedio

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/konka/mm/tools/FileTool;->checkEndsWithInStringArray(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_8

    const v4, 0x7f02006c    # com.konka.mm.R.drawable.media_default_video_uns

    invoke-virtual {p2, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0

    :cond_8
    iget-object v4, p0, Lcom/konka/mm/adapters/FileListAdapter;->activity:Lcom/konka/mm/filemanager/FileListActivity;

    invoke-virtual {v4}, Lcom/konka/mm/filemanager/FileListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f070003    # com.konka.mm.R.array.fileEndingApk

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/konka/mm/tools/FileTool;->checkEndsWithInStringArray(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_9

    const/high16 v4, 0x7f020000    # com.konka.mm.R.drawable.apk_install_icon

    invoke-virtual {p2, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0

    :cond_9
    invoke-virtual {p2, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0
.end method


# virtual methods
.method public dip2px(Landroid/content/Context;F)I
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # F

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v0, v1, Landroid/util/DisplayMetrics;->density:F

    mul-float v1, p2, v0

    const/high16 v2, 0x3f000000    # 0.5f

    add-float/2addr v1, v2

    float-to-int v1, v1

    return v1
.end method

.method public fileToBitmap(Ljava/lang/String;DD)Landroid/graphics/Bitmap;
    .locals 17
    .param p1    # Ljava/lang/String;
    .param p2    # D
    .param p4    # D

    new-instance v13, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v13}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    const/4 v3, 0x1

    iput-boolean v3, v13, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v3, v13, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    move-object/from16 v0, p1

    invoke-static {v0, v13}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v2

    const/4 v3, 0x0

    iput-boolean v3, v13, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    iget v3, v13, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    int-to-float v3, v3

    const/high16 v4, 0x43480000    # 200.0f

    div-float/2addr v3, v4

    float-to-int v9, v3

    iput v9, v13, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    move-object/from16 v0, p1

    invoke-static {v0, v13}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    int-to-float v0, v3

    move/from16 v16, v0

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    int-to-float v11, v3

    new-instance v7, Landroid/graphics/Matrix;

    invoke-direct {v7}, Landroid/graphics/Matrix;-><init>()V

    move-wide/from16 v0, p4

    double-to-float v3, v0

    div-float v15, v3, v16

    move-wide/from16 v0, p2

    double-to-float v3, v0

    div-float v14, v3, v11

    invoke-virtual {v7, v15, v14}, Landroid/graphics/Matrix;->postScale(FF)Z

    const/4 v3, 0x0

    const/4 v4, 0x0

    move/from16 v0, v16

    float-to-int v5, v0

    float-to-int v6, v11

    const/4 v8, 0x1

    invoke-static/range {v2 .. v8}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v12

    :goto_0
    return-object v12

    :cond_0
    move-wide/from16 v0, p4

    double-to-int v3, v0

    move-wide/from16 v0, p2

    double-to-int v4, v0

    mul-int/2addr v3, v4

    :try_start_0
    move-object/from16 v0, p1

    invoke-static {v0, v3}, Lcom/konka/mm/tools/PicTool;->decodeFileDescriptor(Ljava/lang/String;I)Landroid/graphics/Bitmap;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    int-to-float v0, v3

    move/from16 v16, v0

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    int-to-float v11, v3

    new-instance v7, Landroid/graphics/Matrix;

    invoke-direct {v7}, Landroid/graphics/Matrix;-><init>()V

    move-wide/from16 v0, p4

    double-to-float v3, v0

    div-float v15, v3, v16

    move-wide/from16 v0, p2

    double-to-float v3, v0

    div-float v14, v3, v11

    invoke-virtual {v7, v15, v14}, Landroid/graphics/Matrix;->postScale(FF)Z

    const/4 v3, 0x0

    const/4 v4, 0x0

    move/from16 v0, v16

    float-to-int v5, v0

    float-to-int v6, v11

    const/4 v8, 0x1

    invoke-static/range {v2 .. v8}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    :cond_1
    :goto_1
    move-object v12, v2

    goto :goto_0

    :catch_0
    move-exception v10

    invoke-virtual {v10}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public getBitmapResult(Ljava/util/List;)Landroid/graphics/Bitmap;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;)",
            "Landroid/graphics/Bitmap;"
        }
    .end annotation

    const/high16 v12, 0x41e00000    # 28.0f

    const/high16 v10, 0x42100000    # 36.0f

    const/4 v8, 0x0

    const/4 v11, 0x0

    const/4 v7, 0x0

    iget-object v5, p0, Lcom/konka/mm/adapters/FileListAdapter;->r:Landroid/content/res/Resources;

    const v6, 0x7f020069    # com.konka.mm.R.drawable.media_default_file_uns

    invoke-static {v5, v6}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v5

    if-eqz v5, :cond_0

    if-nez p1, :cond_1

    :cond_0
    move-object v1, v7

    :goto_0
    return-object v1

    :cond_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_2

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v4, v3, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {v2, v0, v8, v8, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    invoke-interface {p1, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/Bitmap;

    iget-object v6, p0, Lcom/konka/mm/adapters/FileListAdapter;->activity:Lcom/konka/mm/filemanager/FileListActivity;

    invoke-virtual {p0, v6, v12}, Lcom/konka/mm/adapters/FileListAdapter;->dip2px(Landroid/content/Context;F)I

    move-result v6

    int-to-float v6, v6

    iget-object v8, p0, Lcom/konka/mm/adapters/FileListAdapter;->activity:Lcom/konka/mm/filemanager/FileListActivity;

    invoke-virtual {p0, v8, v10}, Lcom/konka/mm/adapters/FileListAdapter;->dip2px(Landroid/content/Context;F)I

    move-result v8

    int-to-float v8, v8

    invoke-virtual {v2, v5, v6, v8, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_0

    :cond_2
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v5

    const/4 v6, 0x2

    if-ne v5, v6, :cond_3

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v4, v3, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {v2, v0, v8, v8, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    invoke-interface {p1, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/Bitmap;

    iget-object v6, p0, Lcom/konka/mm/adapters/FileListAdapter;->activity:Lcom/konka/mm/filemanager/FileListActivity;

    invoke-virtual {p0, v6, v12}, Lcom/konka/mm/adapters/FileListAdapter;->dip2px(Landroid/content/Context;F)I

    move-result v6

    int-to-float v6, v6

    iget-object v8, p0, Lcom/konka/mm/adapters/FileListAdapter;->activity:Lcom/konka/mm/filemanager/FileListActivity;

    invoke-virtual {p0, v8, v10}, Lcom/konka/mm/adapters/FileListAdapter;->dip2px(Landroid/content/Context;F)I

    move-result v8

    int-to-float v8, v8

    invoke-virtual {v2, v5, v6, v8, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    const/4 v5, 0x1

    invoke-interface {p1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/Bitmap;

    invoke-interface {p1, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    iget-object v8, p0, Lcom/konka/mm/adapters/FileListAdapter;->activity:Lcom/konka/mm/filemanager/FileListActivity;

    const/high16 v9, 0x42000000    # 32.0f

    invoke-virtual {p0, v8, v9}, Lcom/konka/mm/adapters/FileListAdapter;->dip2px(Landroid/content/Context;F)I

    move-result v8

    add-int/2addr v6, v8

    int-to-float v6, v6

    iget-object v8, p0, Lcom/konka/mm/adapters/FileListAdapter;->activity:Lcom/konka/mm/filemanager/FileListActivity;

    invoke-virtual {p0, v8, v10}, Lcom/konka/mm/adapters/FileListAdapter;->dip2px(Landroid/content/Context;F)I

    move-result v8

    int-to-float v8, v8

    invoke-virtual {v2, v5, v6, v8, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_0

    :cond_3
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v5

    const/4 v6, 0x3

    if-ne v5, v6, :cond_4

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v4, v3, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {v2, v0, v8, v8, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    invoke-interface {p1, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/Bitmap;

    iget-object v6, p0, Lcom/konka/mm/adapters/FileListAdapter;->activity:Lcom/konka/mm/filemanager/FileListActivity;

    invoke-virtual {p0, v6, v12}, Lcom/konka/mm/adapters/FileListAdapter;->dip2px(Landroid/content/Context;F)I

    move-result v6

    int-to-float v6, v6

    iget-object v8, p0, Lcom/konka/mm/adapters/FileListAdapter;->activity:Lcom/konka/mm/filemanager/FileListActivity;

    invoke-virtual {p0, v8, v10}, Lcom/konka/mm/adapters/FileListAdapter;->dip2px(Landroid/content/Context;F)I

    move-result v8

    int-to-float v8, v8

    invoke-virtual {v2, v5, v6, v8, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    const/4 v5, 0x1

    invoke-interface {p1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/Bitmap;

    invoke-interface {p1, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    iget-object v8, p0, Lcom/konka/mm/adapters/FileListAdapter;->activity:Lcom/konka/mm/filemanager/FileListActivity;

    const/high16 v9, 0x42000000    # 32.0f

    invoke-virtual {p0, v8, v9}, Lcom/konka/mm/adapters/FileListAdapter;->dip2px(Landroid/content/Context;F)I

    move-result v8

    add-int/2addr v6, v8

    int-to-float v6, v6

    iget-object v8, p0, Lcom/konka/mm/adapters/FileListAdapter;->activity:Lcom/konka/mm/filemanager/FileListActivity;

    invoke-virtual {p0, v8, v10}, Lcom/konka/mm/adapters/FileListAdapter;->dip2px(Landroid/content/Context;F)I

    move-result v8

    int-to-float v8, v8

    invoke-virtual {v2, v5, v6, v8, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    const/4 v5, 0x2

    invoke-interface {p1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/Bitmap;

    iget-object v6, p0, Lcom/konka/mm/adapters/FileListAdapter;->activity:Lcom/konka/mm/filemanager/FileListActivity;

    invoke-virtual {p0, v6, v12}, Lcom/konka/mm/adapters/FileListAdapter;->dip2px(Landroid/content/Context;F)I

    move-result v6

    int-to-float v8, v6

    const/4 v6, 0x1

    invoke-interface {p1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    iget-object v9, p0, Lcom/konka/mm/adapters/FileListAdapter;->activity:Lcom/konka/mm/filemanager/FileListActivity;

    const/high16 v10, 0x42300000    # 44.0f

    invoke-virtual {p0, v9, v10}, Lcom/konka/mm/adapters/FileListAdapter;->dip2px(Landroid/content/Context;F)I

    move-result v9

    add-int/2addr v6, v9

    int-to-float v6, v6

    invoke-virtual {v2, v5, v8, v6, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    :cond_4
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v5

    const/4 v6, 0x4

    if-lt v5, v6, :cond_5

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v4, v3, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {v2, v0, v8, v8, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    invoke-interface {p1, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/Bitmap;

    iget-object v6, p0, Lcom/konka/mm/adapters/FileListAdapter;->activity:Lcom/konka/mm/filemanager/FileListActivity;

    invoke-virtual {p0, v6, v12}, Lcom/konka/mm/adapters/FileListAdapter;->dip2px(Landroid/content/Context;F)I

    move-result v6

    int-to-float v6, v6

    iget-object v8, p0, Lcom/konka/mm/adapters/FileListAdapter;->activity:Lcom/konka/mm/filemanager/FileListActivity;

    invoke-virtual {p0, v8, v10}, Lcom/konka/mm/adapters/FileListAdapter;->dip2px(Landroid/content/Context;F)I

    move-result v8

    int-to-float v8, v8

    invoke-virtual {v2, v5, v6, v8, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    const/4 v5, 0x1

    invoke-interface {p1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/Bitmap;

    invoke-interface {p1, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    iget-object v8, p0, Lcom/konka/mm/adapters/FileListAdapter;->activity:Lcom/konka/mm/filemanager/FileListActivity;

    const/high16 v9, 0x42000000    # 32.0f

    invoke-virtual {p0, v8, v9}, Lcom/konka/mm/adapters/FileListAdapter;->dip2px(Landroid/content/Context;F)I

    move-result v8

    add-int/2addr v6, v8

    int-to-float v6, v6

    iget-object v8, p0, Lcom/konka/mm/adapters/FileListAdapter;->activity:Lcom/konka/mm/filemanager/FileListActivity;

    invoke-virtual {p0, v8, v10}, Lcom/konka/mm/adapters/FileListAdapter;->dip2px(Landroid/content/Context;F)I

    move-result v8

    int-to-float v8, v8

    invoke-virtual {v2, v5, v6, v8, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    const/4 v5, 0x2

    invoke-interface {p1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/Bitmap;

    iget-object v6, p0, Lcom/konka/mm/adapters/FileListAdapter;->activity:Lcom/konka/mm/filemanager/FileListActivity;

    invoke-virtual {p0, v6, v12}, Lcom/konka/mm/adapters/FileListAdapter;->dip2px(Landroid/content/Context;F)I

    move-result v6

    int-to-float v8, v6

    const/4 v6, 0x1

    invoke-interface {p1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    iget-object v9, p0, Lcom/konka/mm/adapters/FileListAdapter;->activity:Lcom/konka/mm/filemanager/FileListActivity;

    const/high16 v10, 0x42300000    # 44.0f

    invoke-virtual {p0, v9, v10}, Lcom/konka/mm/adapters/FileListAdapter;->dip2px(Landroid/content/Context;F)I

    move-result v9

    add-int/2addr v6, v9

    int-to-float v6, v6

    invoke-virtual {v2, v5, v8, v6, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    const/4 v5, 0x3

    invoke-interface {p1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/Bitmap;

    invoke-interface {p1, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    iget-object v8, p0, Lcom/konka/mm/adapters/FileListAdapter;->activity:Lcom/konka/mm/filemanager/FileListActivity;

    const/high16 v9, 0x42000000    # 32.0f

    invoke-virtual {p0, v8, v9}, Lcom/konka/mm/adapters/FileListAdapter;->dip2px(Landroid/content/Context;F)I

    move-result v8

    add-int/2addr v6, v8

    int-to-float v8, v6

    invoke-interface {p1, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    iget-object v9, p0, Lcom/konka/mm/adapters/FileListAdapter;->activity:Lcom/konka/mm/filemanager/FileListActivity;

    const/high16 v10, 0x42300000    # 44.0f

    invoke-virtual {p0, v9, v10}, Lcom/konka/mm/adapters/FileListAdapter;->dip2px(Landroid/content/Context;F)I

    move-result v9

    add-int/2addr v6, v9

    int-to-float v6, v6

    invoke-virtual {v2, v5, v8, v6, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    :cond_5
    move-object v1, v7

    goto/16 :goto_0
.end method

.method public getCount()I
    .locals 1

    iget v0, p0, Lcom/konka/mm/adapters/FileListAdapter;->pageNum:I

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1    # I

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1    # I

    iput p1, p0, Lcom/konka/mm/adapters/FileListAdapter;->mPosition:I

    int-to-long v0, p1

    return-wide v0
.end method

.method public getPicName([Ljava/io/File;)Ljava/util/List;
    .locals 5
    .param p1    # [Ljava/io/File;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/io/File;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    if-eqz p1, :cond_0

    array-length v3, p1

    if-nez v3, :cond_2

    :cond_0
    const/4 v2, 0x0

    :cond_1
    return-object v2

    :cond_2
    const/4 v1, 0x0

    :goto_0
    array-length v3, p1

    if-ge v1, v3, :cond_1

    aget-object v3, p1, v1

    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/konka/mm/adapters/FileListAdapter;->activity:Lcom/konka/mm/filemanager/FileListActivity;

    invoke-virtual {v3}, Lcom/konka/mm/filemanager/FileListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const/high16 v4, 0x7f070000    # com.konka.mm.R.array.fileEndingImage

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/konka/mm/tools/FileTool;->checkEndsWithInStringArray(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_3
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    const/4 v4, 0x4

    if-eq v3, v4, :cond_1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    const/4 v5, 0x0

    const/4 v1, 0x0

    iput p1, p0, Lcom/konka/mm/adapters/FileListAdapter;->mPosition:I

    if-nez p2, :cond_2

    new-instance v1, Lcom/konka/mm/adapters/FileListAdapter$ViewHolder;

    invoke-direct {v1, v5}, Lcom/konka/mm/adapters/FileListAdapter$ViewHolder;-><init>(Lcom/konka/mm/adapters/FileListAdapter$ViewHolder;)V

    sget v3, Lcom/konka/mm/filemanager/FileDiskActivity;->List_Mode:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_1

    iget-object v3, p0, Lcom/konka/mm/adapters/FileListAdapter;->inflate:Landroid/view/LayoutInflater;

    const v4, 0x7f030007    # com.konka.mm.R.layout.file_listadapter

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    const v3, 0x7f0b0026    # com.konka.mm.R.id.picitem

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, v1, Lcom/konka/mm/adapters/FileListAdapter$ViewHolder;->icon:Landroid/widget/ImageView;

    const v3, 0x7f0b0028    # com.konka.mm.R.id.textitem

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v1, Lcom/konka/mm/adapters/FileListAdapter$ViewHolder;->name:Landroid/widget/TextView;

    :goto_0
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :goto_1
    :try_start_0
    iget-object v3, p0, Lcom/konka/mm/adapters/FileListAdapter;->activity:Lcom/konka/mm/filemanager/FileListActivity;

    iget-object v3, v3, Lcom/konka/mm/filemanager/FileListActivity;->infos:Ljava/util/List;

    iget v4, p0, Lcom/konka/mm/adapters/FileListAdapter;->indexPage:I

    iget v5, p0, Lcom/konka/mm/adapters/FileListAdapter;->countPerPage:I

    mul-int/2addr v4, v5

    add-int/2addr v4, p1

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/io/File;

    if-eqz v2, :cond_0

    iget-object v3, v1, Lcom/konka/mm/adapters/FileListAdapter$ViewHolder;->name:Landroid/widget/TextView;

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, v1, Lcom/konka/mm/adapters/FileListAdapter$ViewHolder;->icon:Landroid/widget/ImageView;

    iget-object v4, v1, Lcom/konka/mm/adapters/FileListAdapter$ViewHolder;->handler:Lcom/konka/mm/photo/ImageThumbHandler;

    invoke-direct {p0, v2, v3, v4}, Lcom/konka/mm/adapters/FileListAdapter;->setItemIcon(Ljava/io/File;Landroid/widget/ImageView;Lcom/konka/mm/photo/ImageThumbHandler;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_2
    return-object p2

    :cond_1
    iget-object v3, p0, Lcom/konka/mm/adapters/FileListAdapter;->inflate:Landroid/view/LayoutInflater;

    const v4, 0x7f030011    # com.konka.mm.R.layout.item_template

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    const v3, 0x7f0b0042    # com.konka.mm.R.id.img_item_icon

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, v1, Lcom/konka/mm/adapters/FileListAdapter$ViewHolder;->icon:Landroid/widget/ImageView;

    const v3, 0x7f0b0044    # com.konka.mm.R.id.tv_item_name

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v1, Lcom/konka/mm/adapters/FileListAdapter$ViewHolder;->name:Landroid/widget/TextView;

    goto :goto_0

    :cond_2
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/konka/mm/adapters/FileListAdapter$ViewHolder;

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2
.end method

.method public getmPosition()I
    .locals 1

    iget v0, p0, Lcom/konka/mm/adapters/FileListAdapter;->mPosition:I

    return v0
.end method

.method public resourcesToBitmap(III)Landroid/graphics/Bitmap;
    .locals 10
    .param p1    # I
    .param p2    # I
    .param p3    # I

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/konka/mm/adapters/FileListAdapter;->r:Landroid/content/res/Resources;

    invoke-virtual {v2, p3}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v2

    invoke-static {v2}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    int-to-float v2, p1

    int-to-float v6, v3

    div-float v9, v2, v6

    int-to-float v2, p2

    int-to-float v6, v4

    div-float v8, v2, v6

    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    invoke-virtual {v5, v9, v8}, Landroid/graphics/Matrix;->postScale(FF)Z

    const/4 v6, 0x1

    move v2, v1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v7

    return-object v7
.end method
