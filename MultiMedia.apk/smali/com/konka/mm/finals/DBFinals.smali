.class public Lcom/konka/mm/finals/DBFinals;
.super Ljava/lang/Object;
.source "DBFinals.java"

# interfaces
.implements Landroid/provider/BaseColumns;


# static fields
.field public static final ARTIST:Ljava/lang/String; = "artist"

.field public static final AUBUM:Ljava/lang/String; = "album"

.field public static final CREATED_COLUMN:I = 0x2

.field public static final DATABASE_STORAGE_BACKGROUND_PATH:Ljava/lang/String; = "/.multimediaThumbs/databases/background/storage/"

.field public static final DATABASE_STORAGE_PARENT_PATH:Ljava/lang/String; = "/.multimediaThumbs"

.field public static final DATABASE_STORAGE_PATH:Ljava/lang/String; = "/.multimediaThumbs/databases/storage/"

.field public static final DATE:Ljava/lang/String; = "date"

.field public static final DISPLAYNAME:Ljava/lang/String; = "displayname"

.field public static final HOSTIP:Ljava/lang/String; = "hostip"

.field public static final MODIFIED:Ljava/lang/String; = "modifiedDate"

.field public static final MODIFIED_COLUMN:I = 0x3

.field public static final PASSWORD:Ljava/lang/String; = "password"

.field public static final PATH:Ljava/lang/String; = "path"

.field public static final PATH_COLUMN:I = 0x1

.field public static final PROJECTION:[Ljava/lang/String;

.field public static final SIZE:Ljava/lang/String; = "size"

.field public static final TITLE:Ljava/lang/String; = "title"

.field public static final TYPE:Ljava/lang/String; = "type"

.field public static final USENAME:Ljava/lang/String; = "usename"

.field public static final _ID_COLUMN:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "path"

    aput-object v2, v0, v1

    sput-object v0, Lcom/konka/mm/finals/DBFinals;->PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
