.class public final enum Lcom/konka/mm/finals/CommonFinals$MMPlayControlType;
.super Ljava/lang/Enum;
.source "CommonFinals.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/mm/finals/CommonFinals;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "MMPlayControlType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/konka/mm/finals/CommonFinals$MMPlayControlType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lcom/konka/mm/finals/CommonFinals$MMPlayControlType;

.field public static final enum KONKA_MM_PLAY_CONTROL_FB:Lcom/konka/mm/finals/CommonFinals$MMPlayControlType;

.field public static final enum KONKA_MM_PLAY_CONTROL_FF:Lcom/konka/mm/finals/CommonFinals$MMPlayControlType;

.field public static final enum KONKA_MM_PLAY_CONTROL_PAUSE:Lcom/konka/mm/finals/CommonFinals$MMPlayControlType;

.field public static final enum KONKA_MM_PLAY_CONTROL_RESUME:Lcom/konka/mm/finals/CommonFinals$MMPlayControlType;

.field public static final enum KONKA_MM_PLAY_CONTROL_SEEK:Lcom/konka/mm/finals/CommonFinals$MMPlayControlType;

.field public static final enum KONKA_MM_PLAY_CONTROL_STOP:Lcom/konka/mm/finals/CommonFinals$MMPlayControlType;

.field public static final enum KONKA_MM_PLAY_CONTROL_UNKNOWN:Lcom/konka/mm/finals/CommonFinals$MMPlayControlType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/konka/mm/finals/CommonFinals$MMPlayControlType;

    const-string v1, "KONKA_MM_PLAY_CONTROL_RESUME"

    invoke-direct {v0, v1, v3}, Lcom/konka/mm/finals/CommonFinals$MMPlayControlType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/mm/finals/CommonFinals$MMPlayControlType;->KONKA_MM_PLAY_CONTROL_RESUME:Lcom/konka/mm/finals/CommonFinals$MMPlayControlType;

    new-instance v0, Lcom/konka/mm/finals/CommonFinals$MMPlayControlType;

    const-string v1, "KONKA_MM_PLAY_CONTROL_PAUSE"

    invoke-direct {v0, v1, v4}, Lcom/konka/mm/finals/CommonFinals$MMPlayControlType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/mm/finals/CommonFinals$MMPlayControlType;->KONKA_MM_PLAY_CONTROL_PAUSE:Lcom/konka/mm/finals/CommonFinals$MMPlayControlType;

    new-instance v0, Lcom/konka/mm/finals/CommonFinals$MMPlayControlType;

    const-string v1, "KONKA_MM_PLAY_CONTROL_FF"

    invoke-direct {v0, v1, v5}, Lcom/konka/mm/finals/CommonFinals$MMPlayControlType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/mm/finals/CommonFinals$MMPlayControlType;->KONKA_MM_PLAY_CONTROL_FF:Lcom/konka/mm/finals/CommonFinals$MMPlayControlType;

    new-instance v0, Lcom/konka/mm/finals/CommonFinals$MMPlayControlType;

    const-string v1, "KONKA_MM_PLAY_CONTROL_FB"

    invoke-direct {v0, v1, v6}, Lcom/konka/mm/finals/CommonFinals$MMPlayControlType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/mm/finals/CommonFinals$MMPlayControlType;->KONKA_MM_PLAY_CONTROL_FB:Lcom/konka/mm/finals/CommonFinals$MMPlayControlType;

    new-instance v0, Lcom/konka/mm/finals/CommonFinals$MMPlayControlType;

    const-string v1, "KONKA_MM_PLAY_CONTROL_SEEK"

    invoke-direct {v0, v1, v7}, Lcom/konka/mm/finals/CommonFinals$MMPlayControlType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/mm/finals/CommonFinals$MMPlayControlType;->KONKA_MM_PLAY_CONTROL_SEEK:Lcom/konka/mm/finals/CommonFinals$MMPlayControlType;

    new-instance v0, Lcom/konka/mm/finals/CommonFinals$MMPlayControlType;

    const-string v1, "KONKA_MM_PLAY_CONTROL_STOP"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/konka/mm/finals/CommonFinals$MMPlayControlType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/mm/finals/CommonFinals$MMPlayControlType;->KONKA_MM_PLAY_CONTROL_STOP:Lcom/konka/mm/finals/CommonFinals$MMPlayControlType;

    new-instance v0, Lcom/konka/mm/finals/CommonFinals$MMPlayControlType;

    const-string v1, "KONKA_MM_PLAY_CONTROL_UNKNOWN"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/konka/mm/finals/CommonFinals$MMPlayControlType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/mm/finals/CommonFinals$MMPlayControlType;->KONKA_MM_PLAY_CONTROL_UNKNOWN:Lcom/konka/mm/finals/CommonFinals$MMPlayControlType;

    const/4 v0, 0x7

    new-array v0, v0, [Lcom/konka/mm/finals/CommonFinals$MMPlayControlType;

    sget-object v1, Lcom/konka/mm/finals/CommonFinals$MMPlayControlType;->KONKA_MM_PLAY_CONTROL_RESUME:Lcom/konka/mm/finals/CommonFinals$MMPlayControlType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/konka/mm/finals/CommonFinals$MMPlayControlType;->KONKA_MM_PLAY_CONTROL_PAUSE:Lcom/konka/mm/finals/CommonFinals$MMPlayControlType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/konka/mm/finals/CommonFinals$MMPlayControlType;->KONKA_MM_PLAY_CONTROL_FF:Lcom/konka/mm/finals/CommonFinals$MMPlayControlType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/konka/mm/finals/CommonFinals$MMPlayControlType;->KONKA_MM_PLAY_CONTROL_FB:Lcom/konka/mm/finals/CommonFinals$MMPlayControlType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/konka/mm/finals/CommonFinals$MMPlayControlType;->KONKA_MM_PLAY_CONTROL_SEEK:Lcom/konka/mm/finals/CommonFinals$MMPlayControlType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/konka/mm/finals/CommonFinals$MMPlayControlType;->KONKA_MM_PLAY_CONTROL_STOP:Lcom/konka/mm/finals/CommonFinals$MMPlayControlType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/konka/mm/finals/CommonFinals$MMPlayControlType;->KONKA_MM_PLAY_CONTROL_UNKNOWN:Lcom/konka/mm/finals/CommonFinals$MMPlayControlType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/konka/mm/finals/CommonFinals$MMPlayControlType;->ENUM$VALUES:[Lcom/konka/mm/finals/CommonFinals$MMPlayControlType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/konka/mm/finals/CommonFinals$MMPlayControlType;
    .locals 1

    const-class v0, Lcom/konka/mm/finals/CommonFinals$MMPlayControlType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/konka/mm/finals/CommonFinals$MMPlayControlType;

    return-object v0
.end method

.method public static values()[Lcom/konka/mm/finals/CommonFinals$MMPlayControlType;
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lcom/konka/mm/finals/CommonFinals$MMPlayControlType;->ENUM$VALUES:[Lcom/konka/mm/finals/CommonFinals$MMPlayControlType;

    array-length v1, v0

    new-array v2, v1, [Lcom/konka/mm/finals/CommonFinals$MMPlayControlType;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
