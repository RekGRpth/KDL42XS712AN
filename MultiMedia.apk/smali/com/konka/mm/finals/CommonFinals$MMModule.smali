.class public final enum Lcom/konka/mm/finals/CommonFinals$MMModule;
.super Ljava/lang/Enum;
.source "CommonFinals.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/mm/finals/CommonFinals;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "MMModule"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/konka/mm/finals/CommonFinals$MMModule;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lcom/konka/mm/finals/CommonFinals$MMModule;

.field public static final enum KONKA_MM_MODULE_AUDIO:Lcom/konka/mm/finals/CommonFinals$MMModule;

.field public static final enum KONKA_MM_MODULE_FILE_MANAGER:Lcom/konka/mm/finals/CommonFinals$MMModule;

.field public static final enum KONKA_MM_MODULE_IMAGE:Lcom/konka/mm/finals/CommonFinals$MMModule;

.field public static final enum KONKA_MM_MODULE_INVALIDATE:Lcom/konka/mm/finals/CommonFinals$MMModule;

.field public static final enum KONKA_MM_MODULE_SHARE:Lcom/konka/mm/finals/CommonFinals$MMModule;

.field public static final enum KONKA_MM_MODULE_VIDEO:Lcom/konka/mm/finals/CommonFinals$MMModule;

.field public static final enum KONKA_MM_MODULE_WIDGET:Lcom/konka/mm/finals/CommonFinals$MMModule;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/konka/mm/finals/CommonFinals$MMModule;

    const-string v1, "KONKA_MM_MODULE_INVALIDATE"

    invoke-direct {v0, v1, v3}, Lcom/konka/mm/finals/CommonFinals$MMModule;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/mm/finals/CommonFinals$MMModule;->KONKA_MM_MODULE_INVALIDATE:Lcom/konka/mm/finals/CommonFinals$MMModule;

    new-instance v0, Lcom/konka/mm/finals/CommonFinals$MMModule;

    const-string v1, "KONKA_MM_MODULE_AUDIO"

    invoke-direct {v0, v1, v4}, Lcom/konka/mm/finals/CommonFinals$MMModule;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/mm/finals/CommonFinals$MMModule;->KONKA_MM_MODULE_AUDIO:Lcom/konka/mm/finals/CommonFinals$MMModule;

    new-instance v0, Lcom/konka/mm/finals/CommonFinals$MMModule;

    const-string v1, "KONKA_MM_MODULE_VIDEO"

    invoke-direct {v0, v1, v5}, Lcom/konka/mm/finals/CommonFinals$MMModule;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/mm/finals/CommonFinals$MMModule;->KONKA_MM_MODULE_VIDEO:Lcom/konka/mm/finals/CommonFinals$MMModule;

    new-instance v0, Lcom/konka/mm/finals/CommonFinals$MMModule;

    const-string v1, "KONKA_MM_MODULE_IMAGE"

    invoke-direct {v0, v1, v6}, Lcom/konka/mm/finals/CommonFinals$MMModule;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/mm/finals/CommonFinals$MMModule;->KONKA_MM_MODULE_IMAGE:Lcom/konka/mm/finals/CommonFinals$MMModule;

    new-instance v0, Lcom/konka/mm/finals/CommonFinals$MMModule;

    const-string v1, "KONKA_MM_MODULE_SHARE"

    invoke-direct {v0, v1, v7}, Lcom/konka/mm/finals/CommonFinals$MMModule;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/mm/finals/CommonFinals$MMModule;->KONKA_MM_MODULE_SHARE:Lcom/konka/mm/finals/CommonFinals$MMModule;

    new-instance v0, Lcom/konka/mm/finals/CommonFinals$MMModule;

    const-string v1, "KONKA_MM_MODULE_FILE_MANAGER"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/konka/mm/finals/CommonFinals$MMModule;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/mm/finals/CommonFinals$MMModule;->KONKA_MM_MODULE_FILE_MANAGER:Lcom/konka/mm/finals/CommonFinals$MMModule;

    new-instance v0, Lcom/konka/mm/finals/CommonFinals$MMModule;

    const-string v1, "KONKA_MM_MODULE_WIDGET"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/konka/mm/finals/CommonFinals$MMModule;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/mm/finals/CommonFinals$MMModule;->KONKA_MM_MODULE_WIDGET:Lcom/konka/mm/finals/CommonFinals$MMModule;

    const/4 v0, 0x7

    new-array v0, v0, [Lcom/konka/mm/finals/CommonFinals$MMModule;

    sget-object v1, Lcom/konka/mm/finals/CommonFinals$MMModule;->KONKA_MM_MODULE_INVALIDATE:Lcom/konka/mm/finals/CommonFinals$MMModule;

    aput-object v1, v0, v3

    sget-object v1, Lcom/konka/mm/finals/CommonFinals$MMModule;->KONKA_MM_MODULE_AUDIO:Lcom/konka/mm/finals/CommonFinals$MMModule;

    aput-object v1, v0, v4

    sget-object v1, Lcom/konka/mm/finals/CommonFinals$MMModule;->KONKA_MM_MODULE_VIDEO:Lcom/konka/mm/finals/CommonFinals$MMModule;

    aput-object v1, v0, v5

    sget-object v1, Lcom/konka/mm/finals/CommonFinals$MMModule;->KONKA_MM_MODULE_IMAGE:Lcom/konka/mm/finals/CommonFinals$MMModule;

    aput-object v1, v0, v6

    sget-object v1, Lcom/konka/mm/finals/CommonFinals$MMModule;->KONKA_MM_MODULE_SHARE:Lcom/konka/mm/finals/CommonFinals$MMModule;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/konka/mm/finals/CommonFinals$MMModule;->KONKA_MM_MODULE_FILE_MANAGER:Lcom/konka/mm/finals/CommonFinals$MMModule;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/konka/mm/finals/CommonFinals$MMModule;->KONKA_MM_MODULE_WIDGET:Lcom/konka/mm/finals/CommonFinals$MMModule;

    aput-object v2, v0, v1

    sput-object v0, Lcom/konka/mm/finals/CommonFinals$MMModule;->ENUM$VALUES:[Lcom/konka/mm/finals/CommonFinals$MMModule;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/konka/mm/finals/CommonFinals$MMModule;
    .locals 1

    const-class v0, Lcom/konka/mm/finals/CommonFinals$MMModule;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/konka/mm/finals/CommonFinals$MMModule;

    return-object v0
.end method

.method public static values()[Lcom/konka/mm/finals/CommonFinals$MMModule;
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lcom/konka/mm/finals/CommonFinals$MMModule;->ENUM$VALUES:[Lcom/konka/mm/finals/CommonFinals$MMModule;

    array-length v1, v0

    new-array v2, v1, [Lcom/konka/mm/finals/CommonFinals$MMModule;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
