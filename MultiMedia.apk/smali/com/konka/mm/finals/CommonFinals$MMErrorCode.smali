.class public final enum Lcom/konka/mm/finals/CommonFinals$MMErrorCode;
.super Ljava/lang/Enum;
.source "CommonFinals.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/mm/finals/CommonFinals;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "MMErrorCode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/konka/mm/finals/CommonFinals$MMErrorCode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lcom/konka/mm/finals/CommonFinals$MMErrorCode;

.field public static final enum KONKA_DISK_DROP:Lcom/konka/mm/finals/CommonFinals$MMErrorCode;

.field public static final enum KONKA_DISK_INUSE_DROP:Lcom/konka/mm/finals/CommonFinals$MMErrorCode;

.field public static final enum KONKA_NO_ERROR:Lcom/konka/mm/finals/CommonFinals$MMErrorCode;

.field public static final enum KONKA_NO_SUPPORTED_FILE_FOUND:Lcom/konka/mm/finals/CommonFinals$MMErrorCode;

.field public static final enum KONKA_OPERATION_NOT_SUPPORT:Lcom/konka/mm/finals/CommonFinals$MMErrorCode;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/konka/mm/finals/CommonFinals$MMErrorCode;

    const-string v1, "KONKA_NO_ERROR"

    invoke-direct {v0, v1, v2}, Lcom/konka/mm/finals/CommonFinals$MMErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/mm/finals/CommonFinals$MMErrorCode;->KONKA_NO_ERROR:Lcom/konka/mm/finals/CommonFinals$MMErrorCode;

    new-instance v0, Lcom/konka/mm/finals/CommonFinals$MMErrorCode;

    const-string v1, "KONKA_DISK_INUSE_DROP"

    invoke-direct {v0, v1, v3}, Lcom/konka/mm/finals/CommonFinals$MMErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/mm/finals/CommonFinals$MMErrorCode;->KONKA_DISK_INUSE_DROP:Lcom/konka/mm/finals/CommonFinals$MMErrorCode;

    new-instance v0, Lcom/konka/mm/finals/CommonFinals$MMErrorCode;

    const-string v1, "KONKA_DISK_DROP"

    invoke-direct {v0, v1, v4}, Lcom/konka/mm/finals/CommonFinals$MMErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/mm/finals/CommonFinals$MMErrorCode;->KONKA_DISK_DROP:Lcom/konka/mm/finals/CommonFinals$MMErrorCode;

    new-instance v0, Lcom/konka/mm/finals/CommonFinals$MMErrorCode;

    const-string v1, "KONKA_NO_SUPPORTED_FILE_FOUND"

    invoke-direct {v0, v1, v5}, Lcom/konka/mm/finals/CommonFinals$MMErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/mm/finals/CommonFinals$MMErrorCode;->KONKA_NO_SUPPORTED_FILE_FOUND:Lcom/konka/mm/finals/CommonFinals$MMErrorCode;

    new-instance v0, Lcom/konka/mm/finals/CommonFinals$MMErrorCode;

    const-string v1, "KONKA_OPERATION_NOT_SUPPORT"

    invoke-direct {v0, v1, v6}, Lcom/konka/mm/finals/CommonFinals$MMErrorCode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/mm/finals/CommonFinals$MMErrorCode;->KONKA_OPERATION_NOT_SUPPORT:Lcom/konka/mm/finals/CommonFinals$MMErrorCode;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/konka/mm/finals/CommonFinals$MMErrorCode;

    sget-object v1, Lcom/konka/mm/finals/CommonFinals$MMErrorCode;->KONKA_NO_ERROR:Lcom/konka/mm/finals/CommonFinals$MMErrorCode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/konka/mm/finals/CommonFinals$MMErrorCode;->KONKA_DISK_INUSE_DROP:Lcom/konka/mm/finals/CommonFinals$MMErrorCode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/konka/mm/finals/CommonFinals$MMErrorCode;->KONKA_DISK_DROP:Lcom/konka/mm/finals/CommonFinals$MMErrorCode;

    aput-object v1, v0, v4

    sget-object v1, Lcom/konka/mm/finals/CommonFinals$MMErrorCode;->KONKA_NO_SUPPORTED_FILE_FOUND:Lcom/konka/mm/finals/CommonFinals$MMErrorCode;

    aput-object v1, v0, v5

    sget-object v1, Lcom/konka/mm/finals/CommonFinals$MMErrorCode;->KONKA_OPERATION_NOT_SUPPORT:Lcom/konka/mm/finals/CommonFinals$MMErrorCode;

    aput-object v1, v0, v6

    sput-object v0, Lcom/konka/mm/finals/CommonFinals$MMErrorCode;->ENUM$VALUES:[Lcom/konka/mm/finals/CommonFinals$MMErrorCode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/konka/mm/finals/CommonFinals$MMErrorCode;
    .locals 1

    const-class v0, Lcom/konka/mm/finals/CommonFinals$MMErrorCode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/konka/mm/finals/CommonFinals$MMErrorCode;

    return-object v0
.end method

.method public static values()[Lcom/konka/mm/finals/CommonFinals$MMErrorCode;
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lcom/konka/mm/finals/CommonFinals$MMErrorCode;->ENUM$VALUES:[Lcom/konka/mm/finals/CommonFinals$MMErrorCode;

    array-length v1, v0

    new-array v2, v1, [Lcom/konka/mm/finals/CommonFinals$MMErrorCode;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
