.class public Lcom/konka/mm/finals/CommonFinals$MyOnClickedListener;
.super Ljava/lang/Object;
.source "CommonFinals.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/mm/finals/CommonFinals;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MyOnClickedListener"
.end annotation


# instance fields
.field private context:Landroid/app/Activity;

.field private file:Ljava/io/File;

.field private flag:I

.field private popupWindow:Landroid/widget/PopupWindow;


# direct methods
.method public constructor <init>(Landroid/app/Activity;ILandroid/widget/PopupWindow;)V
    .locals 0
    .param p1    # Landroid/app/Activity;
    .param p2    # I
    .param p3    # Landroid/widget/PopupWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p2, p0, Lcom/konka/mm/finals/CommonFinals$MyOnClickedListener;->flag:I

    iput-object p1, p0, Lcom/konka/mm/finals/CommonFinals$MyOnClickedListener;->context:Landroid/app/Activity;

    iput-object p3, p0, Lcom/konka/mm/finals/CommonFinals$MyOnClickedListener;->popupWindow:Landroid/widget/PopupWindow;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;ILandroid/widget/PopupWindow;Ljava/io/File;)V
    .locals 0
    .param p1    # Landroid/app/Activity;
    .param p2    # I
    .param p3    # Landroid/widget/PopupWindow;
    .param p4    # Ljava/io/File;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p2, p0, Lcom/konka/mm/finals/CommonFinals$MyOnClickedListener;->flag:I

    iput-object p1, p0, Lcom/konka/mm/finals/CommonFinals$MyOnClickedListener;->context:Landroid/app/Activity;

    iput-object p3, p0, Lcom/konka/mm/finals/CommonFinals$MyOnClickedListener;->popupWindow:Landroid/widget/PopupWindow;

    iput-object p4, p0, Lcom/konka/mm/finals/CommonFinals$MyOnClickedListener;->file:Ljava/io/File;

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/konka/mm/finals/CommonFinals$MyOnClickedListener;->popupWindow:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/konka/mm/finals/CommonFinals$MyOnClickedListener;->popupWindow:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    :cond_1
    iget-object v0, p0, Lcom/konka/mm/finals/CommonFinals$MyOnClickedListener;->file:Ljava/io/File;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/konka/mm/finals/CommonFinals$MyOnClickedListener;->context:Landroid/app/Activity;

    iget v1, p0, Lcom/konka/mm/finals/CommonFinals$MyOnClickedListener;->flag:I

    # invokes: Lcom/konka/mm/finals/CommonFinals;->doSomeCtrl(Landroid/app/Activity;I)V
    invoke-static {v0, v1}, Lcom/konka/mm/finals/CommonFinals;->access$0(Landroid/app/Activity;I)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/konka/mm/finals/CommonFinals$MyOnClickedListener;->context:Landroid/app/Activity;

    iget v1, p0, Lcom/konka/mm/finals/CommonFinals$MyOnClickedListener;->flag:I

    iget-object v2, p0, Lcom/konka/mm/finals/CommonFinals$MyOnClickedListener;->file:Ljava/io/File;

    # invokes: Lcom/konka/mm/finals/CommonFinals;->doSomeCtrl(Landroid/app/Activity;ILjava/io/File;)V
    invoke-static {v0, v1, v2}, Lcom/konka/mm/finals/CommonFinals;->access$1(Landroid/app/Activity;ILjava/io/File;)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/konka/mm/finals/CommonFinals$MyOnClickedListener;->popupWindow:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/mm/finals/CommonFinals$MyOnClickedListener;->popupWindow:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7f0b00a6
        :pswitch_0    # com.konka.mm.R.id.btn_popup_yes
        :pswitch_1    # com.konka.mm.R.id.btn_popup_no
    .end packed-switch
.end method
