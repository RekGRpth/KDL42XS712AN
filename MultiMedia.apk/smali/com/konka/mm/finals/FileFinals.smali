.class public Lcom/konka/mm/finals/FileFinals;
.super Ljava/lang/Object;
.source "FileFinals.java"


# static fields
.field public static final COM_KONKA_MM_MOVIE_LIST:Ljava/lang/String; = "com.konka.mm.movie.list"

.field public static final COM_KONKA_VIDEO_PLAYER:Ljava/lang/String; = "com.konka.kkvideoplayer"

.field public static final COM_KONKA_VIDEO_PLAYER_MAIN:Ljava/lang/String; = "com.konka.kkvideoplayer.VideoPlayerMain"

.field public static final FILE_ALL_LISTS:Ljava/lang/String; = "com.konka.mm.file.all.lists"

.field public static final FILE_COME_FROM_CAMERA:Ljava/lang/String; = "com.konka.mm.file.com.from.camera"

.field public static final FILE_COME_FROM_FILEMANAGER:Ljava/lang/String; = "com.konka.mm.file.come.from.filemanager"

.field public static final FILE_COME_FROM_ONE_DISK:Ljava/lang/String; = "com.konka.mm.file.come.from.one.disk"

.field public static final FILE_COME_FROM_OTHER:Ljava/lang/String; = "com.konka.mm.file.come.from.other"

.field public static final FILE_COME_FROM_SAMBA:Ljava/lang/String; = "com.konka.mm.file.come.from.samba"

.field public static final FILE_INDEX_POSSTION:Ljava/lang/String; = "com.konka.mm.file.index.posstion"

.field public static final FILE_PARENT_PATH:Ljava/lang/String; = "com.konka.mm.file.parent.path"

.field public static final FILE_ROOT_PATH:Ljava/lang/String; = "com.konka.mm.file.root.path"

.field public static final FILE_SORT_TYPE:Ljava/lang/String; = "com.konka.mm.file.sort.type"

.field public static final FILE_WHERE_COME_FROM:Ljava/lang/String; = "com.konka.mm.file.where.come.from"

.field public static final IS_NEED_KILL_MUSIC_SERVICE_WHEN_QUIT:Ljava/lang/String; = "com.konka.mm.is.need.kill.music.service.when.quit"

.field public static final MODULE_CALLED_APK:Ljava/lang/String; = "com.konka.mm.module.called.apk"

.field public static final MODULE_CALLED_AUDIO:Ljava/lang/String; = "com.konka.mm.module.called.audio"

.field public static final MODULE_CALLED_IMAGE:Ljava/lang/String; = "com.konka.mm.module.called.image"

.field public static final MODULE_CALLED_INDEX_DISK:Ljava/lang/String; = "com.konka.mm.module.called.index.disk"

.field public static final MODULE_CALLED_TXT:Ljava/lang/String; = "com.konka.mm.module.called.txt"

.field public static final MODULE_CALLED_VIDEO:Ljava/lang/String; = "com.konka.mm.module.called.video"

.field public static final MODULE_CALLED_WHO:Ljava/lang/String; = "com.konka.mm.module.called.who"

.field public static final SAMBA_CURRENT_IP:Ljava/lang/String; = "com.konka.mm.samba.current.ip"

.field public static final VIDEO_FILE_PATH:Ljava/lang/String; = "videofile"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
