.class public Lcom/konka/mm/finals/CommonFinals;
.super Ljava/lang/Object;
.source "CommonFinals.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/mm/finals/CommonFinals$MMErrorCode;,
        Lcom/konka/mm/finals/CommonFinals$MMModule;,
        Lcom/konka/mm/finals/CommonFinals$MMPlayControlType;,
        Lcom/konka/mm/finals/CommonFinals$MMResolution;,
        Lcom/konka/mm/finals/CommonFinals$MMSourceType;,
        Lcom/konka/mm/finals/CommonFinals$MyOnClickedListener;
    }
.end annotation


# static fields
.field public static final ALL_MEDIA_FILE_TO_SHOW:I = 0x1

.field public static final CACHDIR:Ljava/lang/String; = "kk.com.konka.mm.ImgCach"

.field public static CHARSET_LIST:[Ljava/lang/String; = null

.field public static final DELETE_ALL_FLAG:I = 0x2

.field public static final DELETE_FILE_FLAG:I = 0x1

.field public static final ERROR_FLAG:I = 0x0

.field public static final IMAGE_FILE_TO_SHOW:I = 0x4

.field public static final INTO_EDIT_FLAG:I = 0x3

.field public static final INTO_RENAME_FLAG:I = 0x4

.field public static final IS_OPEN_HOMESHARE_DB:Z = true

.field public static final MENTION_FLAG:I = 0x2

.field public static MM_LOG_TAG:Ljava/lang/String; = null

.field public static final MUSIC_FILE_TO_SHOW:I = 0x3

.field public static final NUM_COLUMNS_GRID:I = 0x6

.field public static final NUM_COLUMNS_LIST:I = 0x3

.field public static final PAGE_GRID_SIZE:I = 0x12

.field public static final PAGE_LIST_SIZE:I = 0x21

.field public static final QUIT_ACTIVITY_FLAG:I = 0x0

.field public static final RECYCLE:Ljava/lang/String; = "$RECYCLE.BIN"

.field public static final RENAME_FILE_FLAG:I = 0x5

.field private static final TAG:Ljava/lang/String; = "CommonFinals"

.field public static final VIDEO_FILE_TO_SHOW:I = 0x2

.field public static final WARM_FLAG:I = 0x1

.field public static dlg:Landroid/app/AlertDialog; = null

.field public static final mIsAbroad:Z = true

.field public static proDlg:Landroid/app/ProgressDialog;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x0

    const-string v0, "KONKA_MM"

    sput-object v0, Lcom/konka/mm/finals/CommonFinals;->MM_LOG_TAG:Ljava/lang/String;

    const/16 v0, 0x18

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "C:"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "D:"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "E:"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "F:"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "G:"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "H:"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "I:"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "J:"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "K:"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "L:"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "M:"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "N:"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "O:"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "P:"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "Q:"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "R:"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "S:"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "T:"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "U:"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "V:"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "W:"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "X:"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "Y:"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "Z:"

    aput-object v2, v0, v1

    sput-object v0, Lcom/konka/mm/finals/CommonFinals;->CHARSET_LIST:[Ljava/lang/String;

    sput-object v3, Lcom/konka/mm/finals/CommonFinals;->dlg:Landroid/app/AlertDialog;

    sput-object v3, Lcom/konka/mm/finals/CommonFinals;->proDlg:Landroid/app/ProgressDialog;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0(Landroid/app/Activity;I)V
    .locals 0

    invoke-static {p0, p1}, Lcom/konka/mm/finals/CommonFinals;->doSomeCtrl(Landroid/app/Activity;I)V

    return-void
.end method

.method static synthetic access$1(Landroid/app/Activity;ILjava/io/File;)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/konka/mm/finals/CommonFinals;->doSomeCtrl(Landroid/app/Activity;ILjava/io/File;)V

    return-void
.end method

.method private static doSomeCtrl(Landroid/app/Activity;I)V
    .locals 5
    .param p0    # Landroid/app/Activity;
    .param p1    # I

    const/4 v4, 0x1

    const-string v2, "CommonFinals"

    const-string v3, "doSomeCtrl no File"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    packed-switch p1, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :pswitch_2
    move-object v0, p0

    check-cast v0, Lcom/konka/mm/filemanager/FileListActivity;

    iput-boolean v4, v0, Lcom/konka/mm/filemanager/FileListActivity;->isManager:Z

    iget-object v2, v0, Lcom/konka/mm/filemanager/FileListActivity;->observable:Lcom/konka/mm/filemanager/FileListActivity$MyObservable;

    invoke-virtual {v2, v4}, Lcom/konka/mm/filemanager/FileListActivity$MyObservable;->toobarStatusChanged(Z)V

    goto :goto_0

    :pswitch_3
    move-object v1, p0

    check-cast v1, Lcom/konka/mm/filemanager/FileListActivity;

    iput-boolean v4, v1, Lcom/konka/mm/filemanager/FileListActivity;->isRenameManager:Z

    iget-object v2, v1, Lcom/konka/mm/filemanager/FileListActivity;->observable:Lcom/konka/mm/filemanager/FileListActivity$MyObservable;

    invoke-virtual {v2, v4}, Lcom/konka/mm/filemanager/FileListActivity$MyObservable;->toobarStatusChanged(Z)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private static doSomeCtrl(Landroid/app/Activity;ILjava/io/File;)V
    .locals 12
    .param p0    # Landroid/app/Activity;
    .param p1    # I
    .param p2    # Ljava/io/File;

    const/4 v11, 0x0

    const/16 v10, 0x3e8

    const-string v7, "CommonFinals"

    const-string v8, "doSomeCtrl has File"

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :pswitch_2
    move-object v2, p0

    check-cast v2, Lcom/konka/mm/filemanager/FileListActivity;

    const/4 v4, 0x0

    invoke-static {p2}, Lcom/konka/mm/tools/FileTool;->deleteFileOrDirectory(Ljava/io/File;)Z

    move-result v4

    if-eqz v4, :cond_1

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Lcom/konka/mm/filemanager/FileListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0900af    # com.konka.mm.R.string.FILE_DELETE_SUCCESS

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v8, "!"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v2, v7, v10}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/Toast;->show()V

    :goto_1
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v7

    new-instance v8, Lcom/konka/mm/filemanager/FileManagerFilter;

    invoke-direct {v8, v2}, Lcom/konka/mm/filemanager/FileManagerFilter;-><init>(Landroid/content/Context;)V

    invoke-virtual {v7, v8}, Ljava/io/File;->listFiles(Ljava/io/FileFilter;)[Ljava/io/File;

    move-result-object v5

    if-eqz v5, :cond_2

    array-length v7, v5

    if-lez v7, :cond_2

    invoke-virtual {p2}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x4

    invoke-virtual {v2, v7, v8}, Lcom/konka/mm/filemanager/FileListActivity;->broweTo(Ljava/lang/String;I)V

    goto :goto_0

    :cond_1
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Lcom/konka/mm/filemanager/FileListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f090081    # com.konka.mm.R.string.FILE_DELETE

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v2}, Lcom/konka/mm/filemanager/FileListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f09008b    # com.konka.mm.R.string.FAILE

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "!"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v2, v7, v10}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/Toast;->show()V

    goto :goto_1

    :cond_2
    invoke-virtual {v2, v11}, Lcom/konka/mm/filemanager/FileListActivity;->initFileInfo(Ljava/io/File;)V

    invoke-virtual {v2}, Lcom/konka/mm/filemanager/FileListActivity;->up2One()V

    goto/16 :goto_0

    :pswitch_3
    const-string v7, "CommonFinals"

    const-string v8, "\u8fdb\u5165\u6700\u7ec8\u64cd\u4f5c"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :pswitch_4
    const-string v7, "CommonFinals"

    const-string v8, "Delete All"

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object v3, p0

    check-cast v3, Lcom/konka/mm/filemanager/FileListActivity;

    invoke-virtual {p2}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v7

    if-eqz v7, :cond_4

    invoke-virtual {p2}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v7

    invoke-virtual {v7}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    if-eqz v1, :cond_3

    const/4 v6, 0x0

    :goto_2
    array-length v7, v1

    if-lt v6, v7, :cond_5

    :cond_3
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Lcom/konka/mm/filemanager/FileListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f090082    # com.konka.mm.R.string.DELETE_ALL_FILE

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v3}, Lcom/konka/mm/filemanager/FileListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f09008a    # com.konka.mm.R.string.SUCCESS

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "!"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v3, v7, v10}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/Toast;->show()V

    :cond_4
    invoke-virtual {v3}, Lcom/konka/mm/filemanager/FileListActivity;->up2One()V

    invoke-virtual {v3, v11}, Lcom/konka/mm/filemanager/FileListActivity;->initFileInfo(Ljava/io/File;)V

    goto/16 :goto_0

    :cond_5
    aget-object v0, v1, v6

    invoke-static {v0}, Lcom/konka/mm/tools/FileTool;->deleteFileOrDirectory(Ljava/io/File;)Z

    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public static isUsbRemove(Ljava/util/List;Ljava/lang/String;)Z
    .locals 4
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")Z"
        }
    .end annotation

    const/4 v1, 0x0

    if-eqz p0, :cond_1

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_1

    if-eqz p1, :cond_1

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_2

    const/4 v1, 0x1

    :cond_1
    :goto_0
    return v1

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    goto :goto_0
.end method

.method public static mmFileNotFound(Landroid/app/Activity;Ljava/lang/String;)V
    .locals 4
    .param p0    # Landroid/app/Activity;
    .param p1    # Ljava/lang/String;

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090092    # com.konka.mm.R.string.NOT_MM_FILE_FOUND

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x108009b    # android.R.drawable.ic_dialog_info

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09000c    # com.konka.mm.R.string.MM_IS_QUIT_APP

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\uff1f"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09000a    # com.konka.mm.R.string.MM_CANCLE

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090009    # com.konka.mm.R.string.MM_QUIE

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/konka/mm/finals/CommonFinals$7;

    invoke-direct {v2, p0}, Lcom/konka/mm/finals/CommonFinals$7;-><init>(Landroid/app/Activity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    return-void
.end method

.method public static quitActivity(Landroid/app/Activity;Ljava/lang/String;)V
    .locals 4
    .param p0    # Landroid/app/Activity;
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    return-void

    const v3, 0x7f0900aa    # com.konka.mm.R.string.MM_QUIT_FILE_MANAGER

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090097    # com.konka.mm.R.string.mention_info

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x1080027    # android.R.drawable.ic_dialog_alert

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x108009b    # android.R.drawable.ic_dialog_info

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09000a    # com.konka.mm.R.string.MM_CANCLE

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090009    # com.konka.mm.R.string.MM_QUIE

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/konka/mm/finals/CommonFinals$1;

    invoke-direct {v3, p0}, Lcom/konka/mm/finals/CommonFinals$1;-><init>(Landroid/app/Activity;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    return-void

    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090017    # com.konka.mm.R.string.MM_AUDIO

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0900ad    # com.konka.mm.R.string.QUIT_Audio

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090004    # com.konka.mm.R.string.LaunchImage

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0900ab    # com.konka.mm.R.string.QUIT_LaunchImage

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_2
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090001    # com.konka.mm.R.string.VIDEO

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0900ac    # com.konka.mm.R.string.QUIT_Video

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_3
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090005    # com.konka.mm.R.string.LaunchShare

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0900ae    # com.konka.mm.R.string.QUIT_Share

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_4
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0
.end method

.method public static quitActivityByPopup(Landroid/app/Activity;Ljava/lang/String;III)V
    .locals 11
    .param p0    # Landroid/app/Activity;
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v10, 0x0

    const/4 v9, 0x1

    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v6

    const v7, 0x7f030028    # com.konka.mm.R.layout.popup_quit_activity

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    const v6, 0x7f0b00a6    # com.konka.mm.R.id.btn_popup_yes

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    const v6, 0x7f0b00a7    # com.konka.mm.R.id.btn_popup_no

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const v6, 0x7f0b00a5    # com.konka.mm.R.id.tv_popup_content

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v6, 0x7f0b00a3    # com.konka.mm.R.id.img_popup_icon

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    invoke-static {v2, p2}, Lcom/konka/mm/finals/CommonFinals;->setPopupIcon(Landroid/widget/ImageView;I)V

    invoke-static {p0, v3, p1, p3}, Lcom/konka/mm/finals/CommonFinals;->setPopupContext(Landroid/app/Activity;Landroid/widget/TextView;Ljava/lang/String;I)V

    invoke-static {p3, v1}, Lcom/konka/mm/finals/CommonFinals;->setBtnTextByFlag(ILandroid/widget/Button;)V

    if-nez p4, :cond_0

    new-instance v4, Landroid/widget/PopupWindow;

    const/16 v6, 0x15e

    const/16 v7, 0xc8

    invoke-direct {v4, v5, v6, v7, v9}, Landroid/widget/PopupWindow;-><init>(Landroid/view/View;IIZ)V

    :goto_0
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v6

    const/16 v7, 0x11

    invoke-virtual {v4, v6, v7, v10, v10}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    new-instance v6, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v6}, Landroid/graphics/drawable/BitmapDrawable;-><init>()V

    invoke-virtual {v4, v6}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v0, v9}, Landroid/widget/Button;->setFocusable(Z)V

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    new-instance v6, Lcom/konka/mm/finals/CommonFinals$MyOnClickedListener;

    invoke-direct {v6, p0, p3, v4}, Lcom/konka/mm/finals/CommonFinals$MyOnClickedListener;-><init>(Landroid/app/Activity;ILandroid/widget/PopupWindow;)V

    invoke-virtual {v1, v6}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v6, Lcom/konka/mm/finals/CommonFinals$MyOnClickedListener;

    invoke-direct {v6, p0, p3, v4}, Lcom/konka/mm/finals/CommonFinals$MyOnClickedListener;-><init>(Landroid/app/Activity;ILandroid/widget/PopupWindow;)V

    invoke-virtual {v0, v6}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v6, Lcom/konka/mm/finals/CommonFinals$2;

    invoke-direct {v6, v4}, Lcom/konka/mm/finals/CommonFinals$2;-><init>(Landroid/widget/PopupWindow;)V

    invoke-virtual {v5, v6}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    return-void

    :cond_0
    new-instance v4, Landroid/widget/PopupWindow;

    const/16 v6, 0x226

    const/16 v7, 0x12c

    invoke-direct {v4, v5, v6, v7, v9}, Landroid/widget/PopupWindow;-><init>(Landroid/view/View;IIZ)V

    goto :goto_0
.end method

.method public static quitActivityByPopup(Landroid/app/Activity;Ljava/lang/String;IILjava/io/File;I)V
    .locals 10
    .param p0    # Landroid/app/Activity;
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # I
    .param p4    # Ljava/io/File;
    .param p5    # I

    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v6

    const v7, 0x7f030028    # com.konka.mm.R.layout.popup_quit_activity

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    const v6, 0x7f0b00a6    # com.konka.mm.R.id.btn_popup_yes

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    const v6, 0x7f0b00a7    # com.konka.mm.R.id.btn_popup_no

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const v6, 0x7f0b00a5    # com.konka.mm.R.id.tv_popup_content

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v6, 0x7f0b00a3    # com.konka.mm.R.id.img_popup_icon

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    invoke-static {v2, p2}, Lcom/konka/mm/finals/CommonFinals;->setPopupIcon(Landroid/widget/ImageView;I)V

    invoke-static {p0, v3, p1, p3}, Lcom/konka/mm/finals/CommonFinals;->setPopupContext(Landroid/app/Activity;Landroid/widget/TextView;Ljava/lang/String;I)V

    invoke-static {p3, v1}, Lcom/konka/mm/finals/CommonFinals;->setBtnTextByFlag(ILandroid/widget/Button;)V

    if-nez p5, :cond_0

    new-instance v4, Landroid/widget/PopupWindow;

    const/16 v6, 0x15e

    const/16 v7, 0xc8

    const/4 v8, 0x1

    invoke-direct {v4, v5, v6, v7, v8}, Landroid/widget/PopupWindow;-><init>(Landroid/view/View;IIZ)V

    :goto_0
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v6

    const/16 v7, 0x11

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual {v4, v6, v7, v8, v9}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    new-instance v6, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v6}, Landroid/graphics/drawable/BitmapDrawable;-><init>()V

    invoke-virtual {v4, v6}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    const/4 v6, 0x1

    invoke-virtual {v0, v6}, Landroid/widget/Button;->setFocusable(Z)V

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    new-instance v6, Lcom/konka/mm/finals/CommonFinals$MyOnClickedListener;

    invoke-direct {v6, p0, p3, v4, p4}, Lcom/konka/mm/finals/CommonFinals$MyOnClickedListener;-><init>(Landroid/app/Activity;ILandroid/widget/PopupWindow;Ljava/io/File;)V

    invoke-virtual {v1, v6}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v6, Lcom/konka/mm/finals/CommonFinals$MyOnClickedListener;

    invoke-direct {v6, p0, p3, v4, p4}, Lcom/konka/mm/finals/CommonFinals$MyOnClickedListener;-><init>(Landroid/app/Activity;ILandroid/widget/PopupWindow;Ljava/io/File;)V

    invoke-virtual {v0, v6}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v6, Lcom/konka/mm/finals/CommonFinals$3;

    invoke-direct {v6, v4}, Lcom/konka/mm/finals/CommonFinals$3;-><init>(Landroid/widget/PopupWindow;)V

    invoke-virtual {v5, v6}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    return-void

    :cond_0
    new-instance v4, Landroid/widget/PopupWindow;

    const/16 v6, 0x226

    const/16 v7, 0x12c

    const/4 v8, 0x1

    invoke-direct {v4, v5, v6, v7, v8}, Landroid/widget/PopupWindow;-><init>(Landroid/view/View;IIZ)V

    goto :goto_0
.end method

.method public static quitActivityByPopup(Landroid/app/Activity;Ljava/lang/String;IILjava/io/File;IIII)V
    .locals 15
    .param p0    # Landroid/app/Activity;
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # I
    .param p4    # Ljava/io/File;
    .param p5    # I
    .param p6    # I
    .param p7    # I
    .param p8    # I

    const/4 v11, 0x5

    move/from16 v0, p3

    if-eq v0, v11, :cond_1

    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v11

    const v12, 0x7f030028    # com.konka.mm.R.layout.popup_quit_activity

    const/4 v13, 0x0

    invoke-virtual {v11, v12, v13}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v10

    const v11, 0x7f0b00a6    # com.konka.mm.R.id.btn_popup_yes

    invoke-virtual {v10, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    const v11, 0x7f0b00a7    # com.konka.mm.R.id.btn_popup_no

    invoke-virtual {v10, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    const v11, 0x7f0b00a5    # com.konka.mm.R.id.tv_popup_content

    invoke-virtual {v10, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    const v11, 0x7f0b00a3    # com.konka.mm.R.id.img_popup_icon

    invoke-virtual {v10, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    move/from16 v0, p2

    invoke-static {v6, v0}, Lcom/konka/mm/finals/CommonFinals;->setPopupIcon(Landroid/widget/ImageView;I)V

    move-object/from16 v0, p1

    move/from16 v1, p3

    invoke-static {p0, v7, v0, v1}, Lcom/konka/mm/finals/CommonFinals;->setPopupContext(Landroid/app/Activity;Landroid/widget/TextView;Ljava/lang/String;I)V

    move/from16 v0, p3

    invoke-static {v0, v3}, Lcom/konka/mm/finals/CommonFinals;->setBtnTextByFlag(ILandroid/widget/Button;)V

    if-nez p5, :cond_0

    new-instance v9, Landroid/widget/PopupWindow;

    const/16 v11, 0x15e

    const/16 v12, 0xc8

    const/4 v13, 0x1

    invoke-direct {v9, v10, v11, v12, v13}, Landroid/widget/PopupWindow;-><init>(Landroid/view/View;IIZ)V

    :goto_0
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v11

    invoke-virtual {v11}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v11

    const/16 v12, 0x11

    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-virtual {v9, v11, v12, v13, v14}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    new-instance v11, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v11}, Landroid/graphics/drawable/BitmapDrawable;-><init>()V

    invoke-virtual {v9, v11}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    const/4 v11, 0x1

    invoke-virtual {v2, v11}, Landroid/widget/Button;->setFocusable(Z)V

    invoke-virtual {v2}, Landroid/widget/Button;->requestFocus()Z

    new-instance v11, Lcom/konka/mm/finals/CommonFinals$MyOnClickedListener;

    move/from16 v0, p3

    move-object/from16 v1, p4

    invoke-direct {v11, p0, v0, v9, v1}, Lcom/konka/mm/finals/CommonFinals$MyOnClickedListener;-><init>(Landroid/app/Activity;ILandroid/widget/PopupWindow;Ljava/io/File;)V

    invoke-virtual {v3, v11}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v11, Lcom/konka/mm/finals/CommonFinals$MyOnClickedListener;

    move/from16 v0, p3

    move-object/from16 v1, p4

    invoke-direct {v11, p0, v0, v9, v1}, Lcom/konka/mm/finals/CommonFinals$MyOnClickedListener;-><init>(Landroid/app/Activity;ILandroid/widget/PopupWindow;Ljava/io/File;)V

    invoke-virtual {v2, v11}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v11, Lcom/konka/mm/finals/CommonFinals$4;

    invoke-direct {v11, v9}, Lcom/konka/mm/finals/CommonFinals$4;-><init>(Landroid/widget/PopupWindow;)V

    invoke-virtual {v10, v11}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    :goto_1
    return-void

    :cond_0
    new-instance v9, Landroid/widget/PopupWindow;

    const/16 v11, 0x226

    const/16 v12, 0x12c

    const/4 v13, 0x1

    invoke-direct {v9, v10, v11, v12, v13}, Landroid/widget/PopupWindow;-><init>(Landroid/view/View;IIZ)V

    goto :goto_0

    :cond_1
    move-object v5, p0

    check-cast v5, Lcom/konka/mm/filemanager/FileListActivity;

    new-instance v8, Lcom/konka/mm/filemanager/FileItemClickListener;

    invoke-direct {v8, v5}, Lcom/konka/mm/filemanager/FileItemClickListener;-><init>(Lcom/konka/mm/filemanager/FileListActivity;)V

    invoke-virtual/range {p4 .. p4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v8, v4}, Lcom/konka/mm/filemanager/FileItemClickListener;->setFile(Ljava/lang/String;)V

    mul-int v11, p6, p8

    add-int v11, v11, p7

    invoke-virtual {v8, v11}, Lcom/konka/mm/filemanager/FileItemClickListener;->setPosition(I)V

    invoke-virtual/range {p4 .. p4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v8, v11}, Lcom/konka/mm/filemanager/FileItemClickListener;->setName(Ljava/lang/String;)V

    const/4 v11, 0x0

    const/4 v12, 0x6

    invoke-virtual {v8, v11, v12}, Lcom/konka/mm/filemanager/FileItemClickListener;->onClick(Landroid/view/View;I)V

    const/4 v11, 0x0

    iput-boolean v11, v5, Lcom/konka/mm/filemanager/FileListActivity;->isRenameManager:Z

    iget-object v11, v5, Lcom/konka/mm/filemanager/FileListActivity;->observable:Lcom/konka/mm/filemanager/FileListActivity$MyObservable;

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Lcom/konka/mm/filemanager/FileListActivity$MyObservable;->toobarStatusChanged(Z)V

    goto :goto_1
.end method

.method public static quitDialog(Landroid/app/Activity;Ljava/lang/String;)V
    .locals 4
    .param p0    # Landroid/app/Activity;
    .param p1    # Ljava/lang/String;

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090022    # com.konka.mm.R.string.MM_FSM_MODULE_NAME

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x108009b    # android.R.drawable.ic_dialog_info

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090024    # com.konka.mm.R.string.MM_NOFOUND_SHARE_HOST

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090009    # com.konka.mm.R.string.MM_QUIE

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/konka/mm/finals/CommonFinals$6;

    invoke-direct {v2, p0}, Lcom/konka/mm/finals/CommonFinals$6;-><init>(Landroid/app/Activity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    return-void
.end method

.method public static quitNoFileDialog(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p0    # Landroid/app/Activity;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x108009b    # android.R.drawable.ic_dialog_info

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, ",  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09000b    # com.konka.mm.R.string.MM_QUIT_APP

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090009    # com.konka.mm.R.string.MM_QUIE

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/konka/mm/finals/CommonFinals$5;

    invoke-direct {v2, p0}, Lcom/konka/mm/finals/CommonFinals$5;-><init>(Landroid/app/Activity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    return-void
.end method

.method public static sdCardNoFound(Landroid/app/Activity;)V
    .locals 4
    .param p0    # Landroid/app/Activity;

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09008f    # com.konka.mm.R.string.storage

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090090    # com.konka.mm.R.string.remove

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x108009b    # android.R.drawable.ic_dialog_info

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090091    # com.konka.mm.R.string.NOT_STORAGE

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090009    # com.konka.mm.R.string.MM_QUIE

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/konka/mm/finals/CommonFinals$8;

    invoke-direct {v3, p0}, Lcom/konka/mm/finals/CommonFinals$8;-><init>(Landroid/app/Activity;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    sput-object v1, Lcom/konka/mm/finals/CommonFinals;->dlg:Landroid/app/AlertDialog;

    sget-object v1, Lcom/konka/mm/finals/CommonFinals;->dlg:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    return-void
.end method

.method public static sdCardNoFound(Landroid/app/Activity;Ljava/lang/String;)V
    .locals 4
    .param p0    # Landroid/app/Activity;
    .param p1    # Ljava/lang/String;

    invoke-static {p0}, Lcom/konka/mm/tools/FileTool;->usbIsExists(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09000d    # com.konka.mm.R.string.MM_SDCARD_NOT_FOUND

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x108009b    # android.R.drawable.ic_dialog_info

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090043    # com.konka.mm.R.string.sdcarderror

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090009    # com.konka.mm.R.string.MM_QUIE

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/konka/mm/finals/CommonFinals$9;

    invoke-direct {v3, p0}, Lcom/konka/mm/finals/CommonFinals$9;-><init>(Landroid/app/Activity;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    sput-object v1, Lcom/konka/mm/finals/CommonFinals;->dlg:Landroid/app/AlertDialog;

    sget-object v1, Lcom/konka/mm/finals/CommonFinals;->dlg:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    :goto_0
    return-void

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09008f    # com.konka.mm.R.string.storage

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090090    # com.konka.mm.R.string.remove

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x3e8

    invoke-static {p0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public static sdCardScan(Landroid/content/Context;)V
    .locals 2
    .param p0    # Landroid/content/Context;

    sget-object v0, Lcom/konka/mm/finals/CommonFinals;->proDlg:Landroid/app/ProgressDialog;

    if-nez v0, :cond_0

    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/konka/mm/finals/CommonFinals;->proDlg:Landroid/app/ProgressDialog;

    :cond_0
    sget-object v0, Lcom/konka/mm/finals/CommonFinals;->proDlg:Landroid/app/ProgressDialog;

    const-string v1, "\u6b63\u5728\u626b\u63cf\u78c1\u76d8..."

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    sget-object v0, Lcom/konka/mm/finals/CommonFinals;->proDlg:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    return-void
.end method

.method private static setBtnTextByFlag(ILandroid/widget/Button;)V
    .locals 1
    .param p0    # I
    .param p1    # Landroid/widget/Button;

    const v0, 0x7f09009b    # com.konka.mm.R.string.into

    packed-switch p0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p1, v0}, Landroid/widget/Button;->setText(I)V

    goto :goto_0

    :pswitch_1
    invoke-virtual {p1, v0}, Landroid/widget/Button;->setText(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static setPopupContext(Landroid/app/Activity;Landroid/widget/TextView;Ljava/lang/String;I)V
    .locals 3
    .param p0    # Landroid/app/Activity;
    .param p1    # Landroid/widget/TextView;
    .param p2    # Ljava/lang/String;
    .param p3    # I

    packed-switch p3, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09000c    # com.konka.mm.R.string.MM_IS_QUIT_APP

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090088    # com.konka.mm.R.string.SURE_DELETE

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090089    # com.konka.mm.R.string.SURE_DELETE_ALL

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_3
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_4
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private static setPopupIcon(Landroid/widget/ImageView;I)V
    .locals 1
    .param p0    # Landroid/widget/ImageView;
    .param p1    # I

    packed-switch p1, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    const v0, 0x7f0200b9    # com.konka.mm.R.drawable.popup_error_info_icon

    invoke-virtual {p0, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    :pswitch_2
    const v0, 0x7f0200bb    # com.konka.mm.R.drawable.popup_warm_info_icon

    invoke-virtual {p0, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method
