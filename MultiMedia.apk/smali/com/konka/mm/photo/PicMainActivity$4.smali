.class Lcom/konka/mm/photo/PicMainActivity$4;
.super Ljava/lang/Object;
.source "PicMainActivity.java"

# interfaces
.implements Landroid/widget/RadioGroup$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/mm/photo/PicMainActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/mm/photo/PicMainActivity;


# direct methods
.method constructor <init>(Lcom/konka/mm/photo/PicMainActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/photo/PicMainActivity$4;->this$0:Lcom/konka/mm/photo/PicMainActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/RadioGroup;I)V
    .locals 6
    .param p1    # Landroid/widget/RadioGroup;
    .param p2    # I

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    const v1, 0x7f09005e    # com.konka.mm.R.string.SORT

    iget-object v0, p0, Lcom/konka/mm/photo/PicMainActivity$4;->this$0:Lcom/konka/mm/photo/PicMainActivity;

    invoke-static {v0}, Lcom/konka/mm/tools/FileTool;->usbIsExists(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/konka/mm/photo/PicMainActivity$4;->this$0:Lcom/konka/mm/photo/PicMainActivity;

    # getter for: Lcom/konka/mm/photo/PicMainActivity;->popupWindow:Landroid/widget/PopupWindow;
    invoke-static {v0}, Lcom/konka/mm/photo/PicMainActivity;->access$2(Lcom/konka/mm/photo/PicMainActivity;)Landroid/widget/PopupWindow;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/mm/photo/PicMainActivity$4;->this$0:Lcom/konka/mm/photo/PicMainActivity;

    # getter for: Lcom/konka/mm/photo/PicMainActivity;->popupWindow:Landroid/widget/PopupWindow;
    invoke-static {v0}, Lcom/konka/mm/photo/PicMainActivity;->access$2(Lcom/konka/mm/photo/PicMainActivity;)Landroid/widget/PopupWindow;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    iget-object v0, p0, Lcom/konka/mm/photo/PicMainActivity$4;->this$0:Lcom/konka/mm/photo/PicMainActivity;

    invoke-static {v0, v2}, Lcom/konka/mm/photo/PicMainActivity;->access$3(Lcom/konka/mm/photo/PicMainActivity;Z)V

    :cond_0
    iget-object v0, p0, Lcom/konka/mm/photo/PicMainActivity$4;->this$0:Lcom/konka/mm/photo/PicMainActivity;

    iget-boolean v0, v0, Lcom/konka/mm/photo/PicMainActivity;->isFoundPhoto:Z

    if-eqz v0, :cond_1

    sparse-switch p2, :sswitch_data_0

    :cond_1
    :goto_0
    return-void

    :sswitch_0
    iget-object v0, p0, Lcom/konka/mm/photo/PicMainActivity$4;->this$0:Lcom/konka/mm/photo/PicMainActivity;

    # invokes: Lcom/konka/mm/photo/PicMainActivity;->showProgressDialog(II)V
    invoke-static {v0, v1, v1}, Lcom/konka/mm/photo/PicMainActivity;->access$4(Lcom/konka/mm/photo/PicMainActivity;II)V

    iget-object v0, p0, Lcom/konka/mm/photo/PicMainActivity$4;->this$0:Lcom/konka/mm/photo/PicMainActivity;

    invoke-static {v0, v3}, Lcom/konka/mm/photo/PicMainActivity;->access$5(Lcom/konka/mm/photo/PicMainActivity;I)V

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/konka/mm/photo/PicMainActivity$sortThread;

    iget-object v2, p0, Lcom/konka/mm/photo/PicMainActivity$4;->this$0:Lcom/konka/mm/photo/PicMainActivity;

    invoke-direct {v1, v2}, Lcom/konka/mm/photo/PicMainActivity$sortThread;-><init>(Lcom/konka/mm/photo/PicMainActivity;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0

    :sswitch_1
    iget-object v0, p0, Lcom/konka/mm/photo/PicMainActivity$4;->this$0:Lcom/konka/mm/photo/PicMainActivity;

    # invokes: Lcom/konka/mm/photo/PicMainActivity;->showProgressDialog(II)V
    invoke-static {v0, v1, v1}, Lcom/konka/mm/photo/PicMainActivity;->access$4(Lcom/konka/mm/photo/PicMainActivity;II)V

    iget-object v0, p0, Lcom/konka/mm/photo/PicMainActivity$4;->this$0:Lcom/konka/mm/photo/PicMainActivity;

    invoke-static {v0, v4}, Lcom/konka/mm/photo/PicMainActivity;->access$5(Lcom/konka/mm/photo/PicMainActivity;I)V

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/konka/mm/photo/PicMainActivity$sortThread;

    iget-object v2, p0, Lcom/konka/mm/photo/PicMainActivity$4;->this$0:Lcom/konka/mm/photo/PicMainActivity;

    invoke-direct {v1, v2}, Lcom/konka/mm/photo/PicMainActivity$sortThread;-><init>(Lcom/konka/mm/photo/PicMainActivity;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/konka/mm/photo/PicMainActivity$4;->this$0:Lcom/konka/mm/photo/PicMainActivity;

    # invokes: Lcom/konka/mm/photo/PicMainActivity;->showProgressDialog(II)V
    invoke-static {v0, v1, v1}, Lcom/konka/mm/photo/PicMainActivity;->access$4(Lcom/konka/mm/photo/PicMainActivity;II)V

    iget-object v0, p0, Lcom/konka/mm/photo/PicMainActivity$4;->this$0:Lcom/konka/mm/photo/PicMainActivity;

    invoke-static {v0, v5}, Lcom/konka/mm/photo/PicMainActivity;->access$5(Lcom/konka/mm/photo/PicMainActivity;I)V

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/konka/mm/photo/PicMainActivity$sortThread;

    iget-object v2, p0, Lcom/konka/mm/photo/PicMainActivity$4;->this$0:Lcom/konka/mm/photo/PicMainActivity;

    invoke-direct {v1, v2}, Lcom/konka/mm/photo/PicMainActivity$sortThread;-><init>(Lcom/konka/mm/photo/PicMainActivity;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/konka/mm/photo/PicMainActivity$4;->this$0:Lcom/konka/mm/photo/PicMainActivity;

    # invokes: Lcom/konka/mm/photo/PicMainActivity;->showProgressDialog(II)V
    invoke-static {v0, v1, v1}, Lcom/konka/mm/photo/PicMainActivity;->access$4(Lcom/konka/mm/photo/PicMainActivity;II)V

    iget-object v0, p0, Lcom/konka/mm/photo/PicMainActivity$4;->this$0:Lcom/konka/mm/photo/PicMainActivity;

    invoke-static {v0, v5}, Lcom/konka/mm/photo/PicMainActivity;->access$5(Lcom/konka/mm/photo/PicMainActivity;I)V

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/konka/mm/photo/PicMainActivity$sortThread;

    iget-object v2, p0, Lcom/konka/mm/photo/PicMainActivity$4;->this$0:Lcom/konka/mm/photo/PicMainActivity;

    invoke-direct {v1, v2}, Lcom/konka/mm/photo/PicMainActivity$sortThread;-><init>(Lcom/konka/mm/photo/PicMainActivity;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0

    :sswitch_4
    sput v3, Lcom/konka/mm/photo/PhotoDiskActivity;->List_Mode:I

    iget-object v0, p0, Lcom/konka/mm/photo/PicMainActivity$4;->this$0:Lcom/konka/mm/photo/PicMainActivity;

    invoke-virtual {v0}, Lcom/konka/mm/photo/PicMainActivity;->setGridAdapter()V

    iget-object v0, p0, Lcom/konka/mm/photo/PicMainActivity$4;->this$0:Lcom/konka/mm/photo/PicMainActivity;

    invoke-virtual {v0, v2}, Lcom/konka/mm/photo/PicMainActivity;->setBtnFocuseFlag(Z)V

    goto :goto_0

    :sswitch_5
    sput v4, Lcom/konka/mm/photo/PhotoDiskActivity;->List_Mode:I

    iget-object v0, p0, Lcom/konka/mm/photo/PicMainActivity$4;->this$0:Lcom/konka/mm/photo/PicMainActivity;

    invoke-virtual {v0}, Lcom/konka/mm/photo/PicMainActivity;->setGridAdapter()V

    iget-object v0, p0, Lcom/konka/mm/photo/PicMainActivity$4;->this$0:Lcom/konka/mm/photo/PicMainActivity;

    invoke-virtual {v0, v2}, Lcom/konka/mm/photo/PicMainActivity;->setBtnFocuseFlag(Z)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x7f0b00b4 -> :sswitch_4    # com.konka.mm.R.id.list_small_style
        0x7f0b00b5 -> :sswitch_5    # com.konka.mm.R.id.list_big_style
        0x7f0b00fe -> :sswitch_1    # com.konka.mm.R.id.rb_sort_by_filename
        0x7f0b00ff -> :sswitch_2    # com.konka.mm.R.id.rb_sort_by_filesize
        0x7f0b0100 -> :sswitch_3    # com.konka.mm.R.id.rb_sort_by_filetype
        0x7f0b0101 -> :sswitch_0    # com.konka.mm.R.id.rb_sort_by_default
    .end sparse-switch
.end method
