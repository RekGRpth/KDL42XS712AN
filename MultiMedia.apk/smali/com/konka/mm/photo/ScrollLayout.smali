.class public Lcom/konka/mm/photo/ScrollLayout;
.super Landroid/view/ViewGroup;
.source "ScrollLayout.java"


# static fields
.field private static final SNAP_VELOCITY:I = 0x258

.field private static final TAG:Ljava/lang/String; = "ScrollLayout"

.field private static final TOUCH_STATE_REST:I = 0x0

.field private static final TOUCH_STATE_SCROLLING:I = 0x1


# instance fields
.field private mCurScreen:I

.field private mDefaultScreen:I

.field private mLastMotionX:F

.field private mLastMotionY:F

.field public mScroller:Landroid/widget/Scroller;

.field private mTouchSlop:I

.field private mTouchState:I

.field private mVelocityTracker:Landroid/view/VelocityTracker;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    iput v0, p0, Lcom/konka/mm/photo/ScrollLayout;->mDefaultScreen:I

    iput v0, p0, Lcom/konka/mm/photo/ScrollLayout;->mTouchState:I

    new-instance v0, Landroid/widget/Scroller;

    invoke-direct {v0, p1}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/konka/mm/photo/ScrollLayout;->mScroller:Landroid/widget/Scroller;

    iget v0, p0, Lcom/konka/mm/photo/ScrollLayout;->mDefaultScreen:I

    iput v0, p0, Lcom/konka/mm/photo/ScrollLayout;->mCurScreen:I

    invoke-virtual {p0}, Lcom/konka/mm/photo/ScrollLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    iput v0, p0, Lcom/konka/mm/photo/ScrollLayout;->mTouchSlop:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/konka/mm/photo/ScrollLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput v0, p0, Lcom/konka/mm/photo/ScrollLayout;->mDefaultScreen:I

    iput v0, p0, Lcom/konka/mm/photo/ScrollLayout;->mTouchState:I

    new-instance v0, Landroid/widget/Scroller;

    invoke-direct {v0, p1}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/konka/mm/photo/ScrollLayout;->mScroller:Landroid/widget/Scroller;

    iget v0, p0, Lcom/konka/mm/photo/ScrollLayout;->mDefaultScreen:I

    iput v0, p0, Lcom/konka/mm/photo/ScrollLayout;->mCurScreen:I

    invoke-virtual {p0}, Lcom/konka/mm/photo/ScrollLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    iput v0, p0, Lcom/konka/mm/photo/ScrollLayout;->mTouchSlop:I

    return-void
.end method


# virtual methods
.method public computeScroll()V
    .locals 2

    iget-object v0, p0, Lcom/konka/mm/photo/ScrollLayout;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->computeScrollOffset()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/mm/photo/ScrollLayout;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->getCurrX()I

    move-result v0

    iget-object v1, p0, Lcom/konka/mm/photo/ScrollLayout;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v1}, Landroid/widget/Scroller;->getCurrY()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/konka/mm/photo/ScrollLayout;->scrollTo(II)V

    invoke-virtual {p0}, Lcom/konka/mm/photo/ScrollLayout;->postInvalidate()V

    :cond_0
    return-void
.end method

.method public getCurScreen()I
    .locals 1

    iget v0, p0, Lcom/konka/mm/photo/ScrollLayout;->mCurScreen:I

    return v0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 9
    .param p1    # Landroid/view/MotionEvent;

    const/4 v5, 0x0

    const/4 v6, 0x1

    const-string v4, "ScrollLayout"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "onInterceptTouchEvent-slop:"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v8, p0, Lcom/konka/mm/photo/ScrollLayout;->mTouchSlop:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v4, 0x2

    if-ne v0, v4, :cond_1

    iget v4, p0, Lcom/konka/mm/photo/ScrollLayout;->mTouchState:I

    if-eqz v4, :cond_1

    :cond_0
    :goto_0
    return v6

    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    packed-switch v0, :pswitch_data_0

    :cond_2
    :goto_1
    iget v4, p0, Lcom/konka/mm/photo/ScrollLayout;->mTouchState:I

    if-nez v4, :cond_0

    move v6, v5

    goto :goto_0

    :pswitch_0
    iget v4, p0, Lcom/konka/mm/photo/ScrollLayout;->mLastMotionX:F

    sub-float/2addr v4, v1

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    float-to-int v2, v4

    iget v4, p0, Lcom/konka/mm/photo/ScrollLayout;->mTouchSlop:I

    if-le v2, v4, :cond_2

    iput v6, p0, Lcom/konka/mm/photo/ScrollLayout;->mTouchState:I

    goto :goto_1

    :pswitch_1
    iput v1, p0, Lcom/konka/mm/photo/ScrollLayout;->mLastMotionX:F

    iput v3, p0, Lcom/konka/mm/photo/ScrollLayout;->mLastMotionY:F

    iget-object v4, p0, Lcom/konka/mm/photo/ScrollLayout;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v4}, Landroid/widget/Scroller;->isFinished()Z

    move-result v4

    if-eqz v4, :cond_3

    move v4, v5

    :goto_2
    iput v4, p0, Lcom/konka/mm/photo/ScrollLayout;->mTouchState:I

    goto :goto_1

    :cond_3
    move v4, v6

    goto :goto_2

    :pswitch_2
    iput v5, p0, Lcom/konka/mm/photo/ScrollLayout;->mTouchState:I

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method protected onLayout(ZIIII)V
    .locals 8
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    const-string v5, "ScrollLayout"

    const-string v6, "onLayout"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/konka/mm/photo/ScrollLayout;->getChildCount()I

    move-result v0

    const/4 v4, 0x0

    :goto_0
    if-lt v4, v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0, v4}, Lcom/konka/mm/photo/ScrollLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v5

    const/16 v6, 0x8

    if-eq v5, v6, :cond_1

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    const/4 v5, 0x0

    add-int v6, v1, v3

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v7

    invoke-virtual {v2, v1, v5, v6, v7}, Landroid/view/View;->layout(IIII)V

    add-int/2addr v1, v3

    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 8
    .param p1    # I
    .param p2    # I

    const/high16 v7, 0x40000000    # 2.0f

    const-string v5, "ScrollLayout"

    const-string v6, "onMeasure"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->onMeasure(II)V

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v4

    if-eq v4, v7, :cond_0

    new-instance v5, Ljava/lang/IllegalStateException;

    const-string v6, "ScrollLayout only canmCurScreen run at EXACTLY mode!"

    invoke-direct {v5, v6}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_0
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    if-eq v1, v7, :cond_1

    new-instance v5, Ljava/lang/IllegalStateException;

    const-string v6, "ScrollLayout only can run at EXACTLY mode!"

    invoke-direct {v5, v6}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_1
    invoke-virtual {p0}, Lcom/konka/mm/photo/ScrollLayout;->getChildCount()I

    move-result v0

    const/4 v2, 0x0

    :goto_0
    if-lt v2, v0, :cond_2

    iget v5, p0, Lcom/konka/mm/photo/ScrollLayout;->mCurScreen:I

    mul-int/2addr v5, v3

    const/4 v6, 0x0

    invoke-virtual {p0, v5, v6}, Lcom/konka/mm/photo/ScrollLayout;->scrollTo(II)V

    return-void

    :cond_2
    invoke-virtual {p0, v2}, Lcom/konka/mm/photo/ScrollLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, p1, p2}, Landroid/view/View;->measure(II)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    const/4 v0, 0x1

    return v0
.end method

.method public setCurScreen(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/konka/mm/photo/ScrollLayout;->mCurScreen:I

    return-void
.end method

.method public setToScreen(I)V
    .locals 2
    .param p1    # I

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/konka/mm/photo/ScrollLayout;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result p1

    iput p1, p0, Lcom/konka/mm/photo/ScrollLayout;->mCurScreen:I

    invoke-virtual {p0}, Lcom/konka/mm/photo/ScrollLayout;->getWidth()I

    move-result v0

    mul-int/2addr v0, p1

    invoke-virtual {p0, v0, v1}, Lcom/konka/mm/photo/ScrollLayout;->scrollTo(II)V

    return-void
.end method

.method public snapToDestination()V
    .locals 4

    invoke-virtual {p0}, Lcom/konka/mm/photo/ScrollLayout;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/konka/mm/photo/ScrollLayout;->getScrollX()I

    move-result v2

    div-int/lit8 v3, v1, 0x2

    add-int/2addr v2, v3

    div-int v0, v2, v1

    invoke-virtual {p0, v0}, Lcom/konka/mm/photo/ScrollLayout;->snapToScreen(I)V

    return-void
.end method

.method public snapToScreen(I)V
    .locals 6
    .param p1    # I

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/konka/mm/photo/ScrollLayout;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result p1

    invoke-virtual {p0}, Lcom/konka/mm/photo/ScrollLayout;->getScrollX()I

    move-result v0

    invoke-virtual {p0}, Lcom/konka/mm/photo/ScrollLayout;->getWidth()I

    move-result v1

    mul-int/2addr v1, p1

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/konka/mm/photo/ScrollLayout;->getWidth()I

    move-result v0

    mul-int/2addr v0, p1

    invoke-virtual {p0}, Lcom/konka/mm/photo/ScrollLayout;->getScrollX()I

    move-result v1

    sub-int v3, v0, v1

    iget-object v0, p0, Lcom/konka/mm/photo/ScrollLayout;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {p0}, Lcom/konka/mm/photo/ScrollLayout;->getScrollX()I

    move-result v1

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v4

    mul-int/lit8 v5, v4, 0x2

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/widget/Scroller;->startScroll(IIIII)V

    iput p1, p0, Lcom/konka/mm/photo/ScrollLayout;->mCurScreen:I

    invoke-virtual {p0}, Lcom/konka/mm/photo/ScrollLayout;->invalidate()V

    :cond_0
    return-void
.end method
