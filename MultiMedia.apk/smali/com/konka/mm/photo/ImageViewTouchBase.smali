.class public Lcom/konka/mm/photo/ImageViewTouchBase;
.super Landroid/widget/ImageView;
.source "ImageViewTouchBase.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/mm/photo/ImageViewTouchBase$Recycler;
    }
.end annotation


# static fields
.field static final SCALE_RATE:F = 1.5f

.field private static final TAG:Ljava/lang/String; = "ImageViewTouchBase"


# instance fields
.field private flag:I

.field protected mBaseMatrix:Landroid/graphics/Matrix;

.field protected final mBitmapDisplayed:Lcom/konka/mm/photo/RotateBitmap;

.field protected mDisplayMatrix:Landroid/graphics/Matrix;

.field protected mHandler:Landroid/os/Handler;

.field public mLastXTouchPos:I

.field public mLastYTouchPos:I

.field private final mMatrixValues:[F

.field private mOnLayoutRunnable:Ljava/lang/Runnable;

.field private mRecycler:Lcom/konka/mm/photo/ImageViewTouchBase$Recycler;

.field private mRotateCounter:I

.field protected mSuppMatrix:Landroid/graphics/Matrix;

.field mThisHeight:I

.field mThisWidth:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1    # Landroid/content/Context;

    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, -0x1

    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mBaseMatrix:Landroid/graphics/Matrix;

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mSuppMatrix:Landroid/graphics/Matrix;

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mDisplayMatrix:Landroid/graphics/Matrix;

    const/16 v0, 0x9

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mMatrixValues:[F

    new-instance v0, Lcom/konka/mm/photo/RotateBitmap;

    invoke-direct {v0, v3}, Lcom/konka/mm/photo/RotateBitmap;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mBitmapDisplayed:Lcom/konka/mm/photo/RotateBitmap;

    iput v1, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mThisWidth:I

    iput v1, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mThisHeight:I

    iput v2, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mRotateCounter:I

    iput v2, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->flag:I

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mHandler:Landroid/os/Handler;

    iput-object v3, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mOnLayoutRunnable:Ljava/lang/Runnable;

    invoke-direct {p0}, Lcom/konka/mm/photo/ImageViewTouchBase;->init()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, -0x1

    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mBaseMatrix:Landroid/graphics/Matrix;

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mSuppMatrix:Landroid/graphics/Matrix;

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mDisplayMatrix:Landroid/graphics/Matrix;

    const/16 v0, 0x9

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mMatrixValues:[F

    new-instance v0, Lcom/konka/mm/photo/RotateBitmap;

    invoke-direct {v0, v3}, Lcom/konka/mm/photo/RotateBitmap;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mBitmapDisplayed:Lcom/konka/mm/photo/RotateBitmap;

    iput v1, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mThisWidth:I

    iput v1, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mThisHeight:I

    iput v2, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mRotateCounter:I

    iput v2, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->flag:I

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mHandler:Landroid/os/Handler;

    iput-object v3, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mOnLayoutRunnable:Ljava/lang/Runnable;

    invoke-direct {p0}, Lcom/konka/mm/photo/ImageViewTouchBase;->init()V

    return-void
.end method

.method private getProperBaseMatrix(Lcom/konka/mm/photo/RotateBitmap;Landroid/graphics/Matrix;)V
    .locals 10
    .param p1    # Lcom/konka/mm/photo/RotateBitmap;
    .param p2    # Landroid/graphics/Matrix;

    const/high16 v8, 0x40400000    # 3.0f

    const/high16 v9, 0x40000000    # 2.0f

    invoke-virtual {p0}, Lcom/konka/mm/photo/ImageViewTouchBase;->getWidth()I

    move-result v7

    int-to-float v4, v7

    invoke-virtual {p0}, Lcom/konka/mm/photo/ImageViewTouchBase;->getHeight()I

    move-result v7

    int-to-float v3, v7

    invoke-virtual {p1}, Lcom/konka/mm/photo/RotateBitmap;->getWidth()I

    move-result v7

    int-to-float v5, v7

    invoke-virtual {p1}, Lcom/konka/mm/photo/RotateBitmap;->getHeight()I

    move-result v7

    int-to-float v0, v7

    invoke-virtual {p2}, Landroid/graphics/Matrix;->reset()V

    div-float v7, v4, v5

    invoke-static {v7, v8}, Ljava/lang/Math;->min(FF)F

    move-result v6

    div-float v7, v3, v0

    invoke-static {v7, v8}, Ljava/lang/Math;->min(FF)F

    move-result v1

    invoke-static {v6, v1}, Ljava/lang/Math;->min(FF)F

    move-result v2

    invoke-virtual {p1}, Lcom/konka/mm/photo/RotateBitmap;->getRotateMatrix()Landroid/graphics/Matrix;

    move-result-object v7

    invoke-virtual {p2, v7}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    invoke-virtual {p2, v2, v2}, Landroid/graphics/Matrix;->postScale(FF)Z

    mul-float v7, v5, v2

    sub-float v7, v4, v7

    div-float/2addr v7, v9

    mul-float v8, v0, v2

    sub-float v8, v3, v8

    div-float/2addr v8, v9

    invoke-virtual {p2, v7, v8}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    return-void
.end method

.method private init()V
    .locals 1

    sget-object v0, Landroid/widget/ImageView$ScaleType;->MATRIX:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {p0, v0}, Lcom/konka/mm/photo/ImageViewTouchBase;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    return-void
.end method

.method private prepareDisplayMatrix()V
    .locals 17

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/konka/mm/photo/ImageViewTouchBase;->mBitmapDisplayed:Lcom/konka/mm/photo/RotateBitmap;

    invoke-virtual {v13}, Lcom/konka/mm/photo/RotateBitmap;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v13

    invoke-virtual {v13}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v13

    int-to-float v6, v13

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/konka/mm/photo/ImageViewTouchBase;->mBitmapDisplayed:Lcom/konka/mm/photo/RotateBitmap;

    invoke-virtual {v13}, Lcom/konka/mm/photo/RotateBitmap;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v13

    invoke-virtual {v13}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v13

    int-to-float v5, v13

    invoke-virtual/range {p0 .. p0}, Lcom/konka/mm/photo/ImageViewTouchBase;->getWidth()I

    move-result v13

    int-to-float v10, v13

    invoke-virtual/range {p0 .. p0}, Lcom/konka/mm/photo/ImageViewTouchBase;->getHeight()I

    move-result v13

    int-to-float v9, v13

    move-object/from16 v0, p0

    iget v13, v0, Lcom/konka/mm/photo/ImageViewTouchBase;->mRotateCounter:I

    invoke-static {v13}, Ljava/lang/Math;->abs(I)I

    move-result v13

    rem-int/lit8 v13, v13, 0x2

    const/4 v14, 0x1

    if-ne v13, v14, :cond_2

    div-float v13, v10, v5

    const/high16 v14, 0x40400000    # 3.0f

    invoke-static {v13, v14}, Ljava/lang/Math;->min(FF)F

    move-result v12

    div-float v13, v9, v6

    const/high16 v14, 0x40400000    # 3.0f

    invoke-static {v13, v14}, Ljava/lang/Math;->min(FF)F

    move-result v4

    :goto_0
    invoke-static {v12, v4}, Ljava/lang/Math;->min(FF)F

    move-result v8

    const-string v13, "ImageViewBase"

    new-instance v14, Ljava/lang/StringBuilder;

    const-string v15, "counter:"

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget v15, v0, Lcom/konka/mm/photo/ImageViewTouchBase;->mRotateCounter:I

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ",widthScale:"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v12}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ",heightScale:"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/konka/mm/photo/ImageViewTouchBase;->mBaseMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v13}, Landroid/graphics/Matrix;->reset()V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/konka/mm/photo/ImageViewTouchBase;->mBaseMatrix:Landroid/graphics/Matrix;

    new-instance v14, Landroid/graphics/Matrix;

    invoke-direct {v14}, Landroid/graphics/Matrix;-><init>()V

    invoke-virtual {v13, v14}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/konka/mm/photo/ImageViewTouchBase;->mBaseMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v13, v8, v8}, Landroid/graphics/Matrix;->postScale(FF)Z

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/konka/mm/photo/ImageViewTouchBase;->mBaseMatrix:Landroid/graphics/Matrix;

    mul-float v14, v6, v8

    sub-float v14, v10, v14

    const/high16 v15, 0x40000000    # 2.0f

    div-float/2addr v14, v15

    mul-float v15, v5, v8

    sub-float v15, v9, v15

    const/high16 v16, 0x40000000    # 2.0f

    div-float v15, v15, v16

    invoke-virtual {v13, v14, v15}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/konka/mm/photo/ImageViewTouchBase;->mDisplayMatrix:Landroid/graphics/Matrix;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/konka/mm/photo/ImageViewTouchBase;->mBaseMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v13, v14}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/konka/mm/photo/ImageViewTouchBase;->mDisplayMatrix:Landroid/graphics/Matrix;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/konka/mm/photo/ImageViewTouchBase;->mSuppMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v13, v14}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    new-instance v7, Landroid/graphics/RectF;

    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-direct {v7, v13, v14, v6, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    const-string v13, "ImageViewBase"

    new-instance v14, Ljava/lang/StringBuilder;

    const-string v15, "lf:"

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v15, v7, Landroid/graphics/RectF;->left:F

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ",tp:"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget v15, v7, Landroid/graphics/RectF;->top:F

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ",rg:"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget v15, v7, Landroid/graphics/RectF;->right:F

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ",bm:"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget v15, v7, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/konka/mm/photo/ImageViewTouchBase;->mDisplayMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v13, v7}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    invoke-virtual {v7}, Landroid/graphics/RectF;->height()F

    move-result v3

    invoke-virtual {v7}, Landroid/graphics/RectF;->width()F

    move-result v11

    const/4 v1, 0x0

    const/4 v2, 0x0

    const-string v13, "ImageViewBase"

    new-instance v14, Ljava/lang/StringBuilder;

    const-string v15, "width:"

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v14, v11}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ",height:"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v13, "ImageViewBase"

    new-instance v14, Ljava/lang/StringBuilder;

    const-string v15, "rglf:"

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v15, v7, Landroid/graphics/RectF;->left:F

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ",tp:"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget v15, v7, Landroid/graphics/RectF;->top:F

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ",rg:"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget v15, v7, Landroid/graphics/RectF;->right:F

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ",bm:"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget v15, v7, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    cmpg-float v13, v3, v9

    if-gez v13, :cond_3

    sub-float v13, v9, v3

    const/high16 v14, 0x40000000    # 2.0f

    div-float/2addr v13, v14

    iget v14, v7, Landroid/graphics/RectF;->top:F

    sub-float v2, v13, v14

    :cond_0
    :goto_1
    cmpg-float v13, v11, v10

    if-gez v13, :cond_5

    sub-float v13, v10, v11

    const/high16 v14, 0x40000000    # 2.0f

    div-float/2addr v13, v14

    iget v14, v7, Landroid/graphics/RectF;->left:F

    sub-float v1, v13, v14

    :cond_1
    :goto_2
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/konka/mm/photo/ImageViewTouchBase;->mDisplayMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v13, v1, v2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    return-void

    :cond_2
    div-float v13, v10, v6

    const/high16 v14, 0x40400000    # 3.0f

    invoke-static {v13, v14}, Ljava/lang/Math;->min(FF)F

    move-result v12

    div-float v13, v9, v5

    const/high16 v14, 0x40400000    # 3.0f

    invoke-static {v13, v14}, Ljava/lang/Math;->min(FF)F

    move-result v4

    goto/16 :goto_0

    :cond_3
    iget v13, v7, Landroid/graphics/RectF;->top:F

    const/4 v14, 0x0

    cmpl-float v13, v13, v14

    if-lez v13, :cond_4

    iget v13, v7, Landroid/graphics/RectF;->top:F

    neg-float v2, v13

    goto :goto_1

    :cond_4
    iget v13, v7, Landroid/graphics/RectF;->bottom:F

    cmpg-float v13, v13, v9

    if-gez v13, :cond_0

    iget v13, v7, Landroid/graphics/RectF;->bottom:F

    sub-float v2, v9, v13

    goto :goto_1

    :cond_5
    iget v13, v7, Landroid/graphics/RectF;->left:F

    const/4 v14, 0x0

    cmpl-float v13, v13, v14

    if-lez v13, :cond_6

    iget v13, v7, Landroid/graphics/RectF;->left:F

    neg-float v1, v13

    goto :goto_2

    :cond_6
    iget v13, v7, Landroid/graphics/RectF;->right:F

    cmpg-float v13, v13, v10

    if-gez v13, :cond_1

    iget v13, v7, Landroid/graphics/RectF;->right:F

    sub-float v1, v10, v13

    goto :goto_2
.end method

.method private set2Original(I)V
    .locals 10
    .param p1    # I

    const/high16 v5, 0x40000000    # 2.0f

    const/high16 v9, 0x3fc00000    # 1.5f

    const/high16 v8, 0x3f800000    # 1.0f

    const v7, 0x3f2aaaab

    invoke-virtual {p0}, Lcom/konka/mm/photo/ImageViewTouchBase;->getWidth()I

    move-result v4

    int-to-float v4, v4

    div-float v0, v4, v5

    invoke-virtual {p0}, Lcom/konka/mm/photo/ImageViewTouchBase;->getHeight()I

    move-result v4

    int-to-float v4, v4

    div-float v1, v4, v5

    iget-object v4, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mSuppMatrix:Landroid/graphics/Matrix;

    const/4 v5, 0x0

    invoke-virtual {p0, v4, v5}, Lcom/konka/mm/photo/ImageViewTouchBase;->getValue(Landroid/graphics/Matrix;I)F

    move-result v3

    const-string v4, "ImageViewBase"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "set2Original oldScaleX:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ",zoomTimes:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v4, 0x0

    cmpl-float v4, v3, v4

    if-lez v4, :cond_2

    iget-object v4, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mSuppMatrix:Landroid/graphics/Matrix;

    div-float v5, v8, v3

    div-float v6, v8, v3

    invoke-virtual {v4, v5, v6, v0, v1}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    :cond_0
    invoke-direct {p0}, Lcom/konka/mm/photo/ImageViewTouchBase;->prepareDisplayMatrix()V

    iget-object v4, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mDisplayMatrix:Landroid/graphics/Matrix;

    invoke-virtual {p0, v4}, Lcom/konka/mm/photo/ImageViewTouchBase;->setImageMatrix(Landroid/graphics/Matrix;)V

    :cond_1
    return-void

    :cond_2
    if-lez p1, :cond_3

    const/4 v2, 0x0

    :goto_0
    if-ge v2, p1, :cond_0

    iget-object v4, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mSuppMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v4, v7, v7, v0, v1}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_3
    if-gez p1, :cond_1

    const/4 v2, 0x0

    :goto_1
    neg-int v4, p1

    if-ge v2, v4, :cond_0

    iget-object v4, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mSuppMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v4, v9, v9, v0, v1}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method private setImageBitmap(Landroid/graphics/Bitmap;I)V
    .locals 3
    .param p1    # Landroid/graphics/Bitmap;
    .param p2    # I

    invoke-super {p0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    invoke-virtual {p0}, Lcom/konka/mm/photo/ImageViewTouchBase;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/Drawable;->setDither(Z)V

    :cond_0
    iget-object v2, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mBitmapDisplayed:Lcom/konka/mm/photo/RotateBitmap;

    invoke-virtual {v2}, Lcom/konka/mm/photo/RotateBitmap;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mBitmapDisplayed:Lcom/konka/mm/photo/RotateBitmap;

    invoke-virtual {v2, p1}, Lcom/konka/mm/photo/RotateBitmap;->setBitmap(Landroid/graphics/Bitmap;)V

    iget-object v2, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mBitmapDisplayed:Lcom/konka/mm/photo/RotateBitmap;

    invoke-virtual {v2, p2}, Lcom/konka/mm/photo/RotateBitmap;->setRotation(I)V

    if-eqz v1, :cond_1

    if-eq v1, p1, :cond_1

    iget-object v2, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mRecycler:Lcom/konka/mm/photo/ImageViewTouchBase$Recycler;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mRecycler:Lcom/konka/mm/photo/ImageViewTouchBase$Recycler;

    invoke-interface {v2, v1}, Lcom/konka/mm/photo/ImageViewTouchBase$Recycler;->recycle(Landroid/graphics/Bitmap;)V

    :cond_1
    return-void
.end method


# virtual methods
.method protected center(ZZ)V
    .locals 12
    .param p1    # Z
    .param p2    # Z

    const/high16 v11, 0x40000000    # 2.0f

    const/4 v10, 0x0

    iget-object v8, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mBitmapDisplayed:Lcom/konka/mm/photo/RotateBitmap;

    invoke-virtual {v8}, Lcom/konka/mm/photo/RotateBitmap;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v8

    if-nez v8, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/konka/mm/photo/ImageViewTouchBase;->getImageViewMatrix()Landroid/graphics/Matrix;

    move-result-object v3

    new-instance v4, Landroid/graphics/RectF;

    iget-object v8, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mBitmapDisplayed:Lcom/konka/mm/photo/RotateBitmap;

    invoke-virtual {v8}, Lcom/konka/mm/photo/RotateBitmap;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v8

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    int-to-float v8, v8

    iget-object v9, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mBitmapDisplayed:Lcom/konka/mm/photo/RotateBitmap;

    invoke-virtual {v9}, Lcom/konka/mm/photo/RotateBitmap;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v9

    invoke-virtual {v9}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    int-to-float v9, v9

    invoke-direct {v4, v10, v10, v8, v9}, Landroid/graphics/RectF;-><init>(FFFF)V

    invoke-virtual {v3, v4}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v2

    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v7

    const/4 v0, 0x0

    const/4 v1, 0x0

    if-eqz p2, :cond_1

    invoke-virtual {p0}, Lcom/konka/mm/photo/ImageViewTouchBase;->getHeight()I

    move-result v5

    int-to-float v8, v5

    cmpg-float v8, v2, v8

    if-gez v8, :cond_3

    int-to-float v8, v5

    sub-float/2addr v8, v2

    div-float/2addr v8, v11

    iget v9, v4, Landroid/graphics/RectF;->top:F

    sub-float v1, v8, v9

    :cond_1
    :goto_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Lcom/konka/mm/photo/ImageViewTouchBase;->getWidth()I

    move-result v6

    int-to-float v8, v6

    cmpg-float v8, v7, v8

    if-gez v8, :cond_5

    int-to-float v8, v6

    sub-float/2addr v8, v7

    div-float/2addr v8, v11

    iget v9, v4, Landroid/graphics/RectF;->left:F

    sub-float v0, v8, v9

    :cond_2
    :goto_2
    invoke-virtual {p0, v0, v1}, Lcom/konka/mm/photo/ImageViewTouchBase;->postTranslate(FF)V

    invoke-virtual {p0}, Lcom/konka/mm/photo/ImageViewTouchBase;->getImageViewMatrix()Landroid/graphics/Matrix;

    move-result-object v8

    invoke-virtual {p0, v8}, Lcom/konka/mm/photo/ImageViewTouchBase;->setImageMatrix(Landroid/graphics/Matrix;)V

    goto :goto_0

    :cond_3
    iget v8, v4, Landroid/graphics/RectF;->top:F

    cmpl-float v8, v8, v10

    if-lez v8, :cond_4

    iget v8, v4, Landroid/graphics/RectF;->top:F

    neg-float v1, v8

    goto :goto_1

    :cond_4
    iget v8, v4, Landroid/graphics/RectF;->bottom:F

    int-to-float v9, v5

    cmpg-float v8, v8, v9

    if-gez v8, :cond_1

    invoke-virtual {p0}, Lcom/konka/mm/photo/ImageViewTouchBase;->getHeight()I

    move-result v8

    int-to-float v8, v8

    iget v9, v4, Landroid/graphics/RectF;->bottom:F

    sub-float v1, v8, v9

    goto :goto_1

    :cond_5
    iget v8, v4, Landroid/graphics/RectF;->left:F

    cmpl-float v8, v8, v10

    if-lez v8, :cond_6

    iget v8, v4, Landroid/graphics/RectF;->left:F

    neg-float v0, v8

    goto :goto_2

    :cond_6
    iget v8, v4, Landroid/graphics/RectF;->right:F

    int-to-float v9, v6

    cmpg-float v8, v8, v9

    if-gez v8, :cond_2

    int-to-float v8, v6

    iget v9, v4, Landroid/graphics/RectF;->right:F

    sub-float v0, v8, v9

    goto :goto_2
.end method

.method public clear()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/konka/mm/photo/ImageViewTouchBase;->setImageBitmapResetBase(Landroid/graphics/Bitmap;Z)V

    return-void
.end method

.method protected getImageViewMatrix()Landroid/graphics/Matrix;
    .locals 2

    iget-object v0, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mDisplayMatrix:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mBaseMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    iget-object v0, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mDisplayMatrix:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mSuppMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    iget-object v0, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mDisplayMatrix:Landroid/graphics/Matrix;

    return-object v0
.end method

.method public getScale()F
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mSuppMatrix:Landroid/graphics/Matrix;

    invoke-virtual {p0, v0}, Lcom/konka/mm/photo/ImageViewTouchBase;->getScale(Landroid/graphics/Matrix;)F

    move-result v0

    return v0
.end method

.method protected getScale(Landroid/graphics/Matrix;)F
    .locals 1
    .param p1    # Landroid/graphics/Matrix;

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/konka/mm/photo/ImageViewTouchBase;->getValue(Landroid/graphics/Matrix;I)F

    move-result v0

    return v0
.end method

.method protected getValue(Landroid/graphics/Matrix;I)F
    .locals 1
    .param p1    # Landroid/graphics/Matrix;
    .param p2    # I

    iget-object v0, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mMatrixValues:[F

    invoke-virtual {p1, v0}, Landroid/graphics/Matrix;->getValues([F)V

    iget-object v0, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mMatrixValues:[F

    aget v0, v0, p2

    return v0
.end method

.method protected maxZoom()F
    .locals 5

    iget-object v3, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mBitmapDisplayed:Lcom/konka/mm/photo/RotateBitmap;

    invoke-virtual {v3}, Lcom/konka/mm/photo/RotateBitmap;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v3

    if-nez v3, :cond_0

    const/high16 v2, 0x3f800000    # 1.0f

    :goto_0
    return v2

    :cond_0
    iget-object v3, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mBitmapDisplayed:Lcom/konka/mm/photo/RotateBitmap;

    invoke-virtual {v3}, Lcom/konka/mm/photo/RotateBitmap;->getWidth()I

    move-result v3

    int-to-float v3, v3

    iget v4, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mThisWidth:I

    int-to-float v4, v4

    div-float v1, v3, v4

    iget-object v3, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mBitmapDisplayed:Lcom/konka/mm/photo/RotateBitmap;

    invoke-virtual {v3}, Lcom/konka/mm/photo/RotateBitmap;->getHeight()I

    move-result v3

    int-to-float v3, v3

    iget v4, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mThisHeight:I

    int-to-float v4, v4

    div-float v0, v3, v4

    invoke-static {v1, v0}, Ljava/lang/Math;->max(FF)F

    move-result v3

    const/high16 v4, 0x40800000    # 4.0f

    mul-float v2, v3, v4

    goto :goto_0
.end method

.method protected minZoom()F
    .locals 5

    iget-object v3, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mBitmapDisplayed:Lcom/konka/mm/photo/RotateBitmap;

    invoke-virtual {v3}, Lcom/konka/mm/photo/RotateBitmap;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v3

    if-nez v3, :cond_0

    const/high16 v2, 0x3f800000    # 1.0f

    :goto_0
    return v2

    :cond_0
    iget-object v3, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mBitmapDisplayed:Lcom/konka/mm/photo/RotateBitmap;

    invoke-virtual {v3}, Lcom/konka/mm/photo/RotateBitmap;->getWidth()I

    move-result v3

    int-to-float v3, v3

    iget v4, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mThisWidth:I

    int-to-float v4, v4

    div-float v1, v3, v4

    iget-object v3, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mBitmapDisplayed:Lcom/konka/mm/photo/RotateBitmap;

    invoke-virtual {v3}, Lcom/konka/mm/photo/RotateBitmap;->getHeight()I

    move-result v3

    int-to-float v3, v3

    iget v4, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mThisHeight:I

    int-to-float v4, v4

    div-float v0, v3, v4

    invoke-static {v1, v0}, Ljava/lang/Math;->max(FF)F

    move-result v3

    const/high16 v4, 0x40800000    # 4.0f

    div-float v2, v3, v4

    goto :goto_0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p2}, Landroid/view/KeyEvent;->startTracking()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/ImageView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    invoke-virtual {p2}, Landroid/view/KeyEvent;->isTracking()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Landroid/view/KeyEvent;->isCanceled()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/konka/mm/photo/ImageViewTouchBase;->getScale()F

    move-result v0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    invoke-virtual {p0, v1}, Lcom/konka/mm/photo/ImageViewTouchBase;->zoomTo(F)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/ImageView;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 3
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    invoke-super/range {p0 .. p5}, Landroid/widget/ImageView;->onLayout(ZIIII)V

    sub-int v1, p4, p2

    iput v1, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mThisWidth:I

    sub-int v1, p5, p3

    iput v1, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mThisHeight:I

    iget-object v0, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mOnLayoutRunnable:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mOnLayoutRunnable:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    :cond_0
    iget-object v1, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mBitmapDisplayed:Lcom/konka/mm/photo/RotateBitmap;

    invoke-virtual {v1}, Lcom/konka/mm/photo/RotateBitmap;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mBitmapDisplayed:Lcom/konka/mm/photo/RotateBitmap;

    iget-object v2, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mBaseMatrix:Landroid/graphics/Matrix;

    invoke-direct {p0, v1, v2}, Lcom/konka/mm/photo/ImageViewTouchBase;->getProperBaseMatrix(Lcom/konka/mm/photo/RotateBitmap;Landroid/graphics/Matrix;)V

    invoke-virtual {p0}, Lcom/konka/mm/photo/ImageViewTouchBase;->getImageViewMatrix()Landroid/graphics/Matrix;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/konka/mm/photo/ImageViewTouchBase;->setImageMatrix(Landroid/graphics/Matrix;)V

    :cond_1
    return-void
.end method

.method protected panBy(FF)V
    .locals 1
    .param p1    # F
    .param p2    # F

    invoke-virtual {p0, p1, p2}, Lcom/konka/mm/photo/ImageViewTouchBase;->postTranslate(FF)V

    invoke-virtual {p0}, Lcom/konka/mm/photo/ImageViewTouchBase;->getImageViewMatrix()Landroid/graphics/Matrix;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/konka/mm/photo/ImageViewTouchBase;->setImageMatrix(Landroid/graphics/Matrix;)V

    return-void
.end method

.method protected postTranslate(FF)V
    .locals 1
    .param p1    # F
    .param p2    # F

    iget-object v0, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mSuppMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    return-void
.end method

.method public rotateImage(F)V
    .locals 7
    .param p1    # F

    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v5, 0x1

    iget-object v4, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mBitmapDisplayed:Lcom/konka/mm/photo/RotateBitmap;

    invoke-virtual {v4}, Lcom/konka/mm/photo/RotateBitmap;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v4

    if-nez v4, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v4, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mBitmapDisplayed:Lcom/konka/mm/photo/RotateBitmap;

    invoke-virtual {v4}, Lcom/konka/mm/photo/RotateBitmap;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    int-to-float v0, v4

    iget-object v4, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mBitmapDisplayed:Lcom/konka/mm/photo/RotateBitmap;

    invoke-virtual {v4}, Lcom/konka/mm/photo/RotateBitmap;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    int-to-float v3, v4

    cmpg-float v4, v0, v3

    if-gez v4, :cond_1

    iget v4, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->flag:I

    if-nez v4, :cond_2

    div-float v1, v0, v3

    iget-object v4, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mSuppMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v4, v1, v6}, Landroid/graphics/Matrix;->postScale(FF)Z

    iput v5, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->flag:I

    :cond_1
    :goto_1
    iget-object v4, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mSuppMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v4, p1}, Landroid/graphics/Matrix;->postRotate(F)Z

    invoke-virtual {p0}, Lcom/konka/mm/photo/ImageViewTouchBase;->getImageViewMatrix()Landroid/graphics/Matrix;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/konka/mm/photo/ImageViewTouchBase;->setImageMatrix(Landroid/graphics/Matrix;)V

    invoke-virtual {p0, v5, v5}, Lcom/konka/mm/photo/ImageViewTouchBase;->center(ZZ)V

    goto :goto_0

    :cond_2
    iget v4, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->flag:I

    if-ne v4, v5, :cond_1

    div-float v2, v3, v0

    iget-object v4, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mSuppMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v4, v6, v2}, Landroid/graphics/Matrix;->postScale(FF)Z

    const/4 v4, 0x0

    iput v4, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->flag:I

    goto :goto_1
.end method

.method public rotateImageLeft(I)V
    .locals 3
    .param p1    # I

    iget-object v0, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mBitmapDisplayed:Lcom/konka/mm/photo/RotateBitmap;

    invoke-virtual {v0}, Lcom/konka/mm/photo/RotateBitmap;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, p1}, Lcom/konka/mm/photo/ImageViewTouchBase;->set2Original(I)V

    iget v0, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mRotateCounter:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mRotateCounter:I

    iget-object v0, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mSuppMatrix:Landroid/graphics/Matrix;

    const/high16 v1, -0x3d4c0000    # -90.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->postRotate(F)Z

    invoke-direct {p0}, Lcom/konka/mm/photo/ImageViewTouchBase;->prepareDisplayMatrix()V

    const-string v0, "ImageViewBase"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "af rotateImageLeft oldScaleX:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/konka/mm/photo/ImageViewTouchBase;->getScale()F

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mDisplayMatrix:Landroid/graphics/Matrix;

    invoke-virtual {p0, v0}, Lcom/konka/mm/photo/ImageViewTouchBase;->setImageMatrix(Landroid/graphics/Matrix;)V

    goto :goto_0
.end method

.method public rotateImageRight(I)V
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mBitmapDisplayed:Lcom/konka/mm/photo/RotateBitmap;

    invoke-virtual {v0}, Lcom/konka/mm/photo/RotateBitmap;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, p1}, Lcom/konka/mm/photo/ImageViewTouchBase;->set2Original(I)V

    iget v0, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mRotateCounter:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mRotateCounter:I

    iget-object v0, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mSuppMatrix:Landroid/graphics/Matrix;

    const/high16 v1, 0x42b40000    # 90.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->postRotate(F)Z

    invoke-direct {p0}, Lcom/konka/mm/photo/ImageViewTouchBase;->prepareDisplayMatrix()V

    iget-object v0, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mDisplayMatrix:Landroid/graphics/Matrix;

    invoke-virtual {p0, v0}, Lcom/konka/mm/photo/ImageViewTouchBase;->setImageMatrix(Landroid/graphics/Matrix;)V

    goto :goto_0
.end method

.method public setImageBitmap(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1    # Landroid/graphics/Bitmap;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/konka/mm/photo/ImageViewTouchBase;->setImageBitmap(Landroid/graphics/Bitmap;I)V

    return-void
.end method

.method public setImageBitmapResetBase(Landroid/graphics/Bitmap;Z)V
    .locals 1
    .param p1    # Landroid/graphics/Bitmap;
    .param p2    # Z

    new-instance v0, Lcom/konka/mm/photo/RotateBitmap;

    invoke-direct {v0, p1}, Lcom/konka/mm/photo/RotateBitmap;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {p0, v0, p2}, Lcom/konka/mm/photo/ImageViewTouchBase;->setImageRotateBitmapResetBase(Lcom/konka/mm/photo/RotateBitmap;Z)V

    return-void
.end method

.method public setImageRotateBitmapResetBase(Lcom/konka/mm/photo/RotateBitmap;Z)V
    .locals 3
    .param p1    # Lcom/konka/mm/photo/RotateBitmap;
    .param p2    # Z

    invoke-virtual {p0}, Lcom/konka/mm/photo/ImageViewTouchBase;->getWidth()I

    move-result v0

    if-gtz v0, :cond_0

    new-instance v1, Lcom/konka/mm/photo/ImageViewTouchBase$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/konka/mm/photo/ImageViewTouchBase$1;-><init>(Lcom/konka/mm/photo/ImageViewTouchBase;Lcom/konka/mm/photo/RotateBitmap;Z)V

    iput-object v1, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mOnLayoutRunnable:Ljava/lang/Runnable;

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/konka/mm/photo/RotateBitmap;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mBaseMatrix:Landroid/graphics/Matrix;

    invoke-direct {p0, p1, v1}, Lcom/konka/mm/photo/ImageViewTouchBase;->getProperBaseMatrix(Lcom/konka/mm/photo/RotateBitmap;Landroid/graphics/Matrix;)V

    invoke-virtual {p1}, Lcom/konka/mm/photo/RotateBitmap;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {p1}, Lcom/konka/mm/photo/RotateBitmap;->getRotation()I

    move-result v2

    invoke-direct {p0, v1, v2}, Lcom/konka/mm/photo/ImageViewTouchBase;->setImageBitmap(Landroid/graphics/Bitmap;I)V

    :goto_1
    if-eqz p2, :cond_1

    iget-object v1, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mSuppMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v1}, Landroid/graphics/Matrix;->reset()V

    :cond_1
    invoke-virtual {p0}, Lcom/konka/mm/photo/ImageViewTouchBase;->getImageViewMatrix()Landroid/graphics/Matrix;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/konka/mm/photo/ImageViewTouchBase;->setImageMatrix(Landroid/graphics/Matrix;)V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mBaseMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v1}, Landroid/graphics/Matrix;->reset()V

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/konka/mm/photo/ImageViewTouchBase;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_1
.end method

.method public setImageRotateBitmapResetBase(Lcom/konka/mm/photo/RotateBitmap;ZLcom/konka/mm/photo/AutoShowPicActivity;)V
    .locals 3
    .param p1    # Lcom/konka/mm/photo/RotateBitmap;
    .param p2    # Z
    .param p3    # Lcom/konka/mm/photo/AutoShowPicActivity;

    invoke-virtual {p0}, Lcom/konka/mm/photo/ImageViewTouchBase;->getWidth()I

    move-result v0

    if-gtz v0, :cond_0

    new-instance v1, Lcom/konka/mm/photo/ImageViewTouchBase$2;

    invoke-direct {v1, p0, p1, p2}, Lcom/konka/mm/photo/ImageViewTouchBase$2;-><init>(Lcom/konka/mm/photo/ImageViewTouchBase;Lcom/konka/mm/photo/RotateBitmap;Z)V

    iput-object v1, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mOnLayoutRunnable:Ljava/lang/Runnable;

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/konka/mm/photo/RotateBitmap;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mBaseMatrix:Landroid/graphics/Matrix;

    invoke-direct {p0, p1, v1}, Lcom/konka/mm/photo/ImageViewTouchBase;->getProperBaseMatrix(Lcom/konka/mm/photo/RotateBitmap;Landroid/graphics/Matrix;)V

    invoke-virtual {p1}, Lcom/konka/mm/photo/RotateBitmap;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {p1}, Lcom/konka/mm/photo/RotateBitmap;->getRotation()I

    move-result v2

    invoke-direct {p0, v1, v2}, Lcom/konka/mm/photo/ImageViewTouchBase;->setImageBitmap(Landroid/graphics/Bitmap;I)V

    if-eqz p3, :cond_1

    invoke-virtual {p3}, Lcom/konka/mm/photo/AutoShowPicActivity;->setDefaultPhoto()Landroid/graphics/Bitmap;

    :cond_1
    :goto_1
    if-eqz p2, :cond_2

    iget-object v1, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mSuppMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v1}, Landroid/graphics/Matrix;->reset()V

    :cond_2
    invoke-virtual {p0}, Lcom/konka/mm/photo/ImageViewTouchBase;->getImageViewMatrix()Landroid/graphics/Matrix;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/konka/mm/photo/ImageViewTouchBase;->setImageMatrix(Landroid/graphics/Matrix;)V

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mBaseMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v1}, Landroid/graphics/Matrix;->reset()V

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/konka/mm/photo/ImageViewTouchBase;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_1
.end method

.method public setImageRotateBitmapResetBase1(Lcom/konka/mm/photo/RotateBitmap;ZLcom/konka/mm/photo/GalleryShowPicActivity;)V
    .locals 3
    .param p1    # Lcom/konka/mm/photo/RotateBitmap;
    .param p2    # Z
    .param p3    # Lcom/konka/mm/photo/GalleryShowPicActivity;

    invoke-virtual {p0}, Lcom/konka/mm/photo/ImageViewTouchBase;->getWidth()I

    move-result v0

    if-gtz v0, :cond_0

    new-instance v1, Lcom/konka/mm/photo/ImageViewTouchBase$3;

    invoke-direct {v1, p0, p1, p2}, Lcom/konka/mm/photo/ImageViewTouchBase$3;-><init>(Lcom/konka/mm/photo/ImageViewTouchBase;Lcom/konka/mm/photo/RotateBitmap;Z)V

    iput-object v1, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mOnLayoutRunnable:Ljava/lang/Runnable;

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/konka/mm/photo/RotateBitmap;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mBaseMatrix:Landroid/graphics/Matrix;

    invoke-direct {p0, p1, v1}, Lcom/konka/mm/photo/ImageViewTouchBase;->getProperBaseMatrix(Lcom/konka/mm/photo/RotateBitmap;Landroid/graphics/Matrix;)V

    invoke-virtual {p1}, Lcom/konka/mm/photo/RotateBitmap;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {p1}, Lcom/konka/mm/photo/RotateBitmap;->getRotation()I

    move-result v2

    invoke-direct {p0, v1, v2}, Lcom/konka/mm/photo/ImageViewTouchBase;->setImageBitmap(Landroid/graphics/Bitmap;I)V

    if-eqz p3, :cond_1

    invoke-virtual {p3}, Lcom/konka/mm/photo/GalleryShowPicActivity;->setDefaultPhoto()Landroid/graphics/Bitmap;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Lcom/konka/mm/photo/RotateBitmap;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getByteCount()I

    invoke-virtual {p3}, Lcom/konka/mm/photo/GalleryShowPicActivity;->setDefaultPhoto()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getByteCount()I

    :cond_1
    :goto_1
    if-eqz p2, :cond_2

    iget-object v1, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mSuppMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v1}, Landroid/graphics/Matrix;->reset()V

    :cond_2
    invoke-virtual {p0}, Lcom/konka/mm/photo/ImageViewTouchBase;->getImageViewMatrix()Landroid/graphics/Matrix;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/konka/mm/photo/ImageViewTouchBase;->setImageMatrix(Landroid/graphics/Matrix;)V

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mBaseMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v1}, Landroid/graphics/Matrix;->reset()V

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/konka/mm/photo/ImageViewTouchBase;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_1
.end method

.method public setRecycler(Lcom/konka/mm/photo/ImageViewTouchBase$Recycler;)V
    .locals 0
    .param p1    # Lcom/konka/mm/photo/ImageViewTouchBase$Recycler;

    iput-object p1, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mRecycler:Lcom/konka/mm/photo/ImageViewTouchBase$Recycler;

    return-void
.end method

.method public zoomIn()V
    .locals 1

    const/high16 v0, 0x3fc00000    # 1.5f

    invoke-virtual {p0, v0}, Lcom/konka/mm/photo/ImageViewTouchBase;->zoomIn(F)V

    return-void
.end method

.method protected zoomIn(F)V
    .locals 4
    .param p1    # F

    const/high16 v3, 0x40000000    # 2.0f

    iget-object v2, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mBitmapDisplayed:Lcom/konka/mm/photo/RotateBitmap;

    invoke-virtual {v2}, Lcom/konka/mm/photo/RotateBitmap;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    if-nez v2, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/konka/mm/photo/ImageViewTouchBase;->getWidth()I

    move-result v2

    int-to-float v2, v2

    div-float v0, v2, v3

    invoke-virtual {p0}, Lcom/konka/mm/photo/ImageViewTouchBase;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float v1, v2, v3

    iget-object v2, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mSuppMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v2, p1, p1, v0, v1}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    invoke-virtual {p0}, Lcom/konka/mm/photo/ImageViewTouchBase;->getImageViewMatrix()Landroid/graphics/Matrix;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/konka/mm/photo/ImageViewTouchBase;->setImageMatrix(Landroid/graphics/Matrix;)V

    goto :goto_0
.end method

.method public zoomOut()V
    .locals 1

    const/high16 v0, 0x3fc00000    # 1.5f

    invoke-virtual {p0, v0}, Lcom/konka/mm/photo/ImageViewTouchBase;->zoomOut(F)V

    return-void
.end method

.method protected zoomOut(F)V
    .locals 7
    .param p1    # F

    const/4 v6, 0x1

    const/high16 v4, 0x40000000    # 2.0f

    const/high16 v5, 0x3f800000    # 1.0f

    iget-object v3, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mBitmapDisplayed:Lcom/konka/mm/photo/RotateBitmap;

    invoke-virtual {v3}, Lcom/konka/mm/photo/RotateBitmap;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v3

    if-nez v3, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/konka/mm/photo/ImageViewTouchBase;->getWidth()I

    move-result v3

    int-to-float v3, v3

    div-float v0, v3, v4

    invoke-virtual {p0}, Lcom/konka/mm/photo/ImageViewTouchBase;->getHeight()I

    move-result v3

    int-to-float v3, v3

    div-float v1, v3, v4

    new-instance v2, Landroid/graphics/Matrix;

    iget-object v3, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mSuppMatrix:Landroid/graphics/Matrix;

    invoke-direct {v2, v3}, Landroid/graphics/Matrix;-><init>(Landroid/graphics/Matrix;)V

    div-float v3, v5, p1

    div-float v4, v5, p1

    invoke-virtual {v2, v3, v4, v0, v1}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    iget-object v3, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mSuppMatrix:Landroid/graphics/Matrix;

    div-float v4, v5, p1

    div-float/2addr v5, p1

    invoke-virtual {v3, v4, v5, v0, v1}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    invoke-virtual {p0}, Lcom/konka/mm/photo/ImageViewTouchBase;->getImageViewMatrix()Landroid/graphics/Matrix;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/konka/mm/photo/ImageViewTouchBase;->setImageMatrix(Landroid/graphics/Matrix;)V

    invoke-virtual {p0, v6, v6}, Lcom/konka/mm/photo/ImageViewTouchBase;->center(ZZ)V

    goto :goto_0
.end method

.method protected zoomTo(F)V
    .locals 4
    .param p1    # F

    const/high16 v3, 0x40000000    # 2.0f

    invoke-virtual {p0}, Lcom/konka/mm/photo/ImageViewTouchBase;->getWidth()I

    move-result v2

    int-to-float v2, v2

    div-float v0, v2, v3

    invoke-virtual {p0}, Lcom/konka/mm/photo/ImageViewTouchBase;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float v1, v2, v3

    invoke-virtual {p0, p1, v0, v1}, Lcom/konka/mm/photo/ImageViewTouchBase;->zoomTo(FFF)V

    return-void
.end method

.method protected zoomTo(FFF)V
    .locals 4
    .param p1    # F
    .param p2    # F
    .param p3    # F

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/konka/mm/photo/ImageViewTouchBase;->getScale()F

    move-result v1

    div-float v0, p1, v1

    iget-object v2, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mSuppMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v2, v0, v0, p2, p3}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    invoke-virtual {p0}, Lcom/konka/mm/photo/ImageViewTouchBase;->getImageViewMatrix()Landroid/graphics/Matrix;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/konka/mm/photo/ImageViewTouchBase;->setImageMatrix(Landroid/graphics/Matrix;)V

    invoke-virtual {p0, v3, v3}, Lcom/konka/mm/photo/ImageViewTouchBase;->center(ZZ)V

    return-void
.end method

.method protected zoomTo(FFFF)V
    .locals 10
    .param p1    # F
    .param p2    # F
    .param p3    # F
    .param p4    # F

    invoke-virtual {p0}, Lcom/konka/mm/photo/ImageViewTouchBase;->getScale()F

    move-result v0

    sub-float v0, p1, v0

    div-float v6, v0, p4

    invoke-virtual {p0}, Lcom/konka/mm/photo/ImageViewTouchBase;->getScale()F

    move-result v5

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    iget-object v9, p0, Lcom/konka/mm/photo/ImageViewTouchBase;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/konka/mm/photo/ImageViewTouchBase$4;

    move-object v1, p0

    move v2, p4

    move v7, p2

    move v8, p3

    invoke-direct/range {v0 .. v8}, Lcom/konka/mm/photo/ImageViewTouchBase$4;-><init>(Lcom/konka/mm/photo/ImageViewTouchBase;FJFFFF)V

    invoke-virtual {v9, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method protected zoomToPoint(FFF)V
    .locals 4
    .param p1    # F
    .param p2    # F
    .param p3    # F

    const/high16 v3, 0x40000000    # 2.0f

    invoke-virtual {p0}, Lcom/konka/mm/photo/ImageViewTouchBase;->getWidth()I

    move-result v2

    int-to-float v2, v2

    div-float v0, v2, v3

    invoke-virtual {p0}, Lcom/konka/mm/photo/ImageViewTouchBase;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float v1, v2, v3

    sub-float v2, v0, p2

    sub-float v3, v1, p3

    invoke-virtual {p0, v2, v3}, Lcom/konka/mm/photo/ImageViewTouchBase;->panBy(FF)V

    invoke-virtual {p0, p1, v0, v1}, Lcom/konka/mm/photo/ImageViewTouchBase;->zoomTo(FFF)V

    return-void
.end method
