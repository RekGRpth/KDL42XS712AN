.class Lcom/konka/mm/photo/GalleryShowPicActivity$6;
.super Ljava/lang/Object;
.source "GalleryShowPicActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/mm/photo/GalleryShowPicActivity;->setThumbImage(Landroid/widget/ImageView;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/mm/photo/GalleryShowPicActivity;

.field private final synthetic val$icon:Landroid/widget/ImageView;

.field private final synthetic val$position:I


# direct methods
.method constructor <init>(Lcom/konka/mm/photo/GalleryShowPicActivity;ILandroid/widget/ImageView;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/photo/GalleryShowPicActivity$6;->this$0:Lcom/konka/mm/photo/GalleryShowPicActivity;

    iput p2, p0, Lcom/konka/mm/photo/GalleryShowPicActivity$6;->val$position:I

    iput-object p3, p0, Lcom/konka/mm/photo/GalleryShowPicActivity$6;->val$icon:Landroid/widget/ImageView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    iget-object v3, p0, Lcom/konka/mm/photo/GalleryShowPicActivity$6;->this$0:Lcom/konka/mm/photo/GalleryShowPicActivity;

    # getter for: Lcom/konka/mm/photo/GalleryShowPicActivity;->picPaths:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/konka/mm/photo/GalleryShowPicActivity;->access$0(Lcom/konka/mm/photo/GalleryShowPicActivity;)Ljava/util/ArrayList;

    move-result-object v3

    iget v4, p0, Lcom/konka/mm/photo/GalleryShowPicActivity$6;->val$position:I

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const v4, 0x8000

    invoke-static {v3, v4}, Lcom/konka/mm/tools/PicTool;->decodeFileDescriptor(Ljava/lang/String;I)Landroid/graphics/Bitmap;

    move-result-object v0

    new-instance v2, Landroid/os/Message;

    invoke-direct {v2}, Landroid/os/Message;-><init>()V

    const/4 v3, 0x0

    iput v3, v2, Landroid/os/Message;->what:I

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/konka/mm/photo/GalleryShowPicActivity$6;->val$icon:Landroid/widget/ImageView;

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iput-object v1, v2, Landroid/os/Message;->obj:Ljava/lang/Object;

    iget-object v3, p0, Lcom/konka/mm/photo/GalleryShowPicActivity$6;->this$0:Lcom/konka/mm/photo/GalleryShowPicActivity;

    # getter for: Lcom/konka/mm/photo/GalleryShowPicActivity;->handler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/konka/mm/photo/GalleryShowPicActivity;->access$6(Lcom/konka/mm/photo/GalleryShowPicActivity;)Landroid/os/Handler;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method
