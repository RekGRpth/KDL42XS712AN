.class public Lcom/konka/mm/photo/DiskAdapter;
.super Landroid/widget/BaseAdapter;
.source "DiskAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/mm/photo/DiskAdapter$ListItemViewHolder;
    }
.end annotation


# instance fields
.field private context:Landroid/content/Context;

.field private diskName:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private layoutInflater:Landroid/view/LayoutInflater;

.field private usbsCount:I

.field private usbsList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;I)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p3    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;I)V"
        }
    .end annotation

    const/4 v3, 0x0

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    iput v3, p0, Lcom/konka/mm/photo/DiskAdapter;->usbsCount:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/konka/mm/photo/DiskAdapter;->diskName:Ljava/util/List;

    iput-object p1, p0, Lcom/konka/mm/photo/DiskAdapter;->context:Landroid/content/Context;

    iput-object p2, p0, Lcom/konka/mm/photo/DiskAdapter;->usbsList:Ljava/util/List;

    iget v0, p0, Lcom/konka/mm/photo/DiskAdapter;->usbsCount:I

    iput v0, p0, Lcom/konka/mm/photo/DiskAdapter;->usbsCount:I

    iget-object v0, p0, Lcom/konka/mm/photo/DiskAdapter;->usbsList:Ljava/util/List;

    invoke-static {v0}, Lcom/konka/mm/tools/FileTool;->changeDiskName(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/mm/photo/DiskAdapter;->diskName:Ljava/util/List;

    iget-object v0, p0, Lcom/konka/mm/photo/DiskAdapter;->diskName:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/mm/photo/DiskAdapter;->diskName:Ljava/util/List;

    iget-object v1, p0, Lcom/konka/mm/photo/DiskAdapter;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090014    # com.konka.mm.R.string.all

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v3, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    :cond_0
    iget-object v0, p0, Lcom/konka/mm/photo/DiskAdapter;->context:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/mm/photo/DiskAdapter;->layoutInflater:Landroid/view/LayoutInflater;

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/photo/DiskAdapter;->usbsList:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/konka/mm/photo/DiskAdapter;->usbsList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/konka/mm/photo/DiskAdapter;->usbsList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1    # I

    int-to-long v0, p1

    return-wide v0
.end method

.method public getUsbsList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/konka/mm/photo/DiskAdapter;->usbsList:Ljava/util/List;

    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    const/4 v0, 0x0

    if-nez p2, :cond_0

    iget-object v1, p0, Lcom/konka/mm/photo/DiskAdapter;->layoutInflater:Landroid/view/LayoutInflater;

    const v2, 0x7f03002b    # com.konka.mm.R.layout.samba_list_adapter

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    new-instance v0, Lcom/konka/mm/photo/DiskAdapter$ListItemViewHolder;

    invoke-direct {v0}, Lcom/konka/mm/photo/DiskAdapter$ListItemViewHolder;-><init>()V

    const v1, 0x7f0b00ae    # com.konka.mm.R.id.samba_icon

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, v0, Lcom/konka/mm/photo/DiskAdapter$ListItemViewHolder;->iconImage:Landroid/widget/ImageView;

    const v1, 0x7f0b00af    # com.konka.mm.R.id.samba_ip

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v0, Lcom/konka/mm/photo/DiskAdapter$ListItemViewHolder;->nameText:Landroid/widget/TextView;

    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :goto_0
    iget-object v1, v0, Lcom/konka/mm/photo/DiskAdapter$ListItemViewHolder;->iconImage:Landroid/widget/ImageView;

    const v2, 0x7f020070    # com.konka.mm.R.drawable.media_disk_uns_06

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v2, v0, Lcom/konka/mm/photo/DiskAdapter$ListItemViewHolder;->nameText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/mm/photo/DiskAdapter;->diskName:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-object p2

    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/konka/mm/photo/DiskAdapter$ListItemViewHolder;

    goto :goto_0
.end method

.method public setUsbsList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/konka/mm/photo/DiskAdapter;->usbsList:Ljava/util/List;

    return-void
.end method
