.class public Lcom/konka/mm/photo/PhotoDiskActivity;
.super Landroid/app/Activity;
.source "PhotoDiskActivity.java"

# interfaces
.implements Lcom/konka/mm/IActivity;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/mm/photo/PhotoDiskActivity$BtnScanDialogListener;,
        Lcom/konka/mm/photo/PhotoDiskActivity$UsbReceiver;
    }
.end annotation


# static fields
.field public static final DIALOG_SCAN_DB:I = 0x0

.field public static List_Mode:I = 0x0

.field public static final OPTION_STATE_PICTURE:I = 0x1

.field public static clearImageData:Z

.field private static diskRoot:Ljava/lang/String;


# instance fields
.field private adapter:Lcom/konka/mm/photo/DiskAdapter;

.field public bDialogScanDB:Z

.field private gridView:Landroid/widget/GridView;

.field private listListener:Landroid/widget/AdapterView$OnItemClickListener;

.field private mUsbsList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private progressDialog:Landroid/app/ProgressDialog;

.field protected updateHandler:Landroid/os/Handler;

.field private usbReceiver:Lcom/konka/mm/photo/PhotoDiskActivity$UsbReceiver;

.field private usbsCount:I

.field private version_text:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, ""

    sput-object v0, Lcom/konka/mm/photo/PhotoDiskActivity;->diskRoot:Ljava/lang/String;

    const/4 v0, 0x1

    sput-boolean v0, Lcom/konka/mm/photo/PhotoDiskActivity;->clearImageData:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput-boolean v0, p0, Lcom/konka/mm/photo/PhotoDiskActivity;->bDialogScanDB:Z

    iput v0, p0, Lcom/konka/mm/photo/PhotoDiskActivity;->usbsCount:I

    new-instance v0, Lcom/konka/mm/photo/PhotoDiskActivity$1;

    invoke-direct {v0, p0}, Lcom/konka/mm/photo/PhotoDiskActivity$1;-><init>(Lcom/konka/mm/photo/PhotoDiskActivity;)V

    iput-object v0, p0, Lcom/konka/mm/photo/PhotoDiskActivity;->updateHandler:Landroid/os/Handler;

    new-instance v0, Lcom/konka/mm/photo/PhotoDiskActivity$2;

    invoke-direct {v0, p0}, Lcom/konka/mm/photo/PhotoDiskActivity$2;-><init>(Lcom/konka/mm/photo/PhotoDiskActivity;)V

    iput-object v0, p0, Lcom/konka/mm/photo/PhotoDiskActivity;->listListener:Landroid/widget/AdapterView$OnItemClickListener;

    return-void
.end method

.method static synthetic access$0(Lcom/konka/mm/photo/PhotoDiskActivity;)Landroid/app/ProgressDialog;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/photo/PhotoDiskActivity;->progressDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$1(Lcom/konka/mm/photo/PhotoDiskActivity;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/photo/PhotoDiskActivity;->mUsbsList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$2()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/konka/mm/photo/PhotoDiskActivity;->diskRoot:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3(Ljava/lang/String;)V
    .locals 0

    sput-object p0, Lcom/konka/mm/photo/PhotoDiskActivity;->diskRoot:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public InitData()V
    .locals 1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/konka/mm/photo/PhotoDiskActivity;->mUsbsList:Ljava/util/List;

    new-instance v0, Lcom/konka/mm/photo/PhotoDiskActivity$UsbReceiver;

    invoke-direct {v0, p0}, Lcom/konka/mm/photo/PhotoDiskActivity$UsbReceiver;-><init>(Lcom/konka/mm/photo/PhotoDiskActivity;)V

    iput-object v0, p0, Lcom/konka/mm/photo/PhotoDiskActivity;->usbReceiver:Lcom/konka/mm/photo/PhotoDiskActivity$UsbReceiver;

    invoke-static {p0}, Lcom/konka/mm/tools/FileTool;->getUsbs(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/mm/photo/PhotoDiskActivity;->mUsbsList:Ljava/util/List;

    return-void
.end method

.method public broweToRoot()V
    .locals 2

    invoke-static {p0}, Lcom/konka/mm/tools/FileTool;->getUsbs(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/mm/photo/PhotoDiskActivity;->mUsbsList:Ljava/util/List;

    iget-object v0, p0, Lcom/konka/mm/photo/PhotoDiskActivity;->mUsbsList:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/mm/photo/PhotoDiskActivity;->mUsbsList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p0}, Lcom/konka/mm/photo/PhotoDiskActivity;->setAdapter()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/konka/mm/photo/PhotoDiskActivity;->adapter:Lcom/konka/mm/photo/DiskAdapter;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/konka/mm/photo/PhotoDiskActivity;->adapter:Lcom/konka/mm/photo/DiskAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/konka/mm/photo/DiskAdapter;->setUsbsList(Ljava/util/List;)V

    iget-object v0, p0, Lcom/konka/mm/photo/PhotoDiskActivity;->adapter:Lcom/konka/mm/photo/DiskAdapter;

    invoke-virtual {v0}, Lcom/konka/mm/photo/DiskAdapter;->notifyDataSetChanged()V

    :cond_1
    invoke-static {p0}, Lcom/konka/mm/finals/CommonFinals;->sdCardNoFound(Landroid/app/Activity;)V

    goto :goto_0
.end method

.method public cancelProgressDlg()V
    .locals 0

    return-void
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 2
    .param p1    # Landroid/view/KeyEvent;

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    invoke-super {p0, p1}, Landroid/app/Activity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    return v0

    :pswitch_0
    invoke-virtual {p0}, Lcom/konka/mm/photo/PhotoDiskActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090004    # com.konka.mm.R.string.LaunchImage

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/konka/mm/finals/CommonFinals;->quitActivity(Landroid/app/Activity;Ljava/lang/String;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method

.method public findView()V
    .locals 6

    const v1, 0x7f0b0002    # com.konka.mm.R.id.version

    invoke-virtual {p0, v1}, Lcom/konka/mm/photo/PhotoDiskActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/mm/photo/PhotoDiskActivity;->version_text:Landroid/widget/TextView;

    const v1, 0x7f0b0003    # com.konka.mm.R.id.usbs_list

    invoke-virtual {p0, v1}, Lcom/konka/mm/photo/PhotoDiskActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/GridView;

    iput-object v1, p0, Lcom/konka/mm/photo/PhotoDiskActivity;->gridView:Landroid/widget/GridView;

    :try_start_0
    iget-object v1, p0, Lcom/konka/mm/photo/PhotoDiskActivity;->version_text:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    const v3, 0x7f09000e    # com.konka.mm.R.string.Version

    invoke-virtual {p0, v3}, Lcom/konka/mm/photo/PhotoDiskActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/konka/mm/photo/PhotoDiskActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {p0}, Lcom/konka/mm/photo/PhotoDiskActivity;->getPackageName()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v3

    iget-object v3, v3, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".00 "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x7f09000f    # com.konka.mm.R.string.Build

    invoke-virtual {p0, v3}, Lcom/konka/mm/photo/PhotoDiskActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/konka/mm/photo/PhotoDiskActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {p0}, Lcom/konka/mm/photo/PhotoDiskActivity;->getPackageName()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v3

    iget v3, v3, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v1, 0x7f030001    # com.konka.mm.R.layout.disk_main

    invoke-virtual {p0, v1}, Lcom/konka/mm/photo/PhotoDiskActivity;->setContentView(I)V

    const/4 v1, 0x2

    sput v1, Lcom/konka/mm/photo/PhotoDiskActivity;->List_Mode:I

    invoke-virtual {p0}, Lcom/konka/mm/photo/PhotoDiskActivity;->InitData()V

    invoke-virtual {p0}, Lcom/konka/mm/photo/PhotoDiskActivity;->findView()V

    invoke-virtual {p0}, Lcom/konka/mm/photo/PhotoDiskActivity;->broweToRoot()V

    sget-object v1, Lcom/konka/mm/finals/CommonFinals;->MM_LOG_TAG:Ljava/lang/String;

    invoke-static {v1}, Liapp/eric/utils/base/Trace;->setTag(Ljava/lang/String;)V

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/mm/photo/PhotoDiskActivity;->usbReceiver:Lcom/konka/mm/photo/PhotoDiskActivity$UsbReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/konka/mm/photo/PhotoDiskActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 4
    .param p1    # Landroid/view/Menu;

    const/4 v0, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    const-string v3, "\ufffd\ufffd\ufffd\ufffd"

    invoke-interface {p1, v0, v1, v2, v3}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v3

    return v3
.end method

.method protected onDestroy()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    iget-object v0, p0, Lcom/konka/mm/photo/PhotoDiskActivity;->usbReceiver:Lcom/konka/mm/photo/PhotoDiskActivity$UsbReceiver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/mm/photo/PhotoDiskActivity;->usbReceiver:Lcom/konka/mm/photo/PhotoDiskActivity$UsbReceiver;

    invoke-virtual {p0, v0}, Lcom/konka/mm/photo/PhotoDiskActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    :cond_0
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/System;->exit(I)V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1    # Landroid/view/MenuItem;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    const/4 v0, 0x0

    return v0

    :pswitch_0
    invoke-static {p0}, Lcom/konka/mm/tools/VersionTool;->VersionShow(Landroid/content/Context;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public setAdapter()V
    .locals 3

    sget-object v0, Lcom/konka/mm/finals/CommonFinals;->dlg:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/konka/mm/finals/CommonFinals;->dlg:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->cancel()V

    :cond_0
    iget-object v0, p0, Lcom/konka/mm/photo/PhotoDiskActivity;->gridView:Landroid/widget/GridView;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setNumColumns(I)V

    iget-object v0, p0, Lcom/konka/mm/photo/PhotoDiskActivity;->gridView:Landroid/widget/GridView;

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setSelector(Landroid/graphics/drawable/Drawable;)V

    new-instance v0, Lcom/konka/mm/photo/DiskAdapter;

    iget-object v1, p0, Lcom/konka/mm/photo/PhotoDiskActivity;->mUsbsList:Ljava/util/List;

    iget v2, p0, Lcom/konka/mm/photo/PhotoDiskActivity;->usbsCount:I

    invoke-direct {v0, p0, v1, v2}, Lcom/konka/mm/photo/DiskAdapter;-><init>(Landroid/content/Context;Ljava/util/List;I)V

    iput-object v0, p0, Lcom/konka/mm/photo/PhotoDiskActivity;->adapter:Lcom/konka/mm/photo/DiskAdapter;

    iget-object v0, p0, Lcom/konka/mm/photo/PhotoDiskActivity;->gridView:Landroid/widget/GridView;

    iget-object v1, p0, Lcom/konka/mm/photo/PhotoDiskActivity;->adapter:Lcom/konka/mm/photo/DiskAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/konka/mm/photo/PhotoDiskActivity;->gridView:Landroid/widget/GridView;

    new-instance v1, Lcom/konka/mm/photo/PhotoDiskActivity$3;

    invoke-direct {v1, p0}, Lcom/konka/mm/photo/PhotoDiskActivity$3;-><init>(Lcom/konka/mm/photo/PhotoDiskActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    return-void
.end method

.method public showProgressDialog(II)V
    .locals 0
    .param p1    # I
    .param p2    # I

    return-void
.end method

.method public startMudulesActivity(I)V
    .locals 5
    .param p1    # I

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    new-instance v1, Landroid/content/ComponentName;

    const-string v3, "com.konka.mm"

    const-string v4, "com.konka.mm.modules.ModulesActivity"

    invoke-direct {v1, v3, v4}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "android.intent.action.MAIN"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v3, "android.intent.category.DEFAULT"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {v2, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    const/high16 v3, 0x10000000

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v3, "com.konka.mm.module.called.who"

    const-string v4, "com.konka.mm.module.called.image"

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "com.konka.mm.module.called.index.disk"

    invoke-virtual {v0, v3, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v2, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    invoke-virtual {p0, v2}, Lcom/konka/mm/photo/PhotoDiskActivity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public startPicMainActivity(I)V
    .locals 4
    .param p1    # I

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/konka/mm/photo/PicMainActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v3, "com.konka.mm.file.root.path"

    iget-object v2, p0, Lcom/konka/mm/photo/PhotoDiskActivity;->mUsbsList:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    invoke-virtual {p0, v1}, Lcom/konka/mm/photo/PhotoDiskActivity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method
