.class public Lcom/konka/mm/photo/SwitcherImageLoader;
.super Landroid/os/AsyncTask;
.source "SwitcherImageLoader.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field protected bitmap:Landroid/graphics/Bitmap;

.field private bmp:Landroid/graphics/Bitmap;

.field private context:Landroid/content/Context;

.field private image:Landroid/widget/ImageSwitcher;

.field protected mAlphaIn:Landroid/view/animation/Animation;

.field private path:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/widget/ImageSwitcher;Ljava/lang/String;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/widget/ImageSwitcher;
    .param p3    # Ljava/lang/String;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    iput-object p2, p0, Lcom/konka/mm/photo/SwitcherImageLoader;->image:Landroid/widget/ImageSwitcher;

    iput-object p3, p0, Lcom/konka/mm/photo/SwitcherImageLoader;->path:Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/mm/photo/SwitcherImageLoader;->context:Landroid/content/Context;

    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/4 v1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    iput-object v0, p0, Lcom/konka/mm/photo/SwitcherImageLoader;->mAlphaIn:Landroid/view/animation/Animation;

    iget-object v0, p0, Lcom/konka/mm/photo/SwitcherImageLoader;->mAlphaIn:Landroid/view/animation/Animation;

    const-wide/16 v1, 0x5dc

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v0, p3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/konka/mm/photo/SwitcherImageLoader;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 2
    .param p1    # [Ljava/lang/Void;

    iget-object v0, p0, Lcom/konka/mm/photo/SwitcherImageLoader;->path:Ljava/lang/String;

    const v1, 0x1fa400

    invoke-static {v0, v1}, Lcom/konka/mm/tools/PicTool;->decodeFileDescriptor(Ljava/lang/String;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/mm/photo/SwitcherImageLoader;->bitmap:Landroid/graphics/Bitmap;

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Void;

    invoke-virtual {p0, v0}, Lcom/konka/mm/photo/SwitcherImageLoader;->publishProgress([Ljava/lang/Object;)V

    invoke-virtual {p0}, Lcom/konka/mm/photo/SwitcherImageLoader;->isCancelled()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/konka/mm/photo/SwitcherImageLoader;->cancel(Z)Z

    const/4 v0, 0x0

    return-object v0
.end method

.method public bridge varargs synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/konka/mm/photo/SwitcherImageLoader;->onProgressUpdate([Ljava/lang/Void;)V

    return-void
.end method

.method public varargs onProgressUpdate([Ljava/lang/Void;)V
    .locals 3
    .param p1    # [Ljava/lang/Void;

    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "---->onProgressUpdate"

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v1, p0, Lcom/konka/mm/photo/SwitcherImageLoader;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/mm/photo/SwitcherImageLoader;->bitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1, v2}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    iget-object v1, p0, Lcom/konka/mm/photo/SwitcherImageLoader;->image:Landroid/widget/ImageSwitcher;

    invoke-virtual {v1, v0}, Landroid/widget/ImageSwitcher;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method
