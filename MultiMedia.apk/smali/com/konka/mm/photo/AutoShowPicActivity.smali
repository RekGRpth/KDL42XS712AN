.class public Lcom/konka/mm/photo/AutoShowPicActivity;
.super Landroid/app/Activity;
.source "AutoShowPicActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/mm/photo/AutoShowPicActivity$EthStateReceiver;,
        Lcom/konka/mm/photo/AutoShowPicActivity$ImageViewOnTounchListener;,
        Lcom/konka/mm/photo/AutoShowPicActivity$SetWallpaperTask;
    }
.end annotation


# static fields
.field private static final DEFAULT_TIMEOUT:I = 0x3a98

.field static final DRAG:I = 0x1

.field public static final HIDE_PLAYER_CONTROL:I = 0x11

.field public static final HIDE_PLAYER_PICTURE_CONTROL:I = 0x12

.field public static final HIDE_TEXT_STATE:I = 0x14

.field static final NONE:I = 0x0

.field public static final OPTION_STATE_BACK:I = 0x2

.field public static final OPTION_STATE_BIG:I = 0x8

.field public static final OPTION_STATE_FULLSCREEN:I = 0x10

.field public static final OPTION_STATE_GALLERY:I = 0x1

.field public static final OPTION_STATE_NEXT:I = 0x4

.field public static final OPTION_STATE_PLAY:I = 0x3

.field public static final OPTION_STATE_PLAY_CIRCULATE:I = 0x12

.field public static final OPTION_STATE_PLAY_SEQUENCE:I = 0x11

.field public static final OPTION_STATE_SMALL:I = 0x9

.field public static final OPTION_STATE_STOP:I = 0x5

.field public static final OPTION_STATE_TURNLEFT:I = 0x6

.field public static final OPTION_STATE_TURNRIGHT:I = 0x7

.field public static final OPTION_STATE_WALLPALER:I = 0x0

.field private static final PLAY_4K2K_PHOTO:I = 0xa1

.field private static final PLAY_FIRST_PHOTO:I = 0xa

.field private static final PLAY_NOT_4K2K_PHOTO:I = 0xa2

.field public static final PROGRESS_DIALOG_LOAD:I = 0x15

.field public static final TAG:Ljava/lang/String; = "AutoShowPicActivity"

.field static final ZOOM:I = 0x2

.field private static final ZOOM_IN_SCALE:D = 1.25

.field private static final ZOOM_OUT_SCALE:D = 0.8

.field protected static bAutoPlayKey:Z

.field protected static descaleDrawable:[I

.field protected static enlargeDrawable:[I

.field protected static is4K2KMode:Z

.field protected static isAutoPlaying:Z

.field protected static isCanMove:Z

.field protected static isForwardorBackward:Z

.field private static isProgressDlg:Z

.field protected static isSupport4K2K:Z

.field private static mThread:Ljava/lang/Thread;

.field private static options:Landroid/graphics/BitmapFactory$Options;

.field protected static panel4k2kmode:Z

.field private static progressDialog:Landroid/app/ProgressDialog;

.field protected static sourceFileComeFrom:Ljava/lang/String;


# instance fields
.field protected final PPT_PLAYER:I

.field private abroadCustomer:Ljava/lang/String;

.field private bTeacCustomer:Z

.field protected bitmapArray:Lcom/konka/mm/photo/RotateBitmap;

.field private commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

.field private currentIp:Ljava/lang/String;

.field private currentNum:I

.field protected handler:Landroid/os/Handler;

.field protected hideHandler:Landroid/os/Handler;

.field private is3dKeyDown:Z

.field private isControllerShow:Z

.field private isEnterOtherAct:Z

.field private isInitScrollimgview:Z

.field public isNeetCirculate:Z

.field private keyIsVolume:Z

.field private layout1:Landroid/widget/LinearLayout;

.field protected mAlphaIn:Landroid/view/animation/Animation;

.field protected mAlphaOut:Landroid/view/animation/Animation;

.field private mDelayed_time:I

.field private mEthStateReceiver:Lcom/konka/mm/photo/AutoShowPicActivity$EthStateReceiver;

.field private mFileInputStream:Ljava/io/FileInputStream;

.field protected mPicControlInAnim:Landroid/view/animation/Animation;

.field protected mPicControlOutAnim:Landroid/view/animation/Animation;

.field protected mPicPushLeftIn:Landroid/view/animation/Animation;

.field protected mPicPushRightIn:Landroid/view/animation/Animation;

.field private mReceiver:Landroid/content/BroadcastReceiver;

.field protected mRotation:Landroid/view/animation/Animation;

.field private mSetWallpaperTask:Lcom/konka/mm/photo/AutoShowPicActivity$SetWallpaperTask;

.field private mSurfaceView4K2K:Lcom/konka/mm/photo/SurfaceView4K2K;

.field mid:Landroid/graphics/PointF;

.field mode:I

.field moveflag:Z

.field oldDist:F

.field protected parentPath:Ljava/lang/String;

.field private picImageview:Lcom/konka/mm/photo/ImageViewTouch;

.field public picPaths:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected picShowHolder:Lcom/konka/mm/photo/PicShowHolder;

.field protected playStateDrawable:[I

.field protected root_path:Ljava/lang/String;

.field private rotationClickCount:I

.field savedMatrix:Landroid/graphics/Matrix;

.field private scaleHeight:F

.field private scaleHeight1:F

.field private scaleImageView:Landroid/widget/ImageView;

.field private scaleWidth:F

.field private scaleWidth1:F

.field screenHeight:I

.field screenWidth:I

.field private serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

.field start:Landroid/graphics/PointF;

.field private state:I

.field private stateDrawable:Landroid/widget/ImageView;

.field private textView:Landroid/widget/TextView;

.field private title_layout:Landroid/widget/RelativeLayout;

.field toast_file_not_exist:Landroid/widget/Toast;

.field private ursaType:Lcom/konka/kkinterface/tv/CommonDesk$UrsaSelectEnumType;

.field protected zoomTimes:I

.field private zoom_level:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v2, 0x5

    const/4 v1, 0x0

    new-instance v0, Ljava/lang/Thread;

    invoke-direct {v0}, Ljava/lang/Thread;-><init>()V

    sput-object v0, Lcom/konka/mm/photo/AutoShowPicActivity;->mThread:Ljava/lang/Thread;

    const/4 v0, 0x1

    sput-boolean v0, Lcom/konka/mm/photo/AutoShowPicActivity;->bAutoPlayKey:Z

    sput-boolean v1, Lcom/konka/mm/photo/AutoShowPicActivity;->is4K2KMode:Z

    sput-boolean v1, Lcom/konka/mm/photo/AutoShowPicActivity;->panel4k2kmode:Z

    sput-boolean v1, Lcom/konka/mm/photo/AutoShowPicActivity;->isForwardorBackward:Z

    sput-boolean v1, Lcom/konka/mm/photo/AutoShowPicActivity;->isSupport4K2K:Z

    sput-boolean v1, Lcom/konka/mm/photo/AutoShowPicActivity;->isAutoPlaying:Z

    sput-boolean v1, Lcom/konka/mm/photo/AutoShowPicActivity;->isCanMove:Z

    new-array v0, v2, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/konka/mm/photo/AutoShowPicActivity;->enlargeDrawable:[I

    new-array v0, v2, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/konka/mm/photo/AutoShowPicActivity;->descaleDrawable:[I

    sput-boolean v1, Lcom/konka/mm/photo/AutoShowPicActivity;->isProgressDlg:Z

    return-void

    nop

    :array_0
    .array-data 4
        0x7f0200a8    # com.konka.mm.R.drawable.pic_icon_enlarge_uns
        0x7f0200a4    # com.konka.mm.R.drawable.pic_icon_enlarge2_s
        0x7f0200a5    # com.konka.mm.R.drawable.pic_icon_enlarge4_s
        0x7f0200a6    # com.konka.mm.R.drawable.pic_icon_enlarge8_s
        0x7f0200a3    # com.konka.mm.R.drawable.pic_icon_enlarge12_s
    .end array-data

    :array_1
    .array-data 4
        0x7f0200a2    # com.konka.mm.R.drawable.pic_icon_descale_uns
        0x7f02009e    # com.konka.mm.R.drawable.pic_icon_descale2_s
        0x7f02009f    # com.konka.mm.R.drawable.pic_icon_descale4_s
        0x7f0200a0    # com.konka.mm.R.drawable.pic_icon_descale8_s
        0x7f02009d    # com.konka.mm.R.drawable.pic_icon_descale12_s
    .end array-data
.end method

.method public constructor <init>()V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput v4, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->state:I

    const v0, 0x50000001

    iput v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->PPT_PLAYER:I

    iput-boolean v1, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->isControllerShow:Z

    new-instance v0, Lcom/konka/mm/photo/RotateBitmap;

    invoke-direct {v0, v3}, Lcom/konka/mm/photo/RotateBitmap;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->bitmapArray:Lcom/konka/mm/photo/RotateBitmap;

    iput-object v3, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->mFileInputStream:Ljava/io/FileInputStream;

    iput v1, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->currentNum:I

    iput-boolean v1, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->keyIsVolume:Z

    iput v1, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->rotationClickCount:I

    iput-object v3, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->root_path:Ljava/lang/String;

    iput-boolean v4, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->isEnterOtherAct:Z

    iput-boolean v1, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->is3dKeyDown:Z

    iput-boolean v1, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->isNeetCirculate:Z

    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->playStateDrawable:[I

    iput v2, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->scaleWidth:F

    iput v2, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->scaleHeight:F

    iput-boolean v1, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->isInitScrollimgview:Z

    iput v1, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->zoomTimes:I

    const/16 v0, 0x1770

    iput v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->mDelayed_time:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picPaths:Ljava/util/ArrayList;

    new-instance v0, Lcom/konka/mm/photo/AutoShowPicActivity$1;

    invoke-direct {v0, p0}, Lcom/konka/mm/photo/AutoShowPicActivity$1;-><init>(Lcom/konka/mm/photo/AutoShowPicActivity;)V

    iput-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->handler:Landroid/os/Handler;

    new-instance v0, Lcom/konka/mm/photo/AutoShowPicActivity$2;

    invoke-direct {v0, p0}, Lcom/konka/mm/photo/AutoShowPicActivity$2;-><init>(Lcom/konka/mm/photo/AutoShowPicActivity;)V

    iput-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->hideHandler:Landroid/os/Handler;

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->savedMatrix:Landroid/graphics/Matrix;

    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->start:Landroid/graphics/PointF;

    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->mid:Landroid/graphics/PointF;

    iput-boolean v1, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->moveflag:Z

    iput v2, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->scaleWidth1:F

    iput v2, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->scaleHeight1:F

    iput v1, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->zoom_level:I

    iput v1, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->mode:I

    return-void

    nop

    :array_0
    .array-data 4
        0x7f0200b1    # com.konka.mm.R.drawable.playstate1
        0x7f0200b2    # com.konka.mm.R.drawable.playstate2
        0x7f0200b3    # com.konka.mm.R.drawable.playstate3
        0x7f0200b4    # com.konka.mm.R.drawable.playstate4
    .end array-data
.end method

.method static synthetic access$0(Lcom/konka/mm/photo/AutoShowPicActivity;)I
    .locals 1

    iget v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->mDelayed_time:I

    return v0
.end method

.method static synthetic access$1(Lcom/konka/mm/photo/AutoShowPicActivity;I)V
    .locals 0

    iput p1, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->mDelayed_time:I

    return-void
.end method

.method static synthetic access$2(Lcom/konka/mm/photo/AutoShowPicActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/mm/photo/AutoShowPicActivity;->set4K2KPicture()V

    return-void
.end method

.method static synthetic access$3(Lcom/konka/mm/photo/AutoShowPicActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/mm/photo/AutoShowPicActivity;->playFirstPhoto()V

    return-void
.end method

.method static synthetic access$4(Lcom/konka/mm/photo/AutoShowPicActivity;)Lcom/konka/mm/photo/ImageViewTouch;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picImageview:Lcom/konka/mm/photo/ImageViewTouch;

    return-object v0
.end method

.method static synthetic access$5(Lcom/konka/mm/photo/AutoShowPicActivity;)Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->stateDrawable:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$6(Lcom/konka/mm/photo/AutoShowPicActivity;Landroid/view/MotionEvent;)F
    .locals 1

    invoke-direct {p0, p1}, Lcom/konka/mm/photo/AutoShowPicActivity;->spacing(Landroid/view/MotionEvent;)F

    move-result v0

    return v0
.end method

.method static synthetic access$7(Lcom/konka/mm/photo/AutoShowPicActivity;Landroid/graphics/PointF;Landroid/view/MotionEvent;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/konka/mm/photo/AutoShowPicActivity;->midPoint(Landroid/graphics/PointF;Landroid/view/MotionEvent;)V

    return-void
.end method

.method static synthetic access$8(Lcom/konka/mm/photo/AutoShowPicActivity;Landroid/content/Intent;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/mm/photo/AutoShowPicActivity;->onReceiveSdCardBroadCast(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic access$9(Lcom/konka/mm/photo/AutoShowPicActivity;)Z
    .locals 1

    invoke-direct {p0}, Lcom/konka/mm/photo/AutoShowPicActivity;->initBitmap()Z

    move-result v0

    return v0
.end method

.method private checkImageSize(Landroid/graphics/BitmapFactory$Options;)Z
    .locals 6
    .param p1    # Landroid/graphics/BitmapFactory$Options;

    const-wide/32 v0, 0x5f5e100

    iget v4, p1, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iget v5, p1, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    mul-int/2addr v4, v5

    mul-int/lit8 v4, v4, 0x4

    int-to-long v2, v4

    cmp-long v4, v2, v0

    if-gtz v4, :cond_0

    const/4 v4, 0x1

    :goto_0
    return v4

    :cond_0
    const/4 v4, 0x0

    goto :goto_0
.end method

.method private closeSilently(Ljava/io/Closeable;)V
    .locals 1
    .param p1    # Ljava/io/Closeable;

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    :try_start_0
    invoke-interface {p1}, Ljava/io/Closeable;->close()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private decodeBitmapFailed(I)V
    .locals 1
    .param p1    # I

    new-instance v0, Lcom/konka/mm/photo/AutoShowPicActivity$5;

    invoke-direct {v0, p0}, Lcom/konka/mm/photo/AutoShowPicActivity$5;-><init>(Lcom/konka/mm/photo/AutoShowPicActivity;)V

    invoke-virtual {p0, v0}, Lcom/konka/mm/photo/AutoShowPicActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method private decodeBitmapFromLocal(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 8
    .param p1    # Ljava/lang/String;

    const/4 v7, 0x0

    const/4 v0, 0x0

    new-instance v4, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v4}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    sput-object v4, Lcom/konka/mm/photo/AutoShowPicActivity;->options:Landroid/graphics/BitmapFactory$Options;

    :try_start_0
    iget-object v4, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->mFileInputStream:Ljava/io/FileInputStream;

    invoke-direct {p0, v4}, Lcom/konka/mm/photo/AutoShowPicActivity;->closeSilently(Ljava/io/Closeable;)V

    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, p1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    iput-object v4, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->mFileInputStream:Ljava/io/FileInputStream;

    iget-object v4, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->mFileInputStream:Ljava/io/FileInputStream;

    invoke-virtual {v4}, Ljava/io/FileInputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v3

    if-nez v3, :cond_0

    iget-object v4, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->mFileInputStream:Ljava/io/FileInputStream;

    invoke-direct {p0, v4}, Lcom/konka/mm/photo/AutoShowPicActivity;->closeSilently(Ljava/io/Closeable;)V

    const/4 v4, 0x1

    invoke-direct {p0, v4}, Lcom/konka/mm/photo/AutoShowPicActivity;->decodeBitmapFailed(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    iget-object v4, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->mFileInputStream:Ljava/io/FileInputStream;

    invoke-direct {p0, v4}, Lcom/konka/mm/photo/AutoShowPicActivity;->closeSilently(Ljava/io/Closeable;)V

    :goto_1
    return-object v7

    :cond_0
    :try_start_1
    sget-object v4, Lcom/konka/mm/photo/AutoShowPicActivity;->options:Landroid/graphics/BitmapFactory$Options;

    const/4 v5, 0x0

    iput-boolean v5, v4, Landroid/graphics/BitmapFactory$Options;->inPurgeable:Z

    sget-object v4, Lcom/konka/mm/photo/AutoShowPicActivity;->options:Landroid/graphics/BitmapFactory$Options;

    const/4 v5, 0x1

    iput-boolean v5, v4, Landroid/graphics/BitmapFactory$Options;->inInputShareable:Z

    sget-object v4, Lcom/konka/mm/photo/AutoShowPicActivity;->options:Landroid/graphics/BitmapFactory$Options;

    const/4 v5, 0x1

    iput-boolean v5, v4, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    const/4 v4, 0x0

    sget-object v5, Lcom/konka/mm/photo/AutoShowPicActivity;->options:Landroid/graphics/BitmapFactory$Options;

    invoke-static {v3, v4, v5}, Landroid/graphics/BitmapFactory;->decodeFileDescriptor(Ljava/io/FileDescriptor;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    const-string v4, "AutoShowPicActivity"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "options "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v6, Lcom/konka/mm/photo/AutoShowPicActivity;->options:Landroid/graphics/BitmapFactory$Options;

    iget v6, v6, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/konka/mm/photo/AutoShowPicActivity;->options:Landroid/graphics/BitmapFactory$Options;

    iget v6, v6, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v4, 0x0

    sput-boolean v4, Lcom/konka/mm/photo/AutoShowPicActivity;->is4K2KMode:Z

    sget-boolean v4, Lcom/konka/mm/photo/AutoShowPicActivity;->isSupport4K2K:Z

    if-eqz v4, :cond_1

    sget-object v4, Lcom/konka/mm/photo/AutoShowPicActivity;->options:Landroid/graphics/BitmapFactory$Options;

    iget v4, v4, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    const/16 v5, 0x870

    if-lt v4, v5, :cond_1

    sget-object v4, Lcom/konka/mm/photo/AutoShowPicActivity;->options:Landroid/graphics/BitmapFactory$Options;

    iget v4, v4, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    const/16 v5, 0xf00

    if-lt v4, v5, :cond_1

    invoke-virtual {p0, p1}, Lcom/konka/mm/photo/AutoShowPicActivity;->isNeed4K2KMode(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    const-string v4, "AutoShowPicActivity"

    const-string v5, "====================is4K2KMode = true================"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v4, 0x1

    sput-boolean v4, Lcom/konka/mm/photo/AutoShowPicActivity;->is4K2KMode:Z

    iget-object v4, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->mFileInputStream:Ljava/io/FileInputStream;

    invoke-direct {p0, v4}, Lcom/konka/mm/photo/AutoShowPicActivity;->closeSilently(Ljava/io/Closeable;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v1

    :try_start_2
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    iget-object v4, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->mFileInputStream:Ljava/io/FileInputStream;

    invoke-direct {p0, v4}, Lcom/konka/mm/photo/AutoShowPicActivity;->closeSilently(Ljava/io/Closeable;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :try_start_4
    iget-object v4, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->mFileInputStream:Ljava/io/FileInputStream;

    invoke-direct {p0, v4}, Lcom/konka/mm/photo/AutoShowPicActivity;->closeSilently(Ljava/io/Closeable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :goto_2
    iget-object v4, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->mFileInputStream:Ljava/io/FileInputStream;

    invoke-direct {p0, v4}, Lcom/konka/mm/photo/AutoShowPicActivity;->closeSilently(Ljava/io/Closeable;)V

    goto :goto_1

    :catch_1
    move-exception v2

    :try_start_5
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :try_start_6
    iget-object v4, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->mFileInputStream:Ljava/io/FileInputStream;

    invoke-direct {p0, v4}, Lcom/konka/mm/photo/AutoShowPicActivity;->closeSilently(Ljava/io/Closeable;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v4

    iget-object v5, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->mFileInputStream:Ljava/io/FileInputStream;

    invoke-direct {p0, v5}, Lcom/konka/mm/photo/AutoShowPicActivity;->closeSilently(Ljava/io/Closeable;)V

    throw v4

    :catchall_1
    move-exception v4

    :try_start_7
    iget-object v5, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->mFileInputStream:Ljava/io/FileInputStream;

    invoke-direct {p0, v5}, Lcom/konka/mm/photo/AutoShowPicActivity;->closeSilently(Ljava/io/Closeable;)V

    throw v4
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :cond_1
    iget-object v4, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->mFileInputStream:Ljava/io/FileInputStream;

    invoke-direct {p0, v4}, Lcom/konka/mm/photo/AutoShowPicActivity;->closeSilently(Ljava/io/Closeable;)V

    goto/16 :goto_1
.end method

.method public static ensureGLCompatibleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 3
    .param p0    # Landroid/graphics/Bitmap;

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v1

    if-eqz v1, :cond_1

    :cond_0
    move-object v0, p0

    :goto_0
    return-object v0

    :cond_1
    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->recycle()V

    invoke-static {}, Ljava/lang/System;->gc()V

    goto :goto_0
.end method

.method protected static hideDialog(I)V
    .locals 1
    .param p0    # I

    const/16 v0, 0x15

    if-ne p0, v0, :cond_0

    sget-object v0, Lcom/konka/mm/photo/AutoShowPicActivity;->progressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/konka/mm/photo/AutoShowPicActivity;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    const/4 v0, 0x0

    sput-boolean v0, Lcom/konka/mm/photo/AutoShowPicActivity;->isProgressDlg:Z

    :cond_0
    return-void
.end method

.method private initBitmap()Z
    .locals 4

    const/4 v3, 0x0

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->bitmapArray:Lcom/konka/mm/photo/RotateBitmap;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->bitmapArray:Lcom/konka/mm/photo/RotateBitmap;

    invoke-virtual {v1}, Lcom/konka/mm/photo/RotateBitmap;->recycle()V

    invoke-static {}, Ljava/lang/System;->gc()V

    :cond_0
    iget-object v1, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picPaths:Ljava/util/ArrayList;

    iget v2, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->currentNum:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/konka/mm/photo/AutoShowPicActivity;->decodeBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    sget-boolean v1, Lcom/konka/mm/photo/AutoShowPicActivity;->is4K2KMode:Z

    if-eqz v1, :cond_2

    if-nez v0, :cond_2

    const-string v1, "AutoShowPicActivity"

    const-string v2, "******Draw 4k2k photo******"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->handler:Landroid/os/Handler;

    const/16 v2, 0xa1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    sget-boolean v1, Lcom/konka/mm/photo/AutoShowPicActivity;->panel4k2kmode:Z

    if-nez v1, :cond_1

    const/4 v1, 0x1

    sput-boolean v1, Lcom/konka/mm/photo/AutoShowPicActivity;->panel4k2kmode:Z

    sget-object v1, Lcom/mstar/android/MDisplay$PanelMode;->E_PANELMODE_4K2K_15HZ:Lcom/mstar/android/MDisplay$PanelMode;

    invoke-static {v1}, Lcom/mstar/android/MDisplay;->setPanelMode(Lcom/mstar/android/MDisplay$PanelMode;)V

    const-string v1, "AutoShowPicActivity"

    const-string v2, "==================PanelMode.E_PANELMODE_4K2K_15HZ================"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_0
    return v3

    :cond_2
    sget-boolean v1, Lcom/konka/mm/photo/AutoShowPicActivity;->panel4k2kmode:Z

    if-eqz v1, :cond_3

    sput-boolean v3, Lcom/konka/mm/photo/AutoShowPicActivity;->panel4k2kmode:Z

    sget-object v1, Lcom/mstar/android/MDisplay$PanelMode;->E_PANELMODE_NORMAL:Lcom/mstar/android/MDisplay$PanelMode;

    invoke-static {v1}, Lcom/mstar/android/MDisplay;->setPanelMode(Lcom/mstar/android/MDisplay$PanelMode;)V

    const-string v1, "AutoShowPicActivity"

    const-string v2, "==================PanelMode.E_PANELMODE_NORMAL================"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    const/4 v0, 0x0

    invoke-static {}, Ljava/lang/System;->gc()V

    goto :goto_0
.end method

.method private midPoint(Landroid/graphics/PointF;Landroid/view/MotionEvent;)V
    .locals 7
    .param p1    # Landroid/graphics/PointF;
    .param p2    # Landroid/view/MotionEvent;

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/high16 v4, 0x40000000    # 2.0f

    invoke-virtual {p2, v5}, Landroid/view/MotionEvent;->getX(I)F

    move-result v2

    invoke-virtual {p2, v6}, Landroid/view/MotionEvent;->getX(I)F

    move-result v3

    add-float v0, v2, v3

    invoke-virtual {p2, v5}, Landroid/view/MotionEvent;->getY(I)F

    move-result v2

    invoke-virtual {p2, v6}, Landroid/view/MotionEvent;->getY(I)F

    move-result v3

    add-float v1, v2, v3

    div-float v2, v0, v4

    div-float v3, v1, v4

    invoke-virtual {p1, v2, v3}, Landroid/graphics/PointF;->set(FF)V

    return-void
.end method

.method private onReceiveSdCardBroadCast(Landroid/content/Intent;)V
    .locals 3
    .param p1    # Landroid/content/Intent;

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/konka/mm/photo/AutoShowPicActivity;->sourceFileComeFrom:Ljava/lang/String;

    const-string v2, "com.konka.mm.file.come.from.samba"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->root_path:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->root_path:Ljava/lang/String;

    invoke-static {p0, v1}, Lcom/konka/mm/tools/FileTool;->checkUsbExist(Landroid/app/Activity;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/konka/mm/photo/AutoShowPicActivity;->finishActivity()V

    goto :goto_0
.end method

.method private playFirstPhoto()V
    .locals 4

    sget-object v0, Lcom/konka/mm/photo/AutoShowPicActivity;->mThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "AutoShowPicActivity"

    const-string v1, "****playFirstPhoto****mThread.isAlive()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/konka/mm/photo/AutoShowPicActivity;->options:Landroid/graphics/BitmapFactory$Options;

    invoke-virtual {v0}, Landroid/graphics/BitmapFactory$Options;->requestCancelDecode()V

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->handler:Landroid/os/Handler;

    const/16 v1, 0xa

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/konka/mm/photo/AutoShowPicActivity;->parsePhotoThread()V

    goto :goto_0
.end method

.method private set4K2KPicture()V
    .locals 4

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/konka/mm/photo/AutoShowPicActivity;->changBtnShowStatus()V

    iget-object v1, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picPaths:Ljava/util/ArrayList;

    iget v2, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->currentNum:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->mSurfaceView4K2K:Lcom/konka/mm/photo/SurfaceView4K2K;

    invoke-virtual {v1}, Lcom/konka/mm/photo/SurfaceView4K2K;->destroyDrawingCache()V

    iget-object v1, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->mSurfaceView4K2K:Lcom/konka/mm/photo/SurfaceView4K2K;

    invoke-virtual {v1, v0}, Lcom/konka/mm/photo/SurfaceView4K2K;->setImagePath(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->mSurfaceView4K2K:Lcom/konka/mm/photo/SurfaceView4K2K;

    invoke-virtual {v1}, Lcom/konka/mm/photo/SurfaceView4K2K;->drawImage()V

    iget-object v1, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->mSurfaceView4K2K:Lcom/konka/mm/photo/SurfaceView4K2K;

    invoke-virtual {v1, v3}, Lcom/konka/mm/photo/SurfaceView4K2K;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    sput-boolean v3, Lcom/konka/mm/photo/AutoShowPicActivity;->is4K2KMode:Z

    sput-boolean v3, Lcom/konka/mm/photo/AutoShowPicActivity;->panel4k2kmode:Z

    sget-object v1, Lcom/mstar/android/MDisplay$PanelMode;->E_PANELMODE_NORMAL:Lcom/mstar/android/MDisplay$PanelMode;

    invoke-static {v1}, Lcom/mstar/android/MDisplay;->setPanelMode(Lcom/mstar/android/MDisplay$PanelMode;)V

    invoke-virtual {p0}, Lcom/konka/mm/photo/AutoShowPicActivity;->setPhotoView()V

    goto :goto_0
.end method

.method private showController()V
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/konka/mm/photo/AutoShowPicActivity;->changePlayBtnState()V

    sput-boolean v2, Lcom/konka/mm/photo/AutoShowPicActivity;->isCanMove:Z

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picShowHolder:Lcom/konka/mm/photo/PicShowHolder;

    iget-object v0, v0, Lcom/konka/mm/photo/PicShowHolder;->pic_bts_layout:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->mPicControlInAnim:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->title_layout:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->mAlphaIn:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picShowHolder:Lcom/konka/mm/photo/PicShowHolder;

    iget-object v0, v0, Lcom/konka/mm/photo/PicShowHolder;->pic_bts_layout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->title_layout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->stateDrawable:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->isControllerShow:Z

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picImageview:Lcom/konka/mm/photo/ImageViewTouch;

    invoke-virtual {v0, v2}, Lcom/konka/mm/photo/ImageViewTouch;->setFocusable(Z)V

    return-void
.end method

.method private spacing(Landroid/view/MotionEvent;)F
    .locals 6
    .param p1    # Landroid/view/MotionEvent;

    const/4 v5, 0x1

    const/4 v4, 0x0

    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getX(I)F

    move-result v2

    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getX(I)F

    move-result v3

    sub-float v0, v2, v3

    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getY(I)F

    move-result v2

    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getY(I)F

    move-result v3

    sub-float v1, v2, v3

    mul-float v2, v0, v0

    mul-float v3, v1, v1

    add-float/2addr v2, v3

    invoke-static {v2}, Landroid/util/FloatMath;->sqrt(F)F

    move-result v2

    return v2
.end method


# virtual methods
.method public InitData()V
    .locals 6

    const/4 v5, 0x0

    const v3, 0x7f0b002d    # com.konka.mm.R.id.showview

    invoke-virtual {p0, v3}, Lcom/konka/mm/photo/AutoShowPicActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/konka/mm/photo/ImageViewTouch;

    iput-object v3, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picImageview:Lcom/konka/mm/photo/ImageViewTouch;

    iget-object v3, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picImageview:Lcom/konka/mm/photo/ImageViewTouch;

    iget-object v4, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->mAlphaIn:Landroid/view/animation/Animation;

    invoke-virtual {v3, v4}, Lcom/konka/mm/photo/ImageViewTouch;->setAnimation(Landroid/view/animation/Animation;)V

    const v3, 0x7f0b0102    # com.konka.mm.R.id.imageSurfaceView

    invoke-virtual {p0, v3}, Lcom/konka/mm/photo/AutoShowPicActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/konka/mm/photo/SurfaceView4K2K;

    iput-object v3, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->mSurfaceView4K2K:Lcom/konka/mm/photo/SurfaceView4K2K;

    const v3, 0x7f0b00d7    # com.konka.mm.R.id.textview1

    invoke-virtual {p0, v3}, Lcom/konka/mm/photo/AutoShowPicActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->textView:Landroid/widget/TextView;

    const v3, 0x7f0b00d6    # com.konka.mm.R.id.titil_layout

    invoke-virtual {p0, v3}, Lcom/konka/mm/photo/AutoShowPicActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    iput-object v3, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->title_layout:Landroid/widget/RelativeLayout;

    const v3, 0x7f0b00da    # com.konka.mm.R.id.state_image

    invoke-virtual {p0, v3}, Lcom/konka/mm/photo/AutoShowPicActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->stateDrawable:Landroid/widget/ImageView;

    const v3, 0x7f0b00db    # com.konka.mm.R.id.scale_btn

    invoke-virtual {p0, v3}, Lcom/konka/mm/photo/AutoShowPicActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->scaleImageView:Landroid/widget/ImageView;

    const v3, 0x7f0b002c    # com.konka.mm.R.id.layout1

    invoke-virtual {p0, v3}, Lcom/konka/mm/photo/AutoShowPicActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    iput-object v3, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->layout1:Landroid/widget/LinearLayout;

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picPaths:Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/konka/mm/photo/AutoShowPicActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {p0}, Lcom/konka/mm/photo/AutoShowPicActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    check-cast v2, Lcom/konka/mm/GlobalData;

    const-string v3, "com.konka.mm.file.where.come.from"

    const-string v4, "com.konka.mm.file.come.from.other"

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    sput-object v3, Lcom/konka/mm/photo/AutoShowPicActivity;->sourceFileComeFrom:Ljava/lang/String;

    sget-object v3, Lcom/konka/mm/photo/AutoShowPicActivity;->sourceFileComeFrom:Ljava/lang/String;

    const-string v4, "com.konka.mm.file.come.from.filemanager"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "com.konka.mm.file.parent.path"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->parentPath:Ljava/lang/String;

    const-string v3, "com.konka.mm.file.index.posstion"

    invoke-virtual {v0, v3, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    iput v3, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->currentNum:I

    const-string v3, "com.konka.mm.file.root.path"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->root_path:Ljava/lang/String;

    invoke-virtual {v2}, Lcom/konka/mm/GlobalData;->getmMMFileList()Ljava/util/ArrayList;

    move-result-object v3

    iput-object v3, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picPaths:Ljava/util/ArrayList;

    :goto_0
    iget-object v3, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picImageview:Lcom/konka/mm/photo/ImageViewTouch;

    new-instance v4, Lcom/konka/mm/photo/AutoShowPicActivity$4;

    invoke-direct {v4, p0}, Lcom/konka/mm/photo/AutoShowPicActivity$4;-><init>(Lcom/konka/mm/photo/AutoShowPicActivity;)V

    invoke-virtual {v3, v4}, Lcom/konka/mm/photo/ImageViewTouch;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    return-void

    :cond_0
    sget-object v3, Lcom/konka/mm/photo/AutoShowPicActivity;->sourceFileComeFrom:Ljava/lang/String;

    const-string v4, "com.konka.mm.file.come.from.samba"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "com.konka.mm.file.index.posstion"

    invoke-virtual {v0, v3, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    iput v3, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->currentNum:I

    invoke-virtual {v2}, Lcom/konka/mm/GlobalData;->getmMMFileList()Ljava/util/ArrayList;

    move-result-object v3

    iput-object v3, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picPaths:Ljava/util/ArrayList;

    const-string v3, "com.konka.mm.samba.current.ip"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->currentIp:Ljava/lang/String;

    goto :goto_0

    :cond_1
    sget-object v3, Lcom/konka/mm/photo/AutoShowPicActivity;->sourceFileComeFrom:Ljava/lang/String;

    const-string v4, "com.konka.mm.file.come.from.one.disk"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    const-string v3, "com.konka.mm.file.index.posstion"

    invoke-virtual {v0, v3, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    iput v3, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->currentNum:I

    const-string v3, "com.konka.mm.file.root.path"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->root_path:Ljava/lang/String;

    invoke-virtual {v2}, Lcom/konka/mm/GlobalData;->getmMMFileList()Ljava/util/ArrayList;

    move-result-object v3

    iput-object v3, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picPaths:Ljava/util/ArrayList;

    goto :goto_0

    :cond_2
    const-string v3, "com.konka.mm.file.index.posstion"

    invoke-virtual {v0, v3, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    iput v3, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->currentNum:I

    const-string v3, "com.konka.mm.file.root.path"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->root_path:Ljava/lang/String;

    const-string v3, "com.konka.mm.file.all.lists"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    iput-object v3, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picPaths:Ljava/util/ArrayList;

    goto :goto_0
.end method

.method public changBtnShowStatus()V
    .locals 3

    const/16 v2, 0x8

    const/4 v1, 0x0

    sget-boolean v0, Lcom/konka/mm/photo/AutoShowPicActivity;->is4K2KMode:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picShowHolder:Lcom/konka/mm/photo/PicShowHolder;

    iget-object v0, v0, Lcom/konka/mm/photo/PicShowHolder;->layout_gap:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picShowHolder:Lcom/konka/mm/photo/PicShowHolder;

    iget-object v0, v0, Lcom/konka/mm/photo/PicShowHolder;->bt_leftRotion:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picShowHolder:Lcom/konka/mm/photo/PicShowHolder;

    iget-object v0, v0, Lcom/konka/mm/photo/PicShowHolder;->bt_rightRotion:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picShowHolder:Lcom/konka/mm/photo/PicShowHolder;

    iget-object v0, v0, Lcom/konka/mm/photo/PicShowHolder;->bt_big:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picShowHolder:Lcom/konka/mm/photo/PicShowHolder;

    iget-object v0, v0, Lcom/konka/mm/photo/PicShowHolder;->bt_small:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picShowHolder:Lcom/konka/mm/photo/PicShowHolder;

    iget-object v0, v0, Lcom/konka/mm/photo/PicShowHolder;->bt_galleryShow:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picShowHolder:Lcom/konka/mm/photo/PicShowHolder;

    iget-object v0, v0, Lcom/konka/mm/photo/PicShowHolder;->bt_wallpaper:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picShowHolder:Lcom/konka/mm/photo/PicShowHolder;

    iget-object v0, v0, Lcom/konka/mm/photo/PicShowHolder;->bt_fullScreen:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picImageview:Lcom/konka/mm/photo/ImageViewTouch;

    invoke-virtual {v0, v2}, Lcom/konka/mm/photo/ImageViewTouch;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->mSurfaceView4K2K:Lcom/konka/mm/photo/SurfaceView4K2K;

    invoke-virtual {v0, v1}, Lcom/konka/mm/photo/SurfaceView4K2K;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picShowHolder:Lcom/konka/mm/photo/PicShowHolder;

    iget-object v0, v0, Lcom/konka/mm/photo/PicShowHolder;->layout_gap:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picShowHolder:Lcom/konka/mm/photo/PicShowHolder;

    iget-object v0, v0, Lcom/konka/mm/photo/PicShowHolder;->bt_leftRotion:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picShowHolder:Lcom/konka/mm/photo/PicShowHolder;

    iget-object v0, v0, Lcom/konka/mm/photo/PicShowHolder;->bt_rightRotion:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picShowHolder:Lcom/konka/mm/photo/PicShowHolder;

    iget-object v0, v0, Lcom/konka/mm/photo/PicShowHolder;->bt_big:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picShowHolder:Lcom/konka/mm/photo/PicShowHolder;

    iget-object v0, v0, Lcom/konka/mm/photo/PicShowHolder;->bt_small:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picShowHolder:Lcom/konka/mm/photo/PicShowHolder;

    iget-object v0, v0, Lcom/konka/mm/photo/PicShowHolder;->bt_galleryShow:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picShowHolder:Lcom/konka/mm/photo/PicShowHolder;

    iget-object v0, v0, Lcom/konka/mm/photo/PicShowHolder;->bt_wallpaper:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picShowHolder:Lcom/konka/mm/photo/PicShowHolder;

    iget-object v0, v0, Lcom/konka/mm/photo/PicShowHolder;->bt_fullScreen:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->mSurfaceView4K2K:Lcom/konka/mm/photo/SurfaceView4K2K;

    invoke-virtual {v0, v2}, Lcom/konka/mm/photo/SurfaceView4K2K;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picImageview:Lcom/konka/mm/photo/ImageViewTouch;

    invoke-virtual {v0, v1}, Lcom/konka/mm/photo/ImageViewTouch;->setVisibility(I)V

    goto :goto_0
.end method

.method public changePlayBtnState()V
    .locals 4

    const/16 v3, 0x8

    const/4 v2, 0x1

    const/4 v1, 0x0

    sget-boolean v0, Lcom/konka/mm/photo/AutoShowPicActivity;->isAutoPlaying:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picShowHolder:Lcom/konka/mm/photo/PicShowHolder;

    iget-object v0, v0, Lcom/konka/mm/photo/PicShowHolder;->bt_autoPlay:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picShowHolder:Lcom/konka/mm/photo/PicShowHolder;

    iget-object v0, v0, Lcom/konka/mm/photo/PicShowHolder;->bt_Pause:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picShowHolder:Lcom/konka/mm/photo/PicShowHolder;

    iget-object v0, v0, Lcom/konka/mm/photo/PicShowHolder;->bt_autoPlay:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picShowHolder:Lcom/konka/mm/photo/PicShowHolder;

    iget-object v0, v0, Lcom/konka/mm/photo/PicShowHolder;->bt_autoPlay:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picShowHolder:Lcom/konka/mm/photo/PicShowHolder;

    iget-object v0, v0, Lcom/konka/mm/photo/PicShowHolder;->bt_Pause:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picShowHolder:Lcom/konka/mm/photo/PicShowHolder;

    iget-object v0, v0, Lcom/konka/mm/photo/PicShowHolder;->bt_autoPlay:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picShowHolder:Lcom/konka/mm/photo/PicShowHolder;

    iget-object v0, v0, Lcom/konka/mm/photo/PicShowHolder;->bt_Pause:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picShowHolder:Lcom/konka/mm/photo/PicShowHolder;

    iget-object v0, v0, Lcom/konka/mm/photo/PicShowHolder;->bt_Pause:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    goto :goto_0
.end method

.method public changePlayOrderBtnState()V
    .locals 4

    const/16 v3, 0x8

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->isNeetCirculate:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picShowHolder:Lcom/konka/mm/photo/PicShowHolder;

    iget-object v0, v0, Lcom/konka/mm/photo/PicShowHolder;->bt_circulate:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picShowHolder:Lcom/konka/mm/photo/PicShowHolder;

    iget-object v0, v0, Lcom/konka/mm/photo/PicShowHolder;->bt_suquence:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picShowHolder:Lcom/konka/mm/photo/PicShowHolder;

    iget-object v0, v0, Lcom/konka/mm/photo/PicShowHolder;->bt_circulate:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picShowHolder:Lcom/konka/mm/photo/PicShowHolder;

    iget-object v0, v0, Lcom/konka/mm/photo/PicShowHolder;->bt_circulate:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picShowHolder:Lcom/konka/mm/photo/PicShowHolder;

    iget-object v0, v0, Lcom/konka/mm/photo/PicShowHolder;->bt_suquence:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picShowHolder:Lcom/konka/mm/photo/PicShowHolder;

    iget-object v0, v0, Lcom/konka/mm/photo/PicShowHolder;->bt_circulate:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picShowHolder:Lcom/konka/mm/photo/PicShowHolder;

    iget-object v0, v0, Lcom/konka/mm/photo/PicShowHolder;->bt_suquence:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picShowHolder:Lcom/konka/mm/photo/PicShowHolder;

    iget-object v0, v0, Lcom/konka/mm/photo/PicShowHolder;->bt_suquence:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    goto :goto_0
.end method

.method public decodeBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 3
    .param p1    # Ljava/lang/String;

    const-string v0, "AutoShowPicActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "===============decodeBitmap, url : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, p1}, Lcom/konka/mm/photo/AutoShowPicActivity;->decodeBitmapFromLocal(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 3
    .param p1    # Landroid/view/KeyEvent;

    const/4 v0, 0x0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_2

    iget-object v1, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picShowHolder:Lcom/konka/mm/photo/PicShowHolder;

    iget-object v1, v1, Lcom/konka/mm/photo/PicShowHolder;->pic_bts_layout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1}, Landroid/widget/RelativeLayout;->isShown()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/konka/mm/photo/AutoShowPicActivity;->hideController()V

    :goto_0
    return v0

    :cond_0
    sget-boolean v1, Lcom/konka/mm/photo/AutoShowPicActivity;->isCanMove:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picShowHolder:Lcom/konka/mm/photo/PicShowHolder;

    iget-object v1, v1, Lcom/konka/mm/photo/PicShowHolder;->bt_fullScreen:Landroid/widget/Button;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v1, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picShowHolder:Lcom/konka/mm/photo/PicShowHolder;

    iget-object v1, v1, Lcom/konka/mm/photo/PicShowHolder;->bt_fullScreen:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->requestFocus()Z

    invoke-direct {p0}, Lcom/konka/mm/photo/AutoShowPicActivity;->showController()V

    goto :goto_0

    :cond_1
    iput-boolean v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->isEnterOtherAct:Z

    invoke-virtual {p0}, Lcom/konka/mm/photo/AutoShowPicActivity;->finishActivity()V

    goto :goto_0

    :cond_2
    invoke-super {p0, p1}, Landroid/app/Activity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public finishActivity()V
    .locals 2

    const/4 v1, 0x0

    sget-boolean v0, Lcom/konka/mm/photo/AutoShowPicActivity;->is4K2KMode:Z

    if-eqz v0, :cond_0

    sput-boolean v1, Lcom/konka/mm/photo/AutoShowPicActivity;->is4K2KMode:Z

    sput-boolean v1, Lcom/konka/mm/photo/AutoShowPicActivity;->panel4k2kmode:Z

    sget-object v0, Lcom/mstar/android/MDisplay$PanelMode;->E_PANELMODE_NORMAL:Lcom/mstar/android/MDisplay$PanelMode;

    invoke-static {v0}, Lcom/mstar/android/MDisplay;->setPanelMode(Lcom/mstar/android/MDisplay$PanelMode;)V

    :cond_0
    invoke-virtual {p0}, Lcom/konka/mm/photo/AutoShowPicActivity;->finish()V

    return-void
.end method

.method public galleryViewShow()V
    .locals 5

    const/4 v4, 0x0

    sget-object v2, Lcom/konka/mm/photo/AutoShowPicActivity;->sourceFileComeFrom:Ljava/lang/String;

    const-string v3, "com.konka.mm.file.come.from.samba"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->currentIp:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->currentIp:Ljava/lang/String;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/konka/mm/samba/SmbClient;->pingHost(Ljava/lang/String;I)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    iget-object v2, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picShowHolder:Lcom/konka/mm/photo/PicShowHolder;

    iget-object v2, v2, Lcom/konka/mm/photo/PicShowHolder;->toast_not_connect:Landroid/widget/Toast;

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    :goto_0
    return-void

    :cond_1
    sput-boolean v4, Lcom/konka/mm/photo/AutoShowPicActivity;->isAutoPlaying:Z

    iput v4, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->rotationClickCount:I

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-class v2, Lcom/konka/mm/photo/GalleryShowPicActivity;

    invoke-virtual {v1, p0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "com.konka.mm.file.index.posstion"

    iget v3, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->currentNum:I

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "com.konka.mm.file.root.path"

    iget-object v3, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->root_path:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "com.konka.mm.file.where.come.from"

    sget-object v3, Lcom/konka/mm/photo/AutoShowPicActivity;->sourceFileComeFrom:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v2, Lcom/konka/mm/photo/AutoShowPicActivity;->sourceFileComeFrom:Ljava/lang/String;

    const-string v3, "com.konka.mm.file.come.from.samba"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "com.konka.mm.samba.current.ip"

    iget-object v3, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->currentIp:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    invoke-virtual {p0, v1, v4}, Lcom/konka/mm/photo/AutoShowPicActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method public getState()I
    .locals 1

    iget v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->state:I

    return v0
.end method

.method public hideControlDelay()V
    .locals 4

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->hideHandler:Landroid/os/Handler;

    const/16 v1, 0x11

    const-wide/16 v2, 0x3a98

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    return-void
.end method

.method public hideController()V
    .locals 3

    const/16 v2, 0x8

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picShowHolder:Lcom/konka/mm/photo/PicShowHolder;

    iget-object v0, v0, Lcom/konka/mm/photo/PicShowHolder;->pic_bts_layout:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->mPicControlOutAnim:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->title_layout:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->mAlphaOut:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picShowHolder:Lcom/konka/mm/photo/PicShowHolder;

    iget-object v0, v0, Lcom/konka/mm/photo/PicShowHolder;->pic_bts_layout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->title_layout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->isControllerShow:Z

    sget-boolean v0, Lcom/konka/mm/photo/AutoShowPicActivity;->isAutoPlaying:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->playStateDrawable:[I

    const/4 v1, 0x1

    aget v0, v0, v1

    invoke-virtual {p0, v0}, Lcom/konka/mm/photo/AutoShowPicActivity;->setStateDrawable(I)V

    :cond_0
    return-void
.end method

.method public isAutoPlaying()Z
    .locals 1

    sget-boolean v0, Lcom/konka/mm/photo/AutoShowPicActivity;->isAutoPlaying:Z

    return v0
.end method

.method public isNeed4K2KMode(Ljava/lang/String;)Z
    .locals 11
    .param p1    # Ljava/lang/String;

    const-wide/16 v9, 0x400

    const/4 v6, 0x1

    const/4 v5, 0x0

    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v7

    div-long/2addr v7, v9

    div-long/2addr v7, v9

    const-wide/16 v9, 0x64

    cmp-long v7, v7, v9

    if-lez v7, :cond_1

    :cond_0
    :goto_0
    return v5

    :cond_1
    new-instance v4, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v4}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    :try_start_0
    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, p1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/FileInputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v7, 0x0

    iput-boolean v7, v4, Landroid/graphics/BitmapFactory$Options;->inPurgeable:Z

    const/4 v7, 0x1

    iput-boolean v7, v4, Landroid/graphics/BitmapFactory$Options;->inInputShareable:Z

    const/4 v7, 0x1

    iput-boolean v7, v4, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    const/4 v7, 0x0

    invoke-static {v1, v7, v4}, Landroid/graphics/BitmapFactory;->decodeFileDescriptor(Ljava/io/FileDescriptor;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    const-string v7, "AutoShowPicActivity"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "options "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v9, v4, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, v4, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, v4}, Lcom/konka/mm/photo/AutoShowPicActivity;->checkImageSize(Landroid/graphics/BitmapFactory$Options;)Z

    move-result v7

    if-nez v7, :cond_2

    invoke-direct {p0, v3}, Lcom/konka/mm/photo/AutoShowPicActivity;->closeSilently(Ljava/io/Closeable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :cond_2
    move v5, v6

    goto :goto_0
.end method

.method protected moveNextOrPrevious(I)V
    .locals 6
    .param p1    # I

    const/4 v3, 0x0

    sget-object v1, Lcom/konka/mm/photo/AutoShowPicActivity;->mThread:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->isAlive()Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v1, Lcom/konka/mm/photo/AutoShowPicActivity;->sourceFileComeFrom:Ljava/lang/String;

    const-string v2, "com.konka.mm.file.come.from.samba"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->currentIp:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->currentIp:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/konka/mm/samba/SmbClient;->pingHost(Ljava/lang/String;I)Z

    move-result v1

    if-nez v1, :cond_2

    :cond_1
    iget-object v1, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picShowHolder:Lcom/konka/mm/photo/PicShowHolder;

    iget-object v1, v1, Lcom/konka/mm/photo/PicShowHolder;->toast_not_connect:Landroid/widget/Toast;

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_2
    iput v3, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->zoomTimes:I

    invoke-virtual {p0}, Lcom/konka/mm/photo/AutoShowPicActivity;->setScaleDrawable()V

    iget v1, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->currentNum:I

    add-int/2addr v1, p1

    iput v1, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->currentNum:I

    iget v1, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->currentNum:I

    const/4 v2, -0x1

    if-gt v1, v2, :cond_5

    iget-boolean v1, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->isNeetCirculate:Z

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picPaths:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->currentNum:I

    :cond_3
    :goto_1
    iget-object v1, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picImageview:Lcom/konka/mm/photo/ImageViewTouch;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/konka/mm/photo/ImageViewTouch;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v1, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picImageview:Lcom/konka/mm/photo/ImageViewTouch;

    invoke-virtual {v1}, Lcom/konka/mm/photo/ImageViewTouch;->clear()V

    iget-object v2, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picPaths:Ljava/util/ArrayList;

    monitor-enter v2

    :try_start_0
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picPaths:Ljava/util/ArrayList;

    iget v3, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->currentNum:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_8

    iget-object v3, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->toast_file_not_exist:Landroid/widget/Toast;

    new-instance v4, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picPaths:Ljava/util/ArrayList;

    iget v5, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->currentNum:I

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, " "

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/konka/mm/photo/AutoShowPicActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090010    # com.konka.mm.R.string.file_not_exist

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->toast_file_not_exist:Landroid/widget/Toast;

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    monitor-exit v2

    goto/16 :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    :cond_4
    iput v3, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->currentNum:I

    iget-object v1, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picShowHolder:Lcom/konka/mm/photo/PicShowHolder;

    iget-object v1, v1, Lcom/konka/mm/photo/PicShowHolder;->toast_first_image:Landroid/widget/Toast;

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    sput-boolean v3, Lcom/konka/mm/photo/AutoShowPicActivity;->isAutoPlaying:Z

    iget-object v1, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picShowHolder:Lcom/konka/mm/photo/PicShowHolder;

    iget-object v1, v1, Lcom/konka/mm/photo/PicShowHolder;->toast_first_image:Landroid/widget/Toast;

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    :cond_5
    iget v1, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->currentNum:I

    iget-object v2, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picPaths:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v1, v2, :cond_3

    iget-boolean v1, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->isNeetCirculate:Z

    if-eqz v1, :cond_6

    iput v3, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->currentNum:I

    goto/16 :goto_1

    :cond_6
    iget-object v1, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picPaths:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->currentNum:I

    iget-object v1, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picShowHolder:Lcom/konka/mm/photo/PicShowHolder;

    iget-object v1, v1, Lcom/konka/mm/photo/PicShowHolder;->toast_last_image:Landroid/widget/Toast;

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    sput-boolean v3, Lcom/konka/mm/photo/AutoShowPicActivity;->isAutoPlaying:Z

    iget-object v1, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picShowHolder:Lcom/konka/mm/photo/PicShowHolder;

    iget-object v1, v1, Lcom/konka/mm/photo/PicShowHolder;->toast_last_image:Landroid/widget/Toast;

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    iget-boolean v1, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->isControllerShow:Z

    if-nez v1, :cond_7

    invoke-direct {p0}, Lcom/konka/mm/photo/AutoShowPicActivity;->showController()V

    :cond_7
    invoke-virtual {p0}, Lcom/konka/mm/photo/AutoShowPicActivity;->changePlayBtnState()V

    goto/16 :goto_0

    :cond_8
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {p0}, Lcom/konka/mm/photo/AutoShowPicActivity;->parsePhotoThread()V

    goto/16 :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 4
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    packed-switch p2, :pswitch_data_0

    :goto_0
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    :goto_1
    return-void

    :pswitch_0
    sget-object v2, Lcom/konka/mm/photo/AutoShowPicActivity;->sourceFileComeFrom:Ljava/lang/String;

    const-string v3, "com.konka.mm.file.come.from.samba"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->currentIp:Ljava/lang/String;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/konka/mm/samba/SmbClient;->pingHost(Ljava/lang/String;I)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p0}, Lcom/konka/mm/photo/AutoShowPicActivity;->finishActivity()V

    goto :goto_1

    :cond_0
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "currentNum"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->currentNum:I

    iget-object v2, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picPaths:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p0}, Lcom/konka/mm/photo/AutoShowPicActivity;->finishActivity()V

    goto :goto_1

    :cond_1
    iget v2, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->currentNum:I

    if-gt v1, v2, :cond_2

    add-int/lit8 v2, v1, -0x1

    iput v2, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->currentNum:I

    :cond_2
    invoke-virtual {p0}, Lcom/konka/mm/photo/AutoShowPicActivity;->parsePhotoThread()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1    # Landroid/os/Bundle;

    const/16 v5, 0x400

    const/4 v7, 0x0

    const/4 v6, 0x1

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0, v6}, Lcom/konka/mm/photo/AutoShowPicActivity;->requestWindowFeature(I)Z

    invoke-virtual {p0}, Lcom/konka/mm/photo/AutoShowPicActivity;->getWindow()Landroid/view/Window;

    move-result-object v4

    invoke-virtual {v4, v5, v5}, Landroid/view/Window;->setFlags(II)V

    const v4, 0x7f030033    # com.konka.mm.R.layout.showpiclayout1

    invoke-virtual {p0, v4}, Lcom/konka/mm/photo/AutoShowPicActivity;->setContentView(I)V

    new-instance v4, Lcom/konka/mm/photo/PicShowHolder;

    invoke-direct {v4, p0}, Lcom/konka/mm/photo/PicShowHolder;-><init>(Lcom/konka/mm/photo/AutoShowPicActivity;)V

    iput-object v4, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picShowHolder:Lcom/konka/mm/photo/PicShowHolder;

    iget-object v4, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picShowHolder:Lcom/konka/mm/photo/PicShowHolder;

    invoke-virtual {v4}, Lcom/konka/mm/photo/PicShowHolder;->findBtnView()V

    iget-object v4, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picShowHolder:Lcom/konka/mm/photo/PicShowHolder;

    iget-object v4, v4, Lcom/konka/mm/photo/PicShowHolder;->bt_galleryShow:Landroid/widget/Button;

    invoke-virtual {v4}, Landroid/widget/Button;->requestFocus()Z

    invoke-static {p0}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v4

    iput-object v4, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    iget-object v4, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v4

    iput-object v4, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    iget-object v4, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/CommonDesk;->getUrsaSelect()Lcom/konka/kkinterface/tv/CommonDesk$UrsaSelectEnumType;

    move-result-object v4

    iput-object v4, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->ursaType:Lcom/konka/kkinterface/tv/CommonDesk$UrsaSelectEnumType;

    iget-object v4, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/CommonDesk;->getCustomerInfo()Lcom/konka/kkinterface/tv/CommonDesk$CustomerInfo;

    move-result-object v4

    iget-object v4, v4, Lcom/konka/kkinterface/tv/CommonDesk$CustomerInfo;->strCustomerName:Ljava/lang/String;

    iput-object v4, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->abroadCustomer:Ljava/lang/String;

    iget-object v4, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->abroadCustomer:Ljava/lang/String;

    const-string v5, "teac"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    iput-boolean v4, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->bTeacCustomer:Z

    iget-object v4, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->ursaType:Lcom/konka/kkinterface/tv/CommonDesk$UrsaSelectEnumType;

    sget-object v5, Lcom/konka/kkinterface/tv/CommonDesk$UrsaSelectEnumType;->SIXM40_IC:Lcom/konka/kkinterface/tv/CommonDesk$UrsaSelectEnumType;

    if-ne v4, v5, :cond_0

    sput-boolean v6, Lcom/konka/mm/photo/AutoShowPicActivity;->isSupport4K2K:Z

    const-string v4, "AutoShowPicActivity"

    const-string v5, "========Support 4k2k MODE play======="

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    invoke-virtual {p0}, Lcom/konka/mm/photo/AutoShowPicActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    check-cast v2, Lcom/konka/mm/GlobalData;

    const/4 v4, -0x1

    invoke-virtual {v2, v4}, Lcom/konka/mm/GlobalData;->setmPicCurrentPos(I)V

    invoke-virtual {p0}, Lcom/konka/mm/photo/AutoShowPicActivity;->InitData()V

    const v4, 0x7f040007    # com.konka.mm.R.anim.pic_control_in

    invoke-static {p0, v4}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v4

    iput-object v4, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->mPicControlInAnim:Landroid/view/animation/Animation;

    const v4, 0x7f040008    # com.konka.mm.R.anim.pic_control_out

    invoke-static {p0, v4}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v4

    iput-object v4, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->mPicControlOutAnim:Landroid/view/animation/Animation;

    const v4, 0x7f040005    # com.konka.mm.R.anim.photo_push_left_in

    invoke-static {p0, v4}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v4

    iput-object v4, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->mPicPushLeftIn:Landroid/view/animation/Animation;

    const v4, 0x7f040006    # com.konka.mm.R.anim.photo_push_right_in

    invoke-static {p0, v4}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v4

    iput-object v4, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->mPicPushRightIn:Landroid/view/animation/Animation;

    const/high16 v4, 0x7f040000    # com.konka.mm.R.anim.alpha_in

    invoke-static {p0, v4}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v4

    iput-object v4, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->mAlphaIn:Landroid/view/animation/Animation;

    const v4, 0x7f040001    # com.konka.mm.R.anim.alpha_out

    invoke-static {p0, v4}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v4

    iput-object v4, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->mAlphaOut:Landroid/view/animation/Animation;

    const v4, 0x7f04000b    # com.konka.mm.R.anim.rotation

    invoke-static {p0, v4}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v4

    iput-object v4, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->mRotation:Landroid/view/animation/Animation;

    iget-object v4, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picImageview:Lcom/konka/mm/photo/ImageViewTouch;

    invoke-virtual {v4, v6}, Lcom/konka/mm/photo/ImageViewTouch;->setEnableTrackballScroll(Z)V

    iget-object v4, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picImageview:Lcom/konka/mm/photo/ImageViewTouch;

    new-instance v5, Lcom/konka/mm/photo/AutoShowPicActivity$ImageViewOnTounchListener;

    invoke-direct {v5, p0}, Lcom/konka/mm/photo/AutoShowPicActivity$ImageViewOnTounchListener;-><init>(Lcom/konka/mm/photo/AutoShowPicActivity;)V

    invoke-virtual {v4, v5}, Lcom/konka/mm/photo/ImageViewTouch;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    invoke-virtual {p0}, Lcom/konka/mm/photo/AutoShowPicActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090010    # com.konka.mm.R.string.file_not_exist

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {p0, v4, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    iput-object v4, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->toast_file_not_exist:Landroid/widget/Toast;

    iget-object v4, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picPaths:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-nez v4, :cond_1

    invoke-virtual {p0}, Lcom/konka/mm/photo/AutoShowPicActivity;->finishActivity()V

    :goto_1
    return-void

    :cond_0
    sput-boolean v7, Lcom/konka/mm/photo/AutoShowPicActivity;->isSupport4K2K:Z

    const-string v4, "AutoShowPicActivity"

    const-string v5, "========don\'t support 4k2k MODE play========"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    iget-object v4, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picPaths:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    iget v5, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->currentNum:I

    if-gt v4, v5, :cond_2

    iget-object v4, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picPaths:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    iput v4, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->currentNum:I

    :cond_2
    invoke-virtual {p0}, Lcom/konka/mm/photo/AutoShowPicActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v3

    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v4

    iput v4, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->screenWidth:I

    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v4

    iput v4, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->screenHeight:I

    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    const-string v4, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v1, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v4, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v1, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v4, "file"

    invoke-virtual {v1, v4}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    new-instance v4, Lcom/konka/mm/photo/AutoShowPicActivity$3;

    invoke-direct {v4, p0}, Lcom/konka/mm/photo/AutoShowPicActivity$3;-><init>(Lcom/konka/mm/photo/AutoShowPicActivity;)V

    iput-object v4, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    iget-object v4, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v4, v1}, Lcom/konka/mm/photo/AutoShowPicActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/konka/mm/photo/AutoShowPicActivity;->registEthernetReceiver()V

    invoke-direct {p0}, Lcom/konka/mm/photo/AutoShowPicActivity;->playFirstPhoto()V

    goto :goto_1
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 4
    .param p1    # I

    const/4 v3, 0x1

    packed-switch p1, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    sput-boolean v3, Lcom/konka/mm/photo/AutoShowPicActivity;->isProgressDlg:Z

    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/konka/mm/photo/AutoShowPicActivity;->progressDialog:Landroid/app/ProgressDialog;

    sget-object v0, Lcom/konka/mm/photo/AutoShowPicActivity;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/konka/mm/photo/AutoShowPicActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090051    # com.konka.mm.R.string.loader

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    sget-object v0, Lcom/konka/mm/photo/AutoShowPicActivity;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    sget-object v0, Lcom/konka/mm/photo/AutoShowPicActivity;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    sget-object v0, Lcom/konka/mm/photo/AutoShowPicActivity;->progressDialog:Landroid/app/ProgressDialog;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x15
        :pswitch_0
    .end packed-switch
.end method

.method protected onDestroy()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    sput-boolean v1, Lcom/konka/mm/photo/AutoShowPicActivity;->isAutoPlaying:Z

    iget-object v1, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->bitmapArray:Lcom/konka/mm/photo/RotateBitmap;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->bitmapArray:Lcom/konka/mm/photo/RotateBitmap;

    invoke-virtual {v1}, Lcom/konka/mm/photo/RotateBitmap;->recycle()V

    :cond_0
    iput-object v2, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picImageview:Lcom/konka/mm/photo/ImageViewTouch;

    iput-object v2, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picPaths:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->handler:Landroid/os/Handler;

    const v2, 0x50000001

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v1, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1}, Lcom/konka/mm/photo/AutoShowPicActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    invoke-virtual {p0}, Lcom/konka/mm/photo/AutoShowPicActivity;->unregistEthernetReceiver()V

    invoke-virtual {p0}, Lcom/konka/mm/photo/AutoShowPicActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/konka/mm/GlobalData;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/konka/mm/GlobalData;->setmPicCurrentPos(I)V

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 6
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/16 v5, 0x8

    const/4 v4, 0x3

    const v3, 0x50000001

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-boolean v2, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->bTeacCustomer:Z

    if-nez v2, :cond_a

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_0
    :goto_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    :goto_1
    return v1

    :sswitch_0
    sget-boolean v2, Lcom/konka/mm/photo/AutoShowPicActivity;->isCanMove:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->isControllerShow:Z

    if-nez v2, :cond_0

    sget-boolean v2, Lcom/konka/mm/photo/AutoShowPicActivity;->isAutoPlaying:Z

    if-nez v2, :cond_0

    const/4 v2, -0x1

    invoke-virtual {p0, v2}, Lcom/konka/mm/photo/AutoShowPicActivity;->moveNextOrPrevious(I)V

    iget-object v2, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->playStateDrawable:[I

    aget v2, v2, v0

    invoke-virtual {p0, v2}, Lcom/konka/mm/photo/AutoShowPicActivity;->setStateDrawable(I)V

    iget-object v2, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picImageview:Lcom/konka/mm/photo/ImageViewTouch;

    invoke-virtual {v2, v0}, Lcom/konka/mm/photo/ImageViewTouch;->playSoundEffect(I)V

    goto :goto_1

    :sswitch_1
    sget-boolean v2, Lcom/konka/mm/photo/AutoShowPicActivity;->isCanMove:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->isControllerShow:Z

    if-nez v2, :cond_0

    sget-boolean v2, Lcom/konka/mm/photo/AutoShowPicActivity;->isAutoPlaying:Z

    if-nez v2, :cond_0

    invoke-virtual {p0, v1}, Lcom/konka/mm/photo/AutoShowPicActivity;->moveNextOrPrevious(I)V

    iget-object v2, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->playStateDrawable:[I

    const/4 v3, 0x2

    aget v2, v2, v3

    invoke-virtual {p0, v2}, Lcom/konka/mm/photo/AutoShowPicActivity;->setStateDrawable(I)V

    iget-object v2, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picImageview:Lcom/konka/mm/photo/ImageViewTouch;

    invoke-virtual {v2, v0}, Lcom/konka/mm/photo/ImageViewTouch;->playSoundEffect(I)V

    goto :goto_1

    :sswitch_2
    iget-boolean v2, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->isControllerShow:Z

    if-nez v2, :cond_0

    sget-boolean v2, Lcom/konka/mm/photo/AutoShowPicActivity;->isCanMove:Z

    if-nez v2, :cond_0

    sget-boolean v2, Lcom/konka/mm/photo/AutoShowPicActivity;->isAutoPlaying:Z

    if-eqz v2, :cond_1

    :goto_2
    sput-boolean v0, Lcom/konka/mm/photo/AutoShowPicActivity;->isAutoPlaying:Z

    sget-boolean v0, Lcom/konka/mm/photo/AutoShowPicActivity;->isAutoPlaying:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->handler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->handler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->playStateDrawable:[I

    aget v0, v0, v1

    invoke-virtual {p0, v0}, Lcom/konka/mm/photo/AutoShowPicActivity;->setStateDrawable(I)V

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->playStateDrawable:[I

    aget v0, v0, v4

    invoke-virtual {p0, v0}, Lcom/konka/mm/photo/AutoShowPicActivity;->setStateDrawable(I)V

    goto :goto_0

    :sswitch_3
    iput v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->rotationClickCount:I

    sput-boolean v0, Lcom/konka/mm/photo/AutoShowPicActivity;->isAutoPlaying:Z

    iput-boolean v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->isEnterOtherAct:Z

    invoke-virtual {p0}, Lcom/konka/mm/photo/AutoShowPicActivity;->finish()V

    goto :goto_0

    :sswitch_4
    iput v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->rotationClickCount:I

    iput v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->rotationClickCount:I

    iput v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->zoomTimes:I

    sget-boolean v2, Lcom/konka/mm/photo/AutoShowPicActivity;->isAutoPlaying:Z

    if-eqz v2, :cond_3

    :goto_3
    sput-boolean v0, Lcom/konka/mm/photo/AutoShowPicActivity;->isAutoPlaying:Z

    invoke-virtual {p0}, Lcom/konka/mm/photo/AutoShowPicActivity;->changePlayBtnState()V

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->hideHandler:Landroid/os/Handler;

    const/16 v2, 0x11

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    sget-boolean v0, Lcom/konka/mm/photo/AutoShowPicActivity;->isAutoPlaying:Z

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lcom/konka/mm/photo/AutoShowPicActivity;->hideControlDelay()V

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->playStateDrawable:[I

    aget v0, v0, v1

    invoke-virtual {p0, v0}, Lcom/konka/mm/photo/AutoShowPicActivity;->setStateDrawable(I)V

    :goto_4
    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picImageview:Lcom/konka/mm/photo/ImageViewTouch;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/konka/mm/photo/ImageViewTouch;->zoomTo(F)V

    invoke-virtual {p0}, Lcom/konka/mm/photo/AutoShowPicActivity;->setScaleDrawable()V

    sget-boolean v0, Lcom/konka/mm/photo/AutoShowPicActivity;->isAutoPlaying:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->handler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->handler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    :cond_3
    move v0, v1

    goto :goto_3

    :cond_4
    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->playStateDrawable:[I

    aget v0, v0, v4

    invoke-virtual {p0, v0}, Lcom/konka/mm/photo/AutoShowPicActivity;->setStateDrawable(I)V

    goto :goto_4

    :sswitch_5
    iget-boolean v2, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->isControllerShow:Z

    if-eqz v2, :cond_6

    invoke-virtual {p0}, Lcom/konka/mm/photo/AutoShowPicActivity;->hideController()V

    sget-boolean v0, Lcom/konka/mm/photo/AutoShowPicActivity;->isAutoPlaying:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->playStateDrawable:[I

    aget v0, v0, v1

    invoke-virtual {p0, v0}, Lcom/konka/mm/photo/AutoShowPicActivity;->setStateDrawable(I)V

    :cond_5
    :goto_5
    sget-boolean v0, Lcom/konka/mm/photo/AutoShowPicActivity;->isAutoPlaying:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/konka/mm/photo/AutoShowPicActivity;->hideControlDelay()V

    goto/16 :goto_0

    :cond_6
    sget-boolean v2, Lcom/konka/mm/photo/AutoShowPicActivity;->isCanMove:Z

    if-eqz v2, :cond_8

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picShowHolder:Lcom/konka/mm/photo/PicShowHolder;

    iget-object v0, v0, Lcom/konka/mm/photo/PicShowHolder;->bt_fullScreen:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picShowHolder:Lcom/konka/mm/photo/PicShowHolder;

    iget-object v0, v0, Lcom/konka/mm/photo/PicShowHolder;->bt_fullScreen:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    :cond_7
    :goto_6
    invoke-direct {p0}, Lcom/konka/mm/photo/AutoShowPicActivity;->showController()V

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->stateDrawable:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_5

    :cond_8
    sget-boolean v2, Lcom/konka/mm/photo/AutoShowPicActivity;->isAutoPlaying:Z

    if-eqz v2, :cond_7

    sget-boolean v2, Lcom/konka/mm/photo/AutoShowPicActivity;->isAutoPlaying:Z

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picShowHolder:Lcom/konka/mm/photo/PicShowHolder;

    iget-object v2, v2, Lcom/konka/mm/photo/PicShowHolder;->bt_autoPlay:Landroid/widget/Button;

    invoke-virtual {v2, v0}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picShowHolder:Lcom/konka/mm/photo/PicShowHolder;

    iget-object v0, v0, Lcom/konka/mm/photo/PicShowHolder;->bt_Pause:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picShowHolder:Lcom/konka/mm/photo/PicShowHolder;

    iget-object v0, v0, Lcom/konka/mm/photo/PicShowHolder;->bt_autoPlay:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picShowHolder:Lcom/konka/mm/photo/PicShowHolder;

    iget-object v0, v0, Lcom/konka/mm/photo/PicShowHolder;->bt_autoPlay:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    goto :goto_6

    :cond_9
    iget-object v2, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picShowHolder:Lcom/konka/mm/photo/PicShowHolder;

    iget-object v2, v2, Lcom/konka/mm/photo/PicShowHolder;->bt_Pause:Landroid/widget/Button;

    invoke-virtual {v2, v0}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picShowHolder:Lcom/konka/mm/photo/PicShowHolder;

    iget-object v0, v0, Lcom/konka/mm/photo/PicShowHolder;->bt_autoPlay:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picShowHolder:Lcom/konka/mm/photo/PicShowHolder;

    iget-object v0, v0, Lcom/konka/mm/photo/PicShowHolder;->bt_Pause:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picShowHolder:Lcom/konka/mm/photo/PicShowHolder;

    iget-object v0, v0, Lcom/konka/mm/photo/PicShowHolder;->bt_Pause:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    goto :goto_6

    :sswitch_6
    iput-boolean v1, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->is3dKeyDown:Z

    goto/16 :goto_0

    :sswitch_7
    iput-boolean v1, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->keyIsVolume:Z

    move v1, v0

    goto/16 :goto_1

    :cond_a
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_1

    goto/16 :goto_0

    :sswitch_8
    sget-boolean v2, Lcom/konka/mm/photo/AutoShowPicActivity;->isCanMove:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->isControllerShow:Z

    if-nez v2, :cond_0

    sget-boolean v2, Lcom/konka/mm/photo/AutoShowPicActivity;->isAutoPlaying:Z

    if-nez v2, :cond_0

    const/4 v2, -0x1

    invoke-virtual {p0, v2}, Lcom/konka/mm/photo/AutoShowPicActivity;->moveNextOrPrevious(I)V

    iget-object v2, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->playStateDrawable:[I

    aget v2, v2, v0

    invoke-virtual {p0, v2}, Lcom/konka/mm/photo/AutoShowPicActivity;->setStateDrawable(I)V

    iget-object v2, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picImageview:Lcom/konka/mm/photo/ImageViewTouch;

    invoke-virtual {v2, v0}, Lcom/konka/mm/photo/ImageViewTouch;->playSoundEffect(I)V

    goto/16 :goto_1

    :sswitch_9
    sget-boolean v2, Lcom/konka/mm/photo/AutoShowPicActivity;->isCanMove:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->isControllerShow:Z

    if-nez v2, :cond_0

    sget-boolean v2, Lcom/konka/mm/photo/AutoShowPicActivity;->isAutoPlaying:Z

    if-nez v2, :cond_0

    invoke-virtual {p0, v1}, Lcom/konka/mm/photo/AutoShowPicActivity;->moveNextOrPrevious(I)V

    iget-object v2, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->playStateDrawable:[I

    const/4 v3, 0x2

    aget v2, v2, v3

    invoke-virtual {p0, v2}, Lcom/konka/mm/photo/AutoShowPicActivity;->setStateDrawable(I)V

    iget-object v2, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picImageview:Lcom/konka/mm/photo/ImageViewTouch;

    invoke-virtual {v2, v0}, Lcom/konka/mm/photo/ImageViewTouch;->playSoundEffect(I)V

    goto/16 :goto_1

    :sswitch_a
    iget-boolean v2, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->isControllerShow:Z

    if-nez v2, :cond_0

    sget-boolean v2, Lcom/konka/mm/photo/AutoShowPicActivity;->isCanMove:Z

    if-nez v2, :cond_0

    sget-boolean v2, Lcom/konka/mm/photo/AutoShowPicActivity;->isAutoPlaying:Z

    if-eqz v2, :cond_b

    :goto_7
    sput-boolean v0, Lcom/konka/mm/photo/AutoShowPicActivity;->isAutoPlaying:Z

    sget-boolean v0, Lcom/konka/mm/photo/AutoShowPicActivity;->isAutoPlaying:Z

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->handler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->handler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->playStateDrawable:[I

    aget v0, v0, v1

    invoke-virtual {p0, v0}, Lcom/konka/mm/photo/AutoShowPicActivity;->setStateDrawable(I)V

    goto/16 :goto_0

    :cond_b
    move v0, v1

    goto :goto_7

    :cond_c
    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->playStateDrawable:[I

    aget v0, v0, v4

    invoke-virtual {p0, v0}, Lcom/konka/mm/photo/AutoShowPicActivity;->setStateDrawable(I)V

    goto/16 :goto_0

    :sswitch_b
    iput v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->rotationClickCount:I

    sput-boolean v0, Lcom/konka/mm/photo/AutoShowPicActivity;->isAutoPlaying:Z

    iput-boolean v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->isEnterOtherAct:Z

    invoke-virtual {p0}, Lcom/konka/mm/photo/AutoShowPicActivity;->finish()V

    goto/16 :goto_0

    :sswitch_c
    iput v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->rotationClickCount:I

    iput v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->rotationClickCount:I

    iput v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->zoomTimes:I

    sget-boolean v2, Lcom/konka/mm/photo/AutoShowPicActivity;->isAutoPlaying:Z

    if-eqz v2, :cond_d

    :goto_8
    sput-boolean v0, Lcom/konka/mm/photo/AutoShowPicActivity;->isAutoPlaying:Z

    invoke-virtual {p0}, Lcom/konka/mm/photo/AutoShowPicActivity;->changePlayBtnState()V

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->hideHandler:Landroid/os/Handler;

    const/16 v2, 0x11

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    sget-boolean v0, Lcom/konka/mm/photo/AutoShowPicActivity;->isAutoPlaying:Z

    if-eqz v0, :cond_e

    invoke-virtual {p0}, Lcom/konka/mm/photo/AutoShowPicActivity;->hideControlDelay()V

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->playStateDrawable:[I

    aget v0, v0, v1

    invoke-virtual {p0, v0}, Lcom/konka/mm/photo/AutoShowPicActivity;->setStateDrawable(I)V

    :goto_9
    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picImageview:Lcom/konka/mm/photo/ImageViewTouch;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/konka/mm/photo/ImageViewTouch;->zoomTo(F)V

    invoke-virtual {p0}, Lcom/konka/mm/photo/AutoShowPicActivity;->setScaleDrawable()V

    sget-boolean v0, Lcom/konka/mm/photo/AutoShowPicActivity;->isAutoPlaying:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->handler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->handler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    :cond_d
    move v0, v1

    goto :goto_8

    :cond_e
    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->playStateDrawable:[I

    aget v0, v0, v4

    invoke-virtual {p0, v0}, Lcom/konka/mm/photo/AutoShowPicActivity;->setStateDrawable(I)V

    goto :goto_9

    :sswitch_d
    iget-boolean v2, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->isControllerShow:Z

    if-eqz v2, :cond_10

    invoke-virtual {p0}, Lcom/konka/mm/photo/AutoShowPicActivity;->hideController()V

    sget-boolean v0, Lcom/konka/mm/photo/AutoShowPicActivity;->isAutoPlaying:Z

    if-eqz v0, :cond_f

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->playStateDrawable:[I

    aget v0, v0, v1

    invoke-virtual {p0, v0}, Lcom/konka/mm/photo/AutoShowPicActivity;->setStateDrawable(I)V

    :cond_f
    :goto_a
    sget-boolean v0, Lcom/konka/mm/photo/AutoShowPicActivity;->isAutoPlaying:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/konka/mm/photo/AutoShowPicActivity;->hideControlDelay()V

    goto/16 :goto_0

    :cond_10
    sget-boolean v2, Lcom/konka/mm/photo/AutoShowPicActivity;->isCanMove:Z

    if-eqz v2, :cond_12

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picShowHolder:Lcom/konka/mm/photo/PicShowHolder;

    iget-object v0, v0, Lcom/konka/mm/photo/PicShowHolder;->bt_fullScreen:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picShowHolder:Lcom/konka/mm/photo/PicShowHolder;

    iget-object v0, v0, Lcom/konka/mm/photo/PicShowHolder;->bt_fullScreen:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    :cond_11
    :goto_b
    invoke-direct {p0}, Lcom/konka/mm/photo/AutoShowPicActivity;->showController()V

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->stateDrawable:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_a

    :cond_12
    sget-boolean v2, Lcom/konka/mm/photo/AutoShowPicActivity;->isAutoPlaying:Z

    if-eqz v2, :cond_11

    sget-boolean v2, Lcom/konka/mm/photo/AutoShowPicActivity;->isAutoPlaying:Z

    if-eqz v2, :cond_13

    iget-object v2, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picShowHolder:Lcom/konka/mm/photo/PicShowHolder;

    iget-object v2, v2, Lcom/konka/mm/photo/PicShowHolder;->bt_autoPlay:Landroid/widget/Button;

    invoke-virtual {v2, v0}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picShowHolder:Lcom/konka/mm/photo/PicShowHolder;

    iget-object v0, v0, Lcom/konka/mm/photo/PicShowHolder;->bt_Pause:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picShowHolder:Lcom/konka/mm/photo/PicShowHolder;

    iget-object v0, v0, Lcom/konka/mm/photo/PicShowHolder;->bt_autoPlay:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picShowHolder:Lcom/konka/mm/photo/PicShowHolder;

    iget-object v0, v0, Lcom/konka/mm/photo/PicShowHolder;->bt_autoPlay:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    goto :goto_b

    :cond_13
    iget-object v2, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picShowHolder:Lcom/konka/mm/photo/PicShowHolder;

    iget-object v2, v2, Lcom/konka/mm/photo/PicShowHolder;->bt_Pause:Landroid/widget/Button;

    invoke-virtual {v2, v0}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picShowHolder:Lcom/konka/mm/photo/PicShowHolder;

    iget-object v0, v0, Lcom/konka/mm/photo/PicShowHolder;->bt_autoPlay:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picShowHolder:Lcom/konka/mm/photo/PicShowHolder;

    iget-object v0, v0, Lcom/konka/mm/photo/PicShowHolder;->bt_Pause:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picShowHolder:Lcom/konka/mm/photo/PicShowHolder;

    iget-object v0, v0, Lcom/konka/mm/photo/PicShowHolder;->bt_Pause:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    goto :goto_b

    :sswitch_e
    iput-boolean v1, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->is3dKeyDown:Z

    goto/16 :goto_0

    :sswitch_f
    iput-boolean v1, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->keyIsVolume:Z

    move v1, v0

    goto/16 :goto_1

    :sswitch_data_0
    .sparse-switch
        0x15 -> :sswitch_0
        0x16 -> :sswitch_1
        0x18 -> :sswitch_7
        0x19 -> :sswitch_7
        0x42 -> :sswitch_2
        0x52 -> :sswitch_5
        0xa4 -> :sswitch_7
        0xce -> :sswitch_6
        0x106 -> :sswitch_1
        0x12e -> :sswitch_4
        0x12f -> :sswitch_3
        0x133 -> :sswitch_0
        0x205 -> :sswitch_6
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x15 -> :sswitch_8
        0x16 -> :sswitch_9
        0x18 -> :sswitch_f
        0x19 -> :sswitch_f
        0x42 -> :sswitch_a
        0x52 -> :sswitch_d
        0x56 -> :sswitch_b
        0x7e -> :sswitch_c
        0xa4 -> :sswitch_f
        0xce -> :sswitch_e
        0x12e -> :sswitch_9
        0x132 -> :sswitch_8
        0x205 -> :sswitch_e
    .end sparse-switch
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 5
    .param p1    # Landroid/content/Intent;

    const/4 v4, 0x0

    invoke-super {p0, p1}, Landroid/app/Activity;->onNewIntent(Landroid/content/Intent;)V

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {p0}, Lcom/konka/mm/photo/AutoShowPicActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lcom/konka/mm/GlobalData;

    const-string v2, "com.konka.mm.file.where.come.from"

    const-string v3, "com.konka.mm.file.come.from.other"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/konka/mm/photo/AutoShowPicActivity;->sourceFileComeFrom:Ljava/lang/String;

    sget-object v2, Lcom/konka/mm/photo/AutoShowPicActivity;->sourceFileComeFrom:Ljava/lang/String;

    const-string v3, "com.konka.mm.file.come.from.filemanager"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "com.konka.mm.file.parent.path"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->parentPath:Ljava/lang/String;

    const-string v2, "com.konka.mm.file.index.posstion"

    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->currentNum:I

    const-string v2, "com.konka.mm.file.root.path"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->root_path:Ljava/lang/String;

    invoke-virtual {v1}, Lcom/konka/mm/GlobalData;->getmMMFileList()Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picPaths:Ljava/util/ArrayList;

    :goto_0
    invoke-virtual {p0}, Lcom/konka/mm/photo/AutoShowPicActivity;->parsePhotoThread()V

    return-void

    :cond_0
    sget-object v2, Lcom/konka/mm/photo/AutoShowPicActivity;->sourceFileComeFrom:Ljava/lang/String;

    const-string v3, "com.konka.mm.file.come.from.samba"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "com.konka.mm.file.index.posstion"

    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->currentNum:I

    invoke-virtual {v1}, Lcom/konka/mm/GlobalData;->getmMMFileList()Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picPaths:Ljava/util/ArrayList;

    const-string v2, "com.konka.mm.samba.current.ip"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->currentIp:Ljava/lang/String;

    goto :goto_0

    :cond_1
    sget-object v2, Lcom/konka/mm/photo/AutoShowPicActivity;->sourceFileComeFrom:Ljava/lang/String;

    const-string v3, "com.konka.mm.file.come.from.one.disk"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "com.konka.mm.file.index.posstion"

    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->currentNum:I

    const-string v2, "com.konka.mm.file.root.path"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->root_path:Ljava/lang/String;

    invoke-virtual {v1}, Lcom/konka/mm/GlobalData;->getmMMFileList()Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picPaths:Ljava/util/ArrayList;

    goto :goto_0

    :cond_2
    const-string v2, "com.konka.mm.file.index.posstion"

    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->currentNum:I

    const-string v2, "com.konka.mm.file.root.path"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->root_path:Ljava/lang/String;

    const-string v2, "com.konka.mm.file.all.lists"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picPaths:Ljava/util/ArrayList;

    goto :goto_0
.end method

.method protected onPause()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    sget-object v0, Lcom/konka/mm/photo/AutoShowPicActivity;->mThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/konka/mm/photo/AutoShowPicActivity;->options:Landroid/graphics/BitmapFactory$Options;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/konka/mm/photo/AutoShowPicActivity;->options:Landroid/graphics/BitmapFactory$Options;

    invoke-virtual {v0}, Landroid/graphics/BitmapFactory$Options;->requestCancelDecode()V

    :cond_0
    iget-boolean v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->isEnterOtherAct:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->is3dKeyDown:Z

    if-nez v0, :cond_1

    invoke-static {p0}, Lcom/konka/mm/tools/FileTool;->killRunningServiceInfo(Landroid/content/Context;)V

    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->is3dKeyDown:Z

    return-void
.end method

.method protected onRestart()V
    .locals 3

    invoke-super {p0}, Landroid/app/Activity;->onRestart()V

    invoke-virtual {p0}, Lcom/konka/mm/photo/AutoShowPicActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/konka/mm/GlobalData;

    invoke-virtual {v0}, Lcom/konka/mm/GlobalData;->getmPicCurrentPos()I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    iget v1, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->currentNum:I

    invoke-virtual {v0}, Lcom/konka/mm/GlobalData;->getmPicCurrentPos()I

    move-result v2

    if-eq v1, v2, :cond_0

    invoke-virtual {v0}, Lcom/konka/mm/GlobalData;->getmPicCurrentPos()I

    move-result v1

    iput v1, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->currentNum:I

    invoke-virtual {p0}, Lcom/konka/mm/photo/AutoShowPicActivity;->parsePhotoThread()V

    :cond_0
    return-void
.end method

.method protected onStop()V
    .locals 3

    const/4 v2, 0x0

    sput-boolean v2, Lcom/konka/mm/photo/AutoShowPicActivity;->isAutoPlaying:Z

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->handler:Landroid/os/Handler;

    const v1, 0x50000001

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    sget-boolean v0, Lcom/konka/mm/photo/AutoShowPicActivity;->is4K2KMode:Z

    if-eqz v0, :cond_0

    sput-boolean v2, Lcom/konka/mm/photo/AutoShowPicActivity;->is4K2KMode:Z

    sput-boolean v2, Lcom/konka/mm/photo/AutoShowPicActivity;->panel4k2kmode:Z

    sget-object v0, Lcom/mstar/android/MDisplay$PanelMode;->E_PANELMODE_NORMAL:Lcom/mstar/android/MDisplay$PanelMode;

    invoke-static {v0}, Lcom/mstar/android/MDisplay;->setPanelMode(Lcom/mstar/android/MDisplay$PanelMode;)V

    invoke-virtual {p0}, Lcom/konka/mm/photo/AutoShowPicActivity;->finish()V

    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    return-void
.end method

.method public parsePhotoThread()V
    .locals 2

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/konka/mm/photo/AutoShowPicActivity$6;

    invoke-direct {v1, p0}, Lcom/konka/mm/photo/AutoShowPicActivity$6;-><init>(Lcom/konka/mm/photo/AutoShowPicActivity;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    sput-object v0, Lcom/konka/mm/photo/AutoShowPicActivity;->mThread:Ljava/lang/Thread;

    sget-object v0, Lcom/konka/mm/photo/AutoShowPicActivity;->mThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method public registEthernetReceiver()V
    .locals 3

    new-instance v2, Lcom/konka/mm/photo/AutoShowPicActivity$EthStateReceiver;

    invoke-direct {v2, p0}, Lcom/konka/mm/photo/AutoShowPicActivity$EthStateReceiver;-><init>(Lcom/konka/mm/photo/AutoShowPicActivity;)V

    iput-object v2, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->mEthStateReceiver:Lcom/konka/mm/photo/AutoShowPicActivity$EthStateReceiver;

    invoke-static {}, Landroid/net/ethernet/EthernetManager;->getInstance()Landroid/net/ethernet/EthernetManager;

    move-result-object v1

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v2, "android.net.ethernet.ETHERNET_STATE_CHANGED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->mEthStateReceiver:Lcom/konka/mm/photo/AutoShowPicActivity$EthStateReceiver;

    invoke-virtual {p0, v2, v0}, Lcom/konka/mm/photo/AutoShowPicActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method public registerListeners()V
    .locals 7

    const v6, 0x50000001

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-boolean v4, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->isControllerShow:Z

    if-eqz v4, :cond_0

    iget v4, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->state:I

    packed-switch v4, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    iget-object v3, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->mSetWallpaperTask:Lcom/konka/mm/photo/AutoShowPicActivity$SetWallpaperTask;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->mSetWallpaperTask:Lcom/konka/mm/photo/AutoShowPicActivity$SetWallpaperTask;

    invoke-virtual {v3}, Lcom/konka/mm/photo/AutoShowPicActivity$SetWallpaperTask;->isFinished()Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {p0}, Lcom/konka/mm/photo/AutoShowPicActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090059    # com.konka.mm.R.string.wallpaper_waitting

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v3, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_1
    new-instance v2, Lcom/konka/mm/photo/AutoShowPicActivity$SetWallpaperTask;

    invoke-direct {v2, p0}, Lcom/konka/mm/photo/AutoShowPicActivity$SetWallpaperTask;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->mSetWallpaperTask:Lcom/konka/mm/photo/AutoShowPicActivity$SetWallpaperTask;

    :try_start_0
    iget-object v2, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picImageview:Lcom/konka/mm/photo/ImageViewTouch;

    invoke-virtual {v2}, Lcom/konka/mm/photo/ImageViewTouch;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    check-cast v2, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->mSetWallpaperTask:Lcom/konka/mm/photo/AutoShowPicActivity$SetWallpaperTask;

    const/4 v3, 0x1

    new-array v3, v3, [Landroid/graphics/Bitmap;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v2, v3}, Lcom/konka/mm/photo/AutoShowPicActivity$SetWallpaperTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v2, "AutoShwoPicActivity"

    const-string v3, "classCastException"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :pswitch_2
    iput v2, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->rotationClickCount:I

    iput v2, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->zoomTimes:I

    sput-boolean v2, Lcom/konka/mm/photo/AutoShowPicActivity;->isAutoPlaying:Z

    invoke-virtual {p0}, Lcom/konka/mm/photo/AutoShowPicActivity;->changePlayBtnState()V

    iput-boolean v2, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->isEnterOtherAct:Z

    invoke-virtual {p0}, Lcom/konka/mm/photo/AutoShowPicActivity;->setScaleDrawable()V

    invoke-virtual {p0}, Lcom/konka/mm/photo/AutoShowPicActivity;->galleryViewShow()V

    goto :goto_0

    :pswitch_3
    iput v2, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->rotationClickCount:I

    sput-boolean v2, Lcom/konka/mm/photo/AutoShowPicActivity;->isAutoPlaying:Z

    iget-object v2, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picImageview:Lcom/konka/mm/photo/ImageViewTouch;

    invoke-virtual {v2, v5}, Lcom/konka/mm/photo/ImageViewTouch;->zoomTo(F)V

    invoke-virtual {p0}, Lcom/konka/mm/photo/AutoShowPicActivity;->changePlayBtnState()V

    const/4 v2, -0x1

    invoke-virtual {p0, v2}, Lcom/konka/mm/photo/AutoShowPicActivity;->moveNextOrPrevious(I)V

    goto :goto_0

    :pswitch_4
    iput v2, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->rotationClickCount:I

    iput v2, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->rotationClickCount:I

    iput v2, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->zoomTimes:I

    sget-boolean v4, Lcom/konka/mm/photo/AutoShowPicActivity;->isAutoPlaying:Z

    if-eqz v4, :cond_3

    :goto_1
    sput-boolean v2, Lcom/konka/mm/photo/AutoShowPicActivity;->isAutoPlaying:Z

    invoke-virtual {p0}, Lcom/konka/mm/photo/AutoShowPicActivity;->changePlayBtnState()V

    iget-object v2, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->hideHandler:Landroid/os/Handler;

    const/16 v3, 0x11

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeMessages(I)V

    sget-boolean v2, Lcom/konka/mm/photo/AutoShowPicActivity;->isAutoPlaying:Z

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lcom/konka/mm/photo/AutoShowPicActivity;->hideControlDelay()V

    :cond_2
    iget-object v2, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picImageview:Lcom/konka/mm/photo/ImageViewTouch;

    invoke-virtual {v2, v5}, Lcom/konka/mm/photo/ImageViewTouch;->zoomTo(F)V

    invoke-virtual {p0}, Lcom/konka/mm/photo/AutoShowPicActivity;->setScaleDrawable()V

    sget-boolean v2, Lcom/konka/mm/photo/AutoShowPicActivity;->isAutoPlaying:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->handler:Landroid/os/Handler;

    invoke-virtual {v2, v6}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v2, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->handler:Landroid/os/Handler;

    invoke-virtual {v2, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    :cond_3
    move v2, v3

    goto :goto_1

    :pswitch_5
    iput v2, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->rotationClickCount:I

    iget-object v4, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picImageview:Lcom/konka/mm/photo/ImageViewTouch;

    invoke-virtual {v4, v5}, Lcom/konka/mm/photo/ImageViewTouch;->zoomTo(F)V

    sput-boolean v2, Lcom/konka/mm/photo/AutoShowPicActivity;->isAutoPlaying:Z

    invoke-virtual {p0}, Lcom/konka/mm/photo/AutoShowPicActivity;->changePlayBtnState()V

    invoke-virtual {p0, v3}, Lcom/konka/mm/photo/AutoShowPicActivity;->moveNextOrPrevious(I)V

    goto/16 :goto_0

    :pswitch_6
    iput v2, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->rotationClickCount:I

    sput-boolean v2, Lcom/konka/mm/photo/AutoShowPicActivity;->isAutoPlaying:Z

    iput-boolean v2, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->isEnterOtherAct:Z

    invoke-virtual {p0}, Lcom/konka/mm/photo/AutoShowPicActivity;->finishActivity()V

    goto/16 :goto_0

    :pswitch_7
    iput-boolean v3, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->isNeetCirculate:Z

    invoke-virtual {p0}, Lcom/konka/mm/photo/AutoShowPicActivity;->changePlayOrderBtnState()V

    goto/16 :goto_0

    :pswitch_8
    iput-boolean v2, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->isNeetCirculate:Z

    invoke-virtual {p0}, Lcom/konka/mm/photo/AutoShowPicActivity;->changePlayOrderBtnState()V

    goto/16 :goto_0

    :pswitch_9
    sput-boolean v2, Lcom/konka/mm/photo/AutoShowPicActivity;->isAutoPlaying:Z

    iget-object v2, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picImageview:Lcom/konka/mm/photo/ImageViewTouch;

    const/high16 v3, -0x3d4c0000    # -90.0f

    invoke-virtual {v2, v3}, Lcom/konka/mm/photo/ImageViewTouch;->rotateImage(F)V

    invoke-virtual {p0}, Lcom/konka/mm/photo/AutoShowPicActivity;->changePlayBtnState()V

    goto/16 :goto_0

    :pswitch_a
    sput-boolean v2, Lcom/konka/mm/photo/AutoShowPicActivity;->isAutoPlaying:Z

    iget-object v2, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picImageview:Lcom/konka/mm/photo/ImageViewTouch;

    const/high16 v3, 0x42b40000    # 90.0f

    invoke-virtual {v2, v3}, Lcom/konka/mm/photo/ImageViewTouch;->rotateImage(F)V

    invoke-virtual {p0}, Lcom/konka/mm/photo/AutoShowPicActivity;->changePlayBtnState()V

    goto/16 :goto_0

    :pswitch_b
    sput-boolean v2, Lcom/konka/mm/photo/AutoShowPicActivity;->isAutoPlaying:Z

    invoke-virtual {p0}, Lcom/konka/mm/photo/AutoShowPicActivity;->changePlayBtnState()V

    invoke-virtual {p0}, Lcom/konka/mm/photo/AutoShowPicActivity;->zoomIn()V

    goto/16 :goto_0

    :pswitch_c
    sput-boolean v2, Lcom/konka/mm/photo/AutoShowPicActivity;->isAutoPlaying:Z

    invoke-virtual {p0}, Lcom/konka/mm/photo/AutoShowPicActivity;->changePlayBtnState()V

    invoke-virtual {p0}, Lcom/konka/mm/photo/AutoShowPicActivity;->zoomOut()V

    goto/16 :goto_0

    :pswitch_d
    sput-boolean v2, Lcom/konka/mm/photo/AutoShowPicActivity;->isAutoPlaying:Z

    sput-boolean v3, Lcom/konka/mm/photo/AutoShowPicActivity;->isCanMove:Z

    iget-object v2, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->hideHandler:Landroid/os/Handler;

    const/16 v3, 0x12

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_d
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public setAutoPlaying(Z)V
    .locals 0
    .param p1    # Z

    sput-boolean p1, Lcom/konka/mm/photo/AutoShowPicActivity;->isAutoPlaying:Z

    return-void
.end method

.method public setDefaultPhoto()Landroid/graphics/Bitmap;
    .locals 4

    const/4 v3, 0x1

    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    sget-object v2, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    iput-object v2, v1, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    iput-boolean v3, v1, Landroid/graphics/BitmapFactory$Options;->inPurgeable:Z

    iput-boolean v3, v1, Landroid/graphics/BitmapFactory$Options;->inInputShareable:Z

    invoke-virtual {p0}, Lcom/konka/mm/photo/AutoShowPicActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020036    # com.konka.mm.R.drawable.default_bg

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v0

    const/4 v2, 0x0

    invoke-static {v0, v2, v1}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-static {v2}, Lcom/konka/mm/photo/AutoShowPicActivity;->ensureGLCompatibleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v2

    return-object v2
.end method

.method public setPhotoView()V
    .locals 13

    const/4 v12, 0x1

    const/4 v11, 0x0

    invoke-virtual {p0}, Lcom/konka/mm/photo/AutoShowPicActivity;->changBtnShowStatus()V

    const-wide/16 v5, 0x0

    :try_start_0
    new-instance v9, Ljava/io/File;

    iget-object v8, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picPaths:Ljava/util/ArrayList;

    iget v10, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->currentNum:I

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-direct {v9, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v9}, Lcom/konka/mm/tools/FileTool;->getFileSizes(Ljava/io/File;)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v5

    :goto_0
    const-wide/16 v0, 0x400

    sget-object v8, Lcom/konka/mm/photo/AutoShowPicActivity;->sourceFileComeFrom:Ljava/lang/String;

    const-string v9, "com.konka.mm.file.come.from.samba"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    const-wide/16 v0, 0x400

    :goto_1
    cmp-long v8, v5, v0

    if-lez v8, :cond_4

    sput-boolean v11, Lcom/konka/mm/photo/AutoShowPicActivity;->bAutoPlayKey:Z

    new-instance v3, Lcom/konka/mm/photo/CallbackImg;

    iget-object v8, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picImageview:Lcom/konka/mm/photo/ImageViewTouch;

    invoke-direct {v3, v8, p0}, Lcom/konka/mm/photo/CallbackImg;-><init>(Lcom/konka/mm/photo/ImageViewTouch;Lcom/konka/mm/photo/AutoShowPicActivity;)V

    iget-object v8, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picPaths:Ljava/util/ArrayList;

    iget v9, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->currentNum:I

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-static {v8, v3, v12}, Lcom/konka/mm/photo/AsyncImageLoader;->loadBitmap(Ljava/lang/String;Lcom/konka/mm/photo/AsyncImageLoader$ImageCallback;I)Landroid/graphics/Bitmap;

    move-result-object v2

    if-nez v2, :cond_0

    const-wide/32 v8, 0x800000

    cmp-long v8, v5, v8

    if-lez v8, :cond_0

    const/16 v8, 0x15

    invoke-virtual {p0, v8}, Lcom/konka/mm/photo/AutoShowPicActivity;->showDialog(I)V

    :cond_0
    if-eqz v2, :cond_3

    iget-object v8, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->bitmapArray:Lcom/konka/mm/photo/RotateBitmap;

    invoke-virtual {v8, v2}, Lcom/konka/mm/photo/RotateBitmap;->setBitmap(Landroid/graphics/Bitmap;)V

    iget-object v8, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picImageview:Lcom/konka/mm/photo/ImageViewTouch;

    iget-object v8, v8, Lcom/konka/mm/photo/ImageViewTouch;->mSuppMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v8}, Landroid/graphics/Matrix;->reset()V

    iget-object v8, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picImageview:Lcom/konka/mm/photo/ImageViewTouch;

    iget-object v9, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->bitmapArray:Lcom/konka/mm/photo/RotateBitmap;

    invoke-virtual {v8, v9, v11, p0}, Lcom/konka/mm/photo/ImageViewTouch;->setImageRotateBitmapResetBase(Lcom/konka/mm/photo/RotateBitmap;ZLcom/konka/mm/photo/AutoShowPicActivity;)V

    iget-object v8, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picImageview:Lcom/konka/mm/photo/ImageViewTouch;

    iget-object v9, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->mAlphaIn:Landroid/view/animation/Animation;

    invoke-virtual {v8, v9}, Lcom/konka/mm/photo/ImageViewTouch;->startAnimation(Landroid/view/animation/Animation;)V

    invoke-virtual {p0}, Lcom/konka/mm/photo/AutoShowPicActivity;->settextViewName()V

    :cond_1
    :goto_2
    return-void

    :catch_0
    move-exception v4

    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    :cond_2
    const-wide/32 v0, 0xd00000

    goto :goto_1

    :cond_3
    const-wide/32 v8, 0x1400000

    cmp-long v8, v5, v8

    if-lez v8, :cond_1

    const/16 v8, 0x2328

    iput v8, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->mDelayed_time:I

    goto :goto_2

    :cond_4
    sput-boolean v12, Lcom/konka/mm/photo/AutoShowPicActivity;->bAutoPlayKey:Z

    iget-object v8, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picPaths:Ljava/util/ArrayList;

    iget v9, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->currentNum:I

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    const v9, 0x1fa400

    invoke-static {v8, v9}, Lcom/konka/mm/tools/PicTool;->decodeFileDescriptor(Ljava/lang/String;I)Landroid/graphics/Bitmap;

    move-result-object v7

    iget-object v8, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->bitmapArray:Lcom/konka/mm/photo/RotateBitmap;

    invoke-virtual {v8, v7}, Lcom/konka/mm/photo/RotateBitmap;->setBitmap(Landroid/graphics/Bitmap;)V

    iget-object v8, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picImageview:Lcom/konka/mm/photo/ImageViewTouch;

    iget-object v8, v8, Lcom/konka/mm/photo/ImageViewTouch;->mSuppMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v8}, Landroid/graphics/Matrix;->reset()V

    iget-object v8, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picImageview:Lcom/konka/mm/photo/ImageViewTouch;

    iget-object v9, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->bitmapArray:Lcom/konka/mm/photo/RotateBitmap;

    invoke-virtual {v8, v9, v11, p0}, Lcom/konka/mm/photo/ImageViewTouch;->setImageRotateBitmapResetBase(Lcom/konka/mm/photo/RotateBitmap;ZLcom/konka/mm/photo/AutoShowPicActivity;)V

    iget-object v8, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picImageview:Lcom/konka/mm/photo/ImageViewTouch;

    iget-object v9, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->mAlphaIn:Landroid/view/animation/Animation;

    invoke-virtual {v8, v9}, Lcom/konka/mm/photo/ImageViewTouch;->startAnimation(Landroid/view/animation/Animation;)V

    invoke-virtual {p0}, Lcom/konka/mm/photo/AutoShowPicActivity;->settextViewName()V

    const/16 v8, 0x1770

    iput v8, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->mDelayed_time:I

    goto :goto_2
.end method

.method public setScaleDrawable()V
    .locals 3

    iget v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->zoomTimes:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->scaleImageView:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    iget v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->zoomTimes:I

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->scaleImageView:Landroid/widget/ImageView;

    sget-object v1, Lcom/konka/mm/photo/AutoShowPicActivity;->enlargeDrawable:[I

    iget v2, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->zoomTimes:I

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_1
    iget v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->zoomTimes:I

    if-gez v0, :cond_2

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->scaleImageView:Landroid/widget/ImageView;

    sget-object v1, Lcom/konka/mm/photo/AutoShowPicActivity;->descaleDrawable:[I

    iget v2, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->zoomTimes:I

    neg-int v2, v2

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_2
    return-void
.end method

.method public setState(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->state:I

    return-void
.end method

.method public setStateDrawable(I)V
    .locals 1
    .param p1    # I

    iget-boolean v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->isControllerShow:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/konka/mm/photo/AutoShowPicActivity;->isAutoPlaying:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->stateDrawable:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->stateDrawable:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method

.method public settextViewName()V
    .locals 4

    iget-object v2, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picPaths:Ljava/util/ArrayList;

    iget v3, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->currentNum:I

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->textView:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public unregistEthernetReceiver()V
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->mEthStateReceiver:Lcom/konka/mm/photo/AutoShowPicActivity$EthStateReceiver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->mEthStateReceiver:Lcom/konka/mm/photo/AutoShowPicActivity$EthStateReceiver;

    invoke-virtual {p0, v0}, Lcom/konka/mm/photo/AutoShowPicActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    :cond_0
    return-void
.end method

.method public zoomIn()V
    .locals 2

    iget v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->zoomTimes:I

    const/4 v1, 0x4

    if-lt v0, v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->zoomTimes:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->zoomTimes:I

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picImageview:Lcom/konka/mm/photo/ImageViewTouch;

    invoke-virtual {v0}, Lcom/konka/mm/photo/ImageViewTouch;->zoomIn()V

    invoke-virtual {p0}, Lcom/konka/mm/photo/AutoShowPicActivity;->setScaleDrawable()V

    goto :goto_0
.end method

.method public zoomOut()V
    .locals 2

    iget v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->zoomTimes:I

    const/4 v1, -0x4

    if-gt v0, v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->zoomTimes:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->zoomTimes:I

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity;->picImageview:Lcom/konka/mm/photo/ImageViewTouch;

    invoke-virtual {v0}, Lcom/konka/mm/photo/ImageViewTouch;->zoomOut()V

    invoke-virtual {p0}, Lcom/konka/mm/photo/AutoShowPicActivity;->setScaleDrawable()V

    goto :goto_0
.end method
