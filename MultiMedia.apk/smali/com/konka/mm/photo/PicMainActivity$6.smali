.class Lcom/konka/mm/photo/PicMainActivity$6;
.super Ljava/lang/Object;
.source "PicMainActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/mm/photo/PicMainActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/mm/photo/PicMainActivity;


# direct methods
.method constructor <init>(Lcom/konka/mm/photo/PicMainActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/photo/PicMainActivity$6;->this$0:Lcom/konka/mm/photo/PicMainActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    new-instance v1, Ljava/io/File;

    iget-object v3, p0, Lcom/konka/mm/photo/PicMainActivity$6;->this$0:Lcom/konka/mm/photo/PicMainActivity;

    iget-object v3, v3, Lcom/konka/mm/photo/PicMainActivity;->picPaths:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/konka/mm/photo/PicMainActivity$6;->this$0:Lcom/konka/mm/photo/PicMainActivity;

    iget v4, v4, Lcom/konka/mm/photo/PicMainActivity;->currentPage:I

    iget-object v5, p0, Lcom/konka/mm/photo/PicMainActivity$6;->this$0:Lcom/konka/mm/photo/PicMainActivity;

    iget v5, v5, Lcom/konka/mm/photo/PicMainActivity;->mCountPerPage:I

    mul-int/2addr v4, v5

    add-int/2addr v4, p3

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/konka/mm/photo/PicMainActivity$6;->this$0:Lcom/konka/mm/photo/PicMainActivity;

    iget-object v3, v3, Lcom/konka/mm/photo/PicMainActivity;->toast_file_not_exist:Landroid/widget/Toast;

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    :goto_0
    return-void

    :cond_0
    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, Lcom/konka/mm/photo/PicMainActivity$6;->this$0:Lcom/konka/mm/photo/PicMainActivity;

    const-class v4, Lcom/konka/mm/photo/AutoShowPicActivity;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v3, "com.konka.mm.file.where.come.from"

    const-string v4, "com.konka.mm.file.come.from.one.disk"

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "com.konka.mm.file.index.posstion"

    iget-object v4, p0, Lcom/konka/mm/photo/PicMainActivity$6;->this$0:Lcom/konka/mm/photo/PicMainActivity;

    iget v4, v4, Lcom/konka/mm/photo/PicMainActivity;->currentPage:I

    iget-object v5, p0, Lcom/konka/mm/photo/PicMainActivity$6;->this$0:Lcom/konka/mm/photo/PicMainActivity;

    iget v5, v5, Lcom/konka/mm/photo/PicMainActivity;->mCountPerPage:I

    mul-int/2addr v4, v5

    add-int/2addr v4, p3

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v3, "com.konka.mm.file.root.path"

    sget-object v4, Lcom/konka/mm/photo/PicMainActivity;->mRootPath:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    iget-object v3, p0, Lcom/konka/mm/photo/PicMainActivity$6;->this$0:Lcom/konka/mm/photo/PicMainActivity;

    invoke-virtual {v3, v2}, Lcom/konka/mm/photo/PicMainActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
