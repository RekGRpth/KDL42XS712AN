.class public Lcom/konka/mm/photo/PhotoThumbLoader;
.super Ljava/lang/Object;
.source "PhotoThumbLoader.java"

# interfaces
.implements Ljava/lang/Runnable;


# static fields
.field private static final DEFAULT_POOL_SIZE:I = 0x2

.field public static final GET_PHOTO_DONE:I = 0x1

.field private static final TAG:Ljava/lang/String; = "PhotoThumbLoader"

.field private static downloadingList:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/HashSet",
            "<",
            "Landroid/os/Handler;",
            ">;>;"
        }
    .end annotation
.end field

.field private static executor:Ljava/util/concurrent/ThreadPoolExecutor;

.field private static imageCache:Lcom/konka/mm/photo/ImageCache;

.field private static sBitmapOptions:Landroid/graphics/BitmapFactory$Options;


# instance fields
.field private mPath:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v0, 0x2

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ThreadPoolExecutor;

    sput-object v0, Lcom/konka/mm/photo/PhotoThumbLoader;->executor:Ljava/util/concurrent/ThreadPoolExecutor;

    invoke-static {}, Lcom/konka/mm/photo/ImageCache;->getInstance()Lcom/konka/mm/photo/ImageCache;

    move-result-object v0

    sput-object v0, Lcom/konka/mm/photo/PhotoThumbLoader;->imageCache:Lcom/konka/mm/photo/ImageCache;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/konka/mm/photo/PhotoThumbLoader;->downloadingList:Ljava/util/HashMap;

    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    sput-object v0, Lcom/konka/mm/photo/PhotoThumbLoader;->sBitmapOptions:Landroid/graphics/BitmapFactory$Options;

    sget-object v0, Lcom/konka/mm/photo/PhotoThumbLoader;->sBitmapOptions:Landroid/graphics/BitmapFactory$Options;

    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inPurgeable:Z

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/konka/mm/photo/PhotoThumbLoader;->mPath:Ljava/lang/String;

    return-void
.end method

.method private static generateKey(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0    # Ljava/lang/String;

    const-string v1, "/"

    const-string v2, ""

    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ":"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "?"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static initThreadPool()V
    .locals 1

    const/4 v0, 0x2

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ThreadPoolExecutor;

    sput-object v0, Lcom/konka/mm/photo/PhotoThumbLoader;->executor:Ljava/util/concurrent/ThreadPoolExecutor;

    return-void
.end method

.method public static remove(Ljava/lang/String;Lcom/konka/mm/photo/ImageThumbHandler;)Z
    .locals 6
    .param p0    # Ljava/lang/String;
    .param p1    # Lcom/konka/mm/photo/ImageThumbHandler;

    invoke-static {p0}, Lcom/konka/mm/photo/PhotoThumbLoader;->generateKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v1, 0x0

    sget-object v5, Lcom/konka/mm/photo/PhotoThumbLoader;->downloadingList:Ljava/util/HashMap;

    monitor-enter v5

    :try_start_0
    sget-object v4, Lcom/konka/mm/photo/PhotoThumbLoader;->downloadingList:Ljava/util/HashMap;

    invoke-virtual {v4, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    move-object v0, v4

    check-cast v0, Ljava/util/HashSet;

    move-object v1, v0

    if-nez v1, :cond_0

    monitor-exit v5

    const/4 v3, 0x0

    :goto_0
    return v3

    :cond_0
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    monitor-enter v1

    :try_start_1
    invoke-virtual {v1, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    move-result v3

    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v4

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v4

    :catchall_1
    move-exception v4

    :try_start_2
    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v4
.end method

.method public static shutdownThreadPool()V
    .locals 1

    sget-object v0, Lcom/konka/mm/photo/PhotoThumbLoader;->executor:Ljava/util/concurrent/ThreadPoolExecutor;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/konka/mm/photo/PhotoThumbLoader;->executor:Ljava/util/concurrent/ThreadPoolExecutor;

    invoke-virtual {v0}, Ljava/util/concurrent/ThreadPoolExecutor;->shutdownNow()Ljava/util/List;

    :cond_0
    return-void
.end method

.method public static start(Ljava/lang/String;Lcom/konka/mm/photo/ImageThumbHandler;)Z
    .locals 11
    .param p0    # Ljava/lang/String;
    .param p1    # Lcom/konka/mm/photo/ImageThumbHandler;

    const/4 v8, 0x1

    const/4 v9, 0x0

    invoke-static {p0}, Lcom/konka/mm/photo/PhotoThumbLoader;->generateKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    sget-object v10, Lcom/konka/mm/photo/PhotoThumbLoader;->imageCache:Lcom/konka/mm/photo/ImageCache;

    invoke-virtual {v10, v6}, Lcom/konka/mm/photo/ImageCache;->getBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v2

    if-eqz v2, :cond_0

    new-instance v7, Landroid/os/Message;

    invoke-direct {v7}, Landroid/os/Message;-><init>()V

    iput v8, v7, Landroid/os/Message;->what:I

    iput-object v2, v7, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {p1, v7}, Lcom/konka/mm/photo/ImageThumbHandler;->sendMessage(Landroid/os/Message;)Z

    :goto_0
    return v8

    :cond_0
    invoke-virtual {p1}, Lcom/konka/mm/photo/ImageThumbHandler;->preHandleMessage()V

    const/4 v5, 0x0

    const/4 v3, 0x0

    sget-object v10, Lcom/konka/mm/photo/PhotoThumbLoader;->downloadingList:Ljava/util/HashMap;

    monitor-enter v10

    :try_start_0
    sget-object v8, Lcom/konka/mm/photo/PhotoThumbLoader;->downloadingList:Ljava/util/HashMap;

    invoke-virtual {v8, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    move-object v0, v8

    check-cast v0, Ljava/util/HashSet;

    move-object v3, v0

    if-nez v3, :cond_1

    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    sget-object v8, Lcom/konka/mm/photo/PhotoThumbLoader;->downloadingList:Ljava/util/HashMap;

    invoke-virtual {v8, v6, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-object v3, v4

    :goto_1
    :try_start_2
    monitor-exit v10
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-enter v3

    :try_start_3
    invoke-virtual {v3, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move v8, v9

    goto :goto_0

    :cond_1
    const/4 v5, 0x1

    goto :goto_1

    :catchall_0
    move-exception v8

    :goto_2
    :try_start_4
    monitor-exit v10
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v8

    :cond_2
    :try_start_5
    monitor-exit v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    if-nez v5, :cond_3

    sget-object v8, Lcom/konka/mm/photo/PhotoThumbLoader;->executor:Ljava/util/concurrent/ThreadPoolExecutor;

    new-instance v10, Lcom/konka/mm/photo/PhotoThumbLoader;

    invoke-direct {v10, p0}, Lcom/konka/mm/photo/PhotoThumbLoader;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v10}, Ljava/util/concurrent/ThreadPoolExecutor;->execute(Ljava/lang/Runnable;)V

    :cond_3
    move v8, v9

    goto :goto_0

    :catchall_1
    move-exception v8

    :try_start_6
    monitor-exit v3
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    throw v8

    :catchall_2
    move-exception v8

    move-object v3, v4

    goto :goto_2
.end method


# virtual methods
.method public run()V
    .locals 13

    iget-object v9, p0, Lcom/konka/mm/photo/PhotoThumbLoader;->mPath:Ljava/lang/String;

    invoke-static {v9}, Lcom/konka/mm/photo/PhotoThumbLoader;->generateKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const/4 v1, 0x0

    :try_start_0
    invoke-static {}, Lcom/konka/mm/photo/StorageCache;->getInstance()Lcom/konka/mm/photo/StorageCache;

    move-result-object v9

    invoke-virtual {v9, v7}, Lcom/konka/mm/photo/StorageCache;->get(Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    if-eqz v2, :cond_1

    sget-object v10, Lcom/konka/mm/photo/PhotoThumbLoader;->imageCache:Lcom/konka/mm/photo/ImageCache;

    monitor-enter v10
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    new-instance v9, Ljava/io/FileInputStream;

    invoke-direct {v9, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    const/4 v11, 0x0

    sget-object v12, Lcom/konka/mm/photo/PhotoThumbLoader;->sBitmapOptions:Landroid/graphics/BitmapFactory$Options;

    invoke-static {v9, v11, v12}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v1

    if-eqz v1, :cond_0

    sget-object v9, Lcom/konka/mm/photo/PhotoThumbLoader;->imageCache:Lcom/konka/mm/photo/ImageCache;

    invoke-virtual {v9, v7, v1}, Lcom/konka/mm/photo/ImageCache;->pubBitmap(Ljava/lang/String;Landroid/graphics/Bitmap;)Z

    :cond_0
    monitor-exit v10
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    :goto_0
    if-nez v1, :cond_2

    :try_start_2
    iget-object v9, p0, Lcom/konka/mm/photo/PhotoThumbLoader;->mPath:Ljava/lang/String;

    const/high16 v10, 0x20000

    invoke-static {v9, v10}, Lcom/konka/mm/tools/PicTool;->decodeFileDescriptor(Ljava/lang/String;I)Landroid/graphics/Bitmap;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object v1

    :cond_2
    :goto_1
    const/4 v5, 0x0

    sget-object v10, Lcom/konka/mm/photo/PhotoThumbLoader;->downloadingList:Ljava/util/HashMap;

    monitor-enter v10

    :try_start_3
    sget-object v9, Lcom/konka/mm/photo/PhotoThumbLoader;->downloadingList:Ljava/util/HashMap;

    invoke-virtual {v9, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    move-object v0, v9

    check-cast v0, Ljava/util/HashSet;

    move-object v5, v0

    monitor-exit v10
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    sget-object v10, Lcom/konka/mm/photo/PhotoThumbLoader;->downloadingList:Ljava/util/HashMap;

    monitor-enter v10

    if-nez v5, :cond_3

    :try_start_4
    monitor-exit v10
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    :goto_2
    return-void

    :catchall_0
    move-exception v9

    :try_start_5
    monitor-exit v10
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    throw v9
    :try_end_6
    .catch Ljava/io/FileNotFoundException; {:try_start_6 .. :try_end_6} :catch_0

    :catch_0
    move-exception v3

    invoke-virtual {v3}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v3

    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    :catchall_1
    move-exception v9

    :try_start_7
    monitor-exit v10
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    throw v9

    :cond_3
    :try_start_8
    invoke-virtual {v5}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-nez v9, :cond_4

    invoke-virtual {v5}, Ljava/util/HashSet;->clear()V

    monitor-exit v10
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    sget-object v10, Lcom/konka/mm/photo/PhotoThumbLoader;->downloadingList:Ljava/util/HashMap;

    monitor-enter v10

    :try_start_9
    sget-object v9, Lcom/konka/mm/photo/PhotoThumbLoader;->downloadingList:Ljava/util/HashMap;

    invoke-virtual {v9, v7}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v10

    goto :goto_2

    :catchall_2
    move-exception v9

    monitor-exit v10
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    throw v9

    :cond_4
    :try_start_a
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/konka/mm/photo/ImageThumbHandler;

    new-instance v8, Landroid/os/Message;

    invoke-direct {v8}, Landroid/os/Message;-><init>()V

    const/4 v9, 0x1

    iput v9, v8, Landroid/os/Message;->what:I

    iput-object v1, v8, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v4, v8}, Lcom/konka/mm/photo/ImageThumbHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_3

    :catchall_3
    move-exception v9

    monitor-exit v10
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_3

    throw v9
.end method
