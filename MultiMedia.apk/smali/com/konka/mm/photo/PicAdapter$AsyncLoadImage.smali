.class Lcom/konka/mm/photo/PicAdapter$AsyncLoadImage;
.super Landroid/os/AsyncTask;
.source "PicAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/mm/photo/PicAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "AsyncLoadImage"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Object;",
        "Ljava/lang/Object;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/mm/photo/PicAdapter;


# direct methods
.method constructor <init>(Lcom/konka/mm/photo/PicAdapter;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/photo/PicAdapter$AsyncLoadImage;->this$0:Lcom/konka/mm/photo/PicAdapter;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Object;

    invoke-virtual {p0, p1}, Lcom/konka/mm/photo/PicAdapter$AsyncLoadImage;->doInBackground([Ljava/lang/Object;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Object;)Ljava/lang/Void;
    .locals 6
    .param p1    # [Ljava/lang/Object;

    const/4 v4, 0x0

    :try_start_0
    aget-object v2, p1, v4

    check-cast v2, Landroid/widget/ImageView;

    const/4 v4, 0x1

    aget-object v3, p1, v4

    check-cast v3, Ljava/lang/String;

    invoke-static {v3}, Lcom/konka/mm/photo/PicAdapter;->getBitmapByUrl(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v2, v4, v5

    const/4 v5, 0x1

    aput-object v0, v4, v5

    invoke-virtual {p0, v4}, Lcom/konka/mm/photo/PicAdapter$AsyncLoadImage;->publishProgress([Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    const/4 v4, 0x0

    return-object v4

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/net/MalformedURLException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v1

    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method protected varargs onProgressUpdate([Ljava/lang/Object;)V
    .locals 2
    .param p1    # [Ljava/lang/Object;

    const/4 v1, 0x0

    aget-object v0, p1, v1

    check-cast v0, Landroid/widget/ImageView;

    const/4 v1, 0x1

    aget-object v1, p1, v1

    check-cast v1, Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    return-void
.end method
