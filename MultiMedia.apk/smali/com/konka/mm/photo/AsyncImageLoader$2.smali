.class Lcom/konka/mm/photo/AsyncImageLoader$2;
.super Ljava/lang/Thread;
.source "AsyncImageLoader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/mm/photo/AsyncImageLoader;->loadBitmap(Ljava/lang/String;Lcom/konka/mm/photo/AsyncImageLoader$ImageCallback;I)Landroid/graphics/Bitmap;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final synthetic val$handler:Landroid/os/Handler;

.field private final synthetic val$imagePath:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Landroid/os/Handler;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/photo/AsyncImageLoader$2;->val$imagePath:Ljava/lang/String;

    iput-object p2, p0, Lcom/konka/mm/photo/AsyncImageLoader$2;->val$handler:Landroid/os/Handler;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    iget-object v2, p0, Lcom/konka/mm/photo/AsyncImageLoader$2;->val$imagePath:Ljava/lang/String;

    const v3, 0x1fa400

    invoke-static {v2, v3}, Lcom/konka/mm/tools/PicTool;->decodeFileDescriptor(Ljava/lang/String;I)Landroid/graphics/Bitmap;

    move-result-object v0

    # getter for: Lcom/konka/mm/photo/AsyncImageLoader;->imageCache:Ljava/util/Map;
    invoke-static {}, Lcom/konka/mm/photo/AsyncImageLoader;->access$0()Ljava/util/Map;

    move-result-object v2

    iget-object v3, p0, Lcom/konka/mm/photo/AsyncImageLoader$2;->val$imagePath:Ljava/lang/String;

    new-instance v4, Ljava/lang/ref/SoftReference;

    invoke-direct {v4, v0}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, p0, Lcom/konka/mm/photo/AsyncImageLoader$2;->val$handler:Landroid/os/Handler;

    const/4 v3, 0x0

    invoke-virtual {v2, v3, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/mm/photo/AsyncImageLoader$2;->val$handler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method
