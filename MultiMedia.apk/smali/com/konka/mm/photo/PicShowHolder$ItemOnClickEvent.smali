.class public Lcom/konka/mm/photo/PicShowHolder$ItemOnClickEvent;
.super Ljava/lang/Object;
.source "PicShowHolder.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/mm/photo/PicShowHolder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ItemOnClickEvent"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/mm/photo/PicShowHolder;


# direct methods
.method public constructor <init>(Lcom/konka/mm/photo/PicShowHolder;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/photo/PicShowHolder$ItemOnClickEvent;->this$0:Lcom/konka/mm/photo/PicShowHolder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    const/4 v3, 0x3

    const/4 v1, 0x2

    const/4 v2, 0x1

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder$ItemOnClickEvent;->this$0:Lcom/konka/mm/photo/PicShowHolder;

    # getter for: Lcom/konka/mm/photo/PicShowHolder;->mPicShowActivity:Lcom/konka/mm/photo/AutoShowPicActivity;
    invoke-static {v0}, Lcom/konka/mm/photo/PicShowHolder;->access$1(Lcom/konka/mm/photo/PicShowHolder;)Lcom/konka/mm/photo/AutoShowPicActivity;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/konka/mm/photo/AutoShowPicActivity;->setState(I)V

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder$ItemOnClickEvent;->this$0:Lcom/konka/mm/photo/PicShowHolder;

    # getter for: Lcom/konka/mm/photo/PicShowHolder;->mPicShowActivity:Lcom/konka/mm/photo/AutoShowPicActivity;
    invoke-static {v0}, Lcom/konka/mm/photo/PicShowHolder;->access$1(Lcom/konka/mm/photo/PicShowHolder;)Lcom/konka/mm/photo/AutoShowPicActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/mm/photo/AutoShowPicActivity;->registerListeners()V

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder$ItemOnClickEvent;->this$0:Lcom/konka/mm/photo/PicShowHolder;

    iget-object v0, v0, Lcom/konka/mm/photo/PicShowHolder;->bt_wallpaper:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder$ItemOnClickEvent;->this$0:Lcom/konka/mm/photo/PicShowHolder;

    iget-object v0, v0, Lcom/konka/mm/photo/PicShowHolder;->bt_wallpaper:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder$ItemOnClickEvent;->this$0:Lcom/konka/mm/photo/PicShowHolder;

    # getter for: Lcom/konka/mm/photo/PicShowHolder;->mPicShowActivity:Lcom/konka/mm/photo/AutoShowPicActivity;
    invoke-static {v0}, Lcom/konka/mm/photo/PicShowHolder;->access$1(Lcom/konka/mm/photo/PicShowHolder;)Lcom/konka/mm/photo/AutoShowPicActivity;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/konka/mm/photo/AutoShowPicActivity;->setState(I)V

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder$ItemOnClickEvent;->this$0:Lcom/konka/mm/photo/PicShowHolder;

    # getter for: Lcom/konka/mm/photo/PicShowHolder;->mPicShowActivity:Lcom/konka/mm/photo/AutoShowPicActivity;
    invoke-static {v0}, Lcom/konka/mm/photo/PicShowHolder;->access$1(Lcom/konka/mm/photo/PicShowHolder;)Lcom/konka/mm/photo/AutoShowPicActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/mm/photo/AutoShowPicActivity;->registerListeners()V

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder$ItemOnClickEvent;->this$0:Lcom/konka/mm/photo/PicShowHolder;

    iget-object v0, v0, Lcom/konka/mm/photo/PicShowHolder;->bt_galleryShow:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder$ItemOnClickEvent;->this$0:Lcom/konka/mm/photo/PicShowHolder;

    iget-object v0, v0, Lcom/konka/mm/photo/PicShowHolder;->bt_galleryShow:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder$ItemOnClickEvent;->this$0:Lcom/konka/mm/photo/PicShowHolder;

    # getter for: Lcom/konka/mm/photo/PicShowHolder;->mPicShowActivity:Lcom/konka/mm/photo/AutoShowPicActivity;
    invoke-static {v0}, Lcom/konka/mm/photo/PicShowHolder;->access$1(Lcom/konka/mm/photo/PicShowHolder;)Lcom/konka/mm/photo/AutoShowPicActivity;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/konka/mm/photo/AutoShowPicActivity;->setState(I)V

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder$ItemOnClickEvent;->this$0:Lcom/konka/mm/photo/PicShowHolder;

    # getter for: Lcom/konka/mm/photo/PicShowHolder;->mPicShowActivity:Lcom/konka/mm/photo/AutoShowPicActivity;
    invoke-static {v0}, Lcom/konka/mm/photo/PicShowHolder;->access$1(Lcom/konka/mm/photo/PicShowHolder;)Lcom/konka/mm/photo/AutoShowPicActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/mm/photo/AutoShowPicActivity;->registerListeners()V

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder$ItemOnClickEvent;->this$0:Lcom/konka/mm/photo/PicShowHolder;

    iget-object v0, v0, Lcom/konka/mm/photo/PicShowHolder;->bt_pre:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder$ItemOnClickEvent;->this$0:Lcom/konka/mm/photo/PicShowHolder;

    iget-object v0, v0, Lcom/konka/mm/photo/PicShowHolder;->bt_pre:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    goto :goto_0

    :pswitch_4
    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder$ItemOnClickEvent;->this$0:Lcom/konka/mm/photo/PicShowHolder;

    # getter for: Lcom/konka/mm/photo/PicShowHolder;->mPicShowActivity:Lcom/konka/mm/photo/AutoShowPicActivity;
    invoke-static {v0}, Lcom/konka/mm/photo/PicShowHolder;->access$1(Lcom/konka/mm/photo/PicShowHolder;)Lcom/konka/mm/photo/AutoShowPicActivity;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/konka/mm/photo/AutoShowPicActivity;->setState(I)V

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder$ItemOnClickEvent;->this$0:Lcom/konka/mm/photo/PicShowHolder;

    # getter for: Lcom/konka/mm/photo/PicShowHolder;->mPicShowActivity:Lcom/konka/mm/photo/AutoShowPicActivity;
    invoke-static {v0}, Lcom/konka/mm/photo/PicShowHolder;->access$1(Lcom/konka/mm/photo/PicShowHolder;)Lcom/konka/mm/photo/AutoShowPicActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/mm/photo/AutoShowPicActivity;->registerListeners()V

    goto :goto_0

    :pswitch_5
    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder$ItemOnClickEvent;->this$0:Lcom/konka/mm/photo/PicShowHolder;

    # getter for: Lcom/konka/mm/photo/PicShowHolder;->mPicShowActivity:Lcom/konka/mm/photo/AutoShowPicActivity;
    invoke-static {v0}, Lcom/konka/mm/photo/PicShowHolder;->access$1(Lcom/konka/mm/photo/PicShowHolder;)Lcom/konka/mm/photo/AutoShowPicActivity;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/konka/mm/photo/AutoShowPicActivity;->setState(I)V

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder$ItemOnClickEvent;->this$0:Lcom/konka/mm/photo/PicShowHolder;

    # getter for: Lcom/konka/mm/photo/PicShowHolder;->mPicShowActivity:Lcom/konka/mm/photo/AutoShowPicActivity;
    invoke-static {v0}, Lcom/konka/mm/photo/PicShowHolder;->access$1(Lcom/konka/mm/photo/PicShowHolder;)Lcom/konka/mm/photo/AutoShowPicActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/mm/photo/AutoShowPicActivity;->registerListeners()V

    goto/16 :goto_0

    :pswitch_6
    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder$ItemOnClickEvent;->this$0:Lcom/konka/mm/photo/PicShowHolder;

    # getter for: Lcom/konka/mm/photo/PicShowHolder;->mPicShowActivity:Lcom/konka/mm/photo/AutoShowPicActivity;
    invoke-static {v0}, Lcom/konka/mm/photo/PicShowHolder;->access$1(Lcom/konka/mm/photo/PicShowHolder;)Lcom/konka/mm/photo/AutoShowPicActivity;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/konka/mm/photo/AutoShowPicActivity;->setState(I)V

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder$ItemOnClickEvent;->this$0:Lcom/konka/mm/photo/PicShowHolder;

    # getter for: Lcom/konka/mm/photo/PicShowHolder;->mPicShowActivity:Lcom/konka/mm/photo/AutoShowPicActivity;
    invoke-static {v0}, Lcom/konka/mm/photo/PicShowHolder;->access$1(Lcom/konka/mm/photo/PicShowHolder;)Lcom/konka/mm/photo/AutoShowPicActivity;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/konka/mm/photo/AutoShowPicActivity;->setState(I)V

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder$ItemOnClickEvent;->this$0:Lcom/konka/mm/photo/PicShowHolder;

    # getter for: Lcom/konka/mm/photo/PicShowHolder;->mPicShowActivity:Lcom/konka/mm/photo/AutoShowPicActivity;
    invoke-static {v0}, Lcom/konka/mm/photo/PicShowHolder;->access$1(Lcom/konka/mm/photo/PicShowHolder;)Lcom/konka/mm/photo/AutoShowPicActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/mm/photo/AutoShowPicActivity;->registerListeners()V

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder$ItemOnClickEvent;->this$0:Lcom/konka/mm/photo/PicShowHolder;

    iget-object v0, v0, Lcom/konka/mm/photo/PicShowHolder;->bt_next:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder$ItemOnClickEvent;->this$0:Lcom/konka/mm/photo/PicShowHolder;

    iget-object v0, v0, Lcom/konka/mm/photo/PicShowHolder;->bt_next:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    goto/16 :goto_0

    :pswitch_7
    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder$ItemOnClickEvent;->this$0:Lcom/konka/mm/photo/PicShowHolder;

    # getter for: Lcom/konka/mm/photo/PicShowHolder;->mPicShowActivity:Lcom/konka/mm/photo/AutoShowPicActivity;
    invoke-static {v0}, Lcom/konka/mm/photo/PicShowHolder;->access$1(Lcom/konka/mm/photo/PicShowHolder;)Lcom/konka/mm/photo/AutoShowPicActivity;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/konka/mm/photo/AutoShowPicActivity;->setState(I)V

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder$ItemOnClickEvent;->this$0:Lcom/konka/mm/photo/PicShowHolder;

    # getter for: Lcom/konka/mm/photo/PicShowHolder;->mPicShowActivity:Lcom/konka/mm/photo/AutoShowPicActivity;
    invoke-static {v0}, Lcom/konka/mm/photo/PicShowHolder;->access$1(Lcom/konka/mm/photo/PicShowHolder;)Lcom/konka/mm/photo/AutoShowPicActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/mm/photo/AutoShowPicActivity;->registerListeners()V

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder$ItemOnClickEvent;->this$0:Lcom/konka/mm/photo/PicShowHolder;

    iget-object v0, v0, Lcom/konka/mm/photo/PicShowHolder;->bt_stop:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder$ItemOnClickEvent;->this$0:Lcom/konka/mm/photo/PicShowHolder;

    iget-object v0, v0, Lcom/konka/mm/photo/PicShowHolder;->bt_stop:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    goto/16 :goto_0

    :pswitch_8
    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder$ItemOnClickEvent;->this$0:Lcom/konka/mm/photo/PicShowHolder;

    # getter for: Lcom/konka/mm/photo/PicShowHolder;->mPicShowActivity:Lcom/konka/mm/photo/AutoShowPicActivity;
    invoke-static {v0}, Lcom/konka/mm/photo/PicShowHolder;->access$1(Lcom/konka/mm/photo/PicShowHolder;)Lcom/konka/mm/photo/AutoShowPicActivity;

    move-result-object v0

    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Lcom/konka/mm/photo/AutoShowPicActivity;->setState(I)V

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder$ItemOnClickEvent;->this$0:Lcom/konka/mm/photo/PicShowHolder;

    # getter for: Lcom/konka/mm/photo/PicShowHolder;->mPicShowActivity:Lcom/konka/mm/photo/AutoShowPicActivity;
    invoke-static {v0}, Lcom/konka/mm/photo/PicShowHolder;->access$1(Lcom/konka/mm/photo/PicShowHolder;)Lcom/konka/mm/photo/AutoShowPicActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/mm/photo/AutoShowPicActivity;->registerListeners()V

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder$ItemOnClickEvent;->this$0:Lcom/konka/mm/photo/PicShowHolder;

    iget-object v0, v0, Lcom/konka/mm/photo/PicShowHolder;->text_play_order:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/mm/photo/PicShowHolder$ItemOnClickEvent;->this$0:Lcom/konka/mm/photo/PicShowHolder;

    # getter for: Lcom/konka/mm/photo/PicShowHolder;->mPicShowActivity:Lcom/konka/mm/photo/AutoShowPicActivity;
    invoke-static {v1}, Lcom/konka/mm/photo/PicShowHolder;->access$1(Lcom/konka/mm/photo/PicShowHolder;)Lcom/konka/mm/photo/AutoShowPicActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/konka/mm/photo/AutoShowPicActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09006a    # com.konka.mm.R.string.cycleTxt

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :pswitch_9
    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder$ItemOnClickEvent;->this$0:Lcom/konka/mm/photo/PicShowHolder;

    # getter for: Lcom/konka/mm/photo/PicShowHolder;->mPicShowActivity:Lcom/konka/mm/photo/AutoShowPicActivity;
    invoke-static {v0}, Lcom/konka/mm/photo/PicShowHolder;->access$1(Lcom/konka/mm/photo/PicShowHolder;)Lcom/konka/mm/photo/AutoShowPicActivity;

    move-result-object v0

    const/16 v1, 0x12

    invoke-virtual {v0, v1}, Lcom/konka/mm/photo/AutoShowPicActivity;->setState(I)V

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder$ItemOnClickEvent;->this$0:Lcom/konka/mm/photo/PicShowHolder;

    # getter for: Lcom/konka/mm/photo/PicShowHolder;->mPicShowActivity:Lcom/konka/mm/photo/AutoShowPicActivity;
    invoke-static {v0}, Lcom/konka/mm/photo/PicShowHolder;->access$1(Lcom/konka/mm/photo/PicShowHolder;)Lcom/konka/mm/photo/AutoShowPicActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/mm/photo/AutoShowPicActivity;->registerListeners()V

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder$ItemOnClickEvent;->this$0:Lcom/konka/mm/photo/PicShowHolder;

    iget-object v0, v0, Lcom/konka/mm/photo/PicShowHolder;->text_play_order:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/mm/photo/PicShowHolder$ItemOnClickEvent;->this$0:Lcom/konka/mm/photo/PicShowHolder;

    # getter for: Lcom/konka/mm/photo/PicShowHolder;->mPicShowActivity:Lcom/konka/mm/photo/AutoShowPicActivity;
    invoke-static {v1}, Lcom/konka/mm/photo/PicShowHolder;->access$1(Lcom/konka/mm/photo/PicShowHolder;)Lcom/konka/mm/photo/AutoShowPicActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/konka/mm/photo/AutoShowPicActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090068    # com.konka.mm.R.string.sequenceTxt

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :pswitch_a
    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder$ItemOnClickEvent;->this$0:Lcom/konka/mm/photo/PicShowHolder;

    # getter for: Lcom/konka/mm/photo/PicShowHolder;->mPicShowActivity:Lcom/konka/mm/photo/AutoShowPicActivity;
    invoke-static {v0}, Lcom/konka/mm/photo/PicShowHolder;->access$1(Lcom/konka/mm/photo/PicShowHolder;)Lcom/konka/mm/photo/AutoShowPicActivity;

    move-result-object v0

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lcom/konka/mm/photo/AutoShowPicActivity;->setState(I)V

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder$ItemOnClickEvent;->this$0:Lcom/konka/mm/photo/PicShowHolder;

    # getter for: Lcom/konka/mm/photo/PicShowHolder;->mPicShowActivity:Lcom/konka/mm/photo/AutoShowPicActivity;
    invoke-static {v0}, Lcom/konka/mm/photo/PicShowHolder;->access$1(Lcom/konka/mm/photo/PicShowHolder;)Lcom/konka/mm/photo/AutoShowPicActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/mm/photo/AutoShowPicActivity;->registerListeners()V

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder$ItemOnClickEvent;->this$0:Lcom/konka/mm/photo/PicShowHolder;

    iget-object v0, v0, Lcom/konka/mm/photo/PicShowHolder;->bt_leftRotion:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder$ItemOnClickEvent;->this$0:Lcom/konka/mm/photo/PicShowHolder;

    iget-object v0, v0, Lcom/konka/mm/photo/PicShowHolder;->bt_leftRotion:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    goto/16 :goto_0

    :pswitch_b
    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder$ItemOnClickEvent;->this$0:Lcom/konka/mm/photo/PicShowHolder;

    # getter for: Lcom/konka/mm/photo/PicShowHolder;->mPicShowActivity:Lcom/konka/mm/photo/AutoShowPicActivity;
    invoke-static {v0}, Lcom/konka/mm/photo/PicShowHolder;->access$1(Lcom/konka/mm/photo/PicShowHolder;)Lcom/konka/mm/photo/AutoShowPicActivity;

    move-result-object v0

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Lcom/konka/mm/photo/AutoShowPicActivity;->setState(I)V

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder$ItemOnClickEvent;->this$0:Lcom/konka/mm/photo/PicShowHolder;

    # getter for: Lcom/konka/mm/photo/PicShowHolder;->mPicShowActivity:Lcom/konka/mm/photo/AutoShowPicActivity;
    invoke-static {v0}, Lcom/konka/mm/photo/PicShowHolder;->access$1(Lcom/konka/mm/photo/PicShowHolder;)Lcom/konka/mm/photo/AutoShowPicActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/mm/photo/AutoShowPicActivity;->registerListeners()V

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder$ItemOnClickEvent;->this$0:Lcom/konka/mm/photo/PicShowHolder;

    iget-object v0, v0, Lcom/konka/mm/photo/PicShowHolder;->bt_rightRotion:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder$ItemOnClickEvent;->this$0:Lcom/konka/mm/photo/PicShowHolder;

    iget-object v0, v0, Lcom/konka/mm/photo/PicShowHolder;->bt_rightRotion:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    goto/16 :goto_0

    :pswitch_c
    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder$ItemOnClickEvent;->this$0:Lcom/konka/mm/photo/PicShowHolder;

    # getter for: Lcom/konka/mm/photo/PicShowHolder;->mPicShowActivity:Lcom/konka/mm/photo/AutoShowPicActivity;
    invoke-static {v0}, Lcom/konka/mm/photo/PicShowHolder;->access$1(Lcom/konka/mm/photo/PicShowHolder;)Lcom/konka/mm/photo/AutoShowPicActivity;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/konka/mm/photo/AutoShowPicActivity;->setState(I)V

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder$ItemOnClickEvent;->this$0:Lcom/konka/mm/photo/PicShowHolder;

    # getter for: Lcom/konka/mm/photo/PicShowHolder;->mPicShowActivity:Lcom/konka/mm/photo/AutoShowPicActivity;
    invoke-static {v0}, Lcom/konka/mm/photo/PicShowHolder;->access$1(Lcom/konka/mm/photo/PicShowHolder;)Lcom/konka/mm/photo/AutoShowPicActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/mm/photo/AutoShowPicActivity;->registerListeners()V

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder$ItemOnClickEvent;->this$0:Lcom/konka/mm/photo/PicShowHolder;

    iget-object v0, v0, Lcom/konka/mm/photo/PicShowHolder;->bt_big:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder$ItemOnClickEvent;->this$0:Lcom/konka/mm/photo/PicShowHolder;

    iget-object v0, v0, Lcom/konka/mm/photo/PicShowHolder;->bt_big:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    goto/16 :goto_0

    :pswitch_d
    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder$ItemOnClickEvent;->this$0:Lcom/konka/mm/photo/PicShowHolder;

    # getter for: Lcom/konka/mm/photo/PicShowHolder;->mPicShowActivity:Lcom/konka/mm/photo/AutoShowPicActivity;
    invoke-static {v0}, Lcom/konka/mm/photo/PicShowHolder;->access$1(Lcom/konka/mm/photo/PicShowHolder;)Lcom/konka/mm/photo/AutoShowPicActivity;

    move-result-object v0

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Lcom/konka/mm/photo/AutoShowPicActivity;->setState(I)V

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder$ItemOnClickEvent;->this$0:Lcom/konka/mm/photo/PicShowHolder;

    # getter for: Lcom/konka/mm/photo/PicShowHolder;->mPicShowActivity:Lcom/konka/mm/photo/AutoShowPicActivity;
    invoke-static {v0}, Lcom/konka/mm/photo/PicShowHolder;->access$1(Lcom/konka/mm/photo/PicShowHolder;)Lcom/konka/mm/photo/AutoShowPicActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/mm/photo/AutoShowPicActivity;->registerListeners()V

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder$ItemOnClickEvent;->this$0:Lcom/konka/mm/photo/PicShowHolder;

    iget-object v0, v0, Lcom/konka/mm/photo/PicShowHolder;->bt_small:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder$ItemOnClickEvent;->this$0:Lcom/konka/mm/photo/PicShowHolder;

    iget-object v0, v0, Lcom/konka/mm/photo/PicShowHolder;->bt_small:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    goto/16 :goto_0

    :pswitch_e
    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder$ItemOnClickEvent;->this$0:Lcom/konka/mm/photo/PicShowHolder;

    # getter for: Lcom/konka/mm/photo/PicShowHolder;->mPicShowActivity:Lcom/konka/mm/photo/AutoShowPicActivity;
    invoke-static {v0}, Lcom/konka/mm/photo/PicShowHolder;->access$1(Lcom/konka/mm/photo/PicShowHolder;)Lcom/konka/mm/photo/AutoShowPicActivity;

    move-result-object v0

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Lcom/konka/mm/photo/AutoShowPicActivity;->setState(I)V

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder$ItemOnClickEvent;->this$0:Lcom/konka/mm/photo/PicShowHolder;

    # getter for: Lcom/konka/mm/photo/PicShowHolder;->mPicShowActivity:Lcom/konka/mm/photo/AutoShowPicActivity;
    invoke-static {v0}, Lcom/konka/mm/photo/PicShowHolder;->access$1(Lcom/konka/mm/photo/PicShowHolder;)Lcom/konka/mm/photo/AutoShowPicActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/mm/photo/AutoShowPicActivity;->registerListeners()V

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder$ItemOnClickEvent;->this$0:Lcom/konka/mm/photo/PicShowHolder;

    iget-object v0, v0, Lcom/konka/mm/photo/PicShowHolder;->bt_fullScreen:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder$ItemOnClickEvent;->this$0:Lcom/konka/mm/photo/PicShowHolder;

    iget-object v0, v0, Lcom/konka/mm/photo/PicShowHolder;->bt_fullScreen:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7f0b00de
        :pswitch_1    # com.konka.mm.R.id.buttomitem0
        :pswitch_0    # com.konka.mm.R.id.textview_item0
        :pswitch_2    # com.konka.mm.R.id.buttomitem1
        :pswitch_0    # com.konka.mm.R.id.textview_item1
        :pswitch_3    # com.konka.mm.R.id.buttomitem2
        :pswitch_0    # com.konka.mm.R.id.textview_item2
        :pswitch_4    # com.konka.mm.R.id.buttomitem3
        :pswitch_5    # com.konka.mm.R.id.buttomitem13
        :pswitch_0    # com.konka.mm.R.id.textview_item3
        :pswitch_6    # com.konka.mm.R.id.buttomitem4
        :pswitch_0    # com.konka.mm.R.id.textview_item4
        :pswitch_7    # com.konka.mm.R.id.buttomitem5
        :pswitch_0    # com.konka.mm.R.id.textview_item5
        :pswitch_8    # com.konka.mm.R.id.buttomitem11
        :pswitch_9    # com.konka.mm.R.id.buttomitem12
        :pswitch_a    # com.konka.mm.R.id.buttomitem6
        :pswitch_0    # com.konka.mm.R.id.textview_item6
        :pswitch_b    # com.konka.mm.R.id.buttomitem7
        :pswitch_0    # com.konka.mm.R.id.textview_item7
        :pswitch_c    # com.konka.mm.R.id.buttomitem8
        :pswitch_0    # com.konka.mm.R.id.textview_item8
        :pswitch_d    # com.konka.mm.R.id.buttomitem9
        :pswitch_0    # com.konka.mm.R.id.textview_item9
        :pswitch_e    # com.konka.mm.R.id.buttomitem10
    .end packed-switch
.end method
