.class public Lcom/konka/mm/photo/GalleryShowPicActivity$EthStateReceiver;
.super Landroid/content/BroadcastReceiver;
.source "GalleryShowPicActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/mm/photo/GalleryShowPicActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "EthStateReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/mm/photo/GalleryShowPicActivity;


# direct methods
.method public constructor <init>(Lcom/konka/mm/photo/GalleryShowPicActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/photo/GalleryShowPicActivity$EthStateReceiver;->this$0:Lcom/konka/mm/photo/GalleryShowPicActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.net.ethernet.ETHERNET_STATE_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/mm/photo/GalleryShowPicActivity$EthStateReceiver;->this$0:Lcom/konka/mm/photo/GalleryShowPicActivity;

    invoke-static {v0}, Lcom/konka/mm/model/FamilyShareModel;->isConneted2Network(Landroid/app/Activity;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/konka/mm/photo/GalleryShowPicActivity$EthStateReceiver;->this$0:Lcom/konka/mm/photo/GalleryShowPicActivity;

    # getter for: Lcom/konka/mm/photo/GalleryShowPicActivity;->sourceComeFrom:Ljava/lang/String;
    invoke-static {v0}, Lcom/konka/mm/photo/GalleryShowPicActivity;->access$1(Lcom/konka/mm/photo/GalleryShowPicActivity;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.konka.mm.file.come.from.samba"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/mm/photo/GalleryShowPicActivity$EthStateReceiver;->this$0:Lcom/konka/mm/photo/GalleryShowPicActivity;

    invoke-virtual {v0}, Lcom/konka/mm/photo/GalleryShowPicActivity;->finish()V

    :cond_0
    return-void
.end method
