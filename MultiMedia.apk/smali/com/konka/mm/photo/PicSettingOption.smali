.class public Lcom/konka/mm/photo/PicSettingOption;
.super Landroid/widget/LinearLayout;
.source "PicSettingOption.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/mm/photo/PicSettingOption$ItemOnChangeEvent;,
        Lcom/konka/mm/photo/PicSettingOption$ItemOnClickEvent;,
        Lcom/konka/mm/photo/PicSettingOption$ItemOnKeyEvent;
    }
.end annotation


# static fields
.field public static final DEFAULT_SORT:I = 0x1

.field private static DOWN_KEY_CODE:I = 0x0

.field public static final IMG_SETTING_ITEM_FOUCE:I = 0x7f02004a

.field private static ITEM_HEIGHT:I = 0x0

.field private static ITEM_WIDTH:I = 0x0

.field public static final NAME_SORT:I = 0x2

.field public static final NameOfSort:[I

.field public static final SIZE_SORT:I = 0x3

.field public static final TEXT_COLOR_ITEM_FOCUSED:I = -0x1

.field public static final TEXT_COLOR_ITEM_UNFOCUSED:I = -0x7c7c78

.field public static final TEXT_SIZE_ITEM_FOCUSED:I = 0x12

.field public static final TEXT_SIZE_ITEM_UNFOCUSED:I = 0x10

.field private static UP_KEY_CODE:I


# instance fields
.field private container:Landroid/widget/LinearLayout;

.field private imgIcon:Landroid/widget/ImageView;

.field private itemName:Ljava/lang/CharSequence;

.field private mPicMainAct:Lcom/konka/mm/photo/PicMainActivity;

.field private txtItemNameView:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/konka/mm/photo/PicSettingOption;->NameOfSort:[I

    const/16 v0, 0x13

    sput v0, Lcom/konka/mm/photo/PicSettingOption;->UP_KEY_CODE:I

    const/16 v0, 0x14

    sput v0, Lcom/konka/mm/photo/PicSettingOption;->DOWN_KEY_CODE:I

    const/16 v0, 0x12c

    sput v0, Lcom/konka/mm/photo/PicSettingOption;->ITEM_WIDTH:I

    const/16 v0, 0x28

    sput v0, Lcom/konka/mm/photo/PicSettingOption;->ITEM_HEIGHT:I

    return-void

    nop

    :array_0
    .array-data 4
        0x7f0900d4    # com.konka.mm.R.string.pic_setting_defaule_sort
        0x7f0900d5    # com.konka.mm.R.string.pic_setting_name_sort
        0x7f0900d6    # com.konka.mm.R.string.pic_setting_size_sort
    .end array-data
.end method

.method public constructor <init>(Lcom/konka/mm/photo/PicMainActivity;I)V
    .locals 8
    .param p1    # Lcom/konka/mm/photo/PicMainActivity;
    .param p2    # I

    const/4 v7, 0x0

    const/4 v6, 0x0

    const/4 v5, 0x1

    const/4 v4, -0x1

    invoke-direct {p0, p1, v6}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object p1, p0, Lcom/konka/mm/photo/PicSettingOption;->mPicMainAct:Lcom/konka/mm/photo/PicMainActivity;

    invoke-virtual {p0}, Lcom/konka/mm/photo/PicSettingOption;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/mm/photo/PicSettingOption;->itemName:Ljava/lang/CharSequence;

    invoke-virtual {p0, v7}, Lcom/konka/mm/photo/PicSettingOption;->setOrientation(I)V

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    sget v2, Lcom/konka/mm/photo/PicSettingOption;->ITEM_WIDTH:I

    sget v3, Lcom/konka/mm/photo/PicSettingOption;->ITEM_HEIGHT:I

    invoke-direct {v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v1}, Lcom/konka/mm/photo/PicSettingOption;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v1, Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/konka/mm/photo/PicSettingOption;->mPicMainAct:Lcom/konka/mm/photo/PicMainActivity;

    invoke-direct {v1, v2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/konka/mm/photo/PicSettingOption;->container:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/konka/mm/photo/PicSettingOption;->container:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v7}, Landroid/widget/LinearLayout;->setOrientation(I)V

    iget-object v1, p0, Lcom/konka/mm/photo/PicSettingOption;->container:Landroid/widget/LinearLayout;

    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v2, v4, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p0, Lcom/konka/mm/photo/PicSettingOption;->container:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->setClickable(Z)V

    iget-object v1, p0, Lcom/konka/mm/photo/PicSettingOption;->container:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    iget-object v1, p0, Lcom/konka/mm/photo/PicSettingOption;->container:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->setFocusableInTouchMode(Z)V

    new-instance v1, Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/konka/mm/photo/PicSettingOption;->mPicMainAct:Lcom/konka/mm/photo/PicMainActivity;

    invoke-direct {v1, v2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/konka/mm/photo/PicSettingOption;->imgIcon:Landroid/widget/ImageView;

    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/16 v1, 0x28

    invoke-direct {v0, v1, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    iget-object v1, p0, Lcom/konka/mm/photo/PicSettingOption;->imgIcon:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p0, Lcom/konka/mm/photo/PicSettingOption;->imgIcon:Landroid/widget/ImageView;

    const v2, 0x7f02004a    # com.konka.mm.R.drawable.file_select_button

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    iget-object v1, p0, Lcom/konka/mm/photo/PicSettingOption;->imgIcon:Landroid/widget/ImageView;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p0, Lcom/konka/mm/photo/PicSettingOption;->imgIcon:Landroid/widget/ImageView;

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setClickable(Z)V

    iget-object v1, p0, Lcom/konka/mm/photo/PicSettingOption;->container:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/konka/mm/photo/PicSettingOption;->imgIcon:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    new-instance v1, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/konka/mm/photo/PicSettingOption;->mPicMainAct:Lcom/konka/mm/photo/PicMainActivity;

    invoke-direct {v1, v2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/konka/mm/photo/PicSettingOption;->txtItemNameView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/mm/photo/PicSettingOption;->txtItemNameView:Landroid/widget/TextView;

    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    const/16 v3, 0x6e

    invoke-direct {v2, v3, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p0, Lcom/konka/mm/photo/PicSettingOption;->txtItemNameView:Landroid/widget/TextView;

    const v2, -0x7c7c78

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v1, p0, Lcom/konka/mm/photo/PicSettingOption;->txtItemNameView:Landroid/widget/TextView;

    const/high16 v2, 0x41800000    # 16.0f

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextSize(F)V

    iget-object v1, p0, Lcom/konka/mm/photo/PicSettingOption;->txtItemNameView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/konka/mm/photo/PicSettingOption;->itemName:Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/mm/photo/PicSettingOption;->txtItemNameView:Landroid/widget/TextView;

    const/16 v2, 0x11

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setGravity(I)V

    iget-object v1, p0, Lcom/konka/mm/photo/PicSettingOption;->container:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/konka/mm/photo/PicSettingOption;->txtItemNameView:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/konka/mm/photo/PicSettingOption;->container:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v1}, Lcom/konka/mm/photo/PicSettingOption;->addView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/konka/mm/photo/PicSettingOption;->container:Landroid/widget/LinearLayout;

    new-instance v2, Lcom/konka/mm/photo/PicSettingOption$ItemOnKeyEvent;

    invoke-direct {v2, p0, v6}, Lcom/konka/mm/photo/PicSettingOption$ItemOnKeyEvent;-><init>(Lcom/konka/mm/photo/PicSettingOption;Lcom/konka/mm/photo/PicSettingOption$ItemOnKeyEvent;)V

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v1, p0, Lcom/konka/mm/photo/PicSettingOption;->container:Landroid/widget/LinearLayout;

    new-instance v2, Lcom/konka/mm/photo/PicSettingOption$ItemOnChangeEvent;

    invoke-direct {v2, p0, v6}, Lcom/konka/mm/photo/PicSettingOption$ItemOnChangeEvent;-><init>(Lcom/konka/mm/photo/PicSettingOption;Lcom/konka/mm/photo/PicSettingOption$ItemOnChangeEvent;)V

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v1, p0, Lcom/konka/mm/photo/PicSettingOption;->container:Landroid/widget/LinearLayout;

    new-instance v2, Lcom/konka/mm/photo/PicSettingOption$ItemOnClickEvent;

    invoke-direct {v2, p0, v6}, Lcom/konka/mm/photo/PicSettingOption$ItemOnClickEvent;-><init>(Lcom/konka/mm/photo/PicSettingOption;Lcom/konka/mm/photo/PicSettingOption$ItemOnClickEvent;)V

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method static synthetic access$0(Lcom/konka/mm/photo/PicSettingOption;)Landroid/widget/LinearLayout;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/photo/PicSettingOption;->container:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$1(Lcom/konka/mm/photo/PicSettingOption;)Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/photo/PicSettingOption;->imgIcon:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$2(Lcom/konka/mm/photo/PicSettingOption;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/photo/PicSettingOption;->txtItemNameView:Landroid/widget/TextView;

    return-object v0
.end method
