.class public Lcom/konka/mm/photo/ImageCache;
.super Ljava/lang/Object;
.source "ImageCache.java"


# static fields
.field private static final HARD_CACHE_MAX_SIZE:I = 0x800000

.field private static final SOFT_CACHE_CAPACITY:I = 0x28

.field private static final TAG:Ljava/lang/String; = "ImageCache"

.field private static instance:Lcom/konka/mm/photo/ImageCache;

.field private static final sSoftBitmapCache:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/ref/SoftReference",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Lcom/konka/mm/photo/ImageCache;

    invoke-direct {v0}, Lcom/konka/mm/photo/ImageCache;-><init>()V

    sput-object v0, Lcom/konka/mm/photo/ImageCache;->instance:Lcom/konka/mm/photo/ImageCache;

    new-instance v0, Lcom/konka/mm/photo/ImageCache$1;

    const/16 v1, 0x28

    const/high16 v2, 0x3f400000    # 0.75f

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/konka/mm/photo/ImageCache$1;-><init>(IFZ)V

    sput-object v0, Lcom/konka/mm/photo/ImageCache;->sSoftBitmapCache:Ljava/util/LinkedHashMap;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getInstance()Lcom/konka/mm/photo/ImageCache;
    .locals 1

    sget-object v0, Lcom/konka/mm/photo/ImageCache;->instance:Lcom/konka/mm/photo/ImageCache;

    return-object v0
.end method


# virtual methods
.method public getBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 4
    .param p1    # Ljava/lang/String;

    sget-object v3, Lcom/konka/mm/photo/ImageCache;->sSoftBitmapCache:Ljava/util/LinkedHashMap;

    monitor-enter v3

    :try_start_0
    sget-object v2, Lcom/konka/mm/photo/ImageCache;->sSoftBitmapCache:Ljava/util/LinkedHashMap;

    invoke-virtual {v2, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/ref/SoftReference;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    monitor-exit v3

    :goto_0
    return-object v0

    :cond_0
    sget-object v2, Lcom/konka/mm/photo/ImageCache;->sSoftBitmapCache:Ljava/util/LinkedHashMap;

    invoke-virtual {v2, p1}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    monitor-exit v3

    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public pubBitmap(Ljava/lang/String;Landroid/graphics/Bitmap;)Z
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/graphics/Bitmap;

    if-eqz p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
