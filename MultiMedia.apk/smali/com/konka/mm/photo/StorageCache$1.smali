.class Lcom/konka/mm/photo/StorageCache$1;
.super Landroid/util/LruCache;
.source "StorageCache.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/mm/photo/StorageCache;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/util/LruCache",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Long;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/mm/photo/StorageCache;


# direct methods
.method constructor <init>(Lcom/konka/mm/photo/StorageCache;I)V
    .locals 0
    .param p2    # I

    iput-object p1, p0, Lcom/konka/mm/photo/StorageCache$1;->this$0:Lcom/konka/mm/photo/StorageCache;

    invoke-direct {p0, p2}, Landroid/util/LruCache;-><init>(I)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic entryRemoved(ZLjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    check-cast p2, Ljava/lang/String;

    check-cast p3, Ljava/lang/Long;

    check-cast p4, Ljava/lang/Long;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/konka/mm/photo/StorageCache$1;->entryRemoved(ZLjava/lang/String;Ljava/lang/Long;Ljava/lang/Long;)V

    return-void
.end method

.method protected entryRemoved(ZLjava/lang/String;Ljava/lang/Long;Ljava/lang/Long;)V
    .locals 3
    .param p1    # Z
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/Long;
    .param p4    # Ljava/lang/Long;

    :try_start_0
    iget-object v2, p0, Lcom/konka/mm/photo/StorageCache$1;->this$0:Lcom/konka/mm/photo/StorageCache;

    # invokes: Lcom/konka/mm/photo/StorageCache;->getFile(Ljava/lang/String;)Ljava/io/File;
    invoke-static {v2, p2}, Lcom/konka/mm/photo/StorageCache;->access$0(Lcom/konka/mm/photo/StorageCache;Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/konka/mm/photo/StorageCache$1;->this$0:Lcom/konka/mm/photo/StorageCache;

    # getter for: Lcom/konka/mm/photo/StorageCache;->sExternalCache:Landroid/util/LruCache;
    invoke-static {v2}, Lcom/konka/mm/photo/StorageCache;->access$1(Lcom/konka/mm/photo/StorageCache;)Landroid/util/LruCache;

    move-result-object v2

    invoke-virtual {v2, p2, p3}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method protected bridge synthetic sizeOf(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Ljava/lang/String;

    check-cast p2, Ljava/lang/Long;

    invoke-virtual {p0, p1, p2}, Lcom/konka/mm/photo/StorageCache$1;->sizeOf(Ljava/lang/String;Ljava/lang/Long;)I

    move-result v0

    return v0
.end method

.method protected sizeOf(Ljava/lang/String;Ljava/lang/Long;)I
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/Long;

    invoke-virtual {p2}, Ljava/lang/Long;->intValue()I

    move-result v0

    return v0
.end method
