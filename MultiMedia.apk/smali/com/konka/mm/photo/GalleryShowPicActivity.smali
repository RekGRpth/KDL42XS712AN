.class public Lcom/konka/mm/photo/GalleryShowPicActivity;
.super Landroid/app/Activity;
.source "GalleryShowPicActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ShowToast"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/mm/photo/GalleryShowPicActivity$EthStateReceiver;,
        Lcom/konka/mm/photo/GalleryShowPicActivity$ImageAdapter;,
        Lcom/konka/mm/photo/GalleryShowPicActivity$PicItem;
    }
.end annotation


# static fields
.field public static final PROGRESS_DIALOG_LOAD:I = 0x1

.field protected static executor:Ljava/util/concurrent/ExecutorService;

.field private static isProgressDlg:Z

.field private static progressDialog:Landroid/app/ProgressDialog;

.field private static sNextInAnim:Landroid/view/animation/AnimationSet;

.field private static sNextOutAnim:Landroid/view/animation/AnimationSet;

.field private static sPrevInAnim:Landroid/view/animation/AnimationSet;

.field private static sPrevOutAnim:Landroid/view/animation/AnimationSet;


# instance fields
.field protected bitmapArray:Lcom/konka/mm/photo/RotateBitmap;

.field private currentIp:Ljava/lang/String;

.field private currentNum:I

.field private gallery:Landroid/widget/Gallery;

.field private handler:Landroid/os/Handler;

.field protected imgPost:I

.field private is3dKeyDown:Z

.field private isEnterOtherAct:Z

.field protected mAlphaIn:Landroid/view/animation/Animation;

.field protected mAlphaOut:Landroid/view/animation/Animation;

.field private mEthStateReceiver:Lcom/konka/mm/photo/GalleryShowPicActivity$EthStateReceiver;

.field private mReceiver:Landroid/content/BroadcastReceiver;

.field private picImageview:Lcom/konka/mm/photo/ImageViewTouch;

.field private picPaths:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private prevAnimView:Landroid/view/View;

.field private previousNum:I

.field private root_path:Ljava/lang/String;

.field private sourceComeFrom:Ljava/lang/String;

.field toast_file_not_exist:Landroid/widget/Toast;

.field private toast_first_image:Landroid/widget/Toast;

.field private toast_last_image:Landroid/widget/Toast;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x3

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    sput-object v0, Lcom/konka/mm/photo/GalleryShowPicActivity;->executor:Ljava/util/concurrent/ExecutorService;

    const/4 v0, 0x0

    sput-boolean v0, Lcom/konka/mm/photo/GalleryShowPicActivity;->isProgressDlg:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput-object v1, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->root_path:Ljava/lang/String;

    iput-object v1, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->toast_first_image:Landroid/widget/Toast;

    iput-object v1, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->toast_last_image:Landroid/widget/Toast;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->picPaths:Ljava/util/ArrayList;

    new-instance v0, Lcom/konka/mm/photo/RotateBitmap;

    invoke-direct {v0, v1}, Lcom/konka/mm/photo/RotateBitmap;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->bitmapArray:Lcom/konka/mm/photo/RotateBitmap;

    const/4 v0, -0x1

    iput v0, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->imgPost:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->isEnterOtherAct:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->is3dKeyDown:Z

    new-instance v0, Lcom/konka/mm/photo/GalleryShowPicActivity$1;

    invoke-direct {v0, p0}, Lcom/konka/mm/photo/GalleryShowPicActivity$1;-><init>(Lcom/konka/mm/photo/GalleryShowPicActivity;)V

    iput-object v0, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->handler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$0(Lcom/konka/mm/photo/GalleryShowPicActivity;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->picPaths:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1(Lcom/konka/mm/photo/GalleryShowPicActivity;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->sourceComeFrom:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2(Lcom/konka/mm/photo/GalleryShowPicActivity;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->currentIp:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3(Lcom/konka/mm/photo/GalleryShowPicActivity;Landroid/widget/ImageView;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/konka/mm/photo/GalleryShowPicActivity;->setThumbImage(Landroid/widget/ImageView;I)V

    return-void
.end method

.method static synthetic access$4(Lcom/konka/mm/photo/GalleryShowPicActivity;Landroid/content/Intent;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/mm/photo/GalleryShowPicActivity;->onReceiveSdCardBroadCast(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic access$5(Lcom/konka/mm/photo/GalleryShowPicActivity;I)V
    .locals 0

    iput p1, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->currentNum:I

    return-void
.end method

.method static synthetic access$6(Lcom/konka/mm/photo/GalleryShowPicActivity;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method public static ensureGLCompatibleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 3
    .param p0    # Landroid/graphics/Bitmap;

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v1

    if-eqz v1, :cond_1

    :cond_0
    move-object v0, p0

    :goto_0
    return-object v0

    :cond_1
    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->recycle()V

    invoke-static {}, Ljava/lang/System;->gc()V

    goto :goto_0
.end method

.method protected static hideDialog(I)V
    .locals 1
    .param p0    # I

    const/4 v0, 0x1

    if-ne p0, v0, :cond_0

    sget-object v0, Lcom/konka/mm/photo/GalleryShowPicActivity;->progressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/konka/mm/photo/GalleryShowPicActivity;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    const/4 v0, 0x0

    sput-boolean v0, Lcom/konka/mm/photo/GalleryShowPicActivity;->isProgressDlg:Z

    :cond_0
    return-void
.end method

.method private onReceiveSdCardBroadCast(Landroid/content/Intent;)V
    .locals 3
    .param p1    # Landroid/content/Intent;

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->sourceComeFrom:Ljava/lang/String;

    const-string v2, "com.konka.mm.file.come.from.samba"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->root_path:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->root_path:Ljava/lang/String;

    invoke-static {p0, v1}, Lcom/konka/mm/tools/FileTool;->checkUsbExist(Landroid/app/Activity;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/konka/mm/photo/GalleryShowPicActivity;->finish()V

    goto :goto_0
.end method

.method private setThumbImage(Landroid/widget/ImageView;I)V
    .locals 2
    .param p1    # Landroid/widget/ImageView;
    .param p2    # I

    sget-object v0, Lcom/konka/mm/photo/GalleryShowPicActivity;->executor:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/konka/mm/photo/GalleryShowPicActivity$6;

    invoke-direct {v1, p0, p2, p1}, Lcom/konka/mm/photo/GalleryShowPicActivity$6;-><init>(Lcom/konka/mm/photo/GalleryShowPicActivity;ILandroid/widget/ImageView;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    return-void
.end method


# virtual methods
.method public InitAnimationSet()V
    .locals 17

    const/high16 v5, 0x7f040000    # com.konka.mm.R.anim.alpha_in

    move-object/from16 v0, p0

    invoke-static {v0, v5}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/konka/mm/photo/GalleryShowPicActivity;->mAlphaIn:Landroid/view/animation/Animation;

    const v5, 0x7f040001    # com.konka.mm.R.anim.alpha_out

    move-object/from16 v0, p0

    invoke-static {v0, v5}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/konka/mm/photo/GalleryShowPicActivity;->mAlphaOut:Landroid/view/animation/Animation;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/konka/mm/photo/GalleryShowPicActivity;->mAlphaIn:Landroid/view/animation/Animation;

    const-wide/16 v6, 0x5dc

    invoke-virtual {v5, v6, v7}, Landroid/view/animation/Animation;->setDuration(J)V

    new-instance v1, Landroid/view/animation/TranslateAnimation;

    const/4 v2, 0x1

    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x0

    invoke-direct/range {v1 .. v9}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    new-instance v13, Landroid/view/animation/AlphaAnimation;

    const/4 v5, 0x0

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-direct {v13, v5, v6}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    new-instance v5, Landroid/view/animation/AnimationSet;

    const/4 v6, 0x1

    invoke-direct {v5, v6}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    sput-object v5, Lcom/konka/mm/photo/GalleryShowPicActivity;->sNextInAnim:Landroid/view/animation/AnimationSet;

    sget-object v5, Lcom/konka/mm/photo/GalleryShowPicActivity;->sNextInAnim:Landroid/view/animation/AnimationSet;

    invoke-virtual {v5, v1}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    sget-object v5, Lcom/konka/mm/photo/GalleryShowPicActivity;->sNextInAnim:Landroid/view/animation/AnimationSet;

    invoke-virtual {v5, v13}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    sget-object v5, Lcom/konka/mm/photo/GalleryShowPicActivity;->sNextInAnim:Landroid/view/animation/AnimationSet;

    const-wide/16 v6, 0x320

    invoke-virtual {v5, v6, v7}, Landroid/view/animation/AnimationSet;->setDuration(J)V

    new-instance v2, Landroid/view/animation/TranslateAnimation;

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/high16 v6, -0x40800000    # -1.0f

    const/4 v7, 0x1

    const/4 v8, 0x0

    const/4 v9, 0x1

    const/4 v10, 0x0

    invoke-direct/range {v2 .. v10}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    new-instance v14, Landroid/view/animation/AlphaAnimation;

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    invoke-direct {v14, v5, v6}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    new-instance v5, Landroid/view/animation/AnimationSet;

    const/4 v6, 0x1

    invoke-direct {v5, v6}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    sput-object v5, Lcom/konka/mm/photo/GalleryShowPicActivity;->sNextOutAnim:Landroid/view/animation/AnimationSet;

    sget-object v5, Lcom/konka/mm/photo/GalleryShowPicActivity;->sNextOutAnim:Landroid/view/animation/AnimationSet;

    invoke-virtual {v5, v2}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    sget-object v5, Lcom/konka/mm/photo/GalleryShowPicActivity;->sNextOutAnim:Landroid/view/animation/AnimationSet;

    invoke-virtual {v5, v14}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    sget-object v5, Lcom/konka/mm/photo/GalleryShowPicActivity;->sNextOutAnim:Landroid/view/animation/AnimationSet;

    const-wide/16 v6, 0x320

    invoke-virtual {v5, v6, v7}, Landroid/view/animation/AnimationSet;->setDuration(J)V

    new-instance v3, Landroid/view/animation/TranslateAnimation;

    const/4 v4, 0x1

    const/high16 v5, -0x40800000    # -1.0f

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x0

    const/4 v10, 0x1

    const/4 v11, 0x0

    invoke-direct/range {v3 .. v11}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    new-instance v15, Landroid/view/animation/AlphaAnimation;

    const/4 v5, 0x0

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-direct {v15, v5, v6}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    new-instance v5, Landroid/view/animation/AnimationSet;

    const/4 v6, 0x1

    invoke-direct {v5, v6}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    sput-object v5, Lcom/konka/mm/photo/GalleryShowPicActivity;->sPrevInAnim:Landroid/view/animation/AnimationSet;

    sget-object v5, Lcom/konka/mm/photo/GalleryShowPicActivity;->sPrevInAnim:Landroid/view/animation/AnimationSet;

    invoke-virtual {v5, v3}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    sget-object v5, Lcom/konka/mm/photo/GalleryShowPicActivity;->sPrevInAnim:Landroid/view/animation/AnimationSet;

    invoke-virtual {v5, v15}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    sget-object v5, Lcom/konka/mm/photo/GalleryShowPicActivity;->sPrevInAnim:Landroid/view/animation/AnimationSet;

    const-wide/16 v6, 0x320

    invoke-virtual {v5, v6, v7}, Landroid/view/animation/AnimationSet;->setDuration(J)V

    new-instance v4, Landroid/view/animation/TranslateAnimation;

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/high16 v8, 0x3f800000    # 1.0f

    const/4 v9, 0x1

    const/4 v10, 0x0

    const/4 v11, 0x1

    const/4 v12, 0x0

    invoke-direct/range {v4 .. v12}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    new-instance v16, Landroid/view/animation/AlphaAnimation;

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    move-object/from16 v0, v16

    invoke-direct {v0, v5, v6}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    new-instance v5, Landroid/view/animation/AnimationSet;

    const/4 v6, 0x1

    invoke-direct {v5, v6}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    sput-object v5, Lcom/konka/mm/photo/GalleryShowPicActivity;->sPrevOutAnim:Landroid/view/animation/AnimationSet;

    sget-object v5, Lcom/konka/mm/photo/GalleryShowPicActivity;->sPrevOutAnim:Landroid/view/animation/AnimationSet;

    invoke-virtual {v5, v4}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    sget-object v5, Lcom/konka/mm/photo/GalleryShowPicActivity;->sPrevOutAnim:Landroid/view/animation/AnimationSet;

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    sget-object v5, Lcom/konka/mm/photo/GalleryShowPicActivity;->sPrevOutAnim:Landroid/view/animation/AnimationSet;

    const-wide/16 v6, 0x320

    invoke-virtual {v5, v6, v7}, Landroid/view/animation/AnimationSet;->setDuration(J)V

    return-void
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 6
    .param p1    # Landroid/view/KeyEvent;

    const/4 v3, 0x0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_0

    invoke-virtual {p0}, Lcom/konka/mm/photo/GalleryShowPicActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    check-cast v2, Lcom/konka/mm/GlobalData;

    iget v4, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->currentNum:I

    invoke-virtual {v2, v4}, Lcom/konka/mm/GlobalData;->setmPicCurrentPos(I)V

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v4, "currentNum"

    iget v5, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->currentNum:I

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    const/4 v4, -0x1

    invoke-virtual {p0, v4, v1}, Lcom/konka/mm/photo/GalleryShowPicActivity;->setResult(ILandroid/content/Intent;)V

    iput-boolean v3, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->isEnterOtherAct:Z

    invoke-virtual {p0}, Lcom/konka/mm/photo/GalleryShowPicActivity;->finish()V

    :goto_0
    return v3

    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v3

    goto :goto_0
.end method

.method public findView()V
    .locals 6

    const/4 v5, 0x0

    const v3, 0x7f0b00f8    # com.konka.mm.R.id.pic_show_gallery

    invoke-virtual {p0, v3}, Lcom/konka/mm/photo/GalleryShowPicActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Gallery;

    iput-object v3, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->gallery:Landroid/widget/Gallery;

    const v3, 0x7f0b00f7    # com.konka.mm.R.id.switch_image

    invoke-virtual {p0, v3}, Lcom/konka/mm/photo/GalleryShowPicActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/konka/mm/photo/ImageViewTouch;

    iput-object v3, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->picImageview:Lcom/konka/mm/photo/ImageViewTouch;

    invoke-virtual {p0}, Lcom/konka/mm/photo/GalleryShowPicActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090052    # com.konka.mm.R.string.first_photo

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v3, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    iput-object v3, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->toast_first_image:Landroid/widget/Toast;

    invoke-virtual {p0}, Lcom/konka/mm/photo/GalleryShowPicActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090053    # com.konka.mm.R.string.last_photo

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v3, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    iput-object v3, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->toast_last_image:Landroid/widget/Toast;

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->picPaths:Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/konka/mm/photo/GalleryShowPicActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v3, "com.konka.mm.file.where.come.from"

    const-string v4, "com.konka.mm.file.come.from.other"

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->sourceComeFrom:Ljava/lang/String;

    const-string v3, "com.konka.mm.file.index.posstion"

    invoke-virtual {v0, v3, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    iput v3, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->currentNum:I

    iget v3, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->currentNum:I

    iput v3, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->previousNum:I

    const-string v3, "com.konka.mm.file.root.path"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->root_path:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/konka/mm/photo/GalleryShowPicActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    check-cast v2, Lcom/konka/mm/GlobalData;

    iget-object v3, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->sourceComeFrom:Ljava/lang/String;

    const-string v4, "com.konka.mm.file.come.from.samba"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "com.konka.mm.samba.current.ip"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->currentIp:Ljava/lang/String;

    invoke-virtual {v2}, Lcom/konka/mm/GlobalData;->getmMMFileList()Ljava/util/ArrayList;

    move-result-object v3

    iput-object v3, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->picPaths:Ljava/util/ArrayList;

    :goto_0
    iget-object v3, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->gallery:Landroid/widget/Gallery;

    const/16 v4, 0xa

    invoke-virtual {v3, v4}, Landroid/widget/Gallery;->setSpacing(I)V

    iget-object v3, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->gallery:Landroid/widget/Gallery;

    new-instance v4, Lcom/konka/mm/photo/GalleryShowPicActivity$ImageAdapter;

    invoke-direct {v4, p0, p0}, Lcom/konka/mm/photo/GalleryShowPicActivity$ImageAdapter;-><init>(Lcom/konka/mm/photo/GalleryShowPicActivity;Landroid/content/Context;)V

    invoke-virtual {v3, v4}, Landroid/widget/Gallery;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v3, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->gallery:Landroid/widget/Gallery;

    iget v4, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->currentNum:I

    invoke-virtual {v3, v4}, Landroid/widget/Gallery;->setSelection(I)V

    iget-object v3, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->gallery:Landroid/widget/Gallery;

    invoke-virtual {v3, p0}, Landroid/widget/Gallery;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v3, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->gallery:Landroid/widget/Gallery;

    new-instance v4, Lcom/konka/mm/photo/GalleryShowPicActivity$3;

    invoke-direct {v4, p0}, Lcom/konka/mm/photo/GalleryShowPicActivity$3;-><init>(Lcom/konka/mm/photo/GalleryShowPicActivity;)V

    invoke-virtual {v3, v4}, Landroid/widget/Gallery;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    iget-object v3, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->gallery:Landroid/widget/Gallery;

    new-instance v4, Lcom/konka/mm/photo/GalleryShowPicActivity$4;

    invoke-direct {v4, p0}, Lcom/konka/mm/photo/GalleryShowPicActivity$4;-><init>(Lcom/konka/mm/photo/GalleryShowPicActivity;)V

    invoke-virtual {v3, v4}, Landroid/widget/Gallery;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v3, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->gallery:Landroid/widget/Gallery;

    new-instance v4, Lcom/konka/mm/photo/GalleryShowPicActivity$5;

    invoke-direct {v4, p0}, Lcom/konka/mm/photo/GalleryShowPicActivity$5;-><init>(Lcom/konka/mm/photo/GalleryShowPicActivity;)V

    invoke-virtual {v3, v4}, Landroid/widget/Gallery;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    return-void

    :cond_0
    iget-object v3, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->sourceComeFrom:Ljava/lang/String;

    const-string v4, "com.konka.mm.file.come.from.one.disk"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->sourceComeFrom:Ljava/lang/String;

    const-string v4, "com.konka.mm.file.come.from.filemanager"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    :cond_1
    invoke-virtual {v2}, Lcom/konka/mm/GlobalData;->getmMMFileList()Ljava/util/ArrayList;

    move-result-object v3

    iput-object v3, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->picPaths:Ljava/util/ArrayList;

    goto :goto_0

    :cond_2
    const-string v3, "com.konka.mm.file.all.lists"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    iput-object v3, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->picPaths:Ljava/util/ArrayList;

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/view/View;

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    const/16 v2, 0x400

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/konka/mm/photo/GalleryShowPicActivity;->requestWindowFeature(I)Z

    invoke-virtual {p0}, Lcom/konka/mm/photo/GalleryShowPicActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v2, v2}, Landroid/view/Window;->setFlags(II)V

    const v1, 0x7f030034    # com.konka.mm.R.layout.showpiclayout2

    invoke-virtual {p0, v1}, Lcom/konka/mm/photo/GalleryShowPicActivity;->setContentView(I)V

    invoke-virtual {p0}, Lcom/konka/mm/photo/GalleryShowPicActivity;->InitAnimationSet()V

    invoke-virtual {p0}, Lcom/konka/mm/photo/GalleryShowPicActivity;->findView()V

    invoke-virtual {p0}, Lcom/konka/mm/photo/GalleryShowPicActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090010    # com.konka.mm.R.string.file_not_exist

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {p0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->toast_file_not_exist:Landroid/widget/Toast;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    new-instance v1, Lcom/konka/mm/photo/GalleryShowPicActivity$2;

    invoke-direct {v1, p0}, Lcom/konka/mm/photo/GalleryShowPicActivity$2;-><init>(Lcom/konka/mm/photo/GalleryShowPicActivity;)V

    iput-object v1, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    iget-object v1, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/konka/mm/photo/GalleryShowPicActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    new-instance v1, Lcom/konka/mm/photo/GalleryShowPicActivity$EthStateReceiver;

    invoke-direct {v1, p0}, Lcom/konka/mm/photo/GalleryShowPicActivity$EthStateReceiver;-><init>(Lcom/konka/mm/photo/GalleryShowPicActivity;)V

    iput-object v1, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->mEthStateReceiver:Lcom/konka/mm/photo/GalleryShowPicActivity$EthStateReceiver;

    invoke-virtual {p0}, Lcom/konka/mm/photo/GalleryShowPicActivity;->registEthernetReceiver()V

    iget-object v1, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->picImageview:Lcom/konka/mm/photo/ImageViewTouch;

    invoke-virtual {v1, p0}, Lcom/konka/mm/photo/ImageViewTouch;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 4
    .param p1    # I

    const/4 v3, 0x1

    packed-switch p1, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    sput-boolean v3, Lcom/konka/mm/photo/GalleryShowPicActivity;->isProgressDlg:Z

    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/konka/mm/photo/GalleryShowPicActivity;->progressDialog:Landroid/app/ProgressDialog;

    sget-object v0, Lcom/konka/mm/photo/GalleryShowPicActivity;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/konka/mm/photo/GalleryShowPicActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090051    # com.konka.mm.R.string.loader

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    sget-object v0, Lcom/konka/mm/photo/GalleryShowPicActivity;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    sget-object v0, Lcom/konka/mm/photo/GalleryShowPicActivity;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    sget-object v0, Lcom/konka/mm/photo/GalleryShowPicActivity;->progressDialog:Landroid/app/ProgressDialog;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method protected onDestroy()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->picImageview:Lcom/konka/mm/photo/ImageViewTouch;

    iput-object v0, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->picPaths:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/konka/mm/photo/GalleryShowPicActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    invoke-virtual {p0}, Lcom/konka/mm/photo/GalleryShowPicActivity;->unregistEthernetReceiver()V

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v0, 0x1

    const/16 v1, 0x42

    if-ne p1, v1, :cond_3

    iget v1, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->currentNum:I

    iget v2, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->previousNum:I

    if-eq v1, v2, :cond_2

    iget-object v1, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->sourceComeFrom:Ljava/lang/String;

    const-string v2, "com.konka.mm.file.come.from.samba"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->currentIp:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->currentIp:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/konka/mm/samba/SmbClient;->pingHost(Ljava/lang/String;I)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/konka/mm/photo/GalleryShowPicActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090032    # com.konka.mm.R.string.MM_CONPUTER_UNCONNECT

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {p0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->picPaths:Ljava/util/ArrayList;

    monitor-enter v1

    :try_start_0
    invoke-virtual {p0}, Lcom/konka/mm/photo/GalleryShowPicActivity;->setPhotoView()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_2
    iget v1, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->currentNum:I

    iput v1, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->previousNum:I

    :cond_3
    const/16 v1, 0xce

    if-eq p1, v1, :cond_4

    const/16 v1, 0x205

    if-ne p1, v1, :cond_5

    :cond_4
    iput-boolean v0, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->is3dKeyDown:Z

    :cond_5
    iget-object v0, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->gallery:Landroid/widget/Gallery;

    invoke-virtual {v0, p1, p2}, Landroid/widget/Gallery;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 4
    .param p1    # Landroid/content/Intent;

    invoke-super {p0, p1}, Landroid/app/Activity;->onNewIntent(Landroid/content/Intent;)V

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "com.konka.mm.file.where.come.from"

    const-string v3, "com.konka.mm.file.come.from.other"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->sourceComeFrom:Ljava/lang/String;

    const-string v2, "com.konka.mm.file.index.posstion"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->currentNum:I

    iget v2, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->currentNum:I

    iput v2, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->previousNum:I

    const-string v2, "com.konka.mm.file.root.path"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->root_path:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/konka/mm/photo/GalleryShowPicActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lcom/konka/mm/GlobalData;

    iget-object v2, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->sourceComeFrom:Ljava/lang/String;

    const-string v3, "com.konka.mm.file.come.from.samba"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "com.konka.mm.samba.current.ip"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->currentIp:Ljava/lang/String;

    invoke-virtual {v1}, Lcom/konka/mm/GlobalData;->getmMMFileList()Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->picPaths:Ljava/util/ArrayList;

    :goto_0
    iget-object v2, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->gallery:Landroid/widget/Gallery;

    new-instance v3, Lcom/konka/mm/photo/GalleryShowPicActivity$ImageAdapter;

    invoke-direct {v3, p0, p0}, Lcom/konka/mm/photo/GalleryShowPicActivity$ImageAdapter;-><init>(Lcom/konka/mm/photo/GalleryShowPicActivity;Landroid/content/Context;)V

    invoke-virtual {v2, v3}, Landroid/widget/Gallery;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v2, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->gallery:Landroid/widget/Gallery;

    iget v3, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->currentNum:I

    invoke-virtual {v2, v3}, Landroid/widget/Gallery;->setSelection(I)V

    invoke-virtual {p0}, Lcom/konka/mm/photo/GalleryShowPicActivity;->setPhotoView()V

    return-void

    :cond_0
    iget-object v2, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->sourceComeFrom:Ljava/lang/String;

    const-string v3, "com.konka.mm.file.come.from.one.disk"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->sourceComeFrom:Ljava/lang/String;

    const-string v3, "com.konka.mm.file.come.from.filemanager"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_1
    invoke-virtual {v1}, Lcom/konka/mm/GlobalData;->getmMMFileList()Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->picPaths:Ljava/util/ArrayList;

    goto :goto_0

    :cond_2
    const-string v2, "com.konka.mm.file.all.lists"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->picPaths:Ljava/util/ArrayList;

    goto :goto_0
.end method

.method protected onPause()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    iget-boolean v0, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->isEnterOtherAct:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->is3dKeyDown:Z

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/konka/mm/tools/FileTool;->killRunningServiceInfo(Landroid/content/Context;)V

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->is3dKeyDown:Z

    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/view/MotionEvent;

    iget-object v0, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->gallery:Landroid/widget/Gallery;

    invoke-virtual {v0, p2}, Landroid/widget/Gallery;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public registEthernetReceiver()V
    .locals 3

    new-instance v2, Lcom/konka/mm/photo/GalleryShowPicActivity$EthStateReceiver;

    invoke-direct {v2, p0}, Lcom/konka/mm/photo/GalleryShowPicActivity$EthStateReceiver;-><init>(Lcom/konka/mm/photo/GalleryShowPicActivity;)V

    iput-object v2, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->mEthStateReceiver:Lcom/konka/mm/photo/GalleryShowPicActivity$EthStateReceiver;

    invoke-static {}, Landroid/net/ethernet/EthernetManager;->getInstance()Landroid/net/ethernet/EthernetManager;

    move-result-object v1

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v2, "android.net.ethernet.ETHERNET_STATE_CHANGED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->mEthStateReceiver:Lcom/konka/mm/photo/GalleryShowPicActivity$EthStateReceiver;

    invoke-virtual {p0, v2, v0}, Lcom/konka/mm/photo/GalleryShowPicActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method public setDefaultPhoto()Landroid/graphics/Bitmap;
    .locals 4

    const/4 v3, 0x1

    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    sget-object v2, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    iput-object v2, v1, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    iput-boolean v3, v1, Landroid/graphics/BitmapFactory$Options;->inPurgeable:Z

    iput-boolean v3, v1, Landroid/graphics/BitmapFactory$Options;->inInputShareable:Z

    invoke-virtual {p0}, Lcom/konka/mm/photo/GalleryShowPicActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020036    # com.konka.mm.R.drawable.default_bg

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v0

    const/4 v2, 0x0

    invoke-static {v0, v2, v1}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-static {v2}, Lcom/konka/mm/photo/GalleryShowPicActivity;->ensureGLCompatibleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v2

    return-object v2
.end method

.method public setPhotoView()V
    .locals 12

    const/4 v11, 0x0

    const-wide/16 v5, 0x0

    :try_start_0
    new-instance v9, Ljava/io/File;

    iget-object v8, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->picPaths:Ljava/util/ArrayList;

    iget v10, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->currentNum:I

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-direct {v9, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v9}, Lcom/konka/mm/tools/FileTool;->getFileSizes(Ljava/io/File;)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v5

    :goto_0
    const-wide/32 v0, 0xd00000

    iget-object v8, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->sourceComeFrom:Ljava/lang/String;

    const-string v9, "com.konka.mm.file.come.from.samba"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    const-wide/16 v0, 0x400

    :goto_1
    cmp-long v8, v5, v0

    if-lez v8, :cond_3

    new-instance v3, Lcom/konka/mm/photo/CallbackImg;

    iget-object v8, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->picImageview:Lcom/konka/mm/photo/ImageViewTouch;

    invoke-direct {v3, v8, p0}, Lcom/konka/mm/photo/CallbackImg;-><init>(Lcom/konka/mm/photo/ImageViewTouch;Lcom/konka/mm/photo/GalleryShowPicActivity;)V

    iget-object v8, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->picPaths:Ljava/util/ArrayList;

    iget v9, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->currentNum:I

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    const/4 v9, 0x2

    invoke-static {v8, v3, v9}, Lcom/konka/mm/photo/AsyncImageLoader;->loadBitmap(Ljava/lang/String;Lcom/konka/mm/photo/AsyncImageLoader$ImageCallback;I)Landroid/graphics/Bitmap;

    move-result-object v2

    if-nez v2, :cond_0

    const-wide/32 v8, 0x800000

    cmp-long v8, v5, v8

    if-lez v8, :cond_0

    const/4 v8, 0x1

    invoke-virtual {p0, v8}, Lcom/konka/mm/photo/GalleryShowPicActivity;->showDialog(I)V

    :cond_0
    if-eqz v2, :cond_1

    iget-object v8, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->bitmapArray:Lcom/konka/mm/photo/RotateBitmap;

    invoke-virtual {v8, v2}, Lcom/konka/mm/photo/RotateBitmap;->setBitmap(Landroid/graphics/Bitmap;)V

    iget-object v8, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->picImageview:Lcom/konka/mm/photo/ImageViewTouch;

    iget-object v8, v8, Lcom/konka/mm/photo/ImageViewTouch;->mSuppMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v8}, Landroid/graphics/Matrix;->reset()V

    iget-object v8, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->picImageview:Lcom/konka/mm/photo/ImageViewTouch;

    iget-object v9, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->bitmapArray:Lcom/konka/mm/photo/RotateBitmap;

    invoke-virtual {v8, v9, v11, p0}, Lcom/konka/mm/photo/ImageViewTouch;->setImageRotateBitmapResetBase1(Lcom/konka/mm/photo/RotateBitmap;ZLcom/konka/mm/photo/GalleryShowPicActivity;)V

    iget-object v8, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->picImageview:Lcom/konka/mm/photo/ImageViewTouch;

    iget-object v9, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->mAlphaIn:Landroid/view/animation/Animation;

    invoke-virtual {v8, v9}, Lcom/konka/mm/photo/ImageViewTouch;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_1
    :goto_2
    return-void

    :catch_0
    move-exception v4

    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    :cond_2
    const-wide/32 v0, 0xd00000

    goto :goto_1

    :cond_3
    iget-object v8, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->picPaths:Ljava/util/ArrayList;

    iget v9, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->currentNum:I

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    const v9, 0x1fa400

    invoke-static {v8, v9}, Lcom/konka/mm/tools/PicTool;->decodeFileDescriptor(Ljava/lang/String;I)Landroid/graphics/Bitmap;

    move-result-object v7

    iget-object v8, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->bitmapArray:Lcom/konka/mm/photo/RotateBitmap;

    invoke-virtual {v8, v7}, Lcom/konka/mm/photo/RotateBitmap;->setBitmap(Landroid/graphics/Bitmap;)V

    iget-object v8, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->picImageview:Lcom/konka/mm/photo/ImageViewTouch;

    iget-object v8, v8, Lcom/konka/mm/photo/ImageViewTouch;->mSuppMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v8}, Landroid/graphics/Matrix;->reset()V

    iget-object v8, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->picImageview:Lcom/konka/mm/photo/ImageViewTouch;

    iget-object v9, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->bitmapArray:Lcom/konka/mm/photo/RotateBitmap;

    invoke-virtual {v8, v9, v11, p0}, Lcom/konka/mm/photo/ImageViewTouch;->setImageRotateBitmapResetBase1(Lcom/konka/mm/photo/RotateBitmap;ZLcom/konka/mm/photo/GalleryShowPicActivity;)V

    iget-object v8, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->picImageview:Lcom/konka/mm/photo/ImageViewTouch;

    iget-object v9, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->mAlphaIn:Landroid/view/animation/Animation;

    invoke-virtual {v8, v9}, Lcom/konka/mm/photo/ImageViewTouch;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_2
.end method

.method public unregistEthernetReceiver()V
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->mEthStateReceiver:Lcom/konka/mm/photo/GalleryShowPicActivity$EthStateReceiver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/mm/photo/GalleryShowPicActivity;->mEthStateReceiver:Lcom/konka/mm/photo/GalleryShowPicActivity$EthStateReceiver;

    invoke-virtual {p0, v0}, Lcom/konka/mm/photo/GalleryShowPicActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    :cond_0
    return-void
.end method
