.class Lcom/konka/mm/photo/GalleryShowPicActivity$ImageAdapter;
.super Landroid/widget/BaseAdapter;
.source "GalleryShowPicActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/mm/photo/GalleryShowPicActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ImageAdapter"
.end annotation


# instance fields
.field private context:Landroid/content/Context;

.field final synthetic this$0:Lcom/konka/mm/photo/GalleryShowPicActivity;


# direct methods
.method public constructor <init>(Lcom/konka/mm/photo/GalleryShowPicActivity;Landroid/content/Context;)V
    .locals 0
    .param p2    # Landroid/content/Context;

    iput-object p1, p0, Lcom/konka/mm/photo/GalleryShowPicActivity$ImageAdapter;->this$0:Lcom/konka/mm/photo/GalleryShowPicActivity;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    iput-object p2, p0, Lcom/konka/mm/photo/GalleryShowPicActivity$ImageAdapter;->context:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/photo/GalleryShowPicActivity$ImageAdapter;->this$0:Lcom/konka/mm/photo/GalleryShowPicActivity;

    # getter for: Lcom/konka/mm/photo/GalleryShowPicActivity;->picPaths:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/konka/mm/photo/GalleryShowPicActivity;->access$0(Lcom/konka/mm/photo/GalleryShowPicActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1    # I

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1    # I

    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    if-nez p2, :cond_2

    new-instance v0, Lcom/konka/mm/photo/GalleryShowPicActivity$PicItem;

    iget-object v3, p0, Lcom/konka/mm/photo/GalleryShowPicActivity$ImageAdapter;->this$0:Lcom/konka/mm/photo/GalleryShowPicActivity;

    invoke-direct {v0, v3}, Lcom/konka/mm/photo/GalleryShowPicActivity$PicItem;-><init>(Lcom/konka/mm/photo/GalleryShowPicActivity;)V

    iget-object v3, p0, Lcom/konka/mm/photo/GalleryShowPicActivity$ImageAdapter;->context:Landroid/content/Context;

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    const v4, 0x7f030024    # com.konka.mm.R.layout.photoadapter

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    const v3, 0x7f0b0099    # com.konka.mm.R.id.photoitem

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, v0, Lcom/konka/mm/photo/GalleryShowPicActivity$PicItem;->mPicIcon:Landroid/widget/ImageView;

    const v3, 0x7f0b009a    # com.konka.mm.R.id.photo_title

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v0, Lcom/konka/mm/photo/GalleryShowPicActivity$PicItem;->mPicName:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/konka/mm/photo/GalleryShowPicActivity$ImageAdapter;->this$0:Lcom/konka/mm/photo/GalleryShowPicActivity;

    # getter for: Lcom/konka/mm/photo/GalleryShowPicActivity;->sourceComeFrom:Ljava/lang/String;
    invoke-static {v3}, Lcom/konka/mm/photo/GalleryShowPicActivity;->access$1(Lcom/konka/mm/photo/GalleryShowPicActivity;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "com.konka.mm.file.come.from.samba"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/konka/mm/photo/GalleryShowPicActivity$ImageAdapter;->this$0:Lcom/konka/mm/photo/GalleryShowPicActivity;

    # getter for: Lcom/konka/mm/photo/GalleryShowPicActivity;->currentIp:Ljava/lang/String;
    invoke-static {v3}, Lcom/konka/mm/photo/GalleryShowPicActivity;->access$2(Lcom/konka/mm/photo/GalleryShowPicActivity;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/konka/mm/photo/GalleryShowPicActivity$ImageAdapter;->this$0:Lcom/konka/mm/photo/GalleryShowPicActivity;

    # getter for: Lcom/konka/mm/photo/GalleryShowPicActivity;->currentIp:Ljava/lang/String;
    invoke-static {v3}, Lcom/konka/mm/photo/GalleryShowPicActivity;->access$2(Lcom/konka/mm/photo/GalleryShowPicActivity;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    invoke-static {v3, v4}, Lcom/konka/mm/samba/SmbClient;->pingHost(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/konka/mm/photo/GalleryShowPicActivity$ImageAdapter;->this$0:Lcom/konka/mm/photo/GalleryShowPicActivity;

    iget-object v4, v0, Lcom/konka/mm/photo/GalleryShowPicActivity$PicItem;->mPicIcon:Landroid/widget/ImageView;

    # invokes: Lcom/konka/mm/photo/GalleryShowPicActivity;->setThumbImage(Landroid/widget/ImageView;I)V
    invoke-static {v3, v4, p1}, Lcom/konka/mm/photo/GalleryShowPicActivity;->access$3(Lcom/konka/mm/photo/GalleryShowPicActivity;Landroid/widget/ImageView;I)V

    :cond_0
    :goto_0
    iget-object v3, p0, Lcom/konka/mm/photo/GalleryShowPicActivity$ImageAdapter;->this$0:Lcom/konka/mm/photo/GalleryShowPicActivity;

    # getter for: Lcom/konka/mm/photo/GalleryShowPicActivity;->picPaths:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/konka/mm/photo/GalleryShowPicActivity;->access$0(Lcom/konka/mm/photo/GalleryShowPicActivity;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    iget-object v3, v0, Lcom/konka/mm/photo/GalleryShowPicActivity$PicItem;->mPicName:Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :goto_1
    return-object p2

    :cond_1
    iget-object v3, p0, Lcom/konka/mm/photo/GalleryShowPicActivity$ImageAdapter;->this$0:Lcom/konka/mm/photo/GalleryShowPicActivity;

    iget-object v4, v0, Lcom/konka/mm/photo/GalleryShowPicActivity$PicItem;->mPicIcon:Landroid/widget/ImageView;

    # invokes: Lcom/konka/mm/photo/GalleryShowPicActivity;->setThumbImage(Landroid/widget/ImageView;I)V
    invoke-static {v3, v4, p1}, Lcom/konka/mm/photo/GalleryShowPicActivity;->access$3(Lcom/konka/mm/photo/GalleryShowPicActivity;Landroid/widget/ImageView;I)V

    goto :goto_0

    :cond_2
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/konka/mm/photo/GalleryShowPicActivity$PicItem;

    goto :goto_1
.end method
