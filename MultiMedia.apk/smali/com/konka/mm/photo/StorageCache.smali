.class public Lcom/konka/mm/photo/StorageCache;
.super Ljava/lang/Object;
.source "StorageCache.java"


# static fields
.field private static final EXTERNAL_CACHE_MAX_SIZE:I = 0xa00000

.field private static final INTERNAL_CACHE_MAX_SIZE:I = 0x1400000

.field private static final TAG:Ljava/lang/String; = "StorageCache"

.field private static instance:Lcom/konka/mm/photo/StorageCache;


# instance fields
.field private mCacheDir:Ljava/io/File;

.field private final sExternalCache:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final sInternalCache:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/konka/mm/photo/StorageCache;

    invoke-direct {v0}, Lcom/konka/mm/photo/StorageCache;-><init>()V

    sput-object v0, Lcom/konka/mm/photo/StorageCache;->instance:Lcom/konka/mm/photo/StorageCache;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/konka/mm/photo/StorageCache$1;

    const/high16 v1, 0x1400000

    invoke-direct {v0, p0, v1}, Lcom/konka/mm/photo/StorageCache$1;-><init>(Lcom/konka/mm/photo/StorageCache;I)V

    iput-object v0, p0, Lcom/konka/mm/photo/StorageCache;->sInternalCache:Landroid/util/LruCache;

    new-instance v0, Lcom/konka/mm/photo/StorageCache$2;

    const/high16 v1, 0xa00000

    invoke-direct {v0, p0, v1}, Lcom/konka/mm/photo/StorageCache$2;-><init>(Lcom/konka/mm/photo/StorageCache;I)V

    iput-object v0, p0, Lcom/konka/mm/photo/StorageCache;->sExternalCache:Landroid/util/LruCache;

    return-void
.end method

.method static synthetic access$0(Lcom/konka/mm/photo/StorageCache;Ljava/lang/String;)Ljava/io/File;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/konka/mm/photo/StorageCache;->getFile(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1(Lcom/konka/mm/photo/StorageCache;)Landroid/util/LruCache;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/photo/StorageCache;->sExternalCache:Landroid/util/LruCache;

    return-object v0
.end method

.method private static clearFolder(Ljava/io/File;)I
    .locals 7
    .param p0    # Ljava/io/File;

    const/4 v1, 0x0

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/io/File;->isDirectory()Z

    move-result v3

    if-eqz v3, :cond_0

    :try_start_0
    invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v4

    array-length v5, v4

    const/4 v3, 0x0

    :goto_0
    if-lt v3, v5, :cond_1

    :cond_0
    :goto_1
    return v1

    :cond_1
    aget-object v0, v4, v3

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-static {v0}, Lcom/konka/mm/photo/StorageCache;->clearFolder(Ljava/io/File;)I

    move-result v6

    add-int/2addr v1, v6

    :cond_2
    invoke-virtual {v0}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v6

    if-eqz v6, :cond_3

    add-int/lit8 v1, v1, 0x1

    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method private getFile(Ljava/lang/String;)Ljava/io/File;
    .locals 2
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/konka/mm/photo/StorageCache;->mCacheDir:Ljava/io/File;

    invoke-direct {v0, v1, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->isFile()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    new-instance v1, Ljava/io/FileNotFoundException;

    invoke-direct {v1}, Ljava/io/FileNotFoundException;-><init>()V

    throw v1

    :cond_1
    return-object v0
.end method

.method public static getInstance()Lcom/konka/mm/photo/StorageCache;
    .locals 1

    sget-object v0, Lcom/konka/mm/photo/StorageCache;->instance:Lcom/konka/mm/photo/StorageCache;

    return-object v0
.end method

.method private getOutputStream(Ljava/lang/String;)Ljava/io/FileOutputStream;
    .locals 3
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    iget-object v1, p0, Lcom/konka/mm/photo/StorageCache;->mCacheDir:Ljava/io/File;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/io/FileOutputStream;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/konka/mm/photo/StorageCache;->mCacheDir:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public clearCacheFolder()I
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/photo/StorageCache;->mCacheDir:Ljava/io/File;

    invoke-static {v0}, Lcom/konka/mm/photo/StorageCache;->clearFolder(Ljava/io/File;)I

    move-result v0

    return v0
.end method

.method public get(Ljava/lang/String;)Ljava/io/File;
    .locals 2
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    iget-object v1, p0, Lcom/konka/mm/photo/StorageCache;->sInternalCache:Landroid/util/LruCache;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/konka/mm/photo/StorageCache;->sInternalCache:Landroid/util/LruCache;

    invoke-virtual {v0, p1}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/konka/mm/photo/StorageCache;->getFile(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    monitor-exit v1

    :goto_0
    return-object v0

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v1, p0, Lcom/konka/mm/photo/StorageCache;->sExternalCache:Landroid/util/LruCache;

    monitor-enter v1

    :try_start_1
    iget-object v0, p0, Lcom/konka/mm/photo/StorageCache;->sExternalCache:Landroid/util/LruCache;

    invoke-virtual {v0, p1}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public putBitmap(Ljava/lang/String;Landroid/graphics/Bitmap;)Z
    .locals 9
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/graphics/Bitmap;

    const/4 v4, 0x1

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/konka/mm/photo/StorageCache;->get(Ljava/lang/String;)Ljava/io/File;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    if-eqz v1, :cond_0

    :goto_0
    return v4

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    :cond_0
    :try_start_1
    invoke-direct {p0, p1}, Lcom/konka/mm/photo/StorageCache;->getOutputStream(Ljava/lang/String;)Ljava/io/FileOutputStream;

    move-result-object v2

    sget-object v5, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v6, 0x64

    invoke-virtual {p2, v5, v6, v2}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    move-result v3

    invoke-virtual {v2}, Ljava/io/FileOutputStream;->flush()V

    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V

    if-eqz v3, :cond_1

    iget-object v5, p0, Lcom/konka/mm/photo/StorageCache;->sInternalCache:Landroid/util/LruCache;

    monitor-enter v5
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    :try_start_2
    iget-object v6, p0, Lcom/konka/mm/photo/StorageCache;->sInternalCache:Landroid/util/LruCache;

    invoke-direct {p0, p1}, Lcom/konka/mm/photo/StorageCache;->getFile(Ljava/lang/String;)Ljava/io/File;

    move-result-object v7

    invoke-virtual {v7}, Ljava/io/File;->length()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v6, p1, v7}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v5

    goto :goto_0

    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v4
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    :goto_1
    const/4 v4, 0x0

    goto :goto_0

    :cond_1
    :try_start_4
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v5, "save failed"

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_1

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1
.end method

.method public setCacheDir(Ljava/io/File;)V
    .locals 0
    .param p1    # Ljava/io/File;

    iput-object p1, p0, Lcom/konka/mm/photo/StorageCache;->mCacheDir:Ljava/io/File;

    invoke-virtual {p0}, Lcom/konka/mm/photo/StorageCache;->clearCacheFolder()I

    return-void
.end method
