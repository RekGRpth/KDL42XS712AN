.class Lcom/konka/mm/photo/PicMainActivity$3;
.super Ljava/lang/Object;
.source "PicMainActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/mm/photo/PicMainActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/mm/photo/PicMainActivity;


# direct methods
.method constructor <init>(Lcom/konka/mm/photo/PicMainActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/photo/PicMainActivity$3;->this$0:Lcom/konka/mm/photo/PicMainActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    :goto_0
    return-void

    :sswitch_0
    iget-object v0, p0, Lcom/konka/mm/photo/PicMainActivity$3;->this$0:Lcom/konka/mm/photo/PicMainActivity;

    invoke-virtual {v0, v1}, Lcom/konka/mm/photo/PicMainActivity;->setBtnFocuseFlag(Z)V

    iget-object v0, p0, Lcom/konka/mm/photo/PicMainActivity$3;->this$0:Lcom/konka/mm/photo/PicMainActivity;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/konka/mm/photo/PicMainActivity;->snapScreen(I)V

    goto :goto_0

    :sswitch_1
    iget-object v0, p0, Lcom/konka/mm/photo/PicMainActivity$3;->this$0:Lcom/konka/mm/photo/PicMainActivity;

    invoke-virtual {v0, v1}, Lcom/konka/mm/photo/PicMainActivity;->setBtnFocuseFlag(Z)V

    iget-object v0, p0, Lcom/konka/mm/photo/PicMainActivity$3;->this$0:Lcom/konka/mm/photo/PicMainActivity;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/konka/mm/photo/PicMainActivity;->snapScreen(I)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/konka/mm/photo/PicMainActivity$3;->this$0:Lcom/konka/mm/photo/PicMainActivity;

    invoke-virtual {v0}, Lcom/konka/mm/photo/PicMainActivity;->sortBtnOnKeyOrOnClickEvent()V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/konka/mm/photo/PicMainActivity$3;->this$0:Lcom/konka/mm/photo/PicMainActivity;

    invoke-virtual {v0}, Lcom/konka/mm/photo/PicMainActivity;->updateDatabase()V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0b002f -> :sswitch_0    # com.konka.mm.R.id.btn_list_left
        0x7f0b0034 -> :sswitch_1    # com.konka.mm.R.id.btn_list_right
        0x7f0b005b -> :sswitch_3    # com.konka.mm.R.id.btn_update
        0x7f0b009e -> :sswitch_2    # com.konka.mm.R.id.sort_btn
    .end sparse-switch
.end method
