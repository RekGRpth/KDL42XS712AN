.class Lcom/konka/mm/photo/PicAdapter$2;
.super Ljava/lang/Object;
.source "PicAdapter.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/mm/photo/PicAdapter;->setThumbImage(Landroid/widget/ImageView;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/mm/photo/PicAdapter;

.field private final synthetic val$icon:Landroid/widget/ImageView;

.field private final synthetic val$position:I


# direct methods
.method constructor <init>(Lcom/konka/mm/photo/PicAdapter;ILandroid/widget/ImageView;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/photo/PicAdapter$2;->this$0:Lcom/konka/mm/photo/PicAdapter;

    iput p2, p0, Lcom/konka/mm/photo/PicAdapter$2;->val$position:I

    iput-object p3, p0, Lcom/konka/mm/photo/PicAdapter$2;->val$icon:Landroid/widget/ImageView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    iget-object v3, p0, Lcom/konka/mm/photo/PicAdapter$2;->this$0:Lcom/konka/mm/photo/PicAdapter;

    # getter for: Lcom/konka/mm/photo/PicAdapter;->activity:Lcom/konka/mm/photo/PicMainActivity;
    invoke-static {v3}, Lcom/konka/mm/photo/PicAdapter;->access$0(Lcom/konka/mm/photo/PicAdapter;)Lcom/konka/mm/photo/PicMainActivity;

    move-result-object v3

    iget-object v3, v3, Lcom/konka/mm/photo/PicMainActivity;->picPaths:Ljava/util/ArrayList;

    iget v4, p0, Lcom/konka/mm/photo/PicAdapter$2;->val$position:I

    iget-object v5, p0, Lcom/konka/mm/photo/PicAdapter$2;->this$0:Lcom/konka/mm/photo/PicAdapter;

    # getter for: Lcom/konka/mm/photo/PicAdapter;->pageIndex:I
    invoke-static {v5}, Lcom/konka/mm/photo/PicAdapter;->access$1(Lcom/konka/mm/photo/PicAdapter;)I

    move-result v5

    iget-object v6, p0, Lcom/konka/mm/photo/PicAdapter$2;->this$0:Lcom/konka/mm/photo/PicAdapter;

    # getter for: Lcom/konka/mm/photo/PicAdapter;->perPageSize:I
    invoke-static {v6}, Lcom/konka/mm/photo/PicAdapter;->access$2(Lcom/konka/mm/photo/PicAdapter;)I

    move-result v6

    mul-int/2addr v5, v6

    add-int/2addr v4, v5

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const/16 v4, 0x4000

    invoke-static {v3, v4}, Lcom/konka/mm/tools/PicTool;->decodeFileDescriptor(Ljava/lang/String;I)Landroid/graphics/Bitmap;

    move-result-object v0

    new-instance v2, Landroid/os/Message;

    invoke-direct {v2}, Landroid/os/Message;-><init>()V

    const/4 v3, 0x0

    iput v3, v2, Landroid/os/Message;->what:I

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/konka/mm/photo/PicAdapter$2;->val$icon:Landroid/widget/ImageView;

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iput-object v1, v2, Landroid/os/Message;->obj:Ljava/lang/Object;

    iget-object v3, p0, Lcom/konka/mm/photo/PicAdapter$2;->this$0:Lcom/konka/mm/photo/PicAdapter;

    # getter for: Lcom/konka/mm/photo/PicAdapter;->handler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/konka/mm/photo/PicAdapter;->access$3(Lcom/konka/mm/photo/PicAdapter;)Landroid/os/Handler;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method
