.class Lcom/konka/mm/photo/AutoShowPicActivity$4;
.super Ljava/lang/Object;
.source "AutoShowPicActivity.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/mm/photo/AutoShowPicActivity;->InitData()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/mm/photo/AutoShowPicActivity;


# direct methods
.method constructor <init>(Lcom/konka/mm/photo/AutoShowPicActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/photo/AutoShowPicActivity$4;->this$0:Lcom/konka/mm/photo/AutoShowPicActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 5
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # Landroid/view/KeyEvent;

    const/high16 v4, 0x41f00000    # 30.0f

    const/high16 v3, -0x3e100000    # -30.0f

    const/4 v2, 0x0

    sget-boolean v0, Lcom/konka/mm/photo/AutoShowPicActivity;->isCanMove:Z

    if-eqz v0, :cond_3

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/16 v1, 0x15

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity$4;->this$0:Lcom/konka/mm/photo/AutoShowPicActivity;

    # getter for: Lcom/konka/mm/photo/AutoShowPicActivity;->picImageview:Lcom/konka/mm/photo/ImageViewTouch;
    invoke-static {v0}, Lcom/konka/mm/photo/AutoShowPicActivity;->access$4(Lcom/konka/mm/photo/AutoShowPicActivity;)Lcom/konka/mm/photo/ImageViewTouch;

    move-result-object v0

    iget-object v0, v0, Lcom/konka/mm/photo/ImageViewTouch;->mDisplayMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v0, v3, v2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity$4;->this$0:Lcom/konka/mm/photo/AutoShowPicActivity;

    # getter for: Lcom/konka/mm/photo/AutoShowPicActivity;->picImageview:Lcom/konka/mm/photo/ImageViewTouch;
    invoke-static {v0}, Lcom/konka/mm/photo/AutoShowPicActivity;->access$4(Lcom/konka/mm/photo/AutoShowPicActivity;)Lcom/konka/mm/photo/ImageViewTouch;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/mm/photo/AutoShowPicActivity$4;->this$0:Lcom/konka/mm/photo/AutoShowPicActivity;

    # getter for: Lcom/konka/mm/photo/AutoShowPicActivity;->picImageview:Lcom/konka/mm/photo/ImageViewTouch;
    invoke-static {v1}, Lcom/konka/mm/photo/AutoShowPicActivity;->access$4(Lcom/konka/mm/photo/AutoShowPicActivity;)Lcom/konka/mm/photo/ImageViewTouch;

    move-result-object v1

    iget-object v1, v1, Lcom/konka/mm/photo/ImageViewTouch;->mDisplayMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Lcom/konka/mm/photo/ImageViewTouch;->setImageMatrix(Landroid/graphics/Matrix;)V

    :cond_0
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/16 v1, 0x16

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity$4;->this$0:Lcom/konka/mm/photo/AutoShowPicActivity;

    # getter for: Lcom/konka/mm/photo/AutoShowPicActivity;->picImageview:Lcom/konka/mm/photo/ImageViewTouch;
    invoke-static {v0}, Lcom/konka/mm/photo/AutoShowPicActivity;->access$4(Lcom/konka/mm/photo/AutoShowPicActivity;)Lcom/konka/mm/photo/ImageViewTouch;

    move-result-object v0

    iget-object v0, v0, Lcom/konka/mm/photo/ImageViewTouch;->mDisplayMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v0, v4, v2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity$4;->this$0:Lcom/konka/mm/photo/AutoShowPicActivity;

    # getter for: Lcom/konka/mm/photo/AutoShowPicActivity;->picImageview:Lcom/konka/mm/photo/ImageViewTouch;
    invoke-static {v0}, Lcom/konka/mm/photo/AutoShowPicActivity;->access$4(Lcom/konka/mm/photo/AutoShowPicActivity;)Lcom/konka/mm/photo/ImageViewTouch;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/mm/photo/AutoShowPicActivity$4;->this$0:Lcom/konka/mm/photo/AutoShowPicActivity;

    # getter for: Lcom/konka/mm/photo/AutoShowPicActivity;->picImageview:Lcom/konka/mm/photo/ImageViewTouch;
    invoke-static {v1}, Lcom/konka/mm/photo/AutoShowPicActivity;->access$4(Lcom/konka/mm/photo/AutoShowPicActivity;)Lcom/konka/mm/photo/ImageViewTouch;

    move-result-object v1

    iget-object v1, v1, Lcom/konka/mm/photo/ImageViewTouch;->mDisplayMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Lcom/konka/mm/photo/ImageViewTouch;->setImageMatrix(Landroid/graphics/Matrix;)V

    :cond_1
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/16 v1, 0x13

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity$4;->this$0:Lcom/konka/mm/photo/AutoShowPicActivity;

    # getter for: Lcom/konka/mm/photo/AutoShowPicActivity;->picImageview:Lcom/konka/mm/photo/ImageViewTouch;
    invoke-static {v0}, Lcom/konka/mm/photo/AutoShowPicActivity;->access$4(Lcom/konka/mm/photo/AutoShowPicActivity;)Lcom/konka/mm/photo/ImageViewTouch;

    move-result-object v0

    iget-object v0, v0, Lcom/konka/mm/photo/ImageViewTouch;->mDisplayMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity$4;->this$0:Lcom/konka/mm/photo/AutoShowPicActivity;

    # getter for: Lcom/konka/mm/photo/AutoShowPicActivity;->picImageview:Lcom/konka/mm/photo/ImageViewTouch;
    invoke-static {v0}, Lcom/konka/mm/photo/AutoShowPicActivity;->access$4(Lcom/konka/mm/photo/AutoShowPicActivity;)Lcom/konka/mm/photo/ImageViewTouch;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/mm/photo/AutoShowPicActivity$4;->this$0:Lcom/konka/mm/photo/AutoShowPicActivity;

    # getter for: Lcom/konka/mm/photo/AutoShowPicActivity;->picImageview:Lcom/konka/mm/photo/ImageViewTouch;
    invoke-static {v1}, Lcom/konka/mm/photo/AutoShowPicActivity;->access$4(Lcom/konka/mm/photo/AutoShowPicActivity;)Lcom/konka/mm/photo/ImageViewTouch;

    move-result-object v1

    iget-object v1, v1, Lcom/konka/mm/photo/ImageViewTouch;->mDisplayMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Lcom/konka/mm/photo/ImageViewTouch;->setImageMatrix(Landroid/graphics/Matrix;)V

    :cond_2
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/16 v1, 0x14

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity$4;->this$0:Lcom/konka/mm/photo/AutoShowPicActivity;

    # getter for: Lcom/konka/mm/photo/AutoShowPicActivity;->picImageview:Lcom/konka/mm/photo/ImageViewTouch;
    invoke-static {v0}, Lcom/konka/mm/photo/AutoShowPicActivity;->access$4(Lcom/konka/mm/photo/AutoShowPicActivity;)Lcom/konka/mm/photo/ImageViewTouch;

    move-result-object v0

    iget-object v0, v0, Lcom/konka/mm/photo/ImageViewTouch;->mDisplayMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v0, v2, v4}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    iget-object v0, p0, Lcom/konka/mm/photo/AutoShowPicActivity$4;->this$0:Lcom/konka/mm/photo/AutoShowPicActivity;

    # getter for: Lcom/konka/mm/photo/AutoShowPicActivity;->picImageview:Lcom/konka/mm/photo/ImageViewTouch;
    invoke-static {v0}, Lcom/konka/mm/photo/AutoShowPicActivity;->access$4(Lcom/konka/mm/photo/AutoShowPicActivity;)Lcom/konka/mm/photo/ImageViewTouch;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/mm/photo/AutoShowPicActivity$4;->this$0:Lcom/konka/mm/photo/AutoShowPicActivity;

    # getter for: Lcom/konka/mm/photo/AutoShowPicActivity;->picImageview:Lcom/konka/mm/photo/ImageViewTouch;
    invoke-static {v1}, Lcom/konka/mm/photo/AutoShowPicActivity;->access$4(Lcom/konka/mm/photo/AutoShowPicActivity;)Lcom/konka/mm/photo/ImageViewTouch;

    move-result-object v1

    iget-object v1, v1, Lcom/konka/mm/photo/ImageViewTouch;->mDisplayMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Lcom/konka/mm/photo/ImageViewTouch;->setImageMatrix(Landroid/graphics/Matrix;)V

    :cond_3
    const/4 v0, 0x0

    return v0
.end method
