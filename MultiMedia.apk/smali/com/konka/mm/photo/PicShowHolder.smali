.class public Lcom/konka/mm/photo/PicShowHolder;
.super Ljava/lang/Object;
.source "PicShowHolder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/mm/photo/PicShowHolder$ItemOnClickEvent;
    }
.end annotation


# instance fields
.field public bt_Pause:Landroid/widget/Button;

.field public bt_autoPlay:Landroid/widget/Button;

.field public bt_big:Landroid/widget/Button;

.field public bt_circulate:Landroid/widget/Button;

.field public bt_fullScreen:Landroid/widget/Button;

.field public bt_galleryShow:Landroid/widget/Button;

.field public bt_leftRotion:Landroid/widget/Button;

.field public bt_next:Landroid/widget/Button;

.field public bt_pre:Landroid/widget/Button;

.field public bt_rightRotion:Landroid/widget/Button;

.field public bt_small:Landroid/widget/Button;

.field public bt_stop:Landroid/widget/Button;

.field public bt_suquence:Landroid/widget/Button;

.field public bt_wallpaper:Landroid/widget/Button;

.field public layout_gap:Landroid/widget/LinearLayout;

.field private mPicShowActivity:Lcom/konka/mm/photo/AutoShowPicActivity;

.field private onFocusChange:Landroid/view/View$OnFocusChangeListener;

.field protected pic_bts_layout:Landroid/widget/RelativeLayout;

.field public text_autoplay:Landroid/widget/TextView;

.field public text_descale:Landroid/widget/TextView;

.field public text_enlarge:Landroid/widget/TextView;

.field public text_fullscreen:Landroid/widget/TextView;

.field public text_leferotion:Landroid/widget/TextView;

.field public text_list:Landroid/widget/TextView;

.field public text_next:Landroid/widget/TextView;

.field public text_play_order:Landroid/widget/TextView;

.field public text_pre:Landroid/widget/TextView;

.field public text_rightrotion:Landroid/widget/TextView;

.field public text_stop:Landroid/widget/TextView;

.field public text_wallpaper:Landroid/widget/TextView;

.field protected toast_first_image:Landroid/widget/Toast;

.field protected toast_last_image:Landroid/widget/Toast;

.field protected toast_not_connect:Landroid/widget/Toast;


# direct methods
.method public constructor <init>(Lcom/konka/mm/photo/AutoShowPicActivity;)V
    .locals 3
    .param p1    # Lcom/konka/mm/photo/AutoShowPicActivity;

    const/4 v0, 0x0

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->toast_first_image:Landroid/widget/Toast;

    iput-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->toast_last_image:Landroid/widget/Toast;

    iput-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->toast_not_connect:Landroid/widget/Toast;

    new-instance v0, Lcom/konka/mm/photo/PicShowHolder$1;

    invoke-direct {v0, p0}, Lcom/konka/mm/photo/PicShowHolder$1;-><init>(Lcom/konka/mm/photo/PicShowHolder;)V

    iput-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->onFocusChange:Landroid/view/View$OnFocusChangeListener;

    iput-object p1, p0, Lcom/konka/mm/photo/PicShowHolder;->mPicShowActivity:Lcom/konka/mm/photo/AutoShowPicActivity;

    invoke-virtual {p1}, Lcom/konka/mm/photo/AutoShowPicActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090052    # com.konka.mm.R.string.first_photo

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->toast_first_image:Landroid/widget/Toast;

    invoke-virtual {p1}, Lcom/konka/mm/photo/AutoShowPicActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090053    # com.konka.mm.R.string.last_photo

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->toast_last_image:Landroid/widget/Toast;

    invoke-virtual {p1}, Lcom/konka/mm/photo/AutoShowPicActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090032    # com.konka.mm.R.string.MM_CONPUTER_UNCONNECT

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->toast_not_connect:Landroid/widget/Toast;

    return-void
.end method

.method static synthetic access$0(Lcom/konka/mm/photo/PicShowHolder;Landroid/widget/TextView;Z)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/konka/mm/photo/PicShowHolder;->setTextViewVisible(Landroid/widget/TextView;Z)V

    return-void
.end method

.method static synthetic access$1(Lcom/konka/mm/photo/PicShowHolder;)Lcom/konka/mm/photo/AutoShowPicActivity;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->mPicShowActivity:Lcom/konka/mm/photo/AutoShowPicActivity;

    return-object v0
.end method

.method static synthetic access$2(Lcom/konka/mm/photo/PicShowHolder;Landroid/widget/TextView;Z)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/konka/mm/photo/PicShowHolder;->setTextPlayOrderVisble(Landroid/widget/TextView;Z)V

    return-void
.end method

.method private setTextPlayOrderVisble(Landroid/widget/TextView;Z)V
    .locals 2
    .param p1    # Landroid/widget/TextView;
    .param p2    # Z

    if-eqz p2, :cond_1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->mPicShowActivity:Lcom/konka/mm/photo/AutoShowPicActivity;

    iget-boolean v0, v0, Lcom/konka/mm/photo/AutoShowPicActivity;->isNeetCirculate:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->mPicShowActivity:Lcom/konka/mm/photo/AutoShowPicActivity;

    invoke-virtual {v0}, Lcom/konka/mm/photo/AutoShowPicActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09006a    # com.konka.mm.R.string.cycleTxt

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->mPicShowActivity:Lcom/konka/mm/photo/AutoShowPicActivity;

    invoke-virtual {v0}, Lcom/konka/mm/photo/AutoShowPicActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090068    # com.konka.mm.R.string.sequenceTxt

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private setTextViewVisible(Landroid/widget/TextView;Z)V
    .locals 1
    .param p1    # Landroid/widget/TextView;
    .param p2    # Z

    if-eqz p2, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public findBtnView()V
    .locals 2

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->mPicShowActivity:Lcom/konka/mm/photo/AutoShowPicActivity;

    const v1, 0x7f0b00dd    # com.konka.mm.R.id.layout_gap

    invoke-virtual {v0, v1}, Lcom/konka/mm/photo/AutoShowPicActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->layout_gap:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->mPicShowActivity:Lcom/konka/mm/photo/AutoShowPicActivity;

    const v1, 0x7f0b00de    # com.konka.mm.R.id.buttomitem0

    invoke-virtual {v0, v1}, Lcom/konka/mm/photo/AutoShowPicActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->bt_wallpaper:Landroid/widget/Button;

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->mPicShowActivity:Lcom/konka/mm/photo/AutoShowPicActivity;

    const v1, 0x7f0b00e0    # com.konka.mm.R.id.buttomitem1

    invoke-virtual {v0, v1}, Lcom/konka/mm/photo/AutoShowPicActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->bt_galleryShow:Landroid/widget/Button;

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->mPicShowActivity:Lcom/konka/mm/photo/AutoShowPicActivity;

    const v1, 0x7f0b00e2    # com.konka.mm.R.id.buttomitem2

    invoke-virtual {v0, v1}, Lcom/konka/mm/photo/AutoShowPicActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->bt_pre:Landroid/widget/Button;

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->mPicShowActivity:Lcom/konka/mm/photo/AutoShowPicActivity;

    const v1, 0x7f0b00e4    # com.konka.mm.R.id.buttomitem3

    invoke-virtual {v0, v1}, Lcom/konka/mm/photo/AutoShowPicActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->bt_autoPlay:Landroid/widget/Button;

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->mPicShowActivity:Lcom/konka/mm/photo/AutoShowPicActivity;

    const v1, 0x7f0b00e5    # com.konka.mm.R.id.buttomitem13

    invoke-virtual {v0, v1}, Lcom/konka/mm/photo/AutoShowPicActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->bt_Pause:Landroid/widget/Button;

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->mPicShowActivity:Lcom/konka/mm/photo/AutoShowPicActivity;

    const v1, 0x7f0b00e7    # com.konka.mm.R.id.buttomitem4

    invoke-virtual {v0, v1}, Lcom/konka/mm/photo/AutoShowPicActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->bt_next:Landroid/widget/Button;

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->mPicShowActivity:Lcom/konka/mm/photo/AutoShowPicActivity;

    const v1, 0x7f0b00e9    # com.konka.mm.R.id.buttomitem5

    invoke-virtual {v0, v1}, Lcom/konka/mm/photo/AutoShowPicActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->bt_stop:Landroid/widget/Button;

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->mPicShowActivity:Lcom/konka/mm/photo/AutoShowPicActivity;

    const v1, 0x7f0b00ed    # com.konka.mm.R.id.buttomitem6

    invoke-virtual {v0, v1}, Lcom/konka/mm/photo/AutoShowPicActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->bt_leftRotion:Landroid/widget/Button;

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->mPicShowActivity:Lcom/konka/mm/photo/AutoShowPicActivity;

    const v1, 0x7f0b00ef    # com.konka.mm.R.id.buttomitem7

    invoke-virtual {v0, v1}, Lcom/konka/mm/photo/AutoShowPicActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->bt_rightRotion:Landroid/widget/Button;

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->mPicShowActivity:Lcom/konka/mm/photo/AutoShowPicActivity;

    const v1, 0x7f0b00f1    # com.konka.mm.R.id.buttomitem8

    invoke-virtual {v0, v1}, Lcom/konka/mm/photo/AutoShowPicActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->bt_big:Landroid/widget/Button;

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->mPicShowActivity:Lcom/konka/mm/photo/AutoShowPicActivity;

    const v1, 0x7f0b00f3    # com.konka.mm.R.id.buttomitem9

    invoke-virtual {v0, v1}, Lcom/konka/mm/photo/AutoShowPicActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->bt_small:Landroid/widget/Button;

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->mPicShowActivity:Lcom/konka/mm/photo/AutoShowPicActivity;

    const v1, 0x7f0b00f5    # com.konka.mm.R.id.buttomitem10

    invoke-virtual {v0, v1}, Lcom/konka/mm/photo/AutoShowPicActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->bt_fullScreen:Landroid/widget/Button;

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->mPicShowActivity:Lcom/konka/mm/photo/AutoShowPicActivity;

    const v1, 0x7f0b00eb    # com.konka.mm.R.id.buttomitem11

    invoke-virtual {v0, v1}, Lcom/konka/mm/photo/AutoShowPicActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->bt_suquence:Landroid/widget/Button;

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->mPicShowActivity:Lcom/konka/mm/photo/AutoShowPicActivity;

    const v1, 0x7f0b00ec    # com.konka.mm.R.id.buttomitem12

    invoke-virtual {v0, v1}, Lcom/konka/mm/photo/AutoShowPicActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->bt_circulate:Landroid/widget/Button;

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->mPicShowActivity:Lcom/konka/mm/photo/AutoShowPicActivity;

    const v1, 0x7f0b00df    # com.konka.mm.R.id.textview_item0

    invoke-virtual {v0, v1}, Lcom/konka/mm/photo/AutoShowPicActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->text_wallpaper:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->mPicShowActivity:Lcom/konka/mm/photo/AutoShowPicActivity;

    const v1, 0x7f0b00e1    # com.konka.mm.R.id.textview_item1

    invoke-virtual {v0, v1}, Lcom/konka/mm/photo/AutoShowPicActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->text_list:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->mPicShowActivity:Lcom/konka/mm/photo/AutoShowPicActivity;

    const v1, 0x7f0b00e3    # com.konka.mm.R.id.textview_item2

    invoke-virtual {v0, v1}, Lcom/konka/mm/photo/AutoShowPicActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->text_pre:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->mPicShowActivity:Lcom/konka/mm/photo/AutoShowPicActivity;

    const v1, 0x7f0b00e6    # com.konka.mm.R.id.textview_item3

    invoke-virtual {v0, v1}, Lcom/konka/mm/photo/AutoShowPicActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->text_autoplay:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->mPicShowActivity:Lcom/konka/mm/photo/AutoShowPicActivity;

    const v1, 0x7f0b00e8    # com.konka.mm.R.id.textview_item4

    invoke-virtual {v0, v1}, Lcom/konka/mm/photo/AutoShowPicActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->text_next:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->mPicShowActivity:Lcom/konka/mm/photo/AutoShowPicActivity;

    const v1, 0x7f0b00ea    # com.konka.mm.R.id.textview_item5

    invoke-virtual {v0, v1}, Lcom/konka/mm/photo/AutoShowPicActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->text_stop:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->mPicShowActivity:Lcom/konka/mm/photo/AutoShowPicActivity;

    const v1, 0x7f0b00ee    # com.konka.mm.R.id.textview_item6

    invoke-virtual {v0, v1}, Lcom/konka/mm/photo/AutoShowPicActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->text_leferotion:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->mPicShowActivity:Lcom/konka/mm/photo/AutoShowPicActivity;

    const v1, 0x7f0b00f0    # com.konka.mm.R.id.textview_item7

    invoke-virtual {v0, v1}, Lcom/konka/mm/photo/AutoShowPicActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->text_rightrotion:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->mPicShowActivity:Lcom/konka/mm/photo/AutoShowPicActivity;

    const v1, 0x7f0b00f2    # com.konka.mm.R.id.textview_item8

    invoke-virtual {v0, v1}, Lcom/konka/mm/photo/AutoShowPicActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->text_enlarge:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->mPicShowActivity:Lcom/konka/mm/photo/AutoShowPicActivity;

    const v1, 0x7f0b00f4    # com.konka.mm.R.id.textview_item9

    invoke-virtual {v0, v1}, Lcom/konka/mm/photo/AutoShowPicActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->text_descale:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->mPicShowActivity:Lcom/konka/mm/photo/AutoShowPicActivity;

    const v1, 0x7f0b00f6    # com.konka.mm.R.id.textview_item10

    invoke-virtual {v0, v1}, Lcom/konka/mm/photo/AutoShowPicActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->text_fullscreen:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->mPicShowActivity:Lcom/konka/mm/photo/AutoShowPicActivity;

    const v1, 0x7f0b008e    # com.konka.mm.R.id.txt_play_order

    invoke-virtual {v0, v1}, Lcom/konka/mm/photo/AutoShowPicActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->text_play_order:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->mPicShowActivity:Lcom/konka/mm/photo/AutoShowPicActivity;

    const v1, 0x7f0b00dc    # com.konka.mm.R.id.buttoms_set

    invoke-virtual {v0, v1}, Lcom/konka/mm/photo/AutoShowPicActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->pic_bts_layout:Landroid/widget/RelativeLayout;

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->bt_wallpaper:Landroid/widget/Button;

    new-instance v1, Lcom/konka/mm/photo/PicShowHolder$ItemOnClickEvent;

    invoke-direct {v1, p0}, Lcom/konka/mm/photo/PicShowHolder$ItemOnClickEvent;-><init>(Lcom/konka/mm/photo/PicShowHolder;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->bt_galleryShow:Landroid/widget/Button;

    new-instance v1, Lcom/konka/mm/photo/PicShowHolder$ItemOnClickEvent;

    invoke-direct {v1, p0}, Lcom/konka/mm/photo/PicShowHolder$ItemOnClickEvent;-><init>(Lcom/konka/mm/photo/PicShowHolder;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->bt_pre:Landroid/widget/Button;

    new-instance v1, Lcom/konka/mm/photo/PicShowHolder$ItemOnClickEvent;

    invoke-direct {v1, p0}, Lcom/konka/mm/photo/PicShowHolder$ItemOnClickEvent;-><init>(Lcom/konka/mm/photo/PicShowHolder;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->bt_autoPlay:Landroid/widget/Button;

    new-instance v1, Lcom/konka/mm/photo/PicShowHolder$ItemOnClickEvent;

    invoke-direct {v1, p0}, Lcom/konka/mm/photo/PicShowHolder$ItemOnClickEvent;-><init>(Lcom/konka/mm/photo/PicShowHolder;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->bt_Pause:Landroid/widget/Button;

    new-instance v1, Lcom/konka/mm/photo/PicShowHolder$ItemOnClickEvent;

    invoke-direct {v1, p0}, Lcom/konka/mm/photo/PicShowHolder$ItemOnClickEvent;-><init>(Lcom/konka/mm/photo/PicShowHolder;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->bt_next:Landroid/widget/Button;

    new-instance v1, Lcom/konka/mm/photo/PicShowHolder$ItemOnClickEvent;

    invoke-direct {v1, p0}, Lcom/konka/mm/photo/PicShowHolder$ItemOnClickEvent;-><init>(Lcom/konka/mm/photo/PicShowHolder;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->bt_stop:Landroid/widget/Button;

    new-instance v1, Lcom/konka/mm/photo/PicShowHolder$ItemOnClickEvent;

    invoke-direct {v1, p0}, Lcom/konka/mm/photo/PicShowHolder$ItemOnClickEvent;-><init>(Lcom/konka/mm/photo/PicShowHolder;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->bt_leftRotion:Landroid/widget/Button;

    new-instance v1, Lcom/konka/mm/photo/PicShowHolder$ItemOnClickEvent;

    invoke-direct {v1, p0}, Lcom/konka/mm/photo/PicShowHolder$ItemOnClickEvent;-><init>(Lcom/konka/mm/photo/PicShowHolder;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->bt_rightRotion:Landroid/widget/Button;

    new-instance v1, Lcom/konka/mm/photo/PicShowHolder$ItemOnClickEvent;

    invoke-direct {v1, p0}, Lcom/konka/mm/photo/PicShowHolder$ItemOnClickEvent;-><init>(Lcom/konka/mm/photo/PicShowHolder;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->bt_big:Landroid/widget/Button;

    new-instance v1, Lcom/konka/mm/photo/PicShowHolder$ItemOnClickEvent;

    invoke-direct {v1, p0}, Lcom/konka/mm/photo/PicShowHolder$ItemOnClickEvent;-><init>(Lcom/konka/mm/photo/PicShowHolder;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->bt_small:Landroid/widget/Button;

    new-instance v1, Lcom/konka/mm/photo/PicShowHolder$ItemOnClickEvent;

    invoke-direct {v1, p0}, Lcom/konka/mm/photo/PicShowHolder$ItemOnClickEvent;-><init>(Lcom/konka/mm/photo/PicShowHolder;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->bt_fullScreen:Landroid/widget/Button;

    new-instance v1, Lcom/konka/mm/photo/PicShowHolder$ItemOnClickEvent;

    invoke-direct {v1, p0}, Lcom/konka/mm/photo/PicShowHolder$ItemOnClickEvent;-><init>(Lcom/konka/mm/photo/PicShowHolder;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->bt_suquence:Landroid/widget/Button;

    new-instance v1, Lcom/konka/mm/photo/PicShowHolder$ItemOnClickEvent;

    invoke-direct {v1, p0}, Lcom/konka/mm/photo/PicShowHolder$ItemOnClickEvent;-><init>(Lcom/konka/mm/photo/PicShowHolder;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->bt_circulate:Landroid/widget/Button;

    new-instance v1, Lcom/konka/mm/photo/PicShowHolder$ItemOnClickEvent;

    invoke-direct {v1, p0}, Lcom/konka/mm/photo/PicShowHolder$ItemOnClickEvent;-><init>(Lcom/konka/mm/photo/PicShowHolder;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->bt_wallpaper:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/mm/photo/PicShowHolder;->onFocusChange:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->bt_galleryShow:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/mm/photo/PicShowHolder;->onFocusChange:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->bt_pre:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/mm/photo/PicShowHolder;->onFocusChange:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->bt_autoPlay:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/mm/photo/PicShowHolder;->onFocusChange:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->bt_Pause:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/mm/photo/PicShowHolder;->onFocusChange:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->bt_next:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/mm/photo/PicShowHolder;->onFocusChange:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->bt_stop:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/mm/photo/PicShowHolder;->onFocusChange:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->bt_leftRotion:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/mm/photo/PicShowHolder;->onFocusChange:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->bt_rightRotion:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/mm/photo/PicShowHolder;->onFocusChange:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->bt_big:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/mm/photo/PicShowHolder;->onFocusChange:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->bt_small:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/mm/photo/PicShowHolder;->onFocusChange:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->bt_fullScreen:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/mm/photo/PicShowHolder;->onFocusChange:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->bt_suquence:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/mm/photo/PicShowHolder;->onFocusChange:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lcom/konka/mm/photo/PicShowHolder;->bt_circulate:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/mm/photo/PicShowHolder;->onFocusChange:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    return-void
.end method
