.class public Lcom/konka/mm/photo/PicAdapter;
.super Landroid/widget/BaseAdapter;
.source "PicAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/mm/photo/PicAdapter$AsyncLoadImage;,
        Lcom/konka/mm/photo/PicAdapter$ImageCallback;,
        Lcom/konka/mm/photo/PicAdapter$PicItem;
    }
.end annotation


# instance fields
.field private activity:Lcom/konka/mm/photo/PicMainActivity;

.field private handler:Landroid/os/Handler;

.field private pageCount:I

.field private pageIndex:I

.field private perPageSize:I


# direct methods
.method public constructor <init>(Lcom/konka/mm/photo/PicMainActivity;III)V
    .locals 1
    .param p1    # Lcom/konka/mm/photo/PicMainActivity;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/konka/mm/photo/PicAdapter;->pageIndex:I

    new-instance v0, Lcom/konka/mm/photo/PicAdapter$1;

    invoke-direct {v0, p0}, Lcom/konka/mm/photo/PicAdapter$1;-><init>(Lcom/konka/mm/photo/PicAdapter;)V

    iput-object v0, p0, Lcom/konka/mm/photo/PicAdapter;->handler:Landroid/os/Handler;

    iput-object p1, p0, Lcom/konka/mm/photo/PicAdapter;->activity:Lcom/konka/mm/photo/PicMainActivity;

    iput p2, p0, Lcom/konka/mm/photo/PicAdapter;->pageIndex:I

    iput p3, p0, Lcom/konka/mm/photo/PicAdapter;->pageCount:I

    iput p4, p0, Lcom/konka/mm/photo/PicAdapter;->perPageSize:I

    return-void
.end method

.method public constructor <init>(Lcom/konka/mm/photo/PicMainActivity;[Ljava/lang/String;)V
    .locals 1
    .param p1    # Lcom/konka/mm/photo/PicMainActivity;
    .param p2    # [Ljava/lang/String;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/konka/mm/photo/PicAdapter;->pageIndex:I

    new-instance v0, Lcom/konka/mm/photo/PicAdapter$1;

    invoke-direct {v0, p0}, Lcom/konka/mm/photo/PicAdapter$1;-><init>(Lcom/konka/mm/photo/PicAdapter;)V

    iput-object v0, p0, Lcom/konka/mm/photo/PicAdapter;->handler:Landroid/os/Handler;

    iput-object p1, p0, Lcom/konka/mm/photo/PicAdapter;->activity:Lcom/konka/mm/photo/PicMainActivity;

    return-void
.end method

.method static synthetic access$0(Lcom/konka/mm/photo/PicAdapter;)Lcom/konka/mm/photo/PicMainActivity;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/photo/PicAdapter;->activity:Lcom/konka/mm/photo/PicMainActivity;

    return-object v0
.end method

.method static synthetic access$1(Lcom/konka/mm/photo/PicAdapter;)I
    .locals 1

    iget v0, p0, Lcom/konka/mm/photo/PicAdapter;->pageIndex:I

    return v0
.end method

.method static synthetic access$2(Lcom/konka/mm/photo/PicAdapter;)I
    .locals 1

    iget v0, p0, Lcom/konka/mm/photo/PicAdapter;->perPageSize:I

    return v0
.end method

.method static synthetic access$3(Lcom/konka/mm/photo/PicAdapter;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/photo/PicAdapter;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method public static getBitmapByUrl(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 4
    .param p0    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/MalformedURLException;,
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v2, Ljava/net/URL;

    invoke-direct {v2, p0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v1

    const/16 v3, 0x61a8

    invoke-virtual {v1, v3}, Ljava/net/URLConnection;->setConnectTimeout(I)V

    const v3, 0x15f90

    invoke-virtual {v1, v3}, Ljava/net/URLConnection;->setReadTimeout(I)V

    invoke-virtual {v1}, Ljava/net/URLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v3

    invoke-static {v3}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method private setThumbImage(Landroid/widget/ImageView;I)V
    .locals 2
    .param p1    # Landroid/widget/ImageView;
    .param p2    # I

    sget-object v0, Lcom/konka/mm/photo/PicMainActivity;->executorService:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/konka/mm/photo/PicAdapter$2;

    invoke-direct {v1, p0, p2, p1}, Lcom/konka/mm/photo/PicAdapter$2;-><init>(Lcom/konka/mm/photo/PicAdapter;ILandroid/widget/ImageView;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    iget v0, p0, Lcom/konka/mm/photo/PicAdapter;->pageCount:I

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1    # I

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1    # I

    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    const/4 v7, 0x0

    const/4 v8, 0x2

    if-nez p2, :cond_2

    new-instance v2, Lcom/konka/mm/photo/PicAdapter$PicItem;

    invoke-direct {v2, p0}, Lcom/konka/mm/photo/PicAdapter$PicItem;-><init>(Lcom/konka/mm/photo/PicAdapter;)V

    sget v5, Lcom/konka/mm/photo/PhotoDiskActivity;->List_Mode:I

    if-ne v5, v8, :cond_1

    iget-object v5, p0, Lcom/konka/mm/photo/PicAdapter;->activity:Lcom/konka/mm/photo/PicMainActivity;

    invoke-static {v5}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v5

    const v6, 0x7f030026    # com.konka.mm.R.layout.picadapter

    invoke-virtual {v5, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    :goto_0
    invoke-virtual {p2, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :goto_1
    const v5, 0x7f0b0026    # com.konka.mm.R.id.picitem

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    iput-object v5, v2, Lcom/konka/mm/photo/PicAdapter$PicItem;->mPicIcon:Landroid/widget/ImageView;

    const v5, 0x7f0b0028    # com.konka.mm.R.id.textitem

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, v2, Lcom/konka/mm/photo/PicAdapter$PicItem;->mPicName:Landroid/widget/TextView;

    const/4 v4, 0x0

    const-string v3, ""

    :try_start_0
    iget-object v5, p0, Lcom/konka/mm/photo/PicAdapter;->activity:Lcom/konka/mm/photo/PicMainActivity;

    iget-object v5, v5, Lcom/konka/mm/photo/PicMainActivity;->picPaths:Ljava/util/ArrayList;

    iget v6, p0, Lcom/konka/mm/photo/PicAdapter;->pageIndex:I

    iget v7, p0, Lcom/konka/mm/photo/PicAdapter;->perPageSize:I

    mul-int/2addr v6, v7

    add-int/2addr v6, p1

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    move-object v0, v5

    check-cast v0, Ljava/lang/String;

    move-object v4, v0

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    :goto_2
    if-nez v4, :cond_3

    :cond_0
    :goto_3
    return-object p2

    :cond_1
    iget-object v5, p0, Lcom/konka/mm/photo/PicAdapter;->activity:Lcom/konka/mm/photo/PicMainActivity;

    invoke-static {v5}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v5

    const v6, 0x7f030012    # com.konka.mm.R.layout.listadapter

    invoke-virtual {v5, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    goto :goto_0

    :cond_2
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/konka/mm/photo/PicAdapter$PicItem;

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    :cond_3
    iget-object v5, v2, Lcom/konka/mm/photo/PicAdapter$PicItem;->mPicIcon:Landroid/widget/ImageView;

    const v6, 0x7f02006a    # com.konka.mm.R.drawable.media_default_images_uns

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v5, v2, Lcom/konka/mm/photo/PicAdapter$PicItem;->mPicName:Landroid/widget/TextView;

    invoke-virtual {v5, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    sget v5, Lcom/konka/mm/photo/PhotoDiskActivity;->List_Mode:I

    if-ne v5, v8, :cond_0

    iget-object v5, v2, Lcom/konka/mm/photo/PicAdapter$PicItem;->handler:Lcom/konka/mm/photo/ImageThumbHandler;

    if-eqz v5, :cond_4

    iget-object v5, p0, Lcom/konka/mm/photo/PicAdapter;->activity:Lcom/konka/mm/photo/PicMainActivity;

    iget-object v5, v5, Lcom/konka/mm/photo/PicMainActivity;->picPaths:Ljava/util/ArrayList;

    iget v6, p0, Lcom/konka/mm/photo/PicAdapter;->pageIndex:I

    iget v7, p0, Lcom/konka/mm/photo/PicAdapter;->perPageSize:I

    mul-int/2addr v6, v7

    add-int/2addr v6, p1

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    iget-object v6, v2, Lcom/konka/mm/photo/PicAdapter$PicItem;->handler:Lcom/konka/mm/photo/ImageThumbHandler;

    invoke-static {v5, v6}, Lcom/konka/mm/photo/PhotoThumbLoader;->remove(Ljava/lang/String;Lcom/konka/mm/photo/ImageThumbHandler;)Z

    :cond_4
    new-instance v5, Lcom/konka/mm/photo/ThumbFileDownloadHandler;

    iget-object v6, v2, Lcom/konka/mm/photo/PicAdapter$PicItem;->mPicIcon:Landroid/widget/ImageView;

    invoke-direct {v5, v6}, Lcom/konka/mm/photo/ThumbFileDownloadHandler;-><init>(Landroid/widget/ImageView;)V

    iput-object v5, v2, Lcom/konka/mm/photo/PicAdapter$PicItem;->handler:Lcom/konka/mm/photo/ImageThumbHandler;

    iget-object v5, p0, Lcom/konka/mm/photo/PicAdapter;->activity:Lcom/konka/mm/photo/PicMainActivity;

    iget-object v5, v5, Lcom/konka/mm/photo/PicMainActivity;->picPaths:Ljava/util/ArrayList;

    iget v6, p0, Lcom/konka/mm/photo/PicAdapter;->pageIndex:I

    iget v7, p0, Lcom/konka/mm/photo/PicAdapter;->perPageSize:I

    mul-int/2addr v6, v7

    add-int/2addr v6, p1

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    iget-object v6, v2, Lcom/konka/mm/photo/PicAdapter$PicItem;->handler:Lcom/konka/mm/photo/ImageThumbHandler;

    invoke-static {v5, v6}, Lcom/konka/mm/photo/PhotoThumbLoader;->start(Ljava/lang/String;Lcom/konka/mm/photo/ImageThumbHandler;)Z

    goto :goto_3
.end method
