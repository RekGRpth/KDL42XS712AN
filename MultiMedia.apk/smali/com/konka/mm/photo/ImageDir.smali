.class public Lcom/konka/mm/photo/ImageDir;
.super Ljava/lang/Object;
.source "ImageDir.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/mm/photo/ImageDir$DirPathInfo;,
        Lcom/konka/mm/photo/ImageDir$FILE_TYPE;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/konka/mm/photo/ImageDir;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mCreatedTime:Ljava/lang/String;

.field private mFileId:Ljava/lang/String;

.field private mFileSize:I

.field private mFileVersion:I

.field private mModifiedTime:Ljava/lang/String;

.field private mName:Ljava/lang/String;

.field private mOperationVersion:I

.field private mParentId:Ljava/lang/String;

.field private mPath:Ljava/lang/String;

.field private mSha1:Ljava/lang/String;

.field private mSharedId:Ljava/lang/String;

.field private mType:Ljava/lang/String;

.field private mUserId:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/konka/mm/photo/ImageDir$1;

    invoke-direct {v0}, Lcom/konka/mm/photo/ImageDir$1;-><init>()V

    sput-object v0, Lcom/konka/mm/photo/ImageDir;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1    # Landroid/os/Parcel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-direct {p0, p1}, Lcom/konka/mm/photo/ImageDir;->readFromParcel(Landroid/os/Parcel;)V

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/konka/mm/photo/ImageDir;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/mm/photo/ImageDir;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public static getFilesOnlyList(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/konka/mm/photo/ImageDir;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/konka/mm/photo/ImageDir;",
            ">;"
        }
    .end annotation

    if-nez p0, :cond_1

    const/4 v2, 0x0

    :cond_0
    return-object v2

    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/konka/mm/photo/ImageDir;

    invoke-virtual {v1}, Lcom/konka/mm/photo/ImageDir;->getEnumFileType()Lcom/konka/mm/photo/ImageDir$FILE_TYPE;

    move-result-object v3

    sget-object v4, Lcom/konka/mm/photo/ImageDir$FILE_TYPE;->FILE:Lcom/konka/mm/photo/ImageDir$FILE_TYPE;

    invoke-virtual {v3, v4}, Lcom/konka/mm/photo/ImageDir$FILE_TYPE;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1    # Landroid/os/Parcel;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/mm/photo/ImageDir;->mSha1:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/konka/mm/photo/ImageDir;->mUserId:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/mm/photo/ImageDir;->mName:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/konka/mm/photo/ImageDir;->mFileVersion:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/mm/photo/ImageDir;->mParentId:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/mm/photo/ImageDir;->mFileId:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/mm/photo/ImageDir;->mCreatedTime:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/mm/photo/ImageDir;->mModifiedTime:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/mm/photo/ImageDir;->mSharedId:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/konka/mm/photo/ImageDir;->mOperationVersion:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/mm/photo/ImageDir;->mType:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/konka/mm/photo/ImageDir;->mFileSize:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/mm/photo/ImageDir;->mPath:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getCreatedTime()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/photo/ImageDir;->mCreatedTime:Ljava/lang/String;

    return-object v0
.end method

.method public getEnumFileType()Lcom/konka/mm/photo/ImageDir$FILE_TYPE;
    .locals 2

    iget-object v0, p0, Lcom/konka/mm/photo/ImageDir;->mType:Ljava/lang/String;

    const-string v1, "file"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/konka/mm/photo/ImageDir;->mType:Ljava/lang/String;

    const-string v1, "File"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/konka/mm/photo/ImageDir$FILE_TYPE;->FILE:Lcom/konka/mm/photo/ImageDir$FILE_TYPE;

    :goto_0
    return-object v0

    :cond_1
    sget-object v0, Lcom/konka/mm/photo/ImageDir$FILE_TYPE;->FOLDER:Lcom/konka/mm/photo/ImageDir$FILE_TYPE;

    goto :goto_0
.end method

.method public getFileSize()I
    .locals 1

    iget v0, p0, Lcom/konka/mm/photo/ImageDir;->mFileSize:I

    return v0
.end method

.method public getFileType()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/photo/ImageDir;->mType:Ljava/lang/String;

    return-object v0
.end method

.method public getFileVersion()I
    .locals 1

    iget v0, p0, Lcom/konka/mm/photo/ImageDir;->mFileVersion:I

    return v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/photo/ImageDir;->mFileId:Ljava/lang/String;

    return-object v0
.end method

.method public getModifiedTime()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/photo/ImageDir;->mModifiedTime:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/photo/ImageDir;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getNameByPathPostFix()Ljava/lang/String;
    .locals 4

    const/16 v3, 0x2f

    iget-object v1, p0, Lcom/konka/mm/photo/ImageDir;->mPath:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/konka/mm/photo/ImageDir;->mPath:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/konka/mm/photo/ImageDir;->mPath:Ljava/lang/String;

    add-int/lit8 v2, v0, 0x1

    iget-object v3, p0, Lcom/konka/mm/photo/ImageDir;->mPath:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    :goto_1
    return-object v1

    :cond_0
    iget-object v1, p0, Lcom/konka/mm/photo/ImageDir;->mPath:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    goto :goto_0

    :cond_1
    const-string v1, "unknown"

    goto :goto_1
.end method

.method public getOperationVersion()I
    .locals 1

    iget v0, p0, Lcom/konka/mm/photo/ImageDir;->mOperationVersion:I

    return v0
.end method

.method public getParentId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/photo/ImageDir;->mParentId:Ljava/lang/String;

    return-object v0
.end method

.method public getPath()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/photo/ImageDir;->mPath:Ljava/lang/String;

    return-object v0
.end method

.method public getSha1()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/photo/ImageDir;->mSha1:Ljava/lang/String;

    return-object v0
.end method

.method public getSharedId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/photo/ImageDir;->mSharedId:Ljava/lang/String;

    return-object v0
.end method

.method public getUserId()I
    .locals 1

    iget v0, p0, Lcom/konka/mm/photo/ImageDir;->mUserId:I

    return v0
.end method

.method public setCreatedTime(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/mm/photo/ImageDir;->mCreatedTime:Ljava/lang/String;

    return-void
.end method

.method public setFileSize(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/konka/mm/photo/ImageDir;->mFileSize:I

    return-void
.end method

.method public setFileType(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/mm/photo/ImageDir;->mType:Ljava/lang/String;

    return-void
.end method

.method public setFileVersion(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/konka/mm/photo/ImageDir;->mFileVersion:I

    return-void
.end method

.method public setId(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/mm/photo/ImageDir;->mFileId:Ljava/lang/String;

    return-void
.end method

.method public setModifiedTime(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/mm/photo/ImageDir;->mModifiedTime:Ljava/lang/String;

    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/mm/photo/ImageDir;->mName:Ljava/lang/String;

    return-void
.end method

.method public setOperationVersion(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/konka/mm/photo/ImageDir;->mOperationVersion:I

    return-void
.end method

.method public setParentId(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/mm/photo/ImageDir;->mParentId:Ljava/lang/String;

    return-void
.end method

.method public setPath(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/mm/photo/ImageDir;->mPath:Ljava/lang/String;

    return-void
.end method

.method public setSha1(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/mm/photo/ImageDir;->mSha1:Ljava/lang/String;

    return-void
.end method

.method public setSharedId(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/mm/photo/ImageDir;->mSharedId:Ljava/lang/String;

    return-void
.end method

.method public setUserId(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/konka/mm/photo/ImageDir;->mUserId:I

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "mSha1:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/mm/photo/ImageDir;->mSha1:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", userId:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/konka/mm/photo/ImageDir;->mUserId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mName:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/mm/photo/ImageDir;->mName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mFileVersion:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/konka/mm/photo/ImageDir;->mFileVersion:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mParentId:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/mm/photo/ImageDir;->mParentId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mFileId:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/mm/photo/ImageDir;->mFileId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mCreatedTime:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/mm/photo/ImageDir;->mCreatedTime:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mModifiedTime:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/mm/photo/ImageDir;->mModifiedTime:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mSharedId:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/mm/photo/ImageDir;->mSharedId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mOperationVersion:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/konka/mm/photo/ImageDir;->mOperationVersion:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mType:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/mm/photo/ImageDir;->mType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mFileSize:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/konka/mm/photo/ImageDir;->mFileSize:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget-object v0, p0, Lcom/konka/mm/photo/ImageDir;->mSha1:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget v0, p0, Lcom/konka/mm/photo/ImageDir;->mUserId:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/konka/mm/photo/ImageDir;->mName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget v0, p0, Lcom/konka/mm/photo/ImageDir;->mFileVersion:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/konka/mm/photo/ImageDir;->mParentId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/mm/photo/ImageDir;->mFileId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/mm/photo/ImageDir;->mCreatedTime:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/mm/photo/ImageDir;->mModifiedTime:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/mm/photo/ImageDir;->mSharedId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget v0, p0, Lcom/konka/mm/photo/ImageDir;->mOperationVersion:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/konka/mm/photo/ImageDir;->mType:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget v0, p0, Lcom/konka/mm/photo/ImageDir;->mFileSize:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/konka/mm/photo/ImageDir;->mPath:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method
