.class Lcom/konka/mm/photo/PicMainActivity$5;
.super Ljava/lang/Object;
.source "PicMainActivity.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/mm/photo/PicMainActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/mm/photo/PicMainActivity;


# direct methods
.method constructor <init>(Lcom/konka/mm/photo/PicMainActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/photo/PicMainActivity$5;->this$0:Lcom/konka/mm/photo/PicMainActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # Landroid/view/KeyEvent;

    const/4 v2, 0x1

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    const/4 v0, 0x0

    return v0

    :pswitch_0
    const-string v0, "ScrollLayout"

    const-string v1, "PAGE UP"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/mm/photo/PicMainActivity$5;->this$0:Lcom/konka/mm/photo/PicMainActivity;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/konka/mm/photo/PicMainActivity;->snapScreen(I)V

    goto :goto_0

    :pswitch_1
    const-string v0, "ScrollLayout"

    const-string v1, "PAGE DOWN"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/mm/photo/PicMainActivity$5;->this$0:Lcom/konka/mm/photo/PicMainActivity;

    iget v0, v0, Lcom/konka/mm/photo/PicMainActivity;->currentPage:I

    iget-object v1, p0, Lcom/konka/mm/photo/PicMainActivity$5;->this$0:Lcom/konka/mm/photo/PicMainActivity;

    iget v1, v1, Lcom/konka/mm/photo/PicMainActivity;->PageCount:I

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Lcom/konka/mm/photo/PicMainActivity$5;->this$0:Lcom/konka/mm/photo/PicMainActivity;

    invoke-static {v0, v2}, Lcom/konka/mm/photo/PicMainActivity;->access$6(Lcom/konka/mm/photo/PicMainActivity;Z)V

    iget-object v0, p0, Lcom/konka/mm/photo/PicMainActivity$5;->this$0:Lcom/konka/mm/photo/PicMainActivity;

    invoke-virtual {v0, v2}, Lcom/konka/mm/photo/PicMainActivity;->snapScreen(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x5c
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
