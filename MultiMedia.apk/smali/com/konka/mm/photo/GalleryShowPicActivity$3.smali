.class Lcom/konka/mm/photo/GalleryShowPicActivity$3;
.super Ljava/lang/Object;
.source "GalleryShowPicActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/mm/photo/GalleryShowPicActivity;->findView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemSelectedListener;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/mm/photo/GalleryShowPicActivity;


# direct methods
.method constructor <init>(Lcom/konka/mm/photo/GalleryShowPicActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/photo/GalleryShowPicActivity$3;->this$0:Lcom/konka/mm/photo/GalleryShowPicActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/konka/mm/photo/GalleryShowPicActivity$3;->this$0:Lcom/konka/mm/photo/GalleryShowPicActivity;

    # getter for: Lcom/konka/mm/photo/GalleryShowPicActivity;->picPaths:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/konka/mm/photo/GalleryShowPicActivity;->access$0(Lcom/konka/mm/photo/GalleryShowPicActivity;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/mm/photo/GalleryShowPicActivity$3;->this$0:Lcom/konka/mm/photo/GalleryShowPicActivity;

    invoke-static {v1, p3}, Lcom/konka/mm/photo/GalleryShowPicActivity;->access$5(Lcom/konka/mm/photo/GalleryShowPicActivity;I)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/konka/mm/photo/GalleryShowPicActivity$3;->this$0:Lcom/konka/mm/photo/GalleryShowPicActivity;

    iget-object v1, v1, Lcom/konka/mm/photo/GalleryShowPicActivity;->toast_file_not_exist:Landroid/widget/Toast;

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/konka/mm/photo/GalleryShowPicActivity$3;->this$0:Lcom/konka/mm/photo/GalleryShowPicActivity;

    invoke-virtual {v1}, Lcom/konka/mm/photo/GalleryShowPicActivity;->setPhotoView()V

    goto :goto_0
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    return-void
.end method
