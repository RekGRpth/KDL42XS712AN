.class public Lcom/konka/mm/photo/CallbackImg;
.super Ljava/lang/Object;
.source "CallbackImg.java"

# interfaces
.implements Lcom/konka/mm/photo/AsyncImageLoader$ImageCallback;


# instance fields
.field private autoShowPicActivity:Lcom/konka/mm/photo/AutoShowPicActivity;

.field private galleryShowPicActivity:Lcom/konka/mm/photo/GalleryShowPicActivity;

.field private imageSwitcher:Lcom/konka/mm/photo/ImageViewTouch;

.field private imageView:Lcom/konka/mm/photo/ImageViewTouch;

.field protected mAlphaIn:Landroid/view/animation/Animation;

.field private rotateBitmap:Lcom/konka/mm/photo/RotateBitmap;


# direct methods
.method public constructor <init>(Lcom/konka/mm/photo/ImageViewTouch;Lcom/konka/mm/photo/AutoShowPicActivity;)V
    .locals 2
    .param p1    # Lcom/konka/mm/photo/ImageViewTouch;
    .param p2    # Lcom/konka/mm/photo/AutoShowPicActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/konka/mm/photo/RotateBitmap;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/konka/mm/photo/RotateBitmap;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lcom/konka/mm/photo/CallbackImg;->rotateBitmap:Lcom/konka/mm/photo/RotateBitmap;

    iput-object p1, p0, Lcom/konka/mm/photo/CallbackImg;->imageView:Lcom/konka/mm/photo/ImageViewTouch;

    iput-object p2, p0, Lcom/konka/mm/photo/CallbackImg;->autoShowPicActivity:Lcom/konka/mm/photo/AutoShowPicActivity;

    return-void
.end method

.method public constructor <init>(Lcom/konka/mm/photo/ImageViewTouch;Lcom/konka/mm/photo/GalleryShowPicActivity;)V
    .locals 2
    .param p1    # Lcom/konka/mm/photo/ImageViewTouch;
    .param p2    # Lcom/konka/mm/photo/GalleryShowPicActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/konka/mm/photo/RotateBitmap;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/konka/mm/photo/RotateBitmap;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lcom/konka/mm/photo/CallbackImg;->rotateBitmap:Lcom/konka/mm/photo/RotateBitmap;

    iput-object p1, p0, Lcom/konka/mm/photo/CallbackImg;->imageSwitcher:Lcom/konka/mm/photo/ImageViewTouch;

    iput-object p2, p0, Lcom/konka/mm/photo/CallbackImg;->galleryShowPicActivity:Lcom/konka/mm/photo/GalleryShowPicActivity;

    return-void
.end method


# virtual methods
.method public imageLoaded(Landroid/graphics/Bitmap;)V
    .locals 3
    .param p1    # Landroid/graphics/Bitmap;

    const/4 v2, 0x1

    sget v0, Lcom/konka/mm/photo/AsyncImageLoader;->mFlag:I

    if-ne v0, v2, :cond_2

    iget-object v0, p0, Lcom/konka/mm/photo/CallbackImg;->rotateBitmap:Lcom/konka/mm/photo/RotateBitmap;

    invoke-virtual {v0, p1}, Lcom/konka/mm/photo/RotateBitmap;->setBitmap(Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Lcom/konka/mm/photo/CallbackImg;->imageView:Lcom/konka/mm/photo/ImageViewTouch;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/mm/photo/CallbackImg;->imageView:Lcom/konka/mm/photo/ImageViewTouch;

    invoke-virtual {v0, p1}, Lcom/konka/mm/photo/ImageViewTouch;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :cond_0
    sput-boolean v2, Lcom/konka/mm/photo/AutoShowPicActivity;->bAutoPlayKey:Z

    const/16 v0, 0x15

    invoke-static {v0}, Lcom/konka/mm/photo/AutoShowPicActivity;->hideDialog(I)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    sget v0, Lcom/konka/mm/photo/AsyncImageLoader;->mFlag:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/konka/mm/photo/CallbackImg;->rotateBitmap:Lcom/konka/mm/photo/RotateBitmap;

    invoke-virtual {v0, p1}, Lcom/konka/mm/photo/RotateBitmap;->setBitmap(Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Lcom/konka/mm/photo/CallbackImg;->imageSwitcher:Lcom/konka/mm/photo/ImageViewTouch;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/konka/mm/photo/CallbackImg;->imageSwitcher:Lcom/konka/mm/photo/ImageViewTouch;

    invoke-virtual {v0, p1}, Lcom/konka/mm/photo/ImageViewTouch;->setImageBitmap(Landroid/graphics/Bitmap;)V

    invoke-static {v2}, Lcom/konka/mm/photo/GalleryShowPicActivity;->hideDialog(I)V

    goto :goto_0
.end method
