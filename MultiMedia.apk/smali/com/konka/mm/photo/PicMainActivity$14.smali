.class Lcom/konka/mm/photo/PicMainActivity$14;
.super Ljava/lang/Object;
.source "PicMainActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/mm/photo/PicMainActivity;->showDataChangeDlg(Landroid/app/Activity;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/mm/photo/PicMainActivity;


# direct methods
.method constructor <init>(Lcom/konka/mm/photo/PicMainActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/photo/PicMainActivity$14;->this$0:Lcom/konka/mm/photo/PicMainActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    iget-object v1, p0, Lcom/konka/mm/photo/PicMainActivity$14;->this$0:Lcom/konka/mm/photo/PicMainActivity;

    iget-object v1, v1, Lcom/konka/mm/photo/PicMainActivity;->picPaths:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    iget-object v1, p0, Lcom/konka/mm/photo/PicMainActivity$14;->this$0:Lcom/konka/mm/photo/PicMainActivity;

    iget-object v2, p0, Lcom/konka/mm/photo/PicMainActivity$14;->this$0:Lcom/konka/mm/photo/PicMainActivity;

    # getter for: Lcom/konka/mm/photo/PicMainActivity;->mDBHelper:Lcom/konka/mm/tools/DBHelper1;
    invoke-static {v2}, Lcom/konka/mm/photo/PicMainActivity;->access$8(Lcom/konka/mm/photo/PicMainActivity;)Lcom/konka/mm/tools/DBHelper1;

    move-result-object v2

    sget-object v3, Lcom/konka/mm/photo/PicMainActivity;->mRootPath:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/konka/mm/tools/DBHelper1;->getImageSortByType(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, v1, Lcom/konka/mm/photo/PicMainActivity;->picPaths:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/konka/mm/photo/PicMainActivity$14;->this$0:Lcom/konka/mm/photo/PicMainActivity;

    # getter for: Lcom/konka/mm/photo/PicMainActivity;->sortHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/konka/mm/photo/PicMainActivity;->access$9(Lcom/konka/mm/photo/PicMainActivity;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    const/4 v1, 0x0

    iput v1, v0, Landroid/os/Message;->what:I

    iget-object v1, p0, Lcom/konka/mm/photo/PicMainActivity$14;->this$0:Lcom/konka/mm/photo/PicMainActivity;

    # getter for: Lcom/konka/mm/photo/PicMainActivity;->sortHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/konka/mm/photo/PicMainActivity;->access$9(Lcom/konka/mm/photo/PicMainActivity;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method
