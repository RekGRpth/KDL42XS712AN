.class public Lcom/konka/mm/photo/AsyncImageLoader;
.super Ljava/lang/Object;
.source "AsyncImageLoader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/mm/photo/AsyncImageLoader$ImageCallback;
    }
.end annotation


# static fields
.field public static final FLAG_IMAGESWITCHER:I = 0x2

.field public static final FLAG_IMAGEVIEW:I = 0x1

.field private static imageCache:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/ref/SoftReference",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;>;"
        }
    .end annotation
.end field

.field public static mFlag:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/konka/mm/photo/AsyncImageLoader;->imageCache:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0()Ljava/util/Map;
    .locals 1

    sget-object v0, Lcom/konka/mm/photo/AsyncImageLoader;->imageCache:Ljava/util/Map;

    return-object v0
.end method

.method public static loadBitmap(Ljava/lang/String;Lcom/konka/mm/photo/AsyncImageLoader$ImageCallback;I)Landroid/graphics/Bitmap;
    .locals 3
    .param p0    # Ljava/lang/String;
    .param p1    # Lcom/konka/mm/photo/AsyncImageLoader$ImageCallback;
    .param p2    # I

    sput p2, Lcom/konka/mm/photo/AsyncImageLoader;->mFlag:I

    sget-object v2, Lcom/konka/mm/photo/AsyncImageLoader;->imageCache:Ljava/util/Map;

    invoke-interface {v2, p0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    sget-object v2, Lcom/konka/mm/photo/AsyncImageLoader;->imageCache:Ljava/util/Map;

    invoke-interface {v2, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/ref/SoftReference;

    invoke-virtual {v1}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Bitmap;

    :goto_0
    return-object v2

    :cond_0
    new-instance v0, Lcom/konka/mm/photo/AsyncImageLoader$1;

    invoke-direct {v0, p1}, Lcom/konka/mm/photo/AsyncImageLoader$1;-><init>(Lcom/konka/mm/photo/AsyncImageLoader$ImageCallback;)V

    new-instance v2, Lcom/konka/mm/photo/AsyncImageLoader$2;

    invoke-direct {v2, p0, v0}, Lcom/konka/mm/photo/AsyncImageLoader$2;-><init>(Ljava/lang/String;Landroid/os/Handler;)V

    invoke-virtual {v2}, Lcom/konka/mm/photo/AsyncImageLoader$2;->start()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static loadBitmap1(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 4
    .param p0    # Ljava/lang/String;

    sget-object v2, Lcom/konka/mm/photo/AsyncImageLoader;->imageCache:Ljava/util/Map;

    invoke-interface {v2, p0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    sget-object v2, Lcom/konka/mm/photo/AsyncImageLoader;->imageCache:Ljava/util/Map;

    invoke-interface {v2, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/ref/SoftReference;

    invoke-virtual {v1}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Bitmap;

    :goto_0
    return-object v2

    :cond_0
    const v2, 0x1fa400

    invoke-static {p0, v2}, Lcom/konka/mm/tools/PicTool;->decodeFileDescriptor(Ljava/lang/String;I)Landroid/graphics/Bitmap;

    move-result-object v0

    sget-object v2, Lcom/konka/mm/photo/AsyncImageLoader;->imageCache:Ljava/util/Map;

    new-instance v3, Ljava/lang/ref/SoftReference;

    invoke-direct {v3, v0}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    invoke-interface {v2, p0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v2, v0

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method
