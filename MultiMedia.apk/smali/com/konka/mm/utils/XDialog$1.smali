.class Lcom/konka/mm/utils/XDialog$1;
.super Ljava/lang/Object;
.source "XDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/mm/utils/XDialog;->inputDialog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/konka/mm/utils/XDialog$InputClick;)Landroid/app/AlertDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final synthetic val$clickListener:Lcom/konka/mm/utils/XDialog$InputClick;

.field private final synthetic val$edit:Landroid/widget/EditText;


# direct methods
.method constructor <init>(Lcom/konka/mm/utils/XDialog$InputClick;Landroid/widget/EditText;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/utils/XDialog$1;->val$clickListener:Lcom/konka/mm/utils/XDialog$InputClick;

    iput-object p2, p0, Lcom/konka/mm/utils/XDialog$1;->val$edit:Landroid/widget/EditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    iget-object v0, p0, Lcom/konka/mm/utils/XDialog$1;->val$clickListener:Lcom/konka/mm/utils/XDialog$InputClick;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/mm/utils/XDialog$1;->val$clickListener:Lcom/konka/mm/utils/XDialog$InputClick;

    iget-object v1, p0, Lcom/konka/mm/utils/XDialog$1;->val$edit:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Lcom/konka/mm/utils/XDialog$InputClick;->onClickListener(Ljava/lang/String;I)V

    :cond_0
    return-void
.end method
