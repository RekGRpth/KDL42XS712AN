.class public Lcom/konka/mm/data/sambaDataManager;
.super Ljava/lang/Object;
.source "sambaDataManager.java"


# static fields
.field protected static directoryEntries:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/konka/mm/data/OtherData;",
            ">;"
        }
    .end annotation
.end field

.field protected static fileList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/konka/mm/data/OtherData;",
            ">;"
        }
    .end annotation
.end field

.field protected static folderList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/konka/mm/data/OtherData;",
            ">;"
        }
    .end annotation
.end field

.field private static rootDirectory:Ljava/io/File;


# instance fields
.field private context:Landroid/content/Context;

.field private currentDirectory:Ljava/io/File;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v0, 0x0

    sput-object v0, Lcom/konka/mm/data/sambaDataManager;->directoryEntries:Ljava/util/List;

    sput-object v0, Lcom/konka/mm/data/sambaDataManager;->folderList:Ljava/util/List;

    sput-object v0, Lcom/konka/mm/data/sambaDataManager;->fileList:Ljava/util/List;

    new-instance v0, Ljava/io/File;

    const-string v1, "/mnt/usb"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/konka/mm/data/sambaDataManager;->rootDirectory:Ljava/io/File;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/konka/mm/data/sambaDataManager;->currentDirectory:Ljava/io/File;

    iput-object p1, p0, Lcom/konka/mm/data/sambaDataManager;->context:Landroid/content/Context;

    sget-object v0, Lcom/konka/mm/data/sambaDataManager;->directoryEntries:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/konka/mm/data/sambaDataManager;->directoryEntries:Ljava/util/List;

    :cond_0
    sget-object v0, Lcom/konka/mm/data/sambaDataManager;->folderList:Ljava/util/List;

    if-nez v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/konka/mm/data/sambaDataManager;->folderList:Ljava/util/List;

    :cond_1
    sget-object v0, Lcom/konka/mm/data/sambaDataManager;->fileList:Ljava/util/List;

    if-nez v0, :cond_2

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/konka/mm/data/sambaDataManager;->fileList:Ljava/util/List;

    :cond_2
    return-void
.end method

.method private checkEmptyDirectory(Ljava/io/File;)Z
    .locals 4
    .param p1    # Ljava/io/File;

    const/4 v1, 0x0

    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v1

    :cond_0
    invoke-virtual {p1}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {p1}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v2

    array-length v2, v2

    if-nez v2, :cond_3

    :cond_1
    const/4 v1, 0x1

    :cond_2
    :goto_0
    return v1

    :cond_3
    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    array-length v3, v2

    if-eqz v3, :cond_2

    aget-object v0, v2, v1

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-direct {p0, v0}, Lcom/konka/mm/data/sambaDataManager;->checkEmptyDirectory(Ljava/io/File;)Z

    move-result v1

    goto :goto_0
.end method

.method private checkExtension(Ljava/lang/String;[Ljava/lang/String;)Z
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # [Ljava/lang/String;

    const/4 v1, 0x0

    array-length v3, p2

    move v2, v1

    :goto_0
    if-lt v2, v3, :cond_0

    :goto_1
    return v1

    :cond_0
    aget-object v0, p2, v2

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v1, 0x1

    goto :goto_1

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private scan([Ljava/io/File;)V
    .locals 8
    .param p1    # [Ljava/io/File;

    array-length v5, p1

    const/4 v4, 0x0

    :goto_0
    if-lt v4, v5, :cond_2

    sget-object v4, Lcom/konka/mm/data/sambaDataManager;->fileList:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    sget-object v4, Lcom/konka/mm/data/sambaDataManager;->folderList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_0

    sget-object v4, Lcom/konka/mm/data/sambaDataManager;->folderList:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    sget-object v4, Lcom/konka/mm/data/sambaDataManager;->directoryEntries:Ljava/util/List;

    sget-object v5, Lcom/konka/mm/data/sambaDataManager;->folderList:Ljava/util/List;

    invoke-interface {v4, v5}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_0
    sget-object v4, Lcom/konka/mm/data/sambaDataManager;->fileList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_1

    sget-object v4, Lcom/konka/mm/data/sambaDataManager;->fileList:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    sget-object v4, Lcom/konka/mm/data/sambaDataManager;->directoryEntries:Ljava/util/List;

    sget-object v5, Lcom/konka/mm/data/sambaDataManager;->fileList:Ljava/util/List;

    invoke-interface {v4, v5}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_1
    return-void

    :cond_2
    aget-object v0, p1, v4

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/konka/mm/data/OtherData;

    invoke-direct {v2}, Lcom/konka/mm/data/OtherData;-><init>()V

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v6

    if-eqz v6, :cond_3

    const-string v6, "dir"

    invoke-virtual {v2, v6}, Lcom/konka/mm/data/OtherData;->setType(Ljava/lang/String;)V

    sget-object v6, Lcom/konka/mm/data/sambaDataManager;->folderList:Ljava/util/List;

    invoke-interface {v6, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_1
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/konka/mm/data/OtherData;->setPath(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Lcom/konka/mm/data/OtherData;->setFileName(Ljava/lang/String;)V

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_3
    const-string v6, "."

    invoke-virtual {v1, v6}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v3

    if-lez v3, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v6

    add-int/lit8 v7, v3, 0x1

    invoke-virtual {v6, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/konka/mm/data/OtherData;->setFormat(Ljava/lang/String;)V

    :cond_4
    iget-object v6, p0, Lcom/konka/mm/data/sambaDataManager;->context:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const/high16 v7, 0x7f070000    # com.konka.mm.R.array.fileEndingImage

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v1, v6}, Lcom/konka/mm/data/sambaDataManager;->checkExtension(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_5

    const-string v6, "file"

    invoke-virtual {v2, v6}, Lcom/konka/mm/data/OtherData;->setType(Ljava/lang/String;)V

    sget-object v6, Lcom/konka/mm/data/sambaDataManager;->fileList:Ljava/util/List;

    invoke-interface {v6, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v6

    invoke-static {v6}, Lcom/konka/mm/tools/Tools;->formatSize(Ljava/math/BigInteger;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/konka/mm/data/OtherData;->setDescription(Ljava/lang/String;)V

    goto :goto_1

    :cond_5
    iget-object v6, p0, Lcom/konka/mm/data/sambaDataManager;->context:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f070001    # com.konka.mm.R.array.fileEndingAudio

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v1, v6}, Lcom/konka/mm/data/sambaDataManager;->checkExtension(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_6

    const-string v6, "file"

    invoke-virtual {v2, v6}, Lcom/konka/mm/data/OtherData;->setType(Ljava/lang/String;)V

    sget-object v6, Lcom/konka/mm/data/sambaDataManager;->fileList:Ljava/util/List;

    invoke-interface {v6, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_6
    iget-object v6, p0, Lcom/konka/mm/data/sambaDataManager;->context:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f070002    # com.konka.mm.R.array.fileEndingVedio

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v1, v6}, Lcom/konka/mm/data/sambaDataManager;->checkExtension(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_7

    const-string v6, "file"

    invoke-virtual {v2, v6}, Lcom/konka/mm/data/OtherData;->setType(Ljava/lang/String;)V

    sget-object v6, Lcom/konka/mm/data/sambaDataManager;->fileList:Ljava/util/List;

    invoke-interface {v6, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    :cond_7
    iget-object v6, p0, Lcom/konka/mm/data/sambaDataManager;->context:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f070005    # com.konka.mm.R.array.fileEndingPackage

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v1, v6}, Lcom/konka/mm/data/sambaDataManager;->checkExtension(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_8

    const-string v6, "other"

    invoke-virtual {v2, v6}, Lcom/konka/mm/data/OtherData;->setType(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v6

    invoke-static {v6}, Lcom/konka/mm/tools/Tools;->formatSize(Ljava/math/BigInteger;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/konka/mm/data/OtherData;->setDescription(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_8
    iget-object v6, p0, Lcom/konka/mm/data/sambaDataManager;->context:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f070006    # com.konka.mm.R.array.fileEndingWebText

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v1, v6}, Lcom/konka/mm/data/sambaDataManager;->checkExtension(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_9

    const-string v6, "other"

    invoke-virtual {v2, v6}, Lcom/konka/mm/data/OtherData;->setType(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v6

    invoke-static {v6}, Lcom/konka/mm/tools/Tools;->formatSize(Ljava/math/BigInteger;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/konka/mm/data/OtherData;->setDescription(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_9
    const-string v6, "other"

    invoke-virtual {v2, v6}, Lcom/konka/mm/data/OtherData;->setType(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v6

    invoke-static {v6}, Lcom/konka/mm/tools/Tools;->formatSize(Ljava/math/BigInteger;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/konka/mm/data/OtherData;->setDescription(Ljava/lang/String;)V

    goto/16 :goto_1
.end method


# virtual methods
.method public browseTo(Ljava/io/File;Landroid/content/Context;)Z
    .locals 2
    .param p1    # Ljava/io/File;
    .param p2    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    sget-object v1, Lcom/konka/mm/data/sambaDataManager;->directoryEntries:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    sget-object v1, Lcom/konka/mm/data/sambaDataManager;->folderList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    sget-object v1, Lcom/konka/mm/data/sambaDataManager;->fileList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    iput-object p1, p0, Lcom/konka/mm/data/sambaDataManager;->currentDirectory:Ljava/io/File;

    invoke-virtual {p1}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v1

    array-length v1, v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    array-length v0, v0

    if-lez v0, :cond_2

    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/konka/mm/data/sambaDataManager;->scan([Ljava/io/File;)V

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public browseToRoot(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    sget-object v0, Lcom/konka/mm/data/sambaDataManager;->rootDirectory:Ljava/io/File;

    invoke-virtual {p0, v0, p1}, Lcom/konka/mm/data/sambaDataManager;->browseTo(Ljava/io/File;Landroid/content/Context;)Z

    return-void
.end method

.method public clearAll()V
    .locals 1

    sget-object v0, Lcom/konka/mm/data/sambaDataManager;->directoryEntries:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/konka/mm/data/sambaDataManager;->directoryEntries:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    :cond_0
    return-void
.end method

.method public getCurrentIndex(Ljava/lang/String;ILjava/util/ArrayList;)I
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/konka/mm/data/OtherData;",
            ">;)I"
        }
    .end annotation

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lt v0, v1, :cond_0

    const/4 v1, 0x0

    :goto_1
    return v1

    :cond_0
    invoke-virtual {p3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/konka/mm/data/OtherData;

    invoke-virtual {v1}, Lcom/konka/mm/data/OtherData;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    rem-int v1, v0, p2

    goto :goto_1

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getCurrentPage(Ljava/lang/String;I)I
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # I

    sget-object v1, Lcom/konka/mm/data/sambaDataManager;->directoryEntries:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge p2, v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    sget-object v1, Lcom/konka/mm/data/sambaDataManager;->directoryEntries:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lt v0, v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_1
    return v1

    :cond_1
    sget-object v1, Lcom/konka/mm/data/sambaDataManager;->directoryEntries:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/konka/mm/data/OtherData;

    invoke-virtual {v1}, Lcom/konka/mm/data/OtherData;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    div-int v1, v0, p2

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getDirectoryEntry(I)Lcom/konka/mm/data/OtherData;
    .locals 1
    .param p1    # I

    sget-object v0, Lcom/konka/mm/data/sambaDataManager;->directoryEntries:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/konka/mm/data/sambaDataManager;->directoryEntries:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/konka/mm/data/OtherData;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getRootDirectory()Ljava/io/File;
    .locals 1

    sget-object v0, Lcom/konka/mm/data/sambaDataManager;->rootDirectory:Ljava/io/File;

    return-object v0
.end method

.method public getSambaDataCount()I
    .locals 1

    sget-object v0, Lcom/konka/mm/data/sambaDataManager;->directoryEntries:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/konka/mm/data/sambaDataManager;->directoryEntries:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSambaDataList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/konka/mm/data/OtherData;",
            ">;"
        }
    .end annotation

    sget-object v0, Lcom/konka/mm/data/sambaDataManager;->directoryEntries:Ljava/util/List;

    check-cast v0, Ljava/util/ArrayList;

    return-object v0
.end method
