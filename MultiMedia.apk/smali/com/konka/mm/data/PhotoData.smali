.class public Lcom/konka/mm/data/PhotoData;
.super Ljava/lang/Object;
.source "PhotoData.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/konka/mm/data/PhotoData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private date:J

.field private displayName:Ljava/lang/String;

.field private id:J

.field private mimeType:Ljava/lang/String;

.field private path:Ljava/lang/String;

.field private size:J

.field private title:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/konka/mm/data/PhotoData$1;

    invoke-direct {v0}, Lcom/konka/mm/data/PhotoData$1;-><init>()V

    sput-object v0, Lcom/konka/mm/data/PhotoData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/konka/mm/data/PhotoData;J)V
    .locals 0

    iput-wide p1, p0, Lcom/konka/mm/data/PhotoData;->id:J

    return-void
.end method

.method static synthetic access$1(Lcom/konka/mm/data/PhotoData;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/data/PhotoData;->path:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$2(Lcom/konka/mm/data/PhotoData;J)V
    .locals 0

    iput-wide p1, p0, Lcom/konka/mm/data/PhotoData;->size:J

    return-void
.end method

.method static synthetic access$3(Lcom/konka/mm/data/PhotoData;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/data/PhotoData;->mimeType:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$4(Lcom/konka/mm/data/PhotoData;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/data/PhotoData;->title:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$5(Lcom/konka/mm/data/PhotoData;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/data/PhotoData;->displayName:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$6(Lcom/konka/mm/data/PhotoData;J)V
    .locals 0

    iput-wide p1, p0, Lcom/konka/mm/data/PhotoData;->date:J

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getDate()J
    .locals 2

    iget-wide v0, p0, Lcom/konka/mm/data/PhotoData;->date:J

    return-wide v0
.end method

.method public getDisplayName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/data/PhotoData;->displayName:Ljava/lang/String;

    return-object v0
.end method

.method public getId()J
    .locals 2

    iget-wide v0, p0, Lcom/konka/mm/data/PhotoData;->id:J

    return-wide v0
.end method

.method public getMimeType()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/data/PhotoData;->mimeType:Ljava/lang/String;

    return-object v0
.end method

.method public getPath()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/data/PhotoData;->path:Ljava/lang/String;

    return-object v0
.end method

.method public getSize()J
    .locals 2

    iget-wide v0, p0, Lcom/konka/mm/data/PhotoData;->size:J

    return-wide v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/data/PhotoData;->title:Ljava/lang/String;

    return-object v0
.end method

.method public setDate(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/konka/mm/data/PhotoData;->date:J

    return-void
.end method

.method public setDisplayName(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/mm/data/PhotoData;->displayName:Ljava/lang/String;

    return-void
.end method

.method public setId(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/konka/mm/data/PhotoData;->id:J

    return-void
.end method

.method public setMimeType(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/mm/data/PhotoData;->mimeType:Ljava/lang/String;

    return-void
.end method

.method public setPath(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/mm/data/PhotoData;->path:Ljava/lang/String;

    return-void
.end method

.method public setSize(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/konka/mm/data/PhotoData;->size:J

    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/mm/data/PhotoData;->title:Ljava/lang/String;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[id:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v1, p0, Lcom/konka/mm/data/PhotoData;->id:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "; path:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/mm/data/PhotoData;->path:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "; type:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/mm/data/PhotoData;->mimeType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "; title:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/mm/data/PhotoData;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "; displayName:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/mm/data/PhotoData;->displayName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "; date:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/konka/mm/data/PhotoData;->date:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget-wide v0, p0, Lcom/konka/mm/data/PhotoData;->id:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    iget-object v0, p0, Lcom/konka/mm/data/PhotoData;->path:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-wide v0, p0, Lcom/konka/mm/data/PhotoData;->size:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    iget-object v0, p0, Lcom/konka/mm/data/PhotoData;->mimeType:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/mm/data/PhotoData;->title:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/mm/data/PhotoData;->displayName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-wide v0, p0, Lcom/konka/mm/data/PhotoData;->date:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    return-void
.end method
