.class Lcom/konka/mm/data/PhotoData$1;
.super Ljava/lang/Object;
.source "PhotoData.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/mm/data/PhotoData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/konka/mm/data/PhotoData;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/konka/mm/data/PhotoData;
    .locals 3
    .param p1    # Landroid/os/Parcel;

    new-instance v0, Lcom/konka/mm/data/PhotoData;

    invoke-direct {v0}, Lcom/konka/mm/data/PhotoData;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Lcom/konka/mm/data/PhotoData;->access$0(Lcom/konka/mm/data/PhotoData;J)V

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/konka/mm/data/PhotoData;->access$1(Lcom/konka/mm/data/PhotoData;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Lcom/konka/mm/data/PhotoData;->access$2(Lcom/konka/mm/data/PhotoData;J)V

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/konka/mm/data/PhotoData;->access$3(Lcom/konka/mm/data/PhotoData;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/konka/mm/data/PhotoData;->access$4(Lcom/konka/mm/data/PhotoData;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/konka/mm/data/PhotoData;->access$5(Lcom/konka/mm/data/PhotoData;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Lcom/konka/mm/data/PhotoData;->access$6(Lcom/konka/mm/data/PhotoData;J)V

    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/konka/mm/data/PhotoData$1;->createFromParcel(Landroid/os/Parcel;)Lcom/konka/mm/data/PhotoData;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/konka/mm/data/PhotoData;
    .locals 1
    .param p1    # I

    new-array v0, p1, [Lcom/konka/mm/data/PhotoData;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/konka/mm/data/PhotoData$1;->newArray(I)[Lcom/konka/mm/data/PhotoData;

    move-result-object v0

    return-object v0
.end method
