.class public Lcom/konka/mm/data/HomeShareData;
.super Ljava/lang/Object;
.source "HomeShareData.java"


# instance fields
.field private hostip:Ljava/lang/String;

.field private password:Ljava/lang/String;

.field private usename:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/konka/mm/data/HomeShareData;->hostip:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/konka/mm/data/HomeShareData;->usename:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/konka/mm/data/HomeShareData;->password:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/konka/mm/data/HomeShareData;->hostip:Ljava/lang/String;

    iput-object p2, p0, Lcom/konka/mm/data/HomeShareData;->usename:Ljava/lang/String;

    iput-object p3, p0, Lcom/konka/mm/data/HomeShareData;->password:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getHostip()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/data/HomeShareData;->hostip:Ljava/lang/String;

    return-object v0
.end method

.method public getPassword()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/data/HomeShareData;->password:Ljava/lang/String;

    return-object v0
.end method

.method public getUsename()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/data/HomeShareData;->usename:Ljava/lang/String;

    return-object v0
.end method

.method public setHostip(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/mm/data/HomeShareData;->hostip:Ljava/lang/String;

    return-void
.end method

.method public setPassword(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/mm/data/HomeShareData;->password:Ljava/lang/String;

    return-void
.end method

.method public setUsename(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/mm/data/HomeShareData;->usename:Ljava/lang/String;

    return-void
.end method
