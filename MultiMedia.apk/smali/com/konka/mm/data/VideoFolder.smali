.class public Lcom/konka/mm/data/VideoFolder;
.super Ljava/lang/Object;
.source "VideoFolder.java"


# instance fields
.field private bucketId:Ljava/lang/String;

.field private name:Ljava/lang/String;

.field private path:Ljava/lang/String;

.field private videoDataList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/konka/mm/data/VideoData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/konka/mm/data/VideoFolder;->videoDataList:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public addVideoData(Lcom/konka/mm/data/VideoData;)V
    .locals 1
    .param p1    # Lcom/konka/mm/data/VideoData;

    iget-object v0, p0, Lcom/konka/mm/data/VideoFolder;->videoDataList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public getAllVideoDataList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/konka/mm/data/VideoData;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/konka/mm/data/VideoFolder;->videoDataList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getBucketId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/data/VideoFolder;->bucketId:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/data/VideoFolder;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getPath()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/data/VideoFolder;->path:Ljava/lang/String;

    return-object v0
.end method

.method public getVideoDataCount()I
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/data/VideoFolder;->videoDataList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/mm/data/VideoFolder;->videoDataList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getVideoDataList(II)Ljava/util/ArrayList;
    .locals 4
    .param p1    # I
    .param p2    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/konka/mm/data/VideoData;",
            ">;"
        }
    .end annotation

    add-int/lit8 v3, p2, -0x1

    mul-int v0, p1, v3

    const/4 v1, 0x0

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    :goto_0
    iget-object v3, p0, Lcom/konka/mm/data/VideoFolder;->videoDataList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v0, v3, :cond_0

    if-lt v1, p1, :cond_1

    :cond_0
    return-object v2

    :cond_1
    iget-object v3, p0, Lcom/konka/mm/data/VideoFolder;->videoDataList:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/konka/mm/data/VideoData;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public setBucketId(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/mm/data/VideoFolder;->bucketId:Ljava/lang/String;

    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/mm/data/VideoFolder;->name:Ljava/lang/String;

    return-void
.end method

.method public setPath(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/mm/data/VideoFolder;->path:Ljava/lang/String;

    return-void
.end method
