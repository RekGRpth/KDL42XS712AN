.class public Lcom/konka/mm/data/AudioManager;
.super Ljava/lang/Object;
.source "AudioManager.java"


# static fields
.field private static audioDataList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/konka/mm/data/AudioData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/konka/mm/data/AudioManager;->audioDataList:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/konka/mm/data/AudioManager;->audioDataList:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/konka/mm/data/AudioManager;->audioDataList:Ljava/util/ArrayList;

    :cond_0
    return-void
.end method


# virtual methods
.method public addAudioData(Lcom/konka/mm/data/AudioData;)V
    .locals 1
    .param p1    # Lcom/konka/mm/data/AudioData;

    sget-object v0, Lcom/konka/mm/data/AudioManager;->audioDataList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/konka/mm/data/AudioManager;->audioDataList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/konka/mm/data/AudioManager;->audioDataList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public clearAll()V
    .locals 1

    sget-object v0, Lcom/konka/mm/data/AudioManager;->audioDataList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/konka/mm/data/AudioManager;->audioDataList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    :cond_0
    return-void
.end method

.method public getAllAudioDataList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/konka/mm/data/AudioData;",
            ">;"
        }
    .end annotation

    sget-object v0, Lcom/konka/mm/data/AudioManager;->audioDataList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getAudioData(I)Lcom/konka/mm/data/AudioData;
    .locals 1
    .param p1    # I

    sget-object v0, Lcom/konka/mm/data/AudioManager;->audioDataList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/konka/mm/data/AudioManager;->audioDataList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/konka/mm/data/AudioData;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getAudioDataCount()I
    .locals 1

    sget-object v0, Lcom/konka/mm/data/AudioManager;->audioDataList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/konka/mm/data/AudioManager;->audioDataList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getAudioDataList(II)Ljava/util/ArrayList;
    .locals 4
    .param p1    # I
    .param p2    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/konka/mm/data/AudioData;",
            ">;"
        }
    .end annotation

    add-int/lit8 v3, p2, -0x1

    mul-int v0, p1, v3

    const/4 v1, 0x0

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    :goto_0
    sget-object v3, Lcom/konka/mm/data/AudioManager;->audioDataList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v0, v3, :cond_0

    if-lt v1, p1, :cond_1

    :cond_0
    return-object v2

    :cond_1
    sget-object v3, Lcom/konka/mm/data/AudioManager;->audioDataList:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/konka/mm/data/AudioData;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
