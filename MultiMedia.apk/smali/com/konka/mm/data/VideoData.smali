.class public Lcom/konka/mm/data/VideoData;
.super Ljava/lang/Object;
.source "VideoData.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/konka/mm/data/VideoData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private _duration:J

.field private dateTaken:J

.field private displayName:Ljava/lang/String;

.field private duration:Ljava/lang/String;

.field private id:J

.field private mimeType:Ljava/lang/String;

.field private miniThumbMagic:J

.field private modifyTime:J

.field private path:Ljava/lang/String;

.field private resolution:Ljava/lang/String;

.field private size:Ljava/lang/String;

.field private thumbnailPath:Ljava/lang/String;

.field private title:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/konka/mm/data/VideoData$1;

    invoke-direct {v0}, Lcom/konka/mm/data/VideoData$1;-><init>()V

    sput-object v0, Lcom/konka/mm/data/VideoData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/konka/mm/data/VideoData;->modifyTime:J

    return-void
.end method

.method static synthetic access$0(Lcom/konka/mm/data/VideoData;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/data/VideoData;->path:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$1(Lcom/konka/mm/data/VideoData;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/data/VideoData;->size:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$2(Lcom/konka/mm/data/VideoData;J)V
    .locals 0

    iput-wide p1, p0, Lcom/konka/mm/data/VideoData;->_duration:J

    return-void
.end method

.method static synthetic access$3(Lcom/konka/mm/data/VideoData;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/data/VideoData;->duration:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$4(Lcom/konka/mm/data/VideoData;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/data/VideoData;->title:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$5(Lcom/konka/mm/data/VideoData;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/data/VideoData;->displayName:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$6(Lcom/konka/mm/data/VideoData;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/data/VideoData;->resolution:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getDateTaken()J
    .locals 2

    iget-wide v0, p0, Lcom/konka/mm/data/VideoData;->dateTaken:J

    return-wide v0
.end method

.method public getDisplayName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/data/VideoData;->displayName:Ljava/lang/String;

    return-object v0
.end method

.method public getDuration()J
    .locals 2

    iget-wide v0, p0, Lcom/konka/mm/data/VideoData;->_duration:J

    return-wide v0
.end method

.method public getDuration2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/data/VideoData;->duration:Ljava/lang/String;

    return-object v0
.end method

.method public getId()J
    .locals 2

    iget-wide v0, p0, Lcom/konka/mm/data/VideoData;->id:J

    return-wide v0
.end method

.method public getMimeType()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/data/VideoData;->mimeType:Ljava/lang/String;

    return-object v0
.end method

.method public getMiniThumbMagic()J
    .locals 2

    iget-wide v0, p0, Lcom/konka/mm/data/VideoData;->miniThumbMagic:J

    return-wide v0
.end method

.method public getModifyTime()J
    .locals 2

    iget-wide v0, p0, Lcom/konka/mm/data/VideoData;->modifyTime:J

    return-wide v0
.end method

.method public getPath()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/data/VideoData;->path:Ljava/lang/String;

    return-object v0
.end method

.method public getResolution()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/data/VideoData;->resolution:Ljava/lang/String;

    return-object v0
.end method

.method public getSize()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/data/VideoData;->size:Ljava/lang/String;

    return-object v0
.end method

.method public getThumbnailPath()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/data/VideoData;->thumbnailPath:Ljava/lang/String;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/data/VideoData;->title:Ljava/lang/String;

    return-object v0
.end method

.method public setDateTaken(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/konka/mm/data/VideoData;->dateTaken:J

    return-void
.end method

.method public setDisplayName(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/mm/data/VideoData;->displayName:Ljava/lang/String;

    return-void
.end method

.method public setDuration(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/konka/mm/data/VideoData;->_duration:J

    return-void
.end method

.method public setId(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/konka/mm/data/VideoData;->id:J

    return-void
.end method

.method public setMimeType(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/mm/data/VideoData;->mimeType:Ljava/lang/String;

    return-void
.end method

.method public setMiniThumbMagic(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/konka/mm/data/VideoData;->miniThumbMagic:J

    return-void
.end method

.method public setModifyTime(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/konka/mm/data/VideoData;->modifyTime:J

    return-void
.end method

.method public setPath(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/mm/data/VideoData;->path:Ljava/lang/String;

    return-void
.end method

.method public setResolution(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/mm/data/VideoData;->resolution:Ljava/lang/String;

    return-void
.end method

.method public setSize(J)V
    .locals 0
    .param p1    # J

    return-void
.end method

.method public setThumbnailPath(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/mm/data/VideoData;->thumbnailPath:Ljava/lang/String;

    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/mm/data/VideoData;->title:Ljava/lang/String;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[id:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v1, p0, Lcom/konka/mm/data/VideoData;->id:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "; title:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/mm/data/VideoData;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "; duration:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/mm/data/VideoData;->duration:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "; displayName:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/mm/data/VideoData;->displayName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "; path:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/mm/data/VideoData;->path:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ;size:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/mm/data/VideoData;->size:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget-object v0, p0, Lcom/konka/mm/data/VideoData;->path:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/mm/data/VideoData;->size:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-wide v0, p0, Lcom/konka/mm/data/VideoData;->_duration:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    iget-object v0, p0, Lcom/konka/mm/data/VideoData;->duration:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/mm/data/VideoData;->title:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/mm/data/VideoData;->displayName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/mm/data/VideoData;->resolution:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method
