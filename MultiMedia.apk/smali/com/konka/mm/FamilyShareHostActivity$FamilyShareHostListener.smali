.class Lcom/konka/mm/FamilyShareHostActivity$FamilyShareHostListener;
.super Ljava/lang/Object;
.source "FamilyShareHostActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/mm/FamilyShareHostActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "FamilyShareHostListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/mm/FamilyShareHostActivity;


# direct methods
.method constructor <init>(Lcom/konka/mm/FamilyShareHostActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/FamilyShareHostActivity$FamilyShareHostListener;->this$0:Lcom/konka/mm/FamilyShareHostActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/konka/mm/FamilyShareHostActivity$FamilyShareHostListener;->this$0:Lcom/konka/mm/FamilyShareHostActivity;

    # getter for: Lcom/konka/mm/FamilyShareHostActivity;->HostItem1:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/konka/mm/FamilyShareHostActivity;->access$0(Lcom/konka/mm/FamilyShareHostActivity;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/konka/mm/finals/CommonFinals;->MM_LOG_TAG:Ljava/lang/String;

    const-string v1, "click HostItem1"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/konka/mm/FamilyShareHostActivity$FamilyShareHostListener;->this$0:Lcom/konka/mm/FamilyShareHostActivity;

    # getter for: Lcom/konka/mm/FamilyShareHostActivity;->HostItem2:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/konka/mm/FamilyShareHostActivity;->access$1(Lcom/konka/mm/FamilyShareHostActivity;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/konka/mm/finals/CommonFinals;->MM_LOG_TAG:Ljava/lang/String;

    const-string v1, "click HostItem2"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/konka/mm/FamilyShareHostActivity$FamilyShareHostListener;->this$0:Lcom/konka/mm/FamilyShareHostActivity;

    # getter for: Lcom/konka/mm/FamilyShareHostActivity;->HostItem3:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/konka/mm/FamilyShareHostActivity;->access$2(Lcom/konka/mm/FamilyShareHostActivity;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/konka/mm/finals/CommonFinals;->MM_LOG_TAG:Ljava/lang/String;

    const-string v1, "click HostItem3"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/konka/mm/FamilyShareHostActivity$FamilyShareHostListener;->this$0:Lcom/konka/mm/FamilyShareHostActivity;

    # getter for: Lcom/konka/mm/FamilyShareHostActivity;->HostItem4:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/konka/mm/FamilyShareHostActivity;->access$3(Lcom/konka/mm/FamilyShareHostActivity;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Lcom/konka/mm/finals/CommonFinals;->MM_LOG_TAG:Ljava/lang/String;

    const-string v1, "click HostItem4"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/konka/mm/FamilyShareHostActivity$FamilyShareHostListener;->this$0:Lcom/konka/mm/FamilyShareHostActivity;

    # getter for: Lcom/konka/mm/FamilyShareHostActivity;->HostItem5:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/konka/mm/FamilyShareHostActivity;->access$4(Lcom/konka/mm/FamilyShareHostActivity;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    sget-object v0, Lcom/konka/mm/finals/CommonFinals;->MM_LOG_TAG:Ljava/lang/String;

    const-string v1, "click HostItem5"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_4
    sget-object v0, Lcom/konka/mm/finals/CommonFinals;->MM_LOG_TAG:Ljava/lang/String;

    const-string v1, "unknown host clicked!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
