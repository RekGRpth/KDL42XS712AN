.class public Lcom/konka/mm/model/MenuData;
.super Ljava/lang/Object;
.source "MenuData.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/mm/model/MenuData$MenuDataOneScreen;,
        Lcom/konka/mm/model/MenuData$ThumbnailItem;
    }
.end annotation


# static fields
.field public static final NUM_IN_ONE_SCREEN:I = 0x6


# instance fields
.field mScreens:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/konka/mm/model/MenuData$MenuDataOneScreen;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/konka/mm/model/MenuData;->mScreens:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public getScreenByIndex(I)Lcom/konka/mm/model/MenuData$MenuDataOneScreen;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/konka/mm/model/MenuData;->mScreens:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/konka/mm/model/MenuData$MenuDataOneScreen;

    return-object v0
.end method

.method public getScreenNum()I
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/model/MenuData;->mScreens:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public setThumbnailItems(Ljava/util/ArrayList;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/konka/mm/model/MenuData$ThumbnailItem;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v6

    div-int/lit8 v5, v6, 0x6

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v6

    rem-int/lit8 v3, v6, 0x6

    if-nez v3, :cond_0

    const/4 v6, 0x0

    :goto_0
    add-int/2addr v5, v6

    const/4 v2, 0x0

    const/4 v0, 0x0

    :goto_1
    if-lt v0, v5, :cond_1

    return-void

    :cond_0
    const/4 v6, 0x1

    goto :goto_0

    :cond_1
    new-instance v4, Lcom/konka/mm/model/MenuData$MenuDataOneScreen;

    invoke-direct {v4}, Lcom/konka/mm/model/MenuData$MenuDataOneScreen;-><init>()V

    const/4 v1, 0x0

    :goto_2
    const/4 v6, 0x6

    if-lt v1, v6, :cond_2

    iget-object v6, p0, Lcom/konka/mm/model/MenuData;->mScreens:Ljava/util/ArrayList;

    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    if-ge v2, v6, :cond_3

    iget-object v7, v4, Lcom/konka/mm/model/MenuData$MenuDataOneScreen;->mThumbnails:Ljava/util/ArrayList;

    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/konka/mm/model/MenuData$ThumbnailItem;

    invoke-virtual {v7, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2
.end method
