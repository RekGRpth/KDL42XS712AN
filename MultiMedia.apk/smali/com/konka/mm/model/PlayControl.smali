.class public Lcom/konka/mm/model/PlayControl;
.super Ljava/lang/Object;
.source "PlayControl.java"


# static fields
.field private static synthetic $SWITCH_TABLE$com$konka$mm$finals$CommonFinals$MMPlayControlType:[I

.field private static synthetic $SWITCH_TABLE$com$konka$mm$finals$CommonFinals$MMSourceType:[I

.field private static MMPlayer:Landroid/media/MediaPlayer;


# direct methods
.method static synthetic $SWITCH_TABLE$com$konka$mm$finals$CommonFinals$MMPlayControlType()[I
    .locals 3

    sget-object v0, Lcom/konka/mm/model/PlayControl;->$SWITCH_TABLE$com$konka$mm$finals$CommonFinals$MMPlayControlType:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/konka/mm/finals/CommonFinals$MMPlayControlType;->values()[Lcom/konka/mm/finals/CommonFinals$MMPlayControlType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/konka/mm/finals/CommonFinals$MMPlayControlType;->KONKA_MM_PLAY_CONTROL_FB:Lcom/konka/mm/finals/CommonFinals$MMPlayControlType;

    invoke-virtual {v1}, Lcom/konka/mm/finals/CommonFinals$MMPlayControlType;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_6

    :goto_1
    :try_start_1
    sget-object v1, Lcom/konka/mm/finals/CommonFinals$MMPlayControlType;->KONKA_MM_PLAY_CONTROL_FF:Lcom/konka/mm/finals/CommonFinals$MMPlayControlType;

    invoke-virtual {v1}, Lcom/konka/mm/finals/CommonFinals$MMPlayControlType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_5

    :goto_2
    :try_start_2
    sget-object v1, Lcom/konka/mm/finals/CommonFinals$MMPlayControlType;->KONKA_MM_PLAY_CONTROL_PAUSE:Lcom/konka/mm/finals/CommonFinals$MMPlayControlType;

    invoke-virtual {v1}, Lcom/konka/mm/finals/CommonFinals$MMPlayControlType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_4

    :goto_3
    :try_start_3
    sget-object v1, Lcom/konka/mm/finals/CommonFinals$MMPlayControlType;->KONKA_MM_PLAY_CONTROL_RESUME:Lcom/konka/mm/finals/CommonFinals$MMPlayControlType;

    invoke-virtual {v1}, Lcom/konka/mm/finals/CommonFinals$MMPlayControlType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_3

    :goto_4
    :try_start_4
    sget-object v1, Lcom/konka/mm/finals/CommonFinals$MMPlayControlType;->KONKA_MM_PLAY_CONTROL_SEEK:Lcom/konka/mm/finals/CommonFinals$MMPlayControlType;

    invoke-virtual {v1}, Lcom/konka/mm/finals/CommonFinals$MMPlayControlType;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_2

    :goto_5
    :try_start_5
    sget-object v1, Lcom/konka/mm/finals/CommonFinals$MMPlayControlType;->KONKA_MM_PLAY_CONTROL_STOP:Lcom/konka/mm/finals/CommonFinals$MMPlayControlType;

    invoke-virtual {v1}, Lcom/konka/mm/finals/CommonFinals$MMPlayControlType;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_1

    :goto_6
    :try_start_6
    sget-object v1, Lcom/konka/mm/finals/CommonFinals$MMPlayControlType;->KONKA_MM_PLAY_CONTROL_UNKNOWN:Lcom/konka/mm/finals/CommonFinals$MMPlayControlType;

    invoke-virtual {v1}, Lcom/konka/mm/finals/CommonFinals$MMPlayControlType;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_0

    :goto_7
    sput-object v0, Lcom/konka/mm/model/PlayControl;->$SWITCH_TABLE$com$konka$mm$finals$CommonFinals$MMPlayControlType:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_7

    :catch_1
    move-exception v1

    goto :goto_6

    :catch_2
    move-exception v1

    goto :goto_5

    :catch_3
    move-exception v1

    goto :goto_4

    :catch_4
    move-exception v1

    goto :goto_3

    :catch_5
    move-exception v1

    goto :goto_2

    :catch_6
    move-exception v1

    goto :goto_1
.end method

.method static synthetic $SWITCH_TABLE$com$konka$mm$finals$CommonFinals$MMSourceType()[I
    .locals 3

    sget-object v0, Lcom/konka/mm/model/PlayControl;->$SWITCH_TABLE$com$konka$mm$finals$CommonFinals$MMSourceType:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/konka/mm/finals/CommonFinals$MMSourceType;->values()[Lcom/konka/mm/finals/CommonFinals$MMSourceType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/konka/mm/finals/CommonFinals$MMSourceType;->KONKA_MM_SOURCE_APK:Lcom/konka/mm/finals/CommonFinals$MMSourceType;

    invoke-virtual {v1}, Lcom/konka/mm/finals/CommonFinals$MMSourceType;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_5

    :goto_1
    :try_start_1
    sget-object v1, Lcom/konka/mm/finals/CommonFinals$MMSourceType;->KONKA_MM_SOURCE_AUDIO:Lcom/konka/mm/finals/CommonFinals$MMSourceType;

    invoke-virtual {v1}, Lcom/konka/mm/finals/CommonFinals$MMSourceType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_4

    :goto_2
    :try_start_2
    sget-object v1, Lcom/konka/mm/finals/CommonFinals$MMSourceType;->KONKA_MM_SOURCE_IMAGE:Lcom/konka/mm/finals/CommonFinals$MMSourceType;

    invoke-virtual {v1}, Lcom/konka/mm/finals/CommonFinals$MMSourceType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_3

    :goto_3
    :try_start_3
    sget-object v1, Lcom/konka/mm/finals/CommonFinals$MMSourceType;->KONKA_MM_SOURCE_TXT:Lcom/konka/mm/finals/CommonFinals$MMSourceType;

    invoke-virtual {v1}, Lcom/konka/mm/finals/CommonFinals$MMSourceType;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_2

    :goto_4
    :try_start_4
    sget-object v1, Lcom/konka/mm/finals/CommonFinals$MMSourceType;->KONKA_MM_SOURCE_UNKNOWN:Lcom/konka/mm/finals/CommonFinals$MMSourceType;

    invoke-virtual {v1}, Lcom/konka/mm/finals/CommonFinals$MMSourceType;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_1

    :goto_5
    :try_start_5
    sget-object v1, Lcom/konka/mm/finals/CommonFinals$MMSourceType;->KONKA_MM_SOURCE_VIDEO:Lcom/konka/mm/finals/CommonFinals$MMSourceType;

    invoke-virtual {v1}, Lcom/konka/mm/finals/CommonFinals$MMSourceType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_0

    :goto_6
    sput-object v0, Lcom/konka/mm/model/PlayControl;->$SWITCH_TABLE$com$konka$mm$finals$CommonFinals$MMSourceType:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_6

    :catch_1
    move-exception v1

    goto :goto_5

    :catch_2
    move-exception v1

    goto :goto_4

    :catch_3
    move-exception v1

    goto :goto_3

    :catch_4
    move-exception v1

    goto :goto_2

    :catch_5
    move-exception v1

    goto :goto_1
.end method

.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Landroid/media/MediaPlayer;

    invoke-direct {v0}, Landroid/media/MediaPlayer;-><init>()V

    sput-object v0, Lcom/konka/mm/model/PlayControl;->MMPlayer:Landroid/media/MediaPlayer;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static Control(Lcom/konka/mm/finals/CommonFinals$MMPlayControlType;I)V
    .locals 2
    .param p0    # Lcom/konka/mm/finals/CommonFinals$MMPlayControlType;
    .param p1    # I

    invoke-static {}, Lcom/konka/mm/model/PlayControl;->$SWITCH_TABLE$com$konka$mm$finals$CommonFinals$MMPlayControlType()[I

    move-result-object v0

    invoke-virtual {p0}, Lcom/konka/mm/finals/CommonFinals$MMPlayControlType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    const-string v0, "PlayControl"

    const-string v1, "Not the type of support"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :pswitch_0
    sget-object v0, Lcom/konka/mm/model/PlayControl;->MMPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    goto :goto_0

    :pswitch_1
    sget-object v0, Lcom/konka/mm/model/PlayControl;->MMPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->pause()V

    goto :goto_0

    :pswitch_2
    sget-object v0, Lcom/konka/mm/model/PlayControl;->MMPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p1}, Landroid/media/MediaPlayer;->seekTo(I)V

    goto :goto_0

    :pswitch_3
    sget-object v0, Lcom/konka/mm/model/PlayControl;->MMPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    goto :goto_0

    :pswitch_4
    const-string v0, "PlayControl"

    const-string v1, "\u03b4\u02b5\ufffd\ufffd"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_4
        :pswitch_4
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static Play(Ljava/lang/String;Lcom/konka/mm/finals/CommonFinals$MMSourceType;)V
    .locals 5
    .param p0    # Ljava/lang/String;
    .param p1    # Lcom/konka/mm/finals/CommonFinals$MMSourceType;

    :try_start_0
    sget-object v1, Lcom/konka/mm/model/PlayControl;->MMPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1, p0}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    :goto_0
    invoke-static {}, Lcom/konka/mm/model/PlayControl;->$SWITCH_TABLE$com$konka$mm$finals$CommonFinals$MMSourceType()[I

    move-result-object v1

    invoke-virtual {p1}, Lcom/konka/mm/finals/CommonFinals$MMSourceType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    const-string v1, "PlayControl"

    const-string v2, "Not the type of support!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    const-string v1, "LOG"

    const-string v2, "1111111111111111111111111111111111111"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_1
    sget-object v1, Lcom/konka/mm/model/PlayControl;->MMPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->prepare()V
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4

    :goto_2
    const-string v1, "LOG"

    const-string v2, "222222222222222222222222222222222222"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v1, Lcom/konka/mm/model/PlayControl;->MMPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->start()V

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    :pswitch_1
    sget-object v1, Lcom/konka/mm/model/PlayControl;->MMPlayer:Landroid/media/MediaPlayer;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    goto :goto_1

    :pswitch_2
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "Add to-do here to handle source type %d"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-virtual {v1, v2, v3}, Ljava/io/PrintStream;->printf(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintStream;

    goto :goto_1

    :catch_3
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_2

    :catch_4
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
