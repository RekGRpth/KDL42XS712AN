.class public Lcom/konka/mm/model/MiscModel;
.super Ljava/lang/Object;
.source "MiscModel.java"


# static fields
.field private static CurDensity:F

.field private static CurLanguage:Ljava/util/Locale;

.field private static eCurResolution:Lcom/konka/mm/finals/CommonFinals$MMResolution;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/high16 v0, 0x3f800000    # 1.0f

    sput v0, Lcom/konka/mm/model/MiscModel;->CurDensity:F

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static dip2px(F)I
    .locals 2
    .param p0    # F

    sget v0, Lcom/konka/mm/model/MiscModel;->CurDensity:F

    mul-float/2addr v0, p0

    const/high16 v1, 0x3f000000    # 0.5f

    add-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method public static getLanguage()Ljava/util/Locale;
    .locals 1

    sget-object v0, Lcom/konka/mm/model/MiscModel;->CurLanguage:Ljava/util/Locale;

    return-object v0
.end method

.method public static getResolution()Lcom/konka/mm/finals/CommonFinals$MMResolution;
    .locals 1

    sget-object v0, Lcom/konka/mm/model/MiscModel;->eCurResolution:Lcom/konka/mm/finals/CommonFinals$MMResolution;

    return-object v0
.end method

.method public static px2dip(F)I
    .locals 2
    .param p0    # F

    sget v0, Lcom/konka/mm/model/MiscModel;->CurDensity:F

    div-float v0, p0, v0

    const/high16 v1, 0x3f000000    # 0.5f

    add-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method public static px2sp(F)I
    .locals 1
    .param p0    # F

    invoke-static {p0}, Lcom/konka/mm/model/MiscModel;->px2dip(F)I

    move-result v0

    return v0
.end method

.method public static setLanguage(Ljava/util/Locale;)V
    .locals 0
    .param p0    # Ljava/util/Locale;

    sput-object p0, Lcom/konka/mm/model/MiscModel;->CurLanguage:Ljava/util/Locale;

    return-void
.end method

.method public static setResolution(Landroid/util/DisplayMetrics;)V
    .locals 2
    .param p0    # Landroid/util/DisplayMetrics;

    iget v0, p0, Landroid/util/DisplayMetrics;->density:F

    sput v0, Lcom/konka/mm/model/MiscModel;->CurDensity:F

    iget v0, p0, Landroid/util/DisplayMetrics;->widthPixels:I

    const/16 v1, 0x780

    if-ne v0, v1, :cond_0

    iget v0, p0, Landroid/util/DisplayMetrics;->heightPixels:I

    const/16 v1, 0x438

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/konka/mm/finals/CommonFinals$MMResolution;->KONKA_MM_RESOLUTION_1920x1080:Lcom/konka/mm/finals/CommonFinals$MMResolution;

    sput-object v0, Lcom/konka/mm/model/MiscModel;->eCurResolution:Lcom/konka/mm/finals/CommonFinals$MMResolution;

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Landroid/util/DisplayMetrics;->widthPixels:I

    const/16 v1, 0x500

    if-ne v0, v1, :cond_1

    iget v0, p0, Landroid/util/DisplayMetrics;->heightPixels:I

    const/16 v1, 0x2d0

    if-ne v0, v1, :cond_1

    sget-object v0, Lcom/konka/mm/finals/CommonFinals$MMResolution;->KONKA_MM_RESOLUTION_1280x720:Lcom/konka/mm/finals/CommonFinals$MMResolution;

    sput-object v0, Lcom/konka/mm/model/MiscModel;->eCurResolution:Lcom/konka/mm/finals/CommonFinals$MMResolution;

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/konka/mm/finals/CommonFinals;->MM_LOG_TAG:Ljava/lang/String;

    const-string v1, "unknown resolution,maybe wrong!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/konka/mm/finals/CommonFinals$MMResolution;->KONKA_MM_RESOLUTION_UNKNOWN:Lcom/konka/mm/finals/CommonFinals$MMResolution;

    sput-object v0, Lcom/konka/mm/model/MiscModel;->eCurResolution:Lcom/konka/mm/finals/CommonFinals$MMResolution;

    goto :goto_0
.end method
