.class public Lcom/konka/mm/model/MediaPlayer;
.super Ljava/lang/Object;
.source "MediaPlayer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/mm/model/MediaPlayer$EventHandler;,
        Lcom/konka/mm/model/MediaPlayer$OnBufferingUpdateListener;,
        Lcom/konka/mm/model/MediaPlayer$OnCompletionListener;,
        Lcom/konka/mm/model/MediaPlayer$OnErrorListener;,
        Lcom/konka/mm/model/MediaPlayer$OnInfoListener;,
        Lcom/konka/mm/model/MediaPlayer$OnPreparedListener;,
        Lcom/konka/mm/model/MediaPlayer$OnSeekCompleteListener;,
        Lcom/konka/mm/model/MediaPlayer$OnVideoSizeChangedListener;
    }
.end annotation


# static fields
.field public static final APPLY_METADATA_FILTER:Z = true

.field public static final BYPASS_METADATA_FILTER:Z = false

.field private static final IMEDIA_PLAYER:Ljava/lang/String; = "android.media.IMediaPlayer"

.field private static final MEDIA_BUFFERING_UPDATE:I = 0x3

.field private static final MEDIA_ERROR:I = 0x64

.field public static final MEDIA_ERROR_NOT_VALID_FOR_PROGRESSIVE_PLAYBACK:I = 0xc8

.field public static final MEDIA_ERROR_SERVER_DIED:I = 0x64

.field public static final MEDIA_ERROR_UNKNOWN:I = 0x1

.field private static final MEDIA_INFO:I = 0xc8

.field public static final MEDIA_INFO_BAD_INTERLEAVING:I = 0x320

.field public static final MEDIA_INFO_BUFFERING_END:I = 0x2be

.field public static final MEDIA_INFO_BUFFERING_START:I = 0x2bd

.field public static final MEDIA_INFO_METADATA_UPDATE:I = 0x322

.field public static final MEDIA_INFO_NOT_SEEKABLE:I = 0x321

.field public static final MEDIA_INFO_UNKNOWN:I = 0x1

.field public static final MEDIA_INFO_VIDEO_TRACK_LAGGING:I = 0x2bc

.field private static final MEDIA_NOP:I = 0x0

.field private static final MEDIA_PLAYBACK_COMPLETE:I = 0x2

.field private static final MEDIA_PREPARED:I = 0x1

.field private static final MEDIA_SEEK_COMPLETE:I = 0x4

.field private static final MEDIA_SET_VIDEO_SIZE:I = 0x5

.field public static final METADATA_ALL:Z = false

.field public static final METADATA_UPDATE_ONLY:Z = true

.field private static final TAG:Ljava/lang/String; = "MediaPlayer"


# instance fields
.field private mEventHandler:Lcom/konka/mm/model/MediaPlayer$EventHandler;

.field private mListenerContext:I

.field private mNativeContext:I

.field private mOnBufferingUpdateListener:Lcom/konka/mm/model/MediaPlayer$OnBufferingUpdateListener;

.field private mOnCompletionListener:Lcom/konka/mm/model/MediaPlayer$OnCompletionListener;

.field private mOnErrorListener:Lcom/konka/mm/model/MediaPlayer$OnErrorListener;

.field private mOnInfoListener:Lcom/konka/mm/model/MediaPlayer$OnInfoListener;

.field private mOnPreparedListener:Lcom/konka/mm/model/MediaPlayer$OnPreparedListener;

.field private mOnSeekCompleteListener:Lcom/konka/mm/model/MediaPlayer$OnSeekCompleteListener;

.field private mOnVideoSizeChangedListener:Lcom/konka/mm/model/MediaPlayer$OnVideoSizeChangedListener;

.field private mScreenOnWhilePlaying:Z

.field private mStayAwake:Z

.field private mSurface:Landroid/view/Surface;

.field private mSurfaceHolder:Landroid/view/SurfaceHolder;

.field private mWakeLock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "media_jni"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    invoke-static {}, Lcom/konka/mm/model/MediaPlayer;->native_init()V

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/konka/mm/model/MediaPlayer;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Lcom/konka/mm/model/MediaPlayer$EventHandler;

    invoke-direct {v1, p0, p0, v0}, Lcom/konka/mm/model/MediaPlayer$EventHandler;-><init>(Lcom/konka/mm/model/MediaPlayer;Lcom/konka/mm/model/MediaPlayer;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/konka/mm/model/MediaPlayer;->mEventHandler:Lcom/konka/mm/model/MediaPlayer$EventHandler;

    :goto_0
    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-direct {p0, v1}, Lcom/konka/mm/model/MediaPlayer;->native_setup(Ljava/lang/Object;)V

    return-void

    :cond_0
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    if-eqz v0, :cond_1

    new-instance v1, Lcom/konka/mm/model/MediaPlayer$EventHandler;

    invoke-direct {v1, p0, p0, v0}, Lcom/konka/mm/model/MediaPlayer$EventHandler;-><init>(Lcom/konka/mm/model/MediaPlayer;Lcom/konka/mm/model/MediaPlayer;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/konka/mm/model/MediaPlayer;->mEventHandler:Lcom/konka/mm/model/MediaPlayer$EventHandler;

    goto :goto_0

    :cond_1
    iput-object v1, p0, Lcom/konka/mm/model/MediaPlayer;->mEventHandler:Lcom/konka/mm/model/MediaPlayer$EventHandler;

    goto :goto_0
.end method

.method private native _pause()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method private native _release()V
.end method

.method private native _reset()V
.end method

.method private native _setSubtitleSurface()V
.end method

.method private native _setVideoSurface()V
.end method

.method private native _start()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method private native _stop()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method static synthetic access$0(Lcom/konka/mm/model/MediaPlayer;)I
    .locals 1

    iget v0, p0, Lcom/konka/mm/model/MediaPlayer;->mNativeContext:I

    return v0
.end method

.method static synthetic access$1(Lcom/konka/mm/model/MediaPlayer;)Lcom/konka/mm/model/MediaPlayer$OnPreparedListener;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/model/MediaPlayer;->mOnPreparedListener:Lcom/konka/mm/model/MediaPlayer$OnPreparedListener;

    return-object v0
.end method

.method static synthetic access$2(Lcom/konka/mm/model/MediaPlayer;)Lcom/konka/mm/model/MediaPlayer$OnCompletionListener;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/model/MediaPlayer;->mOnCompletionListener:Lcom/konka/mm/model/MediaPlayer$OnCompletionListener;

    return-object v0
.end method

.method static synthetic access$3(Lcom/konka/mm/model/MediaPlayer;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/mm/model/MediaPlayer;->stayAwake(Z)V

    return-void
.end method

.method static synthetic access$4(Lcom/konka/mm/model/MediaPlayer;)Lcom/konka/mm/model/MediaPlayer$OnBufferingUpdateListener;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/model/MediaPlayer;->mOnBufferingUpdateListener:Lcom/konka/mm/model/MediaPlayer$OnBufferingUpdateListener;

    return-object v0
.end method

.method static synthetic access$5(Lcom/konka/mm/model/MediaPlayer;)Lcom/konka/mm/model/MediaPlayer$OnSeekCompleteListener;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/model/MediaPlayer;->mOnSeekCompleteListener:Lcom/konka/mm/model/MediaPlayer$OnSeekCompleteListener;

    return-object v0
.end method

.method static synthetic access$6(Lcom/konka/mm/model/MediaPlayer;)Lcom/konka/mm/model/MediaPlayer$OnVideoSizeChangedListener;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/model/MediaPlayer;->mOnVideoSizeChangedListener:Lcom/konka/mm/model/MediaPlayer$OnVideoSizeChangedListener;

    return-object v0
.end method

.method static synthetic access$7(Lcom/konka/mm/model/MediaPlayer;)Lcom/konka/mm/model/MediaPlayer$OnErrorListener;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/model/MediaPlayer;->mOnErrorListener:Lcom/konka/mm/model/MediaPlayer$OnErrorListener;

    return-object v0
.end method

.method static synthetic access$8(Lcom/konka/mm/model/MediaPlayer;)Lcom/konka/mm/model/MediaPlayer$OnInfoListener;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/model/MediaPlayer;->mOnInfoListener:Lcom/konka/mm/model/MediaPlayer$OnInfoListener;

    return-object v0
.end method

.method public static create(Landroid/content/Context;I)Lcom/konka/mm/model/MediaPlayer;
    .locals 9
    .param p0    # Landroid/content/Context;
    .param p1    # I

    const/4 v8, 0x0

    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->openRawResourceFd(I)Landroid/content/res/AssetFileDescriptor;

    move-result-object v6

    if-nez v6, :cond_0

    move-object v0, v8

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/konka/mm/model/MediaPlayer;

    invoke-direct {v0}, Lcom/konka/mm/model/MediaPlayer;-><init>()V

    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v1

    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->getStartOffset()J

    move-result-wide v2

    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->getLength()J

    move-result-wide v4

    invoke-virtual/range {v0 .. v5}, Lcom/konka/mm/model/MediaPlayer;->setDataSource(Ljava/io/FileDescriptor;JJ)V

    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->close()V

    invoke-virtual {v0}, Lcom/konka/mm/model/MediaPlayer;->prepare()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    :catch_0
    move-exception v7

    const-string v1, "MediaPlayer"

    const-string v2, "create failed:"

    invoke-static {v1, v2, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_1
    move-object v0, v8

    goto :goto_0

    :catch_1
    move-exception v7

    const-string v1, "MediaPlayer"

    const-string v2, "create failed:"

    invoke-static {v1, v2, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    :catch_2
    move-exception v7

    const-string v1, "MediaPlayer"

    const-string v2, "create failed:"

    invoke-static {v1, v2, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public static create(Landroid/content/Context;Landroid/net/Uri;)Lcom/konka/mm/model/MediaPlayer;
    .locals 1
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/net/Uri;

    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/konka/mm/model/MediaPlayer;->create(Landroid/content/Context;Landroid/net/Uri;Landroid/view/SurfaceHolder;)Lcom/konka/mm/model/MediaPlayer;

    move-result-object v0

    return-object v0
.end method

.method public static create(Landroid/content/Context;Landroid/net/Uri;Landroid/view/SurfaceHolder;)Lcom/konka/mm/model/MediaPlayer;
    .locals 4
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/net/Uri;
    .param p2    # Landroid/view/SurfaceHolder;

    :try_start_0
    new-instance v1, Lcom/konka/mm/model/MediaPlayer;

    invoke-direct {v1}, Lcom/konka/mm/model/MediaPlayer;-><init>()V

    invoke-virtual {v1, p0, p1}, Lcom/konka/mm/model/MediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V

    if-eqz p2, :cond_0

    invoke-virtual {v1, p2}, Lcom/konka/mm/model/MediaPlayer;->setDisplay(Landroid/view/SurfaceHolder;)V

    :cond_0
    invoke-virtual {v1}, Lcom/konka/mm/model/MediaPlayer;->prepare()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_2

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    const-string v2, "MediaPlayer"

    const-string v3, "create failed:"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_1
    const/4 v1, 0x0

    goto :goto_0

    :catch_1
    move-exception v0

    const-string v2, "MediaPlayer"

    const-string v3, "create failed:"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    :catch_2
    move-exception v0

    const-string v2, "MediaPlayer"

    const-string v3, "create failed:"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method private final native native_finalize()V
.end method

.method private final native native_getMetadata(ZZLandroid/os/Parcel;)Z
.end method

.method private static final native native_init()V
.end method

.method private final native native_invoke(Landroid/os/Parcel;Landroid/os/Parcel;)I
.end method

.method private final native native_setMetadataFilter(Landroid/os/Parcel;)I
.end method

.method private final native native_setup(Ljava/lang/Object;)V
.end method

.method private native native_suspend_resume(Z)I
.end method

.method private static postEventFromNative(Ljava/lang/Object;IIILjava/lang/Object;)V
    .locals 3
    .param p0    # Ljava/lang/Object;
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # Ljava/lang/Object;

    check-cast p0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/konka/mm/model/MediaPlayer;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, v1, Lcom/konka/mm/model/MediaPlayer;->mEventHandler:Lcom/konka/mm/model/MediaPlayer$EventHandler;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/konka/mm/model/MediaPlayer;->mEventHandler:Lcom/konka/mm/model/MediaPlayer$EventHandler;

    invoke-virtual {v2, p1, p2, p3, p4}, Lcom/konka/mm/model/MediaPlayer$EventHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    iget-object v2, v1, Lcom/konka/mm/model/MediaPlayer;->mEventHandler:Lcom/konka/mm/model/MediaPlayer$EventHandler;

    invoke-virtual {v2, v0}, Lcom/konka/mm/model/MediaPlayer$EventHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private stayAwake(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/konka/mm/model/MediaPlayer;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/konka/mm/model/MediaPlayer;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/konka/mm/model/MediaPlayer;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    :cond_0
    :goto_0
    iput-boolean p1, p0, Lcom/konka/mm/model/MediaPlayer;->mStayAwake:Z

    invoke-direct {p0}, Lcom/konka/mm/model/MediaPlayer;->updateSurfaceScreenOn()V

    return-void

    :cond_1
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/konka/mm/model/MediaPlayer;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/mm/model/MediaPlayer;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    goto :goto_0
.end method

.method private updateSurfaceScreenOn()V
    .locals 2

    iget-object v0, p0, Lcom/konka/mm/model/MediaPlayer;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/konka/mm/model/MediaPlayer;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    iget-boolean v0, p0, Lcom/konka/mm/model/MediaPlayer;->mScreenOnWhilePlaying:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/konka/mm/model/MediaPlayer;->mStayAwake:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-interface {v1, v0}, Landroid/view/SurfaceHolder;->setKeepScreenOn(Z)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public native attachAuxEffect(I)V
.end method

.method protected finalize()V
    .locals 0

    invoke-direct {p0}, Lcom/konka/mm/model/MediaPlayer;->native_finalize()V

    return-void
.end method

.method public native getAudioSessionId()I
.end method

.method public getAudioTrackInfo(Z)Lcom/konka/mm/model/AudioTrackInfo;
    .locals 5
    .param p1    # Z

    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    new-instance v0, Lcom/konka/mm/model/Metadata;

    invoke-direct {v0}, Lcom/konka/mm/model/Metadata;-><init>()V

    invoke-virtual {p0, p1, v2}, Lcom/konka/mm/model/MediaPlayer;->native_getAudioTrackInfo(ZLandroid/os/Parcel;)V

    invoke-virtual {v0, v2}, Lcom/konka/mm/model/Metadata;->parse(Landroid/os/Parcel;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "MediaPlayer"

    const-string v4, "parse error!"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Lcom/konka/mm/model/AudioTrackInfo;

    invoke-direct {v1, p1, v0}, Lcom/konka/mm/model/AudioTrackInfo;-><init>(ZLcom/konka/mm/model/Metadata;)V

    goto :goto_0
.end method

.method public native getCurrentPosition()I
.end method

.method public native getDuration()I
.end method

.method public native getFrameAt(I)Landroid/graphics/Bitmap;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method public getMetadata(ZZ)Lcom/konka/mm/model/Metadata;
    .locals 4
    .param p1    # Z
    .param p2    # Z

    const/4 v2, 0x0

    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    new-instance v0, Lcom/konka/mm/model/Metadata;

    invoke-direct {v0}, Lcom/konka/mm/model/Metadata;-><init>()V

    invoke-direct {p0, p1, p2, v1}, Lcom/konka/mm/model/MediaPlayer;->native_getMetadata(ZZLandroid/os/Parcel;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    move-object v0, v2

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {v0, v1}, Lcom/konka/mm/model/Metadata;->parse(Landroid/os/Parcel;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    move-object v0, v2

    goto :goto_0
.end method

.method public native getPlayMode()I
.end method

.method public native getSubtitleData()Ljava/lang/String;
.end method

.method public native getVideoHeight()I
.end method

.method public native getVideoWidth()I
.end method

.method public invoke(Landroid/os/Parcel;Landroid/os/Parcel;)I
    .locals 2
    .param p1    # Landroid/os/Parcel;
    .param p2    # Landroid/os/Parcel;

    invoke-direct {p0, p1, p2}, Lcom/konka/mm/model/MediaPlayer;->native_invoke(Landroid/os/Parcel;Landroid/os/Parcel;)I

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->setDataPosition(I)V

    return v0
.end method

.method public native isLooping()Z
.end method

.method public native isPlaying()Z
.end method

.method public native native_getAudioTrackInfo(ZLandroid/os/Parcel;)V
.end method

.method public newRequest()Landroid/os/Parcel;
    .locals 2

    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    const-string v1, "android.media.IMediaPlayer"

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    return-object v0
.end method

.method public native offSubtitleTrack()V
.end method

.method public native onSubtitleTrack()V
.end method

.method public pause()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/konka/mm/model/MediaPlayer;->stayAwake(Z)V

    invoke-direct {p0}, Lcom/konka/mm/model/MediaPlayer;->_pause()V

    return-void
.end method

.method public native prepare()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method public native prepareAsync()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method public release()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/konka/mm/model/MediaPlayer;->stayAwake(Z)V

    invoke-direct {p0}, Lcom/konka/mm/model/MediaPlayer;->updateSurfaceScreenOn()V

    iput-object v1, p0, Lcom/konka/mm/model/MediaPlayer;->mOnPreparedListener:Lcom/konka/mm/model/MediaPlayer$OnPreparedListener;

    iput-object v1, p0, Lcom/konka/mm/model/MediaPlayer;->mOnBufferingUpdateListener:Lcom/konka/mm/model/MediaPlayer$OnBufferingUpdateListener;

    iput-object v1, p0, Lcom/konka/mm/model/MediaPlayer;->mOnCompletionListener:Lcom/konka/mm/model/MediaPlayer$OnCompletionListener;

    iput-object v1, p0, Lcom/konka/mm/model/MediaPlayer;->mOnSeekCompleteListener:Lcom/konka/mm/model/MediaPlayer$OnSeekCompleteListener;

    iput-object v1, p0, Lcom/konka/mm/model/MediaPlayer;->mOnErrorListener:Lcom/konka/mm/model/MediaPlayer$OnErrorListener;

    iput-object v1, p0, Lcom/konka/mm/model/MediaPlayer;->mOnInfoListener:Lcom/konka/mm/model/MediaPlayer$OnInfoListener;

    iput-object v1, p0, Lcom/konka/mm/model/MediaPlayer;->mOnVideoSizeChangedListener:Lcom/konka/mm/model/MediaPlayer$OnVideoSizeChangedListener;

    invoke-direct {p0}, Lcom/konka/mm/model/MediaPlayer;->_release()V

    return-void
.end method

.method public reset()V
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/konka/mm/model/MediaPlayer;->stayAwake(Z)V

    invoke-direct {p0}, Lcom/konka/mm/model/MediaPlayer;->_reset()V

    iget-object v0, p0, Lcom/konka/mm/model/MediaPlayer;->mEventHandler:Lcom/konka/mm/model/MediaPlayer$EventHandler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/konka/mm/model/MediaPlayer$EventHandler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    return-void
.end method

.method public resume()Z
    .locals 3

    const/4 v1, 0x1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/konka/mm/model/MediaPlayer;->native_suspend_resume(Z)I

    move-result v2

    if-gez v2, :cond_0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/konka/mm/model/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0, v1}, Lcom/konka/mm/model/MediaPlayer;->stayAwake(Z)V

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public native seekTo(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method public native setAudioSessionId(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method public native setAudioStreamType(I)V
.end method

.method public native setAudioTrack(I)V
.end method

.method public native setAuxEffectSendLevel(F)V
.end method

.method public setDataSource(Landroid/content/Context;Landroid/net/Uri;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/SecurityException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/konka/mm/model/MediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;Ljava/util/Map;)V

    return-void
.end method

.method public setDataSource(Landroid/content/Context;Landroid/net/Uri;Ljava/util/Map;)V
    .locals 9
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/net/Uri;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/SecurityException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    invoke-virtual {p2}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_0

    const-string v0, "file"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    invoke-virtual {p2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/konka/mm/model/MediaPlayer;->setDataSource(Ljava/lang/String;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    const/4 v6, 0x0

    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string v0, "r"

    invoke-virtual {v7, p2, v0}, Landroid/content/ContentResolver;->openAssetFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v6

    if-nez v6, :cond_3

    :goto_1
    if-eqz v6, :cond_1

    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->close()V

    goto :goto_0

    :cond_3
    :try_start_1
    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->getDeclaredLength()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gez v0, :cond_5

    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/konka/mm/model/MediaPlayer;->setDataSource(Ljava/io/FileDescriptor;)V
    :try_end_1
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catch_0
    move-exception v0

    if-eqz v6, :cond_4

    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->close()V

    :cond_4
    :goto_2
    const-string v0, "MediaPlayer"

    const-string v1, "Couldn\'t open file on client side, trying server side"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p3}, Lcom/konka/mm/model/MediaPlayer;->setDataSource(Ljava/lang/String;Ljava/util/Map;)V

    goto :goto_0

    :cond_5
    :try_start_2
    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v1

    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->getStartOffset()J

    move-result-wide v2

    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->getDeclaredLength()J

    move-result-wide v4

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/konka/mm/model/MediaPlayer;->setDataSource(Ljava/io/FileDescriptor;JJ)V
    :try_end_2
    .catch Ljava/lang/SecurityException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    :catch_1
    move-exception v0

    if-eqz v6, :cond_4

    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->close()V

    goto :goto_2

    :catchall_0
    move-exception v0

    if-eqz v6, :cond_6

    invoke-virtual {v6}, Landroid/content/res/AssetFileDescriptor;->close()V

    :cond_6
    throw v0
.end method

.method public setDataSource(Ljava/io/FileDescriptor;)V
    .locals 6
    .param p1    # Ljava/io/FileDescriptor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    const-wide/16 v2, 0x0

    const-wide v4, 0x7ffffffffffffffL

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/konka/mm/model/MediaPlayer;->setDataSource(Ljava/io/FileDescriptor;JJ)V

    return-void
.end method

.method public native setDataSource(Ljava/io/FileDescriptor;JJ)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method public native setDataSource(Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method public native setDataSource(Ljava/lang/String;Ljava/util/Map;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method public setDisplay(Landroid/view/SurfaceHolder;)V
    .locals 1
    .param p1    # Landroid/view/SurfaceHolder;

    iput-object p1, p0, Lcom/konka/mm/model/MediaPlayer;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/mm/model/MediaPlayer;->mSurface:Landroid/view/Surface;

    :goto_0
    invoke-direct {p0}, Lcom/konka/mm/model/MediaPlayer;->_setVideoSurface()V

    invoke-direct {p0}, Lcom/konka/mm/model/MediaPlayer;->updateSurfaceScreenOn()V

    return-void

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/konka/mm/model/MediaPlayer;->mSurface:Landroid/view/Surface;

    goto :goto_0
.end method

.method public native setLooping(Z)V
.end method

.method public setMetadataFilter(Ljava/util/Set;Ljava/util/Set;)I
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;)I"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/konka/mm/model/MediaPlayer;->newRequest()Landroid/os/Parcel;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Parcel;->dataSize()I

    move-result v3

    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    add-int/lit8 v4, v4, 0x1

    invoke-interface {p2}, Ljava/util/Set;->size()I

    move-result v5

    add-int/2addr v4, v5

    mul-int/lit8 v4, v4, 0x4

    add-int v0, v3, v4

    invoke-virtual {v1}, Landroid/os/Parcel;->dataCapacity()I

    move-result v3

    if-ge v3, v0, :cond_0

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->setDataCapacity(I)V

    :cond_0
    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/os/Parcel;->writeInt(I)V

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_1

    invoke-interface {p2}, Ljava/util/Set;->size()I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/os/Parcel;->writeInt(I)V

    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_2

    invoke-direct {p0, v1}, Lcom/konka/mm/model/MediaPlayer;->native_setMetadataFilter(Landroid/os/Parcel;)I

    move-result v3

    return v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v1, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v1, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_1
.end method

.method public setOnBufferingUpdateListener(Lcom/konka/mm/model/MediaPlayer$OnBufferingUpdateListener;)V
    .locals 0
    .param p1    # Lcom/konka/mm/model/MediaPlayer$OnBufferingUpdateListener;

    iput-object p1, p0, Lcom/konka/mm/model/MediaPlayer;->mOnBufferingUpdateListener:Lcom/konka/mm/model/MediaPlayer$OnBufferingUpdateListener;

    return-void
.end method

.method public setOnCompletionListener(Lcom/konka/mm/model/MediaPlayer$OnCompletionListener;)V
    .locals 0
    .param p1    # Lcom/konka/mm/model/MediaPlayer$OnCompletionListener;

    iput-object p1, p0, Lcom/konka/mm/model/MediaPlayer;->mOnCompletionListener:Lcom/konka/mm/model/MediaPlayer$OnCompletionListener;

    return-void
.end method

.method public setOnErrorListener(Lcom/konka/mm/model/MediaPlayer$OnErrorListener;)V
    .locals 0
    .param p1    # Lcom/konka/mm/model/MediaPlayer$OnErrorListener;

    iput-object p1, p0, Lcom/konka/mm/model/MediaPlayer;->mOnErrorListener:Lcom/konka/mm/model/MediaPlayer$OnErrorListener;

    return-void
.end method

.method public setOnInfoListener(Lcom/konka/mm/model/MediaPlayer$OnInfoListener;)V
    .locals 0
    .param p1    # Lcom/konka/mm/model/MediaPlayer$OnInfoListener;

    iput-object p1, p0, Lcom/konka/mm/model/MediaPlayer;->mOnInfoListener:Lcom/konka/mm/model/MediaPlayer$OnInfoListener;

    return-void
.end method

.method public setOnPreparedListener(Lcom/konka/mm/model/MediaPlayer$OnPreparedListener;)V
    .locals 0
    .param p1    # Lcom/konka/mm/model/MediaPlayer$OnPreparedListener;

    iput-object p1, p0, Lcom/konka/mm/model/MediaPlayer;->mOnPreparedListener:Lcom/konka/mm/model/MediaPlayer$OnPreparedListener;

    return-void
.end method

.method public setOnSeekCompleteListener(Lcom/konka/mm/model/MediaPlayer$OnSeekCompleteListener;)V
    .locals 0
    .param p1    # Lcom/konka/mm/model/MediaPlayer$OnSeekCompleteListener;

    iput-object p1, p0, Lcom/konka/mm/model/MediaPlayer;->mOnSeekCompleteListener:Lcom/konka/mm/model/MediaPlayer$OnSeekCompleteListener;

    return-void
.end method

.method public setOnVideoSizeChangedListener(Lcom/konka/mm/model/MediaPlayer$OnVideoSizeChangedListener;)V
    .locals 0
    .param p1    # Lcom/konka/mm/model/MediaPlayer$OnVideoSizeChangedListener;

    iput-object p1, p0, Lcom/konka/mm/model/MediaPlayer;->mOnVideoSizeChangedListener:Lcom/konka/mm/model/MediaPlayer$OnVideoSizeChangedListener;

    return-void
.end method

.method public native setPlayMode(I)Z
.end method

.method public setScreenOnWhilePlaying(Z)V
    .locals 1
    .param p1    # Z

    iget-boolean v0, p0, Lcom/konka/mm/model/MediaPlayer;->mScreenOnWhilePlaying:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lcom/konka/mm/model/MediaPlayer;->mScreenOnWhilePlaying:Z

    invoke-direct {p0}, Lcom/konka/mm/model/MediaPlayer;->updateSurfaceScreenOn()V

    :cond_0
    return-void
.end method

.method public native setSubtitleDataSource(Ljava/lang/String;)V
.end method

.method public setSubtitleDisplay(Landroid/view/SurfaceHolder;)V
    .locals 1
    .param p1    # Landroid/view/SurfaceHolder;

    iput-object p1, p0, Lcom/konka/mm/model/MediaPlayer;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/mm/model/MediaPlayer;->mSurface:Landroid/view/Surface;

    :goto_0
    invoke-direct {p0}, Lcom/konka/mm/model/MediaPlayer;->_setSubtitleSurface()V

    invoke-direct {p0}, Lcom/konka/mm/model/MediaPlayer;->updateSurfaceScreenOn()V

    return-void

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/konka/mm/model/MediaPlayer;->mSurface:Landroid/view/Surface;

    goto :goto_0
.end method

.method public native setSubtitleTrack(I)V
.end method

.method public native setVolume(FF)V
.end method

.method public setWakeMode(Landroid/content/Context;I)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # I

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/konka/mm/model/MediaPlayer;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/konka/mm/model/MediaPlayer;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/konka/mm/model/MediaPlayer;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->release()V

    :cond_0
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/konka/mm/model/MediaPlayer;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    :cond_1
    const-string v2, "power"

    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    const/high16 v2, 0x20000000

    or-int/2addr v2, p2

    const-class v3, Lcom/konka/mm/model/MediaPlayer;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v2

    iput-object v2, p0, Lcom/konka/mm/model/MediaPlayer;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    iget-object v2, p0, Lcom/konka/mm/model/MediaPlayer;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    if-eqz v1, :cond_2

    iget-object v2, p0, Lcom/konka/mm/model/MediaPlayer;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->acquire()V

    :cond_2
    return-void
.end method

.method public start()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/konka/mm/model/MediaPlayer;->stayAwake(Z)V

    invoke-direct {p0}, Lcom/konka/mm/model/MediaPlayer;->_start()V

    return-void
.end method

.method public stop()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/konka/mm/model/MediaPlayer;->stayAwake(Z)V

    invoke-direct {p0}, Lcom/konka/mm/model/MediaPlayer;->_stop()V

    return-void
.end method

.method public suspend()Z
    .locals 3

    const/4 v1, 0x1

    const/4 v0, 0x0

    invoke-direct {p0, v1}, Lcom/konka/mm/model/MediaPlayer;->native_suspend_resume(Z)I

    move-result v2

    if-gez v2, :cond_0

    :goto_0
    return v0

    :cond_0
    invoke-direct {p0, v0}, Lcom/konka/mm/model/MediaPlayer;->stayAwake(Z)V

    iget-object v0, p0, Lcom/konka/mm/model/MediaPlayer;->mEventHandler:Lcom/konka/mm/model/MediaPlayer$EventHandler;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/konka/mm/model/MediaPlayer$EventHandler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    move v0, v1

    goto :goto_0
.end method
