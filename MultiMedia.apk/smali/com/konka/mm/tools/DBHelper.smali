.class public Lcom/konka/mm/tools/DBHelper;
.super Ljava/lang/Object;
.source "DBHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/mm/tools/DBHelper$MyDBHelper;
    }
.end annotation


# static fields
.field public static final DB_AUDIO:Ljava/lang/String; = "audio"

.field public static final DB_IMAGE:Ljava/lang/String; = "image"

.field public static final DB_TABLENAME:Ljava/lang/String; = "multimedia"

.field public static final DB_VIDEO:Ljava/lang/String; = "video"

.field public static final VERSION:I = 0x6

.field public static dbInstance:Landroid/database/sqlite/SQLiteDatabase;


# instance fields
.field private context:Landroid/content/Context;

.field private myDBHelper:Lcom/konka/mm/tools/DBHelper$MyDBHelper;

.field private tableCreate:Ljava/lang/StringBuffer;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/konka/mm/tools/DBHelper;->context:Landroid/content/Context;

    return-void
.end method

.method static synthetic access$0(Lcom/konka/mm/tools/DBHelper;Ljava/lang/StringBuffer;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/tools/DBHelper;->tableCreate:Ljava/lang/StringBuffer;

    return-void
.end method

.method static synthetic access$1(Lcom/konka/mm/tools/DBHelper;)Ljava/lang/StringBuffer;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/tools/DBHelper;->tableCreate:Ljava/lang/StringBuffer;

    return-object v0
.end method

.method static synthetic access$2(Lcom/konka/mm/tools/DBHelper;)Lcom/konka/mm/tools/DBHelper$MyDBHelper;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/tools/DBHelper;->myDBHelper:Lcom/konka/mm/tools/DBHelper$MyDBHelper;

    return-object v0
.end method


# virtual methods
.method public delete(Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;

    sget-object v0, Lcom/konka/mm/tools/DBHelper;->dbInstance:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "image"

    const-string v2, "ROOTPATH=?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    return-void
.end method

.method public deleteAll(Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;

    sget-object v0, Lcom/konka/mm/tools/DBHelper;->dbInstance:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "image"

    const-string v2, "ROOTPATH=?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    return-void
.end method

.method public getAllUser()Ljava/util/ArrayList;
    .locals 11

    const/4 v2, 0x0

    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    const/4 v8, 0x0

    sget-object v0, Lcom/konka/mm/tools/DBHelper;->dbInstance:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "image"

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_0

    return-object v10

    :cond_0
    new-instance v9, Ljava/util/HashMap;

    invoke-direct {v9}, Ljava/util/HashMap;-><init>()V

    const-string v0, "id"

    const-string v1, "_id"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "path"

    const-string v1, "DATA"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "size"

    const-string v1, "SIZE"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "title"

    const-string v1, "TITLE"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "type"

    const-string v1, "TYPE"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "parentpath"

    const-string v1, "PARENTPATH"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "rootpath"

    const-string v1, "ROOTPATH"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "thumbpath"

    const-string v1, "THUMBPATH"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v10, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0
.end method

.method public getList(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 2
    .param p1    # Ljava/lang/String;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const-string v1, ""

    return-object v0
.end method

.method public insert(Lcom/konka/mm/model/PicInfo;)J
    .locals 4
    .param p1    # Lcom/konka/mm/model/PicInfo;

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "DATA"

    invoke-virtual {p1}, Lcom/konka/mm/model/PicInfo;->getM_path()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "SIZE"

    invoke-virtual {p1}, Lcom/konka/mm/model/PicInfo;->getM_size()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    const-string v1, "TITLE"

    invoke-virtual {p1}, Lcom/konka/mm/model/PicInfo;->getM_title()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v1, Lcom/konka/mm/tools/DBHelper;->dbInstance:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "image"

    const-string v3, "TITLE"

    invoke-virtual {v1, v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v1

    return-wide v1
.end method

.method public modify(Lcom/konka/mm/model/PicInfo;)V
    .locals 0
    .param p1    # Lcom/konka/mm/model/PicInfo;

    return-void
.end method

.method public openDatabase()V
    .locals 4

    sget-object v0, Lcom/konka/mm/tools/DBHelper;->dbInstance:Landroid/database/sqlite/SQLiteDatabase;

    if-nez v0, :cond_0

    new-instance v0, Lcom/konka/mm/tools/DBHelper$MyDBHelper;

    iget-object v1, p0, Lcom/konka/mm/tools/DBHelper;->context:Landroid/content/Context;

    const-string v2, "multimedia"

    const/4 v3, 0x6

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/konka/mm/tools/DBHelper$MyDBHelper;-><init>(Lcom/konka/mm/tools/DBHelper;Landroid/content/Context;Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/konka/mm/tools/DBHelper;->myDBHelper:Lcom/konka/mm/tools/DBHelper$MyDBHelper;

    iget-object v0, p0, Lcom/konka/mm/tools/DBHelper;->myDBHelper:Lcom/konka/mm/tools/DBHelper$MyDBHelper;

    invoke-virtual {v0}, Lcom/konka/mm/tools/DBHelper$MyDBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    sput-object v0, Lcom/konka/mm/tools/DBHelper;->dbInstance:Landroid/database/sqlite/SQLiteDatabase;

    :cond_0
    return-void
.end method
