.class public Lcom/konka/mm/tools/NetworkTool;
.super Ljava/lang/Object;
.source "NetworkTool.java"


# static fields
.field private static IPCHeck:[Z

.field private static IPPool:[I

.field private static index:I

.field private static rangeEnd:I

.field private static rangeStart:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/16 v2, 0xff

    const/4 v1, 0x0

    new-array v0, v2, [I

    sput-object v0, Lcom/konka/mm/tools/NetworkTool;->IPPool:[I

    new-array v0, v2, [Z

    sput-object v0, Lcom/konka/mm/tools/NetworkTool;->IPCHeck:[Z

    const/4 v0, -0x1

    sput v0, Lcom/konka/mm/tools/NetworkTool;->index:I

    sput v1, Lcom/konka/mm/tools/NetworkTool;->rangeStart:I

    sput v1, Lcom/konka/mm/tools/NetworkTool;->rangeEnd:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static GetLocalIP()Ljava/lang/String;
    .locals 7

    :try_start_0
    invoke-static {}, Ljava/net/NetworkInterface;->getNetworkInterfaces()Ljava/util/Enumeration;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v5

    if-nez v5, :cond_1

    :goto_0
    const/4 v5, 0x0

    :goto_1
    return-object v5

    :cond_1
    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/net/NetworkInterface;

    invoke-virtual {v4}, Ljava/net/NetworkInterface;->getInetAddresses()Ljava/util/Enumeration;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/net/InetAddress;

    invoke-virtual {v3}, Ljava/net/InetAddress;->isLoopbackAddress()Z

    move-result v5

    if-nez v5, :cond_2

    invoke-virtual {v3}, Ljava/net/InetAddress;->isSiteLocalAddress()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual {v3}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    goto :goto_1

    :catch_0
    move-exception v2

    sget-object v5, Lcom/konka/mm/finals/CommonFinals;->MM_LOG_TAG:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/net/SocketException;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static GetLocalIP2()Ljava/lang/String;
    .locals 7

    :try_start_0
    invoke-static {}, Ljava/net/NetworkInterface;->getNetworkInterfaces()Ljava/util/Enumeration;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v5

    if-nez v5, :cond_1

    :goto_0
    const/4 v5, 0x0

    :goto_1
    return-object v5

    :cond_1
    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/net/NetworkInterface;

    invoke-virtual {v4}, Ljava/net/NetworkInterface;->getInetAddresses()Ljava/util/Enumeration;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/net/InetAddress;

    invoke-virtual {v3}, Ljava/net/InetAddress;->isLoopbackAddress()Z

    move-result v5

    if-nez v5, :cond_2

    invoke-virtual {v3}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    goto :goto_1

    :catch_0
    move-exception v2

    sget-object v5, Lcom/konka/mm/finals/CommonFinals;->MM_LOG_TAG:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/net/SocketException;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static IPPoolInit()V
    .locals 3

    const/4 v1, -0x1

    sput v1, Lcom/konka/mm/tools/NetworkTool;->index:I

    const/4 v0, 0x1

    :goto_0
    const/16 v1, 0xfe

    if-le v0, v1, :cond_0

    return-void

    :cond_0
    sget-object v1, Lcom/konka/mm/tools/NetworkTool;->IPPool:[I

    aput v0, v1, v0

    sget-object v1, Lcom/konka/mm/tools/NetworkTool;->IPCHeck:[Z

    const/4 v2, 0x0

    aput-boolean v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static Reachable(Ljava/lang/String;)Z
    .locals 4
    .param p0    # Ljava/lang/String;

    const/4 v2, 0x5

    :try_start_0
    invoke-static {p0}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/net/InetAddress;->isReachable(I)Z
    :try_end_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/net/UnknownHostException;->printStackTrace()V

    :goto_1
    const/4 v1, 0x0

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1
.end method

.method public static getNextIP(Ljava/lang/String;)I
    .locals 4
    .param p0    # Ljava/lang/String;

    const/4 v0, -0x1

    const/4 v3, 0x1

    sget v1, Lcom/konka/mm/tools/NetworkTool;->index:I

    if-ne v0, v1, :cond_1

    sput v3, Lcom/konka/mm/tools/NetworkTool;->index:I

    :goto_0
    sget v1, Lcom/konka/mm/tools/NetworkTool;->index:I

    if-lt v1, v3, :cond_0

    sget v1, Lcom/konka/mm/tools/NetworkTool;->index:I

    const/16 v2, 0xfe

    if-gt v1, v2, :cond_0

    sget-object v1, Lcom/konka/mm/tools/NetworkTool;->IPCHeck:[Z

    sget v2, Lcom/konka/mm/tools/NetworkTool;->index:I

    aget-boolean v1, v1, v2

    if-nez v1, :cond_0

    sget-object v0, Lcom/konka/mm/tools/NetworkTool;->IPCHeck:[Z

    sget v1, Lcom/konka/mm/tools/NetworkTool;->index:I

    aput-boolean v3, v0, v1

    sget-object v0, Lcom/konka/mm/tools/NetworkTool;->IPPool:[I

    sget v1, Lcom/konka/mm/tools/NetworkTool;->index:I

    aget v0, v0, v1

    :cond_0
    return v0

    :cond_1
    sget v1, Lcom/konka/mm/tools/NetworkTool;->index:I

    add-int/lit8 v1, v1, 0x1

    sput v1, Lcom/konka/mm/tools/NetworkTool;->index:I

    goto :goto_0
.end method
