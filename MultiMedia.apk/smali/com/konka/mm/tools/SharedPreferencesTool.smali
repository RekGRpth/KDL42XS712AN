.class public Lcom/konka/mm/tools/SharedPreferencesTool;
.super Ljava/lang/Object;
.source "SharedPreferencesTool.java"


# static fields
.field public static final PLAY_MODE:Ljava/lang/String; = "playMode"

.field private static sp:Landroid/content/SharedPreferences;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/konka/mm/tools/SharedPreferencesTool;->sp:Landroid/content/SharedPreferences;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getInt(Landroid/app/Activity;Ljava/lang/String;)I
    .locals 2
    .param p0    # Landroid/app/Activity;
    .param p1    # Ljava/lang/String;

    const-string v0, "music"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/app/Activity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    sput-object v0, Lcom/konka/mm/tools/SharedPreferencesTool;->sp:Landroid/content/SharedPreferences;

    sget-object v0, Lcom/konka/mm/tools/SharedPreferencesTool;->sp:Landroid/content/SharedPreferences;

    const/4 v1, -0x1

    invoke-interface {v0, p1, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public static saveInt(Landroid/app/Activity;Ljava/lang/String;I)V
    .locals 3
    .param p0    # Landroid/app/Activity;
    .param p1    # Ljava/lang/String;
    .param p2    # I

    const-string v1, "music"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/app/Activity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    sput-object v1, Lcom/konka/mm/tools/SharedPreferencesTool;->sp:Landroid/content/SharedPreferences;

    sget-object v1, Lcom/konka/mm/tools/SharedPreferencesTool;->sp:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method
