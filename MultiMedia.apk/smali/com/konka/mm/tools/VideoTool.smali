.class public Lcom/konka/mm/tools/VideoTool;
.super Ljava/lang/Object;
.source "VideoTool.java"


# instance fields
.field private SdName:Ljava/lang/String;

.field private ad:Landroid/app/AlertDialog;

.field private builder:Landroid/app/AlertDialog$Builder;

.field context:Landroid/content/Context;

.field private thumbColumns:[Ljava/lang/String;

.field private videoAttribs:[Ljava/lang/String;

.field private videoInfos:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/konka/mm/model/VideoInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 7
    .param p1    # Landroid/content/Context;

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v3, p0, Lcom/konka/mm/tools/VideoTool;->ad:Landroid/app/AlertDialog;

    iput-object v3, p0, Lcom/konka/mm/tools/VideoTool;->builder:Landroid/app/AlertDialog$Builder;

    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_data"

    aput-object v1, v0, v4

    const-string v1, "title"

    aput-object v1, v0, v5

    const-string v1, "_size"

    aput-object v1, v0, v6

    const/4 v1, 0x3

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "mime_type"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "_display_name"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "duration"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "resolution"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "datetaken"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/konka/mm/tools/VideoTool;->videoAttribs:[Ljava/lang/String;

    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "_data"

    aput-object v1, v0, v4

    const-string v1, "video_id"

    aput-object v1, v0, v5

    iput-object v0, p0, Lcom/konka/mm/tools/VideoTool;->thumbColumns:[Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/mm/tools/VideoTool;->context:Landroid/content/Context;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/konka/mm/tools/VideoTool;->videoInfos:Ljava/util/List;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v0

    const-string v1, "mounted"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/mm/tools/VideoTool;->SdName:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/konka/mm/tools/VideoTool;->InitInfos()V

    :goto_0
    return-void

    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/konka/mm/tools/VideoTool;->builder:Landroid/app/AlertDialog$Builder;

    iget-object v0, p0, Lcom/konka/mm/tools/VideoTool;->builder:Landroid/app/AlertDialog$Builder;

    const-string v1, "\u8bf7\u68c0\u67e5\u662f\u5426\u63d2\u5165SD\u5361\u00b7\u00b7\u00b7"

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const-string v1, "\u786e\u5b9a"

    invoke-virtual {v0, v1, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    iget-object v0, p0, Lcom/konka/mm/tools/VideoTool;->builder:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/mm/tools/VideoTool;->ad:Landroid/app/AlertDialog;

    iget-object v0, p0, Lcom/konka/mm/tools/VideoTool;->ad:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_0
.end method


# virtual methods
.method public InitInfos()V
    .locals 14

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/konka/mm/tools/VideoTool;->context:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    sget-object v1, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    iget-object v2, p0, Lcom/konka/mm/tools/VideoTool;->videoAttribs:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/app/Activity;->managedQuery(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    new-instance v13, Lcom/konka/mm/model/VideoInfo;

    invoke-direct {v13}, Lcom/konka/mm/model/VideoInfo;-><init>()V

    const-string v0, "_data"

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v13, v0}, Lcom/konka/mm/model/VideoInfo;->setM_path(Ljava/lang/String;)V

    const-string v0, "title"

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v13, v0}, Lcom/konka/mm/model/VideoInfo;->setM_title(Ljava/lang/String;)V

    const-string v0, "_size"

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v0

    invoke-virtual {v13, v0, v1}, Lcom/konka/mm/model/VideoInfo;->setM_size(D)V

    const-string v0, "_id"

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v13, v0}, Lcom/konka/mm/model/VideoInfo;->setM_id(Ljava/lang/String;)V

    const-string v0, "mime_type"

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v13, v0}, Lcom/konka/mm/model/VideoInfo;->setM_type(Ljava/lang/String;)V

    const-string v0, "_display_name"

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v13, v0}, Lcom/konka/mm/model/VideoInfo;->setDisplayName(Ljava/lang/String;)V

    const-string v0, "resolution"

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v13, v0}, Lcom/konka/mm/model/VideoInfo;->setResolution(Ljava/lang/String;)V

    const-string v0, "datetaken"

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-virtual {v13, v0, v1}, Lcom/konka/mm/model/VideoInfo;->setM_dateTaken(J)V

    const-string v0, "_id"

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    const-string v7, "video_id=?"

    const/4 v0, 0x1

    new-array v8, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object v10, v8, v0

    iget-object v4, p0, Lcom/konka/mm/tools/VideoTool;->context:Landroid/content/Context;

    check-cast v4, Landroid/app/Activity;

    sget-object v5, Landroid/provider/MediaStore$Video$Thumbnails;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    iget-object v6, p0, Lcom/konka/mm/tools/VideoTool;->thumbColumns:[Ljava/lang/String;

    move-object v9, v3

    invoke-virtual/range {v4 .. v9}, Landroid/app/Activity;->managedQuery(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "_data"

    invoke-interface {v11, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v11, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v13, v0}, Lcom/konka/mm/model/VideoInfo;->setThumbPath(Ljava/lang/String;)V

    :cond_1
    iget-object v0, p0, Lcom/konka/mm/tools/VideoTool;->videoInfos:Ljava/util/List;

    invoke-interface {v0, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_0

    :cond_2
    return-void
.end method

.method public getVideoInfos()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/konka/mm/model/VideoInfo;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/konka/mm/tools/VideoTool;->videoInfos:Ljava/util/List;

    return-object v0
.end method

.method public setVideoInfos(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/konka/mm/model/VideoInfo;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/konka/mm/tools/VideoTool;->videoInfos:Ljava/util/List;

    return-void
.end method
