.class public Lcom/konka/mm/tools/AudioID3V1;
.super Ljava/lang/Object;
.source "AudioID3V1.java"


# instance fields
.field private Album:[B

.field private Artist:[B

.field private Comment:[B

.field private Genre:[B

.field private Header:[B

.field private Reserve:[B

.field private Title:[B

.field private Track:[B

.field private Year:[B


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 10
    .param p1    # Ljava/lang/String;

    const/4 v6, 0x4

    const/16 v8, 0x1f

    const/4 v7, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-array v6, v6, [B

    iput-object v6, p0, Lcom/konka/mm/tools/AudioID3V1;->Header:[B

    new-array v6, v8, [B

    iput-object v6, p0, Lcom/konka/mm/tools/AudioID3V1;->Title:[B

    new-array v6, v8, [B

    iput-object v6, p0, Lcom/konka/mm/tools/AudioID3V1;->Artist:[B

    new-array v6, v8, [B

    iput-object v6, p0, Lcom/konka/mm/tools/AudioID3V1;->Album:[B

    const/4 v6, 0x5

    new-array v6, v6, [B

    iput-object v6, p0, Lcom/konka/mm/tools/AudioID3V1;->Year:[B

    const/16 v6, 0x1d

    new-array v6, v6, [B

    iput-object v6, p0, Lcom/konka/mm/tools/AudioID3V1;->Comment:[B

    new-array v6, v7, [B

    iput-object v6, p0, Lcom/konka/mm/tools/AudioID3V1;->Reserve:[B

    new-array v6, v7, [B

    iput-object v6, p0, Lcom/konka/mm/tools/AudioID3V1;->Track:[B

    new-array v6, v7, [B

    iput-object v6, p0, Lcom/konka/mm/tools/AudioID3V1;->Genre:[B

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v6

    if-gtz v6, :cond_1

    :cond_0
    const-string v6, "wrong audio file!"

    invoke-static {v6}, Liapp/eric/utils/base/Trace;->Warning(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_1
    const/16 v6, 0x2e

    invoke-virtual {p1, v6}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v6

    invoke-virtual {p1, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_2

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_2

    const-string v6, ".mp3"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_3

    :cond_2
    const-string v6, "unsupported audio file!"

    invoke-static {v6}, Liapp/eric/utils/base/Trace;->Warning(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    const/4 v4, 0x0

    const/4 v2, 0x0

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "file="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Liapp/eric/utils/base/Trace;->Debug(Ljava/lang/String;)V

    :try_start_0
    new-instance v4, Ljava/io/FileInputStream;

    new-instance v3, Ljava/io/File;

    invoke-direct {v3, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    :try_start_1
    invoke-direct {v4, v3}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4

    if-eqz v4, :cond_4

    if-nez v3, :cond_5

    :cond_4
    const-string v6, "fin or f null"

    invoke-static {v6}, Liapp/eric/utils/base/Trace;->Debug(Ljava/lang/String;)V

    :cond_5
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "f.length()="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->length()J

    move-result-wide v7

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Liapp/eric/utils/base/Trace;->Debug(Ljava/lang/String;)V

    :try_start_2
    invoke-virtual {v3}, Ljava/io/File;->length()J

    move-result-wide v6

    const-wide/16 v8, 0x80

    sub-long/2addr v6, v8

    invoke-virtual {v4, v6, v7}, Ljava/io/FileInputStream;->skip(J)J

    iget-object v6, p0, Lcom/konka/mm/tools/AudioID3V1;->Header:[B

    const/4 v7, 0x0

    const/4 v8, 0x3

    invoke-virtual {v4, v6, v7, v8}, Ljava/io/FileInputStream;->read([BII)I

    iget-object v6, p0, Lcom/konka/mm/tools/AudioID3V1;->Title:[B

    const/4 v7, 0x0

    const/16 v8, 0x1e

    invoke-virtual {v4, v6, v7, v8}, Ljava/io/FileInputStream;->read([BII)I

    iget-object v6, p0, Lcom/konka/mm/tools/AudioID3V1;->Artist:[B

    const/4 v7, 0x0

    const/16 v8, 0x1e

    invoke-virtual {v4, v6, v7, v8}, Ljava/io/FileInputStream;->read([BII)I

    iget-object v6, p0, Lcom/konka/mm/tools/AudioID3V1;->Album:[B

    const/4 v7, 0x0

    const/16 v8, 0x1e

    invoke-virtual {v4, v6, v7, v8}, Ljava/io/FileInputStream;->read([BII)I

    iget-object v6, p0, Lcom/konka/mm/tools/AudioID3V1;->Year:[B

    const/4 v7, 0x0

    const/4 v8, 0x4

    invoke-virtual {v4, v6, v7, v8}, Ljava/io/FileInputStream;->read([BII)I

    iget-object v6, p0, Lcom/konka/mm/tools/AudioID3V1;->Comment:[B

    const/4 v7, 0x0

    const/16 v8, 0x1c

    invoke-virtual {v4, v6, v7, v8}, Ljava/io/FileInputStream;->read([BII)I

    iget-object v6, p0, Lcom/konka/mm/tools/AudioID3V1;->Reserve:[B

    const/4 v7, 0x0

    const/4 v8, 0x1

    invoke-virtual {v4, v6, v7, v8}, Ljava/io/FileInputStream;->read([BII)I

    iget-object v6, p0, Lcom/konka/mm/tools/AudioID3V1;->Track:[B

    const/4 v7, 0x0

    const/4 v8, 0x1

    invoke-virtual {v4, v6, v7, v8}, Ljava/io/FileInputStream;->read([BII)I

    iget-object v6, p0, Lcom/konka/mm/tools/AudioID3V1;->Genre:[B

    const/4 v7, 0x0

    const/4 v8, 0x1

    invoke-virtual {v4, v6, v7, v8}, Ljava/io/FileInputStream;->read([BII)I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3

    goto/16 :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    :catch_1
    move-exception v0

    :goto_1
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto/16 :goto_0

    :catch_2
    move-exception v1

    :goto_2
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    :catch_3
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    :catch_4
    move-exception v1

    move-object v2, v3

    goto :goto_2

    :catch_5
    move-exception v0

    move-object v2, v3

    goto :goto_1
.end method


# virtual methods
.method public getAlbum()[B
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/tools/AudioID3V1;->Album:[B

    return-object v0
.end method

.method public getArtist()[B
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/tools/AudioID3V1;->Artist:[B

    return-object v0
.end method

.method public getComment()[B
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/tools/AudioID3V1;->Comment:[B

    return-object v0
.end method

.method public getGenre()[B
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/tools/AudioID3V1;->Genre:[B

    return-object v0
.end method

.method public getHeader()[B
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/tools/AudioID3V1;->Header:[B

    return-object v0
.end method

.method public getReserve()[B
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/tools/AudioID3V1;->Reserve:[B

    return-object v0
.end method

.method public getTitle()[B
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/tools/AudioID3V1;->Title:[B

    return-object v0
.end method

.method public getTrack()[B
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/tools/AudioID3V1;->Track:[B

    return-object v0
.end method

.method public getYear()[B
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/tools/AudioID3V1;->Year:[B

    return-object v0
.end method
