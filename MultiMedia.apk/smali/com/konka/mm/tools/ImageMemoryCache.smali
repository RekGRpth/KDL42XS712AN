.class public Lcom/konka/mm/tools/ImageMemoryCache;
.super Ljava/lang/Object;
.source "ImageMemoryCache.java"


# static fields
.field private static final SOFT_CACHE_SIZE:I = 0xf

.field private static mLruCache:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private static mSoftCache:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/ref/SoftReference",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v2, "activity"

    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/ActivityManager;

    invoke-virtual {v2}, Landroid/app/ActivityManager;->getMemoryClass()I

    move-result v1

    const/high16 v2, 0x100000

    mul-int/2addr v2, v1

    div-int/lit8 v0, v2, 0x4

    new-instance v2, Lcom/konka/mm/tools/ImageMemoryCache$1;

    invoke-direct {v2, p0, v0}, Lcom/konka/mm/tools/ImageMemoryCache$1;-><init>(Lcom/konka/mm/tools/ImageMemoryCache;I)V

    sput-object v2, Lcom/konka/mm/tools/ImageMemoryCache;->mLruCache:Landroid/util/LruCache;

    new-instance v2, Lcom/konka/mm/tools/ImageMemoryCache$2;

    const/16 v3, 0xf

    const/high16 v4, 0x3f400000    # 0.75f

    const/4 v5, 0x1

    invoke-direct {v2, p0, v3, v4, v5}, Lcom/konka/mm/tools/ImageMemoryCache$2;-><init>(Lcom/konka/mm/tools/ImageMemoryCache;IFZ)V

    sput-object v2, Lcom/konka/mm/tools/ImageMemoryCache;->mSoftCache:Ljava/util/LinkedHashMap;

    return-void
.end method

.method static synthetic access$0()Ljava/util/LinkedHashMap;
    .locals 1

    sget-object v0, Lcom/konka/mm/tools/ImageMemoryCache;->mSoftCache:Ljava/util/LinkedHashMap;

    return-object v0
.end method


# virtual methods
.method public addBitmapToCache(Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/graphics/Bitmap;

    if-eqz p2, :cond_0

    sget-object v1, Lcom/konka/mm/tools/ImageMemoryCache;->mLruCache:Landroid/util/LruCache;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/konka/mm/tools/ImageMemoryCache;->mLruCache:Landroid/util/LruCache;

    invoke-virtual {v0, p1, p2}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v1

    :cond_0
    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public clearCache()V
    .locals 1

    sget-object v0, Lcom/konka/mm/tools/ImageMemoryCache;->mSoftCache:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->clear()V

    return-void
.end method

.method public getBitmapFromCache(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 5
    .param p1    # Ljava/lang/String;

    sget-object v4, Lcom/konka/mm/tools/ImageMemoryCache;->mLruCache:Landroid/util/LruCache;

    monitor-enter v4

    :try_start_0
    sget-object v3, Lcom/konka/mm/tools/ImageMemoryCache;->mLruCache:Landroid/util/LruCache;

    invoke-virtual {v3, p1}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    sget-object v3, Lcom/konka/mm/tools/ImageMemoryCache;->mLruCache:Landroid/util/LruCache;

    invoke-virtual {v3, p1}, Landroid/util/LruCache;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v3, Lcom/konka/mm/tools/ImageMemoryCache;->mLruCache:Landroid/util/LruCache;

    invoke-virtual {v3, p1, v1}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v4

    move-object v3, v1

    :goto_0
    return-object v3

    :cond_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    sget-object v4, Lcom/konka/mm/tools/ImageMemoryCache;->mSoftCache:Ljava/util/LinkedHashMap;

    monitor-enter v4

    :try_start_1
    sget-object v3, Lcom/konka/mm/tools/ImageMemoryCache;->mSoftCache:Ljava/util/LinkedHashMap;

    invoke-virtual {v3, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/ref/SoftReference;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Landroid/graphics/Bitmap;

    move-object v1, v0

    if-eqz v1, :cond_1

    sget-object v3, Lcom/konka/mm/tools/ImageMemoryCache;->mLruCache:Landroid/util/LruCache;

    invoke-virtual {v3, p1, v1}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v3, Lcom/konka/mm/tools/ImageMemoryCache;->mSoftCache:Ljava/util/LinkedHashMap;

    invoke-virtual {v3, p1}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    const/4 v3, 0x0

    goto :goto_0

    :catchall_0
    move-exception v3

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v3

    :catchall_1
    move-exception v3

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v3
.end method
