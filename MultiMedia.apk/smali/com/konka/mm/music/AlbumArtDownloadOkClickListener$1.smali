.class Lcom/konka/mm/music/AlbumArtDownloadOkClickListener$1;
.super Landroid/os/Handler;
.source "AlbumArtDownloadOkClickListener.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/mm/music/AlbumArtDownloadOkClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/mm/music/AlbumArtDownloadOkClickListener;


# direct methods
.method constructor <init>(Lcom/konka/mm/music/AlbumArtDownloadOkClickListener;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/music/AlbumArtDownloadOkClickListener$1;->this$0:Lcom/konka/mm/music/AlbumArtDownloadOkClickListener;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1    # Landroid/os/Message;

    iget v0, p1, Landroid/os/Message;->what:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/konka/mm/music/AlbumArtDownloadOkClickListener$1;->this$0:Lcom/konka/mm/music/AlbumArtDownloadOkClickListener;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/konka/mm/music/AlbumArtDownloadOkClickListener;->access$0(Lcom/konka/mm/music/AlbumArtDownloadOkClickListener;Z)V

    iget-object v0, p0, Lcom/konka/mm/music/AlbumArtDownloadOkClickListener$1;->this$0:Lcom/konka/mm/music/AlbumArtDownloadOkClickListener;

    new-instance v1, Landroid/app/ProgressDialog;

    iget-object v2, p0, Lcom/konka/mm/music/AlbumArtDownloadOkClickListener$1;->this$0:Lcom/konka/mm/music/AlbumArtDownloadOkClickListener;

    # getter for: Lcom/konka/mm/music/AlbumArtDownloadOkClickListener;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/konka/mm/music/AlbumArtDownloadOkClickListener;->access$1(Lcom/konka/mm/music/AlbumArtDownloadOkClickListener;)Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    invoke-static {v0, v1}, Lcom/konka/mm/music/AlbumArtDownloadOkClickListener;->access$2(Lcom/konka/mm/music/AlbumArtDownloadOkClickListener;Landroid/app/ProgressDialog;)V

    iget-object v0, p0, Lcom/konka/mm/music/AlbumArtDownloadOkClickListener$1;->this$0:Lcom/konka/mm/music/AlbumArtDownloadOkClickListener;

    # getter for: Lcom/konka/mm/music/AlbumArtDownloadOkClickListener;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/konka/mm/music/AlbumArtDownloadOkClickListener;->access$3(Lcom/konka/mm/music/AlbumArtDownloadOkClickListener;)Landroid/app/ProgressDialog;

    move-result-object v0

    const-string v1, "Downloading Album Art"

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/mm/music/AlbumArtDownloadOkClickListener$1;->this$0:Lcom/konka/mm/music/AlbumArtDownloadOkClickListener;

    # getter for: Lcom/konka/mm/music/AlbumArtDownloadOkClickListener;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/konka/mm/music/AlbumArtDownloadOkClickListener;->access$3(Lcom/konka/mm/music/AlbumArtDownloadOkClickListener;)Landroid/app/ProgressDialog;

    move-result-object v0

    const-string v1, "Searching album art..."

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/mm/music/AlbumArtDownloadOkClickListener$1;->this$0:Lcom/konka/mm/music/AlbumArtDownloadOkClickListener;

    # getter for: Lcom/konka/mm/music/AlbumArtDownloadOkClickListener;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/konka/mm/music/AlbumArtDownloadOkClickListener;->access$3(Lcom/konka/mm/music/AlbumArtDownloadOkClickListener;)Landroid/app/ProgressDialog;

    move-result-object v0

    const-string v1, "Cancel"

    iget-object v2, p0, Lcom/konka/mm/music/AlbumArtDownloadOkClickListener$1;->this$0:Lcom/konka/mm/music/AlbumArtDownloadOkClickListener;

    # getter for: Lcom/konka/mm/music/AlbumArtDownloadOkClickListener;->cancelDownloadClickListener:Landroid/content/DialogInterface$OnClickListener;
    invoke-static {v2}, Lcom/konka/mm/music/AlbumArtDownloadOkClickListener;->access$4(Lcom/konka/mm/music/AlbumArtDownloadOkClickListener;)Landroid/content/DialogInterface$OnClickListener;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/app/ProgressDialog;->setButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/mm/music/AlbumArtDownloadOkClickListener$1;->this$0:Lcom/konka/mm/music/AlbumArtDownloadOkClickListener;

    # getter for: Lcom/konka/mm/music/AlbumArtDownloadOkClickListener;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/konka/mm/music/AlbumArtDownloadOkClickListener;->access$3(Lcom/konka/mm/music/AlbumArtDownloadOkClickListener;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    :cond_0
    return-void
.end method
