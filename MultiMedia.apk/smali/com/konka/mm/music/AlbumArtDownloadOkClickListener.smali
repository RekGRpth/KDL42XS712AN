.class public Lcom/konka/mm/music/AlbumArtDownloadOkClickListener;
.super Ljava/lang/Object;
.source "AlbumArtDownloadOkClickListener.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final TAG:Ljava/lang/String;

.field private cancelDownloadClickListener:Landroid/content/DialogInterface$OnClickListener;

.field private mAlbumArtImporter:Lcom/konka/mm/music/AlbumArtImporter;

.field public mArtDownloadTrigger:Landroid/os/Handler;

.field public mArtDownloadUpdateHandler:Landroid/os/Handler;

.field private mContext:Landroid/content/Context;

.field private mDownloading:Z

.field private mProgressDialog:Landroid/app/ProgressDialog;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "AlbumArtDownloadOkClickListener"

    iput-object v0, p0, Lcom/konka/mm/music/AlbumArtDownloadOkClickListener;->TAG:Ljava/lang/String;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/konka/mm/music/AlbumArtDownloadOkClickListener;->mDownloading:Z

    new-instance v0, Lcom/konka/mm/music/AlbumArtDownloadOkClickListener$1;

    invoke-direct {v0, p0}, Lcom/konka/mm/music/AlbumArtDownloadOkClickListener$1;-><init>(Lcom/konka/mm/music/AlbumArtDownloadOkClickListener;)V

    iput-object v0, p0, Lcom/konka/mm/music/AlbumArtDownloadOkClickListener;->mArtDownloadTrigger:Landroid/os/Handler;

    new-instance v0, Lcom/konka/mm/music/AlbumArtDownloadOkClickListener$2;

    invoke-direct {v0, p0}, Lcom/konka/mm/music/AlbumArtDownloadOkClickListener$2;-><init>(Lcom/konka/mm/music/AlbumArtDownloadOkClickListener;)V

    iput-object v0, p0, Lcom/konka/mm/music/AlbumArtDownloadOkClickListener;->mArtDownloadUpdateHandler:Landroid/os/Handler;

    new-instance v0, Lcom/konka/mm/music/AlbumArtDownloadOkClickListener$3;

    invoke-direct {v0, p0}, Lcom/konka/mm/music/AlbumArtDownloadOkClickListener$3;-><init>(Lcom/konka/mm/music/AlbumArtDownloadOkClickListener;)V

    iput-object v0, p0, Lcom/konka/mm/music/AlbumArtDownloadOkClickListener;->cancelDownloadClickListener:Landroid/content/DialogInterface$OnClickListener;

    iput-object p1, p0, Lcom/konka/mm/music/AlbumArtDownloadOkClickListener;->mContext:Landroid/content/Context;

    return-void
.end method

.method static synthetic access$0(Lcom/konka/mm/music/AlbumArtDownloadOkClickListener;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/konka/mm/music/AlbumArtDownloadOkClickListener;->mDownloading:Z

    return-void
.end method

.method static synthetic access$1(Lcom/konka/mm/music/AlbumArtDownloadOkClickListener;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/music/AlbumArtDownloadOkClickListener;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$2(Lcom/konka/mm/music/AlbumArtDownloadOkClickListener;Landroid/app/ProgressDialog;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/music/AlbumArtDownloadOkClickListener;->mProgressDialog:Landroid/app/ProgressDialog;

    return-void
.end method

.method static synthetic access$3(Lcom/konka/mm/music/AlbumArtDownloadOkClickListener;)Landroid/app/ProgressDialog;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/music/AlbumArtDownloadOkClickListener;->mProgressDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$4(Lcom/konka/mm/music/AlbumArtDownloadOkClickListener;)Landroid/content/DialogInterface$OnClickListener;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/music/AlbumArtDownloadOkClickListener;->cancelDownloadClickListener:Landroid/content/DialogInterface$OnClickListener;

    return-object v0
.end method


# virtual methods
.method public isDownloading()Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/mm/music/AlbumArtDownloadOkClickListener;->mDownloading:Z

    return v0
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    const-wide/16 v2, 0x1f4

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    const-string v0, "AlbumArtDownloadOkClickListener"

    const-string v1, "Positive button"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/mm/music/AlbumArtDownloadOkClickListener;->mArtDownloadTrigger:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    :goto_0
    return-void

    :cond_0
    const-string v0, "AlbumArtDownloadOkClickListener"

    const-string v1, "Negative button"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/mm/music/AlbumArtDownloadOkClickListener;->mArtDownloadTrigger:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method

.method public stopArtDownload()V
    .locals 2

    iget-object v1, p0, Lcom/konka/mm/music/AlbumArtDownloadOkClickListener;->mAlbumArtImporter:Lcom/konka/mm/music/AlbumArtImporter;

    invoke-virtual {v1}, Lcom/konka/mm/music/AlbumArtImporter;->stopAlbumArt()V

    :try_start_0
    iget-object v1, p0, Lcom/konka/mm/music/AlbumArtDownloadOkClickListener;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/konka/mm/music/AlbumArtDownloadOkClickListener;->mDownloading:Z

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0
.end method
