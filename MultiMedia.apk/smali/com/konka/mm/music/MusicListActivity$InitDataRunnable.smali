.class public Lcom/konka/mm/music/MusicListActivity$InitDataRunnable;
.super Ljava/lang/Object;
.source "MusicListActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/mm/music/MusicListActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "InitDataRunnable"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/mm/music/MusicListActivity;


# direct methods
.method public constructor <init>(Lcom/konka/mm/music/MusicListActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/music/MusicListActivity$InitDataRunnable;->this$0:Lcom/konka/mm/music/MusicListActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    sget-object v1, Lcom/konka/mm/music/MusicListActivity;->musicPaths:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    iget-object v1, p0, Lcom/konka/mm/music/MusicListActivity$InitDataRunnable;->this$0:Lcom/konka/mm/music/MusicListActivity;

    iget-object v1, v1, Lcom/konka/mm/music/MusicListActivity;->mDBHelper:Lcom/konka/mm/tools/DBHelper1;

    iget-object v2, p0, Lcom/konka/mm/music/MusicListActivity$InitDataRunnable;->this$0:Lcom/konka/mm/music/MusicListActivity;

    # getter for: Lcom/konka/mm/music/MusicListActivity;->mRootPath:Ljava/lang/String;
    invoke-static {v2}, Lcom/konka/mm/music/MusicListActivity;->access$9(Lcom/konka/mm/music/MusicListActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/konka/mm/tools/DBHelper1;->getAudioSortByType(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    sput-object v1, Lcom/konka/mm/music/MusicListActivity;->musicPaths:Ljava/util/ArrayList;

    sget-object v1, Lcom/konka/mm/music/MusicListActivity;->musicPaths:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-gtz v1, :cond_0

    iget-object v1, p0, Lcom/konka/mm/music/MusicListActivity$InitDataRunnable;->this$0:Lcom/konka/mm/music/MusicListActivity;

    iget-object v2, p0, Lcom/konka/mm/music/MusicListActivity$InitDataRunnable;->this$0:Lcom/konka/mm/music/MusicListActivity;

    # getter for: Lcom/konka/mm/music/MusicListActivity;->mRootPath:Ljava/lang/String;
    invoke-static {v2}, Lcom/konka/mm/music/MusicListActivity;->access$9(Lcom/konka/mm/music/MusicListActivity;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/konka/mm/music/MusicListActivity$InitDataRunnable;->this$0:Lcom/konka/mm/music/MusicListActivity;

    # getter for: Lcom/konka/mm/music/MusicListActivity;->mRootPath:Ljava/lang/String;
    invoke-static {v3}, Lcom/konka/mm/music/MusicListActivity;->access$9(Lcom/konka/mm/music/MusicListActivity;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/konka/mm/music/MusicListActivity;->writeToDBCurrent(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    sget-object v1, Lcom/konka/mm/music/MusicListActivity;->musicPaths:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    iget-object v1, p0, Lcom/konka/mm/music/MusicListActivity$InitDataRunnable;->this$0:Lcom/konka/mm/music/MusicListActivity;

    iget-object v1, v1, Lcom/konka/mm/music/MusicListActivity;->mDBHelper:Lcom/konka/mm/tools/DBHelper1;

    iget-object v2, p0, Lcom/konka/mm/music/MusicListActivity$InitDataRunnable;->this$0:Lcom/konka/mm/music/MusicListActivity;

    # getter for: Lcom/konka/mm/music/MusicListActivity;->mRootPath:Ljava/lang/String;
    invoke-static {v2}, Lcom/konka/mm/music/MusicListActivity;->access$9(Lcom/konka/mm/music/MusicListActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/konka/mm/tools/DBHelper1;->getAudioSortByType(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    sput-object v1, Lcom/konka/mm/music/MusicListActivity;->musicPaths:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/konka/mm/music/MusicListActivity$InitDataRunnable;->this$0:Lcom/konka/mm/music/MusicListActivity;

    # getter for: Lcom/konka/mm/music/MusicListActivity;->sortHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/konka/mm/music/MusicListActivity;->access$10(Lcom/konka/mm/music/MusicListActivity;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    const/4 v1, 0x0

    iput v1, v0, Landroid/os/Message;->what:I

    iget-object v1, p0, Lcom/konka/mm/music/MusicListActivity$InitDataRunnable;->this$0:Lcom/konka/mm/music/MusicListActivity;

    # getter for: Lcom/konka/mm/music/MusicListActivity;->sortHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/konka/mm/music/MusicListActivity;->access$10(Lcom/konka/mm/music/MusicListActivity;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method
