.class Lcom/konka/mm/music/MusicListActivity$13;
.super Ljava/lang/Object;
.source "MusicListActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/mm/music/MusicListActivity;->showDataChangeDlg(Landroid/app/Activity;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/mm/music/MusicListActivity;


# direct methods
.method constructor <init>(Lcom/konka/mm/music/MusicListActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/music/MusicListActivity$13;->this$0:Lcom/konka/mm/music/MusicListActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    sget-object v1, Lcom/konka/mm/music/MusicListActivity;->musicPaths:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    iget-object v1, p0, Lcom/konka/mm/music/MusicListActivity$13;->this$0:Lcom/konka/mm/music/MusicListActivity;

    iget-object v1, v1, Lcom/konka/mm/music/MusicListActivity;->mDBHelper:Lcom/konka/mm/tools/DBHelper1;

    iget-object v2, p0, Lcom/konka/mm/music/MusicListActivity$13;->this$0:Lcom/konka/mm/music/MusicListActivity;

    # getter for: Lcom/konka/mm/music/MusicListActivity;->mRootPath:Ljava/lang/String;
    invoke-static {v2}, Lcom/konka/mm/music/MusicListActivity;->access$9(Lcom/konka/mm/music/MusicListActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/konka/mm/tools/DBHelper1;->getAudioSortByType(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    sput-object v1, Lcom/konka/mm/music/MusicListActivity;->musicPaths:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/konka/mm/music/MusicListActivity$13;->this$0:Lcom/konka/mm/music/MusicListActivity;

    # getter for: Lcom/konka/mm/music/MusicListActivity;->sortHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/konka/mm/music/MusicListActivity;->access$10(Lcom/konka/mm/music/MusicListActivity;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    const/4 v1, 0x0

    iput v1, v0, Landroid/os/Message;->what:I

    iget-object v1, p0, Lcom/konka/mm/music/MusicListActivity$13;->this$0:Lcom/konka/mm/music/MusicListActivity;

    # getter for: Lcom/konka/mm/music/MusicListActivity;->sortHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/konka/mm/music/MusicListActivity;->access$10(Lcom/konka/mm/music/MusicListActivity;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method
