.class Lcom/konka/mm/music/MusicActivity$16$1;
.super Ljava/lang/Object;
.source "MusicActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/mm/music/MusicActivity$16;->onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/konka/mm/music/MusicActivity$16;


# direct methods
.method constructor <init>(Lcom/konka/mm/music/MusicActivity$16;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/music/MusicActivity$16$1;->this$1:Lcom/konka/mm/music/MusicActivity$16;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    const/4 v7, 0x4

    iget-object v4, p0, Lcom/konka/mm/music/MusicActivity$16$1;->this$1:Lcom/konka/mm/music/MusicActivity$16;

    # getter for: Lcom/konka/mm/music/MusicActivity$16;->this$0:Lcom/konka/mm/music/MusicActivity;
    invoke-static {v4}, Lcom/konka/mm/music/MusicActivity$16;->access$0(Lcom/konka/mm/music/MusicActivity$16;)Lcom/konka/mm/music/MusicActivity;

    move-result-object v4

    # getter for: Lcom/konka/mm/music/MusicActivity;->result:Lcom/konka/mm/music/SearchResult;
    invoke-static {v4}, Lcom/konka/mm/music/MusicActivity;->access$10(Lcom/konka/mm/music/MusicActivity;)Lcom/konka/mm/music/SearchResult;

    move-result-object v4

    invoke-virtual {v4}, Lcom/konka/mm/music/SearchResult;->getLrcId()I

    move-result v4

    iget-object v5, p0, Lcom/konka/mm/music/MusicActivity$16$1;->this$1:Lcom/konka/mm/music/MusicActivity$16;

    # getter for: Lcom/konka/mm/music/MusicActivity$16;->this$0:Lcom/konka/mm/music/MusicActivity;
    invoke-static {v5}, Lcom/konka/mm/music/MusicActivity$16;->access$0(Lcom/konka/mm/music/MusicActivity$16;)Lcom/konka/mm/music/MusicActivity;

    move-result-object v5

    # getter for: Lcom/konka/mm/music/MusicActivity;->result:Lcom/konka/mm/music/SearchResult;
    invoke-static {v5}, Lcom/konka/mm/music/MusicActivity;->access$10(Lcom/konka/mm/music/MusicActivity;)Lcom/konka/mm/music/SearchResult;

    move-result-object v5

    invoke-virtual {v5}, Lcom/konka/mm/music/SearchResult;->getLrcCode()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/konka/mm/music/LRCUtils;->fetchLyricContent(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v4, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/StringBuilder;

    sget-object v5, Lcom/konka/mm/finals/MusicFinals;->ALBUM_PIC_PATH:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, ".lrc"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    :try_start_0
    new-instance v0, Ljava/io/BufferedWriter;

    new-instance v4, Ljava/io/OutputStreamWriter;

    new-instance v5, Ljava/io/FileOutputStream;

    new-instance v6, Ljava/io/File;

    invoke-direct {v6, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {v5, v6}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    const-string v6, "GBK"

    invoke-direct {v4, v5, v6}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;Ljava/lang/String;)V

    invoke-direct {v0, v4}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V

    invoke-virtual {v0, v1}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/BufferedWriter;->close()V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    iget-object v4, p0, Lcom/konka/mm/music/MusicActivity$16$1;->this$1:Lcom/konka/mm/music/MusicActivity$16;

    # getter for: Lcom/konka/mm/music/MusicActivity$16;->this$0:Lcom/konka/mm/music/MusicActivity;
    invoke-static {v4}, Lcom/konka/mm/music/MusicActivity$16;->access$0(Lcom/konka/mm/music/MusicActivity$16;)Lcom/konka/mm/music/MusicActivity;

    move-result-object v4

    # getter for: Lcom/konka/mm/music/MusicActivity;->lrcHandler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/konka/mm/music/MusicActivity;->access$28(Lcom/konka/mm/music/MusicActivity;)Landroid/os/Handler;

    move-result-object v4

    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :goto_0
    return-void

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    iget-object v4, p0, Lcom/konka/mm/music/MusicActivity$16$1;->this$1:Lcom/konka/mm/music/MusicActivity$16;

    # getter for: Lcom/konka/mm/music/MusicActivity$16;->this$0:Lcom/konka/mm/music/MusicActivity;
    invoke-static {v4}, Lcom/konka/mm/music/MusicActivity$16;->access$0(Lcom/konka/mm/music/MusicActivity$16;)Lcom/konka/mm/music/MusicActivity;

    move-result-object v4

    # getter for: Lcom/konka/mm/music/MusicActivity;->lrcHandler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/konka/mm/music/MusicActivity;->access$28(Lcom/konka/mm/music/MusicActivity;)Landroid/os/Handler;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    :catch_1
    move-exception v2

    invoke-virtual {v2}, Ljava/io/FileNotFoundException;->printStackTrace()V

    iget-object v4, p0, Lcom/konka/mm/music/MusicActivity$16$1;->this$1:Lcom/konka/mm/music/MusicActivity$16;

    # getter for: Lcom/konka/mm/music/MusicActivity$16;->this$0:Lcom/konka/mm/music/MusicActivity;
    invoke-static {v4}, Lcom/konka/mm/music/MusicActivity$16;->access$0(Lcom/konka/mm/music/MusicActivity$16;)Lcom/konka/mm/music/MusicActivity;

    move-result-object v4

    # getter for: Lcom/konka/mm/music/MusicActivity;->lrcHandler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/konka/mm/music/MusicActivity;->access$28(Lcom/konka/mm/music/MusicActivity;)Landroid/os/Handler;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    :catch_2
    move-exception v2

    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    iget-object v4, p0, Lcom/konka/mm/music/MusicActivity$16$1;->this$1:Lcom/konka/mm/music/MusicActivity$16;

    # getter for: Lcom/konka/mm/music/MusicActivity$16;->this$0:Lcom/konka/mm/music/MusicActivity;
    invoke-static {v4}, Lcom/konka/mm/music/MusicActivity$16;->access$0(Lcom/konka/mm/music/MusicActivity$16;)Lcom/konka/mm/music/MusicActivity;

    move-result-object v4

    # getter for: Lcom/konka/mm/music/MusicActivity;->lrcHandler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/konka/mm/music/MusicActivity;->access$28(Lcom/konka/mm/music/MusicActivity;)Landroid/os/Handler;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method
