.class Lcom/konka/mm/music/MusicActivity$12;
.super Ljava/lang/Object;
.source "MusicActivity.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/mm/music/MusicActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/mm/music/MusicActivity;


# direct methods
.method constructor <init>(Lcom/konka/mm/music/MusicActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/music/MusicActivity$12;->this$0:Lcom/konka/mm/music/MusicActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 3
    .param p1    # Landroid/view/View;
    .param p2    # Z

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity$12;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->playPauseTxt:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/mm/music/MusicActivity;->access$41(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity$12;->this$0:Lcom/konka/mm/music/MusicActivity;

    invoke-virtual {v1}, Lcom/konka/mm/music/MusicActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090065    # com.konka.mm.R.string.playTxt

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity$12;->this$0:Lcom/konka/mm/music/MusicActivity;

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity$12;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->playPauseTxt:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/konka/mm/music/MusicActivity;->access$41(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/TextView;

    move-result-object v1

    # invokes: Lcom/konka/mm/music/MusicActivity;->setTextViewVisible(Landroid/widget/TextView;Z)V
    invoke-static {v0, v1, p2}, Lcom/konka/mm/music/MusicActivity;->access$42(Lcom/konka/mm/music/MusicActivity;Landroid/widget/TextView;Z)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity$12;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->playPauseTxt:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/mm/music/MusicActivity;->access$41(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity$12;->this$0:Lcom/konka/mm/music/MusicActivity;

    invoke-virtual {v1}, Lcom/konka/mm/music/MusicActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090066    # com.konka.mm.R.string.pauseTxt

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity$12;->this$0:Lcom/konka/mm/music/MusicActivity;

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity$12;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->playPauseTxt:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/konka/mm/music/MusicActivity;->access$41(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/TextView;

    move-result-object v1

    # invokes: Lcom/konka/mm/music/MusicActivity;->setTextViewVisible(Landroid/widget/TextView;Z)V
    invoke-static {v0, v1, p2}, Lcom/konka/mm/music/MusicActivity;->access$42(Lcom/konka/mm/music/MusicActivity;Landroid/widget/TextView;Z)V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity$12;->this$0:Lcom/konka/mm/music/MusicActivity;

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity$12;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->preTxt:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/konka/mm/music/MusicActivity;->access$43(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/TextView;

    move-result-object v1

    # invokes: Lcom/konka/mm/music/MusicActivity;->setTextViewVisible(Landroid/widget/TextView;Z)V
    invoke-static {v0, v1, p2}, Lcom/konka/mm/music/MusicActivity;->access$42(Lcom/konka/mm/music/MusicActivity;Landroid/widget/TextView;Z)V

    goto :goto_0

    :pswitch_4
    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity$12;->this$0:Lcom/konka/mm/music/MusicActivity;

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity$12;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->nextTxt:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/konka/mm/music/MusicActivity;->access$44(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/TextView;

    move-result-object v1

    # invokes: Lcom/konka/mm/music/MusicActivity;->setTextViewVisible(Landroid/widget/TextView;Z)V
    invoke-static {v0, v1, p2}, Lcom/konka/mm/music/MusicActivity;->access$42(Lcom/konka/mm/music/MusicActivity;Landroid/widget/TextView;Z)V

    goto :goto_0

    :pswitch_5
    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity$12;->this$0:Lcom/konka/mm/music/MusicActivity;

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity$12;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->stopTxt:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/konka/mm/music/MusicActivity;->access$45(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/TextView;

    move-result-object v1

    # invokes: Lcom/konka/mm/music/MusicActivity;->setTextViewVisible(Landroid/widget/TextView;Z)V
    invoke-static {v0, v1, p2}, Lcom/konka/mm/music/MusicActivity;->access$42(Lcom/konka/mm/music/MusicActivity;Landroid/widget/TextView;Z)V

    goto :goto_0

    :pswitch_6
    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity$12;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->musicListTxt:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/mm/music/MusicActivity;->access$27(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity$12;->this$0:Lcom/konka/mm/music/MusicActivity;

    invoke-virtual {v1}, Lcom/konka/mm/music/MusicActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090062    # com.konka.mm.R.string.musicListTxt

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity$12;->this$0:Lcom/konka/mm/music/MusicActivity;

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity$12;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->musicListTxt:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/konka/mm/music/MusicActivity;->access$27(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/TextView;

    move-result-object v1

    # invokes: Lcom/konka/mm/music/MusicActivity;->setTextViewVisible(Landroid/widget/TextView;Z)V
    invoke-static {v0, v1, p2}, Lcom/konka/mm/music/MusicActivity;->access$42(Lcom/konka/mm/music/MusicActivity;Landroid/widget/TextView;Z)V

    goto/16 :goto_0

    :pswitch_7
    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity$12;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->musicListTxt:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/mm/music/MusicActivity;->access$27(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity$12;->this$0:Lcom/konka/mm/music/MusicActivity;

    invoke-virtual {v1}, Lcom/konka/mm/music/MusicActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090063    # com.konka.mm.R.string.lrcTxt

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity$12;->this$0:Lcom/konka/mm/music/MusicActivity;

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity$12;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->musicListTxt:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/konka/mm/music/MusicActivity;->access$27(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/TextView;

    move-result-object v1

    # invokes: Lcom/konka/mm/music/MusicActivity;->setTextViewVisible(Landroid/widget/TextView;Z)V
    invoke-static {v0, v1, p2}, Lcom/konka/mm/music/MusicActivity;->access$42(Lcom/konka/mm/music/MusicActivity;Landroid/widget/TextView;Z)V

    goto/16 :goto_0

    :pswitch_8
    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity$12;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->sequenceTxt:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/mm/music/MusicActivity;->access$18(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity$12;->this$0:Lcom/konka/mm/music/MusicActivity;

    invoke-virtual {v1}, Lcom/konka/mm/music/MusicActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090068    # com.konka.mm.R.string.sequenceTxt

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity$12;->this$0:Lcom/konka/mm/music/MusicActivity;

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity$12;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->sequenceTxt:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/konka/mm/music/MusicActivity;->access$18(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/TextView;

    move-result-object v1

    # invokes: Lcom/konka/mm/music/MusicActivity;->setTextViewVisible(Landroid/widget/TextView;Z)V
    invoke-static {v0, v1, p2}, Lcom/konka/mm/music/MusicActivity;->access$42(Lcom/konka/mm/music/MusicActivity;Landroid/widget/TextView;Z)V

    goto/16 :goto_0

    :pswitch_9
    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity$12;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->sequenceTxt:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/mm/music/MusicActivity;->access$18(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity$12;->this$0:Lcom/konka/mm/music/MusicActivity;

    invoke-virtual {v1}, Lcom/konka/mm/music/MusicActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090069    # com.konka.mm.R.string.repeatTxt

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity$12;->this$0:Lcom/konka/mm/music/MusicActivity;

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity$12;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->sequenceTxt:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/konka/mm/music/MusicActivity;->access$18(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/TextView;

    move-result-object v1

    # invokes: Lcom/konka/mm/music/MusicActivity;->setTextViewVisible(Landroid/widget/TextView;Z)V
    invoke-static {v0, v1, p2}, Lcom/konka/mm/music/MusicActivity;->access$42(Lcom/konka/mm/music/MusicActivity;Landroid/widget/TextView;Z)V

    goto/16 :goto_0

    :pswitch_a
    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity$12;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->sequenceTxt:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/mm/music/MusicActivity;->access$18(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity$12;->this$0:Lcom/konka/mm/music/MusicActivity;

    invoke-virtual {v1}, Lcom/konka/mm/music/MusicActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09006a    # com.konka.mm.R.string.cycleTxt

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity$12;->this$0:Lcom/konka/mm/music/MusicActivity;

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity$12;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->sequenceTxt:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/konka/mm/music/MusicActivity;->access$18(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/TextView;

    move-result-object v1

    # invokes: Lcom/konka/mm/music/MusicActivity;->setTextViewVisible(Landroid/widget/TextView;Z)V
    invoke-static {v0, v1, p2}, Lcom/konka/mm/music/MusicActivity;->access$42(Lcom/konka/mm/music/MusicActivity;Landroid/widget/TextView;Z)V

    goto/16 :goto_0

    :pswitch_b
    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity$12;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->sequenceTxt:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/mm/music/MusicActivity;->access$18(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity$12;->this$0:Lcom/konka/mm/music/MusicActivity;

    invoke-virtual {v1}, Lcom/konka/mm/music/MusicActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09006b    # com.konka.mm.R.string.shuffleTxt

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity$12;->this$0:Lcom/konka/mm/music/MusicActivity;

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity$12;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->sequenceTxt:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/konka/mm/music/MusicActivity;->access$18(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/TextView;

    move-result-object v1

    # invokes: Lcom/konka/mm/music/MusicActivity;->setTextViewVisible(Landroid/widget/TextView;Z)V
    invoke-static {v0, v1, p2}, Lcom/konka/mm/music/MusicActivity;->access$42(Lcom/konka/mm/music/MusicActivity;Landroid/widget/TextView;Z)V

    goto/16 :goto_0

    :pswitch_c
    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity$12;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->voiceTxt:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/mm/music/MusicActivity;->access$46(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity$12;->this$0:Lcom/konka/mm/music/MusicActivity;

    invoke-virtual {v1}, Lcom/konka/mm/music/MusicActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09006d    # com.konka.mm.R.string.voiceTxt

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity$12;->this$0:Lcom/konka/mm/music/MusicActivity;

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity$12;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->voiceTxt:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/konka/mm/music/MusicActivity;->access$46(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/TextView;

    move-result-object v1

    # invokes: Lcom/konka/mm/music/MusicActivity;->setTextViewVisible(Landroid/widget/TextView;Z)V
    invoke-static {v0, v1, p2}, Lcom/konka/mm/music/MusicActivity;->access$42(Lcom/konka/mm/music/MusicActivity;Landroid/widget/TextView;Z)V

    goto/16 :goto_0

    :pswitch_d
    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity$12;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->voiceTxt:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/mm/music/MusicActivity;->access$46(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity$12;->this$0:Lcom/konka/mm/music/MusicActivity;

    invoke-virtual {v1}, Lcom/konka/mm/music/MusicActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09006e    # com.konka.mm.R.string.voiceNoTxt

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity$12;->this$0:Lcom/konka/mm/music/MusicActivity;

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity$12;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->voiceTxt:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/konka/mm/music/MusicActivity;->access$46(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/TextView;

    move-result-object v1

    # invokes: Lcom/konka/mm/music/MusicActivity;->setTextViewVisible(Landroid/widget/TextView;Z)V
    invoke-static {v0, v1, p2}, Lcom/konka/mm/music/MusicActivity;->access$42(Lcom/konka/mm/music/MusicActivity;Landroid/widget/TextView;Z)V

    goto/16 :goto_0

    :pswitch_e
    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity$12;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->searchLrcTxt:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/mm/music/MusicActivity;->access$47(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity$12;->this$0:Lcom/konka/mm/music/MusicActivity;

    invoke-virtual {v1}, Lcom/konka/mm/music/MusicActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09006c    # com.konka.mm.R.string.searchLrc

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity$12;->this$0:Lcom/konka/mm/music/MusicActivity;

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity$12;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->searchLrcTxt:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/konka/mm/music/MusicActivity;->access$47(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/TextView;

    move-result-object v1

    # invokes: Lcom/konka/mm/music/MusicActivity;->setTextViewVisible(Landroid/widget/TextView;Z)V
    invoke-static {v0, v1, p2}, Lcom/konka/mm/music/MusicActivity;->access$42(Lcom/konka/mm/music/MusicActivity;Landroid/widget/TextView;Z)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7f0b007e
        :pswitch_6    # com.konka.mm.R.id.btn_music_list
        :pswitch_7    # com.konka.mm.R.id.btn_music_lrc
        :pswitch_0    # com.konka.mm.R.id.txt_lrc
        :pswitch_3    # com.konka.mm.R.id.btn_music_pre
        :pswitch_0    # com.konka.mm.R.id.txt_pre
        :pswitch_1    # com.konka.mm.R.id.btn_music_play
        :pswitch_2    # com.konka.mm.R.id.btn_music_pause
        :pswitch_0    # com.konka.mm.R.id.txt_play
        :pswitch_4    # com.konka.mm.R.id.btn_music_next
        :pswitch_0    # com.konka.mm.R.id.txt_next
        :pswitch_5    # com.konka.mm.R.id.btn_music_stop
        :pswitch_0    # com.konka.mm.R.id.txt_stop
        :pswitch_8    # com.konka.mm.R.id.btn_music_sequence
        :pswitch_9    # com.konka.mm.R.id.btn_music_repeat_one
        :pswitch_a    # com.konka.mm.R.id.btn_music_cycle
        :pswitch_b    # com.konka.mm.R.id.btn_music_shuffle
        :pswitch_0    # com.konka.mm.R.id.txt_play_order
        :pswitch_e    # com.konka.mm.R.id.btn_search_lrc
        :pswitch_0    # com.konka.mm.R.id.txt_load_lrc
        :pswitch_c    # com.konka.mm.R.id.btn_music_voice
        :pswitch_d    # com.konka.mm.R.id.btn_music_voice_no
    .end packed-switch
.end method
