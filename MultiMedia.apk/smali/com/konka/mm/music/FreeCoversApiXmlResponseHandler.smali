.class Lcom/konka/mm/music/FreeCoversApiXmlResponseHandler;
.super Lorg/xml/sax/helpers/DefaultHandler;
.source "FreeCoversNetFetcher.java"


# instance fields
.field final NAME_TAG:Ljava/lang/String;

.field final PREVIEW_TAG:Ljava/lang/String;

.field final TAG:Ljava/lang/String;

.field final TITLE_TAG:Ljava/lang/String;

.field final TYPE_FRONT:Ljava/lang/String;

.field final TYPE_TAG:Ljava/lang/String;

.field albumArtUrl:Ljava/lang/String;

.field mSearchTarget:Ljava/lang/String;

.field name:Ljava/lang/String;

.field nameTag:Z

.field previewTag:Z

.field titleTag:Z

.field type:Ljava/lang/String;

.field typeTag:Z


# direct methods
.method constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lorg/xml/sax/helpers/DefaultHandler;-><init>()V

    const-string v0, "FreeCoversApiXmlResponseHandler"

    iput-object v0, p0, Lcom/konka/mm/music/FreeCoversApiXmlResponseHandler;->TAG:Ljava/lang/String;

    const-string v0, "title"

    iput-object v0, p0, Lcom/konka/mm/music/FreeCoversApiXmlResponseHandler;->TITLE_TAG:Ljava/lang/String;

    const-string v0, "name"

    iput-object v0, p0, Lcom/konka/mm/music/FreeCoversApiXmlResponseHandler;->NAME_TAG:Ljava/lang/String;

    const-string v0, "type"

    iput-object v0, p0, Lcom/konka/mm/music/FreeCoversApiXmlResponseHandler;->TYPE_TAG:Ljava/lang/String;

    const-string v0, "front"

    iput-object v0, p0, Lcom/konka/mm/music/FreeCoversApiXmlResponseHandler;->TYPE_FRONT:Ljava/lang/String;

    const-string v0, "preview"

    iput-object v0, p0, Lcom/konka/mm/music/FreeCoversApiXmlResponseHandler;->PREVIEW_TAG:Ljava/lang/String;

    iput-boolean v1, p0, Lcom/konka/mm/music/FreeCoversApiXmlResponseHandler;->titleTag:Z

    iput-boolean v1, p0, Lcom/konka/mm/music/FreeCoversApiXmlResponseHandler;->nameTag:Z

    iput-boolean v1, p0, Lcom/konka/mm/music/FreeCoversApiXmlResponseHandler;->typeTag:Z

    iput-boolean v1, p0, Lcom/konka/mm/music/FreeCoversApiXmlResponseHandler;->previewTag:Z

    iput-object v2, p0, Lcom/konka/mm/music/FreeCoversApiXmlResponseHandler;->albumArtUrl:Ljava/lang/String;

    iput-object v2, p0, Lcom/konka/mm/music/FreeCoversApiXmlResponseHandler;->name:Ljava/lang/String;

    iput-object v2, p0, Lcom/konka/mm/music/FreeCoversApiXmlResponseHandler;->type:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/konka/mm/music/FreeCoversApiXmlResponseHandler;->mSearchTarget:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public characters([CII)V
    .locals 3
    .param p1    # [C
    .param p2    # I
    .param p3    # I

    iget-boolean v0, p0, Lcom/konka/mm/music/FreeCoversApiXmlResponseHandler;->nameTag:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/konka/mm/music/FreeCoversApiXmlResponseHandler;->albumArtUrl:Ljava/lang/String;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p1, p2, p3}, Ljava/lang/String;-><init>([CII)V

    iput-object v0, p0, Lcom/konka/mm/music/FreeCoversApiXmlResponseHandler;->name:Ljava/lang/String;

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/konka/mm/music/FreeCoversApiXmlResponseHandler;->typeTag:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/konka/mm/music/FreeCoversApiXmlResponseHandler;->albumArtUrl:Ljava/lang/String;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p1, p2, p3}, Ljava/lang/String;-><init>([CII)V

    iput-object v0, p0, Lcom/konka/mm/music/FreeCoversApiXmlResponseHandler;->type:Ljava/lang/String;

    goto :goto_0

    :cond_2
    iget-boolean v0, p0, Lcom/konka/mm/music/FreeCoversApiXmlResponseHandler;->previewTag:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/mm/music/FreeCoversApiXmlResponseHandler;->albumArtUrl:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/konka/mm/music/FreeCoversApiXmlResponseHandler;->type:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/mm/music/FreeCoversApiXmlResponseHandler;->type:Ljava/lang/String;

    const-string v1, "front"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/mm/music/FreeCoversApiXmlResponseHandler;->name:Ljava/lang/String;

    iget-object v1, p0, Lcom/konka/mm/music/FreeCoversApiXmlResponseHandler;->mSearchTarget:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/konka/mm/music/SearchUtils;->nameIsSimilarEnough(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p1, p2, p3}, Ljava/lang/String;-><init>([CII)V

    iput-object v0, p0, Lcom/konka/mm/music/FreeCoversApiXmlResponseHandler;->albumArtUrl:Ljava/lang/String;

    const-string v0, "FreeCoversApiXmlResponseHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/konka/mm/music/FreeCoversApiXmlResponseHandler;->name:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/mm/music/FreeCoversApiXmlResponseHandler;->albumArtUrl:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public endElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    const/4 v1, 0x0

    const-string v0, "name"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-boolean v1, p0, Lcom/konka/mm/music/FreeCoversApiXmlResponseHandler;->nameTag:Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "type"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iput-boolean v1, p0, Lcom/konka/mm/music/FreeCoversApiXmlResponseHandler;->typeTag:Z

    goto :goto_0

    :cond_2
    const-string v0, "preview"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iput-boolean v1, p0, Lcom/konka/mm/music/FreeCoversApiXmlResponseHandler;->previewTag:Z

    goto :goto_0

    :cond_3
    const-string v0, "title"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iput-boolean v1, p0, Lcom/konka/mm/music/FreeCoversApiXmlResponseHandler;->titleTag:Z

    goto :goto_0
.end method

.method public reset()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/konka/mm/music/FreeCoversApiXmlResponseHandler;->titleTag:Z

    iput-boolean v0, p0, Lcom/konka/mm/music/FreeCoversApiXmlResponseHandler;->nameTag:Z

    iput-boolean v0, p0, Lcom/konka/mm/music/FreeCoversApiXmlResponseHandler;->typeTag:Z

    iput-boolean v0, p0, Lcom/konka/mm/music/FreeCoversApiXmlResponseHandler;->previewTag:Z

    iput-object v1, p0, Lcom/konka/mm/music/FreeCoversApiXmlResponseHandler;->albumArtUrl:Ljava/lang/String;

    iput-object v1, p0, Lcom/konka/mm/music/FreeCoversApiXmlResponseHandler;->name:Ljava/lang/String;

    iput-object v1, p0, Lcom/konka/mm/music/FreeCoversApiXmlResponseHandler;->type:Ljava/lang/String;

    return-void
.end method

.method public setSearchTarget(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/mm/music/FreeCoversApiXmlResponseHandler;->mSearchTarget:Ljava/lang/String;

    return-void
.end method

.method public startElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Lorg/xml/sax/Attributes;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    const/4 v1, 0x1

    const-string v0, "name"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-boolean v1, p0, Lcom/konka/mm/music/FreeCoversApiXmlResponseHandler;->nameTag:Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "type"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iput-boolean v1, p0, Lcom/konka/mm/music/FreeCoversApiXmlResponseHandler;->typeTag:Z

    goto :goto_0

    :cond_2
    const-string v0, "preview"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iput-boolean v1, p0, Lcom/konka/mm/music/FreeCoversApiXmlResponseHandler;->previewTag:Z

    goto :goto_0

    :cond_3
    const-string v0, "title"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iput-boolean v1, p0, Lcom/konka/mm/music/FreeCoversApiXmlResponseHandler;->titleTag:Z

    goto :goto_0
.end method
