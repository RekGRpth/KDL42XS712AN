.class public Lcom/konka/mm/music/Constants;
.super Ljava/lang/Object;
.source "Constants.java"


# static fields
.field static final ALBUM_ART_CHOOSER_ACTIVITY_REQUEST_CODE:I = 0x1

.field static final ALBUM_ART_DOWNLOAD_UI_UPDATE_DONE_IPC_MSG:Ljava/lang/String; = "info_done"

.field static final ALBUM_ART_DOWNLOAD_UI_UPDATE_IPC_MSG:Ljava/lang/String; = "info"

.field static final ALBUM_ART_PROCESSING_UI_UPDATE_DONE_IPC_MSG:Ljava/lang/String; = "info_done"

.field static final ALBUM_ART_PROCESSING_UI_UPDATE_IPC_MSG:Ljava/lang/String; = "info"

.field static final ALBUM_ART_TEXTURE_SIZE:I = 0x100

.field static final ALBUM_NAV_CONTROLS_ID:I = 0x1

.field static final ALBUM_NAV_INFO_ID:I = 0x2

.field static final ALBUM_NAV_VIEW_ID:I = 0x0

.field static final ANALYTICS_EQUALIZER_PAGE:Ljava/lang/String; = "/Equalizer"

.field static final ANALYTICS_MAIN_PAGE:Ljava/lang/String; = "/RockOnNgGl"

.field static final ANALYTICS_MANUAL_ART_PAGE:Ljava/lang/String; = "/ManualAlbumArt"

.field static final ANALYTICS_PREFERENCES_PAGE:Ljava/lang/String; = "/Preferences"

.field static final ANALYTICS_RZ_PROMO:Ljava/lang/String; = "/RZPromo"

.field static final ASYNC_OPEN_COMPLETE:Ljava/lang/String; = "org.abrantix.rockon.rockonnggl.asyncopencomplete"

.field static final BROWSECAT_ALBUM:I = 0x1

.field static final BROWSECAT_ARTIST:I = 0x0

.field static final BROWSECAT_GENRE:I = 0x3

.field static final BROWSECAT_PLAYLIST:I = 0x4

.field static final BROWSECAT_SONG:I = 0x2

.field static final CLICK_ACTION_DELAY:I = 0xfa

.field static final CMDNAME:Ljava/lang/String; = "command"

.field static final CMDNEXT:Ljava/lang/String; = "next"

.field static final CMDPAUSE:Ljava/lang/String; = "pause"

.field static final CMDPREVIOUS:Ljava/lang/String; = "previous"

.field static final CMDRESTART:Ljava/lang/String; = "restart"

.field static final CMDSAVE:Ljava/lang/String; = "save"

.field static final CMDSEEKAMOUNT:Ljava/lang/String; = "seekamount"

.field static final CMDSEEKBACK:Ljava/lang/String; = "seekback"

.field static final CMDSEEKFWD:Ljava/lang/String; = "seekfwd"

.field static final CMDSTOP:Ljava/lang/String; = "stop"

.field static final CMDTOGGLEPAUSE:Ljava/lang/String; = "togglepause"

.field static final CONCERT_APP_MAIN_ACTIVITY:Ljava/lang/String; = "org.abrantix.rockon.concerts.Concerts"

.field static final CONCERT_APP_PACKAGE:Ljava/lang/String; = "org.abrantix.rockon.concerts"

.field static final CPU_SMOOTHNESS:F = 0.1f

.field static final DONATION_AFTER_HAVING_DONATED_INTERVAL:I = 0x186a0

.field static final DONATION_APP_MAIN_ACTIVITY_1:Ljava/lang/String; = "org.abrantix.rockon.rockonnggl.donate.std.pt.Donate"

.field static final DONATION_APP_MAIN_ACTIVITY_2:Ljava/lang/String; = "org.abrantix.rockon.rockonnggl.donate.nicerguy.pt.Donate"

.field static final DONATION_APP_MAIN_ACTIVITY_3:Ljava/lang/String; = "org.abrantix.rockon.rockonnggl.donate.std.Donate"

.field static final DONATION_APP_MAIN_ACTIVITY_4:Ljava/lang/String; = "org.abrantix.rockon.rockonnggl.donate.nicerguy.Donate"

.field static final DONATION_APP_PKG_1:Ljava/lang/String; = "org.abrantix.rockon.rockonnggl.donate.std.pt"

.field static final DONATION_APP_PKG_2:Ljava/lang/String; = "org.abrantix.rockon.rockonnggl.donate.nicerguy.pt"

.field static final DONATION_APP_PKG_3:Ljava/lang/String; = "org.abrantix.rockon.rockonnggl.donate.std"

.field static final DONATION_APP_PKG_4:Ljava/lang/String; = "org.abrantix.rockon.rockonnggl.donate.nicerguy"

.field static final DONATION_INITIAL_INTERVAL:I = 0x14

.field static final DONATION_STANDARD_INTERVAL:I = 0x1e

.field static final DO_NOT_REFRESH:I = 0x2

.field static final FIND_NEXT:I = 0x1

.field static final FIND_PREV:I = 0x0

.field static final FRAME_DURATION_STD:D = 40.0

.field static final FRAME_JUMP_MAX:D = 10.0

.field static final GET_INET_ART_TOO:I = 0x0

.field static final GET_LOCAL_ART_ONLY:I = 0x1

.field static final HARD_REFRESH:I = 0x0

.field static final KEEP_REFRESHING:I = 0x1

.field static final LAST:I = 0x3

.field static final LONG_CLICK:I = 0x1

.field static final MAIN_ACTIVITY_INTENT:I = 0x929

.field static final MAX_CLICK_DOWNTIME:D = 1000.0

.field static final MAX_INACTIVITY_INTERVAL_TO_MAINTAIN_PLAYLIST:D = 4.32E7

.field static final MAX_INACTIVITY_INTERVAL_TO_MAINTAIN_STATE:D = 10000.0

.field static final MAX_LOW_SPEED:F = 0.0025f

.field static final MAX_POSITION_OVERSHOOT:I = 0x1

.field static final MAX_SCROLL:F = 9.0f

.field static final META_CHANGED:Ljava/lang/String; = "org.abrantix.rockon.rockonnggl.metachanged"

.field static final MIN_ALBUM_ART_SIZE:I = 0x64

.field static final MIN_LONG_CLICK_DURATION:I = 0x3e8

.field static final MIN_SCROLL:F = 1.5f

.field static final MIN_SCROLL_TOUCH_MOVE:F = 0.05f

.field static final NEXT:I = 0x2

.field static final NEXT_ACTION:Ljava/lang/String; = "org.abrantix.rockon.rockonnggl.musicservicecommand.next"

.field static final NOW:I = 0x1

.field static final NO_SPECIFIC_ARTIST:I = -0x1

.field static final PAUSE_ACTION:Ljava/lang/String; = "org.abrantix.rockon.rockonnggl.musicservicecommand.pause"

.field static final PLAYBACK_COMPLETE:Ljava/lang/String; = "org.abrantix.rockon.rockonnggl.playbackcomplete"

.field static final PLAYLIST_ALL:I = -0x2

.field static final PLAYLIST_GENRE_OFFSET:I = -0x186a0

.field static final PLAYLIST_GENRE_RANGE:I = 0x1388

.field static final PLAYLIST_ID_KEY:Ljava/lang/String; = "playlistId"

.field static final PLAYLIST_MOST_PLAYED:I = -0x4

.field static final PLAYLIST_MOST_RECENT:I = -0x3

.field static final PLAYLIST_NAME_KEY:Ljava/lang/String; = "playlistName"

.field static final PLAYLIST_UNKNOWN:I = -0x1

.field static final PLAYMODE_CHANGED:Ljava/lang/String; = "org.abrantix.rockon.rockonnggl.playmodechanged"

.field static final PLAYSTATE_CHANGED:Ljava/lang/String; = "org.abrantix.rockon.rockonnggl.playstatechanged"

.field static final PLAY_ACTION_DELAY:I = 0x2ee

.field static final PLAY_NOTIFICATION_ID:I = 0x0

.field static final PONTIFLEX_INTERVAL:I = 0x19

.field static final PREFERENCE_ACTIVITY_REQUEST_CODE:I = 0x0

.field static final PREVIOUS_ACTION:Ljava/lang/String; = "org.abrantix.rockon.rockonnggl.musicservicecommand.previous"

.field static final QUEUE_CHANGED:Ljava/lang/String; = "org.abrantix.rockon.rockonnggl.queuechanged"

.field static final REASONABLE_ALBUM_ART_SIZE:I = 0x100

.field static final REASONABLE_PLAY_QUEUE_SIZE:I = 0x20

.field static final RELEASES_APP_MAIN_ACTIVITY:Ljava/lang/String; = "org.abrantix.releases.Releases"

.field static final RELEASES_APP_PACKAGE:Ljava/lang/String; = "org.abrantix.releases"

.field static final RENDERER_BORING:I = 0x2

.field static final RENDERER_CUBE:I = 0x0

.field static final RENDERER_MORPH:I = 0x3

.field static final RENDERER_WALL:I = 0x1

.field static final REPEAT_ALL:I = 0x2

.field static final REPEAT_CURRENT:I = 0x1

.field static final REPEAT_CURRENT_ACTION:Ljava/lang/String; = "org.abrantix.rockon.rockonnggl.musicservicecommand.repeatcurrent"

.field static final REPEAT_NONE:I = 0x0

.field static final REPEAT_NONE_ACTION:Ljava/lang/String; = "org.abrantix.rockon.rockonnggl.musicservicecommand.repeatnone"

.field static final ROCKON_ALBUM_ART_PATH:Ljava/lang/String; = "/mnt/usb/sda1/albumthumbs/RockOnNg/"

.field static final ROCKON_BASE_PATH:Ljava/lang/String; = "/mnt/usb/sda1/RockOn/"

.field static final ROCKON_DONATION_PATH:Ljava/lang/String; = "/mnt/usb/sda1/RockOn/donate"

.field static final ROCKON_EXTERNAL_DIRECTORIES_FILENAME:Ljava/lang/String; = "/mnt/usb/sda1/RockOn/ext_dirs"

.field static final ROCKON_INTERNAL_DIRECTORIES_FILENAME:Ljava/lang/String; = "/mnt/usb/sda1/RockOn/int_dirs"

.field static final ROCKON_SMALL_ALBUM_ART_PATH:Ljava/lang/String; = "/mnt/usb/sda1/albumthumbs/RockOnNg/small/"

.field private static final ROCKON_STORAGE_PATH:Ljava/lang/String; = "/mnt/usb/sda1/"

.field static final ROCKON_STORAGE_TYPE_FILENAME:Ljava/lang/String; = "/mnt/usb/sda1/RockOn/storage_type"

.field static final ROCKON_UNKNOWN_ART_FILENAME:Ljava/lang/String; = "_____unknown"

.field static final SCROBBLE_PLAYSTATE_COMPLETE:I = 0x3

.field static final SCROBBLE_PLAYSTATE_PAUSE:I = 0x2

.field static final SCROBBLE_PLAYSTATE_RESUME:I = 0x1

.field static final SCROBBLE_PLAYSTATE_START:I = 0x0

.field static final SCROBBLE_SD_API:Ljava/lang/String; = "net.jjc1138.android.scrobbler.action.MUSIC_STATUS"

.field static final SCROBBLE_SLS_API:Ljava/lang/String; = "com.adam.aslfms.notify.playstatechanged"

.field static final SCROLLING_RESET_TIMEOUT:I = 0x1d4c

.field static final SCROLL_MODE_HORIZONTAL:I = 0x1

.field static final SCROLL_MODE_VERTICAL:I = 0x0

.field static final SCROLL_SPEED_BOOST:F = 675.0f

.field static final SCROLL_SPEED_SMOOTHNESS:F = 2.5f

.field static final SERVICECMD:Ljava/lang/String; = "org.abrantix.rockon.rockonnggl.musicservicecommand"

.field static final SHUFFLE_AUTO:I = 0x2

.field static final SHUFFLE_NONE:I = 0x0

.field static final SHUFFLE_NONE_ACTION:Ljava/lang/String; = "org.abrantix.rockon.rockonnggl.musicservicecommand.shufflenone"

.field static final SHUFFLE_NORMAL:I = 0x1

.field static final SHUFFLE_NORMAL_ACTION:Ljava/lang/String; = "org.abrantix.rockon.rockonnggl.musicservicecommand.shufflenormal"

.field static final SIMILARITY_THRESHOLD:F = 0.66f

.field static final SINGLE_CLICK:I = 0x0

.field static final START_STICKY:I = 0x1

.field static final STATE_FULLSCREEN:I = 0x1

.field static final STATE_INTRO:I = -0x1

.field static final STATE_NAVIGATOR:I = 0x0

.field static final SWITCHER_CATEGORY_COUNT:I = 0x3

.field static final SWITCHER_CAT_ALBUM:I = 0x1

.field static final SWITCHER_CAT_ALBUM_STRING_RES:I = 0x7b

.field static final SWITCHER_CAT_ARTIST:I = 0x0

.field static final SWITCHER_CAT_ARTIST_STRING_RES:I = 0x4d2

.field static final SWITCHER_CAT_CIRCLE_RATIO:F = 0.05f

.field static final SWITCHER_CAT_CIRCLE_SPACING:F = 1.0f

.field static final SWITCHER_CAT_SONG:I = 0x2

.field static final SWITCHER_CAT_SONG_STRING_RES:I = 0x4d3

.field static final SWITCHER_HIGH_PRESENCE_ALPHA:I = 0xc0

.field static final SWITCHER_LOW_PRESENCE_ALPHA:I = 0x0

.field static final SWITCHER_MOVEMENT_REQUIRED_TO_SWITCH:F = 0.25f

.field static final SWITCHER_PERSIST_SWITCH_PERIOD:I = 0x2ee

.field static final SWITCHER_PRESENCE_UPDATE_STEP:F = 0.1f

.field static final SWITCHER_TEXT_RATIO:F = 0.66f

.field static final THEME_EARTHQUAKE:I = 0x66

.field static final THEME_EARTHQUAKE_BLOCK_COUNT:I = 0x100

.field static final THEME_EARTHQUAKE_FILE_EXT:Ljava/lang/String; = ".earthquake"

.field static final THEME_EARTHQUAKE_RANDOM_AMOUNT:I = 0x10

.field static final THEME_HALFTONE:I = 0x65

.field static final THEME_HALF_TONE_BLOCK_COUNT:I = 0x40

.field static final THEME_HALF_TONE_FILE_EXT:Ljava/lang/String; = ".halftone"

.field static final THEME_HALF_TONE_PROC_RESOLUTION:I = 0x280

.field static final THEME_NORMAL:I = 0x64

.field static final TOGGLEPAUSE_ACTION:Ljava/lang/String; = "org.abrantix.rockon.rockonnggl.musicservicecommand.togglepause"

.field static final WIDGET_COMPONENT:Ljava/lang/String; = "org.abrantix.rockon.rockonnggl.RockOnNextGenAppWidgetProvider"

.field static final WIDGET_COMPONENT_3x3:Ljava/lang/String; = "org.abrantix.rockon.rockonnggl.RockOnNextGenAppWidgetProvider3x3"

.field static final WIDGET_COMPONENT_4x1:Ljava/lang/String; = "org.abrantix.rockon.rockonnggl.RockOnNextGenAppWidgetProvider4x1"

.field static final WIDGET_COMPONENT_4x4:Ljava/lang/String; = "org.abrantix.rockon.rockonnggl.RockOnNextGenAppWidgetProvider4x4"

.field static final WIDGET_COMPONENT_PACKAGE:Ljava/lang/String; = "org.abrantix.rockon.rockonnggl"

.field static final albumAlphabeticalSortOrder:Ljava/lang/String; = "album_key ASC"

.field static final albumAlphabeticalSortOrderByArtist:Ljava/lang/String; = "artist COLLATE NOCASE ASC, maxyear DESC"

.field static final albumProjection:[Ljava/lang/String;

.field static final albumSongListFrom:[Ljava/lang/String;

.field static final albumSongListLayoutId:I = 0x7f030035

.field static final albumSongListTo:[I

.field static final artistAlbumListFrom:[Ljava/lang/String;

.field static final artistAlbumListLayoutId:I = 0x7f030000

.field static final artistAlbumListTo:[I

.field static final artistAlbumsYearSortOrder:Ljava/lang/String; = "maxyear DESC"

.field static final artistAlphabeticalSortOrder:Ljava/lang/String; = "artist_key ASC"

.field static final artistProjection:[Ljava/lang/String;

.field static final genreAlphabeticalSorting:Ljava/lang/String; = "name COLLATE NOCASE ASC"

.field static final genreMemberProjection:[Ljava/lang/String;

.field static final genreMembersAlbumSorting:Ljava/lang/String; = "album_id ASC"

.field static final genreProjection:[Ljava/lang/String;

.field static final playlistAlphabeticalSorting:Ljava/lang/String; = "name COLLATE NOCASE ASC"

.field static final playlistMembersAlbumSorting:Ljava/lang/String; = "album_id ASC"

.field static final playlistMembersProjection:[Ljava/lang/String;

.field static final playlistProjection:[Ljava/lang/String;

.field static final prefKey_mEqualizerEnabled:Ljava/lang/String; = "mEqualizerEnabled"

.field static final prefKey_mEqualizerSettings:Ljava/lang/String; = "mEqualizerSettings"

.field static final prefkey_mAppCreateCount:Ljava/lang/String; = "mAppCreateCount"

.field static final prefkey_mAppCreateCountForDonation:Ljava/lang/String; = "mAppCreateCountForDonation"

.field static final prefkey_mAppHasDonated:Ljava/lang/String; = "mAppHasDonated"

.field static final prefkey_mBrowseCatMode:Ljava/lang/String; = "mBrowseCatMode"

.field static final prefkey_mControlsOnBottom:Ljava/lang/String; = "mControlsOnBottom"

.field static final prefkey_mFullscreen:Ljava/lang/String; = "mFullScreen"

.field static final prefkey_mLastAppActionTimestamp:Ljava/lang/String; = "mLastAppActionTimestamp"

.field static final prefkey_mLastAppUiActionTimestamp:Ljava/lang/String; = "mLastAppUiActionTimestamp"

.field static final prefkey_mNavigatorPositionX:Ljava/lang/String; = "mNavigatorPositionX"

.field static final prefkey_mNavigatorPositionY:Ljava/lang/String; = "mNavigatorPositionY"

.field static final prefkey_mNavigatorTargetPositionX:Ljava/lang/String; = "mNavigatorTargetPositionX"

.field static final prefkey_mNavigatorTargetPositionY:Ljava/lang/String; = "mNavigatorTargetPositionY"

.field static final prefkey_mParent:Ljava/lang/String; = "parent"

.field static final prefkey_mPlaylistId:Ljava/lang/String; = "mPlaylistId"

.field static final prefkey_mRendererMode:Ljava/lang/String; = "mRendererMode"

.field static final prefkey_mTheme:Ljava/lang/String; = "mTheme"

.field static final prefkey_mThemeBeingProcessed:Ljava/lang/String; = "mThemeBeingProcessed"

.field static final prefkey_mThemeEarthquakeDone:Ljava/lang/String; = "mThemeEarthquakeDone"

.field static final prefkey_mThemeHalfToneDone:Ljava/lang/String; = "mThemeHalfToneDone"

.field static final prefkey_mThemeProcessing:Ljava/lang/String; = "mThemeProcessing"

.field static final queueSongListFrom:[Ljava/lang/String;

.field static final queueSongListLayoutId:I = 0x7f030036

.field static final queueSongListTo:[I

.field static final songListAlbumAndNumericalSorting:Ljava/lang/String; = "album COLLATE NOCASE ASC, track ASC"

.field static final songListNumericalSorting:Ljava/lang/String; = "track ASC"

.field static final songListPlaylistSorting:Ljava/lang/String; = "play_order ASC"

.field static final songListTitleSorting:Ljava/lang/String; = "title_key ASC"

.field static final songProjection:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x2

    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v4

    const-string v1, "album_key"

    aput-object v1, v0, v5

    const-string v1, "album"

    aput-object v1, v0, v3

    const-string v1, "artist"

    aput-object v1, v0, v6

    const-string v1, "album_art"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "maxyear"

    aput-object v2, v0, v1

    sput-object v0, Lcom/konka/mm/music/Constants;->albumProjection:[Ljava/lang/String;

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v4

    const-string v1, "artist_key"

    aput-object v1, v0, v5

    const-string v1, "artist"

    aput-object v1, v0, v3

    const-string v1, "number_of_albums"

    aput-object v1, v0, v6

    const-string v1, "number_of_tracks"

    aput-object v1, v0, v7

    sput-object v0, Lcom/konka/mm/music/Constants;->artistProjection:[Ljava/lang/String;

    const/16 v0, 0xd

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v4

    const-string v1, "album"

    aput-object v1, v0, v5

    const-string v1, "album_id"

    aput-object v1, v0, v3

    const-string v1, "album_key"

    aput-object v1, v0, v6

    const-string v1, "artist"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "artist_id"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "title_key"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "_data"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "_display_name"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "duration"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "is_music"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "track"

    aput-object v2, v0, v1

    sput-object v0, Lcom/konka/mm/music/Constants;->songProjection:[Ljava/lang/String;

    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v4

    const-string v1, "name"

    aput-object v1, v0, v5

    sput-object v0, Lcom/konka/mm/music/Constants;->genreProjection:[Ljava/lang/String;

    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v4

    const-string v1, "album_id"

    aput-object v1, v0, v5

    const-string v1, "album"

    aput-object v1, v0, v3

    const-string v1, "artist"

    aput-object v1, v0, v6

    const-string v1, "artist_id"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "track"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "duration"

    aput-object v2, v0, v1

    sput-object v0, Lcom/konka/mm/music/Constants;->genreMemberProjection:[Ljava/lang/String;

    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v4

    const-string v1, "name"

    aput-object v1, v0, v5

    sput-object v0, Lcom/konka/mm/music/Constants;->playlistProjection:[Ljava/lang/String;

    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v4

    const-string v1, "playlist_id"

    aput-object v1, v0, v5

    const-string v1, "album_id"

    aput-object v1, v0, v3

    const-string v1, "album"

    aput-object v1, v0, v6

    const-string v1, "artist"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "artist_id"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "audio_id"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "title"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "track"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "duration"

    aput-object v2, v0, v1

    sput-object v0, Lcom/konka/mm/music/Constants;->playlistMembersProjection:[Ljava/lang/String;

    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "title"

    aput-object v1, v0, v4

    const-string v1, "duration"

    aput-object v1, v0, v5

    sput-object v0, Lcom/konka/mm/music/Constants;->albumSongListFrom:[Ljava/lang/String;

    new-array v0, v3, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/konka/mm/music/Constants;->albumSongListTo:[I

    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "album"

    aput-object v1, v0, v4

    const-string v1, "maxyear"

    aput-object v1, v0, v5

    sput-object v0, Lcom/konka/mm/music/Constants;->artistAlbumListFrom:[Ljava/lang/String;

    new-array v0, v3, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/konka/mm/music/Constants;->artistAlbumListTo:[I

    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "title"

    aput-object v1, v0, v4

    const-string v1, "artist"

    aput-object v1, v0, v5

    const-string v1, "duration"

    aput-object v1, v0, v3

    sput-object v0, Lcom/konka/mm/music/Constants;->queueSongListFrom:[Ljava/lang/String;

    new-array v0, v6, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/konka/mm/music/Constants;->queueSongListTo:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x7f0b00fa    # com.konka.mm.R.id.songlist_item_song_name
        0x7f0b00f9    # com.konka.mm.R.id.songlist_item_song_duration
    .end array-data

    :array_1
    .array-data 4
        0x7f0b0001    # com.konka.mm.R.id.albumlist_item_album_name
        0x7f0b0000    # com.konka.mm.R.id.albumlist_item_album_year
    .end array-data

    :array_2
    .array-data 4
        0x7f0b00fc    # com.konka.mm.R.id.queue_list_songname
        0x7f0b00fd    # com.konka.mm.R.id.queue_list_artistname
        0x7f0b00fb    # com.konka.mm.R.id.queue_list_songduration
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
