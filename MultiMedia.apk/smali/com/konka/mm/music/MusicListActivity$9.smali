.class Lcom/konka/mm/music/MusicListActivity$9;
.super Ljava/lang/Object;
.source "MusicListActivity.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/mm/music/MusicListActivity;->init()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/mm/music/MusicListActivity;


# direct methods
.method constructor <init>(Lcom/konka/mm/music/MusicListActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/music/MusicListActivity$9;->this$0:Lcom/konka/mm/music/MusicListActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # Landroid/view/KeyEvent;

    const/4 v0, 0x1

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/16 v2, 0x15

    if-ne v1, v2, :cond_0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/16 v2, 0x16

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/konka/mm/music/MusicListActivity$9;->this$0:Lcom/konka/mm/music/MusicListActivity;

    # getter for: Lcom/konka/mm/music/MusicListActivity;->sort_btn:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/konka/mm/music/MusicListActivity;->access$16(Lcom/konka/mm/music/MusicListActivity;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v1, p0, Lcom/konka/mm/music/MusicListActivity$9;->this$0:Lcom/konka/mm/music/MusicListActivity;

    # getter for: Lcom/konka/mm/music/MusicListActivity;->sort_btn:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/konka/mm/music/MusicListActivity;->access$16(Lcom/konka/mm/music/MusicListActivity;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ImageView;->requestFocus()Z

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
