.class Lcom/konka/mm/music/AlbumArtDownloadOkClickListener$2;
.super Landroid/os/Handler;
.source "AlbumArtDownloadOkClickListener.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/mm/music/AlbumArtDownloadOkClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/mm/music/AlbumArtDownloadOkClickListener;


# direct methods
.method constructor <init>(Lcom/konka/mm/music/AlbumArtDownloadOkClickListener;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/music/AlbumArtDownloadOkClickListener$2;->this$0:Lcom/konka/mm/music/AlbumArtDownloadOkClickListener;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1    # Landroid/os/Message;

    iget-object v1, p0, Lcom/konka/mm/music/AlbumArtDownloadOkClickListener$2;->this$0:Lcom/konka/mm/music/AlbumArtDownloadOkClickListener;

    # getter for: Lcom/konka/mm/music/AlbumArtDownloadOkClickListener;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/konka/mm/music/AlbumArtDownloadOkClickListener;->access$3(Lcom/konka/mm/music/AlbumArtDownloadOkClickListener;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "info_done"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    :try_start_0
    iget-object v1, p0, Lcom/konka/mm/music/AlbumArtDownloadOkClickListener$2;->this$0:Lcom/konka/mm/music/AlbumArtDownloadOkClickListener;

    # getter for: Lcom/konka/mm/music/AlbumArtDownloadOkClickListener;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/konka/mm/music/AlbumArtDownloadOkClickListener;->access$3(Lcom/konka/mm/music/AlbumArtDownloadOkClickListener;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v1, p0, Lcom/konka/mm/music/AlbumArtDownloadOkClickListener$2;->this$0:Lcom/konka/mm/music/AlbumArtDownloadOkClickListener;

    invoke-virtual {v1}, Lcom/konka/mm/music/AlbumArtDownloadOkClickListener;->stopArtDownload()V

    :cond_0
    :goto_1
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/konka/mm/music/AlbumArtDownloadOkClickListener$2;->this$0:Lcom/konka/mm/music/AlbumArtDownloadOkClickListener;

    # getter for: Lcom/konka/mm/music/AlbumArtDownloadOkClickListener;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/konka/mm/music/AlbumArtDownloadOkClickListener;->access$3(Lcom/konka/mm/music/AlbumArtDownloadOkClickListener;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "info"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method
