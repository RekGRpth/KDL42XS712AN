.class Lcom/konka/mm/music/MusicActivity$4$1;
.super Ljava/lang/Object;
.source "MusicActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/mm/music/MusicActivity$4;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/konka/mm/music/MusicActivity$4;


# direct methods
.method constructor <init>(Lcom/konka/mm/music/MusicActivity$4;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/music/MusicActivity$4$1;->this$1:Lcom/konka/mm/music/MusicActivity$4;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    :try_start_0
    const-string v3, "www.baidu.com"

    const/4 v4, 0x1

    invoke-static {v3, v4}, Lcom/konka/mm/samba/SmbClient;->pingHost(Ljava/lang/String;I)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/konka/mm/music/MusicActivity$4$1;->this$1:Lcom/konka/mm/music/MusicActivity$4;

    # getter for: Lcom/konka/mm/music/MusicActivity$4;->this$0:Lcom/konka/mm/music/MusicActivity;
    invoke-static {v3}, Lcom/konka/mm/music/MusicActivity$4;->access$0(Lcom/konka/mm/music/MusicActivity$4;)Lcom/konka/mm/music/MusicActivity;

    move-result-object v3

    # getter for: Lcom/konka/mm/music/MusicActivity;->lrcHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/konka/mm/music/MusicActivity;->access$28(Lcom/konka/mm/music/MusicActivity;)Landroid/os/Handler;

    move-result-object v3

    const/4 v4, 0x5

    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :goto_0
    return-void

    :cond_0
    iget-object v3, p0, Lcom/konka/mm/music/MusicActivity$4$1;->this$1:Lcom/konka/mm/music/MusicActivity$4;

    # getter for: Lcom/konka/mm/music/MusicActivity$4;->this$0:Lcom/konka/mm/music/MusicActivity;
    invoke-static {v3}, Lcom/konka/mm/music/MusicActivity$4;->access$0(Lcom/konka/mm/music/MusicActivity$4;)Lcom/konka/mm/music/MusicActivity;

    move-result-object v3

    iget-object v3, v3, Lcom/konka/mm/music/MusicActivity;->mEdit_lrc_singer:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-interface {v3}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/konka/mm/music/MusicActivity$4$1;->this$1:Lcom/konka/mm/music/MusicActivity$4;

    # getter for: Lcom/konka/mm/music/MusicActivity$4;->this$0:Lcom/konka/mm/music/MusicActivity;
    invoke-static {v3}, Lcom/konka/mm/music/MusicActivity$4;->access$0(Lcom/konka/mm/music/MusicActivity$4;)Lcom/konka/mm/music/MusicActivity;

    move-result-object v3

    iget-object v3, v3, Lcom/konka/mm/music/MusicActivity;->mEdit_lrc_song:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-interface {v3}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/konka/mm/music/MusicActivity$4$1;->this$1:Lcom/konka/mm/music/MusicActivity$4;

    # getter for: Lcom/konka/mm/music/MusicActivity$4;->this$0:Lcom/konka/mm/music/MusicActivity;
    invoke-static {v3}, Lcom/konka/mm/music/MusicActivity$4;->access$0(Lcom/konka/mm/music/MusicActivity$4;)Lcom/konka/mm/music/MusicActivity;

    move-result-object v3

    invoke-static {v0, v2}, Lcom/konka/mm/music/LRCUtils;->search(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    iput-object v4, v3, Lcom/konka/mm/music/MusicActivity;->mLrcList:Ljava/util/List;

    iget-object v3, p0, Lcom/konka/mm/music/MusicActivity$4$1;->this$1:Lcom/konka/mm/music/MusicActivity$4;

    # getter for: Lcom/konka/mm/music/MusicActivity$4;->this$0:Lcom/konka/mm/music/MusicActivity;
    invoke-static {v3}, Lcom/konka/mm/music/MusicActivity$4;->access$0(Lcom/konka/mm/music/MusicActivity$4;)Lcom/konka/mm/music/MusicActivity;

    move-result-object v3

    # getter for: Lcom/konka/mm/music/MusicActivity;->lrcHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/konka/mm/music/MusicActivity;->access$28(Lcom/konka/mm/music/MusicActivity;)Landroid/os/Handler;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
