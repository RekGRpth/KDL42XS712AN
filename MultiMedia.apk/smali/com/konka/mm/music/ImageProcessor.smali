.class public Lcom/konka/mm/music/ImageProcessor;
.super Ljava/lang/Object;
.source "ImageProcessor.java"


# instance fields
.field blockAverageIntensity:F

.field color:I

.field mHalfToneBlackPaint:Landroid/graphics/Paint;

.field mHalfToneWhitePaint:Landroid/graphics/Paint;

.field mTheme:I


# direct methods
.method public constructor <init>(I)V
    .locals 0
    .param p1    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/konka/mm/music/ImageProcessor;->mTheme:I

    invoke-direct {p0}, Lcom/konka/mm/music/ImageProcessor;->initPaint()V

    return-void
.end method

.method private initEarthquakePaint()V
    .locals 0

    return-void
.end method

.method private initHalfTonePaint()V
    .locals 3

    const/4 v2, 0x1

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/konka/mm/music/ImageProcessor;->mHalfToneWhitePaint:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/konka/mm/music/ImageProcessor;->mHalfToneWhitePaint:Landroid/graphics/Paint;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/konka/mm/music/ImageProcessor;->mHalfToneWhitePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/konka/mm/music/ImageProcessor;->mHalfToneBlackPaint:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/konka/mm/music/ImageProcessor;->mHalfToneBlackPaint:Landroid/graphics/Paint;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/konka/mm/music/ImageProcessor;->mHalfToneBlackPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    return-void
.end method

.method private initPaint()V
    .locals 1

    iget v0, p0, Lcom/konka/mm/music/ImageProcessor;->mTheme:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-direct {p0}, Lcom/konka/mm/music/ImageProcessor;->initHalfTonePaint()V

    goto :goto_0

    :pswitch_1
    invoke-direct {p0}, Lcom/konka/mm/music/ImageProcessor;->initEarthquakePaint()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x65
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public getThemeFileExt()Ljava/lang/String;
    .locals 1

    iget v0, p0, Lcom/konka/mm/music/ImageProcessor;->mTheme:I

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    const-string v0, ""

    goto :goto_0

    :pswitch_1
    const-string v0, ".halftone"

    goto :goto_0

    :pswitch_2
    const-string v0, ".earthquake"

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public process(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 1
    .param p1    # Landroid/graphics/Bitmap;

    iget v0, p0, Lcom/konka/mm/music/ImageProcessor;->mTheme:I

    packed-switch v0, :pswitch_data_0

    const/4 p1, 0x0

    :goto_0
    :pswitch_0
    return-object p1

    :pswitch_1
    invoke-virtual {p0, p1}, Lcom/konka/mm/music/ImageProcessor;->processHalfTone(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object p1

    goto :goto_0

    :pswitch_2
    invoke-virtual {p0, p1}, Lcom/konka/mm/music/ImageProcessor;->processEarthquake(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object p1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public processEarthquake(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 12
    .param p1    # Landroid/graphics/Bitmap;

    const/4 v11, 0x0

    invoke-static {p1}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    add-int/lit8 v7, v7, 0x10

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    add-int/lit8 v8, v8, 0x10

    invoke-static {v1, v7, v8, v11}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object p1

    new-instance v6, Landroid/graphics/Rect;

    invoke-direct {v6}, Landroid/graphics/Rect;-><init>()V

    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5}, Landroid/graphics/Rect;-><init>()V

    invoke-virtual {p1, v11}, Landroid/graphics/Bitmap;->eraseColor(I)V

    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2}, Landroid/graphics/Canvas;-><init>()V

    invoke-virtual {v2, p1}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    div-int/lit16 v0, v7, 0x100

    const/4 v3, 0x0

    :goto_0
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    div-int/2addr v7, v0

    if-lt v3, v7, :cond_0

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    const/4 v9, 0x1

    invoke-static {p1, v7, v8, v9}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v7

    return-object v7

    :cond_0
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v7

    const-wide/high16 v9, 0x4030000000000000L    # 16.0

    mul-double/2addr v7, v9

    double-to-int v4, v7

    mul-int v7, v3, v0

    iput v7, v6, Landroid/graphics/Rect;->top:I

    mul-int v7, v3, v0

    add-int/2addr v7, v0

    iput v7, v6, Landroid/graphics/Rect;->bottom:I

    iput v11, v6, Landroid/graphics/Rect;->left:I

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    iput v7, v6, Landroid/graphics/Rect;->right:I

    mul-int v7, v3, v0

    iput v7, v5, Landroid/graphics/Rect;->top:I

    mul-int v7, v3, v0

    add-int/2addr v7, v0

    iput v7, v5, Landroid/graphics/Rect;->bottom:I

    iput v4, v5, Landroid/graphics/Rect;->left:I

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    add-int/2addr v7, v4

    iput v7, v5, Landroid/graphics/Rect;->right:I

    const/4 v7, 0x0

    invoke-virtual {v2, v1, v6, v5, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method public processHalfTone(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 13
    .param p1    # Landroid/graphics/Bitmap;

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v12

    const/16 v1, 0x280

    const/16 v2, 0x280

    const/4 v3, 0x1

    invoke-static {p1, v1, v2, v3}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v7

    invoke-static {v7}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object p1

    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0}, Landroid/graphics/Canvas;-><init>()V

    invoke-virtual {v0, p1}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    div-int/lit8 v6, v1, 0x40

    const/4 v8, 0x0

    :goto_0
    const/16 v1, 0x40

    if-lt v8, v1, :cond_0

    const/4 v1, 0x1

    invoke-static {p1, v12, v12, v1}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v1

    return-object v1

    :cond_0
    const/4 v9, 0x0

    :goto_1
    const/16 v1, 0x40

    if-lt v9, v1, :cond_1

    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    iput v1, p0, Lcom/konka/mm/music/ImageProcessor;->blockAverageIntensity:F

    const/4 v10, 0x0

    :goto_2
    if-lt v10, v6, :cond_2

    iget v1, p0, Lcom/konka/mm/music/ImageProcessor;->blockAverageIntensity:F

    mul-int v2, v6, v6

    mul-int/lit8 v2, v2, 0x3

    int-to-float v2, v2

    div-float/2addr v1, v2

    iput v1, p0, Lcom/konka/mm/music/ImageProcessor;->blockAverageIntensity:F

    iget v1, p0, Lcom/konka/mm/music/ImageProcessor;->blockAverageIntensity:F

    const/high16 v2, 0x43800000    # 256.0f

    div-float/2addr v1, v2

    iput v1, p0, Lcom/konka/mm/music/ImageProcessor;->blockAverageIntensity:F

    mul-int v1, v8, v6

    int-to-float v1, v1

    mul-int v2, v9, v6

    int-to-float v2, v2

    add-int/lit8 v3, v8, 0x1

    mul-int/2addr v3, v6

    int-to-float v3, v3

    add-int/lit8 v4, v9, 0x1

    mul-int/2addr v4, v6

    int-to-float v4, v4

    iget-object v5, p0, Lcom/konka/mm/music/ImageProcessor;->mHalfToneBlackPaint:Landroid/graphics/Paint;

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    mul-int v1, v8, v6

    div-int/lit8 v2, v6, 0x2

    add-int/2addr v1, v2

    int-to-float v1, v1

    mul-int v2, v9, v6

    div-int/lit8 v3, v6, 0x2

    add-int/2addr v2, v3

    int-to-float v2, v2

    const/high16 v3, 0x3f800000    # 1.0f

    div-int/lit8 v4, v6, 0x2

    add-int/lit8 v4, v4, -0x1

    int-to-float v4, v4

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    int-to-float v4, v4

    iget v5, p0, Lcom/konka/mm/music/ImageProcessor;->blockAverageIntensity:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iget-object v4, p0, Lcom/konka/mm/music/ImageProcessor;->mHalfToneWhitePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    :cond_2
    const/4 v11, 0x0

    :goto_3
    if-lt v11, v6, :cond_3

    add-int/lit8 v10, v10, 0x1

    goto :goto_2

    :cond_3
    mul-int v1, v8, v6

    add-int/2addr v1, v10

    mul-int v2, v9, v6

    add-int/2addr v2, v11

    invoke-virtual {v7, v1, v2}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v1

    iput v1, p0, Lcom/konka/mm/music/ImageProcessor;->color:I

    iget v1, p0, Lcom/konka/mm/music/ImageProcessor;->blockAverageIntensity:F

    iget v2, p0, Lcom/konka/mm/music/ImageProcessor;->color:I

    invoke-static {v2}, Landroid/graphics/Color;->red(I)I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v1, v2

    iput v1, p0, Lcom/konka/mm/music/ImageProcessor;->blockAverageIntensity:F

    iget v1, p0, Lcom/konka/mm/music/ImageProcessor;->blockAverageIntensity:F

    iget v2, p0, Lcom/konka/mm/music/ImageProcessor;->color:I

    invoke-static {v2}, Landroid/graphics/Color;->green(I)I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v1, v2

    iput v1, p0, Lcom/konka/mm/music/ImageProcessor;->blockAverageIntensity:F

    iget v1, p0, Lcom/konka/mm/music/ImageProcessor;->blockAverageIntensity:F

    iget v2, p0, Lcom/konka/mm/music/ImageProcessor;->color:I

    invoke-static {v2}, Landroid/graphics/Color;->blue(I)I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v1, v2

    iput v1, p0, Lcom/konka/mm/music/ImageProcessor;->blockAverageIntensity:F

    add-int/lit8 v11, v11, 0x1

    goto :goto_3
.end method
