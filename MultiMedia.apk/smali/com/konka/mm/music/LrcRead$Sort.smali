.class Lcom/konka/mm/music/LrcRead$Sort;
.super Ljava/lang/Object;
.source "LrcRead.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/mm/music/LrcRead;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Sort"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/konka/mm/music/LyricContent;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/mm/music/LrcRead;


# direct methods
.method public constructor <init>(Lcom/konka/mm/music/LrcRead;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/music/LrcRead$Sort;->this$0:Lcom/konka/mm/music/LrcRead;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private sortUp(Lcom/konka/mm/music/LyricContent;Lcom/konka/mm/music/LyricContent;)I
    .locals 2
    .param p1    # Lcom/konka/mm/music/LyricContent;
    .param p2    # Lcom/konka/mm/music/LyricContent;

    invoke-virtual {p1}, Lcom/konka/mm/music/LyricContent;->getLyricTime()I

    move-result v0

    invoke-virtual {p2}, Lcom/konka/mm/music/LyricContent;->getLyricTime()I

    move-result v1

    if-ge v0, v1, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Lcom/konka/mm/music/LyricContent;->getLyricTime()I

    move-result v0

    invoke-virtual {p2}, Lcom/konka/mm/music/LyricContent;->getLyricTime()I

    move-result v1

    if-le v0, v1, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public compare(Lcom/konka/mm/music/LyricContent;Lcom/konka/mm/music/LyricContent;)I
    .locals 1
    .param p1    # Lcom/konka/mm/music/LyricContent;
    .param p2    # Lcom/konka/mm/music/LyricContent;

    invoke-direct {p0, p1, p2}, Lcom/konka/mm/music/LrcRead$Sort;->sortUp(Lcom/konka/mm/music/LyricContent;Lcom/konka/mm/music/LyricContent;)I

    move-result v0

    return v0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Lcom/konka/mm/music/LyricContent;

    check-cast p2, Lcom/konka/mm/music/LyricContent;

    invoke-virtual {p0, p1, p2}, Lcom/konka/mm/music/LrcRead$Sort;->compare(Lcom/konka/mm/music/LyricContent;Lcom/konka/mm/music/LyricContent;)I

    move-result v0

    return v0
.end method
