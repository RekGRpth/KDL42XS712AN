.class public Lcom/konka/mm/music/LyricView;
.super Landroid/widget/TextView;
.source "LyricView.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "LyricView"


# instance fields
.field public CurrentPaint:Landroid/graphics/Paint;

.field private CurrentPaintColor:I

.field private NotCurrentPaint:Landroid/graphics/Paint;

.field private TextHigh:F

.field private TextSize:F

.field private currentDunringTime:J

.field private currentTime:J

.field private high:F

.field private index:I

.field private lrcList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/konka/mm/music/LyricContent;",
            ">;"
        }
    .end annotation
.end field

.field private notCurrentPaintColor:I

.field private sentenctTime:J

.field private width:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/konka/mm/music/LyricView;->notCurrentPaintColor:I

    const v0, -0xff0100

    iput v0, p0, Lcom/konka/mm/music/LyricView;->CurrentPaintColor:I

    const/high16 v0, 0x42a00000    # 80.0f

    iput v0, p0, Lcom/konka/mm/music/LyricView;->TextHigh:F

    const/high16 v0, 0x42480000    # 50.0f

    iput v0, p0, Lcom/konka/mm/music/LyricView;->TextSize:F

    const/4 v0, 0x0

    iput v0, p0, Lcom/konka/mm/music/LyricView;->index:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/konka/mm/music/LyricView;->lrcList:Ljava/util/List;

    invoke-direct {p0}, Lcom/konka/mm/music/LyricView;->init()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/konka/mm/music/LyricView;->notCurrentPaintColor:I

    const v0, -0xff0100

    iput v0, p0, Lcom/konka/mm/music/LyricView;->CurrentPaintColor:I

    const/high16 v0, 0x42a00000    # 80.0f

    iput v0, p0, Lcom/konka/mm/music/LyricView;->TextHigh:F

    const/high16 v0, 0x42480000    # 50.0f

    iput v0, p0, Lcom/konka/mm/music/LyricView;->TextSize:F

    const/4 v0, 0x0

    iput v0, p0, Lcom/konka/mm/music/LyricView;->index:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/konka/mm/music/LyricView;->lrcList:Ljava/util/List;

    invoke-direct {p0}, Lcom/konka/mm/music/LyricView;->init()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/konka/mm/music/LyricView;->notCurrentPaintColor:I

    const v0, -0xff0100

    iput v0, p0, Lcom/konka/mm/music/LyricView;->CurrentPaintColor:I

    const/high16 v0, 0x42a00000    # 80.0f

    iput v0, p0, Lcom/konka/mm/music/LyricView;->TextHigh:F

    const/high16 v0, 0x42480000    # 50.0f

    iput v0, p0, Lcom/konka/mm/music/LyricView;->TextSize:F

    const/4 v0, 0x0

    iput v0, p0, Lcom/konka/mm/music/LyricView;->index:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/konka/mm/music/LyricView;->lrcList:Ljava/util/List;

    invoke-direct {p0}, Lcom/konka/mm/music/LyricView;->init()V

    return-void
.end method

.method private init()V
    .locals 3

    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/konka/mm/music/LyricView;->setFocusable(Z)V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/konka/mm/music/LyricView;->CurrentPaint:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/konka/mm/music/LyricView;->CurrentPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v0, p0, Lcom/konka/mm/music/LyricView;->CurrentPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/konka/mm/music/LyricView;->NotCurrentPaint:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/konka/mm/music/LyricView;->NotCurrentPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v0, p0, Lcom/konka/mm/music/LyricView;->NotCurrentPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    return-void
.end method


# virtual methods
.method public SetIndex(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/konka/mm/music/LyricView;->index:I

    return-void
.end method

.method public SetNowPlayIndex(I)V
    .locals 3
    .param p1    # I

    int-to-long v1, p1

    iput-wide v1, p0, Lcom/konka/mm/music/LyricView;->currentTime:J

    iget-object v1, p0, Lcom/konka/mm/music/LyricView;->lrcList:Ljava/util/List;

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iget-object v1, p0, Lcom/konka/mm/music/LyricView;->lrcList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lt v0, v1, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/konka/mm/music/LyricView;->lrcList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/konka/mm/music/LyricContent;

    invoke-virtual {v1}, Lcom/konka/mm/music/LyricContent;->getLyricTime()I

    move-result v1

    if-ge p1, v1, :cond_3

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    iget-object v1, p0, Lcom/konka/mm/music/LyricView;->lrcList:Ljava/util/List;

    add-int/lit8 v2, v0, -0x1

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/konka/mm/music/LyricContent;

    invoke-virtual {v1}, Lcom/konka/mm/music/LyricContent;->getLyricTime()I

    move-result v1

    if-lt p1, v1, :cond_3

    :cond_2
    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lcom/konka/mm/music/LyricView;->index:I

    iget-object v1, p0, Lcom/konka/mm/music/LyricView;->lrcList:Ljava/util/List;

    add-int/lit8 v2, v0, -0x1

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/konka/mm/music/LyricContent;

    invoke-virtual {v1}, Lcom/konka/mm/music/LyricContent;->getLyricTime()I

    move-result v1

    int-to-long v1, v1

    iput-wide v1, p0, Lcom/konka/mm/music/LyricView;->sentenctTime:J

    iget-object v1, p0, Lcom/konka/mm/music/LyricView;->lrcList:Ljava/util/List;

    add-int/lit8 v2, v0, -0x1

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/konka/mm/music/LyricContent;

    invoke-virtual {v1}, Lcom/konka/mm/music/LyricContent;->getSleepTime()I

    move-result v1

    int-to-long v1, v1

    iput-wide v1, p0, Lcom/konka/mm/music/LyricView;->currentDunringTime:J

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getCurrentPaintColor()I
    .locals 1

    iget v0, p0, Lcom/konka/mm/music/LyricView;->CurrentPaintColor:I

    return v0
.end method

.method public getNotCurrentPaintColor()I
    .locals 1

    iget v0, p0, Lcom/konka/mm/music/LyricView;->notCurrentPaintColor:I

    return v0
.end method

.method public getTextHigh()F
    .locals 1

    iget v0, p0, Lcom/konka/mm/music/LyricView;->TextHigh:F

    return v0
.end method

.method public getTextSize()F
    .locals 1

    iget v0, p0, Lcom/konka/mm/music/LyricView;->TextSize:F

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 21
    .param p1    # Landroid/graphics/Canvas;

    invoke-super/range {p0 .. p1}, Landroid/widget/TextView;->onDraw(Landroid/graphics/Canvas;)V

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/mm/music/LyricView;->CurrentPaint:Landroid/graphics/Paint;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/konka/mm/music/LyricView;->CurrentPaintColor:I

    move/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Landroid/graphics/Paint;->setColor(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/mm/music/LyricView;->NotCurrentPaint:Landroid/graphics/Paint;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/konka/mm/music/LyricView;->notCurrentPaintColor:I

    move/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Landroid/graphics/Paint;->setColor(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/mm/music/LyricView;->CurrentPaint:Landroid/graphics/Paint;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/konka/mm/music/LyricView;->TextSize:F

    move/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Landroid/graphics/Paint;->setTextSize(F)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/mm/music/LyricView;->CurrentPaint:Landroid/graphics/Paint;

    move-object/from16 v17, v0

    sget-object v18, Landroid/graphics/Typeface;->SERIF:Landroid/graphics/Typeface;

    invoke-virtual/range {v17 .. v18}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/mm/music/LyricView;->NotCurrentPaint:Landroid/graphics/Paint;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/konka/mm/music/LyricView;->TextSize:F

    move/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Landroid/graphics/Paint;->setTextSize(F)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/mm/music/LyricView;->NotCurrentPaint:Landroid/graphics/Paint;

    move-object/from16 v17, v0

    sget-object v18, Landroid/graphics/Typeface;->SERIF:Landroid/graphics/Typeface;

    invoke-virtual/range {v17 .. v18}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/konka/mm/music/LyricView;->index:I

    move/from16 v17, v0

    const/16 v18, -0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-eq v0, v1, :cond_0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/konka/mm/music/LyricView;->currentDunringTime:J

    move-wide/from16 v17, v0

    const-wide/16 v19, 0x0

    cmp-long v17, v17, v19

    if-nez v17, :cond_3

    const/high16 v15, 0x41a00000    # 20.0f

    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/mm/music/LyricView;->lrcList:Ljava/util/List;

    move-object/from16 v17, v0

    if-eqz v17, :cond_8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/mm/music/LyricView;->lrcList:Ljava/util/List;

    move-object/from16 v17, v0

    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v17

    if-lez v17, :cond_8

    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/mm/music/LyricView;->lrcList:Ljava/util/List;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/konka/mm/music/LyricView;->index:I

    move/from16 v18, v0

    invoke-interface/range {v17 .. v18}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/konka/mm/music/LyricContent;

    invoke-virtual/range {v17 .. v17}, Lcom/konka/mm/music/LyricContent;->getLyric()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v7

    const/16 v14, 0x1a

    const/16 v12, 0x2e

    if-le v7, v12, :cond_5

    const-string v17, " "

    move-object/from16 v0, v17

    invoke-virtual {v9, v0, v12}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v8

    const/16 v17, -0x1

    move/from16 v0, v17

    if-eq v8, v0, :cond_4

    move v12, v8

    :goto_2
    move v14, v12

    :cond_2
    :goto_3
    const/4 v13, 0x1

    move-object/from16 v0, p0

    iget v0, v0, Lcom/konka/mm/music/LyricView;->high:F

    move/from16 v17, v0

    const/high16 v18, 0x40000000    # 2.0f

    div-float v16, v17, v18

    if-le v7, v14, :cond_6

    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v9, v0, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p0

    iget v0, v0, Lcom/konka/mm/music/LyricView;->width:F

    move/from16 v17, v0

    const/high16 v18, 0x40000000    # 2.0f

    div-float v17, v17, v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/mm/music/LyricView;->CurrentPaint:Landroid/graphics/Paint;

    move-object/from16 v18, v0

    move-object/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, v16

    move-object/from16 v3, v18

    invoke-virtual {v0, v10, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    move-object/from16 v0, p0

    iget v0, v0, Lcom/konka/mm/music/LyricView;->TextHigh:F

    move/from16 v17, v0

    add-float v16, v16, v17

    add-int/lit8 v13, v13, 0x1

    invoke-virtual {v9, v14, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p0

    iget v0, v0, Lcom/konka/mm/music/LyricView;->width:F

    move/from16 v17, v0

    const/high16 v18, 0x40000000    # 2.0f

    div-float v17, v17, v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/mm/music/LyricView;->CurrentPaint:Landroid/graphics/Paint;

    move-object/from16 v18, v0

    move-object/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, v16

    move-object/from16 v3, v18

    invoke-virtual {v0, v11, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    :goto_4
    move-object/from16 v0, p0

    iget v0, v0, Lcom/konka/mm/music/LyricView;->high:F

    move/from16 v17, v0

    const/high16 v18, 0x40000000    # 2.0f

    div-float v16, v17, v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/konka/mm/music/LyricView;->index:I

    move/from16 v17, v0

    add-int/lit8 v6, v17, -0x1

    :goto_5
    if-gez v6, :cond_7

    move-object/from16 v0, p0

    iget v0, v0, Lcom/konka/mm/music/LyricView;->high:F

    move/from16 v17, v0

    const/high16 v18, 0x40000000    # 2.0f

    div-float v17, v17, v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/konka/mm/music/LyricView;->TextHigh:F

    move/from16 v18, v0

    add-int/lit8 v19, v13, -0x1

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    mul-float v18, v18, v19

    add-float v16, v17, v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/konka/mm/music/LyricView;->index:I

    move/from16 v17, v0

    add-int/lit8 v6, v17, 0x1

    :goto_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/mm/music/LyricView;->lrcList:Ljava/util/List;

    move-object/from16 v17, v0

    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v17

    move/from16 v0, v17

    if-ge v6, v0, :cond_0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/konka/mm/music/LyricView;->TextHigh:F

    move/from16 v17, v0

    add-float v16, v16, v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/mm/music/LyricView;->lrcList:Ljava/util/List;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/konka/mm/music/LyricContent;

    invoke-virtual/range {v17 .. v17}, Lcom/konka/mm/music/LyricContent;->getLyric()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p0

    iget v0, v0, Lcom/konka/mm/music/LyricView;->width:F

    move/from16 v18, v0

    const/high16 v19, 0x40000000    # 2.0f

    div-float v18, v18, v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/mm/music/LyricView;->NotCurrentPaint:Landroid/graphics/Paint;

    move-object/from16 v19, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    move/from16 v2, v18

    move/from16 v3, v16

    move-object/from16 v4, v19

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v6, v6, 0x1

    goto :goto_6

    :cond_3
    const/high16 v17, 0x41a00000    # 20.0f

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/konka/mm/music/LyricView;->currentTime:J

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    long-to-float v0, v0

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/konka/mm/music/LyricView;->sentenctTime:J

    move-wide/from16 v19, v0

    move-wide/from16 v0, v19

    long-to-float v0, v0

    move/from16 v19, v0

    sub-float v18, v18, v19

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/konka/mm/music/LyricView;->currentDunringTime:J

    move-wide/from16 v19, v0

    move-wide/from16 v0, v19

    long-to-float v0, v0

    move/from16 v19, v0

    div-float v18, v18, v19

    const/high16 v19, 0x41a00000    # 20.0f

    mul-float v18, v18, v19

    add-float v15, v17, v18

    goto/16 :goto_1

    :cond_4
    move v12, v7

    goto/16 :goto_2

    :cond_5
    if-gt v7, v12, :cond_2

    if-le v7, v14, :cond_2

    move v14, v7

    goto/16 :goto_3

    :cond_6
    :try_start_1
    move-object/from16 v0, p0

    iget v0, v0, Lcom/konka/mm/music/LyricView;->width:F

    move/from16 v17, v0

    const/high16 v18, 0x40000000    # 2.0f

    div-float v17, v17, v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/mm/music/LyricView;->CurrentPaint:Landroid/graphics/Paint;

    move-object/from16 v18, v0

    move-object/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, v16

    move-object/from16 v3, v18

    invoke-virtual {v0, v9, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_4

    :catch_0
    move-exception v5

    invoke-virtual {v5}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    :cond_7
    :try_start_2
    move-object/from16 v0, p0

    iget v0, v0, Lcom/konka/mm/music/LyricView;->TextHigh:F

    move/from16 v17, v0

    sub-float v16, v16, v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/mm/music/LyricView;->lrcList:Ljava/util/List;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/konka/mm/music/LyricContent;

    invoke-virtual/range {v17 .. v17}, Lcom/konka/mm/music/LyricContent;->getLyric()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p0

    iget v0, v0, Lcom/konka/mm/music/LyricView;->width:F

    move/from16 v18, v0

    const/high16 v19, 0x40000000    # 2.0f

    div-float v18, v18, v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/mm/music/LyricView;->NotCurrentPaint:Landroid/graphics/Paint;

    move-object/from16 v19, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    move/from16 v2, v18

    move/from16 v3, v16

    move-object/from16 v4, v19

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    add-int/lit8 v6, v6, -0x1

    goto/16 :goto_5

    :cond_8
    invoke-virtual/range {p0 .. p0}, Lcom/konka/mm/music/LyricView;->getResources()Landroid/content/res/Resources;

    move-result-object v17

    const v18, 0x7f09005c    # com.konka.mm.R.string.NOLRC

    invoke-virtual/range {v17 .. v18}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p0

    iget v0, v0, Lcom/konka/mm/music/LyricView;->width:F

    move/from16 v18, v0

    const/high16 v19, 0x40000000    # 2.0f

    div-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/konka/mm/music/LyricView;->high:F

    move/from16 v19, v0

    const/high16 v20, 0x40000000    # 2.0f

    div-float v19, v19, v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/mm/music/LyricView;->CurrentPaint:Landroid/graphics/Paint;

    move-object/from16 v20, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    move/from16 v2, v18

    move/from16 v3, v19

    move-object/from16 v4, v20

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    const-wide/16 v17, 0x0

    move-wide/from16 v0, v17

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/konka/mm/music/LyricView;->currentDunringTime:J

    goto/16 :goto_0
.end method

.method protected onSizeChanged(IIII)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/TextView;->onSizeChanged(IIII)V

    int-to-float v0, p2

    iput v0, p0, Lcom/konka/mm/music/LyricView;->high:F

    int-to-float v0, p1

    iput v0, p0, Lcom/konka/mm/music/LyricView;->width:F

    return-void
.end method

.method public setCurrentPaintColor(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/konka/mm/music/LyricView;->CurrentPaintColor:I

    return-void
.end method

.method public setNotCurrentPaintColor(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/konka/mm/music/LyricView;->notCurrentPaintColor:I

    return-void
.end method

.method public setSentenceEntities(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/konka/mm/music/LyricContent;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/konka/mm/music/LyricView;->lrcList:Ljava/util/List;

    return-void
.end method

.method public setTextHigh(F)V
    .locals 0
    .param p1    # F

    iput p1, p0, Lcom/konka/mm/music/LyricView;->TextHigh:F

    return-void
.end method

.method public setTextSize(F)V
    .locals 0
    .param p1    # F

    iput p1, p0, Lcom/konka/mm/music/LyricView;->TextSize:F

    return-void
.end method
