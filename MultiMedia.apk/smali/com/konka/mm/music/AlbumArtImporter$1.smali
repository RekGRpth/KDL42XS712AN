.class Lcom/konka/mm/music/AlbumArtImporter$1;
.super Ljava/lang/Thread;
.source "AlbumArtImporter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/mm/music/AlbumArtImporter;->getAlbumArt(Ljava/lang/String;Ljava/lang/String;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/mm/music/AlbumArtImporter;

.field private final synthetic val$albumName:Ljava/lang/String;

.field private final synthetic val$artistName:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/konka/mm/music/AlbumArtImporter;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/music/AlbumArtImporter$1;->this$0:Lcom/konka/mm/music/AlbumArtImporter;

    iput-object p2, p0, Lcom/konka/mm/music/AlbumArtImporter$1;->val$artistName:Ljava/lang/String;

    iput-object p3, p0, Lcom/konka/mm/music/AlbumArtImporter$1;->val$albumName:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    :try_start_0
    iget-object v1, p0, Lcom/konka/mm/music/AlbumArtImporter$1;->this$0:Lcom/konka/mm/music/AlbumArtImporter;

    iget-object v2, p0, Lcom/konka/mm/music/AlbumArtImporter$1;->val$artistName:Ljava/lang/String;

    iget-object v3, p0, Lcom/konka/mm/music/AlbumArtImporter$1;->val$albumName:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/konka/mm/music/AlbumArtImporter;->getAlbumArtThreadedWorkText(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/mm/music/AlbumArtImporter$1;->this$0:Lcom/konka/mm/music/AlbumArtImporter;

    # getter for: Lcom/konka/mm/music/AlbumArtImporter;->mContext:Lcom/konka/mm/music/MusicActivity;
    invoke-static {v1}, Lcom/konka/mm/music/AlbumArtImporter;->access$0(Lcom/konka/mm/music/AlbumArtImporter;)Lcom/konka/mm/music/MusicActivity;

    move-result-object v1

    iget-object v1, v1, Lcom/konka/mm/music/MusicActivity;->albumHandler:Landroid/os/Handler;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
