.class Lcom/konka/mm/music/MusicListActivity$6;
.super Ljava/lang/Object;
.source "MusicListActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/mm/music/MusicListActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/mm/music/MusicListActivity;


# direct methods
.method constructor <init>(Lcom/konka/mm/music/MusicListActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/music/MusicListActivity$6;->this$0:Lcom/konka/mm/music/MusicListActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    iget-object v4, p0, Lcom/konka/mm/music/MusicListActivity$6;->this$0:Lcom/konka/mm/music/MusicListActivity;

    # getter for: Lcom/konka/mm/music/MusicListActivity;->currentPage:I
    invoke-static {v4}, Lcom/konka/mm/music/MusicListActivity;->access$6(Lcom/konka/mm/music/MusicListActivity;)I

    move-result v4

    iget-object v5, p0, Lcom/konka/mm/music/MusicListActivity$6;->this$0:Lcom/konka/mm/music/MusicListActivity;

    iget v5, v5, Lcom/konka/mm/music/MusicListActivity;->mCountPerPage:I

    mul-int/2addr v4, v5

    add-int v1, v4, p3

    sget-object v4, Lcom/konka/mm/music/MusicListActivity;->musicPaths:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lt v1, v4, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v2, Ljava/io/File;

    sget-object v4, Lcom/konka/mm/music/MusicListActivity;->musicPaths:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-direct {v2, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, p0, Lcom/konka/mm/music/MusicListActivity$6;->this$0:Lcom/konka/mm/music/MusicListActivity;

    iget-object v4, v4, Lcom/konka/mm/music/MusicListActivity;->toast_file_ont_exist:Landroid/widget/Toast;

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_1
    new-instance v3, Landroid/content/Intent;

    iget-object v4, p0, Lcom/konka/mm/music/MusicListActivity$6;->this$0:Lcom/konka/mm/music/MusicListActivity;

    const-class v5, Lcom/konka/mm/music/MusicActivity;

    invoke-direct {v3, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v4, "com.konka.mm.file.root.path"

    iget-object v5, p0, Lcom/konka/mm/music/MusicListActivity$6;->this$0:Lcom/konka/mm/music/MusicListActivity;

    # getter for: Lcom/konka/mm/music/MusicListActivity;->mRootPath:Ljava/lang/String;
    invoke-static {v5}, Lcom/konka/mm/music/MusicListActivity;->access$9(Lcom/konka/mm/music/MusicListActivity;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "com.konka.mm.file.index.posstion"

    invoke-virtual {v0, v4, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v4, "com.konka.mm.file.where.come.from"

    const-string v5, "com.konka.mm.file.come.from.one.disk"

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    iget-object v4, p0, Lcom/konka/mm/music/MusicListActivity$6;->this$0:Lcom/konka/mm/music/MusicListActivity;

    invoke-virtual {v4, v3}, Lcom/konka/mm/music/MusicListActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
