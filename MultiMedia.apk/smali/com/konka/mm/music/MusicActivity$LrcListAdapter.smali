.class Lcom/konka/mm/music/MusicActivity$LrcListAdapter;
.super Landroid/widget/BaseAdapter;
.source "MusicActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/mm/music/MusicActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "LrcListAdapter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/mm/music/MusicActivity;


# direct methods
.method constructor <init>(Lcom/konka/mm/music/MusicActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/music/MusicActivity$LrcListAdapter;->this$0:Lcom/konka/mm/music/MusicActivity;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/music/MusicActivity$LrcListAdapter;->this$0:Lcom/konka/mm/music/MusicActivity;

    iget-object v0, v0, Lcom/konka/mm/music/MusicActivity;->mLrcList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1    # I

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1    # I

    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    if-nez p2, :cond_0

    new-instance v0, Lcom/konka/mm/music/MusicActivity$LrcHolder;

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity$LrcListAdapter;->this$0:Lcom/konka/mm/music/MusicActivity;

    invoke-direct {v0, v1}, Lcom/konka/mm/music/MusicActivity$LrcHolder;-><init>(Lcom/konka/mm/music/MusicActivity;)V

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity$LrcListAdapter;->this$0:Lcom/konka/mm/music/MusicActivity;

    invoke-virtual {v1}, Lcom/konka/mm/music/MusicActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030014    # com.konka.mm.R.layout.lrc_listview_adapter

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    const v1, 0x7f0b0046    # com.konka.mm.R.id.lrc_index

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v0, Lcom/konka/mm/music/MusicActivity$LrcHolder;->index:Landroid/widget/TextView;

    const v1, 0x7f0b0048    # com.konka.mm.R.id.lrc_singer

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v0, Lcom/konka/mm/music/MusicActivity$LrcHolder;->singerTxt:Landroid/widget/TextView;

    const v1, 0x7f0b0047    # com.konka.mm.R.id.lrc_song

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v0, Lcom/konka/mm/music/MusicActivity$LrcHolder;->songTxt:Landroid/widget/TextView;

    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :goto_0
    iget-object v1, v0, Lcom/konka/mm/music/MusicActivity$LrcHolder;->index:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    add-int/lit8 v3, p1, 0x1

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, v0, Lcom/konka/mm/music/MusicActivity$LrcHolder;->singerTxt:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity$LrcListAdapter;->this$0:Lcom/konka/mm/music/MusicActivity;

    iget-object v1, v1, Lcom/konka/mm/music/MusicActivity;->mLrcList:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/konka/mm/music/SearchResult;

    invoke-virtual {v1}, Lcom/konka/mm/music/SearchResult;->getArtist()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, v0, Lcom/konka/mm/music/MusicActivity$LrcHolder;->songTxt:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity$LrcListAdapter;->this$0:Lcom/konka/mm/music/MusicActivity;

    iget-object v1, v1, Lcom/konka/mm/music/MusicActivity;->mLrcList:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/konka/mm/music/SearchResult;

    invoke-virtual {v1}, Lcom/konka/mm/music/SearchResult;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-object p2

    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/konka/mm/music/MusicActivity$LrcHolder;

    goto :goto_0
.end method
