.class public Lcom/konka/mm/music/LrcRead;
.super Ljava/lang/Object;
.source "LrcRead.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/mm/music/LrcRead$Sort;
    }
.end annotation


# instance fields
.field private LyricList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/konka/mm/music/LyricContent;",
            ">;"
        }
    .end annotation
.end field

.field private mLyricContent:Lcom/konka/mm/music/LyricContent;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/konka/mm/music/LyricContent;

    invoke-direct {v0}, Lcom/konka/mm/music/LyricContent;-><init>()V

    iput-object v0, p0, Lcom/konka/mm/music/LrcRead;->mLyricContent:Lcom/konka/mm/music/LyricContent;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/konka/mm/music/LrcRead;->LyricList:Ljava/util/List;

    return-void
.end method

.method private AnalyzeLRC(Ljava/lang/String;)Ljava/lang/String;
    .locals 14
    .param p1    # Ljava/lang/String;

    const-wide/16 v12, -0x1

    const/4 v11, -0x1

    :try_start_0
    const-string v8, "["

    invoke-virtual {p1, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    const-string v8, "]"

    invoke-virtual {p1, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    if-nez v3, :cond_7

    if-eq v4, v11, :cond_7

    invoke-direct {p0, p1}, Lcom/konka/mm/music/LrcRead;->GetPossiblyTagCount(Ljava/lang/String;)I

    move-result v8

    new-array v6, v8, [Ljava/lang/Long;

    const/4 v8, 0x0

    add-int/lit8 v9, v3, 0x1

    invoke-virtual {p1, v9, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v9}, Lcom/konka/mm/music/LrcRead;->TimeToLong(Ljava/lang/String;)J

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    aput-object v9, v6, v8

    const/4 v8, 0x0

    aget-object v8, v6, v8

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v8

    cmp-long v8, v8, v12

    if-nez v8, :cond_1

    const-string v5, ""

    :cond_0
    :goto_0
    return-object v5

    :cond_1
    move-object v5, p1

    const/4 v1, 0x1

    :cond_2
    :goto_1
    if-nez v3, :cond_3

    if-ne v4, v11, :cond_5

    :cond_3
    :try_start_1
    new-instance v7, Lcom/konka/mm/music/LyricContent;

    invoke-direct {v7}, Lcom/konka/mm/music/LyricContent;-><init>()V

    const/4 v2, 0x0

    :goto_2
    array-length v8, v6

    if-ge v2, v8, :cond_0

    aget-object v8, v6, v2

    if-eqz v8, :cond_4

    aget-object v8, v6, v2

    invoke-virtual {v8}, Ljava/lang/Long;->intValue()I

    move-result v8

    invoke-virtual {v7, v8}, Lcom/konka/mm/music/LyricContent;->setLyricTime(I)V

    invoke-virtual {v7, v5}, Lcom/konka/mm/music/LyricContent;->setLyric(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/konka/mm/music/LrcRead;->LyricList:Ljava/util/List;

    invoke-interface {v8, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v7, Lcom/konka/mm/music/LyricContent;

    invoke-direct {v7}, Lcom/konka/mm/music/LyricContent;-><init>()V

    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_5
    add-int/lit8 v8, v4, 0x1

    invoke-virtual {v5, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    const-string v8, "["

    invoke-virtual {v5, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    const-string v8, "]"

    invoke-virtual {v5, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    if-eq v4, v11, :cond_2

    add-int/lit8 v8, v3, 0x1

    invoke-virtual {v5, v8, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, Lcom/konka/mm/music/LrcRead;->TimeToLong(Ljava/lang/String;)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v1

    aget-object v8, v6, v1

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-wide v8

    cmp-long v8, v8, v12

    if-nez v8, :cond_6

    const-string v5, ""

    goto :goto_0

    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_7
    const-string v5, ""

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v5, ""

    goto :goto_0
.end method

.method private GetPossiblyTagCount(Ljava/lang/String;)I
    .locals 4
    .param p1    # Ljava/lang/String;

    const-string v2, "\\["

    invoke-virtual {p1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    const-string v2, "\\]"

    invoke-virtual {p1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    array-length v2, v0

    if-nez v2, :cond_0

    array-length v2, v1

    if-nez v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    array-length v2, v0

    array-length v3, v1

    if-le v2, v3, :cond_1

    array-length v2, v0

    goto :goto_0

    :cond_1
    array-length v2, v1

    goto :goto_0
.end method


# virtual methods
.method public GetCharset(Ljava/io/File;)Ljava/lang/String;
    .locals 13
    .param p1    # Ljava/io/File;

    const/16 v12, 0xbf

    const/16 v11, 0x80

    const/4 v10, -0x1

    const-string v1, "GBK"

    const/4 v8, 0x3

    new-array v5, v8, [B

    const/4 v3, 0x0

    :try_start_0
    new-instance v0, Ljava/io/BufferedInputStream;

    new-instance v8, Ljava/io/FileInputStream;

    invoke-direct {v8, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v0, v8}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    const/4 v8, 0x0

    invoke-virtual {v0, v8}, Ljava/io/BufferedInputStream;->mark(I)V

    const/4 v8, 0x0

    const/4 v9, 0x3

    invoke-virtual {v0, v5, v8, v9}, Ljava/io/BufferedInputStream;->read([BII)I

    move-result v7

    if-ne v7, v10, :cond_0

    move-object v2, v1

    :goto_0
    return-object v2

    :cond_0
    const/4 v8, 0x0

    aget-byte v8, v5, v8

    if-ne v8, v10, :cond_4

    const/4 v8, 0x1

    aget-byte v8, v5, v8

    const/4 v9, -0x2

    if-ne v8, v9, :cond_4

    const-string v1, "UTF-16LE"

    const/4 v3, 0x1

    :cond_1
    :goto_1
    invoke-virtual {v0}, Ljava/io/BufferedInputStream;->reset()V

    if-nez v3, :cond_3

    const/4 v6, 0x0

    :cond_2
    :goto_2
    invoke-virtual {v0}, Ljava/io/BufferedInputStream;->read()I

    move-result v7

    if-ne v7, v10, :cond_6

    :cond_3
    :goto_3
    invoke-virtual {v0}, Ljava/io/BufferedInputStream;->close()V

    :goto_4
    move-object v2, v1

    goto :goto_0

    :cond_4
    const/4 v8, 0x0

    aget-byte v8, v5, v8

    const/4 v9, -0x2

    if-ne v8, v9, :cond_5

    const/4 v8, 0x1

    aget-byte v8, v5, v8

    if-ne v8, v10, :cond_5

    const-string v1, "UTF-16BE"

    const/4 v3, 0x1

    goto :goto_1

    :cond_5
    const/4 v8, 0x0

    aget-byte v8, v5, v8

    const/16 v9, -0x11

    if-ne v8, v9, :cond_1

    const/4 v8, 0x1

    aget-byte v8, v5, v8

    const/16 v9, -0x45

    if-ne v8, v9, :cond_1

    const/4 v8, 0x2

    aget-byte v8, v5, v8

    const/16 v9, -0x41

    if-ne v8, v9, :cond_1

    const-string v1, "UTF-8"

    const/4 v3, 0x1

    goto :goto_1

    :cond_6
    add-int/lit8 v6, v6, 0x1

    const/16 v8, 0xf0

    if-ge v7, v8, :cond_3

    if-gt v11, v7, :cond_7

    if-le v7, v12, :cond_3

    :cond_7
    const/16 v8, 0xc0

    if-gt v8, v7, :cond_8

    const/16 v8, 0xdf

    if-gt v7, v8, :cond_8

    invoke-virtual {v0}, Ljava/io/BufferedInputStream;->read()I

    move-result v7

    if-gt v11, v7, :cond_3

    if-gt v7, v12, :cond_3

    goto :goto_2

    :cond_8
    const/16 v8, 0xe0

    if-gt v8, v7, :cond_2

    const/16 v8, 0xef

    if-gt v7, v8, :cond_2

    invoke-virtual {v0}, Ljava/io/BufferedInputStream;->read()I

    move-result v7

    if-gt v11, v7, :cond_3

    if-gt v7, v12, :cond_3

    invoke-virtual {v0}, Ljava/io/BufferedInputStream;->read()I

    move-result v7

    if-gt v11, v7, :cond_3

    if-gt v7, v12, :cond_3

    const-string v1, "UTF-8"
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    :catch_0
    move-exception v4

    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_4
.end method

.method public GetLyricContent()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/konka/mm/music/LyricContent;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/konka/mm/music/LrcRead;->LyricList:Ljava/util/List;

    return-object v0
.end method

.method public Read(Ljava/io/File;)V
    .locals 17
    .param p1    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;,
            Ljava/io/IOException;
        }
    .end annotation

    const-string v1, ""

    const-string v3, "GBK"

    :try_start_0
    invoke-static/range {p1 .. p1}, Lcom/konka/mm/tools/FileTool;->getFileCharacterEnding(Ljava/io/File;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    :goto_0
    sget-object v14, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v15, Ljava/lang/StringBuilder;

    const-string v16, "charset: "

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v15, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    new-instance v8, Ljava/io/FileInputStream;

    move-object/from16 v0, p1

    invoke-direct {v8, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    new-instance v9, Ljava/io/InputStreamReader;

    invoke-direct {v9, v8, v3}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    new-instance v7, Ljava/io/BufferedReader;

    invoke-direct {v7, v9}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    :cond_0
    :goto_1
    invoke-virtual {v7}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    invoke-virtual {v7}, Ljava/io/BufferedReader;->close()V

    invoke-virtual {v9}, Ljava/io/InputStreamReader;->close()V

    return-void

    :catch_0
    move-exception v4

    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    :cond_1
    :try_start_1
    const-string v14, "]["

    const-string v15, "@"

    invoke-virtual {v1, v14, v15}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    const-string v14, "]"

    const-string v15, "@"

    invoke-virtual {v1, v14, v15}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    const-string v14, "["

    const-string v15, "@"

    invoke-virtual {v1, v14, v15}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    const/4 v14, 0x0

    const/4 v15, 0x1

    invoke-virtual {v1, v14, v15}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v14

    const-string v15, "@"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_2

    const/4 v14, 0x1

    invoke-virtual {v1, v14}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    :cond_2
    const-string v14, "@"

    invoke-virtual {v1, v14}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    array-length v14, v12

    const/4 v15, 0x1

    if-le v14, v15, :cond_3

    array-length v14, v12

    const/4 v15, 0x2

    if-le v14, v15, :cond_9

    const/4 v5, 0x0

    :goto_2
    array-length v14, v12

    if-lt v5, v14, :cond_5

    :cond_3
    :goto_3
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/konka/mm/music/LrcRead;->LyricList:Ljava/util/List;

    new-instance v15, Lcom/konka/mm/music/LrcRead$Sort;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Lcom/konka/mm/music/LrcRead$Sort;-><init>(Lcom/konka/mm/music/LrcRead;)V

    invoke-static {v14, v15}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    const/4 v5, 0x0

    :goto_4
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/konka/mm/music/LrcRead;->LyricList:Ljava/util/List;

    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v14

    if-ge v5, v14, :cond_0

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/konka/mm/music/LrcRead;->LyricList:Ljava/util/List;

    invoke-interface {v14, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/konka/mm/music/LyricContent;

    add-int/lit8 v14, v5, 0x1

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/konka/mm/music/LrcRead;->LyricList:Ljava/util/List;

    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v15

    if-ge v14, v15, :cond_4

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/konka/mm/music/LrcRead;->LyricList:Ljava/util/List;

    add-int/lit8 v15, v5, 0x1

    invoke-interface {v14, v15}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/konka/mm/music/LyricContent;

    invoke-virtual {v13}, Lcom/konka/mm/music/LyricContent;->getLyricTime()I

    move-result v14

    invoke-virtual {v10}, Lcom/konka/mm/music/LyricContent;->getLyricTime()I

    move-result v15

    sub-int/2addr v14, v15

    invoke-virtual {v10, v14}, Lcom/konka/mm/music/LyricContent;->setSleepTime(I)V

    :cond_4
    add-int/lit8 v5, v5, 0x1

    goto :goto_4

    :cond_5
    aget-object v14, v12, v5

    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v14

    const-string v15, "00:00.00"

    invoke-virtual {v15}, Ljava/lang/String;->length()I

    move-result v15

    if-ne v14, v15, :cond_6

    aget-object v14, v12, v5

    const-string v15, ":"

    invoke-virtual {v14, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v14

    if-eqz v14, :cond_6

    aget-object v14, v12, v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/konka/mm/music/LrcRead;->TimeStr(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/konka/mm/music/LrcRead;->mLyricContent:Lcom/konka/mm/music/LyricContent;

    invoke-virtual {v14, v2}, Lcom/konka/mm/music/LyricContent;->setLyricTime(I)V

    move v6, v5

    :goto_5
    array-length v14, v12

    if-lt v5, v14, :cond_7

    :goto_6
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/konka/mm/music/LrcRead;->LyricList:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/konka/mm/music/LrcRead;->mLyricContent:Lcom/konka/mm/music/LyricContent;

    invoke-interface {v14, v15}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v14, Lcom/konka/mm/music/LyricContent;

    invoke-direct {v14}, Lcom/konka/mm/music/LyricContent;-><init>()V

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/konka/mm/music/LrcRead;->mLyricContent:Lcom/konka/mm/music/LyricContent;

    :cond_6
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_2

    :cond_7
    aget-object v11, v12, v6

    aget-object v14, v12, v6

    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v14

    const-string v15, "00:00.00"

    invoke-virtual {v15}, Ljava/lang/String;->length()I

    move-result v15

    if-eq v14, v15, :cond_8

    aget-object v14, v12, v6

    const-string v15, ":"

    invoke-virtual {v14, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v14

    if-nez v14, :cond_8

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/konka/mm/music/LrcRead;->mLyricContent:Lcom/konka/mm/music/LyricContent;

    aget-object v15, v12, v6

    invoke-virtual {v14, v15}, Lcom/konka/mm/music/LyricContent;->setLyric(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_6

    :catch_1
    move-exception v4

    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_1

    :cond_8
    add-int/lit8 v6, v6, 0x1

    goto :goto_5

    :cond_9
    :try_start_2
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/konka/mm/music/LrcRead;->mLyricContent:Lcom/konka/mm/music/LyricContent;

    const/4 v15, 0x1

    aget-object v15, v12, v15

    invoke-virtual {v14, v15}, Lcom/konka/mm/music/LyricContent;->setLyric(Ljava/lang/String;)V

    const/4 v14, 0x0

    aget-object v14, v12, v14

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/konka/mm/music/LrcRead;->TimeStr(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/konka/mm/music/LrcRead;->mLyricContent:Lcom/konka/mm/music/LyricContent;

    invoke-virtual {v14, v2}, Lcom/konka/mm/music/LyricContent;->setLyricTime(I)V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/konka/mm/music/LrcRead;->LyricList:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/konka/mm/music/LrcRead;->mLyricContent:Lcom/konka/mm/music/LyricContent;

    invoke-interface {v14, v15}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v14, Lcom/konka/mm/music/LyricContent;

    invoke-direct {v14}, Lcom/konka/mm/music/LyricContent;-><init>()V

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/konka/mm/music/LrcRead;->mLyricContent:Lcom/konka/mm/music/LyricContent;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_3
.end method

.method public ReadLRC(Ljava/lang/String;)V
    .locals 12
    .param p1    # Ljava/lang/String;

    :try_start_0
    const-string v0, ""

    new-instance v4, Ljava/io/File;

    invoke-direct {v4, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const-string v2, "GBK"

    if-eqz v4, :cond_0

    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v10

    if-nez v10, :cond_2

    :cond_0
    const/4 v10, 0x0

    iput-object v10, p0, Lcom/konka/mm/music/LrcRead;->LyricList:Ljava/util/List;

    :cond_1
    :goto_0
    return-void

    :cond_2
    new-instance v6, Ljava/io/BufferedInputStream;

    new-instance v10, Ljava/io/FileInputStream;

    invoke-direct {v10, v4}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v6, v10}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    new-instance v1, Ljava/io/BufferedReader;

    new-instance v10, Ljava/io/InputStreamReader;

    invoke-virtual {p0, v4}, Lcom/konka/mm/music/LrcRead;->GetCharset(Ljava/io/File;)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v6, v11}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    invoke-direct {v1, v10}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    const-string v8, ""

    :goto_1
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v8

    if-nez v8, :cond_4

    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V

    invoke-virtual {v6}, Ljava/io/InputStream;->close()V

    iget-object v10, p0, Lcom/konka/mm/music/LrcRead;->LyricList:Ljava/util/List;

    new-instance v11, Lcom/konka/mm/music/LrcRead$Sort;

    invoke-direct {v11, p0}, Lcom/konka/mm/music/LrcRead$Sort;-><init>(Lcom/konka/mm/music/LrcRead;)V

    invoke-static {v10, v11}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    const/4 v5, 0x0

    :goto_2
    iget-object v10, p0, Lcom/konka/mm/music/LrcRead;->LyricList:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v10

    if-ge v5, v10, :cond_1

    iget-object v10, p0, Lcom/konka/mm/music/LrcRead;->LyricList:Ljava/util/List;

    invoke-interface {v10, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/konka/mm/music/LyricContent;

    add-int/lit8 v10, v5, 0x1

    iget-object v11, p0, Lcom/konka/mm/music/LrcRead;->LyricList:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v11

    if-ge v10, v11, :cond_3

    iget-object v10, p0, Lcom/konka/mm/music/LrcRead;->LyricList:Ljava/util/List;

    add-int/lit8 v11, v5, 0x1

    invoke-interface {v10, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/konka/mm/music/LyricContent;

    invoke-virtual {v9}, Lcom/konka/mm/music/LyricContent;->getLyricTime()I

    move-result v10

    invoke-virtual {v7}, Lcom/konka/mm/music/LyricContent;->getLyricTime()I

    move-result v11

    sub-int/2addr v10, v11

    invoke-virtual {v7, v10}, Lcom/konka/mm/music/LyricContent;->setSleepTime(I)V

    :cond_3
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    :cond_4
    invoke-direct {p0, v8}, Lcom/konka/mm/music/LrcRead;->AnalyzeLRC(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v8

    goto :goto_1

    :catch_0
    move-exception v3

    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public TimeStr(Ljava/lang/String;)I
    .locals 7
    .param p1    # Ljava/lang/String;

    const-string v5, ":"

    const-string v6, "."

    invoke-virtual {p1, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    const-string v5, "."

    const-string v6, "@"

    invoke-virtual {p1, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    const-string v5, "@"

    invoke-virtual {p1, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    aget-object v5, v4, v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    const/4 v5, 0x1

    aget-object v5, v4, v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    const/4 v5, 0x2

    aget-object v5, v4, v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    mul-int/lit8 v5, v2, 0x3c

    add-int/2addr v5, v3

    mul-int/lit16 v5, v5, 0x3e8

    mul-int/lit8 v6, v1, 0xa

    add-int v0, v5, v6

    return v0
.end method

.method public TimeToLong(Ljava/lang/String;)J
    .locals 9
    .param p1    # Ljava/lang/String;

    const/4 v8, 0x1

    :try_start_0
    const-string v6, ":"

    invoke-virtual {p1, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    const/4 v6, 0x0

    aget-object v6, v3, v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    const/4 v6, 0x1

    aget-object v6, v3, v6

    const-string v7, "\\."

    invoke-virtual {v6, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    const/4 v6, 0x0

    aget-object v6, v4, v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    const/4 v1, 0x0

    array-length v6, v4

    if-le v6, v8, :cond_0

    const/4 v6, 0x1

    aget-object v6, v4, v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :cond_0
    mul-int/lit8 v6, v2, 0x3c

    mul-int/lit16 v6, v6, 0x3e8

    mul-int/lit16 v7, v5, 0x3e8

    add-int/2addr v6, v7

    mul-int/lit8 v7, v1, 0xa

    add-int/2addr v6, v7

    int-to-long v6, v6

    :goto_0
    return-wide v6

    :catch_0
    move-exception v0

    const-wide/16 v6, -0x1

    goto :goto_0
.end method

.method public sortList(Ljava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/konka/mm/music/LyricContent;",
            ">;)V"
        }
    .end annotation

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v6

    if-lez v6, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v5

    const/4 v3, 0x0

    :goto_0
    add-int/lit8 v6, v5, -0x1

    if-lt v3, v6, :cond_1

    :cond_0
    return-void

    :cond_1
    const/4 v4, 0x0

    :goto_1
    sub-int v6, v5, v3

    add-int/lit8 v6, v6, -0x1

    if-lt v4, v6, :cond_2

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/konka/mm/music/LyricContent;

    add-int/lit8 v6, v4, 0x1

    invoke-interface {p1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/konka/mm/music/LyricContent;

    invoke-virtual {v1}, Lcom/konka/mm/music/LyricContent;->getLyricTime()I

    move-result v6

    invoke-virtual {v2}, Lcom/konka/mm/music/LyricContent;->getLyricTime()I

    move-result v7

    if-le v6, v7, :cond_3

    new-instance v0, Lcom/konka/mm/music/LyricContent;

    invoke-direct {v0}, Lcom/konka/mm/music/LyricContent;-><init>()V

    invoke-virtual {v2}, Lcom/konka/mm/music/LyricContent;->getLyric()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Lcom/konka/mm/music/LyricContent;->setLyric(Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/konka/mm/music/LyricContent;->getLyricTime()I

    move-result v6

    invoke-virtual {v0, v6}, Lcom/konka/mm/music/LyricContent;->setLyricTime(I)V

    invoke-virtual {v1}, Lcom/konka/mm/music/LyricContent;->getLyric()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/konka/mm/music/LyricContent;->setLyric(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/konka/mm/music/LyricContent;->getLyricTime()I

    move-result v6

    invoke-virtual {v2, v6}, Lcom/konka/mm/music/LyricContent;->setLyricTime(I)V

    invoke-virtual {v0}, Lcom/konka/mm/music/LyricContent;->getLyric()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/konka/mm/music/LyricContent;->setLyric(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/konka/mm/music/LyricContent;->getLyricTime()I

    move-result v6

    invoke-virtual {v1, v6}, Lcom/konka/mm/music/LyricContent;->setLyricTime(I)V

    :cond_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_1
.end method
