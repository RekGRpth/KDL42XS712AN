.class public Lcom/konka/mm/music/SearchResult;
.super Ljava/lang/Object;
.source "SearchResult.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/mm/music/SearchResult$Task;
    }
.end annotation


# instance fields
.field private artist:Ljava/lang/String;

.field private content:Ljava/lang/String;

.field private lrcCode:Ljava/lang/String;

.field private lrcId:I

.field private task:Lcom/konka/mm/music/SearchResult$Task;

.field private title:Ljava/lang/String;


# direct methods
.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/konka/mm/music/SearchResult$Task;)V
    .locals 0
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Lcom/konka/mm/music/SearchResult$Task;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/konka/mm/music/SearchResult;->lrcId:I

    iput-object p2, p0, Lcom/konka/mm/music/SearchResult;->lrcCode:Ljava/lang/String;

    iput-object p3, p0, Lcom/konka/mm/music/SearchResult;->artist:Ljava/lang/String;

    iput-object p4, p0, Lcom/konka/mm/music/SearchResult;->title:Ljava/lang/String;

    iput-object p5, p0, Lcom/konka/mm/music/SearchResult;->task:Lcom/konka/mm/music/SearchResult$Task;

    return-void
.end method


# virtual methods
.method public getArtist()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/music/SearchResult;->artist:Ljava/lang/String;

    return-object v0
.end method

.method public getContent()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/music/SearchResult;->content:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/konka/mm/music/SearchResult;->task:Lcom/konka/mm/music/SearchResult$Task;

    invoke-interface {v0}, Lcom/konka/mm/music/SearchResult$Task;->getLyricContent()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/mm/music/SearchResult;->content:Ljava/lang/String;

    :cond_0
    iget-object v0, p0, Lcom/konka/mm/music/SearchResult;->content:Ljava/lang/String;

    return-object v0
.end method

.method public getLrcCode()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/music/SearchResult;->lrcCode:Ljava/lang/String;

    return-object v0
.end method

.method public getLrcId()I
    .locals 1

    iget v0, p0, Lcom/konka/mm/music/SearchResult;->lrcId:I

    return v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/music/SearchResult;->title:Ljava/lang/String;

    return-object v0
.end method

.method public save(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/konka/mm/music/SearchResult;->artist:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/mm/music/SearchResult;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
