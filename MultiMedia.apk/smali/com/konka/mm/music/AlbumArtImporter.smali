.class public Lcom/konka/mm/music/AlbumArtImporter;
.super Ljava/lang/Object;
.source "AlbumArtImporter.java"


# instance fields
.field private final TAG:Ljava/lang/String;

.field private mContext:Lcom/konka/mm/music/MusicActivity;

.field private mGetFromInet:Z

.field private mUiProgressHandler:Landroid/os/Handler;

.field private mWorkerThread:Ljava/lang/Thread;


# direct methods
.method constructor <init>(Lcom/konka/mm/music/MusicActivity;Landroid/os/Handler;Z)V
    .locals 5
    .param p1    # Lcom/konka/mm/music/MusicActivity;
    .param p2    # Landroid/os/Handler;
    .param p3    # Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v3, "AlbumArtImporter"

    iput-object v3, p0, Lcom/konka/mm/music/AlbumArtImporter;->TAG:Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/mm/music/AlbumArtImporter;->mContext:Lcom/konka/mm/music/MusicActivity;

    iput-object p2, p0, Lcom/konka/mm/music/AlbumArtImporter;->mUiProgressHandler:Landroid/os/Handler;

    iput-boolean p3, p0, Lcom/konka/mm/music/AlbumArtImporter;->mGetFromInet:Z

    invoke-direct {p0}, Lcom/konka/mm/music/AlbumArtImporter;->createAlbumArtDirectories()V

    :try_start_0
    iget-object v3, p0, Lcom/konka/mm/music/AlbumArtImporter;->mContext:Lcom/konka/mm/music/MusicActivity;

    const-string v4, "connectivity"

    invoke-virtual {v3, v4}, Lcom/konka/mm/music/MusicActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v2

    iget-boolean v3, p0, Lcom/konka/mm/music/AlbumArtImporter;->mGetFromInet:Z

    if-eqz v3, :cond_1

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    const-string v3, "Internet connection unavailable!"

    invoke-direct {p0, v3}, Lcom/konka/mm/music/AlbumArtImporter;->updateUi(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    return-void

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method static synthetic access$0(Lcom/konka/mm/music/AlbumArtImporter;)Lcom/konka/mm/music/MusicActivity;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/music/AlbumArtImporter;->mContext:Lcom/konka/mm/music/MusicActivity;

    return-object v0
.end method

.method private closeUi()V
    .locals 5

    const/4 v4, 0x0

    iget-object v2, p0, Lcom/konka/mm/music/AlbumArtImporter;->mUiProgressHandler:Landroid/os/Handler;

    if-eqz v2, :cond_0

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    new-instance v1, Landroid/os/Message;

    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    invoke-virtual {v0}, Landroid/os/Bundle;->clear()V

    const-string v2, "info_done"

    const-string v3, ""

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iput v4, v1, Landroid/os/Message;->what:I

    iget-object v2, p0, Lcom/konka/mm/music/AlbumArtImporter;->mUiProgressHandler:Landroid/os/Handler;

    invoke-virtual {v2, v4}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v2, p0, Lcom/konka/mm/music/AlbumArtImporter;->mUiProgressHandler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_0
    return-void
.end method

.method private createAlbumArtDirectories()V
    .locals 3

    new-instance v0, Ljava/io/File;

    const-string v2, "/mnt/usb/sda1/albumthumbs/RockOnNg/"

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    new-instance v1, Ljava/io/File;

    const-string v2, "/mnt/usb/sda1/albumthumbs/RockOnNg/small/"

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    return-void
.end method

.method private updateUi(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v2, p0, Lcom/konka/mm/music/AlbumArtImporter;->mUiProgressHandler:Landroid/os/Handler;

    if-eqz v2, :cond_0

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    new-instance v1, Landroid/os/Message;

    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    invoke-virtual {v0}, Landroid/os/Bundle;->clear()V

    const-string v2, "info"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iput v3, v1, Landroid/os/Message;->what:I

    iget-object v2, p0, Lcom/konka/mm/music/AlbumArtImporter;->mUiProgressHandler:Landroid/os/Handler;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v2, p0, Lcom/konka/mm/music/AlbumArtImporter;->mUiProgressHandler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_0
    return-void
.end method


# virtual methods
.method public getAlbumArt(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    new-instance v0, Lcom/konka/mm/music/AlbumArtImporter$1;

    invoke-direct {v0, p0, p1, p2}, Lcom/konka/mm/music/AlbumArtImporter$1;-><init>(Lcom/konka/mm/music/AlbumArtImporter;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/konka/mm/music/AlbumArtImporter;->mWorkerThread:Ljava/lang/Thread;

    iget-object v0, p0, Lcom/konka/mm/music/AlbumArtImporter;->mWorkerThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    const/4 v0, 0x1

    return v0
.end method

.method public getAlbumArtThreadedWork()V
    .locals 19

    new-instance v14, Lcom/konka/mm/music/CursorUtils;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/konka/mm/music/AlbumArtImporter;->mContext:Lcom/konka/mm/music/MusicActivity;

    invoke-direct {v14, v15}, Lcom/konka/mm/music/CursorUtils;-><init>(Landroid/content/Context;)V

    const/4 v15, -0x2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/mm/music/AlbumArtImporter;->mContext:Lcom/konka/mm/music/MusicActivity;

    move-object/from16 v16, v0

    invoke-static/range {v16 .. v16}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v16

    const-string v17, "mPreferArtistSorting"

    const/16 v18, 0x1

    invoke-interface/range {v16 .. v18}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v16

    invoke-virtual/range {v14 .. v16}, Lcom/konka/mm/music/CursorUtils;->getAlbumListFromPlaylist(IZ)Landroid/database/Cursor;

    move-result-object v1

    const-string v14, "Searching album art..."

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/konka/mm/music/AlbumArtImporter;->updateUi(Ljava/lang/String;)V

    new-instance v9, Lcom/konka/mm/music/FreeCoversNetFetcher;

    invoke-direct {v9}, Lcom/konka/mm/music/FreeCoversNetFetcher;-><init>()V

    new-instance v10, Lcom/konka/mm/music/GoogleImagesFetcher;

    invoke-direct {v10}, Lcom/konka/mm/music/GoogleImagesFetcher;-><init>()V

    const/4 v11, 0x0

    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v14

    if-lt v11, v14, :cond_1

    invoke-direct/range {p0 .. p0}, Lcom/konka/mm/music/AlbumArtImporter;->closeUi()V

    :cond_0
    return-void

    :cond_1
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/konka/mm/music/AlbumArtImporter;->mGetFromInet:Z

    if-eqz v14, :cond_2

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v14

    if-nez v14, :cond_0

    :cond_2
    invoke-interface {v1, v11}, Landroid/database/Cursor;->moveToPosition(I)Z

    const/4 v7, 0x0

    const/4 v12, 0x0

    const-string v14, "album_art"

    invoke-interface {v1, v14}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    invoke-interface {v1, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    const-string v14, "artist"

    invoke-interface {v1, v14}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    invoke-interface {v1, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    const-string v14, "album"

    invoke-interface {v1, v14}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    invoke-interface {v1, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string v14, "album_key"

    invoke-interface {v1, v14}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    invoke-interface {v1, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v14, "_id"

    invoke-interface {v1, v14}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    invoke-interface {v1, v14}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const-string v14, "AlbumArtImporter"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v16, " - "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v14, Ljava/lang/StringBuilder;

    add-int/lit8 v15, v11, 0x1

    invoke-static {v15}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v15

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v15, " / "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "\n"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "\n"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/konka/mm/music/AlbumArtImporter;->updateUi(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/konka/mm/music/AlbumArtImporter;->mContext:Lcom/konka/mm/music/MusicActivity;

    invoke-static {v14}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v14

    const-string v15, "mUseAlwaysEmbeddedAlbumArt"

    const/16 v16, 0x1

    invoke-interface/range {v14 .. v16}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v13

    invoke-virtual {v2}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lcom/konka/mm/music/RockOnFileUtils;->validateFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-static {v8, v14}, Lcom/konka/mm/music/AlbumArtUtils;->getAlbumArtPath(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lcom/konka/mm/music/AlbumArtUtils;->getImageSize(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/konka/mm/music/AlbumArtImporter;->mGetFromInet:Z

    if-eqz v14, :cond_7

    if-nez v13, :cond_3

    const/16 v14, 0x100

    if-lt v5, v14, :cond_4

    :cond_3
    if-eqz v13, :cond_7

    if-gtz v5, :cond_7

    :cond_4
    const-string v14, "<unknown>"

    invoke-virtual {v6, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_7

    invoke-static {v6}, Lcom/konka/mm/music/SearchUtils;->filterString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-static {v4}, Lcom/konka/mm/music/SearchUtils;->filterString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v9, v14, v15}, Lcom/konka/mm/music/FreeCoversNetFetcher;->fetch(Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v12

    if-eqz v12, :cond_5

    invoke-virtual {v12}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v14

    const/16 v15, 0x100

    if-lt v14, v15, :cond_5

    invoke-virtual {v12}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v14

    const/16 v15, 0x100

    if-ge v14, v15, :cond_6

    :cond_5
    invoke-static {v6}, Lcom/konka/mm/music/SearchUtils;->filterString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-static {v4}, Lcom/konka/mm/music/SearchUtils;->filterString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v10, v14, v15}, Lcom/konka/mm/music/GoogleImagesFetcher;->fetch(Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v12

    if-eqz v12, :cond_6

    invoke-virtual {v12}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v14

    const/16 v15, 0x100

    if-lt v14, v15, :cond_6

    invoke-virtual {v12}, Landroid/graphics/Bitmap;->getHeight()I

    :cond_6
    if-eqz v12, :cond_7

    invoke-virtual {v2}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lcom/konka/mm/music/RockOnFileUtils;->validateFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-static {v12, v14}, Lcom/konka/mm/music/AlbumArtUtils;->saveAlbumCoverInSdCard(Landroid/graphics/Bitmap;Ljava/lang/String;)Z

    :cond_7
    if-eqz v12, :cond_9

    invoke-virtual {v2}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lcom/konka/mm/music/RockOnFileUtils;->validateFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-static {v12, v14}, Lcom/konka/mm/music/AlbumArtUtils;->saveSmallAlbumCoverInSdCard(Landroid/graphics/Bitmap;Ljava/lang/String;)Z

    invoke-virtual {v12}, Landroid/graphics/Bitmap;->recycle()V

    :cond_8
    :goto_1
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_0

    :cond_9
    if-eqz v8, :cond_8

    invoke-static {v8}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v7

    if-eqz v7, :cond_8

    invoke-virtual {v2}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lcom/konka/mm/music/RockOnFileUtils;->validateFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-static {v7, v14}, Lcom/konka/mm/music/AlbumArtUtils;->saveSmallAlbumCoverInSdCard(Landroid/graphics/Bitmap;Ljava/lang/String;)Z

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_1
.end method

.method public getAlbumArtThreadedWorkText(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    const/16 v5, 0x100

    const/4 v2, 0x0

    new-instance v0, Lcom/konka/mm/music/FreeCoversNetFetcher;

    invoke-direct {v0}, Lcom/konka/mm/music/FreeCoversNetFetcher;-><init>()V

    new-instance v1, Lcom/konka/mm/music/GoogleImagesFetcher;

    invoke-direct {v1}, Lcom/konka/mm/music/GoogleImagesFetcher;-><init>()V

    invoke-static {p1}, Lcom/konka/mm/music/SearchUtils;->filterString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p2}, Lcom/konka/mm/music/SearchUtils;->filterString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lcom/konka/mm/music/FreeCoversNetFetcher;->fetch(Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    if-lt v3, v5, :cond_0

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    if-ge v3, v5, :cond_1

    :cond_0
    invoke-static {p1}, Lcom/konka/mm/music/SearchUtils;->filterString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p2}, Lcom/konka/mm/music/SearchUtils;->filterString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lcom/konka/mm/music/GoogleImagesFetcher;->fetch(Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    if-lt v3, v5, :cond_1

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    :cond_1
    if-eqz v2, :cond_2

    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&  get album art thumt success!"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    invoke-static {p2}, Lcom/konka/mm/music/RockOnFileUtils;->validateFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/konka/mm/music/AlbumArtUtils;->saveAlbumCoverInSdCard(Landroid/graphics/Bitmap;Ljava/lang/String;)Z

    :cond_2
    return-void
.end method

.method public stopAlbumArt()V
    .locals 2

    const-string v0, "AlbumArtImporter"

    const-string v1, "Stopping worker thread"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/mm/music/AlbumArtImporter;->mWorkerThread:Ljava/lang/Thread;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/mm/music/AlbumArtImporter;->mWorkerThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    :cond_0
    return-void
.end method
