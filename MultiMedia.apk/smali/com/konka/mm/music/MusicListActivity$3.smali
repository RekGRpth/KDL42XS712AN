.class Lcom/konka/mm/music/MusicListActivity$3;
.super Ljava/lang/Object;
.source "MusicListActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/mm/music/MusicListActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/mm/music/MusicListActivity;


# direct methods
.method constructor <init>(Lcom/konka/mm/music/MusicListActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/music/MusicListActivity$3;->this$0:Lcom/konka/mm/music/MusicListActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    :goto_0
    return-void

    :sswitch_0
    iget-object v0, p0, Lcom/konka/mm/music/MusicListActivity$3;->this$0:Lcom/konka/mm/music/MusicListActivity;

    invoke-virtual {v0}, Lcom/konka/mm/music/MusicListActivity;->sortBtnOnKeyOrOnClickEvent()V

    goto :goto_0

    :sswitch_1
    sget v0, Lcom/konka/mm/music/MusicDiskActivity;->List_Mode:I

    if-ne v0, v3, :cond_0

    sput v2, Lcom/konka/mm/music/MusicDiskActivity;->List_Mode:I

    :goto_1
    iget-object v0, p0, Lcom/konka/mm/music/MusicListActivity$3;->this$0:Lcom/konka/mm/music/MusicListActivity;

    # invokes: Lcom/konka/mm/music/MusicListActivity;->initGridView()V
    invoke-static {v0}, Lcom/konka/mm/music/MusicListActivity;->access$0(Lcom/konka/mm/music/MusicListActivity;)V

    goto :goto_0

    :cond_0
    sput v3, Lcom/konka/mm/music/MusicDiskActivity;->List_Mode:I

    goto :goto_1

    :sswitch_2
    iget-object v0, p0, Lcom/konka/mm/music/MusicListActivity$3;->this$0:Lcom/konka/mm/music/MusicListActivity;

    invoke-virtual {v0, v1}, Lcom/konka/mm/music/MusicListActivity;->setBtnFocuseFlag(Z)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicListActivity$3;->this$0:Lcom/konka/mm/music/MusicListActivity;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/konka/mm/music/MusicListActivity;->snapScreen(I)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/konka/mm/music/MusicListActivity$3;->this$0:Lcom/konka/mm/music/MusicListActivity;

    invoke-virtual {v0, v1}, Lcom/konka/mm/music/MusicListActivity;->setBtnFocuseFlag(Z)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicListActivity$3;->this$0:Lcom/konka/mm/music/MusicListActivity;

    invoke-virtual {v0, v2}, Lcom/konka/mm/music/MusicListActivity;->snapScreen(I)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/konka/mm/music/MusicListActivity$3;->this$0:Lcom/konka/mm/music/MusicListActivity;

    invoke-virtual {v0}, Lcom/konka/mm/music/MusicListActivity;->updateDatabase()V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0b0019 -> :sswitch_1    # com.konka.mm.R.id.list_btn
        0x7f0b002f -> :sswitch_2    # com.konka.mm.R.id.btn_list_left
        0x7f0b0034 -> :sswitch_3    # com.konka.mm.R.id.btn_list_right
        0x7f0b005b -> :sswitch_4    # com.konka.mm.R.id.btn_update
        0x7f0b005c -> :sswitch_0    # com.konka.mm.R.id.img_sort
    .end sparse-switch
.end method
