.class Lcom/konka/mm/music/MusicActivity$2;
.super Landroid/os/Handler;
.source "MusicActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/mm/music/MusicActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/mm/music/MusicActivity;


# direct methods
.method constructor <init>(Lcom/konka/mm/music/MusicActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/music/MusicActivity$2;->this$0:Lcom/konka/mm/music/MusicActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1    # Landroid/os/Message;

    const/4 v4, 0x0

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity$2;->this$0:Lcom/konka/mm/music/MusicActivity;

    iget-object v1, v1, Lcom/konka/mm/music/MusicActivity;->mSearchDlg:Landroid/app/ProgressDialog;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity$2;->this$0:Lcom/konka/mm/music/MusicActivity;

    iget-object v1, v1, Lcom/konka/mm/music/MusicActivity;->mSearchDlg:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    :cond_0
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity$2;->this$0:Lcom/konka/mm/music/MusicActivity;

    iget-object v1, v1, Lcom/konka/mm/music/MusicActivity;->mLrcAdapter:Lcom/konka/mm/music/MusicActivity$LrcListAdapter;

    invoke-virtual {v1}, Lcom/konka/mm/music/MusicActivity$LrcListAdapter;->notifyDataSetChanged()V

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity$2;->this$0:Lcom/konka/mm/music/MusicActivity;

    iget-object v1, v1, Lcom/konka/mm/music/MusicActivity;->mLrcList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity$2;->this$0:Lcom/konka/mm/music/MusicActivity;

    invoke-virtual {v2}, Lcom/konka/mm/music/MusicActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090079    # com.konka.mm.R.string.lrc_no_found1

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity$2;->this$0:Lcom/konka/mm/music/MusicActivity;

    iget-object v2, v2, Lcom/konka/mm/music/MusicActivity;->mEdit_lrc_song:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity$2;->this$0:Lcom/konka/mm/music/MusicActivity;

    invoke-virtual {v2}, Lcom/konka/mm/music/MusicActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09007a    # com.konka.mm.R.string.lrc_no_found2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity$2;->this$0:Lcom/konka/mm/music/MusicActivity;

    iget-object v1, v1, Lcom/konka/mm/music/MusicActivity;->mTxtNofoundLrc:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity$2;->this$0:Lcom/konka/mm/music/MusicActivity;

    iget-object v1, v1, Lcom/konka/mm/music/MusicActivity;->mTxtNofoundLrc:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity$2;->this$0:Lcom/konka/mm/music/MusicActivity;

    iget-object v1, v1, Lcom/konka/mm/music/MusicActivity;->mTxtNofoundLrc:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    :pswitch_2
    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity$2;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->result:Lcom/konka/mm/music/SearchResult;
    invoke-static {v1}, Lcom/konka/mm/music/MusicActivity;->access$10(Lcom/konka/mm/music/MusicActivity;)Lcom/konka/mm/music/SearchResult;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity$2;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->mAlbumArtImporter:Lcom/konka/mm/music/AlbumArtImporter;
    invoke-static {v1}, Lcom/konka/mm/music/MusicActivity;->access$11(Lcom/konka/mm/music/MusicActivity;)Lcom/konka/mm/music/AlbumArtImporter;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity$2;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->result:Lcom/konka/mm/music/SearchResult;
    invoke-static {v2}, Lcom/konka/mm/music/MusicActivity;->access$10(Lcom/konka/mm/music/MusicActivity;)Lcom/konka/mm/music/SearchResult;

    move-result-object v2

    invoke-virtual {v2}, Lcom/konka/mm/music/SearchResult;->getArtist()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/konka/mm/music/MusicActivity$2;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->result:Lcom/konka/mm/music/SearchResult;
    invoke-static {v3}, Lcom/konka/mm/music/MusicActivity;->access$10(Lcom/konka/mm/music/MusicActivity;)Lcom/konka/mm/music/SearchResult;

    move-result-object v3

    invoke-virtual {v3}, Lcom/konka/mm/music/SearchResult;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/konka/mm/music/AlbumArtImporter;->getAlbumArt(Ljava/lang/String;Ljava/lang/String;)Z

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity$2;->this$0:Lcom/konka/mm/music/MusicActivity;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/konka/mm/music/MusicActivity;->access$12(Lcom/konka/mm/music/MusicActivity;Lcom/konka/mm/music/SearchResult;)V

    :cond_2
    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity$2;->this$0:Lcom/konka/mm/music/MusicActivity;

    iget-object v1, v1, Lcom/konka/mm/music/MusicActivity;->mLrcSearchDlg:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity$2;->this$0:Lcom/konka/mm/music/MusicActivity;

    iget-object v1, v1, Lcom/konka/mm/music/MusicActivity;->mLrcSearchDlg:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->dismiss()V

    :cond_3
    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity$2;->this$0:Lcom/konka/mm/music/MusicActivity;

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity$2;->this$0:Lcom/konka/mm/music/MusicActivity;

    invoke-virtual {v2}, Lcom/konka/mm/music/MusicActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090078    # com.konka.mm.R.string.lrc_load_success

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity$2;->this$0:Lcom/konka/mm/music/MusicActivity;

    # invokes: Lcom/konka/mm/music/MusicActivity;->playNext()V
    invoke-static {v1}, Lcom/konka/mm/music/MusicActivity;->access$13(Lcom/konka/mm/music/MusicActivity;)V

    goto/16 :goto_0

    :pswitch_3
    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity$2;->this$0:Lcom/konka/mm/music/MusicActivity;

    iget-object v1, v1, Lcom/konka/mm/music/MusicActivity;->mLrcSearchDlg:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity$2;->this$0:Lcom/konka/mm/music/MusicActivity;

    iget-object v1, v1, Lcom/konka/mm/music/MusicActivity;->mLrcSearchDlg:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->dismiss()V

    :cond_4
    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity$2;->this$0:Lcom/konka/mm/music/MusicActivity;

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity$2;->this$0:Lcom/konka/mm/music/MusicActivity;

    invoke-virtual {v2}, Lcom/konka/mm/music/MusicActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09007f    # com.konka.mm.R.string.lrc_write_failed

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    :pswitch_4
    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity$2;->this$0:Lcom/konka/mm/music/MusicActivity;

    iget-object v1, v1, Lcom/konka/mm/music/MusicActivity;->mTxtNofoundLrc:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity$2;->this$0:Lcom/konka/mm/music/MusicActivity;

    iget-object v1, v1, Lcom/konka/mm/music/MusicActivity;->mTxtNofoundLrc:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity$2;->this$0:Lcom/konka/mm/music/MusicActivity;

    invoke-virtual {v2}, Lcom/konka/mm/music/MusicActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09007b    # com.konka.mm.R.string.network_connected_failed

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
