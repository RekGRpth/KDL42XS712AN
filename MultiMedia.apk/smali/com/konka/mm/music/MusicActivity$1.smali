.class Lcom/konka/mm/music/MusicActivity$1;
.super Landroid/os/Handler;
.source "MusicActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/mm/music/MusicActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/mm/music/MusicActivity;


# direct methods
.method constructor <init>(Lcom/konka/mm/music/MusicActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/music/MusicActivity$1;->this$0:Lcom/konka/mm/music/MusicActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1    # Landroid/os/Message;

    iget v1, p1, Landroid/os/Message;->what:I

    sparse-switch v1, :sswitch_data_0

    :cond_0
    :goto_0
    :sswitch_0
    return-void

    :sswitch_1
    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity$1;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->proDlg:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/konka/mm/music/MusicActivity;->access$0(Lcom/konka/mm/music/MusicActivity;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity$1;->this$0:Lcom/konka/mm/music/MusicActivity;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/konka/mm/music/MusicActivity;->access$1(Lcom/konka/mm/music/MusicActivity;Landroid/app/ProgressDialog;)V

    goto :goto_0

    :sswitch_2
    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity$1;->this$0:Lcom/konka/mm/music/MusicActivity;

    invoke-virtual {v1}, Lcom/konka/mm/music/MusicActivity;->finish()V

    goto :goto_0

    :sswitch_3
    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity$1;->this$0:Lcom/konka/mm/music/MusicActivity;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Landroid/media/MediaPlayer;

    invoke-static {v2, v1}, Lcom/konka/mm/music/MusicActivity;->access$2(Lcom/konka/mm/music/MusicActivity;Landroid/media/MediaPlayer;)V

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity$1;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->player:Landroid/media/MediaPlayer;
    invoke-static {v1}, Lcom/konka/mm/music/MusicActivity;->access$3(Lcom/konka/mm/music/MusicActivity;)Landroid/media/MediaPlayer;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity$1;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->player:Landroid/media/MediaPlayer;
    invoke-static {v1}, Lcom/konka/mm/music/MusicActivity;->access$3(Lcom/konka/mm/music/MusicActivity;)Landroid/media/MediaPlayer;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity$1;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->completionListener:Landroid/media/MediaPlayer$OnCompletionListener;
    invoke-static {v2}, Lcom/konka/mm/music/MusicActivity;->access$4(Lcom/konka/mm/music/MusicActivity;)Landroid/media/MediaPlayer$OnCompletionListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity$1;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->player:Landroid/media/MediaPlayer;
    invoke-static {v1}, Lcom/konka/mm/music/MusicActivity;->access$3(Lcom/konka/mm/music/MusicActivity;)Landroid/media/MediaPlayer;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity$1;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->errorListener:Landroid/media/MediaPlayer$OnErrorListener;
    invoke-static {v2}, Lcom/konka/mm/music/MusicActivity;->access$5(Lcom/konka/mm/music/MusicActivity;)Landroid/media/MediaPlayer$OnErrorListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity$1;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->player:Landroid/media/MediaPlayer;
    invoke-static {v1}, Lcom/konka/mm/music/MusicActivity;->access$3(Lcom/konka/mm/music/MusicActivity;)Landroid/media/MediaPlayer;

    move-result-object v1

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v0

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity$1;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->IsMusicNotSurport:Z
    invoke-static {v1}, Lcom/konka/mm/music/MusicActivity;->access$6(Lcom/konka/mm/music/MusicActivity;)Z

    move-result v1

    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity$1;->this$0:Lcom/konka/mm/music/MusicActivity;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/konka/mm/music/MusicActivity;->access$7(Lcom/konka/mm/music/MusicActivity;Z)V

    const/4 v0, 0x0

    :cond_1
    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity$1;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->seekBar:Landroid/widget/SeekBar;
    invoke-static {v1}, Lcom/konka/mm/music/MusicActivity;->access$8(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/SeekBar;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/SeekBar;->setMax(I)V

    iget-object v1, p0, Lcom/konka/mm/music/MusicActivity$1;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->time:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/konka/mm/music/MusicActivity;->access$9(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/TextView;

    move-result-object v1

    invoke-static {v0}, Lcom/konka/mm/tools/MusicTool;->calculateDuration(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x21 -> :sswitch_3
    .end sparse-switch
.end method
