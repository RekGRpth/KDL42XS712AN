.class Lcom/konka/mm/music/MusicActivity$18;
.super Ljava/lang/Object;
.source "MusicActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/mm/music/MusicActivity;->setpicGridViewStatus()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/mm/music/MusicActivity;


# direct methods
.method constructor <init>(Lcom/konka/mm/music/MusicActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/music/MusicActivity$18;->this$0:Lcom/konka/mm/music/MusicActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity$18;->this$0:Lcom/konka/mm/music/MusicActivity;

    iget-object v3, p0, Lcom/konka/mm/music/MusicActivity$18;->this$0:Lcom/konka/mm/music/MusicActivity;

    iget v3, v3, Lcom/konka/mm/music/MusicActivity;->currentPage:I

    mul-int/lit8 v3, v3, 0xc

    add-int/2addr v3, p3

    invoke-static {v2, v3}, Lcom/konka/mm/music/MusicActivity;->access$52(Lcom/konka/mm/music/MusicActivity;I)V

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity$18;->this$0:Lcom/konka/mm/music/MusicActivity;

    iget-object v2, v2, Lcom/konka/mm/music/MusicActivity;->musicPaths:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/konka/mm/music/MusicActivity$18;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->curPlay:I
    invoke-static {v3}, Lcom/konka/mm/music/MusicActivity;->access$14(Lcom/konka/mm/music/MusicActivity;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    sget-object v2, Lcom/konka/mm/music/MusicActivity;->sourceFileComeFrom:Ljava/lang/String;

    const-string v3, "com.konka.mm.file.come.from.samba"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity$18;->this$0:Lcom/konka/mm/music/MusicActivity;

    invoke-virtual {v2}, Lcom/konka/mm/music/MusicActivity;->isCanPing()Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity$18;->this$0:Lcom/konka/mm/music/MusicActivity;

    iget-object v3, p0, Lcom/konka/mm/music/MusicActivity$18;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->playBtn:Landroid/widget/Button;
    invoke-static {v3}, Lcom/konka/mm/music/MusicActivity;->access$36(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/Button;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/mm/music/MusicActivity$18;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->pauseBtn:Landroid/widget/Button;
    invoke-static {v4}, Lcom/konka/mm/music/MusicActivity;->access$37(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/Button;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/konka/mm/music/MusicActivity;->setBtnShowHide(Landroid/widget/Button;Landroid/widget/Button;)V

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity$18;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->playPauseTxt:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/konka/mm/music/MusicActivity;->access$41(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/TextView;

    move-result-object v2

    iget-object v3, p0, Lcom/konka/mm/music/MusicActivity$18;->this$0:Lcom/konka/mm/music/MusicActivity;

    invoke-virtual {v3}, Lcom/konka/mm/music/MusicActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090066    # com.konka.mm.R.string.pauseTxt

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/konka/mm/music/MusicActivity$18;->this$0:Lcom/konka/mm/music/MusicActivity;

    invoke-virtual {v3}, Lcom/konka/mm/music/MusicActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f070001    # com.konka.mm.R.array.fileEndingAudio

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/konka/mm/tools/FileTool;->checkEndsWithInStringArray(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity$18;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->toast_file_not_exist:Landroid/widget/Toast;
    invoke-static {v2}, Lcom/konka/mm/music/MusicActivity;->access$53(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/Toast;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/mm/music/MusicActivity$18;->this$0:Lcom/konka/mm/music/MusicActivity;

    invoke-virtual {v4}, Lcom/konka/mm/music/MusicActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090010    # com.konka.mm.R.string.file_not_exist

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity$18;->this$0:Lcom/konka/mm/music/MusicActivity;

    # getter for: Lcom/konka/mm/music/MusicActivity;->toast_file_not_exist:Landroid/widget/Toast;
    invoke-static {v2}, Lcom/konka/mm/music/MusicActivity;->access$53(Lcom/konka/mm/music/MusicActivity;)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    :cond_2
    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity$18;->this$0:Lcom/konka/mm/music/MusicActivity;

    # invokes: Lcom/konka/mm/music/MusicActivity;->initBarInfo(Ljava/lang/String;)V
    invoke-static {v2, v1}, Lcom/konka/mm/music/MusicActivity;->access$15(Lcom/konka/mm/music/MusicActivity;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity$18;->this$0:Lcom/konka/mm/music/MusicActivity;

    const/4 v3, 0x5

    # invokes: Lcom/konka/mm/music/MusicActivity;->playMusicService(ILjava/lang/String;)V
    invoke-static {v2, v3, v1}, Lcom/konka/mm/music/MusicActivity;->access$54(Lcom/konka/mm/music/MusicActivity;ILjava/lang/String;)V

    goto/16 :goto_0

    :cond_3
    iget-object v2, p0, Lcom/konka/mm/music/MusicActivity$18;->this$0:Lcom/konka/mm/music/MusicActivity;

    invoke-static {v2}, Lcom/konka/mm/tools/MusicTool;->formatNotSupport(Landroid/app/Activity;)V

    goto/16 :goto_0
.end method
