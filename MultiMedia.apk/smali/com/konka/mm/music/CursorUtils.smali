.class public Lcom/konka/mm/music/CursorUtils;
.super Ljava/lang/Object;
.source "CursorUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/mm/music/CursorUtils$ArtistAlbumHelper;
    }
.end annotation


# instance fields
.field private final TAG:Ljava/lang/String;

.field ctx:Landroid/content/Context;

.field oAlbumIdColumnIndex:I

.field oArtistColumnIndex:I

.field test:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "CursorUtils"

    iput-object v0, p0, Lcom/konka/mm/music/CursorUtils;->TAG:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/konka/mm/music/CursorUtils;->oAlbumIdColumnIndex:I

    iput-object p1, p0, Lcom/konka/mm/music/CursorUtils;->ctx:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public addSongsToPlaylist(J[J)Z
    .locals 15
    .param p1    # J
    .param p3    # [J

    if-nez p3, :cond_0

    const-string v5, "CursorUtils"

    const-string v6, "ListSelection null"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v5, 0x0

    :goto_0
    return v5

    :cond_0
    move-object/from16 v0, p3

    array-length v11, v0

    new-array v12, v11, [Landroid/content/ContentValues;

    iget-object v5, p0, Lcom/konka/mm/music/CursorUtils;->ctx:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v5, 0x1

    new-array v4, v5, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "count(*)"

    aput-object v6, v4, v5

    invoke-static {}, Lcom/konka/mm/music/DirectoryFilter;->usesExternalStorage()Z

    move-result v5

    if-eqz v5, :cond_1

    const-string v5, "external"

    move-wide/from16 v0, p1

    invoke-static {v5, v0, v1}, Landroid/provider/MediaStore$Audio$Playlists$Members;->getContentUri(Ljava/lang/String;J)Landroid/net/Uri;

    move-result-object v3

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    :goto_1
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    const/4 v5, 0x0

    invoke-interface {v9, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    const/4 v10, 0x0

    :goto_2
    if-lt v10, v11, :cond_2

    invoke-virtual {v2, v3, v12}, Landroid/content/ContentResolver;->bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I

    const/4 v5, 0x1

    goto :goto_0

    :cond_1
    const-string v5, "internal"

    move-wide/from16 v0, p1

    invoke-static {v5, v0, v1}, Landroid/provider/MediaStore$Audio$Playlists$Members;->getContentUri(Ljava/lang/String;J)Landroid/net/Uri;

    move-result-object v3

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    goto :goto_1

    :cond_2
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    aput-object v5, v12, v10

    aget-object v5, v12, v10

    const-string v6, "play_order"

    add-int v7, v8, v10

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    aget-object v5, v12, v10

    const-string v6, "audio_id"

    aget-wide v13, p3, v10

    invoke-static {v13, v14}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    add-int/lit8 v10, v10, 0x1

    goto :goto_2
.end method

.method public clearGenre(I)Z
    .locals 5
    .param p1    # I

    :try_start_0
    invoke-static {}, Lcom/konka/mm/music/DirectoryFilter;->usesExternalStorage()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "external"

    int-to-long v3, p1

    invoke-static {v2, v3, v4}, Landroid/provider/MediaStore$Audio$Genres$Members;->getContentUri(Ljava/lang/String;J)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/mm/music/CursorUtils;->ctx:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v2, v1, v3, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    :goto_0
    const/4 v2, 0x1

    :goto_1
    return v2

    :cond_0
    const-string v2, "internal"

    int-to-long v3, p1

    invoke-static {v2, v3, v4}, Landroid/provider/MediaStore$Audio$Genres$Members;->getContentUri(Ljava/lang/String;J)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/mm/music/CursorUtils;->ctx:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v2, v1, v3, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_1
.end method

.method public clearPlaylist(I)Z
    .locals 5
    .param p1    # I

    :try_start_0
    invoke-static {}, Lcom/konka/mm/music/DirectoryFilter;->usesExternalStorage()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "external"

    int-to-long v3, p1

    invoke-static {v2, v3, v4}, Landroid/provider/MediaStore$Audio$Playlists$Members;->getContentUri(Ljava/lang/String;J)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/mm/music/CursorUtils;->ctx:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v2, v1, v3, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    :goto_0
    const/4 v2, 0x1

    :goto_1
    return v2

    :cond_0
    const-string v2, "internal"

    int-to-long v3, p1

    invoke-static {v2, v3, v4}, Landroid/provider/MediaStore$Audio$Playlists$Members;->getContentUri(Ljava/lang/String;J)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/mm/music/CursorUtils;->ctx:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v2, v1, v3, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_1
.end method

.method public createPlaylist(Ljava/lang/String;)Z
    .locals 4
    .param p1    # Ljava/lang/String;

    :try_start_0
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    const-string v3, "name"

    invoke-virtual {v2, v3, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/mm/music/CursorUtils;->ctx:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {}, Lcom/konka/mm/music/DirectoryFilter;->usesExternalStorage()Z

    move-result v3

    if-eqz v3, :cond_0

    sget-object v3, Landroid/provider/MediaStore$Audio$Playlists;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1, v3, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    :goto_0
    const/4 v3, 0x1

    :goto_1
    return v3

    :cond_0
    sget-object v3, Landroid/provider/MediaStore$Audio$Playlists;->INTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1, v3, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    const/4 v3, 0x0

    goto :goto_1
.end method

.method public deleteGenre(I)Z
    .locals 5
    .param p1    # I

    invoke-virtual {p0, p1}, Lcom/konka/mm/music/CursorUtils;->clearGenre(I)Z

    :try_start_0
    invoke-static {}, Lcom/konka/mm/music/DirectoryFilter;->usesExternalStorage()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/konka/mm/music/CursorUtils;->ctx:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Landroid/provider/MediaStore$Audio$Genres;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "_id="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    :goto_0
    const/4 v1, 0x1

    :goto_1
    return v1

    :cond_0
    iget-object v1, p0, Lcom/konka/mm/music/CursorUtils;->ctx:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Landroid/provider/MediaStore$Audio$Genres;->INTERNAL_CONTENT_URI:Landroid/net/Uri;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "_id="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_1
.end method

.method public deletePlaylist(I)Z
    .locals 6
    .param p1    # I

    const/4 v1, 0x0

    invoke-virtual {p0, p1}, Lcom/konka/mm/music/CursorUtils;->clearPlaylist(I)Z

    move-result v2

    if-eqz v2, :cond_0

    :try_start_0
    invoke-static {}, Lcom/konka/mm/music/DirectoryFilter;->usesExternalStorage()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/konka/mm/music/CursorUtils;->ctx:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Landroid/provider/MediaStore$Audio$Playlists;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "_id="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    :goto_0
    const/4 v1, 0x1

    :cond_0
    :goto_1
    return v1

    :cond_1
    iget-object v2, p0, Lcom/konka/mm/music/CursorUtils;->ctx:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Landroid/provider/MediaStore$Audio$Playlists;->INTERNAL_CONTENT_URI:Landroid/net/Uri;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "_id="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public deleteSong(I)Z
    .locals 7
    .param p1    # I

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x1

    :try_start_0
    new-array v1, v4, [J

    const/4 v4, 0x0

    int-to-long v5, p1

    aput-wide v5, v1, v4

    invoke-virtual {p0, v1}, Lcom/konka/mm/music/CursorUtils;->deleteTracks([J)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    move v2, v3

    goto :goto_0
.end method

.method public deleteTracks([J)V
    .locals 18
    .param p1    # [J

    const/4 v1, 0x3

    new-array v3, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v3, v1

    const/4 v1, 0x1

    const-string v2, "_data"

    aput-object v2, v3, v1

    const/4 v1, 0x2

    const-string v2, "album_id"

    aput-object v2, v3, v1

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "_id IN ("

    move-object/from16 v0, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v13, 0x0

    :goto_0
    move-object/from16 v0, p1

    array-length v1, v0

    if-lt v13, v1, :cond_1

    const-string v1, ")"

    move-object/from16 v0, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v9, 0x0

    invoke-static {}, Lcom/konka/mm/music/DirectoryFilter;->usesExternalStorage()Z

    move-result v11

    if-eqz v11, :cond_3

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/mm/music/CursorUtils;->ctx:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    :goto_1
    if-eqz v9, :cond_0

    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    :goto_2
    invoke-interface {v9}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v1

    if-eqz v1, :cond_4

    if-eqz v11, :cond_5

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/mm/music/CursorUtils;->ctx:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v1, v2, v4, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    :goto_3
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    :goto_4
    invoke-interface {v9}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/mm/music/CursorUtils;->ctx:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "content://media"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v4}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    return-void

    :cond_1
    aget-wide v1, p1, v13

    move-object/from16 v0, v17

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-object/from16 v0, p1

    array-length v1, v0

    add-int/lit8 v1, v1, -0x1

    if-ge v13, v1, :cond_2

    const-string v1, ","

    move-object/from16 v0, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    add-int/lit8 v13, v13, 0x1

    goto :goto_0

    :cond_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/mm/music/CursorUtils;->ctx:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Landroid/provider/MediaStore$Audio$Media;->INTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    goto :goto_1

    :cond_4
    const/4 v1, 0x0

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    const/4 v1, 0x2

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_2

    :cond_5
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/mm/music/CursorUtils;->ctx:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Landroid/provider/MediaStore$Audio$Media;->INTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v1, v2, v4, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_3

    :cond_6
    const/4 v1, 0x1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    new-instance v12, Ljava/io/File;

    move-object/from16 v0, v16

    invoke-direct {v12, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    :try_start_0
    invoke-virtual {v12}, Ljava/io/File;->delete()Z

    move-result v1

    if-nez v1, :cond_7

    const-string v1, "CursorUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "Failed to delete file "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_7
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_4

    :catch_0
    move-exception v10

    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    goto/16 :goto_4
.end method

.method public fillArtistAlbumHelperArray(Landroid/database/Cursor;[Lcom/konka/mm/music/CursorUtils$ArtistAlbumHelper;)Z
    .locals 5
    .param p1    # Landroid/database/Cursor;
    .param p2    # [Lcom/konka/mm/music/CursorUtils$ArtistAlbumHelper;

    invoke-interface {p1}, Landroid/database/Cursor;->isClosed()Z

    move-result v3

    if-nez v3, :cond_5

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v3

    array-length v4, p2

    if-ne v3, v4, :cond_5

    const-string v3, "_id"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/konka/mm/music/CursorUtils;->oArtistColumnIndex:I

    const/4 v2, 0x0

    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-lt v2, v3, :cond_0

    const/4 v3, 0x1

    :goto_1
    return v3

    :cond_0
    :try_start_0
    invoke-interface {p1, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    aget-object v3, p2, v2

    if-nez v3, :cond_1

    new-instance v3, Lcom/konka/mm/music/CursorUtils$ArtistAlbumHelper;

    invoke-direct {v3, p0}, Lcom/konka/mm/music/CursorUtils$ArtistAlbumHelper;-><init>(Lcom/konka/mm/music/CursorUtils;)V

    aput-object v3, p2, v2

    :cond_1
    aget-object v3, p2, v2

    iget v4, p0, Lcom/konka/mm/music/CursorUtils;->oArtistColumnIndex:I

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/konka/mm/music/CursorUtils$ArtistAlbumHelper;->artistId:Ljava/lang/String;

    iget v3, p0, Lcom/konka/mm/music/CursorUtils;->oArtistColumnIndex:I

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-virtual {p0, v3, v4}, Lcom/konka/mm/music/CursorUtils;->getAlbumListFromArtistId(J)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/database/CursorIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/database/StaleDataException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v0

    :goto_2
    if-eqz v0, :cond_4

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-lez v3, :cond_3

    iget v3, p0, Lcom/konka/mm/music/CursorUtils;->oAlbumIdColumnIndex:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_2

    const-string v3, "_id"

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/konka/mm/music/CursorUtils;->oAlbumIdColumnIndex:I

    :cond_2
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    aget-object v3, p2, v2

    iget v4, p0, Lcom/konka/mm/music/CursorUtils;->oAlbumIdColumnIndex:I

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/konka/mm/music/CursorUtils$ArtistAlbumHelper;->albumId:Ljava/lang/String;

    :goto_3
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :goto_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/IllegalStateException;->printStackTrace()V

    const/4 v0, 0x0

    goto :goto_2

    :catch_1
    move-exception v1

    invoke-virtual {v1}, Landroid/database/CursorIndexOutOfBoundsException;->printStackTrace()V

    const/4 v0, 0x0

    goto :goto_2

    :catch_2
    move-exception v1

    invoke-virtual {v1}, Landroid/database/StaleDataException;->printStackTrace()V

    const/4 v0, 0x0

    goto :goto_2

    :cond_3
    aget-object v3, p2, v2

    const-string v4, ""

    iput-object v4, v3, Lcom/konka/mm/music/CursorUtils$ArtistAlbumHelper;->albumId:Ljava/lang/String;

    goto :goto_3

    :cond_4
    aget-object v3, p2, v2

    const-string v4, ""

    iput-object v4, v3, Lcom/konka/mm/music/CursorUtils$ArtistAlbumHelper;->albumId:Ljava/lang/String;

    goto :goto_4

    :cond_5
    const/4 v3, 0x0

    goto :goto_1
.end method

.method getAlbumFromAlbumId(J)Landroid/database/Cursor;
    .locals 7
    .param p1    # J

    const/4 v4, 0x0

    iget-object v1, p0, Lcom/konka/mm/music/CursorUtils;->ctx:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v6, 0x0

    invoke-static {}, Lcom/konka/mm/music/DirectoryFilter;->usesExternalStorage()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Landroid/provider/MediaStore$Audio$Albums;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/konka/mm/music/Constants;->albumProjection:[Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "_id = "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v5, "album_key ASC"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    :goto_0
    return-object v6

    :cond_0
    sget-object v1, Landroid/provider/MediaStore$Audio$Albums;->INTERNAL_CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/konka/mm/music/Constants;->albumProjection:[Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "_id = "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v5, "album_key ASC"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    goto :goto_0
.end method

.method getAlbumListFromArtistId(J)Landroid/database/Cursor;
    .locals 7
    .param p1    # J

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/konka/mm/music/CursorUtils;->ctx:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v6, 0x0

    invoke-static {}, Lcom/konka/mm/music/DirectoryFilter;->usesExternalStorage()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "external"

    invoke-static {v1, p1, p2}, Landroid/provider/MediaStore$Audio$Artists$Albums;->getContentUri(Ljava/lang/String;J)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/konka/mm/music/Constants;->albumProjection:[Ljava/lang/String;

    const-string v5, "maxyear DESC"

    move-object v4, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    :goto_0
    return-object v6

    :cond_0
    const-string v1, "internal"

    invoke-static {v1, p1, p2}, Landroid/provider/MediaStore$Audio$Artists$Albums;->getContentUri(Ljava/lang/String;J)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/konka/mm/music/Constants;->albumProjection:[Ljava/lang/String;

    const-string v5, "maxyear DESC"

    move-object v4, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    goto :goto_0
.end method

.method getAlbumListFromArtistId(JI)Landroid/database/Cursor;
    .locals 1
    .param p1    # J
    .param p3    # I

    packed-switch p3, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    invoke-virtual {p0, p1, p2}, Lcom/konka/mm/music/CursorUtils;->getAlbumListFromArtistId(J)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_0
    .end packed-switch
.end method

.method getAlbumListFromPlaylist(IZ)Landroid/database/Cursor;
    .locals 7
    .param p1    # I
    .param p2    # Z

    const/4 v3, 0x0

    const/4 v1, -0x2

    if-ne v1, p1, :cond_3

    iget-object v1, p0, Lcom/konka/mm/music/CursorUtils;->ctx:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v6, 0x0

    invoke-static {}, Lcom/konka/mm/music/DirectoryFilter;->usesExternalStorage()Z

    move-result v1

    if-eqz v1, :cond_1

    if-eqz p2, :cond_0

    sget-object v1, Landroid/provider/MediaStore$Audio$Albums;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/konka/mm/music/Constants;->albumProjection:[Ljava/lang/String;

    const-string v5, "artist COLLATE NOCASE ASC, maxyear DESC"

    move-object v4, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    :goto_0
    return-object v6

    :cond_0
    sget-object v1, Landroid/provider/MediaStore$Audio$Albums;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/konka/mm/music/Constants;->albumProjection:[Ljava/lang/String;

    const-string v5, "album_key ASC"

    move-object v4, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    goto :goto_0

    :cond_1
    if-eqz p2, :cond_2

    sget-object v1, Landroid/provider/MediaStore$Audio$Albums;->INTERNAL_CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/konka/mm/music/Constants;->albumProjection:[Ljava/lang/String;

    const-string v5, "artist COLLATE NOCASE ASC, maxyear DESC"

    move-object v4, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    goto :goto_0

    :cond_2
    sget-object v1, Landroid/provider/MediaStore$Audio$Albums;->INTERNAL_CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/konka/mm/music/Constants;->albumProjection:[Ljava/lang/String;

    const-string v5, "album_key ASC"

    move-object v4, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    goto :goto_0

    :cond_3
    move-object v6, v3

    goto :goto_0
.end method

.method getAllSongsFromPlaylist(I)Landroid/database/Cursor;
    .locals 2
    .param p1    # I

    const-string v0, "is_music=1"

    const/4 v1, 0x1

    invoke-virtual {p0, p1, v0, v1}, Lcom/konka/mm/music/CursorUtils;->getSongsFromPlaylistWithConstraint(ILjava/lang/String;Z)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method getAllSongsListOrderedBySongTitle()Landroid/database/Cursor;
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/konka/mm/music/CursorUtils;->getSongListOrderedBySongTitle(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method getArtistListFromPlaylist(I)Landroid/database/Cursor;
    .locals 7
    .param p1    # I

    const/4 v3, 0x0

    const/4 v1, -0x2

    if-ne v1, p1, :cond_1

    iget-object v1, p0, Lcom/konka/mm/music/CursorUtils;->ctx:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v6, 0x0

    invoke-static {}, Lcom/konka/mm/music/DirectoryFilter;->usesExternalStorage()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Landroid/provider/MediaStore$Audio$Artists;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/konka/mm/music/Constants;->artistProjection:[Ljava/lang/String;

    const-string v5, "artist_key ASC"

    move-object v4, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    :goto_0
    return-object v6

    :cond_0
    sget-object v1, Landroid/provider/MediaStore$Audio$Artists;->INTERNAL_CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/konka/mm/music/Constants;->artistProjection:[Ljava/lang/String;

    const-string v5, "artist_key ASC"

    move-object v4, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    goto :goto_0

    :cond_1
    move-object v6, v3

    goto :goto_0
.end method

.method getGenres()Landroid/database/Cursor;
    .locals 9

    const/4 v8, 0x0

    const/4 v6, 0x0

    :try_start_0
    invoke-static {}, Lcom/konka/mm/music/DirectoryFilter;->usesExternalStorage()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/mm/music/CursorUtils;->ctx:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Audio$Genres;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/konka/mm/music/Constants;->genreProjection:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, "name COLLATE NOCASE ASC"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    :goto_0
    return-object v6

    :cond_0
    iget-object v0, p0, Lcom/konka/mm/music/CursorUtils;->ctx:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Audio$Genres;->INTERNAL_CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/konka/mm/music/Constants;->genreProjection:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, "name COLLATE NOCASE ASC"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    goto :goto_0

    :catch_0
    move-exception v7

    invoke-virtual {v7}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    move-object v6, v8

    goto :goto_0
.end method

.method getNextPrevAlbumId(IJII)J
    .locals 9
    .param p1    # I
    .param p2    # J
    .param p4    # I
    .param p5    # I

    const/4 v7, 0x1

    const-wide/16 v2, -0x1

    if-nez p5, :cond_7

    const-string v5, "CursorUtils"

    const-string v6, "Fetching next album - SHUFFLE_NONE"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, p4, v7}, Lcom/konka/mm/music/CursorUtils;->getAlbumListFromPlaylist(IZ)Landroid/database/Cursor;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v1, 0x0

    :goto_0
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v5

    if-lt v1, v5, :cond_3

    :cond_0
    if-ne p1, v7, :cond_5

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_4

    add-int/lit8 v5, v1, 0x1

    invoke-interface {v0, v5}, Landroid/database/Cursor;->moveToPosition(I)Z

    :cond_1
    :goto_1
    const-string v5, "_id"

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_2
    :goto_2
    return-wide v2

    :cond_3
    invoke-interface {v0, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    const-string v5, "_id"

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    cmp-long v5, p2, v5

    if-eqz v5, :cond_0

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_4
    const/4 v5, 0x0

    invoke-interface {v0, v5}, Landroid/database/Cursor;->moveToPosition(I)Z

    goto :goto_1

    :cond_5
    if-nez p1, :cond_1

    if-lez v1, :cond_6

    add-int/lit8 v5, v1, -0x1

    invoke-interface {v0, v5}, Landroid/database/Cursor;->moveToPosition(I)Z

    goto :goto_1

    :cond_6
    invoke-interface {v0}, Landroid/database/Cursor;->moveToLast()Z

    goto :goto_1

    :cond_7
    if-eq p5, v7, :cond_8

    const/4 v5, 0x2

    if-ne p5, v5, :cond_2

    :cond_8
    invoke-virtual {p0, p4, v7}, Lcom/konka/mm/music/CursorUtils;->getAlbumListFromPlaylist(IZ)Landroid/database/Cursor;

    move-result-object v0

    move-wide v2, p2

    const/4 v4, 0x0

    :goto_3
    cmp-long v5, v2, p2

    if-nez v5, :cond_9

    const/16 v5, 0xa

    if-lt v4, v5, :cond_a

    :cond_9
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_2

    :cond_a
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v5

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    int-to-double v7, v7

    mul-double/2addr v5, v7

    double-to-int v5, v5

    invoke-interface {v0, v5}, Landroid/database/Cursor;->moveToPosition(I)Z

    const-string v5, "_id"

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    add-int/lit8 v4, v4, 0x1

    goto :goto_3
.end method

.method getNextPrevAudioId(IJIII)J
    .locals 13
    .param p1    # I
    .param p2    # J
    .param p4    # I
    .param p5    # I
    .param p6    # I

    const-wide/16 v10, -0x1

    const/4 v1, 0x1

    move/from16 v0, p5

    if-eq v0, v1, :cond_0

    const/4 v1, 0x2

    move/from16 v0, p5

    if-ne v0, v1, :cond_5

    :cond_0
    move/from16 v0, p6

    invoke-virtual {p0, v0}, Lcom/konka/mm/music/CursorUtils;->getAllSongsFromPlaylist(I)Landroid/database/Cursor;

    move-result-object v7

    const/4 v12, 0x0

    move-wide v10, p2

    :goto_0
    cmp-long v1, v10, p2

    if-nez v1, :cond_1

    const/16 v1, 0x14

    if-le v12, v1, :cond_4

    :cond_1
    const/16 v1, 0x14

    if-ne v12, v1, :cond_2

    const-wide/16 v10, -0x1

    :cond_2
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_3
    :goto_1
    return-wide v10

    :cond_4
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v1

    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    int-to-double v3, v3

    mul-double/2addr v1, v3

    double-to-int v1, v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    invoke-static {v7}, Lcom/konka/mm/music/ContentProviderUnifier;->getAudioIdFromUnknownCursor(Landroid/database/Cursor;)J

    move-result-wide v10

    goto :goto_0

    :cond_5
    if-nez p5, :cond_3

    move/from16 v0, p4

    int-to-long v1, v0

    move/from16 v0, p6

    invoke-virtual {p0, v1, v2, v0}, Lcom/konka/mm/music/CursorUtils;->getSongListCursorFromAlbumId(JI)Landroid/database/Cursor;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v8, 0x0

    :goto_2
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lt v8, v1, :cond_7

    :cond_6
    const/4 v1, 0x1

    if-ne p1, v1, :cond_9

    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v8, v1, :cond_8

    add-int/lit8 v1, v8, 0x1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    invoke-static {v7}, Lcom/konka/mm/music/ContentProviderUnifier;->getAudioIdFromUnknownCursor(Landroid/database/Cursor;)J

    move-result-wide v10

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_1

    :cond_7
    invoke-interface {v7, v8}, Landroid/database/Cursor;->moveToPosition(I)Z

    invoke-static {v7}, Lcom/konka/mm/music/ContentProviderUnifier;->getAudioIdFromUnknownCursor(Landroid/database/Cursor;)J

    move-result-wide v1

    cmp-long v1, p2, v1

    if-eqz v1, :cond_6

    add-int/lit8 v8, v8, 0x1

    goto :goto_2

    :cond_8
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    move/from16 v0, p4

    int-to-long v3, v0

    move-object v1, p0

    move v2, p1

    move/from16 v5, p6

    move/from16 v6, p5

    invoke-virtual/range {v1 .. v6}, Lcom/konka/mm/music/CursorUtils;->getNextPrevAlbumId(IJII)J

    move-result-wide v1

    move/from16 v0, p6

    invoke-virtual {p0, v1, v2, v0}, Lcom/konka/mm/music/CursorUtils;->getSongListCursorFromAlbumId(JI)Landroid/database/Cursor;

    move-result-object v9

    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    invoke-static {v9}, Lcom/konka/mm/music/ContentProviderUnifier;->getAudioIdFromUnknownCursor(Landroid/database/Cursor;)J

    move-result-wide v10

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto :goto_1

    :cond_9
    if-nez p1, :cond_3

    if-lez v8, :cond_a

    add-int/lit8 v1, v8, -0x1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    invoke-static {v7}, Lcom/konka/mm/music/ContentProviderUnifier;->getAudioIdFromUnknownCursor(Landroid/database/Cursor;)J

    move-result-wide v10

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_1

    :cond_a
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    move/from16 v0, p4

    int-to-long v3, v0

    move-object v1, p0

    move v2, p1

    move/from16 v5, p6

    move/from16 v6, p5

    invoke-virtual/range {v1 .. v6}, Lcom/konka/mm/music/CursorUtils;->getNextPrevAlbumId(IJII)J

    move-result-wide v1

    move/from16 v0, p6

    invoke-virtual {p0, v1, v2, v0}, Lcom/konka/mm/music/CursorUtils;->getSongListCursorFromAlbumId(JI)Landroid/database/Cursor;

    move-result-object v9

    invoke-interface {v9}, Landroid/database/Cursor;->moveToLast()Z

    invoke-static {v9}, Lcom/konka/mm/music/ContentProviderUnifier;->getAudioIdFromUnknownCursor(Landroid/database/Cursor;)J

    move-result-wide v10

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto/16 :goto_1
.end method

.method public getPlaylistIdFromName(Ljava/lang/String;)J
    .locals 7
    .param p1    # Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v1, p0, Lcom/konka/mm/music/CursorUtils;->ctx:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v6, 0x0

    invoke-static {}, Lcom/konka/mm/music/DirectoryFilter;->usesExternalStorage()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Landroid/provider/MediaStore$Audio$Playlists;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/konka/mm/music/Constants;->playlistProjection:[Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "name=\'"

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "\'"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    const-string v1, "_id"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    return-wide v1

    :cond_0
    sget-object v1, Landroid/provider/MediaStore$Audio$Playlists;->INTERNAL_CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/konka/mm/music/Constants;->playlistProjection:[Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "name=\'"

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "\'"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    goto :goto_0
.end method

.method getPlaylists()Landroid/database/Cursor;
    .locals 9

    const/4 v8, 0x0

    const/4 v6, 0x0

    :try_start_0
    invoke-static {}, Lcom/konka/mm/music/DirectoryFilter;->usesExternalStorage()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/mm/music/CursorUtils;->ctx:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Audio$Playlists;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/konka/mm/music/Constants;->playlistProjection:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, "name COLLATE NOCASE ASC"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    :goto_0
    return-object v6

    :cond_0
    iget-object v0, p0, Lcom/konka/mm/music/CursorUtils;->ctx:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Audio$Playlists;->INTERNAL_CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/konka/mm/music/Constants;->playlistProjection:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, "name COLLATE NOCASE ASC"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    goto :goto_0

    :catch_0
    move-exception v7

    invoke-virtual {v7}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    move-object v6, v8

    goto :goto_0
.end method

.method getSongListCursorFromAlbumAndArtistId(JJI)Landroid/database/Cursor;
    .locals 2
    .param p1    # J
    .param p3    # J
    .param p5    # I

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "album_id = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "artist_id"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "is_music"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "=1"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, p5, v0, v1}, Lcom/konka/mm/music/CursorUtils;->getSongsFromPlaylistWithConstraint(ILjava/lang/String;Z)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method getSongListCursorFromAlbumId(JI)Landroid/database/Cursor;
    .locals 2
    .param p1    # J
    .param p3    # I

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "album_id = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "is_music"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "=1"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, p3, v0, v1}, Lcom/konka/mm/music/CursorUtils;->getSongsFromPlaylistWithConstraint(ILjava/lang/String;Z)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method getSongListCursorFromArtistId(JI)Landroid/database/Cursor;
    .locals 2
    .param p1    # J
    .param p3    # I

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "artist_id = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "is_music"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "=1"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, p3, v0, v1}, Lcom/konka/mm/music/CursorUtils;->getSongsFromPlaylistWithConstraint(ILjava/lang/String;Z)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method getSongListCursorFromArtistName(Ljava/lang/String;I)Landroid/database/Cursor;
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # I

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "artist = \'"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "is_music"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "=1"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, p2, v0, v1}, Lcom/konka/mm/music/CursorUtils;->getSongsFromPlaylistWithConstraint(ILjava/lang/String;Z)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method getSongListCursorFromSongList([JII)Landroid/database/Cursor;
    .locals 12
    .param p1    # [J
    .param p2    # I
    .param p3    # I

    const/4 v4, 0x0

    if-eqz p1, :cond_3

    array-length v0, p1

    if-lez v0, :cond_3

    array-length v0, p1

    sub-int/2addr v0, p2

    add-int v1, p2, p3

    add-int/lit8 v1, v1, 0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    new-array v6, v0, [Landroid/database/Cursor;

    invoke-static {}, Lcom/konka/mm/music/DirectoryFilter;->usesExternalStorage()Z

    move-result v7

    move v8, p2

    :goto_0
    array-length v0, p1

    add-int v1, p2, p3

    add-int/lit8 v1, v1, 0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    if-lt v8, v0, :cond_1

    const/4 v9, 0x0

    if-eqz v6, :cond_0

    new-instance v9, Landroid/database/MergeCursor;

    invoke-direct {v9, v6}, Landroid/database/MergeCursor;-><init>([Landroid/database/Cursor;)V

    :cond_0
    :goto_1
    return-object v9

    :cond_1
    const-string v0, "CursorUtils"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    aget-wide v10, p1, v8

    invoke-virtual {v1, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "_id="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-wide v1, p1, v8

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    if-eqz v7, :cond_2

    sub-int v10, v8, p2

    iget-object v0, p0, Lcom/konka/mm/music/CursorUtils;->ctx:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/konka/mm/music/Constants;->songProjection:[Ljava/lang/String;

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    aput-object v0, v6, v10

    :goto_2
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    :cond_2
    sub-int v10, v8, p2

    iget-object v0, p0, Lcom/konka/mm/music/CursorUtils;->ctx:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->INTERNAL_CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/konka/mm/music/Constants;->songProjection:[Ljava/lang/String;

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    aput-object v0, v6, v10

    goto :goto_2

    :cond_3
    move-object v9, v4

    goto :goto_1
.end method

.method getSongListOrderedBySongTitle(Ljava/lang/String;)Landroid/database/Cursor;
    .locals 8
    .param p1    # Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v6, 0x0

    invoke-static {}, Lcom/konka/mm/music/DirectoryFilter;->usesExternalStorage()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/konka/mm/music/CursorUtils;->ctx:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/konka/mm/music/DirectoryFilter;->getFolderSqlStatement(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/konka/mm/music/CursorUtils;->ctx:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/konka/mm/music/Constants;->songProjection:[Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, " AND ("

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ")"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v5, "title_key ASC"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    :goto_0
    return-object v6

    :cond_0
    iget-object v0, p0, Lcom/konka/mm/music/CursorUtils;->ctx:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/konka/mm/music/Constants;->songProjection:[Ljava/lang/String;

    const-string v5, "title_key ASC"

    move-object v3, v7

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/konka/mm/music/CursorUtils;->ctx:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/konka/mm/music/Constants;->songProjection:[Ljava/lang/String;

    const-string v5, "title_key ASC"

    move-object v3, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/konka/mm/music/CursorUtils;->ctx:Landroid/content/Context;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/konka/mm/music/DirectoryFilter;->getFolderSqlStatement(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_4

    if-eqz p1, :cond_3

    iget-object v0, p0, Lcom/konka/mm/music/CursorUtils;->ctx:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->INTERNAL_CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/konka/mm/music/Constants;->songProjection:[Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, " AND ("

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ")"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v5, "title_key ASC"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/konka/mm/music/CursorUtils;->ctx:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->INTERNAL_CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/konka/mm/music/Constants;->songProjection:[Ljava/lang/String;

    const-string v5, "title_key ASC"

    move-object v3, v7

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/konka/mm/music/CursorUtils;->ctx:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->INTERNAL_CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/konka/mm/music/Constants;->songProjection:[Ljava/lang/String;

    const-string v5, "title_key ASC"

    move-object v3, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    goto/16 :goto_0
.end method

.method getSongsFromPlaylistWithConstraint(ILjava/lang/String;Z)Landroid/database/Cursor;
    .locals 20
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # Z

    const/16 v19, 0x0

    invoke-static {}, Lcom/konka/mm/music/DirectoryFilter;->usesExternalStorage()Z

    move-result v1

    if-eqz v1, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/mm/music/CursorUtils;->ctx:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/konka/mm/music/DirectoryFilter;->getFolderSqlStatement(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v19

    :goto_0
    const/4 v4, 0x0

    if-eqz v19, :cond_2

    if-eqz p2, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static/range {p2 .. p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " AND ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, v19

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    :goto_1
    const/16 v17, 0x0

    const/4 v1, -0x2

    move/from16 v0, p1

    if-ne v0, v1, :cond_4

    invoke-static {}, Lcom/konka/mm/music/DirectoryFilter;->usesExternalStorage()Z

    move-result v1

    if-eqz v1, :cond_3

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/mm/music/CursorUtils;->ctx:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    sget-object v3, Lcom/konka/mm/music/Constants;->songProjection:[Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "album COLLATE NOCASE ASC, track ASC"

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v17

    :goto_2
    move-object/from16 v1, v17

    :goto_3
    return-object v1

    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/mm/music/CursorUtils;->ctx:Landroid/content/Context;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/konka/mm/music/DirectoryFilter;->getFolderSqlStatement(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v19

    goto :goto_0

    :cond_1
    move-object/from16 v4, v19

    goto :goto_1

    :cond_2
    move-object/from16 v4, p2

    goto :goto_1

    :cond_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/mm/music/CursorUtils;->ctx:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Landroid/provider/MediaStore$Audio$Media;->INTERNAL_CONTENT_URI:Landroid/net/Uri;

    sget-object v3, Lcom/konka/mm/music/Constants;->songProjection:[Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "album COLLATE NOCASE ASC, track ASC"

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v17

    goto :goto_2

    :cond_4
    if-ltz p1, :cond_8

    const/4 v10, 0x0

    if-eqz p3, :cond_5

    const-string v10, "play_order ASC"

    :goto_4
    if-eqz v4, :cond_6

    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " AND "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "is_music"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "=1"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    :goto_5
    invoke-static {}, Lcom/konka/mm/music/DirectoryFilter;->usesExternalStorage()Z

    move-result v1

    if-eqz v1, :cond_7

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/mm/music/CursorUtils;->ctx:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v1, "external"

    move/from16 v0, p1

    int-to-long v2, v0

    invoke-static {v1, v2, v3}, Landroid/provider/MediaStore$Audio$Playlists$Members;->getContentUri(Ljava/lang/String;J)Landroid/net/Uri;

    move-result-object v6

    sget-object v7, Lcom/konka/mm/music/Constants;->playlistMembersProjection:[Ljava/lang/String;

    const/4 v9, 0x0

    invoke-virtual/range {v5 .. v10}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v17

    :goto_6
    move-object/from16 v1, v17

    goto :goto_3

    :cond_5
    const-string v10, "album COLLATE NOCASE ASC, track ASC"

    goto :goto_4

    :cond_6
    :try_start_1
    const-string v8, "is_music=1"

    goto :goto_5

    :cond_7
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/mm/music/CursorUtils;->ctx:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v1, "internal"

    move/from16 v0, p1

    int-to-long v2, v0

    invoke-static {v1, v2, v3}, Landroid/provider/MediaStore$Audio$Playlists$Members;->getContentUri(Ljava/lang/String;J)Landroid/net/Uri;

    move-result-object v6

    sget-object v7, Lcom/konka/mm/music/Constants;->playlistMembersProjection:[Ljava/lang/String;

    const/4 v9, 0x0

    invoke-virtual/range {v5 .. v10}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v17

    goto :goto_6

    :catch_0
    move-exception v18

    invoke-virtual/range {v18 .. v18}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    goto :goto_6

    :cond_8
    const v1, -0x186a0

    move/from16 v0, p1

    if-gt v0, v1, :cond_b

    const v1, -0x19a28

    move/from16 v0, p1

    if-le v0, v1, :cond_b

    if-eqz v4, :cond_9

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " AND "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "is_music"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "=1"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    :goto_7
    :try_start_2
    invoke-static {}, Lcom/konka/mm/music/DirectoryFilter;->usesExternalStorage()Z

    move-result v1

    if-eqz v1, :cond_a

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/mm/music/CursorUtils;->ctx:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v11

    const-string v1, "external"

    const v2, -0x186a0

    sub-int v2, p1, v2

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    int-to-long v2, v2

    invoke-static {v1, v2, v3}, Landroid/provider/MediaStore$Audio$Genres$Members;->getContentUri(Ljava/lang/String;J)Landroid/net/Uri;

    move-result-object v12

    sget-object v13, Lcom/konka/mm/music/Constants;->genreMemberProjection:[Ljava/lang/String;

    const/4 v15, 0x0

    const-string v16, "album COLLATE NOCASE ASC, track ASC"

    move-object v14, v8

    invoke-virtual/range {v11 .. v16}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object v17

    :goto_8
    move-object/from16 v1, v17

    goto/16 :goto_3

    :cond_9
    const-string v8, "is_music=1"

    goto :goto_7

    :cond_a
    :try_start_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/mm/music/CursorUtils;->ctx:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v11

    const-string v1, "internal"

    const v2, -0x186a0

    sub-int v2, p1, v2

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    int-to-long v2, v2

    invoke-static {v1, v2, v3}, Landroid/provider/MediaStore$Audio$Genres$Members;->getContentUri(Ljava/lang/String;J)Landroid/net/Uri;

    move-result-object v12

    sget-object v13, Lcom/konka/mm/music/Constants;->genreMemberProjection:[Ljava/lang/String;

    const/4 v15, 0x0

    const-string v16, "album COLLATE NOCASE ASC, track ASC"

    move-object v14, v8

    invoke-virtual/range {v11 .. v16}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_3
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_3 .. :try_end_3} :catch_1

    move-result-object v17

    goto :goto_8

    :catch_1
    move-exception v18

    invoke-virtual/range {v18 .. v18}, Landroid/database/sqlite/SQLiteException;->printStackTrace()V

    goto :goto_8

    :cond_b
    const/4 v1, 0x0

    goto/16 :goto_3
.end method
