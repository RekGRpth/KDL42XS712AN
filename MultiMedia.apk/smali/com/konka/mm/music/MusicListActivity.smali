.class public Lcom/konka/mm/music/MusicListActivity;
.super Landroid/app/Activity;
.source "MusicListActivity.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/mm/music/MusicListActivity$BtnScanDialogListener;,
        Lcom/konka/mm/music/MusicListActivity$InitDataRunnable;,
        Lcom/konka/mm/music/MusicListActivity$ScanSdReceiver;,
        Lcom/konka/mm/music/MusicListActivity$UpdateDataRunnable;,
        Lcom/konka/mm/music/MusicListActivity$sortThread;,
        Lcom/konka/mm/music/MusicListActivity$writeAllDataToDBRunnable;
    }
.end annotation


# static fields
.field public static final AUDIO_DATA_HAS_CHANGE:I = 0x3

.field public static final LANGUAGE_EN:Ljava/lang/String; = "en"

.field public static final LANGUAGE_ZH:Ljava/lang/String; = "zh"

.field public static final SCAN_DISK_MUSIC_DIALOG:I = 0x1

.field public static final SCREEN_1080P:I = 0x1

.field public static final SCREEN_720P:I = 0x0

.field private static final TAG:Ljava/lang/String; = "MusicListActivity"

.field public static final UPDATA_DISK_MUSIC_DIALOG:I = 0x2

.field public static instance:Lcom/konka/mm/music/MusicListActivity;

.field public static musicPaths:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private CURRENT_LANGUAGE:Ljava/lang/String;

.field private adapter:Lcom/konka/mm/adapters/MusicListAdapter;

.field private bNextPageKey:Z

.field private changeListener:Landroid/widget/RadioGroup$OnCheckedChangeListener;

.field private clickListener:Landroid/view/View$OnClickListener;

.field private curPage:I

.field private curScreen:I

.field private currentPage:I

.field private gridOnItemClick:Landroid/widget/AdapterView$OnItemClickListener;

.field private gridOnKey:Landroid/view/View$OnKeyListener;

.field private indexPage:I

.field private infoText:Landroid/widget/TextView;

.field private isAddViewRun:Z

.field private isPopup:Z

.field mAlbumArtDownloadOkClickListener:Lcom/konka/mm/music/AlbumArtDownloadOkClickListener;

.field protected mCountPerPage:I

.field public mDBBackgroundHelper:Lcom/konka/mm/tools/DBBackgroundHelper;

.field public mDBHelper:Lcom/konka/mm/tools/DBHelper1;

.field private mDialogScan:Landroid/app/ProgressDialog;

.field private mHandler:Landroid/os/Handler;

.field private mPushLeftInAnim:Landroid/view/animation/Animation;

.field private mPushLeftOutAnim:Landroid/view/animation/Animation;

.field private mPustRightInAnim:Landroid/view/animation/Animation;

.field private mPustRightOutAnim:Landroid/view/animation/Animation;

.field private mRadioGroup:Landroid/widget/RadioGroup;

.field private mReceiver:Landroid/content/BroadcastReceiver;

.field private mRootPath:Ljava/lang/String;

.field protected mScreen_mode:I

.field private numColumns:I

.field private pageCount:I

.field private pageGridView:[Landroid/widget/GridView;

.field private pageInfomation:Landroid/widget/TextView;

.field private pageLeft:Landroid/widget/Button;

.field private pageRight:Landroid/widget/Button;

.field private page_linearLayout:Landroid/widget/LinearLayout;

.field private perColumnWidth:I

.field private popupView:Landroid/view/View;

.field private popupWindow:Landroid/widget/PopupWindow;

.field private proDlg:Landroid/app/ProgressDialog;

.field private screenHeight:I

.field private screenHeight_720p:I

.field private screenWidth:I

.field private screenWidth_720p:I

.field private sortHandler:Landroid/os/Handler;

.field private sortType:I

.field private sort_btn:Landroid/widget/ImageView;

.field startX:F

.field public stopScanRun:Z

.field public stopScanRunBackground:Z

.field toast_file_ont_exist:Landroid/widget/Toast;

.field private update_btn:Landroid/widget/ImageView;

.field private viewFilpper:Landroid/widget/ViewFlipper;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/konka/mm/music/MusicListActivity;->instance:Lcom/konka/mm/music/MusicListActivity;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/konka/mm/music/MusicListActivity;->musicPaths:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput-object v2, p0, Lcom/konka/mm/music/MusicListActivity;->pageGridView:[Landroid/widget/GridView;

    iput-object v2, p0, Lcom/konka/mm/music/MusicListActivity;->mRootPath:Ljava/lang/String;

    const/16 v0, 0x500

    iput v0, p0, Lcom/konka/mm/music/MusicListActivity;->screenWidth_720p:I

    const/16 v0, 0x2d0

    iput v0, p0, Lcom/konka/mm/music/MusicListActivity;->screenHeight_720p:I

    iput v3, p0, Lcom/konka/mm/music/MusicListActivity;->mScreen_mode:I

    iput-object v2, p0, Lcom/konka/mm/music/MusicListActivity;->mDialogScan:Landroid/app/ProgressDialog;

    iput-boolean v1, p0, Lcom/konka/mm/music/MusicListActivity;->stopScanRun:Z

    iput-boolean v1, p0, Lcom/konka/mm/music/MusicListActivity;->stopScanRunBackground:Z

    new-instance v0, Lcom/konka/mm/music/MusicListActivity$1;

    invoke-direct {v0, p0}, Lcom/konka/mm/music/MusicListActivity$1;-><init>(Lcom/konka/mm/music/MusicListActivity;)V

    iput-object v0, p0, Lcom/konka/mm/music/MusicListActivity;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/konka/mm/music/MusicListActivity$2;

    invoke-direct {v0, p0}, Lcom/konka/mm/music/MusicListActivity$2;-><init>(Lcom/konka/mm/music/MusicListActivity;)V

    iput-object v0, p0, Lcom/konka/mm/music/MusicListActivity;->sortHandler:Landroid/os/Handler;

    iput-boolean v1, p0, Lcom/konka/mm/music/MusicListActivity;->isPopup:Z

    const/16 v0, 0x14a

    iput v0, p0, Lcom/konka/mm/music/MusicListActivity;->perColumnWidth:I

    iput v1, p0, Lcom/konka/mm/music/MusicListActivity;->pageCount:I

    iput v1, p0, Lcom/konka/mm/music/MusicListActivity;->currentPage:I

    iput v1, p0, Lcom/konka/mm/music/MusicListActivity;->curScreen:I

    const/4 v0, 0x6

    iput v0, p0, Lcom/konka/mm/music/MusicListActivity;->numColumns:I

    iput v1, p0, Lcom/konka/mm/music/MusicListActivity;->curPage:I

    iput v1, p0, Lcom/konka/mm/music/MusicListActivity;->sortType:I

    iput-boolean v1, p0, Lcom/konka/mm/music/MusicListActivity;->bNextPageKey:Z

    iput-boolean v3, p0, Lcom/konka/mm/music/MusicListActivity;->isAddViewRun:Z

    iput v3, p0, Lcom/konka/mm/music/MusicListActivity;->indexPage:I

    new-instance v0, Lcom/konka/mm/music/MusicListActivity$3;

    invoke-direct {v0, p0}, Lcom/konka/mm/music/MusicListActivity$3;-><init>(Lcom/konka/mm/music/MusicListActivity;)V

    iput-object v0, p0, Lcom/konka/mm/music/MusicListActivity;->clickListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/konka/mm/music/MusicListActivity$4;

    invoke-direct {v0, p0}, Lcom/konka/mm/music/MusicListActivity$4;-><init>(Lcom/konka/mm/music/MusicListActivity;)V

    iput-object v0, p0, Lcom/konka/mm/music/MusicListActivity;->changeListener:Landroid/widget/RadioGroup$OnCheckedChangeListener;

    new-instance v0, Lcom/konka/mm/music/MusicListActivity$5;

    invoke-direct {v0, p0}, Lcom/konka/mm/music/MusicListActivity$5;-><init>(Lcom/konka/mm/music/MusicListActivity;)V

    iput-object v0, p0, Lcom/konka/mm/music/MusicListActivity;->gridOnKey:Landroid/view/View$OnKeyListener;

    new-instance v0, Lcom/konka/mm/music/MusicListActivity$6;

    invoke-direct {v0, p0}, Lcom/konka/mm/music/MusicListActivity$6;-><init>(Lcom/konka/mm/music/MusicListActivity;)V

    iput-object v0, p0, Lcom/konka/mm/music/MusicListActivity;->gridOnItemClick:Landroid/widget/AdapterView$OnItemClickListener;

    iput-object v2, p0, Lcom/konka/mm/music/MusicListActivity;->mAlbumArtDownloadOkClickListener:Lcom/konka/mm/music/AlbumArtDownloadOkClickListener;

    return-void
.end method

.method static synthetic access$0(Lcom/konka/mm/music/MusicListActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/mm/music/MusicListActivity;->initGridView()V

    return-void
.end method

.method static synthetic access$1(Lcom/konka/mm/music/MusicListActivity;)Landroid/app/ProgressDialog;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/music/MusicListActivity;->mDialogScan:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$10(Lcom/konka/mm/music/MusicListActivity;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/music/MusicListActivity;->sortHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$11(Lcom/konka/mm/music/MusicListActivity;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/music/MusicListActivity;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$12(Lcom/konka/mm/music/MusicListActivity;)I
    .locals 1

    iget v0, p0, Lcom/konka/mm/music/MusicListActivity;->sortType:I

    return v0
.end method

.method static synthetic access$13(Lcom/konka/mm/music/MusicListActivity;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/konka/mm/music/MusicListActivity;->isAddViewRun:Z

    return-void
.end method

.method static synthetic access$14(Lcom/konka/mm/music/MusicListActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/mm/music/MusicListActivity;->dismissPopupWindow()V

    return-void
.end method

.method static synthetic access$15(Lcom/konka/mm/music/MusicListActivity;)Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/music/MusicListActivity;->update_btn:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$16(Lcom/konka/mm/music/MusicListActivity;)Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/music/MusicListActivity;->sort_btn:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$17(Lcom/konka/mm/music/MusicListActivity;Landroid/content/Intent;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/mm/music/MusicListActivity;->onReceiveSdCardBroadCast(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic access$18(Lcom/konka/mm/music/MusicListActivity;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/mm/music/MusicListActivity;->displayMusicInfo(I)V

    return-void
.end method

.method static synthetic access$2(Lcom/konka/mm/music/MusicListActivity;)Landroid/app/ProgressDialog;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/music/MusicListActivity;->proDlg:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$3(Lcom/konka/mm/music/MusicListActivity;I)V
    .locals 0

    iput p1, p0, Lcom/konka/mm/music/MusicListActivity;->sortType:I

    return-void
.end method

.method static synthetic access$4(Lcom/konka/mm/music/MusicListActivity;)Landroid/widget/PopupWindow;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/music/MusicListActivity;->popupWindow:Landroid/widget/PopupWindow;

    return-object v0
.end method

.method static synthetic access$5(Lcom/konka/mm/music/MusicListActivity;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/konka/mm/music/MusicListActivity;->isPopup:Z

    return-void
.end method

.method static synthetic access$6(Lcom/konka/mm/music/MusicListActivity;)I
    .locals 1

    iget v0, p0, Lcom/konka/mm/music/MusicListActivity;->currentPage:I

    return v0
.end method

.method static synthetic access$7(Lcom/konka/mm/music/MusicListActivity;)I
    .locals 1

    iget v0, p0, Lcom/konka/mm/music/MusicListActivity;->pageCount:I

    return v0
.end method

.method static synthetic access$8(Lcom/konka/mm/music/MusicListActivity;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/konka/mm/music/MusicListActivity;->bNextPageKey:Z

    return-void
.end method

.method static synthetic access$9(Lcom/konka/mm/music/MusicListActivity;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/music/MusicListActivity;->mRootPath:Ljava/lang/String;

    return-object v0
.end method

.method private dismissPopupWindow()V
    .locals 2

    iget-object v0, p0, Lcom/konka/mm/music/MusicListActivity;->popupWindow:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/konka/mm/music/MusicListActivity;->isPopup:Z

    iget-object v0, p0, Lcom/konka/mm/music/MusicListActivity;->sort_btn:Landroid/widget/ImageView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicListActivity;->sort_btn:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->requestFocus()Z

    return-void
.end method

.method private displayMusicInfo(I)V
    .locals 6
    .param p1    # I

    const/4 v3, -0x1

    if-ne p1, v3, :cond_0

    iget-object v3, p0, Lcom/konka/mm/music/MusicListActivity;->infoText:Landroid/widget/TextView;

    const-string v4, " "

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    iget v3, p0, Lcom/konka/mm/music/MusicListActivity;->currentPage:I

    iget v4, p0, Lcom/konka/mm/music/MusicListActivity;->mCountPerPage:I

    mul-int/2addr v3, v4

    add-int/2addr v3, p1

    sget-object v4, Lcom/konka/mm/music/MusicListActivity;->musicPaths:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v3, v4, :cond_1

    iget-boolean v3, p0, Lcom/konka/mm/music/MusicListActivity;->bNextPageKey:Z

    if-eqz v3, :cond_2

    if-eqz p1, :cond_2

    :cond_1
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/konka/mm/music/MusicListActivity;->bNextPageKey:Z

    goto :goto_0

    :cond_2
    sget-object v3, Lcom/konka/mm/music/MusicListActivity;->musicPaths:Ljava/util/ArrayList;

    iget v4, p0, Lcom/konka/mm/music/MusicListActivity;->currentPage:I

    iget v5, p0, Lcom/konka/mm/music/MusicListActivity;->mCountPerPage:I

    mul-int/2addr v4, v5

    add-int/2addr v4, p1

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->length()J

    move-result-wide v3

    long-to-double v0, v3

    iget-object v3, p0, Lcom/konka/mm/music/MusicListActivity;->infoText:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "/"

    invoke-virtual {v2, v5}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {v2, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v0, v1}, Lcom/konka/mm/tools/MusicTool;->byte2M(D)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "M"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private init()V
    .locals 8

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/konka/mm/music/MusicListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "com.konka.mm.file.root.path"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/konka/mm/music/MusicListActivity;->mRootPath:Ljava/lang/String;

    const-string v2, "com.konka.mm.file.sort.type"

    invoke-virtual {v0, v2, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/konka/mm/music/MusicListActivity;->sortType:I

    const v2, 0x7f0b0030    # com.konka.mm.R.id.viewFlipper

    invoke-virtual {p0, v2}, Lcom/konka/mm/music/MusicListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ViewFlipper;

    iput-object v2, p0, Lcom/konka/mm/music/MusicListActivity;->viewFilpper:Landroid/widget/ViewFlipper;

    const/4 v2, 0x3

    new-array v2, v2, [Landroid/widget/GridView;

    iput-object v2, p0, Lcom/konka/mm/music/MusicListActivity;->pageGridView:[Landroid/widget/GridView;

    iget-object v3, p0, Lcom/konka/mm/music/MusicListActivity;->pageGridView:[Landroid/widget/GridView;

    iget-object v2, p0, Lcom/konka/mm/music/MusicListActivity;->viewFilpper:Landroid/widget/ViewFlipper;

    const v4, 0x7f0b0031    # com.konka.mm.R.id.page1

    invoke-virtual {v2, v4}, Landroid/widget/ViewFlipper;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/GridView;

    aput-object v2, v3, v5

    iget-object v3, p0, Lcom/konka/mm/music/MusicListActivity;->pageGridView:[Landroid/widget/GridView;

    iget-object v2, p0, Lcom/konka/mm/music/MusicListActivity;->viewFilpper:Landroid/widget/ViewFlipper;

    const v4, 0x7f0b0032    # com.konka.mm.R.id.page2

    invoke-virtual {v2, v4}, Landroid/widget/ViewFlipper;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/GridView;

    aput-object v2, v3, v6

    iget-object v3, p0, Lcom/konka/mm/music/MusicListActivity;->pageGridView:[Landroid/widget/GridView;

    iget-object v2, p0, Lcom/konka/mm/music/MusicListActivity;->viewFilpper:Landroid/widget/ViewFlipper;

    const v4, 0x7f0b0033    # com.konka.mm.R.id.page3

    invoke-virtual {v2, v4}, Landroid/widget/ViewFlipper;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/GridView;

    aput-object v2, v3, v7

    iget-object v2, p0, Lcom/konka/mm/music/MusicListActivity;->pageGridView:[Landroid/widget/GridView;

    aget-object v2, v2, v5

    invoke-virtual {v2, p0}, Landroid/widget/GridView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v2, p0, Lcom/konka/mm/music/MusicListActivity;->pageGridView:[Landroid/widget/GridView;

    aget-object v2, v2, v6

    invoke-virtual {v2, p0}, Landroid/widget/GridView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v2, p0, Lcom/konka/mm/music/MusicListActivity;->pageGridView:[Landroid/widget/GridView;

    aget-object v2, v2, v7

    invoke-virtual {v2, p0}, Landroid/widget/GridView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    const v2, 0x7f0b0075    # com.konka.mm.R.id.tv_music_list_info

    invoke-virtual {p0, v2}, Lcom/konka/mm/music/MusicListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/konka/mm/music/MusicListActivity;->infoText:Landroid/widget/TextView;

    const v2, 0x7f0b002e    # com.konka.mm.R.id.page_linearlayout

    invoke-virtual {p0, v2}, Lcom/konka/mm/music/MusicListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/konka/mm/music/MusicListActivity;->page_linearLayout:Landroid/widget/LinearLayout;

    const v2, 0x7f040005    # com.konka.mm.R.anim.photo_push_left_in

    invoke-static {p0, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v2

    iput-object v2, p0, Lcom/konka/mm/music/MusicListActivity;->mPushLeftInAnim:Landroid/view/animation/Animation;

    const v2, 0x7f040009    # com.konka.mm.R.anim.push_left_out

    invoke-static {p0, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v2

    iput-object v2, p0, Lcom/konka/mm/music/MusicListActivity;->mPushLeftOutAnim:Landroid/view/animation/Animation;

    const v2, 0x7f040006    # com.konka.mm.R.anim.photo_push_right_in

    invoke-static {p0, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v2

    iput-object v2, p0, Lcom/konka/mm/music/MusicListActivity;->mPustRightInAnim:Landroid/view/animation/Animation;

    const v2, 0x7f04000a    # com.konka.mm.R.anim.push_right_out

    invoke-static {p0, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v2

    iput-object v2, p0, Lcom/konka/mm/music/MusicListActivity;->mPustRightOutAnim:Landroid/view/animation/Animation;

    sput-object p0, Lcom/konka/mm/music/MusicListActivity;->instance:Lcom/konka/mm/music/MusicListActivity;

    const v2, 0x7f0b002f    # com.konka.mm.R.id.btn_list_left

    invoke-virtual {p0, v2}, Lcom/konka/mm/music/MusicListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/konka/mm/music/MusicListActivity;->pageLeft:Landroid/widget/Button;

    const v2, 0x7f0b0034    # com.konka.mm.R.id.btn_list_right

    invoke-virtual {p0, v2}, Lcom/konka/mm/music/MusicListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/konka/mm/music/MusicListActivity;->pageRight:Landroid/widget/Button;

    const v2, 0x7f0b0035    # com.konka.mm.R.id.tv_page_info

    invoke-virtual {p0, v2}, Lcom/konka/mm/music/MusicListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/konka/mm/music/MusicListActivity;->pageInfomation:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/konka/mm/music/MusicListActivity;->pageLeft:Landroid/widget/Button;

    iget-object v3, p0, Lcom/konka/mm/music/MusicListActivity;->clickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, Lcom/konka/mm/music/MusicListActivity;->pageRight:Landroid/widget/Button;

    iget-object v3, p0, Lcom/konka/mm/music/MusicListActivity;->clickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v2, 0x7f0b005c    # com.konka.mm.R.id.img_sort

    invoke-virtual {p0, v2}, Lcom/konka/mm/music/MusicListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/konka/mm/music/MusicListActivity;->sort_btn:Landroid/widget/ImageView;

    const v2, 0x7f0b005b    # com.konka.mm.R.id.btn_update

    invoke-virtual {p0, v2}, Lcom/konka/mm/music/MusicListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/konka/mm/music/MusicListActivity;->update_btn:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/konka/mm/music/MusicListActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f030037    # com.konka.mm.R.layout.sort_popup_window_template

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/konka/mm/music/MusicListActivity;->popupView:Landroid/view/View;

    iget-object v2, p0, Lcom/konka/mm/music/MusicListActivity;->popupView:Landroid/view/View;

    const v3, 0x7f0b00b3    # com.konka.mm.R.id.rg_sort_btns

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RadioGroup;

    iput-object v2, p0, Lcom/konka/mm/music/MusicListActivity;->mRadioGroup:Landroid/widget/RadioGroup;

    iput-boolean v5, p0, Lcom/konka/mm/music/MusicListActivity;->stopScanRun:Z

    iput-boolean v5, p0, Lcom/konka/mm/music/MusicListActivity;->stopScanRunBackground:Z

    invoke-virtual {p0}, Lcom/konka/mm/music/MusicListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090012    # com.konka.mm.R.string.file_not_exist_promte

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    iput-object v2, p0, Lcom/konka/mm/music/MusicListActivity;->toast_file_ont_exist:Landroid/widget/Toast;

    iget-object v2, p0, Lcom/konka/mm/music/MusicListActivity;->popupView:Landroid/view/View;

    new-instance v3, Lcom/konka/mm/music/MusicListActivity$7;

    invoke-direct {v3, p0}, Lcom/konka/mm/music/MusicListActivity$7;-><init>(Lcom/konka/mm/music/MusicListActivity;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v2, p0, Lcom/konka/mm/music/MusicListActivity;->sort_btn:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/konka/mm/music/MusicListActivity;->clickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, Lcom/konka/mm/music/MusicListActivity;->update_btn:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/konka/mm/music/MusicListActivity;->clickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, Lcom/konka/mm/music/MusicListActivity;->mRadioGroup:Landroid/widget/RadioGroup;

    iget-object v3, p0, Lcom/konka/mm/music/MusicListActivity;->changeListener:Landroid/widget/RadioGroup$OnCheckedChangeListener;

    invoke-virtual {v2, v3}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    iget-object v2, p0, Lcom/konka/mm/music/MusicListActivity;->sort_btn:Landroid/widget/ImageView;

    new-instance v3, Lcom/konka/mm/music/MusicListActivity$8;

    invoke-direct {v3, p0}, Lcom/konka/mm/music/MusicListActivity$8;-><init>(Lcom/konka/mm/music/MusicListActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v2, p0, Lcom/konka/mm/music/MusicListActivity;->update_btn:Landroid/widget/ImageView;

    new-instance v3, Lcom/konka/mm/music/MusicListActivity$9;

    invoke-direct {v3, p0}, Lcom/konka/mm/music/MusicListActivity$9;-><init>(Lcom/konka/mm/music/MusicListActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    new-instance v2, Lcom/konka/mm/tools/DBHelper1;

    iget-object v3, p0, Lcom/konka/mm/music/MusicListActivity;->mRootPath:Ljava/lang/String;

    invoke-direct {v2, p0, v3}, Lcom/konka/mm/tools/DBHelper1;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v2, p0, Lcom/konka/mm/music/MusicListActivity;->mDBHelper:Lcom/konka/mm/tools/DBHelper1;

    new-instance v2, Lcom/konka/mm/tools/DBBackgroundHelper;

    iget-object v3, p0, Lcom/konka/mm/music/MusicListActivity;->mRootPath:Ljava/lang/String;

    invoke-direct {v2, p0, v3}, Lcom/konka/mm/tools/DBBackgroundHelper;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v2, p0, Lcom/konka/mm/music/MusicListActivity;->mDBBackgroundHelper:Lcom/konka/mm/tools/DBBackgroundHelper;

    invoke-virtual {p0}, Lcom/konka/mm/music/MusicListActivity;->InitMusicInfos()V

    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    const-string v2, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v2, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v2, "file"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    new-instance v2, Lcom/konka/mm/music/MusicListActivity$10;

    invoke-direct {v2, p0}, Lcom/konka/mm/music/MusicListActivity$10;-><init>(Lcom/konka/mm/music/MusicListActivity;)V

    iput-object v2, p0, Lcom/konka/mm/music/MusicListActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    iget-object v2, p0, Lcom/konka/mm/music/MusicListActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v2, v1}, Lcom/konka/mm/music/MusicListActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method private initGridView()V
    .locals 13

    const/16 v12, 0x12

    const/4 v7, 0x3

    const/16 v11, 0x14

    const/4 v10, 0x1

    const/4 v9, 0x0

    iput v9, p0, Lcom/konka/mm/music/MusicListActivity;->currentPage:I

    iput v9, p0, Lcom/konka/mm/music/MusicListActivity;->pageCount:I

    sget v5, Lcom/konka/mm/music/MusicDiskActivity;->List_Mode:I

    if-ne v5, v10, :cond_0

    const/16 v5, 0x21

    iput v5, p0, Lcom/konka/mm/music/MusicListActivity;->mCountPerPage:I

    iput v7, p0, Lcom/konka/mm/music/MusicListActivity;->numColumns:I

    :goto_0
    sget-object v5, Lcom/konka/mm/music/MusicListActivity;->musicPaths:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v3

    int-to-float v5, v3

    iget v6, p0, Lcom/konka/mm/music/MusicListActivity;->mCountPerPage:I

    int-to-float v6, v6

    div-float/2addr v5, v6

    float-to-double v5, v5

    invoke-static {v5, v6}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v5

    double-to-int v5, v5

    iput v5, p0, Lcom/konka/mm/music/MusicListActivity;->pageCount:I

    invoke-virtual {p0}, Lcom/konka/mm/music/MusicListActivity;->setPageInfo()V

    iget v5, p0, Lcom/konka/mm/music/MusicListActivity;->pageCount:I

    if-gt v5, v7, :cond_1

    iget v0, p0, Lcom/konka/mm/music/MusicListActivity;->pageCount:I

    :goto_1
    iget v1, p0, Lcom/konka/mm/music/MusicListActivity;->curScreen:I

    :goto_2
    iget v5, p0, Lcom/konka/mm/music/MusicListActivity;->curScreen:I

    add-int/2addr v5, v0

    if-lt v1, v5, :cond_2

    invoke-virtual {p0}, Lcom/konka/mm/music/MusicListActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    check-cast v2, Lcom/konka/mm/GlobalData;

    sget-object v5, Lcom/konka/mm/music/MusicListActivity;->musicPaths:Ljava/util/ArrayList;

    invoke-virtual {v2, v5}, Lcom/konka/mm/GlobalData;->setmMMFileList(Ljava/util/ArrayList;)V

    invoke-direct {p0, v9}, Lcom/konka/mm/music/MusicListActivity;->displayMusicInfo(I)V

    return-void

    :cond_0
    iput v12, p0, Lcom/konka/mm/music/MusicListActivity;->mCountPerPage:I

    const/4 v5, 0x6

    iput v5, p0, Lcom/konka/mm/music/MusicListActivity;->numColumns:I

    goto :goto_0

    :cond_1
    const/4 v0, 0x3

    goto :goto_1

    :cond_2
    iget-object v5, p0, Lcom/konka/mm/music/MusicListActivity;->pageGridView:[Landroid/widget/GridView;

    rem-int/lit8 v6, v1, 0x3

    aget-object v5, v5, v6

    iget v6, p0, Lcom/konka/mm/music/MusicListActivity;->numColumns:I

    invoke-virtual {v5, v6}, Landroid/widget/GridView;->setNumColumns(I)V

    iget v5, p0, Lcom/konka/mm/music/MusicListActivity;->curScreen:I

    sub-int v5, v1, v5

    iget v6, p0, Lcom/konka/mm/music/MusicListActivity;->pageCount:I

    add-int/lit8 v6, v6, -0x1

    if-ge v5, v6, :cond_4

    iget v4, p0, Lcom/konka/mm/music/MusicListActivity;->mCountPerPage:I

    :cond_3
    :goto_3
    iget-object v5, p0, Lcom/konka/mm/music/MusicListActivity;->pageGridView:[Landroid/widget/GridView;

    rem-int/lit8 v6, v1, 0x3

    aget-object v5, v5, v6

    invoke-virtual {v5, v11, v9, v11, v9}, Landroid/widget/GridView;->setPadding(IIII)V

    sget v5, Lcom/konka/mm/music/MusicDiskActivity;->List_Mode:I

    if-ne v5, v10, :cond_6

    iget v5, p0, Lcom/konka/mm/music/MusicListActivity;->mScreen_mode:I

    if-nez v5, :cond_5

    iget-object v5, p0, Lcom/konka/mm/music/MusicListActivity;->pageGridView:[Landroid/widget/GridView;

    rem-int/lit8 v6, v1, 0x3

    aget-object v5, v5, v6

    const/16 v6, -0x14

    invoke-virtual {v5, v6}, Landroid/widget/GridView;->setVerticalSpacing(I)V

    :goto_4
    iget-object v5, p0, Lcom/konka/mm/music/MusicListActivity;->page_linearLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v9, v9, v9, v9}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    :goto_5
    iget-object v5, p0, Lcom/konka/mm/music/MusicListActivity;->pageGridView:[Landroid/widget/GridView;

    rem-int/lit8 v6, v1, 0x3

    aget-object v5, v5, v6

    new-instance v6, Lcom/konka/mm/adapters/MusicListAdapter;

    iget v7, p0, Lcom/konka/mm/music/MusicListActivity;->curScreen:I

    sub-int v7, v1, v7

    iget v8, p0, Lcom/konka/mm/music/MusicListActivity;->mCountPerPage:I

    invoke-direct {v6, p0, v7, v4, v8}, Lcom/konka/mm/adapters/MusicListAdapter;-><init>(Lcom/konka/mm/music/MusicListActivity;III)V

    invoke-virtual {v5, v6}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v5, p0, Lcom/konka/mm/music/MusicListActivity;->pageGridView:[Landroid/widget/GridView;

    rem-int/lit8 v6, v1, 0x3

    aget-object v5, v5, v6

    new-instance v6, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v6, v9}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v5, v6}, Landroid/widget/GridView;->setSelector(Landroid/graphics/drawable/Drawable;)V

    iget-object v5, p0, Lcom/konka/mm/music/MusicListActivity;->pageGridView:[Landroid/widget/GridView;

    rem-int/lit8 v6, v1, 0x3

    aget-object v5, v5, v6

    iget-object v6, p0, Lcom/konka/mm/music/MusicListActivity;->gridOnKey:Landroid/view/View$OnKeyListener;

    invoke-virtual {v5, v6}, Landroid/widget/GridView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v5, p0, Lcom/konka/mm/music/MusicListActivity;->pageGridView:[Landroid/widget/GridView;

    rem-int/lit8 v6, v1, 0x3

    aget-object v5, v5, v6

    iget-object v6, p0, Lcom/konka/mm/music/MusicListActivity;->gridOnItemClick:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v5, v6}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v5, p0, Lcom/konka/mm/music/MusicListActivity;->pageGridView:[Landroid/widget/GridView;

    rem-int/lit8 v6, v1, 0x3

    aget-object v5, v5, v6

    new-instance v6, Lcom/konka/mm/music/MusicListActivity$12;

    invoke-direct {v6, p0}, Lcom/konka/mm/music/MusicListActivity$12;-><init>(Lcom/konka/mm/music/MusicListActivity;)V

    invoke-virtual {v5, v6}, Landroid/widget/GridView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    iget-object v5, p0, Lcom/konka/mm/music/MusicListActivity;->viewFilpper:Landroid/widget/ViewFlipper;

    invoke-virtual {v5}, Landroid/widget/ViewFlipper;->getCurrentView()Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v10}, Landroid/view/View;->setFocusable(Z)V

    iget-object v5, p0, Lcom/konka/mm/music/MusicListActivity;->viewFilpper:Landroid/widget/ViewFlipper;

    invoke-virtual {v5}, Landroid/widget/ViewFlipper;->getCurrentView()Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->requestFocus()Z

    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_2

    :cond_4
    iget v5, p0, Lcom/konka/mm/music/MusicListActivity;->mCountPerPage:I

    iget v6, p0, Lcom/konka/mm/music/MusicListActivity;->pageCount:I

    add-int/lit8 v6, v6, -0x1

    mul-int/2addr v5, v6

    sub-int v4, v3, v5

    iget v5, p0, Lcom/konka/mm/music/MusicListActivity;->mCountPerPage:I

    if-lt v4, v5, :cond_3

    iget v4, p0, Lcom/konka/mm/music/MusicListActivity;->mCountPerPage:I

    goto/16 :goto_3

    :cond_5
    iget-object v5, p0, Lcom/konka/mm/music/MusicListActivity;->pageGridView:[Landroid/widget/GridView;

    rem-int/lit8 v6, v1, 0x3

    aget-object v5, v5, v6

    const/16 v6, -0x1e

    invoke-virtual {v5, v6}, Landroid/widget/GridView;->setVerticalSpacing(I)V

    goto :goto_4

    :cond_6
    iget-object v5, p0, Lcom/konka/mm/music/MusicListActivity;->pageGridView:[Landroid/widget/GridView;

    rem-int/lit8 v6, v1, 0x3

    aget-object v5, v5, v6

    invoke-virtual {v5, v12}, Landroid/widget/GridView;->setVerticalSpacing(I)V

    iget-object v5, p0, Lcom/konka/mm/music/MusicListActivity;->page_linearLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v9, v11, v9, v9}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    goto/16 :goto_5
.end method

.method private onReceiveSdCardBroadCast(Landroid/content/Intent;)V
    .locals 2
    .param p1    # Landroid/content/Intent;

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/konka/mm/music/MusicListActivity;->mRootPath:Ljava/lang/String;

    invoke-static {p0, v1}, Lcom/konka/mm/tools/FileTool;->checkUsbExist(Landroid/app/Activity;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/konka/mm/music/MusicListActivity;->finish()V

    :cond_0
    return-void
.end method

.method private registerBroacast()V
    .locals 2

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.intent.action.MEDIA_SCANNER_FINISHED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.MEDIA_SCANNER_STARTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    return-void
.end method

.method private showAlbumArtDownloadDialog()V
    .locals 3

    iget-object v1, p0, Lcom/konka/mm/music/MusicListActivity;->mAlbumArtDownloadOkClickListener:Lcom/konka/mm/music/AlbumArtDownloadOkClickListener;

    if-nez v1, :cond_0

    new-instance v1, Lcom/konka/mm/music/AlbumArtDownloadOkClickListener;

    invoke-direct {v1, p0}, Lcom/konka/mm/music/AlbumArtDownloadOkClickListener;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/konka/mm/music/MusicListActivity;->mAlbumArtDownloadOkClickListener:Lcom/konka/mm/music/AlbumArtDownloadOkClickListener;

    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const-string v1, "Album Art"

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    const-string v1, "\ufffd\ufffd\u04aa\ufffd\ufffd\ufffd\ufffd\u05e8\ufffd\ufffd\u037c\u01ac\ufffd\ufffd"

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    const-string v1, "\ufffd\ufffd"

    iget-object v2, p0, Lcom/konka/mm/music/MusicListActivity;->mAlbumArtDownloadOkClickListener:Lcom/konka/mm/music/AlbumArtDownloadOkClickListener;

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const-string v1, "\ufffd\ufffd"

    iget-object v2, p0, Lcom/konka/mm/music/MusicListActivity;->mAlbumArtDownloadOkClickListener:Lcom/konka/mm/music/AlbumArtDownloadOkClickListener;

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    return-void
.end method

.method private sortFiles([Ljava/io/File;I)[Ljava/io/File;
    .locals 1
    .param p1    # [Ljava/io/File;
    .param p2    # I

    const/4 v0, 0x0

    packed-switch p2, :pswitch_data_0

    :goto_0
    return-object v0

    :pswitch_0
    move-object v0, p1

    goto :goto_0

    :pswitch_1
    invoke-static {p1}, Lcom/konka/mm/tools/FileTool;->getSortFilesByName([Ljava/io/File;)[Ljava/io/File;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    invoke-static {p1}, Lcom/konka/mm/tools/FileTool;->getSortFilesBySize([Ljava/io/File;)[Ljava/io/File;

    move-result-object v0

    goto :goto_0

    :pswitch_3
    invoke-static {p1}, Lcom/konka/mm/tools/FileTool;->getSortFilesByType([Ljava/io/File;)[Ljava/io/File;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public InitMusicInfos()V
    .locals 3

    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/konka/mm/music/MusicListActivity;->showDialog(I)V

    new-instance v0, Lcom/konka/mm/music/MusicListActivity$InitDataRunnable;

    invoke-direct {v0, p0}, Lcom/konka/mm/music/MusicListActivity$InitDataRunnable;-><init>(Lcom/konka/mm/music/MusicListActivity;)V

    new-instance v1, Ljava/lang/Thread;

    invoke-direct {v1, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    invoke-virtual {p0}, Lcom/konka/mm/music/MusicListActivity;->writeAllDataToDB()V

    return-void
.end method

.method public checkScreenMode()V
    .locals 3

    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    invoke-virtual {p0}, Lcom/konka/mm/music/MusicListActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    iget v1, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v1, p0, Lcom/konka/mm/music/MusicListActivity;->screenWidth:I

    iget v1, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    iput v1, p0, Lcom/konka/mm/music/MusicListActivity;->screenHeight:I

    iget v1, p0, Lcom/konka/mm/music/MusicListActivity;->screenWidth:I

    iget v2, p0, Lcom/konka/mm/music/MusicListActivity;->screenWidth_720p:I

    if-ne v1, v2, :cond_0

    iget v1, p0, Lcom/konka/mm/music/MusicListActivity;->screenHeight:I

    iget v2, p0, Lcom/konka/mm/music/MusicListActivity;->screenHeight_720p:I

    if-ne v1, v2, :cond_0

    const/4 v1, 0x0

    iput v1, p0, Lcom/konka/mm/music/MusicListActivity;->mScreen_mode:I

    :goto_0
    return-void

    :cond_0
    const/4 v1, 0x1

    iput v1, p0, Lcom/konka/mm/music/MusicListActivity;->mScreen_mode:I

    goto :goto_0
.end method

.method public compareDataChange()Z
    .locals 2

    iget-object v0, p0, Lcom/konka/mm/music/MusicListActivity;->mDBBackgroundHelper:Lcom/konka/mm/tools/DBBackgroundHelper;

    invoke-virtual {v0}, Lcom/konka/mm/tools/DBBackgroundHelper;->getAudioTotalCount()I

    move-result v0

    iget-object v1, p0, Lcom/konka/mm/music/MusicListActivity;->mDBHelper:Lcom/konka/mm/tools/DBHelper1;

    invoke-virtual {v1}, Lcom/konka/mm/tools/DBHelper1;->getAudioTotalCount()I

    move-result v1

    if-ne v0, v1, :cond_0

    const-string v0, "MusicListActivity"

    const-string v1, "the audio data dont change"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const-string v0, "MusicListActivity"

    const-string v1, "the audio data has change"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f03001d    # com.konka.mm.R.layout.music_list_1280x720

    invoke-virtual {p0, v0}, Lcom/konka/mm/music/MusicListActivity;->setContentView(I)V

    invoke-direct {p0}, Lcom/konka/mm/music/MusicListActivity;->init()V

    invoke-virtual {p0}, Lcom/konka/mm/music/MusicListActivity;->checkScreenMode()V

    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 5
    .param p1    # I

    const v4, 0x7f090009    # com.konka.mm.R.string.MM_QUIE

    const/4 v3, 0x1

    packed-switch p1, :pswitch_data_0

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    :pswitch_0
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/konka/mm/music/MusicListActivity;->mDialogScan:Landroid/app/ProgressDialog;

    iget-object v0, p0, Lcom/konka/mm/music/MusicListActivity;->mDialogScan:Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/konka/mm/music/MusicListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090074    # com.konka.mm.R.string.scan_disk_music

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicListActivity;->mDialogScan:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicListActivity;->mDialogScan:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicListActivity;->mDialogScan:Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/konka/mm/music/MusicListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/konka/mm/music/MusicListActivity$BtnScanDialogListener;

    invoke-direct {v2, p0}, Lcom/konka/mm/music/MusicListActivity$BtnScanDialogListener;-><init>(Lcom/konka/mm/music/MusicListActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/ProgressDialog;->setButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicListActivity;->mDialogScan:Landroid/app/ProgressDialog;

    goto :goto_0

    :pswitch_1
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/konka/mm/music/MusicListActivity;->mDialogScan:Landroid/app/ProgressDialog;

    iget-object v0, p0, Lcom/konka/mm/music/MusicListActivity;->mDialogScan:Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/konka/mm/music/MusicListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090075    # com.konka.mm.R.string.update_disk_music

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicListActivity;->mDialogScan:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicListActivity;->mDialogScan:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicListActivity;->mDialogScan:Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/konka/mm/music/MusicListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/konka/mm/music/MusicListActivity$BtnScanDialogListener;

    invoke-direct {v2, p0}, Lcom/konka/mm/music/MusicListActivity$BtnScanDialogListener;-><init>(Lcom/konka/mm/music/MusicListActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/ProgressDialog;->setButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicListActivity;->mDialogScan:Landroid/app/ProgressDialog;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onDestroy()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    iget-object v0, p0, Lcom/konka/mm/music/MusicListActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/mm/music/MusicListActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/konka/mm/music/MusicListActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/konka/mm/music/MusicListActivity;->stopScanRunBackground:Z

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    :try_start_0
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    sparse-switch v1, :sswitch_data_0

    :goto_0
    :sswitch_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    return v1

    :sswitch_1
    const/4 v1, -0x1

    :try_start_1
    invoke-virtual {p0, v1}, Lcom/konka/mm/music/MusicListActivity;->snapScreen(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    :sswitch_2
    const/4 v1, 0x1

    :try_start_2
    invoke-virtual {p0, v1}, Lcom/konka/mm/music/MusicListActivity;->snapScreen(I)V

    goto :goto_0

    :sswitch_3
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/konka/mm/music/MusicListActivity;->setBtnFocuseFlag(Z)V

    goto :goto_0

    :sswitch_4
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/konka/mm/music/MusicListActivity;->setBtnFocuseFlag(Z)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_0
        0x13 -> :sswitch_3
        0x14 -> :sswitch_4
        0x15 -> :sswitch_1
        0x16 -> :sswitch_2
    .end sparse-switch
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/view/MotionEvent;

    const/high16 v3, 0x42480000    # 50.0f

    const/4 v0, 0x1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_0
    invoke-super {p0, p2}, Landroid/app/Activity;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    :goto_1
    return v0

    :pswitch_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/konka/mm/music/MusicListActivity;->startX:F

    goto :goto_0

    :pswitch_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    iget v2, p0, Lcom/konka/mm/music/MusicListActivity;->startX:F

    cmpl-float v1, v1, v2

    if-lez v1, :cond_1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    iget v2, p0, Lcom/konka/mm/music/MusicListActivity;->startX:F

    sub-float/2addr v1, v2

    cmpl-float v1, v1, v3

    if-lez v1, :cond_1

    const/4 v1, -0x1

    invoke-virtual {p0, v1}, Lcom/konka/mm/music/MusicListActivity;->snapScreen(I)V

    goto :goto_1

    :cond_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    iget v2, p0, Lcom/konka/mm/music/MusicListActivity;->startX:F

    cmpg-float v1, v1, v2

    if-gez v1, :cond_0

    iget v1, p0, Lcom/konka/mm/music/MusicListActivity;->startX:F

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    sub-float/2addr v1, v2

    cmpl-float v1, v1, v3

    if-lez v1, :cond_0

    invoke-virtual {p0, v0}, Lcom/konka/mm/music/MusicListActivity;->snapScreen(I)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setBtnFocuseFlag(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/konka/mm/music/MusicListActivity;->sort_btn:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicListActivity;->update_btn:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setFocusable(Z)V

    return-void
.end method

.method public setPageInfo()V
    .locals 6

    const/4 v5, 0x4

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/konka/mm/music/MusicListActivity;->pageInfomation:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/konka/mm/music/MusicListActivity;->currentPage:I

    add-int/lit8 v2, v2, 0x1

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/konka/mm/music/MusicListActivity;->pageCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/konka/mm/music/MusicListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09009a    # com.konka.mm.R.string.page

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget v0, p0, Lcom/konka/mm/music/MusicListActivity;->pageCount:I

    const/4 v1, 0x1

    if-gt v0, v1, :cond_1

    iget-object v0, p0, Lcom/konka/mm/music/MusicListActivity;->pageLeft:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicListActivity;->pageRight:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setVisibility(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/konka/mm/music/MusicListActivity;->currentPage:I

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/konka/mm/music/MusicListActivity;->pageLeft:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicListActivity;->pageRight:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0

    :cond_2
    iget v0, p0, Lcom/konka/mm/music/MusicListActivity;->currentPage:I

    if-eqz v0, :cond_3

    iget v0, p0, Lcom/konka/mm/music/MusicListActivity;->currentPage:I

    iget v1, p0, Lcom/konka/mm/music/MusicListActivity;->pageCount:I

    add-int/lit8 v1, v1, -0x1

    if-eq v0, v1, :cond_3

    iget-object v0, p0, Lcom/konka/mm/music/MusicListActivity;->pageLeft:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicListActivity;->pageRight:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0

    :cond_3
    iget v0, p0, Lcom/konka/mm/music/MusicListActivity;->currentPage:I

    iget v1, p0, Lcom/konka/mm/music/MusicListActivity;->pageCount:I

    add-int/lit8 v1, v1, -0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/konka/mm/music/MusicListActivity;->pageLeft:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicListActivity;->pageRight:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0
.end method

.method public setPreOrNextScreen(I)V
    .locals 6
    .param p1    # I

    iget v2, p0, Lcom/konka/mm/music/MusicListActivity;->currentPage:I

    add-int/2addr v2, p1

    iput v2, p0, Lcom/konka/mm/music/MusicListActivity;->currentPage:I

    iget v2, p0, Lcom/konka/mm/music/MusicListActivity;->currentPage:I

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v2, p0, Lcom/konka/mm/music/MusicListActivity;->currentPage:I

    iget v3, p0, Lcom/konka/mm/music/MusicListActivity;->pageCount:I

    add-int/lit8 v3, v3, -0x1

    if-ge v2, v3, :cond_0

    iget v2, p0, Lcom/konka/mm/music/MusicListActivity;->curScreen:I

    add-int/2addr v2, p1

    add-int/lit8 v2, v2, 0x3

    rem-int/lit8 v0, v2, 0x3

    iget-object v2, p0, Lcom/konka/mm/music/MusicListActivity;->pageGridView:[Landroid/widget/GridView;

    aget-object v2, v2, v0

    iget v3, p0, Lcom/konka/mm/music/MusicListActivity;->numColumns:I

    invoke-virtual {v2, v3}, Landroid/widget/GridView;->setNumColumns(I)V

    const/4 v1, 0x0

    iget v2, p0, Lcom/konka/mm/music/MusicListActivity;->currentPage:I

    add-int/2addr v2, p1

    iget v3, p0, Lcom/konka/mm/music/MusicListActivity;->pageCount:I

    add-int/lit8 v3, v3, -0x1

    if-ge v2, v3, :cond_3

    iget v1, p0, Lcom/konka/mm/music/MusicListActivity;->mCountPerPage:I

    :cond_2
    :goto_1
    iget-object v2, p0, Lcom/konka/mm/music/MusicListActivity;->pageGridView:[Landroid/widget/GridView;

    aget-object v2, v2, v0

    new-instance v3, Lcom/konka/mm/adapters/MusicListAdapter;

    iget v4, p0, Lcom/konka/mm/music/MusicListActivity;->currentPage:I

    add-int/2addr v4, p1

    iget v5, p0, Lcom/konka/mm/music/MusicListActivity;->mCountPerPage:I

    invoke-direct {v3, p0, v4, v1, v5}, Lcom/konka/mm/adapters/MusicListAdapter;-><init>(Lcom/konka/mm/music/MusicListActivity;III)V

    invoke-virtual {v2, v3}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    goto :goto_0

    :cond_3
    sget-object v2, Lcom/konka/mm/music/MusicListActivity;->musicPaths:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    iget v3, p0, Lcom/konka/mm/music/MusicListActivity;->mCountPerPage:I

    iget v4, p0, Lcom/konka/mm/music/MusicListActivity;->pageCount:I

    add-int/lit8 v4, v4, -0x1

    mul-int/2addr v3, v4

    sub-int v1, v2, v3

    iget v2, p0, Lcom/konka/mm/music/MusicListActivity;->mCountPerPage:I

    if-lt v1, v2, :cond_2

    iget v1, p0, Lcom/konka/mm/music/MusicListActivity;->mCountPerPage:I

    goto :goto_1
.end method

.method public showDataChangeDlg(Landroid/app/Activity;)V
    .locals 3
    .param p1    # Landroid/app/Activity;

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090097    # com.konka.mm.R.string.mention_info

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x1080027    # android.R.drawable.ic_dialog_alert

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x108009b    # android.R.drawable.ic_dialog_info

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090013    # com.konka.mm.R.string.file_has_change

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09000a    # com.konka.mm.R.string.MM_CANCLE

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09002a    # com.konka.mm.R.string.ok

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/konka/mm/music/MusicListActivity$13;

    invoke-direct {v2, p0}, Lcom/konka/mm/music/MusicListActivity$13;-><init>(Lcom/konka/mm/music/MusicListActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    return-void
.end method

.method public showProgressDialog(II)V
    .locals 3
    .param p1    # I
    .param p2    # I

    iget-object v0, p0, Lcom/konka/mm/music/MusicListActivity;->proDlg:Landroid/app/ProgressDialog;

    if-nez v0, :cond_0

    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/konka/mm/music/MusicListActivity;->proDlg:Landroid/app/ProgressDialog;

    :cond_0
    iget-object v0, p0, Lcom/konka/mm/music/MusicListActivity;->proDlg:Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/konka/mm/music/MusicListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicListActivity;->proDlg:Landroid/app/ProgressDialog;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/konka/mm/music/MusicListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "..."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicListActivity;->proDlg:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    return-void
.end method

.method public snapScreen(I)V
    .locals 5
    .param p1    # I

    const/4 v4, 0x0

    const/4 v3, -0x1

    const/4 v2, 0x1

    invoke-virtual {p0, v4}, Lcom/konka/mm/music/MusicListActivity;->setBtnFocuseFlag(Z)V

    if-ne p1, v2, :cond_4

    iget v0, p0, Lcom/konka/mm/music/MusicListActivity;->currentPage:I

    iget v1, p0, Lcom/konka/mm/music/MusicListActivity;->pageCount:I

    add-int/lit8 v1, v1, -0x1

    if-lt v0, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/konka/mm/music/MusicListActivity;->viewFilpper:Landroid/widget/ViewFlipper;

    iget-object v1, p0, Lcom/konka/mm/music/MusicListActivity;->mPushLeftInAnim:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->setInAnimation(Landroid/view/animation/Animation;)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicListActivity;->viewFilpper:Landroid/widget/ViewFlipper;

    iget-object v1, p0, Lcom/konka/mm/music/MusicListActivity;->mPustRightOutAnim:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->setOutAnimation(Landroid/view/animation/Animation;)V

    iget v0, p0, Lcom/konka/mm/music/MusicListActivity;->curScreen:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/konka/mm/music/MusicListActivity;->curScreen:I

    iget v0, p0, Lcom/konka/mm/music/MusicListActivity;->curScreen:I

    rem-int/lit8 v0, v0, 0x3

    iput v0, p0, Lcom/konka/mm/music/MusicListActivity;->curScreen:I

    iget-object v0, p0, Lcom/konka/mm/music/MusicListActivity;->viewFilpper:Landroid/widget/ViewFlipper;

    invoke-virtual {v0}, Landroid/widget/ViewFlipper;->showNext()V

    invoke-virtual {p0, v2}, Lcom/konka/mm/music/MusicListActivity;->setPreOrNextScreen(I)V

    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/konka/mm/music/MusicListActivity;->pageGridView:[Landroid/widget/GridView;

    iget v1, p0, Lcom/konka/mm/music/MusicListActivity;->curScreen:I

    aget-object v0, v0, v1

    invoke-virtual {v0, v2}, Landroid/widget/GridView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicListActivity;->pageGridView:[Landroid/widget/GridView;

    iget v1, p0, Lcom/konka/mm/music/MusicListActivity;->curScreen:I

    aget-object v0, v0, v1

    invoke-virtual {v0}, Landroid/widget/GridView;->requestFocus()Z

    if-ne p1, v2, :cond_5

    iget-object v0, p0, Lcom/konka/mm/music/MusicListActivity;->pageGridView:[Landroid/widget/GridView;

    iget v1, p0, Lcom/konka/mm/music/MusicListActivity;->curScreen:I

    aget-object v0, v0, v1

    invoke-virtual {v0, v4}, Landroid/widget/GridView;->setSelection(I)V

    :goto_2
    if-ne p1, v2, :cond_6

    invoke-direct {p0, v4}, Lcom/konka/mm/music/MusicListActivity;->displayMusicInfo(I)V

    :cond_3
    :goto_3
    invoke-virtual {p0}, Lcom/konka/mm/music/MusicListActivity;->setPageInfo()V

    goto :goto_0

    :cond_4
    if-ne p1, v3, :cond_2

    iget v0, p0, Lcom/konka/mm/music/MusicListActivity;->currentPage:I

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/konka/mm/music/MusicListActivity;->viewFilpper:Landroid/widget/ViewFlipper;

    iget-object v1, p0, Lcom/konka/mm/music/MusicListActivity;->mPustRightInAnim:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->setInAnimation(Landroid/view/animation/Animation;)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicListActivity;->viewFilpper:Landroid/widget/ViewFlipper;

    iget-object v1, p0, Lcom/konka/mm/music/MusicListActivity;->mPushLeftOutAnim:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->setOutAnimation(Landroid/view/animation/Animation;)V

    iget v0, p0, Lcom/konka/mm/music/MusicListActivity;->curScreen:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/konka/mm/music/MusicListActivity;->curScreen:I

    iget v0, p0, Lcom/konka/mm/music/MusicListActivity;->curScreen:I

    add-int/lit8 v0, v0, 0x3

    rem-int/lit8 v0, v0, 0x3

    iput v0, p0, Lcom/konka/mm/music/MusicListActivity;->curScreen:I

    iget-object v0, p0, Lcom/konka/mm/music/MusicListActivity;->viewFilpper:Landroid/widget/ViewFlipper;

    invoke-virtual {v0}, Landroid/widget/ViewFlipper;->showPrevious()V

    invoke-virtual {p0, v3}, Lcom/konka/mm/music/MusicListActivity;->setPreOrNextScreen(I)V

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lcom/konka/mm/music/MusicListActivity;->pageGridView:[Landroid/widget/GridView;

    iget v1, p0, Lcom/konka/mm/music/MusicListActivity;->curScreen:I

    aget-object v0, v0, v1

    iget v1, p0, Lcom/konka/mm/music/MusicListActivity;->numColumns:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setSelection(I)V

    goto :goto_2

    :cond_6
    if-ne p1, v3, :cond_3

    iget v0, p0, Lcom/konka/mm/music/MusicListActivity;->numColumns:I

    add-int/lit8 v0, v0, -0x1

    invoke-direct {p0, v0}, Lcom/konka/mm/music/MusicListActivity;->displayMusicInfo(I)V

    goto :goto_3
.end method

.method public sortBtnOnKeyOrOnClickEvent()V
    .locals 6

    const/16 v3, 0x113

    const/4 v5, 0x1

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/konka/mm/music/MusicListActivity;->popupWindow:Landroid/widget/PopupWindow;

    if-nez v0, :cond_0

    iget v0, p0, Lcom/konka/mm/music/MusicListActivity;->mScreen_mode:I

    if-nez v0, :cond_1

    new-instance v0, Landroid/widget/PopupWindow;

    iget-object v1, p0, Lcom/konka/mm/music/MusicListActivity;->popupView:Landroid/view/View;

    const/16 v2, 0x96

    const/16 v3, 0xb6

    invoke-direct {v0, v1, v2, v3, v5}, Landroid/widget/PopupWindow;-><init>(Landroid/view/View;IIZ)V

    iput-object v0, p0, Lcom/konka/mm/music/MusicListActivity;->popupWindow:Landroid/widget/PopupWindow;

    :goto_0
    iget-object v0, p0, Lcom/konka/mm/music/MusicListActivity;->popupWindow:Landroid/widget/PopupWindow;

    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v1}, Landroid/graphics/drawable/BitmapDrawable;-><init>()V

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicListActivity;->popupWindow:Landroid/widget/PopupWindow;

    new-instance v1, Lcom/konka/mm/music/MusicListActivity$11;

    invoke-direct {v1, p0}, Lcom/konka/mm/music/MusicListActivity$11;-><init>(Lcom/konka/mm/music/MusicListActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V

    :cond_0
    iget-boolean v0, p0, Lcom/konka/mm/music/MusicListActivity;->isPopup:Z

    if-nez v0, :cond_5

    iget v0, p0, Lcom/konka/mm/music/MusicListActivity;->mScreen_mode:I

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/konka/mm/music/MusicListActivity;->popupWindow:Landroid/widget/PopupWindow;

    iget-object v1, p0, Lcom/konka/mm/music/MusicListActivity;->sort_btn:Landroid/widget/ImageView;

    const/16 v2, -0x3d

    invoke-virtual {v0, v1, v2, v4}, Landroid/widget/PopupWindow;->showAsDropDown(Landroid/view/View;II)V

    :goto_1
    iput-boolean v5, p0, Lcom/konka/mm/music/MusicListActivity;->isPopup:Z

    :goto_2
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/konka/mm/music/MusicListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f090000    # com.konka.mm.R.string.str_sys_language

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/mm/music/MusicListActivity;->CURRENT_LANGUAGE:Ljava/lang/String;

    const-string v0, "zh"

    iget-object v1, p0, Lcom/konka/mm/music/MusicListActivity;->CURRENT_LANGUAGE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Landroid/widget/PopupWindow;

    iget-object v1, p0, Lcom/konka/mm/music/MusicListActivity;->popupView:Landroid/view/View;

    const/16 v2, 0xc8

    invoke-direct {v0, v1, v2, v3, v5}, Landroid/widget/PopupWindow;-><init>(Landroid/view/View;IIZ)V

    iput-object v0, p0, Lcom/konka/mm/music/MusicListActivity;->popupWindow:Landroid/widget/PopupWindow;

    goto :goto_0

    :cond_2
    new-instance v0, Landroid/widget/PopupWindow;

    iget-object v1, p0, Lcom/konka/mm/music/MusicListActivity;->popupView:Landroid/view/View;

    const/16 v2, 0x140

    invoke-direct {v0, v1, v2, v3, v5}, Landroid/widget/PopupWindow;-><init>(Landroid/view/View;IIZ)V

    iput-object v0, p0, Lcom/konka/mm/music/MusicListActivity;->popupWindow:Landroid/widget/PopupWindow;

    goto :goto_0

    :cond_3
    const-string v0, "zh"

    iget-object v1, p0, Lcom/konka/mm/music/MusicListActivity;->CURRENT_LANGUAGE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/konka/mm/music/MusicListActivity;->popupWindow:Landroid/widget/PopupWindow;

    iget-object v1, p0, Lcom/konka/mm/music/MusicListActivity;->popupView:Landroid/view/View;

    const/16 v2, -0x4b

    invoke-virtual {v0, v1, v2, v4}, Landroid/widget/PopupWindow;->showAsDropDown(Landroid/view/View;II)V

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lcom/konka/mm/music/MusicListActivity;->popupWindow:Landroid/widget/PopupWindow;

    iget-object v1, p0, Lcom/konka/mm/music/MusicListActivity;->sort_btn:Landroid/widget/ImageView;

    const/16 v2, -0xbe

    invoke-virtual {v0, v1, v2, v4}, Landroid/widget/PopupWindow;->showAsDropDown(Landroid/view/View;II)V

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lcom/konka/mm/music/MusicListActivity;->popupWindow:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    iput-boolean v4, p0, Lcom/konka/mm/music/MusicListActivity;->isPopup:Z

    goto :goto_2
.end method

.method public updateDatabase()V
    .locals 3

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/konka/mm/music/MusicListActivity;->stopScanRunBackground:Z

    const/4 v2, 0x2

    invoke-virtual {p0, v2}, Lcom/konka/mm/music/MusicListActivity;->showDialog(I)V

    new-instance v1, Lcom/konka/mm/music/MusicListActivity$UpdateDataRunnable;

    invoke-direct {v1, p0}, Lcom/konka/mm/music/MusicListActivity$UpdateDataRunnable;-><init>(Lcom/konka/mm/music/MusicListActivity;)V

    new-instance v0, Ljava/lang/Thread;

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method public writeAllDataToDB()V
    .locals 4

    const-string v2, "MusicListActivity"

    const-string v3, "the background thread start scan audio data and write data to database!"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Lcom/konka/mm/music/MusicListActivity$writeAllDataToDBRunnable;

    invoke-direct {v1, p0}, Lcom/konka/mm/music/MusicListActivity$writeAllDataToDBRunnable;-><init>(Lcom/konka/mm/music/MusicListActivity;)V

    new-instance v0, Ljava/lang/Thread;

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method public writeToDBBackground(Ljava/lang/String;Ljava/lang/String;)V
    .locals 20
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    new-instance v5, Ljava/io/File;

    move-object/from16 v0, p1

    invoke-direct {v5, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v8

    if-nez v8, :cond_1

    :cond_0
    return-void

    :cond_1
    const/4 v9, 0x0

    :goto_0
    array-length v0, v8

    move/from16 v17, v0

    move/from16 v0, v17

    if-ge v9, v0, :cond_0

    aget-object v17, v8, v9

    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v17

    const-string v18, "$RECYCLE.BIN"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-nez v17, :cond_2

    aget-object v17, v8, v9

    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v17

    const-string v18, "kk.com.konka.mm.ImgCach"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_3

    :cond_2
    :goto_1
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    :cond_3
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/konka/mm/music/MusicListActivity;->stopScanRunBackground:Z

    move/from16 v17, v0

    if-nez v17, :cond_0

    aget-object v17, v8, v9

    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->isDirectory()Z

    move-result v17

    if-eqz v17, :cond_4

    aget-object v17, v8, v9

    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move-object/from16 v2, p2

    invoke-virtual {v0, v1, v2}, Lcom/konka/mm/music/MusicListActivity;->writeToDBBackground(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_4
    aget-object v17, v8, v9

    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {p0 .. p0}, Lcom/konka/mm/music/MusicListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    const v19, 0x7f070001    # com.konka.mm.R.array.fileEndingAudio

    invoke-virtual/range {v18 .. v19}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/konka/mm/tools/FileTool;->checkEndsWithInStringArray(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_2

    aget-object v17, v8, v9

    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/mm/music/MusicListActivity;->mRootPath:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->length()I

    move-result v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v13

    new-instance v10, Lcom/konka/mm/data/MusicData;

    invoke-direct {v10}, Lcom/konka/mm/data/MusicData;-><init>()V

    invoke-virtual {v10, v13}, Lcom/konka/mm/data/MusicData;->setPath(Ljava/lang/String;)V

    const-string v17, "."

    move-object/from16 v0, v17

    invoke-virtual {v13, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v17

    add-int/lit8 v17, v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v13, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v10, v0}, Lcom/konka/mm/data/MusicData;->setMimeType(Ljava/lang/String;)V

    const-string v17, "/"

    move-object/from16 v0, v17

    invoke-virtual {v13, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v17

    add-int/lit8 v17, v17, 0x1

    const-string v18, "."

    move-object/from16 v0, v18

    invoke-virtual {v13, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v18

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v10, v15}, Lcom/konka/mm/data/MusicData;->setTitle(Ljava/lang/String;)V

    const-string v17, "/"

    move-object/from16 v0, v17

    invoke-virtual {v13, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v17

    add-int/lit8 v17, v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v13, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v10, v14}, Lcom/konka/mm/data/MusicData;->setDisplayName(Ljava/lang/String;)V

    new-instance v7, Ljava/io/File;

    aget-object v17, v8, v9

    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-direct {v7, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const-wide/16 v11, 0x0

    :try_start_0
    invoke-static {v7}, Lcom/konka/mm/tools/FileTool;->getFileSizes(Ljava/io/File;)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v11

    :goto_2
    invoke-virtual {v10, v11, v12}, Lcom/konka/mm/data/MusicData;->setSize(J)V

    invoke-virtual {v7}, Ljava/io/File;->lastModified()J

    move-result-wide v3

    invoke-virtual {v10, v3, v4}, Lcom/konka/mm/data/MusicData;->setDate(J)V

    const-string v17, "unknown"

    move-object/from16 v0, v17

    invoke-virtual {v10, v0}, Lcom/konka/mm/data/MusicData;->setAlbum(Ljava/lang/String;)V

    const-string v17, "unknown"

    move-object/from16 v0, v17

    invoke-virtual {v10, v0}, Lcom/konka/mm/data/MusicData;->setArtist(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/mm/music/MusicListActivity;->mDBBackgroundHelper:Lcom/konka/mm/tools/DBBackgroundHelper;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v10}, Lcom/konka/mm/tools/DBBackgroundHelper;->insertAudio(Lcom/konka/mm/data/MusicData;)V

    goto/16 :goto_1

    :catch_0
    move-exception v6

    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2
.end method

.method public writeToDBCurrent(Ljava/lang/String;Ljava/lang/String;)V
    .locals 20
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    new-instance v5, Ljava/io/File;

    move-object/from16 v0, p1

    invoke-direct {v5, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v8

    if-nez v8, :cond_1

    :cond_0
    return-void

    :cond_1
    const/4 v9, 0x0

    :goto_0
    array-length v0, v8

    move/from16 v17, v0

    move/from16 v0, v17

    if-ge v9, v0, :cond_0

    aget-object v17, v8, v9

    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v17

    const-string v18, "$RECYCLE.BIN"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-nez v17, :cond_2

    aget-object v17, v8, v9

    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v17

    const-string v18, "kk.com.konka.mm.ImgCach"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_3

    :cond_2
    :goto_1
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    :cond_3
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/konka/mm/music/MusicListActivity;->stopScanRun:Z

    move/from16 v17, v0

    if-nez v17, :cond_0

    aget-object v17, v8, v9

    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->isDirectory()Z

    move-result v17

    if-eqz v17, :cond_4

    aget-object v17, v8, v9

    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move-object/from16 v2, p2

    invoke-virtual {v0, v1, v2}, Lcom/konka/mm/music/MusicListActivity;->writeToDBCurrent(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_4
    aget-object v17, v8, v9

    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {p0 .. p0}, Lcom/konka/mm/music/MusicListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    const v19, 0x7f070001    # com.konka.mm.R.array.fileEndingAudio

    invoke-virtual/range {v18 .. v19}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/konka/mm/tools/FileTool;->checkEndsWithInStringArray(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_2

    aget-object v17, v8, v9

    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/mm/music/MusicListActivity;->mRootPath:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->length()I

    move-result v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v13

    new-instance v10, Lcom/konka/mm/data/MusicData;

    invoke-direct {v10}, Lcom/konka/mm/data/MusicData;-><init>()V

    invoke-virtual {v10, v13}, Lcom/konka/mm/data/MusicData;->setPath(Ljava/lang/String;)V

    const-string v17, "."

    move-object/from16 v0, v17

    invoke-virtual {v13, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v17

    add-int/lit8 v17, v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v13, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v10, v0}, Lcom/konka/mm/data/MusicData;->setMimeType(Ljava/lang/String;)V

    const-string v17, "/"

    move-object/from16 v0, v17

    invoke-virtual {v13, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v17

    add-int/lit8 v17, v17, 0x1

    const-string v18, "."

    move-object/from16 v0, v18

    invoke-virtual {v13, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v18

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v10, v15}, Lcom/konka/mm/data/MusicData;->setTitle(Ljava/lang/String;)V

    const-string v17, "/"

    move-object/from16 v0, v17

    invoke-virtual {v13, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v17

    add-int/lit8 v17, v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v13, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v10, v14}, Lcom/konka/mm/data/MusicData;->setDisplayName(Ljava/lang/String;)V

    new-instance v7, Ljava/io/File;

    aget-object v17, v8, v9

    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-direct {v7, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const-wide/16 v11, 0x0

    :try_start_0
    invoke-static {v7}, Lcom/konka/mm/tools/FileTool;->getFileSizes(Ljava/io/File;)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v11

    :goto_2
    invoke-virtual {v10, v11, v12}, Lcom/konka/mm/data/MusicData;->setSize(J)V

    invoke-virtual {v7}, Ljava/io/File;->lastModified()J

    move-result-wide v3

    invoke-virtual {v10, v3, v4}, Lcom/konka/mm/data/MusicData;->setDate(J)V

    const-string v17, "unknown"

    move-object/from16 v0, v17

    invoke-virtual {v10, v0}, Lcom/konka/mm/data/MusicData;->setAlbum(Ljava/lang/String;)V

    const-string v17, "unknown"

    move-object/from16 v0, v17

    invoke-virtual {v10, v0}, Lcom/konka/mm/data/MusicData;->setArtist(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/mm/music/MusicListActivity;->mDBHelper:Lcom/konka/mm/tools/DBHelper1;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v10}, Lcom/konka/mm/tools/DBHelper1;->insertAudio(Lcom/konka/mm/data/MusicData;)V

    goto/16 :goto_1

    :catch_0
    move-exception v6

    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2
.end method
