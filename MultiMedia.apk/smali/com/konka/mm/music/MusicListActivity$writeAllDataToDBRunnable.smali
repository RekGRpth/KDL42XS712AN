.class public Lcom/konka/mm/music/MusicListActivity$writeAllDataToDBRunnable;
.super Ljava/lang/Object;
.source "MusicListActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/mm/music/MusicListActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "writeAllDataToDBRunnable"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/mm/music/MusicListActivity;


# direct methods
.method public constructor <init>(Lcom/konka/mm/music/MusicListActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/music/MusicListActivity$writeAllDataToDBRunnable;->this$0:Lcom/konka/mm/music/MusicListActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    iget-object v0, p0, Lcom/konka/mm/music/MusicListActivity$writeAllDataToDBRunnable;->this$0:Lcom/konka/mm/music/MusicListActivity;

    iget-object v0, v0, Lcom/konka/mm/music/MusicListActivity;->mDBBackgroundHelper:Lcom/konka/mm/tools/DBBackgroundHelper;

    invoke-virtual {v0}, Lcom/konka/mm/tools/DBBackgroundHelper;->deleteAllAudio()V

    iget-object v0, p0, Lcom/konka/mm/music/MusicListActivity$writeAllDataToDBRunnable;->this$0:Lcom/konka/mm/music/MusicListActivity;

    iget-object v1, p0, Lcom/konka/mm/music/MusicListActivity$writeAllDataToDBRunnable;->this$0:Lcom/konka/mm/music/MusicListActivity;

    # getter for: Lcom/konka/mm/music/MusicListActivity;->mRootPath:Ljava/lang/String;
    invoke-static {v1}, Lcom/konka/mm/music/MusicListActivity;->access$9(Lcom/konka/mm/music/MusicListActivity;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/mm/music/MusicListActivity$writeAllDataToDBRunnable;->this$0:Lcom/konka/mm/music/MusicListActivity;

    # getter for: Lcom/konka/mm/music/MusicListActivity;->mRootPath:Ljava/lang/String;
    invoke-static {v2}, Lcom/konka/mm/music/MusicListActivity;->access$9(Lcom/konka/mm/music/MusicListActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/konka/mm/music/MusicListActivity;->writeToDBBackground(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "MusicListActivity"

    const-string v1, "background thread scan audio data finish"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/mm/music/MusicListActivity$writeAllDataToDBRunnable;->this$0:Lcom/konka/mm/music/MusicListActivity;

    iget-boolean v0, v0, Lcom/konka/mm/music/MusicListActivity;->stopScanRunBackground:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/konka/mm/music/MusicListActivity$writeAllDataToDBRunnable;->this$0:Lcom/konka/mm/music/MusicListActivity;

    invoke-virtual {v0}, Lcom/konka/mm/music/MusicListActivity;->compareDataChange()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/konka/mm/music/MusicListActivity$writeAllDataToDBRunnable;->this$0:Lcom/konka/mm/music/MusicListActivity;

    # getter for: Lcom/konka/mm/music/MusicListActivity;->mRootPath:Ljava/lang/String;
    invoke-static {v1}, Lcom/konka/mm/music/MusicListActivity;->access$9(Lcom/konka/mm/music/MusicListActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "/.multimediaThumbs/databases/background/storage/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "multimedia.db"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/konka/mm/music/MusicListActivity$writeAllDataToDBRunnable;->this$0:Lcom/konka/mm/music/MusicListActivity;

    # getter for: Lcom/konka/mm/music/MusicListActivity;->mRootPath:Ljava/lang/String;
    invoke-static {v2}, Lcom/konka/mm/music/MusicListActivity;->access$9(Lcom/konka/mm/music/MusicListActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "/.multimediaThumbs/databases/storage/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "multimedia.db"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/konka/mm/tools/FileTool;->copyFile(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/mm/music/MusicListActivity$writeAllDataToDBRunnable;->this$0:Lcom/konka/mm/music/MusicListActivity;

    # getter for: Lcom/konka/mm/music/MusicListActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/konka/mm/music/MusicListActivity;->access$11(Lcom/konka/mm/music/MusicListActivity;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method
