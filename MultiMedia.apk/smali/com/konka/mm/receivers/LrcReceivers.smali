.class public Lcom/konka/mm/receivers/LrcReceivers;
.super Landroid/content/BroadcastReceiver;
.source "LrcReceivers.java"


# instance fields
.field private textView:Lcom/konka/mm/music/LyricView;


# direct methods
.method public constructor <init>(Lcom/konka/mm/music/LyricView;)V
    .locals 0
    .param p1    # Lcom/konka/mm/music/LyricView;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    iput-object p1, p0, Lcom/konka/mm/receivers/LrcReceivers;->textView:Lcom/konka/mm/music/LyricView;

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const-string v1, "message"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/mm/receivers/LrcReceivers;->textView:Lcom/konka/mm/music/LyricView;

    invoke-virtual {v1, v0}, Lcom/konka/mm/music/LyricView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
