.class public Lcom/konka/mm/receivers/SmartControl;
.super Ljava/lang/Object;
.source "SmartControl.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/mm/receivers/SmartControl$PlayerType;
    }
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$konka$mm$receivers$SmartControl$PlayerType:[I


# instance fields
.field private mReceiver:Landroid/content/BroadcastReceiver;

.field private m_Activity:Landroid/content/Context;

.field private m_MusicActivity:Lcom/konka/mm/music/MusicActivity;

.field private m_PlayerType:Lcom/konka/mm/receivers/SmartControl$PlayerType;


# direct methods
.method static synthetic $SWITCH_TABLE$com$konka$mm$receivers$SmartControl$PlayerType()[I
    .locals 3

    sget-object v0, Lcom/konka/mm/receivers/SmartControl;->$SWITCH_TABLE$com$konka$mm$receivers$SmartControl$PlayerType:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/konka/mm/receivers/SmartControl$PlayerType;->values()[Lcom/konka/mm/receivers/SmartControl$PlayerType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/konka/mm/receivers/SmartControl$PlayerType;->MUSIC:Lcom/konka/mm/receivers/SmartControl$PlayerType;

    invoke-virtual {v1}, Lcom/konka/mm/receivers/SmartControl$PlayerType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_2

    :goto_1
    :try_start_1
    sget-object v1, Lcom/konka/mm/receivers/SmartControl$PlayerType;->PHOTO:Lcom/konka/mm/receivers/SmartControl$PlayerType;

    invoke-virtual {v1}, Lcom/konka/mm/receivers/SmartControl$PlayerType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    :try_start_2
    sget-object v1, Lcom/konka/mm/receivers/SmartControl$PlayerType;->VIDEO:Lcom/konka/mm/receivers/SmartControl$PlayerType;

    invoke-virtual {v1}, Lcom/konka/mm/receivers/SmartControl$PlayerType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_0

    :goto_3
    sput-object v0, Lcom/konka/mm/receivers/SmartControl;->$SWITCH_TABLE$com$konka$mm$receivers$SmartControl$PlayerType:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_3

    :catch_1
    move-exception v1

    goto :goto_2

    :catch_2
    move-exception v1

    goto :goto_1
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/konka/mm/receivers/SmartControl$PlayerType;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/konka/mm/receivers/SmartControl$PlayerType;

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/konka/mm/receivers/SmartControl;->m_Activity:Landroid/content/Context;

    iput-object v0, p0, Lcom/konka/mm/receivers/SmartControl;->m_MusicActivity:Lcom/konka/mm/music/MusicActivity;

    new-instance v0, Lcom/konka/mm/receivers/SmartControl$1;

    invoke-direct {v0, p0}, Lcom/konka/mm/receivers/SmartControl$1;-><init>(Lcom/konka/mm/receivers/SmartControl;)V

    iput-object v0, p0, Lcom/konka/mm/receivers/SmartControl;->mReceiver:Landroid/content/BroadcastReceiver;

    iput-object p1, p0, Lcom/konka/mm/receivers/SmartControl;->m_Activity:Landroid/content/Context;

    iput-object p2, p0, Lcom/konka/mm/receivers/SmartControl;->m_PlayerType:Lcom/konka/mm/receivers/SmartControl$PlayerType;

    invoke-static {}, Lcom/konka/mm/receivers/SmartControl;->$SWITCH_TABLE$com$konka$mm$receivers$SmartControl$PlayerType()[I

    move-result-object v0

    invoke-virtual {p2}, Lcom/konka/mm/receivers/SmartControl$PlayerType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    const-string v0, "\u8fd8\u6ca1\u6709\u6dfb\u52a0\u6b64\u7c7b\u578b"

    invoke-static {v0}, Liapp/eric/utils/base/Trace;->Info(Ljava/lang/String;)V

    :goto_0
    return-void

    :pswitch_0
    check-cast p1, Lcom/konka/mm/music/MusicActivity;

    iput-object p1, p0, Lcom/konka/mm/receivers/SmartControl;->m_MusicActivity:Lcom/konka/mm/music/MusicActivity;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method static synthetic access$0(Lcom/konka/mm/receivers/SmartControl;)Lcom/konka/mm/receivers/SmartControl$PlayerType;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/receivers/SmartControl;->m_PlayerType:Lcom/konka/mm/receivers/SmartControl$PlayerType;

    return-object v0
.end method

.method static synthetic access$1(Lcom/konka/mm/receivers/SmartControl;)Lcom/konka/mm/music/MusicActivity;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/receivers/SmartControl;->m_MusicActivity:Lcom/konka/mm/music/MusicActivity;

    return-object v0
.end method

.method private initVAFBroadcastReceiver()V
    .locals 3

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "konka.voice.control.action.PLAYER_CONTROL"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/mm/receivers/SmartControl;->m_Activity:Landroid/content/Context;

    iget-object v2, p0, Lcom/konka/mm/receivers/SmartControl;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method


# virtual methods
.method public End()V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.konka.smartcontrol.palyer_exit"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/konka/mm/receivers/SmartControl;->m_Activity:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    iget-object v1, p0, Lcom/konka/mm/receivers/SmartControl;->m_Activity:Landroid/content/Context;

    iget-object v2, p0, Lcom/konka/mm/receivers/SmartControl;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method

.method public Start()V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.konka.smartcontrol.palyer_start"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/konka/mm/receivers/SmartControl;->m_Activity:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    invoke-direct {p0}, Lcom/konka/mm/receivers/SmartControl;->initVAFBroadcastReceiver()V

    return-void
.end method
