.class public final enum Lcom/konka/mm/receivers/SmartControl$PlayerType;
.super Ljava/lang/Enum;
.source "SmartControl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/mm/receivers/SmartControl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "PlayerType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/konka/mm/receivers/SmartControl$PlayerType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lcom/konka/mm/receivers/SmartControl$PlayerType;

.field public static final enum MUSIC:Lcom/konka/mm/receivers/SmartControl$PlayerType;

.field public static final enum PHOTO:Lcom/konka/mm/receivers/SmartControl$PlayerType;

.field public static final enum VIDEO:Lcom/konka/mm/receivers/SmartControl$PlayerType;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/konka/mm/receivers/SmartControl$PlayerType;

    const-string v1, "MUSIC"

    invoke-direct {v0, v1, v2}, Lcom/konka/mm/receivers/SmartControl$PlayerType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/mm/receivers/SmartControl$PlayerType;->MUSIC:Lcom/konka/mm/receivers/SmartControl$PlayerType;

    new-instance v0, Lcom/konka/mm/receivers/SmartControl$PlayerType;

    const-string v1, "VIDEO"

    invoke-direct {v0, v1, v3}, Lcom/konka/mm/receivers/SmartControl$PlayerType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/mm/receivers/SmartControl$PlayerType;->VIDEO:Lcom/konka/mm/receivers/SmartControl$PlayerType;

    new-instance v0, Lcom/konka/mm/receivers/SmartControl$PlayerType;

    const-string v1, "PHOTO"

    invoke-direct {v0, v1, v4}, Lcom/konka/mm/receivers/SmartControl$PlayerType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/mm/receivers/SmartControl$PlayerType;->PHOTO:Lcom/konka/mm/receivers/SmartControl$PlayerType;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/konka/mm/receivers/SmartControl$PlayerType;

    sget-object v1, Lcom/konka/mm/receivers/SmartControl$PlayerType;->MUSIC:Lcom/konka/mm/receivers/SmartControl$PlayerType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/konka/mm/receivers/SmartControl$PlayerType;->VIDEO:Lcom/konka/mm/receivers/SmartControl$PlayerType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/konka/mm/receivers/SmartControl$PlayerType;->PHOTO:Lcom/konka/mm/receivers/SmartControl$PlayerType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/konka/mm/receivers/SmartControl$PlayerType;->ENUM$VALUES:[Lcom/konka/mm/receivers/SmartControl$PlayerType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/konka/mm/receivers/SmartControl$PlayerType;
    .locals 1

    const-class v0, Lcom/konka/mm/receivers/SmartControl$PlayerType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/konka/mm/receivers/SmartControl$PlayerType;

    return-object v0
.end method

.method public static values()[Lcom/konka/mm/receivers/SmartControl$PlayerType;
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lcom/konka/mm/receivers/SmartControl$PlayerType;->ENUM$VALUES:[Lcom/konka/mm/receivers/SmartControl$PlayerType;

    array-length v1, v0

    new-array v2, v1, [Lcom/konka/mm/receivers/SmartControl$PlayerType;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
