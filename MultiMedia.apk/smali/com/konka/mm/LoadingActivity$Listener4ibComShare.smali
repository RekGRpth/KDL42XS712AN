.class Lcom/konka/mm/LoadingActivity$Listener4ibComShare;
.super Ljava/lang/Object;
.source "LoadingActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/mm/LoadingActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "Listener4ibComShare"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/mm/LoadingActivity;


# direct methods
.method constructor <init>(Lcom/konka/mm/LoadingActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/mm/LoadingActivity$Listener4ibComShare;->this$0:Lcom/konka/mm/LoadingActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    const-string v2, "MMLoading"

    const-string v3, "goto share!"

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    iget-object v2, p0, Lcom/konka/mm/LoadingActivity$Listener4ibComShare;->this$0:Lcom/konka/mm/LoadingActivity;

    const-class v3, Lcom/konka/mm/FamilyShareMainActivity;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    :try_start_0
    iget-object v2, p0, Lcom/konka/mm/LoadingActivity$Listener4ibComShare;->this$0:Lcom/konka/mm/LoadingActivity;

    invoke-virtual {v2, v1}, Lcom/konka/mm/LoadingActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method
