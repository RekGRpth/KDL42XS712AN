.class public Lcom/konka/mm/GlobalData;
.super Landroid/app/Application;
.source "GlobalData.java"


# instance fields
.field private mMMFileList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mPicCurrentPos:I

.field private ma:Lcom/konka/mm/music/MusicActivity;

.field private showWhat:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/konka/mm/GlobalData;->showWhat:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/konka/mm/GlobalData;->ma:Lcom/konka/mm/music/MusicActivity;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/konka/mm/GlobalData;->mMMFileList:Ljava/util/ArrayList;

    const/4 v0, -0x1

    iput v0, p0, Lcom/konka/mm/GlobalData;->mPicCurrentPos:I

    return-void
.end method


# virtual methods
.method public getMa()Lcom/konka/mm/music/MusicActivity;
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/GlobalData;->ma:Lcom/konka/mm/music/MusicActivity;

    return-object v0
.end method

.method public getShowWhat()I
    .locals 1

    iget v0, p0, Lcom/konka/mm/GlobalData;->showWhat:I

    return v0
.end method

.method public getmMMFileList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/konka/mm/GlobalData;->mMMFileList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getmPicCurrentPos()I
    .locals 1

    iget v0, p0, Lcom/konka/mm/GlobalData;->mPicCurrentPos:I

    return v0
.end method

.method public setMa(Lcom/konka/mm/music/MusicActivity;)V
    .locals 0
    .param p1    # Lcom/konka/mm/music/MusicActivity;

    iput-object p1, p0, Lcom/konka/mm/GlobalData;->ma:Lcom/konka/mm/music/MusicActivity;

    return-void
.end method

.method public setShowWhat(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/konka/mm/GlobalData;->showWhat:I

    return-void
.end method

.method public setmMMFileList(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/konka/mm/GlobalData;->mMMFileList:Ljava/util/ArrayList;

    return-void
.end method

.method public setmPicCurrentPos(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/konka/mm/GlobalData;->mPicCurrentPos:I

    return-void
.end method

.method public stopMusicService()V
    .locals 1

    iget-object v0, p0, Lcom/konka/mm/GlobalData;->ma:Lcom/konka/mm/music/MusicActivity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/mm/GlobalData;->ma:Lcom/konka/mm/music/MusicActivity;

    invoke-virtual {v0}, Lcom/konka/mm/music/MusicActivity;->stopMusicService()V

    :goto_0
    return-void

    :cond_0
    const-string v0, "Musci activity is null!"

    invoke-static {v0}, Liapp/eric/utils/base/Trace;->Info(Ljava/lang/String;)V

    goto :goto_0
.end method
