.class public Lcom/konka/kkimplements/tv/vo/KonkaProgCount;
.super Ljava/lang/Object;
.source "KonkaProgCount.java"


# static fields
.field private static synthetic $SWITCH_TABLE$com$mstar$android$tvapi$common$vo$EnumProgramCountType:[I

.field private static synthetic $SWITCH_TABLE$com$mstar$android$tvapi$common$vo$EnumServiceType:[I


# instance fields
.field public DTVAllProgCount:I

.field public DTVDataProgCount:I

.field public DTVRadioProgCount:I

.field public DTVTvProgCount:I


# direct methods
.method static synthetic $SWITCH_TABLE$com$mstar$android$tvapi$common$vo$EnumProgramCountType()[I
    .locals 3

    sget-object v0, Lcom/konka/kkimplements/tv/vo/KonkaProgCount;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$EnumProgramCountType:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;->values()[Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;->E_COUNT_ATV:Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_6

    :goto_1
    :try_start_1
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;->E_COUNT_ATV_DTV:Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_5

    :goto_2
    :try_start_2
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;->E_COUNT_DTV:Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_4

    :goto_3
    :try_start_3
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;->E_COUNT_DTV_DATA:Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_3

    :goto_4
    :try_start_4
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;->E_COUNT_DTV_RADIO:Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_2

    :goto_5
    :try_start_5
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;->E_COUNT_DTV_TV:Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_1

    :goto_6
    :try_start_6
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;->E_COUNT_TYPE_MAX:Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_0

    :goto_7
    sput-object v0, Lcom/konka/kkimplements/tv/vo/KonkaProgCount;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$EnumProgramCountType:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_7

    :catch_1
    move-exception v1

    goto :goto_6

    :catch_2
    move-exception v1

    goto :goto_5

    :catch_3
    move-exception v1

    goto :goto_4

    :catch_4
    move-exception v1

    goto :goto_3

    :catch_5
    move-exception v1

    goto :goto_2

    :catch_6
    move-exception v1

    goto :goto_1
.end method

.method static synthetic $SWITCH_TABLE$com$mstar$android$tvapi$common$vo$EnumServiceType()[I
    .locals 3

    sget-object v0, Lcom/konka/kkimplements/tv/vo/KonkaProgCount;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$EnumServiceType:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->values()[Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->E_SERVICETYPE_ATV:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_5

    :goto_1
    :try_start_1
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->E_SERVICETYPE_DATA:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_4

    :goto_2
    :try_start_2
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->E_SERVICETYPE_DTV:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_3

    :goto_3
    :try_start_3
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->E_SERVICETYPE_INVALID:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_2

    :goto_4
    :try_start_4
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->E_SERVICETYPE_RADIO:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_1

    :goto_5
    :try_start_5
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->E_SERVICETYPE_UNITED_TV:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_0

    :goto_6
    sput-object v0, Lcom/konka/kkimplements/tv/vo/KonkaProgCount;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$EnumServiceType:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_6

    :catch_1
    move-exception v1

    goto :goto_5

    :catch_2
    move-exception v1

    goto :goto_4

    :catch_3
    move-exception v1

    goto :goto_3

    :catch_4
    move-exception v1

    goto :goto_2

    :catch_5
    move-exception v1

    goto :goto_1
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/konka/kkimplements/tv/vo/KonkaProgCount;->DTVTvProgCount:I

    iput v0, p0, Lcom/konka/kkimplements/tv/vo/KonkaProgCount;->DTVRadioProgCount:I

    iput v0, p0, Lcom/konka/kkimplements/tv/vo/KonkaProgCount;->DTVDataProgCount:I

    iput v0, p0, Lcom/konka/kkimplements/tv/vo/KonkaProgCount;->DTVAllProgCount:I

    iput v0, p0, Lcom/konka/kkimplements/tv/vo/KonkaProgCount;->DTVTvProgCount:I

    iput v0, p0, Lcom/konka/kkimplements/tv/vo/KonkaProgCount;->DTVRadioProgCount:I

    iput v0, p0, Lcom/konka/kkimplements/tv/vo/KonkaProgCount;->DTVDataProgCount:I

    iput v0, p0, Lcom/konka/kkimplements/tv/vo/KonkaProgCount;->DTVAllProgCount:I

    return-void
.end method

.method public constructor <init>(IIII)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/konka/kkimplements/tv/vo/KonkaProgCount;->DTVTvProgCount:I

    iput v0, p0, Lcom/konka/kkimplements/tv/vo/KonkaProgCount;->DTVRadioProgCount:I

    iput v0, p0, Lcom/konka/kkimplements/tv/vo/KonkaProgCount;->DTVDataProgCount:I

    iput v0, p0, Lcom/konka/kkimplements/tv/vo/KonkaProgCount;->DTVAllProgCount:I

    iput p1, p0, Lcom/konka/kkimplements/tv/vo/KonkaProgCount;->DTVTvProgCount:I

    iput p2, p0, Lcom/konka/kkimplements/tv/vo/KonkaProgCount;->DTVRadioProgCount:I

    iput p3, p0, Lcom/konka/kkimplements/tv/vo/KonkaProgCount;->DTVDataProgCount:I

    iput p4, p0, Lcom/konka/kkimplements/tv/vo/KonkaProgCount;->DTVAllProgCount:I

    return-void
.end method


# virtual methods
.method public getProgCountByServiceType(Lcom/mstar/android/tvapi/common/vo/EnumServiceType;)I
    .locals 3
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    const/4 v0, 0x0

    invoke-static {}, Lcom/konka/kkimplements/tv/vo/KonkaProgCount;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$EnumServiceType()[I

    move-result-object v1

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_0
    iget v0, p0, Lcom/konka/kkimplements/tv/vo/KonkaProgCount;->DTVTvProgCount:I

    goto :goto_0

    :pswitch_1
    iget v0, p0, Lcom/konka/kkimplements/tv/vo/KonkaProgCount;->DTVRadioProgCount:I

    goto :goto_0

    :pswitch_2
    iget v0, p0, Lcom/konka/kkimplements/tv/vo/KonkaProgCount;->DTVDataProgCount:I

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public isNoProgInServiceType(Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;)Z
    .locals 5
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v0, 0x0

    invoke-static {}, Lcom/konka/kkimplements/tv/vo/KonkaProgCount;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$EnumProgramCountType()[I

    move-result-object v3

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    iget v3, p0, Lcom/konka/kkimplements/tv/vo/KonkaProgCount;->DTVAllProgCount:I

    if-nez v3, :cond_3

    move v0, v1

    :goto_0
    return v0

    :pswitch_0
    iget v3, p0, Lcom/konka/kkimplements/tv/vo/KonkaProgCount;->DTVTvProgCount:I

    if-nez v3, :cond_0

    move v0, v1

    :goto_1
    goto :goto_0

    :cond_0
    move v0, v2

    goto :goto_1

    :pswitch_1
    iget v3, p0, Lcom/konka/kkimplements/tv/vo/KonkaProgCount;->DTVRadioProgCount:I

    if-nez v3, :cond_1

    move v0, v1

    :goto_2
    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_2

    :pswitch_2
    iget v3, p0, Lcom/konka/kkimplements/tv/vo/KonkaProgCount;->DTVDataProgCount:I

    if-nez v3, :cond_2

    move v0, v1

    :goto_3
    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_3

    :cond_3
    move v0, v2

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
