.class public final enum Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_COLOR_LEVEL_STRETCH_Index_Main;
.super Ljava/lang/Enum;
.source "GruleIndex.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/kkimplements/tv/mstar/GruleIndex;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "MST_GRule_COLOR_LEVEL_STRETCH_Index_Main"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_COLOR_LEVEL_STRETCH_Index_Main;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_COLOR_LEVEL_STRETCH_Index_Main;

.field public static final enum PQ_GRule_COLOR_LEVEL_STRETCH_Off_Main:Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_COLOR_LEVEL_STRETCH_Index_Main;

.field public static final enum PQ_GRule_COLOR_LEVEL_STRETCH_On_Main:Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_COLOR_LEVEL_STRETCH_Index_Main;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_COLOR_LEVEL_STRETCH_Index_Main;

    const-string v1, "PQ_GRule_COLOR_LEVEL_STRETCH_Off_Main"

    invoke-direct {v0, v1, v2}, Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_COLOR_LEVEL_STRETCH_Index_Main;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_COLOR_LEVEL_STRETCH_Index_Main;->PQ_GRule_COLOR_LEVEL_STRETCH_Off_Main:Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_COLOR_LEVEL_STRETCH_Index_Main;

    new-instance v0, Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_COLOR_LEVEL_STRETCH_Index_Main;

    const-string v1, "PQ_GRule_COLOR_LEVEL_STRETCH_On_Main"

    invoke-direct {v0, v1, v3}, Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_COLOR_LEVEL_STRETCH_Index_Main;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_COLOR_LEVEL_STRETCH_Index_Main;->PQ_GRule_COLOR_LEVEL_STRETCH_On_Main:Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_COLOR_LEVEL_STRETCH_Index_Main;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_COLOR_LEVEL_STRETCH_Index_Main;

    sget-object v1, Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_COLOR_LEVEL_STRETCH_Index_Main;->PQ_GRule_COLOR_LEVEL_STRETCH_Off_Main:Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_COLOR_LEVEL_STRETCH_Index_Main;

    aput-object v1, v0, v2

    sget-object v1, Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_COLOR_LEVEL_STRETCH_Index_Main;->PQ_GRule_COLOR_LEVEL_STRETCH_On_Main:Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_COLOR_LEVEL_STRETCH_Index_Main;

    aput-object v1, v0, v3

    sput-object v0, Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_COLOR_LEVEL_STRETCH_Index_Main;->ENUM$VALUES:[Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_COLOR_LEVEL_STRETCH_Index_Main;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_COLOR_LEVEL_STRETCH_Index_Main;
    .locals 1

    const-class v0, Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_COLOR_LEVEL_STRETCH_Index_Main;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_COLOR_LEVEL_STRETCH_Index_Main;

    return-object v0
.end method

.method public static values()[Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_COLOR_LEVEL_STRETCH_Index_Main;
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_COLOR_LEVEL_STRETCH_Index_Main;->ENUM$VALUES:[Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_COLOR_LEVEL_STRETCH_Index_Main;

    array-length v1, v0

    new-array v2, v1, [Lcom/konka/kkimplements/tv/mstar/GruleIndex$MST_GRule_COLOR_LEVEL_STRETCH_Index_Main;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
