.class public Lcom/konka/kkimplements/tv/mstar/PvrDeskImpl;
.super Lcom/konka/kkimplements/tv/mstar/BaseDeskImpl;
.source "PvrDeskImpl.java"

# interfaces
.implements Lcom/konka/kkinterface/tv/PvrDesk;


# static fields
.field private static pvrMgrImpl:Lcom/konka/kkimplements/tv/mstar/PvrDeskImpl;


# instance fields
.field private com:Lcom/konka/kkinterface/tv/CommonDesk;

.field private context:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/konka/kkimplements/tv/mstar/PvrDeskImpl;->pvrMgrImpl:Lcom/konka/kkimplements/tv/mstar/PvrDeskImpl;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Lcom/konka/kkimplements/tv/mstar/BaseDeskImpl;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/PvrDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    iput-object p1, p0, Lcom/konka/kkimplements/tv/mstar/PvrDeskImpl;->context:Landroid/content/Context;

    invoke-static {p1}, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/PvrDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/PvrDeskImpl;->com:Lcom/konka/kkinterface/tv/CommonDesk;

    const-string v1, "TvService"

    const-string v2, "PvrManagerImpl constructor!!"

    invoke-interface {v0, v1, v2}, Lcom/konka/kkinterface/tv/CommonDesk;->printfI(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static getPvrMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/PvrDeskImpl;
    .locals 1
    .param p0    # Landroid/content/Context;

    sget-object v0, Lcom/konka/kkimplements/tv/mstar/PvrDeskImpl;->pvrMgrImpl:Lcom/konka/kkimplements/tv/mstar/PvrDeskImpl;

    if-nez v0, :cond_0

    new-instance v0, Lcom/konka/kkimplements/tv/mstar/PvrDeskImpl;

    invoke-direct {v0, p0}, Lcom/konka/kkimplements/tv/mstar/PvrDeskImpl;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/konka/kkimplements/tv/mstar/PvrDeskImpl;->pvrMgrImpl:Lcom/konka/kkimplements/tv/mstar/PvrDeskImpl;

    :cond_0
    sget-object v0, Lcom/konka/kkimplements/tv/mstar/PvrDeskImpl;->pvrMgrImpl:Lcom/konka/kkimplements/tv/mstar/PvrDeskImpl;

    return-object v0
.end method


# virtual methods
.method public assignThumbnailFileInfoHandler(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/mstar/android/tvapi/common/PvrManager;->assignThumbnailFileInfoHandler(Ljava/lang/String;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public captureThumbnail()Lcom/mstar/android/tvapi/common/vo/CaptureThumbnailResult;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/PvrManager;->captureThumbnail()Lcom/mstar/android/tvapi/common/vo/CaptureThumbnailResult;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public changeDevice(S)Z
    .locals 1
    .param p1    # S
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/mstar/android/tvapi/common/PvrManager;->changeDevice(S)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public checkUsbSpeed()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/PvrManager;->checkUsbSpeed()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public clearMetaData()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/PvrManager;->clearMetadata()V

    :cond_0
    return-void
.end method

.method public createMetaData(Ljava/lang/String;)Z
    .locals 2
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    const/4 v0, 0x0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/mstar/android/tvapi/common/PvrManager;->createMetadata(Ljava/lang/String;)Z

    move-result v0

    :cond_0
    return v0
.end method

.method public deletefile(ILjava/lang/String;)V
    .locals 1
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/mstar/android/tvapi/common/PvrManager;->deletefile(ILjava/lang/String;)V

    :cond_0
    return-void
.end method

.method public doPlaybackFastBackward()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/PvrManager;->doPlaybackFastBackward()V

    :cond_0
    return-void
.end method

.method public doPlaybackFastForward()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/PvrManager;->doPlaybackFastForward()V

    :cond_0
    return-void
.end method

.method public doPlaybackJumpBackward()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/PvrManager;->doPlaybackJumpBackward()V

    :cond_0
    return-void
.end method

.method public doPlaybackJumpForward()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/PvrManager;->doPlaybackJumpForward()V

    :cond_0
    return-void
.end method

.method public getCurPlaybackTimeInSecond()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/PvrManager;->getCurPlaybackTimeInSecond()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCurPlaybackingFileName()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/PvrManager;->getCurPlaybackingFileName()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCurRecordTimeInSecond()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/PvrManager;->getCurRecordTimeInSecond()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCurRecordingFileName()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/PvrManager;->getCurRecordingFileName()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getEstimateRecordRemainingTime()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/PvrManager;->getEstimateRecordRemainingTime()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getFileEventName(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/mstar/android/tvapi/common/PvrManager;->getFileEventName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    :cond_0
    return-object p1
.end method

.method public getFileLcn(I)I
    .locals 1
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/mstar/android/tvapi/common/PvrManager;->getFileLcn(I)I

    move-result p1

    :cond_0
    return p1
.end method

.method public getFileServiceName(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/mstar/android/tvapi/common/PvrManager;->getFileServiceName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    :cond_0
    return-object p1
.end method

.method public getMetadataSortKey()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    const/4 v0, 0x0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/PvrManager;->getMetadataSortKey()I

    move-result v0

    :cond_0
    return v0
.end method

.method public getPlaybackSpeed()Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/PvrManager;->getPlaybackSpeed()Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getPvrFileInfo(II)Lcom/mstar/android/tvapi/common/vo/PvrFileInfo;
    .locals 1
    .param p1    # I
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/mstar/android/tvapi/common/PvrManager;->getPvrFileInfo(II)Lcom/mstar/android/tvapi/common/vo/PvrFileInfo;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getPvrFileNumber()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/PvrManager;->getPvrFileNumber()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getPvrMountPath()Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    const/4 v0, 0x0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/PvrManager;->getPvrMountPath()Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public getRecordedFileDurationTime(Ljava/lang/String;)I
    .locals 1
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/mstar/android/tvapi/common/PvrManager;->getRecordedFileDurationTime(Ljava/lang/String;)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getThumbnailDisplay(I)Ljava/lang/String;
    .locals 1
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/mstar/android/tvapi/common/PvrManager;->getThumbnailDisplay(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getThumbnailNumber()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/PvrManager;->getThumbnailNumber()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getThumbnailPath(I)Ljava/lang/String;
    .locals 1
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/mstar/android/tvapi/common/PvrManager;->getThumbnailPath(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getThumbnailTimeStamp(I)[I
    .locals 1
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/mstar/android/tvapi/common/PvrManager;->getThumbnailTimeStamp(I)[I

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getUsbDeviceIndex()S
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/PvrManager;->getUsbDeviceIndex()S

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getUsbDeviceLabel(I)Lcom/mstar/android/tvapi/common/vo/PvrUsbDeviceLabel$EnumPvrUsbDeviceLabel;
    .locals 1
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/mstar/android/tvapi/common/PvrManager;->getUsbDeviceLabel(I)Lcom/mstar/android/tvapi/common/vo/PvrUsbDeviceLabel$EnumPvrUsbDeviceLabel;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getUsbDeviceNumber()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/PvrManager;->getUsbDeviceNumber()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getUsbPartitionNumber()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/PvrManager;->getUsbPartitionNumber()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isAlwaysTimeShiftPlaybackPaused()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/PvrManager;->isAlwaysTimeShiftPlaybackPaused()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isAlwaysTimeShiftRecording()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/PvrManager;->isAlwaysTimeShiftRecording()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isMetadataSortAscending()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    const/4 v0, 0x0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/PvrManager;->isMetadataSortAscending()Z

    move-result v0

    :cond_0
    return v0
.end method

.method public isPlaybackParentalLock()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/PvrManager;->isPlaybackParentalLock()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPlaybackPaused()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/PvrManager;->isPlaybackPaused()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPlaybacking()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/PvrManager;->isPlaybacking()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isRecordPaused()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/PvrManager;->isRecordPaused()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isRecording()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/PvrManager;->isRecording()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isTimeShiftRecording()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/PvrManager;->isTimeShiftRecording()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public jumpPlaybackTime(I)Z
    .locals 1
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/mstar/android/tvapi/common/PvrManager;->jumpPlaybackTime(I)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public jumpToThumbnail(I)Z
    .locals 1
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/mstar/android/tvapi/common/PvrManager;->jumpToThumbnail(I)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public pauseAlwaysTimeShiftPlayback(Z)Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;
    .locals 1
    .param p1    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/mstar/android/tvapi/common/PvrManager;->pauseAlwaysTimeShiftPlayback(Z)Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public pauseAlwaysTimeShiftRecord()S
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/PvrManager;->pauseAlwaysTimeShiftRecord()S

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public pausePlayback()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/PvrManager;->pausePlayback()V

    :cond_0
    return-void
.end method

.method public pauseRecord()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/PvrManager;->pauseRecord()V

    :cond_0
    return-void
.end method

.method public resumePlayback()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/PvrManager;->resumePlayback()V

    :cond_0
    return-void
.end method

.method public resumeRecord()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/PvrManager;->resumeRecord()V

    :cond_0
    return-void
.end method

.method public setMetadataSortAscending(Z)V
    .locals 1
    .param p1    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/mstar/android/tvapi/common/PvrManager;->setMetadataSortAscending(Z)V

    :cond_0
    return-void
.end method

.method public setMetadataSortKey(I)V
    .locals 2
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    if-ltz p1, :cond_0

    const/4 v0, 0x5

    if-le p1, v0, :cond_1

    :cond_0
    new-instance v0, Lcom/mstar/android/tvapi/common/exception/TvCommonException;

    const-string v1, "nSortKey out of range exception!"

    invoke-direct {v0, v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/mstar/android/tvapi/common/PvrManager;->setMetadataSortKey(I)V

    :cond_2
    return-void
.end method

.method public setOnPvrEventListener(Lcom/mstar/android/tvapi/common/PvrManager$OnPvrEventListener;)V
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/PvrManager$OnPvrEventListener;

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/mstar/android/tvapi/common/PvrManager;->setOnPvrEventListener(Lcom/mstar/android/tvapi/common/PvrManager$OnPvrEventListener;)V

    :cond_0
    return-void
.end method

.method public setPVRParas(Ljava/lang/String;S)Z
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # S
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/mstar/android/tvapi/common/PvrManager;->setPvrParams(Ljava/lang/String;S)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setPlaybackSpeed(Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;)V
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/mstar/android/tvapi/common/PvrManager;->setPlaybackSpeed(Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;)V

    :cond_0
    return-void
.end method

.method public setPlaybackWindow(Lcom/mstar/android/tvapi/common/vo/VideoWindowType;II)V
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/vo/VideoWindowType;
    .param p2    # I
    .param p3    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/mstar/android/tvapi/common/PvrManager;->setPlaybackWindow(Lcom/mstar/android/tvapi/common/vo/VideoWindowType;II)V

    :cond_0
    return-void
.end method

.method public setRecordAll(Z)V
    .locals 1
    .param p1    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/mstar/android/tvapi/common/PvrManager;->setRecordAll(Z)V

    :cond_0
    return-void
.end method

.method public setTimeShiftFileSize(J)V
    .locals 1
    .param p1    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/mstar/android/tvapi/common/PvrManager;->setTimeShiftFileSize(J)V

    :cond_0
    return-void
.end method

.method public startAlwaysTimeShiftPlayback()S
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/PvrManager;->startAlwaysTimeShiftPlayback()S

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public startAlwaysTimeShiftRecord()S
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/PvrManager;->startAlwaysTimeShiftRecord()S

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public startPlayback(Ljava/lang/String;)Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;
    .locals 1
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/mstar/android/tvapi/common/PvrManager;->startPlayback(Ljava/lang/String;)Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public startPlayback(Ljava/lang/String;I)Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/mstar/android/tvapi/common/PvrManager;->startPlayback(Ljava/lang/String;I)Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public startPlayback(Ljava/lang/String;II)Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/mstar/android/tvapi/common/PvrManager;->startPlayback(Ljava/lang/String;I)Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public startPlaybackLoop(II)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/mstar/android/tvapi/common/PvrManager;->startPlaybackLoop(II)V

    :cond_0
    return-void
.end method

.method public startPvrFormat()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    return-void
.end method

.method public startRecord()Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/PvrManager;->startRecord()Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public startTimeShiftPlayback()Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/PvrManager;->startTimeShiftPlayback()Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public startTimeShiftRecord()Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/PvrManager;->startTimeShiftRecord()Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public stepInPlayback()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/PvrManager;->stepInPlayback()V

    :cond_0
    return-void
.end method

.method public stopAlwaysTimeShiftPlayback()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/PvrManager;->stopAlwaysTimeShiftPlayback()V

    :cond_0
    return-void
.end method

.method public stopAlwaysTimeShiftRecord()S
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/PvrManager;->stopAlwaysTimeShiftRecord()S

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public stopPlayback()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/PvrManager;->stopPlayback()V

    :cond_0
    return-void
.end method

.method public stopPlaybackLoop()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/PvrManager;->stopPlaybackLoop()V

    :cond_0
    return-void
.end method

.method public stopPvr()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/PvrManager;->stopPvr()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public stopRecord()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/PvrManager;->stopRecord()V

    :cond_0
    return-void
.end method

.method public stopTimeShift()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/PvrManager;->stopTimeShift()V

    :cond_0
    return-void
.end method

.method public stopTimeShiftPlayback()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/PvrManager;->stopTimeShiftPlayback()V

    :cond_0
    return-void
.end method

.method public stopTimeShiftRecord()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/PvrManager;->stopTimeShiftRecord()V

    :cond_0
    return-void
.end method
