.class public Lcom/konka/kkinterface/tv/DtvInterface$EPG_EVENT_INFO;
.super Ljava/lang/Object;
.source "DtvInterface.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/kkinterface/tv/DtvInterface;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "EPG_EVENT_INFO"
.end annotation


# static fields
.field public static final EN_EPG_FUNC_STATUS_CRID_NOT_FOUND:I = 0x5

.field public static final EN_EPG_FUNC_STATUS_DB_NO_CHANNEL_DB:I = 0xc

.field public static final EN_EPG_FUNC_STATUS_DB_NO_CONNECT:I = 0xa

.field public static final EN_EPG_FUNC_STATUS_DB_NO_LOCK:I = 0xb

.field public static final EN_EPG_FUNC_STATUS_INVALID:I = 0x1

.field public static final EN_EPG_FUNC_STATUS_NO_CHANNEL:I = 0x4

.field public static final EN_EPG_FUNC_STATUS_NO_EVENT:I = 0x2

.field public static final EN_EPG_FUNC_STATUS_NO_FUNCTION:I = 0xff

.field public static final EN_EPG_FUNC_STATUS_NO_STRING:I = 0x3

.field public static final EN_EPG_FUNC_STATUS_SUCCESS:I


# instance fields
.field public description:Ljava/lang/String;

.field public durationTime:I

.field public endTime:I

.field public eventId:I

.field public functionStatus:I

.field public genre:S

.field public isScrambled:Z

.field public name:Ljava/lang/String;

.field public originalStartTime:I

.field public parentalRating:S

.field public startTime:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
