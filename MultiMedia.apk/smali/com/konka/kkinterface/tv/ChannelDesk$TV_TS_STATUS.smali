.class public final enum Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;
.super Ljava/lang/Enum;
.source "ChannelDesk.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/kkinterface/tv/ChannelDesk;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "TV_TS_STATUS"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

.field public static final enum E_TS_ATV_AUTO_TUNING:Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

.field public static final enum E_TS_ATV_MANU_TUNING_LEFT:Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

.field public static final enum E_TS_ATV_MANU_TUNING_RIGHT:Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

.field public static final enum E_TS_ATV_SCAN_PAUSING:Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

.field public static final enum E_TS_DTV_AUTO_TUNING:Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

.field public static final enum E_TS_DTV_FULL_TUNING:Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

.field public static final enum E_TS_DTV_MANU_TUNING:Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

.field public static final enum E_TS_DTV_SCAN_PAUSING:Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

.field public static final enum E_TS_NONE:Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

    const-string v1, "E_TS_NONE"

    invoke-direct {v0, v1, v3}, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;->E_TS_NONE:Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

    new-instance v0, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

    const-string v1, "E_TS_ATV_MANU_TUNING_LEFT"

    invoke-direct {v0, v1, v4}, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;->E_TS_ATV_MANU_TUNING_LEFT:Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

    new-instance v0, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

    const-string v1, "E_TS_ATV_MANU_TUNING_RIGHT"

    invoke-direct {v0, v1, v5}, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;->E_TS_ATV_MANU_TUNING_RIGHT:Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

    new-instance v0, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

    const-string v1, "E_TS_ATV_AUTO_TUNING"

    invoke-direct {v0, v1, v6}, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;->E_TS_ATV_AUTO_TUNING:Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

    new-instance v0, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

    const-string v1, "E_TS_ATV_SCAN_PAUSING"

    invoke-direct {v0, v1, v7}, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;->E_TS_ATV_SCAN_PAUSING:Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

    new-instance v0, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

    const-string v1, "E_TS_DTV_MANU_TUNING"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;->E_TS_DTV_MANU_TUNING:Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

    new-instance v0, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

    const-string v1, "E_TS_DTV_AUTO_TUNING"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;->E_TS_DTV_AUTO_TUNING:Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

    new-instance v0, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

    const-string v1, "E_TS_DTV_FULL_TUNING"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;->E_TS_DTV_FULL_TUNING:Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

    new-instance v0, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

    const-string v1, "E_TS_DTV_SCAN_PAUSING"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;->E_TS_DTV_SCAN_PAUSING:Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

    const/16 v0, 0x9

    new-array v0, v0, [Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

    sget-object v1, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;->E_TS_NONE:Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

    aput-object v1, v0, v3

    sget-object v1, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;->E_TS_ATV_MANU_TUNING_LEFT:Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

    aput-object v1, v0, v4

    sget-object v1, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;->E_TS_ATV_MANU_TUNING_RIGHT:Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

    aput-object v1, v0, v5

    sget-object v1, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;->E_TS_ATV_AUTO_TUNING:Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

    aput-object v1, v0, v6

    sget-object v1, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;->E_TS_ATV_SCAN_PAUSING:Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;->E_TS_DTV_MANU_TUNING:Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;->E_TS_DTV_AUTO_TUNING:Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;->E_TS_DTV_FULL_TUNING:Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;->E_TS_DTV_SCAN_PAUSING:Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

    aput-object v2, v0, v1

    sput-object v0, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;->ENUM$VALUES:[Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;
    .locals 1

    const-class v0, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

    return-object v0
.end method

.method public static values()[Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;->ENUM$VALUES:[Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

    array-length v1, v0

    new-array v2, v1, [Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
