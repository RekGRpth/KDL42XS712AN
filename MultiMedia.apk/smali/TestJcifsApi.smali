.class public LTestJcifsApi;
.super Ljunit/framework/TestCase;
.source "TestJcifsApi.java"


# instance fields
.field private context:Lorg/jmock/Mockery;

.field private testClient:Lcom/mstar/SmbClient;

.field private testfile:Ljcifs/smb/SmbFile;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljunit/framework/TestCase;-><init>()V

    new-instance v0, LTestJcifsApi$1;

    invoke-direct {v0, p0}, LTestJcifsApi$1;-><init>(LTestJcifsApi;)V

    iput-object v0, p0, LTestJcifsApi;->context:Lorg/jmock/Mockery;

    const/4 v0, 0x0

    iput-object v0, p0, LTestJcifsApi;->testfile:Ljcifs/smb/SmbFile;

    new-instance v0, Lcom/mstar/SmbClient;

    invoke-direct {v0}, Lcom/mstar/SmbClient;-><init>()V

    iput-object v0, p0, LTestJcifsApi;->testClient:Lcom/mstar/SmbClient;

    return-void
.end method

.method static synthetic access$0(LTestJcifsApi;)Ljcifs/smb/SmbFile;
    .locals 1

    iget-object v0, p0, LTestJcifsApi;->testfile:Ljcifs/smb/SmbFile;

    return-object v0
.end method


# virtual methods
.method public setUp()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .annotation runtime Lorg/junit/Before;
    .end annotation

    invoke-super {p0}, Ljunit/framework/TestCase;->setUp()V

    iget-object v0, p0, LTestJcifsApi;->context:Lorg/jmock/Mockery;

    const-class v1, Ljcifs/smb/SmbFile;

    invoke-virtual {v0, v1}, Lorg/jmock/Mockery;->mock(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljcifs/smb/SmbFile;

    iput-object v0, p0, LTestJcifsApi;->testfile:Ljcifs/smb/SmbFile;

    iget-object v0, p0, LTestJcifsApi;->context:Lorg/jmock/Mockery;

    new-instance v1, LTestJcifsApi$2;

    invoke-direct {v1, p0}, LTestJcifsApi$2;-><init>(LTestJcifsApi;)V

    invoke-virtual {v0, v1}, Lorg/jmock/Mockery;->checking(Lorg/jmock/internal/ExpectationBuilder;)V

    return-void
.end method

.method public tearDown()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .annotation runtime Lorg/junit/After;
    .end annotation

    const/4 v0, 0x0

    iput-object v0, p0, LTestJcifsApi;->testClient:Lcom/mstar/SmbClient;

    iput-object v0, p0, LTestJcifsApi;->context:Lorg/jmock/Mockery;

    iput-object v0, p0, LTestJcifsApi;->testfile:Ljcifs/smb/SmbFile;

    return-void
.end method

.method public testisHosts()V
    .locals 4
    .annotation runtime Lorg/junit/Test;
    .end annotation

    :try_start_0
    iget-object v2, p0, LTestJcifsApi;->testClient:Lcom/mstar/SmbClient;

    iget-object v3, p0, LTestJcifsApi;->testfile:Ljcifs/smb/SmbFile;

    invoke-virtual {v2, v3}, Lcom/mstar/SmbClient;->isHost(Ljcifs/smb/SmbFile;)Z

    move-result v0

    const/4 v2, 0x1

    invoke-static {v2, v0}, LTestJcifsApi;->assertEquals(ZZ)V

    iget-object v2, p0, LTestJcifsApi;->testClient:Lcom/mstar/SmbClient;

    iget-object v3, p0, LTestJcifsApi;->testfile:Ljcifs/smb/SmbFile;

    invoke-virtual {v2, v3}, Lcom/mstar/SmbClient;->isHost(Ljcifs/smb/SmbFile;)Z

    move-result v0

    const/4 v2, 0x0

    invoke-static {v2, v0}, LTestJcifsApi;->assertEquals(ZZ)V

    iget-object v2, p0, LTestJcifsApi;->testClient:Lcom/mstar/SmbClient;

    iget-object v3, p0, LTestJcifsApi;->testfile:Ljcifs/smb/SmbFile;

    invoke-virtual {v2, v3}, Lcom/mstar/SmbClient;->isHost(Ljcifs/smb/SmbFile;)Z

    move-result v0

    const/4 v2, 0x1

    invoke-static {v2, v0}, LTestJcifsApi;->assertEquals(ZZ)V

    iget-object v2, p0, LTestJcifsApi;->testClient:Lcom/mstar/SmbClient;

    iget-object v3, p0, LTestJcifsApi;->testfile:Ljcifs/smb/SmbFile;

    invoke-virtual {v2, v3}, Lcom/mstar/SmbClient;->isHost(Ljcifs/smb/SmbFile;)Z

    move-result v0

    const/4 v2, 0x1

    invoke-static {v2, v0}, LTestJcifsApi;->assertEquals(ZZ)V

    iget-object v2, p0, LTestJcifsApi;->context:Lorg/jmock/Mockery;

    invoke-virtual {v2}, Lorg/jmock/Mockery;->assertIsSatisfied()V
    :try_end_0
    .catch Ljcifs/smb/SmbException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljcifs/smb/SmbException;->printStackTrace()V

    goto :goto_0
.end method
