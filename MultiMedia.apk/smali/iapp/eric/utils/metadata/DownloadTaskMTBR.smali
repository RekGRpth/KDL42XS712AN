.class public Liapp/eric/utils/metadata/DownloadTaskMTBR;
.super Ljava/lang/Object;
.source "DownloadTaskMTBR.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Liapp/eric/utils/metadata/DownloadTaskMTBR$TaskStatusType;
    }
.end annotation


# instance fields
.field public completedSize:J

.field public id:Ljava/lang/String;

.field public isFinish:Z

.field public saveFile:Ljava/io/File;

.field public status:Liapp/eric/utils/metadata/DownloadTaskMTBR$TaskStatusType;

.field public threadNum:I

.field public totalSize:J

.field public url:Ljava/net/URL;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/net/URL;Ljava/io/File;IJJZLiapp/eric/utils/metadata/DownloadTaskMTBR$TaskStatusType;)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/net/URL;
    .param p3    # Ljava/io/File;
    .param p4    # I
    .param p5    # J
    .param p7    # J
    .param p9    # Z
    .param p10    # Liapp/eric/utils/metadata/DownloadTaskMTBR$TaskStatusType;

    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Liapp/eric/utils/metadata/DownloadTaskMTBR;->id:Ljava/lang/String;

    iput-object v0, p0, Liapp/eric/utils/metadata/DownloadTaskMTBR;->url:Ljava/net/URL;

    iput-object v0, p0, Liapp/eric/utils/metadata/DownloadTaskMTBR;->saveFile:Ljava/io/File;

    iput v1, p0, Liapp/eric/utils/metadata/DownloadTaskMTBR;->threadNum:I

    iput-wide v2, p0, Liapp/eric/utils/metadata/DownloadTaskMTBR;->totalSize:J

    iput-wide v2, p0, Liapp/eric/utils/metadata/DownloadTaskMTBR;->completedSize:J

    iput-boolean v1, p0, Liapp/eric/utils/metadata/DownloadTaskMTBR;->isFinish:Z

    sget-object v0, Liapp/eric/utils/metadata/DownloadTaskMTBR$TaskStatusType;->IDLE:Liapp/eric/utils/metadata/DownloadTaskMTBR$TaskStatusType;

    iput-object v0, p0, Liapp/eric/utils/metadata/DownloadTaskMTBR;->status:Liapp/eric/utils/metadata/DownloadTaskMTBR$TaskStatusType;

    iput-object p1, p0, Liapp/eric/utils/metadata/DownloadTaskMTBR;->id:Ljava/lang/String;

    iput-object p2, p0, Liapp/eric/utils/metadata/DownloadTaskMTBR;->url:Ljava/net/URL;

    iput-object p3, p0, Liapp/eric/utils/metadata/DownloadTaskMTBR;->saveFile:Ljava/io/File;

    iput p4, p0, Liapp/eric/utils/metadata/DownloadTaskMTBR;->threadNum:I

    iput-wide p5, p0, Liapp/eric/utils/metadata/DownloadTaskMTBR;->totalSize:J

    iput-wide p7, p0, Liapp/eric/utils/metadata/DownloadTaskMTBR;->completedSize:J

    iput-boolean p9, p0, Liapp/eric/utils/metadata/DownloadTaskMTBR;->isFinish:Z

    iput-object p10, p0, Liapp/eric/utils/metadata/DownloadTaskMTBR;->status:Liapp/eric/utils/metadata/DownloadTaskMTBR$TaskStatusType;

    return-void
.end method
