.class public Liapp/eric/utils/metadata/ID3V1Tag;
.super Ljava/lang/Object;
.source "ID3V1Tag.java"


# instance fields
.field private Album:[B

.field private Artist:[B

.field private Comment:[B

.field private Genre:[B

.field private Header:[B

.field private Reserve:[B

.field private Title:[B

.field private Track:[B

.field private Year:[B


# direct methods
.method public constructor <init>()V
    .locals 3

    const/16 v2, 0x1f

    const/4 v1, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x4

    new-array v0, v0, [B

    iput-object v0, p0, Liapp/eric/utils/metadata/ID3V1Tag;->Header:[B

    new-array v0, v2, [B

    iput-object v0, p0, Liapp/eric/utils/metadata/ID3V1Tag;->Title:[B

    new-array v0, v2, [B

    iput-object v0, p0, Liapp/eric/utils/metadata/ID3V1Tag;->Artist:[B

    new-array v0, v2, [B

    iput-object v0, p0, Liapp/eric/utils/metadata/ID3V1Tag;->Album:[B

    const/4 v0, 0x5

    new-array v0, v0, [B

    iput-object v0, p0, Liapp/eric/utils/metadata/ID3V1Tag;->Year:[B

    const/16 v0, 0x1d

    new-array v0, v0, [B

    iput-object v0, p0, Liapp/eric/utils/metadata/ID3V1Tag;->Comment:[B

    new-array v0, v1, [B

    iput-object v0, p0, Liapp/eric/utils/metadata/ID3V1Tag;->Reserve:[B

    new-array v0, v1, [B

    iput-object v0, p0, Liapp/eric/utils/metadata/ID3V1Tag;->Track:[B

    new-array v0, v1, [B

    iput-object v0, p0, Liapp/eric/utils/metadata/ID3V1Tag;->Genre:[B

    return-void
.end method


# virtual methods
.method public getAlbum()[B
    .locals 1

    iget-object v0, p0, Liapp/eric/utils/metadata/ID3V1Tag;->Album:[B

    return-object v0
.end method

.method public getArtist()[B
    .locals 1

    iget-object v0, p0, Liapp/eric/utils/metadata/ID3V1Tag;->Artist:[B

    return-object v0
.end method

.method public getComment()[B
    .locals 1

    iget-object v0, p0, Liapp/eric/utils/metadata/ID3V1Tag;->Comment:[B

    return-object v0
.end method

.method public getGenre()[B
    .locals 1

    iget-object v0, p0, Liapp/eric/utils/metadata/ID3V1Tag;->Genre:[B

    return-object v0
.end method

.method public getHeader()[B
    .locals 1

    iget-object v0, p0, Liapp/eric/utils/metadata/ID3V1Tag;->Header:[B

    return-object v0
.end method

.method public getReserve()[B
    .locals 1

    iget-object v0, p0, Liapp/eric/utils/metadata/ID3V1Tag;->Reserve:[B

    return-object v0
.end method

.method public getTitle()[B
    .locals 1

    iget-object v0, p0, Liapp/eric/utils/metadata/ID3V1Tag;->Title:[B

    return-object v0
.end method

.method public getTrack()[B
    .locals 1

    iget-object v0, p0, Liapp/eric/utils/metadata/ID3V1Tag;->Track:[B

    return-object v0
.end method

.method public getYear()[B
    .locals 1

    iget-object v0, p0, Liapp/eric/utils/metadata/ID3V1Tag;->Year:[B

    return-object v0
.end method

.method public setAlbum([B)V
    .locals 0
    .param p1    # [B

    iput-object p1, p0, Liapp/eric/utils/metadata/ID3V1Tag;->Album:[B

    return-void
.end method

.method public setArtist([B)V
    .locals 0
    .param p1    # [B

    iput-object p1, p0, Liapp/eric/utils/metadata/ID3V1Tag;->Artist:[B

    return-void
.end method

.method public setComment([B)V
    .locals 0
    .param p1    # [B

    iput-object p1, p0, Liapp/eric/utils/metadata/ID3V1Tag;->Comment:[B

    return-void
.end method

.method public setGenre([B)V
    .locals 0
    .param p1    # [B

    iput-object p1, p0, Liapp/eric/utils/metadata/ID3V1Tag;->Genre:[B

    return-void
.end method

.method public setHeader([B)V
    .locals 0
    .param p1    # [B

    iput-object p1, p0, Liapp/eric/utils/metadata/ID3V1Tag;->Header:[B

    return-void
.end method

.method public setReserve([B)V
    .locals 0
    .param p1    # [B

    iput-object p1, p0, Liapp/eric/utils/metadata/ID3V1Tag;->Reserve:[B

    return-void
.end method

.method public setTitle([B)V
    .locals 0
    .param p1    # [B

    iput-object p1, p0, Liapp/eric/utils/metadata/ID3V1Tag;->Title:[B

    return-void
.end method

.method public setTrack([B)V
    .locals 0
    .param p1    # [B

    iput-object p1, p0, Liapp/eric/utils/metadata/ID3V1Tag;->Track:[B

    return-void
.end method

.method public setYear([B)V
    .locals 0
    .param p1    # [B

    iput-object p1, p0, Liapp/eric/utils/metadata/ID3V1Tag;->Year:[B

    return-void
.end method
