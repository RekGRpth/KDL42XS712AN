.class public Liapp/eric/utils/base/DownloadManager;
.super Ljava/lang/Object;
.source "DownloadManager.java"

# interfaces
.implements Liapp/eric/utils/interfaces/Downloader;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Liapp/eric/utils/base/DownloadManager$DownloadThreadBR;
    }
.end annotation


# instance fields
.field public m_Context:Landroid/content/Context;

.field private m_Table:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/net/URL;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private m_taskList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Liapp/eric/utils/metadata/DownloadTaskMTBR;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Liapp/eric/utils/base/DownloadManager;->m_Context:Landroid/content/Context;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Liapp/eric/utils/base/DownloadManager;->m_Table:Ljava/util/Map;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Liapp/eric/utils/base/DownloadManager;->m_taskList:Ljava/util/List;

    iput-object p1, p0, Liapp/eric/utils/base/DownloadManager;->m_Context:Landroid/content/Context;

    return-void
.end method

.method private downloadBR(Liapp/eric/utils/metadata/DownloadTaskMTBR;Liapp/eric/utils/interfaces/DownloadListener;)I
    .locals 17
    .param p1    # Liapp/eric/utils/metadata/DownloadTaskMTBR;
    .param p2    # Liapp/eric/utils/interfaces/DownloadListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    :try_start_0
    new-instance v10, Ljava/io/RandomAccessFile;

    move-object/from16 v0, p1

    iget-object v13, v0, Liapp/eric/utils/metadata/DownloadTaskMTBR;->saveFile:Ljava/io/File;

    const-string v14, "rw"

    invoke-direct {v10, v13, v14}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    move-object/from16 v0, p1

    iget-wide v13, v0, Liapp/eric/utils/metadata/DownloadTaskMTBR;->totalSize:J

    const-wide/16 v15, 0x0

    cmp-long v13, v13, v15

    if-lez v13, :cond_0

    move-object/from16 v0, p1

    iget-wide v13, v0, Liapp/eric/utils/metadata/DownloadTaskMTBR;->totalSize:J

    invoke-virtual {v10, v13, v14}, Ljava/io/RandomAccessFile;->setLength(J)V

    :cond_0
    invoke-virtual {v10}, Ljava/io/RandomAccessFile;->close()V

    new-instance v1, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v1}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v13

    move-object/from16 v0, p1

    iget v14, v0, Liapp/eric/utils/metadata/DownloadTaskMTBR;->threadNum:I

    if-eq v13, v14, :cond_1

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    const/4 v8, 0x0

    :goto_0
    move-object/from16 v0, p1

    iget v13, v0, Liapp/eric/utils/metadata/DownloadTaskMTBR;->threadNum:I

    if-lt v8, v13, :cond_3

    :cond_1
    move-object/from16 v0, p1

    iget-wide v13, v0, Liapp/eric/utils/metadata/DownloadTaskMTBR;->totalSize:J

    move-object/from16 v0, p1

    iget v15, v0, Liapp/eric/utils/metadata/DownloadTaskMTBR;->threadNum:I

    int-to-long v15, v15

    rem-long/2addr v13, v15

    const-wide/16 v15, 0x0

    cmp-long v13, v13, v15

    if-nez v13, :cond_4

    move-object/from16 v0, p1

    iget-wide v13, v0, Liapp/eric/utils/metadata/DownloadTaskMTBR;->totalSize:J

    move-object/from16 v0, p1

    iget v15, v0, Liapp/eric/utils/metadata/DownloadTaskMTBR;->threadNum:I

    int-to-long v15, v15

    div-long v2, v13, v15

    :goto_1
    const-wide/16 v5, 0x0

    move-object/from16 v0, p1

    iget v13, v0, Liapp/eric/utils/metadata/DownloadTaskMTBR;->threadNum:I

    new-array v12, v13, [Liapp/eric/utils/base/DownloadManager$DownloadThreadBR;

    const/4 v8, 0x0

    :goto_2
    move-object/from16 v0, p1

    iget v13, v0, Liapp/eric/utils/metadata/DownloadTaskMTBR;->threadNum:I

    if-lt v8, v13, :cond_5

    const/4 v9, 0x1

    :cond_2
    :goto_3
    if-nez v9, :cond_7

    const v13, 0x1869f

    return v13

    :cond_3
    add-int/lit8 v13, v8, 0x1

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    const/4 v14, 0x0

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-interface {v1, v13, v14}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    :cond_4
    move-object/from16 v0, p1

    iget-wide v13, v0, Liapp/eric/utils/metadata/DownloadTaskMTBR;->totalSize:J

    move-object/from16 v0, p1

    iget v15, v0, Liapp/eric/utils/metadata/DownloadTaskMTBR;->threadNum:I

    int-to-long v15, v15

    div-long/2addr v13, v15

    const-wide/16 v15, 0x1

    add-long v2, v13, v15

    goto :goto_1

    :cond_5
    add-int/lit8 v13, v8, 0x1

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-interface {v1, v13}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/Integer;

    invoke-virtual {v13}, Ljava/lang/Integer;->intValue()I

    move-result v4

    new-instance v11, Liapp/eric/utils/metadata/DownloadTaskBR;

    invoke-direct {v11}, Liapp/eric/utils/metadata/DownloadTaskBR;-><init>()V

    int-to-long v13, v4

    cmp-long v13, v13, v2

    if-gez v13, :cond_6

    move-object/from16 v0, p1

    iget-wide v13, v0, Liapp/eric/utils/metadata/DownloadTaskMTBR;->totalSize:J

    cmp-long v13, v5, v13

    if-gez v13, :cond_6

    new-instance v13, Liapp/eric/utils/base/DownloadManager$DownloadThreadBR;

    move-object/from16 v0, p0

    invoke-direct {v13, v0, v11}, Liapp/eric/utils/base/DownloadManager$DownloadThreadBR;-><init>(Liapp/eric/utils/base/DownloadManager;Liapp/eric/utils/metadata/DownloadTaskBR;)V

    aput-object v13, v12, v8

    aget-object v13, v12, v8

    const/4 v14, 0x7

    invoke-virtual {v13, v14}, Liapp/eric/utils/base/DownloadManager$DownloadThreadBR;->setPriority(I)V

    aget-object v13, v12, v8

    invoke-virtual {v13}, Liapp/eric/utils/base/DownloadManager$DownloadThreadBR;->start()V

    :goto_4
    add-int/lit8 v8, v8, 0x1

    goto :goto_2

    :cond_6
    const/4 v13, 0x0

    aput-object v13, v12, v8
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_4

    :catch_0
    move-exception v7

    invoke-virtual {v7}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Liapp/eric/utils/base/Constant;->trace(Ljava/lang/String;)V

    new-instance v13, Ljava/lang/Exception;

    const-string v14, "file download fail"

    invoke-direct {v13, v14}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v13

    :cond_7
    const-wide/16 v13, 0x384

    :try_start_1
    invoke-static {v13, v14}, Ljava/lang/Thread;->sleep(J)V

    const/4 v9, 0x0

    const/4 v8, 0x0

    :goto_5
    array-length v13, v12

    if-lt v8, v13, :cond_8

    if-eqz p2, :cond_2

    move-object/from16 v0, p2

    invoke-interface {v0, v5, v6}, Liapp/eric/utils/interfaces/DownloadListener;->onDownloadSize(J)V

    goto/16 :goto_3

    :cond_8
    aget-object v13, v12, v8

    if-eqz v13, :cond_9

    aget-object v13, v12, v8

    # getter for: Liapp/eric/utils/base/DownloadManager$DownloadThreadBR;->task:Liapp/eric/utils/metadata/DownloadTaskBR;
    invoke-static {v13}, Liapp/eric/utils/base/DownloadManager$DownloadThreadBR;->access$0(Liapp/eric/utils/base/DownloadManager$DownloadThreadBR;)Liapp/eric/utils/metadata/DownloadTaskBR;

    move-result-object v13

    iget-boolean v13, v13, Liapp/eric/utils/metadata/DownloadTaskBR;->isFinish:Z

    if-nez v13, :cond_9

    const/4 v9, 0x1

    aget-object v13, v12, v8

    # getter for: Liapp/eric/utils/base/DownloadManager$DownloadThreadBR;->task:Liapp/eric/utils/metadata/DownloadTaskBR;
    invoke-static {v13}, Liapp/eric/utils/base/DownloadManager$DownloadThreadBR;->access$0(Liapp/eric/utils/base/DownloadManager$DownloadThreadBR;)Liapp/eric/utils/metadata/DownloadTaskBR;

    move-result-object v13

    iget-wide v13, v13, Liapp/eric/utils/metadata/DownloadTaskBR;->completedSize:J

    const-wide/16 v15, -0x1

    cmp-long v13, v13, v15

    if-nez v13, :cond_9

    new-instance v13, Liapp/eric/utils/base/DownloadManager$DownloadThreadBR;

    aget-object v14, v12, v8

    # getter for: Liapp/eric/utils/base/DownloadManager$DownloadThreadBR;->task:Liapp/eric/utils/metadata/DownloadTaskBR;
    invoke-static {v14}, Liapp/eric/utils/base/DownloadManager$DownloadThreadBR;->access$0(Liapp/eric/utils/base/DownloadManager$DownloadThreadBR;)Liapp/eric/utils/metadata/DownloadTaskBR;

    move-result-object v14

    move-object/from16 v0, p0

    invoke-direct {v13, v0, v14}, Liapp/eric/utils/base/DownloadManager$DownloadThreadBR;-><init>(Liapp/eric/utils/base/DownloadManager;Liapp/eric/utils/metadata/DownloadTaskBR;)V

    aput-object v13, v12, v8

    aget-object v13, v12, v8

    const/4 v14, 0x7

    invoke-virtual {v13, v14}, Liapp/eric/utils/base/DownloadManager$DownloadThreadBR;->setPriority(I)V

    aget-object v13, v12, v8

    invoke-virtual {v13}, Liapp/eric/utils/base/DownloadManager$DownloadThreadBR;->start()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    :cond_9
    add-int/lit8 v8, v8, 0x1

    goto :goto_5
.end method

.method private getDownloadLength(Ljava/net/URL;)J
    .locals 6
    .param p1    # Ljava/net/URL;

    const-wide/16 v2, 0x0

    :try_start_0
    invoke-virtual {p1}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;

    const/16 v4, 0x1388

    invoke-virtual {v0, v4}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    const-string v4, "GET"

    invoke-virtual {v0, v4}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    const-string v4, "Accept"

    const-string v5, "image/gif, image/jpeg, image/pjpeg, image/pjpeg, application/x-shockwave-flash, application/xaml+xml, application/vnd.ms-xpsdocument, application/x-ms-xbap, application/x-ms-application, application/vnd.ms-excel, application/vnd.ms-powerpoint, application/msword, */*"

    invoke-virtual {v0, v4, v5}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "Accept-Language"

    const-string v5, "zh-CN"

    invoke-virtual {v0, v4, v5}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "Referer"

    invoke-virtual {p1}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "Charset"

    const-string v5, "UTF-8"

    invoke-virtual {v0, v4, v5}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "User-Agent"

    const-string v5, "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.2; Trident/4.0; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.30; .NET CLR 3.0.4506.2152; .NET CLR 3.5.30729)"

    invoke-virtual {v0, v4, v5}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "Connection"

    const-string v5, "Keep-Alive"

    invoke-virtual {v0, v4, v5}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->connect()V

    invoke-static {v0}, Liapp/eric/utils/base/DownloadManager;->showResponseHeader(Ljava/net/HttpURLConnection;)V

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v4

    const/16 v5, 0xc8

    if-ne v4, v5, :cond_0

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getContentLength()I

    move-result v4

    int-to-long v2, v4

    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-gtz v4, :cond_1

    new-instance v4, Ljava/lang/RuntimeException;

    const-string v5, "Unkown file size "

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Liapp/eric/utils/base/Constant;->trace(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/RuntimeException;

    const-string v5, "don\'t connection this url"

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_0
    :try_start_1
    new-instance v4, Ljava/lang/RuntimeException;

    const-string v5, "server no response "

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    :cond_1
    return-wide v2
.end method

.method private static getHttpResponseHeader(Ljava/net/HttpURLConnection;)Ljava/util/Map;
    .locals 4
    .param p0    # Ljava/net/HttpURLConnection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/net/HttpURLConnection;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v1}, Ljava/net/HttpURLConnection;->getHeaderField(I)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    return-object v0

    :cond_0
    invoke-virtual {p0, v1}, Ljava/net/HttpURLConnection;->getHeaderFieldKey(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private getIdleTask()Liapp/eric/utils/metadata/DownloadTaskMTBR;
    .locals 4

    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Liapp/eric/utils/base/DownloadManager;->m_taskList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lt v0, v2, :cond_1

    const/4 v1, 0x0

    :cond_0
    return-object v1

    :cond_1
    iget-object v2, p0, Liapp/eric/utils/base/DownloadManager;->m_taskList:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Liapp/eric/utils/metadata/DownloadTaskMTBR;

    sget-object v2, Liapp/eric/utils/metadata/DownloadTaskMTBR$TaskStatusType;->IDLE:Liapp/eric/utils/metadata/DownloadTaskMTBR$TaskStatusType;

    iget-object v3, v1, Liapp/eric/utils/metadata/DownloadTaskMTBR;->status:Liapp/eric/utils/metadata/DownloadTaskMTBR$TaskStatusType;

    if-eq v2, v3, :cond_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private static showResponseHeader(Ljava/net/HttpURLConnection;)V
    .locals 6
    .param p0    # Ljava/net/HttpURLConnection;

    invoke-static {p0}, Liapp/eric/utils/base/DownloadManager;->getHttpResponseHeader(Ljava/net/HttpURLConnection;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_0

    return-void

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_1

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v5, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, ":"

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :goto_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v5, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Liapp/eric/utils/base/Constant;->trace(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const-string v2, ""

    goto :goto_1
.end method


# virtual methods
.method public addTask(Ljava/net/URL;Ljava/lang/String;I)Ljava/lang/String;
    .locals 12
    .param p1    # Ljava/net/URL;
    .param p2    # Ljava/lang/String;
    .param p3    # I

    invoke-virtual {p1}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    invoke-static {v2}, Liapp/eric/utils/base/MD5;->get([B)Ljava/lang/String;

    move-result-object v1

    new-instance v0, Liapp/eric/utils/metadata/DownloadTaskMTBR;

    new-instance v3, Ljava/io/File;

    invoke-direct {v3, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, p1}, Liapp/eric/utils/base/DownloadManager;->getDownloadLength(Ljava/net/URL;)J

    move-result-wide v5

    const-wide/16 v7, 0x0

    const/4 v9, 0x0

    sget-object v10, Liapp/eric/utils/metadata/DownloadTaskMTBR$TaskStatusType;->IDLE:Liapp/eric/utils/metadata/DownloadTaskMTBR$TaskStatusType;

    move-object v2, p1

    move v4, p3

    invoke-direct/range {v0 .. v10}, Liapp/eric/utils/metadata/DownloadTaskMTBR;-><init>(Ljava/lang/String;Ljava/net/URL;Ljava/io/File;IJJZLiapp/eric/utils/metadata/DownloadTaskMTBR$TaskStatusType;)V

    iget-object v2, p0, Liapp/eric/utils/base/DownloadManager;->m_taskList:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Liapp/eric/utils/base/DownloadManager;->m_Table:Ljava/util/Map;

    invoke-interface {v2, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v2, 0x0

    :try_start_0
    invoke-direct {p0, v0, v2}, Liapp/eric/utils/base/DownloadManager;->downloadBR(Liapp/eric/utils/metadata/DownloadTaskMTBR;Liapp/eric/utils/interfaces/DownloadListener;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v1

    :catch_0
    move-exception v11

    invoke-virtual {v11}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public deleteTask(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "nonononononono"

    invoke-static {v0}, Liapp/eric/utils/base/Constant;->trace(Ljava/lang/String;)V

    return-void
.end method

.method public deleteTask(Ljava/net/URL;)V
    .locals 1
    .param p1    # Ljava/net/URL;

    const-string v0, "nonononononono"

    invoke-static {v0}, Liapp/eric/utils/base/Constant;->trace(Ljava/lang/String;)V

    return-void
.end method

.method public isFull()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public resumeTask(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "nonononononono"

    invoke-static {v0}, Liapp/eric/utils/base/Constant;->trace(Ljava/lang/String;)V

    return-void
.end method

.method public resumeTask(Ljava/net/URL;)V
    .locals 1
    .param p1    # Ljava/net/URL;

    const-string v0, "nonononononono"

    invoke-static {v0}, Liapp/eric/utils/base/Constant;->trace(Ljava/lang/String;)V

    return-void
.end method
