.class Liapp/eric/utils/base/FileOperations$MyComparator;
.super Ljava/lang/Object;
.source "FileOperations.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Liapp/eric/utils/base/FileOperations;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Ljava/io/File;",
        ">;"
    }
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$iapp$eric$utils$base$FileOperations$SortordType:[I


# instance fields
.field private m_Ascend:Z

.field private m_SortordType:Liapp/eric/utils/base/FileOperations$SortordType;

.field final synthetic this$0:Liapp/eric/utils/base/FileOperations;


# direct methods
.method static synthetic $SWITCH_TABLE$iapp$eric$utils$base$FileOperations$SortordType()[I
    .locals 3

    sget-object v0, Liapp/eric/utils/base/FileOperations$MyComparator;->$SWITCH_TABLE$iapp$eric$utils$base$FileOperations$SortordType:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Liapp/eric/utils/base/FileOperations$SortordType;->values()[Liapp/eric/utils/base/FileOperations$SortordType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Liapp/eric/utils/base/FileOperations$SortordType;->SORT_BY_DIR:Liapp/eric/utils/base/FileOperations$SortordType;

    invoke-virtual {v1}, Liapp/eric/utils/base/FileOperations$SortordType;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_4

    :goto_1
    :try_start_1
    sget-object v1, Liapp/eric/utils/base/FileOperations$SortordType;->SORT_BY_MODIFIED_DATE:Liapp/eric/utils/base/FileOperations$SortordType;

    invoke-virtual {v1}, Liapp/eric/utils/base/FileOperations$SortordType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_3

    :goto_2
    :try_start_2
    sget-object v1, Liapp/eric/utils/base/FileOperations$SortordType;->SORT_BY_NAME:Liapp/eric/utils/base/FileOperations$SortordType;

    invoke-virtual {v1}, Liapp/eric/utils/base/FileOperations$SortordType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :goto_3
    :try_start_3
    sget-object v1, Liapp/eric/utils/base/FileOperations$SortordType;->SORT_BY_SIZE:Liapp/eric/utils/base/FileOperations$SortordType;

    invoke-virtual {v1}, Liapp/eric/utils/base/FileOperations$SortordType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_1

    :goto_4
    :try_start_4
    sget-object v1, Liapp/eric/utils/base/FileOperations$SortordType;->SORT_BY_TYPE:Liapp/eric/utils/base/FileOperations$SortordType;

    invoke-virtual {v1}, Liapp/eric/utils/base/FileOperations$SortordType;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_0

    :goto_5
    sput-object v0, Liapp/eric/utils/base/FileOperations$MyComparator;->$SWITCH_TABLE$iapp$eric$utils$base$FileOperations$SortordType:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_5

    :catch_1
    move-exception v1

    goto :goto_4

    :catch_2
    move-exception v1

    goto :goto_3

    :catch_3
    move-exception v1

    goto :goto_2

    :catch_4
    move-exception v1

    goto :goto_1
.end method

.method public constructor <init>(Liapp/eric/utils/base/FileOperations;Liapp/eric/utils/base/FileOperations$SortordType;Z)V
    .locals 1
    .param p2    # Liapp/eric/utils/base/FileOperations$SortordType;
    .param p3    # Z

    iput-object p1, p0, Liapp/eric/utils/base/FileOperations$MyComparator;->this$0:Liapp/eric/utils/base/FileOperations;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Liapp/eric/utils/base/FileOperations$SortordType;->SORT_BY_NAME:Liapp/eric/utils/base/FileOperations$SortordType;

    iput-object v0, p0, Liapp/eric/utils/base/FileOperations$MyComparator;->m_SortordType:Liapp/eric/utils/base/FileOperations$SortordType;

    const/4 v0, 0x0

    iput-boolean v0, p0, Liapp/eric/utils/base/FileOperations$MyComparator;->m_Ascend:Z

    iput-object p2, p0, Liapp/eric/utils/base/FileOperations$MyComparator;->m_SortordType:Liapp/eric/utils/base/FileOperations$SortordType;

    iput-boolean p3, p0, Liapp/eric/utils/base/FileOperations$MyComparator;->m_Ascend:Z

    return-void
.end method

.method private compareByDir(Ljava/io/File;Ljava/io/File;)I
    .locals 1
    .param p1    # Ljava/io/File;
    .param p2    # Ljava/io/File;

    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Ljava/io/File;->isFile()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Ljava/io/File;->isFile()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p2}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    invoke-direct {p0, p1, p2}, Liapp/eric/utils/base/FileOperations$MyComparator;->compareByName(Ljava/io/File;Ljava/io/File;)I

    move-result v0

    goto :goto_0
.end method

.method private compareByModifiedDate(Ljava/io/File;Ljava/io/File;Z)I
    .locals 7
    .param p1    # Ljava/io/File;
    .param p2    # Ljava/io/File;
    .param p3    # Z

    const/4 v4, 0x1

    const/4 v5, -0x1

    invoke-virtual {p1}, Ljava/io/File;->lastModified()J

    move-result-wide v0

    invoke-virtual {p2}, Ljava/io/File;->lastModified()J

    move-result-wide v2

    cmp-long v6, v0, v2

    if-nez v6, :cond_1

    const/4 v4, 0x0

    :cond_0
    :goto_0
    return v4

    :cond_1
    if-eqz p3, :cond_2

    cmp-long v6, v0, v2

    if-gtz v6, :cond_0

    move v4, v5

    goto :goto_0

    :cond_2
    cmp-long v6, v0, v2

    if-ltz v6, :cond_0

    move v4, v5

    goto :goto_0
.end method

.method private compareByName(Ljava/io/File;Ljava/io/File;)I
    .locals 3
    .param p1    # Ljava/io/File;
    .param p2    # Ljava/io/File;

    sget-object v1, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    invoke-static {v1}, Ljava/text/Collator;->getInstance(Ljava/util/Locale;)Ljava/text/Collator;

    move-result-object v0

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v1

    return v1
.end method

.method private compareBySize(Ljava/io/File;Ljava/io/File;Z)I
    .locals 8
    .param p1    # Ljava/io/File;
    .param p2    # Ljava/io/File;
    .param p3    # Z

    const/4 v6, 0x0

    const/4 v4, 0x1

    const/4 v5, -0x1

    const-string v7, "111111111111111111"

    invoke-static {v7}, Liapp/eric/utils/base/Constant;->trace(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-virtual {p2}, Ljava/io/File;->isDirectory()Z

    move-result v7

    if-eqz v7, :cond_1

    move v4, v6

    :cond_0
    :goto_0
    return v4

    :cond_1
    const-string v7, "22222222222222222222"

    invoke-static {v7}, Liapp/eric/utils/base/Constant;->trace(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-virtual {p2}, Ljava/io/File;->isFile()Z

    move-result v7

    if-eqz v7, :cond_2

    move v4, v5

    goto :goto_0

    :cond_2
    const-string v7, "3333333333333333333333"

    invoke-static {v7}, Liapp/eric/utils/base/Constant;->trace(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/io/File;->isFile()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-virtual {p2}, Ljava/io/File;->isDirectory()Z

    move-result v7

    if-nez v7, :cond_0

    :cond_3
    const-string v7, "4444444444444444444444444"

    invoke-static {v7}, Liapp/eric/utils/base/Constant;->trace(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v0

    invoke-virtual {p2}, Ljava/io/File;->length()J

    move-result-wide v2

    cmp-long v7, v0, v2

    if-nez v7, :cond_4

    const-string v4, "555555555555555555"

    invoke-static {v4}, Liapp/eric/utils/base/Constant;->trace(Ljava/lang/String;)V

    move v4, v6

    goto :goto_0

    :cond_4
    if-eqz p3, :cond_5

    const-string v6, "6666666666666666666"

    invoke-static {v6}, Liapp/eric/utils/base/Constant;->trace(Ljava/lang/String;)V

    cmp-long v6, v0, v2

    if-gtz v6, :cond_0

    move v4, v5

    goto :goto_0

    :cond_5
    const-string v6, "77777777777777777777"

    invoke-static {v6}, Liapp/eric/utils/base/Constant;->trace(Ljava/lang/String;)V

    cmp-long v6, v0, v2

    if-ltz v6, :cond_0

    move v4, v5

    goto :goto_0
.end method


# virtual methods
.method public compare(Ljava/io/File;Ljava/io/File;)I
    .locals 3
    .param p1    # Ljava/io/File;
    .param p2    # Ljava/io/File;

    const/4 v0, 0x0

    invoke-static {}, Liapp/eric/utils/base/FileOperations$MyComparator;->$SWITCH_TABLE$iapp$eric$utils$base$FileOperations$SortordType()[I

    move-result-object v1

    iget-object v2, p0, Liapp/eric/utils/base/FileOperations$MyComparator;->m_SortordType:Liapp/eric/utils/base/FileOperations$SortordType;

    invoke-virtual {v2}, Liapp/eric/utils/base/FileOperations$SortordType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    :goto_0
    :pswitch_0
    return v0

    :pswitch_1
    invoke-direct {p0, p1, p2}, Liapp/eric/utils/base/FileOperations$MyComparator;->compareByName(Ljava/io/File;Ljava/io/File;)I

    move-result v0

    goto :goto_0

    :pswitch_2
    iget-boolean v1, p0, Liapp/eric/utils/base/FileOperations$MyComparator;->m_Ascend:Z

    invoke-direct {p0, p1, p2, v1}, Liapp/eric/utils/base/FileOperations$MyComparator;->compareBySize(Ljava/io/File;Ljava/io/File;Z)I

    move-result v0

    goto :goto_0

    :pswitch_3
    iget-boolean v1, p0, Liapp/eric/utils/base/FileOperations$MyComparator;->m_Ascend:Z

    invoke-direct {p0, p1, p2, v1}, Liapp/eric/utils/base/FileOperations$MyComparator;->compareByModifiedDate(Ljava/io/File;Ljava/io/File;Z)I

    move-result v0

    goto :goto_0

    :pswitch_4
    invoke-direct {p0, p1, p2}, Liapp/eric/utils/base/FileOperations$MyComparator;->compareByDir(Ljava/io/File;Ljava/io/File;)I

    move-result v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Ljava/io/File;

    check-cast p2, Ljava/io/File;

    invoke-virtual {p0, p1, p2}, Liapp/eric/utils/base/FileOperations$MyComparator;->compare(Ljava/io/File;Ljava/io/File;)I

    move-result v0

    return v0
.end method
