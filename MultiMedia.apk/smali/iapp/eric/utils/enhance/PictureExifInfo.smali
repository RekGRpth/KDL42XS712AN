.class public Liapp/eric/utils/enhance/PictureExifInfo;
.super Ljava/lang/Object;
.source "PictureExifInfo.java"


# instance fields
.field private attrDimension:Ljava/lang/String;

.field private attrSize:Ljava/lang/String;

.field private attrType:Ljava/lang/String;

.field private tagArtist:Ljava/lang/String;

.field private tagAuthor:Ljava/lang/String;

.field private tagDatetimeoriginal:Ljava/lang/String;

.field private tagKeywords:Ljava/lang/String;

.field private tagMake:Ljava/lang/String;

.field private tagModel:Ljava/lang/String;

.field private tagOrientation:Ljava/lang/String;

.field private tagSubject:Ljava/lang/String;

.field private tagXResolution:Ljava/lang/String;

.field private tagYResolution:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 15
    .param p1    # Ljava/lang/String;

    const/16 v14, 0x11b

    const/16 v13, 0x11a

    const/16 v12, 0x112

    const/16 v11, 0x110

    const/16 v10, 0x10f

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Liapp/eric/utils/base/Timestamp;->timerStart()V

    new-instance v6, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v6}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    const/4 v7, 0x1

    iput-boolean v7, v6, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    move-object/from16 v0, p1

    invoke-static {v0, v6}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    new-instance v4, Ljava/io/File;

    move-object/from16 v0, p1

    invoke-direct {v4, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const/4 v3, 0x0

    :try_start_0
    new-instance v7, Ljava/lang/StringBuilder;

    iget v8, v6, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v8, "x"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, v6, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Liapp/eric/utils/enhance/PictureExifInfo;->attrDimension:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/io/File;->length()J

    move-result-wide v7

    invoke-static {v7, v8}, Liapp/eric/utils/base/FormatConversion;->fileSize(J)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Liapp/eric/utils/enhance/PictureExifInfo;->attrSize:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v8

    const/16 v9, 0x2e

    invoke-virtual {v8, v9}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v8

    add-int/lit8 v8, v8, 0x1

    invoke-virtual {v7, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Liapp/eric/utils/enhance/PictureExifInfo;->attrType:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    :try_start_1
    invoke-static {v4}, Lcom/drew/imaging/ImageMetadataReader;->readMetadata(Ljava/io/File;)Lcom/drew/metadata/Metadata;

    move-result-object v5

    const-class v7, Lcom/drew/metadata/exif/ExifIFD0Directory;

    invoke-virtual {v5, v7}, Lcom/drew/metadata/Metadata;->getDirectory(Ljava/lang/Class;)Lcom/drew/metadata/Directory;

    move-result-object v7

    move-object v0, v7

    check-cast v0, Lcom/drew/metadata/exif/ExifIFD0Directory;

    move-object v3, v0
    :try_end_1
    .catch Lcom/drew/imaging/ImageProcessingException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    :goto_1
    if-nez v3, :cond_0

    const-string v7, ""

    iput-object v7, p0, Liapp/eric/utils/enhance/PictureExifInfo;->tagMake:Ljava/lang/String;

    const-string v7, ""

    iput-object v7, p0, Liapp/eric/utils/enhance/PictureExifInfo;->tagModel:Ljava/lang/String;

    const-string v7, ""

    iput-object v7, p0, Liapp/eric/utils/enhance/PictureExifInfo;->tagOrientation:Ljava/lang/String;

    const-string v7, ""

    iput-object v7, p0, Liapp/eric/utils/enhance/PictureExifInfo;->tagXResolution:Ljava/lang/String;

    const-string v7, ""

    iput-object v7, p0, Liapp/eric/utils/enhance/PictureExifInfo;->tagYResolution:Ljava/lang/String;

    const-string v7, ""

    iput-object v7, p0, Liapp/eric/utils/enhance/PictureExifInfo;->tagDatetimeoriginal:Ljava/lang/String;

    const-string v7, ""

    iput-object v7, p0, Liapp/eric/utils/enhance/PictureExifInfo;->tagArtist:Ljava/lang/String;

    const-string v7, ""

    iput-object v7, p0, Liapp/eric/utils/enhance/PictureExifInfo;->tagAuthor:Ljava/lang/String;

    const-string v7, ""

    iput-object v7, p0, Liapp/eric/utils/enhance/PictureExifInfo;->tagKeywords:Ljava/lang/String;

    const-string v7, ""

    iput-object v7, p0, Liapp/eric/utils/enhance/PictureExifInfo;->tagSubject:Ljava/lang/String;

    :goto_2
    return-void

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v1

    invoke-virtual {v1}, Lcom/drew/imaging/ImageProcessingException;->printStackTrace()V

    goto :goto_1

    :catch_2
    move-exception v1

    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    :cond_0
    invoke-virtual {v3, v10}, Lcom/drew/metadata/exif/ExifIFD0Directory;->containsTag(I)Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-virtual {v3, v10}, Lcom/drew/metadata/exif/ExifIFD0Directory;->getDescription(I)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Liapp/eric/utils/enhance/PictureExifInfo;->tagMake:Ljava/lang/String;

    :goto_3
    invoke-virtual {v3, v11}, Lcom/drew/metadata/exif/ExifIFD0Directory;->containsTag(I)Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-virtual {v3, v11}, Lcom/drew/metadata/exif/ExifIFD0Directory;->getDescription(I)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Liapp/eric/utils/enhance/PictureExifInfo;->tagModel:Ljava/lang/String;

    :goto_4
    const/16 v7, 0x132

    invoke-virtual {v3, v7}, Lcom/drew/metadata/exif/ExifIFD0Directory;->containsTag(I)Z

    move-result v7

    if-eqz v7, :cond_3

    const/16 v7, 0x132

    invoke-virtual {v3, v7}, Lcom/drew/metadata/exif/ExifIFD0Directory;->getDescription(I)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Liapp/eric/utils/enhance/PictureExifInfo;->tagDatetimeoriginal:Ljava/lang/String;

    iget-object v7, p0, Liapp/eric/utils/enhance/PictureExifInfo;->tagDatetimeoriginal:Ljava/lang/String;

    const-string v8, ":"

    const-string v9, "-"

    invoke-virtual {v7, v8, v9}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Liapp/eric/utils/enhance/PictureExifInfo;->tagDatetimeoriginal:Ljava/lang/String;

    :goto_5
    invoke-virtual {v3, v12}, Lcom/drew/metadata/exif/ExifIFD0Directory;->containsTag(I)Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-virtual {v3, v12}, Lcom/drew/metadata/exif/ExifIFD0Directory;->getDescription(I)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Liapp/eric/utils/enhance/PictureExifInfo;->tagOrientation:Ljava/lang/String;

    :goto_6
    invoke-virtual {v3, v13}, Lcom/drew/metadata/exif/ExifIFD0Directory;->containsTag(I)Z

    move-result v7

    if-eqz v7, :cond_5

    invoke-virtual {v3, v13}, Lcom/drew/metadata/exif/ExifIFD0Directory;->getDescription(I)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Liapp/eric/utils/enhance/PictureExifInfo;->tagXResolution:Ljava/lang/String;

    :goto_7
    invoke-virtual {v3, v14}, Lcom/drew/metadata/exif/ExifIFD0Directory;->containsTag(I)Z

    move-result v7

    if-eqz v7, :cond_6

    invoke-virtual {v3, v14}, Lcom/drew/metadata/exif/ExifIFD0Directory;->getDescription(I)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Liapp/eric/utils/enhance/PictureExifInfo;->tagYResolution:Ljava/lang/String;

    :goto_8
    const/16 v7, 0x13b

    invoke-virtual {v3, v7}, Lcom/drew/metadata/exif/ExifIFD0Directory;->containsTag(I)Z

    move-result v7

    if-eqz v7, :cond_7

    const/16 v7, 0x13b

    invoke-virtual {v3, v7}, Lcom/drew/metadata/exif/ExifIFD0Directory;->getDescription(I)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Liapp/eric/utils/enhance/PictureExifInfo;->tagArtist:Ljava/lang/String;

    :goto_9
    const v7, 0x9c9d

    invoke-virtual {v3, v7}, Lcom/drew/metadata/exif/ExifIFD0Directory;->containsTag(I)Z

    move-result v7

    if-eqz v7, :cond_8

    const v7, 0x9c9d

    invoke-virtual {v3, v7}, Lcom/drew/metadata/exif/ExifIFD0Directory;->getDescription(I)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Liapp/eric/utils/enhance/PictureExifInfo;->tagAuthor:Ljava/lang/String;

    :goto_a
    const v7, 0x9c9e

    invoke-virtual {v3, v7}, Lcom/drew/metadata/exif/ExifIFD0Directory;->containsTag(I)Z

    move-result v7

    if-eqz v7, :cond_9

    const v7, 0x9c9e

    invoke-virtual {v3, v7}, Lcom/drew/metadata/exif/ExifIFD0Directory;->getDescription(I)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Liapp/eric/utils/enhance/PictureExifInfo;->tagKeywords:Ljava/lang/String;

    :goto_b
    const v7, 0x9c9f

    invoke-virtual {v3, v7}, Lcom/drew/metadata/exif/ExifIFD0Directory;->containsTag(I)Z

    move-result v7

    if-eqz v7, :cond_a

    const v7, 0x9c9f

    invoke-virtual {v3, v7}, Lcom/drew/metadata/exif/ExifIFD0Directory;->getDescription(I)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Liapp/eric/utils/enhance/PictureExifInfo;->tagSubject:Ljava/lang/String;

    :goto_c
    const-string v7, "PictureExifInfo"

    invoke-static {v7}, Liapp/eric/utils/base/Timestamp;->timerEnd(Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_1
    const-string v7, ""

    iput-object v7, p0, Liapp/eric/utils/enhance/PictureExifInfo;->tagMake:Ljava/lang/String;

    goto/16 :goto_3

    :cond_2
    const-string v7, ""

    iput-object v7, p0, Liapp/eric/utils/enhance/PictureExifInfo;->tagModel:Ljava/lang/String;

    goto/16 :goto_4

    :cond_3
    const-string v7, ""

    iput-object v7, p0, Liapp/eric/utils/enhance/PictureExifInfo;->tagDatetimeoriginal:Ljava/lang/String;

    goto/16 :goto_5

    :cond_4
    const-string v7, ""

    iput-object v7, p0, Liapp/eric/utils/enhance/PictureExifInfo;->tagOrientation:Ljava/lang/String;

    goto :goto_6

    :cond_5
    const-string v7, ""

    iput-object v7, p0, Liapp/eric/utils/enhance/PictureExifInfo;->tagXResolution:Ljava/lang/String;

    goto :goto_7

    :cond_6
    const-string v7, ""

    iput-object v7, p0, Liapp/eric/utils/enhance/PictureExifInfo;->tagYResolution:Ljava/lang/String;

    goto :goto_8

    :cond_7
    const-string v7, ""

    iput-object v7, p0, Liapp/eric/utils/enhance/PictureExifInfo;->tagArtist:Ljava/lang/String;

    goto :goto_9

    :cond_8
    const-string v7, ""

    iput-object v7, p0, Liapp/eric/utils/enhance/PictureExifInfo;->tagAuthor:Ljava/lang/String;

    goto :goto_a

    :cond_9
    const-string v7, ""

    iput-object v7, p0, Liapp/eric/utils/enhance/PictureExifInfo;->tagKeywords:Ljava/lang/String;

    goto :goto_b

    :cond_a
    const-string v7, ""

    iput-object v7, p0, Liapp/eric/utils/enhance/PictureExifInfo;->tagSubject:Ljava/lang/String;

    goto :goto_c
.end method

.method private showInfo(Lcom/drew/metadata/exif/ExifIFD0Directory;)V
    .locals 4
    .param p1    # Lcom/drew/metadata/exif/ExifIFD0Directory;

    invoke-virtual {p1}, Lcom/drew/metadata/exif/ExifIFD0Directory;->getTags()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Liapp/eric/utils/enhance/PictureExifInfo;->tagArtist:Ljava/lang/String;

    invoke-static {v3}, Liapp/eric/utils/base/Trace;->Debug(Ljava/lang/String;)V

    iget-object v3, p0, Liapp/eric/utils/enhance/PictureExifInfo;->tagAuthor:Ljava/lang/String;

    invoke-static {v3}, Liapp/eric/utils/base/Trace;->Debug(Ljava/lang/String;)V

    iget-object v3, p0, Liapp/eric/utils/enhance/PictureExifInfo;->tagKeywords:Ljava/lang/String;

    invoke-static {v3}, Liapp/eric/utils/base/Trace;->Debug(Ljava/lang/String;)V

    iget-object v3, p0, Liapp/eric/utils/enhance/PictureExifInfo;->tagSubject:Ljava/lang/String;

    invoke-static {v3}, Liapp/eric/utils/base/Trace;->Debug(Ljava/lang/String;)V

    return-void

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/drew/metadata/Tag;

    invoke-virtual {v1}, Lcom/drew/metadata/Tag;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Liapp/eric/utils/base/Trace;->Debug(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public getAttrDimension()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Liapp/eric/utils/enhance/PictureExifInfo;->attrDimension:Ljava/lang/String;

    return-object v0
.end method

.method public getAttrSize()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Liapp/eric/utils/enhance/PictureExifInfo;->attrSize:Ljava/lang/String;

    return-object v0
.end method

.method public getAttrType()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Liapp/eric/utils/enhance/PictureExifInfo;->attrType:Ljava/lang/String;

    return-object v0
.end method

.method public getTagArtist()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Liapp/eric/utils/enhance/PictureExifInfo;->tagArtist:Ljava/lang/String;

    return-object v0
.end method

.method public getTagAuthor()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Liapp/eric/utils/enhance/PictureExifInfo;->tagAuthor:Ljava/lang/String;

    return-object v0
.end method

.method public getTagDatetimeoriginal()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Liapp/eric/utils/enhance/PictureExifInfo;->tagDatetimeoriginal:Ljava/lang/String;

    return-object v0
.end method

.method public getTagKeywords()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Liapp/eric/utils/enhance/PictureExifInfo;->tagKeywords:Ljava/lang/String;

    return-object v0
.end method

.method public getTagMake()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Liapp/eric/utils/enhance/PictureExifInfo;->tagMake:Ljava/lang/String;

    return-object v0
.end method

.method public getTagModel()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Liapp/eric/utils/enhance/PictureExifInfo;->tagModel:Ljava/lang/String;

    return-object v0
.end method

.method public getTagOrientation()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Liapp/eric/utils/enhance/PictureExifInfo;->tagOrientation:Ljava/lang/String;

    return-object v0
.end method

.method public getTagSubject()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Liapp/eric/utils/enhance/PictureExifInfo;->tagSubject:Ljava/lang/String;

    return-object v0
.end method

.method public getTagXResolution()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Liapp/eric/utils/enhance/PictureExifInfo;->tagXResolution:Ljava/lang/String;

    return-object v0
.end method

.method public getTagYResolution()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Liapp/eric/utils/enhance/PictureExifInfo;->tagYResolution:Ljava/lang/String;

    return-object v0
.end method

.method public setAttrDimension(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Liapp/eric/utils/enhance/PictureExifInfo;->attrDimension:Ljava/lang/String;

    return-void
.end method

.method public setAttrSize(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Liapp/eric/utils/enhance/PictureExifInfo;->attrSize:Ljava/lang/String;

    return-void
.end method

.method public setAttrType(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Liapp/eric/utils/enhance/PictureExifInfo;->attrType:Ljava/lang/String;

    return-void
.end method

.method public setTagArtist(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Liapp/eric/utils/enhance/PictureExifInfo;->tagArtist:Ljava/lang/String;

    return-void
.end method

.method public setTagAuthor(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Liapp/eric/utils/enhance/PictureExifInfo;->tagAuthor:Ljava/lang/String;

    return-void
.end method

.method public setTagDatetimeoriginal(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Liapp/eric/utils/enhance/PictureExifInfo;->tagDatetimeoriginal:Ljava/lang/String;

    return-void
.end method

.method public setTagKeywords(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Liapp/eric/utils/enhance/PictureExifInfo;->tagKeywords:Ljava/lang/String;

    return-void
.end method

.method public setTagMake(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Liapp/eric/utils/enhance/PictureExifInfo;->tagMake:Ljava/lang/String;

    return-void
.end method

.method public setTagModel(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Liapp/eric/utils/enhance/PictureExifInfo;->tagModel:Ljava/lang/String;

    return-void
.end method

.method public setTagOrientation(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Liapp/eric/utils/enhance/PictureExifInfo;->tagOrientation:Ljava/lang/String;

    return-void
.end method

.method public setTagSubject(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Liapp/eric/utils/enhance/PictureExifInfo;->tagSubject:Ljava/lang/String;

    return-void
.end method

.method public setTagXResolution(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Liapp/eric/utils/enhance/PictureExifInfo;->tagXResolution:Ljava/lang/String;

    return-void
.end method

.method public setTagYResolution(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Liapp/eric/utils/enhance/PictureExifInfo;->tagYResolution:Ljava/lang/String;

    return-void
.end method
