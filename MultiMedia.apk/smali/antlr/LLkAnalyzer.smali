.class public Lantlr/LLkAnalyzer;
.super Ljava/lang/Object;
.source "LLkAnalyzer.java"

# interfaces
.implements Lantlr/LLkGrammarAnalyzer;


# instance fields
.field public DEBUG_ANALYZER:Z

.field charFormatter:Lantlr/CharFormatter;

.field private currentBlock:Lantlr/AlternativeBlock;

.field protected grammar:Lantlr/Grammar;

.field protected lexicalAnalysis:Z

.field protected tool:Lantlr/Tool;


# direct methods
.method public constructor <init>(Lantlr/Tool;)V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v0, p0, Lantlr/LLkAnalyzer;->DEBUG_ANALYZER:Z

    iput-object v1, p0, Lantlr/LLkAnalyzer;->tool:Lantlr/Tool;

    iput-object v1, p0, Lantlr/LLkAnalyzer;->grammar:Lantlr/Grammar;

    iput-boolean v0, p0, Lantlr/LLkAnalyzer;->lexicalAnalysis:Z

    new-instance v0, Lantlr/JavaCharFormatter;

    invoke-direct {v0}, Lantlr/JavaCharFormatter;-><init>()V

    iput-object v0, p0, Lantlr/LLkAnalyzer;->charFormatter:Lantlr/CharFormatter;

    iput-object p1, p0, Lantlr/LLkAnalyzer;->tool:Lantlr/Tool;

    return-void
.end method

.method private getAltLookahead(Lantlr/AlternativeBlock;II)Lantlr/Lookahead;
    .locals 3

    invoke-virtual {p1, p2}, Lantlr/AlternativeBlock;->getAlternativeAt(I)Lantlr/Alternative;

    move-result-object v1

    iget-object v0, v1, Lantlr/Alternative;->head:Lantlr/AlternativeElement;

    iget-object v2, v1, Lantlr/Alternative;->cache:[Lantlr/Lookahead;

    aget-object v2, v2, p3

    if-nez v2, :cond_0

    invoke-virtual {v0, p3}, Lantlr/AlternativeElement;->look(I)Lantlr/Lookahead;

    move-result-object v0

    iget-object v1, v1, Lantlr/Alternative;->cache:[Lantlr/Lookahead;

    aput-object v0, v1, p3

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, v1, Lantlr/Alternative;->cache:[Lantlr/Lookahead;

    aget-object v0, v0, p3

    goto :goto_0
.end method

.method public static lookaheadEquivForApproxAndFullAnalysis([Lantlr/Lookahead;I)Z
    .locals 3

    const/4 v0, 0x1

    move v1, v0

    :goto_0
    add-int/lit8 v2, p1, -0x1

    if-gt v1, v2, :cond_0

    aget-object v2, p0, v1

    iget-object v2, v2, Lantlr/Lookahead;->fset:Lantlr/collections/impl/BitSet;

    invoke-virtual {v2}, Lantlr/collections/impl/BitSet;->degree()I

    move-result v2

    if-le v2, v0, :cond_1

    const/4 v0, 0x0

    :cond_0
    return v0

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private removeCompetingPredictionSets(Lantlr/collections/impl/BitSet;Lantlr/AlternativeElement;)V
    .locals 3

    iget-object v0, p0, Lantlr/LLkAnalyzer;->currentBlock:Lantlr/AlternativeBlock;

    iget-object v1, p0, Lantlr/LLkAnalyzer;->currentBlock:Lantlr/AlternativeBlock;

    iget v1, v1, Lantlr/AlternativeBlock;->analysisAlt:I

    invoke-virtual {v0, v1}, Lantlr/AlternativeBlock;->getAlternativeAt(I)Lantlr/Alternative;

    move-result-object v0

    iget-object v0, v0, Lantlr/Alternative;->head:Lantlr/AlternativeElement;

    instance-of v1, v0, Lantlr/TreeElement;

    if-eqz v1, :cond_1

    check-cast v0, Lantlr/TreeElement;

    iget-object v0, v0, Lantlr/TreeElement;->root:Lantlr/GrammarAtom;

    if-eq v0, p2, :cond_2

    :cond_0
    return-void

    :cond_1
    if-ne p2, v0, :cond_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lantlr/LLkAnalyzer;->currentBlock:Lantlr/AlternativeBlock;

    iget v1, v1, Lantlr/AlternativeBlock;->analysisAlt:I

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lantlr/LLkAnalyzer;->currentBlock:Lantlr/AlternativeBlock;

    invoke-virtual {v1, v0}, Lantlr/AlternativeBlock;->getAlternativeAt(I)Lantlr/Alternative;

    move-result-object v1

    iget-object v1, v1, Lantlr/Alternative;->head:Lantlr/AlternativeElement;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lantlr/AlternativeElement;->look(I)Lantlr/Lookahead;

    move-result-object v1

    iget-object v1, v1, Lantlr/Lookahead;->fset:Lantlr/collections/impl/BitSet;

    invoke-virtual {p1, v1}, Lantlr/collections/impl/BitSet;->subtractInPlace(Lantlr/collections/impl/BitSet;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private removeCompetingPredictionSetsFromWildcard([Lantlr/Lookahead;Lantlr/AlternativeElement;I)V
    .locals 4

    const/4 v0, 0x1

    move v1, v0

    :goto_0
    if-gt v1, p3, :cond_1

    const/4 v0, 0x0

    :goto_1
    iget-object v2, p0, Lantlr/LLkAnalyzer;->currentBlock:Lantlr/AlternativeBlock;

    iget v2, v2, Lantlr/AlternativeBlock;->analysisAlt:I

    if-ge v0, v2, :cond_0

    iget-object v2, p0, Lantlr/LLkAnalyzer;->currentBlock:Lantlr/AlternativeBlock;

    invoke-virtual {v2, v0}, Lantlr/AlternativeBlock;->getAlternativeAt(I)Lantlr/Alternative;

    move-result-object v2

    iget-object v2, v2, Lantlr/Alternative;->head:Lantlr/AlternativeElement;

    aget-object v3, p1, v1

    iget-object v3, v3, Lantlr/Lookahead;->fset:Lantlr/collections/impl/BitSet;

    invoke-virtual {v2, v1}, Lantlr/AlternativeElement;->look(I)Lantlr/Lookahead;

    move-result-object v2

    iget-object v2, v2, Lantlr/Lookahead;->fset:Lantlr/collections/impl/BitSet;

    invoke-virtual {v3, v2}, Lantlr/collections/impl/BitSet;->subtractInPlace(Lantlr/collections/impl/BitSet;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return-void
.end method

.method private reset()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    iput-object v1, p0, Lantlr/LLkAnalyzer;->grammar:Lantlr/Grammar;

    iput-boolean v0, p0, Lantlr/LLkAnalyzer;->DEBUG_ANALYZER:Z

    iput-object v1, p0, Lantlr/LLkAnalyzer;->currentBlock:Lantlr/AlternativeBlock;

    iput-boolean v0, p0, Lantlr/LLkAnalyzer;->lexicalAnalysis:Z

    return-void
.end method


# virtual methods
.method public FOLLOW(ILantlr/RuleEndElement;)Lantlr/Lookahead;
    .locals 11

    const/4 v10, 0x0

    const/4 v9, 0x1

    const/4 v3, 0x0

    iget-object v0, p2, Lantlr/RuleEndElement;->block:Lantlr/AlternativeBlock;

    check-cast v0, Lantlr/RuleBlock;

    iget-boolean v1, p0, Lantlr/LLkAnalyzer;->lexicalAnalysis:Z

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lantlr/RuleBlock;->getRuleName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lantlr/CodeGenerator;->encodeLexerRuleName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    :goto_0
    iget-boolean v0, p0, Lantlr/LLkAnalyzer;->DEBUG_ANALYZER:Z

    if-eqz v0, :cond_0

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v4, "FOLLOW("

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v4, ","

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v4, ")"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p2, Lantlr/RuleEndElement;->lock:[Z

    aget-boolean v0, v0, p1

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lantlr/LLkAnalyzer;->DEBUG_ANALYZER:Z

    if-eqz v0, :cond_1

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "FOLLOW cycle to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_1
    new-instance v0, Lantlr/Lookahead;

    invoke-direct {v0, v1}, Lantlr/Lookahead;-><init>(Ljava/lang/String;)V

    :goto_1
    return-object v0

    :cond_2
    invoke-virtual {v0}, Lantlr/RuleBlock;->getRuleName()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    :cond_3
    iget-object v0, p2, Lantlr/RuleEndElement;->cache:[Lantlr/Lookahead;

    aget-object v0, v0, p1

    if-eqz v0, :cond_a

    iget-boolean v0, p0, Lantlr/LLkAnalyzer;->DEBUG_ANALYZER:Z

    if-eqz v0, :cond_4

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "cache entry FOLLOW("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, ") for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    iget-object v3, p2, Lantlr/RuleEndElement;->cache:[Lantlr/Lookahead;

    aget-object v3, v3, p1

    const-string v4, ","

    iget-object v5, p0, Lantlr/LLkAnalyzer;->charFormatter:Lantlr/CharFormatter;

    iget-object v6, p0, Lantlr/LLkAnalyzer;->grammar:Lantlr/Grammar;

    invoke-virtual {v3, v4, v5, v6}, Lantlr/Lookahead;->toString(Ljava/lang/String;Lantlr/CharFormatter;Lantlr/Grammar;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_4
    iget-object v0, p2, Lantlr/RuleEndElement;->cache:[Lantlr/Lookahead;

    aget-object v0, v0, p1

    iget-object v0, v0, Lantlr/Lookahead;->cycle:Ljava/lang/String;

    if-nez v0, :cond_5

    iget-object v0, p2, Lantlr/RuleEndElement;->cache:[Lantlr/Lookahead;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Lantlr/Lookahead;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lantlr/Lookahead;

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lantlr/LLkAnalyzer;->grammar:Lantlr/Grammar;

    iget-object v2, p2, Lantlr/RuleEndElement;->cache:[Lantlr/Lookahead;

    aget-object v2, v2, p1

    iget-object v2, v2, Lantlr/Lookahead;->cycle:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lantlr/Grammar;->getSymbol(Ljava/lang/String;)Lantlr/GrammarSymbol;

    move-result-object v0

    check-cast v0, Lantlr/RuleSymbol;

    invoke-virtual {v0}, Lantlr/RuleSymbol;->getBlock()Lantlr/RuleBlock;

    move-result-object v0

    iget-object v2, v0, Lantlr/RuleBlock;->endNode:Lantlr/RuleEndElement;

    iget-object v0, v2, Lantlr/RuleEndElement;->cache:[Lantlr/Lookahead;

    aget-object v0, v0, p1

    if-nez v0, :cond_6

    iget-object v0, p2, Lantlr/RuleEndElement;->cache:[Lantlr/Lookahead;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Lantlr/Lookahead;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lantlr/Lookahead;

    goto/16 :goto_1

    :cond_6
    iget-boolean v0, p0, Lantlr/LLkAnalyzer;->DEBUG_ANALYZER:Z

    if-eqz v0, :cond_7

    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string v4, "combining FOLLOW("

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v4, ") for "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v4, ": from "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-object v4, p2, Lantlr/RuleEndElement;->cache:[Lantlr/Lookahead;

    aget-object v4, v4, p1

    const-string v5, ","

    iget-object v6, p0, Lantlr/LLkAnalyzer;->charFormatter:Lantlr/CharFormatter;

    iget-object v7, p0, Lantlr/LLkAnalyzer;->grammar:Lantlr/Grammar;

    invoke-virtual {v4, v5, v6, v7}, Lantlr/Lookahead;->toString(Ljava/lang/String;Lantlr/CharFormatter;Lantlr/Grammar;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v4, " with FOLLOW for "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    iget-object v0, v2, Lantlr/RuleEndElement;->block:Lantlr/AlternativeBlock;

    check-cast v0, Lantlr/RuleBlock;

    invoke-virtual {v0}, Lantlr/RuleBlock;->getRuleName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v4, ": "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-object v4, v2, Lantlr/RuleEndElement;->cache:[Lantlr/Lookahead;

    aget-object v4, v4, p1

    const-string v5, ","

    iget-object v6, p0, Lantlr/LLkAnalyzer;->charFormatter:Lantlr/CharFormatter;

    iget-object v7, p0, Lantlr/LLkAnalyzer;->grammar:Lantlr/Grammar;

    invoke-virtual {v4, v5, v6, v7}, Lantlr/Lookahead;->toString(Ljava/lang/String;Lantlr/CharFormatter;Lantlr/Grammar;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_7
    iget-object v0, v2, Lantlr/RuleEndElement;->cache:[Lantlr/Lookahead;

    aget-object v0, v0, p1

    iget-object v0, v0, Lantlr/Lookahead;->cycle:Ljava/lang/String;

    if-nez v0, :cond_9

    iget-object v0, p2, Lantlr/RuleEndElement;->cache:[Lantlr/Lookahead;

    aget-object v0, v0, p1

    iget-object v2, v2, Lantlr/RuleEndElement;->cache:[Lantlr/Lookahead;

    aget-object v2, v2, p1

    invoke-virtual {v0, v2}, Lantlr/Lookahead;->combineWith(Lantlr/Lookahead;)V

    iget-object v0, p2, Lantlr/RuleEndElement;->cache:[Lantlr/Lookahead;

    aget-object v0, v0, p1

    iput-object v10, v0, Lantlr/Lookahead;->cycle:Ljava/lang/String;

    :goto_2
    iget-boolean v0, p0, Lantlr/LLkAnalyzer;->DEBUG_ANALYZER:Z

    if-eqz v0, :cond_8

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "saving FOLLOW("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, ") for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, ": from "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p2, Lantlr/RuleEndElement;->cache:[Lantlr/Lookahead;

    aget-object v2, v2, p1

    const-string v3, ","

    iget-object v4, p0, Lantlr/LLkAnalyzer;->charFormatter:Lantlr/CharFormatter;

    iget-object v5, p0, Lantlr/LLkAnalyzer;->grammar:Lantlr/Grammar;

    invoke-virtual {v2, v3, v4, v5}, Lantlr/Lookahead;->toString(Ljava/lang/String;Lantlr/CharFormatter;Lantlr/Grammar;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_8
    iget-object v0, p2, Lantlr/RuleEndElement;->cache:[Lantlr/Lookahead;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Lantlr/Lookahead;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lantlr/Lookahead;

    goto/16 :goto_1

    :cond_9
    invoke-virtual {p0, p1, v2}, Lantlr/LLkAnalyzer;->FOLLOW(ILantlr/RuleEndElement;)Lantlr/Lookahead;

    move-result-object v0

    iget-object v2, p2, Lantlr/RuleEndElement;->cache:[Lantlr/Lookahead;

    aget-object v2, v2, p1

    invoke-virtual {v2, v0}, Lantlr/Lookahead;->combineWith(Lantlr/Lookahead;)V

    iget-object v2, p2, Lantlr/RuleEndElement;->cache:[Lantlr/Lookahead;

    aget-object v2, v2, p1

    iget-object v0, v0, Lantlr/Lookahead;->cycle:Ljava/lang/String;

    iput-object v0, v2, Lantlr/Lookahead;->cycle:Ljava/lang/String;

    goto :goto_2

    :cond_a
    iget-object v0, p2, Lantlr/RuleEndElement;->lock:[Z

    aput-boolean v9, v0, p1

    new-instance v4, Lantlr/Lookahead;

    invoke-direct {v4}, Lantlr/Lookahead;-><init>()V

    iget-object v0, p0, Lantlr/LLkAnalyzer;->grammar:Lantlr/Grammar;

    invoke-virtual {v0, v1}, Lantlr/Grammar;->getSymbol(Ljava/lang/String;)Lantlr/GrammarSymbol;

    move-result-object v0

    check-cast v0, Lantlr/RuleSymbol;

    move v2, v3

    :goto_3
    invoke-virtual {v0}, Lantlr/RuleSymbol;->numReferences()I

    move-result v5

    if-ge v2, v5, :cond_f

    invoke-virtual {v0, v2}, Lantlr/RuleSymbol;->getReference(I)Lantlr/RuleRefElement;

    move-result-object v5

    iget-boolean v6, p0, Lantlr/LLkAnalyzer;->DEBUG_ANALYZER:Z

    if-eqz v6, :cond_b

    sget-object v6, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v7, Ljava/lang/StringBuffer;

    invoke-direct {v7}, Ljava/lang/StringBuffer;-><init>()V

    const-string v8, "next["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    const-string v8, "] is "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    iget-object v8, v5, Lantlr/RuleRefElement;->next:Lantlr/AlternativeElement;

    invoke-virtual {v8}, Lantlr/AlternativeElement;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_b
    iget-object v5, v5, Lantlr/RuleRefElement;->next:Lantlr/AlternativeElement;

    invoke-virtual {v5, p1}, Lantlr/AlternativeElement;->look(I)Lantlr/Lookahead;

    move-result-object v5

    iget-boolean v6, p0, Lantlr/LLkAnalyzer;->DEBUG_ANALYZER:Z

    if-eqz v6, :cond_c

    sget-object v6, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v7, Ljava/lang/StringBuffer;

    invoke-direct {v7}, Ljava/lang/StringBuffer;-><init>()V

    const-string v8, "FIRST of next["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    const-string v8, "] ptr is "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v5}, Lantlr/Lookahead;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_c
    iget-object v6, v5, Lantlr/Lookahead;->cycle:Ljava/lang/String;

    if-eqz v6, :cond_d

    iget-object v6, v5, Lantlr/Lookahead;->cycle:Ljava/lang/String;

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_d

    iput-object v10, v5, Lantlr/Lookahead;->cycle:Ljava/lang/String;

    :cond_d
    invoke-virtual {v4, v5}, Lantlr/Lookahead;->combineWith(Lantlr/Lookahead;)V

    iget-boolean v5, p0, Lantlr/LLkAnalyzer;->DEBUG_ANALYZER:Z

    if-eqz v5, :cond_e

    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6}, Ljava/lang/StringBuffer;-><init>()V

    const-string v7, "combined FOLLOW["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    const-string v7, "] is "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v4}, Lantlr/Lookahead;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_e
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_3

    :cond_f
    iget-object v0, p2, Lantlr/RuleEndElement;->lock:[Z

    aput-boolean v3, v0, p1

    iget-object v0, v4, Lantlr/Lookahead;->fset:Lantlr/collections/impl/BitSet;

    invoke-virtual {v0}, Lantlr/collections/impl/BitSet;->nil()Z

    move-result v0

    if-eqz v0, :cond_10

    iget-object v0, v4, Lantlr/Lookahead;->cycle:Ljava/lang/String;

    if-nez v0, :cond_10

    iget-object v0, p0, Lantlr/LLkAnalyzer;->grammar:Lantlr/Grammar;

    instance-of v0, v0, Lantlr/TreeWalkerGrammar;

    if-eqz v0, :cond_12

    iget-object v0, v4, Lantlr/Lookahead;->fset:Lantlr/collections/impl/BitSet;

    const/4 v2, 0x3

    invoke-virtual {v0, v2}, Lantlr/collections/impl/BitSet;->add(I)V

    :cond_10
    :goto_4
    iget-boolean v0, p0, Lantlr/LLkAnalyzer;->DEBUG_ANALYZER:Z

    if-eqz v0, :cond_11

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "saving FOLLOW("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, ") for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, ","

    iget-object v3, p0, Lantlr/LLkAnalyzer;->charFormatter:Lantlr/CharFormatter;

    iget-object v5, p0, Lantlr/LLkAnalyzer;->grammar:Lantlr/Grammar;

    invoke-virtual {v4, v2, v3, v5}, Lantlr/Lookahead;->toString(Ljava/lang/String;Lantlr/CharFormatter;Lantlr/Grammar;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_11
    iget-object v1, p2, Lantlr/RuleEndElement;->cache:[Lantlr/Lookahead;

    invoke-virtual {v4}, Lantlr/Lookahead;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lantlr/Lookahead;

    aput-object v0, v1, p1

    move-object v0, v4

    goto/16 :goto_1

    :cond_12
    iget-object v0, p0, Lantlr/LLkAnalyzer;->grammar:Lantlr/Grammar;

    instance-of v0, v0, Lantlr/LexerGrammar;

    if-eqz v0, :cond_13

    invoke-virtual {v4}, Lantlr/Lookahead;->setEpsilon()V

    goto :goto_4

    :cond_13
    iget-object v0, v4, Lantlr/Lookahead;->fset:Lantlr/collections/impl/BitSet;

    invoke-virtual {v0, v9}, Lantlr/collections/impl/BitSet;->add(I)V

    goto :goto_4
.end method

.method protected altUsesWildcardDefault(Lantlr/Alternative;)Z
    .locals 3

    const/4 v2, 0x1

    iget-object v1, p1, Lantlr/Alternative;->head:Lantlr/AlternativeElement;

    instance-of v0, v1, Lantlr/TreeElement;

    if-eqz v0, :cond_0

    move-object v0, v1

    check-cast v0, Lantlr/TreeElement;

    iget-object v0, v0, Lantlr/TreeElement;->root:Lantlr/GrammarAtom;

    instance-of v0, v0, Lantlr/WildcardElement;

    if-eqz v0, :cond_0

    move v0, v2

    :goto_0
    return v0

    :cond_0
    instance-of v0, v1, Lantlr/WildcardElement;

    if-eqz v0, :cond_1

    iget-object v0, v1, Lantlr/AlternativeElement;->next:Lantlr/AlternativeElement;

    instance-of v0, v0, Lantlr/BlockEndElement;

    if-eqz v0, :cond_1

    move v0, v2

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public deterministic(Lantlr/AlternativeBlock;)Z
    .locals 17

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lantlr/LLkAnalyzer;->DEBUG_ANALYZER:Z

    if-eqz v2, :cond_0

    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string v4, "deterministic("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    const/4 v2, 0x1

    move-object/from16 v0, p1

    iget-object v3, v0, Lantlr/AlternativeBlock;->alternatives:Lantlr/collections/impl/Vector;

    invoke-virtual {v3}, Lantlr/collections/impl/Vector;->size()I

    move-result v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lantlr/LLkAnalyzer;->currentBlock:Lantlr/AlternativeBlock;

    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lantlr/LLkAnalyzer;->currentBlock:Lantlr/AlternativeBlock;

    move-object/from16 v0, p1

    iget-boolean v3, v0, Lantlr/AlternativeBlock;->greedy:Z

    if-nez v3, :cond_1

    move-object/from16 v0, p1

    instance-of v3, v0, Lantlr/OneOrMoreBlock;

    if-nez v3, :cond_1

    move-object/from16 v0, p1

    instance-of v3, v0, Lantlr/ZeroOrMoreBlock;

    if-nez v3, :cond_1

    move-object/from16 v0, p0

    iget-object v3, v0, Lantlr/LLkAnalyzer;->tool:Lantlr/Tool;

    const-string v4, "Being nongreedy only makes sense for (...)+ and (...)*"

    move-object/from16 v0, p0

    iget-object v5, v0, Lantlr/LLkAnalyzer;->grammar:Lantlr/Grammar;

    invoke-virtual {v5}, Lantlr/Grammar;->getFilename()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {p1 .. p1}, Lantlr/AlternativeBlock;->getLine()I

    move-result v6

    invoke-virtual/range {p1 .. p1}, Lantlr/AlternativeBlock;->getColumn()I

    move-result v7

    invoke-virtual {v3, v4, v5, v6, v7}, Lantlr/Tool;->warning(Ljava/lang/String;Ljava/lang/String;II)V

    :cond_1
    const/4 v3, 0x1

    if-ne v11, v3, :cond_2

    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lantlr/AlternativeBlock;->getAlternativeAt(I)Lantlr/Alternative;

    move-result-object v2

    iget-object v2, v2, Lantlr/Alternative;->head:Lantlr/AlternativeElement;

    move-object/from16 v0, p0

    iget-object v3, v0, Lantlr/LLkAnalyzer;->currentBlock:Lantlr/AlternativeBlock;

    const/4 v4, 0x0

    iput v4, v3, Lantlr/AlternativeBlock;->alti:I

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lantlr/AlternativeBlock;->getAlternativeAt(I)Lantlr/Alternative;

    move-result-object v3

    iget-object v3, v3, Lantlr/Alternative;->cache:[Lantlr/Lookahead;

    const/4 v4, 0x1

    const/4 v5, 0x1

    invoke-virtual {v2, v5}, Lantlr/AlternativeElement;->look(I)Lantlr/Lookahead;

    move-result-object v2

    aput-object v2, v3, v4

    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lantlr/AlternativeBlock;->getAlternativeAt(I)Lantlr/Alternative;

    move-result-object v2

    const/4 v3, 0x1

    iput v3, v2, Lantlr/Alternative;->lookaheadDepth:I

    move-object/from16 v0, p0

    iput-object v12, v0, Lantlr/LLkAnalyzer;->currentBlock:Lantlr/AlternativeBlock;

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_2
    const/4 v8, 0x0

    :goto_1
    add-int/lit8 v3, v11, -0x1

    if-ge v8, v3, :cond_14

    move-object/from16 v0, p0

    iget-object v3, v0, Lantlr/LLkAnalyzer;->currentBlock:Lantlr/AlternativeBlock;

    iput v8, v3, Lantlr/AlternativeBlock;->alti:I

    move-object/from16 v0, p0

    iget-object v3, v0, Lantlr/LLkAnalyzer;->currentBlock:Lantlr/AlternativeBlock;

    iput v8, v3, Lantlr/AlternativeBlock;->analysisAlt:I

    move-object/from16 v0, p0

    iget-object v3, v0, Lantlr/LLkAnalyzer;->currentBlock:Lantlr/AlternativeBlock;

    add-int/lit8 v4, v8, 0x1

    iput v4, v3, Lantlr/AlternativeBlock;->altj:I

    add-int/lit8 v9, v8, 0x1

    :goto_2
    if-ge v9, v11, :cond_13

    move-object/from16 v0, p0

    iget-object v3, v0, Lantlr/LLkAnalyzer;->currentBlock:Lantlr/AlternativeBlock;

    iput v9, v3, Lantlr/AlternativeBlock;->altj:I

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lantlr/LLkAnalyzer;->DEBUG_ANALYZER:Z

    if-eqz v3, :cond_3

    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const-string v5, "comparing "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4, v8}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v5, " against alt "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4, v9}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lantlr/LLkAnalyzer;->currentBlock:Lantlr/AlternativeBlock;

    iput v9, v3, Lantlr/AlternativeBlock;->analysisAlt:I

    const/4 v3, 0x1

    move-object/from16 v0, p0

    iget-object v4, v0, Lantlr/LLkAnalyzer;->grammar:Lantlr/Grammar;

    iget v4, v4, Lantlr/Grammar;->maxk:I

    add-int/lit8 v4, v4, 0x1

    new-array v7, v4, [Lantlr/Lookahead;

    :cond_4
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lantlr/LLkAnalyzer;->DEBUG_ANALYZER:Z

    if-eqz v5, :cond_5

    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6}, Ljava/lang/StringBuffer;-><init>()V

    const-string v10, "checking depth "

    invoke-virtual {v6, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v6

    const-string v10, "<="

    invoke-virtual {v6, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v10, v0, Lantlr/LLkAnalyzer;->grammar:Lantlr/Grammar;

    iget v10, v10, Lantlr/Grammar;->maxk:I

    invoke-virtual {v6, v10}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_5
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v8, v3}, Lantlr/LLkAnalyzer;->getAltLookahead(Lantlr/AlternativeBlock;II)Lantlr/Lookahead;

    move-result-object v5

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v9, v3}, Lantlr/LLkAnalyzer;->getAltLookahead(Lantlr/AlternativeBlock;II)Lantlr/Lookahead;

    move-result-object v6

    move-object/from16 v0, p0

    iget-boolean v10, v0, Lantlr/LLkAnalyzer;->DEBUG_ANALYZER:Z

    if-eqz v10, :cond_6

    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v13, Ljava/lang/StringBuffer;

    invoke-direct {v13}, Ljava/lang/StringBuffer;-><init>()V

    const-string v14, "p is "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v13

    const-string v14, ","

    move-object/from16 v0, p0

    iget-object v15, v0, Lantlr/LLkAnalyzer;->charFormatter:Lantlr/CharFormatter;

    move-object/from16 v0, p0

    iget-object v0, v0, Lantlr/LLkAnalyzer;->grammar:Lantlr/Grammar;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v5, v14, v15, v0}, Lantlr/Lookahead;->toString(Ljava/lang/String;Lantlr/CharFormatter;Lantlr/Grammar;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v10, v13}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_6
    move-object/from16 v0, p0

    iget-boolean v10, v0, Lantlr/LLkAnalyzer;->DEBUG_ANALYZER:Z

    if-eqz v10, :cond_7

    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v13, Ljava/lang/StringBuffer;

    invoke-direct {v13}, Ljava/lang/StringBuffer;-><init>()V

    const-string v14, "q is "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v13

    const-string v14, ","

    move-object/from16 v0, p0

    iget-object v15, v0, Lantlr/LLkAnalyzer;->charFormatter:Lantlr/CharFormatter;

    move-object/from16 v0, p0

    iget-object v0, v0, Lantlr/LLkAnalyzer;->grammar:Lantlr/Grammar;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v6, v14, v15, v0}, Lantlr/Lookahead;->toString(Ljava/lang/String;Lantlr/CharFormatter;Lantlr/Grammar;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v10, v13}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_7
    invoke-virtual {v5, v6}, Lantlr/Lookahead;->intersection(Lantlr/Lookahead;)Lantlr/Lookahead;

    move-result-object v5

    aput-object v5, v7, v3

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lantlr/LLkAnalyzer;->DEBUG_ANALYZER:Z

    if-eqz v5, :cond_8

    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6}, Ljava/lang/StringBuffer;-><init>()V

    const-string v10, "intersection at depth "

    invoke-virtual {v6, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v6

    const-string v10, " is "

    invoke-virtual {v6, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    aget-object v10, v7, v3

    invoke-virtual {v10}, Lantlr/Lookahead;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v6, v10}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_8
    aget-object v5, v7, v3

    invoke-virtual {v5}, Lantlr/Lookahead;->nil()Z

    move-result v5

    if-nez v5, :cond_9

    const/4 v4, 0x1

    add-int/lit8 v3, v3, 0x1

    :cond_9
    if-eqz v4, :cond_a

    move-object/from16 v0, p0

    iget-object v5, v0, Lantlr/LLkAnalyzer;->grammar:Lantlr/Grammar;

    iget v5, v5, Lantlr/Grammar;->maxk:I

    if-le v3, v5, :cond_4

    :cond_a
    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Lantlr/AlternativeBlock;->getAlternativeAt(I)Lantlr/Alternative;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Lantlr/AlternativeBlock;->getAlternativeAt(I)Lantlr/Alternative;

    move-result-object v6

    if-eqz v4, :cond_12

    const/4 v10, 0x0

    const v2, 0x7fffffff

    iput v2, v5, Lantlr/Alternative;->lookaheadDepth:I

    const v2, 0x7fffffff

    iput v2, v6, Lantlr/Alternative;->lookaheadDepth:I

    iget-object v2, v5, Lantlr/Alternative;->synPred:Lantlr/SynPredBlock;

    if-eqz v2, :cond_b

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lantlr/LLkAnalyzer;->DEBUG_ANALYZER:Z

    if-eqz v2, :cond_15

    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string v4, "alt "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3, v8}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string v4, " has a syn pred"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    move v2, v10

    :goto_3
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_2

    :cond_b
    iget-object v2, v5, Lantlr/Alternative;->semPred:Ljava/lang/String;

    if-eqz v2, :cond_c

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lantlr/LLkAnalyzer;->DEBUG_ANALYZER:Z

    if-eqz v2, :cond_15

    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string v4, "alt "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3, v8}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string v4, " has a sem pred"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    move v2, v10

    goto :goto_3

    :cond_c
    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lantlr/LLkAnalyzer;->altUsesWildcardDefault(Lantlr/Alternative;)Z

    move-result v2

    if-eqz v2, :cond_d

    move v2, v10

    goto :goto_3

    :cond_d
    move-object/from16 v0, p1

    iget-boolean v2, v0, Lantlr/AlternativeBlock;->warnWhenFollowAmbig:Z

    if-nez v2, :cond_e

    iget-object v2, v5, Lantlr/Alternative;->head:Lantlr/AlternativeElement;

    instance-of v2, v2, Lantlr/BlockEndElement;

    if-nez v2, :cond_15

    iget-object v2, v6, Lantlr/Alternative;->head:Lantlr/AlternativeElement;

    instance-of v2, v2, Lantlr/BlockEndElement;

    if-eqz v2, :cond_e

    move v2, v10

    goto :goto_3

    :cond_e
    move-object/from16 v0, p1

    iget-boolean v2, v0, Lantlr/AlternativeBlock;->generateAmbigWarnings:Z

    if-nez v2, :cond_f

    move v2, v10

    goto :goto_3

    :cond_f
    move-object/from16 v0, p1

    iget-boolean v2, v0, Lantlr/AlternativeBlock;->greedySet:Z

    if-eqz v2, :cond_11

    move-object/from16 v0, p1

    iget-boolean v2, v0, Lantlr/AlternativeBlock;->greedy:Z

    if-eqz v2, :cond_11

    iget-object v2, v5, Lantlr/Alternative;->head:Lantlr/AlternativeElement;

    instance-of v2, v2, Lantlr/BlockEndElement;

    if-eqz v2, :cond_10

    iget-object v2, v6, Lantlr/Alternative;->head:Lantlr/AlternativeElement;

    instance-of v2, v2, Lantlr/BlockEndElement;

    if-eqz v2, :cond_15

    :cond_10
    iget-object v2, v6, Lantlr/Alternative;->head:Lantlr/AlternativeElement;

    instance-of v2, v2, Lantlr/BlockEndElement;

    if-eqz v2, :cond_11

    iget-object v2, v5, Lantlr/Alternative;->head:Lantlr/AlternativeElement;

    instance-of v2, v2, Lantlr/BlockEndElement;

    if-nez v2, :cond_11

    move v2, v10

    goto :goto_3

    :cond_11
    move-object/from16 v0, p0

    iget-object v2, v0, Lantlr/LLkAnalyzer;->tool:Lantlr/Tool;

    iget-object v2, v2, Lantlr/Tool;->errorHandler:Lantlr/ToolErrorHandler;

    move-object/from16 v0, p0

    iget-object v3, v0, Lantlr/LLkAnalyzer;->grammar:Lantlr/Grammar;

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lantlr/LLkAnalyzer;->lexicalAnalysis:Z

    move-object/from16 v0, p0

    iget-object v4, v0, Lantlr/LLkAnalyzer;->grammar:Lantlr/Grammar;

    iget v6, v4, Lantlr/Grammar;->maxk:I

    move-object/from16 v4, p1

    invoke-interface/range {v2 .. v9}, Lantlr/ToolErrorHandler;->warnAltAmbiguity(Lantlr/Grammar;Lantlr/AlternativeBlock;ZI[Lantlr/Lookahead;II)V

    move v2, v10

    goto/16 :goto_3

    :cond_12
    iget v4, v5, Lantlr/Alternative;->lookaheadDepth:I

    invoke-static {v4, v3}, Ljava/lang/Math;->max(II)I

    move-result v4

    iput v4, v5, Lantlr/Alternative;->lookaheadDepth:I

    iget v4, v6, Lantlr/Alternative;->lookaheadDepth:I

    invoke-static {v4, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    iput v3, v6, Lantlr/Alternative;->lookaheadDepth:I

    goto/16 :goto_3

    :cond_13
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_1

    :cond_14
    move-object/from16 v0, p0

    iput-object v12, v0, Lantlr/LLkAnalyzer;->currentBlock:Lantlr/AlternativeBlock;

    goto/16 :goto_0

    :cond_15
    move v2, v10

    goto/16 :goto_3
.end method

.method public deterministic(Lantlr/OneOrMoreBlock;)Z
    .locals 3

    iget-boolean v0, p0, Lantlr/LLkAnalyzer;->DEBUG_ANALYZER:Z

    if-eqz v0, :cond_0

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "deterministic(...)+("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lantlr/LLkAnalyzer;->currentBlock:Lantlr/AlternativeBlock;

    iput-object p1, p0, Lantlr/LLkAnalyzer;->currentBlock:Lantlr/AlternativeBlock;

    invoke-virtual {p0, p1}, Lantlr/LLkAnalyzer;->deterministic(Lantlr/AlternativeBlock;)Z

    move-result v1

    invoke-virtual {p0, p1}, Lantlr/LLkAnalyzer;->deterministicImpliedPath(Lantlr/BlockWithImpliedExitPath;)Z

    move-result v2

    iput-object v0, p0, Lantlr/LLkAnalyzer;->currentBlock:Lantlr/AlternativeBlock;

    if-eqz v2, :cond_1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public deterministic(Lantlr/ZeroOrMoreBlock;)Z
    .locals 3

    iget-boolean v0, p0, Lantlr/LLkAnalyzer;->DEBUG_ANALYZER:Z

    if-eqz v0, :cond_0

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "deterministic(...)*("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lantlr/LLkAnalyzer;->currentBlock:Lantlr/AlternativeBlock;

    iput-object p1, p0, Lantlr/LLkAnalyzer;->currentBlock:Lantlr/AlternativeBlock;

    invoke-virtual {p0, p1}, Lantlr/LLkAnalyzer;->deterministic(Lantlr/AlternativeBlock;)Z

    move-result v1

    invoke-virtual {p0, p1}, Lantlr/LLkAnalyzer;->deterministicImpliedPath(Lantlr/BlockWithImpliedExitPath;)Z

    move-result v2

    iput-object v0, p0, Lantlr/LLkAnalyzer;->currentBlock:Lantlr/AlternativeBlock;

    if-eqz v2, :cond_1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public deterministicImpliedPath(Lantlr/BlockWithImpliedExitPath;)Z
    .locals 14

    const/4 v1, 0x1

    invoke-virtual {p1}, Lantlr/BlockWithImpliedExitPath;->getAlternatives()Lantlr/collections/impl/Vector;

    move-result-object v0

    invoke-virtual {v0}, Lantlr/collections/impl/Vector;->size()I

    move-result v8

    iget-object v0, p0, Lantlr/LLkAnalyzer;->currentBlock:Lantlr/AlternativeBlock;

    const/4 v2, -0x1

    iput v2, v0, Lantlr/AlternativeBlock;->altj:I

    iget-boolean v0, p0, Lantlr/LLkAnalyzer;->DEBUG_ANALYZER:Z

    if-eqz v0, :cond_0

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "deterministicImpliedPath"

    invoke-virtual {v0, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    const/4 v6, 0x0

    :goto_0
    if-ge v6, v8, :cond_f

    invoke-virtual {p1, v6}, Lantlr/BlockWithImpliedExitPath;->getAlternativeAt(I)Lantlr/Alternative;

    move-result-object v3

    iget-object v0, v3, Lantlr/Alternative;->head:Lantlr/AlternativeElement;

    instance-of v0, v0, Lantlr/BlockEndElement;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lantlr/LLkAnalyzer;->tool:Lantlr/Tool;

    const-string v2, "empty alternative makes no sense in (...)* or (...)+"

    iget-object v4, p0, Lantlr/LLkAnalyzer;->grammar:Lantlr/Grammar;

    invoke-virtual {v4}, Lantlr/Grammar;->getFilename()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lantlr/BlockWithImpliedExitPath;->getLine()I

    move-result v5

    invoke-virtual {p1}, Lantlr/BlockWithImpliedExitPath;->getColumn()I

    move-result v7

    invoke-virtual {v0, v2, v4, v5, v7}, Lantlr/Tool;->warning(Ljava/lang/String;Ljava/lang/String;II)V

    :cond_1
    const/4 v0, 0x1

    iget-object v2, p0, Lantlr/LLkAnalyzer;->grammar:Lantlr/Grammar;

    iget v2, v2, Lantlr/Grammar;->maxk:I

    add-int/lit8 v2, v2, 0x1

    new-array v5, v2, [Lantlr/Lookahead;

    :cond_2
    const/4 v2, 0x0

    iget-boolean v4, p0, Lantlr/LLkAnalyzer;->DEBUG_ANALYZER:Z

    if-eqz v4, :cond_3

    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v7, Ljava/lang/StringBuffer;

    invoke-direct {v7}, Ljava/lang/StringBuffer;-><init>()V

    const-string v9, "checking depth "

    invoke-virtual {v7, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v7

    const-string v9, "<="

    invoke-virtual {v7, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    iget-object v9, p0, Lantlr/LLkAnalyzer;->grammar:Lantlr/Grammar;

    iget v9, v9, Lantlr/Grammar;->maxk:I

    invoke-virtual {v7, v9}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_3
    iget-object v4, p1, Lantlr/BlockWithImpliedExitPath;->next:Lantlr/AlternativeElement;

    invoke-virtual {v4, v0}, Lantlr/AlternativeElement;->look(I)Lantlr/Lookahead;

    move-result-object v4

    iget-object v7, p1, Lantlr/BlockWithImpliedExitPath;->exitCache:[Lantlr/Lookahead;

    aput-object v4, v7, v0

    iget-object v7, p0, Lantlr/LLkAnalyzer;->currentBlock:Lantlr/AlternativeBlock;

    iput v6, v7, Lantlr/AlternativeBlock;->alti:I

    invoke-direct {p0, p1, v6, v0}, Lantlr/LLkAnalyzer;->getAltLookahead(Lantlr/AlternativeBlock;II)Lantlr/Lookahead;

    move-result-object v7

    iget-boolean v9, p0, Lantlr/LLkAnalyzer;->DEBUG_ANALYZER:Z

    if-eqz v9, :cond_4

    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuffer;

    invoke-direct {v10}, Ljava/lang/StringBuffer;-><init>()V

    const-string v11, "follow is "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    const-string v11, ","

    iget-object v12, p0, Lantlr/LLkAnalyzer;->charFormatter:Lantlr/CharFormatter;

    iget-object v13, p0, Lantlr/LLkAnalyzer;->grammar:Lantlr/Grammar;

    invoke-virtual {v4, v11, v12, v13}, Lantlr/Lookahead;->toString(Ljava/lang/String;Lantlr/CharFormatter;Lantlr/Grammar;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_4
    iget-boolean v9, p0, Lantlr/LLkAnalyzer;->DEBUG_ANALYZER:Z

    if-eqz v9, :cond_5

    sget-object v9, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v10, Ljava/lang/StringBuffer;

    invoke-direct {v10}, Ljava/lang/StringBuffer;-><init>()V

    const-string v11, "p is "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    const-string v11, ","

    iget-object v12, p0, Lantlr/LLkAnalyzer;->charFormatter:Lantlr/CharFormatter;

    iget-object v13, p0, Lantlr/LLkAnalyzer;->grammar:Lantlr/Grammar;

    invoke-virtual {v7, v11, v12, v13}, Lantlr/Lookahead;->toString(Ljava/lang/String;Lantlr/CharFormatter;Lantlr/Grammar;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_5
    invoke-virtual {v4, v7}, Lantlr/Lookahead;->intersection(Lantlr/Lookahead;)Lantlr/Lookahead;

    move-result-object v4

    aput-object v4, v5, v0

    iget-boolean v4, p0, Lantlr/LLkAnalyzer;->DEBUG_ANALYZER:Z

    if-eqz v4, :cond_6

    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v7, Ljava/lang/StringBuffer;

    invoke-direct {v7}, Ljava/lang/StringBuffer;-><init>()V

    const-string v9, "intersection at depth "

    invoke-virtual {v7, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v7

    const-string v9, " is "

    invoke-virtual {v7, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    aget-object v9, v5, v0

    invoke-virtual {v7, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_6
    aget-object v4, v5, v0

    invoke-virtual {v4}, Lantlr/Lookahead;->nil()Z

    move-result v4

    if-nez v4, :cond_7

    const/4 v2, 0x1

    add-int/lit8 v0, v0, 0x1

    :cond_7
    if-eqz v2, :cond_8

    iget-object v4, p0, Lantlr/LLkAnalyzer;->grammar:Lantlr/Grammar;

    iget v4, v4, Lantlr/Grammar;->maxk:I

    if-le v0, v4, :cond_2

    :cond_8
    if-eqz v2, :cond_e

    const/4 v7, 0x0

    const v0, 0x7fffffff

    iput v0, v3, Lantlr/Alternative;->lookaheadDepth:I

    const v0, 0x7fffffff

    iput v0, p1, Lantlr/BlockWithImpliedExitPath;->exitLookaheadDepth:I

    iget-object v0, p0, Lantlr/LLkAnalyzer;->currentBlock:Lantlr/AlternativeBlock;

    iget v0, v0, Lantlr/AlternativeBlock;->alti:I

    invoke-virtual {p1, v0}, Lantlr/BlockWithImpliedExitPath;->getAlternativeAt(I)Lantlr/Alternative;

    move-result-object v0

    iget-boolean v1, p1, Lantlr/BlockWithImpliedExitPath;->warnWhenFollowAmbig:Z

    if-nez v1, :cond_9

    move v0, v7

    :goto_1
    add-int/lit8 v6, v6, 0x1

    move v1, v0

    goto/16 :goto_0

    :cond_9
    iget-boolean v1, p1, Lantlr/BlockWithImpliedExitPath;->generateAmbigWarnings:Z

    if-nez v1, :cond_a

    move v0, v7

    goto :goto_1

    :cond_a
    iget-boolean v1, p1, Lantlr/BlockWithImpliedExitPath;->greedy:Z

    const/4 v2, 0x1

    if-ne v1, v2, :cond_b

    iget-boolean v1, p1, Lantlr/BlockWithImpliedExitPath;->greedySet:Z

    if-eqz v1, :cond_b

    iget-object v1, v0, Lantlr/Alternative;->head:Lantlr/AlternativeElement;

    instance-of v1, v1, Lantlr/BlockEndElement;

    if-nez v1, :cond_b

    iget-boolean v0, p0, Lantlr/LLkAnalyzer;->DEBUG_ANALYZER:Z

    if-eqz v0, :cond_10

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "greedy loop"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    move v0, v7

    goto :goto_1

    :cond_b
    iget-boolean v1, p1, Lantlr/BlockWithImpliedExitPath;->greedy:Z

    if-nez v1, :cond_d

    iget-object v0, v0, Lantlr/Alternative;->head:Lantlr/AlternativeElement;

    instance-of v0, v0, Lantlr/BlockEndElement;

    if-nez v0, :cond_d

    iget-boolean v0, p0, Lantlr/LLkAnalyzer;->DEBUG_ANALYZER:Z

    if-eqz v0, :cond_c

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "nongreedy loop"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_c
    iget-object v0, p1, Lantlr/BlockWithImpliedExitPath;->exitCache:[Lantlr/Lookahead;

    iget-object v1, p0, Lantlr/LLkAnalyzer;->grammar:Lantlr/Grammar;

    iget v1, v1, Lantlr/Grammar;->maxk:I

    invoke-static {v0, v1}, Lantlr/LLkAnalyzer;->lookaheadEquivForApproxAndFullAnalysis([Lantlr/Lookahead;I)Z

    move-result v0

    if-nez v0, :cond_10

    iget-object v0, p0, Lantlr/LLkAnalyzer;->tool:Lantlr/Tool;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "nongreedy block may exit incorrectly due"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "\tto limitations of linear approximate lookahead (first k-1 sets"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "\tin lookahead not singleton)."

    aput-object v3, v1, v2

    iget-object v2, p0, Lantlr/LLkAnalyzer;->grammar:Lantlr/Grammar;

    invoke-virtual {v2}, Lantlr/Grammar;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lantlr/BlockWithImpliedExitPath;->getLine()I

    move-result v3

    invoke-virtual {p1}, Lantlr/BlockWithImpliedExitPath;->getColumn()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lantlr/Tool;->warning([Ljava/lang/String;Ljava/lang/String;II)V

    move v0, v7

    goto :goto_1

    :cond_d
    iget-object v0, p0, Lantlr/LLkAnalyzer;->tool:Lantlr/Tool;

    iget-object v0, v0, Lantlr/Tool;->errorHandler:Lantlr/ToolErrorHandler;

    iget-object v1, p0, Lantlr/LLkAnalyzer;->grammar:Lantlr/Grammar;

    iget-boolean v3, p0, Lantlr/LLkAnalyzer;->lexicalAnalysis:Z

    iget-object v2, p0, Lantlr/LLkAnalyzer;->grammar:Lantlr/Grammar;

    iget v4, v2, Lantlr/Grammar;->maxk:I

    move-object v2, p1

    invoke-interface/range {v0 .. v6}, Lantlr/ToolErrorHandler;->warnAltExitAmbiguity(Lantlr/Grammar;Lantlr/BlockWithImpliedExitPath;ZI[Lantlr/Lookahead;I)V

    move v0, v7

    goto :goto_1

    :cond_e
    iget v2, v3, Lantlr/Alternative;->lookaheadDepth:I

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v2

    iput v2, v3, Lantlr/Alternative;->lookaheadDepth:I

    iget v2, p1, Lantlr/BlockWithImpliedExitPath;->exitLookaheadDepth:I

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p1, Lantlr/BlockWithImpliedExitPath;->exitLookaheadDepth:I

    move v0, v1

    goto/16 :goto_1

    :cond_f
    return v1

    :cond_10
    move v0, v7

    goto/16 :goto_1
.end method

.method public look(ILantlr/ActionElement;)Lantlr/Lookahead;
    .locals 3

    iget-boolean v0, p0, Lantlr/LLkAnalyzer;->DEBUG_ANALYZER:Z

    if-eqz v0, :cond_0

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "lookAction("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p2, Lantlr/ActionElement;->next:Lantlr/AlternativeElement;

    invoke-virtual {v0, p1}, Lantlr/AlternativeElement;->look(I)Lantlr/Lookahead;

    move-result-object v0

    return-object v0
.end method

.method public look(ILantlr/AlternativeBlock;)Lantlr/Lookahead;
    .locals 8

    const/4 v1, 0x0

    iget-boolean v0, p0, Lantlr/LLkAnalyzer;->DEBUG_ANALYZER:Z

    if-eqz v0, :cond_0

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "lookAltBlk("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    iget-object v2, p0, Lantlr/LLkAnalyzer;->currentBlock:Lantlr/AlternativeBlock;

    iput-object p2, p0, Lantlr/LLkAnalyzer;->currentBlock:Lantlr/AlternativeBlock;

    new-instance v3, Lantlr/Lookahead;

    invoke-direct {v3}, Lantlr/Lookahead;-><init>()V

    move v0, v1

    :goto_0
    iget-object v4, p2, Lantlr/AlternativeBlock;->alternatives:Lantlr/collections/impl/Vector;

    invoke-virtual {v4}, Lantlr/collections/impl/Vector;->size()I

    move-result v4

    if-ge v0, v4, :cond_3

    iget-boolean v4, p0, Lantlr/LLkAnalyzer;->DEBUG_ANALYZER:Z

    if-eqz v4, :cond_1

    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    const-string v6, "alt "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string v6, " of "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_1
    iget-object v4, p0, Lantlr/LLkAnalyzer;->currentBlock:Lantlr/AlternativeBlock;

    iput v0, v4, Lantlr/AlternativeBlock;->analysisAlt:I

    invoke-virtual {p2, v0}, Lantlr/AlternativeBlock;->getAlternativeAt(I)Lantlr/Alternative;

    move-result-object v4

    iget-object v5, v4, Lantlr/Alternative;->head:Lantlr/AlternativeElement;

    iget-boolean v6, p0, Lantlr/LLkAnalyzer;->DEBUG_ANALYZER:Z

    if-eqz v6, :cond_2

    iget-object v6, v4, Lantlr/Alternative;->head:Lantlr/AlternativeElement;

    iget-object v4, v4, Lantlr/Alternative;->tail:Lantlr/AlternativeElement;

    if-ne v6, v4, :cond_2

    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6}, Ljava/lang/StringBuffer;-><init>()V

    const-string v7, "alt "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v6

    const-string v7, " is empty"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v5, p1}, Lantlr/AlternativeElement;->look(I)Lantlr/Lookahead;

    move-result-object v4

    invoke-virtual {v3, v4}, Lantlr/Lookahead;->combineWith(Lantlr/Lookahead;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    const/4 v0, 0x1

    if-ne p1, v0, :cond_5

    iget-boolean v0, p2, Lantlr/AlternativeBlock;->not:Z

    if-eqz v0, :cond_5

    iget-boolean v0, p0, Lantlr/LLkAnalyzer;->lexicalAnalysis:Z

    invoke-virtual {p0, p2, v0}, Lantlr/LLkAnalyzer;->subruleCanBeInverted(Lantlr/AlternativeBlock;Z)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-boolean v0, p0, Lantlr/LLkAnalyzer;->lexicalAnalysis:Z

    if-eqz v0, :cond_6

    iget-object v0, p0, Lantlr/LLkAnalyzer;->grammar:Lantlr/Grammar;

    check-cast v0, Lantlr/LexerGrammar;

    iget-object v0, v0, Lantlr/LexerGrammar;->charVocabulary:Lantlr/collections/impl/BitSet;

    invoke-virtual {v0}, Lantlr/collections/impl/BitSet;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lantlr/collections/impl/BitSet;

    iget-object v4, v3, Lantlr/Lookahead;->fset:Lantlr/collections/impl/BitSet;

    invoke-virtual {v4}, Lantlr/collections/impl/BitSet;->toArray()[I

    move-result-object v4

    :goto_1
    array-length v5, v4

    if-ge v1, v5, :cond_4

    aget v5, v4, v1

    invoke-virtual {v0, v5}, Lantlr/collections/impl/BitSet;->remove(I)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_4
    iput-object v0, v3, Lantlr/Lookahead;->fset:Lantlr/collections/impl/BitSet;

    :cond_5
    :goto_2
    iput-object v2, p0, Lantlr/LLkAnalyzer;->currentBlock:Lantlr/AlternativeBlock;

    return-object v3

    :cond_6
    iget-object v0, v3, Lantlr/Lookahead;->fset:Lantlr/collections/impl/BitSet;

    const/4 v1, 0x4

    iget-object v4, p0, Lantlr/LLkAnalyzer;->grammar:Lantlr/Grammar;

    iget-object v4, v4, Lantlr/Grammar;->tokenManager:Lantlr/TokenManager;

    invoke-interface {v4}, Lantlr/TokenManager;->maxTokenType()I

    move-result v4

    invoke-virtual {v0, v1, v4}, Lantlr/collections/impl/BitSet;->notInPlace(II)V

    goto :goto_2
.end method

.method public look(ILantlr/BlockEndElement;)Lantlr/Lookahead;
    .locals 3

    iget-boolean v0, p0, Lantlr/LLkAnalyzer;->DEBUG_ANALYZER:Z

    if-eqz v0, :cond_0

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "lookBlockEnd("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p2, Lantlr/BlockEndElement;->block:Lantlr/AlternativeBlock;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "); lock is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p2, Lantlr/BlockEndElement;->lock:[Z

    aget-boolean v2, v2, p1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p2, Lantlr/BlockEndElement;->lock:[Z

    aget-boolean v0, v0, p1

    if-eqz v0, :cond_1

    new-instance v0, Lantlr/Lookahead;

    invoke-direct {v0}, Lantlr/Lookahead;-><init>()V

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p2, Lantlr/BlockEndElement;->block:Lantlr/AlternativeBlock;

    instance-of v0, v0, Lantlr/ZeroOrMoreBlock;

    if-nez v0, :cond_2

    iget-object v0, p2, Lantlr/BlockEndElement;->block:Lantlr/AlternativeBlock;

    instance-of v0, v0, Lantlr/OneOrMoreBlock;

    if-eqz v0, :cond_3

    :cond_2
    iget-object v0, p2, Lantlr/BlockEndElement;->lock:[Z

    const/4 v1, 0x1

    aput-boolean v1, v0, p1

    iget-object v0, p2, Lantlr/BlockEndElement;->block:Lantlr/AlternativeBlock;

    invoke-virtual {p0, p1, v0}, Lantlr/LLkAnalyzer;->look(ILantlr/AlternativeBlock;)Lantlr/Lookahead;

    move-result-object v0

    iget-object v1, p2, Lantlr/BlockEndElement;->lock:[Z

    const/4 v2, 0x0

    aput-boolean v2, v1, p1

    :goto_1
    iget-object v1, p2, Lantlr/BlockEndElement;->block:Lantlr/AlternativeBlock;

    instance-of v1, v1, Lantlr/TreeElement;

    if-eqz v1, :cond_4

    const/4 v1, 0x3

    invoke-static {v1}, Lantlr/Lookahead;->of(I)Lantlr/Lookahead;

    move-result-object v1

    invoke-virtual {v0, v1}, Lantlr/Lookahead;->combineWith(Lantlr/Lookahead;)V

    goto :goto_0

    :cond_3
    new-instance v0, Lantlr/Lookahead;

    invoke-direct {v0}, Lantlr/Lookahead;-><init>()V

    goto :goto_1

    :cond_4
    iget-object v1, p2, Lantlr/BlockEndElement;->block:Lantlr/AlternativeBlock;

    instance-of v1, v1, Lantlr/SynPredBlock;

    if-eqz v1, :cond_5

    invoke-virtual {v0}, Lantlr/Lookahead;->setEpsilon()V

    goto :goto_0

    :cond_5
    iget-object v1, p2, Lantlr/BlockEndElement;->block:Lantlr/AlternativeBlock;

    iget-object v1, v1, Lantlr/AlternativeBlock;->next:Lantlr/AlternativeElement;

    invoke-virtual {v1, p1}, Lantlr/AlternativeElement;->look(I)Lantlr/Lookahead;

    move-result-object v1

    invoke-virtual {v0, v1}, Lantlr/Lookahead;->combineWith(Lantlr/Lookahead;)V

    goto :goto_0
.end method

.method public look(ILantlr/CharLiteralElement;)Lantlr/Lookahead;
    .locals 4

    iget-boolean v0, p0, Lantlr/LLkAnalyzer;->DEBUG_ANALYZER:Z

    if-eqz v0, :cond_0

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "lookCharLiteral("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    const/4 v0, 0x1

    if-le p1, v0, :cond_1

    iget-object v0, p2, Lantlr/CharLiteralElement;->next:Lantlr/AlternativeElement;

    add-int/lit8 v1, p1, -0x1

    invoke-virtual {v0, v1}, Lantlr/AlternativeElement;->look(I)Lantlr/Lookahead;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_1
    iget-boolean v0, p0, Lantlr/LLkAnalyzer;->lexicalAnalysis:Z

    if-eqz v0, :cond_5

    iget-boolean v0, p2, Lantlr/CharLiteralElement;->not:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lantlr/LLkAnalyzer;->grammar:Lantlr/Grammar;

    check-cast v0, Lantlr/LexerGrammar;

    iget-object v0, v0, Lantlr/LexerGrammar;->charVocabulary:Lantlr/collections/impl/BitSet;

    invoke-virtual {v0}, Lantlr/collections/impl/BitSet;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lantlr/collections/impl/BitSet;

    iget-boolean v1, p0, Lantlr/LLkAnalyzer;->DEBUG_ANALYZER:Z

    if-eqz v1, :cond_2

    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "charVocab is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v0}, Lantlr/collections/impl/BitSet;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_2
    invoke-direct {p0, v0, p2}, Lantlr/LLkAnalyzer;->removeCompetingPredictionSets(Lantlr/collections/impl/BitSet;Lantlr/AlternativeElement;)V

    iget-boolean v1, p0, Lantlr/LLkAnalyzer;->DEBUG_ANALYZER:Z

    if-eqz v1, :cond_3

    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "charVocab after removal of prior alt lookahead "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v0}, Lantlr/collections/impl/BitSet;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_3
    invoke-virtual {p2}, Lantlr/CharLiteralElement;->getType()I

    move-result v1

    invoke-virtual {v0, v1}, Lantlr/collections/impl/BitSet;->clear(I)V

    new-instance v1, Lantlr/Lookahead;

    invoke-direct {v1, v0}, Lantlr/Lookahead;-><init>(Lantlr/collections/impl/BitSet;)V

    move-object v0, v1

    goto :goto_0

    :cond_4
    invoke-virtual {p2}, Lantlr/CharLiteralElement;->getType()I

    move-result v0

    invoke-static {v0}, Lantlr/Lookahead;->of(I)Lantlr/Lookahead;

    move-result-object v0

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lantlr/LLkAnalyzer;->tool:Lantlr/Tool;

    const-string v1, "Character literal reference found in parser"

    invoke-virtual {v0, v1}, Lantlr/Tool;->panic(Ljava/lang/String;)V

    invoke-virtual {p2}, Lantlr/CharLiteralElement;->getType()I

    move-result v0

    invoke-static {v0}, Lantlr/Lookahead;->of(I)Lantlr/Lookahead;

    move-result-object v0

    goto :goto_0
.end method

.method public look(ILantlr/CharRangeElement;)Lantlr/Lookahead;
    .locals 3

    iget-boolean v0, p0, Lantlr/LLkAnalyzer;->DEBUG_ANALYZER:Z

    if-eqz v0, :cond_0

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "lookCharRange("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    const/4 v0, 0x1

    if-le p1, v0, :cond_1

    iget-object v0, p2, Lantlr/CharRangeElement;->next:Lantlr/AlternativeElement;

    add-int/lit8 v1, p1, -0x1

    invoke-virtual {v0, v1}, Lantlr/AlternativeElement;->look(I)Lantlr/Lookahead;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_1
    iget-char v0, p2, Lantlr/CharRangeElement;->begin:C

    invoke-static {v0}, Lantlr/collections/impl/BitSet;->of(I)Lantlr/collections/impl/BitSet;

    move-result-object v1

    iget-char v0, p2, Lantlr/CharRangeElement;->begin:C

    add-int/lit8 v0, v0, 0x1

    :goto_1
    iget-char v2, p2, Lantlr/CharRangeElement;->end:C

    if-gt v0, v2, :cond_2

    invoke-virtual {v1, v0}, Lantlr/collections/impl/BitSet;->add(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    new-instance v0, Lantlr/Lookahead;

    invoke-direct {v0, v1}, Lantlr/Lookahead;-><init>(Lantlr/collections/impl/BitSet;)V

    goto :goto_0
.end method

.method public look(ILantlr/GrammarAtom;)Lantlr/Lookahead;
    .locals 4

    iget-boolean v0, p0, Lantlr/LLkAnalyzer;->DEBUG_ANALYZER:Z

    if-eqz v0, :cond_0

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "look("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p2}, Lantlr/GrammarAtom;->getType()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "])"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    iget-boolean v0, p0, Lantlr/LLkAnalyzer;->lexicalAnalysis:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lantlr/LLkAnalyzer;->tool:Lantlr/Tool;

    const-string v1, "token reference found in lexer"

    invoke-virtual {v0, v1}, Lantlr/Tool;->panic(Ljava/lang/String;)V

    :cond_1
    const/4 v0, 0x1

    if-le p1, v0, :cond_3

    iget-object v0, p2, Lantlr/GrammarAtom;->next:Lantlr/AlternativeElement;

    add-int/lit8 v1, p1, -0x1

    invoke-virtual {v0, v1}, Lantlr/AlternativeElement;->look(I)Lantlr/Lookahead;

    move-result-object v0

    :cond_2
    :goto_0
    return-object v0

    :cond_3
    invoke-virtual {p2}, Lantlr/GrammarAtom;->getType()I

    move-result v0

    invoke-static {v0}, Lantlr/Lookahead;->of(I)Lantlr/Lookahead;

    move-result-object v0

    iget-boolean v1, p2, Lantlr/GrammarAtom;->not:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lantlr/LLkAnalyzer;->grammar:Lantlr/Grammar;

    iget-object v1, v1, Lantlr/Grammar;->tokenManager:Lantlr/TokenManager;

    invoke-interface {v1}, Lantlr/TokenManager;->maxTokenType()I

    move-result v1

    iget-object v2, v0, Lantlr/Lookahead;->fset:Lantlr/collections/impl/BitSet;

    const/4 v3, 0x4

    invoke-virtual {v2, v3, v1}, Lantlr/collections/impl/BitSet;->notInPlace(II)V

    iget-object v1, v0, Lantlr/Lookahead;->fset:Lantlr/collections/impl/BitSet;

    invoke-direct {p0, v1, p2}, Lantlr/LLkAnalyzer;->removeCompetingPredictionSets(Lantlr/collections/impl/BitSet;Lantlr/AlternativeElement;)V

    goto :goto_0
.end method

.method public look(ILantlr/OneOrMoreBlock;)Lantlr/Lookahead;
    .locals 3

    iget-boolean v0, p0, Lantlr/LLkAnalyzer;->DEBUG_ANALYZER:Z

    if-eqz v0, :cond_0

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "look+"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p0, p1, p2}, Lantlr/LLkAnalyzer;->look(ILantlr/AlternativeBlock;)Lantlr/Lookahead;

    move-result-object v0

    return-object v0
.end method

.method public look(ILantlr/RuleBlock;)Lantlr/Lookahead;
    .locals 3

    iget-boolean v0, p0, Lantlr/LLkAnalyzer;->DEBUG_ANALYZER:Z

    if-eqz v0, :cond_0

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "lookRuleBlk("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p0, p1, p2}, Lantlr/LLkAnalyzer;->look(ILantlr/AlternativeBlock;)Lantlr/Lookahead;

    move-result-object v0

    return-object v0
.end method

.method public look(ILantlr/RuleEndElement;)Lantlr/Lookahead;
    .locals 3

    iget-boolean v0, p0, Lantlr/LLkAnalyzer;->DEBUG_ANALYZER:Z

    if-eqz v0, :cond_0

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "lookRuleBlockEnd("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "); noFOLLOW="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-boolean v2, p2, Lantlr/RuleEndElement;->noFOLLOW:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "; lock is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p2, Lantlr/RuleEndElement;->lock:[Z

    aget-boolean v2, v2, p1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    iget-boolean v0, p2, Lantlr/RuleEndElement;->noFOLLOW:Z

    if-eqz v0, :cond_1

    new-instance v0, Lantlr/Lookahead;

    invoke-direct {v0}, Lantlr/Lookahead;-><init>()V

    invoke-virtual {v0}, Lantlr/Lookahead;->setEpsilon()V

    invoke-static {p1}, Lantlr/collections/impl/BitSet;->of(I)Lantlr/collections/impl/BitSet;

    move-result-object v1

    iput-object v1, v0, Lantlr/Lookahead;->epsilonDepth:Lantlr/collections/impl/BitSet;

    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0, p1, p2}, Lantlr/LLkAnalyzer;->FOLLOW(ILantlr/RuleEndElement;)Lantlr/Lookahead;

    move-result-object v0

    goto :goto_0
.end method

.method public look(ILantlr/RuleRefElement;)Lantlr/Lookahead;
    .locals 6

    iget-boolean v0, p0, Lantlr/LLkAnalyzer;->DEBUG_ANALYZER:Z

    if-eqz v0, :cond_0

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "lookRuleRef("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lantlr/LLkAnalyzer;->grammar:Lantlr/Grammar;

    iget-object v1, p2, Lantlr/RuleRefElement;->targetRule:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lantlr/Grammar;->getSymbol(Ljava/lang/String;)Lantlr/GrammarSymbol;

    move-result-object v0

    check-cast v0, Lantlr/RuleSymbol;

    if-eqz v0, :cond_1

    iget-boolean v1, v0, Lantlr/RuleSymbol;->defined:Z

    if-nez v1, :cond_2

    :cond_1
    iget-object v0, p0, Lantlr/LLkAnalyzer;->tool:Lantlr/Tool;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "no definition of rule "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p2, Lantlr/RuleRefElement;->targetRule:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lantlr/LLkAnalyzer;->grammar:Lantlr/Grammar;

    invoke-virtual {v2}, Lantlr/Grammar;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Lantlr/RuleRefElement;->getLine()I

    move-result v3

    invoke-virtual {p2}, Lantlr/RuleRefElement;->getColumn()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lantlr/Tool;->error(Ljava/lang/String;Ljava/lang/String;II)V

    new-instance v0, Lantlr/Lookahead;

    invoke-direct {v0}, Lantlr/Lookahead;-><init>()V

    :goto_0
    return-object v0

    :cond_2
    invoke-virtual {v0}, Lantlr/RuleSymbol;->getBlock()Lantlr/RuleBlock;

    move-result-object v0

    iget-object v0, v0, Lantlr/RuleBlock;->endNode:Lantlr/RuleEndElement;

    iget-boolean v2, v0, Lantlr/RuleEndElement;->noFOLLOW:Z

    const/4 v1, 0x1

    iput-boolean v1, v0, Lantlr/RuleEndElement;->noFOLLOW:Z

    iget-object v1, p2, Lantlr/RuleRefElement;->targetRule:Ljava/lang/String;

    invoke-virtual {p0, p1, v1}, Lantlr/LLkAnalyzer;->look(ILjava/lang/String;)Lantlr/Lookahead;

    move-result-object v1

    iget-boolean v3, p0, Lantlr/LLkAnalyzer;->DEBUG_ANALYZER:Z

    if-eqz v3, :cond_3

    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const-string v5, "back from rule ref to "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    iget-object v5, p2, Lantlr/RuleRefElement;->targetRule:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_3
    iput-boolean v2, v0, Lantlr/RuleEndElement;->noFOLLOW:Z

    iget-object v0, v1, Lantlr/Lookahead;->cycle:Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lantlr/LLkAnalyzer;->tool:Lantlr/Tool;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "infinite recursion to rule "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    iget-object v3, v1, Lantlr/Lookahead;->cycle:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, " from rule "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    iget-object v3, p2, Lantlr/RuleRefElement;->enclosingRuleName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lantlr/LLkAnalyzer;->grammar:Lantlr/Grammar;

    invoke-virtual {v3}, Lantlr/Grammar;->getFilename()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, Lantlr/RuleRefElement;->getLine()I

    move-result v4

    invoke-virtual {p2}, Lantlr/RuleRefElement;->getColumn()I

    move-result v5

    invoke-virtual {v0, v2, v3, v4, v5}, Lantlr/Tool;->error(Ljava/lang/String;Ljava/lang/String;II)V

    :cond_4
    invoke-virtual {v1}, Lantlr/Lookahead;->containsEpsilon()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-boolean v0, p0, Lantlr/LLkAnalyzer;->DEBUG_ANALYZER:Z

    if-eqz v0, :cond_5

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "rule ref to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    iget-object v3, p2, Lantlr/RuleRefElement;->targetRule:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, " has eps, depth: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    iget-object v3, v1, Lantlr/Lookahead;->epsilonDepth:Lantlr/collections/impl/BitSet;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_5
    invoke-virtual {v1}, Lantlr/Lookahead;->resetEpsilon()V

    iget-object v0, v1, Lantlr/Lookahead;->epsilonDepth:Lantlr/collections/impl/BitSet;

    invoke-virtual {v0}, Lantlr/collections/impl/BitSet;->toArray()[I

    move-result-object v2

    const/4 v0, 0x0

    iput-object v0, v1, Lantlr/Lookahead;->epsilonDepth:Lantlr/collections/impl/BitSet;

    const/4 v0, 0x0

    :goto_1
    array-length v3, v2

    if-ge v0, v3, :cond_6

    aget v3, v2, v0

    sub-int v3, p1, v3

    sub-int v3, p1, v3

    iget-object v4, p2, Lantlr/RuleRefElement;->next:Lantlr/AlternativeElement;

    invoke-virtual {v4, v3}, Lantlr/AlternativeElement;->look(I)Lantlr/Lookahead;

    move-result-object v3

    invoke-virtual {v1, v3}, Lantlr/Lookahead;->combineWith(Lantlr/Lookahead;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_6
    move-object v0, v1

    goto/16 :goto_0
.end method

.method public look(ILantlr/StringLiteralElement;)Lantlr/Lookahead;
    .locals 4

    iget-boolean v0, p0, Lantlr/LLkAnalyzer;->DEBUG_ANALYZER:Z

    if-eqz v0, :cond_0

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "lookStringLiteral("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    iget-boolean v0, p0, Lantlr/LLkAnalyzer;->lexicalAnalysis:Z

    if-eqz v0, :cond_3

    iget-object v0, p2, Lantlr/StringLiteralElement;->processedAtomText:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-le p1, v0, :cond_2

    iget-object v0, p2, Lantlr/StringLiteralElement;->next:Lantlr/AlternativeElement;

    iget-object v1, p2, Lantlr/StringLiteralElement;->processedAtomText:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    sub-int v1, p1, v1

    invoke-virtual {v0, v1}, Lantlr/AlternativeElement;->look(I)Lantlr/Lookahead;

    move-result-object v0

    :cond_1
    :goto_0
    return-object v0

    :cond_2
    iget-object v0, p2, Lantlr/StringLiteralElement;->processedAtomText:Ljava/lang/String;

    add-int/lit8 v1, p1, -0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-static {v0}, Lantlr/Lookahead;->of(I)Lantlr/Lookahead;

    move-result-object v0

    goto :goto_0

    :cond_3
    const/4 v0, 0x1

    if-le p1, v0, :cond_4

    iget-object v0, p2, Lantlr/StringLiteralElement;->next:Lantlr/AlternativeElement;

    add-int/lit8 v1, p1, -0x1

    invoke-virtual {v0, v1}, Lantlr/AlternativeElement;->look(I)Lantlr/Lookahead;

    move-result-object v0

    goto :goto_0

    :cond_4
    invoke-virtual {p2}, Lantlr/StringLiteralElement;->getType()I

    move-result v0

    invoke-static {v0}, Lantlr/Lookahead;->of(I)Lantlr/Lookahead;

    move-result-object v0

    iget-boolean v1, p2, Lantlr/StringLiteralElement;->not:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lantlr/LLkAnalyzer;->grammar:Lantlr/Grammar;

    iget-object v1, v1, Lantlr/Grammar;->tokenManager:Lantlr/TokenManager;

    invoke-interface {v1}, Lantlr/TokenManager;->maxTokenType()I

    move-result v1

    iget-object v2, v0, Lantlr/Lookahead;->fset:Lantlr/collections/impl/BitSet;

    const/4 v3, 0x4

    invoke-virtual {v2, v3, v1}, Lantlr/collections/impl/BitSet;->notInPlace(II)V

    goto :goto_0
.end method

.method public look(ILantlr/SynPredBlock;)Lantlr/Lookahead;
    .locals 3

    iget-boolean v0, p0, Lantlr/LLkAnalyzer;->DEBUG_ANALYZER:Z

    if-eqz v0, :cond_0

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "look=>("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p2, Lantlr/SynPredBlock;->next:Lantlr/AlternativeElement;

    invoke-virtual {v0, p1}, Lantlr/AlternativeElement;->look(I)Lantlr/Lookahead;

    move-result-object v0

    return-object v0
.end method

.method public look(ILantlr/TokenRangeElement;)Lantlr/Lookahead;
    .locals 3

    iget-boolean v0, p0, Lantlr/LLkAnalyzer;->DEBUG_ANALYZER:Z

    if-eqz v0, :cond_0

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "lookTokenRange("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    const/4 v0, 0x1

    if-le p1, v0, :cond_1

    iget-object v0, p2, Lantlr/TokenRangeElement;->next:Lantlr/AlternativeElement;

    add-int/lit8 v1, p1, -0x1

    invoke-virtual {v0, v1}, Lantlr/AlternativeElement;->look(I)Lantlr/Lookahead;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_1
    iget v0, p2, Lantlr/TokenRangeElement;->begin:I

    invoke-static {v0}, Lantlr/collections/impl/BitSet;->of(I)Lantlr/collections/impl/BitSet;

    move-result-object v1

    iget v0, p2, Lantlr/TokenRangeElement;->begin:I

    add-int/lit8 v0, v0, 0x1

    :goto_1
    iget v2, p2, Lantlr/TokenRangeElement;->end:I

    if-gt v0, v2, :cond_2

    invoke-virtual {v1, v0}, Lantlr/collections/impl/BitSet;->add(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    new-instance v0, Lantlr/Lookahead;

    invoke-direct {v0, v1}, Lantlr/Lookahead;-><init>(Lantlr/collections/impl/BitSet;)V

    goto :goto_0
.end method

.method public look(ILantlr/TreeElement;)Lantlr/Lookahead;
    .locals 4

    const/4 v3, 0x1

    iget-boolean v0, p0, Lantlr/LLkAnalyzer;->DEBUG_ANALYZER:Z

    if-eqz v0, :cond_0

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "look("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p2, Lantlr/TreeElement;->root:Lantlr/GrammarAtom;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p2, Lantlr/TreeElement;->root:Lantlr/GrammarAtom;

    invoke-virtual {v2}, Lantlr/GrammarAtom;->getType()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "])"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    if-le p1, v3, :cond_2

    iget-object v0, p2, Lantlr/TreeElement;->next:Lantlr/AlternativeElement;

    add-int/lit8 v1, p1, -0x1

    invoke-virtual {v0, v1}, Lantlr/AlternativeElement;->look(I)Lantlr/Lookahead;

    move-result-object v0

    :cond_1
    :goto_0
    return-object v0

    :cond_2
    iget-object v0, p2, Lantlr/TreeElement;->root:Lantlr/GrammarAtom;

    instance-of v0, v0, Lantlr/WildcardElement;

    if-eqz v0, :cond_3

    iget-object v0, p2, Lantlr/TreeElement;->root:Lantlr/GrammarAtom;

    invoke-virtual {v0, v3}, Lantlr/GrammarAtom;->look(I)Lantlr/Lookahead;

    move-result-object v0

    goto :goto_0

    :cond_3
    iget-object v0, p2, Lantlr/TreeElement;->root:Lantlr/GrammarAtom;

    invoke-virtual {v0}, Lantlr/GrammarAtom;->getType()I

    move-result v0

    invoke-static {v0}, Lantlr/Lookahead;->of(I)Lantlr/Lookahead;

    move-result-object v0

    iget-object v1, p2, Lantlr/TreeElement;->root:Lantlr/GrammarAtom;

    iget-boolean v1, v1, Lantlr/GrammarAtom;->not:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lantlr/LLkAnalyzer;->grammar:Lantlr/Grammar;

    iget-object v1, v1, Lantlr/Grammar;->tokenManager:Lantlr/TokenManager;

    invoke-interface {v1}, Lantlr/TokenManager;->maxTokenType()I

    move-result v1

    iget-object v2, v0, Lantlr/Lookahead;->fset:Lantlr/collections/impl/BitSet;

    const/4 v3, 0x4

    invoke-virtual {v2, v3, v1}, Lantlr/collections/impl/BitSet;->notInPlace(II)V

    goto :goto_0
.end method

.method public look(ILantlr/WildcardElement;)Lantlr/Lookahead;
    .locals 4

    const/4 v3, 0x1

    iget-boolean v0, p0, Lantlr/LLkAnalyzer;->DEBUG_ANALYZER:Z

    if-eqz v0, :cond_0

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "look("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    if-le p1, v3, :cond_1

    iget-object v0, p2, Lantlr/WildcardElement;->next:Lantlr/AlternativeElement;

    add-int/lit8 v1, p1, -0x1

    invoke-virtual {v0, v1}, Lantlr/AlternativeElement;->look(I)Lantlr/Lookahead;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_1
    iget-boolean v0, p0, Lantlr/LLkAnalyzer;->lexicalAnalysis:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lantlr/LLkAnalyzer;->grammar:Lantlr/Grammar;

    check-cast v0, Lantlr/LexerGrammar;

    iget-object v0, v0, Lantlr/LexerGrammar;->charVocabulary:Lantlr/collections/impl/BitSet;

    invoke-virtual {v0}, Lantlr/collections/impl/BitSet;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lantlr/collections/impl/BitSet;

    :cond_2
    :goto_1
    new-instance v1, Lantlr/Lookahead;

    invoke-direct {v1, v0}, Lantlr/Lookahead;-><init>(Lantlr/collections/impl/BitSet;)V

    move-object v0, v1

    goto :goto_0

    :cond_3
    new-instance v0, Lantlr/collections/impl/BitSet;

    invoke-direct {v0, v3}, Lantlr/collections/impl/BitSet;-><init>(I)V

    iget-object v1, p0, Lantlr/LLkAnalyzer;->grammar:Lantlr/Grammar;

    iget-object v1, v1, Lantlr/Grammar;->tokenManager:Lantlr/TokenManager;

    invoke-interface {v1}, Lantlr/TokenManager;->maxTokenType()I

    move-result v1

    const/4 v2, 0x4

    invoke-virtual {v0, v2, v1}, Lantlr/collections/impl/BitSet;->notInPlace(II)V

    iget-boolean v1, p0, Lantlr/LLkAnalyzer;->DEBUG_ANALYZER:Z

    if-eqz v1, :cond_2

    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "look("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, ") after not: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public look(ILantlr/ZeroOrMoreBlock;)Lantlr/Lookahead;
    .locals 3

    iget-boolean v0, p0, Lantlr/LLkAnalyzer;->DEBUG_ANALYZER:Z

    if-eqz v0, :cond_0

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "look*("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p0, p1, p2}, Lantlr/LLkAnalyzer;->look(ILantlr/AlternativeBlock;)Lantlr/Lookahead;

    move-result-object v0

    iget-object v1, p2, Lantlr/ZeroOrMoreBlock;->next:Lantlr/AlternativeElement;

    invoke-virtual {v1, p1}, Lantlr/AlternativeElement;->look(I)Lantlr/Lookahead;

    move-result-object v1

    invoke-virtual {v0, v1}, Lantlr/Lookahead;->combineWith(Lantlr/Lookahead;)V

    return-object v0
.end method

.method public look(ILjava/lang/String;)Lantlr/Lookahead;
    .locals 7

    iget-boolean v0, p0, Lantlr/LLkAnalyzer;->DEBUG_ANALYZER:Z

    if-eqz v0, :cond_0

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "lookRuleName("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lantlr/LLkAnalyzer;->grammar:Lantlr/Grammar;

    invoke-virtual {v0, p2}, Lantlr/Grammar;->getSymbol(Ljava/lang/String;)Lantlr/GrammarSymbol;

    move-result-object v0

    check-cast v0, Lantlr/RuleSymbol;

    invoke-virtual {v0}, Lantlr/RuleSymbol;->getBlock()Lantlr/RuleBlock;

    move-result-object v2

    iget-object v0, v2, Lantlr/RuleBlock;->lock:[Z

    aget-boolean v0, v0, p1

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lantlr/LLkAnalyzer;->DEBUG_ANALYZER:Z

    if-eqz v0, :cond_1

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "infinite recursion to rule "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v2}, Lantlr/RuleBlock;->getRuleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_1
    new-instance v0, Lantlr/Lookahead;

    invoke-direct {v0, p2}, Lantlr/Lookahead;-><init>(Ljava/lang/String;)V

    :goto_0
    return-object v0

    :cond_2
    iget-object v0, v2, Lantlr/RuleBlock;->cache:[Lantlr/Lookahead;

    aget-object v0, v0, p1

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lantlr/LLkAnalyzer;->DEBUG_ANALYZER:Z

    if-eqz v0, :cond_3

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "found depth "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v3, " result in FIRST "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v3, " cache: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v3, v2, Lantlr/RuleBlock;->cache:[Lantlr/Lookahead;

    aget-object v3, v3, p1

    const-string v4, ","

    iget-object v5, p0, Lantlr/LLkAnalyzer;->charFormatter:Lantlr/CharFormatter;

    iget-object v6, p0, Lantlr/LLkAnalyzer;->grammar:Lantlr/Grammar;

    invoke-virtual {v3, v4, v5, v6}, Lantlr/Lookahead;->toString(Ljava/lang/String;Lantlr/CharFormatter;Lantlr/Grammar;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_3
    iget-object v0, v2, Lantlr/RuleBlock;->cache:[Lantlr/Lookahead;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Lantlr/Lookahead;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lantlr/Lookahead;

    goto :goto_0

    :cond_4
    iget-object v0, v2, Lantlr/RuleBlock;->lock:[Z

    const/4 v1, 0x1

    aput-boolean v1, v0, p1

    invoke-virtual {p0, p1, v2}, Lantlr/LLkAnalyzer;->look(ILantlr/RuleBlock;)Lantlr/Lookahead;

    move-result-object v1

    iget-object v0, v2, Lantlr/RuleBlock;->lock:[Z

    const/4 v3, 0x0

    aput-boolean v3, v0, p1

    iget-object v3, v2, Lantlr/RuleBlock;->cache:[Lantlr/Lookahead;

    invoke-virtual {v1}, Lantlr/Lookahead;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lantlr/Lookahead;

    aput-object v0, v3, p1

    iget-boolean v0, p0, Lantlr/LLkAnalyzer;->DEBUG_ANALYZER:Z

    if-eqz v0, :cond_5

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string v4, "saving depth "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string v4, " result in FIRST "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string v4, " cache: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    iget-object v2, v2, Lantlr/RuleBlock;->cache:[Lantlr/Lookahead;

    aget-object v2, v2, p1

    const-string v4, ","

    iget-object v5, p0, Lantlr/LLkAnalyzer;->charFormatter:Lantlr/CharFormatter;

    iget-object v6, p0, Lantlr/LLkAnalyzer;->grammar:Lantlr/Grammar;

    invoke-virtual {v2, v4, v5, v6}, Lantlr/Lookahead;->toString(Ljava/lang/String;Lantlr/CharFormatter;Lantlr/Grammar;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_5
    move-object v0, v1

    goto/16 :goto_0
.end method

.method public setGrammar(Lantlr/Grammar;)V
    .locals 1

    iget-object v0, p0, Lantlr/LLkAnalyzer;->grammar:Lantlr/Grammar;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lantlr/LLkAnalyzer;->reset()V

    :cond_0
    iput-object p1, p0, Lantlr/LLkAnalyzer;->grammar:Lantlr/Grammar;

    iget-object v0, p0, Lantlr/LLkAnalyzer;->grammar:Lantlr/Grammar;

    instance-of v0, v0, Lantlr/LexerGrammar;

    iput-boolean v0, p0, Lantlr/LLkAnalyzer;->lexicalAnalysis:Z

    iget-object v0, p0, Lantlr/LLkAnalyzer;->grammar:Lantlr/Grammar;

    iget-boolean v0, v0, Lantlr/Grammar;->analyzerDebug:Z

    iput-boolean v0, p0, Lantlr/LLkAnalyzer;->DEBUG_ANALYZER:Z

    return-void
.end method

.method public subruleCanBeInverted(Lantlr/AlternativeBlock;Z)Z
    .locals 5

    const/4 v2, 0x1

    const/4 v1, 0x0

    instance-of v0, p1, Lantlr/ZeroOrMoreBlock;

    if-nez v0, :cond_0

    instance-of v0, p1, Lantlr/OneOrMoreBlock;

    if-nez v0, :cond_0

    instance-of v0, p1, Lantlr/SynPredBlock;

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    iget-object v0, p1, Lantlr/AlternativeBlock;->alternatives:Lantlr/collections/impl/Vector;

    invoke-virtual {v0}, Lantlr/collections/impl/Vector;->size()I

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_1
    iget-object v3, p1, Lantlr/AlternativeBlock;->alternatives:Lantlr/collections/impl/Vector;

    invoke-virtual {v3}, Lantlr/collections/impl/Vector;->size()I

    move-result v3

    if-ge v0, v3, :cond_3

    invoke-virtual {p1, v0}, Lantlr/AlternativeBlock;->getAlternativeAt(I)Lantlr/Alternative;

    move-result-object v3

    iget-object v4, v3, Lantlr/Alternative;->synPred:Lantlr/SynPredBlock;

    if-nez v4, :cond_0

    iget-object v4, v3, Lantlr/Alternative;->semPred:Ljava/lang/String;

    if-nez v4, :cond_0

    iget-object v4, v3, Lantlr/Alternative;->exceptionSpec:Lantlr/ExceptionSpec;

    if-nez v4, :cond_0

    iget-object v3, v3, Lantlr/Alternative;->head:Lantlr/AlternativeElement;

    instance-of v4, v3, Lantlr/CharLiteralElement;

    if-nez v4, :cond_2

    instance-of v4, v3, Lantlr/TokenRefElement;

    if-nez v4, :cond_2

    instance-of v4, v3, Lantlr/CharRangeElement;

    if-nez v4, :cond_2

    instance-of v4, v3, Lantlr/TokenRangeElement;

    if-nez v4, :cond_2

    instance-of v4, v3, Lantlr/StringLiteralElement;

    if-eqz v4, :cond_0

    if-nez p2, :cond_0

    :cond_2
    iget-object v4, v3, Lantlr/AlternativeElement;->next:Lantlr/AlternativeElement;

    instance-of v4, v4, Lantlr/BlockEndElement;

    if-eqz v4, :cond_0

    invoke-virtual {v3}, Lantlr/AlternativeElement;->getAutoGenType()I

    move-result v3

    if-ne v3, v2, :cond_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    move v1, v2

    goto :goto_0
.end method
