.class public Lantlr/MakeGrammar;
.super Lantlr/DefineGrammarSymbols;
.source "MakeGrammar.java"


# instance fields
.field protected blocks:Lantlr/collections/Stack;

.field currentExceptionSpec:Lantlr/ExceptionSpec;

.field protected grammarError:Z

.field protected lastRuleRef:Lantlr/RuleRefElement;

.field protected nested:I

.field protected ruleBlock:Lantlr/RuleBlock;

.field protected ruleEnd:Lantlr/RuleEndElement;


# direct methods
.method public constructor <init>(Lantlr/Tool;[Ljava/lang/String;Lantlr/LLkAnalyzer;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, p3}, Lantlr/DefineGrammarSymbols;-><init>(Lantlr/Tool;[Ljava/lang/String;Lantlr/LLkAnalyzer;)V

    new-instance v0, Lantlr/collections/impl/LList;

    invoke-direct {v0}, Lantlr/collections/impl/LList;-><init>()V

    iput-object v0, p0, Lantlr/MakeGrammar;->blocks:Lantlr/collections/Stack;

    iput v1, p0, Lantlr/MakeGrammar;->nested:I

    iput-boolean v1, p0, Lantlr/MakeGrammar;->grammarError:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lantlr/MakeGrammar;->currentExceptionSpec:Lantlr/ExceptionSpec;

    return-void
.end method

.method public static createNextTokenRule(Lantlr/Grammar;Lantlr/collections/impl/Vector;Ljava/lang/String;)Lantlr/RuleBlock;
    .locals 11

    const/4 v3, 0x0

    const/4 v10, 0x1

    new-instance v4, Lantlr/RuleBlock;

    invoke-direct {v4, p0, p2}, Lantlr/RuleBlock;-><init>(Lantlr/Grammar;Ljava/lang/String;)V

    invoke-virtual {p0}, Lantlr/Grammar;->getDefaultErrorHandler()Z

    move-result v0

    invoke-virtual {v4, v0}, Lantlr/RuleBlock;->setDefaultErrorHandler(Z)V

    new-instance v5, Lantlr/RuleEndElement;

    invoke-direct {v5, p0}, Lantlr/RuleEndElement;-><init>(Lantlr/Grammar;)V

    invoke-virtual {v4, v5}, Lantlr/RuleBlock;->setEndElement(Lantlr/RuleEndElement;)V

    iput-object v4, v5, Lantlr/RuleEndElement;->block:Lantlr/AlternativeBlock;

    move v2, v3

    :goto_0
    invoke-virtual {p1}, Lantlr/collections/impl/Vector;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    invoke-virtual {p1, v2}, Lantlr/collections/impl/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lantlr/RuleSymbol;

    invoke-virtual {v0}, Lantlr/RuleSymbol;->isDefined()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lantlr/Grammar;->antlrTool:Lantlr/Tool;

    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6}, Ljava/lang/StringBuffer;-><init>()V

    const-string v7, "Lexer rule "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v6

    iget-object v0, v0, Lantlr/RuleSymbol;->id:Ljava/lang/String;

    invoke-virtual {v0, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v6, " is not defined"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lantlr/Tool;->error(Ljava/lang/String;)V

    :cond_0
    :goto_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    iget-object v1, v0, Lantlr/RuleSymbol;->access:Ljava/lang/String;

    const-string v6, "public"

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v6, Lantlr/Alternative;

    invoke-direct {v6}, Lantlr/Alternative;-><init>()V

    invoke-virtual {v0}, Lantlr/RuleSymbol;->getBlock()Lantlr/RuleBlock;

    move-result-object v1

    invoke-virtual {v1}, Lantlr/RuleBlock;->getAlternatives()Lantlr/collections/impl/Vector;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lantlr/collections/impl/Vector;->size()I

    move-result v7

    if-ne v7, v10, :cond_2

    invoke-virtual {v1, v3}, Lantlr/collections/impl/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lantlr/Alternative;

    iget-object v7, v1, Lantlr/Alternative;->semPred:Ljava/lang/String;

    if-eqz v7, :cond_2

    iget-object v1, v1, Lantlr/Alternative;->semPred:Ljava/lang/String;

    iput-object v1, v6, Lantlr/Alternative;->semPred:Ljava/lang/String;

    :cond_2
    new-instance v1, Lantlr/RuleRefElement;

    new-instance v7, Lantlr/CommonToken;

    const/16 v8, 0x29

    invoke-virtual {v0}, Lantlr/RuleSymbol;->getId()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v8, v9}, Lantlr/CommonToken;-><init>(ILjava/lang/String;)V

    invoke-direct {v1, p0, v7, v10}, Lantlr/RuleRefElement;-><init>(Lantlr/Grammar;Lantlr/Token;I)V

    const-string v7, "theRetToken"

    invoke-virtual {v1, v7}, Lantlr/RuleRefElement;->setLabel(Ljava/lang/String;)V

    const-string v7, "nextToken"

    iput-object v7, v1, Lantlr/RuleRefElement;->enclosingRuleName:Ljava/lang/String;

    iput-object v5, v1, Lantlr/RuleRefElement;->next:Lantlr/AlternativeElement;

    invoke-virtual {v6, v1}, Lantlr/Alternative;->addElement(Lantlr/AlternativeElement;)V

    invoke-virtual {v6, v10}, Lantlr/Alternative;->setAutoGen(Z)V

    invoke-virtual {v4, v6}, Lantlr/RuleBlock;->addAlternative(Lantlr/Alternative;)V

    invoke-virtual {v0, v1}, Lantlr/RuleSymbol;->addReference(Lantlr/RuleRefElement;)V

    goto :goto_1

    :cond_3
    invoke-virtual {v4, v10}, Lantlr/RuleBlock;->setAutoGen(Z)V

    invoke-virtual {v4}, Lantlr/RuleBlock;->prepareForAnalysis()V

    return-object v4
.end method

.method private createOptionalRuleRef(Ljava/lang/String;Lantlr/Token;)Lantlr/AlternativeBlock;
    .locals 5

    new-instance v0, Lantlr/AlternativeBlock;

    iget-object v1, p0, Lantlr/MakeGrammar;->grammar:Lantlr/Grammar;

    const/4 v2, 0x0

    invoke-direct {v0, v1, p2, v2}, Lantlr/AlternativeBlock;-><init>(Lantlr/Grammar;Lantlr/Token;Z)V

    invoke-static {p1}, Lantlr/CodeGenerator;->encodeLexerRuleName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lantlr/MakeGrammar;->grammar:Lantlr/Grammar;

    invoke-virtual {v2, v1}, Lantlr/Grammar;->isDefined(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lantlr/MakeGrammar;->grammar:Lantlr/Grammar;

    new-instance v3, Lantlr/RuleSymbol;

    invoke-direct {v3, v1}, Lantlr/RuleSymbol;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Lantlr/Grammar;->define(Lantlr/RuleSymbol;)V

    :cond_0
    new-instance v1, Lantlr/CommonToken;

    const/16 v2, 0x18

    invoke-direct {v1, v2, p1}, Lantlr/CommonToken;-><init>(ILjava/lang/String;)V

    invoke-virtual {p2}, Lantlr/Token;->getLine()I

    move-result v2

    invoke-virtual {v1, v2}, Lantlr/Token;->setLine(I)V

    invoke-virtual {p2}, Lantlr/Token;->getColumn()I

    move-result v2

    invoke-virtual {v1, v2}, Lantlr/Token;->setLine(I)V

    new-instance v2, Lantlr/RuleRefElement;

    iget-object v3, p0, Lantlr/MakeGrammar;->grammar:Lantlr/Grammar;

    const/4 v4, 0x1

    invoke-direct {v2, v3, v1, v4}, Lantlr/RuleRefElement;-><init>(Lantlr/Grammar;Lantlr/Token;I)V

    iget-object v1, p0, Lantlr/MakeGrammar;->ruleBlock:Lantlr/RuleBlock;

    iget-object v1, v1, Lantlr/RuleBlock;->ruleName:Ljava/lang/String;

    iput-object v1, v2, Lantlr/RuleRefElement;->enclosingRuleName:Ljava/lang/String;

    new-instance v1, Lantlr/BlockEndElement;

    iget-object v3, p0, Lantlr/MakeGrammar;->grammar:Lantlr/Grammar;

    invoke-direct {v1, v3}, Lantlr/BlockEndElement;-><init>(Lantlr/Grammar;)V

    iput-object v0, v1, Lantlr/BlockEndElement;->block:Lantlr/AlternativeBlock;

    new-instance v3, Lantlr/Alternative;

    invoke-direct {v3, v2}, Lantlr/Alternative;-><init>(Lantlr/AlternativeElement;)V

    invoke-virtual {v3, v1}, Lantlr/Alternative;->addElement(Lantlr/AlternativeElement;)V

    invoke-virtual {v0, v3}, Lantlr/AlternativeBlock;->addAlternative(Lantlr/Alternative;)V

    new-instance v2, Lantlr/Alternative;

    invoke-direct {v2}, Lantlr/Alternative;-><init>()V

    invoke-virtual {v2, v1}, Lantlr/Alternative;->addElement(Lantlr/AlternativeElement;)V

    invoke-virtual {v0, v2}, Lantlr/AlternativeBlock;->addAlternative(Lantlr/Alternative;)V

    invoke-virtual {v0}, Lantlr/AlternativeBlock;->prepareForAnalysis()V

    return-object v0
.end method

.method private labelElement(Lantlr/AlternativeElement;Lantlr/Token;)V
    .locals 5

    if-eqz p2, :cond_0

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lantlr/MakeGrammar;->ruleBlock:Lantlr/RuleBlock;

    iget-object v0, v0, Lantlr/RuleBlock;->labeledElements:Lantlr/collections/impl/Vector;

    invoke-virtual {v0}, Lantlr/collections/impl/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    iget-object v0, p0, Lantlr/MakeGrammar;->ruleBlock:Lantlr/RuleBlock;

    iget-object v0, v0, Lantlr/RuleBlock;->labeledElements:Lantlr/collections/impl/Vector;

    invoke-virtual {v0, v1}, Lantlr/collections/impl/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lantlr/AlternativeElement;

    invoke-virtual {v0}, Lantlr/AlternativeElement;->getLabel()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p2}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lantlr/MakeGrammar;->tool:Lantlr/Tool;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "Label \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p2}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "\' has already been defined"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lantlr/MakeGrammar;->grammar:Lantlr/Grammar;

    invoke-virtual {v2}, Lantlr/Grammar;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Lantlr/Token;->getLine()I

    move-result v3

    invoke-virtual {p2}, Lantlr/Token;->getColumn()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lantlr/Tool;->error(Ljava/lang/String;Ljava/lang/String;II)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    invoke-virtual {p2}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lantlr/AlternativeElement;->setLabel(Ljava/lang/String;)V

    iget-object v0, p0, Lantlr/MakeGrammar;->ruleBlock:Lantlr/RuleBlock;

    iget-object v0, v0, Lantlr/RuleBlock;->labeledElements:Lantlr/collections/impl/Vector;

    invoke-virtual {v0, p1}, Lantlr/collections/impl/Vector;->appendElement(Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public static setBlock(Lantlr/AlternativeBlock;Lantlr/AlternativeBlock;)V
    .locals 1

    invoke-virtual {p1}, Lantlr/AlternativeBlock;->getAlternatives()Lantlr/collections/impl/Vector;

    move-result-object v0

    invoke-virtual {p0, v0}, Lantlr/AlternativeBlock;->setAlternatives(Lantlr/collections/impl/Vector;)V

    iget-object v0, p1, Lantlr/AlternativeBlock;->initAction:Ljava/lang/String;

    iput-object v0, p0, Lantlr/AlternativeBlock;->initAction:Ljava/lang/String;

    iget-object v0, p1, Lantlr/AlternativeBlock;->label:Ljava/lang/String;

    iput-object v0, p0, Lantlr/AlternativeBlock;->label:Ljava/lang/String;

    iget-boolean v0, p1, Lantlr/AlternativeBlock;->hasASynPred:Z

    iput-boolean v0, p0, Lantlr/AlternativeBlock;->hasASynPred:Z

    iget-boolean v0, p1, Lantlr/AlternativeBlock;->hasAnAction:Z

    iput-boolean v0, p0, Lantlr/AlternativeBlock;->hasAnAction:Z

    iget-boolean v0, p1, Lantlr/AlternativeBlock;->warnWhenFollowAmbig:Z

    iput-boolean v0, p0, Lantlr/AlternativeBlock;->warnWhenFollowAmbig:Z

    iget-boolean v0, p1, Lantlr/AlternativeBlock;->generateAmbigWarnings:Z

    iput-boolean v0, p0, Lantlr/AlternativeBlock;->generateAmbigWarnings:Z

    iget v0, p1, Lantlr/AlternativeBlock;->line:I

    iput v0, p0, Lantlr/AlternativeBlock;->line:I

    iget-boolean v0, p1, Lantlr/AlternativeBlock;->greedy:Z

    iput-boolean v0, p0, Lantlr/AlternativeBlock;->greedy:Z

    iget-boolean v0, p1, Lantlr/AlternativeBlock;->greedySet:Z

    iput-boolean v0, p0, Lantlr/AlternativeBlock;->greedySet:Z

    return-void
.end method


# virtual methods
.method public abortGrammar()V
    .locals 4

    const-string v0, "unknown grammar"

    iget-object v1, p0, Lantlr/MakeGrammar;->grammar:Lantlr/Grammar;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lantlr/MakeGrammar;->grammar:Lantlr/Grammar;

    invoke-virtual {v0}, Lantlr/Grammar;->getClassName()Ljava/lang/String;

    move-result-object v0

    :cond_0
    iget-object v1, p0, Lantlr/MakeGrammar;->tool:Lantlr/Tool;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "aborting grammar \'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v2, "\' due to errors"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lantlr/Tool;->error(Ljava/lang/String;)V

    invoke-super {p0}, Lantlr/DefineGrammarSymbols;->abortGrammar()V

    return-void
.end method

.method protected addElementToCurrentAlt(Lantlr/AlternativeElement;)V
    .locals 1

    iget-object v0, p0, Lantlr/MakeGrammar;->ruleBlock:Lantlr/RuleBlock;

    iget-object v0, v0, Lantlr/RuleBlock;->ruleName:Ljava/lang/String;

    iput-object v0, p1, Lantlr/AlternativeElement;->enclosingRuleName:Ljava/lang/String;

    invoke-virtual {p0}, Lantlr/MakeGrammar;->context()Lantlr/BlockContext;

    move-result-object v0

    invoke-virtual {v0, p1}, Lantlr/BlockContext;->addAlternativeElement(Lantlr/AlternativeElement;)V

    return-void
.end method

.method public beginAlt(Z)V
    .locals 2

    invoke-super {p0, p1}, Lantlr/DefineGrammarSymbols;->beginAlt(Z)V

    new-instance v0, Lantlr/Alternative;

    invoke-direct {v0}, Lantlr/Alternative;-><init>()V

    invoke-virtual {v0, p1}, Lantlr/Alternative;->setAutoGen(Z)V

    invoke-virtual {p0}, Lantlr/MakeGrammar;->context()Lantlr/BlockContext;

    move-result-object v1

    iget-object v1, v1, Lantlr/BlockContext;->block:Lantlr/AlternativeBlock;

    invoke-virtual {v1, v0}, Lantlr/AlternativeBlock;->addAlternative(Lantlr/Alternative;)V

    return-void
.end method

.method public beginChildList()V
    .locals 2

    invoke-super {p0}, Lantlr/DefineGrammarSymbols;->beginChildList()V

    invoke-virtual {p0}, Lantlr/MakeGrammar;->context()Lantlr/BlockContext;

    move-result-object v0

    iget-object v0, v0, Lantlr/BlockContext;->block:Lantlr/AlternativeBlock;

    new-instance v1, Lantlr/Alternative;

    invoke-direct {v1}, Lantlr/Alternative;-><init>()V

    invoke-virtual {v0, v1}, Lantlr/AlternativeBlock;->addAlternative(Lantlr/Alternative;)V

    return-void
.end method

.method public beginExceptionGroup()V
    .locals 2

    invoke-super {p0}, Lantlr/DefineGrammarSymbols;->beginExceptionGroup()V

    invoke-virtual {p0}, Lantlr/MakeGrammar;->context()Lantlr/BlockContext;

    move-result-object v0

    iget-object v0, v0, Lantlr/BlockContext;->block:Lantlr/AlternativeBlock;

    instance-of v0, v0, Lantlr/RuleBlock;

    if-nez v0, :cond_0

    iget-object v0, p0, Lantlr/MakeGrammar;->tool:Lantlr/Tool;

    const-string v1, "beginExceptionGroup called outside of rule block"

    invoke-virtual {v0, v1}, Lantlr/Tool;->panic(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public beginExceptionSpec(Lantlr/Token;)V
    .locals 2

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v0

    const-string v1, " \n\r\t"

    invoke-static {v0, v1}, Lantlr/StringUtils;->stripBack(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, " \n\r\t"

    invoke-static {v0, v1}, Lantlr/StringUtils;->stripFront(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lantlr/Token;->setText(Ljava/lang/String;)V

    :cond_0
    invoke-super {p0, p1}, Lantlr/DefineGrammarSymbols;->beginExceptionSpec(Lantlr/Token;)V

    new-instance v0, Lantlr/ExceptionSpec;

    invoke-direct {v0, p1}, Lantlr/ExceptionSpec;-><init>(Lantlr/Token;)V

    iput-object v0, p0, Lantlr/MakeGrammar;->currentExceptionSpec:Lantlr/ExceptionSpec;

    return-void
.end method

.method public beginSubRule(Lantlr/Token;Lantlr/Token;Z)V
    .locals 3

    invoke-super {p0, p1, p2, p3}, Lantlr/DefineGrammarSymbols;->beginSubRule(Lantlr/Token;Lantlr/Token;Z)V

    iget-object v0, p0, Lantlr/MakeGrammar;->blocks:Lantlr/collections/Stack;

    new-instance v1, Lantlr/BlockContext;

    invoke-direct {v1}, Lantlr/BlockContext;-><init>()V

    invoke-interface {v0, v1}, Lantlr/collections/Stack;->push(Ljava/lang/Object;)V

    invoke-virtual {p0}, Lantlr/MakeGrammar;->context()Lantlr/BlockContext;

    move-result-object v0

    new-instance v1, Lantlr/AlternativeBlock;

    iget-object v2, p0, Lantlr/MakeGrammar;->grammar:Lantlr/Grammar;

    invoke-direct {v1, v2, p2, p3}, Lantlr/AlternativeBlock;-><init>(Lantlr/Grammar;Lantlr/Token;Z)V

    iput-object v1, v0, Lantlr/BlockContext;->block:Lantlr/AlternativeBlock;

    invoke-virtual {p0}, Lantlr/MakeGrammar;->context()Lantlr/BlockContext;

    move-result-object v0

    const/4 v1, 0x0

    iput v1, v0, Lantlr/BlockContext;->altNum:I

    iget v0, p0, Lantlr/MakeGrammar;->nested:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lantlr/MakeGrammar;->nested:I

    invoke-virtual {p0}, Lantlr/MakeGrammar;->context()Lantlr/BlockContext;

    move-result-object v0

    new-instance v1, Lantlr/BlockEndElement;

    iget-object v2, p0, Lantlr/MakeGrammar;->grammar:Lantlr/Grammar;

    invoke-direct {v1, v2}, Lantlr/BlockEndElement;-><init>(Lantlr/Grammar;)V

    iput-object v1, v0, Lantlr/BlockContext;->blockEnd:Lantlr/BlockEndElement;

    invoke-virtual {p0}, Lantlr/MakeGrammar;->context()Lantlr/BlockContext;

    move-result-object v0

    iget-object v0, v0, Lantlr/BlockContext;->blockEnd:Lantlr/BlockEndElement;

    invoke-virtual {p0}, Lantlr/MakeGrammar;->context()Lantlr/BlockContext;

    move-result-object v1

    iget-object v1, v1, Lantlr/BlockContext;->block:Lantlr/AlternativeBlock;

    iput-object v1, v0, Lantlr/BlockEndElement;->block:Lantlr/AlternativeBlock;

    invoke-virtual {p0}, Lantlr/MakeGrammar;->context()Lantlr/BlockContext;

    move-result-object v0

    iget-object v0, v0, Lantlr/BlockContext;->block:Lantlr/AlternativeBlock;

    invoke-direct {p0, v0, p1}, Lantlr/MakeGrammar;->labelElement(Lantlr/AlternativeElement;Lantlr/Token;)V

    return-void
.end method

.method public beginTree(Lantlr/Token;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/SemanticException;
        }
    .end annotation

    iget-object v0, p0, Lantlr/MakeGrammar;->grammar:Lantlr/Grammar;

    instance-of v0, v0, Lantlr/TreeWalkerGrammar;

    if-nez v0, :cond_0

    iget-object v0, p0, Lantlr/MakeGrammar;->tool:Lantlr/Tool;

    const-string v1, "Trees only allowed in TreeParser"

    iget-object v2, p0, Lantlr/MakeGrammar;->grammar:Lantlr/Grammar;

    invoke-virtual {v2}, Lantlr/Grammar;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lantlr/Token;->getLine()I

    move-result v3

    invoke-virtual {p1}, Lantlr/Token;->getColumn()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lantlr/Tool;->error(Ljava/lang/String;Ljava/lang/String;II)V

    new-instance v0, Lantlr/SemanticException;

    const-string v1, "Trees only allowed in TreeParser"

    invoke-direct {v0, v1}, Lantlr/SemanticException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-super {p0, p1}, Lantlr/DefineGrammarSymbols;->beginTree(Lantlr/Token;)V

    iget-object v0, p0, Lantlr/MakeGrammar;->blocks:Lantlr/collections/Stack;

    new-instance v1, Lantlr/TreeBlockContext;

    invoke-direct {v1}, Lantlr/TreeBlockContext;-><init>()V

    invoke-interface {v0, v1}, Lantlr/collections/Stack;->push(Ljava/lang/Object;)V

    invoke-virtual {p0}, Lantlr/MakeGrammar;->context()Lantlr/BlockContext;

    move-result-object v0

    new-instance v1, Lantlr/TreeElement;

    iget-object v2, p0, Lantlr/MakeGrammar;->grammar:Lantlr/Grammar;

    invoke-direct {v1, v2, p1}, Lantlr/TreeElement;-><init>(Lantlr/Grammar;Lantlr/Token;)V

    iput-object v1, v0, Lantlr/BlockContext;->block:Lantlr/AlternativeBlock;

    invoke-virtual {p0}, Lantlr/MakeGrammar;->context()Lantlr/BlockContext;

    move-result-object v0

    const/4 v1, 0x0

    iput v1, v0, Lantlr/BlockContext;->altNum:I

    return-void
.end method

.method public context()Lantlr/BlockContext;
    .locals 1

    iget-object v0, p0, Lantlr/MakeGrammar;->blocks:Lantlr/collections/Stack;

    invoke-interface {v0}, Lantlr/collections/Stack;->height()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lantlr/MakeGrammar;->blocks:Lantlr/collections/Stack;

    invoke-interface {v0}, Lantlr/collections/Stack;->top()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lantlr/BlockContext;

    goto :goto_0
.end method

.method public defineRuleName(Lantlr/Token;Ljava/lang/String;ZLjava/lang/String;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/SemanticException;
        }
    .end annotation

    const/16 v5, 0x18

    iget v0, p1, Lantlr/Token;->type:I

    if-ne v0, v5, :cond_2

    iget-object v0, p0, Lantlr/MakeGrammar;->grammar:Lantlr/Grammar;

    instance-of v0, v0, Lantlr/LexerGrammar;

    if-nez v0, :cond_0

    iget-object v0, p0, Lantlr/MakeGrammar;->tool:Lantlr/Tool;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "Lexical rule "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p1}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, " defined outside of lexer"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lantlr/MakeGrammar;->grammar:Lantlr/Grammar;

    invoke-virtual {v2}, Lantlr/Grammar;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lantlr/Token;->getLine()I

    move-result v3

    invoke-virtual {p1}, Lantlr/Token;->getColumn()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lantlr/Tool;->error(Ljava/lang/String;Ljava/lang/String;II)V

    invoke-virtual {p1}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lantlr/Token;->setText(Ljava/lang/String;)V

    :cond_0
    :goto_0
    invoke-super {p0, p1, p2, p3, p4}, Lantlr/DefineGrammarSymbols;->defineRuleName(Lantlr/Token;Ljava/lang/String;ZLjava/lang/String;)V

    invoke-virtual {p1}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v0

    iget v1, p1, Lantlr/Token;->type:I

    if-ne v1, v5, :cond_1

    invoke-static {v0}, Lantlr/CodeGenerator;->encodeLexerRuleName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_1
    iget-object v1, p0, Lantlr/MakeGrammar;->grammar:Lantlr/Grammar;

    invoke-virtual {v1, v0}, Lantlr/Grammar;->getSymbol(Ljava/lang/String;)Lantlr/GrammarSymbol;

    move-result-object v0

    check-cast v0, Lantlr/RuleSymbol;

    new-instance v1, Lantlr/RuleBlock;

    iget-object v2, p0, Lantlr/MakeGrammar;->grammar:Lantlr/Grammar;

    invoke-virtual {p1}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lantlr/Token;->getLine()I

    move-result v4

    invoke-direct {v1, v2, v3, v4, p3}, Lantlr/RuleBlock;-><init>(Lantlr/Grammar;Ljava/lang/String;IZ)V

    iget-object v2, p0, Lantlr/MakeGrammar;->grammar:Lantlr/Grammar;

    invoke-virtual {v2}, Lantlr/Grammar;->getDefaultErrorHandler()Z

    move-result v2

    invoke-virtual {v1, v2}, Lantlr/RuleBlock;->setDefaultErrorHandler(Z)V

    iput-object v1, p0, Lantlr/MakeGrammar;->ruleBlock:Lantlr/RuleBlock;

    iget-object v2, p0, Lantlr/MakeGrammar;->blocks:Lantlr/collections/Stack;

    new-instance v3, Lantlr/BlockContext;

    invoke-direct {v3}, Lantlr/BlockContext;-><init>()V

    invoke-interface {v2, v3}, Lantlr/collections/Stack;->push(Ljava/lang/Object;)V

    invoke-virtual {p0}, Lantlr/MakeGrammar;->context()Lantlr/BlockContext;

    move-result-object v2

    iput-object v1, v2, Lantlr/BlockContext;->block:Lantlr/AlternativeBlock;

    invoke-virtual {v0, v1}, Lantlr/RuleSymbol;->setBlock(Lantlr/RuleBlock;)V

    new-instance v0, Lantlr/RuleEndElement;

    iget-object v2, p0, Lantlr/MakeGrammar;->grammar:Lantlr/Grammar;

    invoke-direct {v0, v2}, Lantlr/RuleEndElement;-><init>(Lantlr/Grammar;)V

    iput-object v0, p0, Lantlr/MakeGrammar;->ruleEnd:Lantlr/RuleEndElement;

    iget-object v0, p0, Lantlr/MakeGrammar;->ruleEnd:Lantlr/RuleEndElement;

    invoke-virtual {v1, v0}, Lantlr/RuleBlock;->setEndElement(Lantlr/RuleEndElement;)V

    const/4 v0, 0x0

    iput v0, p0, Lantlr/MakeGrammar;->nested:I

    return-void

    :cond_2
    iget-object v0, p0, Lantlr/MakeGrammar;->grammar:Lantlr/Grammar;

    instance-of v0, v0, Lantlr/LexerGrammar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lantlr/MakeGrammar;->tool:Lantlr/Tool;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "Lexical rule names must be upper case, \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p1}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "\' is not"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lantlr/MakeGrammar;->grammar:Lantlr/Grammar;

    invoke-virtual {v2}, Lantlr/Grammar;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lantlr/Token;->getLine()I

    move-result v3

    invoke-virtual {p1}, Lantlr/Token;->getColumn()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lantlr/Tool;->error(Ljava/lang/String;Ljava/lang/String;II)V

    invoke-virtual {p1}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lantlr/Token;->setText(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public endAlt()V
    .locals 2

    invoke-super {p0}, Lantlr/DefineGrammarSymbols;->endAlt()V

    iget v0, p0, Lantlr/MakeGrammar;->nested:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lantlr/MakeGrammar;->ruleEnd:Lantlr/RuleEndElement;

    invoke-virtual {p0, v0}, Lantlr/MakeGrammar;->addElementToCurrentAlt(Lantlr/AlternativeElement;)V

    :goto_0
    invoke-virtual {p0}, Lantlr/MakeGrammar;->context()Lantlr/BlockContext;

    move-result-object v0

    iget v1, v0, Lantlr/BlockContext;->altNum:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lantlr/BlockContext;->altNum:I

    return-void

    :cond_0
    invoke-virtual {p0}, Lantlr/MakeGrammar;->context()Lantlr/BlockContext;

    move-result-object v0

    iget-object v0, v0, Lantlr/BlockContext;->blockEnd:Lantlr/BlockEndElement;

    invoke-virtual {p0, v0}, Lantlr/MakeGrammar;->addElementToCurrentAlt(Lantlr/AlternativeElement;)V

    goto :goto_0
.end method

.method public endChildList()V
    .locals 2

    invoke-super {p0}, Lantlr/DefineGrammarSymbols;->endChildList()V

    new-instance v0, Lantlr/BlockEndElement;

    iget-object v1, p0, Lantlr/MakeGrammar;->grammar:Lantlr/Grammar;

    invoke-direct {v0, v1}, Lantlr/BlockEndElement;-><init>(Lantlr/Grammar;)V

    invoke-virtual {p0}, Lantlr/MakeGrammar;->context()Lantlr/BlockContext;

    move-result-object v1

    iget-object v1, v1, Lantlr/BlockContext;->block:Lantlr/AlternativeBlock;

    iput-object v1, v0, Lantlr/BlockEndElement;->block:Lantlr/AlternativeBlock;

    invoke-virtual {p0, v0}, Lantlr/MakeGrammar;->addElementToCurrentAlt(Lantlr/AlternativeElement;)V

    return-void
.end method

.method public endExceptionGroup()V
    .locals 0

    invoke-super {p0}, Lantlr/DefineGrammarSymbols;->endExceptionGroup()V

    return-void
.end method

.method public endExceptionSpec()V
    .locals 5

    invoke-super {p0}, Lantlr/DefineGrammarSymbols;->endExceptionSpec()V

    iget-object v0, p0, Lantlr/MakeGrammar;->currentExceptionSpec:Lantlr/ExceptionSpec;

    if-nez v0, :cond_0

    iget-object v0, p0, Lantlr/MakeGrammar;->tool:Lantlr/Tool;

    const-string v1, "exception processing internal error -- no active exception spec"

    invoke-virtual {v0, v1}, Lantlr/Tool;->panic(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lantlr/MakeGrammar;->context()Lantlr/BlockContext;

    move-result-object v0

    iget-object v0, v0, Lantlr/BlockContext;->block:Lantlr/AlternativeBlock;

    instance-of v0, v0, Lantlr/RuleBlock;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lantlr/MakeGrammar;->context()Lantlr/BlockContext;

    move-result-object v0

    iget-object v0, v0, Lantlr/BlockContext;->block:Lantlr/AlternativeBlock;

    check-cast v0, Lantlr/RuleBlock;

    iget-object v1, p0, Lantlr/MakeGrammar;->currentExceptionSpec:Lantlr/ExceptionSpec;

    invoke-virtual {v0, v1}, Lantlr/RuleBlock;->addExceptionSpec(Lantlr/ExceptionSpec;)V

    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, Lantlr/MakeGrammar;->currentExceptionSpec:Lantlr/ExceptionSpec;

    return-void

    :cond_1
    invoke-virtual {p0}, Lantlr/MakeGrammar;->context()Lantlr/BlockContext;

    move-result-object v0

    invoke-virtual {v0}, Lantlr/BlockContext;->currentAlt()Lantlr/Alternative;

    move-result-object v0

    iget-object v0, v0, Lantlr/Alternative;->exceptionSpec:Lantlr/ExceptionSpec;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lantlr/MakeGrammar;->tool:Lantlr/Tool;

    const-string v1, "Alternative already has an exception specification"

    iget-object v2, p0, Lantlr/MakeGrammar;->grammar:Lantlr/Grammar;

    invoke-virtual {v2}, Lantlr/Grammar;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lantlr/MakeGrammar;->context()Lantlr/BlockContext;

    move-result-object v3

    iget-object v3, v3, Lantlr/BlockContext;->block:Lantlr/AlternativeBlock;

    invoke-virtual {v3}, Lantlr/AlternativeBlock;->getLine()I

    move-result v3

    invoke-virtual {p0}, Lantlr/MakeGrammar;->context()Lantlr/BlockContext;

    move-result-object v4

    iget-object v4, v4, Lantlr/BlockContext;->block:Lantlr/AlternativeBlock;

    invoke-virtual {v4}, Lantlr/AlternativeBlock;->getColumn()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lantlr/Tool;->error(Ljava/lang/String;Ljava/lang/String;II)V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lantlr/MakeGrammar;->context()Lantlr/BlockContext;

    move-result-object v0

    invoke-virtual {v0}, Lantlr/BlockContext;->currentAlt()Lantlr/Alternative;

    move-result-object v0

    iget-object v1, p0, Lantlr/MakeGrammar;->currentExceptionSpec:Lantlr/ExceptionSpec;

    iput-object v1, v0, Lantlr/Alternative;->exceptionSpec:Lantlr/ExceptionSpec;

    goto :goto_0
.end method

.method public endGrammar()V
    .locals 1

    iget-boolean v0, p0, Lantlr/MakeGrammar;->grammarError:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lantlr/MakeGrammar;->abortGrammar()V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0}, Lantlr/DefineGrammarSymbols;->endGrammar()V

    goto :goto_0
.end method

.method public endRule(Ljava/lang/String;)V
    .locals 2

    invoke-super {p0, p1}, Lantlr/DefineGrammarSymbols;->endRule(Ljava/lang/String;)V

    iget-object v0, p0, Lantlr/MakeGrammar;->blocks:Lantlr/collections/Stack;

    invoke-interface {v0}, Lantlr/collections/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lantlr/BlockContext;

    iget-object v1, p0, Lantlr/MakeGrammar;->ruleEnd:Lantlr/RuleEndElement;

    iget-object v0, v0, Lantlr/BlockContext;->block:Lantlr/AlternativeBlock;

    iput-object v0, v1, Lantlr/RuleEndElement;->block:Lantlr/AlternativeBlock;

    iget-object v0, p0, Lantlr/MakeGrammar;->ruleEnd:Lantlr/RuleEndElement;

    iget-object v0, v0, Lantlr/RuleEndElement;->block:Lantlr/AlternativeBlock;

    invoke-virtual {v0}, Lantlr/AlternativeBlock;->prepareForAnalysis()V

    return-void
.end method

.method public endSubRule()V
    .locals 8

    const/4 v7, 0x1

    invoke-super {p0}, Lantlr/DefineGrammarSymbols;->endSubRule()V

    iget v0, p0, Lantlr/MakeGrammar;->nested:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lantlr/MakeGrammar;->nested:I

    iget-object v0, p0, Lantlr/MakeGrammar;->blocks:Lantlr/collections/Stack;

    invoke-interface {v0}, Lantlr/collections/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lantlr/BlockContext;

    iget-object v1, v0, Lantlr/BlockContext;->block:Lantlr/AlternativeBlock;

    iget-boolean v2, v1, Lantlr/AlternativeBlock;->not:Z

    if-eqz v2, :cond_0

    instance-of v2, v1, Lantlr/SynPredBlock;

    if-nez v2, :cond_0

    instance-of v2, v1, Lantlr/ZeroOrMoreBlock;

    if-nez v2, :cond_0

    instance-of v2, v1, Lantlr/OneOrMoreBlock;

    if-nez v2, :cond_0

    iget-object v2, p0, Lantlr/MakeGrammar;->analyzer:Lantlr/LLkAnalyzer;

    iget-object v3, p0, Lantlr/MakeGrammar;->grammar:Lantlr/Grammar;

    instance-of v3, v3, Lantlr/LexerGrammar;

    invoke-virtual {v2, v1, v3}, Lantlr/LLkAnalyzer;->subruleCanBeInverted(Lantlr/AlternativeBlock;Z)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "line.separator"

    invoke-static {v2}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lantlr/MakeGrammar;->tool:Lantlr/Tool;

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const-string v5, "This subrule cannot be inverted.  Only subrules of the form:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v5, "    (T1|T2|T3...) or"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v5, "    (\'c1\'|\'c2\'|\'c3\'...)"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v4, "may be inverted (ranges are also allowed)."

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, Lantlr/MakeGrammar;->grammar:Lantlr/Grammar;

    invoke-virtual {v4}, Lantlr/Grammar;->getFilename()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1}, Lantlr/AlternativeBlock;->getLine()I

    move-result v5

    invoke-virtual {v1}, Lantlr/AlternativeBlock;->getColumn()I

    move-result v6

    invoke-virtual {v3, v2, v4, v5, v6}, Lantlr/Tool;->error(Ljava/lang/String;Ljava/lang/String;II)V

    :cond_0
    instance-of v2, v1, Lantlr/SynPredBlock;

    if-eqz v2, :cond_1

    check-cast v1, Lantlr/SynPredBlock;

    invoke-virtual {p0}, Lantlr/MakeGrammar;->context()Lantlr/BlockContext;

    move-result-object v2

    iget-object v2, v2, Lantlr/BlockContext;->block:Lantlr/AlternativeBlock;

    iput-boolean v7, v2, Lantlr/AlternativeBlock;->hasASynPred:Z

    invoke-virtual {p0}, Lantlr/MakeGrammar;->context()Lantlr/BlockContext;

    move-result-object v2

    invoke-virtual {v2}, Lantlr/BlockContext;->currentAlt()Lantlr/Alternative;

    move-result-object v2

    iput-object v1, v2, Lantlr/Alternative;->synPred:Lantlr/SynPredBlock;

    iget-object v2, p0, Lantlr/MakeGrammar;->grammar:Lantlr/Grammar;

    iput-boolean v7, v2, Lantlr/Grammar;->hasSyntacticPredicate:Z

    iget-object v2, p0, Lantlr/MakeGrammar;->grammar:Lantlr/Grammar;

    invoke-virtual {v1, v2}, Lantlr/SynPredBlock;->removeTrackingOfRuleRefs(Lantlr/Grammar;)V

    :goto_0
    iget-object v0, v0, Lantlr/BlockContext;->blockEnd:Lantlr/BlockEndElement;

    iget-object v0, v0, Lantlr/BlockEndElement;->block:Lantlr/AlternativeBlock;

    invoke-virtual {v0}, Lantlr/AlternativeBlock;->prepareForAnalysis()V

    return-void

    :cond_1
    invoke-virtual {p0, v1}, Lantlr/MakeGrammar;->addElementToCurrentAlt(Lantlr/AlternativeElement;)V

    goto :goto_0
.end method

.method public endTree()V
    .locals 1

    invoke-super {p0}, Lantlr/DefineGrammarSymbols;->endTree()V

    iget-object v0, p0, Lantlr/MakeGrammar;->blocks:Lantlr/collections/Stack;

    invoke-interface {v0}, Lantlr/collections/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lantlr/BlockContext;

    iget-object v0, v0, Lantlr/BlockContext;->block:Lantlr/AlternativeBlock;

    invoke-virtual {p0, v0}, Lantlr/MakeGrammar;->addElementToCurrentAlt(Lantlr/AlternativeElement;)V

    return-void
.end method

.method public hasError()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lantlr/MakeGrammar;->grammarError:Z

    return-void
.end method

.method public noAutoGenSubRule()V
    .locals 2

    invoke-virtual {p0}, Lantlr/MakeGrammar;->context()Lantlr/BlockContext;

    move-result-object v0

    iget-object v0, v0, Lantlr/BlockContext;->block:Lantlr/AlternativeBlock;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lantlr/AlternativeBlock;->setAutoGen(Z)V

    return-void
.end method

.method public oneOrMoreSubRule()V
    .locals 5

    invoke-virtual {p0}, Lantlr/MakeGrammar;->context()Lantlr/BlockContext;

    move-result-object v0

    iget-object v0, v0, Lantlr/BlockContext;->block:Lantlr/AlternativeBlock;

    iget-boolean v0, v0, Lantlr/AlternativeBlock;->not:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lantlr/MakeGrammar;->tool:Lantlr/Tool;

    const-string v1, "\'~\' cannot be applied to (...)* subrule"

    iget-object v2, p0, Lantlr/MakeGrammar;->grammar:Lantlr/Grammar;

    invoke-virtual {v2}, Lantlr/Grammar;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lantlr/MakeGrammar;->context()Lantlr/BlockContext;

    move-result-object v3

    iget-object v3, v3, Lantlr/BlockContext;->block:Lantlr/AlternativeBlock;

    invoke-virtual {v3}, Lantlr/AlternativeBlock;->getLine()I

    move-result v3

    invoke-virtual {p0}, Lantlr/MakeGrammar;->context()Lantlr/BlockContext;

    move-result-object v4

    iget-object v4, v4, Lantlr/BlockContext;->block:Lantlr/AlternativeBlock;

    invoke-virtual {v4}, Lantlr/AlternativeBlock;->getColumn()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lantlr/Tool;->error(Ljava/lang/String;Ljava/lang/String;II)V

    :cond_0
    new-instance v1, Lantlr/OneOrMoreBlock;

    iget-object v0, p0, Lantlr/MakeGrammar;->grammar:Lantlr/Grammar;

    invoke-direct {v1, v0}, Lantlr/OneOrMoreBlock;-><init>(Lantlr/Grammar;)V

    invoke-virtual {p0}, Lantlr/MakeGrammar;->context()Lantlr/BlockContext;

    move-result-object v0

    iget-object v0, v0, Lantlr/BlockContext;->block:Lantlr/AlternativeBlock;

    invoke-static {v1, v0}, Lantlr/MakeGrammar;->setBlock(Lantlr/AlternativeBlock;Lantlr/AlternativeBlock;)V

    iget-object v0, p0, Lantlr/MakeGrammar;->blocks:Lantlr/collections/Stack;

    invoke-interface {v0}, Lantlr/collections/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lantlr/BlockContext;

    iget-object v2, p0, Lantlr/MakeGrammar;->blocks:Lantlr/collections/Stack;

    new-instance v3, Lantlr/BlockContext;

    invoke-direct {v3}, Lantlr/BlockContext;-><init>()V

    invoke-interface {v2, v3}, Lantlr/collections/Stack;->push(Ljava/lang/Object;)V

    invoke-virtual {p0}, Lantlr/MakeGrammar;->context()Lantlr/BlockContext;

    move-result-object v2

    iput-object v1, v2, Lantlr/BlockContext;->block:Lantlr/AlternativeBlock;

    invoke-virtual {p0}, Lantlr/MakeGrammar;->context()Lantlr/BlockContext;

    move-result-object v2

    iget-object v0, v0, Lantlr/BlockContext;->blockEnd:Lantlr/BlockEndElement;

    iput-object v0, v2, Lantlr/BlockContext;->blockEnd:Lantlr/BlockEndElement;

    invoke-virtual {p0}, Lantlr/MakeGrammar;->context()Lantlr/BlockContext;

    move-result-object v0

    iget-object v0, v0, Lantlr/BlockContext;->blockEnd:Lantlr/BlockEndElement;

    iput-object v1, v0, Lantlr/BlockEndElement;->block:Lantlr/AlternativeBlock;

    return-void
.end method

.method public optionalSubRule()V
    .locals 5

    invoke-virtual {p0}, Lantlr/MakeGrammar;->context()Lantlr/BlockContext;

    move-result-object v0

    iget-object v0, v0, Lantlr/BlockContext;->block:Lantlr/AlternativeBlock;

    iget-boolean v0, v0, Lantlr/AlternativeBlock;->not:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lantlr/MakeGrammar;->tool:Lantlr/Tool;

    const-string v1, "\'~\' cannot be applied to (...)? subrule"

    iget-object v2, p0, Lantlr/MakeGrammar;->grammar:Lantlr/Grammar;

    invoke-virtual {v2}, Lantlr/Grammar;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lantlr/MakeGrammar;->context()Lantlr/BlockContext;

    move-result-object v3

    iget-object v3, v3, Lantlr/BlockContext;->block:Lantlr/AlternativeBlock;

    invoke-virtual {v3}, Lantlr/AlternativeBlock;->getLine()I

    move-result v3

    invoke-virtual {p0}, Lantlr/MakeGrammar;->context()Lantlr/BlockContext;

    move-result-object v4

    iget-object v4, v4, Lantlr/BlockContext;->block:Lantlr/AlternativeBlock;

    invoke-virtual {v4}, Lantlr/AlternativeBlock;->getColumn()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lantlr/Tool;->error(Ljava/lang/String;Ljava/lang/String;II)V

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lantlr/MakeGrammar;->beginAlt(Z)V

    invoke-virtual {p0}, Lantlr/MakeGrammar;->endAlt()V

    return-void
.end method

.method public refAction(Lantlr/Token;)V
    .locals 2

    invoke-super {p0, p1}, Lantlr/DefineGrammarSymbols;->refAction(Lantlr/Token;)V

    invoke-virtual {p0}, Lantlr/MakeGrammar;->context()Lantlr/BlockContext;

    move-result-object v0

    iget-object v0, v0, Lantlr/BlockContext;->block:Lantlr/AlternativeBlock;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lantlr/AlternativeBlock;->hasAnAction:Z

    new-instance v0, Lantlr/ActionElement;

    iget-object v1, p0, Lantlr/MakeGrammar;->grammar:Lantlr/Grammar;

    invoke-direct {v0, v1, p1}, Lantlr/ActionElement;-><init>(Lantlr/Grammar;Lantlr/Token;)V

    invoke-virtual {p0, v0}, Lantlr/MakeGrammar;->addElementToCurrentAlt(Lantlr/AlternativeElement;)V

    return-void
.end method

.method public refArgAction(Lantlr/Token;)V
    .locals 2

    invoke-virtual {p0}, Lantlr/MakeGrammar;->context()Lantlr/BlockContext;

    move-result-object v0

    iget-object v0, v0, Lantlr/BlockContext;->block:Lantlr/AlternativeBlock;

    check-cast v0, Lantlr/RuleBlock;

    invoke-virtual {p1}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lantlr/RuleBlock;->argAction:Ljava/lang/String;

    return-void
.end method

.method public refCharLiteral(Lantlr/Token;Lantlr/Token;ZIZ)V
    .locals 6

    iget-object v0, p0, Lantlr/MakeGrammar;->grammar:Lantlr/Grammar;

    instance-of v0, v0, Lantlr/LexerGrammar;

    if-nez v0, :cond_1

    iget-object v0, p0, Lantlr/MakeGrammar;->tool:Lantlr/Tool;

    const-string v1, "Character literal only valid in lexer"

    iget-object v2, p0, Lantlr/MakeGrammar;->grammar:Lantlr/Grammar;

    invoke-virtual {v2}, Lantlr/Grammar;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lantlr/Token;->getLine()I

    move-result v3

    invoke-virtual {p1}, Lantlr/Token;->getColumn()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lantlr/Tool;->error(Ljava/lang/String;Ljava/lang/String;II)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-super/range {p0 .. p5}, Lantlr/DefineGrammarSymbols;->refCharLiteral(Lantlr/Token;Lantlr/Token;ZIZ)V

    new-instance v1, Lantlr/CharLiteralElement;

    iget-object v0, p0, Lantlr/MakeGrammar;->grammar:Lantlr/Grammar;

    check-cast v0, Lantlr/LexerGrammar;

    invoke-direct {v1, v0, p1, p3, p4}, Lantlr/CharLiteralElement;-><init>(Lantlr/LexerGrammar;Lantlr/Token;ZI)V

    iget-object v0, p0, Lantlr/MakeGrammar;->grammar:Lantlr/Grammar;

    check-cast v0, Lantlr/LexerGrammar;

    iget-boolean v0, v0, Lantlr/LexerGrammar;->caseSensitive:Z

    if-nez v0, :cond_2

    invoke-virtual {v1}, Lantlr/CharLiteralElement;->getType()I

    move-result v0

    const/16 v2, 0x80

    if-ge v0, v2, :cond_2

    invoke-virtual {v1}, Lantlr/CharLiteralElement;->getType()I

    move-result v0

    int-to-char v0, v0

    invoke-static {v0}, Ljava/lang/Character;->toLowerCase(C)C

    move-result v0

    invoke-virtual {v1}, Lantlr/CharLiteralElement;->getType()I

    move-result v2

    int-to-char v2, v2

    if-eq v0, v2, :cond_2

    iget-object v0, p0, Lantlr/MakeGrammar;->tool:Lantlr/Tool;

    const-string v2, "Character literal must be lowercase when caseSensitive=false"

    iget-object v3, p0, Lantlr/MakeGrammar;->grammar:Lantlr/Grammar;

    invoke-virtual {v3}, Lantlr/Grammar;->getFilename()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lantlr/Token;->getLine()I

    move-result v4

    invoke-virtual {p1}, Lantlr/Token;->getColumn()I

    move-result v5

    invoke-virtual {v0, v2, v3, v4, v5}, Lantlr/Tool;->warning(Ljava/lang/String;Ljava/lang/String;II)V

    :cond_2
    invoke-virtual {p0, v1}, Lantlr/MakeGrammar;->addElementToCurrentAlt(Lantlr/AlternativeElement;)V

    invoke-direct {p0, v1, p2}, Lantlr/MakeGrammar;->labelElement(Lantlr/AlternativeElement;Lantlr/Token;)V

    iget-object v0, p0, Lantlr/MakeGrammar;->ruleBlock:Lantlr/RuleBlock;

    invoke-virtual {v0}, Lantlr/RuleBlock;->getIgnoreRule()Ljava/lang/String;

    move-result-object v0

    if-nez p5, :cond_0

    if-eqz v0, :cond_0

    invoke-direct {p0, v0, p1}, Lantlr/MakeGrammar;->createOptionalRuleRef(Ljava/lang/String;Lantlr/Token;)Lantlr/AlternativeBlock;

    move-result-object v0

    invoke-virtual {p0, v0}, Lantlr/MakeGrammar;->addElementToCurrentAlt(Lantlr/AlternativeElement;)V

    goto :goto_0
.end method

.method public refCharRange(Lantlr/Token;Lantlr/Token;Lantlr/Token;IZ)V
    .locals 7

    const/16 v6, 0x80

    iget-object v0, p0, Lantlr/MakeGrammar;->grammar:Lantlr/Grammar;

    instance-of v0, v0, Lantlr/LexerGrammar;

    if-nez v0, :cond_1

    iget-object v0, p0, Lantlr/MakeGrammar;->tool:Lantlr/Tool;

    const-string v1, "Character range only valid in lexer"

    iget-object v2, p0, Lantlr/MakeGrammar;->grammar:Lantlr/Grammar;

    invoke-virtual {v2}, Lantlr/Grammar;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lantlr/Token;->getLine()I

    move-result v3

    invoke-virtual {p1}, Lantlr/Token;->getColumn()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lantlr/Tool;->error(Ljava/lang/String;Ljava/lang/String;II)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lantlr/ANTLRLexer;->tokenTypeForCharLiteral(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p2}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lantlr/ANTLRLexer;->tokenTypeForCharLiteral(Ljava/lang/String;)I

    move-result v2

    if-ge v2, v1, :cond_2

    iget-object v0, p0, Lantlr/MakeGrammar;->tool:Lantlr/Tool;

    const-string v1, "Malformed range."

    iget-object v2, p0, Lantlr/MakeGrammar;->grammar:Lantlr/Grammar;

    invoke-virtual {v2}, Lantlr/Grammar;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lantlr/Token;->getLine()I

    move-result v3

    invoke-virtual {p1}, Lantlr/Token;->getColumn()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lantlr/Tool;->error(Ljava/lang/String;Ljava/lang/String;II)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lantlr/MakeGrammar;->grammar:Lantlr/Grammar;

    check-cast v0, Lantlr/LexerGrammar;

    iget-boolean v0, v0, Lantlr/LexerGrammar;->caseSensitive:Z

    if-nez v0, :cond_4

    if-ge v1, v6, :cond_3

    int-to-char v0, v1

    invoke-static {v0}, Ljava/lang/Character;->toLowerCase(C)C

    move-result v0

    int-to-char v1, v1

    if-eq v0, v1, :cond_3

    iget-object v0, p0, Lantlr/MakeGrammar;->tool:Lantlr/Tool;

    const-string v1, "Character literal must be lowercase when caseSensitive=false"

    iget-object v3, p0, Lantlr/MakeGrammar;->grammar:Lantlr/Grammar;

    invoke-virtual {v3}, Lantlr/Grammar;->getFilename()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lantlr/Token;->getLine()I

    move-result v4

    invoke-virtual {p1}, Lantlr/Token;->getColumn()I

    move-result v5

    invoke-virtual {v0, v1, v3, v4, v5}, Lantlr/Tool;->warning(Ljava/lang/String;Ljava/lang/String;II)V

    :cond_3
    if-ge v2, v6, :cond_4

    int-to-char v0, v2

    invoke-static {v0}, Ljava/lang/Character;->toLowerCase(C)C

    move-result v0

    int-to-char v1, v2

    if-eq v0, v1, :cond_4

    iget-object v0, p0, Lantlr/MakeGrammar;->tool:Lantlr/Tool;

    const-string v1, "Character literal must be lowercase when caseSensitive=false"

    iget-object v2, p0, Lantlr/MakeGrammar;->grammar:Lantlr/Grammar;

    invoke-virtual {v2}, Lantlr/Grammar;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Lantlr/Token;->getLine()I

    move-result v3

    invoke-virtual {p2}, Lantlr/Token;->getColumn()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lantlr/Tool;->warning(Ljava/lang/String;Ljava/lang/String;II)V

    :cond_4
    invoke-super/range {p0 .. p5}, Lantlr/DefineGrammarSymbols;->refCharRange(Lantlr/Token;Lantlr/Token;Lantlr/Token;IZ)V

    new-instance v1, Lantlr/CharRangeElement;

    iget-object v0, p0, Lantlr/MakeGrammar;->grammar:Lantlr/Grammar;

    check-cast v0, Lantlr/LexerGrammar;

    invoke-direct {v1, v0, p1, p2, p4}, Lantlr/CharRangeElement;-><init>(Lantlr/LexerGrammar;Lantlr/Token;Lantlr/Token;I)V

    invoke-virtual {p0, v1}, Lantlr/MakeGrammar;->addElementToCurrentAlt(Lantlr/AlternativeElement;)V

    invoke-direct {p0, v1, p3}, Lantlr/MakeGrammar;->labelElement(Lantlr/AlternativeElement;Lantlr/Token;)V

    iget-object v0, p0, Lantlr/MakeGrammar;->ruleBlock:Lantlr/RuleBlock;

    invoke-virtual {v0}, Lantlr/RuleBlock;->getIgnoreRule()Ljava/lang/String;

    move-result-object v0

    if-nez p5, :cond_0

    if-eqz v0, :cond_0

    invoke-direct {p0, v0, p1}, Lantlr/MakeGrammar;->createOptionalRuleRef(Ljava/lang/String;Lantlr/Token;)Lantlr/AlternativeBlock;

    move-result-object v0

    invoke-virtual {p0, v0}, Lantlr/MakeGrammar;->addElementToCurrentAlt(Lantlr/AlternativeElement;)V

    goto/16 :goto_0
.end method

.method public refElementOption(Lantlr/Token;Lantlr/Token;)V
    .locals 5

    invoke-virtual {p0}, Lantlr/MakeGrammar;->context()Lantlr/BlockContext;

    move-result-object v0

    invoke-virtual {v0}, Lantlr/BlockContext;->currentElement()Lantlr/AlternativeElement;

    move-result-object v0

    instance-of v1, v0, Lantlr/StringLiteralElement;

    if-nez v1, :cond_0

    instance-of v1, v0, Lantlr/TokenRefElement;

    if-nez v1, :cond_0

    instance-of v1, v0, Lantlr/WildcardElement;

    if-eqz v1, :cond_1

    :cond_0
    check-cast v0, Lantlr/GrammarAtom;

    invoke-virtual {v0, p1, p2}, Lantlr/GrammarAtom;->setOption(Lantlr/Token;Lantlr/Token;)V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lantlr/MakeGrammar;->tool:Lantlr/Tool;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "cannot use element option ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p1}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, ") for this kind of element"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lantlr/MakeGrammar;->grammar:Lantlr/Grammar;

    invoke-virtual {v2}, Lantlr/Grammar;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lantlr/Token;->getLine()I

    move-result v3

    invoke-virtual {p1}, Lantlr/Token;->getColumn()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lantlr/Tool;->error(Ljava/lang/String;Ljava/lang/String;II)V

    goto :goto_0
.end method

.method public refExceptionHandler(Lantlr/Token;Lantlr/Token;)V
    .locals 2

    invoke-super {p0, p1, p2}, Lantlr/DefineGrammarSymbols;->refExceptionHandler(Lantlr/Token;Lantlr/Token;)V

    iget-object v0, p0, Lantlr/MakeGrammar;->currentExceptionSpec:Lantlr/ExceptionSpec;

    if-nez v0, :cond_0

    iget-object v0, p0, Lantlr/MakeGrammar;->tool:Lantlr/Tool;

    const-string v1, "exception handler processing internal error"

    invoke-virtual {v0, v1}, Lantlr/Tool;->panic(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lantlr/MakeGrammar;->currentExceptionSpec:Lantlr/ExceptionSpec;

    new-instance v1, Lantlr/ExceptionHandler;

    invoke-direct {v1, p1, p2}, Lantlr/ExceptionHandler;-><init>(Lantlr/Token;Lantlr/Token;)V

    invoke-virtual {v0, v1}, Lantlr/ExceptionSpec;->addHandler(Lantlr/ExceptionHandler;)V

    return-void
.end method

.method public refInitAction(Lantlr/Token;)V
    .locals 2

    invoke-super {p0, p1}, Lantlr/DefineGrammarSymbols;->refAction(Lantlr/Token;)V

    invoke-virtual {p0}, Lantlr/MakeGrammar;->context()Lantlr/BlockContext;

    move-result-object v0

    iget-object v0, v0, Lantlr/BlockContext;->block:Lantlr/AlternativeBlock;

    invoke-virtual {p1}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lantlr/AlternativeBlock;->setInitAction(Ljava/lang/String;)V

    return-void
.end method

.method public refMemberAction(Lantlr/Token;)V
    .locals 1

    iget-object v0, p0, Lantlr/MakeGrammar;->grammar:Lantlr/Grammar;

    iput-object p1, v0, Lantlr/Grammar;->classMemberAction:Lantlr/Token;

    return-void
.end method

.method public refPreambleAction(Lantlr/Token;)V
    .locals 0

    invoke-super {p0, p1}, Lantlr/DefineGrammarSymbols;->refPreambleAction(Lantlr/Token;)V

    return-void
.end method

.method public refReturnAction(Lantlr/Token;)V
    .locals 5

    iget-object v0, p0, Lantlr/MakeGrammar;->grammar:Lantlr/Grammar;

    instance-of v0, v0, Lantlr/LexerGrammar;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lantlr/MakeGrammar;->context()Lantlr/BlockContext;

    move-result-object v0

    iget-object v0, v0, Lantlr/BlockContext;->block:Lantlr/AlternativeBlock;

    check-cast v0, Lantlr/RuleBlock;

    invoke-virtual {v0}, Lantlr/RuleBlock;->getRuleName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lantlr/CodeGenerator;->encodeLexerRuleName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lantlr/MakeGrammar;->grammar:Lantlr/Grammar;

    invoke-virtual {v1, v0}, Lantlr/Grammar;->getSymbol(Ljava/lang/String;)Lantlr/GrammarSymbol;

    move-result-object v0

    check-cast v0, Lantlr/RuleSymbol;

    iget-object v0, v0, Lantlr/RuleSymbol;->access:Ljava/lang/String;

    const-string v1, "public"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lantlr/MakeGrammar;->tool:Lantlr/Tool;

    const-string v1, "public Lexical rules cannot specify return type"

    iget-object v2, p0, Lantlr/MakeGrammar;->grammar:Lantlr/Grammar;

    invoke-virtual {v2}, Lantlr/Grammar;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lantlr/Token;->getLine()I

    move-result v3

    invoke-virtual {p1}, Lantlr/Token;->getColumn()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lantlr/Tool;->warning(Ljava/lang/String;Ljava/lang/String;II)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lantlr/MakeGrammar;->context()Lantlr/BlockContext;

    move-result-object v0

    iget-object v0, v0, Lantlr/BlockContext;->block:Lantlr/AlternativeBlock;

    check-cast v0, Lantlr/RuleBlock;

    invoke-virtual {p1}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lantlr/RuleBlock;->returnAction:Ljava/lang/String;

    goto :goto_0
.end method

.method public refRule(Lantlr/Token;Lantlr/Token;Lantlr/Token;Lantlr/Token;I)V
    .locals 6

    const/16 v5, 0x18

    iget-object v0, p0, Lantlr/MakeGrammar;->grammar:Lantlr/Grammar;

    instance-of v0, v0, Lantlr/LexerGrammar;

    if-eqz v0, :cond_1

    iget v0, p2, Lantlr/Token;->type:I

    if-eq v0, v5, :cond_0

    iget-object v0, p0, Lantlr/MakeGrammar;->tool:Lantlr/Tool;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "Parser rule "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p2}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, " referenced in lexer"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lantlr/Tool;->error(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x2

    if-ne p5, v0, :cond_1

    iget-object v0, p0, Lantlr/MakeGrammar;->tool:Lantlr/Tool;

    const-string v1, "AST specification ^ not allowed in lexer"

    iget-object v2, p0, Lantlr/MakeGrammar;->grammar:Lantlr/Grammar;

    invoke-virtual {v2}, Lantlr/Grammar;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Lantlr/Token;->getLine()I

    move-result v3

    invoke-virtual {p2}, Lantlr/Token;->getColumn()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lantlr/Tool;->error(Ljava/lang/String;Ljava/lang/String;II)V

    :cond_1
    invoke-super/range {p0 .. p5}, Lantlr/DefineGrammarSymbols;->refRule(Lantlr/Token;Lantlr/Token;Lantlr/Token;Lantlr/Token;I)V

    new-instance v0, Lantlr/RuleRefElement;

    iget-object v1, p0, Lantlr/MakeGrammar;->grammar:Lantlr/Grammar;

    invoke-direct {v0, v1, p2, p5}, Lantlr/RuleRefElement;-><init>(Lantlr/Grammar;Lantlr/Token;I)V

    iput-object v0, p0, Lantlr/MakeGrammar;->lastRuleRef:Lantlr/RuleRefElement;

    if-eqz p4, :cond_2

    iget-object v0, p0, Lantlr/MakeGrammar;->lastRuleRef:Lantlr/RuleRefElement;

    invoke-virtual {p4}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lantlr/RuleRefElement;->setArgs(Ljava/lang/String;)V

    :cond_2
    if-eqz p1, :cond_3

    iget-object v0, p0, Lantlr/MakeGrammar;->lastRuleRef:Lantlr/RuleRefElement;

    invoke-virtual {p1}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lantlr/RuleRefElement;->setIdAssign(Ljava/lang/String;)V

    :cond_3
    iget-object v0, p0, Lantlr/MakeGrammar;->lastRuleRef:Lantlr/RuleRefElement;

    invoke-virtual {p0, v0}, Lantlr/MakeGrammar;->addElementToCurrentAlt(Lantlr/AlternativeElement;)V

    invoke-virtual {p2}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v0

    iget v1, p2, Lantlr/Token;->type:I

    if-ne v1, v5, :cond_4

    invoke-static {v0}, Lantlr/CodeGenerator;->encodeLexerRuleName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_4
    iget-object v1, p0, Lantlr/MakeGrammar;->grammar:Lantlr/Grammar;

    invoke-virtual {v1, v0}, Lantlr/Grammar;->getSymbol(Ljava/lang/String;)Lantlr/GrammarSymbol;

    move-result-object v0

    check-cast v0, Lantlr/RuleSymbol;

    iget-object v1, p0, Lantlr/MakeGrammar;->lastRuleRef:Lantlr/RuleRefElement;

    invoke-virtual {v0, v1}, Lantlr/RuleSymbol;->addReference(Lantlr/RuleRefElement;)V

    iget-object v0, p0, Lantlr/MakeGrammar;->lastRuleRef:Lantlr/RuleRefElement;

    invoke-direct {p0, v0, p3}, Lantlr/MakeGrammar;->labelElement(Lantlr/AlternativeElement;Lantlr/Token;)V

    goto :goto_0
.end method

.method public refSemPred(Lantlr/Token;)V
    .locals 2

    invoke-super {p0, p1}, Lantlr/DefineGrammarSymbols;->refSemPred(Lantlr/Token;)V

    invoke-virtual {p0}, Lantlr/MakeGrammar;->context()Lantlr/BlockContext;

    move-result-object v0

    invoke-virtual {v0}, Lantlr/BlockContext;->currentAlt()Lantlr/Alternative;

    move-result-object v0

    invoke-virtual {v0}, Lantlr/Alternative;->atStart()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lantlr/MakeGrammar;->context()Lantlr/BlockContext;

    move-result-object v0

    invoke-virtual {v0}, Lantlr/BlockContext;->currentAlt()Lantlr/Alternative;

    move-result-object v0

    invoke-virtual {p1}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lantlr/Alternative;->semPred:Ljava/lang/String;

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lantlr/ActionElement;

    iget-object v1, p0, Lantlr/MakeGrammar;->grammar:Lantlr/Grammar;

    invoke-direct {v0, v1, p1}, Lantlr/ActionElement;-><init>(Lantlr/Grammar;Lantlr/Token;)V

    const/4 v1, 0x1

    iput-boolean v1, v0, Lantlr/ActionElement;->isSemPred:Z

    invoke-virtual {p0, v0}, Lantlr/MakeGrammar;->addElementToCurrentAlt(Lantlr/AlternativeElement;)V

    goto :goto_0
.end method

.method public refStringLiteral(Lantlr/Token;Lantlr/Token;IZ)V
    .locals 6

    invoke-super {p0, p1, p2, p3, p4}, Lantlr/DefineGrammarSymbols;->refStringLiteral(Lantlr/Token;Lantlr/Token;IZ)V

    iget-object v0, p0, Lantlr/MakeGrammar;->grammar:Lantlr/Grammar;

    instance-of v0, v0, Lantlr/TreeWalkerGrammar;

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    if-ne p3, v0, :cond_0

    iget-object v0, p0, Lantlr/MakeGrammar;->tool:Lantlr/Tool;

    const-string v1, "^ not allowed in here for tree-walker"

    iget-object v2, p0, Lantlr/MakeGrammar;->grammar:Lantlr/Grammar;

    invoke-virtual {v2}, Lantlr/Grammar;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lantlr/Token;->getLine()I

    move-result v3

    invoke-virtual {p1}, Lantlr/Token;->getColumn()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lantlr/Tool;->error(Ljava/lang/String;Ljava/lang/String;II)V

    :cond_0
    new-instance v1, Lantlr/StringLiteralElement;

    iget-object v0, p0, Lantlr/MakeGrammar;->grammar:Lantlr/Grammar;

    invoke-direct {v1, v0, p1, p3}, Lantlr/StringLiteralElement;-><init>(Lantlr/Grammar;Lantlr/Token;I)V

    iget-object v0, p0, Lantlr/MakeGrammar;->grammar:Lantlr/Grammar;

    instance-of v0, v0, Lantlr/LexerGrammar;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lantlr/MakeGrammar;->grammar:Lantlr/Grammar;

    check-cast v0, Lantlr/LexerGrammar;

    iget-boolean v0, v0, Lantlr/LexerGrammar;->caseSensitive:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_1

    invoke-virtual {p1}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x80

    if-ge v2, v3, :cond_3

    invoke-static {v2}, Ljava/lang/Character;->toLowerCase(C)C

    move-result v3

    if-eq v3, v2, :cond_3

    iget-object v0, p0, Lantlr/MakeGrammar;->tool:Lantlr/Tool;

    const-string v2, "Characters of string literal must be lowercase when caseSensitive=false"

    iget-object v3, p0, Lantlr/MakeGrammar;->grammar:Lantlr/Grammar;

    invoke-virtual {v3}, Lantlr/Grammar;->getFilename()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lantlr/Token;->getLine()I

    move-result v4

    invoke-virtual {p1}, Lantlr/Token;->getColumn()I

    move-result v5

    invoke-virtual {v0, v2, v3, v4, v5}, Lantlr/Tool;->warning(Ljava/lang/String;Ljava/lang/String;II)V

    :cond_1
    invoke-virtual {p0, v1}, Lantlr/MakeGrammar;->addElementToCurrentAlt(Lantlr/AlternativeElement;)V

    invoke-direct {p0, v1, p2}, Lantlr/MakeGrammar;->labelElement(Lantlr/AlternativeElement;Lantlr/Token;)V

    iget-object v0, p0, Lantlr/MakeGrammar;->ruleBlock:Lantlr/RuleBlock;

    invoke-virtual {v0}, Lantlr/RuleBlock;->getIgnoreRule()Ljava/lang/String;

    move-result-object v0

    if-nez p4, :cond_2

    if-eqz v0, :cond_2

    invoke-direct {p0, v0, p1}, Lantlr/MakeGrammar;->createOptionalRuleRef(Ljava/lang/String;Lantlr/Token;)Lantlr/AlternativeBlock;

    move-result-object v0

    invoke-virtual {p0, v0}, Lantlr/MakeGrammar;->addElementToCurrentAlt(Lantlr/AlternativeElement;)V

    :cond_2
    return-void

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public refToken(Lantlr/Token;Lantlr/Token;Lantlr/Token;Lantlr/Token;ZIZ)V
    .locals 6

    iget-object v0, p0, Lantlr/MakeGrammar;->grammar:Lantlr/Grammar;

    instance-of v0, v0, Lantlr/LexerGrammar;

    if-eqz v0, :cond_3

    const/4 v0, 0x2

    if-ne p6, v0, :cond_0

    iget-object v0, p0, Lantlr/MakeGrammar;->tool:Lantlr/Tool;

    const-string v1, "AST specification ^ not allowed in lexer"

    iget-object v2, p0, Lantlr/MakeGrammar;->grammar:Lantlr/Grammar;

    invoke-virtual {v2}, Lantlr/Grammar;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Lantlr/Token;->getLine()I

    move-result v3

    invoke-virtual {p2}, Lantlr/Token;->getColumn()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lantlr/Tool;->error(Ljava/lang/String;Ljava/lang/String;II)V

    :cond_0
    if-eqz p5, :cond_1

    iget-object v0, p0, Lantlr/MakeGrammar;->tool:Lantlr/Tool;

    const-string v1, "~TOKEN is not allowed in lexer"

    iget-object v2, p0, Lantlr/MakeGrammar;->grammar:Lantlr/Grammar;

    invoke-virtual {v2}, Lantlr/Grammar;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Lantlr/Token;->getLine()I

    move-result v3

    invoke-virtual {p2}, Lantlr/Token;->getColumn()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lantlr/Tool;->error(Ljava/lang/String;Ljava/lang/String;II)V

    :cond_1
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p6

    invoke-virtual/range {v0 .. v5}, Lantlr/MakeGrammar;->refRule(Lantlr/Token;Lantlr/Token;Lantlr/Token;Lantlr/Token;I)V

    iget-object v0, p0, Lantlr/MakeGrammar;->ruleBlock:Lantlr/RuleBlock;

    invoke-virtual {v0}, Lantlr/RuleBlock;->getIgnoreRule()Ljava/lang/String;

    move-result-object v0

    if-nez p7, :cond_2

    if-eqz v0, :cond_2

    invoke-direct {p0, v0, p2}, Lantlr/MakeGrammar;->createOptionalRuleRef(Ljava/lang/String;Lantlr/Token;)Lantlr/AlternativeBlock;

    move-result-object v0

    invoke-virtual {p0, v0}, Lantlr/MakeGrammar;->addElementToCurrentAlt(Lantlr/AlternativeElement;)V

    :cond_2
    :goto_0
    return-void

    :cond_3
    if-eqz p1, :cond_4

    iget-object v0, p0, Lantlr/MakeGrammar;->tool:Lantlr/Tool;

    const-string v1, "Assignment from token reference only allowed in lexer"

    iget-object v2, p0, Lantlr/MakeGrammar;->grammar:Lantlr/Grammar;

    invoke-virtual {v2}, Lantlr/Grammar;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lantlr/Token;->getLine()I

    move-result v3

    invoke-virtual {p1}, Lantlr/Token;->getColumn()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lantlr/Tool;->error(Ljava/lang/String;Ljava/lang/String;II)V

    :cond_4
    if-eqz p4, :cond_5

    iget-object v0, p0, Lantlr/MakeGrammar;->tool:Lantlr/Tool;

    const-string v1, "Token reference arguments only allowed in lexer"

    iget-object v2, p0, Lantlr/MakeGrammar;->grammar:Lantlr/Grammar;

    invoke-virtual {v2}, Lantlr/Grammar;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p4}, Lantlr/Token;->getLine()I

    move-result v3

    invoke-virtual {p4}, Lantlr/Token;->getColumn()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lantlr/Tool;->error(Ljava/lang/String;Ljava/lang/String;II)V

    :cond_5
    invoke-super/range {p0 .. p7}, Lantlr/DefineGrammarSymbols;->refToken(Lantlr/Token;Lantlr/Token;Lantlr/Token;Lantlr/Token;ZIZ)V

    new-instance v0, Lantlr/TokenRefElement;

    iget-object v1, p0, Lantlr/MakeGrammar;->grammar:Lantlr/Grammar;

    invoke-direct {v0, v1, p2, p5, p6}, Lantlr/TokenRefElement;-><init>(Lantlr/Grammar;Lantlr/Token;ZI)V

    invoke-virtual {p0, v0}, Lantlr/MakeGrammar;->addElementToCurrentAlt(Lantlr/AlternativeElement;)V

    invoke-direct {p0, v0, p3}, Lantlr/MakeGrammar;->labelElement(Lantlr/AlternativeElement;Lantlr/Token;)V

    goto :goto_0
.end method

.method public refTokenRange(Lantlr/Token;Lantlr/Token;Lantlr/Token;IZ)V
    .locals 5

    iget-object v0, p0, Lantlr/MakeGrammar;->grammar:Lantlr/Grammar;

    instance-of v0, v0, Lantlr/LexerGrammar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lantlr/MakeGrammar;->tool:Lantlr/Tool;

    const-string v1, "Token range not allowed in lexer"

    iget-object v2, p0, Lantlr/MakeGrammar;->grammar:Lantlr/Grammar;

    invoke-virtual {v2}, Lantlr/Grammar;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lantlr/Token;->getLine()I

    move-result v3

    invoke-virtual {p1}, Lantlr/Token;->getColumn()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lantlr/Tool;->error(Ljava/lang/String;Ljava/lang/String;II)V

    :goto_0
    return-void

    :cond_0
    invoke-super/range {p0 .. p5}, Lantlr/DefineGrammarSymbols;->refTokenRange(Lantlr/Token;Lantlr/Token;Lantlr/Token;IZ)V

    new-instance v0, Lantlr/TokenRangeElement;

    iget-object v1, p0, Lantlr/MakeGrammar;->grammar:Lantlr/Grammar;

    invoke-direct {v0, v1, p1, p2, p4}, Lantlr/TokenRangeElement;-><init>(Lantlr/Grammar;Lantlr/Token;Lantlr/Token;I)V

    iget v1, v0, Lantlr/TokenRangeElement;->end:I

    iget v2, v0, Lantlr/TokenRangeElement;->begin:I

    if-ge v1, v2, :cond_1

    iget-object v0, p0, Lantlr/MakeGrammar;->tool:Lantlr/Tool;

    const-string v1, "Malformed range."

    iget-object v2, p0, Lantlr/MakeGrammar;->grammar:Lantlr/Grammar;

    invoke-virtual {v2}, Lantlr/Grammar;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lantlr/Token;->getLine()I

    move-result v3

    invoke-virtual {p1}, Lantlr/Token;->getColumn()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lantlr/Tool;->error(Ljava/lang/String;Ljava/lang/String;II)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0, v0}, Lantlr/MakeGrammar;->addElementToCurrentAlt(Lantlr/AlternativeElement;)V

    invoke-direct {p0, v0, p3}, Lantlr/MakeGrammar;->labelElement(Lantlr/AlternativeElement;Lantlr/Token;)V

    goto :goto_0
.end method

.method public refTokensSpecElementOption(Lantlr/Token;Lantlr/Token;Lantlr/Token;)V
    .locals 5

    iget-object v0, p0, Lantlr/MakeGrammar;->grammar:Lantlr/Grammar;

    iget-object v0, v0, Lantlr/Grammar;->tokenManager:Lantlr/TokenManager;

    invoke-virtual {p1}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lantlr/TokenManager;->getTokenSymbol(Ljava/lang/String;)Lantlr/TokenSymbol;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v1, p0, Lantlr/MakeGrammar;->tool:Lantlr/Tool;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "cannot find "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {p1}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, "in tokens {...}"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lantlr/Tool;->panic(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p2}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v1

    const-string v2, "AST"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p3}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lantlr/TokenSymbol;->setASTNodeType(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lantlr/MakeGrammar;->grammar:Lantlr/Grammar;

    iget-object v0, v0, Lantlr/Grammar;->antlrTool:Lantlr/Tool;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "invalid tokens {...} element option:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p2}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lantlr/MakeGrammar;->grammar:Lantlr/Grammar;

    invoke-virtual {v2}, Lantlr/Grammar;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Lantlr/Token;->getLine()I

    move-result v3

    invoke-virtual {p2}, Lantlr/Token;->getColumn()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lantlr/Tool;->error(Ljava/lang/String;Ljava/lang/String;II)V

    goto :goto_0
.end method

.method public refTreeSpecifier(Lantlr/Token;)V
    .locals 1

    invoke-virtual {p0}, Lantlr/MakeGrammar;->context()Lantlr/BlockContext;

    move-result-object v0

    invoke-virtual {v0}, Lantlr/BlockContext;->currentAlt()Lantlr/Alternative;

    move-result-object v0

    iput-object p1, v0, Lantlr/Alternative;->treeSpecifier:Lantlr/Token;

    return-void
.end method

.method public refWildcard(Lantlr/Token;Lantlr/Token;I)V
    .locals 2

    invoke-super {p0, p1, p2, p3}, Lantlr/DefineGrammarSymbols;->refWildcard(Lantlr/Token;Lantlr/Token;I)V

    new-instance v0, Lantlr/WildcardElement;

    iget-object v1, p0, Lantlr/MakeGrammar;->grammar:Lantlr/Grammar;

    invoke-direct {v0, v1, p1, p3}, Lantlr/WildcardElement;-><init>(Lantlr/Grammar;Lantlr/Token;I)V

    invoke-virtual {p0, v0}, Lantlr/MakeGrammar;->addElementToCurrentAlt(Lantlr/AlternativeElement;)V

    invoke-direct {p0, v0, p2}, Lantlr/MakeGrammar;->labelElement(Lantlr/AlternativeElement;Lantlr/Token;)V

    return-void
.end method

.method public reset()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-super {p0}, Lantlr/DefineGrammarSymbols;->reset()V

    new-instance v0, Lantlr/collections/impl/LList;

    invoke-direct {v0}, Lantlr/collections/impl/LList;-><init>()V

    iput-object v0, p0, Lantlr/MakeGrammar;->blocks:Lantlr/collections/Stack;

    iput-object v1, p0, Lantlr/MakeGrammar;->lastRuleRef:Lantlr/RuleRefElement;

    iput-object v1, p0, Lantlr/MakeGrammar;->ruleEnd:Lantlr/RuleEndElement;

    iput-object v1, p0, Lantlr/MakeGrammar;->ruleBlock:Lantlr/RuleBlock;

    iput v2, p0, Lantlr/MakeGrammar;->nested:I

    iput-object v1, p0, Lantlr/MakeGrammar;->currentExceptionSpec:Lantlr/ExceptionSpec;

    iput-boolean v2, p0, Lantlr/MakeGrammar;->grammarError:Z

    return-void
.end method

.method public setArgOfRuleRef(Lantlr/Token;)V
    .locals 2

    invoke-super {p0, p1}, Lantlr/DefineGrammarSymbols;->setArgOfRuleRef(Lantlr/Token;)V

    iget-object v0, p0, Lantlr/MakeGrammar;->lastRuleRef:Lantlr/RuleRefElement;

    invoke-virtual {p1}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lantlr/RuleRefElement;->setArgs(Ljava/lang/String;)V

    return-void
.end method

.method public setRuleOption(Lantlr/Token;Lantlr/Token;)V
    .locals 1

    iget-object v0, p0, Lantlr/MakeGrammar;->ruleBlock:Lantlr/RuleBlock;

    invoke-virtual {v0, p1, p2}, Lantlr/RuleBlock;->setOption(Lantlr/Token;Lantlr/Token;)V

    return-void
.end method

.method public setSubruleOption(Lantlr/Token;Lantlr/Token;)V
    .locals 1

    invoke-virtual {p0}, Lantlr/MakeGrammar;->context()Lantlr/BlockContext;

    move-result-object v0

    iget-object v0, v0, Lantlr/BlockContext;->block:Lantlr/AlternativeBlock;

    invoke-virtual {v0, p1, p2}, Lantlr/AlternativeBlock;->setOption(Lantlr/Token;Lantlr/Token;)V

    return-void
.end method

.method public setUserExceptions(Ljava/lang/String;)V
    .locals 1

    invoke-virtual {p0}, Lantlr/MakeGrammar;->context()Lantlr/BlockContext;

    move-result-object v0

    iget-object v0, v0, Lantlr/BlockContext;->block:Lantlr/AlternativeBlock;

    check-cast v0, Lantlr/RuleBlock;

    iput-object p1, v0, Lantlr/RuleBlock;->throwsSpec:Ljava/lang/String;

    return-void
.end method

.method public synPred()V
    .locals 5

    invoke-virtual {p0}, Lantlr/MakeGrammar;->context()Lantlr/BlockContext;

    move-result-object v0

    iget-object v0, v0, Lantlr/BlockContext;->block:Lantlr/AlternativeBlock;

    iget-boolean v0, v0, Lantlr/AlternativeBlock;->not:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lantlr/MakeGrammar;->tool:Lantlr/Tool;

    const-string v1, "\'~\' cannot be applied to syntactic predicate"

    iget-object v2, p0, Lantlr/MakeGrammar;->grammar:Lantlr/Grammar;

    invoke-virtual {v2}, Lantlr/Grammar;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lantlr/MakeGrammar;->context()Lantlr/BlockContext;

    move-result-object v3

    iget-object v3, v3, Lantlr/BlockContext;->block:Lantlr/AlternativeBlock;

    invoke-virtual {v3}, Lantlr/AlternativeBlock;->getLine()I

    move-result v3

    invoke-virtual {p0}, Lantlr/MakeGrammar;->context()Lantlr/BlockContext;

    move-result-object v4

    iget-object v4, v4, Lantlr/BlockContext;->block:Lantlr/AlternativeBlock;

    invoke-virtual {v4}, Lantlr/AlternativeBlock;->getColumn()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lantlr/Tool;->error(Ljava/lang/String;Ljava/lang/String;II)V

    :cond_0
    new-instance v1, Lantlr/SynPredBlock;

    iget-object v0, p0, Lantlr/MakeGrammar;->grammar:Lantlr/Grammar;

    invoke-direct {v1, v0}, Lantlr/SynPredBlock;-><init>(Lantlr/Grammar;)V

    invoke-virtual {p0}, Lantlr/MakeGrammar;->context()Lantlr/BlockContext;

    move-result-object v0

    iget-object v0, v0, Lantlr/BlockContext;->block:Lantlr/AlternativeBlock;

    invoke-static {v1, v0}, Lantlr/MakeGrammar;->setBlock(Lantlr/AlternativeBlock;Lantlr/AlternativeBlock;)V

    iget-object v0, p0, Lantlr/MakeGrammar;->blocks:Lantlr/collections/Stack;

    invoke-interface {v0}, Lantlr/collections/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lantlr/BlockContext;

    iget-object v2, p0, Lantlr/MakeGrammar;->blocks:Lantlr/collections/Stack;

    new-instance v3, Lantlr/BlockContext;

    invoke-direct {v3}, Lantlr/BlockContext;-><init>()V

    invoke-interface {v2, v3}, Lantlr/collections/Stack;->push(Ljava/lang/Object;)V

    invoke-virtual {p0}, Lantlr/MakeGrammar;->context()Lantlr/BlockContext;

    move-result-object v2

    iput-object v1, v2, Lantlr/BlockContext;->block:Lantlr/AlternativeBlock;

    invoke-virtual {p0}, Lantlr/MakeGrammar;->context()Lantlr/BlockContext;

    move-result-object v2

    iget-object v0, v0, Lantlr/BlockContext;->blockEnd:Lantlr/BlockEndElement;

    iput-object v0, v2, Lantlr/BlockContext;->blockEnd:Lantlr/BlockEndElement;

    invoke-virtual {p0}, Lantlr/MakeGrammar;->context()Lantlr/BlockContext;

    move-result-object v0

    iget-object v0, v0, Lantlr/BlockContext;->blockEnd:Lantlr/BlockEndElement;

    iput-object v1, v0, Lantlr/BlockEndElement;->block:Lantlr/AlternativeBlock;

    return-void
.end method

.method public zeroOrMoreSubRule()V
    .locals 5

    invoke-virtual {p0}, Lantlr/MakeGrammar;->context()Lantlr/BlockContext;

    move-result-object v0

    iget-object v0, v0, Lantlr/BlockContext;->block:Lantlr/AlternativeBlock;

    iget-boolean v0, v0, Lantlr/AlternativeBlock;->not:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lantlr/MakeGrammar;->tool:Lantlr/Tool;

    const-string v1, "\'~\' cannot be applied to (...)+ subrule"

    iget-object v2, p0, Lantlr/MakeGrammar;->grammar:Lantlr/Grammar;

    invoke-virtual {v2}, Lantlr/Grammar;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lantlr/MakeGrammar;->context()Lantlr/BlockContext;

    move-result-object v3

    iget-object v3, v3, Lantlr/BlockContext;->block:Lantlr/AlternativeBlock;

    invoke-virtual {v3}, Lantlr/AlternativeBlock;->getLine()I

    move-result v3

    invoke-virtual {p0}, Lantlr/MakeGrammar;->context()Lantlr/BlockContext;

    move-result-object v4

    iget-object v4, v4, Lantlr/BlockContext;->block:Lantlr/AlternativeBlock;

    invoke-virtual {v4}, Lantlr/AlternativeBlock;->getColumn()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lantlr/Tool;->error(Ljava/lang/String;Ljava/lang/String;II)V

    :cond_0
    new-instance v1, Lantlr/ZeroOrMoreBlock;

    iget-object v0, p0, Lantlr/MakeGrammar;->grammar:Lantlr/Grammar;

    invoke-direct {v1, v0}, Lantlr/ZeroOrMoreBlock;-><init>(Lantlr/Grammar;)V

    invoke-virtual {p0}, Lantlr/MakeGrammar;->context()Lantlr/BlockContext;

    move-result-object v0

    iget-object v0, v0, Lantlr/BlockContext;->block:Lantlr/AlternativeBlock;

    invoke-static {v1, v0}, Lantlr/MakeGrammar;->setBlock(Lantlr/AlternativeBlock;Lantlr/AlternativeBlock;)V

    iget-object v0, p0, Lantlr/MakeGrammar;->blocks:Lantlr/collections/Stack;

    invoke-interface {v0}, Lantlr/collections/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lantlr/BlockContext;

    iget-object v2, p0, Lantlr/MakeGrammar;->blocks:Lantlr/collections/Stack;

    new-instance v3, Lantlr/BlockContext;

    invoke-direct {v3}, Lantlr/BlockContext;-><init>()V

    invoke-interface {v2, v3}, Lantlr/collections/Stack;->push(Ljava/lang/Object;)V

    invoke-virtual {p0}, Lantlr/MakeGrammar;->context()Lantlr/BlockContext;

    move-result-object v2

    iput-object v1, v2, Lantlr/BlockContext;->block:Lantlr/AlternativeBlock;

    invoke-virtual {p0}, Lantlr/MakeGrammar;->context()Lantlr/BlockContext;

    move-result-object v2

    iget-object v0, v0, Lantlr/BlockContext;->blockEnd:Lantlr/BlockEndElement;

    iput-object v0, v2, Lantlr/BlockContext;->blockEnd:Lantlr/BlockEndElement;

    invoke-virtual {p0}, Lantlr/MakeGrammar;->context()Lantlr/BlockContext;

    move-result-object v0

    iget-object v0, v0, Lantlr/BlockContext;->blockEnd:Lantlr/BlockEndElement;

    iput-object v1, v0, Lantlr/BlockEndElement;->block:Lantlr/AlternativeBlock;

    return-void
.end method
