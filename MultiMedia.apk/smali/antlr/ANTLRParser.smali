.class public Lantlr/ANTLRParser;
.super Lantlr/LLkParser;
.source "ANTLRParser.java"

# interfaces
.implements Lantlr/ANTLRTokenTypes;


# static fields
.field private static final DEBUG_PARSER:Z

.field public static final _tokenNames:[Ljava/lang/String;

.field public static final _tokenSet_0:Lantlr/collections/impl/BitSet;

.field public static final _tokenSet_1:Lantlr/collections/impl/BitSet;

.field public static final _tokenSet_10:Lantlr/collections/impl/BitSet;

.field public static final _tokenSet_11:Lantlr/collections/impl/BitSet;

.field public static final _tokenSet_2:Lantlr/collections/impl/BitSet;

.field public static final _tokenSet_3:Lantlr/collections/impl/BitSet;

.field public static final _tokenSet_4:Lantlr/collections/impl/BitSet;

.field public static final _tokenSet_5:Lantlr/collections/impl/BitSet;

.field public static final _tokenSet_6:Lantlr/collections/impl/BitSet;

.field public static final _tokenSet_7:Lantlr/collections/impl/BitSet;

.field public static final _tokenSet_8:Lantlr/collections/impl/BitSet;

.field public static final _tokenSet_9:Lantlr/collections/impl/BitSet;


# instance fields
.field antlrTool:Lantlr/Tool;

.field behavior:Lantlr/ANTLRGrammarParseBehavior;

.field protected blockNesting:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/16 v0, 0x40

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "<0>"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "EOF"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "<2>"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "NULL_TREE_LOOKAHEAD"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "\"tokens\""

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "\"header\""

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "STRING_LITERAL"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "ACTION"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "DOC_COMMENT"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "\"lexclass\""

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "\"class\""

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "\"extends\""

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "\"Lexer\""

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "\"TreeParser\""

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "OPTIONS"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "ASSIGN"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "SEMI"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "RCURLY"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "\"charVocabulary\""

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "CHAR_LITERAL"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "INT"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "OR"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "RANGE"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "TOKENS"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "TOKEN_REF"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "OPEN_ELEMENT_OPTION"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "CLOSE_ELEMENT_OPTION"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "LPAREN"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "RPAREN"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "\"Parser\""

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, "\"protected\""

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, "\"public\""

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, "\"private\""

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string v2, "BANG"

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string v2, "ARG_ACTION"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string v2, "\"returns\""

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const-string v2, "COLON"

    aput-object v2, v0, v1

    const/16 v1, 0x25

    const-string v2, "\"throws\""

    aput-object v2, v0, v1

    const/16 v1, 0x26

    const-string v2, "COMMA"

    aput-object v2, v0, v1

    const/16 v1, 0x27

    const-string v2, "\"exception\""

    aput-object v2, v0, v1

    const/16 v1, 0x28

    const-string v2, "\"catch\""

    aput-object v2, v0, v1

    const/16 v1, 0x29

    const-string v2, "RULE_REF"

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    const-string v2, "NOT_OP"

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    const-string v2, "SEMPRED"

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    const-string v2, "TREE_BEGIN"

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    const-string v2, "QUESTION"

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    const-string v2, "STAR"

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    const-string v2, "PLUS"

    aput-object v2, v0, v1

    const/16 v1, 0x30

    const-string v2, "IMPLIES"

    aput-object v2, v0, v1

    const/16 v1, 0x31

    const-string v2, "CARET"

    aput-object v2, v0, v1

    const/16 v1, 0x32

    const-string v2, "WILDCARD"

    aput-object v2, v0, v1

    const/16 v1, 0x33

    const-string v2, "\"options\""

    aput-object v2, v0, v1

    const/16 v1, 0x34

    const-string v2, "WS"

    aput-object v2, v0, v1

    const/16 v1, 0x35

    const-string v2, "COMMENT"

    aput-object v2, v0, v1

    const/16 v1, 0x36

    const-string v2, "SL_COMMENT"

    aput-object v2, v0, v1

    const/16 v1, 0x37

    const-string v2, "ML_COMMENT"

    aput-object v2, v0, v1

    const/16 v1, 0x38

    const-string v2, "ESC"

    aput-object v2, v0, v1

    const/16 v1, 0x39

    const-string v2, "DIGIT"

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    const-string v2, "XDIGIT"

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    const-string v2, "NESTED_ARG_ACTION"

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    const-string v2, "NESTED_ACTION"

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    const-string v2, "WS_LOOP"

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    const-string v2, "INTERNAL_RULE_REF"

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    const-string v2, "WS_OPT"

    aput-object v2, v0, v1

    sput-object v0, Lantlr/ANTLRParser;->_tokenNames:[Ljava/lang/String;

    new-instance v0, Lantlr/collections/impl/BitSet;

    invoke-static {}, Lantlr/ANTLRParser;->mk_tokenSet_0()[J

    move-result-object v1

    invoke-direct {v0, v1}, Lantlr/collections/impl/BitSet;-><init>([J)V

    sput-object v0, Lantlr/ANTLRParser;->_tokenSet_0:Lantlr/collections/impl/BitSet;

    new-instance v0, Lantlr/collections/impl/BitSet;

    invoke-static {}, Lantlr/ANTLRParser;->mk_tokenSet_1()[J

    move-result-object v1

    invoke-direct {v0, v1}, Lantlr/collections/impl/BitSet;-><init>([J)V

    sput-object v0, Lantlr/ANTLRParser;->_tokenSet_1:Lantlr/collections/impl/BitSet;

    new-instance v0, Lantlr/collections/impl/BitSet;

    invoke-static {}, Lantlr/ANTLRParser;->mk_tokenSet_2()[J

    move-result-object v1

    invoke-direct {v0, v1}, Lantlr/collections/impl/BitSet;-><init>([J)V

    sput-object v0, Lantlr/ANTLRParser;->_tokenSet_2:Lantlr/collections/impl/BitSet;

    new-instance v0, Lantlr/collections/impl/BitSet;

    invoke-static {}, Lantlr/ANTLRParser;->mk_tokenSet_3()[J

    move-result-object v1

    invoke-direct {v0, v1}, Lantlr/collections/impl/BitSet;-><init>([J)V

    sput-object v0, Lantlr/ANTLRParser;->_tokenSet_3:Lantlr/collections/impl/BitSet;

    new-instance v0, Lantlr/collections/impl/BitSet;

    invoke-static {}, Lantlr/ANTLRParser;->mk_tokenSet_4()[J

    move-result-object v1

    invoke-direct {v0, v1}, Lantlr/collections/impl/BitSet;-><init>([J)V

    sput-object v0, Lantlr/ANTLRParser;->_tokenSet_4:Lantlr/collections/impl/BitSet;

    new-instance v0, Lantlr/collections/impl/BitSet;

    invoke-static {}, Lantlr/ANTLRParser;->mk_tokenSet_5()[J

    move-result-object v1

    invoke-direct {v0, v1}, Lantlr/collections/impl/BitSet;-><init>([J)V

    sput-object v0, Lantlr/ANTLRParser;->_tokenSet_5:Lantlr/collections/impl/BitSet;

    new-instance v0, Lantlr/collections/impl/BitSet;

    invoke-static {}, Lantlr/ANTLRParser;->mk_tokenSet_6()[J

    move-result-object v1

    invoke-direct {v0, v1}, Lantlr/collections/impl/BitSet;-><init>([J)V

    sput-object v0, Lantlr/ANTLRParser;->_tokenSet_6:Lantlr/collections/impl/BitSet;

    new-instance v0, Lantlr/collections/impl/BitSet;

    invoke-static {}, Lantlr/ANTLRParser;->mk_tokenSet_7()[J

    move-result-object v1

    invoke-direct {v0, v1}, Lantlr/collections/impl/BitSet;-><init>([J)V

    sput-object v0, Lantlr/ANTLRParser;->_tokenSet_7:Lantlr/collections/impl/BitSet;

    new-instance v0, Lantlr/collections/impl/BitSet;

    invoke-static {}, Lantlr/ANTLRParser;->mk_tokenSet_8()[J

    move-result-object v1

    invoke-direct {v0, v1}, Lantlr/collections/impl/BitSet;-><init>([J)V

    sput-object v0, Lantlr/ANTLRParser;->_tokenSet_8:Lantlr/collections/impl/BitSet;

    new-instance v0, Lantlr/collections/impl/BitSet;

    invoke-static {}, Lantlr/ANTLRParser;->mk_tokenSet_9()[J

    move-result-object v1

    invoke-direct {v0, v1}, Lantlr/collections/impl/BitSet;-><init>([J)V

    sput-object v0, Lantlr/ANTLRParser;->_tokenSet_9:Lantlr/collections/impl/BitSet;

    new-instance v0, Lantlr/collections/impl/BitSet;

    invoke-static {}, Lantlr/ANTLRParser;->mk_tokenSet_10()[J

    move-result-object v1

    invoke-direct {v0, v1}, Lantlr/collections/impl/BitSet;-><init>([J)V

    sput-object v0, Lantlr/ANTLRParser;->_tokenSet_10:Lantlr/collections/impl/BitSet;

    new-instance v0, Lantlr/collections/impl/BitSet;

    invoke-static {}, Lantlr/ANTLRParser;->mk_tokenSet_11()[J

    move-result-object v1

    invoke-direct {v0, v1}, Lantlr/collections/impl/BitSet;-><init>([J)V

    sput-object v0, Lantlr/ANTLRParser;->_tokenSet_11:Lantlr/collections/impl/BitSet;

    return-void
.end method

.method public constructor <init>(Lantlr/ParserSharedInputState;)V
    .locals 1

    const/4 v0, 0x2

    invoke-direct {p0, p1, v0}, Lantlr/LLkParser;-><init>(Lantlr/ParserSharedInputState;I)V

    const/4 v0, -0x1

    iput v0, p0, Lantlr/ANTLRParser;->blockNesting:I

    sget-object v0, Lantlr/ANTLRParser;->_tokenNames:[Ljava/lang/String;

    iput-object v0, p0, Lantlr/ANTLRParser;->tokenNames:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lantlr/TokenBuffer;)V
    .locals 1

    const/4 v0, 0x2

    invoke-direct {p0, p1, v0}, Lantlr/ANTLRParser;-><init>(Lantlr/TokenBuffer;I)V

    return-void
.end method

.method protected constructor <init>(Lantlr/TokenBuffer;I)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lantlr/LLkParser;-><init>(Lantlr/TokenBuffer;I)V

    const/4 v0, -0x1

    iput v0, p0, Lantlr/ANTLRParser;->blockNesting:I

    sget-object v0, Lantlr/ANTLRParser;->_tokenNames:[Ljava/lang/String;

    iput-object v0, p0, Lantlr/ANTLRParser;->tokenNames:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lantlr/TokenBuffer;Lantlr/ANTLRGrammarParseBehavior;Lantlr/Tool;)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lantlr/LLkParser;-><init>(Lantlr/TokenBuffer;I)V

    const/4 v0, -0x1

    iput v0, p0, Lantlr/ANTLRParser;->blockNesting:I

    sget-object v0, Lantlr/ANTLRParser;->_tokenNames:[Ljava/lang/String;

    iput-object v0, p0, Lantlr/ANTLRParser;->tokenNames:[Ljava/lang/String;

    iput-object p2, p0, Lantlr/ANTLRParser;->behavior:Lantlr/ANTLRGrammarParseBehavior;

    iput-object p3, p0, Lantlr/ANTLRParser;->antlrTool:Lantlr/Tool;

    return-void
.end method

.method public constructor <init>(Lantlr/TokenStream;)V
    .locals 1

    const/4 v0, 0x2

    invoke-direct {p0, p1, v0}, Lantlr/ANTLRParser;-><init>(Lantlr/TokenStream;I)V

    return-void
.end method

.method protected constructor <init>(Lantlr/TokenStream;I)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lantlr/LLkParser;-><init>(Lantlr/TokenStream;I)V

    const/4 v0, -0x1

    iput v0, p0, Lantlr/ANTLRParser;->blockNesting:I

    sget-object v0, Lantlr/ANTLRParser;->_tokenNames:[Ljava/lang/String;

    iput-object v0, p0, Lantlr/ANTLRParser;->tokenNames:[Ljava/lang/String;

    return-void
.end method

.method private checkForMissingEndRule(Lantlr/Token;)V
    .locals 5

    invoke-virtual {p1}, Lantlr/Token;->getColumn()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lantlr/ANTLRParser;->antlrTool:Lantlr/Tool;

    const-string v1, "did you forget to terminate previous rule?"

    invoke-virtual {p0}, Lantlr/ANTLRParser;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lantlr/Token;->getLine()I

    move-result v3

    invoke-virtual {p1}, Lantlr/Token;->getColumn()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lantlr/Tool;->warning(Ljava/lang/String;Ljava/lang/String;II)V

    :cond_0
    return-void
.end method

.method private lastInRule()Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/4 v0, 0x1

    iget v1, p0, Lantlr/ANTLRParser;->blockNesting:I

    if-nez v1, :cond_1

    invoke-virtual {p0, v0}, Lantlr/ANTLRParser;->LA(I)I

    move-result v1

    const/16 v2, 0x10

    if-eq v1, v2, :cond_0

    invoke-virtual {p0, v0}, Lantlr/ANTLRParser;->LA(I)I

    move-result v1

    const/16 v2, 0x27

    if-eq v1, v2, :cond_0

    invoke-virtual {p0, v0}, Lantlr/ANTLRParser;->LA(I)I

    move-result v1

    const/16 v2, 0x15

    if-ne v1, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static final mk_tokenSet_0()[J
    .locals 1

    const/4 v0, 0x2

    new-array v0, v0, [J

    fill-array-data v0, :array_0

    return-object v0

    nop

    :array_0
    .array-data 8
        0x201c1000100L
        0x0
    .end array-data
.end method

.method private static final mk_tokenSet_1()[J
    .locals 1

    const/4 v0, 0x2

    new-array v0, v0, [J

    fill-array-data v0, :array_0

    return-object v0

    nop

    :array_0
    .array-data 8
        0x23fc1004080L
        0x0
    .end array-data
.end method

.method private static final mk_tokenSet_10()[J
    .locals 1

    const/4 v0, 0x2

    new-array v0, v0, [J

    fill-array-data v0, :array_0

    return-object v0

    nop

    :array_0
    .array-data 8
        0x7ff961b69c0c0L
        0x0
    .end array-data
.end method

.method private static final mk_tokenSet_11()[J
    .locals 1

    const/4 v0, 0x2

    new-array v0, v0, [J

    fill-array-data v0, :array_0

    return-object v0

    nop

    :array_0
    .array-data 8
        0x61e06090800c0L
        0x0
    .end array-data
.end method

.method private static final mk_tokenSet_2()[J
    .locals 1

    const/4 v0, 0x2

    new-array v0, v0, [J

    fill-array-data v0, :array_0

    return-object v0

    nop

    :array_0
    .array-data 8
        0x41e00090800c0L
        0x0
    .end array-data
.end method

.method private static final mk_tokenSet_3()[J
    .locals 1

    const/4 v0, 0x2

    new-array v0, v0, [J

    fill-array-data v0, :array_0

    return-object v0

    nop

    :array_0
    .array-data 8
        0x41e861b2900c0L
        0x0
    .end array-data
.end method

.method private static final mk_tokenSet_4()[J
    .locals 1

    const/4 v0, 0x2

    new-array v0, v0, [J

    fill-array-data v0, :array_0

    return-object v0

    nop

    :array_0
    .array-data 8
        0x4060009080040L
        0x0
    .end array-data
.end method

.method private static final mk_tokenSet_5()[J
    .locals 1

    const/4 v0, 0x2

    new-array v0, v0, [J

    fill-array-data v0, :array_0

    return-object v0

    nop

    :array_0
    .array-data 8
        0x61e961b6940c0L
        0x0
    .end array-data
.end method

.method private static final mk_tokenSet_6()[J
    .locals 1

    const/4 v0, 0x2

    new-array v0, v0, [J

    fill-array-data v0, :array_0

    return-object v0

    nop

    :array_0
    .array-data 8
        0x61e861b6940c0L
        0x0
    .end array-data
.end method

.method private static final mk_tokenSet_7()[J
    .locals 1

    const/4 v0, 0x2

    new-array v0, v0, [J

    fill-array-data v0, :array_0

    return-object v0

    nop

    :array_0
    .array-data 8
        0x4000001080040L
        0x0
    .end array-data
.end method

.method private static final mk_tokenSet_8()[J
    .locals 1

    const/4 v0, 0x2

    new-array v0, v0, [J

    fill-array-data v0, :array_0

    return-object v0

    nop

    :array_0
    .array-data 8
        0x61e861b2900c0L
        0x0
    .end array-data
.end method

.method private static final mk_tokenSet_9()[J
    .locals 1

    const/4 v0, 0x2

    new-array v0, v0, [J

    fill-array-data v0, :array_0

    return-object v0

    nop

    :array_0
    .array-data 8
        0x41e82192800c0L
        0x0
    .end array-data
.end method


# virtual methods
.method public final alternative()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/RecognitionException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lantlr/ANTLRParser;->LA(I)I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    new-instance v0, Lantlr/NoViableAltException;

    invoke-virtual {p0, v1}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v1

    invoke-virtual {p0}, Lantlr/ANTLRParser;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lantlr/NoViableAltException;-><init>(Lantlr/Token;Ljava/lang/String;)V

    throw v0

    :sswitch_0
    const/16 v0, 0x21

    invoke-virtual {p0, v0}, Lantlr/ANTLRParser;->match(I)V

    iget-object v0, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v0, v0, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v0, :cond_3

    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v2, v2, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v2, :cond_0

    iget-object v2, p0, Lantlr/ANTLRParser;->behavior:Lantlr/ANTLRGrammarParseBehavior;

    invoke-interface {v2, v0}, Lantlr/ANTLRGrammarParseBehavior;->beginAlt(Z)V

    :cond_0
    :goto_1
    sget-object v0, Lantlr/ANTLRParser;->_tokenSet_2:Lantlr/collections/impl/BitSet;

    invoke-virtual {p0, v1}, Lantlr/ANTLRParser;->LA(I)I

    move-result v2

    invoke-virtual {v0, v2}, Lantlr/collections/impl/BitSet;->member(I)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lantlr/ANTLRParser;->element()V

    goto :goto_1

    :sswitch_1
    move v0, v1

    goto :goto_0

    :cond_1
    invoke-virtual {p0, v1}, Lantlr/ANTLRParser;->LA(I)I

    move-result v0

    sparse-switch v0, :sswitch_data_1

    new-instance v0, Lantlr/NoViableAltException;

    invoke-virtual {p0, v1}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v1

    invoke-virtual {p0}, Lantlr/ANTLRParser;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lantlr/NoViableAltException;-><init>(Lantlr/Token;Ljava/lang/String;)V

    throw v0

    :sswitch_2
    invoke-virtual {p0}, Lantlr/ANTLRParser;->exceptionSpecNoLabel()V

    :sswitch_3
    iget-object v0, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v0, v0, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v0, :cond_2

    iget-object v0, p0, Lantlr/ANTLRParser;->behavior:Lantlr/ANTLRGrammarParseBehavior;

    invoke-interface {v0}, Lantlr/ANTLRGrammarParseBehavior;->endAlt()V

    :cond_2
    return-void

    :cond_3
    move v0, v1

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x6 -> :sswitch_1
        0x7 -> :sswitch_1
        0x10 -> :sswitch_1
        0x13 -> :sswitch_1
        0x15 -> :sswitch_1
        0x18 -> :sswitch_1
        0x1b -> :sswitch_1
        0x1c -> :sswitch_1
        0x21 -> :sswitch_0
        0x27 -> :sswitch_1
        0x29 -> :sswitch_1
        0x2a -> :sswitch_1
        0x2b -> :sswitch_1
        0x2c -> :sswitch_1
        0x32 -> :sswitch_1
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x10 -> :sswitch_3
        0x15 -> :sswitch_3
        0x1c -> :sswitch_3
        0x27 -> :sswitch_2
    .end sparse-switch
.end method

.method public final ast_type_spec()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/RecognitionException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lantlr/ANTLRParser;->LA(I)I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    new-instance v1, Lantlr/NoViableAltException;

    invoke-virtual {p0, v0}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v0

    invoke-virtual {p0}, Lantlr/ANTLRParser;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lantlr/NoViableAltException;-><init>(Lantlr/Token;Ljava/lang/String;)V

    throw v1

    :sswitch_0
    const/16 v1, 0x31

    invoke-virtual {p0, v1}, Lantlr/ANTLRParser;->match(I)V

    iget-object v1, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v1, v1, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v1, :cond_0

    const/4 v0, 0x2

    :cond_0
    :goto_0
    :sswitch_1
    return v0

    :sswitch_2
    const/16 v1, 0x21

    invoke-virtual {p0, v1}, Lantlr/ANTLRParser;->match(I)V

    iget-object v1, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v1, v1, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v1, :cond_0

    const/4 v0, 0x3

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x6 -> :sswitch_1
        0x7 -> :sswitch_1
        0x10 -> :sswitch_1
        0x13 -> :sswitch_1
        0x15 -> :sswitch_1
        0x18 -> :sswitch_1
        0x19 -> :sswitch_1
        0x1b -> :sswitch_1
        0x1c -> :sswitch_1
        0x21 -> :sswitch_2
        0x22 -> :sswitch_1
        0x27 -> :sswitch_1
        0x29 -> :sswitch_1
        0x2a -> :sswitch_1
        0x2b -> :sswitch_1
        0x2c -> :sswitch_1
        0x31 -> :sswitch_0
        0x32 -> :sswitch_1
    .end sparse-switch
.end method

.method public final block()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/RecognitionException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/16 v1, 0x15

    iget-object v0, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v0, v0, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v0, :cond_0

    iget v0, p0, Lantlr/ANTLRParser;->blockNesting:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lantlr/ANTLRParser;->blockNesting:I

    :cond_0
    invoke-virtual {p0}, Lantlr/ANTLRParser;->alternative()V

    :goto_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lantlr/ANTLRParser;->LA(I)I

    move-result v0

    if-ne v0, v1, :cond_1

    invoke-virtual {p0, v1}, Lantlr/ANTLRParser;->match(I)V

    invoke-virtual {p0}, Lantlr/ANTLRParser;->alternative()V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v0, v0, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v0, :cond_2

    iget v0, p0, Lantlr/ANTLRParser;->blockNesting:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lantlr/ANTLRParser;->blockNesting:I

    :cond_2
    return-void
.end method

.method public final charSet()Lantlr/collections/impl/BitSet;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/RecognitionException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/16 v3, 0x15

    invoke-virtual {p0}, Lantlr/ANTLRParser;->setBlockElement()Lantlr/collections/impl/BitSet;

    move-result-object v0

    :cond_0
    :goto_0
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lantlr/ANTLRParser;->LA(I)I

    move-result v1

    if-ne v1, v3, :cond_1

    invoke-virtual {p0, v3}, Lantlr/ANTLRParser;->match(I)V

    invoke-virtual {p0}, Lantlr/ANTLRParser;->setBlockElement()Lantlr/collections/impl/BitSet;

    move-result-object v1

    iget-object v2, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v2, v2, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v2, :cond_0

    invoke-virtual {v0, v1}, Lantlr/collections/impl/BitSet;->orInPlace(Lantlr/collections/impl/BitSet;)V

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method public final classDef()V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/RecognitionException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/16 v8, 0x18

    const/16 v7, 0xa

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v0, 0x0

    const/4 v1, 0x1

    :try_start_0
    invoke-virtual {p0, v1}, Lantlr/ANTLRParser;->LA(I)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    new-instance v0, Lantlr/NoViableAltException;

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v1

    invoke-virtual {p0}, Lantlr/ANTLRParser;->getFilename()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v1, v4}, Lantlr/NoViableAltException;-><init>(Lantlr/Token;Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Lantlr/RecognitionException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v1

    iget-object v0, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v0, v0, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v0, :cond_b

    instance-of v0, v1, Lantlr/NoViableAltException;

    if-eqz v0, :cond_a

    move-object v0, v1

    check-cast v0, Lantlr/NoViableAltException;

    iget-object v0, v0, Lantlr/NoViableAltException;->token:Lantlr/Token;

    invoke-virtual {v0}, Lantlr/Token;->getType()I

    move-result v0

    const/16 v4, 0x8

    if-ne v0, v4, :cond_9

    const-string v0, "JAVADOC comments may only prefix rules and grammars"

    invoke-virtual {p0, v1, v0}, Lantlr/ANTLRParser;->reportError(Lantlr/RecognitionException;Ljava/lang/String;)V

    :goto_0
    iget-object v0, p0, Lantlr/ANTLRParser;->behavior:Lantlr/ANTLRGrammarParseBehavior;

    invoke-interface {v0}, Lantlr/ANTLRGrammarParseBehavior;->abortGrammar()V

    move v0, v3

    :goto_1
    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lantlr/ANTLRParser;->consume()V

    invoke-virtual {p0, v3}, Lantlr/ANTLRParser;->LA(I)I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    goto :goto_1

    :sswitch_0
    move v0, v2

    goto :goto_1

    :pswitch_0
    const/4 v1, 0x1

    :try_start_1
    invoke-virtual {p0, v1}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v1

    const/4 v4, 0x7

    invoke-virtual {p0, v4}, Lantlr/ANTLRParser;->match(I)V

    iget-object v4, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v4, v4, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v4, :cond_0

    iget-object v4, p0, Lantlr/ANTLRParser;->behavior:Lantlr/ANTLRGrammarParseBehavior;

    invoke-interface {v4, v1}, Lantlr/ANTLRGrammarParseBehavior;->refPreambleAction(Lantlr/Token;)V

    :cond_0
    :pswitch_1
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lantlr/ANTLRParser;->LA(I)I

    move-result v1

    packed-switch v1, :pswitch_data_1

    new-instance v0, Lantlr/NoViableAltException;

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v1

    invoke-virtual {p0}, Lantlr/ANTLRParser;->getFilename()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v1, v4}, Lantlr/NoViableAltException;-><init>(Lantlr/Token;Ljava/lang/String;)V

    throw v0

    :pswitch_2
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v1

    const/16 v4, 0x8

    invoke-virtual {p0, v4}, Lantlr/ANTLRParser;->match(I)V

    iget-object v4, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v4, v4, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v4, :cond_e

    invoke-virtual {v1}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    :goto_2
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lantlr/ANTLRParser;->LA(I)I

    move-result v0

    const/16 v4, 0x9

    if-eq v0, v4, :cond_1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lantlr/ANTLRParser;->LA(I)I

    move-result v0

    if-ne v0, v7, :cond_d

    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lantlr/ANTLRParser;->LA(I)I

    move-result v0

    if-eq v0, v8, :cond_2

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lantlr/ANTLRParser;->LA(I)I

    move-result v0

    const/16 v4, 0x29

    if-ne v0, v4, :cond_d

    :cond_2
    invoke-virtual {p0}, Lantlr/ANTLRParser;->mark()I

    move-result v4

    iget-object v0, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v5, v0, Lantlr/ParserSharedInputState;->guessing:I

    add-int/lit8 v5, v5, 0x1

    iput v5, v0, Lantlr/ParserSharedInputState;->guessing:I
    :try_end_1
    .catch Lantlr/RecognitionException; {:try_start_1 .. :try_end_1} :catch_0

    const/4 v0, 0x1

    :try_start_2
    invoke-virtual {p0, v0}, Lantlr/ANTLRParser;->LA(I)I

    move-result v0

    packed-switch v0, :pswitch_data_2

    new-instance v0, Lantlr/NoViableAltException;

    const/4 v5, 0x1

    invoke-virtual {p0, v5}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v5

    invoke-virtual {p0}, Lantlr/ANTLRParser;->getFilename()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v0, v5, v6}, Lantlr/NoViableAltException;-><init>(Lantlr/Token;Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catch Lantlr/RecognitionException; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception v0

    move v0, v2

    :goto_3
    :try_start_3
    invoke-virtual {p0, v4}, Lantlr/ANTLRParser;->rewind(I)V

    iget-object v4, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v5, v4, Lantlr/ParserSharedInputState;->guessing:I

    add-int/lit8 v5, v5, -0x1

    iput v5, v4, Lantlr/ParserSharedInputState;->guessing:I

    :goto_4
    if-eqz v0, :cond_4

    invoke-virtual {p0, v1}, Lantlr/ANTLRParser;->lexerSpec(Ljava/lang/String;)V

    :goto_5
    invoke-virtual {p0}, Lantlr/ANTLRParser;->rules()V

    iget-object v0, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v0, v0, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v0, :cond_3

    iget-object v0, p0, Lantlr/ANTLRParser;->behavior:Lantlr/ANTLRGrammarParseBehavior;

    invoke-interface {v0}, Lantlr/ANTLRGrammarParseBehavior;->endGrammar()V
    :try_end_3
    .catch Lantlr/RecognitionException; {:try_start_3 .. :try_end_3} :catch_0

    :cond_3
    return-void

    :pswitch_3
    move-object v1, v0

    goto :goto_2

    :pswitch_4
    const/16 v0, 0x9

    :try_start_4
    invoke-virtual {p0, v0}, Lantlr/ANTLRParser;->match(I)V

    :goto_6
    move v0, v3

    goto :goto_3

    :pswitch_5
    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Lantlr/ANTLRParser;->match(I)V

    invoke-virtual {p0}, Lantlr/ANTLRParser;->id()Lantlr/Token;

    const/16 v0, 0xb

    invoke-virtual {p0, v0}, Lantlr/ANTLRParser;->match(I)V

    const/16 v0, 0xc

    invoke-virtual {p0, v0}, Lantlr/ANTLRParser;->match(I)V
    :try_end_4
    .catch Lantlr/RecognitionException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_6

    :cond_4
    const/4 v0, 0x1

    :try_start_5
    invoke-virtual {p0, v0}, Lantlr/ANTLRParser;->LA(I)I

    move-result v0

    if-ne v0, v7, :cond_c

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lantlr/ANTLRParser;->LA(I)I

    move-result v0

    if-eq v0, v8, :cond_5

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lantlr/ANTLRParser;->LA(I)I

    move-result v0

    const/16 v4, 0x29

    if-ne v0, v4, :cond_c

    :cond_5
    invoke-virtual {p0}, Lantlr/ANTLRParser;->mark()I

    move-result v4

    iget-object v0, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v5, v0, Lantlr/ParserSharedInputState;->guessing:I

    add-int/lit8 v5, v5, 0x1

    iput v5, v0, Lantlr/ParserSharedInputState;->guessing:I
    :try_end_5
    .catch Lantlr/RecognitionException; {:try_start_5 .. :try_end_5} :catch_0

    const/16 v0, 0xa

    :try_start_6
    invoke-virtual {p0, v0}, Lantlr/ANTLRParser;->match(I)V

    invoke-virtual {p0}, Lantlr/ANTLRParser;->id()Lantlr/Token;

    const/16 v0, 0xb

    invoke-virtual {p0, v0}, Lantlr/ANTLRParser;->match(I)V

    const/16 v0, 0xd

    invoke-virtual {p0, v0}, Lantlr/ANTLRParser;->match(I)V
    :try_end_6
    .catch Lantlr/RecognitionException; {:try_start_6 .. :try_end_6} :catch_2

    move v0, v3

    :goto_7
    :try_start_7
    invoke-virtual {p0, v4}, Lantlr/ANTLRParser;->rewind(I)V

    iget-object v4, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v5, v4, Lantlr/ParserSharedInputState;->guessing:I

    add-int/lit8 v5, v5, -0x1

    iput v5, v4, Lantlr/ParserSharedInputState;->guessing:I

    :goto_8
    if-eqz v0, :cond_6

    invoke-virtual {p0, v1}, Lantlr/ANTLRParser;->treeParserSpec(Ljava/lang/String;)V

    goto :goto_5

    :catch_2
    move-exception v0

    move v0, v2

    goto :goto_7

    :cond_6
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lantlr/ANTLRParser;->LA(I)I

    move-result v0

    if-ne v0, v7, :cond_8

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lantlr/ANTLRParser;->LA(I)I

    move-result v0

    if-eq v0, v8, :cond_7

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lantlr/ANTLRParser;->LA(I)I

    move-result v0

    const/16 v4, 0x29

    if-ne v0, v4, :cond_8

    :cond_7
    invoke-virtual {p0, v1}, Lantlr/ANTLRParser;->parserSpec(Ljava/lang/String;)V

    goto/16 :goto_5

    :cond_8
    new-instance v0, Lantlr/NoViableAltException;

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v1

    invoke-virtual {p0}, Lantlr/ANTLRParser;->getFilename()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v1, v4}, Lantlr/NoViableAltException;-><init>(Lantlr/Token;Ljava/lang/String;)V

    throw v0
    :try_end_7
    .catch Lantlr/RecognitionException; {:try_start_7 .. :try_end_7} :catch_0

    :cond_9
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string v4, "rule classDef trapped:\n"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v1}, Lantlr/RecognitionException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lantlr/ANTLRParser;->reportError(Lantlr/RecognitionException;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_a
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string v4, "rule classDef trapped:\n"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v1}, Lantlr/RecognitionException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lantlr/ANTLRParser;->reportError(Lantlr/RecognitionException;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_b
    throw v1

    :cond_c
    move v0, v2

    goto :goto_8

    :cond_d
    move v0, v2

    goto/16 :goto_4

    :cond_e
    move-object v1, v0

    goto/16 :goto_2

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x9 -> :sswitch_0
        0xa -> :sswitch_0
    .end sparse-switch

    :pswitch_data_1
    .packed-switch 0x8
        :pswitch_2
        :pswitch_3
        :pswitch_3
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x9
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public final ebnf(Lantlr/Token;Z)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/RecognitionException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/4 v5, 0x2

    const/16 v4, 0x24

    const/4 v3, 0x7

    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v0

    const/16 v1, 0x1b

    invoke-virtual {p0, v1}, Lantlr/ANTLRParser;->match(I)V

    iget-object v1, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v1, v1, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v1, :cond_0

    iget-object v1, p0, Lantlr/ANTLRParser;->behavior:Lantlr/ANTLRGrammarParseBehavior;

    invoke-interface {v1, p1, v0, p2}, Lantlr/ANTLRGrammarParseBehavior;->beginSubRule(Lantlr/Token;Lantlr/Token;Z)V

    :cond_0
    invoke-virtual {p0, v2}, Lantlr/ANTLRParser;->LA(I)I

    move-result v0

    const/16 v1, 0xe

    if-ne v0, v1, :cond_3

    invoke-virtual {p0}, Lantlr/ANTLRParser;->subruleOptionsSpec()V

    invoke-virtual {p0, v2}, Lantlr/ANTLRParser;->LA(I)I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    new-instance v0, Lantlr/NoViableAltException;

    invoke-virtual {p0, v2}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v1

    invoke-virtual {p0}, Lantlr/ANTLRParser;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lantlr/NoViableAltException;-><init>(Lantlr/Token;Ljava/lang/String;)V

    throw v0

    :sswitch_0
    invoke-virtual {p0, v2}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v0

    invoke-virtual {p0, v3}, Lantlr/ANTLRParser;->match(I)V

    iget-object v1, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v1, v1, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v1, :cond_1

    iget-object v1, p0, Lantlr/ANTLRParser;->behavior:Lantlr/ANTLRGrammarParseBehavior;

    invoke-interface {v1, v0}, Lantlr/ANTLRGrammarParseBehavior;->refInitAction(Lantlr/Token;)V

    :cond_1
    :sswitch_1
    invoke-virtual {p0, v4}, Lantlr/ANTLRParser;->match(I)V

    :cond_2
    :goto_0
    invoke-virtual {p0}, Lantlr/ANTLRParser;->block()V

    const/16 v0, 0x1c

    invoke-virtual {p0, v0}, Lantlr/ANTLRParser;->match(I)V

    invoke-virtual {p0, v2}, Lantlr/ANTLRParser;->LA(I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    new-instance v0, Lantlr/NoViableAltException;

    invoke-virtual {p0, v2}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v1

    invoke-virtual {p0}, Lantlr/ANTLRParser;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lantlr/NoViableAltException;-><init>(Lantlr/Token;Ljava/lang/String;)V

    throw v0

    :cond_3
    invoke-virtual {p0, v2}, Lantlr/ANTLRParser;->LA(I)I

    move-result v0

    if-ne v0, v3, :cond_5

    invoke-virtual {p0, v5}, Lantlr/ANTLRParser;->LA(I)I

    move-result v0

    if-ne v0, v4, :cond_5

    invoke-virtual {p0, v2}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v0

    invoke-virtual {p0, v3}, Lantlr/ANTLRParser;->match(I)V

    iget-object v1, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v1, v1, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v1, :cond_4

    iget-object v1, p0, Lantlr/ANTLRParser;->behavior:Lantlr/ANTLRGrammarParseBehavior;

    invoke-interface {v1, v0}, Lantlr/ANTLRGrammarParseBehavior;->refInitAction(Lantlr/Token;)V

    :cond_4
    invoke-virtual {p0, v4}, Lantlr/ANTLRParser;->match(I)V

    goto :goto_0

    :cond_5
    sget-object v0, Lantlr/ANTLRParser;->_tokenSet_9:Lantlr/collections/impl/BitSet;

    invoke-virtual {p0, v2}, Lantlr/ANTLRParser;->LA(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lantlr/collections/impl/BitSet;->member(I)Z

    move-result v0

    if-eqz v0, :cond_6

    sget-object v0, Lantlr/ANTLRParser;->_tokenSet_10:Lantlr/collections/impl/BitSet;

    invoke-virtual {p0, v5}, Lantlr/ANTLRParser;->LA(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lantlr/collections/impl/BitSet;->member(I)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_6
    new-instance v0, Lantlr/NoViableAltException;

    invoke-virtual {p0, v2}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v1

    invoke-virtual {p0}, Lantlr/ANTLRParser;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lantlr/NoViableAltException;-><init>(Lantlr/Token;Ljava/lang/String;)V

    throw v0

    :pswitch_1
    invoke-virtual {p0, v2}, Lantlr/ANTLRParser;->LA(I)I

    move-result v0

    packed-switch v0, :pswitch_data_1

    :pswitch_2
    new-instance v0, Lantlr/NoViableAltException;

    invoke-virtual {p0, v2}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v1

    invoke-virtual {p0}, Lantlr/ANTLRParser;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lantlr/NoViableAltException;-><init>(Lantlr/Token;Ljava/lang/String;)V

    throw v0

    :pswitch_3
    const/16 v0, 0x2d

    invoke-virtual {p0, v0}, Lantlr/ANTLRParser;->match(I)V

    iget-object v0, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v0, v0, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v0, :cond_7

    iget-object v0, p0, Lantlr/ANTLRParser;->behavior:Lantlr/ANTLRGrammarParseBehavior;

    invoke-interface {v0}, Lantlr/ANTLRGrammarParseBehavior;->optionalSubRule()V

    :cond_7
    :goto_1
    :pswitch_4
    invoke-virtual {p0, v2}, Lantlr/ANTLRParser;->LA(I)I

    move-result v0

    sparse-switch v0, :sswitch_data_1

    new-instance v0, Lantlr/NoViableAltException;

    invoke-virtual {p0, v2}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v1

    invoke-virtual {p0}, Lantlr/ANTLRParser;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lantlr/NoViableAltException;-><init>(Lantlr/Token;Ljava/lang/String;)V

    throw v0

    :pswitch_5
    const/16 v0, 0x2e

    invoke-virtual {p0, v0}, Lantlr/ANTLRParser;->match(I)V

    iget-object v0, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v0, v0, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v0, :cond_7

    iget-object v0, p0, Lantlr/ANTLRParser;->behavior:Lantlr/ANTLRGrammarParseBehavior;

    invoke-interface {v0}, Lantlr/ANTLRGrammarParseBehavior;->zeroOrMoreSubRule()V

    goto :goto_1

    :pswitch_6
    const/16 v0, 0x2f

    invoke-virtual {p0, v0}, Lantlr/ANTLRParser;->match(I)V

    iget-object v0, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v0, v0, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v0, :cond_7

    iget-object v0, p0, Lantlr/ANTLRParser;->behavior:Lantlr/ANTLRGrammarParseBehavior;

    invoke-interface {v0}, Lantlr/ANTLRGrammarParseBehavior;->oneOrMoreSubRule()V

    goto :goto_1

    :sswitch_2
    const/16 v0, 0x21

    invoke-virtual {p0, v0}, Lantlr/ANTLRParser;->match(I)V

    iget-object v0, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v0, v0, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v0, :cond_8

    iget-object v0, p0, Lantlr/ANTLRParser;->behavior:Lantlr/ANTLRGrammarParseBehavior;

    invoke-interface {v0}, Lantlr/ANTLRGrammarParseBehavior;->noASTSubRule()V

    :cond_8
    :goto_2
    :sswitch_3
    iget-object v0, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v0, v0, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v0, :cond_9

    iget-object v0, p0, Lantlr/ANTLRParser;->behavior:Lantlr/ANTLRGrammarParseBehavior;

    invoke-interface {v0}, Lantlr/ANTLRGrammarParseBehavior;->endSubRule()V

    :cond_9
    return-void

    :pswitch_7
    const/16 v0, 0x30

    invoke-virtual {p0, v0}, Lantlr/ANTLRParser;->match(I)V

    iget-object v0, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v0, v0, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v0, :cond_8

    iget-object v0, p0, Lantlr/ANTLRParser;->behavior:Lantlr/ANTLRGrammarParseBehavior;

    invoke-interface {v0}, Lantlr/ANTLRGrammarParseBehavior;->synPred()V

    goto :goto_2

    :sswitch_data_0
    .sparse-switch
        0x7 -> :sswitch_0
        0x24 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_7
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x6
        :pswitch_4
        :pswitch_4
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_4
        :pswitch_2
        :pswitch_2
        :pswitch_4
        :pswitch_2
        :pswitch_4
        :pswitch_2
        :pswitch_2
        :pswitch_4
        :pswitch_4
        :pswitch_2
        :pswitch_4
        :pswitch_4
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_4
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_4
        :pswitch_2
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_3
        :pswitch_5
        :pswitch_6
        :pswitch_2
        :pswitch_2
        :pswitch_4
    .end packed-switch

    :sswitch_data_1
    .sparse-switch
        0x6 -> :sswitch_3
        0x7 -> :sswitch_3
        0x10 -> :sswitch_3
        0x13 -> :sswitch_3
        0x15 -> :sswitch_3
        0x18 -> :sswitch_3
        0x19 -> :sswitch_3
        0x1b -> :sswitch_3
        0x1c -> :sswitch_3
        0x21 -> :sswitch_2
        0x27 -> :sswitch_3
        0x29 -> :sswitch_3
        0x2a -> :sswitch_3
        0x2b -> :sswitch_3
        0x2c -> :sswitch_3
        0x32 -> :sswitch_3
    .end sparse-switch
.end method

.method public final element()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/RecognitionException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/4 v1, 0x1

    invoke-virtual {p0}, Lantlr/ANTLRParser;->elementNoOptionSpec()V

    invoke-virtual {p0, v1}, Lantlr/ANTLRParser;->LA(I)I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    new-instance v0, Lantlr/NoViableAltException;

    invoke-virtual {p0, v1}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v1

    invoke-virtual {p0}, Lantlr/ANTLRParser;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lantlr/NoViableAltException;-><init>(Lantlr/Token;Ljava/lang/String;)V

    throw v0

    :sswitch_0
    invoke-virtual {p0}, Lantlr/ANTLRParser;->elementOptionSpec()V

    :sswitch_1
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        0x6 -> :sswitch_1
        0x7 -> :sswitch_1
        0x10 -> :sswitch_1
        0x13 -> :sswitch_1
        0x15 -> :sswitch_1
        0x18 -> :sswitch_1
        0x19 -> :sswitch_0
        0x1b -> :sswitch_1
        0x1c -> :sswitch_1
        0x27 -> :sswitch_1
        0x29 -> :sswitch_1
        0x2a -> :sswitch_1
        0x2b -> :sswitch_1
        0x2c -> :sswitch_1
        0x32 -> :sswitch_1
    .end sparse-switch
.end method

.method public final elementNoOptionSpec()V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/RecognitionException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/16 v9, 0x24

    const/16 v8, 0x29

    const/16 v7, 0x18

    const/4 v5, 0x2

    const/4 v6, 0x1

    const/4 v3, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-virtual {p0, v6}, Lantlr/ANTLRParser;->LA(I)I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    invoke-virtual {p0, v6}, Lantlr/ANTLRParser;->LA(I)I

    move-result v2

    if-eq v2, v7, :cond_0

    invoke-virtual {p0, v6}, Lantlr/ANTLRParser;->LA(I)I

    move-result v2

    if-ne v2, v8, :cond_7

    :cond_0
    invoke-virtual {p0, v5}, Lantlr/ANTLRParser;->LA(I)I

    move-result v2

    const/16 v4, 0xf

    if-ne v2, v4, :cond_7

    invoke-virtual {p0}, Lantlr/ANTLRParser;->id()Lantlr/Token;

    move-result-object v1

    const/16 v2, 0xf

    invoke-virtual {p0, v2}, Lantlr/ANTLRParser;->match(I)V

    invoke-virtual {p0, v6}, Lantlr/ANTLRParser;->LA(I)I

    move-result v2

    if-eq v2, v7, :cond_1

    invoke-virtual {p0, v6}, Lantlr/ANTLRParser;->LA(I)I

    move-result v2

    if-ne v2, v8, :cond_4

    :cond_1
    invoke-virtual {p0, v5}, Lantlr/ANTLRParser;->LA(I)I

    move-result v2

    if-ne v2, v9, :cond_4

    invoke-virtual {p0}, Lantlr/ANTLRParser;->id()Lantlr/Token;

    move-result-object v3

    invoke-virtual {p0, v9}, Lantlr/ANTLRParser;->match(I)V

    iget-object v2, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v2, v2, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v2, :cond_2

    invoke-direct {p0, v3}, Lantlr/ANTLRParser;->checkForMissingEndRule(Lantlr/Token;)V

    :cond_2
    invoke-virtual {p0, v6}, Lantlr/ANTLRParser;->LA(I)I

    move-result v2

    sparse-switch v2, :sswitch_data_1

    new-instance v0, Lantlr/NoViableAltException;

    invoke-virtual {p0, v6}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v1

    invoke-virtual {p0}, Lantlr/ANTLRParser;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lantlr/NoViableAltException;-><init>(Lantlr/Token;Ljava/lang/String;)V

    throw v0

    :sswitch_0
    invoke-virtual {p0, v6}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v0

    const/4 v1, 0x7

    invoke-virtual {p0, v1}, Lantlr/ANTLRParser;->match(I)V

    iget-object v1, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v1, v1, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v1, :cond_3

    iget-object v1, p0, Lantlr/ANTLRParser;->behavior:Lantlr/ANTLRGrammarParseBehavior;

    invoke-interface {v1, v0}, Lantlr/ANTLRGrammarParseBehavior;->refAction(Lantlr/Token;)V

    :cond_3
    :goto_0
    return-void

    :sswitch_1
    invoke-virtual {p0, v6}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v0

    const/16 v1, 0x2b

    invoke-virtual {p0, v1}, Lantlr/ANTLRParser;->match(I)V

    iget-object v1, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v1, v1, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v1, :cond_3

    iget-object v1, p0, Lantlr/ANTLRParser;->behavior:Lantlr/ANTLRGrammarParseBehavior;

    invoke-interface {v1, v0}, Lantlr/ANTLRGrammarParseBehavior;->refSemPred(Lantlr/Token;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p0}, Lantlr/ANTLRParser;->tree()V

    goto :goto_0

    :cond_4
    invoke-virtual {p0, v6}, Lantlr/ANTLRParser;->LA(I)I

    move-result v2

    if-eq v2, v7, :cond_5

    invoke-virtual {p0, v6}, Lantlr/ANTLRParser;->LA(I)I

    move-result v2

    if-ne v2, v8, :cond_6

    :cond_5
    sget-object v2, Lantlr/ANTLRParser;->_tokenSet_3:Lantlr/collections/impl/BitSet;

    invoke-virtual {p0, v5}, Lantlr/ANTLRParser;->LA(I)I

    move-result v4

    invoke-virtual {v2, v4}, Lantlr/collections/impl/BitSet;->member(I)Z

    move-result v2

    if-nez v2, :cond_2

    :cond_6
    new-instance v0, Lantlr/NoViableAltException;

    invoke-virtual {p0, v6}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v1

    invoke-virtual {p0}, Lantlr/ANTLRParser;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lantlr/NoViableAltException;-><init>(Lantlr/Token;Ljava/lang/String;)V

    throw v0

    :sswitch_3
    invoke-virtual {p0, v6}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v2

    invoke-virtual {p0, v8}, Lantlr/ANTLRParser;->match(I)V

    invoke-virtual {p0, v6}, Lantlr/ANTLRParser;->LA(I)I

    move-result v4

    sparse-switch v4, :sswitch_data_2

    new-instance v0, Lantlr/NoViableAltException;

    invoke-virtual {p0, v6}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v1

    invoke-virtual {p0}, Lantlr/ANTLRParser;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lantlr/NoViableAltException;-><init>(Lantlr/Token;Ljava/lang/String;)V

    throw v0

    :sswitch_4
    invoke-virtual {p0, v6}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v4

    const/16 v5, 0x22

    invoke-virtual {p0, v5}, Lantlr/ANTLRParser;->match(I)V

    iget-object v5, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v5, v5, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v5, :cond_14

    :goto_1
    invoke-virtual {p0, v6}, Lantlr/ANTLRParser;->LA(I)I

    move-result v0

    sparse-switch v0, :sswitch_data_3

    new-instance v0, Lantlr/NoViableAltException;

    invoke-virtual {p0, v6}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v1

    invoke-virtual {p0}, Lantlr/ANTLRParser;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lantlr/NoViableAltException;-><init>(Lantlr/Token;Ljava/lang/String;)V

    throw v0

    :sswitch_5
    move-object v4, v0

    goto :goto_1

    :sswitch_6
    const/16 v0, 0x21

    invoke-virtual {p0, v0}, Lantlr/ANTLRParser;->match(I)V

    iget-object v0, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v0, v0, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v0, :cond_13

    const/4 v6, 0x3

    move v5, v6

    :goto_2
    iget-object v0, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v0, v0, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v0, :cond_3

    iget-object v0, p0, Lantlr/ANTLRParser;->behavior:Lantlr/ANTLRGrammarParseBehavior;

    invoke-interface/range {v0 .. v5}, Lantlr/ANTLRGrammarParseBehavior;->refRule(Lantlr/Token;Lantlr/Token;Lantlr/Token;Lantlr/Token;I)V

    goto/16 :goto_0

    :sswitch_7
    move v5, v6

    goto :goto_2

    :sswitch_8
    invoke-virtual {p0, v6}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v2

    invoke-virtual {p0, v7}, Lantlr/ANTLRParser;->match(I)V

    invoke-virtual {p0, v6}, Lantlr/ANTLRParser;->LA(I)I

    move-result v4

    sparse-switch v4, :sswitch_data_4

    new-instance v0, Lantlr/NoViableAltException;

    invoke-virtual {p0, v6}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v1

    invoke-virtual {p0}, Lantlr/ANTLRParser;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lantlr/NoViableAltException;-><init>(Lantlr/Token;Ljava/lang/String;)V

    throw v0

    :sswitch_9
    invoke-virtual {p0, v6}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v4

    const/16 v5, 0x22

    invoke-virtual {p0, v5}, Lantlr/ANTLRParser;->match(I)V

    iget-object v5, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v5, v5, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v5, :cond_12

    :goto_3
    iget-object v0, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v0, v0, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v0, :cond_3

    iget-object v0, p0, Lantlr/ANTLRParser;->behavior:Lantlr/ANTLRGrammarParseBehavior;

    const/4 v5, 0x0

    invoke-direct {p0}, Lantlr/ANTLRParser;->lastInRule()Z

    move-result v7

    invoke-interface/range {v0 .. v7}, Lantlr/ANTLRGrammarParseBehavior;->refToken(Lantlr/Token;Lantlr/Token;Lantlr/Token;Lantlr/Token;ZIZ)V

    goto/16 :goto_0

    :sswitch_a
    move-object v4, v0

    goto :goto_3

    :cond_7
    sget-object v2, Lantlr/ANTLRParser;->_tokenSet_4:Lantlr/collections/impl/BitSet;

    invoke-virtual {p0, v6}, Lantlr/ANTLRParser;->LA(I)I

    move-result v4

    invoke-virtual {v2, v4}, Lantlr/collections/impl/BitSet;->member(I)Z

    move-result v2

    if-eqz v2, :cond_f

    sget-object v2, Lantlr/ANTLRParser;->_tokenSet_5:Lantlr/collections/impl/BitSet;

    invoke-virtual {p0, v5}, Lantlr/ANTLRParser;->LA(I)I

    move-result v4

    invoke-virtual {v2, v4}, Lantlr/collections/impl/BitSet;->member(I)Z

    move-result v2

    if-eqz v2, :cond_f

    invoke-virtual {p0, v6}, Lantlr/ANTLRParser;->LA(I)I

    move-result v2

    if-eq v2, v7, :cond_8

    invoke-virtual {p0, v6}, Lantlr/ANTLRParser;->LA(I)I

    move-result v2

    if-ne v2, v8, :cond_b

    :cond_8
    invoke-virtual {p0, v5}, Lantlr/ANTLRParser;->LA(I)I

    move-result v2

    if-ne v2, v9, :cond_b

    invoke-virtual {p0}, Lantlr/ANTLRParser;->id()Lantlr/Token;

    move-result-object v3

    invoke-virtual {p0, v9}, Lantlr/ANTLRParser;->match(I)V

    iget-object v2, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v2, v2, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v2, :cond_9

    invoke-direct {p0, v3}, Lantlr/ANTLRParser;->checkForMissingEndRule(Lantlr/Token;)V

    :cond_9
    invoke-virtual {p0, v6}, Lantlr/ANTLRParser;->LA(I)I

    move-result v2

    sparse-switch v2, :sswitch_data_5

    invoke-virtual {p0, v6}, Lantlr/ANTLRParser;->LA(I)I

    move-result v0

    const/4 v1, 0x6

    if-eq v0, v1, :cond_a

    invoke-virtual {p0, v6}, Lantlr/ANTLRParser;->LA(I)I

    move-result v0

    const/16 v1, 0x13

    if-eq v0, v1, :cond_a

    invoke-virtual {p0, v6}, Lantlr/ANTLRParser;->LA(I)I

    move-result v0

    if-ne v0, v7, :cond_d

    :cond_a
    invoke-virtual {p0, v5}, Lantlr/ANTLRParser;->LA(I)I

    move-result v0

    const/16 v1, 0x16

    if-ne v0, v1, :cond_d

    invoke-virtual {p0, v3}, Lantlr/ANTLRParser;->range(Lantlr/Token;)V

    goto/16 :goto_0

    :cond_b
    sget-object v2, Lantlr/ANTLRParser;->_tokenSet_4:Lantlr/collections/impl/BitSet;

    invoke-virtual {p0, v6}, Lantlr/ANTLRParser;->LA(I)I

    move-result v4

    invoke-virtual {v2, v4}, Lantlr/collections/impl/BitSet;->member(I)Z

    move-result v2

    if-eqz v2, :cond_c

    sget-object v2, Lantlr/ANTLRParser;->_tokenSet_6:Lantlr/collections/impl/BitSet;

    invoke-virtual {p0, v5}, Lantlr/ANTLRParser;->LA(I)I

    move-result v4

    invoke-virtual {v2, v4}, Lantlr/collections/impl/BitSet;->member(I)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_c
    new-instance v0, Lantlr/NoViableAltException;

    invoke-virtual {p0, v6}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v1

    invoke-virtual {p0}, Lantlr/ANTLRParser;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lantlr/NoViableAltException;-><init>(Lantlr/Token;Ljava/lang/String;)V

    throw v0

    :sswitch_b
    invoke-virtual {p0, v6}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v2

    invoke-virtual {p0, v8}, Lantlr/ANTLRParser;->match(I)V

    invoke-virtual {p0, v6}, Lantlr/ANTLRParser;->LA(I)I

    move-result v4

    sparse-switch v4, :sswitch_data_6

    new-instance v0, Lantlr/NoViableAltException;

    invoke-virtual {p0, v6}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v1

    invoke-virtual {p0}, Lantlr/ANTLRParser;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lantlr/NoViableAltException;-><init>(Lantlr/Token;Ljava/lang/String;)V

    throw v0

    :sswitch_c
    invoke-virtual {p0, v6}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v4

    const/16 v5, 0x22

    invoke-virtual {p0, v5}, Lantlr/ANTLRParser;->match(I)V

    iget-object v5, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v5, v5, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v5, :cond_11

    :goto_4
    invoke-virtual {p0, v6}, Lantlr/ANTLRParser;->LA(I)I

    move-result v0

    sparse-switch v0, :sswitch_data_7

    new-instance v0, Lantlr/NoViableAltException;

    invoke-virtual {p0, v6}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v1

    invoke-virtual {p0}, Lantlr/ANTLRParser;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lantlr/NoViableAltException;-><init>(Lantlr/Token;Ljava/lang/String;)V

    throw v0

    :sswitch_d
    move-object v4, v0

    goto :goto_4

    :sswitch_e
    const/16 v0, 0x21

    invoke-virtual {p0, v0}, Lantlr/ANTLRParser;->match(I)V

    iget-object v0, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v0, v0, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v0, :cond_10

    const/4 v6, 0x3

    move v5, v6

    :goto_5
    iget-object v0, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v0, v0, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v0, :cond_3

    iget-object v0, p0, Lantlr/ANTLRParser;->behavior:Lantlr/ANTLRGrammarParseBehavior;

    invoke-interface/range {v0 .. v5}, Lantlr/ANTLRGrammarParseBehavior;->refRule(Lantlr/Token;Lantlr/Token;Lantlr/Token;Lantlr/Token;I)V

    goto/16 :goto_0

    :sswitch_f
    move v5, v6

    goto :goto_5

    :sswitch_10
    const/16 v0, 0x2a

    invoke-virtual {p0, v0}, Lantlr/ANTLRParser;->match(I)V

    invoke-virtual {p0, v6}, Lantlr/ANTLRParser;->LA(I)I

    move-result v0

    sparse-switch v0, :sswitch_data_8

    new-instance v0, Lantlr/NoViableAltException;

    invoke-virtual {p0, v6}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v1

    invoke-virtual {p0}, Lantlr/ANTLRParser;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lantlr/NoViableAltException;-><init>(Lantlr/Token;Ljava/lang/String;)V

    throw v0

    :sswitch_11
    invoke-virtual {p0, v3}, Lantlr/ANTLRParser;->notTerminal(Lantlr/Token;)V

    goto/16 :goto_0

    :sswitch_12
    invoke-virtual {p0, v3, v6}, Lantlr/ANTLRParser;->ebnf(Lantlr/Token;Z)V

    goto/16 :goto_0

    :sswitch_13
    const/4 v0, 0x0

    invoke-virtual {p0, v3, v0}, Lantlr/ANTLRParser;->ebnf(Lantlr/Token;Z)V

    goto/16 :goto_0

    :cond_d
    sget-object v0, Lantlr/ANTLRParser;->_tokenSet_7:Lantlr/collections/impl/BitSet;

    invoke-virtual {p0, v6}, Lantlr/ANTLRParser;->LA(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lantlr/collections/impl/BitSet;->member(I)Z

    move-result v0

    if-eqz v0, :cond_e

    sget-object v0, Lantlr/ANTLRParser;->_tokenSet_8:Lantlr/collections/impl/BitSet;

    invoke-virtual {p0, v5}, Lantlr/ANTLRParser;->LA(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lantlr/collections/impl/BitSet;->member(I)Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-virtual {p0, v3}, Lantlr/ANTLRParser;->terminal(Lantlr/Token;)V

    goto/16 :goto_0

    :cond_e
    new-instance v0, Lantlr/NoViableAltException;

    invoke-virtual {p0, v6}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v1

    invoke-virtual {p0}, Lantlr/ANTLRParser;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lantlr/NoViableAltException;-><init>(Lantlr/Token;Ljava/lang/String;)V

    throw v0

    :cond_f
    new-instance v0, Lantlr/NoViableAltException;

    invoke-virtual {p0, v6}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v1

    invoke-virtual {p0}, Lantlr/ANTLRParser;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lantlr/NoViableAltException;-><init>(Lantlr/Token;Ljava/lang/String;)V

    throw v0

    :cond_10
    move v5, v6

    goto :goto_5

    :cond_11
    move-object v4, v0

    goto/16 :goto_4

    :cond_12
    move-object v4, v0

    goto/16 :goto_3

    :cond_13
    move v5, v6

    goto/16 :goto_2

    :cond_14
    move-object v4, v0

    goto/16 :goto_1

    :sswitch_data_0
    .sparse-switch
        0x7 -> :sswitch_0
        0x2b -> :sswitch_1
        0x2c -> :sswitch_2
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x18 -> :sswitch_8
        0x29 -> :sswitch_3
    .end sparse-switch

    :sswitch_data_2
    .sparse-switch
        0x6 -> :sswitch_5
        0x7 -> :sswitch_5
        0x10 -> :sswitch_5
        0x13 -> :sswitch_5
        0x15 -> :sswitch_5
        0x18 -> :sswitch_5
        0x19 -> :sswitch_5
        0x1b -> :sswitch_5
        0x1c -> :sswitch_5
        0x21 -> :sswitch_5
        0x22 -> :sswitch_4
        0x27 -> :sswitch_5
        0x29 -> :sswitch_5
        0x2a -> :sswitch_5
        0x2b -> :sswitch_5
        0x2c -> :sswitch_5
        0x32 -> :sswitch_5
    .end sparse-switch

    :sswitch_data_3
    .sparse-switch
        0x6 -> :sswitch_7
        0x7 -> :sswitch_7
        0x10 -> :sswitch_7
        0x13 -> :sswitch_7
        0x15 -> :sswitch_7
        0x18 -> :sswitch_7
        0x19 -> :sswitch_7
        0x1b -> :sswitch_7
        0x1c -> :sswitch_7
        0x21 -> :sswitch_6
        0x27 -> :sswitch_7
        0x29 -> :sswitch_7
        0x2a -> :sswitch_7
        0x2b -> :sswitch_7
        0x2c -> :sswitch_7
        0x32 -> :sswitch_7
    .end sparse-switch

    :sswitch_data_4
    .sparse-switch
        0x6 -> :sswitch_a
        0x7 -> :sswitch_a
        0x10 -> :sswitch_a
        0x13 -> :sswitch_a
        0x15 -> :sswitch_a
        0x18 -> :sswitch_a
        0x19 -> :sswitch_a
        0x1b -> :sswitch_a
        0x1c -> :sswitch_a
        0x22 -> :sswitch_9
        0x27 -> :sswitch_a
        0x29 -> :sswitch_a
        0x2a -> :sswitch_a
        0x2b -> :sswitch_a
        0x2c -> :sswitch_a
        0x32 -> :sswitch_a
    .end sparse-switch

    :sswitch_data_5
    .sparse-switch
        0x1b -> :sswitch_13
        0x29 -> :sswitch_b
        0x2a -> :sswitch_10
    .end sparse-switch

    :sswitch_data_6
    .sparse-switch
        0x6 -> :sswitch_d
        0x7 -> :sswitch_d
        0x10 -> :sswitch_d
        0x13 -> :sswitch_d
        0x15 -> :sswitch_d
        0x18 -> :sswitch_d
        0x19 -> :sswitch_d
        0x1b -> :sswitch_d
        0x1c -> :sswitch_d
        0x21 -> :sswitch_d
        0x22 -> :sswitch_c
        0x27 -> :sswitch_d
        0x29 -> :sswitch_d
        0x2a -> :sswitch_d
        0x2b -> :sswitch_d
        0x2c -> :sswitch_d
        0x32 -> :sswitch_d
    .end sparse-switch

    :sswitch_data_7
    .sparse-switch
        0x6 -> :sswitch_f
        0x7 -> :sswitch_f
        0x10 -> :sswitch_f
        0x13 -> :sswitch_f
        0x15 -> :sswitch_f
        0x18 -> :sswitch_f
        0x19 -> :sswitch_f
        0x1b -> :sswitch_f
        0x1c -> :sswitch_f
        0x21 -> :sswitch_e
        0x27 -> :sswitch_f
        0x29 -> :sswitch_f
        0x2a -> :sswitch_f
        0x2b -> :sswitch_f
        0x2c -> :sswitch_f
        0x32 -> :sswitch_f
    .end sparse-switch

    :sswitch_data_8
    .sparse-switch
        0x13 -> :sswitch_11
        0x18 -> :sswitch_11
        0x1b -> :sswitch_12
    .end sparse-switch
.end method

.method public final elementOptionSpec()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/RecognitionException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/16 v4, 0x10

    const/16 v3, 0xf

    const/16 v0, 0x19

    invoke-virtual {p0, v0}, Lantlr/ANTLRParser;->match(I)V

    invoke-virtual {p0}, Lantlr/ANTLRParser;->id()Lantlr/Token;

    move-result-object v0

    invoke-virtual {p0, v3}, Lantlr/ANTLRParser;->match(I)V

    invoke-virtual {p0}, Lantlr/ANTLRParser;->optionValue()Lantlr/Token;

    move-result-object v1

    iget-object v2, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v2, v2, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v2, :cond_0

    iget-object v2, p0, Lantlr/ANTLRParser;->behavior:Lantlr/ANTLRGrammarParseBehavior;

    invoke-interface {v2, v0, v1}, Lantlr/ANTLRGrammarParseBehavior;->refElementOption(Lantlr/Token;Lantlr/Token;)V

    :cond_0
    :goto_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lantlr/ANTLRParser;->LA(I)I

    move-result v0

    if-ne v0, v4, :cond_1

    invoke-virtual {p0, v4}, Lantlr/ANTLRParser;->match(I)V

    invoke-virtual {p0}, Lantlr/ANTLRParser;->id()Lantlr/Token;

    move-result-object v0

    invoke-virtual {p0, v3}, Lantlr/ANTLRParser;->match(I)V

    invoke-virtual {p0}, Lantlr/ANTLRParser;->optionValue()Lantlr/Token;

    move-result-object v1

    iget-object v2, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v2, v2, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v2, :cond_0

    iget-object v2, p0, Lantlr/ANTLRParser;->behavior:Lantlr/ANTLRGrammarParseBehavior;

    invoke-interface {v2, v0, v1}, Lantlr/ANTLRGrammarParseBehavior;->refElementOption(Lantlr/Token;Lantlr/Token;)V

    goto :goto_0

    :cond_1
    const/16 v0, 0x1a

    invoke-virtual {p0, v0}, Lantlr/ANTLRParser;->match(I)V

    return-void
.end method

.method public final exceptionGroup()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/RecognitionException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/4 v3, 0x1

    iget-object v0, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v0, v0, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lantlr/ANTLRParser;->behavior:Lantlr/ANTLRGrammarParseBehavior;

    invoke-interface {v0}, Lantlr/ANTLRGrammarParseBehavior;->beginExceptionGroup()V

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v3}, Lantlr/ANTLRParser;->LA(I)I

    move-result v1

    const/16 v2, 0x27

    if-ne v1, v2, :cond_1

    invoke-virtual {p0}, Lantlr/ANTLRParser;->exceptionSpec()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    if-lt v0, v3, :cond_3

    iget-object v0, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v0, v0, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v0, :cond_2

    iget-object v0, p0, Lantlr/ANTLRParser;->behavior:Lantlr/ANTLRGrammarParseBehavior;

    invoke-interface {v0}, Lantlr/ANTLRGrammarParseBehavior;->endExceptionGroup()V

    :cond_2
    return-void

    :cond_3
    new-instance v0, Lantlr/NoViableAltException;

    invoke-virtual {p0, v3}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v1

    invoke-virtual {p0}, Lantlr/ANTLRParser;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lantlr/NoViableAltException;-><init>(Lantlr/Token;Ljava/lang/String;)V

    throw v0
.end method

.method public final exceptionHandler()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/RecognitionException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/4 v2, 0x1

    const/16 v0, 0x28

    invoke-virtual {p0, v0}, Lantlr/ANTLRParser;->match(I)V

    invoke-virtual {p0, v2}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v0

    const/16 v1, 0x22

    invoke-virtual {p0, v1}, Lantlr/ANTLRParser;->match(I)V

    invoke-virtual {p0, v2}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v1

    const/4 v2, 0x7

    invoke-virtual {p0, v2}, Lantlr/ANTLRParser;->match(I)V

    iget-object v2, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v2, v2, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v2, :cond_0

    iget-object v2, p0, Lantlr/ANTLRParser;->behavior:Lantlr/ANTLRGrammarParseBehavior;

    invoke-interface {v2, v0, v1}, Lantlr/ANTLRGrammarParseBehavior;->refExceptionHandler(Lantlr/Token;Lantlr/Token;)V

    :cond_0
    return-void
.end method

.method public final exceptionSpec()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/RecognitionException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/4 v3, 0x1

    const/4 v1, 0x0

    const/16 v0, 0x27

    invoke-virtual {p0, v0}, Lantlr/ANTLRParser;->match(I)V

    invoke-virtual {p0, v3}, Lantlr/ANTLRParser;->LA(I)I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    new-instance v0, Lantlr/NoViableAltException;

    invoke-virtual {p0, v3}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v1

    invoke-virtual {p0}, Lantlr/ANTLRParser;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lantlr/NoViableAltException;-><init>(Lantlr/Token;Ljava/lang/String;)V

    throw v0

    :sswitch_0
    invoke-virtual {p0, v3}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v0

    const/16 v2, 0x22

    invoke-virtual {p0, v2}, Lantlr/ANTLRParser;->match(I)V

    iget-object v2, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v2, v2, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v2, :cond_3

    :goto_0
    iget-object v1, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v1, v1, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v1, :cond_0

    iget-object v1, p0, Lantlr/ANTLRParser;->behavior:Lantlr/ANTLRGrammarParseBehavior;

    invoke-interface {v1, v0}, Lantlr/ANTLRGrammarParseBehavior;->beginExceptionSpec(Lantlr/Token;)V

    :cond_0
    :goto_1
    invoke-virtual {p0, v3}, Lantlr/ANTLRParser;->LA(I)I

    move-result v0

    const/16 v1, 0x28

    if-ne v0, v1, :cond_1

    invoke-virtual {p0}, Lantlr/ANTLRParser;->exceptionHandler()V

    goto :goto_1

    :sswitch_1
    move-object v0, v1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v0, v0, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v0, :cond_2

    iget-object v0, p0, Lantlr/ANTLRParser;->behavior:Lantlr/ANTLRGrammarParseBehavior;

    invoke-interface {v0}, Lantlr/ANTLRGrammarParseBehavior;->endExceptionSpec()V

    :cond_2
    return-void

    :cond_3
    move-object v0, v1

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x7 -> :sswitch_1
        0x8 -> :sswitch_1
        0x9 -> :sswitch_1
        0xa -> :sswitch_1
        0x18 -> :sswitch_1
        0x1e -> :sswitch_1
        0x1f -> :sswitch_1
        0x20 -> :sswitch_1
        0x22 -> :sswitch_0
        0x27 -> :sswitch_1
        0x28 -> :sswitch_1
        0x29 -> :sswitch_1
    .end sparse-switch
.end method

.method public final exceptionSpecNoLabel()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/RecognitionException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/16 v0, 0x27

    invoke-virtual {p0, v0}, Lantlr/ANTLRParser;->match(I)V

    iget-object v0, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v0, v0, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lantlr/ANTLRParser;->behavior:Lantlr/ANTLRGrammarParseBehavior;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lantlr/ANTLRGrammarParseBehavior;->beginExceptionSpec(Lantlr/Token;)V

    :cond_0
    :goto_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lantlr/ANTLRParser;->LA(I)I

    move-result v0

    const/16 v1, 0x28

    if-ne v0, v1, :cond_1

    invoke-virtual {p0}, Lantlr/ANTLRParser;->exceptionHandler()V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v0, v0, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v0, :cond_2

    iget-object v0, p0, Lantlr/ANTLRParser;->behavior:Lantlr/ANTLRGrammarParseBehavior;

    invoke-interface {v0}, Lantlr/ANTLRGrammarParseBehavior;->endExceptionSpec()V

    :cond_2
    return-void
.end method

.method public final fileOptionsSpec()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/RecognitionException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/4 v4, 0x1

    const/16 v0, 0xe

    invoke-virtual {p0, v0}, Lantlr/ANTLRParser;->match(I)V

    :goto_0
    invoke-virtual {p0, v4}, Lantlr/ANTLRParser;->LA(I)I

    move-result v0

    const/16 v1, 0x18

    if-eq v0, v1, :cond_0

    invoke-virtual {p0, v4}, Lantlr/ANTLRParser;->LA(I)I

    move-result v0

    const/16 v1, 0x29

    if-ne v0, v1, :cond_2

    :cond_0
    invoke-virtual {p0}, Lantlr/ANTLRParser;->id()Lantlr/Token;

    move-result-object v0

    const/16 v1, 0xf

    invoke-virtual {p0, v1}, Lantlr/ANTLRParser;->match(I)V

    invoke-virtual {p0}, Lantlr/ANTLRParser;->optionValue()Lantlr/Token;

    move-result-object v1

    iget-object v2, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v2, v2, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v2, :cond_1

    iget-object v2, p0, Lantlr/ANTLRParser;->behavior:Lantlr/ANTLRGrammarParseBehavior;

    invoke-virtual {p0}, Lantlr/ANTLRParser;->getInputState()Lantlr/ParserSharedInputState;

    move-result-object v3

    iget-object v3, v3, Lantlr/ParserSharedInputState;->filename:Ljava/lang/String;

    invoke-interface {v2, v0, v1, v3}, Lantlr/ANTLRGrammarParseBehavior;->setFileOption(Lantlr/Token;Lantlr/Token;Ljava/lang/String;)V

    :cond_1
    const/16 v0, 0x10

    invoke-virtual {p0, v0}, Lantlr/ANTLRParser;->match(I)V

    goto :goto_0

    :cond_2
    const/16 v0, 0x11

    invoke-virtual {p0, v0}, Lantlr/ANTLRParser;->match(I)V

    return-void
.end method

.method public final grammar()V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/RecognitionException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/4 v1, 0x0

    const/4 v6, 0x7

    const/4 v5, 0x5

    const/4 v4, 0x1

    move-object v0, v1

    :cond_0
    :goto_0
    const/4 v2, 0x1

    :try_start_0
    invoke-virtual {p0, v2}, Lantlr/ANTLRParser;->LA(I)I

    move-result v2

    if-ne v2, v5, :cond_2

    iget-object v2, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v2, v2, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v2, :cond_1

    move-object v0, v1

    :cond_1
    const/4 v2, 0x5

    invoke-virtual {p0, v2}, Lantlr/ANTLRParser;->match(I)V

    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lantlr/ANTLRParser;->LA(I)I

    move-result v2

    packed-switch v2, :pswitch_data_0

    new-instance v0, Lantlr/NoViableAltException;

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v1

    invoke-virtual {p0}, Lantlr/ANTLRParser;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lantlr/NoViableAltException;-><init>(Lantlr/Token;Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Lantlr/RecognitionException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    iget-object v1, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v1, v1, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v1, :cond_4

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "rule grammar trapped:\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v0}, Lantlr/RecognitionException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lantlr/ANTLRParser;->reportError(Lantlr/RecognitionException;Ljava/lang/String;)V

    invoke-virtual {p0, v4}, Lantlr/ANTLRParser;->consumeUntil(I)V

    :goto_1
    return-void

    :pswitch_0
    const/4 v0, 0x1

    :try_start_1
    invoke-virtual {p0, v0}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v0

    const/4 v2, 0x6

    invoke-virtual {p0, v2}, Lantlr/ANTLRParser;->match(I)V

    :pswitch_1
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v2

    const/4 v3, 0x7

    invoke-virtual {p0, v3}, Lantlr/ANTLRParser;->match(I)V

    iget-object v3, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v3, v3, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v3, :cond_0

    iget-object v3, p0, Lantlr/ANTLRParser;->behavior:Lantlr/ANTLRGrammarParseBehavior;

    invoke-interface {v3, v0, v2}, Lantlr/ANTLRGrammarParseBehavior;->refHeaderAction(Lantlr/Token;Lantlr/Token;)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lantlr/ANTLRParser;->LA(I)I

    move-result v0

    packed-switch v0, :pswitch_data_1

    :pswitch_2
    new-instance v0, Lantlr/NoViableAltException;

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v1

    invoke-virtual {p0}, Lantlr/ANTLRParser;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lantlr/NoViableAltException;-><init>(Lantlr/Token;Ljava/lang/String;)V

    throw v0

    :pswitch_3
    invoke-virtual {p0}, Lantlr/ANTLRParser;->fileOptionsSpec()V

    :goto_2
    :pswitch_4
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lantlr/ANTLRParser;->LA(I)I

    move-result v0

    if-lt v0, v6, :cond_3

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lantlr/ANTLRParser;->LA(I)I

    move-result v0

    const/16 v1, 0xa

    if-gt v0, v1, :cond_3

    invoke-virtual {p0}, Lantlr/ANTLRParser;->classDef()V

    goto :goto_2

    :cond_3
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lantlr/ANTLRParser;->match(I)V
    :try_end_1
    .catch Lantlr/RecognitionException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :cond_4
    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final id()Lantlr/Token;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/RecognitionException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v2}, Lantlr/ANTLRParser;->LA(I)I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    new-instance v0, Lantlr/NoViableAltException;

    invoke-virtual {p0, v2}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v1

    invoke-virtual {p0}, Lantlr/ANTLRParser;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lantlr/NoViableAltException;-><init>(Lantlr/Token;Ljava/lang/String;)V

    throw v0

    :sswitch_0
    invoke-virtual {p0, v2}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v0

    const/16 v2, 0x18

    invoke-virtual {p0, v2}, Lantlr/ANTLRParser;->match(I)V

    iget-object v2, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v2, v2, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :sswitch_1
    invoke-virtual {p0, v2}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v0

    const/16 v2, 0x29

    invoke-virtual {p0, v2}, Lantlr/ANTLRParser;->match(I)V

    iget-object v2, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v2, v2, Lantlr/ParserSharedInputState;->guessing:I

    if-eqz v2, :cond_0

    :cond_1
    move-object v0, v1

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x18 -> :sswitch_0
        0x29 -> :sswitch_1
    .end sparse-switch
.end method

.method public final lexerOptionsSpec()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/RecognitionException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/16 v4, 0x10

    const/16 v3, 0xf

    const/16 v0, 0xe

    invoke-virtual {p0, v0}, Lantlr/ANTLRParser;->match(I)V

    :cond_0
    :goto_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lantlr/ANTLRParser;->LA(I)I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    const/16 v0, 0x11

    invoke-virtual {p0, v0}, Lantlr/ANTLRParser;->match(I)V

    return-void

    :sswitch_0
    const/16 v0, 0x12

    invoke-virtual {p0, v0}, Lantlr/ANTLRParser;->match(I)V

    invoke-virtual {p0, v3}, Lantlr/ANTLRParser;->match(I)V

    invoke-virtual {p0}, Lantlr/ANTLRParser;->charSet()Lantlr/collections/impl/BitSet;

    move-result-object v0

    invoke-virtual {p0, v4}, Lantlr/ANTLRParser;->match(I)V

    iget-object v1, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v1, v1, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v1, :cond_0

    iget-object v1, p0, Lantlr/ANTLRParser;->behavior:Lantlr/ANTLRGrammarParseBehavior;

    invoke-interface {v1, v0}, Lantlr/ANTLRGrammarParseBehavior;->setCharVocabulary(Lantlr/collections/impl/BitSet;)V

    goto :goto_0

    :sswitch_1
    invoke-virtual {p0}, Lantlr/ANTLRParser;->id()Lantlr/Token;

    move-result-object v0

    invoke-virtual {p0, v3}, Lantlr/ANTLRParser;->match(I)V

    invoke-virtual {p0}, Lantlr/ANTLRParser;->optionValue()Lantlr/Token;

    move-result-object v1

    iget-object v2, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v2, v2, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v2, :cond_1

    iget-object v2, p0, Lantlr/ANTLRParser;->behavior:Lantlr/ANTLRGrammarParseBehavior;

    invoke-interface {v2, v0, v1}, Lantlr/ANTLRGrammarParseBehavior;->setGrammarOption(Lantlr/Token;Lantlr/Token;)V

    :cond_1
    invoke-virtual {p0, v4}, Lantlr/ANTLRParser;->match(I)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x12 -> :sswitch_0
        0x18 -> :sswitch_1
        0x29 -> :sswitch_1
    .end sparse-switch
.end method

.method public final lexerSpec(Ljava/lang/String;)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/RecognitionException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/4 v7, 0x1

    const/4 v0, 0x0

    invoke-virtual {p0, v7}, Lantlr/ANTLRParser;->LA(I)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    new-instance v0, Lantlr/NoViableAltException;

    invoke-virtual {p0, v7}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v1

    invoke-virtual {p0}, Lantlr/ANTLRParser;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lantlr/NoViableAltException;-><init>(Lantlr/Token;Ljava/lang/String;)V

    throw v0

    :pswitch_0
    invoke-virtual {p0, v7}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v2

    const/16 v1, 0x9

    invoke-virtual {p0, v1}, Lantlr/ANTLRParser;->match(I)V

    invoke-virtual {p0}, Lantlr/ANTLRParser;->id()Lantlr/Token;

    move-result-object v1

    iget-object v3, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v3, v3, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v3, :cond_0

    iget-object v3, p0, Lantlr/ANTLRParser;->antlrTool:Lantlr/Tool;

    const-string v4, "lexclass\' is deprecated; use \'class X extends Lexer\'"

    invoke-virtual {p0}, Lantlr/ANTLRParser;->getFilename()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2}, Lantlr/Token;->getLine()I

    move-result v6

    invoke-virtual {v2}, Lantlr/Token;->getColumn()I

    move-result v2

    invoke-virtual {v3, v4, v5, v6, v2}, Lantlr/Tool;->warning(Ljava/lang/String;Ljava/lang/String;II)V

    :cond_0
    :goto_0
    :sswitch_0
    iget-object v2, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v2, v2, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v2, :cond_1

    iget-object v2, p0, Lantlr/ANTLRParser;->behavior:Lantlr/ANTLRGrammarParseBehavior;

    invoke-virtual {p0}, Lantlr/ANTLRParser;->getFilename()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, v1, v0, p1}, Lantlr/ANTLRGrammarParseBehavior;->startLexer(Ljava/lang/String;Lantlr/Token;Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    const/16 v0, 0x10

    invoke-virtual {p0, v0}, Lantlr/ANTLRParser;->match(I)V

    invoke-virtual {p0, v7}, Lantlr/ANTLRParser;->LA(I)I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    new-instance v0, Lantlr/NoViableAltException;

    invoke-virtual {p0, v7}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v1

    invoke-virtual {p0}, Lantlr/ANTLRParser;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lantlr/NoViableAltException;-><init>(Lantlr/Token;Ljava/lang/String;)V

    throw v0

    :pswitch_1
    const/16 v1, 0xa

    invoke-virtual {p0, v1}, Lantlr/ANTLRParser;->match(I)V

    invoke-virtual {p0}, Lantlr/ANTLRParser;->id()Lantlr/Token;

    move-result-object v1

    const/16 v2, 0xb

    invoke-virtual {p0, v2}, Lantlr/ANTLRParser;->match(I)V

    const/16 v2, 0xc

    invoke-virtual {p0, v2}, Lantlr/ANTLRParser;->match(I)V

    invoke-virtual {p0, v7}, Lantlr/ANTLRParser;->LA(I)I

    move-result v2

    sparse-switch v2, :sswitch_data_1

    new-instance v0, Lantlr/NoViableAltException;

    invoke-virtual {p0, v7}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v1

    invoke-virtual {p0}, Lantlr/ANTLRParser;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lantlr/NoViableAltException;-><init>(Lantlr/Token;Ljava/lang/String;)V

    throw v0

    :sswitch_1
    invoke-virtual {p0}, Lantlr/ANTLRParser;->superClass()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :sswitch_2
    invoke-virtual {p0}, Lantlr/ANTLRParser;->lexerOptionsSpec()V

    :sswitch_3
    iget-object v0, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v0, v0, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v0, :cond_2

    iget-object v0, p0, Lantlr/ANTLRParser;->behavior:Lantlr/ANTLRGrammarParseBehavior;

    invoke-interface {v0}, Lantlr/ANTLRGrammarParseBehavior;->endOptions()V

    :cond_2
    invoke-virtual {p0, v7}, Lantlr/ANTLRParser;->LA(I)I

    move-result v0

    sparse-switch v0, :sswitch_data_2

    new-instance v0, Lantlr/NoViableAltException;

    invoke-virtual {p0, v7}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v1

    invoke-virtual {p0}, Lantlr/ANTLRParser;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lantlr/NoViableAltException;-><init>(Lantlr/Token;Ljava/lang/String;)V

    throw v0

    :sswitch_4
    invoke-virtual {p0}, Lantlr/ANTLRParser;->tokensSpec()V

    :sswitch_5
    invoke-virtual {p0, v7}, Lantlr/ANTLRParser;->LA(I)I

    move-result v0

    sparse-switch v0, :sswitch_data_3

    new-instance v0, Lantlr/NoViableAltException;

    invoke-virtual {p0, v7}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v1

    invoke-virtual {p0}, Lantlr/ANTLRParser;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lantlr/NoViableAltException;-><init>(Lantlr/Token;Ljava/lang/String;)V

    throw v0

    :sswitch_6
    invoke-virtual {p0, v7}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v0

    const/4 v1, 0x7

    invoke-virtual {p0, v1}, Lantlr/ANTLRParser;->match(I)V

    iget-object v1, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v1, v1, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v1, :cond_3

    iget-object v1, p0, Lantlr/ANTLRParser;->behavior:Lantlr/ANTLRGrammarParseBehavior;

    invoke-interface {v1, v0}, Lantlr/ANTLRGrammarParseBehavior;->refMemberAction(Lantlr/Token;)V

    :cond_3
    :sswitch_7
    return-void

    :pswitch_data_0
    .packed-switch 0x9
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :sswitch_data_0
    .sparse-switch
        0x7 -> :sswitch_3
        0x8 -> :sswitch_3
        0xe -> :sswitch_2
        0x17 -> :sswitch_3
        0x18 -> :sswitch_3
        0x1e -> :sswitch_3
        0x1f -> :sswitch_3
        0x20 -> :sswitch_3
        0x29 -> :sswitch_3
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x10 -> :sswitch_0
        0x1b -> :sswitch_1
    .end sparse-switch

    :sswitch_data_2
    .sparse-switch
        0x7 -> :sswitch_5
        0x8 -> :sswitch_5
        0x17 -> :sswitch_4
        0x18 -> :sswitch_5
        0x1e -> :sswitch_5
        0x1f -> :sswitch_5
        0x20 -> :sswitch_5
        0x29 -> :sswitch_5
    .end sparse-switch

    :sswitch_data_3
    .sparse-switch
        0x7 -> :sswitch_6
        0x8 -> :sswitch_7
        0x18 -> :sswitch_7
        0x1e -> :sswitch_7
        0x1f -> :sswitch_7
        0x20 -> :sswitch_7
        0x29 -> :sswitch_7
    .end sparse-switch
.end method

.method public final notTerminal(Lantlr/Token;)V
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/RecognitionException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/4 v5, 0x0

    const/4 v3, 0x1

    invoke-virtual {p0, v3}, Lantlr/ANTLRParser;->LA(I)I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    new-instance v0, Lantlr/NoViableAltException;

    invoke-virtual {p0, v3}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v1

    invoke-virtual {p0}, Lantlr/ANTLRParser;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lantlr/NoViableAltException;-><init>(Lantlr/Token;Ljava/lang/String;)V

    throw v0

    :sswitch_0
    invoke-virtual {p0, v3}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v1

    const/16 v0, 0x13

    invoke-virtual {p0, v0}, Lantlr/ANTLRParser;->match(I)V

    invoke-virtual {p0, v3}, Lantlr/ANTLRParser;->LA(I)I

    move-result v0

    sparse-switch v0, :sswitch_data_1

    new-instance v0, Lantlr/NoViableAltException;

    invoke-virtual {p0, v3}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v1

    invoke-virtual {p0}, Lantlr/ANTLRParser;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lantlr/NoViableAltException;-><init>(Lantlr/Token;Ljava/lang/String;)V

    throw v0

    :sswitch_1
    const/16 v0, 0x21

    invoke-virtual {p0, v0}, Lantlr/ANTLRParser;->match(I)V

    iget-object v0, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v0, v0, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v0, :cond_1

    const/4 v4, 0x3

    :goto_0
    iget-object v0, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v0, v0, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lantlr/ANTLRParser;->behavior:Lantlr/ANTLRGrammarParseBehavior;

    invoke-direct {p0}, Lantlr/ANTLRParser;->lastInRule()Z

    move-result v5

    move-object v2, p1

    invoke-interface/range {v0 .. v5}, Lantlr/ANTLRGrammarParseBehavior;->refCharLiteral(Lantlr/Token;Lantlr/Token;ZIZ)V

    :cond_0
    :goto_1
    return-void

    :sswitch_2
    move v4, v3

    goto :goto_0

    :sswitch_3
    invoke-virtual {p0, v3}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v6

    const/16 v0, 0x18

    invoke-virtual {p0, v0}, Lantlr/ANTLRParser;->match(I)V

    invoke-virtual {p0}, Lantlr/ANTLRParser;->ast_type_spec()I

    move-result v10

    iget-object v0, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v0, v0, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v0, :cond_0

    iget-object v4, p0, Lantlr/ANTLRParser;->behavior:Lantlr/ANTLRGrammarParseBehavior;

    invoke-direct {p0}, Lantlr/ANTLRParser;->lastInRule()Z

    move-result v11

    move-object v7, p1

    move-object v8, v5

    move v9, v3

    invoke-interface/range {v4 .. v11}, Lantlr/ANTLRGrammarParseBehavior;->refToken(Lantlr/Token;Lantlr/Token;Lantlr/Token;Lantlr/Token;ZIZ)V

    goto :goto_1

    :cond_1
    move v4, v3

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x13 -> :sswitch_0
        0x18 -> :sswitch_3
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x6 -> :sswitch_2
        0x7 -> :sswitch_2
        0x10 -> :sswitch_2
        0x13 -> :sswitch_2
        0x15 -> :sswitch_2
        0x18 -> :sswitch_2
        0x19 -> :sswitch_2
        0x1b -> :sswitch_2
        0x1c -> :sswitch_2
        0x21 -> :sswitch_1
        0x27 -> :sswitch_2
        0x29 -> :sswitch_2
        0x2a -> :sswitch_2
        0x2b -> :sswitch_2
        0x2c -> :sswitch_2
        0x32 -> :sswitch_2
    .end sparse-switch
.end method

.method public final optionValue()Lantlr/Token;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/RecognitionException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v2}, Lantlr/ANTLRParser;->LA(I)I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    new-instance v0, Lantlr/NoViableAltException;

    invoke-virtual {p0, v2}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v1

    invoke-virtual {p0}, Lantlr/ANTLRParser;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lantlr/NoViableAltException;-><init>(Lantlr/Token;Ljava/lang/String;)V

    throw v0

    :sswitch_0
    invoke-virtual {p0}, Lantlr/ANTLRParser;->qualifiedID()Lantlr/Token;

    move-result-object v0

    :cond_0
    :goto_0
    return-object v0

    :sswitch_1
    invoke-virtual {p0, v2}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v0

    const/4 v2, 0x6

    invoke-virtual {p0, v2}, Lantlr/ANTLRParser;->match(I)V

    iget-object v2, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v2, v2, Lantlr/ParserSharedInputState;->guessing:I

    if-eqz v2, :cond_0

    :cond_1
    move-object v0, v1

    goto :goto_0

    :sswitch_2
    invoke-virtual {p0, v2}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v0

    const/16 v2, 0x13

    invoke-virtual {p0, v2}, Lantlr/ANTLRParser;->match(I)V

    iget-object v2, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v2, v2, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v2, :cond_1

    goto :goto_0

    :sswitch_3
    invoke-virtual {p0, v2}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v0

    const/16 v2, 0x14

    invoke-virtual {p0, v2}, Lantlr/ANTLRParser;->match(I)V

    iget-object v2, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v2, v2, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v2, :cond_1

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x6 -> :sswitch_1
        0x13 -> :sswitch_2
        0x14 -> :sswitch_3
        0x18 -> :sswitch_0
        0x29 -> :sswitch_0
    .end sparse-switch
.end method

.method public final parserOptionsSpec()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/RecognitionException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/4 v3, 0x1

    const/16 v0, 0xe

    invoke-virtual {p0, v0}, Lantlr/ANTLRParser;->match(I)V

    :goto_0
    invoke-virtual {p0, v3}, Lantlr/ANTLRParser;->LA(I)I

    move-result v0

    const/16 v1, 0x18

    if-eq v0, v1, :cond_0

    invoke-virtual {p0, v3}, Lantlr/ANTLRParser;->LA(I)I

    move-result v0

    const/16 v1, 0x29

    if-ne v0, v1, :cond_2

    :cond_0
    invoke-virtual {p0}, Lantlr/ANTLRParser;->id()Lantlr/Token;

    move-result-object v0

    const/16 v1, 0xf

    invoke-virtual {p0, v1}, Lantlr/ANTLRParser;->match(I)V

    invoke-virtual {p0}, Lantlr/ANTLRParser;->optionValue()Lantlr/Token;

    move-result-object v1

    iget-object v2, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v2, v2, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v2, :cond_1

    iget-object v2, p0, Lantlr/ANTLRParser;->behavior:Lantlr/ANTLRGrammarParseBehavior;

    invoke-interface {v2, v0, v1}, Lantlr/ANTLRGrammarParseBehavior;->setGrammarOption(Lantlr/Token;Lantlr/Token;)V

    :cond_1
    const/16 v0, 0x10

    invoke-virtual {p0, v0}, Lantlr/ANTLRParser;->match(I)V

    goto :goto_0

    :cond_2
    const/16 v0, 0x11

    invoke-virtual {p0, v0}, Lantlr/ANTLRParser;->match(I)V

    return-void
.end method

.method public final parserSpec(Ljava/lang/String;)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/RecognitionException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/4 v7, 0x1

    const/4 v0, 0x0

    const/16 v1, 0xa

    invoke-virtual {p0, v1}, Lantlr/ANTLRParser;->match(I)V

    invoke-virtual {p0}, Lantlr/ANTLRParser;->id()Lantlr/Token;

    move-result-object v1

    invoke-virtual {p0, v7}, Lantlr/ANTLRParser;->LA(I)I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    new-instance v0, Lantlr/NoViableAltException;

    invoke-virtual {p0, v7}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v1

    invoke-virtual {p0}, Lantlr/ANTLRParser;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lantlr/NoViableAltException;-><init>(Lantlr/Token;Ljava/lang/String;)V

    throw v0

    :sswitch_0
    const/16 v2, 0xb

    invoke-virtual {p0, v2}, Lantlr/ANTLRParser;->match(I)V

    const/16 v2, 0x1d

    invoke-virtual {p0, v2}, Lantlr/ANTLRParser;->match(I)V

    invoke-virtual {p0, v7}, Lantlr/ANTLRParser;->LA(I)I

    move-result v2

    sparse-switch v2, :sswitch_data_1

    new-instance v0, Lantlr/NoViableAltException;

    invoke-virtual {p0, v7}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v1

    invoke-virtual {p0}, Lantlr/ANTLRParser;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lantlr/NoViableAltException;-><init>(Lantlr/Token;Ljava/lang/String;)V

    throw v0

    :sswitch_1
    invoke-virtual {p0}, Lantlr/ANTLRParser;->superClass()Ljava/lang/String;

    move-result-object v0

    :cond_0
    :goto_0
    :sswitch_2
    iget-object v2, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v2, v2, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v2, :cond_1

    iget-object v2, p0, Lantlr/ANTLRParser;->behavior:Lantlr/ANTLRGrammarParseBehavior;

    invoke-virtual {p0}, Lantlr/ANTLRParser;->getFilename()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, v1, v0, p1}, Lantlr/ANTLRGrammarParseBehavior;->startParser(Ljava/lang/String;Lantlr/Token;Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    const/16 v0, 0x10

    invoke-virtual {p0, v0}, Lantlr/ANTLRParser;->match(I)V

    invoke-virtual {p0, v7}, Lantlr/ANTLRParser;->LA(I)I

    move-result v0

    sparse-switch v0, :sswitch_data_2

    new-instance v0, Lantlr/NoViableAltException;

    invoke-virtual {p0, v7}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v1

    invoke-virtual {p0}, Lantlr/ANTLRParser;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lantlr/NoViableAltException;-><init>(Lantlr/Token;Ljava/lang/String;)V

    throw v0

    :sswitch_3
    iget-object v2, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v2, v2, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v2, :cond_0

    iget-object v2, p0, Lantlr/ANTLRParser;->antlrTool:Lantlr/Tool;

    const-string v3, "use \'class X extends Parser\'"

    invoke-virtual {p0}, Lantlr/ANTLRParser;->getFilename()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1}, Lantlr/Token;->getLine()I

    move-result v5

    invoke-virtual {v1}, Lantlr/Token;->getColumn()I

    move-result v6

    invoke-virtual {v2, v3, v4, v5, v6}, Lantlr/Tool;->warning(Ljava/lang/String;Ljava/lang/String;II)V

    goto :goto_0

    :sswitch_4
    invoke-virtual {p0}, Lantlr/ANTLRParser;->parserOptionsSpec()V

    :sswitch_5
    iget-object v0, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v0, v0, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v0, :cond_2

    iget-object v0, p0, Lantlr/ANTLRParser;->behavior:Lantlr/ANTLRGrammarParseBehavior;

    invoke-interface {v0}, Lantlr/ANTLRGrammarParseBehavior;->endOptions()V

    :cond_2
    invoke-virtual {p0, v7}, Lantlr/ANTLRParser;->LA(I)I

    move-result v0

    sparse-switch v0, :sswitch_data_3

    new-instance v0, Lantlr/NoViableAltException;

    invoke-virtual {p0, v7}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v1

    invoke-virtual {p0}, Lantlr/ANTLRParser;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lantlr/NoViableAltException;-><init>(Lantlr/Token;Ljava/lang/String;)V

    throw v0

    :sswitch_6
    invoke-virtual {p0}, Lantlr/ANTLRParser;->tokensSpec()V

    :sswitch_7
    invoke-virtual {p0, v7}, Lantlr/ANTLRParser;->LA(I)I

    move-result v0

    sparse-switch v0, :sswitch_data_4

    new-instance v0, Lantlr/NoViableAltException;

    invoke-virtual {p0, v7}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v1

    invoke-virtual {p0}, Lantlr/ANTLRParser;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lantlr/NoViableAltException;-><init>(Lantlr/Token;Ljava/lang/String;)V

    throw v0

    :sswitch_8
    invoke-virtual {p0, v7}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v0

    const/4 v1, 0x7

    invoke-virtual {p0, v1}, Lantlr/ANTLRParser;->match(I)V

    iget-object v1, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v1, v1, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v1, :cond_3

    iget-object v1, p0, Lantlr/ANTLRParser;->behavior:Lantlr/ANTLRGrammarParseBehavior;

    invoke-interface {v1, v0}, Lantlr/ANTLRGrammarParseBehavior;->refMemberAction(Lantlr/Token;)V

    :cond_3
    :sswitch_9
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        0xb -> :sswitch_0
        0x10 -> :sswitch_3
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x10 -> :sswitch_2
        0x1b -> :sswitch_1
    .end sparse-switch

    :sswitch_data_2
    .sparse-switch
        0x7 -> :sswitch_5
        0x8 -> :sswitch_5
        0xe -> :sswitch_4
        0x17 -> :sswitch_5
        0x18 -> :sswitch_5
        0x1e -> :sswitch_5
        0x1f -> :sswitch_5
        0x20 -> :sswitch_5
        0x29 -> :sswitch_5
    .end sparse-switch

    :sswitch_data_3
    .sparse-switch
        0x7 -> :sswitch_7
        0x8 -> :sswitch_7
        0x17 -> :sswitch_6
        0x18 -> :sswitch_7
        0x1e -> :sswitch_7
        0x1f -> :sswitch_7
        0x20 -> :sswitch_7
        0x29 -> :sswitch_7
    .end sparse-switch

    :sswitch_data_4
    .sparse-switch
        0x7 -> :sswitch_8
        0x8 -> :sswitch_9
        0x18 -> :sswitch_9
        0x1e -> :sswitch_9
        0x1f -> :sswitch_9
        0x20 -> :sswitch_9
        0x29 -> :sswitch_9
    .end sparse-switch
.end method

.method public final qualifiedID()Lantlr/Token;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/RecognitionException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/16 v4, 0x32

    const/4 v1, 0x0

    new-instance v2, Ljava/lang/StringBuffer;

    const/16 v0, 0x1e

    invoke-direct {v2, v0}, Ljava/lang/StringBuffer;-><init>(I)V

    invoke-virtual {p0}, Lantlr/ANTLRParser;->id()Lantlr/Token;

    move-result-object v0

    iget-object v3, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v3, v3, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v3, :cond_0

    invoke-virtual {v0}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_0
    :goto_0
    const/4 v3, 0x1

    invoke-virtual {p0, v3}, Lantlr/ANTLRParser;->LA(I)I

    move-result v3

    if-ne v3, v4, :cond_1

    invoke-virtual {p0, v4}, Lantlr/ANTLRParser;->match(I)V

    invoke-virtual {p0}, Lantlr/ANTLRParser;->id()Lantlr/Token;

    move-result-object v0

    iget-object v3, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v3, v3, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v3, :cond_0

    const/16 v3, 0x2e

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    invoke-virtual {v0}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    :cond_1
    iget-object v3, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v3, v3, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v3, :cond_2

    new-instance v1, Lantlr/CommonToken;

    const/16 v3, 0x18

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v3, v2}, Lantlr/CommonToken;-><init>(ILjava/lang/String;)V

    invoke-virtual {v0}, Lantlr/Token;->getLine()I

    move-result v0

    invoke-virtual {v1, v0}, Lantlr/Token;->setLine(I)V

    move-object v0, v1

    :goto_1
    return-object v0

    :cond_2
    move-object v0, v1

    goto :goto_1
.end method

.method public final range(Lantlr/Token;)V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/RecognitionException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/16 v8, 0x18

    const/16 v7, 0x16

    const/16 v6, 0x13

    const/4 v5, 0x6

    const/4 v4, 0x1

    const/4 v0, 0x0

    const/4 v3, 0x0

    invoke-virtual {p0, v4}, Lantlr/ANTLRParser;->LA(I)I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    new-instance v0, Lantlr/NoViableAltException;

    invoke-virtual {p0, v4}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v1

    invoke-virtual {p0}, Lantlr/ANTLRParser;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lantlr/NoViableAltException;-><init>(Lantlr/Token;Ljava/lang/String;)V

    throw v0

    :sswitch_0
    invoke-virtual {p0, v4}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v1

    invoke-virtual {p0, v6}, Lantlr/ANTLRParser;->match(I)V

    invoke-virtual {p0, v7}, Lantlr/ANTLRParser;->match(I)V

    invoke-virtual {p0, v4}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v2

    invoke-virtual {p0, v6}, Lantlr/ANTLRParser;->match(I)V

    invoke-virtual {p0, v4}, Lantlr/ANTLRParser;->LA(I)I

    move-result v0

    sparse-switch v0, :sswitch_data_1

    new-instance v0, Lantlr/NoViableAltException;

    invoke-virtual {p0, v4}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v1

    invoke-virtual {p0}, Lantlr/ANTLRParser;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lantlr/NoViableAltException;-><init>(Lantlr/Token;Ljava/lang/String;)V

    throw v0

    :sswitch_1
    const/16 v0, 0x21

    invoke-virtual {p0, v0}, Lantlr/ANTLRParser;->match(I)V

    iget-object v0, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v0, v0, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v0, :cond_0

    const/4 v4, 0x3

    :cond_0
    :sswitch_2
    iget-object v0, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v0, v0, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v0, :cond_1

    iget-object v0, p0, Lantlr/ANTLRParser;->behavior:Lantlr/ANTLRGrammarParseBehavior;

    invoke-direct {p0}, Lantlr/ANTLRParser;->lastInRule()Z

    move-result v5

    move-object v3, p1

    invoke-interface/range {v0 .. v5}, Lantlr/ANTLRGrammarParseBehavior;->refCharRange(Lantlr/Token;Lantlr/Token;Lantlr/Token;IZ)V

    :cond_1
    :goto_0
    return-void

    :sswitch_3
    invoke-virtual {p0, v4}, Lantlr/ANTLRParser;->LA(I)I

    move-result v1

    sparse-switch v1, :sswitch_data_2

    new-instance v0, Lantlr/NoViableAltException;

    invoke-virtual {p0, v4}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v1

    invoke-virtual {p0}, Lantlr/ANTLRParser;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lantlr/NoViableAltException;-><init>(Lantlr/Token;Ljava/lang/String;)V

    throw v0

    :sswitch_4
    invoke-virtual {p0, v4}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v1

    invoke-virtual {p0, v8}, Lantlr/ANTLRParser;->match(I)V

    iget-object v2, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v2, v2, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v2, :cond_3

    :cond_2
    :goto_1
    invoke-virtual {p0, v7}, Lantlr/ANTLRParser;->match(I)V

    invoke-virtual {p0, v4}, Lantlr/ANTLRParser;->LA(I)I

    move-result v0

    sparse-switch v0, :sswitch_data_3

    new-instance v0, Lantlr/NoViableAltException;

    invoke-virtual {p0, v4}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v1

    invoke-virtual {p0}, Lantlr/ANTLRParser;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lantlr/NoViableAltException;-><init>(Lantlr/Token;Ljava/lang/String;)V

    throw v0

    :sswitch_5
    invoke-virtual {p0, v4}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v1

    invoke-virtual {p0, v5}, Lantlr/ANTLRParser;->match(I)V

    iget-object v2, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v2, v2, Lantlr/ParserSharedInputState;->guessing:I

    if-eqz v2, :cond_2

    :cond_3
    move-object v1, v0

    goto :goto_1

    :sswitch_6
    invoke-virtual {p0, v4}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v2

    invoke-virtual {p0, v8}, Lantlr/ANTLRParser;->match(I)V

    iget-object v0, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v0, v0, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v0, :cond_5

    :cond_4
    :goto_2
    invoke-virtual {p0}, Lantlr/ANTLRParser;->ast_type_spec()I

    move-result v4

    iget-object v0, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v0, v0, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v0, :cond_1

    iget-object v0, p0, Lantlr/ANTLRParser;->behavior:Lantlr/ANTLRGrammarParseBehavior;

    invoke-direct {p0}, Lantlr/ANTLRParser;->lastInRule()Z

    move-result v5

    move-object v3, p1

    invoke-interface/range {v0 .. v5}, Lantlr/ANTLRGrammarParseBehavior;->refTokenRange(Lantlr/Token;Lantlr/Token;Lantlr/Token;IZ)V

    goto :goto_0

    :sswitch_7
    invoke-virtual {p0, v4}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v2

    invoke-virtual {p0, v5}, Lantlr/ANTLRParser;->match(I)V

    iget-object v0, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v0, v0, Lantlr/ParserSharedInputState;->guessing:I

    if-eqz v0, :cond_4

    :cond_5
    move-object v2, v3

    goto :goto_2

    :sswitch_data_0
    .sparse-switch
        0x6 -> :sswitch_3
        0x13 -> :sswitch_0
        0x18 -> :sswitch_3
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x6 -> :sswitch_2
        0x7 -> :sswitch_2
        0x10 -> :sswitch_2
        0x13 -> :sswitch_2
        0x15 -> :sswitch_2
        0x18 -> :sswitch_2
        0x19 -> :sswitch_2
        0x1b -> :sswitch_2
        0x1c -> :sswitch_2
        0x21 -> :sswitch_1
        0x27 -> :sswitch_2
        0x29 -> :sswitch_2
        0x2a -> :sswitch_2
        0x2b -> :sswitch_2
        0x2c -> :sswitch_2
        0x32 -> :sswitch_2
    .end sparse-switch

    :sswitch_data_2
    .sparse-switch
        0x6 -> :sswitch_5
        0x18 -> :sswitch_4
    .end sparse-switch

    :sswitch_data_3
    .sparse-switch
        0x6 -> :sswitch_7
        0x18 -> :sswitch_6
    .end sparse-switch
.end method

.method public reportError(Lantlr/RecognitionException;)V
    .locals 1

    invoke-virtual {p1}, Lantlr/RecognitionException;->getErrorMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lantlr/ANTLRParser;->reportError(Lantlr/RecognitionException;Ljava/lang/String;)V

    return-void
.end method

.method public reportError(Lantlr/RecognitionException;Ljava/lang/String;)V
    .locals 4

    iget-object v0, p0, Lantlr/ANTLRParser;->antlrTool:Lantlr/Tool;

    invoke-virtual {p1}, Lantlr/RecognitionException;->getFilename()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lantlr/RecognitionException;->getLine()I

    move-result v2

    invoke-virtual {p1}, Lantlr/RecognitionException;->getColumn()I

    move-result v3

    invoke-virtual {v0, p2, v1, v2, v3}, Lantlr/Tool;->error(Ljava/lang/String;Ljava/lang/String;II)V

    return-void
.end method

.method public reportError(Ljava/lang/String;)V
    .locals 3

    const/4 v2, -0x1

    iget-object v0, p0, Lantlr/ANTLRParser;->antlrTool:Lantlr/Tool;

    invoke-virtual {p0}, Lantlr/ANTLRParser;->getFilename()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1, v2, v2}, Lantlr/Tool;->error(Ljava/lang/String;Ljava/lang/String;II)V

    return-void
.end method

.method public reportWarning(Ljava/lang/String;)V
    .locals 3

    const/4 v2, -0x1

    iget-object v0, p0, Lantlr/ANTLRParser;->antlrTool:Lantlr/Tool;

    invoke-virtual {p0}, Lantlr/ANTLRParser;->getFilename()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1, v2, v2}, Lantlr/Tool;->warning(Ljava/lang/String;Ljava/lang/String;II)V

    return-void
.end method

.method public final rootNode()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/RecognitionException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/16 v5, 0x24

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v0, 0x0

    invoke-virtual {p0, v3}, Lantlr/ANTLRParser;->LA(I)I

    move-result v1

    const/16 v2, 0x18

    if-eq v1, v2, :cond_0

    invoke-virtual {p0, v3}, Lantlr/ANTLRParser;->LA(I)I

    move-result v1

    const/16 v2, 0x29

    if-ne v1, v2, :cond_2

    :cond_0
    invoke-virtual {p0, v4}, Lantlr/ANTLRParser;->LA(I)I

    move-result v1

    if-ne v1, v5, :cond_2

    invoke-virtual {p0}, Lantlr/ANTLRParser;->id()Lantlr/Token;

    move-result-object v0

    invoke-virtual {p0, v5}, Lantlr/ANTLRParser;->match(I)V

    iget-object v1, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v1, v1, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v1, :cond_1

    invoke-direct {p0, v0}, Lantlr/ANTLRParser;->checkForMissingEndRule(Lantlr/Token;)V

    :cond_1
    invoke-virtual {p0, v0}, Lantlr/ANTLRParser;->terminal(Lantlr/Token;)V

    return-void

    :cond_2
    sget-object v1, Lantlr/ANTLRParser;->_tokenSet_7:Lantlr/collections/impl/BitSet;

    invoke-virtual {p0, v3}, Lantlr/ANTLRParser;->LA(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lantlr/collections/impl/BitSet;->member(I)Z

    move-result v1

    if-eqz v1, :cond_3

    sget-object v1, Lantlr/ANTLRParser;->_tokenSet_11:Lantlr/collections/impl/BitSet;

    invoke-virtual {p0, v4}, Lantlr/ANTLRParser;->LA(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lantlr/collections/impl/BitSet;->member(I)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_3
    new-instance v0, Lantlr/NoViableAltException;

    invoke-virtual {p0, v3}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v1

    invoke-virtual {p0}, Lantlr/ANTLRParser;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lantlr/NoViableAltException;-><init>(Lantlr/Token;Ljava/lang/String;)V

    throw v0
.end method

.method public final rule()V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/RecognitionException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/16 v6, 0x22

    const/4 v3, 0x1

    const-string v1, "public"

    const/4 v0, 0x0

    const/4 v2, -0x1

    iput v2, p0, Lantlr/ANTLRParser;->blockNesting:I

    invoke-virtual {p0, v3}, Lantlr/ANTLRParser;->LA(I)I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    new-instance v0, Lantlr/NoViableAltException;

    invoke-virtual {p0, v3}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v1

    invoke-virtual {p0}, Lantlr/ANTLRParser;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lantlr/NoViableAltException;-><init>(Lantlr/Token;Ljava/lang/String;)V

    throw v0

    :sswitch_0
    invoke-virtual {p0, v3}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v2

    const/16 v4, 0x8

    invoke-virtual {p0, v4}, Lantlr/ANTLRParser;->match(I)V

    iget-object v4, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v4, v4, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v4, :cond_0

    invoke-virtual {v2}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v0

    :cond_0
    :sswitch_1
    invoke-virtual {p0, v3}, Lantlr/ANTLRParser;->LA(I)I

    move-result v2

    sparse-switch v2, :sswitch_data_1

    new-instance v0, Lantlr/NoViableAltException;

    invoke-virtual {p0, v3}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v1

    invoke-virtual {p0}, Lantlr/ANTLRParser;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lantlr/NoViableAltException;-><init>(Lantlr/Token;Ljava/lang/String;)V

    throw v0

    :sswitch_2
    invoke-virtual {p0, v3}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v2

    const/16 v4, 0x1e

    invoke-virtual {p0, v4}, Lantlr/ANTLRParser;->match(I)V

    iget-object v4, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v4, v4, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v4, :cond_1

    invoke-virtual {v2}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v1

    :cond_1
    :goto_0
    :sswitch_3
    invoke-virtual {p0}, Lantlr/ANTLRParser;->id()Lantlr/Token;

    move-result-object v4

    invoke-virtual {p0, v3}, Lantlr/ANTLRParser;->LA(I)I

    move-result v2

    sparse-switch v2, :sswitch_data_2

    new-instance v0, Lantlr/NoViableAltException;

    invoke-virtual {p0, v3}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v1

    invoke-virtual {p0}, Lantlr/ANTLRParser;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lantlr/NoViableAltException;-><init>(Lantlr/Token;Ljava/lang/String;)V

    throw v0

    :sswitch_4
    invoke-virtual {p0, v3}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v2

    const/16 v4, 0x1f

    invoke-virtual {p0, v4}, Lantlr/ANTLRParser;->match(I)V

    iget-object v4, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v4, v4, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v4, :cond_1

    invoke-virtual {v2}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :sswitch_5
    invoke-virtual {p0, v3}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v2

    const/16 v4, 0x20

    invoke-virtual {p0, v4}, Lantlr/ANTLRParser;->match(I)V

    iget-object v4, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v4, v4, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v4, :cond_1

    invoke-virtual {v2}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :sswitch_6
    const/16 v2, 0x21

    invoke-virtual {p0, v2}, Lantlr/ANTLRParser;->match(I)V

    iget-object v2, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v2, v2, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v2, :cond_7

    const/4 v2, 0x0

    :goto_1
    iget-object v5, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v5, v5, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v5, :cond_2

    iget-object v5, p0, Lantlr/ANTLRParser;->behavior:Lantlr/ANTLRGrammarParseBehavior;

    invoke-interface {v5, v4, v1, v2, v0}, Lantlr/ANTLRGrammarParseBehavior;->defineRuleName(Lantlr/Token;Ljava/lang/String;ZLjava/lang/String;)V

    :cond_2
    invoke-virtual {p0, v3}, Lantlr/ANTLRParser;->LA(I)I

    move-result v0

    sparse-switch v0, :sswitch_data_3

    new-instance v0, Lantlr/NoViableAltException;

    invoke-virtual {p0, v3}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v1

    invoke-virtual {p0}, Lantlr/ANTLRParser;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lantlr/NoViableAltException;-><init>(Lantlr/Token;Ljava/lang/String;)V

    throw v0

    :sswitch_7
    move v2, v3

    goto :goto_1

    :sswitch_8
    invoke-virtual {p0, v3}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v0

    invoke-virtual {p0, v6}, Lantlr/ANTLRParser;->match(I)V

    iget-object v1, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v1, v1, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v1, :cond_3

    iget-object v1, p0, Lantlr/ANTLRParser;->behavior:Lantlr/ANTLRGrammarParseBehavior;

    invoke-interface {v1, v0}, Lantlr/ANTLRGrammarParseBehavior;->refArgAction(Lantlr/Token;)V

    :cond_3
    :sswitch_9
    invoke-virtual {p0, v3}, Lantlr/ANTLRParser;->LA(I)I

    move-result v0

    sparse-switch v0, :sswitch_data_4

    new-instance v0, Lantlr/NoViableAltException;

    invoke-virtual {p0, v3}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v1

    invoke-virtual {p0}, Lantlr/ANTLRParser;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lantlr/NoViableAltException;-><init>(Lantlr/Token;Ljava/lang/String;)V

    throw v0

    :sswitch_a
    const/16 v0, 0x23

    invoke-virtual {p0, v0}, Lantlr/ANTLRParser;->match(I)V

    invoke-virtual {p0, v3}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v0

    invoke-virtual {p0, v6}, Lantlr/ANTLRParser;->match(I)V

    iget-object v1, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v1, v1, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v1, :cond_4

    iget-object v1, p0, Lantlr/ANTLRParser;->behavior:Lantlr/ANTLRGrammarParseBehavior;

    invoke-interface {v1, v0}, Lantlr/ANTLRGrammarParseBehavior;->refReturnAction(Lantlr/Token;)V

    :cond_4
    :sswitch_b
    invoke-virtual {p0, v3}, Lantlr/ANTLRParser;->LA(I)I

    move-result v0

    sparse-switch v0, :sswitch_data_5

    new-instance v0, Lantlr/NoViableAltException;

    invoke-virtual {p0, v3}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v1

    invoke-virtual {p0}, Lantlr/ANTLRParser;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lantlr/NoViableAltException;-><init>(Lantlr/Token;Ljava/lang/String;)V

    throw v0

    :sswitch_c
    invoke-virtual {p0}, Lantlr/ANTLRParser;->throwsSpec()V

    :sswitch_d
    invoke-virtual {p0, v3}, Lantlr/ANTLRParser;->LA(I)I

    move-result v0

    sparse-switch v0, :sswitch_data_6

    new-instance v0, Lantlr/NoViableAltException;

    invoke-virtual {p0, v3}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v1

    invoke-virtual {p0}, Lantlr/ANTLRParser;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lantlr/NoViableAltException;-><init>(Lantlr/Token;Ljava/lang/String;)V

    throw v0

    :sswitch_e
    invoke-virtual {p0}, Lantlr/ANTLRParser;->ruleOptionsSpec()V

    :sswitch_f
    invoke-virtual {p0, v3}, Lantlr/ANTLRParser;->LA(I)I

    move-result v0

    sparse-switch v0, :sswitch_data_7

    new-instance v0, Lantlr/NoViableAltException;

    invoke-virtual {p0, v3}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v1

    invoke-virtual {p0}, Lantlr/ANTLRParser;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lantlr/NoViableAltException;-><init>(Lantlr/Token;Ljava/lang/String;)V

    throw v0

    :sswitch_10
    invoke-virtual {p0, v3}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v0

    const/4 v1, 0x7

    invoke-virtual {p0, v1}, Lantlr/ANTLRParser;->match(I)V

    iget-object v1, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v1, v1, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v1, :cond_5

    iget-object v1, p0, Lantlr/ANTLRParser;->behavior:Lantlr/ANTLRGrammarParseBehavior;

    invoke-interface {v1, v0}, Lantlr/ANTLRGrammarParseBehavior;->refInitAction(Lantlr/Token;)V

    :cond_5
    :sswitch_11
    const/16 v0, 0x24

    invoke-virtual {p0, v0}, Lantlr/ANTLRParser;->match(I)V

    invoke-virtual {p0}, Lantlr/ANTLRParser;->block()V

    const/16 v0, 0x10

    invoke-virtual {p0, v0}, Lantlr/ANTLRParser;->match(I)V

    invoke-virtual {p0, v3}, Lantlr/ANTLRParser;->LA(I)I

    move-result v0

    sparse-switch v0, :sswitch_data_8

    new-instance v0, Lantlr/NoViableAltException;

    invoke-virtual {p0, v3}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v1

    invoke-virtual {p0}, Lantlr/ANTLRParser;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lantlr/NoViableAltException;-><init>(Lantlr/Token;Ljava/lang/String;)V

    throw v0

    :sswitch_12
    invoke-virtual {p0}, Lantlr/ANTLRParser;->exceptionGroup()V

    :sswitch_13
    iget-object v0, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v0, v0, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v0, :cond_6

    iget-object v0, p0, Lantlr/ANTLRParser;->behavior:Lantlr/ANTLRGrammarParseBehavior;

    invoke-virtual {v4}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lantlr/ANTLRGrammarParseBehavior;->endRule(Ljava/lang/String;)V

    :cond_6
    return-void

    :cond_7
    move v2, v3

    goto/16 :goto_1

    :sswitch_data_0
    .sparse-switch
        0x8 -> :sswitch_0
        0x18 -> :sswitch_1
        0x1e -> :sswitch_1
        0x1f -> :sswitch_1
        0x20 -> :sswitch_1
        0x29 -> :sswitch_1
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x18 -> :sswitch_3
        0x1e -> :sswitch_2
        0x1f -> :sswitch_4
        0x20 -> :sswitch_5
        0x29 -> :sswitch_3
    .end sparse-switch

    :sswitch_data_2
    .sparse-switch
        0x7 -> :sswitch_7
        0xe -> :sswitch_7
        0x21 -> :sswitch_6
        0x22 -> :sswitch_7
        0x23 -> :sswitch_7
        0x24 -> :sswitch_7
        0x25 -> :sswitch_7
    .end sparse-switch

    :sswitch_data_3
    .sparse-switch
        0x7 -> :sswitch_9
        0xe -> :sswitch_9
        0x22 -> :sswitch_8
        0x23 -> :sswitch_9
        0x24 -> :sswitch_9
        0x25 -> :sswitch_9
    .end sparse-switch

    :sswitch_data_4
    .sparse-switch
        0x7 -> :sswitch_b
        0xe -> :sswitch_b
        0x23 -> :sswitch_a
        0x24 -> :sswitch_b
        0x25 -> :sswitch_b
    .end sparse-switch

    :sswitch_data_5
    .sparse-switch
        0x7 -> :sswitch_d
        0xe -> :sswitch_d
        0x24 -> :sswitch_d
        0x25 -> :sswitch_c
    .end sparse-switch

    :sswitch_data_6
    .sparse-switch
        0x7 -> :sswitch_f
        0xe -> :sswitch_e
        0x24 -> :sswitch_f
    .end sparse-switch

    :sswitch_data_7
    .sparse-switch
        0x7 -> :sswitch_10
        0x24 -> :sswitch_11
    .end sparse-switch

    :sswitch_data_8
    .sparse-switch
        0x1 -> :sswitch_13
        0x7 -> :sswitch_13
        0x8 -> :sswitch_13
        0x9 -> :sswitch_13
        0xa -> :sswitch_13
        0x18 -> :sswitch_13
        0x1e -> :sswitch_13
        0x1f -> :sswitch_13
        0x20 -> :sswitch_13
        0x27 -> :sswitch_12
        0x29 -> :sswitch_13
    .end sparse-switch
.end method

.method public final ruleOptionsSpec()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/RecognitionException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/4 v3, 0x1

    const/16 v0, 0xe

    invoke-virtual {p0, v0}, Lantlr/ANTLRParser;->match(I)V

    :goto_0
    invoke-virtual {p0, v3}, Lantlr/ANTLRParser;->LA(I)I

    move-result v0

    const/16 v1, 0x18

    if-eq v0, v1, :cond_0

    invoke-virtual {p0, v3}, Lantlr/ANTLRParser;->LA(I)I

    move-result v0

    const/16 v1, 0x29

    if-ne v0, v1, :cond_2

    :cond_0
    invoke-virtual {p0}, Lantlr/ANTLRParser;->id()Lantlr/Token;

    move-result-object v0

    const/16 v1, 0xf

    invoke-virtual {p0, v1}, Lantlr/ANTLRParser;->match(I)V

    invoke-virtual {p0}, Lantlr/ANTLRParser;->optionValue()Lantlr/Token;

    move-result-object v1

    iget-object v2, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v2, v2, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v2, :cond_1

    iget-object v2, p0, Lantlr/ANTLRParser;->behavior:Lantlr/ANTLRGrammarParseBehavior;

    invoke-interface {v2, v0, v1}, Lantlr/ANTLRGrammarParseBehavior;->setRuleOption(Lantlr/Token;Lantlr/Token;)V

    :cond_1
    const/16 v0, 0x10

    invoke-virtual {p0, v0}, Lantlr/ANTLRParser;->match(I)V

    goto :goto_0

    :cond_2
    const/16 v0, 0x11

    invoke-virtual {p0, v0}, Lantlr/ANTLRParser;->match(I)V

    return-void
.end method

.method public final rules()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/RecognitionException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/4 v3, 0x1

    const/4 v0, 0x0

    :goto_0
    sget-object v1, Lantlr/ANTLRParser;->_tokenSet_0:Lantlr/collections/impl/BitSet;

    invoke-virtual {p0, v3}, Lantlr/ANTLRParser;->LA(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lantlr/collections/impl/BitSet;->member(I)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lantlr/ANTLRParser;->_tokenSet_1:Lantlr/collections/impl/BitSet;

    const/4 v2, 0x2

    invoke-virtual {p0, v2}, Lantlr/ANTLRParser;->LA(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lantlr/collections/impl/BitSet;->member(I)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lantlr/ANTLRParser;->rule()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    if-lt v0, v3, :cond_1

    return-void

    :cond_1
    new-instance v0, Lantlr/NoViableAltException;

    invoke-virtual {p0, v3}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v1

    invoke-virtual {p0}, Lantlr/ANTLRParser;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lantlr/NoViableAltException;-><init>(Lantlr/Token;Ljava/lang/String;)V

    throw v0
.end method

.method public final setBlockElement()Lantlr/collections/impl/BitSet;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/RecognitionException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/16 v5, 0x13

    const/4 v4, 0x1

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-virtual {p0, v4}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v2

    invoke-virtual {p0, v5}, Lantlr/ANTLRParser;->match(I)V

    iget-object v3, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v3, v3, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v3, :cond_0

    invoke-virtual {v2}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lantlr/ANTLRLexer;->tokenTypeForCharLiteral(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Lantlr/collections/impl/BitSet;->of(I)Lantlr/collections/impl/BitSet;

    move-result-object v1

    :cond_0
    invoke-virtual {p0, v4}, Lantlr/ANTLRParser;->LA(I)I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    new-instance v0, Lantlr/NoViableAltException;

    invoke-virtual {p0, v4}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v1

    invoke-virtual {p0}, Lantlr/ANTLRParser;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lantlr/NoViableAltException;-><init>(Lantlr/Token;Ljava/lang/String;)V

    throw v0

    :sswitch_0
    const/16 v3, 0x16

    invoke-virtual {p0, v3}, Lantlr/ANTLRParser;->match(I)V

    invoke-virtual {p0, v4}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v3

    invoke-virtual {p0, v5}, Lantlr/ANTLRParser;->match(I)V

    iget-object v4, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v4, v4, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v4, :cond_2

    invoke-virtual {v3}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lantlr/ANTLRLexer;->tokenTypeForCharLiteral(Ljava/lang/String;)I

    move-result v3

    if-ge v3, v0, :cond_1

    iget-object v4, p0, Lantlr/ANTLRParser;->antlrTool:Lantlr/Tool;

    const-string v5, "Malformed range line "

    invoke-virtual {p0}, Lantlr/ANTLRParser;->getFilename()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2}, Lantlr/Token;->getLine()I

    move-result v7

    invoke-virtual {v2}, Lantlr/Token;->getColumn()I

    move-result v2

    invoke-virtual {v4, v5, v6, v7, v2}, Lantlr/Tool;->error(Ljava/lang/String;Ljava/lang/String;II)V

    :cond_1
    add-int/lit8 v0, v0, 0x1

    :goto_0
    if-gt v0, v3, :cond_2

    invoke-virtual {v1, v0}, Lantlr/collections/impl/BitSet;->add(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    :sswitch_1
    return-object v1

    nop

    :sswitch_data_0
    .sparse-switch
        0x10 -> :sswitch_1
        0x15 -> :sswitch_1
        0x16 -> :sswitch_0
    .end sparse-switch
.end method

.method public final subruleOptionsSpec()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/RecognitionException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/4 v3, 0x1

    const/16 v0, 0xe

    invoke-virtual {p0, v0}, Lantlr/ANTLRParser;->match(I)V

    :goto_0
    invoke-virtual {p0, v3}, Lantlr/ANTLRParser;->LA(I)I

    move-result v0

    const/16 v1, 0x18

    if-eq v0, v1, :cond_0

    invoke-virtual {p0, v3}, Lantlr/ANTLRParser;->LA(I)I

    move-result v0

    const/16 v1, 0x29

    if-ne v0, v1, :cond_2

    :cond_0
    invoke-virtual {p0}, Lantlr/ANTLRParser;->id()Lantlr/Token;

    move-result-object v0

    const/16 v1, 0xf

    invoke-virtual {p0, v1}, Lantlr/ANTLRParser;->match(I)V

    invoke-virtual {p0}, Lantlr/ANTLRParser;->optionValue()Lantlr/Token;

    move-result-object v1

    iget-object v2, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v2, v2, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v2, :cond_1

    iget-object v2, p0, Lantlr/ANTLRParser;->behavior:Lantlr/ANTLRGrammarParseBehavior;

    invoke-interface {v2, v0, v1}, Lantlr/ANTLRGrammarParseBehavior;->setSubruleOption(Lantlr/Token;Lantlr/Token;)V

    :cond_1
    const/16 v0, 0x10

    invoke-virtual {p0, v0}, Lantlr/ANTLRParser;->match(I)V

    goto :goto_0

    :cond_2
    const/16 v0, 0x11

    invoke-virtual {p0, v0}, Lantlr/ANTLRParser;->match(I)V

    return-void
.end method

.method public final superClass()Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/RecognitionException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/4 v0, 0x0

    const/16 v1, 0x1b

    invoke-virtual {p0, v1}, Lantlr/ANTLRParser;->match(I)V

    iget-object v1, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v1, v1, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v1, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v0

    invoke-virtual {v0}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v0

    const-string v1, "\""

    const-string v2, "\""

    invoke-static {v0, v1, v2}, Lantlr/StringUtils;->stripFrontBack(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    const/4 v1, 0x6

    invoke-virtual {p0, v1}, Lantlr/ANTLRParser;->match(I)V

    const/16 v1, 0x1c

    invoke-virtual {p0, v1}, Lantlr/ANTLRParser;->match(I)V

    return-object v0
.end method

.method public final terminal(Lantlr/Token;)V
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/RecognitionException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/4 v5, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-virtual {p0, v4}, Lantlr/ANTLRParser;->LA(I)I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    new-instance v0, Lantlr/NoViableAltException;

    invoke-virtual {p0, v4}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v1

    invoke-virtual {p0}, Lantlr/ANTLRParser;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lantlr/NoViableAltException;-><init>(Lantlr/Token;Ljava/lang/String;)V

    throw v0

    :sswitch_0
    invoke-virtual {p0, v4}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v1

    const/16 v0, 0x13

    invoke-virtual {p0, v0}, Lantlr/ANTLRParser;->match(I)V

    invoke-virtual {p0, v4}, Lantlr/ANTLRParser;->LA(I)I

    move-result v0

    sparse-switch v0, :sswitch_data_1

    new-instance v0, Lantlr/NoViableAltException;

    invoke-virtual {p0, v4}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v1

    invoke-virtual {p0}, Lantlr/ANTLRParser;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lantlr/NoViableAltException;-><init>(Lantlr/Token;Ljava/lang/String;)V

    throw v0

    :sswitch_1
    const/16 v0, 0x21

    invoke-virtual {p0, v0}, Lantlr/ANTLRParser;->match(I)V

    iget-object v0, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v0, v0, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v0, :cond_0

    const/4 v4, 0x3

    :cond_0
    :sswitch_2
    iget-object v0, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v0, v0, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v0, :cond_1

    iget-object v0, p0, Lantlr/ANTLRParser;->behavior:Lantlr/ANTLRGrammarParseBehavior;

    invoke-direct {p0}, Lantlr/ANTLRParser;->lastInRule()Z

    move-result v5

    move-object v2, p1

    invoke-interface/range {v0 .. v5}, Lantlr/ANTLRGrammarParseBehavior;->refCharLiteral(Lantlr/Token;Lantlr/Token;ZIZ)V

    :cond_1
    :goto_0
    return-void

    :sswitch_3
    invoke-virtual {p0, v4}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v6

    const/16 v0, 0x18

    invoke-virtual {p0, v0}, Lantlr/ANTLRParser;->match(I)V

    invoke-virtual {p0}, Lantlr/ANTLRParser;->ast_type_spec()I

    move-result v10

    invoke-virtual {p0, v4}, Lantlr/ANTLRParser;->LA(I)I

    move-result v0

    sparse-switch v0, :sswitch_data_2

    new-instance v0, Lantlr/NoViableAltException;

    invoke-virtual {p0, v4}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v1

    invoke-virtual {p0}, Lantlr/ANTLRParser;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lantlr/NoViableAltException;-><init>(Lantlr/Token;Ljava/lang/String;)V

    throw v0

    :sswitch_4
    invoke-virtual {p0, v4}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v8

    const/16 v0, 0x22

    invoke-virtual {p0, v0}, Lantlr/ANTLRParser;->match(I)V

    iget-object v0, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v0, v0, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v0, :cond_2

    :goto_1
    iget-object v0, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v0, v0, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v0, :cond_1

    iget-object v4, p0, Lantlr/ANTLRParser;->behavior:Lantlr/ANTLRGrammarParseBehavior;

    invoke-direct {p0}, Lantlr/ANTLRParser;->lastInRule()Z

    move-result v11

    move-object v7, p1

    move v9, v3

    invoke-interface/range {v4 .. v11}, Lantlr/ANTLRGrammarParseBehavior;->refToken(Lantlr/Token;Lantlr/Token;Lantlr/Token;Lantlr/Token;ZIZ)V

    goto :goto_0

    :sswitch_5
    move-object v8, v5

    goto :goto_1

    :sswitch_6
    invoke-virtual {p0, v4}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v0

    const/4 v1, 0x6

    invoke-virtual {p0, v1}, Lantlr/ANTLRParser;->match(I)V

    invoke-virtual {p0}, Lantlr/ANTLRParser;->ast_type_spec()I

    move-result v1

    iget-object v2, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v2, v2, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v2, :cond_1

    iget-object v2, p0, Lantlr/ANTLRParser;->behavior:Lantlr/ANTLRGrammarParseBehavior;

    invoke-direct {p0}, Lantlr/ANTLRParser;->lastInRule()Z

    move-result v3

    invoke-interface {v2, v0, p1, v1, v3}, Lantlr/ANTLRGrammarParseBehavior;->refStringLiteral(Lantlr/Token;Lantlr/Token;IZ)V

    goto :goto_0

    :sswitch_7
    invoke-virtual {p0, v4}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v0

    const/16 v1, 0x32

    invoke-virtual {p0, v1}, Lantlr/ANTLRParser;->match(I)V

    invoke-virtual {p0}, Lantlr/ANTLRParser;->ast_type_spec()I

    move-result v1

    iget-object v2, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v2, v2, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v2, :cond_1

    iget-object v2, p0, Lantlr/ANTLRParser;->behavior:Lantlr/ANTLRGrammarParseBehavior;

    invoke-interface {v2, v0, p1, v1}, Lantlr/ANTLRGrammarParseBehavior;->refWildcard(Lantlr/Token;Lantlr/Token;I)V

    goto :goto_0

    :cond_2
    move-object v8, v5

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x6 -> :sswitch_6
        0x13 -> :sswitch_0
        0x18 -> :sswitch_3
        0x32 -> :sswitch_7
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x6 -> :sswitch_2
        0x7 -> :sswitch_2
        0x10 -> :sswitch_2
        0x13 -> :sswitch_2
        0x15 -> :sswitch_2
        0x18 -> :sswitch_2
        0x19 -> :sswitch_2
        0x1b -> :sswitch_2
        0x1c -> :sswitch_2
        0x21 -> :sswitch_1
        0x27 -> :sswitch_2
        0x29 -> :sswitch_2
        0x2a -> :sswitch_2
        0x2b -> :sswitch_2
        0x2c -> :sswitch_2
        0x32 -> :sswitch_2
    .end sparse-switch

    :sswitch_data_2
    .sparse-switch
        0x6 -> :sswitch_5
        0x7 -> :sswitch_5
        0x10 -> :sswitch_5
        0x13 -> :sswitch_5
        0x15 -> :sswitch_5
        0x18 -> :sswitch_5
        0x19 -> :sswitch_5
        0x1b -> :sswitch_5
        0x1c -> :sswitch_5
        0x22 -> :sswitch_4
        0x27 -> :sswitch_5
        0x29 -> :sswitch_5
        0x2a -> :sswitch_5
        0x2b -> :sswitch_5
        0x2c -> :sswitch_5
        0x32 -> :sswitch_5
    .end sparse-switch
.end method

.method public final throwsSpec()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/RecognitionException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/16 v3, 0x26

    const/4 v0, 0x0

    const/16 v1, 0x25

    invoke-virtual {p0, v1}, Lantlr/ANTLRParser;->match(I)V

    invoke-virtual {p0}, Lantlr/ANTLRParser;->id()Lantlr/Token;

    move-result-object v1

    iget-object v2, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v2, v2, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v2, :cond_0

    invoke-virtual {v1}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v0

    :cond_0
    :goto_0
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lantlr/ANTLRParser;->LA(I)I

    move-result v1

    if-ne v1, v3, :cond_1

    invoke-virtual {p0, v3}, Lantlr/ANTLRParser;->match(I)V

    invoke-virtual {p0}, Lantlr/ANTLRParser;->id()Lantlr/Token;

    move-result-object v1

    iget-object v2, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v2, v2, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v2, :cond_0

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v1}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v1, v1, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v1, :cond_2

    iget-object v1, p0, Lantlr/ANTLRParser;->behavior:Lantlr/ANTLRGrammarParseBehavior;

    invoke-interface {v1, v0}, Lantlr/ANTLRGrammarParseBehavior;->setUserExceptions(Ljava/lang/String;)V

    :cond_2
    return-void
.end method

.method public final tokensSpec()V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/RecognitionException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/16 v7, 0x18

    const/4 v2, 0x0

    const/4 v6, 0x6

    const/4 v5, 0x1

    const/16 v0, 0x17

    invoke-virtual {p0, v0}, Lantlr/ANTLRParser;->match(I)V

    const/4 v0, 0x0

    move v1, v0

    move-object v0, v2

    :goto_0
    invoke-virtual {p0, v5}, Lantlr/ANTLRParser;->LA(I)I

    move-result v3

    if-eq v3, v6, :cond_0

    invoke-virtual {p0, v5}, Lantlr/ANTLRParser;->LA(I)I

    move-result v3

    if-ne v3, v7, :cond_4

    :cond_0
    invoke-virtual {p0, v5}, Lantlr/ANTLRParser;->LA(I)I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    new-instance v0, Lantlr/NoViableAltException;

    invoke-virtual {p0, v5}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v1

    invoke-virtual {p0}, Lantlr/ANTLRParser;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lantlr/NoViableAltException;-><init>(Lantlr/Token;Ljava/lang/String;)V

    throw v0

    :sswitch_0
    iget-object v3, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v3, v3, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v3, :cond_1

    move-object v0, v2

    :cond_1
    invoke-virtual {p0, v5}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v3

    invoke-virtual {p0, v7}, Lantlr/ANTLRParser;->match(I)V

    invoke-virtual {p0, v5}, Lantlr/ANTLRParser;->LA(I)I

    move-result v4

    sparse-switch v4, :sswitch_data_1

    new-instance v0, Lantlr/NoViableAltException;

    invoke-virtual {p0, v5}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v1

    invoke-virtual {p0}, Lantlr/ANTLRParser;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lantlr/NoViableAltException;-><init>(Lantlr/Token;Ljava/lang/String;)V

    throw v0

    :sswitch_1
    const/16 v0, 0xf

    invoke-virtual {p0, v0}, Lantlr/ANTLRParser;->match(I)V

    invoke-virtual {p0, v5}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v0

    invoke-virtual {p0, v6}, Lantlr/ANTLRParser;->match(I)V

    :sswitch_2
    iget-object v4, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v4, v4, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v4, :cond_2

    iget-object v4, p0, Lantlr/ANTLRParser;->behavior:Lantlr/ANTLRGrammarParseBehavior;

    invoke-interface {v4, v3, v0}, Lantlr/ANTLRGrammarParseBehavior;->defineToken(Lantlr/Token;Lantlr/Token;)V

    :cond_2
    invoke-virtual {p0, v5}, Lantlr/ANTLRParser;->LA(I)I

    move-result v4

    sparse-switch v4, :sswitch_data_2

    new-instance v0, Lantlr/NoViableAltException;

    invoke-virtual {p0, v5}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v1

    invoke-virtual {p0}, Lantlr/ANTLRParser;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lantlr/NoViableAltException;-><init>(Lantlr/Token;Ljava/lang/String;)V

    throw v0

    :sswitch_3
    invoke-virtual {p0, v3}, Lantlr/ANTLRParser;->tokensSpecOptions(Lantlr/Token;)V

    :goto_1
    :sswitch_4
    const/16 v3, 0x10

    invoke-virtual {p0, v3}, Lantlr/ANTLRParser;->match(I)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :sswitch_5
    invoke-virtual {p0, v5}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v3

    invoke-virtual {p0, v6}, Lantlr/ANTLRParser;->match(I)V

    iget-object v4, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v4, v4, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v4, :cond_3

    iget-object v4, p0, Lantlr/ANTLRParser;->behavior:Lantlr/ANTLRGrammarParseBehavior;

    invoke-interface {v4, v2, v3}, Lantlr/ANTLRGrammarParseBehavior;->defineToken(Lantlr/Token;Lantlr/Token;)V

    :cond_3
    invoke-virtual {p0, v5}, Lantlr/ANTLRParser;->LA(I)I

    move-result v4

    sparse-switch v4, :sswitch_data_3

    new-instance v0, Lantlr/NoViableAltException;

    invoke-virtual {p0, v5}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v1

    invoke-virtual {p0}, Lantlr/ANTLRParser;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lantlr/NoViableAltException;-><init>(Lantlr/Token;Ljava/lang/String;)V

    throw v0

    :sswitch_6
    invoke-virtual {p0, v3}, Lantlr/ANTLRParser;->tokensSpecOptions(Lantlr/Token;)V

    goto :goto_1

    :cond_4
    if-lt v1, v5, :cond_5

    const/16 v0, 0x11

    invoke-virtual {p0, v0}, Lantlr/ANTLRParser;->match(I)V

    return-void

    :cond_5
    new-instance v0, Lantlr/NoViableAltException;

    invoke-virtual {p0, v5}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v1

    invoke-virtual {p0}, Lantlr/ANTLRParser;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lantlr/NoViableAltException;-><init>(Lantlr/Token;Ljava/lang/String;)V

    throw v0

    nop

    :sswitch_data_0
    .sparse-switch
        0x6 -> :sswitch_5
        0x18 -> :sswitch_0
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0xf -> :sswitch_1
        0x10 -> :sswitch_2
        0x19 -> :sswitch_2
    .end sparse-switch

    :sswitch_data_2
    .sparse-switch
        0x10 -> :sswitch_4
        0x19 -> :sswitch_3
    .end sparse-switch

    :sswitch_data_3
    .sparse-switch
        0x10 -> :sswitch_4
        0x19 -> :sswitch_6
    .end sparse-switch
.end method

.method public final tokensSpecOptions(Lantlr/Token;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/RecognitionException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/16 v4, 0x10

    const/16 v3, 0xf

    const/16 v0, 0x19

    invoke-virtual {p0, v0}, Lantlr/ANTLRParser;->match(I)V

    invoke-virtual {p0}, Lantlr/ANTLRParser;->id()Lantlr/Token;

    move-result-object v0

    invoke-virtual {p0, v3}, Lantlr/ANTLRParser;->match(I)V

    invoke-virtual {p0}, Lantlr/ANTLRParser;->optionValue()Lantlr/Token;

    move-result-object v1

    iget-object v2, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v2, v2, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v2, :cond_0

    iget-object v2, p0, Lantlr/ANTLRParser;->behavior:Lantlr/ANTLRGrammarParseBehavior;

    invoke-interface {v2, p1, v0, v1}, Lantlr/ANTLRGrammarParseBehavior;->refTokensSpecElementOption(Lantlr/Token;Lantlr/Token;Lantlr/Token;)V

    :cond_0
    :goto_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lantlr/ANTLRParser;->LA(I)I

    move-result v0

    if-ne v0, v4, :cond_1

    invoke-virtual {p0, v4}, Lantlr/ANTLRParser;->match(I)V

    invoke-virtual {p0}, Lantlr/ANTLRParser;->id()Lantlr/Token;

    move-result-object v0

    invoke-virtual {p0, v3}, Lantlr/ANTLRParser;->match(I)V

    invoke-virtual {p0}, Lantlr/ANTLRParser;->optionValue()Lantlr/Token;

    move-result-object v1

    iget-object v2, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v2, v2, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v2, :cond_0

    iget-object v2, p0, Lantlr/ANTLRParser;->behavior:Lantlr/ANTLRGrammarParseBehavior;

    invoke-interface {v2, p1, v0, v1}, Lantlr/ANTLRGrammarParseBehavior;->refTokensSpecElementOption(Lantlr/Token;Lantlr/Token;Lantlr/Token;)V

    goto :goto_0

    :cond_1
    const/16 v0, 0x1a

    invoke-virtual {p0, v0}, Lantlr/ANTLRParser;->match(I)V

    return-void
.end method

.method public final tree()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/RecognitionException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/4 v3, 0x1

    invoke-virtual {p0, v3}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v0

    const/16 v1, 0x2c

    invoke-virtual {p0, v1}, Lantlr/ANTLRParser;->match(I)V

    iget-object v1, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v1, v1, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v1, :cond_0

    iget-object v1, p0, Lantlr/ANTLRParser;->behavior:Lantlr/ANTLRGrammarParseBehavior;

    invoke-interface {v1, v0}, Lantlr/ANTLRGrammarParseBehavior;->beginTree(Lantlr/Token;)V

    :cond_0
    invoke-virtual {p0}, Lantlr/ANTLRParser;->rootNode()V

    iget-object v0, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v0, v0, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v0, :cond_1

    iget-object v0, p0, Lantlr/ANTLRParser;->behavior:Lantlr/ANTLRGrammarParseBehavior;

    invoke-interface {v0}, Lantlr/ANTLRGrammarParseBehavior;->beginChildList()V

    :cond_1
    const/4 v0, 0x0

    :goto_0
    sget-object v1, Lantlr/ANTLRParser;->_tokenSet_2:Lantlr/collections/impl/BitSet;

    invoke-virtual {p0, v3}, Lantlr/ANTLRParser;->LA(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lantlr/collections/impl/BitSet;->member(I)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lantlr/ANTLRParser;->element()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    if-lt v0, v3, :cond_5

    iget-object v0, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v0, v0, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v0, :cond_3

    iget-object v0, p0, Lantlr/ANTLRParser;->behavior:Lantlr/ANTLRGrammarParseBehavior;

    invoke-interface {v0}, Lantlr/ANTLRGrammarParseBehavior;->endChildList()V

    :cond_3
    const/16 v0, 0x1c

    invoke-virtual {p0, v0}, Lantlr/ANTLRParser;->match(I)V

    iget-object v0, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v0, v0, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v0, :cond_4

    iget-object v0, p0, Lantlr/ANTLRParser;->behavior:Lantlr/ANTLRGrammarParseBehavior;

    invoke-interface {v0}, Lantlr/ANTLRGrammarParseBehavior;->endTree()V

    :cond_4
    return-void

    :cond_5
    new-instance v0, Lantlr/NoViableAltException;

    invoke-virtual {p0, v3}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v1

    invoke-virtual {p0}, Lantlr/ANTLRParser;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lantlr/NoViableAltException;-><init>(Lantlr/Token;Ljava/lang/String;)V

    throw v0
.end method

.method public final treeParserOptionsSpec()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/RecognitionException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/4 v3, 0x1

    const/16 v0, 0xe

    invoke-virtual {p0, v0}, Lantlr/ANTLRParser;->match(I)V

    :goto_0
    invoke-virtual {p0, v3}, Lantlr/ANTLRParser;->LA(I)I

    move-result v0

    const/16 v1, 0x18

    if-eq v0, v1, :cond_0

    invoke-virtual {p0, v3}, Lantlr/ANTLRParser;->LA(I)I

    move-result v0

    const/16 v1, 0x29

    if-ne v0, v1, :cond_2

    :cond_0
    invoke-virtual {p0}, Lantlr/ANTLRParser;->id()Lantlr/Token;

    move-result-object v0

    const/16 v1, 0xf

    invoke-virtual {p0, v1}, Lantlr/ANTLRParser;->match(I)V

    invoke-virtual {p0}, Lantlr/ANTLRParser;->optionValue()Lantlr/Token;

    move-result-object v1

    iget-object v2, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v2, v2, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v2, :cond_1

    iget-object v2, p0, Lantlr/ANTLRParser;->behavior:Lantlr/ANTLRGrammarParseBehavior;

    invoke-interface {v2, v0, v1}, Lantlr/ANTLRGrammarParseBehavior;->setGrammarOption(Lantlr/Token;Lantlr/Token;)V

    :cond_1
    const/16 v0, 0x10

    invoke-virtual {p0, v0}, Lantlr/ANTLRParser;->match(I)V

    goto :goto_0

    :cond_2
    const/16 v0, 0x11

    invoke-virtual {p0, v0}, Lantlr/ANTLRParser;->match(I)V

    return-void
.end method

.method public final treeParserSpec(Ljava/lang/String;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/RecognitionException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/4 v4, 0x1

    const/4 v0, 0x0

    const/16 v1, 0xa

    invoke-virtual {p0, v1}, Lantlr/ANTLRParser;->match(I)V

    invoke-virtual {p0}, Lantlr/ANTLRParser;->id()Lantlr/Token;

    move-result-object v1

    const/16 v2, 0xb

    invoke-virtual {p0, v2}, Lantlr/ANTLRParser;->match(I)V

    const/16 v2, 0xd

    invoke-virtual {p0, v2}, Lantlr/ANTLRParser;->match(I)V

    invoke-virtual {p0, v4}, Lantlr/ANTLRParser;->LA(I)I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    new-instance v0, Lantlr/NoViableAltException;

    invoke-virtual {p0, v4}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v1

    invoke-virtual {p0}, Lantlr/ANTLRParser;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lantlr/NoViableAltException;-><init>(Lantlr/Token;Ljava/lang/String;)V

    throw v0

    :sswitch_0
    invoke-virtual {p0}, Lantlr/ANTLRParser;->superClass()Ljava/lang/String;

    move-result-object v0

    :sswitch_1
    iget-object v2, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v2, v2, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v2, :cond_0

    iget-object v2, p0, Lantlr/ANTLRParser;->behavior:Lantlr/ANTLRGrammarParseBehavior;

    invoke-virtual {p0}, Lantlr/ANTLRParser;->getFilename()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, v1, v0, p1}, Lantlr/ANTLRGrammarParseBehavior;->startTreeWalker(Ljava/lang/String;Lantlr/Token;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const/16 v0, 0x10

    invoke-virtual {p0, v0}, Lantlr/ANTLRParser;->match(I)V

    invoke-virtual {p0, v4}, Lantlr/ANTLRParser;->LA(I)I

    move-result v0

    sparse-switch v0, :sswitch_data_1

    new-instance v0, Lantlr/NoViableAltException;

    invoke-virtual {p0, v4}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v1

    invoke-virtual {p0}, Lantlr/ANTLRParser;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lantlr/NoViableAltException;-><init>(Lantlr/Token;Ljava/lang/String;)V

    throw v0

    :sswitch_2
    invoke-virtual {p0}, Lantlr/ANTLRParser;->treeParserOptionsSpec()V

    :sswitch_3
    iget-object v0, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v0, v0, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v0, :cond_1

    iget-object v0, p0, Lantlr/ANTLRParser;->behavior:Lantlr/ANTLRGrammarParseBehavior;

    invoke-interface {v0}, Lantlr/ANTLRGrammarParseBehavior;->endOptions()V

    :cond_1
    invoke-virtual {p0, v4}, Lantlr/ANTLRParser;->LA(I)I

    move-result v0

    sparse-switch v0, :sswitch_data_2

    new-instance v0, Lantlr/NoViableAltException;

    invoke-virtual {p0, v4}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v1

    invoke-virtual {p0}, Lantlr/ANTLRParser;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lantlr/NoViableAltException;-><init>(Lantlr/Token;Ljava/lang/String;)V

    throw v0

    :sswitch_4
    invoke-virtual {p0}, Lantlr/ANTLRParser;->tokensSpec()V

    :sswitch_5
    invoke-virtual {p0, v4}, Lantlr/ANTLRParser;->LA(I)I

    move-result v0

    sparse-switch v0, :sswitch_data_3

    new-instance v0, Lantlr/NoViableAltException;

    invoke-virtual {p0, v4}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v1

    invoke-virtual {p0}, Lantlr/ANTLRParser;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lantlr/NoViableAltException;-><init>(Lantlr/Token;Ljava/lang/String;)V

    throw v0

    :sswitch_6
    invoke-virtual {p0, v4}, Lantlr/ANTLRParser;->LT(I)Lantlr/Token;

    move-result-object v0

    const/4 v1, 0x7

    invoke-virtual {p0, v1}, Lantlr/ANTLRParser;->match(I)V

    iget-object v1, p0, Lantlr/ANTLRParser;->inputState:Lantlr/ParserSharedInputState;

    iget v1, v1, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v1, :cond_2

    iget-object v1, p0, Lantlr/ANTLRParser;->behavior:Lantlr/ANTLRGrammarParseBehavior;

    invoke-interface {v1, v0}, Lantlr/ANTLRGrammarParseBehavior;->refMemberAction(Lantlr/Token;)V

    :cond_2
    :sswitch_7
    return-void

    :sswitch_data_0
    .sparse-switch
        0x10 -> :sswitch_1
        0x1b -> :sswitch_0
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x7 -> :sswitch_3
        0x8 -> :sswitch_3
        0xe -> :sswitch_2
        0x17 -> :sswitch_3
        0x18 -> :sswitch_3
        0x1e -> :sswitch_3
        0x1f -> :sswitch_3
        0x20 -> :sswitch_3
        0x29 -> :sswitch_3
    .end sparse-switch

    :sswitch_data_2
    .sparse-switch
        0x7 -> :sswitch_5
        0x8 -> :sswitch_5
        0x17 -> :sswitch_4
        0x18 -> :sswitch_5
        0x1e -> :sswitch_5
        0x1f -> :sswitch_5
        0x20 -> :sswitch_5
        0x29 -> :sswitch_5
    .end sparse-switch

    :sswitch_data_3
    .sparse-switch
        0x7 -> :sswitch_6
        0x8 -> :sswitch_7
        0x18 -> :sswitch_7
        0x1e -> :sswitch_7
        0x1f -> :sswitch_7
        0x20 -> :sswitch_7
        0x29 -> :sswitch_7
    .end sparse-switch
.end method
