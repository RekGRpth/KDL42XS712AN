.class Lantlr/TokenRefElement;
.super Lantlr/GrammarAtom;
.source "TokenRefElement.java"


# direct methods
.method public constructor <init>(Lantlr/Grammar;Lantlr/Token;ZI)V
    .locals 5

    invoke-direct {p0, p1, p2, p4}, Lantlr/GrammarAtom;-><init>(Lantlr/Grammar;Lantlr/Token;I)V

    iput-boolean p3, p0, Lantlr/TokenRefElement;->not:Z

    iget-object v0, p0, Lantlr/TokenRefElement;->grammar:Lantlr/Grammar;

    iget-object v0, v0, Lantlr/Grammar;->tokenManager:Lantlr/TokenManager;

    iget-object v1, p0, Lantlr/TokenRefElement;->atomText:Ljava/lang/String;

    invoke-interface {v0, v1}, Lantlr/TokenManager;->getTokenSymbol(Ljava/lang/String;)Lantlr/TokenSymbol;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p1, Lantlr/Grammar;->antlrTool:Lantlr/Tool;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "Undefined token symbol: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p0, Lantlr/TokenRefElement;->atomText:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lantlr/TokenRefElement;->grammar:Lantlr/Grammar;

    invoke-virtual {v2}, Lantlr/Grammar;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Lantlr/Token;->getLine()I

    move-result v3

    invoke-virtual {p2}, Lantlr/Token;->getColumn()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lantlr/Tool;->error(Ljava/lang/String;Ljava/lang/String;II)V

    :goto_0
    invoke-virtual {p2}, Lantlr/Token;->getLine()I

    move-result v0

    iput v0, p0, Lantlr/TokenRefElement;->line:I

    return-void

    :cond_0
    invoke-virtual {v0}, Lantlr/TokenSymbol;->getTokenType()I

    move-result v1

    iput v1, p0, Lantlr/TokenRefElement;->tokenType:I

    invoke-virtual {v0}, Lantlr/TokenSymbol;->getASTNodeType()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lantlr/TokenRefElement;->setASTNodeType(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public generate()V
    .locals 1

    iget-object v0, p0, Lantlr/TokenRefElement;->grammar:Lantlr/Grammar;

    iget-object v0, v0, Lantlr/Grammar;->generator:Lantlr/CodeGenerator;

    invoke-virtual {v0, p0}, Lantlr/CodeGenerator;->gen(Lantlr/TokenRefElement;)V

    return-void
.end method

.method public look(I)Lantlr/Lookahead;
    .locals 1

    iget-object v0, p0, Lantlr/TokenRefElement;->grammar:Lantlr/Grammar;

    iget-object v0, v0, Lantlr/Grammar;->theLLkAnalyzer:Lantlr/LLkGrammarAnalyzer;

    invoke-interface {v0, p1, p0}, Lantlr/LLkGrammarAnalyzer;->look(ILantlr/GrammarAtom;)Lantlr/Lookahead;

    move-result-object v0

    return-object v0
.end method
