.class public Lantlr/collections/impl/Vector;
.super Ljava/lang/Object;
.source "Vector.java"

# interfaces
.implements Ljava/lang/Cloneable;


# instance fields
.field protected data:[Ljava/lang/Object;

.field protected lastElement:I


# direct methods
.method public constructor <init>()V
    .locals 1

    const/16 v0, 0xa

    invoke-direct {p0, v0}, Lantlr/collections/impl/Vector;-><init>(I)V

    return-void
.end method

.method public constructor <init>(I)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lantlr/collections/impl/Vector;->lastElement:I

    new-array v0, p1, [Ljava/lang/Object;

    iput-object v0, p0, Lantlr/collections/impl/Vector;->data:[Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public declared-synchronized appendElement(Ljava/lang/Object;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lantlr/collections/impl/Vector;->lastElement:I

    add-int/lit8 v0, v0, 0x2

    invoke-virtual {p0, v0}, Lantlr/collections/impl/Vector;->ensureCapacity(I)V

    iget-object v0, p0, Lantlr/collections/impl/Vector;->data:[Ljava/lang/Object;

    iget v1, p0, Lantlr/collections/impl/Vector;->lastElement:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lantlr/collections/impl/Vector;->lastElement:I

    aput-object p1, v0, v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public capacity()I
    .locals 1

    iget-object v0, p0, Lantlr/collections/impl/Vector;->data:[Ljava/lang/Object;

    array-length v0, v0

    return v0
.end method

.method public clone()Ljava/lang/Object;
    .locals 5

    const/4 v4, 0x0

    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lantlr/collections/impl/Vector;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    invoke-virtual {p0}, Lantlr/collections/impl/Vector;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/Object;

    iput-object v1, v0, Lantlr/collections/impl/Vector;->data:[Ljava/lang/Object;

    iget-object v1, p0, Lantlr/collections/impl/Vector;->data:[Ljava/lang/Object;

    iget-object v2, v0, Lantlr/collections/impl/Vector;->data:[Ljava/lang/Object;

    invoke-virtual {p0}, Lantlr/collections/impl/Vector;->size()I

    move-result v3

    invoke-static {v1, v4, v2, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    sget-object v0, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string v1, "cannot clone Vector.super"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public declared-synchronized elementAt(I)Ljava/lang/Object;
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lantlr/collections/impl/Vector;->data:[Ljava/lang/Object;

    array-length v0, v0

    if-lt p1, v0, :cond_0

    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, " >= "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p0, Lantlr/collections/impl/Vector;->data:[Ljava/lang/Object;

    array-length v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    if-gez p1, :cond_1

    :try_start_1
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, " < 0 "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v0, p0, Lantlr/collections/impl/Vector;->data:[Ljava/lang/Object;

    aget-object v0, v0, p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object v0
.end method

.method public declared-synchronized elements()Ljava/util/Enumeration;
    .locals 1

    monitor-enter p0

    :try_start_0
    new-instance v0, Lantlr/collections/impl/VectorEnumerator;

    invoke-direct {v0, p0}, Lantlr/collections/impl/VectorEnumerator;-><init>(Lantlr/collections/impl/Vector;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized ensureCapacity(I)V
    .locals 5

    monitor-enter p0

    add-int/lit8 v0, p1, 0x1

    :try_start_0
    iget-object v1, p0, Lantlr/collections/impl/Vector;->data:[Ljava/lang/Object;

    array-length v1, v1

    if-le v0, v1, :cond_1

    iget-object v1, p0, Lantlr/collections/impl/Vector;->data:[Ljava/lang/Object;

    iget-object v0, p0, Lantlr/collections/impl/Vector;->data:[Ljava/lang/Object;

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x2

    add-int/lit8 v2, p1, 0x1

    if-le v2, v0, :cond_0

    add-int/lit8 v0, p1, 0x1

    :cond_0
    new-array v0, v0, [Ljava/lang/Object;

    iput-object v0, p0, Lantlr/collections/impl/Vector;->data:[Ljava/lang/Object;

    const/4 v0, 0x0

    iget-object v2, p0, Lantlr/collections/impl/Vector;->data:[Ljava/lang/Object;

    const/4 v3, 0x0

    array-length v4, v1

    invoke-static {v1, v0, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized removeElement(Ljava/lang/Object;)Z
    .locals 5

    const/4 v0, 0x0

    monitor-enter p0

    move v1, v0

    :goto_0
    :try_start_0
    iget v2, p0, Lantlr/collections/impl/Vector;->lastElement:I

    if-gt v1, v2, :cond_0

    iget-object v2, p0, Lantlr/collections/impl/Vector;->data:[Ljava/lang/Object;

    aget-object v2, v2, v1

    if-eq v2, p1, :cond_0

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    iget v2, p0, Lantlr/collections/impl/Vector;->lastElement:I

    if-gt v1, v2, :cond_2

    iget-object v0, p0, Lantlr/collections/impl/Vector;->data:[Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v2, v0, v1

    iget v0, p0, Lantlr/collections/impl/Vector;->lastElement:I

    sub-int/2addr v0, v1

    if-lez v0, :cond_1

    iget-object v2, p0, Lantlr/collections/impl/Vector;->data:[Ljava/lang/Object;

    add-int/lit8 v3, v1, 0x1

    iget-object v4, p0, Lantlr/collections/impl/Vector;->data:[Ljava/lang/Object;

    invoke-static {v2, v3, v4, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    iget v0, p0, Lantlr/collections/impl/Vector;->lastElement:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lantlr/collections/impl/Vector;->lastElement:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x1

    :cond_2
    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setElementAt(Ljava/lang/Object;I)V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lantlr/collections/impl/Vector;->data:[Ljava/lang/Object;

    array-length v0, v0

    if-lt p2, v0, :cond_0

    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, " >= "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p0, Lantlr/collections/impl/Vector;->data:[Ljava/lang/Object;

    array-length v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lantlr/collections/impl/Vector;->data:[Ljava/lang/Object;

    aput-object p1, v0, p2

    iget v0, p0, Lantlr/collections/impl/Vector;->lastElement:I

    if-le p2, v0, :cond_1

    iput p2, p0, Lantlr/collections/impl/Vector;->lastElement:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    monitor-exit p0

    return-void
.end method

.method public size()I
    .locals 1

    iget v0, p0, Lantlr/collections/impl/Vector;->lastElement:I

    add-int/lit8 v0, v0, 0x1

    return v0
.end method
