.class Lantlr/collections/impl/VectorEnumerator;
.super Ljava/lang/Object;
.source "VectorEnumerator.java"

# interfaces
.implements Ljava/util/Enumeration;


# instance fields
.field i:I

.field vector:Lantlr/collections/impl/Vector;


# direct methods
.method constructor <init>(Lantlr/collections/impl/Vector;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lantlr/collections/impl/VectorEnumerator;->vector:Lantlr/collections/impl/Vector;

    const/4 v0, 0x0

    iput v0, p0, Lantlr/collections/impl/VectorEnumerator;->i:I

    return-void
.end method


# virtual methods
.method public hasMoreElements()Z
    .locals 3

    iget-object v1, p0, Lantlr/collections/impl/VectorEnumerator;->vector:Lantlr/collections/impl/Vector;

    monitor-enter v1

    :try_start_0
    iget v0, p0, Lantlr/collections/impl/VectorEnumerator;->i:I

    iget-object v2, p0, Lantlr/collections/impl/VectorEnumerator;->vector:Lantlr/collections/impl/Vector;

    iget v2, v2, Lantlr/collections/impl/Vector;->lastElement:I

    if-gt v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public nextElement()Ljava/lang/Object;
    .locals 4

    iget-object v1, p0, Lantlr/collections/impl/VectorEnumerator;->vector:Lantlr/collections/impl/Vector;

    monitor-enter v1

    :try_start_0
    iget v0, p0, Lantlr/collections/impl/VectorEnumerator;->i:I

    iget-object v2, p0, Lantlr/collections/impl/VectorEnumerator;->vector:Lantlr/collections/impl/Vector;

    iget v2, v2, Lantlr/collections/impl/Vector;->lastElement:I

    if-gt v0, v2, :cond_0

    iget-object v0, p0, Lantlr/collections/impl/VectorEnumerator;->vector:Lantlr/collections/impl/Vector;

    iget-object v0, v0, Lantlr/collections/impl/Vector;->data:[Ljava/lang/Object;

    iget v2, p0, Lantlr/collections/impl/VectorEnumerator;->i:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lantlr/collections/impl/VectorEnumerator;->i:I

    aget-object v0, v0, v2

    monitor-exit v1

    return-object v0

    :cond_0
    new-instance v0, Ljava/util/NoSuchElementException;

    const-string v2, "VectorEnumerator"

    invoke-direct {v0, v2}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    throw v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
