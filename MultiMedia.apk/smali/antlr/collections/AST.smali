.class public interface abstract Lantlr/collections/AST;
.super Ljava/lang/Object;
.source "AST.java"


# virtual methods
.method public abstract addChild(Lantlr/collections/AST;)V
.end method

.method public abstract equals(Lantlr/collections/AST;)Z
.end method

.method public abstract equalsList(Lantlr/collections/AST;)Z
.end method

.method public abstract equalsListPartial(Lantlr/collections/AST;)Z
.end method

.method public abstract equalsTree(Lantlr/collections/AST;)Z
.end method

.method public abstract equalsTreePartial(Lantlr/collections/AST;)Z
.end method

.method public abstract findAll(Lantlr/collections/AST;)Lantlr/collections/ASTEnumeration;
.end method

.method public abstract findAllPartial(Lantlr/collections/AST;)Lantlr/collections/ASTEnumeration;
.end method

.method public abstract getColumn()I
.end method

.method public abstract getFirstChild()Lantlr/collections/AST;
.end method

.method public abstract getLine()I
.end method

.method public abstract getNextSibling()Lantlr/collections/AST;
.end method

.method public abstract getNumberOfChildren()I
.end method

.method public abstract getText()Ljava/lang/String;
.end method

.method public abstract getType()I
.end method

.method public abstract initialize(ILjava/lang/String;)V
.end method

.method public abstract initialize(Lantlr/Token;)V
.end method

.method public abstract initialize(Lantlr/collections/AST;)V
.end method

.method public abstract setFirstChild(Lantlr/collections/AST;)V
.end method

.method public abstract setNextSibling(Lantlr/collections/AST;)V
.end method

.method public abstract setText(Ljava/lang/String;)V
.end method

.method public abstract setType(I)V
.end method

.method public abstract toString()Ljava/lang/String;
.end method

.method public abstract toStringList()Ljava/lang/String;
.end method

.method public abstract toStringTree()Ljava/lang/String;
.end method
