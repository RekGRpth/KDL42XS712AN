.class Lantlr/ExceptionSpec;
.super Ljava/lang/Object;
.source "ExceptionSpec.java"


# instance fields
.field protected handlers:Lantlr/collections/impl/Vector;

.field protected label:Lantlr/Token;


# direct methods
.method public constructor <init>(Lantlr/Token;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lantlr/ExceptionSpec;->label:Lantlr/Token;

    new-instance v0, Lantlr/collections/impl/Vector;

    invoke-direct {v0}, Lantlr/collections/impl/Vector;-><init>()V

    iput-object v0, p0, Lantlr/ExceptionSpec;->handlers:Lantlr/collections/impl/Vector;

    return-void
.end method


# virtual methods
.method public addHandler(Lantlr/ExceptionHandler;)V
    .locals 1

    iget-object v0, p0, Lantlr/ExceptionSpec;->handlers:Lantlr/collections/impl/Vector;

    invoke-virtual {v0, p1}, Lantlr/collections/impl/Vector;->appendElement(Ljava/lang/Object;)V

    return-void
.end method
