.class public abstract Lantlr/Grammar;
.super Ljava/lang/Object;
.source "Grammar.java"


# instance fields
.field protected analyzerDebug:Z

.field protected antlrTool:Lantlr/Tool;

.field protected buildAST:Z

.field protected classMemberAction:Lantlr/Token;

.field protected className:Ljava/lang/String;

.field protected comment:Ljava/lang/String;

.field protected debuggingOutput:Z

.field protected defaultErrorHandler:Z

.field protected exportVocab:Ljava/lang/String;

.field protected fileName:Ljava/lang/String;

.field protected generator:Lantlr/CodeGenerator;

.field protected hasSyntacticPredicate:Z

.field protected hasUserErrorHandling:Z

.field protected importVocab:Ljava/lang/String;

.field protected interactive:Z

.field protected maxk:I

.field protected options:Ljava/util/Hashtable;

.field protected preambleAction:Lantlr/Token;

.field protected rules:Lantlr/collections/impl/Vector;

.field protected superClass:Ljava/lang/String;

.field protected symbols:Ljava/util/Hashtable;

.field protected theLLkAnalyzer:Lantlr/LLkGrammarAnalyzer;

.field protected tokenManager:Lantlr/TokenManager;

.field protected traceRules:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Lantlr/Tool;Ljava/lang/String;)V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v2, p0, Lantlr/Grammar;->buildAST:Z

    iput-boolean v2, p0, Lantlr/Grammar;->analyzerDebug:Z

    iput-boolean v2, p0, Lantlr/Grammar;->interactive:Z

    iput-object v3, p0, Lantlr/Grammar;->superClass:Ljava/lang/String;

    iput-object v3, p0, Lantlr/Grammar;->exportVocab:Ljava/lang/String;

    iput-object v3, p0, Lantlr/Grammar;->importVocab:Ljava/lang/String;

    new-instance v0, Lantlr/CommonToken;

    const-string v1, ""

    invoke-direct {v0, v2, v1}, Lantlr/CommonToken;-><init>(ILjava/lang/String;)V

    iput-object v0, p0, Lantlr/Grammar;->preambleAction:Lantlr/Token;

    iput-object v3, p0, Lantlr/Grammar;->className:Ljava/lang/String;

    iput-object v3, p0, Lantlr/Grammar;->fileName:Ljava/lang/String;

    new-instance v0, Lantlr/CommonToken;

    const-string v1, ""

    invoke-direct {v0, v2, v1}, Lantlr/CommonToken;-><init>(ILjava/lang/String;)V

    iput-object v0, p0, Lantlr/Grammar;->classMemberAction:Lantlr/Token;

    iput-boolean v2, p0, Lantlr/Grammar;->hasSyntacticPredicate:Z

    iput-boolean v2, p0, Lantlr/Grammar;->hasUserErrorHandling:Z

    iput v4, p0, Lantlr/Grammar;->maxk:I

    iput-boolean v2, p0, Lantlr/Grammar;->traceRules:Z

    iput-boolean v2, p0, Lantlr/Grammar;->debuggingOutput:Z

    iput-boolean v4, p0, Lantlr/Grammar;->defaultErrorHandler:Z

    iput-object v3, p0, Lantlr/Grammar;->comment:Ljava/lang/String;

    iput-object p1, p0, Lantlr/Grammar;->className:Ljava/lang/String;

    iput-object p2, p0, Lantlr/Grammar;->antlrTool:Lantlr/Tool;

    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lantlr/Grammar;->symbols:Ljava/util/Hashtable;

    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lantlr/Grammar;->options:Ljava/util/Hashtable;

    new-instance v0, Lantlr/collections/impl/Vector;

    const/16 v1, 0x64

    invoke-direct {v0, v1}, Lantlr/collections/impl/Vector;-><init>(I)V

    iput-object v0, p0, Lantlr/Grammar;->rules:Lantlr/collections/impl/Vector;

    iput-object p3, p0, Lantlr/Grammar;->superClass:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public define(Lantlr/RuleSymbol;)V
    .locals 2

    iget-object v0, p0, Lantlr/Grammar;->rules:Lantlr/collections/impl/Vector;

    invoke-virtual {v0, p1}, Lantlr/collections/impl/Vector;->appendElement(Ljava/lang/Object;)V

    iget-object v0, p0, Lantlr/Grammar;->symbols:Ljava/util/Hashtable;

    invoke-virtual {p1}, Lantlr/RuleSymbol;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public abstract generate()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method protected getClassName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lantlr/Grammar;->className:Ljava/lang/String;

    return-object v0
.end method

.method public getDefaultErrorHandler()Z
    .locals 1

    iget-boolean v0, p0, Lantlr/Grammar;->defaultErrorHandler:Z

    return v0
.end method

.method public getFilename()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lantlr/Grammar;->fileName:Ljava/lang/String;

    return-object v0
.end method

.method public getIntegerOption(Ljava/lang/String;)I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NumberFormatException;
        }
    .end annotation

    iget-object v0, p0, Lantlr/Grammar;->options:Ljava/util/Hashtable;

    invoke-virtual {v0, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lantlr/Token;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lantlr/Token;->getType()I

    move-result v1

    const/16 v2, 0x14

    if-eq v1, v2, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/NumberFormatException;

    invoke-direct {v0}, Ljava/lang/NumberFormatException;-><init>()V

    throw v0

    :cond_1
    invoke-virtual {v0}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public getOption(Ljava/lang/String;)Lantlr/Token;
    .locals 1

    iget-object v0, p0, Lantlr/Grammar;->options:Ljava/util/Hashtable;

    invoke-virtual {v0, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lantlr/Token;

    return-object v0
.end method

.method protected abstract getSuperClass()Ljava/lang/String;
.end method

.method public getSymbol(Ljava/lang/String;)Lantlr/GrammarSymbol;
    .locals 1

    iget-object v0, p0, Lantlr/Grammar;->symbols:Ljava/util/Hashtable;

    invoke-virtual {v0, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lantlr/GrammarSymbol;

    return-object v0
.end method

.method public getSymbols()Ljava/util/Enumeration;
    .locals 1

    iget-object v0, p0, Lantlr/Grammar;->symbols:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->elements()Ljava/util/Enumeration;

    move-result-object v0

    return-object v0
.end method

.method public hasOption(Ljava/lang/String;)Z
    .locals 1

    iget-object v0, p0, Lantlr/Grammar;->options:Ljava/util/Hashtable;

    invoke-virtual {v0, p1}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isDefined(Ljava/lang/String;)Z
    .locals 1

    iget-object v0, p0, Lantlr/Grammar;->symbols:Ljava/util/Hashtable;

    invoke-virtual {v0, p1}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public abstract processArguments([Ljava/lang/String;)V
.end method

.method public setCodeGenerator(Lantlr/CodeGenerator;)V
    .locals 0

    iput-object p1, p0, Lantlr/Grammar;->generator:Lantlr/CodeGenerator;

    return-void
.end method

.method public setFilename(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lantlr/Grammar;->fileName:Ljava/lang/String;

    return-void
.end method

.method public setGrammarAnalyzer(Lantlr/LLkGrammarAnalyzer;)V
    .locals 0

    iput-object p1, p0, Lantlr/Grammar;->theLLkAnalyzer:Lantlr/LLkGrammarAnalyzer;

    return-void
.end method

.method public setOption(Ljava/lang/String;Lantlr/Token;)Z
    .locals 6

    const/4 v1, 0x0

    const/4 v0, 0x1

    iget-object v2, p0, Lantlr/Grammar;->options:Ljava/util/Hashtable;

    invoke-virtual {v2, p1, p2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p2}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v2

    const-string v3, "k"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    :try_start_0
    const-string v1, "k"

    invoke-virtual {p0, v1}, Lantlr/Grammar;->getIntegerOption(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lantlr/Grammar;->maxk:I

    iget v1, p0, Lantlr/Grammar;->maxk:I

    if-gtz v1, :cond_0

    iget-object v1, p0, Lantlr/Grammar;->antlrTool:Lantlr/Tool;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "option \'k\' must be greater than 0 (was "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {p2}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lantlr/Grammar;->getFilename()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, Lantlr/Token;->getLine()I

    move-result v4

    invoke-virtual {p2}, Lantlr/Token;->getColumn()I

    move-result v5

    invoke-virtual {v1, v2, v3, v4, v5}, Lantlr/Tool;->error(Ljava/lang/String;Ljava/lang/String;II)V

    const/4 v1, 0x1

    iput v1, p0, Lantlr/Grammar;->maxk:I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return v0

    :catch_0
    move-exception v1

    iget-object v1, p0, Lantlr/Grammar;->antlrTool:Lantlr/Tool;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "option \'k\' must be an integer (was "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {p2}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lantlr/Grammar;->getFilename()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, Lantlr/Token;->getLine()I

    move-result v4

    invoke-virtual {p2}, Lantlr/Token;->getColumn()I

    move-result v5

    invoke-virtual {v1, v2, v3, v4, v5}, Lantlr/Tool;->error(Ljava/lang/String;Ljava/lang/String;II)V

    goto :goto_0

    :cond_1
    const-string v3, "codeGenMakeSwitchThreshold"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    :try_start_1
    const-string v1, "codeGenMakeSwitchThreshold"

    invoke-virtual {p0, v1}, Lantlr/Grammar;->getIntegerOption(Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v1

    iget-object v1, p0, Lantlr/Grammar;->antlrTool:Lantlr/Tool;

    const-string v2, "option \'codeGenMakeSwitchThreshold\' must be an integer"

    invoke-virtual {p0}, Lantlr/Grammar;->getFilename()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, Lantlr/Token;->getLine()I

    move-result v4

    invoke-virtual {p2}, Lantlr/Token;->getColumn()I

    move-result v5

    invoke-virtual {v1, v2, v3, v4, v5}, Lantlr/Tool;->error(Ljava/lang/String;Ljava/lang/String;II)V

    goto :goto_0

    :cond_2
    const-string v3, "codeGenBitsetTestThreshold"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    :try_start_2
    const-string v1, "codeGenBitsetTestThreshold"

    invoke-virtual {p0, v1}, Lantlr/Grammar;->getIntegerOption(Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_0

    :catch_2
    move-exception v1

    iget-object v1, p0, Lantlr/Grammar;->antlrTool:Lantlr/Tool;

    const-string v2, "option \'codeGenBitsetTestThreshold\' must be an integer"

    invoke-virtual {p0}, Lantlr/Grammar;->getFilename()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, Lantlr/Token;->getLine()I

    move-result v4

    invoke-virtual {p2}, Lantlr/Token;->getColumn()I

    move-result v5

    invoke-virtual {v1, v2, v3, v4, v5}, Lantlr/Tool;->error(Ljava/lang/String;Ljava/lang/String;II)V

    goto :goto_0

    :cond_3
    const-string v3, "defaultErrorHandler"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    const-string v3, "true"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    iput-boolean v0, p0, Lantlr/Grammar;->defaultErrorHandler:Z

    goto/16 :goto_0

    :cond_4
    const-string v3, "false"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    iput-boolean v1, p0, Lantlr/Grammar;->defaultErrorHandler:Z

    goto/16 :goto_0

    :cond_5
    iget-object v1, p0, Lantlr/Grammar;->antlrTool:Lantlr/Tool;

    const-string v2, "Value for defaultErrorHandler must be true or false"

    invoke-virtual {p0}, Lantlr/Grammar;->getFilename()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, Lantlr/Token;->getLine()I

    move-result v4

    invoke-virtual {p2}, Lantlr/Token;->getColumn()I

    move-result v5

    invoke-virtual {v1, v2, v3, v4, v5}, Lantlr/Tool;->error(Ljava/lang/String;Ljava/lang/String;II)V

    goto/16 :goto_0

    :cond_6
    const-string v3, "analyzerDebug"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    const-string v3, "true"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    iput-boolean v0, p0, Lantlr/Grammar;->analyzerDebug:Z

    goto/16 :goto_0

    :cond_7
    const-string v3, "false"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    iput-boolean v1, p0, Lantlr/Grammar;->analyzerDebug:Z

    goto/16 :goto_0

    :cond_8
    iget-object v1, p0, Lantlr/Grammar;->antlrTool:Lantlr/Tool;

    const-string v2, "option \'analyzerDebug\' must be true or false"

    invoke-virtual {p0}, Lantlr/Grammar;->getFilename()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, Lantlr/Token;->getLine()I

    move-result v4

    invoke-virtual {p2}, Lantlr/Token;->getColumn()I

    move-result v5

    invoke-virtual {v1, v2, v3, v4, v5}, Lantlr/Tool;->error(Ljava/lang/String;Ljava/lang/String;II)V

    goto/16 :goto_0

    :cond_9
    const-string v3, "codeGenDebug"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_c

    const-string v3, "true"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    iput-boolean v0, p0, Lantlr/Grammar;->analyzerDebug:Z

    goto/16 :goto_0

    :cond_a
    const-string v3, "false"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    iput-boolean v1, p0, Lantlr/Grammar;->analyzerDebug:Z

    goto/16 :goto_0

    :cond_b
    iget-object v1, p0, Lantlr/Grammar;->antlrTool:Lantlr/Tool;

    const-string v2, "option \'codeGenDebug\' must be true or false"

    invoke-virtual {p0}, Lantlr/Grammar;->getFilename()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, Lantlr/Token;->getLine()I

    move-result v4

    invoke-virtual {p2}, Lantlr/Token;->getColumn()I

    move-result v5

    invoke-virtual {v1, v2, v3, v4, v5}, Lantlr/Tool;->error(Ljava/lang/String;Ljava/lang/String;II)V

    goto/16 :goto_0

    :cond_c
    const-string v2, "classHeaderSuffix"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "classHeaderPrefix"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "namespaceAntlr"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "namespaceStd"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "genHashLines"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "noConstructors"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto/16 :goto_0
.end method

.method public setTokenManager(Lantlr/TokenManager;)V
    .locals 0

    iput-object p1, p0, Lantlr/Grammar;->tokenManager:Lantlr/TokenManager;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    new-instance v1, Ljava/lang/StringBuffer;

    const/16 v0, 0x4e20

    invoke-direct {v1, v0}, Ljava/lang/StringBuffer;-><init>(I)V

    iget-object v0, p0, Lantlr/Grammar;->rules:Lantlr/collections/impl/Vector;

    invoke-virtual {v0}, Lantlr/collections/impl/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lantlr/RuleSymbol;

    iget-object v3, v0, Lantlr/RuleSymbol;->id:Ljava/lang/String;

    const-string v4, "mnextToken"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v0}, Lantlr/RuleSymbol;->getBlock()Lantlr/RuleBlock;

    move-result-object v0

    invoke-virtual {v0}, Lantlr/RuleBlock;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v0, "\n\n"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
