.class Lantlr/TokenSymbol;
.super Lantlr/GrammarSymbol;
.source "TokenSymbol.java"


# instance fields
.field protected ASTNodeType:Ljava/lang/String;

.field protected paraphrase:Ljava/lang/String;

.field protected ttype:I


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0, p1}, Lantlr/GrammarSymbol;-><init>(Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lantlr/TokenSymbol;->paraphrase:Ljava/lang/String;

    const/4 v0, 0x0

    iput v0, p0, Lantlr/TokenSymbol;->ttype:I

    return-void
.end method


# virtual methods
.method public getASTNodeType()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lantlr/TokenSymbol;->ASTNodeType:Ljava/lang/String;

    return-object v0
.end method

.method public getParaphrase()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lantlr/TokenSymbol;->paraphrase:Ljava/lang/String;

    return-object v0
.end method

.method public getTokenType()I
    .locals 1

    iget v0, p0, Lantlr/TokenSymbol;->ttype:I

    return v0
.end method

.method public setASTNodeType(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lantlr/TokenSymbol;->ASTNodeType:Ljava/lang/String;

    return-void
.end method

.method public setParaphrase(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lantlr/TokenSymbol;->paraphrase:Ljava/lang/String;

    return-void
.end method

.method public setTokenType(I)V
    .locals 0

    iput p1, p0, Lantlr/TokenSymbol;->ttype:I

    return-void
.end method
