.class public interface abstract Lantlr/ANTLRTokdefParserTokenTypes;
.super Ljava/lang/Object;
.source "ANTLRTokdefParserTokenTypes.java"


# static fields
.field public static final ASSIGN:I = 0x6

.field public static final DIGIT:I = 0xe

.field public static final EOF:I = 0x1

.field public static final ESC:I = 0xd

.field public static final ID:I = 0x4

.field public static final INT:I = 0x9

.field public static final LPAREN:I = 0x7

.field public static final ML_COMMENT:I = 0xc

.field public static final NULL_TREE_LOOKAHEAD:I = 0x3

.field public static final RPAREN:I = 0x8

.field public static final SL_COMMENT:I = 0xb

.field public static final STRING:I = 0x5

.field public static final WS:I = 0xa

.field public static final XDIGIT:I = 0xf
