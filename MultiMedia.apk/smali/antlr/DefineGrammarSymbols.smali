.class public Lantlr/DefineGrammarSymbols;
.super Ljava/lang/Object;
.source "DefineGrammarSymbols.java"

# interfaces
.implements Lantlr/ANTLRGrammarParseBehavior;


# static fields
.field static final DEFAULT_TOKENMANAGER_NAME:Ljava/lang/String; = "*default"


# instance fields
.field analyzer:Lantlr/LLkAnalyzer;

.field args:[Ljava/lang/String;

.field protected grammar:Lantlr/Grammar;

.field protected grammars:Ljava/util/Hashtable;

.field protected headerActions:Ljava/util/Hashtable;

.field language:Ljava/lang/String;

.field protected numLexers:I

.field protected numParsers:I

.field protected numTreeParsers:I

.field thePreambleAction:Lantlr/Token;

.field protected tokenManagers:Ljava/util/Hashtable;

.field protected tool:Lantlr/Tool;


# direct methods
.method public constructor <init>(Lantlr/Tool;[Ljava/lang/String;Lantlr/LLkAnalyzer;)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lantlr/DefineGrammarSymbols;->grammars:Ljava/util/Hashtable;

    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lantlr/DefineGrammarSymbols;->tokenManagers:Ljava/util/Hashtable;

    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lantlr/DefineGrammarSymbols;->headerActions:Ljava/util/Hashtable;

    new-instance v0, Lantlr/CommonToken;

    const-string v1, ""

    invoke-direct {v0, v2, v1}, Lantlr/CommonToken;-><init>(ILjava/lang/String;)V

    iput-object v0, p0, Lantlr/DefineGrammarSymbols;->thePreambleAction:Lantlr/Token;

    const-string v0, "Java"

    iput-object v0, p0, Lantlr/DefineGrammarSymbols;->language:Ljava/lang/String;

    iput v2, p0, Lantlr/DefineGrammarSymbols;->numLexers:I

    iput v2, p0, Lantlr/DefineGrammarSymbols;->numParsers:I

    iput v2, p0, Lantlr/DefineGrammarSymbols;->numTreeParsers:I

    iput-object p1, p0, Lantlr/DefineGrammarSymbols;->tool:Lantlr/Tool;

    iput-object p2, p0, Lantlr/DefineGrammarSymbols;->args:[Ljava/lang/String;

    iput-object p3, p0, Lantlr/DefineGrammarSymbols;->analyzer:Lantlr/LLkAnalyzer;

    return-void
.end method


# virtual methods
.method public _refStringLiteral(Lantlr/Token;Lantlr/Token;IZ)V
    .locals 2

    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    instance-of v0, v0, Lantlr/LexerGrammar;

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    iget-object v1, v1, Lantlr/Grammar;->tokenManager:Lantlr/TokenManager;

    invoke-interface {v1, v0}, Lantlr/TokenManager;->getTokenSymbol(Ljava/lang/String;)Lantlr/TokenSymbol;

    move-result-object v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v1, Lantlr/StringLiteralSymbol;

    invoke-direct {v1, v0}, Lantlr/StringLiteralSymbol;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    iget-object v0, v0, Lantlr/Grammar;->tokenManager:Lantlr/TokenManager;

    invoke-interface {v0}, Lantlr/TokenManager;->nextTokenType()I

    move-result v0

    invoke-virtual {v1, v0}, Lantlr/StringLiteralSymbol;->setTokenType(I)V

    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    iget-object v0, v0, Lantlr/Grammar;->tokenManager:Lantlr/TokenManager;

    invoke-interface {v0, v1}, Lantlr/TokenManager;->define(Lantlr/TokenSymbol;)V

    goto :goto_0
.end method

.method public _refToken(Lantlr/Token;Lantlr/Token;Lantlr/Token;Lantlr/Token;ZIZ)V
    .locals 3

    invoke-virtual {p2}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    iget-object v1, v1, Lantlr/Grammar;->tokenManager:Lantlr/TokenManager;

    invoke-interface {v1, v0}, Lantlr/TokenManager;->tokenDefined(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    iget-object v1, v1, Lantlr/Grammar;->tokenManager:Lantlr/TokenManager;

    invoke-interface {v1}, Lantlr/TokenManager;->nextTokenType()I

    move-result v1

    new-instance v2, Lantlr/TokenSymbol;

    invoke-direct {v2, v0}, Lantlr/TokenSymbol;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Lantlr/TokenSymbol;->setTokenType(I)V

    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    iget-object v0, v0, Lantlr/Grammar;->tokenManager:Lantlr/TokenManager;

    invoke-interface {v0, v2}, Lantlr/TokenManager;->define(Lantlr/TokenSymbol;)V

    :cond_0
    return-void
.end method

.method public abortGrammar()V
    .locals 2

    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    invoke-virtual {v0}, Lantlr/Grammar;->getClassName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->grammars:Ljava/util/Hashtable;

    iget-object v1, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    invoke-virtual {v1}, Lantlr/Grammar;->getClassName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    return-void
.end method

.method public beginAlt(Z)V
    .locals 0

    return-void
.end method

.method public beginChildList()V
    .locals 0

    return-void
.end method

.method public beginExceptionGroup()V
    .locals 0

    return-void
.end method

.method public beginExceptionSpec(Lantlr/Token;)V
    .locals 0

    return-void
.end method

.method public beginSubRule(Lantlr/Token;Lantlr/Token;Z)V
    .locals 0

    return-void
.end method

.method public beginTree(Lantlr/Token;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/SemanticException;
        }
    .end annotation

    return-void
.end method

.method public defineRuleName(Lantlr/Token;Ljava/lang/String;ZLjava/lang/String;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/SemanticException;
        }
    .end annotation

    invoke-virtual {p1}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v0

    iget v1, p1, Lantlr/Token;->type:I

    const/16 v2, 0x18

    if-ne v1, v2, :cond_0

    invoke-static {v0}, Lantlr/CodeGenerator;->encodeLexerRuleName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    iget-object v1, v1, Lantlr/Grammar;->tokenManager:Lantlr/TokenManager;

    invoke-virtual {p1}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lantlr/TokenManager;->tokenDefined(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    iget-object v1, v1, Lantlr/Grammar;->tokenManager:Lantlr/TokenManager;

    invoke-interface {v1}, Lantlr/TokenManager;->nextTokenType()I

    move-result v1

    new-instance v2, Lantlr/TokenSymbol;

    invoke-virtual {p1}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lantlr/TokenSymbol;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Lantlr/TokenSymbol;->setTokenType(I)V

    iget-object v1, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    iget-object v1, v1, Lantlr/Grammar;->tokenManager:Lantlr/TokenManager;

    invoke-interface {v1, v2}, Lantlr/TokenManager;->define(Lantlr/TokenSymbol;)V

    :cond_0
    move-object v1, v0

    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    invoke-virtual {v0, v1}, Lantlr/Grammar;->isDefined(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    invoke-virtual {v0, v1}, Lantlr/Grammar;->getSymbol(Ljava/lang/String;)Lantlr/GrammarSymbol;

    move-result-object v0

    check-cast v0, Lantlr/RuleSymbol;

    invoke-virtual {v0}, Lantlr/RuleSymbol;->isDefined()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lantlr/DefineGrammarSymbols;->tool:Lantlr/Tool;

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string v4, "redefinition of rule "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    invoke-virtual {v3}, Lantlr/Grammar;->getFilename()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lantlr/Token;->getLine()I

    move-result v4

    invoke-virtual {p1}, Lantlr/Token;->getColumn()I

    move-result v5

    invoke-virtual {v2, v1, v3, v4, v5}, Lantlr/Tool;->error(Ljava/lang/String;Ljava/lang/String;II)V

    :cond_1
    :goto_0
    invoke-virtual {v0}, Lantlr/RuleSymbol;->setDefined()V

    iput-object p2, v0, Lantlr/RuleSymbol;->access:Ljava/lang/String;

    iput-object p4, v0, Lantlr/RuleSymbol;->comment:Ljava/lang/String;

    return-void

    :cond_2
    new-instance v0, Lantlr/RuleSymbol;

    invoke-direct {v0, v1}, Lantlr/RuleSymbol;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    invoke-virtual {v1, v0}, Lantlr/Grammar;->define(Lantlr/RuleSymbol;)V

    goto :goto_0
.end method

.method public defineToken(Lantlr/Token;Lantlr/Token;)V
    .locals 5

    const/4 v0, 0x0

    if-eqz p1, :cond_9

    invoke-virtual {p1}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    :goto_0
    if-eqz p2, :cond_8

    invoke-virtual {p2}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    :goto_1
    if-eqz v1, :cond_6

    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    iget-object v0, v0, Lantlr/Grammar;->tokenManager:Lantlr/TokenManager;

    invoke-interface {v0, v1}, Lantlr/TokenManager;->getTokenSymbol(Ljava/lang/String;)Lantlr/TokenSymbol;

    move-result-object v0

    check-cast v0, Lantlr/StringLiteralSymbol;

    if-eqz v0, :cond_3

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lantlr/StringLiteralSymbol;->getLabel()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_2

    :cond_0
    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->tool:Lantlr/Tool;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "Redefinition of literal in tokens {...}: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    invoke-virtual {v2}, Lantlr/Grammar;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Lantlr/Token;->getLine()I

    move-result v3

    invoke-virtual {p2}, Lantlr/Token;->getColumn()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lantlr/Tool;->warning(Ljava/lang/String;Ljava/lang/String;II)V

    :cond_1
    :goto_2
    return-void

    :cond_2
    if-eqz v2, :cond_3

    invoke-virtual {v0, v2}, Lantlr/StringLiteralSymbol;->setLabel(Ljava/lang/String;)V

    iget-object v3, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    iget-object v3, v3, Lantlr/Grammar;->tokenManager:Lantlr/TokenManager;

    invoke-interface {v3, v2, v0}, Lantlr/TokenManager;->mapToTokenSymbol(Ljava/lang/String;Lantlr/TokenSymbol;)V

    :cond_3
    if-eqz v2, :cond_5

    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    iget-object v0, v0, Lantlr/Grammar;->tokenManager:Lantlr/TokenManager;

    invoke-interface {v0, v2}, Lantlr/TokenManager;->getTokenSymbol(Ljava/lang/String;)Lantlr/TokenSymbol;

    move-result-object v0

    if-eqz v0, :cond_5

    instance-of v3, v0, Lantlr/StringLiteralSymbol;

    if-eqz v3, :cond_4

    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->tool:Lantlr/Tool;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "Redefinition of token in tokens {...}: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    invoke-virtual {v2}, Lantlr/Grammar;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Lantlr/Token;->getLine()I

    move-result v3

    invoke-virtual {p2}, Lantlr/Token;->getColumn()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lantlr/Tool;->warning(Ljava/lang/String;Ljava/lang/String;II)V

    goto :goto_2

    :cond_4
    invoke-virtual {v0}, Lantlr/TokenSymbol;->getTokenType()I

    move-result v0

    new-instance v3, Lantlr/StringLiteralSymbol;

    invoke-direct {v3, v1}, Lantlr/StringLiteralSymbol;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Lantlr/StringLiteralSymbol;->setTokenType(I)V

    invoke-virtual {v3, v2}, Lantlr/StringLiteralSymbol;->setLabel(Ljava/lang/String;)V

    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    iget-object v0, v0, Lantlr/Grammar;->tokenManager:Lantlr/TokenManager;

    invoke-interface {v0, v3}, Lantlr/TokenManager;->define(Lantlr/TokenSymbol;)V

    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    iget-object v0, v0, Lantlr/Grammar;->tokenManager:Lantlr/TokenManager;

    invoke-interface {v0, v2, v3}, Lantlr/TokenManager;->mapToTokenSymbol(Ljava/lang/String;Lantlr/TokenSymbol;)V

    goto :goto_2

    :cond_5
    new-instance v0, Lantlr/StringLiteralSymbol;

    invoke-direct {v0, v1}, Lantlr/StringLiteralSymbol;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    iget-object v1, v1, Lantlr/Grammar;->tokenManager:Lantlr/TokenManager;

    invoke-interface {v1}, Lantlr/TokenManager;->nextTokenType()I

    move-result v1

    invoke-virtual {v0, v1}, Lantlr/StringLiteralSymbol;->setTokenType(I)V

    invoke-virtual {v0, v2}, Lantlr/StringLiteralSymbol;->setLabel(Ljava/lang/String;)V

    iget-object v1, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    iget-object v1, v1, Lantlr/Grammar;->tokenManager:Lantlr/TokenManager;

    invoke-interface {v1, v0}, Lantlr/TokenManager;->define(Lantlr/TokenSymbol;)V

    if-eqz v2, :cond_1

    iget-object v1, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    iget-object v1, v1, Lantlr/Grammar;->tokenManager:Lantlr/TokenManager;

    invoke-interface {v1, v2, v0}, Lantlr/TokenManager;->mapToTokenSymbol(Ljava/lang/String;Lantlr/TokenSymbol;)V

    goto/16 :goto_2

    :cond_6
    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    iget-object v0, v0, Lantlr/Grammar;->tokenManager:Lantlr/TokenManager;

    invoke-interface {v0, v2}, Lantlr/TokenManager;->tokenDefined(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->tool:Lantlr/Tool;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "Redefinition of token in tokens {...}: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    invoke-virtual {v2}, Lantlr/Grammar;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lantlr/Token;->getLine()I

    move-result v3

    invoke-virtual {p1}, Lantlr/Token;->getColumn()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lantlr/Tool;->warning(Ljava/lang/String;Ljava/lang/String;II)V

    goto/16 :goto_2

    :cond_7
    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    iget-object v0, v0, Lantlr/Grammar;->tokenManager:Lantlr/TokenManager;

    invoke-interface {v0}, Lantlr/TokenManager;->nextTokenType()I

    move-result v0

    new-instance v1, Lantlr/TokenSymbol;

    invoke-direct {v1, v2}, Lantlr/TokenSymbol;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Lantlr/TokenSymbol;->setTokenType(I)V

    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    iget-object v0, v0, Lantlr/Grammar;->tokenManager:Lantlr/TokenManager;

    invoke-interface {v0, v1}, Lantlr/TokenManager;->define(Lantlr/TokenSymbol;)V

    goto/16 :goto_2

    :cond_8
    move-object v1, v0

    goto/16 :goto_1

    :cond_9
    move-object v2, v0

    goto/16 :goto_0
.end method

.method public endAlt()V
    .locals 0

    return-void
.end method

.method public endChildList()V
    .locals 0

    return-void
.end method

.method public endExceptionGroup()V
    .locals 0

    return-void
.end method

.method public endExceptionSpec()V
    .locals 0

    return-void
.end method

.method public endGrammar()V
    .locals 0

    return-void
.end method

.method public endOptions()V
    .locals 6

    const/4 v5, 0x0

    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    iget-object v0, v0, Lantlr/Grammar;->exportVocab:Ljava/lang/String;

    if-nez v0, :cond_2

    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    iget-object v0, v0, Lantlr/Grammar;->importVocab:Ljava/lang/String;

    if-nez v0, :cond_2

    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    iget-object v1, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    invoke-virtual {v1}, Lantlr/Grammar;->getClassName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lantlr/Grammar;->exportVocab:Ljava/lang/String;

    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->tokenManagers:Ljava/util/Hashtable;

    const-string v1, "*default"

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    const-string v1, "*default"

    iput-object v1, v0, Lantlr/Grammar;->exportVocab:Ljava/lang/String;

    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->tokenManagers:Ljava/util/Hashtable;

    const-string v1, "*default"

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lantlr/TokenManager;

    iget-object v1, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    invoke-virtual {v1, v0}, Lantlr/Grammar;->setTokenManager(Lantlr/TokenManager;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v0, Lantlr/SimpleTokenManager;

    iget-object v1, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    iget-object v1, v1, Lantlr/Grammar;->exportVocab:Ljava/lang/String;

    iget-object v2, p0, Lantlr/DefineGrammarSymbols;->tool:Lantlr/Tool;

    invoke-direct {v0, v1, v2}, Lantlr/SimpleTokenManager;-><init>(Ljava/lang/String;Lantlr/Tool;)V

    iget-object v1, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    invoke-virtual {v1, v0}, Lantlr/Grammar;->setTokenManager(Lantlr/TokenManager;)V

    iget-object v1, p0, Lantlr/DefineGrammarSymbols;->tokenManagers:Ljava/util/Hashtable;

    iget-object v2, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    iget-object v2, v2, Lantlr/Grammar;->exportVocab:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lantlr/DefineGrammarSymbols;->tokenManagers:Ljava/util/Hashtable;

    const-string v2, "*default"

    invoke-virtual {v1, v2, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    iget-object v0, v0, Lantlr/Grammar;->exportVocab:Ljava/lang/String;

    if-nez v0, :cond_5

    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    iget-object v0, v0, Lantlr/Grammar;->importVocab:Ljava/lang/String;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    iget-object v1, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    invoke-virtual {v1}, Lantlr/Grammar;->getClassName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lantlr/Grammar;->exportVocab:Ljava/lang/String;

    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    iget-object v0, v0, Lantlr/Grammar;->importVocab:Ljava/lang/String;

    iget-object v1, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    iget-object v1, v1, Lantlr/Grammar;->exportVocab:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->tool:Lantlr/Tool;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "Grammar "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    invoke-virtual {v2}, Lantlr/Grammar;->getClassName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, " cannot have importVocab same as default output vocab (grammar name); ignored."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lantlr/Tool;->warning(Ljava/lang/String;)V

    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    const/4 v1, 0x0

    iput-object v1, v0, Lantlr/Grammar;->importVocab:Ljava/lang/String;

    invoke-virtual {p0}, Lantlr/DefineGrammarSymbols;->endOptions()V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->tokenManagers:Ljava/util/Hashtable;

    iget-object v1, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    iget-object v1, v1, Lantlr/Grammar;->importVocab:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->tokenManagers:Ljava/util/Hashtable;

    iget-object v1, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    iget-object v1, v1, Lantlr/Grammar;->importVocab:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lantlr/TokenManager;

    invoke-interface {v0}, Lantlr/TokenManager;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lantlr/TokenManager;

    iget-object v1, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    iget-object v1, v1, Lantlr/Grammar;->exportVocab:Ljava/lang/String;

    invoke-interface {v0, v1}, Lantlr/TokenManager;->setName(Ljava/lang/String;)V

    invoke-interface {v0, v5}, Lantlr/TokenManager;->setReadOnly(Z)V

    iget-object v1, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    invoke-virtual {v1, v0}, Lantlr/Grammar;->setTokenManager(Lantlr/TokenManager;)V

    iget-object v1, p0, Lantlr/DefineGrammarSymbols;->tokenManagers:Ljava/util/Hashtable;

    iget-object v2, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    iget-object v2, v2, Lantlr/Grammar;->exportVocab:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    :cond_4
    new-instance v0, Lantlr/ImportVocabTokenManager;

    iget-object v1, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    iget-object v3, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    iget-object v3, v3, Lantlr/Grammar;->importVocab:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    sget-object v3, Lantlr/CodeGenerator;->TokenTypesFileSuffix:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    sget-object v3, Lantlr/CodeGenerator;->TokenTypesFileExt:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    iget-object v3, v3, Lantlr/Grammar;->exportVocab:Ljava/lang/String;

    iget-object v4, p0, Lantlr/DefineGrammarSymbols;->tool:Lantlr/Tool;

    invoke-direct {v0, v1, v2, v3, v4}, Lantlr/ImportVocabTokenManager;-><init>(Lantlr/Grammar;Ljava/lang/String;Ljava/lang/String;Lantlr/Tool;)V

    invoke-virtual {v0, v5}, Lantlr/ImportVocabTokenManager;->setReadOnly(Z)V

    iget-object v1, p0, Lantlr/DefineGrammarSymbols;->tokenManagers:Ljava/util/Hashtable;

    iget-object v2, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    iget-object v2, v2, Lantlr/Grammar;->exportVocab:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    invoke-virtual {v1, v0}, Lantlr/Grammar;->setTokenManager(Lantlr/TokenManager;)V

    iget-object v1, p0, Lantlr/DefineGrammarSymbols;->tokenManagers:Ljava/util/Hashtable;

    const-string v2, "*default"

    invoke-virtual {v1, v2}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lantlr/DefineGrammarSymbols;->tokenManagers:Ljava/util/Hashtable;

    const-string v2, "*default"

    invoke-virtual {v1, v2, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    :cond_5
    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    iget-object v0, v0, Lantlr/Grammar;->exportVocab:Ljava/lang/String;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    iget-object v0, v0, Lantlr/Grammar;->importVocab:Ljava/lang/String;

    if-nez v0, :cond_7

    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->tokenManagers:Ljava/util/Hashtable;

    iget-object v1, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    iget-object v1, v1, Lantlr/Grammar;->exportVocab:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->tokenManagers:Ljava/util/Hashtable;

    iget-object v1, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    iget-object v1, v1, Lantlr/Grammar;->exportVocab:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lantlr/TokenManager;

    iget-object v1, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    invoke-virtual {v1, v0}, Lantlr/Grammar;->setTokenManager(Lantlr/TokenManager;)V

    goto/16 :goto_0

    :cond_6
    new-instance v0, Lantlr/SimpleTokenManager;

    iget-object v1, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    iget-object v1, v1, Lantlr/Grammar;->exportVocab:Ljava/lang/String;

    iget-object v2, p0, Lantlr/DefineGrammarSymbols;->tool:Lantlr/Tool;

    invoke-direct {v0, v1, v2}, Lantlr/SimpleTokenManager;-><init>(Ljava/lang/String;Lantlr/Tool;)V

    iget-object v1, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    invoke-virtual {v1, v0}, Lantlr/Grammar;->setTokenManager(Lantlr/TokenManager;)V

    iget-object v1, p0, Lantlr/DefineGrammarSymbols;->tokenManagers:Ljava/util/Hashtable;

    iget-object v2, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    iget-object v2, v2, Lantlr/Grammar;->exportVocab:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lantlr/DefineGrammarSymbols;->tokenManagers:Ljava/util/Hashtable;

    const-string v2, "*default"

    invoke-virtual {v1, v2}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lantlr/DefineGrammarSymbols;->tokenManagers:Ljava/util/Hashtable;

    const-string v2, "*default"

    invoke-virtual {v1, v2, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    :cond_7
    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    iget-object v0, v0, Lantlr/Grammar;->exportVocab:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    iget-object v0, v0, Lantlr/Grammar;->importVocab:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    iget-object v0, v0, Lantlr/Grammar;->importVocab:Ljava/lang/String;

    iget-object v1, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    iget-object v1, v1, Lantlr/Grammar;->exportVocab:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->tool:Lantlr/Tool;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "exportVocab of "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    iget-object v2, v2, Lantlr/Grammar;->exportVocab:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, " same as importVocab; probably not what you want"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lantlr/Tool;->error(Ljava/lang/String;)V

    :cond_8
    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->tokenManagers:Ljava/util/Hashtable;

    iget-object v1, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    iget-object v1, v1, Lantlr/Grammar;->importVocab:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->tokenManagers:Ljava/util/Hashtable;

    iget-object v1, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    iget-object v1, v1, Lantlr/Grammar;->importVocab:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lantlr/TokenManager;

    invoke-interface {v0}, Lantlr/TokenManager;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lantlr/TokenManager;

    iget-object v1, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    iget-object v1, v1, Lantlr/Grammar;->exportVocab:Ljava/lang/String;

    invoke-interface {v0, v1}, Lantlr/TokenManager;->setName(Ljava/lang/String;)V

    invoke-interface {v0, v5}, Lantlr/TokenManager;->setReadOnly(Z)V

    iget-object v1, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    invoke-virtual {v1, v0}, Lantlr/Grammar;->setTokenManager(Lantlr/TokenManager;)V

    iget-object v1, p0, Lantlr/DefineGrammarSymbols;->tokenManagers:Ljava/util/Hashtable;

    iget-object v2, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    iget-object v2, v2, Lantlr/Grammar;->exportVocab:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    :cond_9
    new-instance v0, Lantlr/ImportVocabTokenManager;

    iget-object v1, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    iget-object v3, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    iget-object v3, v3, Lantlr/Grammar;->importVocab:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    sget-object v3, Lantlr/CodeGenerator;->TokenTypesFileSuffix:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    sget-object v3, Lantlr/CodeGenerator;->TokenTypesFileExt:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    iget-object v3, v3, Lantlr/Grammar;->exportVocab:Ljava/lang/String;

    iget-object v4, p0, Lantlr/DefineGrammarSymbols;->tool:Lantlr/Tool;

    invoke-direct {v0, v1, v2, v3, v4}, Lantlr/ImportVocabTokenManager;-><init>(Lantlr/Grammar;Ljava/lang/String;Ljava/lang/String;Lantlr/Tool;)V

    invoke-virtual {v0, v5}, Lantlr/ImportVocabTokenManager;->setReadOnly(Z)V

    iget-object v1, p0, Lantlr/DefineGrammarSymbols;->tokenManagers:Ljava/util/Hashtable;

    iget-object v2, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    iget-object v2, v2, Lantlr/Grammar;->exportVocab:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    invoke-virtual {v1, v0}, Lantlr/Grammar;->setTokenManager(Lantlr/TokenManager;)V

    iget-object v1, p0, Lantlr/DefineGrammarSymbols;->tokenManagers:Ljava/util/Hashtable;

    const-string v2, "*default"

    invoke-virtual {v1, v2}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lantlr/DefineGrammarSymbols;->tokenManagers:Ljava/util/Hashtable;

    const-string v2, "*default"

    invoke-virtual {v1, v2, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0
.end method

.method public endRule(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public endSubRule()V
    .locals 0

    return-void
.end method

.method public endTree()V
    .locals 0

    return-void
.end method

.method public getHeaderAction(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->headerActions:Ljava/util/Hashtable;

    invoke-virtual {v0, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lantlr/Token;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public hasError()V
    .locals 0

    return-void
.end method

.method public noASTSubRule()V
    .locals 0

    return-void
.end method

.method public oneOrMoreSubRule()V
    .locals 0

    return-void
.end method

.method public optionalSubRule()V
    .locals 0

    return-void
.end method

.method public refAction(Lantlr/Token;)V
    .locals 0

    return-void
.end method

.method public refArgAction(Lantlr/Token;)V
    .locals 0

    return-void
.end method

.method public refCharLiteral(Lantlr/Token;Lantlr/Token;ZIZ)V
    .locals 0

    return-void
.end method

.method public refCharRange(Lantlr/Token;Lantlr/Token;Lantlr/Token;IZ)V
    .locals 0

    return-void
.end method

.method public refElementOption(Lantlr/Token;Lantlr/Token;)V
    .locals 0

    return-void
.end method

.method public refExceptionHandler(Lantlr/Token;Lantlr/Token;)V
    .locals 0

    return-void
.end method

.method public refHeaderAction(Lantlr/Token;Lantlr/Token;)V
    .locals 4

    if-nez p1, :cond_1

    const-string v0, ""

    :goto_0
    iget-object v1, p0, Lantlr/DefineGrammarSymbols;->headerActions:Ljava/util/Hashtable;

    invoke-virtual {v1, v0}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lantlr/DefineGrammarSymbols;->tool:Lantlr/Tool;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {p2}, Lantlr/Token;->getLine()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, ": header action already defined"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lantlr/Tool;->error(Ljava/lang/String;)V

    :cond_0
    :goto_1
    iget-object v1, p0, Lantlr/DefineGrammarSymbols;->headerActions:Ljava/util/Hashtable;

    invoke-virtual {v1, v0, p2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void

    :cond_1
    invoke-virtual {p1}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v0

    const-string v1, "\""

    const-string v2, "\""

    invoke-static {v0, v1, v2}, Lantlr/StringUtils;->stripFrontBack(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lantlr/DefineGrammarSymbols;->tool:Lantlr/Tool;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {p2}, Lantlr/Token;->getLine()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, ": header action \'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, "\' already defined"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lantlr/Tool;->error(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public refInitAction(Lantlr/Token;)V
    .locals 0

    return-void
.end method

.method public refMemberAction(Lantlr/Token;)V
    .locals 0

    return-void
.end method

.method public refPreambleAction(Lantlr/Token;)V
    .locals 0

    iput-object p1, p0, Lantlr/DefineGrammarSymbols;->thePreambleAction:Lantlr/Token;

    return-void
.end method

.method public refReturnAction(Lantlr/Token;)V
    .locals 0

    return-void
.end method

.method public refRule(Lantlr/Token;Lantlr/Token;Lantlr/Token;Lantlr/Token;I)V
    .locals 3

    invoke-virtual {p2}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v0

    iget v1, p2, Lantlr/Token;->type:I

    const/16 v2, 0x18

    if-ne v1, v2, :cond_0

    invoke-static {v0}, Lantlr/CodeGenerator;->encodeLexerRuleName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    iget-object v1, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    invoke-virtual {v1, v0}, Lantlr/Grammar;->isDefined(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    new-instance v2, Lantlr/RuleSymbol;

    invoke-direct {v2, v0}, Lantlr/RuleSymbol;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lantlr/Grammar;->define(Lantlr/RuleSymbol;)V

    :cond_1
    return-void
.end method

.method public refSemPred(Lantlr/Token;)V
    .locals 0

    return-void
.end method

.method public refStringLiteral(Lantlr/Token;Lantlr/Token;IZ)V
    .locals 0

    invoke-virtual {p0, p1, p2, p3, p4}, Lantlr/DefineGrammarSymbols;->_refStringLiteral(Lantlr/Token;Lantlr/Token;IZ)V

    return-void
.end method

.method public refToken(Lantlr/Token;Lantlr/Token;Lantlr/Token;Lantlr/Token;ZIZ)V
    .locals 0

    invoke-virtual/range {p0 .. p7}, Lantlr/DefineGrammarSymbols;->_refToken(Lantlr/Token;Lantlr/Token;Lantlr/Token;Lantlr/Token;ZIZ)V

    return-void
.end method

.method public refTokenRange(Lantlr/Token;Lantlr/Token;Lantlr/Token;IZ)V
    .locals 9

    const/16 v8, 0x22

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v1, 0x0

    invoke-virtual {p1}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/String;->charAt(I)C

    move-result v0

    if-ne v0, v8, :cond_0

    invoke-virtual {p0, p1, v1, v6, p5}, Lantlr/DefineGrammarSymbols;->refStringLiteral(Lantlr/Token;Lantlr/Token;IZ)V

    :goto_0
    invoke-virtual {p2}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/String;->charAt(I)C

    move-result v0

    if-ne v0, v8, :cond_1

    invoke-virtual {p0, p2, v1, v6, p5}, Lantlr/DefineGrammarSymbols;->_refStringLiteral(Lantlr/Token;Lantlr/Token;IZ)V

    :goto_1
    return-void

    :cond_0
    move-object v0, p0

    move-object v2, p1

    move-object v3, v1

    move-object v4, v1

    move v7, p5

    invoke-virtual/range {v0 .. v7}, Lantlr/DefineGrammarSymbols;->_refToken(Lantlr/Token;Lantlr/Token;Lantlr/Token;Lantlr/Token;ZIZ)V

    goto :goto_0

    :cond_1
    move-object v0, p0

    move-object v2, p2

    move-object v3, v1

    move-object v4, v1

    move v7, p5

    invoke-virtual/range {v0 .. v7}, Lantlr/DefineGrammarSymbols;->_refToken(Lantlr/Token;Lantlr/Token;Lantlr/Token;Lantlr/Token;ZIZ)V

    goto :goto_1
.end method

.method public refTokensSpecElementOption(Lantlr/Token;Lantlr/Token;Lantlr/Token;)V
    .locals 0

    return-void
.end method

.method public refTreeSpecifier(Lantlr/Token;)V
    .locals 0

    return-void
.end method

.method public refWildcard(Lantlr/Token;Lantlr/Token;I)V
    .locals 0

    return-void
.end method

.method public reset()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    return-void
.end method

.method public setArgOfRuleRef(Lantlr/Token;)V
    .locals 0

    return-void
.end method

.method public setCharVocabulary(Lantlr/collections/impl/BitSet;)V
    .locals 1

    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    check-cast v0, Lantlr/LexerGrammar;

    invoke-virtual {v0, p1}, Lantlr/LexerGrammar;->setCharVocabulary(Lantlr/collections/impl/BitSet;)V

    return-void
.end method

.method public setFileOption(Lantlr/Token;Lantlr/Token;Ljava/lang/String;)V
    .locals 4

    const/16 v3, 0x22

    const/4 v2, 0x6

    invoke-virtual {p1}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v0

    const-string v1, "language"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p2}, Lantlr/Token;->getType()I

    move-result v0

    if-ne v0, v2, :cond_1

    invoke-virtual {p2}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v3}, Lantlr/StringUtils;->stripFront(Ljava/lang/String;C)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v3}, Lantlr/StringUtils;->stripBack(Ljava/lang/String;C)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lantlr/DefineGrammarSymbols;->language:Ljava/lang/String;

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p2}, Lantlr/Token;->getType()I

    move-result v0

    const/16 v1, 0x18

    if-eq v0, v1, :cond_2

    invoke-virtual {p2}, Lantlr/Token;->getType()I

    move-result v0

    const/16 v1, 0x29

    if-ne v0, v1, :cond_3

    :cond_2
    invoke-virtual {p2}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lantlr/DefineGrammarSymbols;->language:Ljava/lang/String;

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->tool:Lantlr/Tool;

    const-string v1, "language option must be string or identifier"

    invoke-virtual {p2}, Lantlr/Token;->getLine()I

    move-result v2

    invoke-virtual {p2}, Lantlr/Token;->getColumn()I

    move-result v3

    invoke-virtual {v0, v1, p3, v2, v3}, Lantlr/Tool;->error(Ljava/lang/String;Ljava/lang/String;II)V

    goto :goto_0

    :cond_4
    invoke-virtual {p1}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v0

    const-string v1, "mangleLiteralPrefix"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {p2}, Lantlr/Token;->getType()I

    move-result v0

    if-ne v0, v2, :cond_5

    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->tool:Lantlr/Tool;

    invoke-virtual {p2}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v0

    const-string v1, "\""

    const-string v2, "\""

    invoke-static {v0, v1, v2}, Lantlr/StringUtils;->stripFrontBack(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lantlr/Tool;->literalsPrefix:Ljava/lang/String;

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->tool:Lantlr/Tool;

    const-string v1, "mangleLiteralPrefix option must be string"

    invoke-virtual {p2}, Lantlr/Token;->getLine()I

    move-result v2

    invoke-virtual {p2}, Lantlr/Token;->getColumn()I

    move-result v3

    invoke-virtual {v0, v1, p3, v2, v3}, Lantlr/Tool;->error(Ljava/lang/String;Ljava/lang/String;II)V

    goto :goto_0

    :cond_6
    invoke-virtual {p1}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v0

    const-string v1, "upperCaseMangledLiterals"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-virtual {p2}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v0

    const-string v1, "true"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->tool:Lantlr/Tool;

    const/4 v0, 0x1

    sput-boolean v0, Lantlr/Tool;->upperCaseMangledLiterals:Z

    goto :goto_0

    :cond_7
    invoke-virtual {p2}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v0

    const-string v1, "false"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->tool:Lantlr/Tool;

    const/4 v0, 0x0

    sput-boolean v0, Lantlr/Tool;->upperCaseMangledLiterals:Z

    goto/16 :goto_0

    :cond_8
    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    iget-object v0, v0, Lantlr/Grammar;->antlrTool:Lantlr/Tool;

    const-string v1, "Value for upperCaseMangledLiterals must be true or false"

    invoke-virtual {p1}, Lantlr/Token;->getLine()I

    move-result v2

    invoke-virtual {p1}, Lantlr/Token;->getColumn()I

    move-result v3

    invoke-virtual {v0, v1, p3, v2, v3}, Lantlr/Tool;->error(Ljava/lang/String;Ljava/lang/String;II)V

    goto/16 :goto_0

    :cond_9
    invoke-virtual {p1}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v0

    const-string v1, "namespaceStd"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_a

    invoke-virtual {p1}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v0

    const-string v1, "namespaceAntlr"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_a

    invoke-virtual {p1}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v0

    const-string v1, "genHashLines"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    :cond_a
    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->language:Ljava/lang/String;

    const-string v1, "Cpp"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_b

    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->tool:Lantlr/Tool;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {p1}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, " option only valid for C++"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lantlr/Token;->getLine()I

    move-result v2

    invoke-virtual {p1}, Lantlr/Token;->getColumn()I

    move-result v3

    invoke-virtual {v0, v1, p3, v2, v3}, Lantlr/Tool;->error(Ljava/lang/String;Ljava/lang/String;II)V

    goto/16 :goto_0

    :cond_b
    invoke-virtual {p1}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v0

    const-string v1, "noConstructors"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-virtual {p2}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v0

    const-string v1, "true"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_c

    invoke-virtual {p2}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v0

    const-string v1, "false"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_c

    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->tool:Lantlr/Tool;

    const-string v1, "noConstructors option must be true or false"

    invoke-virtual {p2}, Lantlr/Token;->getLine()I

    move-result v2

    invoke-virtual {p2}, Lantlr/Token;->getColumn()I

    move-result v3

    invoke-virtual {v0, v1, p3, v2, v3}, Lantlr/Tool;->error(Ljava/lang/String;Ljava/lang/String;II)V

    :cond_c
    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->tool:Lantlr/Tool;

    invoke-virtual {p2}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v1

    const-string v2, "true"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    iput-boolean v1, v0, Lantlr/Tool;->noConstructors:Z

    goto/16 :goto_0

    :cond_d
    invoke-virtual {p1}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v0

    const-string v1, "genHashLines"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    invoke-virtual {p2}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v0

    const-string v1, "true"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_e

    invoke-virtual {p2}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v0

    const-string v1, "false"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_e

    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->tool:Lantlr/Tool;

    const-string v1, "genHashLines option must be true or false"

    invoke-virtual {p2}, Lantlr/Token;->getLine()I

    move-result v2

    invoke-virtual {p2}, Lantlr/Token;->getColumn()I

    move-result v3

    invoke-virtual {v0, v1, p3, v2, v3}, Lantlr/Tool;->error(Ljava/lang/String;Ljava/lang/String;II)V

    :cond_e
    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->tool:Lantlr/Tool;

    invoke-virtual {p2}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v1

    const-string v2, "true"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    iput-boolean v1, v0, Lantlr/Tool;->genHashLines:Z

    goto/16 :goto_0

    :cond_f
    invoke-virtual {p2}, Lantlr/Token;->getType()I

    move-result v0

    if-eq v0, v2, :cond_10

    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->tool:Lantlr/Tool;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {p1}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, " option must be a string"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lantlr/Token;->getLine()I

    move-result v2

    invoke-virtual {p2}, Lantlr/Token;->getColumn()I

    move-result v3

    invoke-virtual {v0, v1, p3, v2, v3}, Lantlr/Tool;->error(Ljava/lang/String;Ljava/lang/String;II)V

    goto/16 :goto_0

    :cond_10
    invoke-virtual {p1}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v0

    const-string v1, "namespaceStd"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->tool:Lantlr/Tool;

    invoke-virtual {p2}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lantlr/Tool;->namespaceStd:Ljava/lang/String;

    goto/16 :goto_0

    :cond_11
    invoke-virtual {p1}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v0

    const-string v1, "namespaceAntlr"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->tool:Lantlr/Tool;

    invoke-virtual {p2}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lantlr/Tool;->namespaceAntlr:Ljava/lang/String;

    goto/16 :goto_0

    :cond_12
    invoke-virtual {p1}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v0

    const-string v1, "namespace"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_15

    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->language:Ljava/lang/String;

    const-string v1, "Cpp"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_13

    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->language:Ljava/lang/String;

    const-string v1, "CSharp"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_13

    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->tool:Lantlr/Tool;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {p1}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, " option only valid for C++ and C# (a.k.a CSharp)"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lantlr/Token;->getLine()I

    move-result v2

    invoke-virtual {p1}, Lantlr/Token;->getColumn()I

    move-result v3

    invoke-virtual {v0, v1, p3, v2, v3}, Lantlr/Tool;->error(Ljava/lang/String;Ljava/lang/String;II)V

    goto/16 :goto_0

    :cond_13
    invoke-virtual {p2}, Lantlr/Token;->getType()I

    move-result v0

    if-eq v0, v2, :cond_14

    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->tool:Lantlr/Tool;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {p1}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, " option must be a string"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lantlr/Token;->getLine()I

    move-result v2

    invoke-virtual {p2}, Lantlr/Token;->getColumn()I

    move-result v3

    invoke-virtual {v0, v1, p3, v2, v3}, Lantlr/Tool;->error(Ljava/lang/String;Ljava/lang/String;II)V

    goto/16 :goto_0

    :cond_14
    invoke-virtual {p1}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v0

    const-string v1, "namespace"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->tool:Lantlr/Tool;

    invoke-virtual {p2}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lantlr/Tool;->setNameSpace(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_15
    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->tool:Lantlr/Tool;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "Invalid file-level option: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p1}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lantlr/Token;->getLine()I

    move-result v2

    invoke-virtual {p2}, Lantlr/Token;->getColumn()I

    move-result v3

    invoke-virtual {v0, v1, p3, v2, v3}, Lantlr/Tool;->error(Ljava/lang/String;Ljava/lang/String;II)V

    goto/16 :goto_0
.end method

.method public setGrammarOption(Lantlr/Token;Lantlr/Token;)V
    .locals 5

    const/16 v3, 0x29

    const/16 v2, 0x18

    invoke-virtual {p1}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v0

    const-string v1, "tokdef"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v0

    const-string v1, "tokenVocabulary"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->tool:Lantlr/Tool;

    const-string v1, "tokdef/tokenVocabulary options are invalid >= ANTLR 2.6.0.\n  Use importVocab/exportVocab instead.  Please see the documentation.\n  The previous options were so heinous that Terence changed the whole\n  vocabulary mechanism; it was better to change the names rather than\n  subtly change the functionality of the known options.  Sorry!"

    iget-object v2, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    invoke-virtual {v2}, Lantlr/Grammar;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Lantlr/Token;->getLine()I

    move-result v3

    invoke-virtual {p2}, Lantlr/Token;->getColumn()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lantlr/Tool;->error(Ljava/lang/String;Ljava/lang/String;II)V

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v0

    const-string v1, "literal"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    instance-of v0, v0, Lantlr/LexerGrammar;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->tool:Lantlr/Tool;

    const-string v1, "the literal option is invalid >= ANTLR 2.6.0.\n  Use the \"tokens {...}\" mechanism instead."

    iget-object v2, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    invoke-virtual {v2}, Lantlr/Grammar;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Lantlr/Token;->getLine()I

    move-result v3

    invoke-virtual {p2}, Lantlr/Token;->getColumn()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lantlr/Tool;->error(Ljava/lang/String;Ljava/lang/String;II)V

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v0

    const-string v1, "exportVocab"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p2}, Lantlr/Token;->getType()I

    move-result v0

    if-eq v0, v3, :cond_3

    invoke-virtual {p2}, Lantlr/Token;->getType()I

    move-result v0

    if-ne v0, v2, :cond_4

    :cond_3
    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    invoke-virtual {p2}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lantlr/Grammar;->exportVocab:Ljava/lang/String;

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->tool:Lantlr/Tool;

    const-string v1, "exportVocab must be an identifier"

    iget-object v2, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    invoke-virtual {v2}, Lantlr/Grammar;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Lantlr/Token;->getLine()I

    move-result v3

    invoke-virtual {p2}, Lantlr/Token;->getColumn()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lantlr/Tool;->error(Ljava/lang/String;Ljava/lang/String;II)V

    goto :goto_0

    :cond_5
    invoke-virtual {p1}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v0

    const-string v1, "importVocab"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-virtual {p2}, Lantlr/Token;->getType()I

    move-result v0

    if-eq v0, v3, :cond_6

    invoke-virtual {p2}, Lantlr/Token;->getType()I

    move-result v0

    if-ne v0, v2, :cond_7

    :cond_6
    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    invoke-virtual {p2}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lantlr/Grammar;->importVocab:Ljava/lang/String;

    goto :goto_0

    :cond_7
    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->tool:Lantlr/Tool;

    const-string v1, "importVocab must be an identifier"

    iget-object v2, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    invoke-virtual {v2}, Lantlr/Grammar;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Lantlr/Token;->getLine()I

    move-result v3

    invoke-virtual {p2}, Lantlr/Token;->getColumn()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lantlr/Tool;->error(Ljava/lang/String;Ljava/lang/String;II)V

    goto/16 :goto_0

    :cond_8
    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    invoke-virtual {p1}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Lantlr/Grammar;->setOption(Ljava/lang/String;Lantlr/Token;)Z

    goto/16 :goto_0
.end method

.method public setRuleOption(Lantlr/Token;Lantlr/Token;)V
    .locals 0

    return-void
.end method

.method public setSubruleOption(Lantlr/Token;Lantlr/Token;)V
    .locals 0

    return-void
.end method

.method public setUserExceptions(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public startLexer(Ljava/lang/String;Lantlr/Token;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    iget v0, p0, Lantlr/DefineGrammarSymbols;->numLexers:I

    if-lez v0, :cond_0

    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->tool:Lantlr/Tool;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "You may only have one lexer per grammar file: class "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p2}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lantlr/Tool;->panic(Ljava/lang/String;)V

    :cond_0
    iget v0, p0, Lantlr/DefineGrammarSymbols;->numLexers:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lantlr/DefineGrammarSymbols;->numLexers:I

    invoke-virtual {p0}, Lantlr/DefineGrammarSymbols;->reset()V

    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->grammars:Ljava/util/Hashtable;

    invoke-virtual {v0, p2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lantlr/Grammar;

    if-eqz v0, :cond_2

    instance-of v0, v0, Lantlr/LexerGrammar;

    if-nez v0, :cond_1

    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->tool:Lantlr/Tool;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p2}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "\' is already defined as a non-lexer"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lantlr/Tool;->panic(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->tool:Lantlr/Tool;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "Lexer \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p2}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "\' is already defined"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lantlr/Tool;->panic(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    new-instance v0, Lantlr/LexerGrammar;

    invoke-virtual {p2}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lantlr/DefineGrammarSymbols;->tool:Lantlr/Tool;

    invoke-direct {v0, v1, v2, p3}, Lantlr/LexerGrammar;-><init>(Ljava/lang/String;Lantlr/Tool;Ljava/lang/String;)V

    iput-object p4, v0, Lantlr/LexerGrammar;->comment:Ljava/lang/String;

    iget-object v1, p0, Lantlr/DefineGrammarSymbols;->args:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Lantlr/LexerGrammar;->processArguments([Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Lantlr/LexerGrammar;->setFilename(Ljava/lang/String;)V

    iget-object v1, p0, Lantlr/DefineGrammarSymbols;->grammars:Ljava/util/Hashtable;

    invoke-virtual {v0}, Lantlr/LexerGrammar;->getClassName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lantlr/DefineGrammarSymbols;->thePreambleAction:Lantlr/Token;

    iput-object v1, v0, Lantlr/LexerGrammar;->preambleAction:Lantlr/Token;

    new-instance v1, Lantlr/CommonToken;

    const/4 v2, 0x0

    const-string v3, ""

    invoke-direct {v1, v2, v3}, Lantlr/CommonToken;-><init>(ILjava/lang/String;)V

    iput-object v1, p0, Lantlr/DefineGrammarSymbols;->thePreambleAction:Lantlr/Token;

    iput-object v0, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    goto :goto_0
.end method

.method public startParser(Ljava/lang/String;Lantlr/Token;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    iget v0, p0, Lantlr/DefineGrammarSymbols;->numParsers:I

    if-lez v0, :cond_0

    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->tool:Lantlr/Tool;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "You may only have one parser per grammar file: class "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p2}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lantlr/Tool;->panic(Ljava/lang/String;)V

    :cond_0
    iget v0, p0, Lantlr/DefineGrammarSymbols;->numParsers:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lantlr/DefineGrammarSymbols;->numParsers:I

    invoke-virtual {p0}, Lantlr/DefineGrammarSymbols;->reset()V

    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->grammars:Ljava/util/Hashtable;

    invoke-virtual {v0, p2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lantlr/Grammar;

    if-eqz v0, :cond_2

    instance-of v0, v0, Lantlr/ParserGrammar;

    if-nez v0, :cond_1

    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->tool:Lantlr/Tool;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p2}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "\' is already defined as a non-parser"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lantlr/Tool;->panic(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->tool:Lantlr/Tool;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "Parser \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p2}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "\' is already defined"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lantlr/Tool;->panic(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    new-instance v0, Lantlr/ParserGrammar;

    invoke-virtual {p2}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lantlr/DefineGrammarSymbols;->tool:Lantlr/Tool;

    invoke-direct {v0, v1, v2, p3}, Lantlr/ParserGrammar;-><init>(Ljava/lang/String;Lantlr/Tool;Ljava/lang/String;)V

    iput-object v0, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    iput-object p4, v0, Lantlr/Grammar;->comment:Ljava/lang/String;

    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    iget-object v1, p0, Lantlr/DefineGrammarSymbols;->args:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Lantlr/Grammar;->processArguments([Ljava/lang/String;)V

    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    invoke-virtual {v0, p1}, Lantlr/Grammar;->setFilename(Ljava/lang/String;)V

    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->grammars:Ljava/util/Hashtable;

    iget-object v1, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    invoke-virtual {v1}, Lantlr/Grammar;->getClassName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    iget-object v1, p0, Lantlr/DefineGrammarSymbols;->thePreambleAction:Lantlr/Token;

    iput-object v1, v0, Lantlr/Grammar;->preambleAction:Lantlr/Token;

    new-instance v0, Lantlr/CommonToken;

    const/4 v1, 0x0

    const-string v2, ""

    invoke-direct {v0, v1, v2}, Lantlr/CommonToken;-><init>(ILjava/lang/String;)V

    iput-object v0, p0, Lantlr/DefineGrammarSymbols;->thePreambleAction:Lantlr/Token;

    goto :goto_0
.end method

.method public startTreeWalker(Ljava/lang/String;Lantlr/Token;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    iget v0, p0, Lantlr/DefineGrammarSymbols;->numTreeParsers:I

    if-lez v0, :cond_0

    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->tool:Lantlr/Tool;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "You may only have one tree parser per grammar file: class "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p2}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lantlr/Tool;->panic(Ljava/lang/String;)V

    :cond_0
    iget v0, p0, Lantlr/DefineGrammarSymbols;->numTreeParsers:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lantlr/DefineGrammarSymbols;->numTreeParsers:I

    invoke-virtual {p0}, Lantlr/DefineGrammarSymbols;->reset()V

    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->grammars:Ljava/util/Hashtable;

    invoke-virtual {v0, p2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lantlr/Grammar;

    if-eqz v0, :cond_2

    instance-of v0, v0, Lantlr/TreeWalkerGrammar;

    if-nez v0, :cond_1

    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->tool:Lantlr/Tool;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p2}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "\' is already defined as a non-tree-walker"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lantlr/Tool;->panic(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->tool:Lantlr/Tool;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "Tree-walker \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {p2}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "\' is already defined"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lantlr/Tool;->panic(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    new-instance v0, Lantlr/TreeWalkerGrammar;

    invoke-virtual {p2}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lantlr/DefineGrammarSymbols;->tool:Lantlr/Tool;

    invoke-direct {v0, v1, v2, p3}, Lantlr/TreeWalkerGrammar;-><init>(Ljava/lang/String;Lantlr/Tool;Ljava/lang/String;)V

    iput-object v0, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    iput-object p4, v0, Lantlr/Grammar;->comment:Ljava/lang/String;

    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    iget-object v1, p0, Lantlr/DefineGrammarSymbols;->args:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Lantlr/Grammar;->processArguments([Ljava/lang/String;)V

    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    invoke-virtual {v0, p1}, Lantlr/Grammar;->setFilename(Ljava/lang/String;)V

    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->grammars:Ljava/util/Hashtable;

    iget-object v1, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    invoke-virtual {v1}, Lantlr/Grammar;->getClassName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lantlr/DefineGrammarSymbols;->grammar:Lantlr/Grammar;

    iget-object v1, p0, Lantlr/DefineGrammarSymbols;->thePreambleAction:Lantlr/Token;

    iput-object v1, v0, Lantlr/Grammar;->preambleAction:Lantlr/Token;

    new-instance v0, Lantlr/CommonToken;

    const/4 v1, 0x0

    const-string v2, ""

    invoke-direct {v0, v1, v2}, Lantlr/CommonToken;-><init>(ILjava/lang/String;)V

    iput-object v0, p0, Lantlr/DefineGrammarSymbols;->thePreambleAction:Lantlr/Token;

    goto :goto_0
.end method

.method public synPred()V
    .locals 0

    return-void
.end method

.method public zeroOrMoreSubRule()V
    .locals 0

    return-void
.end method
