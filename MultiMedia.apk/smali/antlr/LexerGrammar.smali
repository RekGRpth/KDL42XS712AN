.class Lantlr/LexerGrammar;
.super Lantlr/Grammar;
.source "LexerGrammar.java"


# instance fields
.field protected caseSensitive:Z

.field protected caseSensitiveLiterals:Z

.field protected charVocabulary:Lantlr/collections/impl/BitSet;

.field protected filterMode:Z

.field protected filterRule:Ljava/lang/String;

.field protected testLiterals:Z


# direct methods
.method constructor <init>(Ljava/lang/String;Lantlr/Tool;Ljava/lang/String;)V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, p3}, Lantlr/Grammar;-><init>(Ljava/lang/String;Lantlr/Tool;Ljava/lang/String;)V

    iput-boolean v0, p0, Lantlr/LexerGrammar;->testLiterals:Z

    iput-boolean v0, p0, Lantlr/LexerGrammar;->caseSensitiveLiterals:Z

    iput-boolean v0, p0, Lantlr/LexerGrammar;->caseSensitive:Z

    iput-boolean v1, p0, Lantlr/LexerGrammar;->filterMode:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lantlr/LexerGrammar;->filterRule:Ljava/lang/String;

    new-instance v0, Lantlr/collections/impl/BitSet;

    invoke-direct {v0}, Lantlr/collections/impl/BitSet;-><init>()V

    iput-object v0, p0, Lantlr/LexerGrammar;->charVocabulary:Lantlr/collections/impl/BitSet;

    iput-boolean v1, p0, Lantlr/LexerGrammar;->defaultErrorHandler:Z

    return-void
.end method


# virtual methods
.method public generate()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lantlr/LexerGrammar;->generator:Lantlr/CodeGenerator;

    invoke-virtual {v0, p0}, Lantlr/CodeGenerator;->gen(Lantlr/LexerGrammar;)V

    return-void
.end method

.method public getSuperClass()Ljava/lang/String;
    .locals 1

    iget-boolean v0, p0, Lantlr/LexerGrammar;->debuggingOutput:Z

    if-eqz v0, :cond_0

    const-string v0, "debug.DebuggingCharScanner"

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "CharScanner"

    goto :goto_0
.end method

.method public getTestLiterals()Z
    .locals 1

    iget-boolean v0, p0, Lantlr/LexerGrammar;->testLiterals:Z

    return v0
.end method

.method public processArguments([Ljava/lang/String;)V
    .locals 4

    const/4 v3, 0x1

    const/4 v0, 0x0

    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_3

    aget-object v1, p1, v0

    const-string v2, "-trace"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iput-boolean v3, p0, Lantlr/LexerGrammar;->traceRules:Z

    iget-object v1, p0, Lantlr/LexerGrammar;->antlrTool:Lantlr/Tool;

    invoke-virtual {v1, v0}, Lantlr/Tool;->setArgOK(I)V

    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    aget-object v1, p1, v0

    const-string v2, "-traceLexer"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iput-boolean v3, p0, Lantlr/LexerGrammar;->traceRules:Z

    iget-object v1, p0, Lantlr/LexerGrammar;->antlrTool:Lantlr/Tool;

    invoke-virtual {v1, v0}, Lantlr/Tool;->setArgOK(I)V

    goto :goto_1

    :cond_2
    aget-object v1, p1, v0

    const-string v2, "-debug"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iput-boolean v3, p0, Lantlr/LexerGrammar;->debuggingOutput:Z

    iget-object v1, p0, Lantlr/LexerGrammar;->antlrTool:Lantlr/Tool;

    invoke-virtual {v1, v0}, Lantlr/Tool;->setArgOK(I)V

    goto :goto_1

    :cond_3
    return-void
.end method

.method public setCharVocabulary(Lantlr/collections/impl/BitSet;)V
    .locals 0

    iput-object p1, p0, Lantlr/LexerGrammar;->charVocabulary:Lantlr/collections/impl/BitSet;

    return-void
.end method

.method public setOption(Ljava/lang/String;Lantlr/Token;)Z
    .locals 6

    const/4 v1, 0x0

    const/4 v0, 0x1

    invoke-virtual {p2}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v2

    const-string v3, "buildAST"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v1, p0, Lantlr/LexerGrammar;->antlrTool:Lantlr/Tool;

    const-string v2, "buildAST option is not valid for lexer"

    invoke-virtual {p0}, Lantlr/LexerGrammar;->getFilename()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, Lantlr/Token;->getLine()I

    move-result v4

    invoke-virtual {p2}, Lantlr/Token;->getColumn()I

    move-result v5

    invoke-virtual {v1, v2, v3, v4, v5}, Lantlr/Tool;->warning(Ljava/lang/String;Ljava/lang/String;II)V

    :cond_0
    :goto_0
    return v0

    :cond_1
    const-string v3, "testLiterals"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    const-string v3, "true"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iput-boolean v0, p0, Lantlr/LexerGrammar;->testLiterals:Z

    goto :goto_0

    :cond_2
    const-string v3, "false"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iput-boolean v1, p0, Lantlr/LexerGrammar;->testLiterals:Z

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lantlr/LexerGrammar;->antlrTool:Lantlr/Tool;

    const-string v2, "testLiterals option must be true or false"

    invoke-virtual {p0}, Lantlr/LexerGrammar;->getFilename()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, Lantlr/Token;->getLine()I

    move-result v4

    invoke-virtual {p2}, Lantlr/Token;->getColumn()I

    move-result v5

    invoke-virtual {v1, v2, v3, v4, v5}, Lantlr/Tool;->warning(Ljava/lang/String;Ljava/lang/String;II)V

    goto :goto_0

    :cond_4
    const-string v3, "interactive"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    const-string v3, "true"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    iput-boolean v0, p0, Lantlr/LexerGrammar;->interactive:Z

    goto :goto_0

    :cond_5
    const-string v3, "false"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    iput-boolean v1, p0, Lantlr/LexerGrammar;->interactive:Z

    goto :goto_0

    :cond_6
    iget-object v1, p0, Lantlr/LexerGrammar;->antlrTool:Lantlr/Tool;

    const-string v2, "interactive option must be true or false"

    invoke-virtual {p0}, Lantlr/LexerGrammar;->getFilename()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, Lantlr/Token;->getLine()I

    move-result v4

    invoke-virtual {p2}, Lantlr/Token;->getColumn()I

    move-result v5

    invoke-virtual {v1, v2, v3, v4, v5}, Lantlr/Tool;->error(Ljava/lang/String;Ljava/lang/String;II)V

    goto :goto_0

    :cond_7
    const-string v3, "caseSensitive"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    const-string v3, "true"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    iput-boolean v0, p0, Lantlr/LexerGrammar;->caseSensitive:Z

    goto :goto_0

    :cond_8
    const-string v3, "false"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    iput-boolean v1, p0, Lantlr/LexerGrammar;->caseSensitive:Z

    goto/16 :goto_0

    :cond_9
    iget-object v1, p0, Lantlr/LexerGrammar;->antlrTool:Lantlr/Tool;

    const-string v2, "caseSensitive option must be true or false"

    invoke-virtual {p0}, Lantlr/LexerGrammar;->getFilename()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, Lantlr/Token;->getLine()I

    move-result v4

    invoke-virtual {p2}, Lantlr/Token;->getColumn()I

    move-result v5

    invoke-virtual {v1, v2, v3, v4, v5}, Lantlr/Tool;->warning(Ljava/lang/String;Ljava/lang/String;II)V

    goto/16 :goto_0

    :cond_a
    const-string v3, "caseSensitiveLiterals"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_d

    const-string v3, "true"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_b

    iput-boolean v0, p0, Lantlr/LexerGrammar;->caseSensitiveLiterals:Z

    goto/16 :goto_0

    :cond_b
    const-string v3, "false"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    iput-boolean v1, p0, Lantlr/LexerGrammar;->caseSensitiveLiterals:Z

    goto/16 :goto_0

    :cond_c
    iget-object v1, p0, Lantlr/LexerGrammar;->antlrTool:Lantlr/Tool;

    const-string v2, "caseSensitiveLiterals option must be true or false"

    invoke-virtual {p0}, Lantlr/LexerGrammar;->getFilename()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, Lantlr/Token;->getLine()I

    move-result v4

    invoke-virtual {p2}, Lantlr/Token;->getColumn()I

    move-result v5

    invoke-virtual {v1, v2, v3, v4, v5}, Lantlr/Tool;->warning(Ljava/lang/String;Ljava/lang/String;II)V

    goto/16 :goto_0

    :cond_d
    const-string v3, "filter"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_11

    const-string v3, "true"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_e

    iput-boolean v0, p0, Lantlr/LexerGrammar;->filterMode:Z

    goto/16 :goto_0

    :cond_e
    const-string v3, "false"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_f

    iput-boolean v1, p0, Lantlr/LexerGrammar;->filterMode:Z

    goto/16 :goto_0

    :cond_f
    invoke-virtual {p2}, Lantlr/Token;->getType()I

    move-result v1

    const/16 v3, 0x18

    if-ne v1, v3, :cond_10

    iput-boolean v0, p0, Lantlr/LexerGrammar;->filterMode:Z

    iput-object v2, p0, Lantlr/LexerGrammar;->filterRule:Ljava/lang/String;

    goto/16 :goto_0

    :cond_10
    iget-object v1, p0, Lantlr/LexerGrammar;->antlrTool:Lantlr/Tool;

    const-string v2, "filter option must be true, false, or a lexer rule name"

    invoke-virtual {p0}, Lantlr/LexerGrammar;->getFilename()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, Lantlr/Token;->getLine()I

    move-result v4

    invoke-virtual {p2}, Lantlr/Token;->getColumn()I

    move-result v5

    invoke-virtual {v1, v2, v3, v4, v5}, Lantlr/Tool;->warning(Ljava/lang/String;Ljava/lang/String;II)V

    goto/16 :goto_0

    :cond_11
    const-string v2, "longestPossible"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_12

    iget-object v1, p0, Lantlr/LexerGrammar;->antlrTool:Lantlr/Tool;

    const-string v2, "longestPossible option has been deprecated; ignoring it..."

    invoke-virtual {p0}, Lantlr/LexerGrammar;->getFilename()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, Lantlr/Token;->getLine()I

    move-result v4

    invoke-virtual {p2}, Lantlr/Token;->getColumn()I

    move-result v5

    invoke-virtual {v1, v2, v3, v4, v5}, Lantlr/Tool;->warning(Ljava/lang/String;Ljava/lang/String;II)V

    goto/16 :goto_0

    :cond_12
    invoke-super {p0, p1, p2}, Lantlr/Grammar;->setOption(Ljava/lang/String;Lantlr/Token;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v0, p0, Lantlr/LexerGrammar;->antlrTool:Lantlr/Tool;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "Invalid option: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lantlr/LexerGrammar;->getFilename()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, Lantlr/Token;->getLine()I

    move-result v4

    invoke-virtual {p2}, Lantlr/Token;->getColumn()I

    move-result v5

    invoke-virtual {v0, v2, v3, v4, v5}, Lantlr/Tool;->error(Ljava/lang/String;Ljava/lang/String;II)V

    move v0, v1

    goto/16 :goto_0
.end method
