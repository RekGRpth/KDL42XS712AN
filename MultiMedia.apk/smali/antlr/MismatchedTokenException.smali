.class public Lantlr/MismatchedTokenException;
.super Lantlr/RecognitionException;
.source "MismatchedTokenException.java"


# static fields
.field public static final NOT_RANGE:I = 0x4

.field public static final NOT_SET:I = 0x6

.field public static final NOT_TOKEN:I = 0x2

.field public static final RANGE:I = 0x3

.field public static final SET:I = 0x5

.field public static final TOKEN:I = 0x1


# instance fields
.field public expecting:I

.field public mismatchType:I

.field public node:Lantlr/collections/AST;

.field public set:Lantlr/collections/impl/BitSet;

.field public token:Lantlr/Token;

.field tokenNames:[Ljava/lang/String;

.field tokenText:Ljava/lang/String;

.field public upper:I


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, -0x1

    const-string v0, "Mismatched Token: expecting any AST node"

    const-string v1, "<AST>"

    invoke-direct {p0, v0, v1, v2, v2}, Lantlr/RecognitionException;-><init>(Ljava/lang/String;Ljava/lang/String;II)V

    const/4 v0, 0x0

    iput-object v0, p0, Lantlr/MismatchedTokenException;->tokenText:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>([Ljava/lang/String;Lantlr/Token;IIZLjava/lang/String;)V
    .locals 3

    const-string v0, "Mismatched Token"

    invoke-virtual {p2}, Lantlr/Token;->getLine()I

    move-result v1

    invoke-virtual {p2}, Lantlr/Token;->getColumn()I

    move-result v2

    invoke-direct {p0, v0, p6, v1, v2}, Lantlr/RecognitionException;-><init>(Ljava/lang/String;Ljava/lang/String;II)V

    const/4 v0, 0x0

    iput-object v0, p0, Lantlr/MismatchedTokenException;->tokenText:Ljava/lang/String;

    iput-object p1, p0, Lantlr/MismatchedTokenException;->tokenNames:[Ljava/lang/String;

    iput-object p2, p0, Lantlr/MismatchedTokenException;->token:Lantlr/Token;

    invoke-virtual {p2}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lantlr/MismatchedTokenException;->tokenText:Ljava/lang/String;

    if-eqz p5, :cond_0

    const/4 v0, 0x4

    :goto_0
    iput v0, p0, Lantlr/MismatchedTokenException;->mismatchType:I

    iput p3, p0, Lantlr/MismatchedTokenException;->expecting:I

    iput p4, p0, Lantlr/MismatchedTokenException;->upper:I

    return-void

    :cond_0
    const/4 v0, 0x3

    goto :goto_0
.end method

.method public constructor <init>([Ljava/lang/String;Lantlr/Token;IZLjava/lang/String;)V
    .locals 3

    const-string v0, "Mismatched Token"

    invoke-virtual {p2}, Lantlr/Token;->getLine()I

    move-result v1

    invoke-virtual {p2}, Lantlr/Token;->getColumn()I

    move-result v2

    invoke-direct {p0, v0, p5, v1, v2}, Lantlr/RecognitionException;-><init>(Ljava/lang/String;Ljava/lang/String;II)V

    const/4 v0, 0x0

    iput-object v0, p0, Lantlr/MismatchedTokenException;->tokenText:Ljava/lang/String;

    iput-object p1, p0, Lantlr/MismatchedTokenException;->tokenNames:[Ljava/lang/String;

    iput-object p2, p0, Lantlr/MismatchedTokenException;->token:Lantlr/Token;

    invoke-virtual {p2}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lantlr/MismatchedTokenException;->tokenText:Ljava/lang/String;

    if-eqz p4, :cond_0

    const/4 v0, 0x2

    :goto_0
    iput v0, p0, Lantlr/MismatchedTokenException;->mismatchType:I

    iput p3, p0, Lantlr/MismatchedTokenException;->expecting:I

    return-void

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public constructor <init>([Ljava/lang/String;Lantlr/Token;Lantlr/collections/impl/BitSet;ZLjava/lang/String;)V
    .locals 3

    const-string v0, "Mismatched Token"

    invoke-virtual {p2}, Lantlr/Token;->getLine()I

    move-result v1

    invoke-virtual {p2}, Lantlr/Token;->getColumn()I

    move-result v2

    invoke-direct {p0, v0, p5, v1, v2}, Lantlr/RecognitionException;-><init>(Ljava/lang/String;Ljava/lang/String;II)V

    const/4 v0, 0x0

    iput-object v0, p0, Lantlr/MismatchedTokenException;->tokenText:Ljava/lang/String;

    iput-object p1, p0, Lantlr/MismatchedTokenException;->tokenNames:[Ljava/lang/String;

    iput-object p2, p0, Lantlr/MismatchedTokenException;->token:Lantlr/Token;

    invoke-virtual {p2}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lantlr/MismatchedTokenException;->tokenText:Ljava/lang/String;

    if-eqz p4, :cond_0

    const/4 v0, 0x6

    :goto_0
    iput v0, p0, Lantlr/MismatchedTokenException;->mismatchType:I

    iput-object p3, p0, Lantlr/MismatchedTokenException;->set:Lantlr/collections/impl/BitSet;

    return-void

    :cond_0
    const/4 v0, 0x5

    goto :goto_0
.end method

.method public constructor <init>([Ljava/lang/String;Lantlr/collections/AST;IIZ)V
    .locals 4

    const/4 v0, -0x1

    const-string v2, "Mismatched Token"

    const-string v3, "<AST>"

    if-nez p2, :cond_0

    move v1, v0

    :goto_0
    if-nez p2, :cond_1

    :goto_1
    invoke-direct {p0, v2, v3, v1, v0}, Lantlr/RecognitionException;-><init>(Ljava/lang/String;Ljava/lang/String;II)V

    const/4 v0, 0x0

    iput-object v0, p0, Lantlr/MismatchedTokenException;->tokenText:Ljava/lang/String;

    iput-object p1, p0, Lantlr/MismatchedTokenException;->tokenNames:[Ljava/lang/String;

    iput-object p2, p0, Lantlr/MismatchedTokenException;->node:Lantlr/collections/AST;

    if-nez p2, :cond_2

    const-string v0, "<empty tree>"

    iput-object v0, p0, Lantlr/MismatchedTokenException;->tokenText:Ljava/lang/String;

    :goto_2
    if-eqz p5, :cond_3

    const/4 v0, 0x4

    :goto_3
    iput v0, p0, Lantlr/MismatchedTokenException;->mismatchType:I

    iput p3, p0, Lantlr/MismatchedTokenException;->expecting:I

    iput p4, p0, Lantlr/MismatchedTokenException;->upper:I

    return-void

    :cond_0
    invoke-interface {p2}, Lantlr/collections/AST;->getLine()I

    move-result v1

    goto :goto_0

    :cond_1
    invoke-interface {p2}, Lantlr/collections/AST;->getColumn()I

    move-result v0

    goto :goto_1

    :cond_2
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lantlr/MismatchedTokenException;->tokenText:Ljava/lang/String;

    goto :goto_2

    :cond_3
    const/4 v0, 0x3

    goto :goto_3
.end method

.method public constructor <init>([Ljava/lang/String;Lantlr/collections/AST;IZ)V
    .locals 4

    const/4 v0, -0x1

    const-string v2, "Mismatched Token"

    const-string v3, "<AST>"

    if-nez p2, :cond_0

    move v1, v0

    :goto_0
    if-nez p2, :cond_1

    :goto_1
    invoke-direct {p0, v2, v3, v1, v0}, Lantlr/RecognitionException;-><init>(Ljava/lang/String;Ljava/lang/String;II)V

    const/4 v0, 0x0

    iput-object v0, p0, Lantlr/MismatchedTokenException;->tokenText:Ljava/lang/String;

    iput-object p1, p0, Lantlr/MismatchedTokenException;->tokenNames:[Ljava/lang/String;

    iput-object p2, p0, Lantlr/MismatchedTokenException;->node:Lantlr/collections/AST;

    if-nez p2, :cond_2

    const-string v0, "<empty tree>"

    iput-object v0, p0, Lantlr/MismatchedTokenException;->tokenText:Ljava/lang/String;

    :goto_2
    if-eqz p4, :cond_3

    const/4 v0, 0x2

    :goto_3
    iput v0, p0, Lantlr/MismatchedTokenException;->mismatchType:I

    iput p3, p0, Lantlr/MismatchedTokenException;->expecting:I

    return-void

    :cond_0
    invoke-interface {p2}, Lantlr/collections/AST;->getLine()I

    move-result v1

    goto :goto_0

    :cond_1
    invoke-interface {p2}, Lantlr/collections/AST;->getColumn()I

    move-result v0

    goto :goto_1

    :cond_2
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lantlr/MismatchedTokenException;->tokenText:Ljava/lang/String;

    goto :goto_2

    :cond_3
    const/4 v0, 0x1

    goto :goto_3
.end method

.method public constructor <init>([Ljava/lang/String;Lantlr/collections/AST;Lantlr/collections/impl/BitSet;Z)V
    .locals 4

    const/4 v0, -0x1

    const-string v2, "Mismatched Token"

    const-string v3, "<AST>"

    if-nez p2, :cond_0

    move v1, v0

    :goto_0
    if-nez p2, :cond_1

    :goto_1
    invoke-direct {p0, v2, v3, v1, v0}, Lantlr/RecognitionException;-><init>(Ljava/lang/String;Ljava/lang/String;II)V

    const/4 v0, 0x0

    iput-object v0, p0, Lantlr/MismatchedTokenException;->tokenText:Ljava/lang/String;

    iput-object p1, p0, Lantlr/MismatchedTokenException;->tokenNames:[Ljava/lang/String;

    iput-object p2, p0, Lantlr/MismatchedTokenException;->node:Lantlr/collections/AST;

    if-nez p2, :cond_2

    const-string v0, "<empty tree>"

    iput-object v0, p0, Lantlr/MismatchedTokenException;->tokenText:Ljava/lang/String;

    :goto_2
    if-eqz p4, :cond_3

    const/4 v0, 0x6

    :goto_3
    iput v0, p0, Lantlr/MismatchedTokenException;->mismatchType:I

    iput-object p3, p0, Lantlr/MismatchedTokenException;->set:Lantlr/collections/impl/BitSet;

    return-void

    :cond_0
    invoke-interface {p2}, Lantlr/collections/AST;->getLine()I

    move-result v1

    goto :goto_0

    :cond_1
    invoke-interface {p2}, Lantlr/collections/AST;->getColumn()I

    move-result v0

    goto :goto_1

    :cond_2
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lantlr/MismatchedTokenException;->tokenText:Ljava/lang/String;

    goto :goto_2

    :cond_3
    const/4 v0, 0x5

    goto :goto_3
.end method

.method private tokenName(I)Ljava/lang/String;
    .locals 2

    if-nez p1, :cond_0

    const-string v0, "<Set of tokens>"

    :goto_0
    return-object v0

    :cond_0
    if-ltz p1, :cond_1

    iget-object v0, p0, Lantlr/MismatchedTokenException;->tokenNames:[Ljava/lang/String;

    array-length v0, v0

    if-lt p1, v0, :cond_2

    :cond_1
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string v1, "<"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, ">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lantlr/MismatchedTokenException;->tokenNames:[Ljava/lang/String;

    aget-object v0, v0, p1

    goto :goto_0
.end method


# virtual methods
.method public getMessage()Ljava/lang/String;
    .locals 4

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    iget v0, p0, Lantlr/MismatchedTokenException;->mismatchType:I

    packed-switch v0, :pswitch_data_0

    invoke-super {p0}, Lantlr/RecognitionException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :goto_0
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :pswitch_0
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "expecting "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget v2, p0, Lantlr/MismatchedTokenException;->expecting:I

    invoke-direct {p0, v2}, Lantlr/MismatchedTokenException;->tokenName(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v2, ", found \'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-object v2, p0, Lantlr/MismatchedTokenException;->tokenText:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v2, "\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    :pswitch_1
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "expecting anything but "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget v2, p0, Lantlr/MismatchedTokenException;->expecting:I

    invoke-direct {p0, v2}, Lantlr/MismatchedTokenException;->tokenName(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v2, "; got it anyway"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    :pswitch_2
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "expecting token in range: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget v2, p0, Lantlr/MismatchedTokenException;->expecting:I

    invoke-direct {p0, v2}, Lantlr/MismatchedTokenException;->tokenName(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v2, ".."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget v2, p0, Lantlr/MismatchedTokenException;->upper:I

    invoke-direct {p0, v2}, Lantlr/MismatchedTokenException;->tokenName(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v2, ", found \'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-object v2, p0, Lantlr/MismatchedTokenException;->tokenText:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v2, "\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto/16 :goto_0

    :pswitch_3
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "expecting token NOT in range: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget v2, p0, Lantlr/MismatchedTokenException;->expecting:I

    invoke-direct {p0, v2}, Lantlr/MismatchedTokenException;->tokenName(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v2, ".."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget v2, p0, Lantlr/MismatchedTokenException;->upper:I

    invoke-direct {p0, v2}, Lantlr/MismatchedTokenException;->tokenName(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v2, ", found \'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-object v2, p0, Lantlr/MismatchedTokenException;->tokenText:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v2, "\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto/16 :goto_0

    :pswitch_4
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "expecting "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    iget v0, p0, Lantlr/MismatchedTokenException;->mismatchType:I

    const/4 v3, 0x6

    if-ne v0, v3, :cond_0

    const-string v0, "NOT "

    :goto_1
    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v2, "one of ("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-object v0, p0, Lantlr/MismatchedTokenException;->set:Lantlr/collections/impl/BitSet;

    invoke-virtual {v0}, Lantlr/collections/impl/BitSet;->toArray()[I

    move-result-object v2

    const/4 v0, 0x0

    :goto_2
    array-length v3, v2

    if-ge v0, v3, :cond_1

    const-string v3, " "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    aget v3, v2, v0

    invoke-direct {p0, v3}, Lantlr/MismatchedTokenException;->tokenName(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_0
    const-string v0, ""

    goto :goto_1

    :cond_1
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "), found \'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-object v2, p0, Lantlr/MismatchedTokenException;->tokenText:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v2, "\'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_4
    .end packed-switch
.end method
