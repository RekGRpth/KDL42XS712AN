.class public Lantlr/TokenStreamHiddenTokenFilter;
.super Lantlr/TokenStreamBasicFilter;
.source "TokenStreamHiddenTokenFilter.java"

# interfaces
.implements Lantlr/TokenStream;


# instance fields
.field protected firstHidden:Lantlr/CommonHiddenStreamToken;

.field protected hideMask:Lantlr/collections/impl/BitSet;

.field protected lastHiddenToken:Lantlr/CommonHiddenStreamToken;

.field protected nextMonitoredToken:Lantlr/CommonHiddenStreamToken;


# direct methods
.method public constructor <init>(Lantlr/TokenStream;)V
    .locals 1

    invoke-direct {p0, p1}, Lantlr/TokenStreamBasicFilter;-><init>(Lantlr/TokenStream;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lantlr/TokenStreamHiddenTokenFilter;->firstHidden:Lantlr/CommonHiddenStreamToken;

    new-instance v0, Lantlr/collections/impl/BitSet;

    invoke-direct {v0}, Lantlr/collections/impl/BitSet;-><init>()V

    iput-object v0, p0, Lantlr/TokenStreamHiddenTokenFilter;->hideMask:Lantlr/collections/impl/BitSet;

    return-void
.end method

.method private consumeFirst()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/4 v3, 0x1

    invoke-virtual {p0}, Lantlr/TokenStreamHiddenTokenFilter;->consume()V

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lantlr/TokenStreamHiddenTokenFilter;->hideMask:Lantlr/collections/impl/BitSet;

    invoke-virtual {p0, v3}, Lantlr/TokenStreamHiddenTokenFilter;->LA(I)Lantlr/CommonHiddenStreamToken;

    move-result-object v2

    invoke-virtual {v2}, Lantlr/CommonHiddenStreamToken;->getType()I

    move-result v2

    invoke-virtual {v1, v2}, Lantlr/collections/impl/BitSet;->member(I)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lantlr/TokenStreamHiddenTokenFilter;->discardMask:Lantlr/collections/impl/BitSet;

    invoke-virtual {p0, v3}, Lantlr/TokenStreamHiddenTokenFilter;->LA(I)Lantlr/CommonHiddenStreamToken;

    move-result-object v2

    invoke-virtual {v2}, Lantlr/CommonHiddenStreamToken;->getType()I

    move-result v2

    invoke-virtual {v1, v2}, Lantlr/collections/impl/BitSet;->member(I)Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_0
    iget-object v1, p0, Lantlr/TokenStreamHiddenTokenFilter;->hideMask:Lantlr/collections/impl/BitSet;

    invoke-virtual {p0, v3}, Lantlr/TokenStreamHiddenTokenFilter;->LA(I)Lantlr/CommonHiddenStreamToken;

    move-result-object v2

    invoke-virtual {v2}, Lantlr/CommonHiddenStreamToken;->getType()I

    move-result v2

    invoke-virtual {v1, v2}, Lantlr/collections/impl/BitSet;->member(I)Z

    move-result v1

    if-eqz v1, :cond_1

    if-nez v0, :cond_2

    invoke-virtual {p0, v3}, Lantlr/TokenStreamHiddenTokenFilter;->LA(I)Lantlr/CommonHiddenStreamToken;

    move-result-object v0

    :goto_1
    iput-object v0, p0, Lantlr/TokenStreamHiddenTokenFilter;->lastHiddenToken:Lantlr/CommonHiddenStreamToken;

    iget-object v1, p0, Lantlr/TokenStreamHiddenTokenFilter;->firstHidden:Lantlr/CommonHiddenStreamToken;

    if-nez v1, :cond_1

    iput-object v0, p0, Lantlr/TokenStreamHiddenTokenFilter;->firstHidden:Lantlr/CommonHiddenStreamToken;

    :cond_1
    invoke-virtual {p0}, Lantlr/TokenStreamHiddenTokenFilter;->consume()V

    goto :goto_0

    :cond_2
    invoke-virtual {p0, v3}, Lantlr/TokenStreamHiddenTokenFilter;->LA(I)Lantlr/CommonHiddenStreamToken;

    move-result-object v1

    invoke-virtual {v0, v1}, Lantlr/CommonHiddenStreamToken;->setHiddenAfter(Lantlr/CommonHiddenStreamToken;)V

    invoke-virtual {p0, v3}, Lantlr/TokenStreamHiddenTokenFilter;->LA(I)Lantlr/CommonHiddenStreamToken;

    move-result-object v1

    invoke-virtual {v1, v0}, Lantlr/CommonHiddenStreamToken;->setHiddenBefore(Lantlr/CommonHiddenStreamToken;)V

    invoke-virtual {p0, v3}, Lantlr/TokenStreamHiddenTokenFilter;->LA(I)Lantlr/CommonHiddenStreamToken;

    move-result-object v0

    goto :goto_1

    :cond_3
    return-void
.end method


# virtual methods
.method protected LA(I)Lantlr/CommonHiddenStreamToken;
    .locals 1

    iget-object v0, p0, Lantlr/TokenStreamHiddenTokenFilter;->nextMonitoredToken:Lantlr/CommonHiddenStreamToken;

    return-object v0
.end method

.method protected consume()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/TokenStreamException;
        }
    .end annotation

    iget-object v0, p0, Lantlr/TokenStreamHiddenTokenFilter;->input:Lantlr/TokenStream;

    invoke-interface {v0}, Lantlr/TokenStream;->nextToken()Lantlr/Token;

    move-result-object v0

    check-cast v0, Lantlr/CommonHiddenStreamToken;

    iput-object v0, p0, Lantlr/TokenStreamHiddenTokenFilter;->nextMonitoredToken:Lantlr/CommonHiddenStreamToken;

    return-void
.end method

.method public getDiscardMask()Lantlr/collections/impl/BitSet;
    .locals 1

    iget-object v0, p0, Lantlr/TokenStreamHiddenTokenFilter;->discardMask:Lantlr/collections/impl/BitSet;

    return-object v0
.end method

.method public getHiddenAfter(Lantlr/CommonHiddenStreamToken;)Lantlr/CommonHiddenStreamToken;
    .locals 1

    invoke-virtual {p1}, Lantlr/CommonHiddenStreamToken;->getHiddenAfter()Lantlr/CommonHiddenStreamToken;

    move-result-object v0

    return-object v0
.end method

.method public getHiddenBefore(Lantlr/CommonHiddenStreamToken;)Lantlr/CommonHiddenStreamToken;
    .locals 1

    invoke-virtual {p1}, Lantlr/CommonHiddenStreamToken;->getHiddenBefore()Lantlr/CommonHiddenStreamToken;

    move-result-object v0

    return-object v0
.end method

.method public getHideMask()Lantlr/collections/impl/BitSet;
    .locals 1

    iget-object v0, p0, Lantlr/TokenStreamHiddenTokenFilter;->hideMask:Lantlr/collections/impl/BitSet;

    return-object v0
.end method

.method public getInitialHiddenToken()Lantlr/CommonHiddenStreamToken;
    .locals 1

    iget-object v0, p0, Lantlr/TokenStreamHiddenTokenFilter;->firstHidden:Lantlr/CommonHiddenStreamToken;

    return-object v0
.end method

.method public hide(I)V
    .locals 1

    iget-object v0, p0, Lantlr/TokenStreamHiddenTokenFilter;->hideMask:Lantlr/collections/impl/BitSet;

    invoke-virtual {v0, p1}, Lantlr/collections/impl/BitSet;->add(I)V

    return-void
.end method

.method public hide(Lantlr/collections/impl/BitSet;)V
    .locals 0

    iput-object p1, p0, Lantlr/TokenStreamHiddenTokenFilter;->hideMask:Lantlr/collections/impl/BitSet;

    return-void
.end method

.method public nextToken()Lantlr/Token;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/4 v4, 0x1

    invoke-virtual {p0, v4}, Lantlr/TokenStreamHiddenTokenFilter;->LA(I)Lantlr/CommonHiddenStreamToken;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lantlr/TokenStreamHiddenTokenFilter;->consumeFirst()V

    :cond_0
    invoke-virtual {p0, v4}, Lantlr/TokenStreamHiddenTokenFilter;->LA(I)Lantlr/CommonHiddenStreamToken;

    move-result-object v1

    iget-object v0, p0, Lantlr/TokenStreamHiddenTokenFilter;->lastHiddenToken:Lantlr/CommonHiddenStreamToken;

    invoke-virtual {v1, v0}, Lantlr/CommonHiddenStreamToken;->setHiddenBefore(Lantlr/CommonHiddenStreamToken;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lantlr/TokenStreamHiddenTokenFilter;->lastHiddenToken:Lantlr/CommonHiddenStreamToken;

    invoke-virtual {p0}, Lantlr/TokenStreamHiddenTokenFilter;->consume()V

    move-object v0, v1

    :goto_0
    iget-object v2, p0, Lantlr/TokenStreamHiddenTokenFilter;->hideMask:Lantlr/collections/impl/BitSet;

    invoke-virtual {p0, v4}, Lantlr/TokenStreamHiddenTokenFilter;->LA(I)Lantlr/CommonHiddenStreamToken;

    move-result-object v3

    invoke-virtual {v3}, Lantlr/CommonHiddenStreamToken;->getType()I

    move-result v3

    invoke-virtual {v2, v3}, Lantlr/collections/impl/BitSet;->member(I)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lantlr/TokenStreamHiddenTokenFilter;->discardMask:Lantlr/collections/impl/BitSet;

    invoke-virtual {p0, v4}, Lantlr/TokenStreamHiddenTokenFilter;->LA(I)Lantlr/CommonHiddenStreamToken;

    move-result-object v3

    invoke-virtual {v3}, Lantlr/CommonHiddenStreamToken;->getType()I

    move-result v3

    invoke-virtual {v2, v3}, Lantlr/collections/impl/BitSet;->member(I)Z

    move-result v2

    if-eqz v2, :cond_4

    :cond_1
    iget-object v2, p0, Lantlr/TokenStreamHiddenTokenFilter;->hideMask:Lantlr/collections/impl/BitSet;

    invoke-virtual {p0, v4}, Lantlr/TokenStreamHiddenTokenFilter;->LA(I)Lantlr/CommonHiddenStreamToken;

    move-result-object v3

    invoke-virtual {v3}, Lantlr/CommonHiddenStreamToken;->getType()I

    move-result v3

    invoke-virtual {v2, v3}, Lantlr/collections/impl/BitSet;->member(I)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {p0, v4}, Lantlr/TokenStreamHiddenTokenFilter;->LA(I)Lantlr/CommonHiddenStreamToken;

    move-result-object v2

    invoke-virtual {v0, v2}, Lantlr/CommonHiddenStreamToken;->setHiddenAfter(Lantlr/CommonHiddenStreamToken;)V

    if-eq v0, v1, :cond_2

    invoke-virtual {p0, v4}, Lantlr/TokenStreamHiddenTokenFilter;->LA(I)Lantlr/CommonHiddenStreamToken;

    move-result-object v2

    invoke-virtual {v2, v0}, Lantlr/CommonHiddenStreamToken;->setHiddenBefore(Lantlr/CommonHiddenStreamToken;)V

    :cond_2
    invoke-virtual {p0, v4}, Lantlr/TokenStreamHiddenTokenFilter;->LA(I)Lantlr/CommonHiddenStreamToken;

    move-result-object v0

    iput-object v0, p0, Lantlr/TokenStreamHiddenTokenFilter;->lastHiddenToken:Lantlr/CommonHiddenStreamToken;

    :cond_3
    invoke-virtual {p0}, Lantlr/TokenStreamHiddenTokenFilter;->consume()V

    goto :goto_0

    :cond_4
    return-object v1
.end method
