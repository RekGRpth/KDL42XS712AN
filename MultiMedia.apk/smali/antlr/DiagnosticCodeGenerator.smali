.class public Lantlr/DiagnosticCodeGenerator;
.super Lantlr/CodeGenerator;
.source "DiagnosticCodeGenerator.java"


# instance fields
.field protected doingLexRules:Z

.field protected syntacticPredLevel:I


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lantlr/CodeGenerator;-><init>()V

    iput v0, p0, Lantlr/DiagnosticCodeGenerator;->syntacticPredLevel:I

    iput-boolean v0, p0, Lantlr/DiagnosticCodeGenerator;->doingLexRules:Z

    new-instance v0, Lantlr/JavaCharFormatter;

    invoke-direct {v0}, Lantlr/JavaCharFormatter;-><init>()V

    iput-object v0, p0, Lantlr/DiagnosticCodeGenerator;->charFormatter:Lantlr/CharFormatter;

    return-void
.end method


# virtual methods
.method public gen()V
    .locals 3

    :try_start_0
    iget-object v0, p0, Lantlr/DiagnosticCodeGenerator;->behavior:Lantlr/DefineGrammarSymbols;

    iget-object v0, v0, Lantlr/DefineGrammarSymbols;->grammars:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->elements()Ljava/util/Enumeration;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lantlr/Grammar;

    iget-object v2, p0, Lantlr/DiagnosticCodeGenerator;->analyzer:Lantlr/LLkGrammarAnalyzer;

    invoke-virtual {v0, v2}, Lantlr/Grammar;->setGrammarAnalyzer(Lantlr/LLkGrammarAnalyzer;)V

    invoke-virtual {v0, p0}, Lantlr/Grammar;->setCodeGenerator(Lantlr/CodeGenerator;)V

    iget-object v2, p0, Lantlr/DiagnosticCodeGenerator;->analyzer:Lantlr/LLkGrammarAnalyzer;

    invoke-interface {v2, v0}, Lantlr/LLkGrammarAnalyzer;->setGrammar(Lantlr/Grammar;)V

    invoke-virtual {v0}, Lantlr/Grammar;->generate()V

    iget-object v0, p0, Lantlr/DiagnosticCodeGenerator;->antlrTool:Lantlr/Tool;

    invoke-virtual {v0}, Lantlr/Tool;->hasError()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lantlr/DiagnosticCodeGenerator;->antlrTool:Lantlr/Tool;

    const-string v2, "Exiting due to errors."

    invoke-virtual {v0, v2}, Lantlr/Tool;->panic(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    iget-object v1, p0, Lantlr/DiagnosticCodeGenerator;->antlrTool:Lantlr/Tool;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lantlr/Tool;->reportException(Ljava/lang/Exception;Ljava/lang/String;)V

    :cond_1
    return-void

    :cond_2
    :try_start_1
    iget-object v0, p0, Lantlr/DiagnosticCodeGenerator;->behavior:Lantlr/DefineGrammarSymbols;

    iget-object v0, v0, Lantlr/DefineGrammarSymbols;->tokenManagers:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->elements()Ljava/util/Enumeration;

    move-result-object v1

    :cond_3
    :goto_1
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lantlr/TokenManager;

    invoke-interface {v0}, Lantlr/TokenManager;->isReadOnly()Z

    move-result v2

    if-nez v2, :cond_3

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->genTokenTypes(Lantlr/TokenManager;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method public gen(Lantlr/ActionElement;)V
    .locals 1

    iget-boolean v0, p1, Lantlr/ActionElement;->isSemPred:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const-string v0, "ACTION: "

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->print(Ljava/lang/String;)V

    iget-object v0, p1, Lantlr/ActionElement;->actionText:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->_printAction(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public gen(Lantlr/AlternativeBlock;)V
    .locals 1

    const-string v0, "Start of alternative block."

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    iget v0, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    invoke-virtual {p0, p1}, Lantlr/DiagnosticCodeGenerator;->genBlockPreamble(Lantlr/AlternativeBlock;)V

    iget-object v0, p0, Lantlr/DiagnosticCodeGenerator;->grammar:Lantlr/Grammar;

    iget-object v0, v0, Lantlr/Grammar;->theLLkAnalyzer:Lantlr/LLkGrammarAnalyzer;

    invoke-interface {v0, p1}, Lantlr/LLkGrammarAnalyzer;->deterministic(Lantlr/AlternativeBlock;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Warning: This alternative block is non-deterministic"

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p0, p1}, Lantlr/DiagnosticCodeGenerator;->genCommonBlock(Lantlr/AlternativeBlock;)V

    iget v0, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    return-void
.end method

.method public gen(Lantlr/BlockEndElement;)V
    .locals 0

    return-void
.end method

.method public gen(Lantlr/CharLiteralElement;)V
    .locals 2

    const-string v0, "Match character "

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->print(Ljava/lang/String;)V

    iget-boolean v0, p1, Lantlr/CharLiteralElement;->not:Z

    if-eqz v0, :cond_0

    const-string v0, "NOT "

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->_print(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p1, Lantlr/CharLiteralElement;->atomText:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->_print(Ljava/lang/String;)V

    iget-object v0, p1, Lantlr/CharLiteralElement;->label:Ljava/lang/String;

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string v1, ", label="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-object v1, p1, Lantlr/CharLiteralElement;->label:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->_print(Ljava/lang/String;)V

    :cond_1
    const-string v0, ""

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->_println(Ljava/lang/String;)V

    return-void
.end method

.method public gen(Lantlr/CharRangeElement;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string v1, "Match character range: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-object v1, p1, Lantlr/CharRangeElement;->beginText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, ".."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-object v1, p1, Lantlr/CharRangeElement;->endText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->print(Ljava/lang/String;)V

    iget-object v0, p1, Lantlr/CharRangeElement;->label:Ljava/lang/String;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string v1, ", label = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-object v1, p1, Lantlr/CharRangeElement;->label:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->_print(Ljava/lang/String;)V

    :cond_0
    const-string v0, ""

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->_println(Ljava/lang/String;)V

    return-void
.end method

.method public gen(Lantlr/LexerGrammar;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v4, 0x0

    invoke-virtual {p0, p1}, Lantlr/DiagnosticCodeGenerator;->setGrammar(Lantlr/Grammar;)V

    iget-object v0, p0, Lantlr/DiagnosticCodeGenerator;->antlrTool:Lantlr/Tool;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "Generating "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p0, Lantlr/DiagnosticCodeGenerator;->grammar:Lantlr/Grammar;

    invoke-virtual {v2}, Lantlr/Grammar;->getClassName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    sget-object v2, Lantlr/DiagnosticCodeGenerator;->TokenTypesFileExt:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lantlr/Tool;->reportProgress(Ljava/lang/String;)V

    iget-object v0, p0, Lantlr/DiagnosticCodeGenerator;->antlrTool:Lantlr/Tool;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    iget-object v2, p0, Lantlr/DiagnosticCodeGenerator;->grammar:Lantlr/Grammar;

    invoke-virtual {v2}, Lantlr/Grammar;->getClassName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    sget-object v2, Lantlr/DiagnosticCodeGenerator;->TokenTypesFileExt:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lantlr/Tool;->openOutputFile(Ljava/lang/String;)Ljava/io/PrintWriter;

    move-result-object v0

    iput-object v0, p0, Lantlr/DiagnosticCodeGenerator;->currentOutput:Ljava/io/PrintWriter;

    iput v4, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lantlr/DiagnosticCodeGenerator;->doingLexRules:Z

    invoke-virtual {p0}, Lantlr/DiagnosticCodeGenerator;->genHeader()V

    const-string v0, ""

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    const-string v0, "*** Lexer Preamble Action."

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    const-string v0, "This action will appear before the declaration of your lexer class:"

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    iget v0, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    iget-object v0, p0, Lantlr/DiagnosticCodeGenerator;->grammar:Lantlr/Grammar;

    iget-object v0, v0, Lantlr/Grammar;->preambleAction:Lantlr/Token;

    invoke-virtual {v0}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    iget v0, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    const-string v0, "*** End of Lexer Preamble Action"

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    const-string v0, ""

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string v1, "*** Your lexer class is called \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-object v1, p0, Lantlr/DiagnosticCodeGenerator;->grammar:Lantlr/Grammar;

    invoke-virtual {v1}, Lantlr/Grammar;->getClassName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, "\' and is a subclass of \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-object v1, p0, Lantlr/DiagnosticCodeGenerator;->grammar:Lantlr/Grammar;

    invoke-virtual {v1}, Lantlr/Grammar;->getSuperClass()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, "\'."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    const-string v0, ""

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    const-string v0, "*** User-defined lexer  class members:"

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    const-string v0, "These are the member declarations that you defined for your class:"

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    iget v0, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    iget-object v0, p0, Lantlr/DiagnosticCodeGenerator;->grammar:Lantlr/Grammar;

    iget-object v0, v0, Lantlr/Grammar;->classMemberAction:Lantlr/Token;

    invoke-virtual {v0}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->printAction(Ljava/lang/String;)V

    iget v0, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    const-string v0, "*** End of user-defined lexer class members"

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    const-string v0, ""

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    const-string v0, "*** String literals used in the parser"

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    const-string v0, "The following string literals were used in the parser."

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    const-string v0, "An actual code generator would arrange to place these literals"

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    const-string v0, "into a table in the generated lexer, so that actions in the"

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    const-string v0, "generated lexer could match token text against the literals."

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    const-string v0, "String literals used in the lexer are not listed here, as they"

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    const-string v0, "are incorporated into the mainstream lexer processing."

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    iget v0, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    iget-object v0, p0, Lantlr/DiagnosticCodeGenerator;->grammar:Lantlr/Grammar;

    invoke-virtual {v0}, Lantlr/Grammar;->getSymbols()Ljava/util/Enumeration;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lantlr/GrammarSymbol;

    instance-of v2, v0, Lantlr/StringLiteralSymbol;

    if-eqz v2, :cond_0

    check-cast v0, Lantlr/StringLiteralSymbol;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v0}, Lantlr/StringLiteralSymbol;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, " = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v0}, Lantlr/StringLiteralSymbol;->getTokenType()I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    iget v0, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    const-string v0, "*** End of string literals used by the parser"

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    invoke-virtual {p0}, Lantlr/DiagnosticCodeGenerator;->genNextToken()V

    const-string v0, ""

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    const-string v0, "*** User-defined Lexer rules:"

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    iget v0, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    iget-object v0, p0, Lantlr/DiagnosticCodeGenerator;->grammar:Lantlr/Grammar;

    iget-object v0, v0, Lantlr/Grammar;->rules:Lantlr/collections/impl/Vector;

    invoke-virtual {v0}, Lantlr/collections/impl/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v1

    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lantlr/RuleSymbol;

    iget-object v2, v0, Lantlr/RuleSymbol;->id:Ljava/lang/String;

    const-string v3, "mnextToken"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->genRule(Lantlr/RuleSymbol;)V

    goto :goto_1

    :cond_3
    iget v0, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    const-string v0, ""

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    const-string v0, "*** End User-defined Lexer rules:"

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    iget-object v0, p0, Lantlr/DiagnosticCodeGenerator;->currentOutput:Ljava/io/PrintWriter;

    invoke-virtual {v0}, Ljava/io/PrintWriter;->close()V

    const/4 v0, 0x0

    iput-object v0, p0, Lantlr/DiagnosticCodeGenerator;->currentOutput:Ljava/io/PrintWriter;

    iput-boolean v4, p0, Lantlr/DiagnosticCodeGenerator;->doingLexRules:Z

    return-void
.end method

.method public gen(Lantlr/OneOrMoreBlock;)V
    .locals 1

    const-string v0, "Start ONE-OR-MORE (...)+ block:"

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    iget v0, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    invoke-virtual {p0, p1}, Lantlr/DiagnosticCodeGenerator;->genBlockPreamble(Lantlr/AlternativeBlock;)V

    iget-object v0, p0, Lantlr/DiagnosticCodeGenerator;->grammar:Lantlr/Grammar;

    iget-object v0, v0, Lantlr/Grammar;->theLLkAnalyzer:Lantlr/LLkGrammarAnalyzer;

    invoke-interface {v0, p1}, Lantlr/LLkGrammarAnalyzer;->deterministic(Lantlr/OneOrMoreBlock;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Warning: This one-or-more block is non-deterministic"

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p0, p1}, Lantlr/DiagnosticCodeGenerator;->genCommonBlock(Lantlr/AlternativeBlock;)V

    iget v0, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    const-string v0, "End ONE-OR-MORE block."

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    return-void
.end method

.method public gen(Lantlr/ParserGrammar;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lantlr/DiagnosticCodeGenerator;->setGrammar(Lantlr/Grammar;)V

    iget-object v0, p0, Lantlr/DiagnosticCodeGenerator;->antlrTool:Lantlr/Tool;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "Generating "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p0, Lantlr/DiagnosticCodeGenerator;->grammar:Lantlr/Grammar;

    invoke-virtual {v2}, Lantlr/Grammar;->getClassName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    sget-object v2, Lantlr/DiagnosticCodeGenerator;->TokenTypesFileExt:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lantlr/Tool;->reportProgress(Ljava/lang/String;)V

    iget-object v0, p0, Lantlr/DiagnosticCodeGenerator;->antlrTool:Lantlr/Tool;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    iget-object v2, p0, Lantlr/DiagnosticCodeGenerator;->grammar:Lantlr/Grammar;

    invoke-virtual {v2}, Lantlr/Grammar;->getClassName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    sget-object v2, Lantlr/DiagnosticCodeGenerator;->TokenTypesFileExt:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lantlr/Tool;->openOutputFile(Ljava/lang/String;)Ljava/io/PrintWriter;

    move-result-object v0

    iput-object v0, p0, Lantlr/DiagnosticCodeGenerator;->currentOutput:Ljava/io/PrintWriter;

    const/4 v0, 0x0

    iput v0, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    invoke-virtual {p0}, Lantlr/DiagnosticCodeGenerator;->genHeader()V

    const-string v0, ""

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    const-string v0, "*** Parser Preamble Action."

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    const-string v0, "This action will appear before the declaration of your parser class:"

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    iget v0, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    iget-object v0, p0, Lantlr/DiagnosticCodeGenerator;->grammar:Lantlr/Grammar;

    iget-object v0, v0, Lantlr/Grammar;->preambleAction:Lantlr/Token;

    invoke-virtual {v0}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    iget v0, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    const-string v0, "*** End of Parser Preamble Action"

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    const-string v0, ""

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string v1, "*** Your parser class is called \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-object v1, p0, Lantlr/DiagnosticCodeGenerator;->grammar:Lantlr/Grammar;

    invoke-virtual {v1}, Lantlr/Grammar;->getClassName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, "\' and is a subclass of \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-object v1, p0, Lantlr/DiagnosticCodeGenerator;->grammar:Lantlr/Grammar;

    invoke-virtual {v1}, Lantlr/Grammar;->getSuperClass()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, "\'."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    const-string v0, ""

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    const-string v0, "*** User-defined parser class members:"

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    const-string v0, "These are the member declarations that you defined for your class:"

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    iget v0, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    iget-object v0, p0, Lantlr/DiagnosticCodeGenerator;->grammar:Lantlr/Grammar;

    iget-object v0, v0, Lantlr/Grammar;->classMemberAction:Lantlr/Token;

    invoke-virtual {v0}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->printAction(Ljava/lang/String;)V

    iget v0, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    const-string v0, "*** End of user-defined parser class members"

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    const-string v0, ""

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    const-string v0, "*** Parser rules:"

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    iget v0, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    iget-object v0, p0, Lantlr/DiagnosticCodeGenerator;->grammar:Lantlr/Grammar;

    iget-object v0, v0, Lantlr/Grammar;->rules:Lantlr/collections/impl/Vector;

    invoke-virtual {v0}, Lantlr/collections/impl/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, ""

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lantlr/GrammarSymbol;

    instance-of v2, v0, Lantlr/RuleSymbol;

    if-eqz v2, :cond_0

    check-cast v0, Lantlr/RuleSymbol;

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->genRule(Lantlr/RuleSymbol;)V

    goto :goto_0

    :cond_1
    iget v0, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    const-string v0, ""

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    const-string v0, "*** End of parser rules"

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    const-string v0, ""

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    const-string v0, "*** End of parser"

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    iget-object v0, p0, Lantlr/DiagnosticCodeGenerator;->currentOutput:Ljava/io/PrintWriter;

    invoke-virtual {v0}, Ljava/io/PrintWriter;->close()V

    const/4 v0, 0x0

    iput-object v0, p0, Lantlr/DiagnosticCodeGenerator;->currentOutput:Ljava/io/PrintWriter;

    return-void
.end method

.method public gen(Lantlr/RuleRefElement;)V
    .locals 3

    iget-object v0, p0, Lantlr/DiagnosticCodeGenerator;->grammar:Lantlr/Grammar;

    iget-object v1, p1, Lantlr/RuleRefElement;->targetRule:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lantlr/Grammar;->getSymbol(Ljava/lang/String;)Lantlr/GrammarSymbol;

    move-result-object v0

    check-cast v0, Lantlr/RuleSymbol;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "Rule Reference: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p1, Lantlr/RuleRefElement;->targetRule:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lantlr/DiagnosticCodeGenerator;->print(Ljava/lang/String;)V

    iget-object v1, p1, Lantlr/RuleRefElement;->idAssign:Ljava/lang/String;

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, ", assigned to \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p1, Lantlr/RuleRefElement;->idAssign:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lantlr/DiagnosticCodeGenerator;->_print(Ljava/lang/String;)V

    :cond_0
    iget-object v1, p1, Lantlr/RuleRefElement;->args:Ljava/lang/String;

    if-eqz v1, :cond_1

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, ", arguments = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p1, Lantlr/RuleRefElement;->args:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lantlr/DiagnosticCodeGenerator;->_print(Ljava/lang/String;)V

    :cond_1
    const-string v1, ""

    invoke-virtual {p0, v1}, Lantlr/DiagnosticCodeGenerator;->_println(Ljava/lang/String;)V

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lantlr/RuleSymbol;->isDefined()Z

    move-result v1

    if-nez v1, :cond_4

    :cond_2
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string v1, "Rule \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-object v1, p1, Lantlr/RuleRefElement;->targetRule:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, "\' is referenced, but that rule is not defined."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    const-string v0, "\tPerhaps the rule is misspelled, or you forgot to define it."

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    :cond_3
    :goto_0
    return-void

    :cond_4
    instance-of v1, v0, Lantlr/RuleSymbol;

    if-nez v1, :cond_5

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string v1, "Rule \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-object v1, p1, Lantlr/RuleRefElement;->targetRule:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, "\' is referenced, but that is not a grammar rule."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    goto :goto_0

    :cond_5
    iget-object v1, p1, Lantlr/RuleRefElement;->idAssign:Ljava/lang/String;

    if-eqz v1, :cond_7

    iget-object v1, v0, Lantlr/RuleSymbol;->block:Lantlr/RuleBlock;

    iget-object v1, v1, Lantlr/RuleBlock;->returnAction:Ljava/lang/String;

    if-nez v1, :cond_6

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "Error: You assigned from Rule \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p1, Lantlr/RuleRefElement;->targetRule:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "\', but that rule has no return type."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    :cond_6
    :goto_1
    iget-object v1, p1, Lantlr/RuleRefElement;->args:Ljava/lang/String;

    if-eqz v1, :cond_3

    iget-object v0, v0, Lantlr/RuleSymbol;->block:Lantlr/RuleBlock;

    iget-object v0, v0, Lantlr/RuleBlock;->argAction:Ljava/lang/String;

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string v1, "Error: Rule \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-object v1, p1, Lantlr/RuleRefElement;->targetRule:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, "\' accepts no arguments."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    goto :goto_0

    :cond_7
    iget-object v1, p0, Lantlr/DiagnosticCodeGenerator;->grammar:Lantlr/Grammar;

    instance-of v1, v1, Lantlr/LexerGrammar;

    if-nez v1, :cond_6

    iget v1, p0, Lantlr/DiagnosticCodeGenerator;->syntacticPredLevel:I

    if-nez v1, :cond_6

    iget-object v1, v0, Lantlr/RuleSymbol;->block:Lantlr/RuleBlock;

    iget-object v1, v1, Lantlr/RuleBlock;->returnAction:Ljava/lang/String;

    if-eqz v1, :cond_6

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "Warning: Rule \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p1, Lantlr/RuleRefElement;->targetRule:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "\' returns a value"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public gen(Lantlr/StringLiteralElement;)V
    .locals 2

    const-string v0, "Match string literal "

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->print(Ljava/lang/String;)V

    iget-object v0, p1, Lantlr/StringLiteralElement;->atomText:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->_print(Ljava/lang/String;)V

    iget-object v0, p1, Lantlr/StringLiteralElement;->label:Ljava/lang/String;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string v1, ", label="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-object v1, p1, Lantlr/StringLiteralElement;->label:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->_print(Ljava/lang/String;)V

    :cond_0
    const-string v0, ""

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->_println(Ljava/lang/String;)V

    return-void
.end method

.method public gen(Lantlr/TokenRangeElement;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string v1, "Match token range: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-object v1, p1, Lantlr/TokenRangeElement;->beginText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, ".."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-object v1, p1, Lantlr/TokenRangeElement;->endText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->print(Ljava/lang/String;)V

    iget-object v0, p1, Lantlr/TokenRangeElement;->label:Ljava/lang/String;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string v1, ", label = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-object v1, p1, Lantlr/TokenRangeElement;->label:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->_print(Ljava/lang/String;)V

    :cond_0
    const-string v0, ""

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->_println(Ljava/lang/String;)V

    return-void
.end method

.method public gen(Lantlr/TokenRefElement;)V
    .locals 2

    const-string v0, "Match token "

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->print(Ljava/lang/String;)V

    iget-boolean v0, p1, Lantlr/TokenRefElement;->not:Z

    if-eqz v0, :cond_0

    const-string v0, "NOT "

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->_print(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p1, Lantlr/TokenRefElement;->atomText:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->_print(Ljava/lang/String;)V

    iget-object v0, p1, Lantlr/TokenRefElement;->label:Ljava/lang/String;

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string v1, ", label="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-object v1, p1, Lantlr/TokenRefElement;->label:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->_print(Ljava/lang/String;)V

    :cond_1
    const-string v0, ""

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->_println(Ljava/lang/String;)V

    return-void
.end method

.method public gen(Lantlr/TreeElement;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string v1, "Tree reference: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->print(Ljava/lang/String;)V

    return-void
.end method

.method public gen(Lantlr/TreeWalkerGrammar;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lantlr/DiagnosticCodeGenerator;->setGrammar(Lantlr/Grammar;)V

    iget-object v0, p0, Lantlr/DiagnosticCodeGenerator;->antlrTool:Lantlr/Tool;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "Generating "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p0, Lantlr/DiagnosticCodeGenerator;->grammar:Lantlr/Grammar;

    invoke-virtual {v2}, Lantlr/Grammar;->getClassName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    sget-object v2, Lantlr/DiagnosticCodeGenerator;->TokenTypesFileExt:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lantlr/Tool;->reportProgress(Ljava/lang/String;)V

    iget-object v0, p0, Lantlr/DiagnosticCodeGenerator;->antlrTool:Lantlr/Tool;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    iget-object v2, p0, Lantlr/DiagnosticCodeGenerator;->grammar:Lantlr/Grammar;

    invoke-virtual {v2}, Lantlr/Grammar;->getClassName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    sget-object v2, Lantlr/DiagnosticCodeGenerator;->TokenTypesFileExt:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lantlr/Tool;->openOutputFile(Ljava/lang/String;)Ljava/io/PrintWriter;

    move-result-object v0

    iput-object v0, p0, Lantlr/DiagnosticCodeGenerator;->currentOutput:Ljava/io/PrintWriter;

    const/4 v0, 0x0

    iput v0, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    invoke-virtual {p0}, Lantlr/DiagnosticCodeGenerator;->genHeader()V

    const-string v0, ""

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    const-string v0, "*** Tree-walker Preamble Action."

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    const-string v0, "This action will appear before the declaration of your tree-walker class:"

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    iget v0, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    iget-object v0, p0, Lantlr/DiagnosticCodeGenerator;->grammar:Lantlr/Grammar;

    iget-object v0, v0, Lantlr/Grammar;->preambleAction:Lantlr/Token;

    invoke-virtual {v0}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    iget v0, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    const-string v0, "*** End of tree-walker Preamble Action"

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    const-string v0, ""

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string v1, "*** Your tree-walker class is called \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-object v1, p0, Lantlr/DiagnosticCodeGenerator;->grammar:Lantlr/Grammar;

    invoke-virtual {v1}, Lantlr/Grammar;->getClassName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, "\' and is a subclass of \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-object v1, p0, Lantlr/DiagnosticCodeGenerator;->grammar:Lantlr/Grammar;

    invoke-virtual {v1}, Lantlr/Grammar;->getSuperClass()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, "\'."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    const-string v0, ""

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    const-string v0, "*** User-defined tree-walker class members:"

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    const-string v0, "These are the member declarations that you defined for your class:"

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    iget v0, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    iget-object v0, p0, Lantlr/DiagnosticCodeGenerator;->grammar:Lantlr/Grammar;

    iget-object v0, v0, Lantlr/Grammar;->classMemberAction:Lantlr/Token;

    invoke-virtual {v0}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->printAction(Ljava/lang/String;)V

    iget v0, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    const-string v0, "*** End of user-defined tree-walker class members"

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    const-string v0, ""

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    const-string v0, "*** tree-walker rules:"

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    iget v0, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    iget-object v0, p0, Lantlr/DiagnosticCodeGenerator;->grammar:Lantlr/Grammar;

    iget-object v0, v0, Lantlr/Grammar;->rules:Lantlr/collections/impl/Vector;

    invoke-virtual {v0}, Lantlr/collections/impl/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, ""

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lantlr/GrammarSymbol;

    instance-of v2, v0, Lantlr/RuleSymbol;

    if-eqz v2, :cond_0

    check-cast v0, Lantlr/RuleSymbol;

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->genRule(Lantlr/RuleSymbol;)V

    goto :goto_0

    :cond_1
    iget v0, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    const-string v0, ""

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    const-string v0, "*** End of tree-walker rules"

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    const-string v0, ""

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    const-string v0, "*** End of tree-walker"

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    iget-object v0, p0, Lantlr/DiagnosticCodeGenerator;->currentOutput:Ljava/io/PrintWriter;

    invoke-virtual {v0}, Ljava/io/PrintWriter;->close()V

    const/4 v0, 0x0

    iput-object v0, p0, Lantlr/DiagnosticCodeGenerator;->currentOutput:Ljava/io/PrintWriter;

    return-void
.end method

.method public gen(Lantlr/WildcardElement;)V
    .locals 2

    const-string v0, "Match wildcard"

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->print(Ljava/lang/String;)V

    invoke-virtual {p1}, Lantlr/WildcardElement;->getLabel()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string v1, ", label = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {p1}, Lantlr/WildcardElement;->getLabel()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->_print(Ljava/lang/String;)V

    :cond_0
    const-string v0, ""

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->_println(Ljava/lang/String;)V

    return-void
.end method

.method public gen(Lantlr/ZeroOrMoreBlock;)V
    .locals 1

    const-string v0, "Start ZERO-OR-MORE (...)+ block:"

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    iget v0, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    invoke-virtual {p0, p1}, Lantlr/DiagnosticCodeGenerator;->genBlockPreamble(Lantlr/AlternativeBlock;)V

    iget-object v0, p0, Lantlr/DiagnosticCodeGenerator;->grammar:Lantlr/Grammar;

    iget-object v0, v0, Lantlr/Grammar;->theLLkAnalyzer:Lantlr/LLkGrammarAnalyzer;

    invoke-interface {v0, p1}, Lantlr/LLkGrammarAnalyzer;->deterministic(Lantlr/ZeroOrMoreBlock;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Warning: This zero-or-more block is non-deterministic"

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p0, p1}, Lantlr/DiagnosticCodeGenerator;->genCommonBlock(Lantlr/AlternativeBlock;)V

    iget v0, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    const-string v0, "End ZERO-OR-MORE block."

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    return-void
.end method

.method protected genAlt(Lantlr/Alternative;)V
    .locals 2

    iget-object v0, p1, Lantlr/Alternative;->head:Lantlr/AlternativeElement;

    :goto_0
    instance-of v1, v0, Lantlr/BlockEndElement;

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lantlr/AlternativeElement;->generate()V

    iget-object v0, v0, Lantlr/AlternativeElement;->next:Lantlr/AlternativeElement;

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Lantlr/Alternative;->getTreeSpecifier()Lantlr/Token;

    move-result-object v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string v1, "AST will be built as: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {p1}, Lantlr/Alternative;->getTreeSpecifier()Lantlr/Token;

    move-result-object v1

    invoke-virtual {v1}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method protected genBlockPreamble(Lantlr/AlternativeBlock;)V
    .locals 2

    iget-object v0, p1, Lantlr/AlternativeBlock;->initAction:Ljava/lang/String;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string v1, "Init action: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-object v1, p1, Lantlr/AlternativeBlock;->initAction:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->printAction(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public genCommonBlock(Lantlr/AlternativeBlock;)V
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p1, Lantlr/AlternativeBlock;->alternatives:Lantlr/collections/impl/Vector;

    invoke-virtual {v2}, Lantlr/collections/impl/Vector;->size()I

    move-result v2

    if-ne v2, v0, :cond_4

    :goto_0
    const-string v2, "Start of an alternative block."

    invoke-virtual {p0, v2}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    iget v2, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    const-string v2, "The lookahead set for this block is:"

    invoke-virtual {p0, v2}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    iget v2, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    invoke-virtual {p0, p1}, Lantlr/DiagnosticCodeGenerator;->genLookaheadSetForBlock(Lantlr/AlternativeBlock;)V

    iget v2, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    if-eqz v0, :cond_5

    const-string v2, "This block has a single alternative"

    invoke-virtual {p0, v2}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    invoke-virtual {p1, v1}, Lantlr/AlternativeBlock;->getAlternativeAt(I)Lantlr/Alternative;

    move-result-object v2

    iget-object v2, v2, Lantlr/Alternative;->synPred:Lantlr/SynPredBlock;

    if-eqz v2, :cond_0

    const-string v2, "Warning: you specified a syntactic predicate for this alternative,"

    invoke-virtual {p0, v2}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    const-string v2, "and it is the only alternative of a block and will be ignored."

    invoke-virtual {p0, v2}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    :cond_0
    :goto_1
    iget-object v2, p1, Lantlr/AlternativeBlock;->alternatives:Lantlr/collections/impl/Vector;

    invoke-virtual {v2}, Lantlr/collections/impl/Vector;->size()I

    move-result v2

    if-ge v1, v2, :cond_9

    invoke-virtual {p1, v1}, Lantlr/AlternativeBlock;->getAlternativeAt(I)Lantlr/Alternative;

    move-result-object v2

    iget-object v3, v2, Lantlr/Alternative;->head:Lantlr/AlternativeElement;

    const-string v3, ""

    invoke-virtual {p0, v3}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    if-eqz v1, :cond_6

    const-string v3, "Otherwise, "

    invoke-virtual {p0, v3}, Lantlr/DiagnosticCodeGenerator;->print(Ljava/lang/String;)V

    :goto_2
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string v4, "Alternate("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    add-int/lit8 v4, v1, 0x1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string v4, ") will be taken IF:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lantlr/DiagnosticCodeGenerator;->_println(Ljava/lang/String;)V

    const-string v3, "The lookahead set: "

    invoke-virtual {p0, v3}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    iget v3, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    invoke-virtual {p0, v2}, Lantlr/DiagnosticCodeGenerator;->genLookaheadSetForAlt(Lantlr/Alternative;)V

    iget v3, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    iget-object v3, v2, Lantlr/Alternative;->semPred:Ljava/lang/String;

    if-nez v3, :cond_1

    iget-object v3, v2, Lantlr/Alternative;->synPred:Lantlr/SynPredBlock;

    if-eqz v3, :cond_7

    :cond_1
    const-string v3, "is matched, AND "

    invoke-virtual {p0, v3}, Lantlr/DiagnosticCodeGenerator;->print(Ljava/lang/String;)V

    :goto_3
    iget-object v3, v2, Lantlr/Alternative;->semPred:Ljava/lang/String;

    if-eqz v3, :cond_2

    const-string v3, "the semantic predicate:"

    invoke-virtual {p0, v3}, Lantlr/DiagnosticCodeGenerator;->_println(Ljava/lang/String;)V

    iget v3, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    iget-object v3, v2, Lantlr/Alternative;->semPred:Ljava/lang/String;

    invoke-virtual {p0, v3}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    iget-object v3, v2, Lantlr/Alternative;->synPred:Lantlr/SynPredBlock;

    if-eqz v3, :cond_8

    const-string v3, "is true, AND "

    invoke-virtual {p0, v3}, Lantlr/DiagnosticCodeGenerator;->print(Ljava/lang/String;)V

    :cond_2
    :goto_4
    iget-object v3, v2, Lantlr/Alternative;->synPred:Lantlr/SynPredBlock;

    if-eqz v3, :cond_3

    const-string v3, "the syntactic predicate:"

    invoke-virtual {p0, v3}, Lantlr/DiagnosticCodeGenerator;->_println(Ljava/lang/String;)V

    iget v3, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    iget-object v3, v2, Lantlr/Alternative;->synPred:Lantlr/SynPredBlock;

    invoke-virtual {p0, v3}, Lantlr/DiagnosticCodeGenerator;->genSynPred(Lantlr/SynPredBlock;)V

    iget v3, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    const-string v3, "is matched."

    invoke-virtual {p0, v3}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    :cond_3
    invoke-virtual {p0, v2}, Lantlr/DiagnosticCodeGenerator;->genAlt(Lantlr/Alternative;)V

    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_1

    :cond_4
    move v0, v1

    goto/16 :goto_0

    :cond_5
    const-string v2, "This block has multiple alternatives:"

    invoke-virtual {p0, v2}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    iget v2, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    goto/16 :goto_1

    :cond_6
    const-string v3, ""

    invoke-virtual {p0, v3}, Lantlr/DiagnosticCodeGenerator;->print(Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_7
    const-string v3, "is matched."

    invoke-virtual {p0, v3}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    goto :goto_3

    :cond_8
    const-string v3, "is true."

    invoke-virtual {p0, v3}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    goto :goto_4

    :cond_9
    const-string v1, ""

    invoke-virtual {p0, v1}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    const-string v1, "OTHERWISE, a NoViableAlt exception will be thrown"

    invoke-virtual {p0, v1}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    const-string v1, ""

    invoke-virtual {p0, v1}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    if-nez v0, :cond_a

    iget v0, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    const-string v0, "End of alternatives"

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    :cond_a
    iget v0, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    const-string v0, "End of alternative block."

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    return-void
.end method

.method public genFollowSetForRuleBlock(Lantlr/RuleBlock;)V
    .locals 3

    const/4 v2, 0x1

    iget-object v0, p0, Lantlr/DiagnosticCodeGenerator;->grammar:Lantlr/Grammar;

    iget-object v0, v0, Lantlr/Grammar;->theLLkAnalyzer:Lantlr/LLkGrammarAnalyzer;

    iget-object v1, p1, Lantlr/RuleBlock;->endNode:Lantlr/RuleEndElement;

    invoke-interface {v0, v2, v1}, Lantlr/LLkGrammarAnalyzer;->FOLLOW(ILantlr/RuleEndElement;)Lantlr/Lookahead;

    move-result-object v0

    iget-object v1, p0, Lantlr/DiagnosticCodeGenerator;->grammar:Lantlr/Grammar;

    iget v1, v1, Lantlr/Grammar;->maxk:I

    invoke-virtual {p0, v1, v2, v0}, Lantlr/DiagnosticCodeGenerator;->printSet(IILantlr/Lookahead;)V

    return-void
.end method

.method protected genHeader()V
    .locals 2

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string v1, "ANTLR-generated file resulting from grammar "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-object v1, p0, Lantlr/DiagnosticCodeGenerator;->antlrTool:Lantlr/Tool;

    iget-object v1, v1, Lantlr/Tool;->grammarFile:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    const-string v0, "Diagnostic output"

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    const-string v0, ""

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    const-string v0, "Terence Parr, MageLang Institute"

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    const-string v0, "with John Lilley, Empathy Software"

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string v1, "ANTLR Version "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-object v1, p0, Lantlr/DiagnosticCodeGenerator;->antlrTool:Lantlr/Tool;

    sget-object v1, Lantlr/Tool;->version:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, "; 1996,1997"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    const-string v0, ""

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    const-string v0, "*** Header Action."

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    const-string v0, "This action will appear at the top of all generated files."

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    iget v0, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    iget-object v0, p0, Lantlr/DiagnosticCodeGenerator;->behavior:Lantlr/DefineGrammarSymbols;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lantlr/DefineGrammarSymbols;->getHeaderAction(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->printAction(Ljava/lang/String;)V

    iget v0, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    const-string v0, "*** End of Header Action"

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    const-string v0, ""

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    return-void
.end method

.method protected genLookaheadSetForAlt(Lantlr/Alternative;)V
    .locals 3

    const/4 v1, 0x1

    iget-boolean v0, p0, Lantlr/DiagnosticCodeGenerator;->doingLexRules:Z

    if-eqz v0, :cond_1

    iget-object v0, p1, Lantlr/Alternative;->cache:[Lantlr/Lookahead;

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lantlr/Lookahead;->containsEpsilon()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "MATCHES ALL"

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    :cond_0
    return-void

    :cond_1
    iget v0, p1, Lantlr/Alternative;->lookaheadDepth:I

    const v2, 0x7fffffff

    if-ne v0, v2, :cond_2

    iget-object v0, p0, Lantlr/DiagnosticCodeGenerator;->grammar:Lantlr/Grammar;

    iget v0, v0, Lantlr/Grammar;->maxk:I

    :cond_2
    :goto_0
    if-gt v1, v0, :cond_0

    iget-object v2, p1, Lantlr/Alternative;->cache:[Lantlr/Lookahead;

    aget-object v2, v2, v1

    invoke-virtual {p0, v0, v1, v2}, Lantlr/DiagnosticCodeGenerator;->printSet(IILantlr/Lookahead;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public genLookaheadSetForBlock(Lantlr/AlternativeBlock;)V
    .locals 5

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v2, p1, Lantlr/AlternativeBlock;->alternatives:Lantlr/collections/impl/Vector;

    invoke-virtual {v2}, Lantlr/collections/impl/Vector;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    invoke-virtual {p1, v0}, Lantlr/AlternativeBlock;->getAlternativeAt(I)Lantlr/Alternative;

    move-result-object v2

    iget v3, v2, Lantlr/Alternative;->lookaheadDepth:I

    const v4, 0x7fffffff

    if-ne v3, v4, :cond_1

    iget-object v0, p0, Lantlr/DiagnosticCodeGenerator;->grammar:Lantlr/Grammar;

    iget v1, v0, Lantlr/Grammar;->maxk:I

    :cond_0
    const/4 v0, 0x1

    :goto_1
    if-gt v0, v1, :cond_3

    iget-object v2, p0, Lantlr/DiagnosticCodeGenerator;->grammar:Lantlr/Grammar;

    iget-object v2, v2, Lantlr/Grammar;->theLLkAnalyzer:Lantlr/LLkGrammarAnalyzer;

    invoke-interface {v2, v0, p1}, Lantlr/LLkGrammarAnalyzer;->look(ILantlr/AlternativeBlock;)Lantlr/Lookahead;

    move-result-object v2

    invoke-virtual {p0, v1, v0, v2}, Lantlr/DiagnosticCodeGenerator;->printSet(IILantlr/Lookahead;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    iget v3, v2, Lantlr/Alternative;->lookaheadDepth:I

    if-ge v1, v3, :cond_2

    iget v1, v2, Lantlr/Alternative;->lookaheadDepth:I

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    return-void
.end method

.method public genNextToken()V
    .locals 3

    const-string v0, ""

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    const-string v0, "*** Lexer nextToken rule:"

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    const-string v0, "The lexer nextToken rule is synthesized from all of the user-defined"

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    const-string v0, "lexer rules.  It logically consists of one big alternative block with"

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    const-string v0, "each user-defined rule being an alternative."

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    const-string v0, ""

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    iget-object v0, p0, Lantlr/DiagnosticCodeGenerator;->grammar:Lantlr/Grammar;

    iget-object v1, p0, Lantlr/DiagnosticCodeGenerator;->grammar:Lantlr/Grammar;

    iget-object v1, v1, Lantlr/Grammar;->rules:Lantlr/collections/impl/Vector;

    const-string v2, "nextToken"

    invoke-static {v0, v1, v2}, Lantlr/MakeGrammar;->createNextTokenRule(Lantlr/Grammar;Lantlr/collections/impl/Vector;Ljava/lang/String;)Lantlr/RuleBlock;

    move-result-object v0

    new-instance v1, Lantlr/RuleSymbol;

    const-string v2, "mnextToken"

    invoke-direct {v1, v2}, Lantlr/RuleSymbol;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lantlr/RuleSymbol;->setDefined()V

    invoke-virtual {v1, v0}, Lantlr/RuleSymbol;->setBlock(Lantlr/RuleBlock;)V

    const-string v2, "private"

    iput-object v2, v1, Lantlr/RuleSymbol;->access:Ljava/lang/String;

    iget-object v2, p0, Lantlr/DiagnosticCodeGenerator;->grammar:Lantlr/Grammar;

    invoke-virtual {v2, v1}, Lantlr/Grammar;->define(Lantlr/RuleSymbol;)V

    iget-object v1, p0, Lantlr/DiagnosticCodeGenerator;->grammar:Lantlr/Grammar;

    iget-object v1, v1, Lantlr/Grammar;->theLLkAnalyzer:Lantlr/LLkGrammarAnalyzer;

    invoke-interface {v1, v0}, Lantlr/LLkGrammarAnalyzer;->deterministic(Lantlr/AlternativeBlock;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "The grammar analyzer has determined that the synthesized"

    invoke-virtual {p0, v1}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    const-string v1, "nextToken rule is non-deterministic (i.e., it has ambiguities)"

    invoke-virtual {p0, v1}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    const-string v1, "This means that there is some overlap of the character"

    invoke-virtual {p0, v1}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    const-string v1, "lookahead for two or more of your lexer rules."

    invoke-virtual {p0, v1}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->genCommonBlock(Lantlr/AlternativeBlock;)V

    const-string v0, "*** End of nextToken lexer rule."

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    return-void
.end method

.method public genRule(Lantlr/RuleSymbol;)V
    .locals 7

    const-string v0, ""

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    iget-boolean v0, p0, Lantlr/DiagnosticCodeGenerator;->doingLexRules:Z

    if-eqz v0, :cond_0

    const-string v0, "Lexer"

    move-object v1, v0

    :goto_0
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "*** "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v2, " Rule: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {p1}, Lantlr/RuleSymbol;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    invoke-virtual {p1}, Lantlr/RuleSymbol;->isDefined()Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "This rule is undefined."

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    const-string v0, "This means that the rule was referenced somewhere in the grammar,"

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    const-string v0, "but a definition for the rule was not encountered."

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    const-string v0, "It is also possible that syntax errors during the parse of"

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    const-string v0, "your grammar file prevented correct processing of the rule."

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "*** End "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, " Rule: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {p1}, Lantlr/RuleSymbol;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    :goto_1
    return-void

    :cond_0
    const-string v0, "Parser"

    move-object v1, v0

    goto :goto_0

    :cond_1
    iget v0, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    iget-object v0, p1, Lantlr/RuleSymbol;->access:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "Access: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-object v2, p1, Lantlr/RuleSymbol;->access:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {p1}, Lantlr/RuleSymbol;->getBlock()Lantlr/RuleBlock;

    move-result-object v3

    iget-object v0, v3, Lantlr/RuleBlock;->returnAction:Ljava/lang/String;

    if-eqz v0, :cond_7

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "Return value(s): "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-object v2, v3, Lantlr/RuleBlock;->returnAction:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    iget-boolean v0, p0, Lantlr/DiagnosticCodeGenerator;->doingLexRules:Z

    if-eqz v0, :cond_3

    const-string v0, "Error: you specified return value(s) for a lexical rule."

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    const-string v0, "\tLexical rules have an implicit return type of \'int\'."

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    :cond_3
    :goto_2
    iget-object v0, v3, Lantlr/RuleBlock;->argAction:Ljava/lang/String;

    if-eqz v0, :cond_4

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "Arguments: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-object v2, v3, Lantlr/RuleBlock;->argAction:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    :cond_4
    invoke-virtual {p0, v3}, Lantlr/DiagnosticCodeGenerator;->genBlockPreamble(Lantlr/AlternativeBlock;)V

    iget-object v0, p0, Lantlr/DiagnosticCodeGenerator;->grammar:Lantlr/Grammar;

    iget-object v0, v0, Lantlr/Grammar;->theLLkAnalyzer:Lantlr/LLkGrammarAnalyzer;

    invoke-interface {v0, v3}, Lantlr/LLkGrammarAnalyzer;->deterministic(Lantlr/AlternativeBlock;)Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "Error: This rule is non-deterministic"

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    :cond_5
    invoke-virtual {p0, v3}, Lantlr/DiagnosticCodeGenerator;->genCommonBlock(Lantlr/AlternativeBlock;)V

    const-string v0, ""

    invoke-virtual {v3, v0}, Lantlr/RuleBlock;->findExceptionSpec(Ljava/lang/String;)Lantlr/ExceptionSpec;

    move-result-object v4

    if-eqz v4, :cond_c

    const-string v0, "You specified error-handler(s) for this rule:"

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    iget v0, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    const/4 v0, 0x0

    move v2, v0

    :goto_3
    iget-object v0, v4, Lantlr/ExceptionSpec;->handlers:Lantlr/collections/impl/Vector;

    invoke-virtual {v0}, Lantlr/collections/impl/Vector;->size()I

    move-result v0

    if-ge v2, v0, :cond_9

    if-eqz v2, :cond_6

    const-string v0, ""

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    :cond_6
    iget-object v0, v4, Lantlr/ExceptionSpec;->handlers:Lantlr/collections/impl/Vector;

    invoke-virtual {v0, v2}, Lantlr/collections/impl/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lantlr/ExceptionHandler;

    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    const-string v6, "Error-handler("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    add-int/lit8 v6, v2, 0x1

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string v6, ") catches ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    iget-object v6, v0, Lantlr/ExceptionHandler;->exceptionTypeAndName:Lantlr/Token;

    invoke-virtual {v6}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    const-string v6, "] and executes:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    iget-object v0, v0, Lantlr/ExceptionHandler;->action:Lantlr/Token;

    invoke-virtual {v0}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->printAction(Ljava/lang/String;)V

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    :cond_7
    iget-boolean v0, p0, Lantlr/DiagnosticCodeGenerator;->doingLexRules:Z

    if-eqz v0, :cond_8

    const-string v0, "Return value: lexical rule returns an implicit token type"

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_8
    const-string v0, "Return value: none"

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_9
    iget v0, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    const-string v0, "End error-handlers."

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    :cond_a
    :goto_4
    iget-boolean v0, p0, Lantlr/DiagnosticCodeGenerator;->doingLexRules:Z

    if-nez v0, :cond_b

    const-string v0, "The follow set for this rule is:"

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    iget v0, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    invoke-virtual {p0, v3}, Lantlr/DiagnosticCodeGenerator;->genFollowSetForRuleBlock(Lantlr/RuleBlock;)V

    iget v0, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    :cond_b
    iget v0, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "*** End "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, " Rule: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {p1}, Lantlr/RuleSymbol;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_c
    iget-boolean v0, p0, Lantlr/DiagnosticCodeGenerator;->doingLexRules:Z

    if-nez v0, :cond_a

    const-string v0, "Default error-handling will be generated, which catches all"

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    const-string v0, "parser exceptions and consumes tokens until the follow-set is seen."

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    goto :goto_4
.end method

.method protected genSynPred(Lantlr/SynPredBlock;)V
    .locals 1

    iget v0, p0, Lantlr/DiagnosticCodeGenerator;->syntacticPredLevel:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lantlr/DiagnosticCodeGenerator;->syntacticPredLevel:I

    invoke-virtual {p0, p1}, Lantlr/DiagnosticCodeGenerator;->gen(Lantlr/AlternativeBlock;)V

    iget v0, p0, Lantlr/DiagnosticCodeGenerator;->syntacticPredLevel:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lantlr/DiagnosticCodeGenerator;->syntacticPredLevel:I

    return-void
.end method

.method protected genTokenTypes(Lantlr/TokenManager;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v0, p0, Lantlr/DiagnosticCodeGenerator;->antlrTool:Lantlr/Tool;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "Generating "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-interface {p1}, Lantlr/TokenManager;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    sget-object v2, Lantlr/DiagnosticCodeGenerator;->TokenTypesFileSuffix:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    sget-object v2, Lantlr/DiagnosticCodeGenerator;->TokenTypesFileExt:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lantlr/Tool;->reportProgress(Ljava/lang/String;)V

    iget-object v0, p0, Lantlr/DiagnosticCodeGenerator;->antlrTool:Lantlr/Tool;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    invoke-interface {p1}, Lantlr/TokenManager;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    sget-object v2, Lantlr/DiagnosticCodeGenerator;->TokenTypesFileSuffix:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    sget-object v2, Lantlr/DiagnosticCodeGenerator;->TokenTypesFileExt:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lantlr/Tool;->openOutputFile(Ljava/lang/String;)Ljava/io/PrintWriter;

    move-result-object v0

    iput-object v0, p0, Lantlr/DiagnosticCodeGenerator;->currentOutput:Ljava/io/PrintWriter;

    const/4 v0, 0x0

    iput v0, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    invoke-virtual {p0}, Lantlr/DiagnosticCodeGenerator;->genHeader()V

    const-string v0, ""

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    const-string v0, "*** Tokens used by the parser"

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    const-string v0, "This is a list of the token numeric values and the corresponding"

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    const-string v0, "token identifiers.  Some tokens are literals, and because of that"

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    const-string v0, "they have no identifiers.  Literals are double-quoted."

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    iget v0, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    invoke-interface {p1}, Lantlr/TokenManager;->getVocabulary()Lantlr/collections/impl/Vector;

    move-result-object v2

    const/4 v0, 0x4

    move v1, v0

    :goto_0
    invoke-virtual {v2}, Lantlr/collections/impl/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    invoke-virtual {v2, v1}, Lantlr/collections/impl/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v0, :cond_0

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v3, " = "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    iget v0, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    const-string v0, "*** End of tokens used by the parser"

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->println(Ljava/lang/String;)V

    iget-object v0, p0, Lantlr/DiagnosticCodeGenerator;->currentOutput:Ljava/io/PrintWriter;

    invoke-virtual {v0}, Ljava/io/PrintWriter;->close()V

    const/4 v0, 0x0

    iput-object v0, p0, Lantlr/DiagnosticCodeGenerator;->currentOutput:Ljava/io/PrintWriter;

    return-void
.end method

.method public getASTCreateString(Lantlr/GrammarAtom;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getASTCreateString(Lantlr/collections/impl/Vector;)Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string v1, "***Create an AST from a vector here***"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, "line.separator"

    invoke-static {v1}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public mapTreeId(Ljava/lang/String;Lantlr/ActionTransInfo;)Ljava/lang/String;
    .locals 0

    return-object p1
.end method

.method public printSet(IILantlr/Lookahead;)V
    .locals 7

    const/4 v2, 0x0

    const/4 v4, 0x5

    iget-object v0, p3, Lantlr/Lookahead;->fset:Lantlr/collections/impl/BitSet;

    invoke-virtual {v0}, Lantlr/collections/impl/BitSet;->toArray()[I

    move-result-object v5

    const/4 v0, 0x1

    if-eq p1, v0, :cond_3

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string v1, "k=="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, ": {"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->print(Ljava/lang/String;)V

    :goto_0
    array-length v0, v5

    if-le v0, v4, :cond_0

    const-string v0, ""

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->_println(Ljava/lang/String;)V

    iget v0, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    const-string v0, ""

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->print(Ljava/lang/String;)V

    :cond_0
    move v1, v2

    move v0, v2

    :goto_1
    array-length v3, v5

    if-ge v1, v3, :cond_5

    add-int/lit8 v3, v0, 0x1

    if-le v3, v4, :cond_1

    const-string v0, ""

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->_println(Ljava/lang/String;)V

    const-string v0, ""

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->print(Ljava/lang/String;)V

    move v3, v2

    :cond_1
    iget-boolean v0, p0, Lantlr/DiagnosticCodeGenerator;->doingLexRules:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lantlr/DiagnosticCodeGenerator;->charFormatter:Lantlr/CharFormatter;

    aget v6, v5, v1

    invoke-interface {v0, v6}, Lantlr/CharFormatter;->literalChar(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->_print(Ljava/lang/String;)V

    :goto_2
    array-length v0, v5

    add-int/lit8 v0, v0, -0x1

    if-eq v1, v0, :cond_2

    const-string v0, ", "

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->_print(Ljava/lang/String;)V

    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v3

    goto :goto_1

    :cond_3
    const-string v0, "{ "

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->print(Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lantlr/DiagnosticCodeGenerator;->grammar:Lantlr/Grammar;

    iget-object v0, v0, Lantlr/Grammar;->tokenManager:Lantlr/TokenManager;

    invoke-interface {v0}, Lantlr/TokenManager;->getVocabulary()Lantlr/collections/impl/Vector;

    move-result-object v0

    aget v6, v5, v1

    invoke-virtual {v0, v6}, Lantlr/collections/impl/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->_print(Ljava/lang/String;)V

    goto :goto_2

    :cond_5
    array-length v0, v5

    if-le v0, v4, :cond_6

    const-string v0, ""

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->_println(Ljava/lang/String;)V

    iget v0, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lantlr/DiagnosticCodeGenerator;->tabs:I

    const-string v0, ""

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->print(Ljava/lang/String;)V

    :cond_6
    const-string v0, " }"

    invoke-virtual {p0, v0}, Lantlr/DiagnosticCodeGenerator;->_println(Ljava/lang/String;)V

    return-void
.end method

.method protected processActionForSpecialSymbols(Ljava/lang/String;ILantlr/RuleBlock;Lantlr/ActionTransInfo;)Ljava/lang/String;
    .locals 0

    return-object p1
.end method
