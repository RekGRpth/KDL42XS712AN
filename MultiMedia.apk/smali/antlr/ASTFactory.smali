.class public Lantlr/ASTFactory;
.super Ljava/lang/Object;
.source "ASTFactory.java"


# static fields
.field static class$antlr$CommonAST:Ljava/lang/Class;

.field static class$antlr$Token:Ljava/lang/Class;


# instance fields
.field protected theASTNodeType:Ljava/lang/String;

.field protected theASTNodeTypeClass:Ljava/lang/Class;

.field protected tokenTypeToASTClassMap:Ljava/util/Hashtable;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lantlr/ASTFactory;->theASTNodeType:Ljava/lang/String;

    iput-object v0, p0, Lantlr/ASTFactory;->theASTNodeTypeClass:Ljava/lang/Class;

    iput-object v0, p0, Lantlr/ASTFactory;->tokenTypeToASTClassMap:Ljava/util/Hashtable;

    return-void
.end method

.method public constructor <init>(Ljava/util/Hashtable;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lantlr/ASTFactory;->theASTNodeType:Ljava/lang/String;

    iput-object v0, p0, Lantlr/ASTFactory;->theASTNodeTypeClass:Ljava/lang/Class;

    iput-object v0, p0, Lantlr/ASTFactory;->tokenTypeToASTClassMap:Ljava/util/Hashtable;

    invoke-virtual {p0, p1}, Lantlr/ASTFactory;->setTokenTypeToASTClassMap(Ljava/util/Hashtable;)V

    return-void
.end method

.method static class$(Ljava/lang/String;)Ljava/lang/Class;
    .locals 2

    :try_start_0
    invoke-static {p0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/NoClassDefFoundError;

    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NoClassDefFoundError;-><init>(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method public addASTChild(Lantlr/ASTPair;Lantlr/collections/AST;)V
    .locals 1

    if-eqz p2, :cond_0

    iget-object v0, p1, Lantlr/ASTPair;->root:Lantlr/collections/AST;

    if-nez v0, :cond_1

    iput-object p2, p1, Lantlr/ASTPair;->root:Lantlr/collections/AST;

    :goto_0
    iput-object p2, p1, Lantlr/ASTPair;->child:Lantlr/collections/AST;

    invoke-virtual {p1}, Lantlr/ASTPair;->advanceChildToEnd()V

    :cond_0
    return-void

    :cond_1
    iget-object v0, p1, Lantlr/ASTPair;->child:Lantlr/collections/AST;

    if-nez v0, :cond_2

    iget-object v0, p1, Lantlr/ASTPair;->root:Lantlr/collections/AST;

    invoke-interface {v0, p2}, Lantlr/collections/AST;->setFirstChild(Lantlr/collections/AST;)V

    goto :goto_0

    :cond_2
    iget-object v0, p1, Lantlr/ASTPair;->child:Lantlr/collections/AST;

    invoke-interface {v0, p2}, Lantlr/collections/AST;->setNextSibling(Lantlr/collections/AST;)V

    goto :goto_0
.end method

.method public create()Lantlr/collections/AST;
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lantlr/ASTFactory;->create(I)Lantlr/collections/AST;

    move-result-object v0

    return-object v0
.end method

.method public create(I)Lantlr/collections/AST;
    .locals 2

    invoke-virtual {p0, p1}, Lantlr/ASTFactory;->getASTNodeType(I)Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p0, v0}, Lantlr/ASTFactory;->create(Ljava/lang/Class;)Lantlr/collections/AST;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, ""

    invoke-interface {v0, p1, v1}, Lantlr/collections/AST;->initialize(ILjava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public create(ILjava/lang/String;)Lantlr/collections/AST;
    .locals 1

    invoke-virtual {p0, p1}, Lantlr/ASTFactory;->create(I)Lantlr/collections/AST;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0, p1, p2}, Lantlr/collections/AST;->initialize(ILjava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public create(ILjava/lang/String;Ljava/lang/String;)Lantlr/collections/AST;
    .locals 1

    invoke-virtual {p0, p3}, Lantlr/ASTFactory;->create(Ljava/lang/String;)Lantlr/collections/AST;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0, p1, p2}, Lantlr/collections/AST;->initialize(ILjava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method public create(Lantlr/Token;)Lantlr/collections/AST;
    .locals 1

    invoke-virtual {p1}, Lantlr/Token;->getType()I

    move-result v0

    invoke-virtual {p0, v0}, Lantlr/ASTFactory;->create(I)Lantlr/collections/AST;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Lantlr/collections/AST;->initialize(Lantlr/Token;)V

    :cond_0
    return-object v0
.end method

.method public create(Lantlr/Token;Ljava/lang/String;)Lantlr/collections/AST;
    .locals 1

    invoke-virtual {p0, p1, p2}, Lantlr/ASTFactory;->createUsingCtor(Lantlr/Token;Ljava/lang/String;)Lantlr/collections/AST;

    move-result-object v0

    return-object v0
.end method

.method public create(Lantlr/collections/AST;)Lantlr/collections/AST;
    .locals 1

    if-nez p1, :cond_1

    const/4 v0, 0x0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-interface {p1}, Lantlr/collections/AST;->getType()I

    move-result v0

    invoke-virtual {p0, v0}, Lantlr/ASTFactory;->create(I)Lantlr/collections/AST;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Lantlr/collections/AST;->initialize(Lantlr/collections/AST;)V

    goto :goto_0
.end method

.method protected create(Ljava/lang/Class;)Lantlr/collections/AST;
    .locals 2

    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lantlr/collections/AST;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string v1, "Can\'t create AST Node "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lantlr/ASTFactory;->error(Ljava/lang/String;)V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public create(Ljava/lang/String;)Lantlr/collections/AST;
    .locals 3

    :try_start_0
    invoke-static {p1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    invoke-virtual {p0, v0}, Lantlr/ASTFactory;->create(Ljava/lang/Class;)Lantlr/collections/AST;

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "Invalid class, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected createUsingCtor(Lantlr/Token;Ljava/lang/String;)Lantlr/collections/AST;
    .locals 4

    :try_start_0
    invoke-static {p2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/Class;

    const/4 v3, 0x0

    sget-object v0, Lantlr/ASTFactory;->class$antlr$Token:Ljava/lang/Class;

    if-nez v0, :cond_1

    const-string v0, "antlr.Token"

    invoke-static {v0}, Lantlr/ASTFactory;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lantlr/ASTFactory;->class$antlr$Token:Ljava/lang/Class;

    :goto_0
    aput-object v0, v2, v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    :try_start_1
    invoke-virtual {v1, v2}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v0

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v2}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lantlr/collections/AST;
    :try_end_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :cond_0
    :goto_1
    return-object v0

    :cond_1
    :try_start_2
    sget-object v0, Lantlr/ASTFactory;->class$antlr$Token:Ljava/lang/Class;

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {p0, v1}, Lantlr/ASTFactory;->create(Ljava/lang/Class;)Lantlr/collections/AST;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Lantlr/collections/AST;->initialize(Lantlr/Token;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "Invalid class or can\'t make instance, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public dup(Lantlr/collections/AST;)Lantlr/collections/AST;
    .locals 1

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p0, v0}, Lantlr/ASTFactory;->create(Ljava/lang/Class;)Lantlr/collections/AST;

    move-result-object v0

    invoke-interface {v0, p1}, Lantlr/collections/AST;->initialize(Lantlr/collections/AST;)V

    goto :goto_0
.end method

.method public dupList(Lantlr/collections/AST;)Lantlr/collections/AST;
    .locals 3

    invoke-virtual {p0, p1}, Lantlr/ASTFactory;->dupTree(Lantlr/collections/AST;)Lantlr/collections/AST;

    move-result-object v1

    move-object v0, v1

    :goto_0
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lantlr/collections/AST;->getNextSibling()Lantlr/collections/AST;

    move-result-object p1

    invoke-virtual {p0, p1}, Lantlr/ASTFactory;->dupTree(Lantlr/collections/AST;)Lantlr/collections/AST;

    move-result-object v2

    invoke-interface {v0, v2}, Lantlr/collections/AST;->setNextSibling(Lantlr/collections/AST;)V

    invoke-interface {v0}, Lantlr/collections/AST;->getNextSibling()Lantlr/collections/AST;

    move-result-object v0

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method public dupTree(Lantlr/collections/AST;)Lantlr/collections/AST;
    .locals 2

    invoke-virtual {p0, p1}, Lantlr/ASTFactory;->dup(Lantlr/collections/AST;)Lantlr/collections/AST;

    move-result-object v0

    if-eqz p1, :cond_0

    invoke-interface {p1}, Lantlr/collections/AST;->getFirstChild()Lantlr/collections/AST;

    move-result-object v1

    invoke-virtual {p0, v1}, Lantlr/ASTFactory;->dupList(Lantlr/collections/AST;)Lantlr/collections/AST;

    move-result-object v1

    invoke-interface {v0, v1}, Lantlr/collections/AST;->setFirstChild(Lantlr/collections/AST;)V

    :cond_0
    return-object v0
.end method

.method public error(Ljava/lang/String;)V
    .locals 1

    sget-object v0, Ljava/lang/System;->err:Ljava/io/PrintStream;

    invoke-virtual {v0, p1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    return-void
.end method

.method public getASTNodeType(I)Ljava/lang/Class;
    .locals 2

    iget-object v0, p0, Lantlr/ASTFactory;->tokenTypeToASTClassMap:Ljava/util/Hashtable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lantlr/ASTFactory;->tokenTypeToASTClassMap:Ljava/util/Hashtable;

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, p1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lantlr/ASTFactory;->theASTNodeTypeClass:Ljava/lang/Class;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lantlr/ASTFactory;->theASTNodeTypeClass:Ljava/lang/Class;

    goto :goto_0

    :cond_1
    sget-object v0, Lantlr/ASTFactory;->class$antlr$CommonAST:Ljava/lang/Class;

    if-nez v0, :cond_2

    const-string v0, "antlr.CommonAST"

    invoke-static {v0}, Lantlr/ASTFactory;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lantlr/ASTFactory;->class$antlr$CommonAST:Ljava/lang/Class;

    goto :goto_0

    :cond_2
    sget-object v0, Lantlr/ASTFactory;->class$antlr$CommonAST:Ljava/lang/Class;

    goto :goto_0
.end method

.method public getTokenTypeToASTClassMap()Ljava/util/Hashtable;
    .locals 1

    iget-object v0, p0, Lantlr/ASTFactory;->tokenTypeToASTClassMap:Ljava/util/Hashtable;

    return-object v0
.end method

.method public make(Lantlr/collections/impl/ASTArray;)Lantlr/collections/AST;
    .locals 1

    iget-object v0, p1, Lantlr/collections/impl/ASTArray;->array:[Lantlr/collections/AST;

    invoke-virtual {p0, v0}, Lantlr/ASTFactory;->make([Lantlr/collections/AST;)Lantlr/collections/AST;

    move-result-object v0

    return-object v0
.end method

.method public make([Lantlr/collections/AST;)Lantlr/collections/AST;
    .locals 5

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    array-length v0, p1

    if-nez v0, :cond_1

    :cond_0
    return-object v1

    :cond_1
    const/4 v0, 0x0

    aget-object v2, p1, v0

    if-eqz v2, :cond_2

    invoke-interface {v2, v1}, Lantlr/collections/AST;->setFirstChild(Lantlr/collections/AST;)V

    :cond_2
    const/4 v0, 0x1

    move v4, v0

    move-object v0, v1

    move-object v1, v2

    move v2, v4

    :goto_0
    array-length v3, p1

    if-ge v2, v3, :cond_0

    aget-object v3, p1, v2

    if-nez v3, :cond_4

    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_4
    if-nez v1, :cond_5

    aget-object v0, p1, v2

    move-object v1, v0

    :goto_1
    invoke-interface {v0}, Lantlr/collections/AST;->getNextSibling()Lantlr/collections/AST;

    move-result-object v3

    if-eqz v3, :cond_3

    invoke-interface {v0}, Lantlr/collections/AST;->getNextSibling()Lantlr/collections/AST;

    move-result-object v0

    goto :goto_1

    :cond_5
    if-nez v0, :cond_6

    aget-object v0, p1, v2

    invoke-interface {v1, v0}, Lantlr/collections/AST;->setFirstChild(Lantlr/collections/AST;)V

    invoke-interface {v1}, Lantlr/collections/AST;->getFirstChild()Lantlr/collections/AST;

    move-result-object v0

    goto :goto_1

    :cond_6
    aget-object v3, p1, v2

    invoke-interface {v0, v3}, Lantlr/collections/AST;->setNextSibling(Lantlr/collections/AST;)V

    invoke-interface {v0}, Lantlr/collections/AST;->getNextSibling()Lantlr/collections/AST;

    move-result-object v0

    goto :goto_1
.end method

.method public makeASTRoot(Lantlr/ASTPair;Lantlr/collections/AST;)V
    .locals 1

    if-eqz p2, :cond_0

    iget-object v0, p1, Lantlr/ASTPair;->root:Lantlr/collections/AST;

    invoke-interface {p2, v0}, Lantlr/collections/AST;->addChild(Lantlr/collections/AST;)V

    iget-object v0, p1, Lantlr/ASTPair;->root:Lantlr/collections/AST;

    iput-object v0, p1, Lantlr/ASTPair;->child:Lantlr/collections/AST;

    invoke-virtual {p1}, Lantlr/ASTPair;->advanceChildToEnd()V

    iput-object p2, p1, Lantlr/ASTPair;->root:Lantlr/collections/AST;

    :cond_0
    return-void
.end method

.method public setASTNodeClass(Ljava/lang/String;)V
    .locals 2

    iput-object p1, p0, Lantlr/ASTFactory;->theASTNodeType:Ljava/lang/String;

    :try_start_0
    invoke-static {p1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    iput-object v0, p0, Lantlr/ASTFactory;->theASTNodeTypeClass:Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string v1, "Can\'t find/access AST Node type"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lantlr/ASTFactory;->error(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setASTNodeType(Ljava/lang/String;)V
    .locals 0

    invoke-virtual {p0, p1}, Lantlr/ASTFactory;->setASTNodeClass(Ljava/lang/String;)V

    return-void
.end method

.method public setTokenTypeASTNodeType(ILjava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    iget-object v0, p0, Lantlr/ASTFactory;->tokenTypeToASTClassMap:Ljava/util/Hashtable;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lantlr/ASTFactory;->tokenTypeToASTClassMap:Ljava/util/Hashtable;

    :cond_0
    if-nez p2, :cond_1

    iget-object v0, p0, Lantlr/ASTFactory;->tokenTypeToASTClassMap:Ljava/util/Hashtable;

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, p1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    return-void

    :cond_1
    :try_start_0
    invoke-static {p2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    iget-object v1, p0, Lantlr/ASTFactory;->tokenTypeToASTClassMap:Ljava/util/Hashtable;

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, p1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v1, v2, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "Invalid class, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setTokenTypeToASTClassMap(Ljava/util/Hashtable;)V
    .locals 0

    iput-object p1, p0, Lantlr/ASTFactory;->tokenTypeToASTClassMap:Ljava/util/Hashtable;

    return-void
.end method
