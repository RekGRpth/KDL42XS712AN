.class public Lantlr/ANTLRTokdefLexer;
.super Lantlr/CharScanner;
.source "ANTLRTokdefLexer.java"

# interfaces
.implements Lantlr/ANTLRTokdefParserTokenTypes;
.implements Lantlr/TokenStream;


# static fields
.field public static final _tokenSet_0:Lantlr/collections/impl/BitSet;

.field public static final _tokenSet_1:Lantlr/collections/impl/BitSet;

.field public static final _tokenSet_2:Lantlr/collections/impl/BitSet;

.field public static final _tokenSet_3:Lantlr/collections/impl/BitSet;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lantlr/collections/impl/BitSet;

    invoke-static {}, Lantlr/ANTLRTokdefLexer;->mk_tokenSet_0()[J

    move-result-object v1

    invoke-direct {v0, v1}, Lantlr/collections/impl/BitSet;-><init>([J)V

    sput-object v0, Lantlr/ANTLRTokdefLexer;->_tokenSet_0:Lantlr/collections/impl/BitSet;

    new-instance v0, Lantlr/collections/impl/BitSet;

    invoke-static {}, Lantlr/ANTLRTokdefLexer;->mk_tokenSet_1()[J

    move-result-object v1

    invoke-direct {v0, v1}, Lantlr/collections/impl/BitSet;-><init>([J)V

    sput-object v0, Lantlr/ANTLRTokdefLexer;->_tokenSet_1:Lantlr/collections/impl/BitSet;

    new-instance v0, Lantlr/collections/impl/BitSet;

    invoke-static {}, Lantlr/ANTLRTokdefLexer;->mk_tokenSet_2()[J

    move-result-object v1

    invoke-direct {v0, v1}, Lantlr/collections/impl/BitSet;-><init>([J)V

    sput-object v0, Lantlr/ANTLRTokdefLexer;->_tokenSet_2:Lantlr/collections/impl/BitSet;

    new-instance v0, Lantlr/collections/impl/BitSet;

    invoke-static {}, Lantlr/ANTLRTokdefLexer;->mk_tokenSet_3()[J

    move-result-object v1

    invoke-direct {v0, v1}, Lantlr/collections/impl/BitSet;-><init>([J)V

    sput-object v0, Lantlr/ANTLRTokdefLexer;->_tokenSet_3:Lantlr/collections/impl/BitSet;

    return-void
.end method

.method public constructor <init>(Lantlr/InputBuffer;)V
    .locals 1

    new-instance v0, Lantlr/LexerSharedInputState;

    invoke-direct {v0, p1}, Lantlr/LexerSharedInputState;-><init>(Lantlr/InputBuffer;)V

    invoke-direct {p0, v0}, Lantlr/ANTLRTokdefLexer;-><init>(Lantlr/LexerSharedInputState;)V

    return-void
.end method

.method public constructor <init>(Lantlr/LexerSharedInputState;)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, p1}, Lantlr/CharScanner;-><init>(Lantlr/LexerSharedInputState;)V

    iput-boolean v0, p0, Lantlr/ANTLRTokdefLexer;->caseSensitiveLiterals:Z

    invoke-virtual {p0, v0}, Lantlr/ANTLRTokdefLexer;->setCaseSensitive(Z)V

    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lantlr/ANTLRTokdefLexer;->literals:Ljava/util/Hashtable;

    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 1

    new-instance v0, Lantlr/ByteBuffer;

    invoke-direct {v0, p1}, Lantlr/ByteBuffer;-><init>(Ljava/io/InputStream;)V

    invoke-direct {p0, v0}, Lantlr/ANTLRTokdefLexer;-><init>(Lantlr/InputBuffer;)V

    return-void
.end method

.method public constructor <init>(Ljava/io/Reader;)V
    .locals 1

    new-instance v0, Lantlr/CharBuffer;

    invoke-direct {v0, p1}, Lantlr/CharBuffer;-><init>(Ljava/io/Reader;)V

    invoke-direct {p0, v0}, Lantlr/ANTLRTokdefLexer;-><init>(Lantlr/InputBuffer;)V

    return-void
.end method

.method private static final mk_tokenSet_0()[J
    .locals 4

    const/16 v0, 0x8

    new-array v1, v0, [J

    const/4 v0, 0x0

    const-wide/16 v2, -0x2408

    aput-wide v2, v1, v0

    const/4 v0, 0x1

    :goto_0
    const/4 v2, 0x3

    if-gt v0, v2, :cond_0

    const-wide/16 v2, -0x1

    aput-wide v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method private static final mk_tokenSet_1()[J
    .locals 4

    const/16 v0, 0x8

    new-array v1, v0, [J

    const/4 v0, 0x0

    const-wide v2, -0x800000000008L

    aput-wide v2, v1, v0

    const/4 v0, 0x1

    :goto_0
    const/4 v2, 0x3

    if-gt v0, v2, :cond_0

    const-wide/16 v2, -0x1

    aput-wide v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method private static final mk_tokenSet_2()[J
    .locals 4

    const/16 v0, 0x8

    new-array v1, v0, [J

    const/4 v0, 0x0

    const-wide v2, -0x40000000408L

    aput-wide v2, v1, v0

    const/4 v0, 0x1

    :goto_0
    const/4 v2, 0x3

    if-gt v0, v2, :cond_0

    const-wide/16 v2, -0x1

    aput-wide v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method private static final mk_tokenSet_3()[J
    .locals 4

    const/16 v0, 0x8

    new-array v1, v0, [J

    const/4 v0, 0x0

    const-wide v2, -0x400000008L

    aput-wide v2, v1, v0

    const/4 v0, 0x1

    const-wide/32 v2, -0x10000001

    aput-wide v2, v1, v0

    const/4 v0, 0x2

    :goto_0
    const/4 v2, 0x3

    if-gt v0, v2, :cond_0

    const-wide/16 v2, -0x1

    aput-wide v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v1
.end method


# virtual methods
.method public final mASSIGN(Z)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/RecognitionException;,
            Lantlr/CharStreamException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/4 v0, 0x0

    iget-object v1, p0, Lantlr/ANTLRTokdefLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v1}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    const/4 v2, 0x6

    const/16 v3, 0x3d

    invoke-virtual {p0, v3}, Lantlr/ANTLRTokdefLexer;->match(C)V

    if-eqz p1, :cond_0

    if-nez v0, :cond_0

    invoke-virtual {p0, v2}, Lantlr/ANTLRTokdefLexer;->makeToken(I)Lantlr/Token;

    move-result-object v0

    new-instance v2, Ljava/lang/String;

    iget-object v3, p0, Lantlr/ANTLRTokdefLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v3}, Lantlr/ANTLRStringBuffer;->getBuffer()[C

    move-result-object v3

    iget-object v4, p0, Lantlr/ANTLRTokdefLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v4

    sub-int/2addr v4, v1

    invoke-direct {v2, v3, v1, v4}, Ljava/lang/String;-><init>([CII)V

    invoke-virtual {v0, v2}, Lantlr/Token;->setText(Ljava/lang/String;)V

    :cond_0
    iput-object v0, p0, Lantlr/ANTLRTokdefLexer;->_returnToken:Lantlr/Token;

    return-void
.end method

.method protected final mDIGIT(Z)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/RecognitionException;,
            Lantlr/CharStreamException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/4 v0, 0x0

    iget-object v1, p0, Lantlr/ANTLRTokdefLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v1}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    const/16 v2, 0xe

    const/16 v3, 0x30

    const/16 v4, 0x39

    invoke-virtual {p0, v3, v4}, Lantlr/ANTLRTokdefLexer;->matchRange(CC)V

    if-eqz p1, :cond_0

    if-nez v0, :cond_0

    invoke-virtual {p0, v2}, Lantlr/ANTLRTokdefLexer;->makeToken(I)Lantlr/Token;

    move-result-object v0

    new-instance v2, Ljava/lang/String;

    iget-object v3, p0, Lantlr/ANTLRTokdefLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v3}, Lantlr/ANTLRStringBuffer;->getBuffer()[C

    move-result-object v3

    iget-object v4, p0, Lantlr/ANTLRTokdefLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v4

    sub-int/2addr v4, v1

    invoke-direct {v2, v3, v1, v4}, Ljava/lang/String;-><init>([CII)V

    invoke-virtual {v0, v2}, Lantlr/Token;->setText(Ljava/lang/String;)V

    :cond_0
    iput-object v0, p0, Lantlr/ANTLRTokdefLexer;->_returnToken:Lantlr/Token;

    return-void
.end method

.method protected final mESC(Z)V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/RecognitionException;,
            Lantlr/CharStreamException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/16 v9, 0xff

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x0

    const/4 v5, 0x1

    const/4 v0, 0x0

    iget-object v1, p0, Lantlr/ANTLRTokdefLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v1}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    const/16 v2, 0xd

    const/16 v3, 0x5c

    invoke-virtual {p0, v3}, Lantlr/ANTLRTokdefLexer;->match(C)V

    invoke-virtual {p0, v5}, Lantlr/ANTLRTokdefLexer;->LA(I)C

    move-result v3

    sparse-switch v3, :sswitch_data_0

    new-instance v0, Lantlr/NoViableAltForCharException;

    invoke-virtual {p0, v5}, Lantlr/ANTLRTokdefLexer;->LA(I)C

    move-result v1

    invoke-virtual {p0}, Lantlr/ANTLRTokdefLexer;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lantlr/ANTLRTokdefLexer;->getLine()I

    move-result v3

    invoke-virtual {p0}, Lantlr/ANTLRTokdefLexer;->getColumn()I

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Lantlr/NoViableAltForCharException;-><init>(CLjava/lang/String;II)V

    throw v0

    :sswitch_0
    const/16 v3, 0x6e

    invoke-virtual {p0, v3}, Lantlr/ANTLRTokdefLexer;->match(C)V

    :cond_0
    :goto_0
    if-eqz p1, :cond_1

    if-nez v0, :cond_1

    invoke-virtual {p0, v2}, Lantlr/ANTLRTokdefLexer;->makeToken(I)Lantlr/Token;

    move-result-object v0

    new-instance v2, Ljava/lang/String;

    iget-object v3, p0, Lantlr/ANTLRTokdefLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v3}, Lantlr/ANTLRStringBuffer;->getBuffer()[C

    move-result-object v3

    iget-object v4, p0, Lantlr/ANTLRTokdefLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v4

    sub-int/2addr v4, v1

    invoke-direct {v2, v3, v1, v4}, Ljava/lang/String;-><init>([CII)V

    invoke-virtual {v0, v2}, Lantlr/Token;->setText(Ljava/lang/String;)V

    :cond_1
    iput-object v0, p0, Lantlr/ANTLRTokdefLexer;->_returnToken:Lantlr/Token;

    return-void

    :sswitch_1
    const/16 v3, 0x72

    invoke-virtual {p0, v3}, Lantlr/ANTLRTokdefLexer;->match(C)V

    goto :goto_0

    :sswitch_2
    const/16 v3, 0x74

    invoke-virtual {p0, v3}, Lantlr/ANTLRTokdefLexer;->match(C)V

    goto :goto_0

    :sswitch_3
    const/16 v3, 0x62

    invoke-virtual {p0, v3}, Lantlr/ANTLRTokdefLexer;->match(C)V

    goto :goto_0

    :sswitch_4
    const/16 v3, 0x66

    invoke-virtual {p0, v3}, Lantlr/ANTLRTokdefLexer;->match(C)V

    goto :goto_0

    :sswitch_5
    const/16 v3, 0x22

    invoke-virtual {p0, v3}, Lantlr/ANTLRTokdefLexer;->match(C)V

    goto :goto_0

    :sswitch_6
    const/16 v3, 0x27

    invoke-virtual {p0, v3}, Lantlr/ANTLRTokdefLexer;->match(C)V

    goto :goto_0

    :sswitch_7
    const/16 v3, 0x5c

    invoke-virtual {p0, v3}, Lantlr/ANTLRTokdefLexer;->match(C)V

    goto :goto_0

    :sswitch_8
    const/16 v3, 0x30

    const/16 v4, 0x33

    invoke-virtual {p0, v3, v4}, Lantlr/ANTLRTokdefLexer;->matchRange(CC)V

    invoke-virtual {p0, v5}, Lantlr/ANTLRTokdefLexer;->LA(I)C

    move-result v3

    const/16 v4, 0x30

    if-lt v3, v4, :cond_4

    invoke-virtual {p0, v5}, Lantlr/ANTLRTokdefLexer;->LA(I)C

    move-result v3

    const/16 v4, 0x39

    if-gt v3, v4, :cond_4

    invoke-virtual {p0, v7}, Lantlr/ANTLRTokdefLexer;->LA(I)C

    move-result v3

    if-lt v3, v8, :cond_4

    invoke-virtual {p0, v7}, Lantlr/ANTLRTokdefLexer;->LA(I)C

    move-result v3

    if-gt v3, v9, :cond_4

    invoke-virtual {p0, v6}, Lantlr/ANTLRTokdefLexer;->mDIGIT(Z)V

    invoke-virtual {p0, v5}, Lantlr/ANTLRTokdefLexer;->LA(I)C

    move-result v3

    const/16 v4, 0x30

    if-lt v3, v4, :cond_2

    invoke-virtual {p0, v5}, Lantlr/ANTLRTokdefLexer;->LA(I)C

    move-result v3

    const/16 v4, 0x39

    if-gt v3, v4, :cond_2

    invoke-virtual {p0, v7}, Lantlr/ANTLRTokdefLexer;->LA(I)C

    move-result v3

    if-lt v3, v8, :cond_2

    invoke-virtual {p0, v7}, Lantlr/ANTLRTokdefLexer;->LA(I)C

    move-result v3

    if-gt v3, v9, :cond_2

    invoke-virtual {p0, v6}, Lantlr/ANTLRTokdefLexer;->mDIGIT(Z)V

    goto/16 :goto_0

    :cond_2
    invoke-virtual {p0, v5}, Lantlr/ANTLRTokdefLexer;->LA(I)C

    move-result v3

    if-lt v3, v8, :cond_3

    invoke-virtual {p0, v5}, Lantlr/ANTLRTokdefLexer;->LA(I)C

    move-result v3

    if-le v3, v9, :cond_0

    :cond_3
    new-instance v0, Lantlr/NoViableAltForCharException;

    invoke-virtual {p0, v5}, Lantlr/ANTLRTokdefLexer;->LA(I)C

    move-result v1

    invoke-virtual {p0}, Lantlr/ANTLRTokdefLexer;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lantlr/ANTLRTokdefLexer;->getLine()I

    move-result v3

    invoke-virtual {p0}, Lantlr/ANTLRTokdefLexer;->getColumn()I

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Lantlr/NoViableAltForCharException;-><init>(CLjava/lang/String;II)V

    throw v0

    :cond_4
    invoke-virtual {p0, v5}, Lantlr/ANTLRTokdefLexer;->LA(I)C

    move-result v3

    if-lt v3, v8, :cond_5

    invoke-virtual {p0, v5}, Lantlr/ANTLRTokdefLexer;->LA(I)C

    move-result v3

    if-le v3, v9, :cond_0

    :cond_5
    new-instance v0, Lantlr/NoViableAltForCharException;

    invoke-virtual {p0, v5}, Lantlr/ANTLRTokdefLexer;->LA(I)C

    move-result v1

    invoke-virtual {p0}, Lantlr/ANTLRTokdefLexer;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lantlr/ANTLRTokdefLexer;->getLine()I

    move-result v3

    invoke-virtual {p0}, Lantlr/ANTLRTokdefLexer;->getColumn()I

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Lantlr/NoViableAltForCharException;-><init>(CLjava/lang/String;II)V

    throw v0

    :sswitch_9
    const/16 v3, 0x34

    const/16 v4, 0x37

    invoke-virtual {p0, v3, v4}, Lantlr/ANTLRTokdefLexer;->matchRange(CC)V

    invoke-virtual {p0, v5}, Lantlr/ANTLRTokdefLexer;->LA(I)C

    move-result v3

    const/16 v4, 0x30

    if-lt v3, v4, :cond_6

    invoke-virtual {p0, v5}, Lantlr/ANTLRTokdefLexer;->LA(I)C

    move-result v3

    const/16 v4, 0x39

    if-gt v3, v4, :cond_6

    invoke-virtual {p0, v7}, Lantlr/ANTLRTokdefLexer;->LA(I)C

    move-result v3

    if-lt v3, v8, :cond_6

    invoke-virtual {p0, v7}, Lantlr/ANTLRTokdefLexer;->LA(I)C

    move-result v3

    if-gt v3, v9, :cond_6

    invoke-virtual {p0, v6}, Lantlr/ANTLRTokdefLexer;->mDIGIT(Z)V

    goto/16 :goto_0

    :cond_6
    invoke-virtual {p0, v5}, Lantlr/ANTLRTokdefLexer;->LA(I)C

    move-result v3

    if-lt v3, v8, :cond_7

    invoke-virtual {p0, v5}, Lantlr/ANTLRTokdefLexer;->LA(I)C

    move-result v3

    if-le v3, v9, :cond_0

    :cond_7
    new-instance v0, Lantlr/NoViableAltForCharException;

    invoke-virtual {p0, v5}, Lantlr/ANTLRTokdefLexer;->LA(I)C

    move-result v1

    invoke-virtual {p0}, Lantlr/ANTLRTokdefLexer;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lantlr/ANTLRTokdefLexer;->getLine()I

    move-result v3

    invoke-virtual {p0}, Lantlr/ANTLRTokdefLexer;->getColumn()I

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Lantlr/NoViableAltForCharException;-><init>(CLjava/lang/String;II)V

    throw v0

    :sswitch_a
    const/16 v3, 0x75

    invoke-virtual {p0, v3}, Lantlr/ANTLRTokdefLexer;->match(C)V

    invoke-virtual {p0, v6}, Lantlr/ANTLRTokdefLexer;->mXDIGIT(Z)V

    invoke-virtual {p0, v6}, Lantlr/ANTLRTokdefLexer;->mXDIGIT(Z)V

    invoke-virtual {p0, v6}, Lantlr/ANTLRTokdefLexer;->mXDIGIT(Z)V

    invoke-virtual {p0, v6}, Lantlr/ANTLRTokdefLexer;->mXDIGIT(Z)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x22 -> :sswitch_5
        0x27 -> :sswitch_6
        0x30 -> :sswitch_8
        0x31 -> :sswitch_8
        0x32 -> :sswitch_8
        0x33 -> :sswitch_8
        0x34 -> :sswitch_9
        0x35 -> :sswitch_9
        0x36 -> :sswitch_9
        0x37 -> :sswitch_9
        0x5c -> :sswitch_7
        0x62 -> :sswitch_3
        0x66 -> :sswitch_4
        0x6e -> :sswitch_0
        0x72 -> :sswitch_1
        0x74 -> :sswitch_2
        0x75 -> :sswitch_a
    .end sparse-switch
.end method

.method public final mID(Z)V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/RecognitionException;,
            Lantlr/CharStreamException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/16 v9, 0x7a

    const/16 v8, 0x61

    const/16 v7, 0x5a

    const/16 v6, 0x41

    const/4 v5, 0x1

    const/4 v0, 0x0

    iget-object v1, p0, Lantlr/ANTLRTokdefLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v1}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    const/4 v2, 0x4

    invoke-virtual {p0, v5}, Lantlr/ANTLRTokdefLexer;->LA(I)C

    move-result v3

    packed-switch v3, :pswitch_data_0

    :pswitch_0
    new-instance v0, Lantlr/NoViableAltForCharException;

    invoke-virtual {p0, v5}, Lantlr/ANTLRTokdefLexer;->LA(I)C

    move-result v1

    invoke-virtual {p0}, Lantlr/ANTLRTokdefLexer;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lantlr/ANTLRTokdefLexer;->getLine()I

    move-result v3

    invoke-virtual {p0}, Lantlr/ANTLRTokdefLexer;->getColumn()I

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Lantlr/NoViableAltForCharException;-><init>(CLjava/lang/String;II)V

    throw v0

    :pswitch_1
    invoke-virtual {p0, v8, v9}, Lantlr/ANTLRTokdefLexer;->matchRange(CC)V

    :goto_0
    invoke-virtual {p0, v5}, Lantlr/ANTLRTokdefLexer;->LA(I)C

    move-result v3

    packed-switch v3, :pswitch_data_1

    :pswitch_2
    if-eqz p1, :cond_0

    if-nez v0, :cond_0

    invoke-virtual {p0, v2}, Lantlr/ANTLRTokdefLexer;->makeToken(I)Lantlr/Token;

    move-result-object v0

    new-instance v2, Ljava/lang/String;

    iget-object v3, p0, Lantlr/ANTLRTokdefLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v3}, Lantlr/ANTLRStringBuffer;->getBuffer()[C

    move-result-object v3

    iget-object v4, p0, Lantlr/ANTLRTokdefLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v4

    sub-int/2addr v4, v1

    invoke-direct {v2, v3, v1, v4}, Ljava/lang/String;-><init>([CII)V

    invoke-virtual {v0, v2}, Lantlr/Token;->setText(Ljava/lang/String;)V

    :cond_0
    iput-object v0, p0, Lantlr/ANTLRTokdefLexer;->_returnToken:Lantlr/Token;

    return-void

    :pswitch_3
    invoke-virtual {p0, v6, v7}, Lantlr/ANTLRTokdefLexer;->matchRange(CC)V

    goto :goto_0

    :pswitch_4
    invoke-virtual {p0, v8, v9}, Lantlr/ANTLRTokdefLexer;->matchRange(CC)V

    goto :goto_0

    :pswitch_5
    invoke-virtual {p0, v6, v7}, Lantlr/ANTLRTokdefLexer;->matchRange(CC)V

    goto :goto_0

    :pswitch_6
    const/16 v3, 0x5f

    invoke-virtual {p0, v3}, Lantlr/ANTLRTokdefLexer;->match(C)V

    goto :goto_0

    :pswitch_7
    const/16 v3, 0x30

    const/16 v4, 0x39

    invoke-virtual {p0, v3, v4}, Lantlr/ANTLRTokdefLexer;->matchRange(CC)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x41
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x30
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_6
        :pswitch_2
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
    .end packed-switch
.end method

.method public final mINT(Z)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/RecognitionException;,
            Lantlr/CharStreamException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/4 v1, 0x0

    const/4 v7, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lantlr/ANTLRTokdefLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v0}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v3

    const/16 v4, 0x9

    move v0, v1

    :goto_0
    invoke-virtual {p0, v7}, Lantlr/ANTLRTokdefLexer;->LA(I)C

    move-result v5

    const/16 v6, 0x30

    if-lt v5, v6, :cond_0

    invoke-virtual {p0, v7}, Lantlr/ANTLRTokdefLexer;->LA(I)C

    move-result v5

    const/16 v6, 0x39

    if-gt v5, v6, :cond_0

    invoke-virtual {p0, v1}, Lantlr/ANTLRTokdefLexer;->mDIGIT(Z)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    if-lt v0, v7, :cond_1

    if-eqz p1, :cond_2

    if-nez v2, :cond_2

    invoke-virtual {p0, v4}, Lantlr/ANTLRTokdefLexer;->makeToken(I)Lantlr/Token;

    move-result-object v0

    new-instance v1, Ljava/lang/String;

    iget-object v2, p0, Lantlr/ANTLRTokdefLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v2}, Lantlr/ANTLRStringBuffer;->getBuffer()[C

    move-result-object v2

    iget-object v4, p0, Lantlr/ANTLRTokdefLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v4

    sub-int/2addr v4, v3

    invoke-direct {v1, v2, v3, v4}, Ljava/lang/String;-><init>([CII)V

    invoke-virtual {v0, v1}, Lantlr/Token;->setText(Ljava/lang/String;)V

    :goto_1
    iput-object v0, p0, Lantlr/ANTLRTokdefLexer;->_returnToken:Lantlr/Token;

    return-void

    :cond_1
    new-instance v0, Lantlr/NoViableAltForCharException;

    invoke-virtual {p0, v7}, Lantlr/ANTLRTokdefLexer;->LA(I)C

    move-result v1

    invoke-virtual {p0}, Lantlr/ANTLRTokdefLexer;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lantlr/ANTLRTokdefLexer;->getLine()I

    move-result v3

    invoke-virtual {p0}, Lantlr/ANTLRTokdefLexer;->getColumn()I

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Lantlr/NoViableAltForCharException;-><init>(CLjava/lang/String;II)V

    throw v0

    :cond_2
    move-object v0, v2

    goto :goto_1
.end method

.method public final mLPAREN(Z)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/RecognitionException;,
            Lantlr/CharStreamException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/4 v0, 0x0

    iget-object v1, p0, Lantlr/ANTLRTokdefLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v1}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    const/4 v2, 0x7

    const/16 v3, 0x28

    invoke-virtual {p0, v3}, Lantlr/ANTLRTokdefLexer;->match(C)V

    if-eqz p1, :cond_0

    if-nez v0, :cond_0

    invoke-virtual {p0, v2}, Lantlr/ANTLRTokdefLexer;->makeToken(I)Lantlr/Token;

    move-result-object v0

    new-instance v2, Ljava/lang/String;

    iget-object v3, p0, Lantlr/ANTLRTokdefLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v3}, Lantlr/ANTLRStringBuffer;->getBuffer()[C

    move-result-object v3

    iget-object v4, p0, Lantlr/ANTLRTokdefLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v4

    sub-int/2addr v4, v1

    invoke-direct {v2, v3, v1, v4}, Ljava/lang/String;-><init>([CII)V

    invoke-virtual {v0, v2}, Lantlr/Token;->setText(Ljava/lang/String;)V

    :cond_0
    iput-object v0, p0, Lantlr/ANTLRTokdefLexer;->_returnToken:Lantlr/Token;

    return-void
.end method

.method public final mML_COMMENT(Z)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/RecognitionException;,
            Lantlr/CharStreamException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/16 v5, 0xa

    const/16 v4, 0x2a

    const/4 v3, 0x1

    const/4 v0, 0x0

    iget-object v1, p0, Lantlr/ANTLRTokdefLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v1}, Lantlr/ANTLRStringBuffer;->length()I

    const-string v1, "/*"

    invoke-virtual {p0, v1}, Lantlr/ANTLRTokdefLexer;->match(Ljava/lang/String;)V

    :goto_0
    invoke-virtual {p0, v3}, Lantlr/ANTLRTokdefLexer;->LA(I)C

    move-result v1

    if-ne v1, v4, :cond_0

    sget-object v1, Lantlr/ANTLRTokdefLexer;->_tokenSet_1:Lantlr/collections/impl/BitSet;

    const/4 v2, 0x2

    invoke-virtual {p0, v2}, Lantlr/ANTLRTokdefLexer;->LA(I)C

    move-result v2

    invoke-virtual {v1, v2}, Lantlr/collections/impl/BitSet;->member(I)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, v4}, Lantlr/ANTLRTokdefLexer;->match(C)V

    const/16 v1, 0x2f

    invoke-virtual {p0, v1}, Lantlr/ANTLRTokdefLexer;->matchNot(C)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0, v3}, Lantlr/ANTLRTokdefLexer;->LA(I)C

    move-result v1

    if-ne v1, v5, :cond_1

    invoke-virtual {p0, v5}, Lantlr/ANTLRTokdefLexer;->match(C)V

    invoke-virtual {p0}, Lantlr/ANTLRTokdefLexer;->newline()V

    goto :goto_0

    :cond_1
    sget-object v1, Lantlr/ANTLRTokdefLexer;->_tokenSet_2:Lantlr/collections/impl/BitSet;

    invoke-virtual {p0, v3}, Lantlr/ANTLRTokdefLexer;->LA(I)C

    move-result v2

    invoke-virtual {v1, v2}, Lantlr/collections/impl/BitSet;->member(I)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p0, v4}, Lantlr/ANTLRTokdefLexer;->matchNot(C)V

    goto :goto_0

    :cond_2
    const-string v1, "*/"

    invoke-virtual {p0, v1}, Lantlr/ANTLRTokdefLexer;->match(Ljava/lang/String;)V

    if-eqz p1, :cond_3

    if-nez v0, :cond_3

    :cond_3
    iput-object v0, p0, Lantlr/ANTLRTokdefLexer;->_returnToken:Lantlr/Token;

    return-void
.end method

.method public final mRPAREN(Z)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/RecognitionException;,
            Lantlr/CharStreamException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/4 v0, 0x0

    iget-object v1, p0, Lantlr/ANTLRTokdefLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v1}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    const/16 v2, 0x8

    const/16 v3, 0x29

    invoke-virtual {p0, v3}, Lantlr/ANTLRTokdefLexer;->match(C)V

    if-eqz p1, :cond_0

    if-nez v0, :cond_0

    invoke-virtual {p0, v2}, Lantlr/ANTLRTokdefLexer;->makeToken(I)Lantlr/Token;

    move-result-object v0

    new-instance v2, Ljava/lang/String;

    iget-object v3, p0, Lantlr/ANTLRTokdefLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v3}, Lantlr/ANTLRStringBuffer;->getBuffer()[C

    move-result-object v3

    iget-object v4, p0, Lantlr/ANTLRTokdefLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v4

    sub-int/2addr v4, v1

    invoke-direct {v2, v3, v1, v4}, Ljava/lang/String;-><init>([CII)V

    invoke-virtual {v0, v2}, Lantlr/Token;->setText(Ljava/lang/String;)V

    :cond_0
    iput-object v0, p0, Lantlr/ANTLRTokdefLexer;->_returnToken:Lantlr/Token;

    return-void
.end method

.method public final mSL_COMMENT(Z)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/RecognitionException;,
            Lantlr/CharStreamException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/16 v4, 0xa

    const/4 v3, 0x1

    const/4 v0, 0x0

    iget-object v1, p0, Lantlr/ANTLRTokdefLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v1}, Lantlr/ANTLRStringBuffer;->length()I

    const-string v1, "//"

    invoke-virtual {p0, v1}, Lantlr/ANTLRTokdefLexer;->match(Ljava/lang/String;)V

    :goto_0
    sget-object v1, Lantlr/ANTLRTokdefLexer;->_tokenSet_0:Lantlr/collections/impl/BitSet;

    invoke-virtual {p0, v3}, Lantlr/ANTLRTokdefLexer;->LA(I)C

    move-result v2

    invoke-virtual {v1, v2}, Lantlr/collections/impl/BitSet;->member(I)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lantlr/ANTLRTokdefLexer;->_tokenSet_0:Lantlr/collections/impl/BitSet;

    invoke-virtual {p0, v1}, Lantlr/ANTLRTokdefLexer;->match(Lantlr/collections/impl/BitSet;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0, v3}, Lantlr/ANTLRTokdefLexer;->LA(I)C

    move-result v1

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    new-instance v0, Lantlr/NoViableAltForCharException;

    invoke-virtual {p0, v3}, Lantlr/ANTLRTokdefLexer;->LA(I)C

    move-result v1

    invoke-virtual {p0}, Lantlr/ANTLRTokdefLexer;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lantlr/ANTLRTokdefLexer;->getLine()I

    move-result v3

    invoke-virtual {p0}, Lantlr/ANTLRTokdefLexer;->getColumn()I

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Lantlr/NoViableAltForCharException;-><init>(CLjava/lang/String;II)V

    throw v0

    :pswitch_1
    invoke-virtual {p0, v4}, Lantlr/ANTLRTokdefLexer;->match(C)V

    :cond_1
    :goto_1
    invoke-virtual {p0}, Lantlr/ANTLRTokdefLexer;->newline()V

    if-eqz p1, :cond_2

    if-nez v0, :cond_2

    :cond_2
    iput-object v0, p0, Lantlr/ANTLRTokdefLexer;->_returnToken:Lantlr/Token;

    return-void

    :pswitch_2
    const/16 v1, 0xd

    invoke-virtual {p0, v1}, Lantlr/ANTLRTokdefLexer;->match(C)V

    invoke-virtual {p0, v3}, Lantlr/ANTLRTokdefLexer;->LA(I)C

    move-result v1

    if-ne v1, v4, :cond_1

    invoke-virtual {p0, v4}, Lantlr/ANTLRTokdefLexer;->match(C)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public final mSTRING(Z)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/RecognitionException;,
            Lantlr/CharStreamException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/4 v6, 0x1

    const/16 v5, 0x22

    const/4 v0, 0x0

    iget-object v1, p0, Lantlr/ANTLRTokdefLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v1}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    const/4 v2, 0x5

    invoke-virtual {p0, v5}, Lantlr/ANTLRTokdefLexer;->match(C)V

    :goto_0
    invoke-virtual {p0, v6}, Lantlr/ANTLRTokdefLexer;->LA(I)C

    move-result v3

    const/16 v4, 0x5c

    if-ne v3, v4, :cond_0

    const/4 v3, 0x0

    invoke-virtual {p0, v3}, Lantlr/ANTLRTokdefLexer;->mESC(Z)V

    goto :goto_0

    :cond_0
    sget-object v3, Lantlr/ANTLRTokdefLexer;->_tokenSet_3:Lantlr/collections/impl/BitSet;

    invoke-virtual {p0, v6}, Lantlr/ANTLRTokdefLexer;->LA(I)C

    move-result v4

    invoke-virtual {v3, v4}, Lantlr/collections/impl/BitSet;->member(I)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {p0, v5}, Lantlr/ANTLRTokdefLexer;->matchNot(C)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0, v5}, Lantlr/ANTLRTokdefLexer;->match(C)V

    if-eqz p1, :cond_2

    if-nez v0, :cond_2

    invoke-virtual {p0, v2}, Lantlr/ANTLRTokdefLexer;->makeToken(I)Lantlr/Token;

    move-result-object v0

    new-instance v2, Ljava/lang/String;

    iget-object v3, p0, Lantlr/ANTLRTokdefLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v3}, Lantlr/ANTLRStringBuffer;->getBuffer()[C

    move-result-object v3

    iget-object v4, p0, Lantlr/ANTLRTokdefLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v4

    sub-int/2addr v4, v1

    invoke-direct {v2, v3, v1, v4}, Ljava/lang/String;-><init>([CII)V

    invoke-virtual {v0, v2}, Lantlr/Token;->setText(Ljava/lang/String;)V

    :cond_2
    iput-object v0, p0, Lantlr/ANTLRTokdefLexer;->_returnToken:Lantlr/Token;

    return-void
.end method

.method public final mWS(Z)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/RecognitionException;,
            Lantlr/CharStreamException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/16 v3, 0xa

    const/4 v2, 0x1

    const/4 v0, 0x0

    iget-object v1, p0, Lantlr/ANTLRTokdefLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v1}, Lantlr/ANTLRStringBuffer;->length()I

    invoke-virtual {p0, v2}, Lantlr/ANTLRTokdefLexer;->LA(I)C

    move-result v1

    sparse-switch v1, :sswitch_data_0

    new-instance v0, Lantlr/NoViableAltForCharException;

    invoke-virtual {p0, v2}, Lantlr/ANTLRTokdefLexer;->LA(I)C

    move-result v1

    invoke-virtual {p0}, Lantlr/ANTLRTokdefLexer;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lantlr/ANTLRTokdefLexer;->getLine()I

    move-result v3

    invoke-virtual {p0}, Lantlr/ANTLRTokdefLexer;->getColumn()I

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Lantlr/NoViableAltForCharException;-><init>(CLjava/lang/String;II)V

    throw v0

    :sswitch_0
    const/16 v1, 0x20

    invoke-virtual {p0, v1}, Lantlr/ANTLRTokdefLexer;->match(C)V

    :goto_0
    if-eqz p1, :cond_0

    if-nez v0, :cond_0

    :cond_0
    iput-object v0, p0, Lantlr/ANTLRTokdefLexer;->_returnToken:Lantlr/Token;

    return-void

    :sswitch_1
    const/16 v1, 0x9

    invoke-virtual {p0, v1}, Lantlr/ANTLRTokdefLexer;->match(C)V

    goto :goto_0

    :sswitch_2
    const/16 v1, 0xd

    invoke-virtual {p0, v1}, Lantlr/ANTLRTokdefLexer;->match(C)V

    invoke-virtual {p0, v2}, Lantlr/ANTLRTokdefLexer;->LA(I)C

    move-result v1

    if-ne v1, v3, :cond_1

    invoke-virtual {p0, v3}, Lantlr/ANTLRTokdefLexer;->match(C)V

    :cond_1
    invoke-virtual {p0}, Lantlr/ANTLRTokdefLexer;->newline()V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p0, v3}, Lantlr/ANTLRTokdefLexer;->match(C)V

    invoke-virtual {p0}, Lantlr/ANTLRTokdefLexer;->newline()V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_1
        0xa -> :sswitch_3
        0xd -> :sswitch_2
        0x20 -> :sswitch_0
    .end sparse-switch
.end method

.method protected final mXDIGIT(Z)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/RecognitionException;,
            Lantlr/CharStreamException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/4 v4, 0x1

    const/4 v0, 0x0

    iget-object v1, p0, Lantlr/ANTLRTokdefLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v1}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v1

    const/16 v2, 0xf

    invoke-virtual {p0, v4}, Lantlr/ANTLRTokdefLexer;->LA(I)C

    move-result v3

    sparse-switch v3, :sswitch_data_0

    new-instance v0, Lantlr/NoViableAltForCharException;

    invoke-virtual {p0, v4}, Lantlr/ANTLRTokdefLexer;->LA(I)C

    move-result v1

    invoke-virtual {p0}, Lantlr/ANTLRTokdefLexer;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lantlr/ANTLRTokdefLexer;->getLine()I

    move-result v3

    invoke-virtual {p0}, Lantlr/ANTLRTokdefLexer;->getColumn()I

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Lantlr/NoViableAltForCharException;-><init>(CLjava/lang/String;II)V

    throw v0

    :sswitch_0
    const/16 v3, 0x30

    const/16 v4, 0x39

    invoke-virtual {p0, v3, v4}, Lantlr/ANTLRTokdefLexer;->matchRange(CC)V

    :goto_0
    if-eqz p1, :cond_0

    if-nez v0, :cond_0

    invoke-virtual {p0, v2}, Lantlr/ANTLRTokdefLexer;->makeToken(I)Lantlr/Token;

    move-result-object v0

    new-instance v2, Ljava/lang/String;

    iget-object v3, p0, Lantlr/ANTLRTokdefLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v3}, Lantlr/ANTLRStringBuffer;->getBuffer()[C

    move-result-object v3

    iget-object v4, p0, Lantlr/ANTLRTokdefLexer;->text:Lantlr/ANTLRStringBuffer;

    invoke-virtual {v4}, Lantlr/ANTLRStringBuffer;->length()I

    move-result v4

    sub-int/2addr v4, v1

    invoke-direct {v2, v3, v1, v4}, Ljava/lang/String;-><init>([CII)V

    invoke-virtual {v0, v2}, Lantlr/Token;->setText(Ljava/lang/String;)V

    :cond_0
    iput-object v0, p0, Lantlr/ANTLRTokdefLexer;->_returnToken:Lantlr/Token;

    return-void

    :sswitch_1
    const/16 v3, 0x61

    const/16 v4, 0x66

    invoke-virtual {p0, v3, v4}, Lantlr/ANTLRTokdefLexer;->matchRange(CC)V

    goto :goto_0

    :sswitch_2
    const/16 v3, 0x41

    const/16 v4, 0x46

    invoke-virtual {p0, v3, v4}, Lantlr/ANTLRTokdefLexer;->matchRange(CC)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x30 -> :sswitch_0
        0x31 -> :sswitch_0
        0x32 -> :sswitch_0
        0x33 -> :sswitch_0
        0x34 -> :sswitch_0
        0x35 -> :sswitch_0
        0x36 -> :sswitch_0
        0x37 -> :sswitch_0
        0x38 -> :sswitch_0
        0x39 -> :sswitch_0
        0x41 -> :sswitch_2
        0x42 -> :sswitch_2
        0x43 -> :sswitch_2
        0x44 -> :sswitch_2
        0x45 -> :sswitch_2
        0x46 -> :sswitch_2
        0x61 -> :sswitch_1
        0x62 -> :sswitch_1
        0x63 -> :sswitch_1
        0x64 -> :sswitch_1
        0x65 -> :sswitch_1
        0x66 -> :sswitch_1
    .end sparse-switch
.end method

.method public nextToken()Lantlr/Token;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/16 v2, 0x2f

    :cond_0
    invoke-virtual {p0}, Lantlr/ANTLRTokdefLexer;->resetText()V

    const/4 v0, 0x1

    :try_start_0
    invoke-virtual {p0, v0}, Lantlr/ANTLRTokdefLexer;->LA(I)C

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lantlr/ANTLRTokdefLexer;->LA(I)C

    move-result v0

    if-ne v0, v2, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lantlr/ANTLRTokdefLexer;->LA(I)C

    move-result v0

    if-ne v0, v2, :cond_1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lantlr/ANTLRTokdefLexer;->mSL_COMMENT(Z)V

    iget-object v0, p0, Lantlr/ANTLRTokdefLexer;->_returnToken:Lantlr/Token;

    :goto_0
    iget-object v0, p0, Lantlr/ANTLRTokdefLexer;->_returnToken:Lantlr/Token;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lantlr/ANTLRTokdefLexer;->_returnToken:Lantlr/Token;

    invoke-virtual {v0}, Lantlr/Token;->getType()I

    move-result v0

    iget-object v1, p0, Lantlr/ANTLRTokdefLexer;->_returnToken:Lantlr/Token;

    invoke-virtual {v1, v0}, Lantlr/Token;->setType(I)V

    iget-object v0, p0, Lantlr/ANTLRTokdefLexer;->_returnToken:Lantlr/Token;

    return-object v0

    :pswitch_1
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lantlr/ANTLRTokdefLexer;->mWS(Z)V

    iget-object v0, p0, Lantlr/ANTLRTokdefLexer;->_returnToken:Lantlr/Token;

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lantlr/ANTLRTokdefLexer;->mLPAREN(Z)V

    iget-object v0, p0, Lantlr/ANTLRTokdefLexer;->_returnToken:Lantlr/Token;

    goto :goto_0

    :pswitch_3
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lantlr/ANTLRTokdefLexer;->mRPAREN(Z)V

    iget-object v0, p0, Lantlr/ANTLRTokdefLexer;->_returnToken:Lantlr/Token;

    goto :goto_0

    :pswitch_4
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lantlr/ANTLRTokdefLexer;->mASSIGN(Z)V

    iget-object v0, p0, Lantlr/ANTLRTokdefLexer;->_returnToken:Lantlr/Token;

    goto :goto_0

    :pswitch_5
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lantlr/ANTLRTokdefLexer;->mSTRING(Z)V

    iget-object v0, p0, Lantlr/ANTLRTokdefLexer;->_returnToken:Lantlr/Token;

    goto :goto_0

    :pswitch_6
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lantlr/ANTLRTokdefLexer;->mID(Z)V

    iget-object v0, p0, Lantlr/ANTLRTokdefLexer;->_returnToken:Lantlr/Token;

    goto :goto_0

    :pswitch_7
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lantlr/ANTLRTokdefLexer;->mINT(Z)V

    iget-object v0, p0, Lantlr/ANTLRTokdefLexer;->_returnToken:Lantlr/Token;

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lantlr/ANTLRTokdefLexer;->LA(I)C

    move-result v0

    if-ne v0, v2, :cond_2

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lantlr/ANTLRTokdefLexer;->LA(I)C

    move-result v0

    const/16 v1, 0x2a

    if-ne v0, v1, :cond_2

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lantlr/ANTLRTokdefLexer;->mML_COMMENT(Z)V

    iget-object v0, p0, Lantlr/ANTLRTokdefLexer;->_returnToken:Lantlr/Token;

    goto :goto_0

    :cond_2
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lantlr/ANTLRTokdefLexer;->LA(I)C

    move-result v0

    const v1, 0xffff

    if-ne v0, v1, :cond_3

    invoke-virtual {p0}, Lantlr/ANTLRTokdefLexer;->uponEOF()V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lantlr/ANTLRTokdefLexer;->makeToken(I)Lantlr/Token;

    move-result-object v0

    iput-object v0, p0, Lantlr/ANTLRTokdefLexer;->_returnToken:Lantlr/Token;
    :try_end_0
    .catch Lantlr/RecognitionException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lantlr/CharStreamException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_1
    new-instance v1, Lantlr/TokenStreamRecognitionException;

    invoke-direct {v1, v0}, Lantlr/TokenStreamRecognitionException;-><init>(Lantlr/RecognitionException;)V

    throw v1
    :try_end_1
    .catch Lantlr/CharStreamException; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    move-exception v0

    instance-of v1, v0, Lantlr/CharStreamIOException;

    if-eqz v1, :cond_4

    new-instance v1, Lantlr/TokenStreamIOException;

    check-cast v0, Lantlr/CharStreamIOException;

    iget-object v0, v0, Lantlr/CharStreamIOException;->io:Ljava/io/IOException;

    invoke-direct {v1, v0}, Lantlr/TokenStreamIOException;-><init>(Ljava/io/IOException;)V

    throw v1

    :cond_3
    :try_start_2
    new-instance v0, Lantlr/NoViableAltForCharException;

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lantlr/ANTLRTokdefLexer;->LA(I)C

    move-result v1

    invoke-virtual {p0}, Lantlr/ANTLRTokdefLexer;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lantlr/ANTLRTokdefLexer;->getLine()I

    move-result v3

    invoke-virtual {p0}, Lantlr/ANTLRTokdefLexer;->getColumn()I

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Lantlr/NoViableAltForCharException;-><init>(CLjava/lang/String;II)V

    throw v0
    :try_end_2
    .catch Lantlr/RecognitionException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lantlr/CharStreamException; {:try_start_2 .. :try_end_2} :catch_1

    :cond_4
    new-instance v1, Lantlr/TokenStreamException;

    invoke-virtual {v0}, Lantlr/CharStreamException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lantlr/TokenStreamException;-><init>(Ljava/lang/String;)V

    throw v1

    nop

    :pswitch_data_0
    .packed-switch 0x9
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
    .end packed-switch
.end method
