.class public Lantlr/ASTPair;
.super Ljava/lang/Object;
.source "ASTPair.java"


# instance fields
.field public child:Lantlr/collections/AST;

.field public root:Lantlr/collections/AST;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final advanceChildToEnd()V
    .locals 1

    iget-object v0, p0, Lantlr/ASTPair;->child:Lantlr/collections/AST;

    if-eqz v0, :cond_0

    :goto_0
    iget-object v0, p0, Lantlr/ASTPair;->child:Lantlr/collections/AST;

    invoke-interface {v0}, Lantlr/collections/AST;->getNextSibling()Lantlr/collections/AST;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lantlr/ASTPair;->child:Lantlr/collections/AST;

    invoke-interface {v0}, Lantlr/collections/AST;->getNextSibling()Lantlr/collections/AST;

    move-result-object v0

    iput-object v0, p0, Lantlr/ASTPair;->child:Lantlr/collections/AST;

    goto :goto_0

    :cond_0
    return-void
.end method

.method public copy()Lantlr/ASTPair;
    .locals 2

    new-instance v0, Lantlr/ASTPair;

    invoke-direct {v0}, Lantlr/ASTPair;-><init>()V

    iget-object v1, p0, Lantlr/ASTPair;->root:Lantlr/collections/AST;

    iput-object v1, v0, Lantlr/ASTPair;->root:Lantlr/collections/AST;

    iget-object v1, p0, Lantlr/ASTPair;->child:Lantlr/collections/AST;

    iput-object v1, v0, Lantlr/ASTPair;->child:Lantlr/collections/AST;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    iget-object v0, p0, Lantlr/ASTPair;->root:Lantlr/collections/AST;

    if-nez v0, :cond_0

    const-string v0, "null"

    :goto_0
    iget-object v1, p0, Lantlr/ASTPair;->child:Lantlr/collections/AST;

    if-nez v1, :cond_1

    const-string v1, "null"

    :goto_1
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v0, p0, Lantlr/ASTPair;->root:Lantlr/collections/AST;

    invoke-interface {v0}, Lantlr/collections/AST;->getText()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lantlr/ASTPair;->child:Lantlr/collections/AST;

    invoke-interface {v1}, Lantlr/collections/AST;->getText()Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method
