.class public Lantlr/CommonASTWithHiddenTokens;
.super Lantlr/CommonAST;
.source "CommonASTWithHiddenTokens.java"


# instance fields
.field protected hiddenAfter:Lantlr/CommonHiddenStreamToken;

.field protected hiddenBefore:Lantlr/CommonHiddenStreamToken;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lantlr/CommonAST;-><init>()V

    return-void
.end method

.method public constructor <init>(Lantlr/Token;)V
    .locals 0

    invoke-direct {p0, p1}, Lantlr/CommonAST;-><init>(Lantlr/Token;)V

    return-void
.end method


# virtual methods
.method public getHiddenAfter()Lantlr/CommonHiddenStreamToken;
    .locals 1

    iget-object v0, p0, Lantlr/CommonASTWithHiddenTokens;->hiddenAfter:Lantlr/CommonHiddenStreamToken;

    return-object v0
.end method

.method public getHiddenBefore()Lantlr/CommonHiddenStreamToken;
    .locals 1

    iget-object v0, p0, Lantlr/CommonASTWithHiddenTokens;->hiddenBefore:Lantlr/CommonHiddenStreamToken;

    return-object v0
.end method

.method public initialize(Lantlr/Token;)V
    .locals 1

    check-cast p1, Lantlr/CommonHiddenStreamToken;

    invoke-super {p0, p1}, Lantlr/CommonAST;->initialize(Lantlr/Token;)V

    invoke-virtual {p1}, Lantlr/CommonHiddenStreamToken;->getHiddenBefore()Lantlr/CommonHiddenStreamToken;

    move-result-object v0

    iput-object v0, p0, Lantlr/CommonASTWithHiddenTokens;->hiddenBefore:Lantlr/CommonHiddenStreamToken;

    invoke-virtual {p1}, Lantlr/CommonHiddenStreamToken;->getHiddenAfter()Lantlr/CommonHiddenStreamToken;

    move-result-object v0

    iput-object v0, p0, Lantlr/CommonASTWithHiddenTokens;->hiddenAfter:Lantlr/CommonHiddenStreamToken;

    return-void
.end method
