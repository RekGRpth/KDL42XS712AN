.class public Lantlr/NoViableAltException;
.super Lantlr/RecognitionException;
.source "NoViableAltException.java"


# instance fields
.field public node:Lantlr/collections/AST;

.field public token:Lantlr/Token;


# direct methods
.method public constructor <init>(Lantlr/Token;Ljava/lang/String;)V
    .locals 3

    const-string v0, "NoViableAlt"

    invoke-virtual {p1}, Lantlr/Token;->getLine()I

    move-result v1

    invoke-virtual {p1}, Lantlr/Token;->getColumn()I

    move-result v2

    invoke-direct {p0, v0, p2, v1, v2}, Lantlr/RecognitionException;-><init>(Ljava/lang/String;Ljava/lang/String;II)V

    iput-object p1, p0, Lantlr/NoViableAltException;->token:Lantlr/Token;

    return-void
.end method

.method public constructor <init>(Lantlr/collections/AST;)V
    .locals 4

    const-string v0, "NoViableAlt"

    const-string v1, "<AST>"

    invoke-interface {p1}, Lantlr/collections/AST;->getLine()I

    move-result v2

    invoke-interface {p1}, Lantlr/collections/AST;->getColumn()I

    move-result v3

    invoke-direct {p0, v0, v1, v2, v3}, Lantlr/RecognitionException;-><init>(Ljava/lang/String;Ljava/lang/String;II)V

    iput-object p1, p0, Lantlr/NoViableAltException;->node:Lantlr/collections/AST;

    return-void
.end method


# virtual methods
.method public getMessage()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lantlr/NoViableAltException;->token:Lantlr/Token;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string v1, "unexpected token: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-object v1, p0, Lantlr/NoViableAltException;->token:Lantlr/Token;

    invoke-virtual {v1}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lantlr/NoViableAltException;->node:Lantlr/collections/AST;

    sget-object v1, Lantlr/TreeParser;->ASTNULL:Lantlr/ASTNULLType;

    if-ne v0, v1, :cond_1

    const-string v0, "unexpected end of subtree"

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string v1, "unexpected AST node: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-object v1, p0, Lantlr/NoViableAltException;->node:Lantlr/collections/AST;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
