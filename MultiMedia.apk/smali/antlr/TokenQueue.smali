.class Lantlr/TokenQueue;
.super Ljava/lang/Object;
.source "TokenQueue.java"


# instance fields
.field private buffer:[Lantlr/Token;

.field protected nbrEntries:I

.field private offset:I

.field private sizeLessOne:I


# direct methods
.method public constructor <init>(I)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-gez p1, :cond_0

    const/16 v0, 0x10

    invoke-direct {p0, v0}, Lantlr/TokenQueue;->init(I)V

    :goto_0
    return-void

    :cond_0
    const v0, 0x3fffffff    # 1.9999999f

    if-lt p1, v0, :cond_1

    const v0, 0x7fffffff

    invoke-direct {p0, v0}, Lantlr/TokenQueue;->init(I)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x2

    :goto_1
    if-ge v0, p1, :cond_2

    mul-int/lit8 v0, v0, 0x2

    goto :goto_1

    :cond_2
    invoke-direct {p0, v0}, Lantlr/TokenQueue;->init(I)V

    goto :goto_0
.end method

.method private final expand()V
    .locals 4

    const/4 v1, 0x0

    iget-object v0, p0, Lantlr/TokenQueue;->buffer:[Lantlr/Token;

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x2

    new-array v2, v0, [Lantlr/Token;

    move v0, v1

    :goto_0
    iget-object v3, p0, Lantlr/TokenQueue;->buffer:[Lantlr/Token;

    array-length v3, v3

    if-ge v0, v3, :cond_0

    invoke-virtual {p0, v0}, Lantlr/TokenQueue;->elementAt(I)Lantlr/Token;

    move-result-object v3

    aput-object v3, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iput-object v2, p0, Lantlr/TokenQueue;->buffer:[Lantlr/Token;

    iget-object v0, p0, Lantlr/TokenQueue;->buffer:[Lantlr/Token;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lantlr/TokenQueue;->sizeLessOne:I

    iput v1, p0, Lantlr/TokenQueue;->offset:I

    return-void
.end method

.method private final init(I)V
    .locals 2

    const/4 v1, 0x0

    new-array v0, p1, [Lantlr/Token;

    iput-object v0, p0, Lantlr/TokenQueue;->buffer:[Lantlr/Token;

    add-int/lit8 v0, p1, -0x1

    iput v0, p0, Lantlr/TokenQueue;->sizeLessOne:I

    iput v1, p0, Lantlr/TokenQueue;->offset:I

    iput v1, p0, Lantlr/TokenQueue;->nbrEntries:I

    return-void
.end method


# virtual methods
.method public final append(Lantlr/Token;)V
    .locals 3

    iget v0, p0, Lantlr/TokenQueue;->nbrEntries:I

    iget-object v1, p0, Lantlr/TokenQueue;->buffer:[Lantlr/Token;

    array-length v1, v1

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lantlr/TokenQueue;->expand()V

    :cond_0
    iget-object v0, p0, Lantlr/TokenQueue;->buffer:[Lantlr/Token;

    iget v1, p0, Lantlr/TokenQueue;->offset:I

    iget v2, p0, Lantlr/TokenQueue;->nbrEntries:I

    add-int/2addr v1, v2

    iget v2, p0, Lantlr/TokenQueue;->sizeLessOne:I

    and-int/2addr v1, v2

    aput-object p1, v0, v1

    iget v0, p0, Lantlr/TokenQueue;->nbrEntries:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lantlr/TokenQueue;->nbrEntries:I

    return-void
.end method

.method public final elementAt(I)Lantlr/Token;
    .locals 3

    iget-object v0, p0, Lantlr/TokenQueue;->buffer:[Lantlr/Token;

    iget v1, p0, Lantlr/TokenQueue;->offset:I

    add-int/2addr v1, p1

    iget v2, p0, Lantlr/TokenQueue;->sizeLessOne:I

    and-int/2addr v1, v2

    aget-object v0, v0, v1

    return-object v0
.end method

.method public final removeFirst()V
    .locals 2

    iget v0, p0, Lantlr/TokenQueue;->offset:I

    add-int/lit8 v0, v0, 0x1

    iget v1, p0, Lantlr/TokenQueue;->sizeLessOne:I

    and-int/2addr v0, v1

    iput v0, p0, Lantlr/TokenQueue;->offset:I

    iget v0, p0, Lantlr/TokenQueue;->nbrEntries:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lantlr/TokenQueue;->nbrEntries:I

    return-void
.end method

.method public final reset()V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lantlr/TokenQueue;->offset:I

    iput v0, p0, Lantlr/TokenQueue;->nbrEntries:I

    return-void
.end method
