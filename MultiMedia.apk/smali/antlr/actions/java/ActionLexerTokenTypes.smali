.class public interface abstract Lantlr/actions/java/ActionLexerTokenTypes;
.super Ljava/lang/Object;
.source "ActionLexerTokenTypes.java"


# static fields
.field public static final ACTION:I = 0x4

.field public static final ARG:I = 0x10

.field public static final AST_CONSTRUCTOR:I = 0xa

.field public static final AST_CTOR_ELEMENT:I = 0xb

.field public static final AST_ITEM:I = 0x6

.field public static final CHAR:I = 0x16

.field public static final COMMENT:I = 0x13

.field public static final DIGIT:I = 0x19

.field public static final EOF:I = 0x1

.field public static final ESC:I = 0x18

.field public static final ID:I = 0x11

.field public static final ID_ELEMENT:I = 0xc

.field public static final INT:I = 0x1a

.field public static final INT_OR_FLOAT:I = 0x1b

.field public static final ML_COMMENT:I = 0x15

.field public static final NULL_TREE_LOOKAHEAD:I = 0x3

.field public static final SL_COMMENT:I = 0x14

.field public static final STRING:I = 0x17

.field public static final STUFF:I = 0x5

.field public static final TEXT_ARG:I = 0xd

.field public static final TEXT_ARG_ELEMENT:I = 0xe

.field public static final TEXT_ARG_ID_ELEMENT:I = 0xf

.field public static final TEXT_ITEM:I = 0x7

.field public static final TREE:I = 0x8

.field public static final TREE_ELEMENT:I = 0x9

.field public static final VAR_ASSIGN:I = 0x12

.field public static final WS:I = 0x1c
