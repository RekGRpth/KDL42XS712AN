.class public Lantlr/Lookahead;
.super Ljava/lang/Object;
.source "Lookahead.java"

# interfaces
.implements Ljava/lang/Cloneable;


# instance fields
.field cycle:Ljava/lang/String;

.field epsilonDepth:Lantlr/collections/impl/BitSet;

.field fset:Lantlr/collections/impl/BitSet;

.field hasEpsilon:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lantlr/Lookahead;->hasEpsilon:Z

    new-instance v0, Lantlr/collections/impl/BitSet;

    invoke-direct {v0}, Lantlr/collections/impl/BitSet;-><init>()V

    iput-object v0, p0, Lantlr/Lookahead;->fset:Lantlr/collections/impl/BitSet;

    return-void
.end method

.method public constructor <init>(Lantlr/collections/impl/BitSet;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lantlr/Lookahead;->hasEpsilon:Z

    iput-object p1, p0, Lantlr/Lookahead;->fset:Lantlr/collections/impl/BitSet;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Lantlr/Lookahead;-><init>()V

    iput-object p1, p0, Lantlr/Lookahead;->cycle:Ljava/lang/String;

    return-void
.end method

.method public static of(I)Lantlr/Lookahead;
    .locals 2

    new-instance v0, Lantlr/Lookahead;

    invoke-direct {v0}, Lantlr/Lookahead;-><init>()V

    iget-object v1, v0, Lantlr/Lookahead;->fset:Lantlr/collections/impl/BitSet;

    invoke-virtual {v1, p0}, Lantlr/collections/impl/BitSet;->add(I)V

    return-object v0
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 2

    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lantlr/Lookahead;

    iget-object v1, p0, Lantlr/Lookahead;->fset:Lantlr/collections/impl/BitSet;

    invoke-virtual {v1}, Lantlr/collections/impl/BitSet;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lantlr/collections/impl/BitSet;

    iput-object v1, v0, Lantlr/Lookahead;->fset:Lantlr/collections/impl/BitSet;

    iget-object v1, p0, Lantlr/Lookahead;->cycle:Ljava/lang/String;

    iput-object v1, v0, Lantlr/Lookahead;->cycle:Ljava/lang/String;

    iget-object v1, p0, Lantlr/Lookahead;->epsilonDepth:Lantlr/collections/impl/BitSet;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lantlr/Lookahead;->epsilonDepth:Lantlr/collections/impl/BitSet;

    invoke-virtual {v1}, Lantlr/collections/impl/BitSet;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lantlr/collections/impl/BitSet;

    iput-object v1, v0, Lantlr/Lookahead;->epsilonDepth:Lantlr/collections/impl/BitSet;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    return-object v0

    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/InternalError;

    invoke-direct {v0}, Ljava/lang/InternalError;-><init>()V

    throw v0
.end method

.method public combineWith(Lantlr/Lookahead;)V
    .locals 2

    iget-object v0, p0, Lantlr/Lookahead;->cycle:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p1, Lantlr/Lookahead;->cycle:Ljava/lang/String;

    iput-object v0, p0, Lantlr/Lookahead;->cycle:Ljava/lang/String;

    :cond_0
    invoke-virtual {p1}, Lantlr/Lookahead;->containsEpsilon()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lantlr/Lookahead;->hasEpsilon:Z

    :cond_1
    iget-object v0, p0, Lantlr/Lookahead;->epsilonDepth:Lantlr/collections/impl/BitSet;

    if-eqz v0, :cond_3

    iget-object v0, p1, Lantlr/Lookahead;->epsilonDepth:Lantlr/collections/impl/BitSet;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lantlr/Lookahead;->epsilonDepth:Lantlr/collections/impl/BitSet;

    iget-object v1, p1, Lantlr/Lookahead;->epsilonDepth:Lantlr/collections/impl/BitSet;

    invoke-virtual {v0, v1}, Lantlr/collections/impl/BitSet;->orInPlace(Lantlr/collections/impl/BitSet;)V

    :cond_2
    :goto_0
    iget-object v0, p0, Lantlr/Lookahead;->fset:Lantlr/collections/impl/BitSet;

    iget-object v1, p1, Lantlr/Lookahead;->fset:Lantlr/collections/impl/BitSet;

    invoke-virtual {v0, v1}, Lantlr/collections/impl/BitSet;->orInPlace(Lantlr/collections/impl/BitSet;)V

    return-void

    :cond_3
    iget-object v0, p1, Lantlr/Lookahead;->epsilonDepth:Lantlr/collections/impl/BitSet;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lantlr/Lookahead;->epsilonDepth:Lantlr/collections/impl/BitSet;

    invoke-virtual {v0}, Lantlr/collections/impl/BitSet;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lantlr/collections/impl/BitSet;

    iput-object v0, p0, Lantlr/Lookahead;->epsilonDepth:Lantlr/collections/impl/BitSet;

    goto :goto_0
.end method

.method public containsEpsilon()Z
    .locals 1

    iget-boolean v0, p0, Lantlr/Lookahead;->hasEpsilon:Z

    return v0
.end method

.method public intersection(Lantlr/Lookahead;)Lantlr/Lookahead;
    .locals 3

    new-instance v0, Lantlr/Lookahead;

    iget-object v1, p0, Lantlr/Lookahead;->fset:Lantlr/collections/impl/BitSet;

    iget-object v2, p1, Lantlr/Lookahead;->fset:Lantlr/collections/impl/BitSet;

    invoke-virtual {v1, v2}, Lantlr/collections/impl/BitSet;->and(Lantlr/collections/impl/BitSet;)Lantlr/collections/impl/BitSet;

    move-result-object v1

    invoke-direct {v0, v1}, Lantlr/Lookahead;-><init>(Lantlr/collections/impl/BitSet;)V

    iget-boolean v1, p0, Lantlr/Lookahead;->hasEpsilon:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p1, Lantlr/Lookahead;->hasEpsilon:Z

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lantlr/Lookahead;->setEpsilon()V

    :cond_0
    return-object v0
.end method

.method public nil()Z
    .locals 1

    iget-object v0, p0, Lantlr/Lookahead;->fset:Lantlr/collections/impl/BitSet;

    invoke-virtual {v0}, Lantlr/collections/impl/BitSet;->nil()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lantlr/Lookahead;->hasEpsilon:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public resetEpsilon()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lantlr/Lookahead;->hasEpsilon:Z

    return-void
.end method

.method public setEpsilon()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lantlr/Lookahead;->hasEpsilon:Z

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    const-string v0, ""

    const-string v1, ""

    const-string v2, ""

    iget-object v3, p0, Lantlr/Lookahead;->fset:Lantlr/collections/impl/BitSet;

    const-string v4, ","

    invoke-virtual {v3, v4}, Lantlr/collections/impl/BitSet;->toString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lantlr/Lookahead;->containsEpsilon()Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v0, "+<epsilon>"

    :cond_0
    iget-object v4, p0, Lantlr/Lookahead;->cycle:Ljava/lang/String;

    if-eqz v4, :cond_1

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v4, "; FOLLOW("

    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v4, p0, Lantlr/Lookahead;->cycle:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v4, ")"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    :cond_1
    iget-object v4, p0, Lantlr/Lookahead;->epsilonDepth:Lantlr/collections/impl/BitSet;

    if-eqz v4, :cond_2

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v4, "; depths="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    iget-object v4, p0, Lantlr/Lookahead;->epsilonDepth:Lantlr/collections/impl/BitSet;

    const-string v5, ","

    invoke-virtual {v4, v5}, Lantlr/collections/impl/BitSet;->toString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    :cond_2
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toString(Ljava/lang/String;Lantlr/CharFormatter;)Ljava/lang/String;
    .locals 6

    const-string v0, ""

    const-string v1, ""

    const-string v2, ""

    iget-object v3, p0, Lantlr/Lookahead;->fset:Lantlr/collections/impl/BitSet;

    invoke-virtual {v3, p1, p2}, Lantlr/collections/impl/BitSet;->toString(Ljava/lang/String;Lantlr/CharFormatter;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lantlr/Lookahead;->containsEpsilon()Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v0, "+<epsilon>"

    :cond_0
    iget-object v4, p0, Lantlr/Lookahead;->cycle:Ljava/lang/String;

    if-eqz v4, :cond_1

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v4, "; FOLLOW("

    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v4, p0, Lantlr/Lookahead;->cycle:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v4, ")"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    :cond_1
    iget-object v4, p0, Lantlr/Lookahead;->epsilonDepth:Lantlr/collections/impl/BitSet;

    if-eqz v4, :cond_2

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v4, "; depths="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    iget-object v4, p0, Lantlr/Lookahead;->epsilonDepth:Lantlr/collections/impl/BitSet;

    const-string v5, ","

    invoke-virtual {v4, v5}, Lantlr/collections/impl/BitSet;->toString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    :cond_2
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toString(Ljava/lang/String;Lantlr/CharFormatter;Lantlr/Grammar;)Ljava/lang/String;
    .locals 1

    instance-of v0, p3, Lantlr/LexerGrammar;

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1, p2}, Lantlr/Lookahead;->toString(Ljava/lang/String;Lantlr/CharFormatter;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p3, Lantlr/Grammar;->tokenManager:Lantlr/TokenManager;

    invoke-interface {v0}, Lantlr/TokenManager;->getVocabulary()Lantlr/collections/impl/Vector;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lantlr/Lookahead;->toString(Ljava/lang/String;Lantlr/collections/impl/Vector;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public toString(Ljava/lang/String;Lantlr/collections/impl/Vector;)Ljava/lang/String;
    .locals 5

    const-string v0, ""

    const-string v1, ""

    iget-object v2, p0, Lantlr/Lookahead;->fset:Lantlr/collections/impl/BitSet;

    invoke-virtual {v2, p1, p2}, Lantlr/collections/impl/BitSet;->toString(Ljava/lang/String;Lantlr/collections/impl/Vector;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lantlr/Lookahead;->cycle:Ljava/lang/String;

    if-eqz v3, :cond_0

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "; FOLLOW("

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-object v3, p0, Lantlr/Lookahead;->cycle:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v3, ")"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_0
    iget-object v3, p0, Lantlr/Lookahead;->epsilonDepth:Lantlr/collections/impl/BitSet;

    if-eqz v3, :cond_1

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "; depths="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v3, p0, Lantlr/Lookahead;->epsilonDepth:Lantlr/collections/impl/BitSet;

    const-string v4, ","

    invoke-virtual {v3, v4}, Lantlr/collections/impl/BitSet;->toString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    :cond_1
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
