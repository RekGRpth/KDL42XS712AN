.class Lantlr/ImportVocabTokenManager;
.super Lantlr/SimpleTokenManager;
.source "ImportVocabTokenManager.java"

# interfaces
.implements Ljava/lang/Cloneable;


# instance fields
.field private filename:Ljava/lang/String;

.field protected grammar:Lantlr/Grammar;


# direct methods
.method constructor <init>(Lantlr/Grammar;Ljava/lang/String;Ljava/lang/String;Lantlr/Tool;)V
    .locals 4

    invoke-direct {p0, p3, p4}, Lantlr/SimpleTokenManager;-><init>(Ljava/lang/String;Lantlr/Tool;)V

    iput-object p1, p0, Lantlr/ImportVocabTokenManager;->grammar:Lantlr/Grammar;

    iput-object p2, p0, Lantlr/ImportVocabTokenManager;->filename:Ljava/lang/String;

    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lantlr/ImportVocabTokenManager;->filename:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lantlr/ImportVocabTokenManager;->antlrTool:Lantlr/Tool;

    invoke-virtual {v1}, Lantlr/Tool;->getOutputDirectory()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lantlr/ImportVocabTokenManager;->filename:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lantlr/ImportVocabTokenManager;->antlrTool:Lantlr/Tool;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "Cannot find importVocab file \'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    iget-object v3, p0, Lantlr/ImportVocabTokenManager;->filename:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, "\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lantlr/Tool;->panic(Ljava/lang/String;)V

    :cond_0
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lantlr/ImportVocabTokenManager;->setReadOnly(Z)V

    :try_start_0
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v2, Ljava/io/FileReader;

    invoke-direct {v2, v0}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    invoke-direct {v1, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    new-instance v0, Lantlr/ANTLRTokdefLexer;

    invoke-direct {v0, v1}, Lantlr/ANTLRTokdefLexer;-><init>(Ljava/io/Reader;)V

    new-instance v1, Lantlr/ANTLRTokdefParser;

    invoke-direct {v1, v0}, Lantlr/ANTLRTokdefParser;-><init>(Lantlr/TokenStream;)V

    iget-object v0, p0, Lantlr/ImportVocabTokenManager;->antlrTool:Lantlr/Tool;

    invoke-virtual {v1, v0}, Lantlr/ANTLRTokdefParser;->setTool(Lantlr/Tool;)V

    iget-object v0, p0, Lantlr/ImportVocabTokenManager;->filename:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lantlr/ANTLRTokdefParser;->setFilename(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Lantlr/ANTLRTokdefParser;->file(Lantlr/ImportVocabTokenManager;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lantlr/RecognitionException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lantlr/TokenStreamException; {:try_start_0 .. :try_end_0} :catch_2

    :goto_0
    return-void

    :catch_0
    move-exception v0

    iget-object v0, p0, Lantlr/ImportVocabTokenManager;->antlrTool:Lantlr/Tool;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "Cannot find importVocab file \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p0, Lantlr/ImportVocabTokenManager;->filename:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lantlr/Tool;->panic(Ljava/lang/String;)V

    goto :goto_0

    :catch_1
    move-exception v0

    iget-object v1, p0, Lantlr/ImportVocabTokenManager;->antlrTool:Lantlr/Tool;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "Error parsing importVocab file \'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    iget-object v3, p0, Lantlr/ImportVocabTokenManager;->filename:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, "\': "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v0}, Lantlr/RecognitionException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lantlr/Tool;->panic(Ljava/lang/String;)V

    goto :goto_0

    :catch_2
    move-exception v0

    iget-object v0, p0, Lantlr/ImportVocabTokenManager;->antlrTool:Lantlr/Tool;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "Error reading importVocab file \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p0, Lantlr/ImportVocabTokenManager;->filename:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lantlr/Tool;->panic(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 2

    invoke-super {p0}, Lantlr/SimpleTokenManager;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lantlr/ImportVocabTokenManager;

    iget-object v1, p0, Lantlr/ImportVocabTokenManager;->filename:Ljava/lang/String;

    iput-object v1, v0, Lantlr/ImportVocabTokenManager;->filename:Ljava/lang/String;

    iget-object v1, p0, Lantlr/ImportVocabTokenManager;->grammar:Lantlr/Grammar;

    iput-object v1, v0, Lantlr/ImportVocabTokenManager;->grammar:Lantlr/Grammar;

    return-object v0
.end method

.method public define(Lantlr/TokenSymbol;)V
    .locals 0

    invoke-super {p0, p1}, Lantlr/SimpleTokenManager;->define(Lantlr/TokenSymbol;)V

    return-void
.end method

.method public define(Ljava/lang/String;I)V
    .locals 2

    const-string v0, "\""

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lantlr/StringLiteralSymbol;

    invoke-direct {v0, p1}, Lantlr/StringLiteralSymbol;-><init>(Ljava/lang/String;)V

    :goto_0
    invoke-virtual {v0, p2}, Lantlr/TokenSymbol;->setTokenType(I)V

    invoke-super {p0, v0}, Lantlr/SimpleTokenManager;->define(Lantlr/TokenSymbol;)V

    add-int/lit8 v0, p2, 0x1

    iget v1, p0, Lantlr/ImportVocabTokenManager;->maxToken:I

    if-le v0, v1, :cond_1

    add-int/lit8 v0, p2, 0x1

    :goto_1
    iput v0, p0, Lantlr/ImportVocabTokenManager;->maxToken:I

    return-void

    :cond_0
    new-instance v0, Lantlr/TokenSymbol;

    invoke-direct {v0, p1}, Lantlr/TokenSymbol;-><init>(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    iget v0, p0, Lantlr/ImportVocabTokenManager;->maxToken:I

    goto :goto_1
.end method

.method public isReadOnly()Z
    .locals 1

    iget-boolean v0, p0, Lantlr/ImportVocabTokenManager;->readOnly:Z

    return v0
.end method

.method public nextTokenType()I
    .locals 1

    invoke-super {p0}, Lantlr/SimpleTokenManager;->nextTokenType()I

    move-result v0

    return v0
.end method
