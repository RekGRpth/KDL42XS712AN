.class Lantlr/StringLiteralElement;
.super Lantlr/GrammarAtom;
.source "StringLiteralElement.java"


# instance fields
.field protected processedAtomText:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lantlr/Grammar;Lantlr/Token;I)V
    .locals 5

    invoke-direct {p0, p1, p2, p3}, Lantlr/GrammarAtom;-><init>(Lantlr/Grammar;Lantlr/Token;I)V

    instance-of v0, p1, Lantlr/LexerGrammar;

    if-nez v0, :cond_0

    iget-object v0, p0, Lantlr/StringLiteralElement;->grammar:Lantlr/Grammar;

    iget-object v0, v0, Lantlr/Grammar;->tokenManager:Lantlr/TokenManager;

    iget-object v1, p0, Lantlr/StringLiteralElement;->atomText:Ljava/lang/String;

    invoke-interface {v0, v1}, Lantlr/TokenManager;->getTokenSymbol(Ljava/lang/String;)Lantlr/TokenSymbol;

    move-result-object v0

    if-nez v0, :cond_2

    iget-object v0, p1, Lantlr/Grammar;->antlrTool:Lantlr/Tool;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "Undefined literal: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    iget-object v2, p0, Lantlr/StringLiteralElement;->atomText:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lantlr/StringLiteralElement;->grammar:Lantlr/Grammar;

    invoke-virtual {v2}, Lantlr/Grammar;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Lantlr/Token;->getLine()I

    move-result v3

    invoke-virtual {p2}, Lantlr/Token;->getColumn()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lantlr/Tool;->error(Ljava/lang/String;Ljava/lang/String;II)V

    :cond_0
    :goto_0
    invoke-virtual {p2}, Lantlr/Token;->getLine()I

    move-result v0

    iput v0, p0, Lantlr/StringLiteralElement;->line:I

    new-instance v0, Ljava/lang/String;

    invoke-direct {v0}, Ljava/lang/String;-><init>()V

    iput-object v0, p0, Lantlr/StringLiteralElement;->processedAtomText:Ljava/lang/String;

    const/4 v0, 0x1

    :goto_1
    iget-object v1, p0, Lantlr/StringLiteralElement;->atomText:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_3

    iget-object v1, p0, Lantlr/StringLiteralElement;->atomText:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v1

    const/16 v2, 0x5c

    if-ne v1, v2, :cond_4

    add-int/lit8 v2, v0, 0x1

    iget-object v3, p0, Lantlr/StringLiteralElement;->atomText:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ge v2, v3, :cond_4

    add-int/lit8 v1, v0, 0x1

    iget-object v0, p0, Lantlr/StringLiteralElement;->atomText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    sparse-switch v0, :sswitch_data_0

    move v2, v1

    move v1, v0

    :goto_2
    instance-of v0, p1, Lantlr/LexerGrammar;

    if-eqz v0, :cond_1

    move-object v0, p1

    check-cast v0, Lantlr/LexerGrammar;

    iget-object v0, v0, Lantlr/LexerGrammar;->charVocabulary:Lantlr/collections/impl/BitSet;

    invoke-virtual {v0, v1}, Lantlr/collections/impl/BitSet;->add(I)V

    :cond_1
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    iget-object v3, p0, Lantlr/StringLiteralElement;->processedAtomText:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lantlr/StringLiteralElement;->processedAtomText:Ljava/lang/String;

    add-int/lit8 v0, v2, 0x1

    goto :goto_1

    :cond_2
    invoke-virtual {v0}, Lantlr/TokenSymbol;->getTokenType()I

    move-result v0

    iput v0, p0, Lantlr/StringLiteralElement;->tokenType:I

    goto :goto_0

    :sswitch_0
    const/16 v0, 0xa

    move v2, v1

    move v1, v0

    goto :goto_2

    :sswitch_1
    const/16 v0, 0xd

    move v2, v1

    move v1, v0

    goto :goto_2

    :sswitch_2
    const/16 v0, 0x9

    move v2, v1

    move v1, v0

    goto :goto_2

    :cond_3
    return-void

    :cond_4
    move v2, v0

    goto :goto_2

    nop

    :sswitch_data_0
    .sparse-switch
        0x6e -> :sswitch_0
        0x72 -> :sswitch_1
        0x74 -> :sswitch_2
    .end sparse-switch
.end method


# virtual methods
.method public generate()V
    .locals 1

    iget-object v0, p0, Lantlr/StringLiteralElement;->grammar:Lantlr/Grammar;

    iget-object v0, v0, Lantlr/Grammar;->generator:Lantlr/CodeGenerator;

    invoke-virtual {v0, p0}, Lantlr/CodeGenerator;->gen(Lantlr/StringLiteralElement;)V

    return-void
.end method

.method public look(I)Lantlr/Lookahead;
    .locals 1

    iget-object v0, p0, Lantlr/StringLiteralElement;->grammar:Lantlr/Grammar;

    iget-object v0, v0, Lantlr/Grammar;->theLLkAnalyzer:Lantlr/LLkGrammarAnalyzer;

    invoke-interface {v0, p1, p0}, Lantlr/LLkGrammarAnalyzer;->look(ILantlr/StringLiteralElement;)Lantlr/Lookahead;

    move-result-object v0

    return-object v0
.end method
