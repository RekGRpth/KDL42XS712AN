.class Lantlr/preprocessor/Grammar;
.super Ljava/lang/Object;
.source "Grammar.java"


# instance fields
.field protected alreadyExpanded:Z

.field protected antlrTool:Lantlr/Tool;

.field protected exportVocab:Ljava/lang/String;

.field protected fileName:Ljava/lang/String;

.field protected hier:Lantlr/preprocessor/Hierarchy;

.field protected importVocab:Ljava/lang/String;

.field protected memberAction:Ljava/lang/String;

.field protected name:Ljava/lang/String;

.field protected options:Lantlr/collections/impl/IndexedVector;

.field protected preambleAction:Ljava/lang/String;

.field protected predefined:Z

.field protected rules:Lantlr/collections/impl/IndexedVector;

.field protected specifiedVocabulary:Z

.field protected superClass:Ljava/lang/String;

.field protected superGrammar:Ljava/lang/String;

.field protected tokenSection:Ljava/lang/String;

.field protected type:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lantlr/Tool;Ljava/lang/String;Ljava/lang/String;Lantlr/collections/impl/IndexedVector;)V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v0, p0, Lantlr/preprocessor/Grammar;->predefined:Z

    iput-boolean v0, p0, Lantlr/preprocessor/Grammar;->alreadyExpanded:Z

    iput-boolean v0, p0, Lantlr/preprocessor/Grammar;->specifiedVocabulary:Z

    iput-object v1, p0, Lantlr/preprocessor/Grammar;->superClass:Ljava/lang/String;

    iput-object v1, p0, Lantlr/preprocessor/Grammar;->importVocab:Ljava/lang/String;

    iput-object v1, p0, Lantlr/preprocessor/Grammar;->exportVocab:Ljava/lang/String;

    iput-object p2, p0, Lantlr/preprocessor/Grammar;->name:Ljava/lang/String;

    iput-object p3, p0, Lantlr/preprocessor/Grammar;->superGrammar:Ljava/lang/String;

    iput-object p4, p0, Lantlr/preprocessor/Grammar;->rules:Lantlr/collections/impl/IndexedVector;

    iput-object p1, p0, Lantlr/preprocessor/Grammar;->antlrTool:Lantlr/Tool;

    return-void
.end method


# virtual methods
.method public addOption(Lantlr/preprocessor/Option;)V
    .locals 2

    iget-object v0, p0, Lantlr/preprocessor/Grammar;->options:Lantlr/collections/impl/IndexedVector;

    if-nez v0, :cond_0

    new-instance v0, Lantlr/collections/impl/IndexedVector;

    invoke-direct {v0}, Lantlr/collections/impl/IndexedVector;-><init>()V

    iput-object v0, p0, Lantlr/preprocessor/Grammar;->options:Lantlr/collections/impl/IndexedVector;

    :cond_0
    iget-object v0, p0, Lantlr/preprocessor/Grammar;->options:Lantlr/collections/impl/IndexedVector;

    invoke-virtual {p1}, Lantlr/preprocessor/Option;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lantlr/collections/impl/IndexedVector;->appendElement(Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method

.method public addRule(Lantlr/preprocessor/Rule;)V
    .locals 2

    iget-object v0, p0, Lantlr/preprocessor/Grammar;->rules:Lantlr/collections/impl/IndexedVector;

    invoke-virtual {p1}, Lantlr/preprocessor/Rule;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lantlr/collections/impl/IndexedVector;->appendElement(Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method

.method public expandInPlace()V
    .locals 6

    const/4 v3, 0x1

    iget-boolean v0, p0, Lantlr/preprocessor/Grammar;->alreadyExpanded:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lantlr/preprocessor/Grammar;->getSuperGrammar()Lantlr/preprocessor/Grammar;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lantlr/preprocessor/Grammar;->exportVocab:Ljava/lang/String;

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lantlr/preprocessor/Grammar;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lantlr/preprocessor/Grammar;->exportVocab:Ljava/lang/String;

    :cond_2
    invoke-virtual {v1}, Lantlr/preprocessor/Grammar;->isPredefined()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {v1}, Lantlr/preprocessor/Grammar;->expandInPlace()V

    iput-boolean v3, p0, Lantlr/preprocessor/Grammar;->alreadyExpanded:Z

    iget-object v0, p0, Lantlr/preprocessor/Grammar;->hier:Lantlr/preprocessor/Hierarchy;

    invoke-virtual {p0}, Lantlr/preprocessor/Grammar;->getFileName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lantlr/preprocessor/Hierarchy;->getFile(Ljava/lang/String;)Lantlr/preprocessor/GrammarFile;

    move-result-object v0

    invoke-virtual {v0, v3}, Lantlr/preprocessor/GrammarFile;->setExpanded(Z)V

    invoke-virtual {v1}, Lantlr/preprocessor/Grammar;->getRules()Lantlr/collections/impl/IndexedVector;

    move-result-object v0

    invoke-virtual {v0}, Lantlr/collections/impl/IndexedVector;->elements()Ljava/util/Enumeration;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lantlr/preprocessor/Rule;

    invoke-virtual {p0, v0, v1}, Lantlr/preprocessor/Grammar;->inherit(Lantlr/preprocessor/Rule;Lantlr/preprocessor/Grammar;)V

    goto :goto_1

    :cond_3
    invoke-virtual {v1}, Lantlr/preprocessor/Grammar;->getOptions()Lantlr/collections/impl/IndexedVector;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lantlr/collections/impl/IndexedVector;->elements()Ljava/util/Enumeration;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lantlr/preprocessor/Option;

    invoke-virtual {p0, v0, v1}, Lantlr/preprocessor/Grammar;->inherit(Lantlr/preprocessor/Option;Lantlr/preprocessor/Grammar;)V

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lantlr/preprocessor/Grammar;->options:Lantlr/collections/impl/IndexedVector;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lantlr/preprocessor/Grammar;->options:Lantlr/collections/impl/IndexedVector;

    const-string v2, "importVocab"

    invoke-virtual {v0, v2}, Lantlr/collections/impl/IndexedVector;->getElement(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_6

    :cond_5
    iget-object v0, p0, Lantlr/preprocessor/Grammar;->options:Lantlr/collections/impl/IndexedVector;

    if-nez v0, :cond_7

    :cond_6
    new-instance v0, Lantlr/preprocessor/Option;

    const-string v2, "importVocab"

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    iget-object v4, v1, Lantlr/preprocessor/Grammar;->exportVocab:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string v4, ";"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3, p0}, Lantlr/preprocessor/Option;-><init>(Ljava/lang/String;Ljava/lang/String;Lantlr/preprocessor/Grammar;)V

    invoke-virtual {p0, v0}, Lantlr/preprocessor/Grammar;->addOption(Lantlr/preprocessor/Option;)V

    invoke-virtual {v1}, Lantlr/preprocessor/Grammar;->getFileName()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lantlr/preprocessor/Grammar;->antlrTool:Lantlr/Tool;

    invoke-virtual {v2, v0}, Lantlr/Tool;->pathToFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    iget-object v3, v1, Lantlr/preprocessor/Grammar;->exportVocab:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    sget-object v3, Lantlr/CodeGenerator;->TokenTypesFileSuffix:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    sget-object v3, Lantlr/CodeGenerator;->TokenTypesFileExt:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lantlr/preprocessor/Grammar;->antlrTool:Lantlr/Tool;

    invoke-virtual {v3, v2}, Lantlr/Tool;->fileMinusPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    const-string v5, "."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v5, "file.separator"

    invoke-static {v5}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    :cond_7
    :goto_3
    iget-object v0, v1, Lantlr/preprocessor/Grammar;->memberAction:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lantlr/preprocessor/Grammar;->inherit(Ljava/lang/String;Lantlr/preprocessor/Grammar;)V

    goto/16 :goto_0

    :cond_8
    :try_start_0
    iget-object v0, p0, Lantlr/preprocessor/Grammar;->antlrTool:Lantlr/Tool;

    invoke-virtual {v0, v2, v3}, Lantlr/Tool;->copyFile(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    :catch_0
    move-exception v0

    iget-object v0, p0, Lantlr/preprocessor/Grammar;->antlrTool:Lantlr/Tool;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "cannot find/copy importVocab file "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lantlr/Tool;->toolError(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public getFileName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lantlr/preprocessor/Grammar;->fileName:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lantlr/preprocessor/Grammar;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getOptions()Lantlr/collections/impl/IndexedVector;
    .locals 1

    iget-object v0, p0, Lantlr/preprocessor/Grammar;->options:Lantlr/collections/impl/IndexedVector;

    return-object v0
.end method

.method public getRules()Lantlr/collections/impl/IndexedVector;
    .locals 1

    iget-object v0, p0, Lantlr/preprocessor/Grammar;->rules:Lantlr/collections/impl/IndexedVector;

    return-object v0
.end method

.method public getSuperGrammar()Lantlr/preprocessor/Grammar;
    .locals 2

    iget-object v0, p0, Lantlr/preprocessor/Grammar;->superGrammar:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lantlr/preprocessor/Grammar;->hier:Lantlr/preprocessor/Hierarchy;

    iget-object v1, p0, Lantlr/preprocessor/Grammar;->superGrammar:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lantlr/preprocessor/Hierarchy;->getGrammar(Ljava/lang/String;)Lantlr/preprocessor/Grammar;

    move-result-object v0

    goto :goto_0
.end method

.method public getSuperGrammarName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lantlr/preprocessor/Grammar;->superGrammar:Ljava/lang/String;

    return-object v0
.end method

.method public getType()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lantlr/preprocessor/Grammar;->type:Ljava/lang/String;

    return-object v0
.end method

.method public inherit(Lantlr/preprocessor/Option;Lantlr/preprocessor/Grammar;)V
    .locals 2

    invoke-virtual {p1}, Lantlr/preprocessor/Option;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "importVocab"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lantlr/preprocessor/Option;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "exportVocab"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x0

    iget-object v1, p0, Lantlr/preprocessor/Grammar;->options:Lantlr/collections/impl/IndexedVector;

    if-eqz v1, :cond_2

    iget-object v0, p0, Lantlr/preprocessor/Grammar;->options:Lantlr/collections/impl/IndexedVector;

    invoke-virtual {p1}, Lantlr/preprocessor/Option;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lantlr/collections/impl/IndexedVector;->getElement(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lantlr/preprocessor/Option;

    :cond_2
    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Lantlr/preprocessor/Grammar;->addOption(Lantlr/preprocessor/Option;)V

    goto :goto_0
.end method

.method public inherit(Lantlr/preprocessor/Rule;Lantlr/preprocessor/Grammar;)V
    .locals 4

    iget-object v0, p0, Lantlr/preprocessor/Grammar;->rules:Lantlr/collections/impl/IndexedVector;

    invoke-virtual {p1}, Lantlr/preprocessor/Rule;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lantlr/collections/impl/IndexedVector;->getElement(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lantlr/preprocessor/Rule;

    if-eqz v0, :cond_1

    invoke-virtual {v0, p1}, Lantlr/preprocessor/Rule;->sameSignature(Lantlr/preprocessor/Rule;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lantlr/preprocessor/Grammar;->antlrTool:Lantlr/Tool;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "rule "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {p0}, Lantlr/preprocessor/Grammar;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v0}, Lantlr/preprocessor/Rule;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, " has different signature than "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {p2}, Lantlr/preprocessor/Grammar;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v0}, Lantlr/preprocessor/Rule;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lantlr/Tool;->warning(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0, p1}, Lantlr/preprocessor/Grammar;->addRule(Lantlr/preprocessor/Rule;)V

    goto :goto_0
.end method

.method public inherit(Ljava/lang/String;Lantlr/preprocessor/Grammar;)V
    .locals 1

    iget-object v0, p0, Lantlr/preprocessor/Grammar;->memberAction:Ljava/lang/String;

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p1, :cond_0

    iput-object p1, p0, Lantlr/preprocessor/Grammar;->memberAction:Ljava/lang/String;

    goto :goto_0
.end method

.method public isPredefined()Z
    .locals 1

    iget-boolean v0, p0, Lantlr/preprocessor/Grammar;->predefined:Z

    return v0
.end method

.method public setFileName(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lantlr/preprocessor/Grammar;->fileName:Ljava/lang/String;

    return-void
.end method

.method public setHierarchy(Lantlr/preprocessor/Hierarchy;)V
    .locals 0

    iput-object p1, p0, Lantlr/preprocessor/Grammar;->hier:Lantlr/preprocessor/Hierarchy;

    return-void
.end method

.method public setMemberAction(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lantlr/preprocessor/Grammar;->memberAction:Ljava/lang/String;

    return-void
.end method

.method public setOptions(Lantlr/collections/impl/IndexedVector;)V
    .locals 0

    iput-object p1, p0, Lantlr/preprocessor/Grammar;->options:Lantlr/collections/impl/IndexedVector;

    return-void
.end method

.method public setPreambleAction(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lantlr/preprocessor/Grammar;->preambleAction:Ljava/lang/String;

    return-void
.end method

.method public setPredefined(Z)V
    .locals 0

    iput-boolean p1, p0, Lantlr/preprocessor/Grammar;->predefined:Z

    return-void
.end method

.method public setTokenSection(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lantlr/preprocessor/Grammar;->tokenSection:Ljava/lang/String;

    return-void
.end method

.method public setType(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lantlr/preprocessor/Grammar;->type:Ljava/lang/String;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    new-instance v2, Ljava/lang/StringBuffer;

    const/16 v0, 0x2710

    invoke-direct {v2, v0}, Ljava/lang/StringBuffer;-><init>(I)V

    iget-object v0, p0, Lantlr/preprocessor/Grammar;->preambleAction:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lantlr/preprocessor/Grammar;->preambleAction:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_0
    iget-object v0, p0, Lantlr/preprocessor/Grammar;->superGrammar:Ljava/lang/String;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string v1, "class "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-object v1, p0, Lantlr/preprocessor/Grammar;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lantlr/preprocessor/Grammar;->superClass:Ljava/lang/String;

    if-eqz v0, :cond_6

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string v1, "class "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-object v1, p0, Lantlr/preprocessor/Grammar;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, " extends "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-object v1, p0, Lantlr/preprocessor/Grammar;->superClass:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :goto_1
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string v1, "line.separator"

    invoke-static {v1}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, "line.separator"

    invoke-static {v1}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-object v0, p0, Lantlr/preprocessor/Grammar;->options:Lantlr/collections/impl/IndexedVector;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lantlr/preprocessor/Grammar;->options:Lantlr/collections/impl/IndexedVector;

    invoke-static {v0}, Lantlr/preprocessor/Hierarchy;->optionsToString(Lantlr/collections/impl/IndexedVector;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_2
    iget-object v0, p0, Lantlr/preprocessor/Grammar;->tokenSection:Ljava/lang/String;

    if-eqz v0, :cond_3

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    iget-object v1, p0, Lantlr/preprocessor/Grammar;->tokenSection:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_3
    iget-object v0, p0, Lantlr/preprocessor/Grammar;->memberAction:Ljava/lang/String;

    if-eqz v0, :cond_4

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    iget-object v1, p0, Lantlr/preprocessor/Grammar;->memberAction:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, "line.separator"

    invoke-static {v1}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_4
    const/4 v0, 0x0

    move v1, v0

    :goto_2
    iget-object v0, p0, Lantlr/preprocessor/Grammar;->rules:Lantlr/collections/impl/IndexedVector;

    invoke-virtual {v0}, Lantlr/collections/impl/IndexedVector;->size()I

    move-result v0

    if-ge v1, v0, :cond_7

    iget-object v0, p0, Lantlr/preprocessor/Grammar;->rules:Lantlr/collections/impl/IndexedVector;

    invoke-virtual {v0, v1}, Lantlr/collections/impl/IndexedVector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lantlr/preprocessor/Rule;

    invoke-virtual {p0}, Lantlr/preprocessor/Grammar;->getName()Ljava/lang/String;

    move-result-object v3

    iget-object v4, v0, Lantlr/preprocessor/Rule;->enclosingGrammar:Lantlr/preprocessor/Grammar;

    invoke-virtual {v4}, Lantlr/preprocessor/Grammar;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    const-string v4, "// inherited from grammar "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    iget-object v4, v0, Lantlr/preprocessor/Rule;->enclosingGrammar:Lantlr/preprocessor/Grammar;

    invoke-virtual {v4}, Lantlr/preprocessor/Grammar;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string v4, "line.separator"

    invoke-static {v4}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_5
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v3, "line.separator"

    invoke-static {v3}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v3, "line.separator"

    invoke-static {v3}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_6
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string v1, "class "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-object v1, p0, Lantlr/preprocessor/Grammar;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, " extends "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-object v1, p0, Lantlr/preprocessor/Grammar;->type:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto/16 :goto_1

    :cond_7
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0
.end method
