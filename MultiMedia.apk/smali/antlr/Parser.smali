.class public abstract Lantlr/Parser;
.super Ljava/lang/Object;
.source "Parser.java"


# instance fields
.field protected astFactory:Lantlr/ASTFactory;

.field private ignoreInvalidDebugCalls:Z

.field protected inputState:Lantlr/ParserSharedInputState;

.field protected returnAST:Lantlr/collections/AST;

.field protected tokenNames:[Ljava/lang/String;

.field protected tokenTypeToASTClassMap:Ljava/util/Hashtable;

.field protected traceDepth:I


# direct methods
.method public constructor <init>()V
    .locals 1

    new-instance v0, Lantlr/ParserSharedInputState;

    invoke-direct {v0}, Lantlr/ParserSharedInputState;-><init>()V

    invoke-direct {p0, v0}, Lantlr/Parser;-><init>(Lantlr/ParserSharedInputState;)V

    return-void
.end method

.method public constructor <init>(Lantlr/ParserSharedInputState;)V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lantlr/Parser;->astFactory:Lantlr/ASTFactory;

    iput-object v1, p0, Lantlr/Parser;->tokenTypeToASTClassMap:Ljava/util/Hashtable;

    iput-boolean v0, p0, Lantlr/Parser;->ignoreInvalidDebugCalls:Z

    iput v0, p0, Lantlr/Parser;->traceDepth:I

    iput-object p1, p0, Lantlr/Parser;->inputState:Lantlr/ParserSharedInputState;

    return-void
.end method

.method public static panic()V
    .locals 2

    sget-object v0, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string v1, "Parser: panic"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/System;->exit(I)V

    return-void
.end method


# virtual methods
.method public abstract LA(I)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/TokenStreamException;
        }
    .end annotation
.end method

.method public abstract LT(I)Lantlr/Token;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/TokenStreamException;
        }
    .end annotation
.end method

.method public addMessageListener(Lantlr/debug/MessageListener;)V
    .locals 2

    iget-boolean v0, p0, Lantlr/Parser;->ignoreInvalidDebugCalls:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "addMessageListener() is only valid if parser built for debugging"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method public addParserListener(Lantlr/debug/ParserListener;)V
    .locals 2

    iget-boolean v0, p0, Lantlr/Parser;->ignoreInvalidDebugCalls:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "addParserListener() is only valid if parser built for debugging"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method public addParserMatchListener(Lantlr/debug/ParserMatchListener;)V
    .locals 2

    iget-boolean v0, p0, Lantlr/Parser;->ignoreInvalidDebugCalls:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "addParserMatchListener() is only valid if parser built for debugging"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method public addParserTokenListener(Lantlr/debug/ParserTokenListener;)V
    .locals 2

    iget-boolean v0, p0, Lantlr/Parser;->ignoreInvalidDebugCalls:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "addParserTokenListener() is only valid if parser built for debugging"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method public addSemanticPredicateListener(Lantlr/debug/SemanticPredicateListener;)V
    .locals 2

    iget-boolean v0, p0, Lantlr/Parser;->ignoreInvalidDebugCalls:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "addSemanticPredicateListener() is only valid if parser built for debugging"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method public addSyntacticPredicateListener(Lantlr/debug/SyntacticPredicateListener;)V
    .locals 2

    iget-boolean v0, p0, Lantlr/Parser;->ignoreInvalidDebugCalls:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "addSyntacticPredicateListener() is only valid if parser built for debugging"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method public addTraceListener(Lantlr/debug/TraceListener;)V
    .locals 2

    iget-boolean v0, p0, Lantlr/Parser;->ignoreInvalidDebugCalls:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "addTraceListener() is only valid if parser built for debugging"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method public abstract consume()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/TokenStreamException;
        }
    .end annotation
.end method

.method public consumeUntil(I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {p0, v1}, Lantlr/Parser;->LA(I)I

    move-result v0

    if-eq v0, v1, :cond_0

    invoke-virtual {p0, v1}, Lantlr/Parser;->LA(I)I

    move-result v0

    if-eq v0, p1, :cond_0

    invoke-virtual {p0}, Lantlr/Parser;->consume()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public consumeUntil(Lantlr/collections/impl/BitSet;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {p0, v1}, Lantlr/Parser;->LA(I)I

    move-result v0

    if-eq v0, v1, :cond_0

    invoke-virtual {p0, v1}, Lantlr/Parser;->LA(I)I

    move-result v0

    invoke-virtual {p1, v0}, Lantlr/collections/impl/BitSet;->member(I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lantlr/Parser;->consume()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method protected defaultDebuggingSetup(Lantlr/TokenStream;Lantlr/TokenBuffer;)V
    .locals 0

    return-void
.end method

.method public getAST()Lantlr/collections/AST;
    .locals 1

    iget-object v0, p0, Lantlr/Parser;->returnAST:Lantlr/collections/AST;

    return-object v0
.end method

.method public getASTFactory()Lantlr/ASTFactory;
    .locals 1

    iget-object v0, p0, Lantlr/Parser;->astFactory:Lantlr/ASTFactory;

    return-object v0
.end method

.method public getFilename()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lantlr/Parser;->inputState:Lantlr/ParserSharedInputState;

    iget-object v0, v0, Lantlr/ParserSharedInputState;->filename:Ljava/lang/String;

    return-object v0
.end method

.method public getInputState()Lantlr/ParserSharedInputState;
    .locals 1

    iget-object v0, p0, Lantlr/Parser;->inputState:Lantlr/ParserSharedInputState;

    return-object v0
.end method

.method public getTokenName(I)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lantlr/Parser;->tokenNames:[Ljava/lang/String;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public getTokenNames()[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lantlr/Parser;->tokenNames:[Ljava/lang/String;

    return-object v0
.end method

.method public getTokenTypeToASTClassMap()Ljava/util/Hashtable;
    .locals 1

    iget-object v0, p0, Lantlr/Parser;->tokenTypeToASTClassMap:Ljava/util/Hashtable;

    return-object v0
.end method

.method public isDebugMode()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public mark()I
    .locals 1

    iget-object v0, p0, Lantlr/Parser;->inputState:Lantlr/ParserSharedInputState;

    iget-object v0, v0, Lantlr/ParserSharedInputState;->input:Lantlr/TokenBuffer;

    invoke-virtual {v0}, Lantlr/TokenBuffer;->mark()I

    move-result v0

    return v0
.end method

.method public match(I)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/MismatchedTokenException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lantlr/Parser;->LA(I)I

    move-result v0

    if-eq v0, p1, :cond_0

    new-instance v0, Lantlr/MismatchedTokenException;

    iget-object v1, p0, Lantlr/Parser;->tokenNames:[Ljava/lang/String;

    invoke-virtual {p0, v2}, Lantlr/Parser;->LT(I)Lantlr/Token;

    move-result-object v2

    const/4 v4, 0x0

    invoke-virtual {p0}, Lantlr/Parser;->getFilename()Ljava/lang/String;

    move-result-object v5

    move v3, p1

    invoke-direct/range {v0 .. v5}, Lantlr/MismatchedTokenException;-><init>([Ljava/lang/String;Lantlr/Token;IZLjava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {p0}, Lantlr/Parser;->consume()V

    return-void
.end method

.method public match(Lantlr/collections/impl/BitSet;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/MismatchedTokenException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lantlr/Parser;->LA(I)I

    move-result v0

    invoke-virtual {p1, v0}, Lantlr/collections/impl/BitSet;->member(I)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lantlr/MismatchedTokenException;

    iget-object v1, p0, Lantlr/Parser;->tokenNames:[Ljava/lang/String;

    invoke-virtual {p0, v2}, Lantlr/Parser;->LT(I)Lantlr/Token;

    move-result-object v2

    const/4 v4, 0x0

    invoke-virtual {p0}, Lantlr/Parser;->getFilename()Ljava/lang/String;

    move-result-object v5

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, Lantlr/MismatchedTokenException;-><init>([Ljava/lang/String;Lantlr/Token;Lantlr/collections/impl/BitSet;ZLjava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {p0}, Lantlr/Parser;->consume()V

    return-void
.end method

.method public matchNot(I)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/MismatchedTokenException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/4 v4, 0x1

    invoke-virtual {p0, v4}, Lantlr/Parser;->LA(I)I

    move-result v0

    if-ne v0, p1, :cond_0

    new-instance v0, Lantlr/MismatchedTokenException;

    iget-object v1, p0, Lantlr/Parser;->tokenNames:[Ljava/lang/String;

    invoke-virtual {p0, v4}, Lantlr/Parser;->LT(I)Lantlr/Token;

    move-result-object v2

    invoke-virtual {p0}, Lantlr/Parser;->getFilename()Ljava/lang/String;

    move-result-object v5

    move v3, p1

    invoke-direct/range {v0 .. v5}, Lantlr/MismatchedTokenException;-><init>([Ljava/lang/String;Lantlr/Token;IZLjava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {p0}, Lantlr/Parser;->consume()V

    return-void
.end method

.method public removeMessageListener(Lantlr/debug/MessageListener;)V
    .locals 2

    iget-boolean v0, p0, Lantlr/Parser;->ignoreInvalidDebugCalls:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "removeMessageListener() is only valid if parser built for debugging"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method public removeParserListener(Lantlr/debug/ParserListener;)V
    .locals 2

    iget-boolean v0, p0, Lantlr/Parser;->ignoreInvalidDebugCalls:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "removeParserListener() is only valid if parser built for debugging"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method public removeParserMatchListener(Lantlr/debug/ParserMatchListener;)V
    .locals 2

    iget-boolean v0, p0, Lantlr/Parser;->ignoreInvalidDebugCalls:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "removeParserMatchListener() is only valid if parser built for debugging"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method public removeParserTokenListener(Lantlr/debug/ParserTokenListener;)V
    .locals 2

    iget-boolean v0, p0, Lantlr/Parser;->ignoreInvalidDebugCalls:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "removeParserTokenListener() is only valid if parser built for debugging"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method public removeSemanticPredicateListener(Lantlr/debug/SemanticPredicateListener;)V
    .locals 2

    iget-boolean v0, p0, Lantlr/Parser;->ignoreInvalidDebugCalls:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "removeSemanticPredicateListener() is only valid if parser built for debugging"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method public removeSyntacticPredicateListener(Lantlr/debug/SyntacticPredicateListener;)V
    .locals 2

    iget-boolean v0, p0, Lantlr/Parser;->ignoreInvalidDebugCalls:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "removeSyntacticPredicateListener() is only valid if parser built for debugging"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method public removeTraceListener(Lantlr/debug/TraceListener;)V
    .locals 2

    iget-boolean v0, p0, Lantlr/Parser;->ignoreInvalidDebugCalls:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "removeTraceListener() is only valid if parser built for debugging"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method public reportError(Lantlr/RecognitionException;)V
    .locals 1

    sget-object v0, Ljava/lang/System;->err:Ljava/io/PrintStream;

    invoke-virtual {v0, p1}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    return-void
.end method

.method public reportError(Ljava/lang/String;)V
    .locals 3

    invoke-virtual {p0}, Lantlr/Parser;->getFilename()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    sget-object v0, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {p0}, Lantlr/Parser;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, ": error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public reportWarning(Ljava/lang/String;)V
    .locals 3

    invoke-virtual {p0}, Lantlr/Parser;->getFilename()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "warning: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    sget-object v0, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {p0}, Lantlr/Parser;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, ": warning: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public rewind(I)V
    .locals 1

    iget-object v0, p0, Lantlr/Parser;->inputState:Lantlr/ParserSharedInputState;

    iget-object v0, v0, Lantlr/ParserSharedInputState;->input:Lantlr/TokenBuffer;

    invoke-virtual {v0, p1}, Lantlr/TokenBuffer;->rewind(I)V

    return-void
.end method

.method public setASTFactory(Lantlr/ASTFactory;)V
    .locals 0

    iput-object p1, p0, Lantlr/Parser;->astFactory:Lantlr/ASTFactory;

    return-void
.end method

.method public setASTNodeClass(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lantlr/Parser;->astFactory:Lantlr/ASTFactory;

    invoke-virtual {v0, p1}, Lantlr/ASTFactory;->setASTNodeType(Ljava/lang/String;)V

    return-void
.end method

.method public setASTNodeType(Ljava/lang/String;)V
    .locals 0

    invoke-virtual {p0, p1}, Lantlr/Parser;->setASTNodeClass(Ljava/lang/String;)V

    return-void
.end method

.method public setDebugMode(Z)V
    .locals 2

    iget-boolean v0, p0, Lantlr/Parser;->ignoreInvalidDebugCalls:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "setDebugMode() only valid if parser built for debugging"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method public setFilename(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lantlr/Parser;->inputState:Lantlr/ParserSharedInputState;

    iput-object p1, v0, Lantlr/ParserSharedInputState;->filename:Ljava/lang/String;

    return-void
.end method

.method public setIgnoreInvalidDebugCalls(Z)V
    .locals 0

    iput-boolean p1, p0, Lantlr/Parser;->ignoreInvalidDebugCalls:Z

    return-void
.end method

.method public setInputState(Lantlr/ParserSharedInputState;)V
    .locals 0

    iput-object p1, p0, Lantlr/Parser;->inputState:Lantlr/ParserSharedInputState;

    return-void
.end method

.method public setTokenBuffer(Lantlr/TokenBuffer;)V
    .locals 1

    iget-object v0, p0, Lantlr/Parser;->inputState:Lantlr/ParserSharedInputState;

    iput-object p1, v0, Lantlr/ParserSharedInputState;->input:Lantlr/TokenBuffer;

    return-void
.end method

.method public traceIn(Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/TokenStreamException;
        }
    .end annotation

    iget v0, p0, Lantlr/Parser;->traceDepth:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lantlr/Parser;->traceDepth:I

    invoke-virtual {p0}, Lantlr/Parser;->traceIndent()V

    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "> "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v2, "; LA(1)=="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lantlr/Parser;->LT(I)Lantlr/Token;

    move-result-object v2

    invoke-virtual {v2}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    iget-object v0, p0, Lantlr/Parser;->inputState:Lantlr/ParserSharedInputState;

    iget v0, v0, Lantlr/ParserSharedInputState;->guessing:I

    if-lez v0, :cond_0

    const-string v0, " [guessing]"

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    return-void

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public traceIndent()V
    .locals 3

    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Lantlr/Parser;->traceDepth:I

    if-ge v0, v1, :cond_0

    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public traceOut(Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/TokenStreamException;
        }
    .end annotation

    invoke-virtual {p0}, Lantlr/Parser;->traceIndent()V

    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "< "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v2, "; LA(1)=="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lantlr/Parser;->LT(I)Lantlr/Token;

    move-result-object v2

    invoke-virtual {v2}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    iget-object v0, p0, Lantlr/Parser;->inputState:Lantlr/ParserSharedInputState;

    iget v0, v0, Lantlr/ParserSharedInputState;->guessing:I

    if-lez v0, :cond_0

    const-string v0, " [guessing]"

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    iget v0, p0, Lantlr/Parser;->traceDepth:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lantlr/Parser;->traceDepth:I

    return-void

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method
