.class Lantlr/TreeSpecifierNode;
.super Ljava/lang/Object;
.source "TreeSpecifierNode.java"


# instance fields
.field private firstChild:Lantlr/TreeSpecifierNode;

.field private nextSibling:Lantlr/TreeSpecifierNode;

.field private parent:Lantlr/TreeSpecifierNode;

.field private tok:Lantlr/Token;


# direct methods
.method constructor <init>(Lantlr/Token;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lantlr/TreeSpecifierNode;->parent:Lantlr/TreeSpecifierNode;

    iput-object v0, p0, Lantlr/TreeSpecifierNode;->firstChild:Lantlr/TreeSpecifierNode;

    iput-object v0, p0, Lantlr/TreeSpecifierNode;->nextSibling:Lantlr/TreeSpecifierNode;

    iput-object p1, p0, Lantlr/TreeSpecifierNode;->tok:Lantlr/Token;

    return-void
.end method


# virtual methods
.method public getFirstChild()Lantlr/TreeSpecifierNode;
    .locals 1

    iget-object v0, p0, Lantlr/TreeSpecifierNode;->firstChild:Lantlr/TreeSpecifierNode;

    return-object v0
.end method

.method public getNextSibling()Lantlr/TreeSpecifierNode;
    .locals 1

    iget-object v0, p0, Lantlr/TreeSpecifierNode;->nextSibling:Lantlr/TreeSpecifierNode;

    return-object v0
.end method

.method public getParent()Lantlr/TreeSpecifierNode;
    .locals 1

    iget-object v0, p0, Lantlr/TreeSpecifierNode;->parent:Lantlr/TreeSpecifierNode;

    return-object v0
.end method

.method public getToken()Lantlr/Token;
    .locals 1

    iget-object v0, p0, Lantlr/TreeSpecifierNode;->tok:Lantlr/Token;

    return-object v0
.end method

.method public setFirstChild(Lantlr/TreeSpecifierNode;)V
    .locals 0

    iput-object p1, p0, Lantlr/TreeSpecifierNode;->firstChild:Lantlr/TreeSpecifierNode;

    iput-object p0, p1, Lantlr/TreeSpecifierNode;->parent:Lantlr/TreeSpecifierNode;

    return-void
.end method

.method public setNextSibling(Lantlr/TreeSpecifierNode;)V
    .locals 1

    iput-object p1, p0, Lantlr/TreeSpecifierNode;->nextSibling:Lantlr/TreeSpecifierNode;

    iget-object v0, p0, Lantlr/TreeSpecifierNode;->parent:Lantlr/TreeSpecifierNode;

    iput-object v0, p1, Lantlr/TreeSpecifierNode;->parent:Lantlr/TreeSpecifierNode;

    return-void
.end method
