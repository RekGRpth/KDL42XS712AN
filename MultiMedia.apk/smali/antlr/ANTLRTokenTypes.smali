.class public interface abstract Lantlr/ANTLRTokenTypes;
.super Ljava/lang/Object;
.source "ANTLRTokenTypes.java"


# static fields
.field public static final ACTION:I = 0x7

.field public static final ARG_ACTION:I = 0x22

.field public static final ASSIGN:I = 0xf

.field public static final BANG:I = 0x21

.field public static final CARET:I = 0x31

.field public static final CHAR_LITERAL:I = 0x13

.field public static final CLOSE_ELEMENT_OPTION:I = 0x1a

.field public static final COLON:I = 0x24

.field public static final COMMA:I = 0x26

.field public static final COMMENT:I = 0x35

.field public static final DIGIT:I = 0x39

.field public static final DOC_COMMENT:I = 0x8

.field public static final EOF:I = 0x1

.field public static final ESC:I = 0x38

.field public static final IMPLIES:I = 0x30

.field public static final INT:I = 0x14

.field public static final INTERNAL_RULE_REF:I = 0x3e

.field public static final LITERAL_Lexer:I = 0xc

.field public static final LITERAL_Parser:I = 0x1d

.field public static final LITERAL_TreeParser:I = 0xd

.field public static final LITERAL_catch:I = 0x28

.field public static final LITERAL_charVocabulary:I = 0x12

.field public static final LITERAL_class:I = 0xa

.field public static final LITERAL_exception:I = 0x27

.field public static final LITERAL_extends:I = 0xb

.field public static final LITERAL_header:I = 0x5

.field public static final LITERAL_lexclass:I = 0x9

.field public static final LITERAL_options:I = 0x33

.field public static final LITERAL_private:I = 0x20

.field public static final LITERAL_protected:I = 0x1e

.field public static final LITERAL_public:I = 0x1f

.field public static final LITERAL_returns:I = 0x23

.field public static final LITERAL_throws:I = 0x25

.field public static final LITERAL_tokens:I = 0x4

.field public static final LPAREN:I = 0x1b

.field public static final ML_COMMENT:I = 0x37

.field public static final NESTED_ACTION:I = 0x3c

.field public static final NESTED_ARG_ACTION:I = 0x3b

.field public static final NOT_OP:I = 0x2a

.field public static final NULL_TREE_LOOKAHEAD:I = 0x3

.field public static final OPEN_ELEMENT_OPTION:I = 0x19

.field public static final OPTIONS:I = 0xe

.field public static final OR:I = 0x15

.field public static final PLUS:I = 0x2f

.field public static final QUESTION:I = 0x2d

.field public static final RANGE:I = 0x16

.field public static final RCURLY:I = 0x11

.field public static final RPAREN:I = 0x1c

.field public static final RULE_REF:I = 0x29

.field public static final SEMI:I = 0x10

.field public static final SEMPRED:I = 0x2b

.field public static final SL_COMMENT:I = 0x36

.field public static final STAR:I = 0x2e

.field public static final STRING_LITERAL:I = 0x6

.field public static final TOKENS:I = 0x17

.field public static final TOKEN_REF:I = 0x18

.field public static final TREE_BEGIN:I = 0x2c

.field public static final WILDCARD:I = 0x32

.field public static final WS:I = 0x34

.field public static final WS_LOOP:I = 0x3d

.field public static final WS_OPT:I = 0x3f

.field public static final XDIGIT:I = 0x3a
