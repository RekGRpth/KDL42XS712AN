.class public Lantlr/ASTIterator;
.super Ljava/lang/Object;
.source "ASTIterator.java"


# instance fields
.field protected cursor:Lantlr/collections/AST;

.field protected original:Lantlr/collections/AST;


# direct methods
.method public constructor <init>(Lantlr/collections/AST;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lantlr/ASTIterator;->cursor:Lantlr/collections/AST;

    iput-object v0, p0, Lantlr/ASTIterator;->original:Lantlr/collections/AST;

    iput-object p1, p0, Lantlr/ASTIterator;->cursor:Lantlr/collections/AST;

    iput-object p1, p0, Lantlr/ASTIterator;->original:Lantlr/collections/AST;

    return-void
.end method


# virtual methods
.method public isSubtree(Lantlr/collections/AST;Lantlr/collections/AST;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-nez p2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-nez p1, :cond_3

    if-eqz p2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_2
    invoke-interface {p1}, Lantlr/collections/AST;->getNextSibling()Lantlr/collections/AST;

    move-result-object p1

    invoke-interface {p2}, Lantlr/collections/AST;->getNextSibling()Lantlr/collections/AST;

    move-result-object p2

    :cond_3
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    invoke-interface {p1}, Lantlr/collections/AST;->getType()I

    move-result v2

    invoke-interface {p2}, Lantlr/collections/AST;->getType()I

    move-result v3

    if-eq v2, v3, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    invoke-interface {p1}, Lantlr/collections/AST;->getFirstChild()Lantlr/collections/AST;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-interface {p1}, Lantlr/collections/AST;->getFirstChild()Lantlr/collections/AST;

    move-result-object v2

    invoke-interface {p2}, Lantlr/collections/AST;->getFirstChild()Lantlr/collections/AST;

    move-result-object v3

    invoke-virtual {p0, v2, v3}, Lantlr/ASTIterator;->isSubtree(Lantlr/collections/AST;Lantlr/collections/AST;)Z

    move-result v2

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0
.end method

.method public next(Lantlr/collections/AST;)Lantlr/collections/AST;
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lantlr/ASTIterator;->cursor:Lantlr/collections/AST;

    if-nez v1, :cond_2

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v1, p0, Lantlr/ASTIterator;->cursor:Lantlr/collections/AST;

    invoke-interface {v1}, Lantlr/collections/AST;->getNextSibling()Lantlr/collections/AST;

    move-result-object v1

    iput-object v1, p0, Lantlr/ASTIterator;->cursor:Lantlr/collections/AST;

    :cond_2
    iget-object v1, p0, Lantlr/ASTIterator;->cursor:Lantlr/collections/AST;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lantlr/ASTIterator;->cursor:Lantlr/collections/AST;

    invoke-interface {v1}, Lantlr/collections/AST;->getType()I

    move-result v1

    invoke-interface {p1}, Lantlr/collections/AST;->getType()I

    move-result v2

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lantlr/ASTIterator;->cursor:Lantlr/collections/AST;

    invoke-interface {v1}, Lantlr/collections/AST;->getFirstChild()Lantlr/collections/AST;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lantlr/ASTIterator;->cursor:Lantlr/collections/AST;

    invoke-interface {v1}, Lantlr/collections/AST;->getFirstChild()Lantlr/collections/AST;

    move-result-object v1

    invoke-interface {p1}, Lantlr/collections/AST;->getFirstChild()Lantlr/collections/AST;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lantlr/ASTIterator;->isSubtree(Lantlr/collections/AST;Lantlr/collections/AST;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v0, p0, Lantlr/ASTIterator;->cursor:Lantlr/collections/AST;

    goto :goto_0
.end method
