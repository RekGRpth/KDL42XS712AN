.class public Lantlr/debug/misc/ASTFrame;
.super Ljavax/swing/JFrame;
.source "ASTFrame.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lantlr/debug/misc/ASTFrame$MyTreeSelectionListener;
    }
.end annotation


# static fields
.field static final HEIGHT:I = 0x12c

.field static final WIDTH:I = 0xc8


# direct methods
.method public constructor <init>(Ljava/lang/String;Lantlr/collections/AST;)V
    .locals 3

    invoke-direct {p0, p1}, Ljavax/swing/JFrame;-><init>(Ljava/lang/String;)V

    new-instance v0, Lantlr/debug/misc/ASTFrame$MyTreeSelectionListener;

    invoke-direct {v0, p0}, Lantlr/debug/misc/ASTFrame$MyTreeSelectionListener;-><init>(Lantlr/debug/misc/ASTFrame;)V

    new-instance v0, Lantlr/debug/misc/JTreeASTPanel;

    new-instance v1, Lantlr/debug/misc/JTreeASTModel;

    invoke-direct {v1, p2}, Lantlr/debug/misc/JTreeASTModel;-><init>(Lantlr/collections/AST;)V

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lantlr/debug/misc/JTreeASTPanel;-><init>(Ljavax/swing/tree/TreeModel;Ljavax/swing/event/TreeSelectionListener;)V

    invoke-virtual {p0}, Lantlr/debug/misc/ASTFrame;->getContentPane()Ljava/awt/Container;

    move-result-object v1

    const-string v2, "Center"

    invoke-virtual {v1, v0, v2}, Ljava/awt/Container;->add(Ljava/awt/Component;Ljava/lang/Object;)V

    new-instance v0, Lantlr/debug/misc/ASTFrame$1;

    invoke-direct {v0, p0}, Lantlr/debug/misc/ASTFrame$1;-><init>(Lantlr/debug/misc/ASTFrame;)V

    invoke-virtual {p0, v0}, Lantlr/debug/misc/ASTFrame;->addWindowListener(Ljava/awt/event/WindowListener;)V

    const/16 v0, 0xc8

    const/16 v1, 0x12c

    invoke-virtual {p0, v0, v1}, Lantlr/debug/misc/ASTFrame;->setSize(II)V

    return-void
.end method

.method public static main([Ljava/lang/String;)V
    .locals 4

    const/4 v3, 0x0

    new-instance v2, Lantlr/ASTFactory;

    invoke-direct {v2}, Lantlr/ASTFactory;-><init>()V

    const-string v0, "ROOT"

    invoke-virtual {v2, v3, v0}, Lantlr/ASTFactory;->create(ILjava/lang/String;)Lantlr/collections/AST;

    move-result-object v0

    check-cast v0, Lantlr/CommonAST;

    const-string v1, "C1"

    invoke-virtual {v2, v3, v1}, Lantlr/ASTFactory;->create(ILjava/lang/String;)Lantlr/collections/AST;

    move-result-object v1

    check-cast v1, Lantlr/CommonAST;

    invoke-virtual {v0, v1}, Lantlr/CommonAST;->addChild(Lantlr/collections/AST;)V

    const-string v1, "C2"

    invoke-virtual {v2, v3, v1}, Lantlr/ASTFactory;->create(ILjava/lang/String;)Lantlr/collections/AST;

    move-result-object v1

    check-cast v1, Lantlr/CommonAST;

    invoke-virtual {v0, v1}, Lantlr/CommonAST;->addChild(Lantlr/collections/AST;)V

    const-string v1, "C3"

    invoke-virtual {v2, v3, v1}, Lantlr/ASTFactory;->create(ILjava/lang/String;)Lantlr/collections/AST;

    move-result-object v1

    check-cast v1, Lantlr/CommonAST;

    invoke-virtual {v0, v1}, Lantlr/CommonAST;->addChild(Lantlr/collections/AST;)V

    new-instance v1, Lantlr/debug/misc/ASTFrame;

    const-string v2, "AST JTree Example"

    invoke-direct {v1, v2, v0}, Lantlr/debug/misc/ASTFrame;-><init>(Ljava/lang/String;Lantlr/collections/AST;)V

    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Lantlr/debug/misc/ASTFrame;->setVisible(Z)V

    return-void
.end method
