.class public Lantlr/debug/misc/JTreeASTPanel;
.super Ljavax/swing/JPanel;
.source "JTreeASTPanel.java"


# instance fields
.field tree:Ljavax/swing/JTree;


# direct methods
.method public constructor <init>(Ljavax/swing/tree/TreeModel;Ljavax/swing/event/TreeSelectionListener;)V
    .locals 3

    invoke-direct {p0}, Ljavax/swing/JPanel;-><init>()V

    new-instance v0, Ljava/awt/BorderLayout;

    invoke-direct {v0}, Ljava/awt/BorderLayout;-><init>()V

    invoke-virtual {p0, v0}, Lantlr/debug/misc/JTreeASTPanel;->setLayout(Ljava/awt/LayoutManager;)V

    new-instance v0, Ljavax/swing/JTree;

    invoke-direct {v0, p1}, Ljavax/swing/JTree;-><init>(Ljavax/swing/tree/TreeModel;)V

    iput-object v0, p0, Lantlr/debug/misc/JTreeASTPanel;->tree:Ljavax/swing/JTree;

    iget-object v0, p0, Lantlr/debug/misc/JTreeASTPanel;->tree:Ljavax/swing/JTree;

    const-string v1, "JTree.lineStyle"

    const-string v2, "Angled"

    invoke-virtual {v0, v1, v2}, Ljavax/swing/JTree;->putClientProperty(Ljava/lang/Object;Ljava/lang/Object;)V

    if-eqz p2, :cond_0

    iget-object v0, p0, Lantlr/debug/misc/JTreeASTPanel;->tree:Ljavax/swing/JTree;

    invoke-virtual {v0, p2}, Ljavax/swing/JTree;->addTreeSelectionListener(Ljavax/swing/event/TreeSelectionListener;)V

    :cond_0
    new-instance v0, Ljavax/swing/JScrollPane;

    invoke-direct {v0}, Ljavax/swing/JScrollPane;-><init>()V

    invoke-virtual {v0}, Ljavax/swing/JScrollPane;->getViewport()Ljavax/swing/JViewport;

    move-result-object v1

    iget-object v2, p0, Lantlr/debug/misc/JTreeASTPanel;->tree:Ljavax/swing/JTree;

    invoke-virtual {v1, v2}, Ljavax/swing/JViewport;->add(Ljava/awt/Component;)Ljava/awt/Component;

    const-string v1, "Center"

    invoke-virtual {p0, v0, v1}, Lantlr/debug/misc/JTreeASTPanel;->add(Ljava/awt/Component;Ljava/lang/Object;)V

    return-void
.end method
