.class public Lantlr/debug/InputBufferEventSupport;
.super Ljava/lang/Object;
.source "InputBufferEventSupport.java"


# static fields
.field protected static final CONSUME:I = 0x0

.field protected static final LA:I = 0x1

.field protected static final MARK:I = 0x2

.field protected static final REWIND:I = 0x3


# instance fields
.field private inputBufferEvent:Lantlr/debug/InputBufferEvent;

.field private inputBufferListeners:Ljava/util/Vector;

.field private source:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Ljava/lang/Object;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lantlr/debug/InputBufferEvent;

    invoke-direct {v0, p1}, Lantlr/debug/InputBufferEvent;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lantlr/debug/InputBufferEventSupport;->inputBufferEvent:Lantlr/debug/InputBufferEvent;

    iput-object p1, p0, Lantlr/debug/InputBufferEventSupport;->source:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public addInputBufferListener(Lantlr/debug/InputBufferListener;)V
    .locals 1

    iget-object v0, p0, Lantlr/debug/InputBufferEventSupport;->inputBufferListeners:Ljava/util/Vector;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lantlr/debug/InputBufferEventSupport;->inputBufferListeners:Ljava/util/Vector;

    :cond_0
    iget-object v0, p0, Lantlr/debug/InputBufferEventSupport;->inputBufferListeners:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    return-void
.end method

.method public fireConsume(C)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lantlr/debug/InputBufferEventSupport;->inputBufferEvent:Lantlr/debug/InputBufferEvent;

    invoke-virtual {v0, v1, p1, v1}, Lantlr/debug/InputBufferEvent;->setValues(ICI)V

    iget-object v0, p0, Lantlr/debug/InputBufferEventSupport;->inputBufferListeners:Ljava/util/Vector;

    invoke-virtual {p0, v1, v0}, Lantlr/debug/InputBufferEventSupport;->fireEvents(ILjava/util/Vector;)V

    return-void
.end method

.method public fireEvent(ILantlr/debug/ListenerBase;)V
    .locals 3

    packed-switch p1, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const-string v2, "bad type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const-string v2, " for fireEvent()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    check-cast p2, Lantlr/debug/InputBufferListener;

    iget-object v0, p0, Lantlr/debug/InputBufferEventSupport;->inputBufferEvent:Lantlr/debug/InputBufferEvent;

    invoke-interface {p2, v0}, Lantlr/debug/InputBufferListener;->inputBufferConsume(Lantlr/debug/InputBufferEvent;)V

    :goto_0
    return-void

    :pswitch_1
    check-cast p2, Lantlr/debug/InputBufferListener;

    iget-object v0, p0, Lantlr/debug/InputBufferEventSupport;->inputBufferEvent:Lantlr/debug/InputBufferEvent;

    invoke-interface {p2, v0}, Lantlr/debug/InputBufferListener;->inputBufferLA(Lantlr/debug/InputBufferEvent;)V

    goto :goto_0

    :pswitch_2
    check-cast p2, Lantlr/debug/InputBufferListener;

    iget-object v0, p0, Lantlr/debug/InputBufferEventSupport;->inputBufferEvent:Lantlr/debug/InputBufferEvent;

    invoke-interface {p2, v0}, Lantlr/debug/InputBufferListener;->inputBufferMark(Lantlr/debug/InputBufferEvent;)V

    goto :goto_0

    :pswitch_3
    check-cast p2, Lantlr/debug/InputBufferListener;

    iget-object v0, p0, Lantlr/debug/InputBufferEventSupport;->inputBufferEvent:Lantlr/debug/InputBufferEvent;

    invoke-interface {p2, v0}, Lantlr/debug/InputBufferListener;->inputBufferRewind(Lantlr/debug/InputBufferEvent;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public fireEvents(ILjava/util/Vector;)V
    .locals 3

    monitor-enter p0

    if-nez p2, :cond_1

    :try_start_0
    monitor-exit p0

    :cond_0
    return-void

    :cond_1
    invoke-virtual {p2}, Ljava/util/Vector;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Vector;

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v1

    if-ge v2, v1, :cond_0

    invoke-virtual {v0, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lantlr/debug/ListenerBase;

    invoke-virtual {p0, p1, v1}, Lantlr/debug/InputBufferEventSupport;->fireEvent(ILantlr/debug/ListenerBase;)V

    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public fireLA(CI)V
    .locals 2

    const/4 v1, 0x1

    iget-object v0, p0, Lantlr/debug/InputBufferEventSupport;->inputBufferEvent:Lantlr/debug/InputBufferEvent;

    invoke-virtual {v0, v1, p1, p2}, Lantlr/debug/InputBufferEvent;->setValues(ICI)V

    iget-object v0, p0, Lantlr/debug/InputBufferEventSupport;->inputBufferListeners:Ljava/util/Vector;

    invoke-virtual {p0, v1, v0}, Lantlr/debug/InputBufferEventSupport;->fireEvents(ILjava/util/Vector;)V

    return-void
.end method

.method public fireMark(I)V
    .locals 3

    const/4 v2, 0x2

    iget-object v0, p0, Lantlr/debug/InputBufferEventSupport;->inputBufferEvent:Lantlr/debug/InputBufferEvent;

    const/16 v1, 0x20

    invoke-virtual {v0, v2, v1, p1}, Lantlr/debug/InputBufferEvent;->setValues(ICI)V

    iget-object v0, p0, Lantlr/debug/InputBufferEventSupport;->inputBufferListeners:Ljava/util/Vector;

    invoke-virtual {p0, v2, v0}, Lantlr/debug/InputBufferEventSupport;->fireEvents(ILjava/util/Vector;)V

    return-void
.end method

.method public fireRewind(I)V
    .locals 3

    const/4 v2, 0x3

    iget-object v0, p0, Lantlr/debug/InputBufferEventSupport;->inputBufferEvent:Lantlr/debug/InputBufferEvent;

    const/16 v1, 0x20

    invoke-virtual {v0, v2, v1, p1}, Lantlr/debug/InputBufferEvent;->setValues(ICI)V

    iget-object v0, p0, Lantlr/debug/InputBufferEventSupport;->inputBufferListeners:Ljava/util/Vector;

    invoke-virtual {p0, v2, v0}, Lantlr/debug/InputBufferEventSupport;->fireEvents(ILjava/util/Vector;)V

    return-void
.end method

.method public getInputBufferListeners()Ljava/util/Vector;
    .locals 1

    iget-object v0, p0, Lantlr/debug/InputBufferEventSupport;->inputBufferListeners:Ljava/util/Vector;

    return-object v0
.end method

.method protected refresh(Ljava/util/Vector;)V
    .locals 3

    monitor-enter p1

    :try_start_0
    invoke-virtual {p1}, Ljava/util/Vector;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Vector;

    monitor-exit p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v1

    if-ge v2, v1, :cond_0

    invoke-virtual {v0, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lantlr/debug/ListenerBase;

    invoke-interface {v1}, Lantlr/debug/ListenerBase;->refresh()V

    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_0
    return-void
.end method

.method public refreshListeners()V
    .locals 1

    iget-object v0, p0, Lantlr/debug/InputBufferEventSupport;->inputBufferListeners:Ljava/util/Vector;

    invoke-virtual {p0, v0}, Lantlr/debug/InputBufferEventSupport;->refresh(Ljava/util/Vector;)V

    return-void
.end method

.method public removeInputBufferListener(Lantlr/debug/InputBufferListener;)V
    .locals 1

    iget-object v0, p0, Lantlr/debug/InputBufferEventSupport;->inputBufferListeners:Ljava/util/Vector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lantlr/debug/InputBufferEventSupport;->inputBufferListeners:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->removeElement(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method
