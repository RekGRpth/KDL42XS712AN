.class public interface abstract Lantlr/debug/InputBufferListener;
.super Ljava/lang/Object;
.source "InputBufferListener.java"

# interfaces
.implements Lantlr/debug/ListenerBase;


# virtual methods
.method public abstract inputBufferConsume(Lantlr/debug/InputBufferEvent;)V
.end method

.method public abstract inputBufferLA(Lantlr/debug/InputBufferEvent;)V
.end method

.method public abstract inputBufferMark(Lantlr/debug/InputBufferEvent;)V
.end method

.method public abstract inputBufferRewind(Lantlr/debug/InputBufferEvent;)V
.end method
