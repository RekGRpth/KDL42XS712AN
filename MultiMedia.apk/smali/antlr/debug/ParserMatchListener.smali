.class public interface abstract Lantlr/debug/ParserMatchListener;
.super Ljava/lang/Object;
.source "ParserMatchListener.java"

# interfaces
.implements Lantlr/debug/ListenerBase;


# virtual methods
.method public abstract parserMatch(Lantlr/debug/ParserMatchEvent;)V
.end method

.method public abstract parserMatchNot(Lantlr/debug/ParserMatchEvent;)V
.end method

.method public abstract parserMismatch(Lantlr/debug/ParserMatchEvent;)V
.end method

.method public abstract parserMismatchNot(Lantlr/debug/ParserMatchEvent;)V
.end method
