.class public Lantlr/debug/ParseTreeDebugParser;
.super Lantlr/LLkParser;
.source "ParseTreeDebugParser.java"


# instance fields
.field protected currentParseTreeRoot:Ljava/util/Stack;

.field protected mostRecentParseTreeRoot:Lantlr/ParseTreeRule;

.field protected numberOfDerivationSteps:I


# direct methods
.method public constructor <init>(I)V
    .locals 1

    invoke-direct {p0, p1}, Lantlr/LLkParser;-><init>(I)V

    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lantlr/debug/ParseTreeDebugParser;->currentParseTreeRoot:Ljava/util/Stack;

    const/4 v0, 0x0

    iput-object v0, p0, Lantlr/debug/ParseTreeDebugParser;->mostRecentParseTreeRoot:Lantlr/ParseTreeRule;

    const/4 v0, 0x1

    iput v0, p0, Lantlr/debug/ParseTreeDebugParser;->numberOfDerivationSteps:I

    return-void
.end method

.method public constructor <init>(Lantlr/ParserSharedInputState;I)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lantlr/LLkParser;-><init>(Lantlr/ParserSharedInputState;I)V

    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lantlr/debug/ParseTreeDebugParser;->currentParseTreeRoot:Ljava/util/Stack;

    const/4 v0, 0x0

    iput-object v0, p0, Lantlr/debug/ParseTreeDebugParser;->mostRecentParseTreeRoot:Lantlr/ParseTreeRule;

    const/4 v0, 0x1

    iput v0, p0, Lantlr/debug/ParseTreeDebugParser;->numberOfDerivationSteps:I

    return-void
.end method

.method public constructor <init>(Lantlr/TokenBuffer;I)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lantlr/LLkParser;-><init>(Lantlr/TokenBuffer;I)V

    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lantlr/debug/ParseTreeDebugParser;->currentParseTreeRoot:Ljava/util/Stack;

    const/4 v0, 0x0

    iput-object v0, p0, Lantlr/debug/ParseTreeDebugParser;->mostRecentParseTreeRoot:Lantlr/ParseTreeRule;

    const/4 v0, 0x1

    iput v0, p0, Lantlr/debug/ParseTreeDebugParser;->numberOfDerivationSteps:I

    return-void
.end method

.method public constructor <init>(Lantlr/TokenStream;I)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lantlr/LLkParser;-><init>(Lantlr/TokenStream;I)V

    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lantlr/debug/ParseTreeDebugParser;->currentParseTreeRoot:Ljava/util/Stack;

    const/4 v0, 0x0

    iput-object v0, p0, Lantlr/debug/ParseTreeDebugParser;->mostRecentParseTreeRoot:Lantlr/ParseTreeRule;

    const/4 v0, 0x1

    iput v0, p0, Lantlr/debug/ParseTreeDebugParser;->numberOfDerivationSteps:I

    return-void
.end method


# virtual methods
.method protected addCurrentTokenToParseTree()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/4 v2, 0x1

    iget-object v0, p0, Lantlr/debug/ParseTreeDebugParser;->inputState:Lantlr/ParserSharedInputState;

    iget v0, v0, Lantlr/ParserSharedInputState;->guessing:I

    if-lez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lantlr/debug/ParseTreeDebugParser;->currentParseTreeRoot:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lantlr/ParseTreeRule;

    invoke-virtual {p0, v2}, Lantlr/debug/ParseTreeDebugParser;->LA(I)I

    move-result v1

    if-ne v1, v2, :cond_1

    new-instance v1, Lantlr/ParseTreeToken;

    new-instance v2, Lantlr/CommonToken;

    const-string v3, "EOF"

    invoke-direct {v2, v3}, Lantlr/CommonToken;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2}, Lantlr/ParseTreeToken;-><init>(Lantlr/Token;)V

    :goto_1
    invoke-virtual {v0, v1}, Lantlr/ParseTreeRule;->addChild(Lantlr/collections/AST;)V

    goto :goto_0

    :cond_1
    new-instance v1, Lantlr/ParseTreeToken;

    invoke-virtual {p0, v2}, Lantlr/debug/ParseTreeDebugParser;->LT(I)Lantlr/Token;

    move-result-object v2

    invoke-direct {v1, v2}, Lantlr/ParseTreeToken;-><init>(Lantlr/Token;)V

    goto :goto_1
.end method

.method public getNumberOfDerivationSteps()I
    .locals 1

    iget v0, p0, Lantlr/debug/ParseTreeDebugParser;->numberOfDerivationSteps:I

    return v0
.end method

.method public getParseTree()Lantlr/ParseTree;
    .locals 1

    iget-object v0, p0, Lantlr/debug/ParseTreeDebugParser;->mostRecentParseTreeRoot:Lantlr/ParseTreeRule;

    return-object v0
.end method

.method public match(I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/MismatchedTokenException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    invoke-virtual {p0}, Lantlr/debug/ParseTreeDebugParser;->addCurrentTokenToParseTree()V

    invoke-super {p0, p1}, Lantlr/LLkParser;->match(I)V

    return-void
.end method

.method public match(Lantlr/collections/impl/BitSet;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/MismatchedTokenException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    invoke-virtual {p0}, Lantlr/debug/ParseTreeDebugParser;->addCurrentTokenToParseTree()V

    invoke-super {p0, p1}, Lantlr/LLkParser;->match(Lantlr/collections/impl/BitSet;)V

    return-void
.end method

.method public matchNot(I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/MismatchedTokenException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    invoke-virtual {p0}, Lantlr/debug/ParseTreeDebugParser;->addCurrentTokenToParseTree()V

    invoke-super {p0, p1}, Lantlr/LLkParser;->matchNot(I)V

    return-void
.end method

.method public traceIn(Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/TokenStreamException;
        }
    .end annotation

    iget-object v0, p0, Lantlr/debug/ParseTreeDebugParser;->inputState:Lantlr/ParserSharedInputState;

    iget v0, v0, Lantlr/ParserSharedInputState;->guessing:I

    if-lez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v1, Lantlr/ParseTreeRule;

    invoke-direct {v1, p1}, Lantlr/ParseTreeRule;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lantlr/debug/ParseTreeDebugParser;->currentParseTreeRoot:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->size()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lantlr/debug/ParseTreeDebugParser;->currentParseTreeRoot:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lantlr/ParseTreeRule;

    invoke-virtual {v0, v1}, Lantlr/ParseTreeRule;->addChild(Lantlr/collections/AST;)V

    :cond_1
    iget-object v0, p0, Lantlr/debug/ParseTreeDebugParser;->currentParseTreeRoot:Ljava/util/Stack;

    invoke-virtual {v0, v1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    iget v0, p0, Lantlr/debug/ParseTreeDebugParser;->numberOfDerivationSteps:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lantlr/debug/ParseTreeDebugParser;->numberOfDerivationSteps:I

    goto :goto_0
.end method

.method public traceOut(Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/TokenStreamException;
        }
    .end annotation

    iget-object v0, p0, Lantlr/debug/ParseTreeDebugParser;->inputState:Lantlr/ParserSharedInputState;

    iget v0, v0, Lantlr/ParserSharedInputState;->guessing:I

    if-lez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lantlr/debug/ParseTreeDebugParser;->currentParseTreeRoot:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lantlr/ParseTreeRule;

    iput-object v0, p0, Lantlr/debug/ParseTreeDebugParser;->mostRecentParseTreeRoot:Lantlr/ParseTreeRule;

    goto :goto_0
.end method
