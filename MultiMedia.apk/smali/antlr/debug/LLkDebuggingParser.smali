.class public Lantlr/debug/LLkDebuggingParser;
.super Lantlr/LLkParser;
.source "LLkDebuggingParser.java"

# interfaces
.implements Lantlr/debug/DebuggingParser;


# static fields
.field static class$antlr$TokenBuffer:Ljava/lang/Class;

.field static class$antlr$TokenStream:Ljava/lang/Class;

.field static class$antlr$debug$LLkDebuggingParser:Ljava/lang/Class;


# instance fields
.field private _notDebugMode:Z

.field protected parserEventSupport:Lantlr/debug/ParserEventSupport;

.field protected ruleNames:[Ljava/lang/String;

.field protected semPredNames:[Ljava/lang/String;


# direct methods
.method public constructor <init>(I)V
    .locals 1

    invoke-direct {p0, p1}, Lantlr/LLkParser;-><init>(I)V

    new-instance v0, Lantlr/debug/ParserEventSupport;

    invoke-direct {v0, p0}, Lantlr/debug/ParserEventSupport;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lantlr/debug/LLkDebuggingParser;->parserEventSupport:Lantlr/debug/ParserEventSupport;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lantlr/debug/LLkDebuggingParser;->_notDebugMode:Z

    return-void
.end method

.method public constructor <init>(Lantlr/ParserSharedInputState;I)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lantlr/LLkParser;-><init>(Lantlr/ParserSharedInputState;I)V

    new-instance v0, Lantlr/debug/ParserEventSupport;

    invoke-direct {v0, p0}, Lantlr/debug/ParserEventSupport;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lantlr/debug/LLkDebuggingParser;->parserEventSupport:Lantlr/debug/ParserEventSupport;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lantlr/debug/LLkDebuggingParser;->_notDebugMode:Z

    return-void
.end method

.method public constructor <init>(Lantlr/TokenBuffer;I)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lantlr/LLkParser;-><init>(Lantlr/TokenBuffer;I)V

    new-instance v0, Lantlr/debug/ParserEventSupport;

    invoke-direct {v0, p0}, Lantlr/debug/ParserEventSupport;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lantlr/debug/LLkDebuggingParser;->parserEventSupport:Lantlr/debug/ParserEventSupport;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lantlr/debug/LLkDebuggingParser;->_notDebugMode:Z

    return-void
.end method

.method public constructor <init>(Lantlr/TokenStream;I)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lantlr/LLkParser;-><init>(Lantlr/TokenStream;I)V

    new-instance v0, Lantlr/debug/ParserEventSupport;

    invoke-direct {v0, p0}, Lantlr/debug/ParserEventSupport;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lantlr/debug/LLkDebuggingParser;->parserEventSupport:Lantlr/debug/ParserEventSupport;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lantlr/debug/LLkDebuggingParser;->_notDebugMode:Z

    return-void
.end method

.method static class$(Ljava/lang/String;)Ljava/lang/Class;
    .locals 2

    :try_start_0
    invoke-static {p0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/NoClassDefFoundError;

    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NoClassDefFoundError;-><init>(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method public LA(I)I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/TokenStreamException;
        }
    .end annotation

    invoke-super {p0, p1}, Lantlr/LLkParser;->LA(I)I

    move-result v0

    iget-object v1, p0, Lantlr/debug/LLkDebuggingParser;->parserEventSupport:Lantlr/debug/ParserEventSupport;

    invoke-virtual {v1, p1, v0}, Lantlr/debug/ParserEventSupport;->fireLA(II)V

    return v0
.end method

.method public addMessageListener(Lantlr/debug/MessageListener;)V
    .locals 1

    iget-object v0, p0, Lantlr/debug/LLkDebuggingParser;->parserEventSupport:Lantlr/debug/ParserEventSupport;

    invoke-virtual {v0, p1}, Lantlr/debug/ParserEventSupport;->addMessageListener(Lantlr/debug/MessageListener;)V

    return-void
.end method

.method public addParserListener(Lantlr/debug/ParserListener;)V
    .locals 1

    iget-object v0, p0, Lantlr/debug/LLkDebuggingParser;->parserEventSupport:Lantlr/debug/ParserEventSupport;

    invoke-virtual {v0, p1}, Lantlr/debug/ParserEventSupport;->addParserListener(Lantlr/debug/ParserListener;)V

    return-void
.end method

.method public addParserMatchListener(Lantlr/debug/ParserMatchListener;)V
    .locals 1

    iget-object v0, p0, Lantlr/debug/LLkDebuggingParser;->parserEventSupport:Lantlr/debug/ParserEventSupport;

    invoke-virtual {v0, p1}, Lantlr/debug/ParserEventSupport;->addParserMatchListener(Lantlr/debug/ParserMatchListener;)V

    return-void
.end method

.method public addParserTokenListener(Lantlr/debug/ParserTokenListener;)V
    .locals 1

    iget-object v0, p0, Lantlr/debug/LLkDebuggingParser;->parserEventSupport:Lantlr/debug/ParserEventSupport;

    invoke-virtual {v0, p1}, Lantlr/debug/ParserEventSupport;->addParserTokenListener(Lantlr/debug/ParserTokenListener;)V

    return-void
.end method

.method public addSemanticPredicateListener(Lantlr/debug/SemanticPredicateListener;)V
    .locals 1

    iget-object v0, p0, Lantlr/debug/LLkDebuggingParser;->parserEventSupport:Lantlr/debug/ParserEventSupport;

    invoke-virtual {v0, p1}, Lantlr/debug/ParserEventSupport;->addSemanticPredicateListener(Lantlr/debug/SemanticPredicateListener;)V

    return-void
.end method

.method public addSyntacticPredicateListener(Lantlr/debug/SyntacticPredicateListener;)V
    .locals 1

    iget-object v0, p0, Lantlr/debug/LLkDebuggingParser;->parserEventSupport:Lantlr/debug/ParserEventSupport;

    invoke-virtual {v0, p1}, Lantlr/debug/ParserEventSupport;->addSyntacticPredicateListener(Lantlr/debug/SyntacticPredicateListener;)V

    return-void
.end method

.method public addTraceListener(Lantlr/debug/TraceListener;)V
    .locals 1

    iget-object v0, p0, Lantlr/debug/LLkDebuggingParser;->parserEventSupport:Lantlr/debug/ParserEventSupport;

    invoke-virtual {v0, p1}, Lantlr/debug/ParserEventSupport;->addTraceListener(Lantlr/debug/TraceListener;)V

    return-void
.end method

.method public consume()V
    .locals 2

    const/16 v0, -0x63

    const/4 v1, 0x1

    :try_start_0
    invoke-virtual {p0, v1}, Lantlr/debug/LLkDebuggingParser;->LA(I)I
    :try_end_0
    .catch Lantlr/TokenStreamException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_0
    invoke-super {p0}, Lantlr/LLkParser;->consume()V

    iget-object v1, p0, Lantlr/debug/LLkDebuggingParser;->parserEventSupport:Lantlr/debug/ParserEventSupport;

    invoke-virtual {v1, v0}, Lantlr/debug/ParserEventSupport;->fireConsume(I)V

    return-void

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method protected fireEnterRule(II)V
    .locals 2

    invoke-virtual {p0}, Lantlr/debug/LLkDebuggingParser;->isDebugMode()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lantlr/debug/LLkDebuggingParser;->parserEventSupport:Lantlr/debug/ParserEventSupport;

    iget-object v1, p0, Lantlr/debug/LLkDebuggingParser;->inputState:Lantlr/ParserSharedInputState;

    iget v1, v1, Lantlr/ParserSharedInputState;->guessing:I

    invoke-virtual {v0, p1, v1, p2}, Lantlr/debug/ParserEventSupport;->fireEnterRule(III)V

    :cond_0
    return-void
.end method

.method protected fireExitRule(II)V
    .locals 2

    invoke-virtual {p0}, Lantlr/debug/LLkDebuggingParser;->isDebugMode()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lantlr/debug/LLkDebuggingParser;->parserEventSupport:Lantlr/debug/ParserEventSupport;

    iget-object v1, p0, Lantlr/debug/LLkDebuggingParser;->inputState:Lantlr/ParserSharedInputState;

    iget v1, v1, Lantlr/ParserSharedInputState;->guessing:I

    invoke-virtual {v0, p1, v1, p2}, Lantlr/debug/ParserEventSupport;->fireExitRule(III)V

    :cond_0
    return-void
.end method

.method protected fireSemanticPredicateEvaluated(IIZ)Z
    .locals 2

    invoke-virtual {p0}, Lantlr/debug/LLkDebuggingParser;->isDebugMode()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lantlr/debug/LLkDebuggingParser;->parserEventSupport:Lantlr/debug/ParserEventSupport;

    iget-object v1, p0, Lantlr/debug/LLkDebuggingParser;->inputState:Lantlr/ParserSharedInputState;

    iget v1, v1, Lantlr/ParserSharedInputState;->guessing:I

    invoke-virtual {v0, p1, p2, p3, v1}, Lantlr/debug/ParserEventSupport;->fireSemanticPredicateEvaluated(IIZI)Z

    move-result p3

    :cond_0
    return p3
.end method

.method protected fireSyntacticPredicateFailed()V
    .locals 2

    invoke-virtual {p0}, Lantlr/debug/LLkDebuggingParser;->isDebugMode()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lantlr/debug/LLkDebuggingParser;->parserEventSupport:Lantlr/debug/ParserEventSupport;

    iget-object v1, p0, Lantlr/debug/LLkDebuggingParser;->inputState:Lantlr/ParserSharedInputState;

    iget v1, v1, Lantlr/ParserSharedInputState;->guessing:I

    invoke-virtual {v0, v1}, Lantlr/debug/ParserEventSupport;->fireSyntacticPredicateFailed(I)V

    :cond_0
    return-void
.end method

.method protected fireSyntacticPredicateStarted()V
    .locals 2

    invoke-virtual {p0}, Lantlr/debug/LLkDebuggingParser;->isDebugMode()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lantlr/debug/LLkDebuggingParser;->parserEventSupport:Lantlr/debug/ParserEventSupport;

    iget-object v1, p0, Lantlr/debug/LLkDebuggingParser;->inputState:Lantlr/ParserSharedInputState;

    iget v1, v1, Lantlr/ParserSharedInputState;->guessing:I

    invoke-virtual {v0, v1}, Lantlr/debug/ParserEventSupport;->fireSyntacticPredicateStarted(I)V

    :cond_0
    return-void
.end method

.method protected fireSyntacticPredicateSucceeded()V
    .locals 2

    invoke-virtual {p0}, Lantlr/debug/LLkDebuggingParser;->isDebugMode()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lantlr/debug/LLkDebuggingParser;->parserEventSupport:Lantlr/debug/ParserEventSupport;

    iget-object v1, p0, Lantlr/debug/LLkDebuggingParser;->inputState:Lantlr/ParserSharedInputState;

    iget v1, v1, Lantlr/ParserSharedInputState;->guessing:I

    invoke-virtual {v0, v1}, Lantlr/debug/ParserEventSupport;->fireSyntacticPredicateSucceeded(I)V

    :cond_0
    return-void
.end method

.method public getRuleName(I)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lantlr/debug/LLkDebuggingParser;->ruleNames:[Ljava/lang/String;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public getSemPredName(I)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lantlr/debug/LLkDebuggingParser;->semPredNames:[Ljava/lang/String;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public declared-synchronized goToSleep()V
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :catch_0
    move-exception v0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public isDebugMode()Z
    .locals 1

    iget-boolean v0, p0, Lantlr/debug/LLkDebuggingParser;->_notDebugMode:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isGuessing()Z
    .locals 1

    iget-object v0, p0, Lantlr/debug/LLkDebuggingParser;->inputState:Lantlr/ParserSharedInputState;

    iget v0, v0, Lantlr/ParserSharedInputState;->guessing:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public match(I)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/MismatchedTokenException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lantlr/debug/LLkDebuggingParser;->LT(I)Lantlr/Token;

    move-result-object v0

    invoke-virtual {v0}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v2}, Lantlr/debug/LLkDebuggingParser;->LA(I)I

    move-result v2

    :try_start_0
    invoke-super {p0, p1}, Lantlr/LLkParser;->match(I)V

    iget-object v0, p0, Lantlr/debug/LLkDebuggingParser;->parserEventSupport:Lantlr/debug/ParserEventSupport;

    iget-object v3, p0, Lantlr/debug/LLkDebuggingParser;->inputState:Lantlr/ParserSharedInputState;

    iget v3, v3, Lantlr/ParserSharedInputState;->guessing:I

    invoke-virtual {v0, p1, v1, v3}, Lantlr/debug/ParserEventSupport;->fireMatch(ILjava/lang/String;I)V
    :try_end_0
    .catch Lantlr/MismatchedTokenException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    iget-object v3, p0, Lantlr/debug/LLkDebuggingParser;->inputState:Lantlr/ParserSharedInputState;

    iget v3, v3, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v3, :cond_0

    iget-object v3, p0, Lantlr/debug/LLkDebuggingParser;->parserEventSupport:Lantlr/debug/ParserEventSupport;

    iget-object v4, p0, Lantlr/debug/LLkDebuggingParser;->inputState:Lantlr/ParserSharedInputState;

    iget v4, v4, Lantlr/ParserSharedInputState;->guessing:I

    invoke-virtual {v3, v2, p1, v1, v4}, Lantlr/debug/ParserEventSupport;->fireMismatch(IILjava/lang/String;I)V

    :cond_0
    throw v0
.end method

.method public match(Lantlr/collections/impl/BitSet;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/MismatchedTokenException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lantlr/debug/LLkDebuggingParser;->LT(I)Lantlr/Token;

    move-result-object v0

    invoke-virtual {v0}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v2}, Lantlr/debug/LLkDebuggingParser;->LA(I)I

    move-result v2

    :try_start_0
    invoke-super {p0, p1}, Lantlr/LLkParser;->match(Lantlr/collections/impl/BitSet;)V

    iget-object v0, p0, Lantlr/debug/LLkDebuggingParser;->parserEventSupport:Lantlr/debug/ParserEventSupport;

    iget-object v3, p0, Lantlr/debug/LLkDebuggingParser;->inputState:Lantlr/ParserSharedInputState;

    iget v3, v3, Lantlr/ParserSharedInputState;->guessing:I

    invoke-virtual {v0, v2, p1, v1, v3}, Lantlr/debug/ParserEventSupport;->fireMatch(ILantlr/collections/impl/BitSet;Ljava/lang/String;I)V
    :try_end_0
    .catch Lantlr/MismatchedTokenException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    iget-object v3, p0, Lantlr/debug/LLkDebuggingParser;->inputState:Lantlr/ParserSharedInputState;

    iget v3, v3, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v3, :cond_0

    iget-object v3, p0, Lantlr/debug/LLkDebuggingParser;->parserEventSupport:Lantlr/debug/ParserEventSupport;

    iget-object v4, p0, Lantlr/debug/LLkDebuggingParser;->inputState:Lantlr/ParserSharedInputState;

    iget v4, v4, Lantlr/ParserSharedInputState;->guessing:I

    invoke-virtual {v3, v2, p1, v1, v4}, Lantlr/debug/ParserEventSupport;->fireMismatch(ILantlr/collections/impl/BitSet;Ljava/lang/String;I)V

    :cond_0
    throw v0
.end method

.method public matchNot(I)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lantlr/MismatchedTokenException;,
            Lantlr/TokenStreamException;
        }
    .end annotation

    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lantlr/debug/LLkDebuggingParser;->LT(I)Lantlr/Token;

    move-result-object v0

    invoke-virtual {v0}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v2}, Lantlr/debug/LLkDebuggingParser;->LA(I)I

    move-result v2

    :try_start_0
    invoke-super {p0, p1}, Lantlr/LLkParser;->matchNot(I)V

    iget-object v0, p0, Lantlr/debug/LLkDebuggingParser;->parserEventSupport:Lantlr/debug/ParserEventSupport;

    iget-object v3, p0, Lantlr/debug/LLkDebuggingParser;->inputState:Lantlr/ParserSharedInputState;

    iget v3, v3, Lantlr/ParserSharedInputState;->guessing:I

    invoke-virtual {v0, v2, p1, v1, v3}, Lantlr/debug/ParserEventSupport;->fireMatchNot(IILjava/lang/String;I)V
    :try_end_0
    .catch Lantlr/MismatchedTokenException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    iget-object v3, p0, Lantlr/debug/LLkDebuggingParser;->inputState:Lantlr/ParserSharedInputState;

    iget v3, v3, Lantlr/ParserSharedInputState;->guessing:I

    if-nez v3, :cond_0

    iget-object v3, p0, Lantlr/debug/LLkDebuggingParser;->parserEventSupport:Lantlr/debug/ParserEventSupport;

    iget-object v4, p0, Lantlr/debug/LLkDebuggingParser;->inputState:Lantlr/ParserSharedInputState;

    iget v4, v4, Lantlr/ParserSharedInputState;->guessing:I

    invoke-virtual {v3, v2, p1, v1, v4}, Lantlr/debug/ParserEventSupport;->fireMismatchNot(IILjava/lang/String;I)V

    :cond_0
    throw v0
.end method

.method public removeMessageListener(Lantlr/debug/MessageListener;)V
    .locals 1

    iget-object v0, p0, Lantlr/debug/LLkDebuggingParser;->parserEventSupport:Lantlr/debug/ParserEventSupport;

    invoke-virtual {v0, p1}, Lantlr/debug/ParserEventSupport;->removeMessageListener(Lantlr/debug/MessageListener;)V

    return-void
.end method

.method public removeParserListener(Lantlr/debug/ParserListener;)V
    .locals 1

    iget-object v0, p0, Lantlr/debug/LLkDebuggingParser;->parserEventSupport:Lantlr/debug/ParserEventSupport;

    invoke-virtual {v0, p1}, Lantlr/debug/ParserEventSupport;->removeParserListener(Lantlr/debug/ParserListener;)V

    return-void
.end method

.method public removeParserMatchListener(Lantlr/debug/ParserMatchListener;)V
    .locals 1

    iget-object v0, p0, Lantlr/debug/LLkDebuggingParser;->parserEventSupport:Lantlr/debug/ParserEventSupport;

    invoke-virtual {v0, p1}, Lantlr/debug/ParserEventSupport;->removeParserMatchListener(Lantlr/debug/ParserMatchListener;)V

    return-void
.end method

.method public removeParserTokenListener(Lantlr/debug/ParserTokenListener;)V
    .locals 1

    iget-object v0, p0, Lantlr/debug/LLkDebuggingParser;->parserEventSupport:Lantlr/debug/ParserEventSupport;

    invoke-virtual {v0, p1}, Lantlr/debug/ParserEventSupport;->removeParserTokenListener(Lantlr/debug/ParserTokenListener;)V

    return-void
.end method

.method public removeSemanticPredicateListener(Lantlr/debug/SemanticPredicateListener;)V
    .locals 1

    iget-object v0, p0, Lantlr/debug/LLkDebuggingParser;->parserEventSupport:Lantlr/debug/ParserEventSupport;

    invoke-virtual {v0, p1}, Lantlr/debug/ParserEventSupport;->removeSemanticPredicateListener(Lantlr/debug/SemanticPredicateListener;)V

    return-void
.end method

.method public removeSyntacticPredicateListener(Lantlr/debug/SyntacticPredicateListener;)V
    .locals 1

    iget-object v0, p0, Lantlr/debug/LLkDebuggingParser;->parserEventSupport:Lantlr/debug/ParserEventSupport;

    invoke-virtual {v0, p1}, Lantlr/debug/ParserEventSupport;->removeSyntacticPredicateListener(Lantlr/debug/SyntacticPredicateListener;)V

    return-void
.end method

.method public removeTraceListener(Lantlr/debug/TraceListener;)V
    .locals 1

    iget-object v0, p0, Lantlr/debug/LLkDebuggingParser;->parserEventSupport:Lantlr/debug/ParserEventSupport;

    invoke-virtual {v0, p1}, Lantlr/debug/ParserEventSupport;->removeTraceListener(Lantlr/debug/TraceListener;)V

    return-void
.end method

.method public reportError(Lantlr/RecognitionException;)V
    .locals 1

    iget-object v0, p0, Lantlr/debug/LLkDebuggingParser;->parserEventSupport:Lantlr/debug/ParserEventSupport;

    invoke-virtual {v0, p1}, Lantlr/debug/ParserEventSupport;->fireReportError(Ljava/lang/Exception;)V

    invoke-super {p0, p1}, Lantlr/LLkParser;->reportError(Lantlr/RecognitionException;)V

    return-void
.end method

.method public reportError(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lantlr/debug/LLkDebuggingParser;->parserEventSupport:Lantlr/debug/ParserEventSupport;

    invoke-virtual {v0, p1}, Lantlr/debug/ParserEventSupport;->fireReportError(Ljava/lang/String;)V

    invoke-super {p0, p1}, Lantlr/LLkParser;->reportError(Ljava/lang/String;)V

    return-void
.end method

.method public reportWarning(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lantlr/debug/LLkDebuggingParser;->parserEventSupport:Lantlr/debug/ParserEventSupport;

    invoke-virtual {v0, p1}, Lantlr/debug/ParserEventSupport;->fireReportWarning(Ljava/lang/String;)V

    invoke-super {p0, p1}, Lantlr/LLkParser;->reportWarning(Ljava/lang/String;)V

    return-void
.end method

.method public setDebugMode(Z)V
    .locals 1

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lantlr/debug/LLkDebuggingParser;->_notDebugMode:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setupDebugging(Lantlr/TokenBuffer;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1}, Lantlr/debug/LLkDebuggingParser;->setupDebugging(Lantlr/TokenStream;Lantlr/TokenBuffer;)V

    return-void
.end method

.method public setupDebugging(Lantlr/TokenStream;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lantlr/debug/LLkDebuggingParser;->setupDebugging(Lantlr/TokenStream;Lantlr/TokenBuffer;)V

    return-void
.end method

.method protected setupDebugging(Lantlr/TokenStream;Lantlr/TokenBuffer;)V
    .locals 5

    const/4 v4, 0x1

    invoke-virtual {p0, v4}, Lantlr/debug/LLkDebuggingParser;->setDebugMode(Z)V

    :try_start_0
    const-string v0, "javax.swing.JButton"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    :try_start_1
    const-string v0, "antlr.parseview.ParseView"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    const/4 v0, 0x3

    new-array v2, v0, [Ljava/lang/Class;

    const/4 v3, 0x0

    sget-object v0, Lantlr/debug/LLkDebuggingParser;->class$antlr$debug$LLkDebuggingParser:Ljava/lang/Class;

    if-nez v0, :cond_0

    const-string v0, "antlr.debug.LLkDebuggingParser"

    invoke-static {v0}, Lantlr/debug/LLkDebuggingParser;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lantlr/debug/LLkDebuggingParser;->class$antlr$debug$LLkDebuggingParser:Ljava/lang/Class;

    :goto_1
    aput-object v0, v2, v3

    const/4 v3, 0x1

    sget-object v0, Lantlr/debug/LLkDebuggingParser;->class$antlr$TokenStream:Ljava/lang/Class;

    if-nez v0, :cond_1

    const-string v0, "antlr.TokenStream"

    invoke-static {v0}, Lantlr/debug/LLkDebuggingParser;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lantlr/debug/LLkDebuggingParser;->class$antlr$TokenStream:Ljava/lang/Class;

    :goto_2
    aput-object v0, v2, v3

    const/4 v3, 0x2

    sget-object v0, Lantlr/debug/LLkDebuggingParser;->class$antlr$TokenBuffer:Ljava/lang/Class;

    if-nez v0, :cond_2

    const-string v0, "antlr.TokenBuffer"

    invoke-static {v0}, Lantlr/debug/LLkDebuggingParser;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    sput-object v0, Lantlr/debug/LLkDebuggingParser;->class$antlr$TokenBuffer:Ljava/lang/Class;

    :goto_3
    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v0

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    const/4 v2, 0x1

    aput-object p1, v1, v2

    const/4 v2, 0x2

    aput-object p2, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    :goto_4
    return-void

    :catch_0
    move-exception v0

    sget-object v0, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string v1, "Swing is required to use ParseView, but is not present in your CLASSPATH"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/System;->exit(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    sget-object v1, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "Error initializing ParseView: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    sget-object v0, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string v1, "Please report this to Scott Stanchfield, thetick@magelang.com"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    invoke-static {v4}, Ljava/lang/System;->exit(I)V

    goto :goto_4

    :cond_0
    :try_start_2
    sget-object v0, Lantlr/debug/LLkDebuggingParser;->class$antlr$debug$LLkDebuggingParser:Ljava/lang/Class;

    goto :goto_1

    :cond_1
    sget-object v0, Lantlr/debug/LLkDebuggingParser;->class$antlr$TokenStream:Ljava/lang/Class;

    goto :goto_2

    :cond_2
    sget-object v0, Lantlr/debug/LLkDebuggingParser;->class$antlr$TokenBuffer:Ljava/lang/Class;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_3
.end method

.method public declared-synchronized wakeUp()V
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
