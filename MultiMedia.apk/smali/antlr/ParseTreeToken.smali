.class public Lantlr/ParseTreeToken;
.super Lantlr/ParseTree;
.source "ParseTreeToken.java"


# instance fields
.field protected token:Lantlr/Token;


# direct methods
.method public constructor <init>(Lantlr/Token;)V
    .locals 0

    invoke-direct {p0}, Lantlr/ParseTree;-><init>()V

    iput-object p1, p0, Lantlr/ParseTreeToken;->token:Lantlr/Token;

    return-void
.end method


# virtual methods
.method protected getLeftmostDerivation(Ljava/lang/StringBuffer;I)I
    .locals 1

    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    invoke-virtual {p0}, Lantlr/ParseTreeToken;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    return p2
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lantlr/ParseTreeToken;->token:Lantlr/Token;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lantlr/ParseTreeToken;->token:Lantlr/Token;

    invoke-virtual {v0}, Lantlr/Token;->getText()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "<missing token>"

    goto :goto_0
.end method
