.class Lantlr/DefaultToolErrorHandler;
.super Ljava/lang/Object;
.source "DefaultToolErrorHandler.java"

# interfaces
.implements Lantlr/ToolErrorHandler;


# instance fields
.field private final antlrTool:Lantlr/Tool;

.field javaCharFormatter:Lantlr/CharFormatter;


# direct methods
.method constructor <init>(Lantlr/Tool;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lantlr/JavaCharFormatter;

    invoke-direct {v0}, Lantlr/JavaCharFormatter;-><init>()V

    iput-object v0, p0, Lantlr/DefaultToolErrorHandler;->javaCharFormatter:Lantlr/CharFormatter;

    iput-object p1, p0, Lantlr/DefaultToolErrorHandler;->antlrTool:Lantlr/Tool;

    return-void
.end method

.method private dumpSets([Ljava/lang/String;ILantlr/Grammar;ZI[Lantlr/Lookahead;)V
    .locals 5

    new-instance v2, Ljava/lang/StringBuffer;

    const/16 v0, 0x64

    invoke-direct {v2, v0}, Ljava/lang/StringBuffer;-><init>(I)V

    const/4 v0, 0x1

    :goto_0
    if-gt v0, p5, :cond_2

    const-string v1, "k=="

    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v1

    const/16 v3, 0x3a

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    if-eqz p4, :cond_1

    aget-object v1, p6, v0

    iget-object v1, v1, Lantlr/Lookahead;->fset:Lantlr/collections/impl/BitSet;

    const-string v3, ","

    iget-object v4, p0, Lantlr/DefaultToolErrorHandler;->javaCharFormatter:Lantlr/CharFormatter;

    invoke-virtual {v1, v3, v4}, Lantlr/collections/impl/BitSet;->toStringWithRanges(Ljava/lang/String;Lantlr/CharFormatter;)Ljava/lang/String;

    move-result-object v1

    aget-object v3, p6, v0

    invoke-virtual {v3}, Lantlr/Lookahead;->containsEpsilon()Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "<end-of-token>"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_0

    const/16 v3, 0x2c

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    :cond_0
    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :goto_1
    add-int/lit8 v1, p2, 0x1

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, p1, p2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->setLength(I)V

    add-int/lit8 v0, v0, 0x1

    move p2, v1

    goto :goto_0

    :cond_1
    aget-object v1, p6, v0

    iget-object v1, v1, Lantlr/Lookahead;->fset:Lantlr/collections/impl/BitSet;

    const-string v3, ","

    iget-object v4, p3, Lantlr/Grammar;->tokenManager:Lantlr/TokenManager;

    invoke-interface {v4}, Lantlr/TokenManager;->getVocabulary()Lantlr/collections/impl/Vector;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lantlr/collections/impl/BitSet;->toString(Ljava/lang/String;Lantlr/collections/impl/Vector;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_1

    :cond_2
    return-void
.end method


# virtual methods
.method public warnAltAmbiguity(Lantlr/Grammar;Lantlr/AlternativeBlock;ZI[Lantlr/Lookahead;II)V
    .locals 7

    new-instance v2, Ljava/lang/StringBuffer;

    const/16 v0, 0x64

    invoke-direct {v2, v0}, Ljava/lang/StringBuffer;-><init>(I)V

    instance-of v0, p2, Lantlr/RuleBlock;

    if-eqz v0, :cond_0

    move-object v0, p2

    check-cast v0, Lantlr/RuleBlock;

    invoke-virtual {v0}, Lantlr/RuleBlock;->isLexerAutoGenRule()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p2, p6}, Lantlr/AlternativeBlock;->getAlternativeAt(I)Lantlr/Alternative;

    move-result-object v0

    invoke-virtual {p2, p7}, Lantlr/AlternativeBlock;->getAlternativeAt(I)Lantlr/Alternative;

    move-result-object v1

    iget-object v0, v0, Lantlr/Alternative;->head:Lantlr/AlternativeElement;

    check-cast v0, Lantlr/RuleRefElement;

    iget-object v1, v1, Lantlr/Alternative;->head:Lantlr/AlternativeElement;

    check-cast v1, Lantlr/RuleRefElement;

    iget-object v0, v0, Lantlr/RuleRefElement;->targetRule:Ljava/lang/String;

    invoke-static {v0}, Lantlr/CodeGenerator;->reverseLexerRuleName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, v1, Lantlr/RuleRefElement;->targetRule:Ljava/lang/String;

    invoke-static {v1}, Lantlr/CodeGenerator;->reverseLexerRuleName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "lexical nondeterminism between rules "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v3, " and "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, " upon"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :goto_0
    add-int/lit8 v0, p4, 0x1

    new-array v1, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    const/4 v2, 0x1

    move-object v0, p0

    move-object v3, p1

    move v4, p3

    move v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lantlr/DefaultToolErrorHandler;->dumpSets([Ljava/lang/String;ILantlr/Grammar;ZI[Lantlr/Lookahead;)V

    iget-object v0, p0, Lantlr/DefaultToolErrorHandler;->antlrTool:Lantlr/Tool;

    invoke-virtual {p1}, Lantlr/Grammar;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Lantlr/AlternativeBlock;->getLine()I

    move-result v3

    invoke-virtual {p2}, Lantlr/AlternativeBlock;->getColumn()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lantlr/Tool;->warning([Ljava/lang/String;Ljava/lang/String;II)V

    return-void

    :cond_0
    if-eqz p3, :cond_1

    const-string v0, "lexical "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_1
    const-string v0, "nondeterminism between alts "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    add-int/lit8 v0, p6, 0x1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, " and "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    add-int/lit8 v0, p7, 0x1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, " of block upon"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0
.end method

.method public warnAltExitAmbiguity(Lantlr/Grammar;Lantlr/BlockWithImpliedExitPath;ZI[Lantlr/Lookahead;I)V
    .locals 7

    add-int/lit8 v0, p4, 0x2

    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    if-eqz p3, :cond_0

    const-string v0, "lexical "

    :goto_0
    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v3, "nondeterminism upon"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v2

    const/4 v2, 0x1

    move-object v0, p0

    move-object v3, p1

    move v4, p3

    move v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lantlr/DefaultToolErrorHandler;->dumpSets([Ljava/lang/String;ILantlr/Grammar;ZI[Lantlr/Lookahead;)V

    add-int/lit8 v0, p4, 0x1

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const-string v3, "between alt "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    add-int/lit8 v3, p6, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, " and exit branch of block"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    iget-object v0, p0, Lantlr/DefaultToolErrorHandler;->antlrTool:Lantlr/Tool;

    invoke-virtual {p1}, Lantlr/Grammar;->getFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Lantlr/BlockWithImpliedExitPath;->getLine()I

    move-result v3

    invoke-virtual {p2}, Lantlr/BlockWithImpliedExitPath;->getColumn()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lantlr/Tool;->warning([Ljava/lang/String;Ljava/lang/String;II)V

    return-void

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method
