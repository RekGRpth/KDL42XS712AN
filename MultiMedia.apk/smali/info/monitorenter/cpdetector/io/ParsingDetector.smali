.class public Linfo/monitorenter/cpdetector/io/ParsingDetector;
.super Linfo/monitorenter/cpdetector/io/AbstractCodepageDetector;
.source "ParsingDetector.java"


# instance fields
.field private m_verbose:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Linfo/monitorenter/cpdetector/io/ParsingDetector;-><init>(Z)V

    return-void
.end method

.method public constructor <init>(Z)V
    .locals 1
    .param p1    # Z

    invoke-direct {p0}, Linfo/monitorenter/cpdetector/io/AbstractCodepageDetector;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Linfo/monitorenter/cpdetector/io/ParsingDetector;->m_verbose:Z

    iput-boolean p1, p0, Linfo/monitorenter/cpdetector/io/ParsingDetector;->m_verbose:Z

    return-void
.end method


# virtual methods
.method public detectCodepage(Ljava/io/InputStream;I)Ljava/nio/charset/Charset;
    .locals 11
    .param p1    # Ljava/io/InputStream;
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-instance v5, Linfo/monitorenter/io/LimitedInputStream;

    invoke-direct {v5, p1, p2}, Linfo/monitorenter/io/LimitedInputStream;-><init>(Ljava/io/InputStream;I)V

    iget-boolean v8, p0, Linfo/monitorenter/cpdetector/io/ParsingDetector;->m_verbose:Z

    if-eqz v8, :cond_0

    sget-object v8, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v9, "  parsing for html-charset/xml-encoding attribute with codepage: US-ASCII"

    invoke-virtual {v8, v9}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    :try_start_0
    new-instance v4, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;

    new-instance v8, Ljava/io/InputStreamReader;

    const-string v9, "US-ASCII"

    invoke-direct {v8, v5, v9}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    invoke-direct {v4, v8}, Linfo/monitorenter/cpdetector/io/parser/EncodingLexer;-><init>(Ljava/io/Reader;)V

    new-instance v6, Linfo/monitorenter/cpdetector/io/parser/EncodingParser;

    invoke-direct {v6, v4}, Linfo/monitorenter/cpdetector/io/parser/EncodingParser;-><init>(Lantlr/TokenStream;)V

    invoke-virtual {v6}, Linfo/monitorenter/cpdetector/io/parser/EncodingParser;->htmlDocument()Ljava/lang/String;
    :try_end_0
    .catch Lantlr/ANTLRException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v2

    if-eqz v2, :cond_2

    :try_start_1
    invoke-static {v2}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;
    :try_end_1
    .catch Ljava/nio/charset/UnsupportedCharsetException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lantlr/ANTLRException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v1

    :cond_1
    :goto_0
    return-object v1

    :catch_0
    move-exception v7

    :try_start_2
    invoke-static {v2}, Linfo/monitorenter/cpdetector/io/UnsupportedCharset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v1

    goto :goto_0

    :cond_2
    invoke-static {}, Linfo/monitorenter/cpdetector/io/UnknownCharset;->getInstance()Ljava/nio/charset/Charset;
    :try_end_2
    .catch Lantlr/ANTLRException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v1

    goto :goto_0

    :catch_1
    move-exception v0

    iget-boolean v8, p0, Linfo/monitorenter/cpdetector/io/ParsingDetector;->m_verbose:Z

    if-eqz v8, :cond_1

    sget-object v8, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "  ANTLR parser exception: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v0}, Lantlr/ANTLRException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0

    :catch_2
    move-exception v3

    iget-boolean v8, p0, Linfo/monitorenter/cpdetector/io/ParsingDetector;->m_verbose:Z

    if-eqz v8, :cond_3

    sget-object v8, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "  Decoding Exception: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v3}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " (unsupported java charset)."

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_3
    if-nez v1, :cond_1

    if-eqz v2, :cond_4

    invoke-static {v2}, Linfo/monitorenter/cpdetector/io/UnsupportedCharset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v1

    goto :goto_0

    :cond_4
    invoke-static {}, Linfo/monitorenter/cpdetector/io/UnknownCharset;->getInstance()Ljava/nio/charset/Charset;

    move-result-object v1

    goto :goto_0
.end method
