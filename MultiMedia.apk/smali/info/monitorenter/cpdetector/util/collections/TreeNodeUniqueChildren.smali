.class public Linfo/monitorenter/cpdetector/util/collections/TreeNodeUniqueChildren;
.super Linfo/monitorenter/cpdetector/util/collections/ITreeNode$DefaultTreeNode;
.source "TreeNodeUniqueChildren.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Linfo/monitorenter/cpdetector/util/collections/ITreeNode$DefaultTreeNode;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    invoke-direct {p0, p1}, Linfo/monitorenter/cpdetector/util/collections/ITreeNode$DefaultTreeNode;-><init>(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public addChildNode(Linfo/monitorenter/cpdetector/util/collections/ITreeNode;)Z
    .locals 8
    .param p1    # Linfo/monitorenter/cpdetector/util/collections/ITreeNode;

    const/4 v5, 0x1

    if-nez p1, :cond_0

    new-instance v6, Ljava/lang/IllegalArgumentException;

    const-string v7, "Argument node is null!"

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    :cond_0
    invoke-interface {p1}, Linfo/monitorenter/cpdetector/util/collections/ITreeNode;->getUserObject()Ljava/lang/Object;

    move-result-object v4

    const/4 v3, 0x0

    const/4 v0, 0x0

    invoke-virtual {p0}, Linfo/monitorenter/cpdetector/util/collections/TreeNodeUniqueChildren;->getChilds()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Linfo/monitorenter/cpdetector/util/collections/ITreeNode;

    invoke-interface {v0}, Linfo/monitorenter/cpdetector/util/collections/ITreeNode;->getUserObject()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v0}, Linfo/monitorenter/cpdetector/util/collections/ITreeNode;->getAllChildren()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v6

    new-array v6, v6, [Linfo/monitorenter/cpdetector/util/collections/ITreeNode;

    invoke-interface {v1, v6}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Linfo/monitorenter/cpdetector/util/collections/ITreeNode;

    check-cast v6, [Linfo/monitorenter/cpdetector/util/collections/ITreeNode;

    invoke-interface {p1, v6}, Linfo/monitorenter/cpdetector/util/collections/ITreeNode;->addChildNodes([Linfo/monitorenter/cpdetector/util/collections/ITreeNode;)Z

    invoke-interface {p1, p0}, Linfo/monitorenter/cpdetector/util/collections/ITreeNode;->setParent(Linfo/monitorenter/cpdetector/util/collections/ITreeNode;)V

    invoke-virtual {p0, v0}, Linfo/monitorenter/cpdetector/util/collections/TreeNodeUniqueChildren;->removeChild(Linfo/monitorenter/cpdetector/util/collections/ITreeNode;)Z

    :cond_2
    invoke-super {p0, p1}, Linfo/monitorenter/cpdetector/util/collections/ITreeNode$DefaultTreeNode;->addChildNode(Linfo/monitorenter/cpdetector/util/collections/ITreeNode;)Z

    move-result v5

    return v5
.end method
