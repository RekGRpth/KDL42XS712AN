.class public Linfo/monitorenter/cpdetector/TestCodepageProcessor;
.super Ljava/lang/Object;
.source "TestCodepageProcessor.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public testIllegalCharsetError()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .annotation runtime Lorg/junit/Test;
    .end annotation

    const/4 v4, 0x7

    new-array v0, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "-m"

    aput-object v5, v0, v4

    const/4 v4, 0x1

    const-string v5, "-r"

    aput-object v5, v0, v4

    const/4 v4, 0x2

    const-string v5, "testdocuments/invalidcharsetdocument/"

    aput-object v5, v0, v4

    const/4 v4, 0x3

    const-string v5, "-o"

    aput-object v5, v0, v4

    const/4 v4, 0x4

    const-string v5, "testoutput/"

    aput-object v5, v0, v4

    const/4 v4, 0x5

    const-string v5, "-t"

    aput-object v5, v0, v4

    const/4 v4, 0x6

    const-string v5, "utf-8"

    aput-object v5, v0, v4

    const/4 v2, 0x0

    const-string v1, "valid"

    invoke-static {v1}, Linfo/monitorenter/util/ExceptionUtil;->findMatchInSystemOut(Ljava/lang/String;)Linfo/monitorenter/util/ExceptionUtil$InputStreamTracer;

    move-result-object v3

    invoke-static {v0}, Linfo/monitorenter/cpdetector/CodepageProcessor;->main([Ljava/lang/String;)V

    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v4}, Ljava/io/PrintStream;->flush()V

    sget-object v4, Ljava/lang/System;->err:Ljava/io/PrintStream;

    invoke-virtual {v4}, Ljava/io/PrintStream;->flush()V

    const-wide/16 v4, 0x1388

    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V

    invoke-virtual {v3}, Linfo/monitorenter/util/ExceptionUtil$InputStreamTracer;->isMatched()Z

    move-result v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Did not find \""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\" in system out."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v2}, Ljunit/framework/Assert;->assertTrue(Ljava/lang/String;Z)V

    return-void
.end method

.method public testVoidError()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .annotation runtime Lorg/junit/Test;
    .end annotation

    const/4 v4, 0x7

    new-array v0, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "-m"

    aput-object v5, v0, v4

    const/4 v4, 0x1

    const-string v5, "-r"

    aput-object v5, v0, v4

    const/4 v4, 0x2

    const-string v5, "testdocuments/voiddocument/"

    aput-object v5, v0, v4

    const/4 v4, 0x3

    const-string v5, "-o"

    aput-object v5, v0, v4

    const/4 v4, 0x4

    const-string v5, "testoutput/"

    aput-object v5, v0, v4

    const/4 v4, 0x5

    const-string v5, "-t"

    aput-object v5, v0, v4

    const/4 v4, 0x6

    const-string v5, "utf-8"

    aput-object v5, v0, v4

    const/4 v2, 0x0

    const-string v1, "Skipping transformation of document /home/achim/workspace/cpdetector/testdocuments/voiddocument/Voiderror.htm because it\'s charset could not be detected."

    invoke-static {v1}, Linfo/monitorenter/util/ExceptionUtil;->findMatchInSystemOut(Ljava/lang/String;)Linfo/monitorenter/util/ExceptionUtil$InputStreamTracer;

    move-result-object v3

    invoke-static {v0}, Linfo/monitorenter/cpdetector/CodepageProcessor;->main([Ljava/lang/String;)V

    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v4}, Ljava/io/PrintStream;->flush()V

    sget-object v4, Ljava/lang/System;->err:Ljava/io/PrintStream;

    invoke-virtual {v4}, Ljava/io/PrintStream;->flush()V

    const-wide/16 v4, 0x1388

    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V

    invoke-virtual {v3}, Linfo/monitorenter/util/ExceptionUtil$InputStreamTracer;->isMatched()Z

    move-result v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Did not find \""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\" in system out."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v2}, Ljunit/framework/Assert;->assertTrue(Ljava/lang/String;Z)V

    return-void
.end method
