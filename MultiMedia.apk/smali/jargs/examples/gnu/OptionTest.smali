.class public Ljargs/examples/gnu/OptionTest;
.super Ljava/lang/Object;
.source "OptionTest.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static main([Ljava/lang/String;)V
    .locals 15
    .param p0    # [Ljava/lang/String;

    new-instance v7, Ljargs/gnu/CmdLineParser;

    invoke-direct {v7}, Ljargs/gnu/CmdLineParser;-><init>()V

    const/16 v12, 0x76

    const-string v13, "verbose"

    invoke-virtual {v7, v12, v13}, Ljargs/gnu/CmdLineParser;->addBooleanOption(CLjava/lang/String;)Ljargs/gnu/CmdLineParser$Option;

    move-result-object v10

    const/16 v12, 0x73

    const-string v13, "size"

    invoke-virtual {v7, v12, v13}, Ljargs/gnu/CmdLineParser;->addIntegerOption(CLjava/lang/String;)Ljargs/gnu/CmdLineParser$Option;

    move-result-object v8

    const/16 v12, 0x6e

    const-string v13, "name"

    invoke-virtual {v7, v12, v13}, Ljargs/gnu/CmdLineParser;->addStringOption(CLjava/lang/String;)Ljargs/gnu/CmdLineParser$Option;

    move-result-object v4

    const/16 v12, 0x66

    const-string v13, "fraction"

    invoke-virtual {v7, v12, v13}, Ljargs/gnu/CmdLineParser;->addDoubleOption(CLjava/lang/String;)Ljargs/gnu/CmdLineParser$Option;

    move-result-object v1

    :try_start_0
    invoke-virtual {v7, p0}, Ljargs/gnu/CmdLineParser;->parse([Ljava/lang/String;)V
    :try_end_0
    .catch Ljargs/gnu/CmdLineParser$OptionException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    invoke-virtual {v7, v10}, Ljargs/gnu/CmdLineParser;->getOptionValue(Ljargs/gnu/CmdLineParser$Option;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Boolean;

    invoke-virtual {v7, v8}, Ljargs/gnu/CmdLineParser;->getOptionValue(Ljargs/gnu/CmdLineParser$Option;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    invoke-virtual {v7, v4}, Ljargs/gnu/CmdLineParser;->getOptionValue(Ljargs/gnu/CmdLineParser$Option;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v7, v1}, Ljargs/gnu/CmdLineParser;->getOptionValue(Ljargs/gnu/CmdLineParser$Option;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Double;

    sget-object v12, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v13, Ljava/lang/StringBuffer;

    invoke-direct {v13}, Ljava/lang/StringBuffer;-><init>()V

    const-string v14, "verbose: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v13

    invoke-virtual {v13, v11}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    sget-object v12, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v13, Ljava/lang/StringBuffer;

    invoke-direct {v13}, Ljava/lang/StringBuffer;-><init>()V

    const-string v14, "size: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v13

    invoke-virtual {v13, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    sget-object v12, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v13, Ljava/lang/StringBuffer;

    invoke-direct {v13}, Ljava/lang/StringBuffer;-><init>()V

    const-string v14, "name: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v13

    invoke-virtual {v13, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    sget-object v12, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v13, Ljava/lang/StringBuffer;

    invoke-direct {v13}, Ljava/lang/StringBuffer;-><init>()V

    const-string v14, "fraction: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v13

    invoke-virtual {v13, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    invoke-virtual {v7}, Ljargs/gnu/CmdLineParser;->getRemainingArgs()[Ljava/lang/String;

    move-result-object v6

    sget-object v12, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v13, "remaining args: "

    invoke-virtual {v12, v13}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    const/4 v3, 0x0

    :goto_1
    array-length v12, v6

    if-lt v3, v12, :cond_0

    const/4 v12, 0x0

    invoke-static {v12}, Ljava/lang/System;->exit(I)V

    return-void

    :catch_0
    move-exception v0

    sget-object v12, Ljava/lang/System;->err:Ljava/io/PrintStream;

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    invoke-static {}, Ljargs/examples/gnu/OptionTest;->printUsage()V

    const/4 v12, 0x2

    invoke-static {v12}, Ljava/lang/System;->exit(I)V

    goto/16 :goto_0

    :cond_0
    sget-object v12, Ljava/lang/System;->out:Ljava/io/PrintStream;

    aget-object v13, v6, v3

    invoke-virtual {v12, v13}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method

.method private static printUsage()V
    .locals 2

    sget-object v0, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string v1, "usage: prog [{-v,--verbose}] [{-n,--name} a_name][{-s,--size} a_number] [{-f,--fraction} a_float]"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    return-void
.end method
