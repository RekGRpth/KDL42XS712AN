.class public Ljargs/examples/gnu/OptionParserSubclassTest;
.super Ljava/lang/Object;
.source "OptionParserSubclassTest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ljargs/examples/gnu/OptionParserSubclassTest$MyOptionsParser;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static main([Ljava/lang/String;)V
    .locals 10
    .param p0    # [Ljava/lang/String;

    const/4 v9, 0x0

    const/4 v8, 0x2

    new-instance v4, Ljargs/examples/gnu/OptionParserSubclassTest$MyOptionsParser;

    invoke-direct {v4}, Ljargs/examples/gnu/OptionParserSubclassTest$MyOptionsParser;-><init>()V

    :try_start_0
    invoke-virtual {v4, p0}, Ljargs/gnu/CmdLineParser;->parse([Ljava/lang/String;)V
    :try_end_0
    .catch Ljargs/gnu/CmdLineParser$UnknownOptionException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljargs/gnu/CmdLineParser$IllegalOptionValueException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    const/4 v6, 0x4

    new-array v0, v6, [Ljargs/gnu/CmdLineParser$Option;

    sget-object v6, Ljargs/examples/gnu/OptionParserSubclassTest$MyOptionsParser;->VERBOSE:Ljargs/gnu/CmdLineParser$Option;

    aput-object v6, v0, v9

    const/4 v6, 0x1

    sget-object v7, Ljargs/examples/gnu/OptionParserSubclassTest$MyOptionsParser;->NAME:Ljargs/gnu/CmdLineParser$Option;

    aput-object v7, v0, v6

    sget-object v6, Ljargs/examples/gnu/OptionParserSubclassTest$MyOptionsParser;->SIZE:Ljargs/gnu/CmdLineParser$Option;

    aput-object v6, v0, v8

    const/4 v6, 0x3

    sget-object v7, Ljargs/examples/gnu/OptionParserSubclassTest$MyOptionsParser;->FRACTION:Ljargs/gnu/CmdLineParser$Option;

    aput-object v7, v0, v6

    const/4 v3, 0x0

    :goto_1
    array-length v6, v0

    if-lt v3, v6, :cond_0

    invoke-virtual {v4}, Ljargs/gnu/CmdLineParser;->getRemainingArgs()[Ljava/lang/String;

    move-result-object v5

    sget-object v6, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v7, "remaining args: "

    invoke-virtual {v6, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    const/4 v2, 0x0

    :goto_2
    array-length v6, v5

    if-lt v2, v6, :cond_1

    invoke-static {v9}, Ljava/lang/System;->exit(I)V

    return-void

    :catch_0
    move-exception v1

    sget-object v6, Ljava/lang/System;->err:Ljava/io/PrintStream;

    invoke-virtual {v1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    invoke-static {}, Ljargs/examples/gnu/OptionParserSubclassTest;->printUsage()V

    invoke-static {v8}, Ljava/lang/System;->exit(I)V

    goto :goto_0

    :catch_1
    move-exception v1

    sget-object v6, Ljava/lang/System;->err:Ljava/io/PrintStream;

    invoke-virtual {v1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    invoke-static {}, Ljargs/examples/gnu/OptionParserSubclassTest;->printUsage()V

    invoke-static {v8}, Ljava/lang/System;->exit(I)V

    goto :goto_0

    :cond_0
    sget-object v6, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v7, Ljava/lang/StringBuffer;

    invoke-direct {v7}, Ljava/lang/StringBuffer;-><init>()V

    aget-object v8, v0, v3

    invoke-virtual {v8}, Ljargs/gnu/CmdLineParser$Option;->longForm()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    const-string v8, ": "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    aget-object v8, v0, v3

    invoke-virtual {v4, v8}, Ljargs/gnu/CmdLineParser;->getOptionValue(Ljargs/gnu/CmdLineParser$Option;)Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_1
    sget-object v6, Ljava/lang/System;->out:Ljava/io/PrintStream;

    aget-object v7, v5, v2

    invoke-virtual {v6, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_2
.end method

.method private static printUsage()V
    .locals 2

    sget-object v0, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string v1, "usage: prog [{-v,--verbose}] [{-n,--name} a_name][{-s,--size} a_number] [{-f,--fraction} a_float]"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    return-void
.end method
