.class public Ljargs/gnu/CmdLineParser$Option$IntegerOption;
.super Ljargs/gnu/CmdLineParser$Option;
.source "CmdLineParser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ljargs/gnu/CmdLineParser$Option;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "IntegerOption"
.end annotation


# direct methods
.method public constructor <init>(CLjava/lang/String;)V
    .locals 1
    .param p1    # C
    .param p2    # Ljava/lang/String;

    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Ljargs/gnu/CmdLineParser$Option;-><init>(CLjava/lang/String;Z)V

    return-void
.end method


# virtual methods
.method protected parseValue(Ljava/lang/String;Ljava/util/Locale;)Ljava/lang/Object;
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/util/Locale;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljargs/gnu/CmdLineParser$IllegalOptionValueException;
        }
    .end annotation

    :try_start_0
    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, p1}, Ljava/lang/Integer;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    :catch_0
    move-exception v0

    new-instance v1, Ljargs/gnu/CmdLineParser$IllegalOptionValueException;

    invoke-direct {v1, p0, p1}, Ljargs/gnu/CmdLineParser$IllegalOptionValueException;-><init>(Ljargs/gnu/CmdLineParser$Option;Ljava/lang/String;)V

    throw v1
.end method
