.class public Ljargs/gnu/CmdLineParser$Option$DoubleOption;
.super Ljargs/gnu/CmdLineParser$Option;
.source "CmdLineParser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ljargs/gnu/CmdLineParser$Option;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DoubleOption"
.end annotation


# direct methods
.method public constructor <init>(CLjava/lang/String;)V
    .locals 1
    .param p1    # C
    .param p2    # Ljava/lang/String;

    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Ljargs/gnu/CmdLineParser$Option;-><init>(CLjava/lang/String;Z)V

    return-void
.end method


# virtual methods
.method protected parseValue(Ljava/lang/String;Ljava/util/Locale;)Ljava/lang/Object;
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/util/Locale;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljargs/gnu/CmdLineParser$IllegalOptionValueException;
        }
    .end annotation

    :try_start_0
    invoke-static {p2}, Ljava/text/NumberFormat;->getNumberInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/text/NumberFormat;->parse(Ljava/lang/String;)Ljava/lang/Number;

    move-result-object v2

    new-instance v3, Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Ljava/lang/Double;-><init>(D)V
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v3

    :catch_0
    move-exception v0

    new-instance v3, Ljargs/gnu/CmdLineParser$IllegalOptionValueException;

    invoke-direct {v3, p0, p1}, Ljargs/gnu/CmdLineParser$IllegalOptionValueException;-><init>(Ljargs/gnu/CmdLineParser$Option;Ljava/lang/String;)V

    throw v3
.end method
