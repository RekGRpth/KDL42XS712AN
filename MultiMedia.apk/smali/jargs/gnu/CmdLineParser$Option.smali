.class public abstract Ljargs/gnu/CmdLineParser$Option;
.super Ljava/lang/Object;
.source "CmdLineParser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ljargs/gnu/CmdLineParser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Option"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ljargs/gnu/CmdLineParser$Option$StringOption;,
        Ljargs/gnu/CmdLineParser$Option$DoubleOption;,
        Ljargs/gnu/CmdLineParser$Option$IntegerOption;,
        Ljargs/gnu/CmdLineParser$Option$BooleanOption;
    }
.end annotation


# instance fields
.field private longForm:Ljava/lang/String;

.field private shortForm:Ljava/lang/String;

.field private wantsValue:Z


# direct methods
.method protected constructor <init>(CLjava/lang/String;Z)V
    .locals 3
    .param p1    # C
    .param p2    # Ljava/lang/String;
    .param p3    # Z

    const/4 v0, 0x0

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Ljargs/gnu/CmdLineParser$Option;->shortForm:Ljava/lang/String;

    iput-object v0, p0, Ljargs/gnu/CmdLineParser$Option;->longForm:Ljava/lang/String;

    iput-boolean v2, p0, Ljargs/gnu/CmdLineParser$Option;->wantsValue:Z

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "null arg forms not allowed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    new-instance v0, Ljava/lang/String;

    const/4 v1, 0x1

    new-array v1, v1, [C

    aput-char p1, v1, v2

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([C)V

    iput-object v0, p0, Ljargs/gnu/CmdLineParser$Option;->shortForm:Ljava/lang/String;

    iput-object p2, p0, Ljargs/gnu/CmdLineParser$Option;->longForm:Ljava/lang/String;

    iput-boolean p3, p0, Ljargs/gnu/CmdLineParser$Option;->wantsValue:Z

    return-void
.end method


# virtual methods
.method public final getValue(Ljava/lang/String;Ljava/util/Locale;)Ljava/lang/Object;
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/util/Locale;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljargs/gnu/CmdLineParser$IllegalOptionValueException;
        }
    .end annotation

    iget-boolean v0, p0, Ljargs/gnu/CmdLineParser$Option;->wantsValue:Z

    if-eqz v0, :cond_1

    if-nez p1, :cond_0

    new-instance v0, Ljargs/gnu/CmdLineParser$IllegalOptionValueException;

    const-string v1, ""

    invoke-direct {v0, p0, v1}, Ljargs/gnu/CmdLineParser$IllegalOptionValueException;-><init>(Ljargs/gnu/CmdLineParser$Option;Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {p0, p1, p2}, Ljargs/gnu/CmdLineParser$Option;->parseValue(Ljava/lang/String;Ljava/util/Locale;)Ljava/lang/Object;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_1
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    goto :goto_0
.end method

.method public longForm()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Ljargs/gnu/CmdLineParser$Option;->longForm:Ljava/lang/String;

    return-object v0
.end method

.method protected parseValue(Ljava/lang/String;Ljava/util/Locale;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/util/Locale;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljargs/gnu/CmdLineParser$IllegalOptionValueException;
        }
    .end annotation

    const/4 v0, 0x0

    return-object v0
.end method

.method public shortForm()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Ljargs/gnu/CmdLineParser$Option;->shortForm:Ljava/lang/String;

    return-object v0
.end method

.method public wantsValue()Z
    .locals 1

    iget-boolean v0, p0, Ljargs/gnu/CmdLineParser$Option;->wantsValue:Z

    return v0
.end method
