.class public Ljargs/test/gnu/CustomOptionTestCase$ShortDateOption;
.super Ljargs/gnu/CmdLineParser$Option;
.source "CustomOptionTestCase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ljargs/test/gnu/CustomOptionTestCase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ShortDateOption"
.end annotation


# direct methods
.method public constructor <init>(CLjava/lang/String;)V
    .locals 1
    .param p1    # C
    .param p2    # Ljava/lang/String;

    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Ljargs/gnu/CmdLineParser$Option;-><init>(CLjava/lang/String;Z)V

    return-void
.end method


# virtual methods
.method protected parseValue(Ljava/lang/String;Ljava/util/Locale;)Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/util/Locale;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljargs/gnu/CmdLineParser$IllegalOptionValueException;
        }
    .end annotation

    const/4 v2, 0x3

    :try_start_0
    invoke-static {v2, p2}, Ljava/text/DateFormat;->getDateInstance(ILjava/util/Locale;)Ljava/text/DateFormat;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/text/DateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    return-object v2

    :catch_0
    move-exception v1

    new-instance v2, Ljargs/gnu/CmdLineParser$IllegalOptionValueException;

    invoke-direct {v2, p0, p1}, Ljargs/gnu/CmdLineParser$IllegalOptionValueException;-><init>(Ljargs/gnu/CmdLineParser$Option;Ljava/lang/String;)V

    throw v2
.end method
