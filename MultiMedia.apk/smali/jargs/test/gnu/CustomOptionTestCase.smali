.class public Ljargs/test/gnu/CustomOptionTestCase;
.super Ljunit/framework/TestCase;
.source "CustomOptionTestCase.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ljargs/test/gnu/CustomOptionTestCase$ShortDateOption;
    }
.end annotation


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Ljunit/framework/TestCase;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public testCustomOption()V
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    const/16 v10, 0xb

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    new-instance v2, Ljargs/gnu/CmdLineParser;

    invoke-direct {v2}, Ljargs/gnu/CmdLineParser;-><init>()V

    new-instance v3, Ljargs/test/gnu/CustomOptionTestCase$ShortDateOption;

    const/16 v4, 0x64

    const-string v5, "date"

    invoke-direct {v3, v4, v5}, Ljargs/test/gnu/CustomOptionTestCase$ShortDateOption;-><init>(CLjava/lang/String;)V

    invoke-virtual {v2, v3}, Ljargs/gnu/CmdLineParser;->addOption(Ljargs/gnu/CmdLineParser$Option;)Ljargs/gnu/CmdLineParser$Option;

    move-result-object v1

    new-array v3, v8, [Ljava/lang/String;

    const-string v4, "-d"

    aput-object v4, v3, v6

    const-string v4, "11/03/2003"

    aput-object v4, v3, v7

    sget-object v4, Ljava/util/Locale;->UK:Ljava/util/Locale;

    invoke-virtual {v2, v3, v4}, Ljargs/gnu/CmdLineParser;->parse([Ljava/lang/String;Ljava/util/Locale;)V

    invoke-virtual {v2, v1}, Ljargs/gnu/CmdLineParser;->getOptionValue(Ljargs/gnu/CmdLineParser$Option;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Date;

    invoke-virtual {v0}, Ljava/util/Date;->getDay()I

    move-result v3

    invoke-static {v10, v3}, Ljunit/framework/Assert;->assertEquals(II)V

    invoke-virtual {v0}, Ljava/util/Date;->getMonth()I

    move-result v3

    invoke-static {v9, v3}, Ljunit/framework/Assert;->assertEquals(II)V

    const/16 v3, 0x7d3

    invoke-virtual {v0}, Ljava/util/Date;->getYear()I

    move-result v4

    invoke-static {v3, v4}, Ljunit/framework/Assert;->assertEquals(II)V

    new-array v3, v8, [Ljava/lang/String;

    const-string v4, "-d"

    aput-object v4, v3, v6

    const-string v4, "11/03/2003"

    aput-object v4, v3, v7

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v2, v3, v4}, Ljargs/gnu/CmdLineParser;->parse([Ljava/lang/String;Ljava/util/Locale;)V

    invoke-virtual {v2, v1}, Ljargs/gnu/CmdLineParser;->getOptionValue(Ljargs/gnu/CmdLineParser$Option;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Date;

    invoke-virtual {v0}, Ljava/util/Date;->getDay()I

    move-result v3

    invoke-static {v9, v3}, Ljunit/framework/Assert;->assertEquals(II)V

    invoke-virtual {v0}, Ljava/util/Date;->getMonth()I

    move-result v3

    invoke-static {v10, v3}, Ljunit/framework/Assert;->assertEquals(II)V

    const/16 v3, 0x7d3

    invoke-virtual {v0}, Ljava/util/Date;->getYear()I

    move-result v4

    invoke-static {v3, v4}, Ljunit/framework/Assert;->assertEquals(II)V

    return-void
.end method

.method public testIllegalCustomOption()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    new-instance v1, Ljargs/gnu/CmdLineParser;

    invoke-direct {v1}, Ljargs/gnu/CmdLineParser;-><init>()V

    new-instance v2, Ljargs/test/gnu/CustomOptionTestCase$ShortDateOption;

    const/16 v3, 0x64

    const-string v4, "date"

    invoke-direct {v2, v3, v4}, Ljargs/test/gnu/CustomOptionTestCase$ShortDateOption;-><init>(CLjava/lang/String;)V

    invoke-virtual {v1, v2}, Ljargs/gnu/CmdLineParser;->addOption(Ljargs/gnu/CmdLineParser$Option;)Ljargs/gnu/CmdLineParser$Option;

    move-result-object v0

    const/4 v2, 0x2

    :try_start_0
    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "-d"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "foobar"

    aput-object v4, v2, v3

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v2, v3}, Ljargs/gnu/CmdLineParser;->parse([Ljava/lang/String;Ljava/util/Locale;)V

    const-string v2, "Expected IllegalOptionValueException"

    invoke-static {v2}, Ljunit/framework/Assert;->fail(Ljava/lang/String;)V
    :try_end_0
    .catch Ljargs/gnu/CmdLineParser$IllegalOptionValueException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v2

    goto :goto_0
.end method
