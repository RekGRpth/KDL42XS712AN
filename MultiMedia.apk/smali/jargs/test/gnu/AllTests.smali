.class public Ljargs/test/gnu/AllTests;
.super Ljava/lang/Object;
.source "AllTests.java"


# static fields
.field static class$jargs$test$gnu$CmdLineParserTestCase:Ljava/lang/Class;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static class$(Ljava/lang/String;)Ljava/lang/Class;
    .locals 3
    .param p0    # Ljava/lang/String;

    :try_start_0
    invoke-static {p0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/NoClassDefFoundError;

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/NoClassDefFoundError;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public static suite()Ljunit/framework/TestSuite;
    .locals 2

    new-instance v0, Ljunit/framework/TestSuite;

    invoke-direct {v0}, Ljunit/framework/TestSuite;-><init>()V

    sget-object v1, Ljargs/test/gnu/AllTests;->class$jargs$test$gnu$CmdLineParserTestCase:Ljava/lang/Class;

    if-nez v1, :cond_0

    const-string v1, "jargs.test.gnu.CmdLineParserTestCase"

    invoke-static {v1}, Ljargs/test/gnu/AllTests;->class$(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    sput-object v1, Ljargs/test/gnu/AllTests;->class$jargs$test$gnu$CmdLineParserTestCase:Ljava/lang/Class;

    :goto_0
    invoke-virtual {v0, v1}, Ljunit/framework/TestSuite;->addTestSuite(Ljava/lang/Class;)V

    return-object v0

    :cond_0
    sget-object v1, Ljargs/test/gnu/AllTests;->class$jargs$test$gnu$CmdLineParserTestCase:Ljava/lang/Class;

    goto :goto_0
.end method
