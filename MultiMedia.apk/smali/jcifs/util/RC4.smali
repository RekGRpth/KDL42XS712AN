.class public Ljcifs/util/RC4;
.super Ljava/lang/Object;
.source "RC4.java"


# instance fields
.field i:I

.field j:I

.field s:[B


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>([B)V
    .locals 2
    .param p1    # [B

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, Ljcifs/util/RC4;->init([BII)V

    return-void
.end method


# virtual methods
.method public init([BII)V
    .locals 7
    .param p1    # [B
    .param p2    # I
    .param p3    # I

    const/16 v6, 0x100

    const/4 v5, 0x0

    new-array v1, v6, [B

    iput-object v1, p0, Ljcifs/util/RC4;->s:[B

    iput v5, p0, Ljcifs/util/RC4;->i:I

    :goto_0
    iget v1, p0, Ljcifs/util/RC4;->i:I

    if-ge v1, v6, :cond_0

    iget-object v1, p0, Ljcifs/util/RC4;->s:[B

    iget v2, p0, Ljcifs/util/RC4;->i:I

    iget v3, p0, Ljcifs/util/RC4;->i:I

    int-to-byte v3, v3

    aput-byte v3, v1, v2

    iget v1, p0, Ljcifs/util/RC4;->i:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Ljcifs/util/RC4;->i:I

    goto :goto_0

    :cond_0
    iput v5, p0, Ljcifs/util/RC4;->j:I

    iput v5, p0, Ljcifs/util/RC4;->i:I

    :goto_1
    iget v1, p0, Ljcifs/util/RC4;->i:I

    if-ge v1, v6, :cond_1

    iget v1, p0, Ljcifs/util/RC4;->j:I

    iget v2, p0, Ljcifs/util/RC4;->i:I

    rem-int/2addr v2, p3

    add-int/2addr v2, p2

    aget-byte v2, p1, v2

    add-int/2addr v1, v2

    iget-object v2, p0, Ljcifs/util/RC4;->s:[B

    iget v3, p0, Ljcifs/util/RC4;->i:I

    aget-byte v2, v2, v3

    add-int/2addr v1, v2

    and-int/lit16 v1, v1, 0xff

    iput v1, p0, Ljcifs/util/RC4;->j:I

    iget-object v1, p0, Ljcifs/util/RC4;->s:[B

    iget v2, p0, Ljcifs/util/RC4;->i:I

    aget-byte v0, v1, v2

    iget-object v1, p0, Ljcifs/util/RC4;->s:[B

    iget v2, p0, Ljcifs/util/RC4;->i:I

    iget-object v3, p0, Ljcifs/util/RC4;->s:[B

    iget v4, p0, Ljcifs/util/RC4;->j:I

    aget-byte v3, v3, v4

    aput-byte v3, v1, v2

    iget-object v1, p0, Ljcifs/util/RC4;->s:[B

    iget v2, p0, Ljcifs/util/RC4;->j:I

    aput-byte v0, v1, v2

    iget v1, p0, Ljcifs/util/RC4;->i:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Ljcifs/util/RC4;->i:I

    goto :goto_1

    :cond_1
    iput v5, p0, Ljcifs/util/RC4;->j:I

    iput v5, p0, Ljcifs/util/RC4;->i:I

    return-void
.end method

.method public update([BII[BI)V
    .locals 9
    .param p1    # [B
    .param p2    # I
    .param p3    # I
    .param p4    # [B
    .param p5    # I

    add-int v1, p2, p3

    move v0, p5

    move v2, p2

    :goto_0
    if-ge v2, v1, :cond_0

    iget v4, p0, Ljcifs/util/RC4;->i:I

    add-int/lit8 v4, v4, 0x1

    and-int/lit16 v4, v4, 0xff

    iput v4, p0, Ljcifs/util/RC4;->i:I

    iget v4, p0, Ljcifs/util/RC4;->j:I

    iget-object v5, p0, Ljcifs/util/RC4;->s:[B

    iget v6, p0, Ljcifs/util/RC4;->i:I

    aget-byte v5, v5, v6

    add-int/2addr v4, v5

    and-int/lit16 v4, v4, 0xff

    iput v4, p0, Ljcifs/util/RC4;->j:I

    iget-object v4, p0, Ljcifs/util/RC4;->s:[B

    iget v5, p0, Ljcifs/util/RC4;->i:I

    aget-byte v3, v4, v5

    iget-object v4, p0, Ljcifs/util/RC4;->s:[B

    iget v5, p0, Ljcifs/util/RC4;->i:I

    iget-object v6, p0, Ljcifs/util/RC4;->s:[B

    iget v7, p0, Ljcifs/util/RC4;->j:I

    aget-byte v6, v6, v7

    aput-byte v6, v4, v5

    iget-object v4, p0, Ljcifs/util/RC4;->s:[B

    iget v5, p0, Ljcifs/util/RC4;->j:I

    aput-byte v3, v4, v5

    add-int/lit8 p5, v0, 0x1

    add-int/lit8 p2, v2, 0x1

    aget-byte v4, p1, v2

    iget-object v5, p0, Ljcifs/util/RC4;->s:[B

    iget-object v6, p0, Ljcifs/util/RC4;->s:[B

    iget v7, p0, Ljcifs/util/RC4;->i:I

    aget-byte v6, v6, v7

    iget-object v7, p0, Ljcifs/util/RC4;->s:[B

    iget v8, p0, Ljcifs/util/RC4;->j:I

    aget-byte v7, v7, v8

    add-int/2addr v6, v7

    and-int/lit16 v6, v6, 0xff

    aget-byte v5, v5, v6

    xor-int/2addr v4, v5

    int-to-byte v4, v4

    aput-byte v4, p4, v0

    move v0, p5

    move v2, p2

    goto :goto_0

    :cond_0
    return-void
.end method
