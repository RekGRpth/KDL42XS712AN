.class Ljcifs/netbios/NodeStatusResponse;
.super Ljcifs/netbios/NameServicePacket;
.source "NodeStatusResponse.java"


# instance fields
.field addressArray:[Ljcifs/netbios/NbtAddress;

.field private macAddress:[B

.field private numberOfNames:I

.field private queryAddress:Ljcifs/netbios/NbtAddress;

.field private stats:[B


# direct methods
.method constructor <init>(Ljcifs/netbios/NbtAddress;)V
    .locals 1
    .param p1    # Ljcifs/netbios/NbtAddress;

    invoke-direct {p0}, Ljcifs/netbios/NameServicePacket;-><init>()V

    iput-object p1, p0, Ljcifs/netbios/NodeStatusResponse;->queryAddress:Ljcifs/netbios/NbtAddress;

    new-instance v0, Ljcifs/netbios/Name;

    invoke-direct {v0}, Ljcifs/netbios/Name;-><init>()V

    iput-object v0, p0, Ljcifs/netbios/NodeStatusResponse;->recordName:Ljcifs/netbios/Name;

    const/4 v0, 0x6

    new-array v0, v0, [B

    iput-object v0, p0, Ljcifs/netbios/NodeStatusResponse;->macAddress:[B

    return-void
.end method

.method private readNodeNameArray([BI)I
    .locals 21
    .param p1    # [B
    .param p2    # I

    move/from16 v19, p2

    move-object/from16 v0, p0

    iget v3, v0, Ljcifs/netbios/NodeStatusResponse;->numberOfNames:I

    new-array v3, v3, [Ljcifs/netbios/NbtAddress;

    move-object/from16 v0, p0

    iput-object v3, v0, Ljcifs/netbios/NodeStatusResponse;->addressArray:[Ljcifs/netbios/NbtAddress;

    move-object/from16 v0, p0

    iget-object v3, v0, Ljcifs/netbios/NodeStatusResponse;->queryAddress:Ljcifs/netbios/NbtAddress;

    iget-object v3, v3, Ljcifs/netbios/NbtAddress;->hostName:Ljcifs/netbios/Name;

    iget-object v0, v3, Ljcifs/netbios/Name;->scope:Ljava/lang/String;

    move-object/from16 v18, v0

    const/4 v13, 0x0

    const/4 v15, 0x0

    :goto_0
    :try_start_0
    move-object/from16 v0, p0

    iget v3, v0, Ljcifs/netbios/NodeStatusResponse;->numberOfNames:I

    if-ge v15, v3, :cond_9

    add-int/lit8 v16, p2, 0xe

    :goto_1
    aget-byte v3, p1, v16

    const/16 v4, 0x20

    if-ne v3, v4, :cond_0

    add-int/lit8 v16, v16, -0x1

    goto :goto_1

    :cond_0
    new-instance v17, Ljava/lang/String;

    sub-int v3, v16, p2

    add-int/lit8 v3, v3, 0x1

    sget-object v4, Ljcifs/netbios/Name;->OEM_ENCODING:Ljava/lang/String;

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    move/from16 v2, p2

    invoke-direct {v0, v1, v2, v3, v4}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    add-int/lit8 v3, p2, 0xf

    aget-byte v3, p1, v3

    and-int/lit16 v14, v3, 0xff

    add-int/lit8 v3, p2, 0x10

    aget-byte v3, p1, v3

    and-int/lit16 v3, v3, 0x80

    const/16 v4, 0x80

    if-ne v3, v4, :cond_3

    const/4 v6, 0x1

    :goto_2
    add-int/lit8 v3, p2, 0x10

    aget-byte v3, p1, v3

    and-int/lit8 v3, v3, 0x60

    shr-int/lit8 v7, v3, 0x5

    add-int/lit8 v3, p2, 0x10

    aget-byte v3, p1, v3

    and-int/lit8 v3, v3, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_4

    const/4 v8, 0x1

    :goto_3
    add-int/lit8 v3, p2, 0x10

    aget-byte v3, p1, v3

    and-int/lit8 v3, v3, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_5

    const/4 v9, 0x1

    :goto_4
    add-int/lit8 v3, p2, 0x10

    aget-byte v3, p1, v3

    and-int/lit8 v3, v3, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_6

    const/4 v10, 0x1

    :goto_5
    add-int/lit8 v3, p2, 0x10

    aget-byte v3, p1, v3

    and-int/lit8 v3, v3, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_7

    const/4 v11, 0x1

    :goto_6
    if-nez v13, :cond_8

    move-object/from16 v0, p0

    iget-object v3, v0, Ljcifs/netbios/NodeStatusResponse;->queryAddress:Ljcifs/netbios/NbtAddress;

    iget-object v3, v3, Ljcifs/netbios/NbtAddress;->hostName:Ljcifs/netbios/Name;

    iget v3, v3, Ljcifs/netbios/Name;->hexCode:I

    if-ne v3, v14, :cond_8

    move-object/from16 v0, p0

    iget-object v3, v0, Ljcifs/netbios/NodeStatusResponse;->queryAddress:Ljcifs/netbios/NbtAddress;

    iget-object v3, v3, Ljcifs/netbios/NbtAddress;->hostName:Ljcifs/netbios/Name;

    sget-object v4, Ljcifs/netbios/NbtAddress;->UNKNOWN_NAME:Ljcifs/netbios/Name;

    if-eq v3, v4, :cond_1

    move-object/from16 v0, p0

    iget-object v3, v0, Ljcifs/netbios/NodeStatusResponse;->queryAddress:Ljcifs/netbios/NbtAddress;

    iget-object v3, v3, Ljcifs/netbios/NbtAddress;->hostName:Ljcifs/netbios/Name;

    iget-object v3, v3, Ljcifs/netbios/Name;->name:Ljava/lang/String;

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Ljcifs/netbios/NodeStatusResponse;->queryAddress:Ljcifs/netbios/NbtAddress;

    iget-object v3, v3, Ljcifs/netbios/NbtAddress;->hostName:Ljcifs/netbios/Name;

    sget-object v4, Ljcifs/netbios/NbtAddress;->UNKNOWN_NAME:Ljcifs/netbios/Name;

    if-ne v3, v4, :cond_2

    move-object/from16 v0, p0

    iget-object v3, v0, Ljcifs/netbios/NodeStatusResponse;->queryAddress:Ljcifs/netbios/NbtAddress;

    new-instance v4, Ljcifs/netbios/Name;

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-direct {v4, v0, v14, v1}, Ljcifs/netbios/Name;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    iput-object v4, v3, Ljcifs/netbios/NbtAddress;->hostName:Ljcifs/netbios/Name;

    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, Ljcifs/netbios/NodeStatusResponse;->queryAddress:Ljcifs/netbios/NbtAddress;

    iput-boolean v6, v3, Ljcifs/netbios/NbtAddress;->groupName:Z

    move-object/from16 v0, p0

    iget-object v3, v0, Ljcifs/netbios/NodeStatusResponse;->queryAddress:Ljcifs/netbios/NbtAddress;

    iput v7, v3, Ljcifs/netbios/NbtAddress;->nodeType:I

    move-object/from16 v0, p0

    iget-object v3, v0, Ljcifs/netbios/NodeStatusResponse;->queryAddress:Ljcifs/netbios/NbtAddress;

    iput-boolean v8, v3, Ljcifs/netbios/NbtAddress;->isBeingDeleted:Z

    move-object/from16 v0, p0

    iget-object v3, v0, Ljcifs/netbios/NodeStatusResponse;->queryAddress:Ljcifs/netbios/NbtAddress;

    iput-boolean v9, v3, Ljcifs/netbios/NbtAddress;->isInConflict:Z

    move-object/from16 v0, p0

    iget-object v3, v0, Ljcifs/netbios/NodeStatusResponse;->queryAddress:Ljcifs/netbios/NbtAddress;

    iput-boolean v10, v3, Ljcifs/netbios/NbtAddress;->isActive:Z

    move-object/from16 v0, p0

    iget-object v3, v0, Ljcifs/netbios/NodeStatusResponse;->queryAddress:Ljcifs/netbios/NbtAddress;

    iput-boolean v11, v3, Ljcifs/netbios/NbtAddress;->isPermanent:Z

    move-object/from16 v0, p0

    iget-object v3, v0, Ljcifs/netbios/NodeStatusResponse;->queryAddress:Ljcifs/netbios/NbtAddress;

    move-object/from16 v0, p0

    iget-object v4, v0, Ljcifs/netbios/NodeStatusResponse;->macAddress:[B

    iput-object v4, v3, Ljcifs/netbios/NbtAddress;->macAddress:[B

    move-object/from16 v0, p0

    iget-object v3, v0, Ljcifs/netbios/NodeStatusResponse;->queryAddress:Ljcifs/netbios/NbtAddress;

    const/4 v4, 0x1

    iput-boolean v4, v3, Ljcifs/netbios/NbtAddress;->isDataFromNodeStatus:Z

    const/4 v13, 0x1

    move-object/from16 v0, p0

    iget-object v3, v0, Ljcifs/netbios/NodeStatusResponse;->addressArray:[Ljcifs/netbios/NbtAddress;

    move-object/from16 v0, p0

    iget-object v4, v0, Ljcifs/netbios/NodeStatusResponse;->queryAddress:Ljcifs/netbios/NbtAddress;

    aput-object v4, v3, v15

    :goto_7
    add-int/lit8 p2, p2, 0x12

    add-int/lit8 v15, v15, 0x1

    goto/16 :goto_0

    :cond_3
    const/4 v6, 0x0

    goto/16 :goto_2

    :cond_4
    const/4 v8, 0x0

    goto/16 :goto_3

    :cond_5
    const/4 v9, 0x0

    goto/16 :goto_4

    :cond_6
    const/4 v10, 0x0

    goto/16 :goto_5

    :cond_7
    const/4 v11, 0x0

    goto/16 :goto_6

    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Ljcifs/netbios/NodeStatusResponse;->addressArray:[Ljcifs/netbios/NbtAddress;

    move-object/from16 v20, v0

    new-instance v3, Ljcifs/netbios/NbtAddress;

    new-instance v4, Ljcifs/netbios/Name;

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-direct {v4, v0, v14, v1}, Ljcifs/netbios/Name;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v5, v0, Ljcifs/netbios/NodeStatusResponse;->queryAddress:Ljcifs/netbios/NbtAddress;

    iget v5, v5, Ljcifs/netbios/NbtAddress;->address:I

    move-object/from16 v0, p0

    iget-object v12, v0, Ljcifs/netbios/NodeStatusResponse;->macAddress:[B

    invoke-direct/range {v3 .. v12}, Ljcifs/netbios/NbtAddress;-><init>(Ljcifs/netbios/Name;IZIZZZZ[B)V

    aput-object v3, v20, v15
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_7

    :catch_0
    move-exception v3

    :cond_9
    sub-int v3, p2, v19

    return v3
.end method


# virtual methods
.method readBodyWireFormat([BI)I
    .locals 1
    .param p1    # [B
    .param p2    # I

    invoke-virtual {p0, p1, p2}, Ljcifs/netbios/NodeStatusResponse;->readResourceRecordWireFormat([BI)I

    move-result v0

    return v0
.end method

.method readRDataWireFormat([BI)I
    .locals 8
    .param p1    # [B
    .param p2    # I

    const/4 v7, 0x0

    move v2, p2

    aget-byte v4, p1, p2

    and-int/lit16 v4, v4, 0xff

    iput v4, p0, Ljcifs/netbios/NodeStatusResponse;->numberOfNames:I

    iget v4, p0, Ljcifs/netbios/NodeStatusResponse;->numberOfNames:I

    mul-int/lit8 v0, v4, 0x12

    iget v4, p0, Ljcifs/netbios/NodeStatusResponse;->rDataLength:I

    sub-int/2addr v4, v0

    add-int/lit8 v3, v4, -0x1

    add-int/lit8 v1, p2, 0x1

    aget-byte v4, p1, p2

    and-int/lit16 v4, v4, 0xff

    iput v4, p0, Ljcifs/netbios/NodeStatusResponse;->numberOfNames:I

    add-int v4, v1, v0

    iget-object v5, p0, Ljcifs/netbios/NodeStatusResponse;->macAddress:[B

    const/4 v6, 0x6

    invoke-static {p1, v4, v5, v7, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    invoke-direct {p0, p1, v1}, Ljcifs/netbios/NodeStatusResponse;->readNodeNameArray([BI)I

    move-result v4

    add-int p2, v1, v4

    new-array v4, v3, [B

    iput-object v4, p0, Ljcifs/netbios/NodeStatusResponse;->stats:[B

    iget-object v4, p0, Ljcifs/netbios/NodeStatusResponse;->stats:[B

    invoke-static {p1, p2, v4, v7, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/2addr p2, v3

    sub-int v4, p2, v2

    return v4
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "NodeStatusResponse["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-super {p0}, Ljcifs/netbios/NameServicePacket;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method writeBodyWireFormat([BI)I
    .locals 1
    .param p1    # [B
    .param p2    # I

    const/4 v0, 0x0

    return v0
.end method

.method writeRDataWireFormat([BI)I
    .locals 1
    .param p1    # [B
    .param p2    # I

    const/4 v0, 0x0

    return v0
.end method
