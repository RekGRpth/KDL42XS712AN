.class Ljcifs/netbios/NameServiceClient;
.super Ljava/lang/Object;
.source "NameServiceClient.java"

# interfaces
.implements Ljava/lang/Runnable;


# static fields
.field static final DEFAULT_RCV_BUF_SIZE:I = 0x240

.field static final DEFAULT_RETRY_COUNT:I = 0x2

.field static final DEFAULT_RETRY_TIMEOUT:I = 0xbb8

.field static final DEFAULT_SND_BUF_SIZE:I = 0x240

.field static final DEFAULT_SO_TIMEOUT:I = 0x1388

.field private static final LADDR:Ljava/net/InetAddress;

.field private static final LPORT:I

.field static final NAME_SERVICE_UDP_PORT:I = 0x89

.field private static final RCV_BUF_SIZE:I

.field static final RESOLVER_BCAST:I = 0x2

.field static final RESOLVER_LMHOSTS:I = 0x1

.field static final RESOLVER_WINS:I = 0x3

.field private static final RETRY_COUNT:I

.field private static final RETRY_TIMEOUT:I

.field private static final RO:Ljava/lang/String;

.field private static final SND_BUF_SIZE:I

.field private static final SO_TIMEOUT:I

.field private static log:Ljcifs/util/LogStream;


# instance fields
.field private final LOCK:Ljava/lang/Object;

.field baddr:Ljava/net/InetAddress;

.field private closeTimeout:I

.field private in:Ljava/net/DatagramPacket;

.field laddr:Ljava/net/InetAddress;

.field private lport:I

.field private nextNameTrnId:I

.field private out:Ljava/net/DatagramPacket;

.field private rcv_buf:[B

.field private resolveOrder:[I

.field private responseTable:Ljava/util/HashMap;

.field private snd_buf:[B

.field private socket:Ljava/net/DatagramSocket;

.field private thread:Ljava/lang/Thread;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/16 v1, 0x240

    const-string v0, "jcifs.netbios.snd_buf_size"

    invoke-static {v0, v1}, Ljcifs/Config;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Ljcifs/netbios/NameServiceClient;->SND_BUF_SIZE:I

    const-string v0, "jcifs.netbios.rcv_buf_size"

    invoke-static {v0, v1}, Ljcifs/Config;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Ljcifs/netbios/NameServiceClient;->RCV_BUF_SIZE:I

    const-string v0, "jcifs.netbios.soTimeout"

    const/16 v1, 0x1388

    invoke-static {v0, v1}, Ljcifs/Config;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Ljcifs/netbios/NameServiceClient;->SO_TIMEOUT:I

    const-string v0, "jcifs.netbios.retryCount"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Ljcifs/Config;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Ljcifs/netbios/NameServiceClient;->RETRY_COUNT:I

    const-string v0, "jcifs.netbios.retryTimeout"

    const/16 v1, 0xbb8

    invoke-static {v0, v1}, Ljcifs/Config;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Ljcifs/netbios/NameServiceClient;->RETRY_TIMEOUT:I

    const-string v0, "jcifs.netbios.lport"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljcifs/Config;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Ljcifs/netbios/NameServiceClient;->LPORT:I

    const-string v0, "jcifs.netbios.laddr"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljcifs/Config;->getInetAddress(Ljava/lang/String;Ljava/net/InetAddress;)Ljava/net/InetAddress;

    move-result-object v0

    sput-object v0, Ljcifs/netbios/NameServiceClient;->LADDR:Ljava/net/InetAddress;

    const-string v0, "jcifs.resolveOrder"

    invoke-static {v0}, Ljcifs/Config;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ljcifs/netbios/NameServiceClient;->RO:Ljava/lang/String;

    invoke-static {}, Ljcifs/util/LogStream;->getInstance()Ljcifs/util/LogStream;

    move-result-object v0

    sput-object v0, Ljcifs/netbios/NameServiceClient;->log:Ljcifs/util/LogStream;

    return-void
.end method

.method constructor <init>()V
    .locals 2

    sget v0, Ljcifs/netbios/NameServiceClient;->LPORT:I

    sget-object v1, Ljcifs/netbios/NameServiceClient;->LADDR:Ljava/net/InetAddress;

    invoke-direct {p0, v0, v1}, Ljcifs/netbios/NameServiceClient;-><init>(ILjava/net/InetAddress;)V

    return-void
.end method

.method constructor <init>(ILjava/net/InetAddress;)V
    .locals 10
    .param p1    # I
    .param p2    # Ljava/net/InetAddress;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v5, Ljava/lang/Object;

    invoke-direct {v5}, Ljava/lang/Object;-><init>()V

    iput-object v5, p0, Ljcifs/netbios/NameServiceClient;->LOCK:Ljava/lang/Object;

    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    iput-object v5, p0, Ljcifs/netbios/NameServiceClient;->responseTable:Ljava/util/HashMap;

    const/4 v5, 0x0

    iput v5, p0, Ljcifs/netbios/NameServiceClient;->nextNameTrnId:I

    iput p1, p0, Ljcifs/netbios/NameServiceClient;->lport:I

    iput-object p2, p0, Ljcifs/netbios/NameServiceClient;->laddr:Ljava/net/InetAddress;

    :try_start_0
    const-string v5, "jcifs.netbios.baddr"

    const-string v6, "255.255.255.255"

    invoke-static {v6}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v6

    invoke-static {v5, v6}, Ljcifs/Config;->getInetAddress(Ljava/lang/String;Ljava/net/InetAddress;)Ljava/net/InetAddress;

    move-result-object v5

    iput-object v5, p0, Ljcifs/netbios/NameServiceClient;->baddr:Ljava/net/InetAddress;
    :try_end_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    sget v5, Ljcifs/netbios/NameServiceClient;->SND_BUF_SIZE:I

    new-array v5, v5, [B

    iput-object v5, p0, Ljcifs/netbios/NameServiceClient;->snd_buf:[B

    sget v5, Ljcifs/netbios/NameServiceClient;->RCV_BUF_SIZE:I

    new-array v5, v5, [B

    iput-object v5, p0, Ljcifs/netbios/NameServiceClient;->rcv_buf:[B

    new-instance v5, Ljava/net/DatagramPacket;

    iget-object v6, p0, Ljcifs/netbios/NameServiceClient;->snd_buf:[B

    sget v7, Ljcifs/netbios/NameServiceClient;->SND_BUF_SIZE:I

    iget-object v8, p0, Ljcifs/netbios/NameServiceClient;->baddr:Ljava/net/InetAddress;

    const/16 v9, 0x89

    invoke-direct {v5, v6, v7, v8, v9}, Ljava/net/DatagramPacket;-><init>([BILjava/net/InetAddress;I)V

    iput-object v5, p0, Ljcifs/netbios/NameServiceClient;->out:Ljava/net/DatagramPacket;

    new-instance v5, Ljava/net/DatagramPacket;

    iget-object v6, p0, Ljcifs/netbios/NameServiceClient;->rcv_buf:[B

    sget v7, Ljcifs/netbios/NameServiceClient;->RCV_BUF_SIZE:I

    invoke-direct {v5, v6, v7}, Ljava/net/DatagramPacket;-><init>([BI)V

    iput-object v5, p0, Ljcifs/netbios/NameServiceClient;->in:Ljava/net/DatagramPacket;

    sget-object v5, Ljcifs/netbios/NameServiceClient;->RO:Ljava/lang/String;

    if-eqz v5, :cond_0

    sget-object v5, Ljcifs/netbios/NameServiceClient;->RO:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-nez v5, :cond_2

    :cond_0
    invoke-static {}, Ljcifs/netbios/NbtAddress;->getWINSAddress()Ljava/net/InetAddress;

    move-result-object v5

    if-nez v5, :cond_1

    const/4 v5, 0x2

    new-array v5, v5, [I

    iput-object v5, p0, Ljcifs/netbios/NameServiceClient;->resolveOrder:[I

    iget-object v5, p0, Ljcifs/netbios/NameServiceClient;->resolveOrder:[I

    const/4 v6, 0x0

    const/4 v7, 0x1

    aput v7, v5, v6

    iget-object v5, p0, Ljcifs/netbios/NameServiceClient;->resolveOrder:[I

    const/4 v6, 0x1

    const/4 v7, 0x2

    aput v7, v5, v6

    :goto_1
    return-void

    :cond_1
    const/4 v5, 0x3

    new-array v5, v5, [I

    iput-object v5, p0, Ljcifs/netbios/NameServiceClient;->resolveOrder:[I

    iget-object v5, p0, Ljcifs/netbios/NameServiceClient;->resolveOrder:[I

    const/4 v6, 0x0

    const/4 v7, 0x1

    aput v7, v5, v6

    iget-object v5, p0, Ljcifs/netbios/NameServiceClient;->resolveOrder:[I

    const/4 v6, 0x1

    const/4 v7, 0x3

    aput v7, v5, v6

    iget-object v5, p0, Ljcifs/netbios/NameServiceClient;->resolveOrder:[I

    const/4 v6, 0x2

    const/4 v7, 0x2

    aput v7, v5, v6

    goto :goto_1

    :cond_2
    const/4 v5, 0x3

    new-array v4, v5, [I

    new-instance v3, Ljava/util/StringTokenizer;

    sget-object v5, Ljcifs/netbios/NameServiceClient;->RO:Ljava/lang/String;

    const-string v6, ","

    invoke-direct {v3, v5, v6}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    :cond_3
    :goto_2
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v5

    if-eqz v5, :cond_8

    invoke-virtual {v3}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    const-string v5, "LMHOSTS"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    add-int/lit8 v1, v0, 0x1

    const/4 v5, 0x1

    aput v5, v4, v0

    move v0, v1

    goto :goto_2

    :cond_4
    const-string v5, "WINS"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_6

    invoke-static {}, Ljcifs/netbios/NbtAddress;->getWINSAddress()Ljava/net/InetAddress;

    move-result-object v5

    if-nez v5, :cond_5

    sget-object v5, Ljcifs/netbios/NameServiceClient;->log:Ljcifs/util/LogStream;

    sget v5, Ljcifs/util/LogStream;->level:I

    const/4 v6, 0x1

    if-le v5, v6, :cond_3

    sget-object v5, Ljcifs/netbios/NameServiceClient;->log:Ljcifs/util/LogStream;

    const-string v6, "NetBIOS resolveOrder specifies WINS however the jcifs.netbios.wins property has not been set"

    invoke-virtual {v5, v6}, Ljcifs/util/LogStream;->println(Ljava/lang/String;)V

    goto :goto_2

    :cond_5
    add-int/lit8 v1, v0, 0x1

    const/4 v5, 0x3

    aput v5, v4, v0

    move v0, v1

    goto :goto_2

    :cond_6
    const-string v5, "BCAST"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_7

    add-int/lit8 v1, v0, 0x1

    const/4 v5, 0x2

    aput v5, v4, v0

    move v0, v1

    goto :goto_2

    :cond_7
    const-string v5, "DNS"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_3

    sget-object v5, Ljcifs/netbios/NameServiceClient;->log:Ljcifs/util/LogStream;

    sget v5, Ljcifs/util/LogStream;->level:I

    const/4 v6, 0x1

    if-le v5, v6, :cond_3

    sget-object v5, Ljcifs/netbios/NameServiceClient;->log:Ljcifs/util/LogStream;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "unknown resolver method: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljcifs/util/LogStream;->println(Ljava/lang/String;)V

    goto :goto_2

    :cond_8
    new-array v5, v0, [I

    iput-object v5, p0, Ljcifs/netbios/NameServiceClient;->resolveOrder:[I

    const/4 v5, 0x0

    iget-object v6, p0, Ljcifs/netbios/NameServiceClient;->resolveOrder:[I

    const/4 v7, 0x0

    invoke-static {v4, v5, v6, v7, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto/16 :goto_1

    :catch_0
    move-exception v5

    goto/16 :goto_0
.end method


# virtual methods
.method ensureOpen(I)V
    .locals 3
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x0

    iput v0, p0, Ljcifs/netbios/NameServiceClient;->closeTimeout:I

    sget v0, Ljcifs/netbios/NameServiceClient;->SO_TIMEOUT:I

    if-eqz v0, :cond_0

    sget v0, Ljcifs/netbios/NameServiceClient;->SO_TIMEOUT:I

    invoke-static {v0, p1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Ljcifs/netbios/NameServiceClient;->closeTimeout:I

    :cond_0
    iget-object v0, p0, Ljcifs/netbios/NameServiceClient;->socket:Ljava/net/DatagramSocket;

    if-nez v0, :cond_1

    new-instance v0, Ljava/net/DatagramSocket;

    iget v1, p0, Ljcifs/netbios/NameServiceClient;->lport:I

    iget-object v2, p0, Ljcifs/netbios/NameServiceClient;->laddr:Ljava/net/InetAddress;

    invoke-direct {v0, v1, v2}, Ljava/net/DatagramSocket;-><init>(ILjava/net/InetAddress;)V

    iput-object v0, p0, Ljcifs/netbios/NameServiceClient;->socket:Ljava/net/DatagramSocket;

    new-instance v0, Ljava/lang/Thread;

    const-string v1, "JCIFS-NameServiceClient"

    invoke-direct {v0, p0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    iput-object v0, p0, Ljcifs/netbios/NameServiceClient;->thread:Ljava/lang/Thread;

    iget-object v0, p0, Ljcifs/netbios/NameServiceClient;->thread:Ljava/lang/Thread;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setDaemon(Z)V

    iget-object v0, p0, Ljcifs/netbios/NameServiceClient;->thread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    :cond_1
    return-void
.end method

.method getAllByName(Ljcifs/netbios/Name;Ljava/net/InetAddress;)[Ljcifs/netbios/NbtAddress;
    .locals 7
    .param p1    # Ljcifs/netbios/Name;
    .param p2    # Ljava/net/InetAddress;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/UnknownHostException;
        }
    .end annotation

    const/4 v5, 0x1

    const/4 v6, 0x0

    new-instance v2, Ljcifs/netbios/NameQueryRequest;

    invoke-direct {v2, p1}, Ljcifs/netbios/NameQueryRequest;-><init>(Ljcifs/netbios/Name;)V

    new-instance v3, Ljcifs/netbios/NameQueryResponse;

    invoke-direct {v3}, Ljcifs/netbios/NameQueryResponse;-><init>()V

    if-eqz p2, :cond_1

    :goto_0
    iput-object p2, v2, Ljcifs/netbios/NameQueryRequest;->addr:Ljava/net/InetAddress;

    iget-object v4, v2, Ljcifs/netbios/NameQueryRequest;->addr:Ljava/net/InetAddress;

    if-nez v4, :cond_2

    move v4, v5

    :goto_1
    iput-boolean v4, v2, Ljcifs/netbios/NameQueryRequest;->isBroadcast:Z

    iget-boolean v4, v2, Ljcifs/netbios/NameQueryRequest;->isBroadcast:Z

    if-eqz v4, :cond_3

    iget-object v4, p0, Ljcifs/netbios/NameServiceClient;->baddr:Ljava/net/InetAddress;

    iput-object v4, v2, Ljcifs/netbios/NameQueryRequest;->addr:Ljava/net/InetAddress;

    sget v1, Ljcifs/netbios/NameServiceClient;->RETRY_COUNT:I

    :cond_0
    :goto_2
    :try_start_0
    sget v4, Ljcifs/netbios/NameServiceClient;->RETRY_TIMEOUT:I

    invoke-virtual {p0, v2, v3, v4}, Ljcifs/netbios/NameServiceClient;->send(Ljcifs/netbios/NameServicePacket;Ljcifs/netbios/NameServicePacket;I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    iget-boolean v4, v3, Ljcifs/netbios/NameQueryResponse;->received:Z

    if-eqz v4, :cond_5

    iget v4, v3, Ljcifs/netbios/NameQueryResponse;->resultCode:I

    if-nez v4, :cond_5

    iget-object v4, v3, Ljcifs/netbios/NameQueryResponse;->addrEntry:[Ljcifs/netbios/NbtAddress;

    return-object v4

    :cond_1
    invoke-static {}, Ljcifs/netbios/NbtAddress;->getWINSAddress()Ljava/net/InetAddress;

    move-result-object p2

    goto :goto_0

    :cond_2
    move v4, v6

    goto :goto_1

    :cond_3
    iput-boolean v6, v2, Ljcifs/netbios/NameQueryRequest;->isBroadcast:Z

    const/4 v1, 0x1

    goto :goto_2

    :catch_0
    move-exception v0

    sget-object v4, Ljcifs/netbios/NameServiceClient;->log:Ljcifs/util/LogStream;

    sget v4, Ljcifs/util/LogStream;->level:I

    if-le v4, v5, :cond_4

    sget-object v4, Ljcifs/netbios/NameServiceClient;->log:Ljcifs/util/LogStream;

    invoke-virtual {v0, v4}, Ljava/io/IOException;->printStackTrace(Ljava/io/PrintStream;)V

    :cond_4
    new-instance v4, Ljava/net/UnknownHostException;

    iget-object v5, p1, Ljcifs/netbios/Name;->name:Ljava/lang/String;

    invoke-direct {v4, v5}, Ljava/net/UnknownHostException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_5
    add-int/lit8 v1, v1, -0x1

    if-lez v1, :cond_6

    iget-boolean v4, v2, Ljcifs/netbios/NameQueryRequest;->isBroadcast:Z

    if-nez v4, :cond_0

    :cond_6
    new-instance v4, Ljava/net/UnknownHostException;

    iget-object v5, p1, Ljcifs/netbios/Name;->name:Ljava/lang/String;

    invoke-direct {v4, v5}, Ljava/net/UnknownHostException;-><init>(Ljava/lang/String;)V

    throw v4
.end method

.method getByName(Ljcifs/netbios/Name;Ljava/net/InetAddress;)Ljcifs/netbios/NbtAddress;
    .locals 12
    .param p1    # Ljcifs/netbios/Name;
    .param p2    # Ljava/net/InetAddress;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/UnknownHostException;
        }
    .end annotation

    const/4 v11, 0x3

    const/4 v9, 0x1

    const/4 v8, 0x0

    new-instance v6, Ljcifs/netbios/NameQueryRequest;

    invoke-direct {v6, p1}, Ljcifs/netbios/NameQueryRequest;-><init>(Ljcifs/netbios/Name;)V

    new-instance v7, Ljcifs/netbios/NameQueryResponse;

    invoke-direct {v7}, Ljcifs/netbios/NameQueryResponse;-><init>()V

    if-eqz p2, :cond_5

    iput-object p2, v6, Ljcifs/netbios/NameQueryRequest;->addr:Ljava/net/InetAddress;

    invoke-virtual {p2}, Ljava/net/InetAddress;->getAddress()[B

    move-result-object v10

    aget-byte v10, v10, v11

    const/4 v11, -0x1

    if-ne v10, v11, :cond_0

    move v8, v9

    :cond_0
    iput-boolean v8, v6, Ljcifs/netbios/NameQueryRequest;->isBroadcast:Z

    sget v4, Ljcifs/netbios/NameServiceClient;->RETRY_COUNT:I

    :cond_1
    :try_start_0
    sget v8, Ljcifs/netbios/NameServiceClient;->RETRY_TIMEOUT:I

    invoke-virtual {p0, v6, v7, v8}, Ljcifs/netbios/NameServiceClient;->send(Ljcifs/netbios/NameServicePacket;Ljcifs/netbios/NameServicePacket;I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    iget-boolean v8, v7, Ljcifs/netbios/NameQueryResponse;->received:Z

    if-eqz v8, :cond_3

    iget v8, v7, Ljcifs/netbios/NameQueryResponse;->resultCode:I

    if-nez v8, :cond_3

    iget-object v8, v7, Ljcifs/netbios/NameQueryResponse;->addrEntry:[Ljcifs/netbios/NbtAddress;

    array-length v8, v8

    add-int/lit8 v3, v8, -0x1

    iget-object v8, v7, Ljcifs/netbios/NameQueryResponse;->addrEntry:[Ljcifs/netbios/NbtAddress;

    aget-object v8, v8, v3

    iget-object v8, v8, Ljcifs/netbios/NbtAddress;->hostName:Ljcifs/netbios/Name;

    invoke-virtual {p2}, Ljava/net/InetAddress;->hashCode()I

    move-result v9

    iput v9, v8, Ljcifs/netbios/Name;->srcHashCode:I

    iget-object v8, v7, Ljcifs/netbios/NameQueryResponse;->addrEntry:[Ljcifs/netbios/NbtAddress;

    aget-object v0, v8, v3

    :goto_0
    return-object v0

    :catch_0
    move-exception v2

    sget-object v8, Ljcifs/netbios/NameServiceClient;->log:Ljcifs/util/LogStream;

    sget v8, Ljcifs/util/LogStream;->level:I

    if-le v8, v9, :cond_2

    sget-object v8, Ljcifs/netbios/NameServiceClient;->log:Ljcifs/util/LogStream;

    invoke-virtual {v2, v8}, Ljava/io/IOException;->printStackTrace(Ljava/io/PrintStream;)V

    :cond_2
    new-instance v8, Ljava/net/UnknownHostException;

    iget-object v9, p1, Ljcifs/netbios/Name;->name:Ljava/lang/String;

    invoke-direct {v8, v9}, Ljava/net/UnknownHostException;-><init>(Ljava/lang/String;)V

    throw v8

    :cond_3
    add-int/lit8 v4, v4, -0x1

    if-lez v4, :cond_4

    iget-boolean v8, v6, Ljcifs/netbios/NameQueryRequest;->isBroadcast:Z

    if-nez v8, :cond_1

    :cond_4
    new-instance v8, Ljava/net/UnknownHostException;

    iget-object v9, p1, Ljcifs/netbios/Name;->name:Ljava/lang/String;

    invoke-direct {v8, v9}, Ljava/net/UnknownHostException;-><init>(Ljava/lang/String;)V

    throw v8

    :cond_5
    const/4 v1, 0x0

    :goto_1
    iget-object v8, p0, Ljcifs/netbios/NameServiceClient;->resolveOrder:[I

    array-length v8, v8

    if-ge v1, v8, :cond_a

    :try_start_1
    iget-object v8, p0, Ljcifs/netbios/NameServiceClient;->resolveOrder:[I

    aget v8, v8, v1

    packed-switch v8, :pswitch_data_0

    :cond_6
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :pswitch_0
    invoke-static {p1}, Ljcifs/netbios/Lmhosts;->getByName(Ljcifs/netbios/Name;)Ljcifs/netbios/NbtAddress;

    move-result-object v0

    if-eqz v0, :cond_6

    iget-object v8, v0, Ljcifs/netbios/NbtAddress;->hostName:Ljcifs/netbios/Name;

    const/4 v10, 0x0

    iput v10, v8, Ljcifs/netbios/Name;->srcHashCode:I

    goto :goto_0

    :catch_1
    move-exception v8

    goto :goto_2

    :pswitch_1
    iget-object v8, p0, Ljcifs/netbios/NameServiceClient;->resolveOrder:[I

    aget v8, v8, v1

    if-ne v8, v11, :cond_7

    iget-object v8, p1, Ljcifs/netbios/Name;->name:Ljava/lang/String;

    const-string v10, "\u0001\u0002__MSBROWSE__\u0002"

    if-eq v8, v10, :cond_7

    iget v8, p1, Ljcifs/netbios/Name;->hexCode:I

    const/16 v10, 0x1d

    if-eq v8, v10, :cond_7

    invoke-static {}, Ljcifs/netbios/NbtAddress;->getWINSAddress()Ljava/net/InetAddress;

    move-result-object v8

    iput-object v8, v6, Ljcifs/netbios/NameQueryRequest;->addr:Ljava/net/InetAddress;

    const/4 v8, 0x0

    iput-boolean v8, v6, Ljcifs/netbios/NameQueryRequest;->isBroadcast:Z

    :goto_3
    sget v4, Ljcifs/netbios/NameServiceClient;->RETRY_COUNT:I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move v5, v4

    :goto_4
    add-int/lit8 v4, v5, -0x1

    if-lez v5, :cond_6

    :try_start_2
    sget v8, Ljcifs/netbios/NameServiceClient;->RETRY_TIMEOUT:I

    invoke-virtual {p0, v6, v7, v8}, Ljcifs/netbios/NameServiceClient;->send(Ljcifs/netbios/NameServicePacket;Ljcifs/netbios/NameServicePacket;I)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    :try_start_3
    iget-boolean v8, v7, Ljcifs/netbios/NameQueryResponse;->received:Z

    if-eqz v8, :cond_9

    iget v8, v7, Ljcifs/netbios/NameQueryResponse;->resultCode:I

    if-nez v8, :cond_9

    iget-object v8, v7, Ljcifs/netbios/NameQueryResponse;->addrEntry:[Ljcifs/netbios/NbtAddress;

    const/4 v10, 0x0

    aget-object v8, v8, v10

    iget-object v8, v8, Ljcifs/netbios/NbtAddress;->hostName:Ljcifs/netbios/Name;

    iget-object v10, v6, Ljcifs/netbios/NameQueryRequest;->addr:Ljava/net/InetAddress;

    invoke-virtual {v10}, Ljava/net/InetAddress;->hashCode()I

    move-result v10

    iput v10, v8, Ljcifs/netbios/Name;->srcHashCode:I

    iget-object v8, v7, Ljcifs/netbios/NameQueryResponse;->addrEntry:[Ljcifs/netbios/NbtAddress;

    const/4 v10, 0x0

    aget-object v0, v8, v10

    goto/16 :goto_0

    :cond_7
    iget-object v8, p0, Ljcifs/netbios/NameServiceClient;->baddr:Ljava/net/InetAddress;

    iput-object v8, v6, Ljcifs/netbios/NameQueryRequest;->addr:Ljava/net/InetAddress;

    const/4 v8, 0x1

    iput-boolean v8, v6, Ljcifs/netbios/NameQueryRequest;->isBroadcast:Z

    goto :goto_3

    :catch_2
    move-exception v2

    sget-object v8, Ljcifs/netbios/NameServiceClient;->log:Ljcifs/util/LogStream;

    sget v8, Ljcifs/util/LogStream;->level:I

    if-le v8, v9, :cond_8

    sget-object v8, Ljcifs/netbios/NameServiceClient;->log:Ljcifs/util/LogStream;

    invoke-virtual {v2, v8}, Ljava/io/IOException;->printStackTrace(Ljava/io/PrintStream;)V

    :cond_8
    new-instance v8, Ljava/net/UnknownHostException;

    iget-object v10, p1, Ljcifs/netbios/Name;->name:Ljava/lang/String;

    invoke-direct {v8, v10}, Ljava/net/UnknownHostException;-><init>(Ljava/lang/String;)V

    throw v8

    :cond_9
    iget-object v8, p0, Ljcifs/netbios/NameServiceClient;->resolveOrder:[I

    aget v8, v8, v1
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    if-eq v8, v11, :cond_6

    move v5, v4

    goto :goto_4

    :cond_a
    new-instance v8, Ljava/net/UnknownHostException;

    iget-object v9, p1, Ljcifs/netbios/Name;->name:Ljava/lang/String;

    invoke-direct {v8, v9}, Ljava/net/UnknownHostException;-><init>(Ljava/lang/String;)V

    throw v8

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method getNextNameTrnId()I
    .locals 2

    iget v0, p0, Ljcifs/netbios/NameServiceClient;->nextNameTrnId:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Ljcifs/netbios/NameServiceClient;->nextNameTrnId:I

    const v1, 0xffff

    and-int/2addr v0, v1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput v0, p0, Ljcifs/netbios/NameServiceClient;->nextNameTrnId:I

    :cond_0
    iget v0, p0, Ljcifs/netbios/NameServiceClient;->nextNameTrnId:I

    return v0
.end method

.method getNodeStatus(Ljcifs/netbios/NbtAddress;)[Ljcifs/netbios/NbtAddress;
    .locals 11
    .param p1    # Ljcifs/netbios/NbtAddress;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/UnknownHostException;
        }
    .end annotation

    new-instance v5, Ljcifs/netbios/NodeStatusResponse;

    invoke-direct {v5, p1}, Ljcifs/netbios/NodeStatusResponse;-><init>(Ljcifs/netbios/NbtAddress;)V

    new-instance v4, Ljcifs/netbios/NodeStatusRequest;

    new-instance v7, Ljcifs/netbios/Name;

    const-string v8, "*\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000"

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-direct {v7, v8, v9, v10}, Ljcifs/netbios/Name;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    invoke-direct {v4, v7}, Ljcifs/netbios/NodeStatusRequest;-><init>(Ljcifs/netbios/Name;)V

    invoke-virtual {p1}, Ljcifs/netbios/NbtAddress;->getInetAddress()Ljava/net/InetAddress;

    move-result-object v7

    iput-object v7, v4, Ljcifs/netbios/NodeStatusRequest;->addr:Ljava/net/InetAddress;

    sget v2, Ljcifs/netbios/NameServiceClient;->RETRY_COUNT:I

    move v3, v2

    :goto_0
    add-int/lit8 v2, v3, -0x1

    if-lez v3, :cond_2

    :try_start_0
    sget v7, Ljcifs/netbios/NameServiceClient;->RETRY_TIMEOUT:I

    invoke-virtual {p0, v4, v5, v7}, Ljcifs/netbios/NameServiceClient;->send(Ljcifs/netbios/NameServicePacket;Ljcifs/netbios/NameServicePacket;I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    iget-boolean v7, v5, Ljcifs/netbios/NodeStatusResponse;->received:Z

    if-eqz v7, :cond_3

    iget v7, v5, Ljcifs/netbios/NodeStatusResponse;->resultCode:I

    if-nez v7, :cond_3

    iget-object v7, v4, Ljcifs/netbios/NodeStatusRequest;->addr:Ljava/net/InetAddress;

    invoke-virtual {v7}, Ljava/net/InetAddress;->hashCode()I

    move-result v6

    const/4 v0, 0x0

    :goto_1
    iget-object v7, v5, Ljcifs/netbios/NodeStatusResponse;->addressArray:[Ljcifs/netbios/NbtAddress;

    array-length v7, v7

    if-ge v0, v7, :cond_1

    iget-object v7, v5, Ljcifs/netbios/NodeStatusResponse;->addressArray:[Ljcifs/netbios/NbtAddress;

    aget-object v7, v7, v0

    iget-object v7, v7, Ljcifs/netbios/NbtAddress;->hostName:Ljcifs/netbios/Name;

    iput v6, v7, Ljcifs/netbios/Name;->srcHashCode:I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :catch_0
    move-exception v1

    sget-object v7, Ljcifs/netbios/NameServiceClient;->log:Ljcifs/util/LogStream;

    sget v7, Ljcifs/util/LogStream;->level:I

    const/4 v8, 0x1

    if-le v7, v8, :cond_0

    sget-object v7, Ljcifs/netbios/NameServiceClient;->log:Ljcifs/util/LogStream;

    invoke-virtual {v1, v7}, Ljava/io/IOException;->printStackTrace(Ljava/io/PrintStream;)V

    :cond_0
    new-instance v7, Ljava/net/UnknownHostException;

    invoke-virtual {p1}, Ljcifs/netbios/NbtAddress;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/net/UnknownHostException;-><init>(Ljava/lang/String;)V

    throw v7

    :cond_1
    iget-object v7, v5, Ljcifs/netbios/NodeStatusResponse;->addressArray:[Ljcifs/netbios/NbtAddress;

    return-object v7

    :cond_2
    new-instance v7, Ljava/net/UnknownHostException;

    iget-object v8, p1, Ljcifs/netbios/NbtAddress;->hostName:Ljcifs/netbios/Name;

    iget-object v8, v8, Ljcifs/netbios/Name;->name:Ljava/lang/String;

    invoke-direct {v7, v8}, Ljava/net/UnknownHostException;-><init>(Ljava/lang/String;)V

    throw v7

    :cond_3
    move v3, v2

    goto :goto_0
.end method

.method public run()V
    .locals 8

    const/4 v7, 0x3

    :cond_0
    :goto_0
    :try_start_0
    iget-object v3, p0, Ljcifs/netbios/NameServiceClient;->thread:Ljava/lang/Thread;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Ljcifs/netbios/NameServiceClient;->in:Ljava/net/DatagramPacket;

    sget v4, Ljcifs/netbios/NameServiceClient;->RCV_BUF_SIZE:I

    invoke-virtual {v3, v4}, Ljava/net/DatagramPacket;->setLength(I)V

    iget-object v3, p0, Ljcifs/netbios/NameServiceClient;->socket:Ljava/net/DatagramSocket;

    iget v4, p0, Ljcifs/netbios/NameServiceClient;->closeTimeout:I

    invoke-virtual {v3, v4}, Ljava/net/DatagramSocket;->setSoTimeout(I)V

    iget-object v3, p0, Ljcifs/netbios/NameServiceClient;->socket:Ljava/net/DatagramSocket;

    iget-object v4, p0, Ljcifs/netbios/NameServiceClient;->in:Ljava/net/DatagramPacket;

    invoke-virtual {v3, v4}, Ljava/net/DatagramSocket;->receive(Ljava/net/DatagramPacket;)V

    sget-object v3, Ljcifs/netbios/NameServiceClient;->log:Ljcifs/util/LogStream;

    sget v3, Ljcifs/util/LogStream;->level:I

    if-le v3, v7, :cond_1

    sget-object v3, Ljcifs/netbios/NameServiceClient;->log:Ljcifs/util/LogStream;

    const-string v4, "NetBIOS: new data read from socket"

    invoke-virtual {v3, v4}, Ljcifs/util/LogStream;->println(Ljava/lang/String;)V

    :cond_1
    iget-object v3, p0, Ljcifs/netbios/NameServiceClient;->rcv_buf:[B

    const/4 v4, 0x0

    invoke-static {v3, v4}, Ljcifs/netbios/NameServicePacket;->readNameTrnId([BI)I

    move-result v1

    iget-object v3, p0, Ljcifs/netbios/NameServiceClient;->responseTable:Ljava/util/HashMap;

    new-instance v4, Ljava/lang/Integer;

    invoke-direct {v4, v1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljcifs/netbios/NameServicePacket;

    if-eqz v2, :cond_0

    iget-boolean v3, v2, Ljcifs/netbios/NameServicePacket;->received:Z

    if-nez v3, :cond_0

    monitor-enter v2
    :try_end_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v3, p0, Ljcifs/netbios/NameServiceClient;->rcv_buf:[B

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Ljcifs/netbios/NameServicePacket;->readWireFormat([BI)I

    const/4 v3, 0x1

    iput-boolean v3, v2, Ljcifs/netbios/NameServicePacket;->received:Z

    sget-object v3, Ljcifs/netbios/NameServiceClient;->log:Ljcifs/util/LogStream;

    sget v3, Ljcifs/util/LogStream;->level:I

    if-le v3, v7, :cond_2

    sget-object v3, Ljcifs/netbios/NameServiceClient;->log:Ljcifs/util/LogStream;

    invoke-virtual {v3, v2}, Ljcifs/util/LogStream;->println(Ljava/lang/Object;)V

    sget-object v3, Ljcifs/netbios/NameServiceClient;->log:Ljcifs/util/LogStream;

    iget-object v4, p0, Ljcifs/netbios/NameServiceClient;->rcv_buf:[B

    const/4 v5, 0x0

    iget-object v6, p0, Ljcifs/netbios/NameServiceClient;->in:Ljava/net/DatagramPacket;

    invoke-virtual {v6}, Ljava/net/DatagramPacket;->getLength()I

    move-result v6

    invoke-static {v3, v4, v5, v6}, Ljcifs/util/Hexdump;->hexdump(Ljava/io/PrintStream;[BII)V

    :cond_2
    invoke-virtual {v2}, Ljava/lang/Object;->notify()V

    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v3

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v3
    :try_end_2
    .catch Ljava/net/SocketTimeoutException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :catch_0
    move-exception v3

    invoke-virtual {p0}, Ljcifs/netbios/NameServiceClient;->tryClose()V

    :goto_1
    return-void

    :cond_3
    invoke-virtual {p0}, Ljcifs/netbios/NameServiceClient;->tryClose()V

    goto :goto_1

    :catch_1
    move-exception v0

    :try_start_3
    sget-object v3, Ljcifs/netbios/NameServiceClient;->log:Ljcifs/util/LogStream;

    sget v3, Ljcifs/util/LogStream;->level:I

    const/4 v4, 0x2

    if-le v3, v4, :cond_4

    sget-object v3, Ljcifs/netbios/NameServiceClient;->log:Ljcifs/util/LogStream;

    invoke-virtual {v0, v3}, Ljava/lang/Exception;->printStackTrace(Ljava/io/PrintStream;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :cond_4
    invoke-virtual {p0}, Ljcifs/netbios/NameServiceClient;->tryClose()V

    goto :goto_1

    :catchall_1
    move-exception v3

    invoke-virtual {p0}, Ljcifs/netbios/NameServiceClient;->tryClose()V

    throw v3
.end method

.method send(Ljcifs/netbios/NameServicePacket;Ljcifs/netbios/NameServicePacket;I)V
    .locals 12
    .param p1    # Ljcifs/netbios/NameServicePacket;
    .param p2    # Ljcifs/netbios/NameServicePacket;
    .param p3    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v3, 0x0

    sget-object v7, Ljcifs/netbios/NbtAddress;->NBNS:[Ljava/net/InetAddress;

    array-length v1, v7

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :cond_0
    monitor-enter p2

    move v2, v1

    move-object v4, v3

    :goto_0
    add-int/lit8 v1, v2, -0x1

    if-lez v2, :cond_6

    :try_start_0
    iget-object v8, p0, Ljcifs/netbios/NameServiceClient;->LOCK:Ljava/lang/Object;

    monitor-enter v8
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_4

    :try_start_1
    invoke-virtual {p0}, Ljcifs/netbios/NameServiceClient;->getNextNameTrnId()I

    move-result v7

    iput v7, p1, Ljcifs/netbios/NameServicePacket;->nameTrnId:I

    new-instance v3, Ljava/lang/Integer;

    iget v7, p1, Ljcifs/netbios/NameServicePacket;->nameTrnId:I

    invoke-direct {v3, v7}, Ljava/lang/Integer;-><init>(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    iget-object v7, p0, Ljcifs/netbios/NameServiceClient;->out:Ljava/net/DatagramPacket;

    iget-object v9, p1, Ljcifs/netbios/NameServicePacket;->addr:Ljava/net/InetAddress;

    invoke-virtual {v7, v9}, Ljava/net/DatagramPacket;->setAddress(Ljava/net/InetAddress;)V

    iget-object v7, p0, Ljcifs/netbios/NameServiceClient;->out:Ljava/net/DatagramPacket;

    iget-object v9, p0, Ljcifs/netbios/NameServiceClient;->snd_buf:[B

    const/4 v10, 0x0

    invoke-virtual {p1, v9, v10}, Ljcifs/netbios/NameServicePacket;->writeWireFormat([BI)I

    move-result v9

    invoke-virtual {v7, v9}, Ljava/net/DatagramPacket;->setLength(I)V

    const/4 v7, 0x0

    iput-boolean v7, p2, Ljcifs/netbios/NameServicePacket;->received:Z

    iget-object v7, p0, Ljcifs/netbios/NameServiceClient;->responseTable:Ljava/util/HashMap;

    invoke-virtual {v7, v3, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit16 v7, p3, 0x3e8

    invoke-virtual {p0, v7}, Ljcifs/netbios/NameServiceClient;->ensureOpen(I)V

    iget-object v7, p0, Ljcifs/netbios/NameServiceClient;->socket:Ljava/net/DatagramSocket;

    iget-object v9, p0, Ljcifs/netbios/NameServiceClient;->out:Ljava/net/DatagramPacket;

    invoke-virtual {v7, v9}, Ljava/net/DatagramSocket;->send(Ljava/net/DatagramPacket;)V

    sget-object v7, Ljcifs/netbios/NameServiceClient;->log:Ljcifs/util/LogStream;

    sget v7, Ljcifs/util/LogStream;->level:I

    const/4 v9, 0x3

    if-le v7, v9, :cond_1

    sget-object v7, Ljcifs/netbios/NameServiceClient;->log:Ljcifs/util/LogStream;

    invoke-virtual {v7, p1}, Ljcifs/util/LogStream;->println(Ljava/lang/Object;)V

    sget-object v7, Ljcifs/netbios/NameServiceClient;->log:Ljcifs/util/LogStream;

    iget-object v9, p0, Ljcifs/netbios/NameServiceClient;->snd_buf:[B

    const/4 v10, 0x0

    iget-object v11, p0, Ljcifs/netbios/NameServiceClient;->out:Ljava/net/DatagramPacket;

    invoke-virtual {v11}, Ljava/net/DatagramPacket;->getLength()I

    move-result v11

    invoke-static {v7, v9, v10, v11}, Ljcifs/util/Hexdump;->hexdump(Ljava/io/PrintStream;[BII)V

    :cond_1
    monitor-exit v8
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_5

    :try_start_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    :goto_1
    if-lez p3, :cond_3

    int-to-long v7, p3

    invoke-virtual {p2, v7, v8}, Ljava/lang/Object;->wait(J)V

    iget-boolean v7, p2, Ljcifs/netbios/NameServicePacket;->received:Z

    if-eqz v7, :cond_2

    iget v7, p1, Ljcifs/netbios/NameServicePacket;->questionType:I

    iget v8, p2, Ljcifs/netbios/NameServicePacket;->recordType:I
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    if-ne v7, v8, :cond_2

    :try_start_4
    iget-object v7, p0, Ljcifs/netbios/NameServiceClient;->responseTable:Ljava/util/HashMap;

    invoke-virtual {v7, v3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit p2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    :goto_2
    return-void

    :catchall_0
    move-exception v7

    move-object v3, v4

    :goto_3
    :try_start_5
    monitor-exit v8
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_5

    :try_start_6
    throw v7
    :try_end_6
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :catch_0
    move-exception v0

    :goto_4
    :try_start_7
    new-instance v7, Ljava/io/IOException;

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v7
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :catchall_1
    move-exception v7

    :goto_5
    :try_start_8
    iget-object v8, p0, Ljcifs/netbios/NameServiceClient;->responseTable:Ljava/util/HashMap;

    invoke-virtual {v8, v3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    throw v7

    :catchall_2
    move-exception v7

    monitor-exit p2
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    throw v7

    :cond_2
    const/4 v7, 0x0

    :try_start_9
    iput-boolean v7, p2, Ljcifs/netbios/NameServicePacket;->received:Z

    int-to-long v7, p3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J
    :try_end_9
    .catch Ljava/lang/InterruptedException; {:try_start_9 .. :try_end_9} :catch_0
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    move-result-wide v9

    sub-long/2addr v9, v5

    sub-long/2addr v7, v9

    long-to-int p3, v7

    goto :goto_1

    :cond_3
    :try_start_a
    iget-object v7, p0, Ljcifs/netbios/NameServiceClient;->responseTable:Ljava/util/HashMap;

    invoke-virtual {v7, v3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v8, p0, Ljcifs/netbios/NameServiceClient;->LOCK:Ljava/lang/Object;

    monitor-enter v8
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    :try_start_b
    iget-object v7, p1, Ljcifs/netbios/NameServicePacket;->addr:Ljava/net/InetAddress;

    invoke-static {v7}, Ljcifs/netbios/NbtAddress;->isWINS(Ljava/net/InetAddress;)Z

    move-result v7

    if-nez v7, :cond_4

    monitor-exit v8
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_3

    :goto_6
    :try_start_c
    monitor-exit p2
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    goto :goto_2

    :cond_4
    :try_start_d
    iget-object v7, p1, Ljcifs/netbios/NameServicePacket;->addr:Ljava/net/InetAddress;

    invoke-static {}, Ljcifs/netbios/NbtAddress;->getWINSAddress()Ljava/net/InetAddress;

    move-result-object v9

    if-ne v7, v9, :cond_5

    invoke-static {}, Ljcifs/netbios/NbtAddress;->switchWINS()Ljava/net/InetAddress;

    :cond_5
    invoke-static {}, Ljcifs/netbios/NbtAddress;->getWINSAddress()Ljava/net/InetAddress;

    move-result-object v7

    iput-object v7, p1, Ljcifs/netbios/NameServicePacket;->addr:Ljava/net/InetAddress;

    monitor-exit v8

    move v2, v1

    move-object v4, v3

    goto/16 :goto_0

    :catchall_3
    move-exception v7

    monitor-exit v8
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_3

    :try_start_e
    throw v7
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_2

    :catchall_4
    move-exception v7

    move-object v3, v4

    goto :goto_5

    :catch_1
    move-exception v0

    move-object v3, v4

    goto :goto_4

    :catchall_5
    move-exception v7

    goto :goto_3

    :cond_6
    move-object v3, v4

    goto :goto_6
.end method

.method tryClose()V
    .locals 2

    iget-object v1, p0, Ljcifs/netbios/NameServiceClient;->LOCK:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Ljcifs/netbios/NameServiceClient;->socket:Ljava/net/DatagramSocket;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ljcifs/netbios/NameServiceClient;->socket:Ljava/net/DatagramSocket;

    invoke-virtual {v0}, Ljava/net/DatagramSocket;->close()V

    const/4 v0, 0x0

    iput-object v0, p0, Ljcifs/netbios/NameServiceClient;->socket:Ljava/net/DatagramSocket;

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Ljcifs/netbios/NameServiceClient;->thread:Ljava/lang/Thread;

    iget-object v0, p0, Ljcifs/netbios/NameServiceClient;->responseTable:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
