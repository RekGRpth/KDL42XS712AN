.class public Ljcifs/ntlmssp/Type3Message;
.super Ljcifs/ntlmssp/NtlmMessage;
.source "Type3Message.java"


# static fields
.field private static final DEFAULT_DOMAIN:Ljava/lang/String;

.field private static final DEFAULT_FLAGS:I

.field private static final DEFAULT_PASSWORD:Ljava/lang/String;

.field private static final DEFAULT_USER:Ljava/lang/String;

.field private static final DEFAULT_WORKSTATION:Ljava/lang/String;

.field private static final LM_COMPATIBILITY:I

.field static final MILLISECONDS_BETWEEN_1970_AND_1601:J = 0xa9730b66800L

.field private static final RANDOM:Ljava/security/SecureRandom;


# instance fields
.field private domain:Ljava/lang/String;

.field private lmResponse:[B

.field private masterKey:[B

.field private ntResponse:[B

.field private sessionKey:[B

.field private user:Ljava/lang/String;

.field private workstation:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v1, 0x1

    const/4 v3, 0x0

    new-instance v2, Ljava/security/SecureRandom;

    invoke-direct {v2}, Ljava/security/SecureRandom;-><init>()V

    sput-object v2, Ljcifs/ntlmssp/Type3Message;->RANDOM:Ljava/security/SecureRandom;

    const-string v2, "jcifs.smb.client.useUnicode"

    invoke-static {v2, v1}, Ljcifs/Config;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    or-int/lit16 v1, v1, 0x200

    sput v1, Ljcifs/ntlmssp/Type3Message;->DEFAULT_FLAGS:I

    const-string v1, "jcifs.smb.client.domain"

    invoke-static {v1, v3}, Ljcifs/Config;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Ljcifs/ntlmssp/Type3Message;->DEFAULT_DOMAIN:Ljava/lang/String;

    const-string v1, "jcifs.smb.client.username"

    invoke-static {v1, v3}, Ljcifs/Config;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Ljcifs/ntlmssp/Type3Message;->DEFAULT_USER:Ljava/lang/String;

    const-string v1, "jcifs.smb.client.password"

    invoke-static {v1, v3}, Ljcifs/Config;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Ljcifs/ntlmssp/Type3Message;->DEFAULT_PASSWORD:Ljava/lang/String;

    const/4 v0, 0x0

    :try_start_0
    invoke-static {}, Ljcifs/netbios/NbtAddress;->getLocalHost()Ljcifs/netbios/NbtAddress;

    move-result-object v1

    invoke-virtual {v1}, Ljcifs/netbios/NbtAddress;->getHostName()Ljava/lang/String;
    :try_end_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_1
    sput-object v0, Ljcifs/ntlmssp/Type3Message;->DEFAULT_WORKSTATION:Ljava/lang/String;

    const-string v1, "jcifs.smb.lmCompatibility"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Ljcifs/Config;->getInt(Ljava/lang/String;I)I

    move-result v1

    sput v1, Ljcifs/ntlmssp/Type3Message;->LM_COMPATIBILITY:I

    return-void

    :cond_0
    const/4 v1, 0x2

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_1
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljcifs/ntlmssp/NtlmMessage;-><init>()V

    iput-object v0, p0, Ljcifs/ntlmssp/Type3Message;->masterKey:[B

    iput-object v0, p0, Ljcifs/ntlmssp/Type3Message;->sessionKey:[B

    invoke-static {}, Ljcifs/ntlmssp/Type3Message;->getDefaultFlags()I

    move-result v0

    invoke-virtual {p0, v0}, Ljcifs/ntlmssp/Type3Message;->setFlags(I)V

    invoke-static {}, Ljcifs/ntlmssp/Type3Message;->getDefaultDomain()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljcifs/ntlmssp/Type3Message;->setDomain(Ljava/lang/String;)V

    invoke-static {}, Ljcifs/ntlmssp/Type3Message;->getDefaultUser()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljcifs/ntlmssp/Type3Message;->setUser(Ljava/lang/String;)V

    invoke-static {}, Ljcifs/ntlmssp/Type3Message;->getDefaultWorkstation()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljcifs/ntlmssp/Type3Message;->setWorkstation(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(I[B[BLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # I
    .param p2    # [B
    .param p3    # [B
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;

    const/4 v0, 0x0

    invoke-direct {p0}, Ljcifs/ntlmssp/NtlmMessage;-><init>()V

    iput-object v0, p0, Ljcifs/ntlmssp/Type3Message;->masterKey:[B

    iput-object v0, p0, Ljcifs/ntlmssp/Type3Message;->sessionKey:[B

    invoke-virtual {p0, p1}, Ljcifs/ntlmssp/Type3Message;->setFlags(I)V

    invoke-virtual {p0, p2}, Ljcifs/ntlmssp/Type3Message;->setLMResponse([B)V

    invoke-virtual {p0, p3}, Ljcifs/ntlmssp/Type3Message;->setNTResponse([B)V

    invoke-virtual {p0, p4}, Ljcifs/ntlmssp/Type3Message;->setDomain(Ljava/lang/String;)V

    invoke-virtual {p0, p5}, Ljcifs/ntlmssp/Type3Message;->setUser(Ljava/lang/String;)V

    invoke-virtual {p0, p6}, Ljcifs/ntlmssp/Type3Message;->setWorkstation(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljcifs/ntlmssp/Type2Message;)V
    .locals 6
    .param p1    # Ljcifs/ntlmssp/Type2Message;

    const/4 v5, 0x0

    invoke-direct {p0}, Ljcifs/ntlmssp/NtlmMessage;-><init>()V

    iput-object v5, p0, Ljcifs/ntlmssp/Type3Message;->masterKey:[B

    iput-object v5, p0, Ljcifs/ntlmssp/Type3Message;->sessionKey:[B

    invoke-static {p1}, Ljcifs/ntlmssp/Type3Message;->getDefaultFlags(Ljcifs/ntlmssp/Type2Message;)I

    move-result v5

    invoke-virtual {p0, v5}, Ljcifs/ntlmssp/Type3Message;->setFlags(I)V

    invoke-static {}, Ljcifs/ntlmssp/Type3Message;->getDefaultWorkstation()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Ljcifs/ntlmssp/Type3Message;->setWorkstation(Ljava/lang/String;)V

    invoke-static {}, Ljcifs/ntlmssp/Type3Message;->getDefaultDomain()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Ljcifs/ntlmssp/Type3Message;->setDomain(Ljava/lang/String;)V

    invoke-static {}, Ljcifs/ntlmssp/Type3Message;->getDefaultUser()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Ljcifs/ntlmssp/Type3Message;->setUser(Ljava/lang/String;)V

    invoke-static {}, Ljcifs/ntlmssp/Type3Message;->getDefaultPassword()Ljava/lang/String;

    move-result-object v3

    sget v5, Ljcifs/ntlmssp/Type3Message;->LM_COMPATIBILITY:I

    packed-switch v5, :pswitch_data_0

    invoke-static {p1, v3}, Ljcifs/ntlmssp/Type3Message;->getLMResponse(Ljcifs/ntlmssp/Type2Message;Ljava/lang/String;)[B

    move-result-object v5

    invoke-virtual {p0, v5}, Ljcifs/ntlmssp/Type3Message;->setLMResponse([B)V

    invoke-static {p1, v3}, Ljcifs/ntlmssp/Type3Message;->getNTResponse(Ljcifs/ntlmssp/Type2Message;Ljava/lang/String;)[B

    move-result-object v5

    invoke-virtual {p0, v5}, Ljcifs/ntlmssp/Type3Message;->setNTResponse([B)V

    :goto_0
    return-void

    :pswitch_0
    invoke-static {p1, v3}, Ljcifs/ntlmssp/Type3Message;->getLMResponse(Ljcifs/ntlmssp/Type2Message;Ljava/lang/String;)[B

    move-result-object v5

    invoke-virtual {p0, v5}, Ljcifs/ntlmssp/Type3Message;->setLMResponse([B)V

    invoke-static {p1, v3}, Ljcifs/ntlmssp/Type3Message;->getNTResponse(Ljcifs/ntlmssp/Type2Message;Ljava/lang/String;)[B

    move-result-object v5

    invoke-virtual {p0, v5}, Ljcifs/ntlmssp/Type3Message;->setNTResponse([B)V

    goto :goto_0

    :pswitch_1
    invoke-static {p1, v3}, Ljcifs/ntlmssp/Type3Message;->getNTResponse(Ljcifs/ntlmssp/Type2Message;Ljava/lang/String;)[B

    move-result-object v2

    invoke-virtual {p0, v2}, Ljcifs/ntlmssp/Type3Message;->setLMResponse([B)V

    invoke-virtual {p0, v2}, Ljcifs/ntlmssp/Type3Message;->setNTResponse([B)V

    goto :goto_0

    :pswitch_2
    const/16 v5, 0x8

    new-array v0, v5, [B

    sget-object v5, Ljcifs/ntlmssp/Type3Message;->RANDOM:Ljava/security/SecureRandom;

    invoke-virtual {v5, v0}, Ljava/security/SecureRandom;->nextBytes([B)V

    invoke-static {p1, v1, v4, v3, v0}, Ljcifs/ntlmssp/Type3Message;->getLMv2Response(Ljcifs/ntlmssp/Type2Message;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[B)[B

    move-result-object v5

    invoke-virtual {p0, v5}, Ljcifs/ntlmssp/Type3Message;->setLMResponse([B)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public constructor <init>(Ljcifs/ntlmssp/Type2Message;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 20
    .param p1    # Ljcifs/ntlmssp/Type2Message;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # I

    invoke-direct/range {p0 .. p0}, Ljcifs/ntlmssp/NtlmMessage;-><init>()V

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput-object v5, v0, Ljcifs/ntlmssp/Type3Message;->masterKey:[B

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput-object v5, v0, Ljcifs/ntlmssp/Type3Message;->sessionKey:[B

    invoke-static/range {p1 .. p1}, Ljcifs/ntlmssp/Type3Message;->getDefaultFlags(Ljcifs/ntlmssp/Type2Message;)I

    move-result v5

    or-int v5, v5, p6

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Ljcifs/ntlmssp/Type3Message;->setFlags(I)V

    if-nez p5, :cond_0

    invoke-static {}, Ljcifs/ntlmssp/Type3Message;->getDefaultWorkstation()Ljava/lang/String;

    move-result-object p5

    :cond_0
    move-object/from16 v0, p0

    move-object/from16 v1, p5

    invoke-virtual {v0, v1}, Ljcifs/ntlmssp/Type3Message;->setWorkstation(Ljava/lang/String;)V

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Ljcifs/ntlmssp/Type3Message;->setDomain(Ljava/lang/String;)V

    move-object/from16 v0, p0

    move-object/from16 v1, p4

    invoke-virtual {v0, v1}, Ljcifs/ntlmssp/Type3Message;->setUser(Ljava/lang/String;)V

    sget v5, Ljcifs/ntlmssp/Type3Message;->LM_COMPATIBILITY:I

    packed-switch v5, :pswitch_data_0

    invoke-static/range {p1 .. p2}, Ljcifs/ntlmssp/Type3Message;->getLMResponse(Ljcifs/ntlmssp/Type2Message;Ljava/lang/String;)[B

    move-result-object v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Ljcifs/ntlmssp/Type3Message;->setLMResponse([B)V

    invoke-static/range {p1 .. p2}, Ljcifs/ntlmssp/Type3Message;->getNTResponse(Ljcifs/ntlmssp/Type2Message;Ljava/lang/String;)[B

    move-result-object v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Ljcifs/ntlmssp/Type3Message;->setNTResponse([B)V

    :cond_1
    :goto_0
    return-void

    :pswitch_0
    invoke-virtual/range {p0 .. p0}, Ljcifs/ntlmssp/Type3Message;->getFlags()I

    move-result v5

    const/high16 v6, 0x80000

    and-int/2addr v5, v6

    if-nez v5, :cond_2

    invoke-static/range {p1 .. p2}, Ljcifs/ntlmssp/Type3Message;->getLMResponse(Ljcifs/ntlmssp/Type2Message;Ljava/lang/String;)[B

    move-result-object v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Ljcifs/ntlmssp/Type3Message;->setLMResponse([B)V

    invoke-static/range {p1 .. p2}, Ljcifs/ntlmssp/Type3Message;->getNTResponse(Ljcifs/ntlmssp/Type2Message;Ljava/lang/String;)[B

    move-result-object v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Ljcifs/ntlmssp/Type3Message;->setNTResponse([B)V

    goto :goto_0

    :cond_2
    const/16 v5, 0x18

    new-array v10, v5, [B

    sget-object v5, Ljcifs/ntlmssp/Type3Message;->RANDOM:Ljava/security/SecureRandom;

    invoke-virtual {v5, v10}, Ljava/security/SecureRandom;->nextBytes([B)V

    const/16 v5, 0x8

    const/16 v6, 0x18

    const/4 v7, 0x0

    invoke-static {v10, v5, v6, v7}, Ljava/util/Arrays;->fill([BIIB)V

    invoke-static/range {p2 .. p2}, Ljcifs/smb/NtlmPasswordAuthentication;->nTOWFv1(Ljava/lang/String;)[B

    move-result-object v17

    invoke-virtual/range {p1 .. p1}, Ljcifs/ntlmssp/Type2Message;->getChallenge()[B

    move-result-object v5

    move-object/from16 v0, v17

    invoke-static {v0, v5, v10}, Ljcifs/smb/NtlmPasswordAuthentication;->getNTLM2Response([B[B[B)[B

    move-result-object v15

    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Ljcifs/ntlmssp/Type3Message;->setLMResponse([B)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Ljcifs/ntlmssp/Type3Message;->setNTResponse([B)V

    invoke-virtual/range {p0 .. p0}, Ljcifs/ntlmssp/Type3Message;->getFlags()I

    move-result v5

    and-int/lit8 v5, v5, 0x10

    const/16 v6, 0x10

    if-ne v5, v6, :cond_1

    const/16 v5, 0x10

    new-array v0, v5, [B

    move-object/from16 v18, v0

    invoke-virtual/range {p1 .. p1}, Ljcifs/ntlmssp/Type2Message;->getChallenge()[B

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v9, 0x8

    move-object/from16 v0, v18

    invoke-static {v5, v6, v0, v7, v9}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    const/4 v5, 0x0

    const/16 v6, 0x8

    const/16 v7, 0x8

    move-object/from16 v0, v18

    invoke-static {v10, v5, v0, v6, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    new-instance v13, Ljcifs/util/MD4;

    invoke-direct {v13}, Ljcifs/util/MD4;-><init>()V

    move-object/from16 v0, v17

    invoke-virtual {v13, v0}, Ljcifs/util/MD4;->update([B)V

    invoke-virtual {v13}, Ljcifs/util/MD4;->digest()[B

    move-result-object v19

    new-instance v12, Ljcifs/util/HMACT64;

    move-object/from16 v0, v19

    invoke-direct {v12, v0}, Ljcifs/util/HMACT64;-><init>([B)V

    move-object/from16 v0, v18

    invoke-virtual {v12, v0}, Ljcifs/util/HMACT64;->update([B)V

    invoke-virtual {v12}, Ljcifs/util/HMACT64;->digest()[B

    move-result-object v16

    invoke-virtual/range {p0 .. p0}, Ljcifs/ntlmssp/Type3Message;->getFlags()I

    move-result v5

    const/high16 v6, 0x40000000    # 2.0f

    and-int/2addr v5, v6

    if-eqz v5, :cond_3

    const/16 v5, 0x10

    new-array v5, v5, [B

    move-object/from16 v0, p0

    iput-object v5, v0, Ljcifs/ntlmssp/Type3Message;->masterKey:[B

    sget-object v5, Ljcifs/ntlmssp/Type3Message;->RANDOM:Ljava/security/SecureRandom;

    move-object/from16 v0, p0

    iget-object v6, v0, Ljcifs/ntlmssp/Type3Message;->masterKey:[B

    invoke-virtual {v5, v6}, Ljava/security/SecureRandom;->nextBytes([B)V

    const/16 v5, 0x10

    new-array v8, v5, [B

    new-instance v4, Ljcifs/util/RC4;

    move-object/from16 v0, v16

    invoke-direct {v4, v0}, Ljcifs/util/RC4;-><init>([B)V

    move-object/from16 v0, p0

    iget-object v5, v0, Ljcifs/ntlmssp/Type3Message;->masterKey:[B

    const/4 v6, 0x0

    const/16 v7, 0x10

    const/4 v9, 0x0

    invoke-virtual/range {v4 .. v9}, Ljcifs/util/RC4;->update([BII[BI)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Ljcifs/ntlmssp/Type3Message;->setSessionKey([B)V

    goto/16 :goto_0

    :cond_3
    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Ljcifs/ntlmssp/Type3Message;->masterKey:[B

    move-object/from16 v0, p0

    iget-object v5, v0, Ljcifs/ntlmssp/Type3Message;->masterKey:[B

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Ljcifs/ntlmssp/Type3Message;->setSessionKey([B)V

    goto/16 :goto_0

    :pswitch_1
    invoke-static/range {p1 .. p2}, Ljcifs/ntlmssp/Type3Message;->getNTResponse(Ljcifs/ntlmssp/Type2Message;Ljava/lang/String;)[B

    move-result-object v14

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Ljcifs/ntlmssp/Type3Message;->setLMResponse([B)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Ljcifs/ntlmssp/Type3Message;->setNTResponse([B)V

    goto/16 :goto_0

    :pswitch_2
    move-object/from16 v0, p3

    move-object/from16 v1, p4

    move-object/from16 v2, p2

    invoke-static {v0, v1, v2}, Ljcifs/smb/NtlmPasswordAuthentication;->nTOWFv2(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v17

    const/16 v5, 0x8

    new-array v10, v5, [B

    sget-object v5, Ljcifs/ntlmssp/Type3Message;->RANDOM:Ljava/security/SecureRandom;

    invoke-virtual {v5, v10}, Ljava/security/SecureRandom;->nextBytes([B)V

    move-object/from16 v0, p1

    move-object/from16 v1, p3

    move-object/from16 v2, p4

    move-object/from16 v3, p2

    invoke-static {v0, v1, v2, v3, v10}, Ljcifs/ntlmssp/Type3Message;->getLMv2Response(Ljcifs/ntlmssp/Type2Message;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[B)[B

    move-result-object v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Ljcifs/ntlmssp/Type3Message;->setLMResponse([B)V

    const/16 v5, 0x8

    new-array v11, v5, [B

    sget-object v5, Ljcifs/ntlmssp/Type3Message;->RANDOM:Ljava/security/SecureRandom;

    invoke-virtual {v5, v11}, Ljava/security/SecureRandom;->nextBytes([B)V

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-static {v0, v1, v11}, Ljcifs/ntlmssp/Type3Message;->getNTLMv2Response(Ljcifs/ntlmssp/Type2Message;[B[B)[B

    move-result-object v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Ljcifs/ntlmssp/Type3Message;->setNTResponse([B)V

    invoke-virtual/range {p0 .. p0}, Ljcifs/ntlmssp/Type3Message;->getFlags()I

    move-result v5

    and-int/lit8 v5, v5, 0x10

    const/16 v6, 0x10

    if-ne v5, v6, :cond_1

    new-instance v12, Ljcifs/util/HMACT64;

    move-object/from16 v0, v17

    invoke-direct {v12, v0}, Ljcifs/util/HMACT64;-><init>([B)V

    move-object/from16 v0, p0

    iget-object v5, v0, Ljcifs/ntlmssp/Type3Message;->ntResponse:[B

    const/4 v6, 0x0

    const/16 v7, 0x10

    invoke-virtual {v12, v5, v6, v7}, Ljcifs/util/HMACT64;->update([BII)V

    invoke-virtual {v12}, Ljcifs/util/HMACT64;->digest()[B

    move-result-object v19

    invoke-virtual/range {p0 .. p0}, Ljcifs/ntlmssp/Type3Message;->getFlags()I

    move-result v5

    const/high16 v6, 0x40000000    # 2.0f

    and-int/2addr v5, v6

    if-eqz v5, :cond_4

    const/16 v5, 0x10

    new-array v5, v5, [B

    move-object/from16 v0, p0

    iput-object v5, v0, Ljcifs/ntlmssp/Type3Message;->masterKey:[B

    sget-object v5, Ljcifs/ntlmssp/Type3Message;->RANDOM:Ljava/security/SecureRandom;

    move-object/from16 v0, p0

    iget-object v6, v0, Ljcifs/ntlmssp/Type3Message;->masterKey:[B

    invoke-virtual {v5, v6}, Ljava/security/SecureRandom;->nextBytes([B)V

    const/16 v5, 0x10

    new-array v8, v5, [B

    new-instance v4, Ljcifs/util/RC4;

    move-object/from16 v0, v19

    invoke-direct {v4, v0}, Ljcifs/util/RC4;-><init>([B)V

    move-object/from16 v0, p0

    iget-object v5, v0, Ljcifs/ntlmssp/Type3Message;->masterKey:[B

    const/4 v6, 0x0

    const/16 v7, 0x10

    const/4 v9, 0x0

    invoke-virtual/range {v4 .. v9}, Ljcifs/util/RC4;->update([BII[BI)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Ljcifs/ntlmssp/Type3Message;->setSessionKey([B)V

    goto/16 :goto_0

    :cond_4
    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Ljcifs/ntlmssp/Type3Message;->masterKey:[B

    move-object/from16 v0, p0

    iget-object v5, v0, Ljcifs/ntlmssp/Type3Message;->masterKey:[B

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Ljcifs/ntlmssp/Type3Message;->setSessionKey([B)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public constructor <init>([B)V
    .locals 1
    .param p1    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x0

    invoke-direct {p0}, Ljcifs/ntlmssp/NtlmMessage;-><init>()V

    iput-object v0, p0, Ljcifs/ntlmssp/Type3Message;->masterKey:[B

    iput-object v0, p0, Ljcifs/ntlmssp/Type3Message;->sessionKey:[B

    invoke-direct {p0, p1}, Ljcifs/ntlmssp/Type3Message;->parse([B)V

    return-void
.end method

.method public static getDefaultDomain()Ljava/lang/String;
    .locals 1

    sget-object v0, Ljcifs/ntlmssp/Type3Message;->DEFAULT_DOMAIN:Ljava/lang/String;

    return-object v0
.end method

.method public static getDefaultFlags()I
    .locals 1

    sget v0, Ljcifs/ntlmssp/Type3Message;->DEFAULT_FLAGS:I

    return v0
.end method

.method public static getDefaultFlags(Ljcifs/ntlmssp/Type2Message;)I
    .locals 2
    .param p0    # Ljcifs/ntlmssp/Type2Message;

    if-nez p0, :cond_0

    sget v0, Ljcifs/ntlmssp/Type3Message;->DEFAULT_FLAGS:I

    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x200

    invoke-virtual {p0}, Ljcifs/ntlmssp/Type2Message;->getFlags()I

    move-result v1

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :goto_1
    or-int/2addr v0, v1

    goto :goto_0

    :cond_1
    const/4 v1, 0x2

    goto :goto_1
.end method

.method public static getDefaultPassword()Ljava/lang/String;
    .locals 1

    sget-object v0, Ljcifs/ntlmssp/Type3Message;->DEFAULT_PASSWORD:Ljava/lang/String;

    return-object v0
.end method

.method public static getDefaultUser()Ljava/lang/String;
    .locals 1

    sget-object v0, Ljcifs/ntlmssp/Type3Message;->DEFAULT_USER:Ljava/lang/String;

    return-object v0
.end method

.method public static getDefaultWorkstation()Ljava/lang/String;
    .locals 1

    sget-object v0, Ljcifs/ntlmssp/Type3Message;->DEFAULT_WORKSTATION:Ljava/lang/String;

    return-object v0
.end method

.method public static getLMResponse(Ljcifs/ntlmssp/Type2Message;Ljava/lang/String;)[B
    .locals 1
    .param p0    # Ljcifs/ntlmssp/Type2Message;
    .param p1    # Ljava/lang/String;

    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0}, Ljcifs/ntlmssp/Type2Message;->getChallenge()[B

    move-result-object v0

    invoke-static {p1, v0}, Ljcifs/smb/NtlmPasswordAuthentication;->getPreNTLMResponse(Ljava/lang/String;[B)[B

    move-result-object v0

    goto :goto_0
.end method

.method public static getLMv2Response(Ljcifs/ntlmssp/Type2Message;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[B)[B
    .locals 1
    .param p0    # Ljcifs/ntlmssp/Type2Message;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # [B

    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    if-nez p4, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0}, Ljcifs/ntlmssp/Type2Message;->getChallenge()[B

    move-result-object v0

    invoke-static {p1, p2, p3, v0, p4}, Ljcifs/smb/NtlmPasswordAuthentication;->getLMv2Response(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[B[B)[B

    move-result-object v0

    goto :goto_0
.end method

.method public static getNTLMv2Response(Ljcifs/ntlmssp/Type2Message;[B[B)[B
    .locals 7
    .param p0    # Ljcifs/ntlmssp/Type2Message;
    .param p1    # [B
    .param p2    # [B

    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide v5, 0xa9730b66800L

    add-long/2addr v0, v5

    const-wide/16 v5, 0x2710

    mul-long v3, v0, v5

    invoke-virtual {p0}, Ljcifs/ntlmssp/Type2Message;->getChallenge()[B

    move-result-object v1

    invoke-virtual {p0}, Ljcifs/ntlmssp/Type2Message;->getTargetInformation()[B

    move-result-object v5

    move-object v0, p1

    move-object v2, p2

    invoke-static/range {v0 .. v5}, Ljcifs/smb/NtlmPasswordAuthentication;->getNTLMv2Response([B[B[BJ[B)[B

    move-result-object v0

    goto :goto_0
.end method

.method public static getNTResponse(Ljcifs/ntlmssp/Type2Message;Ljava/lang/String;)[B
    .locals 1
    .param p0    # Ljcifs/ntlmssp/Type2Message;
    .param p1    # Ljava/lang/String;

    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0}, Ljcifs/ntlmssp/Type2Message;->getChallenge()[B

    move-result-object v0

    invoke-static {p1, v0}, Ljcifs/smb/NtlmPasswordAuthentication;->getNTLMResponse(Ljava/lang/String;[B)[B

    move-result-object v0

    goto :goto_0
.end method

.method private parse([B)V
    .locals 17
    .param p1    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v6, 0x0

    :goto_0
    const/16 v15, 0x8

    if-ge v6, v15, :cond_1

    aget-byte v15, p1, v6

    sget-object v16, Ljcifs/ntlmssp/Type3Message;->NTLMSSP_SIGNATURE:[B

    aget-byte v16, v16, v6

    move/from16 v0, v16

    if-eq v15, v0, :cond_0

    new-instance v15, Ljava/io/IOException;

    const-string v16, "Not an NTLMSSP message."

    invoke-direct/range {v15 .. v16}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v15

    :cond_0
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    :cond_1
    const/16 v15, 0x8

    move-object/from16 v0, p1

    invoke-static {v0, v15}, Ljcifs/ntlmssp/Type3Message;->readULong([BI)I

    move-result v15

    const/16 v16, 0x3

    move/from16 v0, v16

    if-eq v15, v0, :cond_2

    new-instance v15, Ljava/io/IOException;

    const-string v16, "Not a Type 3 message."

    invoke-direct/range {v15 .. v16}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v15

    :cond_2
    const/16 v15, 0xc

    move-object/from16 v0, p1

    invoke-static {v0, v15}, Ljcifs/ntlmssp/Type3Message;->readSecurityBuffer([BI)[B

    move-result-object v7

    const/16 v15, 0x10

    move-object/from16 v0, p1

    invoke-static {v0, v15}, Ljcifs/ntlmssp/Type3Message;->readULong([BI)I

    move-result v8

    const/16 v15, 0x14

    move-object/from16 v0, p1

    invoke-static {v0, v15}, Ljcifs/ntlmssp/Type3Message;->readSecurityBuffer([BI)[B

    move-result-object v9

    const/16 v15, 0x18

    move-object/from16 v0, p1

    invoke-static {v0, v15}, Ljcifs/ntlmssp/Type3Message;->readULong([BI)I

    move-result v10

    const/16 v15, 0x1c

    move-object/from16 v0, p1

    invoke-static {v0, v15}, Ljcifs/ntlmssp/Type3Message;->readSecurityBuffer([BI)[B

    move-result-object v3

    const/16 v15, 0x20

    move-object/from16 v0, p1

    invoke-static {v0, v15}, Ljcifs/ntlmssp/Type3Message;->readULong([BI)I

    move-result v4

    const/16 v15, 0x24

    move-object/from16 v0, p1

    invoke-static {v0, v15}, Ljcifs/ntlmssp/Type3Message;->readSecurityBuffer([BI)[B

    move-result-object v11

    const/16 v15, 0x28

    move-object/from16 v0, p1

    invoke-static {v0, v15}, Ljcifs/ntlmssp/Type3Message;->readULong([BI)I

    move-result v12

    const/16 v15, 0x2c

    move-object/from16 v0, p1

    invoke-static {v0, v15}, Ljcifs/ntlmssp/Type3Message;->readSecurityBuffer([BI)[B

    move-result-object v13

    const/16 v15, 0x30

    move-object/from16 v0, p1

    invoke-static {v0, v15}, Ljcifs/ntlmssp/Type3Message;->readULong([BI)I

    move-result v14

    const/4 v1, 0x0

    const/16 v15, 0x34

    if-eq v8, v15, :cond_3

    const/16 v15, 0x34

    if-eq v10, v15, :cond_3

    const/16 v15, 0x34

    if-eq v4, v15, :cond_3

    const/16 v15, 0x34

    if-eq v12, v15, :cond_3

    const/16 v15, 0x34

    if-ne v14, v15, :cond_4

    :cond_3
    const/16 v5, 0x202

    invoke-static {}, Ljcifs/ntlmssp/Type3Message;->getOEMEncoding()Ljava/lang/String;

    move-result-object v2

    :goto_1
    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Ljcifs/ntlmssp/Type3Message;->setSessionKey([B)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Ljcifs/ntlmssp/Type3Message;->setFlags(I)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Ljcifs/ntlmssp/Type3Message;->setLMResponse([B)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Ljcifs/ntlmssp/Type3Message;->setNTResponse([B)V

    new-instance v15, Ljava/lang/String;

    invoke-direct {v15, v3, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Ljcifs/ntlmssp/Type3Message;->setDomain(Ljava/lang/String;)V

    new-instance v15, Ljava/lang/String;

    invoke-direct {v15, v11, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Ljcifs/ntlmssp/Type3Message;->setUser(Ljava/lang/String;)V

    new-instance v15, Ljava/lang/String;

    invoke-direct {v15, v13, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Ljcifs/ntlmssp/Type3Message;->setWorkstation(Ljava/lang/String;)V

    return-void

    :cond_4
    const/16 v15, 0x34

    move-object/from16 v0, p1

    invoke-static {v0, v15}, Ljcifs/ntlmssp/Type3Message;->readSecurityBuffer([BI)[B

    move-result-object v1

    const/16 v15, 0x3c

    move-object/from16 v0, p1

    invoke-static {v0, v15}, Ljcifs/ntlmssp/Type3Message;->readULong([BI)I

    move-result v5

    and-int/lit8 v15, v5, 0x1

    if-eqz v15, :cond_5

    const-string v2, "UTF-16LE"

    :goto_2
    goto :goto_1

    :cond_5
    invoke-static {}, Ljcifs/ntlmssp/Type3Message;->getOEMEncoding()Ljava/lang/String;

    move-result-object v2

    goto :goto_2
.end method


# virtual methods
.method public getDomain()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Ljcifs/ntlmssp/Type3Message;->domain:Ljava/lang/String;

    return-object v0
.end method

.method public getLMResponse()[B
    .locals 1

    iget-object v0, p0, Ljcifs/ntlmssp/Type3Message;->lmResponse:[B

    return-object v0
.end method

.method public getMasterKey()[B
    .locals 1

    iget-object v0, p0, Ljcifs/ntlmssp/Type3Message;->masterKey:[B

    return-object v0
.end method

.method public getNTResponse()[B
    .locals 1

    iget-object v0, p0, Ljcifs/ntlmssp/Type3Message;->ntResponse:[B

    return-object v0
.end method

.method public getSessionKey()[B
    .locals 1

    iget-object v0, p0, Ljcifs/ntlmssp/Type3Message;->sessionKey:[B

    return-object v0
.end method

.method public getUser()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Ljcifs/ntlmssp/Type3Message;->user:Ljava/lang/String;

    return-object v0
.end method

.method public getWorkstation()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Ljcifs/ntlmssp/Type3Message;->workstation:Ljava/lang/String;

    return-object v0
.end method

.method public setDomain(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Ljcifs/ntlmssp/Type3Message;->domain:Ljava/lang/String;

    return-void
.end method

.method public setLMResponse([B)V
    .locals 0
    .param p1    # [B

    iput-object p1, p0, Ljcifs/ntlmssp/Type3Message;->lmResponse:[B

    return-void
.end method

.method public setNTResponse([B)V
    .locals 0
    .param p1    # [B

    iput-object p1, p0, Ljcifs/ntlmssp/Type3Message;->ntResponse:[B

    return-void
.end method

.method public setSessionKey([B)V
    .locals 0
    .param p1    # [B

    iput-object p1, p0, Ljcifs/ntlmssp/Type3Message;->sessionKey:[B

    return-void
.end method

.method public setUser(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Ljcifs/ntlmssp/Type3Message;->user:Ljava/lang/String;

    return-void
.end method

.method public setWorkstation(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Ljcifs/ntlmssp/Type3Message;->workstation:Ljava/lang/String;

    return-void
.end method

.method public toByteArray()[B
    .locals 30

    :try_start_0
    invoke-virtual/range {p0 .. p0}, Ljcifs/ntlmssp/Type3Message;->getFlags()I

    move-result v9

    and-int/lit8 v26, v9, 0x1

    if-eqz v26, :cond_3

    const/16 v19, 0x1

    :goto_0
    if-eqz v19, :cond_4

    const/4 v15, 0x0

    :goto_1
    invoke-virtual/range {p0 .. p0}, Ljcifs/ntlmssp/Type3Message;->getDomain()Ljava/lang/String;

    move-result-object v7

    const/4 v5, 0x0

    if-eqz v7, :cond_0

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v26

    if-eqz v26, :cond_0

    if-eqz v19, :cond_5

    const-string v26, "UTF-16LE"

    move-object/from16 v0, v26

    invoke-virtual {v7, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v5

    :cond_0
    :goto_2
    if-eqz v5, :cond_6

    array-length v6, v5

    :goto_3
    invoke-virtual/range {p0 .. p0}, Ljcifs/ntlmssp/Type3Message;->getUser()Ljava/lang/String;

    move-result-object v22

    const/16 v20, 0x0

    if-eqz v22, :cond_1

    invoke-virtual/range {v22 .. v22}, Ljava/lang/String;->length()I

    move-result v26

    if-eqz v26, :cond_1

    if-eqz v19, :cond_7

    const-string v26, "UTF-16LE"

    move-object/from16 v0, v22

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v20

    :cond_1
    :goto_4
    if-eqz v20, :cond_8

    move-object/from16 v0, v20

    array-length v0, v0

    move/from16 v21, v0

    :goto_5
    invoke-virtual/range {p0 .. p0}, Ljcifs/ntlmssp/Type3Message;->getWorkstation()Ljava/lang/String;

    move-result-object v25

    const/16 v23, 0x0

    if-eqz v25, :cond_2

    invoke-virtual/range {v25 .. v25}, Ljava/lang/String;->length()I

    move-result v26

    if-eqz v26, :cond_2

    if-eqz v19, :cond_9

    const-string v26, "UTF-16LE"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v23

    :cond_2
    :goto_6
    if-eqz v23, :cond_a

    move-object/from16 v0, v23

    array-length v0, v0

    move/from16 v24, v0

    :goto_7
    invoke-virtual/range {p0 .. p0}, Ljcifs/ntlmssp/Type3Message;->getLMResponse()[B

    move-result-object v12

    if-eqz v12, :cond_b

    array-length v11, v12

    :goto_8
    invoke-virtual/range {p0 .. p0}, Ljcifs/ntlmssp/Type3Message;->getNTResponse()[B

    move-result-object v14

    if-eqz v14, :cond_c

    array-length v13, v14

    :goto_9
    invoke-virtual/range {p0 .. p0}, Ljcifs/ntlmssp/Type3Message;->getSessionKey()[B

    move-result-object v17

    if-eqz v17, :cond_d

    move-object/from16 v0, v17

    array-length v10, v0

    :goto_a
    add-int/lit8 v26, v6, 0x40

    add-int v26, v26, v21

    add-int v26, v26, v24

    add-int v26, v26, v11

    add-int v26, v26, v13

    add-int v26, v26, v10

    move/from16 v0, v26

    new-array v0, v0, [B

    move-object/from16 v18, v0

    sget-object v26, Ljcifs/ntlmssp/Type3Message;->NTLMSSP_SIGNATURE:[B

    const/16 v27, 0x0

    const/16 v28, 0x0

    const/16 v29, 0x8

    move-object/from16 v0, v26

    move/from16 v1, v27

    move-object/from16 v2, v18

    move/from16 v3, v28

    move/from16 v4, v29

    invoke-static {v0, v1, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    const/16 v26, 0x8

    const/16 v27, 0x3

    move-object/from16 v0, v18

    move/from16 v1, v26

    move/from16 v2, v27

    invoke-static {v0, v1, v2}, Ljcifs/ntlmssp/Type3Message;->writeULong([BII)V

    const/16 v16, 0x40

    const/16 v26, 0xc

    move-object/from16 v0, v18

    move/from16 v1, v26

    move/from16 v2, v16

    invoke-static {v0, v1, v2, v12}, Ljcifs/ntlmssp/Type3Message;->writeSecurityBuffer([BII[B)V

    add-int v16, v16, v11

    const/16 v26, 0x14

    move-object/from16 v0, v18

    move/from16 v1, v26

    move/from16 v2, v16

    invoke-static {v0, v1, v2, v14}, Ljcifs/ntlmssp/Type3Message;->writeSecurityBuffer([BII[B)V

    add-int v16, v16, v13

    const/16 v26, 0x1c

    move-object/from16 v0, v18

    move/from16 v1, v26

    move/from16 v2, v16

    invoke-static {v0, v1, v2, v5}, Ljcifs/ntlmssp/Type3Message;->writeSecurityBuffer([BII[B)V

    add-int v16, v16, v6

    const/16 v26, 0x24

    move-object/from16 v0, v18

    move/from16 v1, v26

    move/from16 v2, v16

    move-object/from16 v3, v20

    invoke-static {v0, v1, v2, v3}, Ljcifs/ntlmssp/Type3Message;->writeSecurityBuffer([BII[B)V

    add-int v16, v16, v21

    const/16 v26, 0x2c

    move-object/from16 v0, v18

    move/from16 v1, v26

    move/from16 v2, v16

    move-object/from16 v3, v23

    invoke-static {v0, v1, v2, v3}, Ljcifs/ntlmssp/Type3Message;->writeSecurityBuffer([BII[B)V

    add-int v16, v16, v24

    const/16 v26, 0x34

    move-object/from16 v0, v18

    move/from16 v1, v26

    move/from16 v2, v16

    move-object/from16 v3, v17

    invoke-static {v0, v1, v2, v3}, Ljcifs/ntlmssp/Type3Message;->writeSecurityBuffer([BII[B)V

    const/16 v26, 0x3c

    move-object/from16 v0, v18

    move/from16 v1, v26

    invoke-static {v0, v1, v9}, Ljcifs/ntlmssp/Type3Message;->writeULong([BII)V

    return-object v18

    :cond_3
    const/16 v19, 0x0

    goto/16 :goto_0

    :cond_4
    invoke-static {}, Ljcifs/ntlmssp/Type3Message;->getOEMEncoding()Ljava/lang/String;

    move-result-object v15

    goto/16 :goto_1

    :cond_5
    invoke-virtual {v7, v15}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v5

    goto/16 :goto_2

    :cond_6
    const/4 v6, 0x0

    goto/16 :goto_3

    :cond_7
    invoke-virtual/range {v22 .. v22}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v15}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v20

    goto/16 :goto_4

    :cond_8
    const/16 v21, 0x0

    goto/16 :goto_5

    :cond_9
    invoke-virtual/range {v25 .. v25}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v15}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v23

    goto/16 :goto_6

    :cond_a
    const/16 v24, 0x0

    goto/16 :goto_7

    :cond_b
    const/4 v11, 0x0

    goto/16 :goto_8

    :cond_c
    const/4 v13, 0x0

    goto/16 :goto_9

    :cond_d
    const/4 v10, 0x0

    goto/16 :goto_a

    :catch_0
    move-exception v8

    new-instance v26, Ljava/lang/IllegalStateException;

    invoke-virtual {v8}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v27

    invoke-direct/range {v26 .. v27}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v26
.end method

.method public toString()Ljava/lang/String;
    .locals 9

    invoke-virtual {p0}, Ljcifs/ntlmssp/Type3Message;->getUser()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Ljcifs/ntlmssp/Type3Message;->getDomain()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljcifs/ntlmssp/Type3Message;->getWorkstation()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0}, Ljcifs/ntlmssp/Type3Message;->getLMResponse()[B

    move-result-object v1

    invoke-virtual {p0}, Ljcifs/ntlmssp/Type3Message;->getNTResponse()[B

    move-result-object v2

    invoke-virtual {p0}, Ljcifs/ntlmssp/Type3Message;->getSessionKey()[B

    move-result-object v3

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Type3Message[domain="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ",user="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ",workstation="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ",lmResponse="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    if-nez v1, :cond_0

    const-string v6, "null"

    :goto_0
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ",ntResponse="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    if-nez v2, :cond_1

    const-string v6, "null"

    :goto_1
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ",sessionKey="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    if-nez v3, :cond_2

    const-string v6, "null"

    :goto_2
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ",flags=0x"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p0}, Ljcifs/ntlmssp/Type3Message;->getFlags()I

    move-result v7

    const/16 v8, 0x8

    invoke-static {v7, v8}, Ljcifs/util/Hexdump;->toHexString(II)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    return-object v6

    :cond_0
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "<"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    array-length v8, v1

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " bytes>"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    goto :goto_0

    :cond_1
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "<"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    array-length v8, v2

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " bytes>"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    goto :goto_1

    :cond_2
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "<"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    array-length v8, v3

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " bytes>"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    goto :goto_2
.end method
