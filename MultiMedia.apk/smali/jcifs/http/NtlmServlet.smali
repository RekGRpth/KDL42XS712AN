.class public abstract Ljcifs/http/NtlmServlet;
.super Ljavax/servlet/http/HttpServlet;
.source "NtlmServlet.java"


# instance fields
.field private defaultDomain:Ljava/lang/String;

.field private domainController:Ljava/lang/String;

.field private enableBasic:Z

.field private insecureBasic:Z

.field private loadBalance:Z

.field private realm:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljavax/servlet/http/HttpServlet;-><init>()V

    return-void
.end method


# virtual methods
.method public init(Ljavax/servlet/ServletConfig;)V
    .locals 4
    .param p1    # Ljavax/servlet/ServletConfig;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/servlet/ServletException;
        }
    .end annotation

    invoke-super {p0, p1}, Ljavax/servlet/http/HttpServlet;->init(Ljavax/servlet/ServletConfig;)V

    const-string v2, "jcifs.smb.client.soTimeout"

    const-string v3, "300000"

    invoke-static {v2, v3}, Ljcifs/Config;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v2, "jcifs.netbios.cachePolicy"

    const-string v3, "600"

    invoke-static {v2, v3}, Ljcifs/Config;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    invoke-interface {p1}, Ljavax/servlet/ServletConfig;->getInitParameterNames()Ljava/util/Enumeration;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const-string v2, "jcifs."

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p1, v1}, Ljavax/servlet/ServletConfig;->getInitParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Ljcifs/Config;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    const-string v2, "jcifs.smb.client.domain"

    invoke-static {v2}, Ljcifs/Config;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Ljcifs/http/NtlmServlet;->defaultDomain:Ljava/lang/String;

    const-string v2, "jcifs.http.domainController"

    invoke-static {v2}, Ljcifs/Config;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Ljcifs/http/NtlmServlet;->domainController:Ljava/lang/String;

    iget-object v2, p0, Ljcifs/http/NtlmServlet;->domainController:Ljava/lang/String;

    if-nez v2, :cond_2

    iget-object v2, p0, Ljcifs/http/NtlmServlet;->defaultDomain:Ljava/lang/String;

    iput-object v2, p0, Ljcifs/http/NtlmServlet;->domainController:Ljava/lang/String;

    const-string v2, "jcifs.http.loadBalance"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Ljcifs/Config;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Ljcifs/http/NtlmServlet;->loadBalance:Z

    :cond_2
    const-string v2, "jcifs.http.enableBasic"

    invoke-static {v2}, Ljcifs/Config;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    iput-boolean v2, p0, Ljcifs/http/NtlmServlet;->enableBasic:Z

    const-string v2, "jcifs.http.insecureBasic"

    invoke-static {v2}, Ljcifs/Config;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    iput-boolean v2, p0, Ljcifs/http/NtlmServlet;->insecureBasic:Z

    const-string v2, "jcifs.http.basicRealm"

    invoke-static {v2}, Ljcifs/Config;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Ljcifs/http/NtlmServlet;->realm:Ljava/lang/String;

    iget-object v2, p0, Ljcifs/http/NtlmServlet;->realm:Ljava/lang/String;

    if-nez v2, :cond_3

    const-string v2, "jCIFS"

    iput-object v2, p0, Ljcifs/http/NtlmServlet;->realm:Ljava/lang/String;

    :cond_3
    return-void
.end method

.method protected service(Ljavax/servlet/http/HttpServletRequest;Ljavax/servlet/http/HttpServletResponse;)V
    .locals 17
    .param p1    # Ljavax/servlet/http/HttpServletRequest;
    .param p2    # Ljavax/servlet/http/HttpServletResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/servlet/ServletException;,
            Ljava/io/IOException;
        }
    .end annotation

    move-object/from16 v0, p0

    iget-boolean v14, v0, Ljcifs/http/NtlmServlet;->enableBasic:Z

    if-eqz v14, :cond_2

    move-object/from16 v0, p0

    iget-boolean v14, v0, Ljcifs/http/NtlmServlet;->insecureBasic:Z

    if-nez v14, :cond_0

    invoke-interface/range {p1 .. p1}, Ljavax/servlet/http/HttpServletRequest;->isSecure()Z

    move-result v14

    if-eqz v14, :cond_2

    :cond_0
    const/4 v9, 0x1

    :goto_0
    const-string v14, "Authorization"

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Ljavax/servlet/http/HttpServletRequest;->getHeader(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_d

    const-string v14, "NTLM "

    invoke-virtual {v7, v14}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v14

    if-nez v14, :cond_1

    if-eqz v9, :cond_d

    const-string v14, "Basic "

    invoke-virtual {v7, v14}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_d

    :cond_1
    move-object/from16 v0, p0

    iget-boolean v14, v0, Ljcifs/http/NtlmServlet;->loadBalance:Z

    if-eqz v14, :cond_3

    new-instance v4, Ljcifs/UniAddress;

    move-object/from16 v0, p0

    iget-object v14, v0, Ljcifs/http/NtlmServlet;->domainController:Ljava/lang/String;

    const/16 v15, 0x1c

    const/16 v16, 0x0

    invoke-static/range {v14 .. v16}, Ljcifs/netbios/NbtAddress;->getByName(Ljava/lang/String;ILjava/lang/String;)Ljcifs/netbios/NbtAddress;

    move-result-object v14

    invoke-direct {v4, v14}, Ljcifs/UniAddress;-><init>(Ljava/lang/Object;)V

    :goto_1
    const-string v14, "NTLM "

    invoke-virtual {v7, v14}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_4

    invoke-static {v4}, Ljcifs/smb/SmbSession;->getChallenge(Ljcifs/UniAddress;)[B

    move-result-object v3

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-static {v0, v1, v3}, Ljcifs/http/NtlmSsp;->authenticate(Ljavax/servlet/http/HttpServletRequest;Ljavax/servlet/http/HttpServletResponse;[B)Ljcifs/smb/NtlmPasswordAuthentication;

    move-result-object v8

    if-nez v8, :cond_7

    :goto_2
    return-void

    :cond_2
    const/4 v9, 0x0

    goto :goto_0

    :cond_3
    move-object/from16 v0, p0

    iget-object v14, v0, Ljcifs/http/NtlmServlet;->domainController:Ljava/lang/String;

    const/4 v15, 0x1

    invoke-static {v14, v15}, Ljcifs/UniAddress;->getByName(Ljava/lang/String;Z)Ljcifs/UniAddress;

    move-result-object v4

    goto :goto_1

    :cond_4
    new-instance v2, Ljava/lang/String;

    const/4 v14, 0x6

    invoke-virtual {v7, v14}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Ljcifs/util/Base64;->decode(Ljava/lang/String;)[B

    move-result-object v14

    const-string v15, "US-ASCII"

    invoke-direct {v2, v14, v15}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    const/16 v14, 0x3a

    invoke-virtual {v2, v14}, Ljava/lang/String;->indexOf(I)I

    move-result v6

    const/4 v14, -0x1

    if-eq v6, v14, :cond_9

    const/4 v14, 0x0

    invoke-virtual {v2, v14, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    :goto_3
    const/4 v14, -0x1

    if-eq v6, v14, :cond_a

    add-int/lit8 v14, v6, 0x1

    invoke-virtual {v2, v14}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v10

    :goto_4
    const/16 v14, 0x5c

    invoke-virtual {v13, v14}, Ljava/lang/String;->indexOf(I)I

    move-result v6

    const/4 v14, -0x1

    if-ne v6, v14, :cond_5

    const/16 v14, 0x2f

    invoke-virtual {v13, v14}, Ljava/lang/String;->indexOf(I)I

    move-result v6

    :cond_5
    const/4 v14, -0x1

    if-eq v6, v14, :cond_b

    const/4 v14, 0x0

    invoke-virtual {v13, v14, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    :goto_5
    const/4 v14, -0x1

    if-eq v6, v14, :cond_6

    add-int/lit8 v14, v6, 0x1

    invoke-virtual {v13, v14}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v13

    :cond_6
    new-instance v8, Ljcifs/smb/NtlmPasswordAuthentication;

    invoke-direct {v8, v5, v13, v10}, Ljcifs/smb/NtlmPasswordAuthentication;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_7
    :try_start_0
    invoke-static {v4, v8}, Ljcifs/smb/SmbSession;->logon(Ljcifs/UniAddress;Ljcifs/smb/NtlmPasswordAuthentication;)V
    :try_end_0
    .catch Ljcifs/smb/SmbAuthException; {:try_start_0 .. :try_end_0} :catch_0

    invoke-interface/range {p1 .. p1}, Ljavax/servlet/http/HttpServletRequest;->getSession()Ljavax/servlet/http/HttpSession;

    move-result-object v12

    const-string v14, "NtlmHttpAuth"

    invoke-interface {v12, v14, v8}, Ljavax/servlet/http/HttpSession;->setAttribute(Ljava/lang/String;Ljava/lang/Object;)V

    const-string v14, "ntlmdomain"

    invoke-virtual {v8}, Ljcifs/smb/NtlmPasswordAuthentication;->getDomain()Ljava/lang/String;

    move-result-object v15

    invoke-interface {v12, v14, v15}, Ljavax/servlet/http/HttpSession;->setAttribute(Ljava/lang/String;Ljava/lang/Object;)V

    const-string v14, "ntlmuser"

    invoke-virtual {v8}, Ljcifs/smb/NtlmPasswordAuthentication;->getUsername()Ljava/lang/String;

    move-result-object v15

    invoke-interface {v12, v14, v15}, Ljavax/servlet/http/HttpSession;->setAttribute(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_8
    invoke-super/range {p0 .. p2}, Ljavax/servlet/http/HttpServlet;->service(Ljavax/servlet/http/HttpServletRequest;Ljavax/servlet/http/HttpServletResponse;)V

    goto :goto_2

    :cond_9
    move-object v13, v2

    goto :goto_3

    :cond_a
    const-string v10, ""

    goto :goto_4

    :cond_b
    move-object/from16 v0, p0

    iget-object v5, v0, Ljcifs/http/NtlmServlet;->defaultDomain:Ljava/lang/String;

    goto :goto_5

    :catch_0
    move-exception v11

    const-string v14, "WWW-Authenticate"

    const-string v15, "NTLM"

    move-object/from16 v0, p2

    invoke-interface {v0, v14, v15}, Ljavax/servlet/http/HttpServletResponse;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v9, :cond_c

    const-string v14, "WWW-Authenticate"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Basic realm=\""

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Ljcifs/http/NtlmServlet;->realm:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "\""

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p2

    invoke-interface {v0, v14, v15}, Ljavax/servlet/http/HttpServletResponse;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    :cond_c
    const-string v14, "Connection"

    const-string v15, "close"

    move-object/from16 v0, p2

    invoke-interface {v0, v14, v15}, Ljavax/servlet/http/HttpServletResponse;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v14, 0x191

    move-object/from16 v0, p2

    invoke-interface {v0, v14}, Ljavax/servlet/http/HttpServletResponse;->setStatus(I)V

    invoke-interface/range {p2 .. p2}, Ljavax/servlet/http/HttpServletResponse;->flushBuffer()V

    goto/16 :goto_2

    :cond_d
    const/4 v14, 0x0

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Ljavax/servlet/http/HttpServletRequest;->getSession(Z)Ljavax/servlet/http/HttpSession;

    move-result-object v12

    if-eqz v12, :cond_e

    const-string v14, "NtlmHttpAuth"

    invoke-interface {v12, v14}, Ljavax/servlet/http/HttpSession;->getAttribute(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v14

    if-nez v14, :cond_8

    :cond_e
    const-string v14, "WWW-Authenticate"

    const-string v15, "NTLM"

    move-object/from16 v0, p2

    invoke-interface {v0, v14, v15}, Ljavax/servlet/http/HttpServletResponse;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v9, :cond_f

    const-string v14, "WWW-Authenticate"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Basic realm=\""

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Ljcifs/http/NtlmServlet;->realm:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "\""

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p2

    invoke-interface {v0, v14, v15}, Ljavax/servlet/http/HttpServletResponse;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    :cond_f
    const/16 v14, 0x191

    move-object/from16 v0, p2

    invoke-interface {v0, v14}, Ljavax/servlet/http/HttpServletResponse;->setStatus(I)V

    invoke-interface/range {p2 .. p2}, Ljavax/servlet/http/HttpServletResponse;->flushBuffer()V

    goto/16 :goto_2
.end method
