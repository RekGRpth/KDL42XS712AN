.class public Ljcifs/http/NtlmHttpFilter;
.super Ljava/lang/Object;
.source "NtlmHttpFilter.java"

# interfaces
.implements Ljavax/servlet/Filter;


# static fields
.field private static log:Ljcifs/util/LogStream;


# instance fields
.field private defaultDomain:Ljava/lang/String;

.field private domainController:Ljava/lang/String;

.field private enableBasic:Z

.field private insecureBasic:Z

.field private loadBalance:Z

.field private realm:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Ljcifs/util/LogStream;->getInstance()Ljcifs/util/LogStream;

    move-result-object v0

    sput-object v0, Ljcifs/http/NtlmHttpFilter;->log:Ljcifs/util/LogStream;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public destroy()V
    .locals 0

    return-void
.end method

.method public doFilter(Ljavax/servlet/ServletRequest;Ljavax/servlet/ServletResponse;Ljavax/servlet/FilterChain;)V
    .locals 4
    .param p1    # Ljavax/servlet/ServletRequest;
    .param p2    # Ljavax/servlet/ServletResponse;
    .param p3    # Ljavax/servlet/FilterChain;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljavax/servlet/ServletException;
        }
    .end annotation

    move-object v1, p1

    check-cast v1, Ljavax/servlet/http/HttpServletRequest;

    move-object v2, p2

    check-cast v2, Ljavax/servlet/http/HttpServletResponse;

    const/4 v3, 0x0

    invoke-virtual {p0, v1, v2, v3}, Ljcifs/http/NtlmHttpFilter;->negotiate(Ljavax/servlet/http/HttpServletRequest;Ljavax/servlet/http/HttpServletResponse;Z)Ljcifs/smb/NtlmPasswordAuthentication;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v3, Ljcifs/http/NtlmHttpServletRequest;

    invoke-direct {v3, v1, v0}, Ljcifs/http/NtlmHttpServletRequest;-><init>(Ljavax/servlet/http/HttpServletRequest;Ljava/security/Principal;)V

    invoke-interface {p3, v3, p2}, Ljavax/servlet/FilterChain;->doFilter(Ljavax/servlet/ServletRequest;Ljavax/servlet/ServletResponse;)V

    goto :goto_0
.end method

.method public getFilterConfig()Ljavax/servlet/FilterConfig;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public init(Ljavax/servlet/FilterConfig;)V
    .locals 6
    .param p1    # Ljavax/servlet/FilterConfig;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/servlet/ServletException;
        }
    .end annotation

    const/4 v5, -0x1

    const-string v3, "jcifs.smb.client.soTimeout"

    const-string v4, "1800000"

    invoke-static {v3, v4}, Ljcifs/Config;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v3, "jcifs.netbios.cachePolicy"

    const-string v4, "1200"

    invoke-static {v3, v4}, Ljcifs/Config;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v3, "jcifs.smb.lmCompatibility"

    const-string v4, "0"

    invoke-static {v3, v4}, Ljcifs/Config;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v3, "jcifs.smb.client.useExtendedSecurity"

    const-string v4, "false"

    invoke-static {v3, v4}, Ljcifs/Config;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    invoke-interface {p1}, Ljavax/servlet/FilterConfig;->getInitParameterNames()Ljava/util/Enumeration;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const-string v3, "jcifs."

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {p1, v2}, Ljavax/servlet/FilterConfig;->getInitParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Ljcifs/Config;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    const-string v3, "jcifs.smb.client.domain"

    invoke-static {v3}, Ljcifs/Config;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Ljcifs/http/NtlmHttpFilter;->defaultDomain:Ljava/lang/String;

    const-string v3, "jcifs.http.domainController"

    invoke-static {v3}, Ljcifs/Config;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Ljcifs/http/NtlmHttpFilter;->domainController:Ljava/lang/String;

    iget-object v3, p0, Ljcifs/http/NtlmHttpFilter;->domainController:Ljava/lang/String;

    if-nez v3, :cond_2

    iget-object v3, p0, Ljcifs/http/NtlmHttpFilter;->defaultDomain:Ljava/lang/String;

    iput-object v3, p0, Ljcifs/http/NtlmHttpFilter;->domainController:Ljava/lang/String;

    const-string v3, "jcifs.http.loadBalance"

    const/4 v4, 0x1

    invoke-static {v3, v4}, Ljcifs/Config;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Ljcifs/http/NtlmHttpFilter;->loadBalance:Z

    :cond_2
    const-string v3, "jcifs.http.enableBasic"

    invoke-static {v3}, Ljcifs/Config;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    iput-boolean v3, p0, Ljcifs/http/NtlmHttpFilter;->enableBasic:Z

    const-string v3, "jcifs.http.insecureBasic"

    invoke-static {v3}, Ljcifs/Config;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    iput-boolean v3, p0, Ljcifs/http/NtlmHttpFilter;->insecureBasic:Z

    const-string v3, "jcifs.http.basicRealm"

    invoke-static {v3}, Ljcifs/Config;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Ljcifs/http/NtlmHttpFilter;->realm:Ljava/lang/String;

    iget-object v3, p0, Ljcifs/http/NtlmHttpFilter;->realm:Ljava/lang/String;

    if-nez v3, :cond_3

    const-string v3, "jCIFS"

    iput-object v3, p0, Ljcifs/http/NtlmHttpFilter;->realm:Ljava/lang/String;

    :cond_3
    const-string v3, "jcifs.util.loglevel"

    invoke-static {v3, v5}, Ljcifs/Config;->getInt(Ljava/lang/String;I)I

    move-result v1

    if-eq v1, v5, :cond_4

    invoke-static {v1}, Ljcifs/util/LogStream;->setLevel(I)V

    :cond_4
    sget-object v3, Ljcifs/http/NtlmHttpFilter;->log:Ljcifs/util/LogStream;

    sget v3, Ljcifs/util/LogStream;->level:I

    const/4 v4, 0x2

    if-le v3, v4, :cond_5

    :try_start_0
    sget-object v3, Ljcifs/http/NtlmHttpFilter;->log:Ljcifs/util/LogStream;

    const-string v4, "JCIFS PROPERTIES"

    invoke-static {v3, v4}, Ljcifs/Config;->store(Ljava/io/OutputStream;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_5
    :goto_1
    return-void

    :catch_0
    move-exception v3

    goto :goto_1
.end method

.method protected negotiate(Ljavax/servlet/http/HttpServletRequest;Ljavax/servlet/http/HttpServletResponse;Z)Ljcifs/smb/NtlmPasswordAuthentication;
    .locals 19
    .param p1    # Ljavax/servlet/http/HttpServletRequest;
    .param p2    # Ljavax/servlet/http/HttpServletResponse;
    .param p3    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljavax/servlet/ServletException;
        }
    .end annotation

    const/4 v9, 0x0

    const-string v15, "Authorization"

    move-object/from16 v0, p1

    invoke-interface {v0, v15}, Ljavax/servlet/http/HttpServletRequest;->getHeader(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p0

    iget-boolean v15, v0, Ljcifs/http/NtlmHttpFilter;->enableBasic:Z

    if-eqz v15, :cond_3

    move-object/from16 v0, p0

    iget-boolean v15, v0, Ljcifs/http/NtlmHttpFilter;->insecureBasic:Z

    if-nez v15, :cond_0

    invoke-interface/range {p1 .. p1}, Ljavax/servlet/http/HttpServletRequest;->isSecure()Z

    move-result v15

    if-eqz v15, :cond_3

    :cond_0
    const/4 v10, 0x1

    :goto_0
    if-eqz v8, :cond_11

    const-string v15, "NTLM "

    invoke-virtual {v8, v15}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v15

    if-nez v15, :cond_1

    if-eqz v10, :cond_11

    const-string v15, "Basic "

    invoke-virtual {v8, v15}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_11

    :cond_1
    const-string v15, "NTLM "

    invoke-virtual {v8, v15}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_8

    invoke-interface/range {p1 .. p1}, Ljavax/servlet/http/HttpServletRequest;->getSession()Ljavax/servlet/http/HttpSession;

    move-result-object v13

    move-object/from16 v0, p0

    iget-boolean v15, v0, Ljcifs/http/NtlmHttpFilter;->loadBalance:Z

    if-eqz v15, :cond_4

    const-string v15, "NtlmHttpChal"

    invoke-interface {v13, v15}, Ljavax/servlet/http/HttpSession;->getAttribute(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljcifs/smb/NtlmChallenge;

    if-nez v3, :cond_2

    invoke-static {}, Ljcifs/smb/SmbSession;->getChallengeForDomain()Ljcifs/smb/NtlmChallenge;

    move-result-object v3

    const-string v15, "NtlmHttpChal"

    invoke-interface {v13, v15, v3}, Ljavax/servlet/http/HttpSession;->setAttribute(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_2
    iget-object v5, v3, Ljcifs/smb/NtlmChallenge;->dc:Ljcifs/UniAddress;

    iget-object v4, v3, Ljcifs/smb/NtlmChallenge;->challenge:[B

    :goto_1
    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-static {v0, v1, v4}, Ljcifs/http/NtlmSsp;->authenticate(Ljavax/servlet/http/HttpServletRequest;Ljavax/servlet/http/HttpServletResponse;[B)Ljcifs/smb/NtlmPasswordAuthentication;

    move-result-object v9

    if-nez v9, :cond_5

    const/4 v15, 0x0

    :goto_2
    return-object v15

    :cond_3
    const/4 v10, 0x0

    goto :goto_0

    :cond_4
    move-object/from16 v0, p0

    iget-object v15, v0, Ljcifs/http/NtlmHttpFilter;->domainController:Ljava/lang/String;

    const/16 v16, 0x1

    invoke-static/range {v15 .. v16}, Ljcifs/UniAddress;->getByName(Ljava/lang/String;Z)Ljcifs/UniAddress;

    move-result-object v5

    invoke-static {v5}, Ljcifs/smb/SmbSession;->getChallenge(Ljcifs/UniAddress;)[B

    move-result-object v4

    goto :goto_1

    :cond_5
    const-string v15, "NtlmHttpChal"

    invoke-interface {v13, v15}, Ljavax/servlet/http/HttpSession;->removeAttribute(Ljava/lang/String;)V

    :goto_3
    :try_start_0
    invoke-static {v5, v9}, Ljcifs/smb/SmbSession;->logon(Ljcifs/UniAddress;Ljcifs/smb/NtlmPasswordAuthentication;)V

    sget-object v15, Ljcifs/http/NtlmHttpFilter;->log:Ljcifs/util/LogStream;

    sget v15, Ljcifs/util/LogStream;->level:I

    const/16 v16, 0x2

    move/from16 v0, v16

    if-le v15, v0, :cond_6

    sget-object v15, Ljcifs/http/NtlmHttpFilter;->log:Ljcifs/util/LogStream;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "NtlmHttpFilter: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, " successfully authenticated against "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljcifs/util/LogStream;->println(Ljava/lang/String;)V
    :try_end_0
    .catch Ljcifs/smb/SmbAuthException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_6
    invoke-interface/range {p1 .. p1}, Ljavax/servlet/http/HttpServletRequest;->getSession()Ljavax/servlet/http/HttpSession;

    move-result-object v15

    const-string v16, "NtlmHttpAuth"

    move-object/from16 v0, v16

    invoke-interface {v15, v0, v9}, Ljavax/servlet/http/HttpSession;->setAttribute(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_7
    move-object v15, v9

    goto :goto_2

    :cond_8
    new-instance v2, Ljava/lang/String;

    const/4 v15, 0x6

    invoke-virtual {v8, v15}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v15

    invoke-static {v15}, Ljcifs/util/Base64;->decode(Ljava/lang/String;)[B

    move-result-object v15

    const-string v16, "US-ASCII"

    move-object/from16 v0, v16

    invoke-direct {v2, v15, v0}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    const/16 v15, 0x3a

    invoke-virtual {v2, v15}, Ljava/lang/String;->indexOf(I)I

    move-result v7

    const/4 v15, -0x1

    if-eq v7, v15, :cond_b

    const/4 v15, 0x0

    invoke-virtual {v2, v15, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v14

    :goto_4
    const/4 v15, -0x1

    if-eq v7, v15, :cond_c

    add-int/lit8 v15, v7, 0x1

    invoke-virtual {v2, v15}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v11

    :goto_5
    const/16 v15, 0x5c

    invoke-virtual {v14, v15}, Ljava/lang/String;->indexOf(I)I

    move-result v7

    const/4 v15, -0x1

    if-ne v7, v15, :cond_9

    const/16 v15, 0x2f

    invoke-virtual {v14, v15}, Ljava/lang/String;->indexOf(I)I

    move-result v7

    :cond_9
    const/4 v15, -0x1

    if-eq v7, v15, :cond_d

    const/4 v15, 0x0

    invoke-virtual {v14, v15, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    :goto_6
    const/4 v15, -0x1

    if-eq v7, v15, :cond_a

    add-int/lit8 v15, v7, 0x1

    invoke-virtual {v14, v15}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v14

    :cond_a
    new-instance v9, Ljcifs/smb/NtlmPasswordAuthentication;

    invoke-direct {v9, v6, v14, v11}, Ljcifs/smb/NtlmPasswordAuthentication;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v15, v0, Ljcifs/http/NtlmHttpFilter;->domainController:Ljava/lang/String;

    const/16 v16, 0x1

    invoke-static/range {v15 .. v16}, Ljcifs/UniAddress;->getByName(Ljava/lang/String;Z)Ljcifs/UniAddress;

    move-result-object v5

    goto/16 :goto_3

    :cond_b
    move-object v14, v2

    goto :goto_4

    :cond_c
    const-string v11, ""

    goto :goto_5

    :cond_d
    move-object/from16 v0, p0

    iget-object v6, v0, Ljcifs/http/NtlmHttpFilter;->defaultDomain:Ljava/lang/String;

    goto :goto_6

    :catch_0
    move-exception v12

    sget-object v15, Ljcifs/http/NtlmHttpFilter;->log:Ljcifs/util/LogStream;

    sget v15, Ljcifs/util/LogStream;->level:I

    const/16 v16, 0x1

    move/from16 v0, v16

    if-le v15, v0, :cond_e

    sget-object v15, Ljcifs/http/NtlmHttpFilter;->log:Ljcifs/util/LogStream;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "NtlmHttpFilter: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual {v9}, Ljcifs/smb/NtlmPasswordAuthentication;->getName()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, ": 0x"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual {v12}, Ljcifs/smb/SmbAuthException;->getNtStatus()I

    move-result v17

    const/16 v18, 0x8

    invoke-static/range {v17 .. v18}, Ljcifs/util/Hexdump;->toHexString(II)Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, ": "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljcifs/util/LogStream;->println(Ljava/lang/String;)V

    :cond_e
    invoke-virtual {v12}, Ljcifs/smb/SmbAuthException;->getNtStatus()I

    move-result v15

    const v16, -0x3ffffffb    # -2.0000012f

    move/from16 v0, v16

    if-ne v15, v0, :cond_f

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-interface {v0, v15}, Ljavax/servlet/http/HttpServletRequest;->getSession(Z)Ljavax/servlet/http/HttpSession;

    move-result-object v13

    if-eqz v13, :cond_f

    const-string v15, "NtlmHttpAuth"

    invoke-interface {v13, v15}, Ljavax/servlet/http/HttpSession;->removeAttribute(Ljava/lang/String;)V

    :cond_f
    const-string v15, "WWW-Authenticate"

    const-string v16, "NTLM"

    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-interface {v0, v15, v1}, Ljavax/servlet/http/HttpServletResponse;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v10, :cond_10

    const-string v15, "WWW-Authenticate"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "Basic realm=\""

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v0, v0, Ljcifs/http/NtlmHttpFilter;->realm:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, "\""

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-interface {v0, v15, v1}, Ljavax/servlet/http/HttpServletResponse;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    :cond_10
    const/16 v15, 0x191

    move-object/from16 v0, p2

    invoke-interface {v0, v15}, Ljavax/servlet/http/HttpServletResponse;->setStatus(I)V

    const/4 v15, 0x0

    move-object/from16 v0, p2

    invoke-interface {v0, v15}, Ljavax/servlet/http/HttpServletResponse;->setContentLength(I)V

    invoke-interface/range {p2 .. p2}, Ljavax/servlet/http/HttpServletResponse;->flushBuffer()V

    const/4 v15, 0x0

    goto/16 :goto_2

    :cond_11
    if-nez p3, :cond_7

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-interface {v0, v15}, Ljavax/servlet/http/HttpServletRequest;->getSession(Z)Ljavax/servlet/http/HttpSession;

    move-result-object v13

    if-eqz v13, :cond_12

    const-string v15, "NtlmHttpAuth"

    invoke-interface {v13, v15}, Ljavax/servlet/http/HttpSession;->getAttribute(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljcifs/smb/NtlmPasswordAuthentication;

    if-nez v9, :cond_7

    :cond_12
    const-string v15, "WWW-Authenticate"

    const-string v16, "NTLM"

    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-interface {v0, v15, v1}, Ljavax/servlet/http/HttpServletResponse;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v10, :cond_13

    const-string v15, "WWW-Authenticate"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "Basic realm=\""

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v0, v0, Ljcifs/http/NtlmHttpFilter;->realm:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, "\""

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-interface {v0, v15, v1}, Ljavax/servlet/http/HttpServletResponse;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    :cond_13
    const/16 v15, 0x191

    move-object/from16 v0, p2

    invoke-interface {v0, v15}, Ljavax/servlet/http/HttpServletResponse;->setStatus(I)V

    const/4 v15, 0x0

    move-object/from16 v0, p2

    invoke-interface {v0, v15}, Ljavax/servlet/http/HttpServletResponse;->setContentLength(I)V

    invoke-interface/range {p2 .. p2}, Ljavax/servlet/http/HttpServletResponse;->flushBuffer()V

    const/4 v15, 0x0

    goto/16 :goto_2
.end method

.method public setFilterConfig(Ljavax/servlet/FilterConfig;)V
    .locals 1
    .param p1    # Ljavax/servlet/FilterConfig;

    :try_start_0
    invoke-virtual {p0, p1}, Ljcifs/http/NtlmHttpFilter;->init(Ljavax/servlet/FilterConfig;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
