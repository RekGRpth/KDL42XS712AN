.class Ljcifs/smb/NtTransQuerySecurityDesc;
.super Ljcifs/smb/SmbComNtTransaction;
.source "NtTransQuerySecurityDesc.java"


# instance fields
.field fid:I

.field securityInformation:I


# direct methods
.method constructor <init>(II)V
    .locals 2
    .param p1    # I
    .param p2    # I

    const/4 v1, 0x0

    invoke-direct {p0}, Ljcifs/smb/SmbComNtTransaction;-><init>()V

    iput p1, p0, Ljcifs/smb/NtTransQuerySecurityDesc;->fid:I

    iput p2, p0, Ljcifs/smb/NtTransQuerySecurityDesc;->securityInformation:I

    const/16 v0, -0x60

    iput-byte v0, p0, Ljcifs/smb/NtTransQuerySecurityDesc;->command:B

    const/4 v0, 0x6

    iput v0, p0, Ljcifs/smb/NtTransQuerySecurityDesc;->function:I

    iput v1, p0, Ljcifs/smb/NtTransQuerySecurityDesc;->setupCount:I

    iput v1, p0, Ljcifs/smb/NtTransQuerySecurityDesc;->totalDataCount:I

    const/4 v0, 0x4

    iput v0, p0, Ljcifs/smb/NtTransQuerySecurityDesc;->maxParameterCount:I

    const v0, 0x8000

    iput v0, p0, Ljcifs/smb/NtTransQuerySecurityDesc;->maxDataCount:I

    iput-byte v1, p0, Ljcifs/smb/NtTransQuerySecurityDesc;->maxSetupCount:B

    return-void
.end method


# virtual methods
.method readDataWireFormat([BII)I
    .locals 1
    .param p1    # [B
    .param p2    # I
    .param p3    # I

    const/4 v0, 0x0

    return v0
.end method

.method readParametersWireFormat([BII)I
    .locals 1
    .param p1    # [B
    .param p2    # I
    .param p3    # I

    const/4 v0, 0x0

    return v0
.end method

.method readSetupWireFormat([BII)I
    .locals 1
    .param p1    # [B
    .param p2    # I
    .param p3    # I

    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    new-instance v0, Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "NtTransQuerySecurityDesc["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-super {p0}, Ljcifs/smb/SmbComNtTransaction;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",fid=0x"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Ljcifs/smb/NtTransQuerySecurityDesc;->fid:I

    const/4 v3, 0x4

    invoke-static {v2, v3}, Ljcifs/util/Hexdump;->toHexString(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",securityInformation=0x"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Ljcifs/smb/NtTransQuerySecurityDesc;->securityInformation:I

    const/16 v3, 0x8

    invoke-static {v2, v3}, Ljcifs/util/Hexdump;->toHexString(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method writeDataWireFormat([BI)I
    .locals 1
    .param p1    # [B
    .param p2    # I

    const/4 v0, 0x0

    return v0
.end method

.method writeParametersWireFormat([BI)I
    .locals 5
    .param p1    # [B
    .param p2    # I

    const/4 v4, 0x0

    move v1, p2

    iget v2, p0, Ljcifs/smb/NtTransQuerySecurityDesc;->fid:I

    int-to-long v2, v2

    invoke-static {v2, v3, p1, p2}, Ljcifs/smb/NtTransQuerySecurityDesc;->writeInt2(J[BI)V

    add-int/lit8 p2, p2, 0x2

    add-int/lit8 v0, p2, 0x1

    aput-byte v4, p1, p2

    add-int/lit8 p2, v0, 0x1

    aput-byte v4, p1, v0

    iget v2, p0, Ljcifs/smb/NtTransQuerySecurityDesc;->securityInformation:I

    int-to-long v2, v2

    invoke-static {v2, v3, p1, p2}, Ljcifs/smb/NtTransQuerySecurityDesc;->writeInt4(J[BI)V

    add-int/lit8 p2, p2, 0x4

    sub-int v2, p2, v1

    return v2
.end method

.method writeSetupWireFormat([BI)I
    .locals 1
    .param p1    # [B
    .param p2    # I

    const/4 v0, 0x0

    return v0
.end method
