.class Ljcifs/smb/NetServerEnum2Response;
.super Ljcifs/smb/SmbComTransactionResponse;
.source "NetServerEnum2Response.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ljcifs/smb/NetServerEnum2Response$ServerInfo1;
    }
.end annotation


# instance fields
.field private converter:I

.field lastName:Ljava/lang/String;

.field private totalAvailableEntries:I


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljcifs/smb/SmbComTransactionResponse;-><init>()V

    return-void
.end method


# virtual methods
.method readDataWireFormat([BII)I
    .locals 8
    .param p1    # [B
    .param p2    # I
    .param p3    # I

    const/4 v7, 0x0

    move v4, p2

    const/4 v1, 0x0

    iget v5, p0, Ljcifs/smb/NetServerEnum2Response;->numEntries:I

    new-array v5, v5, [Ljcifs/smb/NetServerEnum2Response$ServerInfo1;

    iput-object v5, p0, Ljcifs/smb/NetServerEnum2Response;->results:[Ljcifs/smb/FileEntry;

    const/4 v2, 0x0

    :goto_0
    iget v5, p0, Ljcifs/smb/NetServerEnum2Response;->numEntries:I

    if-ge v2, v5, :cond_1

    iget-object v5, p0, Ljcifs/smb/NetServerEnum2Response;->results:[Ljcifs/smb/FileEntry;

    new-instance v1, Ljcifs/smb/NetServerEnum2Response$ServerInfo1;

    invoke-direct {v1, p0}, Ljcifs/smb/NetServerEnum2Response$ServerInfo1;-><init>(Ljcifs/smb/NetServerEnum2Response;)V

    aput-object v1, v5, v2

    const/16 v5, 0x10

    invoke-virtual {p0, p1, p2, v5, v7}, Ljcifs/smb/NetServerEnum2Response;->readString([BIIZ)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v1, Ljcifs/smb/NetServerEnum2Response$ServerInfo1;->name:Ljava/lang/String;

    add-int/lit8 p2, p2, 0x10

    add-int/lit8 v0, p2, 0x1

    aget-byte v5, p1, p2

    and-int/lit16 v5, v5, 0xff

    iput v5, v1, Ljcifs/smb/NetServerEnum2Response$ServerInfo1;->versionMajor:I

    add-int/lit8 p2, v0, 0x1

    aget-byte v5, p1, v0

    and-int/lit16 v5, v5, 0xff

    iput v5, v1, Ljcifs/smb/NetServerEnum2Response$ServerInfo1;->versionMinor:I

    invoke-static {p1, p2}, Ljcifs/smb/NetServerEnum2Response;->readInt4([BI)I

    move-result v5

    iput v5, v1, Ljcifs/smb/NetServerEnum2Response$ServerInfo1;->type:I

    add-int/lit8 p2, p2, 0x4

    invoke-static {p1, p2}, Ljcifs/smb/NetServerEnum2Response;->readInt4([BI)I

    move-result v3

    add-int/lit8 p2, p2, 0x4

    const v5, 0xffff

    and-int/2addr v5, v3

    iget v6, p0, Ljcifs/smb/NetServerEnum2Response;->converter:I

    sub-int v3, v5, v6

    add-int/2addr v3, v4

    const/16 v5, 0x30

    invoke-virtual {p0, p1, v3, v5, v7}, Ljcifs/smb/NetServerEnum2Response;->readString([BIIZ)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v1, Ljcifs/smb/NetServerEnum2Response$ServerInfo1;->commentOrMasterBrowser:Ljava/lang/String;

    sget-object v5, Ljcifs/smb/NetServerEnum2Response;->log:Ljcifs/util/LogStream;

    sget v5, Ljcifs/util/LogStream;->level:I

    const/4 v6, 0x4

    if-lt v5, v6, :cond_0

    sget-object v5, Ljcifs/smb/NetServerEnum2Response;->log:Ljcifs/util/LogStream;

    invoke-virtual {v5, v1}, Ljcifs/util/LogStream;->println(Ljava/lang/Object;)V

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    iget v5, p0, Ljcifs/smb/NetServerEnum2Response;->numEntries:I

    if-nez v5, :cond_2

    const/4 v5, 0x0

    :goto_1
    iput-object v5, p0, Ljcifs/smb/NetServerEnum2Response;->lastName:Ljava/lang/String;

    sub-int v5, p2, v4

    return v5

    :cond_2
    iget-object v5, v1, Ljcifs/smb/NetServerEnum2Response$ServerInfo1;->name:Ljava/lang/String;

    goto :goto_1
.end method

.method readParametersWireFormat([BII)I
    .locals 2
    .param p1    # [B
    .param p2    # I
    .param p3    # I

    move v0, p2

    invoke-static {p1, p2}, Ljcifs/smb/NetServerEnum2Response;->readInt2([BI)I

    move-result v1

    iput v1, p0, Ljcifs/smb/NetServerEnum2Response;->status:I

    add-int/lit8 p2, p2, 0x2

    invoke-static {p1, p2}, Ljcifs/smb/NetServerEnum2Response;->readInt2([BI)I

    move-result v1

    iput v1, p0, Ljcifs/smb/NetServerEnum2Response;->converter:I

    add-int/lit8 p2, p2, 0x2

    invoke-static {p1, p2}, Ljcifs/smb/NetServerEnum2Response;->readInt2([BI)I

    move-result v1

    iput v1, p0, Ljcifs/smb/NetServerEnum2Response;->numEntries:I

    add-int/lit8 p2, p2, 0x2

    invoke-static {p1, p2}, Ljcifs/smb/NetServerEnum2Response;->readInt2([BI)I

    move-result v1

    iput v1, p0, Ljcifs/smb/NetServerEnum2Response;->totalAvailableEntries:I

    add-int/lit8 p2, p2, 0x2

    sub-int v1, p2, v0

    return v1
.end method

.method readSetupWireFormat([BII)I
    .locals 1
    .param p1    # [B
    .param p2    # I
    .param p3    # I

    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "NetServerEnum2Response["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-super {p0}, Ljcifs/smb/SmbComTransactionResponse;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",status="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Ljcifs/smb/NetServerEnum2Response;->status:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",converter="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Ljcifs/smb/NetServerEnum2Response;->converter:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",entriesReturned="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Ljcifs/smb/NetServerEnum2Response;->numEntries:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",totalAvailableEntries="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Ljcifs/smb/NetServerEnum2Response;->totalAvailableEntries:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",lastName="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Ljcifs/smb/NetServerEnum2Response;->lastName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method writeDataWireFormat([BI)I
    .locals 1
    .param p1    # [B
    .param p2    # I

    const/4 v0, 0x0

    return v0
.end method

.method writeParametersWireFormat([BI)I
    .locals 1
    .param p1    # [B
    .param p2    # I

    const/4 v0, 0x0

    return v0
.end method

.method writeSetupWireFormat([BI)I
    .locals 1
    .param p1    # [B
    .param p2    # I

    const/4 v0, 0x0

    return v0
.end method
