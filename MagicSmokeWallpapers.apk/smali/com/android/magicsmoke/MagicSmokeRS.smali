.class Lcom/android/magicsmoke/MagicSmokeRS;
.super Lcom/android/magicsmoke/RenderScriptScene;
.source "MagicSmokeRS.java"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/magicsmoke/MagicSmokeRS$Preset;,
        Lcom/android/magicsmoke/MagicSmokeRS$WorldState;
    }
.end annotation


# static fields
.field public static final mPreset:[Lcom/android/magicsmoke/MagicSmokeRS$Preset;


# instance fields
.field alphafactor:F

.field private mContext:Landroid/content/Context;

.field private mFSConst:Lcom/android/magicsmoke/ScriptField_FragmentShaderConstants_s;

.field private mPF4tex:Landroid/renderscript/ProgramFragment;

.field private mPF5tex:Landroid/renderscript/ProgramFragment;

.field private mPStore:Landroid/renderscript/ProgramStore;

.field private mPV4tex:Landroid/renderscript/ProgramVertex;

.field private mPV5tex:Landroid/renderscript/ProgramVertex;

.field private mPVAlloc:Landroid/renderscript/ProgramVertexFixedFunction$Constants;

.field private mRealTextures:[Landroid/renderscript/Allocation;

.field private mSampler:[Landroid/renderscript/Sampler;

.field private mScript:Lcom/android/magicsmoke/ScriptC_clouds;

.field private mSharedPref:Landroid/content/SharedPreferences;

.field private mSourceTextures:[Landroid/renderscript/Allocation;

.field mTextureType:Landroid/renderscript/Type;

.field private mTouchY:F

.field private mVSConst:Lcom/android/magicsmoke/ScriptField_VertexShaderConstants_s;

.field mWorldState:Lcom/android/magicsmoke/MagicSmokeRS$WorldState;


# direct methods
.method static constructor <clinit>()V
    .locals 12

    const/16 v0, 0x14

    new-array v10, v0, [Lcom/android/magicsmoke/MagicSmokeRS$Preset;

    const/4 v11, 0x0

    new-instance v0, Lcom/android/magicsmoke/MagicSmokeRS$Preset;

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const v4, 0xffffff

    const/high16 v5, 0x40000000    # 2.0f

    const/16 v6, 0xf

    const/4 v7, 0x1

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/android/magicsmoke/MagicSmokeRS$Preset;-><init>(IIIIFIZZZ)V

    aput-object v0, v10, v11

    const/4 v11, 0x1

    new-instance v0, Lcom/android/magicsmoke/MagicSmokeRS$Preset;

    const/4 v1, 0x1

    const/16 v2, 0xff

    const/4 v3, 0x0

    const v4, 0xffffff

    const/high16 v5, 0x40000000    # 2.0f

    const/16 v6, 0xf

    const/4 v7, 0x1

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/android/magicsmoke/MagicSmokeRS$Preset;-><init>(IIIIFIZZZ)V

    aput-object v0, v10, v11

    const/4 v11, 0x2

    new-instance v0, Lcom/android/magicsmoke/MagicSmokeRS$Preset;

    const/4 v1, 0x1

    const v2, 0xff00

    const/4 v3, 0x0

    const v4, 0xffffff

    const/high16 v5, 0x40000000    # 2.0f

    const/16 v6, 0xf

    const/4 v7, 0x1

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/android/magicsmoke/MagicSmokeRS$Preset;-><init>(IIIIFIZZZ)V

    aput-object v0, v10, v11

    const/4 v11, 0x3

    new-instance v0, Lcom/android/magicsmoke/MagicSmokeRS$Preset;

    const/4 v1, 0x1

    const v2, 0xff00

    const/4 v3, 0x0

    const v4, 0xffffff

    const/high16 v5, 0x40000000    # 2.0f

    const/16 v6, 0xf

    const/4 v7, 0x1

    const/4 v8, 0x0

    const/4 v9, 0x1

    invoke-direct/range {v0 .. v9}, Lcom/android/magicsmoke/MagicSmokeRS$Preset;-><init>(IIIIFIZZZ)V

    aput-object v0, v10, v11

    const/4 v11, 0x4

    new-instance v0, Lcom/android/magicsmoke/MagicSmokeRS$Preset;

    const/4 v1, 0x1

    const v2, 0xff00

    const v3, 0xff00

    const v4, 0xffffff

    const/high16 v5, 0x40200000    # 2.5f

    const/16 v6, 0x1f

    const/4 v7, 0x1

    const/4 v8, 0x1

    const/4 v9, 0x1

    invoke-direct/range {v0 .. v9}, Lcom/android/magicsmoke/MagicSmokeRS$Preset;-><init>(IIIIFIZZZ)V

    aput-object v0, v10, v11

    const/4 v11, 0x5

    new-instance v0, Lcom/android/magicsmoke/MagicSmokeRS$Preset;

    const/4 v1, 0x1

    const/high16 v2, 0x800000

    const/high16 v3, 0xff0000

    const v4, 0xffffff

    const/high16 v5, 0x40200000    # 2.5f

    const/16 v6, 0x1f

    const/4 v7, 0x1

    const/4 v8, 0x1

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/android/magicsmoke/MagicSmokeRS$Preset;-><init>(IIIIFIZZZ)V

    aput-object v0, v10, v11

    const/4 v11, 0x6

    new-instance v0, Lcom/android/magicsmoke/MagicSmokeRS$Preset;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const v4, 0xffffff

    const/4 v5, 0x0

    const/16 v6, 0x1f

    const/4 v7, 0x1

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/android/magicsmoke/MagicSmokeRS$Preset;-><init>(IIIIFIZZZ)V

    aput-object v0, v10, v11

    const/4 v11, 0x7

    new-instance v0, Lcom/android/magicsmoke/MagicSmokeRS$Preset;

    const/4 v1, 0x1

    const/16 v2, 0xff

    const v3, 0xff00

    const v4, 0xffff00

    const/high16 v5, 0x40000000    # 2.0f

    const/16 v6, 0x1f

    const/4 v7, 0x1

    const/4 v8, 0x1

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/android/magicsmoke/MagicSmokeRS$Preset;-><init>(IIIIFIZZZ)V

    aput-object v0, v10, v11

    const/16 v11, 0x8

    new-instance v0, Lcom/android/magicsmoke/MagicSmokeRS$Preset;

    const/4 v1, 0x1

    const v2, 0x8000

    const v3, 0xff00

    const v4, 0xffffff

    const/high16 v5, 0x40200000    # 2.5f

    const/16 v6, 0x1f

    const/4 v7, 0x1

    const/4 v8, 0x1

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/android/magicsmoke/MagicSmokeRS$Preset;-><init>(IIIIFIZZZ)V

    aput-object v0, v10, v11

    const/16 v11, 0x9

    new-instance v0, Lcom/android/magicsmoke/MagicSmokeRS$Preset;

    const/4 v1, 0x1

    const/high16 v2, 0x800000

    const/high16 v3, 0xff0000

    const v4, 0xffffff

    const/high16 v5, 0x40200000    # 2.5f

    const/16 v6, 0x1f

    const/4 v7, 0x1

    const/4 v8, 0x1

    const/4 v9, 0x1

    invoke-direct/range {v0 .. v9}, Lcom/android/magicsmoke/MagicSmokeRS$Preset;-><init>(IIIIFIZZZ)V

    aput-object v0, v10, v11

    const/16 v11, 0xa

    new-instance v0, Lcom/android/magicsmoke/MagicSmokeRS$Preset;

    const/4 v1, 0x1

    const v2, 0x808080

    const/4 v3, 0x0

    const v4, 0xffffff

    const/high16 v5, 0x40000000    # 2.0f

    const/16 v6, 0xf

    const/4 v7, 0x1

    const/4 v8, 0x0

    const/4 v9, 0x1

    invoke-direct/range {v0 .. v9}, Lcom/android/magicsmoke/MagicSmokeRS$Preset;-><init>(IIIIFIZZZ)V

    aput-object v0, v10, v11

    const/16 v11, 0xb

    new-instance v0, Lcom/android/magicsmoke/MagicSmokeRS$Preset;

    const/4 v1, 0x1

    const/16 v2, 0xff

    const/4 v3, 0x0

    const v4, 0xffffff

    const/high16 v5, 0x40000000    # 2.0f

    const/16 v6, 0xf

    const/4 v7, 0x1

    const/4 v8, 0x0

    const/4 v9, 0x1

    invoke-direct/range {v0 .. v9}, Lcom/android/magicsmoke/MagicSmokeRS$Preset;-><init>(IIIIFIZZZ)V

    aput-object v0, v10, v11

    const/16 v11, 0xc

    new-instance v0, Lcom/android/magicsmoke/MagicSmokeRS$Preset;

    const/4 v1, 0x1

    const/16 v2, 0xff

    const v3, 0xff00

    const v4, 0xffff00

    const/high16 v5, 0x3fc00000    # 1.5f

    const/16 v6, 0x1f

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x1

    invoke-direct/range {v0 .. v9}, Lcom/android/magicsmoke/MagicSmokeRS$Preset;-><init>(IIIIFIZZZ)V

    aput-object v0, v10, v11

    const/16 v11, 0xd

    new-instance v0, Lcom/android/magicsmoke/MagicSmokeRS$Preset;

    const/4 v1, 0x1

    const/16 v2, 0xff

    const v3, 0xff00

    const v4, 0xffff00

    const/high16 v5, 0x40000000    # 2.0f

    const/16 v6, 0x1f

    const/4 v7, 0x1

    const/4 v8, 0x1

    const/4 v9, 0x1

    invoke-direct/range {v0 .. v9}, Lcom/android/magicsmoke/MagicSmokeRS$Preset;-><init>(IIIIFIZZZ)V

    aput-object v0, v10, v11

    const/16 v11, 0xe

    new-instance v0, Lcom/android/magicsmoke/MagicSmokeRS$Preset;

    const/4 v1, 0x1

    const/16 v2, 0xff

    const v3, 0xff00

    const v4, 0xffff00

    const/high16 v5, 0x3fc00000    # 1.5f

    const/16 v6, 0x1f

    const/4 v7, 0x1

    const/4 v8, 0x1

    const/4 v9, 0x1

    invoke-direct/range {v0 .. v9}, Lcom/android/magicsmoke/MagicSmokeRS$Preset;-><init>(IIIIFIZZZ)V

    aput-object v0, v10, v11

    const/16 v11, 0xf

    new-instance v0, Lcom/android/magicsmoke/MagicSmokeRS$Preset;

    const/4 v1, 0x1

    const v2, 0x808080

    const/4 v3, 0x0

    const v4, 0xffffff

    const/high16 v5, 0x40000000    # 2.0f

    const/16 v6, 0xf

    const/4 v7, 0x1

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/android/magicsmoke/MagicSmokeRS$Preset;-><init>(IIIIFIZZZ)V

    aput-object v0, v10, v11

    const/16 v11, 0x10

    new-instance v0, Lcom/android/magicsmoke/MagicSmokeRS$Preset;

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const v4, 0xffffff

    const/high16 v5, 0x40000000    # 2.0f

    const/16 v6, 0xf

    const/4 v7, 0x1

    const/4 v8, 0x1

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/android/magicsmoke/MagicSmokeRS$Preset;-><init>(IIIIFIZZZ)V

    aput-object v0, v10, v11

    const/16 v11, 0x11

    new-instance v0, Lcom/android/magicsmoke/MagicSmokeRS$Preset;

    const/4 v1, 0x2

    const/4 v2, 0x0

    const/16 v3, 0x70

    const v4, 0xff2020

    const/high16 v5, 0x40200000    # 2.5f

    const/16 v6, 0x1f

    const/4 v7, 0x1

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/android/magicsmoke/MagicSmokeRS$Preset;-><init>(IIIIFIZZZ)V

    aput-object v0, v10, v11

    const/16 v11, 0x12

    new-instance v0, Lcom/android/magicsmoke/MagicSmokeRS$Preset;

    const/4 v1, 0x2

    const v2, 0x6060ff

    const/16 v3, 0x70

    const v4, 0xffffff

    const/high16 v5, 0x40200000    # 2.5f

    const/16 v6, 0x1f

    const/4 v7, 0x1

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/android/magicsmoke/MagicSmokeRS$Preset;-><init>(IIIIFIZZZ)V

    aput-object v0, v10, v11

    const/16 v11, 0x13

    new-instance v0, Lcom/android/magicsmoke/MagicSmokeRS$Preset;

    const/4 v1, 0x3

    const/16 v2, 0xf0

    const/4 v3, 0x0

    const v4, 0xffffff

    const/high16 v5, 0x40000000    # 2.0f

    const/16 v6, 0xf

    const/4 v7, 0x1

    const/4 v8, 0x1

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/android/magicsmoke/MagicSmokeRS$Preset;-><init>(IIIIFIZZZ)V

    aput-object v0, v10, v11

    sput-object v10, Lcom/android/magicsmoke/MagicSmokeRS;->mPreset:[Lcom/android/magicsmoke/MagicSmokeRS$Preset;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;II)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # I
    .param p3    # I

    invoke-direct {p0, p2, p3}, Lcom/android/magicsmoke/RenderScriptScene;-><init>(II)V

    new-instance v0, Lcom/android/magicsmoke/MagicSmokeRS$WorldState;

    invoke-direct {v0}, Lcom/android/magicsmoke/MagicSmokeRS$WorldState;-><init>()V

    iput-object v0, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mWorldState:Lcom/android/magicsmoke/MagicSmokeRS$WorldState;

    iput p2, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mWidth:I

    iput p3, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mHeight:I

    iput-object p1, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mContext:Landroid/content/Context;

    iget-object v0, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mContext:Landroid/content/Context;

    const-string v1, "magicsmoke"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mSharedPref:Landroid/content/SharedPreferences;

    iget-object v0, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mSharedPref:Landroid/content/SharedPreferences;

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    invoke-virtual {p0}, Lcom/android/magicsmoke/MagicSmokeRS;->makeNewState()V

    return-void
.end method


# virtual methods
.method protected createScript()Landroid/renderscript/ScriptC;
    .locals 14

    const/16 v13, 0x100

    const/4 v12, 0x4

    const/4 v11, 0x1

    const/4 v10, 0x0

    const/4 v9, 0x5

    new-instance v5, Lcom/android/magicsmoke/ScriptC_clouds;

    iget-object v6, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mRS:Landroid/renderscript/RenderScriptGL;

    iget-object v7, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mResources:Landroid/content/res/Resources;

    const/high16 v8, 0x7f050000    # com.android.magicsmoke.R.raw.clouds

    invoke-direct {v5, v6, v7, v8}, Lcom/android/magicsmoke/ScriptC_clouds;-><init>(Landroid/renderscript/RenderScript;Landroid/content/res/Resources;I)V

    iput-object v5, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mScript:Lcom/android/magicsmoke/ScriptC_clouds;

    new-instance v5, Lcom/android/magicsmoke/ScriptField_VertexShaderConstants_s;

    iget-object v6, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-direct {v5, v6, v11}, Lcom/android/magicsmoke/ScriptField_VertexShaderConstants_s;-><init>(Landroid/renderscript/RenderScript;I)V

    iput-object v5, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mVSConst:Lcom/android/magicsmoke/ScriptField_VertexShaderConstants_s;

    iget-object v5, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mScript:Lcom/android/magicsmoke/ScriptC_clouds;

    iget-object v6, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mVSConst:Lcom/android/magicsmoke/ScriptField_VertexShaderConstants_s;

    invoke-virtual {v5, v6}, Lcom/android/magicsmoke/ScriptC_clouds;->bind_gVSConstants(Lcom/android/magicsmoke/ScriptField_VertexShaderConstants_s;)V

    new-instance v0, Landroid/renderscript/ProgramVertex$Builder;

    iget-object v5, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-direct {v0, v5}, Landroid/renderscript/ProgramVertex$Builder;-><init>(Landroid/renderscript/RenderScript;)V

    iget-object v5, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mResources:Landroid/content/res/Resources;

    const v6, 0x7f050004    # com.android.magicsmoke.R.raw.pv5tex

    invoke-virtual {v0, v5, v6}, Landroid/renderscript/ProgramVertex$Builder;->setShader(Landroid/content/res/Resources;I)Landroid/renderscript/Program$BaseProgramBuilder;

    iget-object v5, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mVSConst:Lcom/android/magicsmoke/ScriptField_VertexShaderConstants_s;

    invoke-virtual {v5}, Lcom/android/magicsmoke/ScriptField_VertexShaderConstants_s;->getAllocation()Landroid/renderscript/Allocation;

    move-result-object v5

    invoke-virtual {v5}, Landroid/renderscript/Allocation;->getType()Landroid/renderscript/Type;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/renderscript/ProgramVertex$Builder;->addConstant(Landroid/renderscript/Type;)Landroid/renderscript/Program$BaseProgramBuilder;

    iget-object v5, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-static {v5}, Lcom/android/magicsmoke/ScriptField_VertexInputs_s;->createElement(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/renderscript/ProgramVertex$Builder;->addInput(Landroid/renderscript/Element;)Landroid/renderscript/ProgramVertex$Builder;

    invoke-virtual {v0}, Landroid/renderscript/ProgramVertex$Builder;->create()Landroid/renderscript/ProgramVertex;

    move-result-object v5

    iput-object v5, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mPV5tex:Landroid/renderscript/ProgramVertex;

    iget-object v5, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mPV5tex:Landroid/renderscript/ProgramVertex;

    iget-object v6, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mVSConst:Lcom/android/magicsmoke/ScriptField_VertexShaderConstants_s;

    invoke-virtual {v6}, Lcom/android/magicsmoke/ScriptField_VertexShaderConstants_s;->getAllocation()Landroid/renderscript/Allocation;

    move-result-object v6

    invoke-virtual {v5, v6, v10}, Landroid/renderscript/ProgramVertex;->bindConstants(Landroid/renderscript/Allocation;I)V

    iget-object v5, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mResources:Landroid/content/res/Resources;

    const v6, 0x7f050003    # com.android.magicsmoke.R.raw.pv4tex

    invoke-virtual {v0, v5, v6}, Landroid/renderscript/ProgramVertex$Builder;->setShader(Landroid/content/res/Resources;I)Landroid/renderscript/Program$BaseProgramBuilder;

    invoke-virtual {v0}, Landroid/renderscript/ProgramVertex$Builder;->create()Landroid/renderscript/ProgramVertex;

    move-result-object v5

    iput-object v5, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mPV4tex:Landroid/renderscript/ProgramVertex;

    iget-object v5, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mPV4tex:Landroid/renderscript/ProgramVertex;

    iget-object v6, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mVSConst:Lcom/android/magicsmoke/ScriptField_VertexShaderConstants_s;

    invoke-virtual {v6}, Lcom/android/magicsmoke/ScriptField_VertexShaderConstants_s;->getAllocation()Landroid/renderscript/Allocation;

    move-result-object v6

    invoke-virtual {v5, v6, v10}, Landroid/renderscript/ProgramVertex;->bindConstants(Landroid/renderscript/Allocation;I)V

    iget-object v5, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mScript:Lcom/android/magicsmoke/ScriptC_clouds;

    iget-object v6, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mPV5tex:Landroid/renderscript/ProgramVertex;

    invoke-virtual {v5, v6}, Lcom/android/magicsmoke/ScriptC_clouds;->set_gPV5tex(Landroid/renderscript/ProgramVertex;)V

    iget-object v5, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mScript:Lcom/android/magicsmoke/ScriptC_clouds;

    iget-object v6, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mPV4tex:Landroid/renderscript/ProgramVertex;

    invoke-virtual {v5, v6}, Lcom/android/magicsmoke/ScriptC_clouds;->set_gPV4tex(Landroid/renderscript/ProgramVertex;)V

    new-array v5, v9, [Landroid/renderscript/Allocation;

    iput-object v5, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mSourceTextures:[Landroid/renderscript/Allocation;

    new-array v5, v9, [Landroid/renderscript/Allocation;

    iput-object v5, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mRealTextures:[Landroid/renderscript/Allocation;

    new-instance v3, Landroid/renderscript/Type$Builder;

    iget-object v5, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mRS:Landroid/renderscript/RenderScriptGL;

    iget-object v6, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-static {v6}, Landroid/renderscript/Element;->RGBA_8888(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v6

    invoke-direct {v3, v5, v6}, Landroid/renderscript/Type$Builder;-><init>(Landroid/renderscript/RenderScript;Landroid/renderscript/Element;)V

    invoke-virtual {v3, v13}, Landroid/renderscript/Type$Builder;->setX(I)Landroid/renderscript/Type$Builder;

    invoke-virtual {v3, v13}, Landroid/renderscript/Type$Builder;->setY(I)Landroid/renderscript/Type$Builder;

    invoke-virtual {v3}, Landroid/renderscript/Type$Builder;->create()Landroid/renderscript/Type;

    move-result-object v5

    iput-object v5, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mTextureType:Landroid/renderscript/Type;

    invoke-virtual {p0}, Lcom/android/magicsmoke/MagicSmokeRS;->loadBitmaps()V

    new-instance v2, Landroid/renderscript/Sampler$Builder;

    iget-object v5, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-direct {v2, v5}, Landroid/renderscript/Sampler$Builder;-><init>(Landroid/renderscript/RenderScript;)V

    sget-object v5, Landroid/renderscript/Sampler$Value;->LINEAR:Landroid/renderscript/Sampler$Value;

    invoke-virtual {v2, v5}, Landroid/renderscript/Sampler$Builder;->setMinification(Landroid/renderscript/Sampler$Value;)V

    sget-object v5, Landroid/renderscript/Sampler$Value;->LINEAR:Landroid/renderscript/Sampler$Value;

    invoke-virtual {v2, v5}, Landroid/renderscript/Sampler$Builder;->setMagnification(Landroid/renderscript/Sampler$Value;)V

    sget-object v5, Landroid/renderscript/Sampler$Value;->WRAP:Landroid/renderscript/Sampler$Value;

    invoke-virtual {v2, v5}, Landroid/renderscript/Sampler$Builder;->setWrapS(Landroid/renderscript/Sampler$Value;)V

    sget-object v5, Landroid/renderscript/Sampler$Value;->WRAP:Landroid/renderscript/Sampler$Value;

    invoke-virtual {v2, v5}, Landroid/renderscript/Sampler$Builder;->setWrapT(Landroid/renderscript/Sampler$Value;)V

    new-array v5, v9, [Landroid/renderscript/Sampler;

    iput-object v5, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mSampler:[Landroid/renderscript/Sampler;

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v9, :cond_0

    iget-object v5, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mSampler:[Landroid/renderscript/Sampler;

    invoke-virtual {v2}, Landroid/renderscript/Sampler$Builder;->create()Landroid/renderscript/Sampler;

    move-result-object v6

    aput-object v6, v5, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    new-instance v5, Lcom/android/magicsmoke/ScriptField_FragmentShaderConstants_s;

    iget-object v6, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-direct {v5, v6, v11}, Lcom/android/magicsmoke/ScriptField_FragmentShaderConstants_s;-><init>(Landroid/renderscript/RenderScript;I)V

    iput-object v5, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mFSConst:Lcom/android/magicsmoke/ScriptField_FragmentShaderConstants_s;

    iget-object v5, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mScript:Lcom/android/magicsmoke/ScriptC_clouds;

    iget-object v6, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mFSConst:Lcom/android/magicsmoke/ScriptField_FragmentShaderConstants_s;

    invoke-virtual {v5, v6}, Lcom/android/magicsmoke/ScriptC_clouds;->bind_gFSConstants(Lcom/android/magicsmoke/ScriptField_FragmentShaderConstants_s;)V

    new-instance v0, Landroid/renderscript/ProgramFragment$Builder;

    iget-object v5, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-direct {v0, v5}, Landroid/renderscript/ProgramFragment$Builder;-><init>(Landroid/renderscript/RenderScript;)V

    iget-object v5, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mResources:Landroid/content/res/Resources;

    const v6, 0x7f050002    # com.android.magicsmoke.R.raw.pf5tex

    invoke-virtual {v0, v5, v6}, Landroid/renderscript/ProgramFragment$Builder;->setShader(Landroid/content/res/Resources;I)Landroid/renderscript/Program$BaseProgramBuilder;

    const/4 v4, 0x0

    :goto_1
    if-ge v4, v9, :cond_1

    sget-object v5, Landroid/renderscript/Program$TextureType;->TEXTURE_2D:Landroid/renderscript/Program$TextureType;

    invoke-virtual {v0, v5}, Landroid/renderscript/ProgramFragment$Builder;->addTexture(Landroid/renderscript/Program$TextureType;)Landroid/renderscript/Program$BaseProgramBuilder;

    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_1
    iget-object v5, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mFSConst:Lcom/android/magicsmoke/ScriptField_FragmentShaderConstants_s;

    invoke-virtual {v5}, Lcom/android/magicsmoke/ScriptField_FragmentShaderConstants_s;->getAllocation()Landroid/renderscript/Allocation;

    move-result-object v5

    invoke-virtual {v5}, Landroid/renderscript/Allocation;->getType()Landroid/renderscript/Type;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/renderscript/ProgramFragment$Builder;->addConstant(Landroid/renderscript/Type;)Landroid/renderscript/Program$BaseProgramBuilder;

    invoke-virtual {v0}, Landroid/renderscript/ProgramFragment$Builder;->create()Landroid/renderscript/ProgramFragment;

    move-result-object v5

    iput-object v5, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mPF5tex:Landroid/renderscript/ProgramFragment;

    const/4 v1, 0x0

    :goto_2
    if-ge v1, v9, :cond_2

    iget-object v5, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mPF5tex:Landroid/renderscript/ProgramFragment;

    iget-object v6, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mSampler:[Landroid/renderscript/Sampler;

    aget-object v6, v6, v1

    invoke-virtual {v5, v6, v1}, Landroid/renderscript/ProgramFragment;->bindSampler(Landroid/renderscript/Sampler;I)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_2
    iget-object v5, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mPF5tex:Landroid/renderscript/ProgramFragment;

    iget-object v6, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mFSConst:Lcom/android/magicsmoke/ScriptField_FragmentShaderConstants_s;

    invoke-virtual {v6}, Lcom/android/magicsmoke/ScriptField_FragmentShaderConstants_s;->getAllocation()Landroid/renderscript/Allocation;

    move-result-object v6

    invoke-virtual {v5, v6, v10}, Landroid/renderscript/ProgramFragment;->bindConstants(Landroid/renderscript/Allocation;I)V

    new-instance v0, Landroid/renderscript/ProgramFragment$Builder;

    iget-object v5, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-direct {v0, v5}, Landroid/renderscript/ProgramFragment$Builder;-><init>(Landroid/renderscript/RenderScript;)V

    iget-object v5, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mResources:Landroid/content/res/Resources;

    const v6, 0x7f050001    # com.android.magicsmoke.R.raw.pf4tex

    invoke-virtual {v0, v5, v6}, Landroid/renderscript/ProgramFragment$Builder;->setShader(Landroid/content/res/Resources;I)Landroid/renderscript/Program$BaseProgramBuilder;

    const/4 v4, 0x0

    :goto_3
    if-ge v4, v12, :cond_3

    sget-object v5, Landroid/renderscript/Program$TextureType;->TEXTURE_2D:Landroid/renderscript/Program$TextureType;

    invoke-virtual {v0, v5}, Landroid/renderscript/ProgramFragment$Builder;->addTexture(Landroid/renderscript/Program$TextureType;)Landroid/renderscript/Program$BaseProgramBuilder;

    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    :cond_3
    iget-object v5, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mFSConst:Lcom/android/magicsmoke/ScriptField_FragmentShaderConstants_s;

    invoke-virtual {v5}, Lcom/android/magicsmoke/ScriptField_FragmentShaderConstants_s;->getAllocation()Landroid/renderscript/Allocation;

    move-result-object v5

    invoke-virtual {v5}, Landroid/renderscript/Allocation;->getType()Landroid/renderscript/Type;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/renderscript/ProgramFragment$Builder;->addConstant(Landroid/renderscript/Type;)Landroid/renderscript/Program$BaseProgramBuilder;

    invoke-virtual {v0}, Landroid/renderscript/ProgramFragment$Builder;->create()Landroid/renderscript/ProgramFragment;

    move-result-object v5

    iput-object v5, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mPF4tex:Landroid/renderscript/ProgramFragment;

    const/4 v1, 0x0

    :goto_4
    if-ge v1, v12, :cond_4

    iget-object v5, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mPF4tex:Landroid/renderscript/ProgramFragment;

    iget-object v6, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mSampler:[Landroid/renderscript/Sampler;

    aget-object v6, v6, v1

    invoke-virtual {v5, v6, v1}, Landroid/renderscript/ProgramFragment;->bindSampler(Landroid/renderscript/Sampler;I)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_4
    iget-object v5, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mPF4tex:Landroid/renderscript/ProgramFragment;

    iget-object v6, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mFSConst:Lcom/android/magicsmoke/ScriptField_FragmentShaderConstants_s;

    invoke-virtual {v6}, Lcom/android/magicsmoke/ScriptField_FragmentShaderConstants_s;->getAllocation()Landroid/renderscript/Allocation;

    move-result-object v6

    invoke-virtual {v5, v6, v10}, Landroid/renderscript/ProgramFragment;->bindConstants(Landroid/renderscript/Allocation;I)V

    iget-object v5, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mScript:Lcom/android/magicsmoke/ScriptC_clouds;

    iget-object v6, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mPF5tex:Landroid/renderscript/ProgramFragment;

    invoke-virtual {v5, v6}, Lcom/android/magicsmoke/ScriptC_clouds;->set_gPF5tex(Landroid/renderscript/ProgramFragment;)V

    iget-object v5, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mScript:Lcom/android/magicsmoke/ScriptC_clouds;

    iget-object v6, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mPF4tex:Landroid/renderscript/ProgramFragment;

    invoke-virtual {v5, v6}, Lcom/android/magicsmoke/ScriptC_clouds;->set_gPF4tex(Landroid/renderscript/ProgramFragment;)V

    new-instance v0, Landroid/renderscript/ProgramStore$Builder;

    iget-object v5, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-direct {v0, v5}, Landroid/renderscript/ProgramStore$Builder;-><init>(Landroid/renderscript/RenderScript;)V

    sget-object v5, Landroid/renderscript/ProgramStore$DepthFunc;->ALWAYS:Landroid/renderscript/ProgramStore$DepthFunc;

    invoke-virtual {v0, v5}, Landroid/renderscript/ProgramStore$Builder;->setDepthFunc(Landroid/renderscript/ProgramStore$DepthFunc;)Landroid/renderscript/ProgramStore$Builder;

    sget-object v5, Landroid/renderscript/ProgramStore$BlendSrcFunc;->ONE:Landroid/renderscript/ProgramStore$BlendSrcFunc;

    sget-object v6, Landroid/renderscript/ProgramStore$BlendDstFunc;->ZERO:Landroid/renderscript/ProgramStore$BlendDstFunc;

    invoke-virtual {v0, v5, v6}, Landroid/renderscript/ProgramStore$Builder;->setBlendFunc(Landroid/renderscript/ProgramStore$BlendSrcFunc;Landroid/renderscript/ProgramStore$BlendDstFunc;)Landroid/renderscript/ProgramStore$Builder;

    invoke-virtual {v0, v11}, Landroid/renderscript/ProgramStore$Builder;->setDitherEnabled(Z)Landroid/renderscript/ProgramStore$Builder;

    invoke-virtual {v0, v10}, Landroid/renderscript/ProgramStore$Builder;->setDepthMaskEnabled(Z)Landroid/renderscript/ProgramStore$Builder;

    invoke-virtual {v0}, Landroid/renderscript/ProgramStore$Builder;->create()Landroid/renderscript/ProgramStore;

    move-result-object v5

    iput-object v5, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mPStore:Landroid/renderscript/ProgramStore;

    iget-object v5, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mScript:Lcom/android/magicsmoke/ScriptC_clouds;

    iget-object v6, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mPStore:Landroid/renderscript/ProgramStore;

    invoke-virtual {v5, v6}, Lcom/android/magicsmoke/ScriptC_clouds;->set_gPStore(Landroid/renderscript/ProgramStore;)V

    iget-object v5, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mScript:Lcom/android/magicsmoke/ScriptC_clouds;

    iget-object v6, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mWorldState:Lcom/android/magicsmoke/MagicSmokeRS$WorldState;

    iget v6, v6, Lcom/android/magicsmoke/MagicSmokeRS$WorldState;->mPreset:I

    invoke-virtual {v5, v6}, Lcom/android/magicsmoke/ScriptC_clouds;->set_gPreset(I)V

    iget-object v5, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mScript:Lcom/android/magicsmoke/ScriptC_clouds;

    iget-object v6, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mWorldState:Lcom/android/magicsmoke/MagicSmokeRS$WorldState;

    iget v6, v6, Lcom/android/magicsmoke/MagicSmokeRS$WorldState;->mTextureMask:I

    invoke-virtual {v5, v6}, Lcom/android/magicsmoke/ScriptC_clouds;->set_gTextureMask(I)V

    iget-object v5, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mScript:Lcom/android/magicsmoke/ScriptC_clouds;

    iget-object v6, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mWorldState:Lcom/android/magicsmoke/MagicSmokeRS$WorldState;

    iget v6, v6, Lcom/android/magicsmoke/MagicSmokeRS$WorldState;->mRotate:I

    invoke-virtual {v5, v6}, Lcom/android/magicsmoke/ScriptC_clouds;->set_gRotate(I)V

    iget-object v5, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mScript:Lcom/android/magicsmoke/ScriptC_clouds;

    iget-object v6, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mWorldState:Lcom/android/magicsmoke/MagicSmokeRS$WorldState;

    iget v6, v6, Lcom/android/magicsmoke/MagicSmokeRS$WorldState;->mTextureSwap:I

    invoke-virtual {v5, v6}, Lcom/android/magicsmoke/ScriptC_clouds;->set_gTextureSwap(I)V

    iget-object v5, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mScript:Lcom/android/magicsmoke/ScriptC_clouds;

    iget-object v6, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mWorldState:Lcom/android/magicsmoke/MagicSmokeRS$WorldState;

    iget v6, v6, Lcom/android/magicsmoke/MagicSmokeRS$WorldState;->mProcessTextureMode:I

    invoke-virtual {v5, v6}, Lcom/android/magicsmoke/ScriptC_clouds;->set_gProcessTextureMode(I)V

    iget-object v5, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mScript:Lcom/android/magicsmoke/ScriptC_clouds;

    iget-object v6, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mWorldState:Lcom/android/magicsmoke/MagicSmokeRS$WorldState;

    iget v6, v6, Lcom/android/magicsmoke/MagicSmokeRS$WorldState;->mBackCol:I

    invoke-virtual {v5, v6}, Lcom/android/magicsmoke/ScriptC_clouds;->set_gBackCol(I)V

    iget-object v5, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mScript:Lcom/android/magicsmoke/ScriptC_clouds;

    iget-object v6, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mWorldState:Lcom/android/magicsmoke/MagicSmokeRS$WorldState;

    iget v6, v6, Lcom/android/magicsmoke/MagicSmokeRS$WorldState;->mLowCol:I

    invoke-virtual {v5, v6}, Lcom/android/magicsmoke/ScriptC_clouds;->set_gLowCol(I)V

    iget-object v5, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mScript:Lcom/android/magicsmoke/ScriptC_clouds;

    iget-object v6, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mWorldState:Lcom/android/magicsmoke/MagicSmokeRS$WorldState;

    iget v6, v6, Lcom/android/magicsmoke/MagicSmokeRS$WorldState;->mHighCol:I

    invoke-virtual {v5, v6}, Lcom/android/magicsmoke/ScriptC_clouds;->set_gHighCol(I)V

    iget-object v5, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mScript:Lcom/android/magicsmoke/ScriptC_clouds;

    iget-object v6, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mWorldState:Lcom/android/magicsmoke/MagicSmokeRS$WorldState;

    iget v6, v6, Lcom/android/magicsmoke/MagicSmokeRS$WorldState;->mAlphaMul:F

    invoke-virtual {v5, v6}, Lcom/android/magicsmoke/ScriptC_clouds;->set_gAlphaMul(F)V

    iget-object v5, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mScript:Lcom/android/magicsmoke/ScriptC_clouds;

    iget-object v6, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mWorldState:Lcom/android/magicsmoke/MagicSmokeRS$WorldState;

    iget v6, v6, Lcom/android/magicsmoke/MagicSmokeRS$WorldState;->mPreMul:I

    invoke-virtual {v5, v6}, Lcom/android/magicsmoke/ScriptC_clouds;->set_gPreMul(I)V

    iget-object v5, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mScript:Lcom/android/magicsmoke/ScriptC_clouds;

    iget-object v6, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mWorldState:Lcom/android/magicsmoke/MagicSmokeRS$WorldState;

    iget v6, v6, Lcom/android/magicsmoke/MagicSmokeRS$WorldState;->mXOffset:F

    invoke-virtual {v5, v6}, Lcom/android/magicsmoke/ScriptC_clouds;->set_gXOffset(F)V

    iget-object v5, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mScript:Lcom/android/magicsmoke/ScriptC_clouds;

    return-object v5
.end method

.method loadBitmap(IILjava/lang/String;FII)V
    .locals 11
    .param p1    # I
    .param p2    # I
    .param p3    # Ljava/lang/String;
    .param p4    # F
    .param p5    # I
    .param p6    # I

    new-instance v10, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v10}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v2, v10, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    iget-object v2, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mResources:Landroid/content/res/Resources;

    invoke-static {v2, p1, v10}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    const/high16 v2, 0x10000

    new-array v1, v2, [I

    const/4 v2, 0x0

    const/16 v3, 0x100

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0x100

    const/16 v7, 0x100

    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    iget-object v2, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mRealTextures:[Landroid/renderscript/Allocation;

    iget-object v3, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mRS:Landroid/renderscript/RenderScriptGL;

    iget-object v4, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mTextureType:Landroid/renderscript/Type;

    sget-object v5, Landroid/renderscript/Allocation$MipmapControl;->MIPMAP_NONE:Landroid/renderscript/Allocation$MipmapControl;

    const/4 v6, 0x3

    invoke-static {v3, v4, v5, v6}, Landroid/renderscript/Allocation;->createTyped(Landroid/renderscript/RenderScript;Landroid/renderscript/Type;Landroid/renderscript/Allocation$MipmapControl;I)Landroid/renderscript/Allocation;

    move-result-object v3

    aput-object v3, v2, p2

    iget-object v2, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mSourceTextures:[Landroid/renderscript/Allocation;

    iget-object v3, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mRS:Landroid/renderscript/RenderScriptGL;

    iget-object v4, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mTextureType:Landroid/renderscript/Type;

    sget-object v5, Landroid/renderscript/Allocation$MipmapControl;->MIPMAP_NONE:Landroid/renderscript/Allocation$MipmapControl;

    const/4 v6, 0x1

    invoke-static {v3, v4, v5, v6}, Landroid/renderscript/Allocation;->createTyped(Landroid/renderscript/RenderScript;Landroid/renderscript/Type;Landroid/renderscript/Allocation$MipmapControl;I)Landroid/renderscript/Allocation;

    move-result-object v3

    aput-object v3, v2, p2

    const/high16 v2, 0x40000

    new-array v8, v2, [B

    const/4 v9, 0x0

    :goto_0
    const/high16 v2, 0x10000

    if-ge v9, v2, :cond_0

    mul-int/lit8 v2, v9, 0x4

    add-int/lit8 v2, v2, 0x0

    aget v3, v1, v9

    and-int/lit16 v3, v3, 0xff

    int-to-byte v3, v3

    aput-byte v3, v8, v2

    mul-int/lit8 v2, v9, 0x4

    add-int/lit8 v2, v2, 0x1

    aget v3, v1, v9

    shr-int/lit8 v3, v3, 0x8

    and-int/lit16 v3, v3, 0xff

    int-to-byte v3, v3

    aput-byte v3, v8, v2

    mul-int/lit8 v2, v9, 0x4

    add-int/lit8 v2, v2, 0x2

    aget v3, v1, v9

    shr-int/lit8 v3, v3, 0x10

    and-int/lit16 v3, v3, 0xff

    int-to-byte v3, v3

    aput-byte v3, v8, v2

    mul-int/lit8 v2, v9, 0x4

    add-int/lit8 v2, v2, 0x3

    aget v3, v1, v9

    shr-int/lit8 v3, v3, 0x18

    and-int/lit16 v3, v3, 0xff

    int-to-byte v3, v3

    aput-byte v3, v8, v2

    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    :cond_0
    iget-object v2, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mSourceTextures:[Landroid/renderscript/Allocation;

    aget-object v2, v2, p2

    invoke-virtual {v2, v8}, Landroid/renderscript/Allocation;->copyFrom([B)V

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    return-void
.end method

.method loadBitmaps()V
    .locals 7

    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/android/magicsmoke/MagicSmokeRS;->alphafactor:F

    sget-object v0, Lcom/android/magicsmoke/MagicSmokeRS;->mPreset:[Lcom/android/magicsmoke/MagicSmokeRS$Preset;

    iget-object v1, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mWorldState:Lcom/android/magicsmoke/MagicSmokeRS$WorldState;

    iget v1, v1, Lcom/android/magicsmoke/MagicSmokeRS$WorldState;->mPreset:I

    aget-object v0, v0, v1

    iget v4, v0, Lcom/android/magicsmoke/MagicSmokeRS$Preset;->mAlphaMul:F

    sget-object v0, Lcom/android/magicsmoke/MagicSmokeRS;->mPreset:[Lcom/android/magicsmoke/MagicSmokeRS$Preset;

    iget-object v1, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mWorldState:Lcom/android/magicsmoke/MagicSmokeRS$WorldState;

    iget v1, v1, Lcom/android/magicsmoke/MagicSmokeRS$WorldState;->mPreset:I

    aget-object v0, v0, v1

    iget v5, v0, Lcom/android/magicsmoke/MagicSmokeRS$Preset;->mLowColor:I

    sget-object v0, Lcom/android/magicsmoke/MagicSmokeRS;->mPreset:[Lcom/android/magicsmoke/MagicSmokeRS$Preset;

    iget-object v1, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mWorldState:Lcom/android/magicsmoke/MagicSmokeRS$WorldState;

    iget v1, v1, Lcom/android/magicsmoke/MagicSmokeRS$WorldState;->mPreset:I

    aget-object v0, v0, v1

    iget v6, v0, Lcom/android/magicsmoke/MagicSmokeRS$Preset;->mHighColor:I

    const v1, 0x7f020002    # com.android.magicsmoke.R.drawable.noise1

    const/4 v2, 0x0

    const-string v3, "Tnoise1"

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lcom/android/magicsmoke/MagicSmokeRS;->loadBitmap(IILjava/lang/String;FII)V

    const v1, 0x7f020003    # com.android.magicsmoke.R.drawable.noise2

    const/4 v2, 0x1

    const-string v3, "Tnoise2"

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lcom/android/magicsmoke/MagicSmokeRS;->loadBitmap(IILjava/lang/String;FII)V

    const v1, 0x7f020004    # com.android.magicsmoke.R.drawable.noise3

    const/4 v2, 0x2

    const-string v3, "Tnoise3"

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lcom/android/magicsmoke/MagicSmokeRS;->loadBitmap(IILjava/lang/String;FII)V

    const v1, 0x7f020005    # com.android.magicsmoke.R.drawable.noise4

    const/4 v2, 0x3

    const-string v3, "Tnoise4"

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lcom/android/magicsmoke/MagicSmokeRS;->loadBitmap(IILjava/lang/String;FII)V

    const v1, 0x7f020006    # com.android.magicsmoke.R.drawable.noise5

    const/4 v2, 0x4

    const-string v3, "Tnoise5"

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lcom/android/magicsmoke/MagicSmokeRS;->loadBitmap(IILjava/lang/String;FII)V

    iget-object v0, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mScript:Lcom/android/magicsmoke/ScriptC_clouds;

    iget-object v1, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mRealTextures:[Landroid/renderscript/Allocation;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Lcom/android/magicsmoke/ScriptC_clouds;->set_gTnoise1(Landroid/renderscript/Allocation;)V

    iget-object v0, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mScript:Lcom/android/magicsmoke/ScriptC_clouds;

    iget-object v1, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mRealTextures:[Landroid/renderscript/Allocation;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Lcom/android/magicsmoke/ScriptC_clouds;->set_gTnoise2(Landroid/renderscript/Allocation;)V

    iget-object v0, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mScript:Lcom/android/magicsmoke/ScriptC_clouds;

    iget-object v1, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mRealTextures:[Landroid/renderscript/Allocation;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Lcom/android/magicsmoke/ScriptC_clouds;->set_gTnoise3(Landroid/renderscript/Allocation;)V

    iget-object v0, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mScript:Lcom/android/magicsmoke/ScriptC_clouds;

    iget-object v1, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mRealTextures:[Landroid/renderscript/Allocation;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Lcom/android/magicsmoke/ScriptC_clouds;->set_gTnoise4(Landroid/renderscript/Allocation;)V

    iget-object v0, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mScript:Lcom/android/magicsmoke/ScriptC_clouds;

    iget-object v1, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mRealTextures:[Landroid/renderscript/Allocation;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Lcom/android/magicsmoke/ScriptC_clouds;->set_gTnoise5(Landroid/renderscript/Allocation;)V

    iget-object v0, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mScript:Lcom/android/magicsmoke/ScriptC_clouds;

    iget-object v1, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mSourceTextures:[Landroid/renderscript/Allocation;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Lcom/android/magicsmoke/ScriptC_clouds;->bind_gNoisesrc1(Landroid/renderscript/Allocation;)V

    iget-object v0, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mScript:Lcom/android/magicsmoke/ScriptC_clouds;

    iget-object v1, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mSourceTextures:[Landroid/renderscript/Allocation;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Lcom/android/magicsmoke/ScriptC_clouds;->bind_gNoisesrc2(Landroid/renderscript/Allocation;)V

    iget-object v0, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mScript:Lcom/android/magicsmoke/ScriptC_clouds;

    iget-object v1, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mSourceTextures:[Landroid/renderscript/Allocation;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Lcom/android/magicsmoke/ScriptC_clouds;->bind_gNoisesrc3(Landroid/renderscript/Allocation;)V

    iget-object v0, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mScript:Lcom/android/magicsmoke/ScriptC_clouds;

    iget-object v1, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mSourceTextures:[Landroid/renderscript/Allocation;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Lcom/android/magicsmoke/ScriptC_clouds;->bind_gNoisesrc4(Landroid/renderscript/Allocation;)V

    iget-object v0, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mScript:Lcom/android/magicsmoke/ScriptC_clouds;

    iget-object v1, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mSourceTextures:[Landroid/renderscript/Allocation;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Lcom/android/magicsmoke/ScriptC_clouds;->bind_gNoisesrc5(Landroid/renderscript/Allocation;)V

    iget-object v0, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mScript:Lcom/android/magicsmoke/ScriptC_clouds;

    iget-object v1, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mRealTextures:[Landroid/renderscript/Allocation;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Lcom/android/magicsmoke/ScriptC_clouds;->bind_gNoisedst1(Landroid/renderscript/Allocation;)V

    iget-object v0, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mScript:Lcom/android/magicsmoke/ScriptC_clouds;

    iget-object v1, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mRealTextures:[Landroid/renderscript/Allocation;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Lcom/android/magicsmoke/ScriptC_clouds;->bind_gNoisedst2(Landroid/renderscript/Allocation;)V

    iget-object v0, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mScript:Lcom/android/magicsmoke/ScriptC_clouds;

    iget-object v1, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mRealTextures:[Landroid/renderscript/Allocation;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Lcom/android/magicsmoke/ScriptC_clouds;->bind_gNoisedst3(Landroid/renderscript/Allocation;)V

    iget-object v0, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mScript:Lcom/android/magicsmoke/ScriptC_clouds;

    iget-object v1, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mRealTextures:[Landroid/renderscript/Allocation;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Lcom/android/magicsmoke/ScriptC_clouds;->bind_gNoisedst4(Landroid/renderscript/Allocation;)V

    iget-object v0, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mScript:Lcom/android/magicsmoke/ScriptC_clouds;

    iget-object v1, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mRealTextures:[Landroid/renderscript/Allocation;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Lcom/android/magicsmoke/ScriptC_clouds;->bind_gNoisedst5(Landroid/renderscript/Allocation;)V

    return-void
.end method

.method makeNewState()V
    .locals 6

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mSharedPref:Landroid/content/SharedPreferences;

    const-string v4, "preset"

    const/16 v5, 0x10

    invoke-interface {v1, v4, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    sget-object v1, Lcom/android/magicsmoke/MagicSmokeRS;->mPreset:[Lcom/android/magicsmoke/MagicSmokeRS$Preset;

    array-length v1, v1

    if-lt v0, v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    iget-object v1, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mWorldState:Lcom/android/magicsmoke/MagicSmokeRS$WorldState;

    iput v0, v1, Lcom/android/magicsmoke/MagicSmokeRS$WorldState;->mPreset:I

    iget-object v1, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mWorldState:Lcom/android/magicsmoke/MagicSmokeRS$WorldState;

    sget-object v4, Lcom/android/magicsmoke/MagicSmokeRS;->mPreset:[Lcom/android/magicsmoke/MagicSmokeRS$Preset;

    aget-object v4, v4, v0

    iget v4, v4, Lcom/android/magicsmoke/MagicSmokeRS$Preset;->mTextureMask:I

    iput v4, v1, Lcom/android/magicsmoke/MagicSmokeRS$WorldState;->mTextureMask:I

    iget-object v4, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mWorldState:Lcom/android/magicsmoke/MagicSmokeRS$WorldState;

    sget-object v1, Lcom/android/magicsmoke/MagicSmokeRS;->mPreset:[Lcom/android/magicsmoke/MagicSmokeRS$Preset;

    aget-object v1, v1, v0

    iget-boolean v1, v1, Lcom/android/magicsmoke/MagicSmokeRS$Preset;->mRotate:Z

    if-eqz v1, :cond_2

    move v1, v2

    :goto_0
    iput v1, v4, Lcom/android/magicsmoke/MagicSmokeRS$WorldState;->mRotate:I

    iget-object v4, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mWorldState:Lcom/android/magicsmoke/MagicSmokeRS$WorldState;

    sget-object v1, Lcom/android/magicsmoke/MagicSmokeRS;->mPreset:[Lcom/android/magicsmoke/MagicSmokeRS$Preset;

    aget-object v1, v1, v0

    iget-boolean v1, v1, Lcom/android/magicsmoke/MagicSmokeRS$Preset;->mTextureSwap:Z

    if-eqz v1, :cond_3

    move v1, v2

    :goto_1
    iput v1, v4, Lcom/android/magicsmoke/MagicSmokeRS$WorldState;->mTextureSwap:I

    iget-object v1, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mWorldState:Lcom/android/magicsmoke/MagicSmokeRS$WorldState;

    sget-object v4, Lcom/android/magicsmoke/MagicSmokeRS;->mPreset:[Lcom/android/magicsmoke/MagicSmokeRS$Preset;

    aget-object v4, v4, v0

    iget v4, v4, Lcom/android/magicsmoke/MagicSmokeRS$Preset;->mProcessTextureMode:I

    iput v4, v1, Lcom/android/magicsmoke/MagicSmokeRS$WorldState;->mProcessTextureMode:I

    iget-object v1, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mWorldState:Lcom/android/magicsmoke/MagicSmokeRS$WorldState;

    sget-object v4, Lcom/android/magicsmoke/MagicSmokeRS;->mPreset:[Lcom/android/magicsmoke/MagicSmokeRS$Preset;

    aget-object v4, v4, v0

    iget v4, v4, Lcom/android/magicsmoke/MagicSmokeRS$Preset;->mBackColor:I

    iput v4, v1, Lcom/android/magicsmoke/MagicSmokeRS$WorldState;->mBackCol:I

    iget-object v1, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mWorldState:Lcom/android/magicsmoke/MagicSmokeRS$WorldState;

    sget-object v4, Lcom/android/magicsmoke/MagicSmokeRS;->mPreset:[Lcom/android/magicsmoke/MagicSmokeRS$Preset;

    aget-object v4, v4, v0

    iget v4, v4, Lcom/android/magicsmoke/MagicSmokeRS$Preset;->mLowColor:I

    iput v4, v1, Lcom/android/magicsmoke/MagicSmokeRS$WorldState;->mLowCol:I

    iget-object v1, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mWorldState:Lcom/android/magicsmoke/MagicSmokeRS$WorldState;

    sget-object v4, Lcom/android/magicsmoke/MagicSmokeRS;->mPreset:[Lcom/android/magicsmoke/MagicSmokeRS$Preset;

    aget-object v4, v4, v0

    iget v4, v4, Lcom/android/magicsmoke/MagicSmokeRS$Preset;->mHighColor:I

    iput v4, v1, Lcom/android/magicsmoke/MagicSmokeRS$WorldState;->mHighCol:I

    iget-object v1, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mWorldState:Lcom/android/magicsmoke/MagicSmokeRS$WorldState;

    sget-object v4, Lcom/android/magicsmoke/MagicSmokeRS;->mPreset:[Lcom/android/magicsmoke/MagicSmokeRS$Preset;

    aget-object v4, v4, v0

    iget v4, v4, Lcom/android/magicsmoke/MagicSmokeRS$Preset;->mAlphaMul:F

    iput v4, v1, Lcom/android/magicsmoke/MagicSmokeRS$WorldState;->mAlphaMul:F

    iget-object v1, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mWorldState:Lcom/android/magicsmoke/MagicSmokeRS$WorldState;

    sget-object v4, Lcom/android/magicsmoke/MagicSmokeRS;->mPreset:[Lcom/android/magicsmoke/MagicSmokeRS$Preset;

    aget-object v4, v4, v0

    iget-boolean v4, v4, Lcom/android/magicsmoke/MagicSmokeRS$Preset;->mPreMul:Z

    if-eqz v4, :cond_4

    :goto_2
    iput v2, v1, Lcom/android/magicsmoke/MagicSmokeRS$WorldState;->mPreMul:I

    iget-object v1, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mScript:Lcom/android/magicsmoke/ScriptC_clouds;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mScript:Lcom/android/magicsmoke/ScriptC_clouds;

    iget-object v2, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mWorldState:Lcom/android/magicsmoke/MagicSmokeRS$WorldState;

    iget v2, v2, Lcom/android/magicsmoke/MagicSmokeRS$WorldState;->mPreset:I

    invoke-virtual {v1, v2}, Lcom/android/magicsmoke/ScriptC_clouds;->set_gPreset(I)V

    iget-object v1, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mScript:Lcom/android/magicsmoke/ScriptC_clouds;

    iget-object v2, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mWorldState:Lcom/android/magicsmoke/MagicSmokeRS$WorldState;

    iget v2, v2, Lcom/android/magicsmoke/MagicSmokeRS$WorldState;->mTextureMask:I

    invoke-virtual {v1, v2}, Lcom/android/magicsmoke/ScriptC_clouds;->set_gTextureMask(I)V

    iget-object v1, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mScript:Lcom/android/magicsmoke/ScriptC_clouds;

    iget-object v2, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mWorldState:Lcom/android/magicsmoke/MagicSmokeRS$WorldState;

    iget v2, v2, Lcom/android/magicsmoke/MagicSmokeRS$WorldState;->mRotate:I

    invoke-virtual {v1, v2}, Lcom/android/magicsmoke/ScriptC_clouds;->set_gRotate(I)V

    iget-object v1, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mScript:Lcom/android/magicsmoke/ScriptC_clouds;

    iget-object v2, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mWorldState:Lcom/android/magicsmoke/MagicSmokeRS$WorldState;

    iget v2, v2, Lcom/android/magicsmoke/MagicSmokeRS$WorldState;->mTextureSwap:I

    invoke-virtual {v1, v2}, Lcom/android/magicsmoke/ScriptC_clouds;->set_gTextureSwap(I)V

    iget-object v1, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mScript:Lcom/android/magicsmoke/ScriptC_clouds;

    iget-object v2, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mWorldState:Lcom/android/magicsmoke/MagicSmokeRS$WorldState;

    iget v2, v2, Lcom/android/magicsmoke/MagicSmokeRS$WorldState;->mProcessTextureMode:I

    invoke-virtual {v1, v2}, Lcom/android/magicsmoke/ScriptC_clouds;->set_gProcessTextureMode(I)V

    iget-object v1, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mScript:Lcom/android/magicsmoke/ScriptC_clouds;

    iget-object v2, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mWorldState:Lcom/android/magicsmoke/MagicSmokeRS$WorldState;

    iget v2, v2, Lcom/android/magicsmoke/MagicSmokeRS$WorldState;->mBackCol:I

    invoke-virtual {v1, v2}, Lcom/android/magicsmoke/ScriptC_clouds;->set_gBackCol(I)V

    iget-object v1, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mScript:Lcom/android/magicsmoke/ScriptC_clouds;

    iget-object v2, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mWorldState:Lcom/android/magicsmoke/MagicSmokeRS$WorldState;

    iget v2, v2, Lcom/android/magicsmoke/MagicSmokeRS$WorldState;->mLowCol:I

    invoke-virtual {v1, v2}, Lcom/android/magicsmoke/ScriptC_clouds;->set_gLowCol(I)V

    iget-object v1, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mScript:Lcom/android/magicsmoke/ScriptC_clouds;

    iget-object v2, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mWorldState:Lcom/android/magicsmoke/MagicSmokeRS$WorldState;

    iget v2, v2, Lcom/android/magicsmoke/MagicSmokeRS$WorldState;->mHighCol:I

    invoke-virtual {v1, v2}, Lcom/android/magicsmoke/ScriptC_clouds;->set_gHighCol(I)V

    iget-object v1, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mScript:Lcom/android/magicsmoke/ScriptC_clouds;

    iget-object v2, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mWorldState:Lcom/android/magicsmoke/MagicSmokeRS$WorldState;

    iget v2, v2, Lcom/android/magicsmoke/MagicSmokeRS$WorldState;->mAlphaMul:F

    invoke-virtual {v1, v2}, Lcom/android/magicsmoke/ScriptC_clouds;->set_gAlphaMul(F)V

    iget-object v1, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mScript:Lcom/android/magicsmoke/ScriptC_clouds;

    iget-object v2, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mWorldState:Lcom/android/magicsmoke/MagicSmokeRS$WorldState;

    iget v2, v2, Lcom/android/magicsmoke/MagicSmokeRS$WorldState;->mPreMul:I

    invoke-virtual {v1, v2}, Lcom/android/magicsmoke/ScriptC_clouds;->set_gPreMul(I)V

    :cond_1
    return-void

    :cond_2
    move v1, v3

    goto/16 :goto_0

    :cond_3
    move v1, v3

    goto/16 :goto_1

    :cond_4
    move v2, v3

    goto :goto_2
.end method

.method public onCommand(Ljava/lang/String;IIILandroid/os/Bundle;Z)Landroid/os/Bundle;
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # Landroid/os/Bundle;
    .param p6    # Z

    const-string v0, "android.wallpaper.tap"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    int-to-float v0, p3

    iput v0, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mTouchY:F

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 1
    .param p1    # Landroid/content/SharedPreferences;
    .param p2    # Ljava/lang/String;

    iget-boolean v0, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mIsStarted:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/magicsmoke/MagicSmokeRS;->start()V

    iget-object v0, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-virtual {v0}, Landroid/renderscript/RenderScriptGL;->finish()V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/magicsmoke/MagicSmokeRS;->stop(Z)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/magicsmoke/MagicSmokeRS;->makeNewState()V

    goto :goto_0
.end method

.method public resize(II)V
    .locals 2
    .param p1    # I
    .param p2    # I

    invoke-super {p0, p1, p2}, Lcom/android/magicsmoke/RenderScriptScene;->resize(II)V

    iget-object v1, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mPVAlloc:Landroid/renderscript/ProgramVertexFixedFunction$Constants;

    if-eqz v1, :cond_0

    new-instance v0, Landroid/renderscript/Matrix4f;

    invoke-direct {v0}, Landroid/renderscript/Matrix4f;-><init>()V

    invoke-virtual {v0, p1, p2}, Landroid/renderscript/Matrix4f;->loadProjectionNormalized(II)V

    iget-object v1, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mPVAlloc:Landroid/renderscript/ProgramVertexFixedFunction$Constants;

    invoke-virtual {v1, v0}, Landroid/renderscript/ProgramVertexFixedFunction$Constants;->setProjection(Landroid/renderscript/Matrix4f;)V

    :cond_0
    return-void
.end method

.method public setOffset(FFFFII)V
    .locals 2
    .param p1    # F
    .param p2    # F
    .param p3    # F
    .param p4    # F
    .param p5    # I
    .param p6    # I

    iget-object v0, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mWorldState:Lcom/android/magicsmoke/MagicSmokeRS$WorldState;

    iput p1, v0, Lcom/android/magicsmoke/MagicSmokeRS$WorldState;->mXOffset:F

    iget-object v0, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mWorldState:Lcom/android/magicsmoke/MagicSmokeRS$WorldState;

    iput p2, v0, Lcom/android/magicsmoke/MagicSmokeRS$WorldState;->mYOffset:F

    iget-object v0, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mScript:Lcom/android/magicsmoke/ScriptC_clouds;

    iget-object v1, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mWorldState:Lcom/android/magicsmoke/MagicSmokeRS$WorldState;

    iget v1, v1, Lcom/android/magicsmoke/MagicSmokeRS$WorldState;->mXOffset:F

    invoke-virtual {v0, v1}, Lcom/android/magicsmoke/ScriptC_clouds;->set_gXOffset(F)V

    iget-object v0, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mScript:Lcom/android/magicsmoke/ScriptC_clouds;

    iget-object v1, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mWorldState:Lcom/android/magicsmoke/MagicSmokeRS$WorldState;

    iget v1, v1, Lcom/android/magicsmoke/MagicSmokeRS$WorldState;->mYOffset:F

    invoke-virtual {v0, v1}, Lcom/android/magicsmoke/ScriptC_clouds;->set_gYOffset(F)V

    return-void
.end method

.method public start()V
    .locals 0

    invoke-virtual {p0}, Lcom/android/magicsmoke/MagicSmokeRS;->makeNewState()V

    invoke-super {p0}, Lcom/android/magicsmoke/RenderScriptScene;->start()V

    return-void
.end method

.method public stop(Z)V
    .locals 1
    .param p1    # Z

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/magicsmoke/MagicSmokeRS;->mSharedPref:Landroid/content/SharedPreferences;

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    :cond_0
    invoke-super {p0, p1}, Lcom/android/magicsmoke/RenderScriptScene;->stop(Z)V

    return-void
.end method
