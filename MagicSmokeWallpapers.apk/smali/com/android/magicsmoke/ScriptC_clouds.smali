.class public Lcom/android/magicsmoke/ScriptC_clouds;
.super Landroid/renderscript/ScriptC;
.source "ScriptC_clouds.java"


# instance fields
.field private __ALLOCATION:Landroid/renderscript/Element;

.field private __F32:Landroid/renderscript/Element;

.field private __I32:Landroid/renderscript/Element;

.field private __PROGRAM_FRAGMENT:Landroid/renderscript/Element;

.field private __PROGRAM_STORE:Landroid/renderscript/Element;

.field private __PROGRAM_VERTEX:Landroid/renderscript/Element;

.field private mExportVar_gAlphaMul:F

.field private mExportVar_gBackCol:I

.field private mExportVar_gFSConstants:Lcom/android/magicsmoke/ScriptField_FragmentShaderConstants_s;

.field private mExportVar_gHighCol:I

.field private mExportVar_gLowCol:I

.field private mExportVar_gNoisedst1:Landroid/renderscript/Allocation;

.field private mExportVar_gNoisedst2:Landroid/renderscript/Allocation;

.field private mExportVar_gNoisedst3:Landroid/renderscript/Allocation;

.field private mExportVar_gNoisedst4:Landroid/renderscript/Allocation;

.field private mExportVar_gNoisedst5:Landroid/renderscript/Allocation;

.field private mExportVar_gNoisesrc1:Landroid/renderscript/Allocation;

.field private mExportVar_gNoisesrc2:Landroid/renderscript/Allocation;

.field private mExportVar_gNoisesrc3:Landroid/renderscript/Allocation;

.field private mExportVar_gNoisesrc4:Landroid/renderscript/Allocation;

.field private mExportVar_gNoisesrc5:Landroid/renderscript/Allocation;

.field private mExportVar_gPF4tex:Landroid/renderscript/ProgramFragment;

.field private mExportVar_gPF5tex:Landroid/renderscript/ProgramFragment;

.field private mExportVar_gPStore:Landroid/renderscript/ProgramStore;

.field private mExportVar_gPV4tex:Landroid/renderscript/ProgramVertex;

.field private mExportVar_gPV5tex:Landroid/renderscript/ProgramVertex;

.field private mExportVar_gPreMul:I

.field private mExportVar_gPreset:I

.field private mExportVar_gProcessTextureMode:I

.field private mExportVar_gRotate:I

.field private mExportVar_gTextureMask:I

.field private mExportVar_gTextureSwap:I

.field private mExportVar_gTnoise1:Landroid/renderscript/Allocation;

.field private mExportVar_gTnoise2:Landroid/renderscript/Allocation;

.field private mExportVar_gTnoise3:Landroid/renderscript/Allocation;

.field private mExportVar_gTnoise4:Landroid/renderscript/Allocation;

.field private mExportVar_gTnoise5:Landroid/renderscript/Allocation;

.field private mExportVar_gVSConstants:Lcom/android/magicsmoke/ScriptField_VertexShaderConstants_s;

.field private mExportVar_gXOffset:F

.field private mExportVar_gYOffset:F


# direct methods
.method public constructor <init>(Landroid/renderscript/RenderScript;Landroid/content/res/Resources;I)V
    .locals 1
    .param p1    # Landroid/renderscript/RenderScript;
    .param p2    # Landroid/content/res/Resources;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/renderscript/ScriptC;-><init>(Landroid/renderscript/RenderScript;Landroid/content/res/Resources;I)V

    invoke-static {p1}, Landroid/renderscript/Element;->F32(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/magicsmoke/ScriptC_clouds;->__F32:Landroid/renderscript/Element;

    invoke-static {p1}, Landroid/renderscript/Element;->I32(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/magicsmoke/ScriptC_clouds;->__I32:Landroid/renderscript/Element;

    invoke-static {p1}, Landroid/renderscript/Element;->PROGRAM_FRAGMENT(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/magicsmoke/ScriptC_clouds;->__PROGRAM_FRAGMENT:Landroid/renderscript/Element;

    invoke-static {p1}, Landroid/renderscript/Element;->PROGRAM_VERTEX(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/magicsmoke/ScriptC_clouds;->__PROGRAM_VERTEX:Landroid/renderscript/Element;

    invoke-static {p1}, Landroid/renderscript/Element;->PROGRAM_STORE(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/magicsmoke/ScriptC_clouds;->__PROGRAM_STORE:Landroid/renderscript/Element;

    invoke-static {p1}, Landroid/renderscript/Element;->ALLOCATION(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/magicsmoke/ScriptC_clouds;->__ALLOCATION:Landroid/renderscript/Element;

    return-void
.end method


# virtual methods
.method public bind_gFSConstants(Lcom/android/magicsmoke/ScriptField_FragmentShaderConstants_s;)V
    .locals 2
    .param p1    # Lcom/android/magicsmoke/ScriptField_FragmentShaderConstants_s;

    const/16 v1, 0xd

    iput-object p1, p0, Lcom/android/magicsmoke/ScriptC_clouds;->mExportVar_gFSConstants:Lcom/android/magicsmoke/ScriptField_FragmentShaderConstants_s;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/android/magicsmoke/ScriptC_clouds;->bindAllocation(Landroid/renderscript/Allocation;I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/android/magicsmoke/ScriptField_FragmentShaderConstants_s;->getAllocation()Landroid/renderscript/Allocation;

    move-result-object v0

    invoke-virtual {p0, v0, v1}, Lcom/android/magicsmoke/ScriptC_clouds;->bindAllocation(Landroid/renderscript/Allocation;I)V

    goto :goto_0
.end method

.method public bind_gNoisedst1(Landroid/renderscript/Allocation;)V
    .locals 2
    .param p1    # Landroid/renderscript/Allocation;

    const/16 v1, 0x1e

    iput-object p1, p0, Lcom/android/magicsmoke/ScriptC_clouds;->mExportVar_gNoisedst1:Landroid/renderscript/Allocation;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/android/magicsmoke/ScriptC_clouds;->bindAllocation(Landroid/renderscript/Allocation;I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, p1, v1}, Lcom/android/magicsmoke/ScriptC_clouds;->bindAllocation(Landroid/renderscript/Allocation;I)V

    goto :goto_0
.end method

.method public bind_gNoisedst2(Landroid/renderscript/Allocation;)V
    .locals 2
    .param p1    # Landroid/renderscript/Allocation;

    const/16 v1, 0x1f

    iput-object p1, p0, Lcom/android/magicsmoke/ScriptC_clouds;->mExportVar_gNoisedst2:Landroid/renderscript/Allocation;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/android/magicsmoke/ScriptC_clouds;->bindAllocation(Landroid/renderscript/Allocation;I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, p1, v1}, Lcom/android/magicsmoke/ScriptC_clouds;->bindAllocation(Landroid/renderscript/Allocation;I)V

    goto :goto_0
.end method

.method public bind_gNoisedst3(Landroid/renderscript/Allocation;)V
    .locals 2
    .param p1    # Landroid/renderscript/Allocation;

    const/16 v1, 0x20

    iput-object p1, p0, Lcom/android/magicsmoke/ScriptC_clouds;->mExportVar_gNoisedst3:Landroid/renderscript/Allocation;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/android/magicsmoke/ScriptC_clouds;->bindAllocation(Landroid/renderscript/Allocation;I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, p1, v1}, Lcom/android/magicsmoke/ScriptC_clouds;->bindAllocation(Landroid/renderscript/Allocation;I)V

    goto :goto_0
.end method

.method public bind_gNoisedst4(Landroid/renderscript/Allocation;)V
    .locals 2
    .param p1    # Landroid/renderscript/Allocation;

    const/16 v1, 0x21

    iput-object p1, p0, Lcom/android/magicsmoke/ScriptC_clouds;->mExportVar_gNoisedst4:Landroid/renderscript/Allocation;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/android/magicsmoke/ScriptC_clouds;->bindAllocation(Landroid/renderscript/Allocation;I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, p1, v1}, Lcom/android/magicsmoke/ScriptC_clouds;->bindAllocation(Landroid/renderscript/Allocation;I)V

    goto :goto_0
.end method

.method public bind_gNoisedst5(Landroid/renderscript/Allocation;)V
    .locals 2
    .param p1    # Landroid/renderscript/Allocation;

    const/16 v1, 0x22

    iput-object p1, p0, Lcom/android/magicsmoke/ScriptC_clouds;->mExportVar_gNoisedst5:Landroid/renderscript/Allocation;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/android/magicsmoke/ScriptC_clouds;->bindAllocation(Landroid/renderscript/Allocation;I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, p1, v1}, Lcom/android/magicsmoke/ScriptC_clouds;->bindAllocation(Landroid/renderscript/Allocation;I)V

    goto :goto_0
.end method

.method public bind_gNoisesrc1(Landroid/renderscript/Allocation;)V
    .locals 2
    .param p1    # Landroid/renderscript/Allocation;

    const/16 v1, 0x19

    iput-object p1, p0, Lcom/android/magicsmoke/ScriptC_clouds;->mExportVar_gNoisesrc1:Landroid/renderscript/Allocation;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/android/magicsmoke/ScriptC_clouds;->bindAllocation(Landroid/renderscript/Allocation;I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, p1, v1}, Lcom/android/magicsmoke/ScriptC_clouds;->bindAllocation(Landroid/renderscript/Allocation;I)V

    goto :goto_0
.end method

.method public bind_gNoisesrc2(Landroid/renderscript/Allocation;)V
    .locals 2
    .param p1    # Landroid/renderscript/Allocation;

    const/16 v1, 0x1a

    iput-object p1, p0, Lcom/android/magicsmoke/ScriptC_clouds;->mExportVar_gNoisesrc2:Landroid/renderscript/Allocation;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/android/magicsmoke/ScriptC_clouds;->bindAllocation(Landroid/renderscript/Allocation;I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, p1, v1}, Lcom/android/magicsmoke/ScriptC_clouds;->bindAllocation(Landroid/renderscript/Allocation;I)V

    goto :goto_0
.end method

.method public bind_gNoisesrc3(Landroid/renderscript/Allocation;)V
    .locals 2
    .param p1    # Landroid/renderscript/Allocation;

    const/16 v1, 0x1b

    iput-object p1, p0, Lcom/android/magicsmoke/ScriptC_clouds;->mExportVar_gNoisesrc3:Landroid/renderscript/Allocation;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/android/magicsmoke/ScriptC_clouds;->bindAllocation(Landroid/renderscript/Allocation;I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, p1, v1}, Lcom/android/magicsmoke/ScriptC_clouds;->bindAllocation(Landroid/renderscript/Allocation;I)V

    goto :goto_0
.end method

.method public bind_gNoisesrc4(Landroid/renderscript/Allocation;)V
    .locals 2
    .param p1    # Landroid/renderscript/Allocation;

    const/16 v1, 0x1c

    iput-object p1, p0, Lcom/android/magicsmoke/ScriptC_clouds;->mExportVar_gNoisesrc4:Landroid/renderscript/Allocation;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/android/magicsmoke/ScriptC_clouds;->bindAllocation(Landroid/renderscript/Allocation;I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, p1, v1}, Lcom/android/magicsmoke/ScriptC_clouds;->bindAllocation(Landroid/renderscript/Allocation;I)V

    goto :goto_0
.end method

.method public bind_gNoisesrc5(Landroid/renderscript/Allocation;)V
    .locals 2
    .param p1    # Landroid/renderscript/Allocation;

    const/16 v1, 0x1d

    iput-object p1, p0, Lcom/android/magicsmoke/ScriptC_clouds;->mExportVar_gNoisesrc5:Landroid/renderscript/Allocation;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/android/magicsmoke/ScriptC_clouds;->bindAllocation(Landroid/renderscript/Allocation;I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, p1, v1}, Lcom/android/magicsmoke/ScriptC_clouds;->bindAllocation(Landroid/renderscript/Allocation;I)V

    goto :goto_0
.end method

.method public bind_gVSConstants(Lcom/android/magicsmoke/ScriptField_VertexShaderConstants_s;)V
    .locals 2
    .param p1    # Lcom/android/magicsmoke/ScriptField_VertexShaderConstants_s;

    const/16 v1, 0xc

    iput-object p1, p0, Lcom/android/magicsmoke/ScriptC_clouds;->mExportVar_gVSConstants:Lcom/android/magicsmoke/ScriptField_VertexShaderConstants_s;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/android/magicsmoke/ScriptC_clouds;->bindAllocation(Landroid/renderscript/Allocation;I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/android/magicsmoke/ScriptField_VertexShaderConstants_s;->getAllocation()Landroid/renderscript/Allocation;

    move-result-object v0

    invoke-virtual {p0, v0, v1}, Lcom/android/magicsmoke/ScriptC_clouds;->bindAllocation(Landroid/renderscript/Allocation;I)V

    goto :goto_0
.end method

.method public declared-synchronized set_gAlphaMul(F)V
    .locals 1
    .param p1    # F

    monitor-enter p0

    const/16 v0, 0xa

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/magicsmoke/ScriptC_clouds;->setVar(IF)V

    iput p1, p0, Lcom/android/magicsmoke/ScriptC_clouds;->mExportVar_gAlphaMul:F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_gBackCol(I)V
    .locals 1
    .param p1    # I

    monitor-enter p0

    const/4 v0, 0x7

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/magicsmoke/ScriptC_clouds;->setVar(II)V

    iput p1, p0, Lcom/android/magicsmoke/ScriptC_clouds;->mExportVar_gBackCol:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_gHighCol(I)V
    .locals 1
    .param p1    # I

    monitor-enter p0

    const/16 v0, 0x9

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/magicsmoke/ScriptC_clouds;->setVar(II)V

    iput p1, p0, Lcom/android/magicsmoke/ScriptC_clouds;->mExportVar_gHighCol:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_gLowCol(I)V
    .locals 1
    .param p1    # I

    monitor-enter p0

    const/16 v0, 0x8

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/magicsmoke/ScriptC_clouds;->setVar(II)V

    iput p1, p0, Lcom/android/magicsmoke/ScriptC_clouds;->mExportVar_gLowCol:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_gPF4tex(Landroid/renderscript/ProgramFragment;)V
    .locals 1
    .param p1    # Landroid/renderscript/ProgramFragment;

    monitor-enter p0

    const/16 v0, 0x11

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/magicsmoke/ScriptC_clouds;->setVar(ILandroid/renderscript/BaseObj;)V

    iput-object p1, p0, Lcom/android/magicsmoke/ScriptC_clouds;->mExportVar_gPF4tex:Landroid/renderscript/ProgramFragment;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_gPF5tex(Landroid/renderscript/ProgramFragment;)V
    .locals 1
    .param p1    # Landroid/renderscript/ProgramFragment;

    monitor-enter p0

    const/16 v0, 0xf

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/magicsmoke/ScriptC_clouds;->setVar(ILandroid/renderscript/BaseObj;)V

    iput-object p1, p0, Lcom/android/magicsmoke/ScriptC_clouds;->mExportVar_gPF5tex:Landroid/renderscript/ProgramFragment;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_gPStore(Landroid/renderscript/ProgramStore;)V
    .locals 1
    .param p1    # Landroid/renderscript/ProgramStore;

    monitor-enter p0

    const/16 v0, 0x13

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/magicsmoke/ScriptC_clouds;->setVar(ILandroid/renderscript/BaseObj;)V

    iput-object p1, p0, Lcom/android/magicsmoke/ScriptC_clouds;->mExportVar_gPStore:Landroid/renderscript/ProgramStore;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_gPV4tex(Landroid/renderscript/ProgramVertex;)V
    .locals 1
    .param p1    # Landroid/renderscript/ProgramVertex;

    monitor-enter p0

    const/16 v0, 0x12

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/magicsmoke/ScriptC_clouds;->setVar(ILandroid/renderscript/BaseObj;)V

    iput-object p1, p0, Lcom/android/magicsmoke/ScriptC_clouds;->mExportVar_gPV4tex:Landroid/renderscript/ProgramVertex;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_gPV5tex(Landroid/renderscript/ProgramVertex;)V
    .locals 1
    .param p1    # Landroid/renderscript/ProgramVertex;

    monitor-enter p0

    const/16 v0, 0x10

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/magicsmoke/ScriptC_clouds;->setVar(ILandroid/renderscript/BaseObj;)V

    iput-object p1, p0, Lcom/android/magicsmoke/ScriptC_clouds;->mExportVar_gPV5tex:Landroid/renderscript/ProgramVertex;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_gPreMul(I)V
    .locals 1
    .param p1    # I

    monitor-enter p0

    const/16 v0, 0xb

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/magicsmoke/ScriptC_clouds;->setVar(II)V

    iput p1, p0, Lcom/android/magicsmoke/ScriptC_clouds;->mExportVar_gPreMul:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_gPreset(I)V
    .locals 1
    .param p1    # I

    monitor-enter p0

    const/4 v0, 0x2

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/magicsmoke/ScriptC_clouds;->setVar(II)V

    iput p1, p0, Lcom/android/magicsmoke/ScriptC_clouds;->mExportVar_gPreset:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_gProcessTextureMode(I)V
    .locals 1
    .param p1    # I

    monitor-enter p0

    const/4 v0, 0x6

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/magicsmoke/ScriptC_clouds;->setVar(II)V

    iput p1, p0, Lcom/android/magicsmoke/ScriptC_clouds;->mExportVar_gProcessTextureMode:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_gRotate(I)V
    .locals 1
    .param p1    # I

    monitor-enter p0

    const/4 v0, 0x4

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/magicsmoke/ScriptC_clouds;->setVar(II)V

    iput p1, p0, Lcom/android/magicsmoke/ScriptC_clouds;->mExportVar_gRotate:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_gTextureMask(I)V
    .locals 1
    .param p1    # I

    monitor-enter p0

    const/4 v0, 0x3

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/magicsmoke/ScriptC_clouds;->setVar(II)V

    iput p1, p0, Lcom/android/magicsmoke/ScriptC_clouds;->mExportVar_gTextureMask:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_gTextureSwap(I)V
    .locals 1
    .param p1    # I

    monitor-enter p0

    const/4 v0, 0x5

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/magicsmoke/ScriptC_clouds;->setVar(II)V

    iput p1, p0, Lcom/android/magicsmoke/ScriptC_clouds;->mExportVar_gTextureSwap:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_gTnoise1(Landroid/renderscript/Allocation;)V
    .locals 1
    .param p1    # Landroid/renderscript/Allocation;

    monitor-enter p0

    const/16 v0, 0x14

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/magicsmoke/ScriptC_clouds;->setVar(ILandroid/renderscript/BaseObj;)V

    iput-object p1, p0, Lcom/android/magicsmoke/ScriptC_clouds;->mExportVar_gTnoise1:Landroid/renderscript/Allocation;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_gTnoise2(Landroid/renderscript/Allocation;)V
    .locals 1
    .param p1    # Landroid/renderscript/Allocation;

    monitor-enter p0

    const/16 v0, 0x15

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/magicsmoke/ScriptC_clouds;->setVar(ILandroid/renderscript/BaseObj;)V

    iput-object p1, p0, Lcom/android/magicsmoke/ScriptC_clouds;->mExportVar_gTnoise2:Landroid/renderscript/Allocation;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_gTnoise3(Landroid/renderscript/Allocation;)V
    .locals 1
    .param p1    # Landroid/renderscript/Allocation;

    monitor-enter p0

    const/16 v0, 0x16

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/magicsmoke/ScriptC_clouds;->setVar(ILandroid/renderscript/BaseObj;)V

    iput-object p1, p0, Lcom/android/magicsmoke/ScriptC_clouds;->mExportVar_gTnoise3:Landroid/renderscript/Allocation;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_gTnoise4(Landroid/renderscript/Allocation;)V
    .locals 1
    .param p1    # Landroid/renderscript/Allocation;

    monitor-enter p0

    const/16 v0, 0x17

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/magicsmoke/ScriptC_clouds;->setVar(ILandroid/renderscript/BaseObj;)V

    iput-object p1, p0, Lcom/android/magicsmoke/ScriptC_clouds;->mExportVar_gTnoise4:Landroid/renderscript/Allocation;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_gTnoise5(Landroid/renderscript/Allocation;)V
    .locals 1
    .param p1    # Landroid/renderscript/Allocation;

    monitor-enter p0

    const/16 v0, 0x18

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/magicsmoke/ScriptC_clouds;->setVar(ILandroid/renderscript/BaseObj;)V

    iput-object p1, p0, Lcom/android/magicsmoke/ScriptC_clouds;->mExportVar_gTnoise5:Landroid/renderscript/Allocation;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_gXOffset(F)V
    .locals 1
    .param p1    # F

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/magicsmoke/ScriptC_clouds;->setVar(IF)V

    iput p1, p0, Lcom/android/magicsmoke/ScriptC_clouds;->mExportVar_gXOffset:F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized set_gYOffset(F)V
    .locals 1
    .param p1    # F

    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    invoke-virtual {p0, v0, p1}, Lcom/android/magicsmoke/ScriptC_clouds;->setVar(IF)V

    iput p1, p0, Lcom/android/magicsmoke/ScriptC_clouds;->mExportVar_gYOffset:F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
