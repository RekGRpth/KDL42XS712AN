.class public Lcom/android/magicsmoke/ScriptField_VertexShaderConstants_s$Item;
.super Ljava/lang/Object;
.source "ScriptField_VertexShaderConstants_s.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/magicsmoke/ScriptField_VertexShaderConstants_s;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Item"
.end annotation


# instance fields
.field layer0:Landroid/renderscript/Float4;

.field layer1:Landroid/renderscript/Float4;

.field layer2:Landroid/renderscript/Float4;

.field layer3:Landroid/renderscript/Float4;

.field layer4:Landroid/renderscript/Float4;

.field panoffset:Landroid/renderscript/Float2;


# direct methods
.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/renderscript/Float4;

    invoke-direct {v0}, Landroid/renderscript/Float4;-><init>()V

    iput-object v0, p0, Lcom/android/magicsmoke/ScriptField_VertexShaderConstants_s$Item;->layer0:Landroid/renderscript/Float4;

    new-instance v0, Landroid/renderscript/Float4;

    invoke-direct {v0}, Landroid/renderscript/Float4;-><init>()V

    iput-object v0, p0, Lcom/android/magicsmoke/ScriptField_VertexShaderConstants_s$Item;->layer1:Landroid/renderscript/Float4;

    new-instance v0, Landroid/renderscript/Float4;

    invoke-direct {v0}, Landroid/renderscript/Float4;-><init>()V

    iput-object v0, p0, Lcom/android/magicsmoke/ScriptField_VertexShaderConstants_s$Item;->layer2:Landroid/renderscript/Float4;

    new-instance v0, Landroid/renderscript/Float4;

    invoke-direct {v0}, Landroid/renderscript/Float4;-><init>()V

    iput-object v0, p0, Lcom/android/magicsmoke/ScriptField_VertexShaderConstants_s$Item;->layer3:Landroid/renderscript/Float4;

    new-instance v0, Landroid/renderscript/Float4;

    invoke-direct {v0}, Landroid/renderscript/Float4;-><init>()V

    iput-object v0, p0, Lcom/android/magicsmoke/ScriptField_VertexShaderConstants_s$Item;->layer4:Landroid/renderscript/Float4;

    new-instance v0, Landroid/renderscript/Float2;

    invoke-direct {v0}, Landroid/renderscript/Float2;-><init>()V

    iput-object v0, p0, Lcom/android/magicsmoke/ScriptField_VertexShaderConstants_s$Item;->panoffset:Landroid/renderscript/Float2;

    return-void
.end method
