.class Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl$2;
.super Ljava/lang/Object;
.source "CommonDeskImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->execStartMsrv(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;Z)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;


# direct methods
.method constructor <init>(Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl$2;->this$0:Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    const/4 v0, 0x1

    sput-boolean v0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->bThreadIsrunning:Z

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl$2;->this$0:Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;

    # getter for: Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->bforcechangesource:Z
    invoke-static {v0}, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->access$1(Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl$2;->this$0:Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl$2;->this$0:Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;

    # getter for: Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->curSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    invoke-static {v1}, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->access$0(Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;)Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->SetInputSource(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)V

    :cond_0
    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl$2;->this$0:Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;

    # getter for: Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->curSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    invoke-static {v0}, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->access$0(Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;)Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_ATV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl$2;->this$0:Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;

    # getter for: Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->context:Landroid/content/Context;
    invoke-static {v0}, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->access$2(Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->getChannelMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;

    move-result-object v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceInputType;->E_FIRST_SERVICE_ATV:Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceInputType;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;->E_DEFAULT:Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;

    invoke-virtual {v0, v1, v2}, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->changeToFirstService(Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceInputType;Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;)Z

    :cond_1
    :goto_0
    const/4 v0, 0x0

    sput-boolean v0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->bThreadIsrunning:Z

    return-void

    :cond_2
    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl$2;->this$0:Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;

    # getter for: Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->curSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    invoke-static {v0}, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->access$0(Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;)Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl$2;->this$0:Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;

    # getter for: Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->context:Landroid/content/Context;
    invoke-static {v0}, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->access$2(Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->getChannelMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;

    move-result-object v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceInputType;->E_FIRST_SERVICE_DTV:Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceInputType;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;->E_DEFAULT:Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;

    invoke-virtual {v0, v1, v2}, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->changeToFirstService(Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceInputType;Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;)Z

    goto :goto_0
.end method
