.class public final enum Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;
.super Ljava/lang/Enum;
.source "DeskTimerEventListener.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EVENT"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;

.field public static final enum EV_DESTROY_COUNTDOWN:Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;

.field public static final enum EV_EPGTIMER_COUNTDOWN:Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;

.field public static final enum EV_EPGTIMER_RECORD_START:Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;

.field public static final enum EV_EPG_TIME_UP:Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;

.field public static final enum EV_LASTMINUTE_WARN:Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;

.field public static final enum EV_OAD_TIMESCAN:Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;

.field public static final enum EV_ONESECOND_BEAT:Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;

.field public static final enum EV_PVR_NOTIFY_RECORD_STOP:Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;

.field public static final enum EV_SIGNAL_LOCK:Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;

.field public static final enum EV_UPDATE_LASTMINUTE:Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;

    const-string v1, "EV_DESTROY_COUNTDOWN"

    invoke-direct {v0, v1, v3}, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;->EV_DESTROY_COUNTDOWN:Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;

    new-instance v0, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;

    const-string v1, "EV_ONESECOND_BEAT"

    invoke-direct {v0, v1, v4}, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;->EV_ONESECOND_BEAT:Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;

    new-instance v0, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;

    const-string v1, "EV_LASTMINUTE_WARN"

    invoke-direct {v0, v1, v5}, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;->EV_LASTMINUTE_WARN:Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;

    new-instance v0, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;

    const-string v1, "EV_UPDATE_LASTMINUTE"

    invoke-direct {v0, v1, v6}, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;->EV_UPDATE_LASTMINUTE:Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;

    new-instance v0, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;

    const-string v1, "EV_SIGNAL_LOCK"

    invoke-direct {v0, v1, v7}, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;->EV_SIGNAL_LOCK:Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;

    new-instance v0, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;

    const-string v1, "EV_EPG_TIME_UP"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;->EV_EPG_TIME_UP:Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;

    new-instance v0, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;

    const-string v1, "EV_EPGTIMER_COUNTDOWN"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;->EV_EPGTIMER_COUNTDOWN:Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;

    new-instance v0, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;

    const-string v1, "EV_EPGTIMER_RECORD_START"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;->EV_EPGTIMER_RECORD_START:Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;

    new-instance v0, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;

    const-string v1, "EV_PVR_NOTIFY_RECORD_STOP"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;->EV_PVR_NOTIFY_RECORD_STOP:Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;

    new-instance v0, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;

    const-string v1, "EV_OAD_TIMESCAN"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;->EV_OAD_TIMESCAN:Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;

    const/16 v0, 0xa

    new-array v0, v0, [Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;

    sget-object v1, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;->EV_DESTROY_COUNTDOWN:Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;

    aput-object v1, v0, v3

    sget-object v1, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;->EV_ONESECOND_BEAT:Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;

    aput-object v1, v0, v4

    sget-object v1, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;->EV_LASTMINUTE_WARN:Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;

    aput-object v1, v0, v5

    sget-object v1, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;->EV_UPDATE_LASTMINUTE:Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;

    aput-object v1, v0, v6

    sget-object v1, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;->EV_SIGNAL_LOCK:Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;->EV_EPG_TIME_UP:Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;->EV_EPGTIMER_COUNTDOWN:Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;->EV_EPGTIMER_RECORD_START:Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;->EV_PVR_NOTIFY_RECORD_STOP:Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;->EV_OAD_TIMESCAN:Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;

    aput-object v2, v0, v1

    sput-object v0, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;->ENUM$VALUES:[Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;
    .locals 1

    const-class v0, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;

    return-object v0
.end method

.method public static values()[Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;->ENUM$VALUES:[Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;

    array-length v1, v0

    new-array v2, v1, [Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
