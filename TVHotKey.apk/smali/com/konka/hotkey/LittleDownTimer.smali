.class public Lcom/konka/hotkey/LittleDownTimer;
.super Ljava/lang/Object;
.source "LittleDownTimer.java"

# interfaces
.implements Ljava/lang/Runnable;


# static fields
.field public static final MSG_TIME_OUT:I = 0x40a

.field public static final TIME_DEFAULT:I = 0x5

.field private static handler:Landroid/os/Handler;

.field private static instance:Lcom/konka/hotkey/LittleDownTimer;

.field private static mbIsStop:Z

.field private static mbIsStopMenu:Z

.field private static miCountMenu:I

.field private static miTotalMenuTime:I


# instance fields
.field private final SLEEP_TIME:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x1

    const/4 v0, 0x5

    sput v0, Lcom/konka/hotkey/LittleDownTimer;->miTotalMenuTime:I

    sget v0, Lcom/konka/hotkey/LittleDownTimer;->miTotalMenuTime:I

    sput v0, Lcom/konka/hotkey/LittleDownTimer;->miCountMenu:I

    sput-boolean v1, Lcom/konka/hotkey/LittleDownTimer;->mbIsStop:Z

    sput-boolean v1, Lcom/konka/hotkey/LittleDownTimer;->mbIsStopMenu:Z

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x3e8

    iput v0, p0, Lcom/konka/hotkey/LittleDownTimer;->SLEEP_TIME:I

    const/4 v0, 0x1

    sput-boolean v0, Lcom/konka/hotkey/LittleDownTimer;->mbIsStop:Z

    const/4 v0, 0x0

    sput-boolean v0, Lcom/konka/hotkey/LittleDownTimer;->mbIsStopMenu:Z

    return-void
.end method

.method public static destroy()V
    .locals 1

    const/4 v0, 0x1

    sput-boolean v0, Lcom/konka/hotkey/LittleDownTimer;->mbIsStop:Z

    return-void
.end method

.method public static getInstance()Lcom/konka/hotkey/LittleDownTimer;
    .locals 1

    sget-object v0, Lcom/konka/hotkey/LittleDownTimer;->instance:Lcom/konka/hotkey/LittleDownTimer;

    if-nez v0, :cond_0

    new-instance v0, Lcom/konka/hotkey/LittleDownTimer;

    invoke-direct {v0}, Lcom/konka/hotkey/LittleDownTimer;-><init>()V

    sput-object v0, Lcom/konka/hotkey/LittleDownTimer;->instance:Lcom/konka/hotkey/LittleDownTimer;

    :cond_0
    sget-object v0, Lcom/konka/hotkey/LittleDownTimer;->instance:Lcom/konka/hotkey/LittleDownTimer;

    return-object v0
.end method

.method static p(Ljava/lang/Object;)V
    .locals 1
    .param p0    # Ljava/lang/Object;

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v0, p0}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    return-void
.end method

.method public static resetMenuTime()V
    .locals 1

    const/4 v0, 0x1

    sput-boolean v0, Lcom/konka/hotkey/LittleDownTimer;->mbIsStopMenu:Z

    sget v0, Lcom/konka/hotkey/LittleDownTimer;->miTotalMenuTime:I

    sput v0, Lcom/konka/hotkey/LittleDownTimer;->miCountMenu:I

    return-void
.end method

.method public static resumeMenuTime()V
    .locals 2

    sget-boolean v0, Lcom/konka/hotkey/LittleDownTimer;->mbIsStop:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    sput-boolean v0, Lcom/konka/hotkey/LittleDownTimer;->mbIsStop:Z

    new-instance v0, Ljava/lang/Thread;

    sget-object v1, Lcom/konka/hotkey/LittleDownTimer;->instance:Lcom/konka/hotkey/LittleDownTimer;

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    :cond_0
    const/4 v0, 0x1

    sput-boolean v0, Lcom/konka/hotkey/LittleDownTimer;->mbIsStopMenu:Z

    sget v0, Lcom/konka/hotkey/LittleDownTimer;->miTotalMenuTime:I

    sput v0, Lcom/konka/hotkey/LittleDownTimer;->miCountMenu:I

    return-void
.end method

.method public static setHandler(Landroid/os/Handler;)V
    .locals 0
    .param p0    # Landroid/os/Handler;

    sput-object p0, Lcom/konka/hotkey/LittleDownTimer;->handler:Landroid/os/Handler;

    return-void
.end method

.method public static setIsStopMenu(Z)V
    .locals 0
    .param p0    # Z

    sput-boolean p0, Lcom/konka/hotkey/LittleDownTimer;->mbIsStopMenu:Z

    return-void
.end method

.method public static setTotalMenuTime(I)V
    .locals 0
    .param p0    # I

    sput p0, Lcom/konka/hotkey/LittleDownTimer;->miTotalMenuTime:I

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    :cond_0
    :goto_0
    sget-boolean v1, Lcom/konka/hotkey/LittleDownTimer;->mbIsStop:Z

    if-eqz v1, :cond_1

    return-void

    :cond_1
    :try_start_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    const-wide/16 v1, 0x3e8

    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    sget-boolean v1, Lcom/konka/hotkey/LittleDownTimer;->mbIsStopMenu:Z

    if-eqz v1, :cond_0

    sget-object v1, Lcom/konka/hotkey/LittleDownTimer;->handler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    sget v1, Lcom/konka/hotkey/LittleDownTimer;->miCountMenu:I

    if-nez v1, :cond_2

    sget-object v1, Lcom/konka/hotkey/LittleDownTimer;->handler:Landroid/os/Handler;

    const/16 v2, 0x40a

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_2
    sget v1, Lcom/konka/hotkey/LittleDownTimer;->miCountMenu:I

    add-int/lit8 v1, v1, -0x1

    sput v1, Lcom/konka/hotkey/LittleDownTimer;->miCountMenu:I

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "count down: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v2, Lcom/konka/hotkey/LittleDownTimer;->miCountMenu:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_1
.end method

.method public start()V
    .locals 1

    sget-boolean v0, Lcom/konka/hotkey/LittleDownTimer;->mbIsStop:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    sput-boolean v0, Lcom/konka/hotkey/LittleDownTimer;->mbIsStop:Z

    new-instance v0, Ljava/lang/Thread;

    invoke-direct {v0, p0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    :cond_0
    const/4 v0, 0x1

    sput-boolean v0, Lcom/konka/hotkey/LittleDownTimer;->mbIsStopMenu:Z

    return-void
.end method
