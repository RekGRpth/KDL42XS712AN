.class public final Lcom/konka/hotkey/R$drawable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/hotkey/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final button_state:I = 0x7f020000

.field public static final freeze:I = 0x7f020001

.field public static final hk_3d_en_depth_offset_bg_selector:I = 0x7f020002

.field public static final hk_3d_en_item_bg_selector:I = 0x7f020003

.field public static final hk_3d_menu_2t3_fbd:I = 0x7f020004

.field public static final hk_3d_menu_2t3_nor:I = 0x7f020005

.field public static final hk_3d_menu_2t3_run:I = 0x7f020006

.field public static final hk_3d_menu_2t3_sel:I = 0x7f020007

.field public static final hk_3d_menu_3t2_fbd:I = 0x7f020008

.field public static final hk_3d_menu_3t2_nor:I = 0x7f020009

.field public static final hk_3d_menu_3t2_run:I = 0x7f02000a

.field public static final hk_3d_menu_3t2_sel:I = 0x7f02000b

.field public static final hk_3d_menu_auto_bg_foot:I = 0x7f02000c

.field public static final hk_3d_menu_auto_bg_head:I = 0x7f02000d

.field public static final hk_3d_menu_auto_bg_nor:I = 0x7f02000e

.field public static final hk_3d_menu_auto_bg_sel:I = 0x7f02000f

.field public static final hk_3d_menu_auto_fbd:I = 0x7f020010

.field public static final hk_3d_menu_auto_nor:I = 0x7f020011

.field public static final hk_3d_menu_auto_run:I = 0x7f020012

.field public static final hk_3d_menu_auto_sel:I = 0x7f020013

.field public static final hk_3d_menu_auto_selector:I = 0x7f020014

.field public static final hk_3d_menu_bg_body:I = 0x7f020015

.field public static final hk_3d_menu_bg_l:I = 0x7f020016

.field public static final hk_3d_menu_bg_r:I = 0x7f020017

.field public static final hk_3d_menu_clar_fbd:I = 0x7f020018

.field public static final hk_3d_menu_clar_nor:I = 0x7f020019

.field public static final hk_3d_menu_clar_run:I = 0x7f02001a

.field public static final hk_3d_menu_clar_sel:I = 0x7f02001b

.field public static final hk_3d_menu_cw_bg_foot:I = 0x7f02001c

.field public static final hk_3d_menu_cw_bg_head:I = 0x7f02001d

.field public static final hk_3d_menu_cw_bg_nor:I = 0x7f02001e

.field public static final hk_3d_menu_cw_bg_sel:I = 0x7f02001f

.field public static final hk_3d_menu_en_bar_style:I = 0x7f020020

.field public static final hk_3d_menu_en_bg_foot:I = 0x7f020021

.field public static final hk_3d_menu_en_bg_head:I = 0x7f020022

.field public static final hk_3d_menu_en_bg_nor:I = 0x7f020023

.field public static final hk_3d_menu_en_bg_sel:I = 0x7f020024

.field public static final hk_3d_menu_en_effect_bar:I = 0x7f020025

.field public static final hk_3d_menu_en_effect_bar_bg:I = 0x7f020026

.field public static final hk_3d_menu_en_effect_bg_nor:I = 0x7f020027

.field public static final hk_3d_menu_en_effect_bg_sel:I = 0x7f020028

.field public static final hk_3d_menu_en_effect_thumb:I = 0x7f020029

.field public static final hk_3d_menu_en_fbd:I = 0x7f02002a

.field public static final hk_3d_menu_en_lister:I = 0x7f02002b

.field public static final hk_3d_menu_en_next_nor:I = 0x7f02002c

.field public static final hk_3d_menu_en_next_sel:I = 0x7f02002d

.field public static final hk_3d_menu_en_nor:I = 0x7f02002e

.field public static final hk_3d_menu_en_pre_nor:I = 0x7f02002f

.field public static final hk_3d_menu_en_pre_sel:I = 0x7f020030

.field public static final hk_3d_menu_en_run:I = 0x7f020031

.field public static final hk_3d_menu_en_sel:I = 0x7f020032

.field public static final hk_3d_menu_lr_fbd:I = 0x7f020033

.field public static final hk_3d_menu_lr_nor:I = 0x7f020034

.field public static final hk_3d_menu_lr_run:I = 0x7f020035

.field public static final hk_3d_menu_lr_sel:I = 0x7f020036

.field public static final hk_3d_menu_off_fbd:I = 0x7f020037

.field public static final hk_3d_menu_off_nor:I = 0x7f020038

.field public static final hk_3d_menu_off_run:I = 0x7f020039

.field public static final hk_3d_menu_off_sel:I = 0x7f02003a

.field public static final hk_3d_menu_ud_fbd:I = 0x7f02003b

.field public static final hk_3d_menu_ud_nor:I = 0x7f02003c

.field public static final hk_3d_menu_ud_run:I = 0x7f02003d

.field public static final hk_3d_menu_ud_sel:I = 0x7f02003e

.field public static final hk_countdown_menu_bg:I = 0x7f02003f

.field public static final hk_dc_intosub_fbd:I = 0x7f020040

.field public static final hk_dc_intosub_nor:I = 0x7f020041

.field public static final hk_dc_intosub_run:I = 0x7f020042

.field public static final hk_dc_intosub_sel:I = 0x7f020043

.field public static final hk_dc_pip_fbd:I = 0x7f020044

.field public static final hk_dc_pip_nor:I = 0x7f020045

.field public static final hk_dc_pip_run:I = 0x7f020046

.field public static final hk_dc_pip_sel:I = 0x7f020047

.field public static final hk_dc_pip_syncwatching_fbd:I = 0x7f020048

.field public static final hk_dc_pip_syncwatching_nor:I = 0x7f020049

.field public static final hk_dc_pip_syncwatching_run:I = 0x7f02004a

.field public static final hk_dc_pip_syncwatching_sel:I = 0x7f02004b

.field public static final hk_dc_pop_fbd:I = 0x7f02004c

.field public static final hk_dc_pop_nor:I = 0x7f02004d

.field public static final hk_dc_pop_run:I = 0x7f02004e

.field public static final hk_dc_pop_sel:I = 0x7f02004f

.field public static final hk_dc_poppip_off_fbd:I = 0x7f020050

.field public static final hk_dc_poppip_off_nor:I = 0x7f020051

.field public static final hk_dc_poppip_off_run:I = 0x7f020052

.field public static final hk_dc_poppip_off_sel:I = 0x7f020053

.field public static final hk_dc_subscr_fbd:I = 0x7f020054

.field public static final hk_dc_subscr_nor:I = 0x7f020055

.field public static final hk_dc_subscr_run:I = 0x7f020056

.field public static final hk_dc_subscr_run_bak:I = 0x7f020057

.field public static final hk_dc_subscr_sel:I = 0x7f020058

.field public static final hk_dc_zbg_3d:I = 0x7f020059

.field public static final hk_dc_zbg_3dpoppip:I = 0x7f02005a

.field public static final hk_icon_exclamation:I = 0x7f02005b

.field public static final hk_order_info_btn_selector:I = 0x7f02005c

.field public static final hk_order_info_menu_bg:I = 0x7f02005d

.field public static final hk_order_info_menu_btn_nor:I = 0x7f02005e

.field public static final hk_order_info_menu_btn_sel:I = 0x7f02005f

.field public static final hk_picture_menu_bg:I = 0x7f020060

.field public static final hk_picture_menu_bg_body:I = 0x7f020061

.field public static final hk_picture_menu_bg_body_sel:I = 0x7f020062

.field public static final hk_picture_menu_bg_l:I = 0x7f020063

.field public static final hk_picture_menu_bg_r:I = 0x7f020064

.field public static final hk_picture_menu_selector:I = 0x7f020065

.field public static final hk_sound_menu_bg_body:I = 0x7f020066

.field public static final hk_sound_menu_bg_body_sel:I = 0x7f020067

.field public static final hk_sound_menu_bg_l:I = 0x7f020068

.field public static final hk_sound_menu_bg_r:I = 0x7f020069

.field public static final hk_volume_menu_bar:I = 0x7f02006a

.field public static final hk_volume_menu_bar_style:I = 0x7f02006b

.field public static final hk_volume_menu_barbg:I = 0x7f02006c

.field public static final hk_volume_menu_bg:I = 0x7f02006d

.field public static final hk_zoom_menu_bg_body:I = 0x7f02006e

.field public static final hk_zoom_menu_bg_body_sel:I = 0x7f02006f

.field public static final hk_zoom_menu_bg_l:I = 0x7f020070

.field public static final hk_zoom_menu_bg_r:I = 0x7f020071

.field public static final hotkey_textview_bg_nor:I = 0x7f020072

.field public static final hotkey_textview_bg_sel:I = 0x7f020073

.field public static final hotkey_textview_selector:I = 0x7f020074

.field public static final ic_launcher:I = 0x7f020075

.field public static final input_item_foot:I = 0x7f020076

.field public static final input_item_head:I = 0x7f020077

.field public static final input_item_nor:I = 0x7f020078

.field public static final input_item_sel:I = 0x7f020079

.field public static final input_menu_item_state:I = 0x7f02007a

.field public static final mute:I = 0x7f02007b

.field public static final picture_mode_img_focus:I = 0x7f02007c


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
