.class Lcom/konka/hotkey/ZoomActivity$KeyListener;
.super Ljava/lang/Object;
.source "ZoomActivity.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/hotkey/ZoomActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "KeyListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/hotkey/ZoomActivity;


# direct methods
.method constructor <init>(Lcom/konka/hotkey/ZoomActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/hotkey/ZoomActivity$KeyListener;->this$0:Lcom/konka/hotkey/ZoomActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 5
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # Landroid/view/KeyEvent;

    const/4 v2, 0x0

    const/4 v1, 0x1

    move-object v0, p1

    check-cast v0, Lcom/konka/hotkey/ZoomActivity$ZoomItem;

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v3

    if-nez v3, :cond_0

    const/16 v3, 0x15

    if-ne v3, p2, :cond_0

    invoke-virtual {v0}, Lcom/konka/hotkey/ZoomActivity$ZoomItem;->getPos()I

    move-result v3

    if-nez v3, :cond_0

    iget-object v2, p0, Lcom/konka/hotkey/ZoomActivity$KeyListener;->this$0:Lcom/konka/hotkey/ZoomActivity;

    # getter for: Lcom/konka/hotkey/ZoomActivity;->mViewItems:[Lcom/konka/hotkey/ZoomActivity$ZoomItem;
    invoke-static {v2}, Lcom/konka/hotkey/ZoomActivity;->access$0(Lcom/konka/hotkey/ZoomActivity;)[Lcom/konka/hotkey/ZoomActivity$ZoomItem;

    move-result-object v2

    iget-object v3, p0, Lcom/konka/hotkey/ZoomActivity$KeyListener;->this$0:Lcom/konka/hotkey/ZoomActivity;

    # getter for: Lcom/konka/hotkey/ZoomActivity;->ITEM_SIZE:I
    invoke-static {v3}, Lcom/konka/hotkey/ZoomActivity;->access$1(Lcom/konka/hotkey/ZoomActivity;)I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    aget-object v2, v2, v3

    invoke-virtual {v2}, Lcom/konka/hotkey/ZoomActivity$ZoomItem;->requestFocus()Z

    :goto_0
    return v1

    :cond_0
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v3

    if-nez v3, :cond_1

    const/16 v3, 0x16

    if-ne v3, p2, :cond_1

    iget-object v3, p0, Lcom/konka/hotkey/ZoomActivity$KeyListener;->this$0:Lcom/konka/hotkey/ZoomActivity;

    # getter for: Lcom/konka/hotkey/ZoomActivity;->ITEM_SIZE:I
    invoke-static {v3}, Lcom/konka/hotkey/ZoomActivity;->access$1(Lcom/konka/hotkey/ZoomActivity;)I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v0}, Lcom/konka/hotkey/ZoomActivity$ZoomItem;->getPos()I

    move-result v4

    if-ne v3, v4, :cond_1

    iget-object v3, p0, Lcom/konka/hotkey/ZoomActivity$KeyListener;->this$0:Lcom/konka/hotkey/ZoomActivity;

    # getter for: Lcom/konka/hotkey/ZoomActivity;->mViewItems:[Lcom/konka/hotkey/ZoomActivity$ZoomItem;
    invoke-static {v3}, Lcom/konka/hotkey/ZoomActivity;->access$0(Lcom/konka/hotkey/ZoomActivity;)[Lcom/konka/hotkey/ZoomActivity$ZoomItem;

    move-result-object v3

    aget-object v2, v3, v2

    invoke-virtual {v2}, Lcom/konka/hotkey/ZoomActivity$ZoomItem;->requestFocus()Z

    goto :goto_0

    :cond_1
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v3

    if-nez v3, :cond_3

    const/16 v3, 0x10e

    if-ne v3, p2, :cond_3

    iget-object v3, p0, Lcom/konka/hotkey/ZoomActivity$KeyListener;->this$0:Lcom/konka/hotkey/ZoomActivity;

    # getter for: Lcom/konka/hotkey/ZoomActivity;->mViewItems:[Lcom/konka/hotkey/ZoomActivity$ZoomItem;
    invoke-static {v3}, Lcom/konka/hotkey/ZoomActivity;->access$0(Lcom/konka/hotkey/ZoomActivity;)[Lcom/konka/hotkey/ZoomActivity$ZoomItem;

    move-result-object v3

    array-length v3, v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v0}, Lcom/konka/hotkey/ZoomActivity$ZoomItem;->getPos()I

    move-result v4

    if-ne v3, v4, :cond_2

    iget-object v3, p0, Lcom/konka/hotkey/ZoomActivity$KeyListener;->this$0:Lcom/konka/hotkey/ZoomActivity;

    # getter for: Lcom/konka/hotkey/ZoomActivity;->mViewItems:[Lcom/konka/hotkey/ZoomActivity$ZoomItem;
    invoke-static {v3}, Lcom/konka/hotkey/ZoomActivity;->access$0(Lcom/konka/hotkey/ZoomActivity;)[Lcom/konka/hotkey/ZoomActivity$ZoomItem;

    move-result-object v3

    aget-object v2, v3, v2

    invoke-virtual {v2}, Lcom/konka/hotkey/ZoomActivity$ZoomItem;->requestFocus()Z

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/konka/hotkey/ZoomActivity$KeyListener;->this$0:Lcom/konka/hotkey/ZoomActivity;

    # getter for: Lcom/konka/hotkey/ZoomActivity;->mViewItems:[Lcom/konka/hotkey/ZoomActivity$ZoomItem;
    invoke-static {v2}, Lcom/konka/hotkey/ZoomActivity;->access$0(Lcom/konka/hotkey/ZoomActivity;)[Lcom/konka/hotkey/ZoomActivity$ZoomItem;

    move-result-object v2

    invoke-virtual {v0}, Lcom/konka/hotkey/ZoomActivity$ZoomItem;->getPos()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    aget-object v2, v2, v3

    invoke-virtual {v2}, Lcom/konka/hotkey/ZoomActivity$ZoomItem;->requestFocus()Z

    goto :goto_0

    :cond_3
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v3

    if-nez v3, :cond_4

    const/4 v3, 0x4

    if-ne v3, p2, :cond_4

    iget-object v2, p0, Lcom/konka/hotkey/ZoomActivity$KeyListener;->this$0:Lcom/konka/hotkey/ZoomActivity;

    invoke-virtual {v2}, Lcom/konka/hotkey/ZoomActivity;->finish()V

    goto :goto_0

    :cond_4
    move v1, v2

    goto :goto_0
.end method
