.class public Lcom/konka/hotkey/ZoomActivity;
.super Landroid/app/Activity;
.source "ZoomActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/hotkey/ZoomActivity$ClickListener;,
        Lcom/konka/hotkey/ZoomActivity$FocusChangeListener;,
        Lcom/konka/hotkey/ZoomActivity$KeyListener;,
        Lcom/konka/hotkey/ZoomActivity$ZoomItem;
    }
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$mstar$android$tvapi$common$vo$TvOsType$EnumInputSource:[I

.field public static final OSD_MODE_HDMI:[Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

.field public static final OSD_MODE_TV:[Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

.field public static final OSD_MODE_USB:[Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

.field public static final OSD_MODE_VGA:[Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

.field public static final OSD_MODE_YPBPR:[Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;


# instance fields
.field private CURRENT_LANGUAGE:Ljava/lang/String;

.field private IMG_HEIGHT:I

.field private IMG_WIDTH:I

.field private ITEM_SIZE:I

.field private final LANGUAGE_EN:Ljava/lang/String;

.field private final LANGUAGE_ZH:Ljava/lang/String;

.field private SIZE_NOR:F

.field private SIZE_SEL:F

.field arcModes:[Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

.field private commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

.field private commonSkin:Lcom/mstar/android/tv/TvCommonManager;

.field private curPicMod:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;

.field private mInputMode:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

.field private mViewItems:[Lcom/konka/hotkey/ZoomActivity$ZoomItem;

.field private mViewLlContainer:Landroid/widget/LinearLayout;

.field private meArcType:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

.field private meMode:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

.field private myHandler:Landroid/os/Handler;

.field private pictureDesk:Lcom/konka/kkinterface/tv/PictureDesk;

.field private pictureSkin:Lcom/mstar/android/tv/TvPictureManager;

.field private serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

.field private final strModeHDMIId:[I

.field private final strModeHDMIId_Interlaced:[I

.field private final strModeTVId:[I

.field private final strModeUSBId:[I

.field private final strModeVGAId:[I

.field private final strModeYPbPrId:[I


# direct methods
.method static synthetic $SWITCH_TABLE$com$mstar$android$tvapi$common$vo$TvOsType$EnumInputSource()[I
    .locals 3

    sget-object v0, Lcom/konka/hotkey/ZoomActivity;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$TvOsType$EnumInputSource:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_ATV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_28

    :goto_1
    :try_start_1
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_27

    :goto_2
    :try_start_2
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_26

    :goto_3
    :try_start_3
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS3:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_25

    :goto_4
    :try_start_4
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS4:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_24

    :goto_5
    :try_start_5
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS5:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_23

    :goto_6
    :try_start_6
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS6:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_22

    :goto_7
    :try_start_7
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS7:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_21

    :goto_8
    :try_start_8
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS8:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_20

    :goto_9
    :try_start_9
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS_MAX:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0xb

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_1f

    :goto_a
    :try_start_a
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x1d

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_1e

    :goto_b
    :try_start_b
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x26

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_1d

    :goto_c
    :try_start_c
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DVI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x1e

    aput v2, v0, v1
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_1c

    :goto_d
    :try_start_d
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DVI2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x1f

    aput v2, v0, v1
    :try_end_d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d .. :try_end_d} :catch_1b

    :goto_e
    :try_start_e
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DVI3:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x20

    aput v2, v0, v1
    :try_end_e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_e .. :try_end_e} :catch_1a

    :goto_f
    :try_start_f
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DVI4:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x21

    aput v2, v0, v1
    :try_end_f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_f .. :try_end_f} :catch_19

    :goto_10
    :try_start_10
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DVI_MAX:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x22

    aput v2, v0, v1
    :try_end_10
    .catch Ljava/lang/NoSuchFieldError; {:try_start_10 .. :try_end_10} :catch_18

    :goto_11
    :try_start_11
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x18

    aput v2, v0, v1
    :try_end_11
    .catch Ljava/lang/NoSuchFieldError; {:try_start_11 .. :try_end_11} :catch_17

    :goto_12
    :try_start_12
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x19

    aput v2, v0, v1
    :try_end_12
    .catch Ljava/lang/NoSuchFieldError; {:try_start_12 .. :try_end_12} :catch_16

    :goto_13
    :try_start_13
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI3:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x1a

    aput v2, v0, v1
    :try_end_13
    .catch Ljava/lang/NoSuchFieldError; {:try_start_13 .. :try_end_13} :catch_15

    :goto_14
    :try_start_14
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI4:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x1b

    aput v2, v0, v1
    :try_end_14
    .catch Ljava/lang/NoSuchFieldError; {:try_start_14 .. :try_end_14} :catch_14

    :goto_15
    :try_start_15
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI_MAX:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x1c

    aput v2, v0, v1
    :try_end_15
    .catch Ljava/lang/NoSuchFieldError; {:try_start_15 .. :try_end_15} :catch_13

    :goto_16
    :try_start_16
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_JPEG:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x25

    aput v2, v0, v1
    :try_end_16
    .catch Ljava/lang/NoSuchFieldError; {:try_start_16 .. :try_end_16} :catch_12

    :goto_17
    :try_start_17
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_KTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x24

    aput v2, v0, v1
    :try_end_17
    .catch Ljava/lang/NoSuchFieldError; {:try_start_17 .. :try_end_17} :catch_11

    :goto_18
    :try_start_18
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_NONE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x29

    aput v2, v0, v1
    :try_end_18
    .catch Ljava/lang/NoSuchFieldError; {:try_start_18 .. :try_end_18} :catch_10

    :goto_19
    :try_start_19
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_NUM:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x28

    aput v2, v0, v1
    :try_end_19
    .catch Ljava/lang/NoSuchFieldError; {:try_start_19 .. :try_end_19} :catch_f

    :goto_1a
    :try_start_1a
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_SCART:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x15

    aput v2, v0, v1
    :try_end_1a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1a .. :try_end_1a} :catch_e

    :goto_1b
    :try_start_1b
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_SCART2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x16

    aput v2, v0, v1
    :try_end_1b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1b .. :try_end_1b} :catch_d

    :goto_1c
    :try_start_1c
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_SCART_MAX:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x17

    aput v2, v0, v1
    :try_end_1c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1c .. :try_end_1c} :catch_c

    :goto_1d
    :try_start_1d
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_STORAGE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x23

    aput v2, v0, v1
    :try_end_1d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1d .. :try_end_1d} :catch_b

    :goto_1e
    :try_start_1e
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_STORAGE2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x27

    aput v2, v0, v1
    :try_end_1e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1e .. :try_end_1e} :catch_a

    :goto_1f
    :try_start_1f
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_SVIDEO:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0xc

    aput v2, v0, v1
    :try_end_1f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1f .. :try_end_1f} :catch_9

    :goto_20
    :try_start_20
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_SVIDEO2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0xd

    aput v2, v0, v1
    :try_end_20
    .catch Ljava/lang/NoSuchFieldError; {:try_start_20 .. :try_end_20} :catch_8

    :goto_21
    :try_start_21
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_SVIDEO3:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0xe

    aput v2, v0, v1
    :try_end_21
    .catch Ljava/lang/NoSuchFieldError; {:try_start_21 .. :try_end_21} :catch_7

    :goto_22
    :try_start_22
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_SVIDEO4:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0xf

    aput v2, v0, v1
    :try_end_22
    .catch Ljava/lang/NoSuchFieldError; {:try_start_22 .. :try_end_22} :catch_6

    :goto_23
    :try_start_23
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_SVIDEO_MAX:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x10

    aput v2, v0, v1
    :try_end_23
    .catch Ljava/lang/NoSuchFieldError; {:try_start_23 .. :try_end_23} :catch_5

    :goto_24
    :try_start_24
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_VGA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_24
    .catch Ljava/lang/NoSuchFieldError; {:try_start_24 .. :try_end_24} :catch_4

    :goto_25
    :try_start_25
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_YPBPR:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x11

    aput v2, v0, v1
    :try_end_25
    .catch Ljava/lang/NoSuchFieldError; {:try_start_25 .. :try_end_25} :catch_3

    :goto_26
    :try_start_26
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_YPBPR2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x12

    aput v2, v0, v1
    :try_end_26
    .catch Ljava/lang/NoSuchFieldError; {:try_start_26 .. :try_end_26} :catch_2

    :goto_27
    :try_start_27
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_YPBPR3:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x13

    aput v2, v0, v1
    :try_end_27
    .catch Ljava/lang/NoSuchFieldError; {:try_start_27 .. :try_end_27} :catch_1

    :goto_28
    :try_start_28
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_YPBPR_MAX:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x14

    aput v2, v0, v1
    :try_end_28
    .catch Ljava/lang/NoSuchFieldError; {:try_start_28 .. :try_end_28} :catch_0

    :goto_29
    sput-object v0, Lcom/konka/hotkey/ZoomActivity;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$TvOsType$EnumInputSource:[I

    goto/16 :goto_0

    :catch_0
    move-exception v1

    goto :goto_29

    :catch_1
    move-exception v1

    goto :goto_28

    :catch_2
    move-exception v1

    goto :goto_27

    :catch_3
    move-exception v1

    goto :goto_26

    :catch_4
    move-exception v1

    goto :goto_25

    :catch_5
    move-exception v1

    goto :goto_24

    :catch_6
    move-exception v1

    goto :goto_23

    :catch_7
    move-exception v1

    goto :goto_22

    :catch_8
    move-exception v1

    goto :goto_21

    :catch_9
    move-exception v1

    goto :goto_20

    :catch_a
    move-exception v1

    goto :goto_1f

    :catch_b
    move-exception v1

    goto/16 :goto_1e

    :catch_c
    move-exception v1

    goto/16 :goto_1d

    :catch_d
    move-exception v1

    goto/16 :goto_1c

    :catch_e
    move-exception v1

    goto/16 :goto_1b

    :catch_f
    move-exception v1

    goto/16 :goto_1a

    :catch_10
    move-exception v1

    goto/16 :goto_19

    :catch_11
    move-exception v1

    goto/16 :goto_18

    :catch_12
    move-exception v1

    goto/16 :goto_17

    :catch_13
    move-exception v1

    goto/16 :goto_16

    :catch_14
    move-exception v1

    goto/16 :goto_15

    :catch_15
    move-exception v1

    goto/16 :goto_14

    :catch_16
    move-exception v1

    goto/16 :goto_13

    :catch_17
    move-exception v1

    goto/16 :goto_12

    :catch_18
    move-exception v1

    goto/16 :goto_11

    :catch_19
    move-exception v1

    goto/16 :goto_10

    :catch_1a
    move-exception v1

    goto/16 :goto_f

    :catch_1b
    move-exception v1

    goto/16 :goto_e

    :catch_1c
    move-exception v1

    goto/16 :goto_d

    :catch_1d
    move-exception v1

    goto/16 :goto_c

    :catch_1e
    move-exception v1

    goto/16 :goto_b

    :catch_1f
    move-exception v1

    goto/16 :goto_a

    :catch_20
    move-exception v1

    goto/16 :goto_9

    :catch_21
    move-exception v1

    goto/16 :goto_8

    :catch_22
    move-exception v1

    goto/16 :goto_7

    :catch_23
    move-exception v1

    goto/16 :goto_6

    :catch_24
    move-exception v1

    goto/16 :goto_5

    :catch_25
    move-exception v1

    goto/16 :goto_4

    :catch_26
    move-exception v1

    goto/16 :goto_3

    :catch_27
    move-exception v1

    goto/16 :goto_2

    :catch_28
    move-exception v1

    goto/16 :goto_1
.end method

.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_16x9:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    aput-object v1, v0, v2

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_4x3:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    aput-object v1, v0, v3

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_Panorama:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    aput-object v1, v0, v4

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_Zoom1:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    aput-object v1, v0, v5

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_Zoom2:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    aput-object v1, v0, v6

    sput-object v0, Lcom/konka/hotkey/ZoomActivity;->OSD_MODE_TV:[Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    new-array v0, v5, [Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_16x9:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    aput-object v1, v0, v2

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_4x3:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    aput-object v1, v0, v3

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_Panorama:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    aput-object v1, v0, v4

    sput-object v0, Lcom/konka/hotkey/ZoomActivity;->OSD_MODE_YPBPR:[Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    new-array v0, v5, [Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_16x9:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    aput-object v1, v0, v2

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_4x3:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    aput-object v1, v0, v3

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_DotByDot:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    aput-object v1, v0, v4

    sput-object v0, Lcom/konka/hotkey/ZoomActivity;->OSD_MODE_VGA:[Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    new-array v0, v6, [Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_16x9:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    aput-object v1, v0, v2

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_4x3:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    aput-object v1, v0, v3

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_Panorama:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    aput-object v1, v0, v4

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_DotByDot:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    aput-object v1, v0, v5

    sput-object v0, Lcom/konka/hotkey/ZoomActivity;->OSD_MODE_HDMI:[Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    new-array v0, v5, [Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_16x9:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    aput-object v1, v0, v2

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_4x3:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    aput-object v1, v0, v3

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_Panorama:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    aput-object v1, v0, v4

    sput-object v0, Lcom/konka/hotkey/ZoomActivity;->OSD_MODE_USB:[Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x3

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_ATV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    iput-object v0, p0, Lcom/konka/hotkey/ZoomActivity;->mInputMode:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    sget-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_16x9:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    iput-object v0, p0, Lcom/konka/hotkey/ZoomActivity;->meMode:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    const/16 v0, 0x87

    iput v0, p0, Lcom/konka/hotkey/ZoomActivity;->IMG_WIDTH:I

    const/16 v0, 0x86

    iput v0, p0, Lcom/konka/hotkey/ZoomActivity;->IMG_HEIGHT:I

    const-string v0, "en"

    iput-object v0, p0, Lcom/konka/hotkey/ZoomActivity;->LANGUAGE_EN:Ljava/lang/String;

    const-string v0, "zh"

    iput-object v0, p0, Lcom/konka/hotkey/ZoomActivity;->LANGUAGE_ZH:Ljava/lang/String;

    iput-object v2, p0, Lcom/konka/hotkey/ZoomActivity;->pictureDesk:Lcom/konka/kkinterface/tv/PictureDesk;

    iput-object v2, p0, Lcom/konka/hotkey/ZoomActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    new-instance v0, Lcom/konka/hotkey/ZoomActivity$1;

    invoke-direct {v0, p0}, Lcom/konka/hotkey/ZoomActivity$1;-><init>(Lcom/konka/hotkey/ZoomActivity;)V

    iput-object v0, p0, Lcom/konka/hotkey/ZoomActivity;->myHandler:Landroid/os/Handler;

    sget-object v0, Lcom/konka/hotkey/ZoomActivity;->OSD_MODE_TV:[Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    iput-object v0, p0, Lcom/konka/hotkey/ZoomActivity;->arcModes:[Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/konka/hotkey/ZoomActivity;->strModeTVId:[I

    new-array v0, v1, [I

    fill-array-data v0, :array_1

    iput-object v0, p0, Lcom/konka/hotkey/ZoomActivity;->strModeYPbPrId:[I

    new-array v0, v1, [I

    fill-array-data v0, :array_2

    iput-object v0, p0, Lcom/konka/hotkey/ZoomActivity;->strModeVGAId:[I

    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_3

    iput-object v0, p0, Lcom/konka/hotkey/ZoomActivity;->strModeHDMIId:[I

    new-array v0, v1, [I

    fill-array-data v0, :array_4

    iput-object v0, p0, Lcom/konka/hotkey/ZoomActivity;->strModeHDMIId_Interlaced:[I

    new-array v0, v1, [I

    fill-array-data v0, :array_5

    iput-object v0, p0, Lcom/konka/hotkey/ZoomActivity;->strModeUSBId:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x7f080015    # com.konka.hotkey.R.string.str_hk_zoom_menu_16x9
        0x7f080016    # com.konka.hotkey.R.string.str_hk_zoom_menu_4x3
        0x7f080018    # com.konka.hotkey.R.string.str_hk_zoom_menu_panorama
        0x7f080019    # com.konka.hotkey.R.string.str_hk_zoom_menu_movie
        0x7f08001a    # com.konka.hotkey.R.string.str_hk_zoom_menu_Subtitle
    .end array-data

    :array_1
    .array-data 4
        0x7f080015    # com.konka.hotkey.R.string.str_hk_zoom_menu_16x9
        0x7f080016    # com.konka.hotkey.R.string.str_hk_zoom_menu_4x3
        0x7f080018    # com.konka.hotkey.R.string.str_hk_zoom_menu_panorama
    .end array-data

    :array_2
    .array-data 4
        0x7f080015    # com.konka.hotkey.R.string.str_hk_zoom_menu_16x9
        0x7f080016    # com.konka.hotkey.R.string.str_hk_zoom_menu_4x3
        0x7f08001b    # com.konka.hotkey.R.string.str_hk_zoom_menu_dotbydot
    .end array-data

    :array_3
    .array-data 4
        0x7f080015    # com.konka.hotkey.R.string.str_hk_zoom_menu_16x9
        0x7f080016    # com.konka.hotkey.R.string.str_hk_zoom_menu_4x3
        0x7f080018    # com.konka.hotkey.R.string.str_hk_zoom_menu_panorama
        0x7f08001b    # com.konka.hotkey.R.string.str_hk_zoom_menu_dotbydot
    .end array-data

    :array_4
    .array-data 4
        0x7f080015    # com.konka.hotkey.R.string.str_hk_zoom_menu_16x9
        0x7f080016    # com.konka.hotkey.R.string.str_hk_zoom_menu_4x3
        0x7f080018    # com.konka.hotkey.R.string.str_hk_zoom_menu_panorama
    .end array-data

    :array_5
    .array-data 4
        0x7f080015    # com.konka.hotkey.R.string.str_hk_zoom_menu_16x9
        0x7f080016    # com.konka.hotkey.R.string.str_hk_zoom_menu_4x3
        0x7f080018    # com.konka.hotkey.R.string.str_hk_zoom_menu_panorama
    .end array-data
.end method

.method static synthetic access$0(Lcom/konka/hotkey/ZoomActivity;)[Lcom/konka/hotkey/ZoomActivity$ZoomItem;
    .locals 1

    iget-object v0, p0, Lcom/konka/hotkey/ZoomActivity;->mViewItems:[Lcom/konka/hotkey/ZoomActivity$ZoomItem;

    return-object v0
.end method

.method static synthetic access$1(Lcom/konka/hotkey/ZoomActivity;)I
    .locals 1

    iget v0, p0, Lcom/konka/hotkey/ZoomActivity;->ITEM_SIZE:I

    return v0
.end method

.method static synthetic access$2(Lcom/konka/hotkey/ZoomActivity;)Lcom/mstar/android/tv/TvPictureManager;
    .locals 1

    iget-object v0, p0, Lcom/konka/hotkey/ZoomActivity;->pictureSkin:Lcom/mstar/android/tv/TvPictureManager;

    return-object v0
.end method

.method static synthetic access$3(Lcom/konka/hotkey/ZoomActivity;)F
    .locals 1

    iget v0, p0, Lcom/konka/hotkey/ZoomActivity;->SIZE_SEL:F

    return v0
.end method

.method static synthetic access$4(Lcom/konka/hotkey/ZoomActivity;)F
    .locals 1

    iget v0, p0, Lcom/konka/hotkey/ZoomActivity;->SIZE_NOR:F

    return v0
.end method

.method static synthetic access$5(Lcom/konka/hotkey/ZoomActivity;)Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    .locals 1

    iget-object v0, p0, Lcom/konka/hotkey/ZoomActivity;->mInputMode:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    return-object v0
.end method

.method static synthetic access$6(Lcom/konka/hotkey/ZoomActivity;)Lcom/konka/kkinterface/tv/PictureDesk;
    .locals 1

    iget-object v0, p0, Lcom/konka/hotkey/ZoomActivity;->pictureDesk:Lcom/konka/kkinterface/tv/PictureDesk;

    return-object v0
.end method

.method static synthetic access$7(Lcom/konka/hotkey/ZoomActivity;)Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;
    .locals 1

    iget-object v0, p0, Lcom/konka/hotkey/ZoomActivity;->curPicMod:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;

    return-object v0
.end method

.method private findFocus(Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;)V
    .locals 6
    .param p1    # Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p1, :cond_0

    iget-object v1, p0, Lcom/konka/hotkey/ZoomActivity;->mViewItems:[Lcom/konka/hotkey/ZoomActivity$ZoomItem;

    if-eqz v1, :cond_1

    const-string v1, "in find focus mode is null"

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/hotkey/ZoomActivity;->mViewItems:[Lcom/konka/hotkey/ZoomActivity$ZoomItem;

    aget-object v1, v1, v4

    invoke-virtual {v1}, Lcom/konka/hotkey/ZoomActivity$ZoomItem;->requestFocus()Z

    :cond_0
    :goto_0
    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, Lcom/konka/hotkey/ZoomActivity;->mViewItems:[Lcom/konka/hotkey/ZoomActivity$ZoomItem;

    array-length v1, v1

    if-lt v0, v1, :cond_2

    const-string v1, "ZoomActiviy"

    const-string v2, "Focus on  item 0"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/konka/hotkey/ZoomActivity;->mViewItems:[Lcom/konka/hotkey/ZoomActivity$ZoomItem;

    aget-object v1, v1, v4

    invoke-virtual {v1}, Lcom/konka/hotkey/ZoomActivity$ZoomItem;->requestFocus()Z

    iget-object v1, p0, Lcom/konka/hotkey/ZoomActivity;->mViewItems:[Lcom/konka/hotkey/ZoomActivity$ZoomItem;

    aget-object v1, v1, v4

    invoke-virtual {v1, v5}, Lcom/konka/hotkey/ZoomActivity$ZoomItem;->setRunning(Z)V

    iget-object v1, p0, Lcom/konka/hotkey/ZoomActivity;->mViewItems:[Lcom/konka/hotkey/ZoomActivity$ZoomItem;

    aget-object v1, v1, v4

    iget v2, p0, Lcom/konka/hotkey/ZoomActivity;->SIZE_SEL:F

    invoke-virtual {v1, v2}, Lcom/konka/hotkey/ZoomActivity$ZoomItem;->setSelBg(F)V

    :goto_2
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/konka/hotkey/ZoomActivity;->finish()V

    goto :goto_0

    :cond_2
    const-string v1, "ZoomActiviy"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "item "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " Mode is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/konka/hotkey/ZoomActivity;->mViewItems:[Lcom/konka/hotkey/ZoomActivity$ZoomItem;

    aget-object v3, v3, v0

    iget-object v3, v3, Lcom/konka/hotkey/ZoomActivity$ZoomItem;->meMode:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/konka/hotkey/ZoomActivity;->mViewItems:[Lcom/konka/hotkey/ZoomActivity$ZoomItem;

    aget-object v1, v1, v0

    iget-object v1, v1, Lcom/konka/hotkey/ZoomActivity$ZoomItem;->meMode:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    if-ne p1, v1, :cond_3

    const-string v1, "ZoomActiviy"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Current meArcType equals to item "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/konka/hotkey/ZoomActivity;->mViewItems:[Lcom/konka/hotkey/ZoomActivity$ZoomItem;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/konka/hotkey/ZoomActivity$ZoomItem;->requestFocus()Z

    iget-object v1, p0, Lcom/konka/hotkey/ZoomActivity;->mViewItems:[Lcom/konka/hotkey/ZoomActivity$ZoomItem;

    aget-object v1, v1, v0

    invoke-virtual {v1, v5}, Lcom/konka/hotkey/ZoomActivity$ZoomItem;->setRunning(Z)V

    iget-object v1, p0, Lcom/konka/hotkey/ZoomActivity;->mViewItems:[Lcom/konka/hotkey/ZoomActivity$ZoomItem;

    aget-object v1, v1, v0

    iget v2, p0, Lcom/konka/hotkey/ZoomActivity;->SIZE_SEL:F

    invoke-virtual {v1, v2}, Lcom/konka/hotkey/ZoomActivity$ZoomItem;->setSelBg(F)V

    goto :goto_2

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_1
.end method

.method private listenerInit()V
    .locals 5

    new-instance v1, Lcom/konka/hotkey/ZoomActivity$FocusChangeListener;

    invoke-direct {v1, p0}, Lcom/konka/hotkey/ZoomActivity$FocusChangeListener;-><init>(Lcom/konka/hotkey/ZoomActivity;)V

    new-instance v0, Lcom/konka/hotkey/ZoomActivity$ClickListener;

    invoke-direct {v0, p0}, Lcom/konka/hotkey/ZoomActivity$ClickListener;-><init>(Lcom/konka/hotkey/ZoomActivity;)V

    new-instance v3, Lcom/konka/hotkey/ZoomActivity$KeyListener;

    invoke-direct {v3, p0}, Lcom/konka/hotkey/ZoomActivity$KeyListener;-><init>(Lcom/konka/hotkey/ZoomActivity;)V

    const/4 v2, 0x0

    :goto_0
    iget v4, p0, Lcom/konka/hotkey/ZoomActivity;->ITEM_SIZE:I

    if-lt v2, v4, :cond_0

    return-void

    :cond_0
    iget-object v4, p0, Lcom/konka/hotkey/ZoomActivity;->mViewItems:[Lcom/konka/hotkey/ZoomActivity$ZoomItem;

    aget-object v4, v4, v2

    invoke-virtual {v4, v0}, Lcom/konka/hotkey/ZoomActivity$ZoomItem;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v4, p0, Lcom/konka/hotkey/ZoomActivity;->mViewItems:[Lcom/konka/hotkey/ZoomActivity$ZoomItem;

    aget-object v4, v4, v2

    invoke-virtual {v4, v1}, Lcom/konka/hotkey/ZoomActivity$ZoomItem;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v4, p0, Lcom/konka/hotkey/ZoomActivity;->mViewItems:[Lcom/konka/hotkey/ZoomActivity$ZoomItem;

    aget-object v4, v4, v2

    invoke-virtual {v4, v3}, Lcom/konka/hotkey/ZoomActivity$ZoomItem;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f030004    # com.konka.hotkey.R.layout.hk_sound_menu

    invoke-virtual {p0, v0}, Lcom/konka/hotkey/ZoomActivity;->setContentView(I)V

    const v0, 0x7f0a0011    # com.konka.hotkey.R.id.hk_sound_menu_container

    invoke-virtual {p0, v0}, Lcom/konka/hotkey/ZoomActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/hotkey/ZoomActivity;->mViewLlContainer:Landroid/widget/LinearLayout;

    invoke-static {}, Lcom/mstar/android/tv/TvCommonManager;->getInstance()Lcom/mstar/android/tv/TvCommonManager;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/hotkey/ZoomActivity;->commonSkin:Lcom/mstar/android/tv/TvCommonManager;

    invoke-virtual {p0}, Lcom/konka/hotkey/ZoomActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->getPictureMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/hotkey/ZoomActivity;->pictureDesk:Lcom/konka/kkinterface/tv/PictureDesk;

    invoke-static {p0}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/hotkey/ZoomActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    iget-object v0, p0, Lcom/konka/hotkey/ZoomActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/hotkey/ZoomActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-static {}, Lcom/mstar/android/tv/TvPictureManager;->getInstance()Lcom/mstar/android/tv/TvPictureManager;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/hotkey/ZoomActivity;->pictureSkin:Lcom/mstar/android/tv/TvPictureManager;

    iget-object v0, p0, Lcom/konka/hotkey/ZoomActivity;->pictureDesk:Lcom/konka/kkinterface/tv/PictureDesk;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/PictureDesk;->GetVideoArc()Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/hotkey/ZoomActivity;->meMode:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-static {}, Lcom/mstar/android/tv/TvCommonManager;->getInstance()Lcom/mstar/android/tv/TvCommonManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tv/TvCommonManager;->getCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/hotkey/ZoomActivity;->mInputMode:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {p0}, Lcom/konka/hotkey/ZoomActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070008    # com.konka.hotkey.R.dimen.TVHotkeyZoomModeItem_Width

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/konka/hotkey/ZoomActivity;->IMG_WIDTH:I

    invoke-virtual {p0}, Lcom/konka/hotkey/ZoomActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070009    # com.konka.hotkey.R.dimen.TVHotkeyZoomModeItem_Height

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/konka/hotkey/ZoomActivity;->IMG_HEIGHT:I

    invoke-virtual {p0}, Lcom/konka/hotkey/ZoomActivity;->viewInit()V

    invoke-direct {p0}, Lcom/konka/hotkey/ZoomActivity;->listenerInit()V

    iget-object v0, p0, Lcom/konka/hotkey/ZoomActivity;->mViewItems:[Lcom/konka/hotkey/ZoomActivity$ZoomItem;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/konka/hotkey/ZoomActivity$ZoomItem;->requestFocus()Z

    iget-object v0, p0, Lcom/konka/hotkey/ZoomActivity;->pictureDesk:Lcom/konka/kkinterface/tv/PictureDesk;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/PictureDesk;->GetVideoArc()Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/hotkey/ZoomActivity;->meArcType:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    iget-object v0, p0, Lcom/konka/hotkey/ZoomActivity;->meArcType:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-direct {p0, v0}, Lcom/konka/hotkey/ZoomActivity;->findFocus(Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;)V

    invoke-static {}, Lcom/konka/hotkey/LittleDownTimer;->getInstance()Lcom/konka/hotkey/LittleDownTimer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/hotkey/LittleDownTimer;->start()V

    return-void
.end method

.method protected onDestroy()V
    .locals 0

    invoke-static {}, Lcom/konka/hotkey/LittleDownTimer;->destroy()V

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method protected onResume()V
    .locals 3

    iget-object v0, p0, Lcom/konka/hotkey/ZoomActivity;->pictureDesk:Lcom/konka/kkinterface/tv/PictureDesk;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/PictureDesk;->GetVideoArc()Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/hotkey/ZoomActivity;->meArcType:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    iget-object v0, p0, Lcom/konka/hotkey/ZoomActivity;->meArcType:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-direct {p0, v0}, Lcom/konka/hotkey/ZoomActivity;->findFocus(Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;)V

    const-string v0, "ZoomActiviy"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Current meArcType"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/hotkey/ZoomActivity;->meArcType:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/hotkey/ZoomActivity;->myHandler:Landroid/os/Handler;

    invoke-static {v0}, Lcom/konka/hotkey/LittleDownTimer;->setHandler(Landroid/os/Handler;)V

    invoke-static {}, Lcom/konka/hotkey/LittleDownTimer;->resumeMenuTime()V

    iget-object v0, p0, Lcom/konka/hotkey/ZoomActivity;->pictureDesk:Lcom/konka/kkinterface/tv/PictureDesk;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/PictureDesk;->GetPictureModeIdx()Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/hotkey/ZoomActivity;->curPicMod:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    return-void
.end method

.method protected onStop()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    invoke-virtual {p0}, Lcom/konka/hotkey/ZoomActivity;->finish()V

    return-void
.end method

.method public onUserInteraction()V
    .locals 0

    invoke-static {}, Lcom/konka/hotkey/LittleDownTimer;->resetMenuTime()V

    invoke-super {p0}, Landroid/app/Activity;->onUserInteraction()V

    return-void
.end method

.method public recoverItemNorBg()V
    .locals 3

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/konka/hotkey/ZoomActivity;->mViewItems:[Lcom/konka/hotkey/ZoomActivity$ZoomItem;

    array-length v1, v1

    if-lt v0, v1, :cond_0

    :goto_1
    return-void

    :cond_0
    iget-object v1, p0, Lcom/konka/hotkey/ZoomActivity;->mViewItems:[Lcom/konka/hotkey/ZoomActivity$ZoomItem;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/konka/hotkey/ZoomActivity$ZoomItem;->isRunning()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/konka/hotkey/ZoomActivity;->mViewItems:[Lcom/konka/hotkey/ZoomActivity$ZoomItem;

    aget-object v1, v1, v0

    iget v2, p0, Lcom/konka/hotkey/ZoomActivity;->SIZE_NOR:F

    invoke-virtual {v1, v2}, Lcom/konka/hotkey/ZoomActivity$ZoomItem;->setNorBg(F)V

    iget-object v1, p0, Lcom/konka/hotkey/ZoomActivity;->mViewItems:[Lcom/konka/hotkey/ZoomActivity$ZoomItem;

    aget-object v1, v1, v0

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/konka/hotkey/ZoomActivity$ZoomItem;->setRunning(Z)V

    goto :goto_1

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public viewInit()V
    .locals 11

    const/high16 v3, 0x41900000    # 18.0f

    const/high16 v2, 0x41800000    # 16.0f

    invoke-virtual {p0}, Lcom/konka/hotkey/ZoomActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080003    # com.konka.hotkey.R.string.str_sys_language

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/hotkey/ZoomActivity;->CURRENT_LANGUAGE:Ljava/lang/String;

    const-string v0, "en"

    iget-object v1, p0, Lcom/konka/hotkey/ZoomActivity;->CURRENT_LANGUAGE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iput v2, p0, Lcom/konka/hotkey/ZoomActivity;->SIZE_NOR:F

    iput v3, p0, Lcom/konka/hotkey/ZoomActivity;->SIZE_SEL:F

    :goto_0
    const/4 v9, 0x0

    invoke-static {}, Lcom/konka/hotkey/ZoomActivity;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$TvOsType$EnumInputSource()[I

    move-result-object v0

    iget-object v1, p0, Lcom/konka/hotkey/ZoomActivity;->mInputMode:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_1
    :pswitch_0
    array-length v0, v9

    new-array v0, v0, [Lcom/konka/hotkey/ZoomActivity$ZoomItem;

    iput-object v0, p0, Lcom/konka/hotkey/ZoomActivity;->mViewItems:[Lcom/konka/hotkey/ZoomActivity$ZoomItem;

    array-length v0, v9

    iput v0, p0, Lcom/konka/hotkey/ZoomActivity;->ITEM_SIZE:I

    const/4 v7, 0x0

    :goto_2
    iget v0, p0, Lcom/konka/hotkey/ZoomActivity;->ITEM_SIZE:I

    if-lt v7, v0, :cond_1

    const/4 v7, 0x0

    :goto_3
    iget v0, p0, Lcom/konka/hotkey/ZoomActivity;->ITEM_SIZE:I

    if-lt v7, v0, :cond_2

    return-void

    :cond_0
    iput v2, p0, Lcom/konka/hotkey/ZoomActivity;->SIZE_NOR:F

    iput v3, p0, Lcom/konka/hotkey/ZoomActivity;->SIZE_SEL:F

    goto :goto_0

    :pswitch_1
    iget-object v9, p0, Lcom/konka/hotkey/ZoomActivity;->strModeTVId:[I

    sget-object v0, Lcom/konka/hotkey/ZoomActivity;->OSD_MODE_TV:[Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    iput-object v0, p0, Lcom/konka/hotkey/ZoomActivity;->arcModes:[Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    goto :goto_1

    :pswitch_2
    iget-object v9, p0, Lcom/konka/hotkey/ZoomActivity;->strModeYPbPrId:[I

    sget-object v0, Lcom/konka/hotkey/ZoomActivity;->OSD_MODE_YPBPR:[Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    iput-object v0, p0, Lcom/konka/hotkey/ZoomActivity;->arcModes:[Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    goto :goto_1

    :pswitch_3
    iget-object v9, p0, Lcom/konka/hotkey/ZoomActivity;->strModeVGAId:[I

    sget-object v0, Lcom/konka/hotkey/ZoomActivity;->OSD_MODE_VGA:[Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    iput-object v0, p0, Lcom/konka/hotkey/ZoomActivity;->arcModes:[Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    goto :goto_1

    :pswitch_4
    iget-object v9, p0, Lcom/konka/hotkey/ZoomActivity;->strModeHDMIId:[I

    sget-object v0, Lcom/konka/hotkey/ZoomActivity;->OSD_MODE_HDMI:[Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    iput-object v0, p0, Lcom/konka/hotkey/ZoomActivity;->arcModes:[Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    goto :goto_1

    :pswitch_5
    iget-object v9, p0, Lcom/konka/hotkey/ZoomActivity;->strModeUSBId:[I

    sget-object v0, Lcom/konka/hotkey/ZoomActivity;->OSD_MODE_USB:[Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    iput-object v0, p0, Lcom/konka/hotkey/ZoomActivity;->arcModes:[Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    goto :goto_1

    :cond_1
    iget-object v10, p0, Lcom/konka/hotkey/ZoomActivity;->mViewItems:[Lcom/konka/hotkey/ZoomActivity$ZoomItem;

    new-instance v0, Lcom/konka/hotkey/ZoomActivity$ZoomItem;

    invoke-virtual {p0}, Lcom/konka/hotkey/ZoomActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget v3, p0, Lcom/konka/hotkey/ZoomActivity;->IMG_WIDTH:I

    iget v4, p0, Lcom/konka/hotkey/ZoomActivity;->IMG_HEIGHT:I

    aget v5, v9, v7

    iget v6, p0, Lcom/konka/hotkey/ZoomActivity;->SIZE_NOR:F

    iget-object v1, p0, Lcom/konka/hotkey/ZoomActivity;->arcModes:[Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    aget-object v8, v1, v7

    move-object v1, p0

    invoke-direct/range {v0 .. v8}, Lcom/konka/hotkey/ZoomActivity$ZoomItem;-><init>(Lcom/konka/hotkey/ZoomActivity;Landroid/content/Context;IIIFILcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;)V

    aput-object v0, v10, v7

    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/konka/hotkey/ZoomActivity;->mViewLlContainer:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/konka/hotkey/ZoomActivity;->mViewItems:[Lcom/konka/hotkey/ZoomActivity$ZoomItem;

    aget-object v1, v1, v7

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    add-int/lit8 v7, v7, 0x1

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_5
    .end packed-switch
.end method
