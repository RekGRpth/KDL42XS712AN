.class Lcom/konka/hotkey/PictureActivity$ClickListener;
.super Ljava/lang/Object;
.source "PictureActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/hotkey/PictureActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ClickListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/hotkey/PictureActivity;


# direct methods
.method constructor <init>(Lcom/konka/hotkey/PictureActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/hotkey/PictureActivity$ClickListener;->this$0:Lcom/konka/hotkey/PictureActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    move-object v0, p1

    check-cast v0, Lcom/konka/hotkey/PictureActivity$PictureItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/PictureActivity$PictureItem;->isRunning()Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/konka/hotkey/PictureActivity$ClickListener;->this$0:Lcom/konka/hotkey/PictureActivity;

    invoke-virtual {v1}, Lcom/konka/hotkey/PictureActivity;->recoverItemNorBg()V

    iget-object v1, p0, Lcom/konka/hotkey/PictureActivity$ClickListener;->this$0:Lcom/konka/hotkey/PictureActivity;

    iget-object v1, v1, Lcom/konka/hotkey/PictureActivity;->pictureSkin:Lcom/mstar/android/tv/TvPictureManager;

    if-eqz v1, :cond_1

    const-string v1, "picture skin is not null"

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/konka/hotkey/PictureActivity$PictureItem;->doUpdate()Z

    :cond_1
    iget-object v1, p0, Lcom/konka/hotkey/PictureActivity$ClickListener;->this$0:Lcom/konka/hotkey/PictureActivity;

    invoke-virtual {v1}, Lcom/konka/hotkey/PictureActivity;->finish()V

    goto :goto_0
.end method
