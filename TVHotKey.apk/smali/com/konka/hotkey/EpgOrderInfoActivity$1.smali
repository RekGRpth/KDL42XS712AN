.class Lcom/konka/hotkey/EpgOrderInfoActivity$1;
.super Landroid/os/Handler;
.source "EpgOrderInfoActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/hotkey/EpgOrderInfoActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/hotkey/EpgOrderInfoActivity;


# direct methods
.method constructor <init>(Lcom/konka/hotkey/EpgOrderInfoActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/hotkey/EpgOrderInfoActivity$1;->this$0:Lcom/konka/hotkey/EpgOrderInfoActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1    # Landroid/os/Message;

    const/4 v4, 0x1

    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v2, p0, Lcom/konka/hotkey/EpgOrderInfoActivity$1;->this$0:Lcom/konka/hotkey/EpgOrderInfoActivity;

    # getter for: Lcom/konka/hotkey/EpgOrderInfoActivity;->mViewTvCountdown:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/konka/hotkey/EpgOrderInfoActivity;->access$0(Lcom/konka/hotkey/EpgOrderInfoActivity;)Landroid/widget/TextView;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/konka/hotkey/EpgOrderInfoActivity$1;->this$0:Lcom/konka/hotkey/EpgOrderInfoActivity;

    # getter for: Lcom/konka/hotkey/EpgOrderInfoActivity;->sec:I
    invoke-static {v4}, Lcom/konka/hotkey/EpgOrderInfoActivity;->access$1(Lcom/konka/hotkey/EpgOrderInfoActivity;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/hotkey/EpgOrderInfoActivity$1;->this$0:Lcom/konka/hotkey/EpgOrderInfoActivity;

    invoke-virtual {v4}, Lcom/konka/hotkey/EpgOrderInfoActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f080034    # com.konka.hotkey.R.string.str_hk_order_info_wram

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_1
    iget-object v2, p0, Lcom/konka/hotkey/EpgOrderInfoActivity$1;->this$0:Lcom/konka/hotkey/EpgOrderInfoActivity;

    # getter for: Lcom/konka/hotkey/EpgOrderInfoActivity;->timer:Ljava/util/Timer;
    invoke-static {v2}, Lcom/konka/hotkey/EpgOrderInfoActivity;->access$2(Lcom/konka/hotkey/EpgOrderInfoActivity;)Ljava/util/Timer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Timer;->cancel()V

    iget-object v2, p0, Lcom/konka/hotkey/EpgOrderInfoActivity$1;->this$0:Lcom/konka/hotkey/EpgOrderInfoActivity;

    # getter for: Lcom/konka/hotkey/EpgOrderInfoActivity;->mTimerInfo:Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;
    invoke-static {v2}, Lcom/konka/hotkey/EpgOrderInfoActivity;->access$3(Lcom/konka/hotkey/EpgOrderInfoActivity;)Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;

    move-result-object v2

    iget v2, v2, Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;->startTime:I

    add-int/lit8 v2, v2, 0x0

    add-int/lit8 v1, v2, 0x1e

    const-string v2, "time out"

    invoke-static {v2}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    const-string v2, "cancelEpgTimerEvent===***=*=***===>>> 1"

    invoke-static {v2}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.konka.tvsettings.action.EPG_COUNTDOWN"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "ProgramNum"

    iget-object v3, p0, Lcom/konka/hotkey/EpgOrderInfoActivity$1;->this$0:Lcom/konka/hotkey/EpgOrderInfoActivity;

    # getter for: Lcom/konka/hotkey/EpgOrderInfoActivity;->mTimerInfo:Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;
    invoke-static {v3}, Lcom/konka/hotkey/EpgOrderInfoActivity;->access$3(Lcom/konka/hotkey/EpgOrderInfoActivity;)Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;

    move-result-object v3

    iget v3, v3, Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;->progNumber:I

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v2, "ServiceType"

    iget-object v3, p0, Lcom/konka/hotkey/EpgOrderInfoActivity$1;->this$0:Lcom/konka/hotkey/EpgOrderInfoActivity;

    # getter for: Lcom/konka/hotkey/EpgOrderInfoActivity;->mTimerInfo:Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;
    invoke-static {v3}, Lcom/konka/hotkey/EpgOrderInfoActivity;->access$3(Lcom/konka/hotkey/EpgOrderInfoActivity;)Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;

    move-result-object v3

    iget v3, v3, Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;->serviceType:I

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v2, p0, Lcom/konka/hotkey/EpgOrderInfoActivity$1;->this$0:Lcom/konka/hotkey/EpgOrderInfoActivity;

    # getter for: Lcom/konka/hotkey/EpgOrderInfoActivity;->mTimerInfo:Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;
    invoke-static {v2}, Lcom/konka/hotkey/EpgOrderInfoActivity;->access$3(Lcom/konka/hotkey/EpgOrderInfoActivity;)Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;

    move-result-object v2

    iget v2, v2, Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;->timerType:I

    if-ne v2, v4, :cond_0

    iget-object v2, p0, Lcom/konka/hotkey/EpgOrderInfoActivity$1;->this$0:Lcom/konka/hotkey/EpgOrderInfoActivity;

    # getter for: Lcom/konka/hotkey/EpgOrderInfoActivity;->epgDesk:Lcom/konka/kkinterface/tv/EpgDesk;
    invoke-static {v2}, Lcom/konka/hotkey/EpgOrderInfoActivity;->access$4(Lcom/konka/hotkey/EpgOrderInfoActivity;)Lcom/konka/kkinterface/tv/EpgDesk;

    move-result-object v2

    invoke-interface {v2, v1}, Lcom/konka/kkinterface/tv/EpgDesk;->cancelEpgTimerEvent(I)V

    :cond_0
    iget-object v2, p0, Lcom/konka/hotkey/EpgOrderInfoActivity$1;->this$0:Lcom/konka/hotkey/EpgOrderInfoActivity;

    invoke-virtual {v2, v0}, Lcom/konka/hotkey/EpgOrderInfoActivity;->sendBroadcast(Landroid/content/Intent;)V

    iget-object v2, p0, Lcom/konka/hotkey/EpgOrderInfoActivity$1;->this$0:Lcom/konka/hotkey/EpgOrderInfoActivity;

    invoke-static {v2, v4}, Lcom/konka/hotkey/EpgOrderInfoActivity;->access$5(Lcom/konka/hotkey/EpgOrderInfoActivity;Z)V

    iget-object v2, p0, Lcom/konka/hotkey/EpgOrderInfoActivity$1;->this$0:Lcom/konka/hotkey/EpgOrderInfoActivity;

    # getter for: Lcom/konka/hotkey/EpgOrderInfoActivity;->epgDesk:Lcom/konka/kkinterface/tv/EpgDesk;
    invoke-static {v2}, Lcom/konka/hotkey/EpgOrderInfoActivity;->access$4(Lcom/konka/hotkey/EpgOrderInfoActivity;)Lcom/konka/kkinterface/tv/EpgDesk;

    move-result-object v2

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/EpgDesk;->closeDB()V

    sput-boolean v4, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->RECEIVE_COUNT_DOWN:Z

    iget-object v2, p0, Lcom/konka/hotkey/EpgOrderInfoActivity$1;->this$0:Lcom/konka/hotkey/EpgOrderInfoActivity;

    invoke-virtual {v2}, Lcom/konka/hotkey/EpgOrderInfoActivity;->finish()V

    goto/16 :goto_0

    :pswitch_2
    iget-object v2, p0, Lcom/konka/hotkey/EpgOrderInfoActivity$1;->this$0:Lcom/konka/hotkey/EpgOrderInfoActivity;

    # getter for: Lcom/konka/hotkey/EpgOrderInfoActivity;->timer:Ljava/util/Timer;
    invoke-static {v2}, Lcom/konka/hotkey/EpgOrderInfoActivity;->access$2(Lcom/konka/hotkey/EpgOrderInfoActivity;)Ljava/util/Timer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Timer;->cancel()V

    iget-object v2, p0, Lcom/konka/hotkey/EpgOrderInfoActivity$1;->this$0:Lcom/konka/hotkey/EpgOrderInfoActivity;

    # getter for: Lcom/konka/hotkey/EpgOrderInfoActivity;->mTimerInfo:Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;
    invoke-static {v2}, Lcom/konka/hotkey/EpgOrderInfoActivity;->access$3(Lcom/konka/hotkey/EpgOrderInfoActivity;)Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;

    move-result-object v2

    iget v2, v2, Lcom/konka/kkinterface/tv/DtvInterface$EPG_SCHEDULE_EVENT_INFO;->startTime:I

    add-int/lit8 v2, v2, 0x0

    add-int/lit8 v1, v2, 0x1e

    const-string v2, "cancelEpgTimerEvent===***=*=***===>>> 2"

    invoke-static {v2}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/hotkey/EpgOrderInfoActivity$1;->this$0:Lcom/konka/hotkey/EpgOrderInfoActivity;

    # getter for: Lcom/konka/hotkey/EpgOrderInfoActivity;->epgDesk:Lcom/konka/kkinterface/tv/EpgDesk;
    invoke-static {v2}, Lcom/konka/hotkey/EpgOrderInfoActivity;->access$4(Lcom/konka/hotkey/EpgOrderInfoActivity;)Lcom/konka/kkinterface/tv/EpgDesk;

    move-result-object v2

    invoke-interface {v2, v1}, Lcom/konka/kkinterface/tv/EpgDesk;->cancelEpgTimerEvent(I)V

    iget-object v2, p0, Lcom/konka/hotkey/EpgOrderInfoActivity$1;->this$0:Lcom/konka/hotkey/EpgOrderInfoActivity;

    # getter for: Lcom/konka/hotkey/EpgOrderInfoActivity;->epgDesk:Lcom/konka/kkinterface/tv/EpgDesk;
    invoke-static {v2}, Lcom/konka/hotkey/EpgOrderInfoActivity;->access$4(Lcom/konka/hotkey/EpgOrderInfoActivity;)Lcom/konka/kkinterface/tv/EpgDesk;

    move-result-object v2

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/EpgDesk;->closeDB()V

    sput-boolean v4, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->RECEIVE_COUNT_DOWN:Z

    iget-object v2, p0, Lcom/konka/hotkey/EpgOrderInfoActivity$1;->this$0:Lcom/konka/hotkey/EpgOrderInfoActivity;

    invoke-virtual {v2}, Lcom/konka/hotkey/EpgOrderInfoActivity;->finish()V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x2885
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
