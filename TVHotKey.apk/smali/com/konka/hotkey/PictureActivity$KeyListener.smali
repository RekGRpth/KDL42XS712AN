.class Lcom/konka/hotkey/PictureActivity$KeyListener;
.super Ljava/lang/Object;
.source "PictureActivity.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/hotkey/PictureActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "KeyListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/hotkey/PictureActivity;


# direct methods
.method constructor <init>(Lcom/konka/hotkey/PictureActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/hotkey/PictureActivity$KeyListener;->this$0:Lcom/konka/hotkey/PictureActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # Landroid/view/KeyEvent;

    const/4 v1, 0x1

    move-object v0, p1

    check-cast v0, Lcom/konka/hotkey/PictureActivity$PictureItem;

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_0

    const/16 v2, 0x15

    if-ne v2, p2, :cond_0

    const-string v2, "Key down left"

    invoke-static {v2}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/hotkey/PictureActivity$KeyListener;->this$0:Lcom/konka/hotkey/PictureActivity;

    invoke-virtual {v2, v0}, Lcom/konka/hotkey/PictureActivity;->focusAnticlockwise(Lcom/konka/hotkey/PictureActivity$PictureItem;)V

    :goto_0
    return v1

    :cond_0
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_1

    const/16 v2, 0x16

    if-ne v2, p2, :cond_1

    const-string v2, "Key down right"

    invoke-static {v2}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/hotkey/PictureActivity$KeyListener;->this$0:Lcom/konka/hotkey/PictureActivity;

    invoke-virtual {v2, v0}, Lcom/konka/hotkey/PictureActivity;->focusClockwise(Lcom/konka/hotkey/PictureActivity$PictureItem;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_2

    const/16 v2, 0xfc

    if-ne v2, p2, :cond_2

    iget-object v2, p0, Lcom/konka/hotkey/PictureActivity$KeyListener;->this$0:Lcom/konka/hotkey/PictureActivity;

    invoke-virtual {v2, v0}, Lcom/konka/hotkey/PictureActivity;->focusClockwise(Lcom/konka/hotkey/PictureActivity$PictureItem;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_3

    const/4 v2, 0x4

    if-ne v2, p2, :cond_3

    iget-object v2, p0, Lcom/konka/hotkey/PictureActivity$KeyListener;->this$0:Lcom/konka/hotkey/PictureActivity;

    invoke-virtual {v2}, Lcom/konka/hotkey/PictureActivity;->finish()V

    goto :goto_0

    :cond_3
    const/4 v1, 0x0

    goto :goto_0
.end method
