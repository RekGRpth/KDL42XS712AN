.class Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity$InputItem;
.super Landroid/widget/RelativeLayout;
.source "PIPSubChannelSelDlgActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "InputItem"
.end annotation


# instance fields
.field public mInputMode:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

.field private mPos:I

.field private mText:Ljava/lang/String;

.field final synthetic this$0:Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;

.field private tv:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;Landroid/content/Context;Ljava/lang/String;Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;I)V
    .locals 6
    .param p2    # Landroid/content/Context;
    .param p3    # Ljava/lang/String;
    .param p4    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    .param p5    # I

    const/4 v5, 0x1

    const/4 v4, -0x2

    const/4 v3, 0x0

    iput-object p1, p0, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity$InputItem;->this$0:Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;

    invoke-direct {p0, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity$InputItem;->mInputMode:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    const/4 v2, 0x0

    iput v2, p0, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity$InputItem;->mPos:I

    iput-object v3, p0, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity$InputItem;->mText:Ljava/lang/String;

    iput-object v3, p0, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity$InputItem;->tv:Landroid/widget/TextView;

    iput-object p4, p0, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity$InputItem;->mInputMode:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    iput p5, p0, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity$InputItem;->mPos:I

    iput-object p3, p0, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity$InputItem;->mText:Ljava/lang/String;

    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    # getter for: Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;->WIDTH:I
    invoke-static {p1}, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;->access$0(Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;)I

    move-result v2

    # getter for: Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;->HEIGHT:I
    invoke-static {p1}, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;->access$1(Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;)I

    move-result v3

    invoke-direct {v0, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0}, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity$InputItem;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p0, v5}, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity$InputItem;->setFocusable(Z)V

    invoke-virtual {p0, v5}, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity$InputItem;->setFocusableInTouchMode(Z)V

    invoke-virtual {p0, p0}, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity$InputItem;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0, p0}, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity$InputItem;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    const v2, 0x7f02007a    # com.konka.hotkey.R.drawable.input_menu_item_state

    invoke-virtual {p0, v2}, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity$InputItem;->setBackgroundResource(I)V

    new-instance v2, Landroid/widget/TextView;

    invoke-direct {v2, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity$InputItem;->tv:Landroid/widget/TextView;

    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v2, 0xd

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    iget-object v2, p0, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity$InputItem;->tv:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v2, p0, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity$InputItem;->tv:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity$InputItem;->mText:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity$InputItem;->tv:Landroid/widget/TextView;

    const/4 v3, 0x2

    const/high16 v4, 0x41a00000    # 20.0f

    invoke-virtual {v2, v3, v4}, Landroid/widget/TextView;->setTextSize(IF)V

    iget-object v2, p0, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity$InputItem;->tv:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity$InputItem;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f060002    # com.konka.hotkey.R.color.text_normal_col

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v2, p0, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity$InputItem;->tv:Landroid/widget/TextView;

    invoke-virtual {p0, v2}, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity$InputItem;->addView(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity$InputItem;->this$0:Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity$InputItem;->mInputMode:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v0, v1}, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;->setSubChannel(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity$InputItem;->this$0:Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;

    invoke-virtual {v0}, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;->finish()V

    return-void
.end method

.method public onFocusChange(Landroid/view/View;Z)V
    .locals 3
    .param p1    # Landroid/view/View;
    .param p2    # Z

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity$InputItem;->tv:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity$InputItem;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f060001    # com.konka.hotkey.R.color.text_select_col

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity$InputItem;->tv:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity$InputItem;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f060002    # com.konka.hotkey.R.color.text_normal_col

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0
.end method
