.class public Lcom/konka/hotkey/video3dpip/MenuHolderPIP;
.super Lcom/konka/hotkey/interfaces/PIPMenuBase;
.source "MenuHolderPIP.java"


# instance fields
.field private final IMG_FBD:[I

.field private final IMG_NOR:[I

.field private final IMG_RUN:[I

.field private final IMG_SEL:[I

.field private final ITEM_NAEM:[I

.field private handler:Landroid/os/Handler;

.field private mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

.field private mCommonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

.field private mContainer:Landroid/widget/LinearLayout;

.field private mItemLastFocus:Lcom/konka/hotkey/view/Com3DPIPItem;

.field protected mItemNormalScreen:Lcom/konka/hotkey/view/Com3DPIPItem;

.field protected mItemPIP:Lcom/konka/hotkey/view/Com3DPIPItem;

.field protected mItemPOP:Lcom/konka/hotkey/view/Com3DPIPItem;

.field protected mItemSubChannelFullScreen:Lcom/konka/hotkey/view/Com3DPIPItem;

.field protected mItemSubChannelSelection:Lcom/konka/hotkey/view/Com3DPIPItem;

.field protected mItemSyncWatching:Lcom/konka/hotkey/view/Com3DPIPItem;

.field private mSubInputSrc:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

.field private m_bIsMenuForbidden:Z

.field private m_bIsPOPSupportable:Z

.field private m_bIsSyncWatchSupportable:Z


# direct methods
.method public constructor <init>(Lcom/konka/hotkey/video3dpip/Video3DActivity;IIFF)V
    .locals 2
    .param p1    # Lcom/konka/hotkey/video3dpip/Video3DActivity;
    .param p2    # I
    .param p3    # I
    .param p4    # F
    .param p5    # F

    const/4 v1, 0x6

    invoke-direct {p0}, Lcom/konka/hotkey/interfaces/PIPMenuBase;-><init>()V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->handler:Landroid/os/Handler;

    new-array v0, v1, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->IMG_SEL:[I

    new-array v0, v1, [I

    fill-array-data v0, :array_1

    iput-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->IMG_NOR:[I

    new-array v0, v1, [I

    fill-array-data v0, :array_2

    iput-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->IMG_RUN:[I

    new-array v0, v1, [I

    fill-array-data v0, :array_3

    iput-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->IMG_FBD:[I

    new-array v0, v1, [I

    fill-array-data v0, :array_4

    iput-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->ITEM_NAEM:[I

    const-string v0, "MenuHolderPIP==>onCreate begin! =================================="

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iput-object p1, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    invoke-direct {p0}, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->checkSyncWatchSupportable()V

    invoke-direct {p0}, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->checkPOPSupportable()V

    iget-boolean v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->m_bIsSyncWatchSupportable:Z

    if-eqz v0, :cond_0

    :goto_0
    iput p2, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->ITEM_WIDTH:I

    iput p3, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->ITEM_HEIGHT:I

    iput p4, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->TXT_SIZE_NOR:F

    iput p5, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->TXT_SIZE_SEL:F

    const/16 v0, 0x51

    iput v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->gravity:I

    invoke-virtual {p0}, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->findViews()V

    const-string v0, "MenuHolderPIP==>onCreate done! =================================="

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    return-void

    :cond_0
    mul-int/lit8 v0, p2, 0x6

    div-int/lit8 p2, v0, 0x5

    goto :goto_0

    nop

    :array_0
    .array-data 4
        0x7f02004b    # com.konka.hotkey.R.drawable.hk_dc_pip_syncwatching_sel
        0x7f02004f    # com.konka.hotkey.R.drawable.hk_dc_pop_sel
        0x7f020047    # com.konka.hotkey.R.drawable.hk_dc_pip_sel
        0x7f020058    # com.konka.hotkey.R.drawable.hk_dc_subscr_sel
        0x7f020043    # com.konka.hotkey.R.drawable.hk_dc_intosub_sel
        0x7f020053    # com.konka.hotkey.R.drawable.hk_dc_poppip_off_sel
    .end array-data

    :array_1
    .array-data 4
        0x7f020049    # com.konka.hotkey.R.drawable.hk_dc_pip_syncwatching_nor
        0x7f02004d    # com.konka.hotkey.R.drawable.hk_dc_pop_nor
        0x7f020045    # com.konka.hotkey.R.drawable.hk_dc_pip_nor
        0x7f020055    # com.konka.hotkey.R.drawable.hk_dc_subscr_nor
        0x7f020041    # com.konka.hotkey.R.drawable.hk_dc_intosub_nor
        0x7f020051    # com.konka.hotkey.R.drawable.hk_dc_poppip_off_nor
    .end array-data

    :array_2
    .array-data 4
        0x7f02004a    # com.konka.hotkey.R.drawable.hk_dc_pip_syncwatching_run
        0x7f02004e    # com.konka.hotkey.R.drawable.hk_dc_pop_run
        0x7f020046    # com.konka.hotkey.R.drawable.hk_dc_pip_run
        0x7f020056    # com.konka.hotkey.R.drawable.hk_dc_subscr_run
        0x7f020042    # com.konka.hotkey.R.drawable.hk_dc_intosub_run
        0x7f020052    # com.konka.hotkey.R.drawable.hk_dc_poppip_off_run
    .end array-data

    :array_3
    .array-data 4
        0x7f020048    # com.konka.hotkey.R.drawable.hk_dc_pip_syncwatching_fbd
        0x7f02004c    # com.konka.hotkey.R.drawable.hk_dc_pop_fbd
        0x7f020044    # com.konka.hotkey.R.drawable.hk_dc_pip_fbd
        0x7f020054    # com.konka.hotkey.R.drawable.hk_dc_subscr_fbd
        0x7f020040    # com.konka.hotkey.R.drawable.hk_dc_intosub_fbd
        0x7f020050    # com.konka.hotkey.R.drawable.hk_dc_poppip_off_fbd
    .end array-data

    :array_4
    .array-data 4
        0x7f08004a    # com.konka.hotkey.R.string.dc_syncwatch
        0x7f080045    # com.konka.hotkey.R.string.dc_pop
        0x7f080046    # com.konka.hotkey.R.string.dc_pip
        0x7f080047    # com.konka.hotkey.R.string.dc_subchannel
        0x7f080048    # com.konka.hotkey.R.string.dc_subfullscreen
        0x7f080049    # com.konka.hotkey.R.string.dc_normalscreen
    .end array-data
.end method

.method static synthetic access$0(Lcom/konka/hotkey/video3dpip/MenuHolderPIP;)Lcom/konka/hotkey/video3dpip/Video3DActivity;
    .locals 1

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    return-object v0
.end method

.method private addItemNormalScreen()V
    .locals 10

    const/4 v9, 0x5

    new-instance v0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP$6;

    iget-object v2, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    iget v3, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->ITEM_WIDTH:I

    iget v4, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->ITEM_HEIGHT:I

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->IMG_SEL:[I

    aget v5, v1, v9

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    iget-object v6, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->ITEM_NAEM:[I

    aget v6, v6, v9

    invoke-virtual {v1, v6}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    iget v7, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->TXT_SIZE_SEL:F

    iget v8, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->gravity:I

    move-object v1, p0

    invoke-direct/range {v0 .. v8}, Lcom/konka/hotkey/video3dpip/MenuHolderPIP$6;-><init>(Lcom/konka/hotkey/video3dpip/MenuHolderPIP;Landroid/content/Context;IIILjava/lang/String;FI)V

    iput-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mItemNormalScreen:Lcom/konka/hotkey/view/Com3DPIPItem;

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mItemNormalScreen:Lcom/konka/hotkey/view/Com3DPIPItem;

    const-string v1, "ItemName_NormalScreen"

    invoke-virtual {v0, v1}, Lcom/konka/hotkey/view/Com3DPIPItem;->setTag(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mItemNormalScreen:Lcom/konka/hotkey/view/Com3DPIPItem;

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->IMG_SEL:[I

    aget v1, v1, v9

    iget-object v2, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->IMG_RUN:[I

    aget v2, v2, v9

    iget-object v3, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->IMG_FBD:[I

    aget v3, v3, v9

    iget-object v4, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->IMG_NOR:[I

    aget v4, v4, v9

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/konka/hotkey/view/Com3DPIPItem;->setImgResOfAllStates(IIII)V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mItemNormalScreen:Lcom/konka/hotkey/view/Com3DPIPItem;

    iget v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->TXT_SIZE_SEL:F

    iget v2, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->TXT_SIZE_NOR:F

    iget v3, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->TXT_SIZE_NOR:F

    iget v4, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->TXT_SIZE_NOR:F

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/konka/hotkey/view/Com3DPIPItem;->setNameSizeOfAllStates(FFFF)V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mContainer:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mItemNormalScreen:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    return-void
.end method

.method private addItemPIP()V
    .locals 10

    const/4 v9, 0x2

    new-instance v0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP$3;

    iget-object v2, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    iget v3, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->ITEM_WIDTH:I

    iget v4, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->ITEM_HEIGHT:I

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->IMG_SEL:[I

    aget v5, v1, v9

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    iget-object v6, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->ITEM_NAEM:[I

    aget v6, v6, v9

    invoke-virtual {v1, v6}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    iget v7, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->TXT_SIZE_SEL:F

    iget v8, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->gravity:I

    move-object v1, p0

    invoke-direct/range {v0 .. v8}, Lcom/konka/hotkey/video3dpip/MenuHolderPIP$3;-><init>(Lcom/konka/hotkey/video3dpip/MenuHolderPIP;Landroid/content/Context;IIILjava/lang/String;FI)V

    iput-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mItemPIP:Lcom/konka/hotkey/view/Com3DPIPItem;

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mItemPIP:Lcom/konka/hotkey/view/Com3DPIPItem;

    const-string v1, "ItemName_PIP"

    invoke-virtual {v0, v1}, Lcom/konka/hotkey/view/Com3DPIPItem;->setTag(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mItemPIP:Lcom/konka/hotkey/view/Com3DPIPItem;

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->IMG_SEL:[I

    aget v1, v1, v9

    iget-object v2, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->IMG_RUN:[I

    aget v2, v2, v9

    iget-object v3, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->IMG_FBD:[I

    aget v3, v3, v9

    iget-object v4, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->IMG_NOR:[I

    aget v4, v4, v9

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/konka/hotkey/view/Com3DPIPItem;->setImgResOfAllStates(IIII)V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mItemPIP:Lcom/konka/hotkey/view/Com3DPIPItem;

    iget v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->TXT_SIZE_SEL:F

    iget v2, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->TXT_SIZE_NOR:F

    iget v3, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->TXT_SIZE_NOR:F

    iget v4, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->TXT_SIZE_NOR:F

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/konka/hotkey/view/Com3DPIPItem;->setNameSizeOfAllStates(FFFF)V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mContainer:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mItemPIP:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    invoke-static {v0}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/CommonDesk;->isEnableAdvancedPipSyncview()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mItemPIP:Lcom/konka/hotkey/view/Com3DPIPItem;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/konka/hotkey/view/Com3DPIPItem;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method private addItemPOP()V
    .locals 10

    const/4 v9, 0x1

    new-instance v0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP$2;

    iget-object v2, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    iget v3, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->ITEM_WIDTH:I

    iget v4, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->ITEM_HEIGHT:I

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->IMG_SEL:[I

    aget v5, v1, v9

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    iget-object v6, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->ITEM_NAEM:[I

    aget v6, v6, v9

    invoke-virtual {v1, v6}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    iget v7, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->TXT_SIZE_SEL:F

    iget v8, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->gravity:I

    move-object v1, p0

    invoke-direct/range {v0 .. v8}, Lcom/konka/hotkey/video3dpip/MenuHolderPIP$2;-><init>(Lcom/konka/hotkey/video3dpip/MenuHolderPIP;Landroid/content/Context;IIILjava/lang/String;FI)V

    iput-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mItemPOP:Lcom/konka/hotkey/view/Com3DPIPItem;

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mItemPOP:Lcom/konka/hotkey/view/Com3DPIPItem;

    const-string v1, "ItemName_POP"

    invoke-virtual {v0, v1}, Lcom/konka/hotkey/view/Com3DPIPItem;->setTag(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mItemPOP:Lcom/konka/hotkey/view/Com3DPIPItem;

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->IMG_SEL:[I

    aget v1, v1, v9

    iget-object v2, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->IMG_RUN:[I

    aget v2, v2, v9

    iget-object v3, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->IMG_FBD:[I

    aget v3, v3, v9

    iget-object v4, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->IMG_NOR:[I

    aget v4, v4, v9

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/konka/hotkey/view/Com3DPIPItem;->setImgResOfAllStates(IIII)V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mItemPOP:Lcom/konka/hotkey/view/Com3DPIPItem;

    iget v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->TXT_SIZE_SEL:F

    iget v2, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->TXT_SIZE_NOR:F

    iget v3, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->TXT_SIZE_NOR:F

    iget v4, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->TXT_SIZE_NOR:F

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/konka/hotkey/view/Com3DPIPItem;->setNameSizeOfAllStates(FFFF)V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mContainer:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mItemPOP:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mItemPOP:Lcom/konka/hotkey/view/Com3DPIPItem;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/konka/hotkey/view/Com3DPIPItem;->setVisibility(I)V

    return-void
.end method

.method private addItemSubChannelFullScreen()V
    .locals 10

    const/4 v9, 0x4

    new-instance v0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP$5;

    iget-object v2, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    iget v3, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->ITEM_WIDTH:I

    iget v4, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->ITEM_HEIGHT:I

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->IMG_SEL:[I

    aget v5, v1, v9

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    iget-object v6, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->ITEM_NAEM:[I

    aget v6, v6, v9

    invoke-virtual {v1, v6}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    iget v7, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->TXT_SIZE_SEL:F

    iget v8, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->gravity:I

    move-object v1, p0

    invoke-direct/range {v0 .. v8}, Lcom/konka/hotkey/video3dpip/MenuHolderPIP$5;-><init>(Lcom/konka/hotkey/video3dpip/MenuHolderPIP;Landroid/content/Context;IIILjava/lang/String;FI)V

    iput-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mItemSubChannelFullScreen:Lcom/konka/hotkey/view/Com3DPIPItem;

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mItemSubChannelFullScreen:Lcom/konka/hotkey/view/Com3DPIPItem;

    const-string v1, "ItemName_SubFullScreen"

    invoke-virtual {v0, v1}, Lcom/konka/hotkey/view/Com3DPIPItem;->setTag(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mItemSubChannelFullScreen:Lcom/konka/hotkey/view/Com3DPIPItem;

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->IMG_SEL:[I

    aget v1, v1, v9

    iget-object v2, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->IMG_RUN:[I

    aget v2, v2, v9

    iget-object v3, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->IMG_FBD:[I

    aget v3, v3, v9

    iget-object v4, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->IMG_NOR:[I

    aget v4, v4, v9

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/konka/hotkey/view/Com3DPIPItem;->setImgResOfAllStates(IIII)V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mItemSubChannelFullScreen:Lcom/konka/hotkey/view/Com3DPIPItem;

    iget v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->TXT_SIZE_SEL:F

    iget v2, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->TXT_SIZE_NOR:F

    iget v3, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->TXT_SIZE_NOR:F

    iget v4, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->TXT_SIZE_NOR:F

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/konka/hotkey/view/Com3DPIPItem;->setNameSizeOfAllStates(FFFF)V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mContainer:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mItemSubChannelFullScreen:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    return-void
.end method

.method private addItemSubChannelSelection()V
    .locals 10

    const/4 v9, 0x3

    new-instance v0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP$4;

    iget-object v2, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    iget v3, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->ITEM_WIDTH:I

    iget v4, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->ITEM_HEIGHT:I

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->IMG_SEL:[I

    aget v5, v1, v9

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    iget-object v6, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->ITEM_NAEM:[I

    aget v6, v6, v9

    invoke-virtual {v1, v6}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    iget v7, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->TXT_SIZE_SEL:F

    iget v8, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->gravity:I

    move-object v1, p0

    invoke-direct/range {v0 .. v8}, Lcom/konka/hotkey/video3dpip/MenuHolderPIP$4;-><init>(Lcom/konka/hotkey/video3dpip/MenuHolderPIP;Landroid/content/Context;IIILjava/lang/String;FI)V

    iput-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mItemSubChannelSelection:Lcom/konka/hotkey/view/Com3DPIPItem;

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mItemSubChannelSelection:Lcom/konka/hotkey/view/Com3DPIPItem;

    const-string v1, "ItemName_SubChannelSelection"

    invoke-virtual {v0, v1}, Lcom/konka/hotkey/view/Com3DPIPItem;->setTag(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mItemSubChannelSelection:Lcom/konka/hotkey/view/Com3DPIPItem;

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->IMG_SEL:[I

    aget v1, v1, v9

    iget-object v2, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->IMG_RUN:[I

    aget v2, v2, v9

    iget-object v3, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->IMG_FBD:[I

    aget v3, v3, v9

    iget-object v4, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->IMG_NOR:[I

    aget v4, v4, v9

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/konka/hotkey/view/Com3DPIPItem;->setImgResOfAllStates(IIII)V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mItemSubChannelSelection:Lcom/konka/hotkey/view/Com3DPIPItem;

    iget v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->TXT_SIZE_SEL:F

    iget v2, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->TXT_SIZE_NOR:F

    iget v3, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->TXT_SIZE_NOR:F

    iget v4, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->TXT_SIZE_NOR:F

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/konka/hotkey/view/Com3DPIPItem;->setNameSizeOfAllStates(FFFF)V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mContainer:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mItemSubChannelSelection:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    return-void
.end method

.method private addItemSyncWatching()V
    .locals 10

    const/4 v9, 0x0

    new-instance v0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP$1;

    iget-object v2, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    iget v3, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->ITEM_WIDTH:I

    iget v4, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->ITEM_HEIGHT:I

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->IMG_SEL:[I

    aget v5, v1, v9

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    iget-object v6, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->ITEM_NAEM:[I

    aget v6, v6, v9

    invoke-virtual {v1, v6}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    iget v7, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->TXT_SIZE_SEL:F

    iget v8, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->gravity:I

    move-object v1, p0

    invoke-direct/range {v0 .. v8}, Lcom/konka/hotkey/video3dpip/MenuHolderPIP$1;-><init>(Lcom/konka/hotkey/video3dpip/MenuHolderPIP;Landroid/content/Context;IIILjava/lang/String;FI)V

    iput-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mItemSyncWatching:Lcom/konka/hotkey/view/Com3DPIPItem;

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mItemSyncWatching:Lcom/konka/hotkey/view/Com3DPIPItem;

    const-string v1, "ItemName_SyncWatching"

    invoke-virtual {v0, v1}, Lcom/konka/hotkey/view/Com3DPIPItem;->setTag(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mItemSyncWatching:Lcom/konka/hotkey/view/Com3DPIPItem;

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->IMG_SEL:[I

    aget v1, v1, v9

    iget-object v2, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->IMG_RUN:[I

    aget v2, v2, v9

    iget-object v3, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->IMG_FBD:[I

    aget v3, v3, v9

    iget-object v4, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->IMG_NOR:[I

    aget v4, v4, v9

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/konka/hotkey/view/Com3DPIPItem;->setImgResOfAllStates(IIII)V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mItemSyncWatching:Lcom/konka/hotkey/view/Com3DPIPItem;

    iget v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->TXT_SIZE_SEL:F

    iget v2, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->TXT_SIZE_NOR:F

    iget v3, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->TXT_SIZE_NOR:F

    iget v4, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->TXT_SIZE_NOR:F

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/konka/hotkey/view/Com3DPIPItem;->setNameSizeOfAllStates(FFFF)V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mContainer:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mItemSyncWatching:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    return-void
.end method

.method private checkPOPSupportable()V
    .locals 2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->m_bIsPOPSupportable:Z

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "MenuHolderPIP==>isPOPSupportable, m_bIsPOPSupportable = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->m_bIsPOPSupportable:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    return-void
.end method

.method private checkSyncWatchSupportable()V
    .locals 5

    new-instance v0, Lcom/konka/kkimplements/common/IniEditor;

    invoke-direct {v0}, Lcom/konka/kkimplements/common/IniEditor;-><init>()V

    const-string v3, "/customercfg/panel/panel.ini"

    invoke-virtual {v0, v3}, Lcom/konka/kkimplements/common/IniEditor;->loadFile(Ljava/lang/String;)Z

    const-string v3, "panel:b3DPanel"

    const-string v4, "0"

    invoke-virtual {v0, v3, v4}, Lcom/konka/kkimplements/common/IniEditor;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "panel:bSGPanelFlag"

    const-string v4, "0"

    invoke-virtual {v0, v3, v4}, Lcom/konka/kkimplements/common/IniEditor;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    invoke-virtual {v3}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v3

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v3

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/CommonDesk;->getPipInfo()Lcom/konka/kkinterface/tv/CommonDesk$PipInfo;

    move-result-object v3

    iget v3, v3, Lcom/konka/kkinterface/tv/CommonDesk$PipInfo;->enableDualView:I

    if-eqz v3, :cond_0

    const-string v3, "1"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "0"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    :goto_0
    iput-boolean v3, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->m_bIsSyncWatchSupportable:Z

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "MenuHolderPIP==>isSyncWatchSupportable, supportable = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v4, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->m_bIsSyncWatchSupportable:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    return-void

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private doSendFocusToNearItem(Lcom/konka/hotkey/view/Com3DPIPItem;Z)V
    .locals 2
    .param p1    # Lcom/konka/hotkey/view/Com3DPIPItem;
    .param p2    # Z

    iput-object p1, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mItemLastFocus:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {p1}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateNormal()V

    if-eqz p2, :cond_0

    invoke-virtual {p1}, Lcom/konka/hotkey/view/Com3DPIPItem;->requestFocus()Z

    invoke-virtual {p1}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateFocus()V

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "MenuHolderPIP==>doSendFocusToNearItem, mItemLastFocus = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/konka/hotkey/view/Com3DPIPItem;->getTag()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "; sendFocus = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public dismiss()V
    .locals 1

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    invoke-virtual {v0}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->finish()V

    return-void
.end method

.method public displayNormalScreen()V
    .locals 3

    const/4 v2, 0x0

    const-string v1, "MenuHolderPIP==>displayNormalScreen, start."

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tv/TvPipPopManager;->isPipEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.konka.hotkey.disablePip"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    invoke-virtual {v1, v0}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->sendBroadcast(Landroid/content/Intent;)V

    const-string v1, "MenuHolderPIP==>displayNormalScreen, disablePip"

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tv/TvPipPopManager;->disablePip()Z

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/mstar/android/tv/TvPipPopManager;->setPipOnFlag(Z)Z

    :cond_0
    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tv/TvPipPopManager;->isPopEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.konka.hotkey.disablePop"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    invoke-virtual {v1, v0}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->sendBroadcast(Landroid/content/Intent;)V

    const-string v1, "MenuHolderPIP==>displayNormalScreen, disablePop"

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tv/TvPipPopManager;->disablePop()Z

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/mstar/android/tv/TvPipPopManager;->setPopOnFlag(Z)Z

    :cond_1
    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tv/TvPipPopManager;->isDualViewEnabled()Z

    move-result v1

    if-eqz v1, :cond_2

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.konka.hotkey.disable3dDualView"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    invoke-virtual {v1, v0}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->sendBroadcast(Landroid/content/Intent;)V

    const-string v1, "MenuHolderPIP==>displayNormalScreen, disable3dDualView"

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tv/TvPipPopManager;->disable3dDualView()Z

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/mstar/android/tv/TvPipPopManager;->setDualViewOnFlag(Z)Z

    :cond_2
    const-string v1, "MenuHolderPIP==>displayNormalScreen, done."

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    return-void
.end method

.method public displayPIP()Z
    .locals 20

    invoke-static {}, Lcom/mstar/android/tv/TvCommonManager;->getInstance()Lcom/mstar/android/tv/TvCommonManager;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/mstar/android/tv/TvCommonManager;->getCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v7

    const-string v18, "MenuHolderPIP==>displayPIP, start."

    invoke-static/range {v18 .. v18}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/mstar/android/tv/TvPipPopManager;->isPopEnabled()Z

    move-result v18

    if-eqz v18, :cond_0

    new-instance v8, Landroid/content/Intent;

    const-string v18, "com.konka.hotkey.disablePop"

    move-object/from16 v0, v18

    invoke-direct {v8, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v8}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->sendBroadcast(Landroid/content/Intent;)V

    const-string v18, "MenuHolderPIP==>displayPIP, disablePop"

    invoke-static/range {v18 .. v18}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/mstar/android/tv/TvPipPopManager;->disablePop()Z

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v18

    const/16 v19, 0x0

    invoke-virtual/range {v18 .. v19}, Lcom/mstar/android/tv/TvPipPopManager;->setPopOnFlag(Z)Z

    :cond_0
    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/mstar/android/tv/TvPipPopManager;->isDualViewEnabled()Z

    move-result v18

    if-eqz v18, :cond_1

    new-instance v8, Landroid/content/Intent;

    const-string v18, "com.konka.hotkey.disable3dDualView"

    move-object/from16 v0, v18

    invoke-direct {v8, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v8}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->sendBroadcast(Landroid/content/Intent;)V

    const-string v18, "MenuHolderPIP==>displayPIP, disable3dDualView"

    invoke-static/range {v18 .. v18}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/mstar/android/tv/TvPipPopManager;->disable3dDualView()Z

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v18

    const/16 v19, 0x0

    invoke-virtual/range {v18 .. v19}, Lcom/mstar/android/tv/TvPipPopManager;->setDualViewOnFlag(Z)Z

    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getDataBaseMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryUserSysSetting()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;

    move-result-object v13

    iget-boolean v0, v13, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_USER_SYSTEM_SETTING;->bMicOn:Z

    move/from16 v18, v0

    if-eqz v18, :cond_2

    sget-object v18, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_KTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-object/from16 v0, v18

    if-ne v7, v0, :cond_2

    const-string v18, "MenuHolderPIP==>yaoyan Mic on"

    invoke-static/range {v18 .. v18}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->micToast()V

    const/16 v18, 0x0

    :goto_0
    return v18

    :cond_2
    const/4 v9, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/mstar/android/tvapi/common/PictureManager;->getPanelWidthHeight()Lcom/mstar/android/tvapi/common/vo/PanelProperty;

    move-result-object v9

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v18

    if-nez v18, :cond_3

    const-string v18, "TvManger.getInstance() == null"

    invoke-static/range {v18 .. v18}, Lcom/konka/debuginfo/logPrint;->Fatal(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_3
    :goto_1
    const/16 v15, 0x780

    const/16 v5, 0x438

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/4 v14, 0x0

    const/4 v4, 0x0

    if-eqz v9, :cond_4

    iget v15, v9, Lcom/mstar/android/tvapi/common/vo/PanelProperty;->width:I

    iget v5, v9, Lcom/mstar/android/tvapi/common/vo/PanelProperty;->height:I

    :cond_4
    div-int/lit8 v14, v15, 0x3

    div-int/lit8 v4, v5, 0x3

    sub-int v16, v15, v14

    const/16 v17, 0x0

    new-instance v2, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;

    invoke-direct {v2}, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;-><init>()V

    move/from16 v0, v16

    iput v0, v2, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->x:I

    move/from16 v0, v17

    iput v0, v2, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->y:I

    iput v14, v2, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->width:I

    iput v4, v2, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->height:I

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/mstar/android/tv/TvPipPopManager;->getSubInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v11

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v18

    const/16 v19, 0x1

    invoke-virtual/range {v18 .. v19}, Lcom/mstar/android/tv/TvPipPopManager;->getSubWindowSourceList(Z)[I

    move-result-object v12

    const/4 v6, 0x0

    const/4 v6, 0x0

    :goto_2
    array-length v0, v12

    move/from16 v18, v0

    move/from16 v0, v18

    if-lt v6, v0, :cond_8

    :cond_5
    array-length v0, v12

    move/from16 v18, v0

    move/from16 v0, v18

    if-lt v6, v0, :cond_6

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v18

    const/16 v19, 0x0

    aget v19, v12, v19

    aget-object v11, v18, v19

    :cond_6
    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v7, v11, v2}, Lcom/mstar/android/tv/TvPipPopManager;->enablePipTV(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;Lcom/mstar/android/tvapi/common/vo/VideoWindowType;)Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;

    move-result-object v10

    sget-object v18, Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;->E_PIP_SUCCESS:Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;

    move-object/from16 v0, v18

    if-eq v10, v0, :cond_7

    sget-object v18, Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;->E_PIP_PIP_MODE_OPENED:Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;

    move-object/from16 v0, v18

    if-ne v10, v0, :cond_9

    :cond_7
    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v18

    const/16 v19, 0x1

    invoke-virtual/range {v18 .. v19}, Lcom/mstar/android/tv/TvPipPopManager;->setPipOnFlag(Z)Z

    const-string v18, "MenuHolderPIP==>displayPIP, enablePip"

    invoke-static/range {v18 .. v18}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    const-string v18, "MenuHolderPIP==>displayPIP, done."

    invoke-static/range {v18 .. v18}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    new-instance v8, Landroid/content/Intent;

    const-string v18, "com.konka.hotkey.enablePip"

    move-object/from16 v0, v18

    invoke-direct {v8, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v8}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->sendBroadcast(Landroid/content/Intent;)V

    const/16 v18, 0x1

    goto/16 :goto_0

    :catch_0
    move-exception v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto/16 :goto_1

    :cond_8
    invoke-virtual {v11}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v18

    aget v19, v12, v6

    move/from16 v0, v18

    move/from16 v1, v19

    if-eq v0, v1, :cond_5

    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    :cond_9
    const-string v18, "MenuHolderPIP==>displayPIP, fail."

    invoke-static/range {v18 .. v18}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    const/16 v18, 0x0

    goto/16 :goto_0
.end method

.method public displayPOP()Z
    .locals 10

    const/4 v7, 0x1

    const/4 v6, 0x0

    invoke-static {}, Lcom/mstar/android/tv/TvCommonManager;->getInstance()Lcom/mstar/android/tv/TvCommonManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/mstar/android/tv/TvCommonManager;->getCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v1

    const-string v8, "MenuHolderPIP==>displayPOP, start."

    invoke-static {v8}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/mstar/android/tv/TvPipPopManager;->isPipEnabled()Z

    move-result v8

    if-eqz v8, :cond_0

    new-instance v2, Landroid/content/Intent;

    const-string v8, "com.konka.hotkey.disablePip"

    invoke-direct {v2, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    invoke-virtual {v8, v2}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->sendBroadcast(Landroid/content/Intent;)V

    const-string v8, "MenuHolderPIP==>displayPOP, disablePip"

    invoke-static {v8}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/mstar/android/tv/TvPipPopManager;->disablePip()Z

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v8

    invoke-virtual {v8, v6}, Lcom/mstar/android/tv/TvPipPopManager;->setPipOnFlag(Z)Z

    :cond_0
    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/mstar/android/tv/TvPipPopManager;->isDualViewEnabled()Z

    move-result v8

    if-eqz v8, :cond_1

    new-instance v2, Landroid/content/Intent;

    const-string v8, "com.konka.hotkey.disable3dDualView"

    invoke-direct {v2, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    invoke-virtual {v8, v2}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->sendBroadcast(Landroid/content/Intent;)V

    const-string v8, "MenuHolderPIP==>displayPOP, disable3dDualView"

    invoke-static {v8}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/mstar/android/tv/TvPipPopManager;->disable3dDualView()Z

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v8

    invoke-virtual {v8, v6}, Lcom/mstar/android/tv/TvPipPopManager;->setDualViewOnFlag(Z)Z

    :cond_1
    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/mstar/android/tv/TvPipPopManager;->getSubInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v4

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v8

    invoke-virtual {v8, v6}, Lcom/mstar/android/tv/TvPipPopManager;->getSubWindowSourceList(Z)[I

    move-result-object v5

    const/4 v0, 0x0

    const/4 v0, 0x0

    :goto_0
    array-length v8, v5

    if-lt v0, v8, :cond_5

    :cond_2
    array-length v8, v5

    if-lt v0, v8, :cond_3

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v8

    aget v9, v5, v6

    aget-object v4, v8, v9

    :cond_3
    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v8

    invoke-virtual {v8, v1, v4}, Lcom/mstar/android/tv/TvPipPopManager;->enablePopTV(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;

    move-result-object v3

    sget-object v8, Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;->E_PIP_SUCCESS:Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;

    if-eq v3, v8, :cond_4

    sget-object v8, Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;->E_PIP_POP_MODE_OPENED:Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;

    if-ne v3, v8, :cond_6

    :cond_4
    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v6

    invoke-virtual {v6, v7}, Lcom/mstar/android/tv/TvPipPopManager;->setPopOnFlag(Z)Z

    const-string v6, "MenuHolderPIP==>displayPOP, enablePop"

    invoke-static {v6}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    const-string v6, "MenuHolderPIP==>displayPOP, done."

    invoke-static {v6}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    new-instance v2, Landroid/content/Intent;

    const-string v6, "com.konka.hotkey.enablePop"

    invoke-direct {v2, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    invoke-virtual {v6, v2}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->sendBroadcast(Landroid/content/Intent;)V

    move v6, v7

    :goto_1
    return v6

    :cond_5
    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v8

    aget v9, v5, v0

    if-eq v8, v9, :cond_2

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_6
    const-string v7, "MenuHolderPIP==>displayPOP, fail."

    invoke-static {v7}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public displaySubChannelFullScreen()V
    .locals 4

    const/4 v3, 0x0

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tv/TvPipPopManager;->getSubInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mSubInputSrc:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    invoke-virtual {v1}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getDataBaseManagerInstance()Lcom/konka/kkinterface/tv/DataBaseDesk;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mSubInputSrc:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-interface {v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk;->saveInputSource(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Z

    const-string v1, "MenuHolderPIP==>displaySubScreenFullScreen, start."

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tv/TvPipPopManager;->isPipEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.konka.hotkey.disablePip"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    invoke-virtual {v1, v0}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->sendBroadcast(Landroid/content/Intent;)V

    const-string v1, "MenuHolderPIP==>displaySubChannelFullScreen, disablePip"

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tv/TvPipPopManager;->disablePip()Z

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/mstar/android/tv/TvPipPopManager;->setPipOnFlag(Z)Z

    :cond_0
    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tv/TvPipPopManager;->isPopEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.konka.hotkey.disablePop"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    invoke-virtual {v1, v0}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->sendBroadcast(Landroid/content/Intent;)V

    const-string v1, "MenuHolderPIP==>displaySubChannelFullScreen, disablePop"

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tv/TvPipPopManager;->disablePop()Z

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/mstar/android/tv/TvPipPopManager;->setPopOnFlag(Z)Z

    :cond_1
    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tv/TvPipPopManager;->isDualViewEnabled()Z

    move-result v1

    if-eqz v1, :cond_2

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.konka.hotkey.disable3dDualView"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    invoke-virtual {v1, v0}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->sendBroadcast(Landroid/content/Intent;)V

    const-string v1, "MenuHolderPIP==>displaySubChannelFullScreen, disable3dDualView"

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tv/TvPipPopManager;->disable3dDualView()Z

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/mstar/android/tv/TvPipPopManager;->setDualViewOnFlag(Z)Z

    :cond_2
    invoke-virtual {p0}, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->enterSubWindow()V

    invoke-virtual {p0}, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->dismiss()V

    return-void
.end method

.method public displaySyncWatching()Z
    .locals 21

    invoke-static {}, Lcom/mstar/android/tv/TvCommonManager;->getInstance()Lcom/mstar/android/tv/TvCommonManager;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/mstar/android/tv/TvCommonManager;->getCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v6

    new-instance v19, Ljava/lang/StringBuilder;

    const-string v20, "Test by zhoujun ----------inputSource =======>>>>>>> "

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    const-string v19, "MenuHolderPIP==>displaySyncWatching, start."

    invoke-static/range {v19 .. v19}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/mstar/android/tv/TvPipPopManager;->isPipEnabled()Z

    move-result v19

    if-eqz v19, :cond_0

    new-instance v7, Landroid/content/Intent;

    const-string v19, "com.konka.hotkey.disablePip"

    move-object/from16 v0, v19

    invoke-direct {v7, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v7}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->sendBroadcast(Landroid/content/Intent;)V

    const-string v19, "MenuHolderPIP==>displaySyncWatching, disablePip"

    invoke-static/range {v19 .. v19}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/mstar/android/tv/TvPipPopManager;->disablePip()Z

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v19

    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Lcom/mstar/android/tv/TvPipPopManager;->setPipOnFlag(Z)Z

    :cond_0
    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/mstar/android/tv/TvPipPopManager;->isPopEnabled()Z

    move-result v19

    if-eqz v19, :cond_1

    new-instance v7, Landroid/content/Intent;

    const-string v19, "com.konka.hotkey.disablePop"

    move-object/from16 v0, v19

    invoke-direct {v7, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v7}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->sendBroadcast(Landroid/content/Intent;)V

    const-string v19, "MenuHolderPIP==>displaySyncWatching, disablePop"

    invoke-static/range {v19 .. v19}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/mstar/android/tv/TvPipPopManager;->disablePop()Z

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v19

    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Lcom/mstar/android/tv/TvPipPopManager;->setPopOnFlag(Z)Z

    :cond_1
    const/4 v10, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v19

    if-eqz v19, :cond_5

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/mstar/android/tvapi/common/PictureManager;->getPanelWidthHeight()Lcom/mstar/android/tvapi/common/vo/PanelProperty;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v10

    :goto_0
    const/16 v16, 0x780

    const/16 v4, 0x438

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/4 v15, 0x0

    const/4 v3, 0x0

    if-eqz v10, :cond_2

    iget v0, v10, Lcom/mstar/android/tvapi/common/vo/PanelProperty;->width:I

    move/from16 v16, v0

    iget v4, v10, Lcom/mstar/android/tvapi/common/vo/PanelProperty;->height:I

    :cond_2
    move/from16 v15, v16

    move v3, v4

    new-instance v9, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;

    invoke-direct {v9}, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;-><init>()V

    new-instance v14, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;

    invoke-direct {v14}, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;-><init>()V

    move/from16 v0, v17

    iput v0, v14, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->x:I

    move/from16 v0, v17

    iput v0, v9, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->x:I

    move/from16 v0, v18

    iput v0, v14, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->y:I

    move/from16 v0, v18

    iput v0, v9, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->y:I

    iput v15, v14, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->width:I

    iput v15, v9, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->width:I

    iput v3, v14, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->height:I

    iput v3, v9, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->height:I

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/mstar/android/tv/TvPipPopManager;->getSubInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v12

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v19

    const/16 v20, 0x1

    invoke-virtual/range {v19 .. v20}, Lcom/mstar/android/tv/TvPipPopManager;->getSubWindowSourceList(Z)[I

    move-result-object v13

    const/4 v5, 0x0

    const/4 v5, 0x0

    :goto_1
    array-length v0, v13

    move/from16 v19, v0

    move/from16 v0, v19

    if-lt v5, v0, :cond_6

    :cond_3
    array-length v0, v13

    move/from16 v19, v0

    move/from16 v0, v19

    if-lt v5, v0, :cond_4

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v19

    const/16 v20, 0x0

    aget v20, v13, v20

    aget-object v12, v19, v20

    :cond_4
    new-instance v8, Landroid/content/Intent;

    const-string v19, "com.konka.tv.hotkey.service.UNFREEZE"

    move-object/from16 v0, v19

    invoke-direct {v8, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v8}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->sendBroadcast(Landroid/content/Intent;)V

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v6, v12, v9, v14}, Lcom/mstar/android/tv/TvPipPopManager;->enable3dDualView(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;Lcom/mstar/android/tvapi/common/vo/VideoWindowType;Lcom/mstar/android/tvapi/common/vo/VideoWindowType;)Z

    move-result v11

    if-eqz v11, :cond_7

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v19

    const/16 v20, 0x1

    invoke-virtual/range {v19 .. v20}, Lcom/mstar/android/tv/TvPipPopManager;->setDualViewOnFlag(Z)Z

    const-string v19, "MenuHolderPIP==>displaySyncWatching, enable3dDualView."

    invoke-static/range {v19 .. v19}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    const-string v19, "MenuHolderPIP==>displaySyncWatching, done."

    invoke-static/range {v19 .. v19}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    new-instance v7, Landroid/content/Intent;

    const-string v19, "com.konka.hotkey.enable3dDualView"

    move-object/from16 v0, v19

    invoke-direct {v7, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v7}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->sendBroadcast(Landroid/content/Intent;)V

    :goto_2
    return v11

    :cond_5
    :try_start_1
    const-string v19, "TvManger.getInstance() == null"

    invoke-static/range {v19 .. v19}, Lcom/konka/debuginfo/logPrint;->Fatal(Ljava/lang/String;)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto/16 :goto_0

    :cond_6
    invoke-virtual {v12}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v19

    aget v20, v13, v5

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_3

    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    :cond_7
    const-string v19, "MenuHolderPIP==>displaySyncWatching, fail."

    invoke-static/range {v19 .. v19}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    goto :goto_2
.end method

.method public doUpdateMenuStateNotSupport(Lcom/konka/hotkey/view/Com3DPIPItem;)V
    .locals 1
    .param p1    # Lcom/konka/hotkey/view/Com3DPIPItem;

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mItemNormalScreen:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->doUpdate()Z

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->doSendFocusToNearItem(Lcom/konka/hotkey/view/Com3DPIPItem;Z)V

    const-string v0, "MenuHolderPIP==>doUpdateMenuStateNotSupport, done."

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    return-void
.end method

.method public enterSubWindow()V
    .locals 5

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Start to enter the mSubInputSrc fullscreen ===="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mSubInputSrc:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    invoke-virtual {v2}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->isKtvOrStorage()Z

    move-result v2

    if-eqz v2, :cond_0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.konka.GO_TO_TV"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    invoke-virtual {v2, v1}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->sendBroadcast(Landroid/content/Intent;)V

    :goto_0
    const-string v2, "MenuHolderPIP==>displaySubScreenFullScreen, done."

    invoke-static {v2}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    return-void

    :cond_0
    iget-object v2, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    invoke-virtual {v2}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v2

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v2

    iget-object v3, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mSubInputSrc:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    const/4 v4, 0x1

    invoke-interface {v2, v3, v4}, Lcom/konka/kkinterface/tv/CommonDesk;->SetInputSource(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;Z)V

    iget-object v2, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mSubInputSrc:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_ATV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    invoke-virtual {v2}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v2

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getChannelManagerInstance()Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v2

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/ChannelDesk;->getCurrProgramInfo()Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    move-result-object v2

    iget v0, v2, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->number:I

    iget-object v2, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    invoke-virtual {v2}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v2

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getChannelManagerInstance()Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v2

    sget-object v3, Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;->E_SERVICETYPE_ATV:Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;

    invoke-interface {v2, v0, v3}, Lcom/konka/kkinterface/tv/ChannelDesk;->programSel(ILcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;)Z

    :cond_1
    :goto_1
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "com.konka.tvsettings"

    const-string v3, "com.konka.tvsettings.popup.SourceInfoActivity"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v2, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    invoke-virtual {v2, v1}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mSubInputSrc:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-ne v2, v3, :cond_1

    iget-object v2, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    invoke-virtual {v2}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v2

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getChannelManagerInstance()Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v2

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/ChannelDesk;->dtvplayCurrentProgram()Z

    goto :goto_1
.end method

.method public findViews()V
    .locals 2

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    const v1, 0x7f0a002a    # com.konka.hotkey.R.id.menu_doublechannel

    invoke-virtual {v0, v1}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mContainer:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    iget-boolean v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->m_bIsSyncWatchSupportable:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->addItemSyncWatching()V

    :goto_0
    invoke-direct {p0}, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->addItemPOP()V

    invoke-direct {p0}, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->addItemPIP()V

    invoke-direct {p0}, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->addItemSubChannelSelection()V

    invoke-direct {p0}, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->addItemSubChannelFullScreen()V

    invoke-direct {p0}, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->addItemNormalScreen()V

    const-string v0, "MenuHolderPIP==>findViews done!"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    return-void

    :cond_0
    const-string v0, "MenuHolderPIP==>findViews, ItemSyncWatch is not added for nonsupport!!!"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getLastFocusItem()Lcom/konka/hotkey/view/Com3DPIPItem;
    .locals 1

    const-string v0, "MenuHolderPIP==>getLastFocusItem"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mItemLastFocus:Lcom/konka/hotkey/view/Com3DPIPItem;

    return-object v0
.end method

.method public initItemStates(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    invoke-virtual {v0}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->is3DEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    invoke-virtual {v0}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->findFocusFor3DMenu()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->m_bIsMenuForbidden:Z

    invoke-virtual {p0}, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->setMenuForbidden()Z

    const-string v0, "MenuHolderPIP==>initItemStates, is playing in 3d mode!!!"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    invoke-virtual {v0}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->isPIPEnabled()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tv/TvPipPopManager;->isDualViewEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0, p1}, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->updateItemStateSyncWatching(Z)V

    :cond_1
    :goto_1
    const-string v0, "MenuHolderPIP==>initItemStates done."

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tv/TvPipPopManager;->isPopEnabled()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0, p1}, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->updateItemStatePOP(Z)V

    goto :goto_1

    :cond_3
    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tv/TvPipPopManager;->isPipEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0, p1}, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->updateItemStatePIP(Z)V

    goto :goto_1

    :cond_4
    invoke-virtual {p0, p1}, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->updateItemStateNormalScreen(Z)V

    goto :goto_1
.end method

.method public loadDataToUI()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x1

    const-string v0, "MenuHolderPIP==>loadDataToUI start!"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tv/TvPipPopManager;->isDualViewEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iput v2, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->CURR_MODE:I

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    invoke-virtual {v0, v1}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->setPIPEnabled(Z)V

    const-string v0, "MenuHolderPIP==>loadDataToUI CURR_MODE = HKDC_SYNCW;"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    :goto_0
    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    invoke-virtual {v0}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->is3DEnabled()Z

    move-result v0

    iput-boolean v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->m_bIsMenuForbidden:Z

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "MenuHolderPIP==>loadDataToUI, m_bIsMenuForbidden = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->m_bIsMenuForbidden:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    const-string v0, "MenuHolderPIP==>loadDataToUI done!"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    return-void

    :cond_0
    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tv/TvPipPopManager;->isPipEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iput v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->CURR_MODE:I

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    invoke-virtual {v0, v1}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->setPIPEnabled(Z)V

    const-string v0, "MenuHolderPIP==>loadDataToUI CURR_MODE = HKDC_PIP;"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tv/TvPipPopManager;->isPopEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    iput v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->CURR_MODE:I

    const-string v0, "MenuHolderPIP==>loadDataToUI CURR_MODE = HKDC_POP;"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x5

    iput v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->CURR_MODE:I

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    invoke-virtual {v0, v2}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->setPIPEnabled(Z)V

    const-string v0, "MenuHolderPIP==>loadDataToUI CURR_MODE = HKDC_NORS;"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public micToast()V
    .locals 7

    const/4 v6, 0x0

    iget-object v3, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    invoke-virtual {v3}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f080044    # com.konka.hotkey.R.string.warning_mic_status

    const/4 v5, 0x1

    invoke-static {v3, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    const/16 v3, 0x11

    invoke-virtual {v1, v3, v6, v6}, Landroid/widget/Toast;->setGravity(III)V

    const/16 v3, 0x3e8

    invoke-virtual {v1, v3}, Landroid/widget/Toast;->setDuration(I)V

    invoke-virtual {v1}, Landroid/widget/Toast;->getView()Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    new-instance v0, Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    invoke-virtual {v3}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v0, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    const-string v3, "===========Warning:Mic opened!========="

    invoke-static {v3}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    return-void
.end method

.method public popToast()V
    .locals 7

    const/4 v6, 0x0

    iget-object v3, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    invoke-virtual {v3}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f080043    # com.konka.hotkey.R.string.warning_notsupport_func

    const/4 v5, 0x1

    invoke-static {v3, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    const/16 v3, 0x11

    invoke-virtual {v1, v3, v6, v6}, Landroid/widget/Toast;->setGravity(III)V

    const/16 v3, 0x3e8

    invoke-virtual {v1, v3}, Landroid/widget/Toast;->setDuration(I)V

    invoke-virtual {v1}, Landroid/widget/Toast;->getView()Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    new-instance v0, Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    invoke-virtual {v3}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v0, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    const-string v3, "===========Warning:can\'t open SyncWatching in this state========="

    invoke-static {v3}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    return-void
.end method

.method public setMenuForbidden()Z
    .locals 2

    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->m_bIsMenuForbidden:Z

    if-nez v1, :cond_1

    iput-boolean v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->m_bIsMenuForbidden:Z

    iget-boolean v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->m_bIsSyncWatchSupportable:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mItemSyncWatching:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v1}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateForbidden()V

    :cond_0
    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mItemPOP:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v1}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateForbidden()V

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mItemPIP:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v1}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateForbidden()V

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mItemSubChannelSelection:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v1}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateForbidden()V

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mItemSubChannelFullScreen:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v1}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateForbidden()V

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mItemNormalScreen:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v1}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateForbidden()V

    const-string v1, "MenuHolderPIP==>setMenuForbidden, m_bIsMenuForbidden:false>>>true! All items forbidden."

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    :goto_0
    return v0

    :cond_1
    const-string v0, "MenuHolderPIP==>setMenuForbidden, m_bIsMenuForbidden:true >>>true! Did nothing, already forbidden!"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setMenuResume()Z
    .locals 2

    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->m_bIsMenuForbidden:Z

    if-eqz v1, :cond_0

    iput-boolean v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->m_bIsMenuForbidden:Z

    invoke-virtual {p0, v0}, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->updateItemStates(Z)V

    const-string v0, "MenuHolderPIP==>setMenuResume, m_bIsMenuForbidden:true >>>false! Done."

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const-string v1, "MenuHolderPIP==>setMenuResume, m_bIsMenuForbidden:false>>>false! Did nothing, already resume!"

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public showSubChannelSelectionDialog()V
    .locals 4

    const-string v0, "MenuHolderPIP==>showSubChannelSelectionDialog, start."

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    const-class v3, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->startActivity(Landroid/content/Intent;)V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mItemSubChannelSelection:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->requestFocus()Z

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mItemSubChannelSelection:Lcom/konka/hotkey/view/Com3DPIPItem;

    const v1, 0x7f020056    # com.konka.hotkey.R.drawable.hk_dc_subscr_run

    invoke-virtual {v0, v1}, Lcom/konka/hotkey/view/Com3DPIPItem;->setBackgroundResource(I)V

    const-string v0, "MenuHolderPIP==>showSubChannelSelectionDialog, done."

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    return-void
.end method

.method public updateItemStateNormalScreen(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mItemNormalScreen:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateInUse()V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mItemSubChannelSelection:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateForbidden()V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mItemSubChannelFullScreen:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateForbidden()V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    invoke-virtual {v0}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->resume3DMenu()V

    iget-boolean v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->m_bIsSyncWatchSupportable:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->m_bIsPOPSupportable:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mItemPOP:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateNormal()V

    :goto_0
    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mItemPIP:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateNormal()V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mItemSyncWatching:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-direct {p0, v0, p1}, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->doSendFocusToNearItem(Lcom/konka/hotkey/view/Com3DPIPItem;Z)V

    :goto_1
    const-string v0, "MenuHolderPIP==>updateItemStateNormalScreen, done."

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mItemPOP:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateForbidden()V

    goto :goto_0

    :cond_1
    iget-boolean v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->m_bIsPOPSupportable:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mItemPIP:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateNormal()V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mItemPOP:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-direct {p0, v0, p1}, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->doSendFocusToNearItem(Lcom/konka/hotkey/view/Com3DPIPItem;Z)V

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mItemPOP:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateForbidden()V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mItemPIP:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-direct {p0, v0, p1}, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->doSendFocusToNearItem(Lcom/konka/hotkey/view/Com3DPIPItem;Z)V

    goto :goto_1
.end method

.method public updateItemStatePIP(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mItemPIP:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateInUse()V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mItemSubChannelSelection:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateNormal()V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mItemSubChannelFullScreen:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateNormal()V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mItemNormalScreen:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateNormal()V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    invoke-virtual {v0}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->forbidden3DMenu()V

    iget-boolean v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->m_bIsSyncWatchSupportable:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->m_bIsPOPSupportable:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mItemPOP:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateNormal()V

    :goto_0
    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mItemSyncWatching:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateForbidden()V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mItemSubChannelSelection:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-direct {p0, v0, p1}, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->doSendFocusToNearItem(Lcom/konka/hotkey/view/Com3DPIPItem;Z)V

    :goto_1
    const-string v0, "MenuHolderPIP==>updateItemStatePIP, done."

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mItemPOP:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateForbidden()V

    goto :goto_0

    :cond_1
    iget-boolean v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->m_bIsPOPSupportable:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mItemSubChannelSelection:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateNormal()V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mItemPOP:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-direct {p0, v0, p1}, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->doSendFocusToNearItem(Lcom/konka/hotkey/view/Com3DPIPItem;Z)V

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mItemPOP:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateForbidden()V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mItemSubChannelSelection:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-direct {p0, v0, p1}, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->doSendFocusToNearItem(Lcom/konka/hotkey/view/Com3DPIPItem;Z)V

    goto :goto_1
.end method

.method public updateItemStatePOP(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mItemPOP:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateInUse()V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mItemSubChannelSelection:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateNormal()V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mItemSubChannelFullScreen:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateNormal()V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mItemNormalScreen:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateNormal()V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    invoke-virtual {v0}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->forbidden3DMenu()V

    iget-boolean v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->m_bIsSyncWatchSupportable:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mItemPIP:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateNormal()V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mItemSyncWatching:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-direct {p0, v0, p1}, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->doSendFocusToNearItem(Lcom/konka/hotkey/view/Com3DPIPItem;Z)V

    :goto_0
    const-string v0, "MenuHolderPIP==>updateItemStatePOP, done."

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mItemPIP:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-direct {p0, v0, p1}, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->doSendFocusToNearItem(Lcom/konka/hotkey/view/Com3DPIPItem;Z)V

    goto :goto_0
.end method

.method public updateItemStateSubChannelFullScreen(Z)V
    .locals 1
    .param p1    # Z

    iget-boolean v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->m_bIsSyncWatchSupportable:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mItemSyncWatching:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateNormal()V

    :cond_0
    iget-boolean v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->m_bIsPOPSupportable:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mItemPOP:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateNormal()V

    :goto_0
    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mItemPIP:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateNormal()V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mItemSubChannelFullScreen:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateInUse()V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mItemNormalScreen:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateNormal()V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    invoke-virtual {v0}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->forbidden3DMenu()V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mItemSubChannelSelection:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-direct {p0, v0, p1}, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->doSendFocusToNearItem(Lcom/konka/hotkey/view/Com3DPIPItem;Z)V

    const-string v0, "MenuHolderPIP==>updateItemStateSubChannelFullScreen, done."

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mItemPOP:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateForbidden()V

    goto :goto_0
.end method

.method public updateItemStateSubChannelSelection(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mItemSubChannelSelection:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-direct {p0, v0, p1}, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->doSendFocusToNearItem(Lcom/konka/hotkey/view/Com3DPIPItem;Z)V

    const-string v0, "MenuHolderPIP==>updateItemStateSubChannelSelection, done."

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    return-void
.end method

.method public updateItemStateSyncWatching(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mItemSyncWatching:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateInUse()V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mItemSubChannelSelection:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateNormal()V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mItemSubChannelFullScreen:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateNormal()V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mItemNormalScreen:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateNormal()V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mActivity:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    invoke-virtual {v0}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->forbidden3DMenu()V

    iget-boolean v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->m_bIsPOPSupportable:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mItemPOP:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateForbidden()V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mItemPIP:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateForbidden()V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mItemSubChannelSelection:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-direct {p0, v0, p1}, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->doSendFocusToNearItem(Lcom/konka/hotkey/view/Com3DPIPItem;Z)V

    :goto_0
    const-string v0, "MenuHolderPIP==>updateItemStateSyncWatching, done."

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mItemPIP:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateNormal()V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->mItemPOP:Lcom/konka/hotkey/view/Com3DPIPItem;

    invoke-direct {p0, v0, p1}, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->doSendFocusToNearItem(Lcom/konka/hotkey/view/Com3DPIPItem;Z)V

    goto :goto_0
.end method

.method public updateItemStates(Z)V
    .locals 0
    .param p1    # Z

    invoke-virtual {p0, p1}, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->initItemStates(Z)V

    return-void
.end method
