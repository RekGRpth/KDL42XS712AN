.class public Lcom/konka/hotkey/video3dpip/Video3DAutoActivity;
.super Landroid/app/Activity;
.source "Video3DAutoActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$KeyListener;,
        Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView;
    }
.end annotation


# static fields
.field private static final ACTION_3DAUTO_RETURN:Ljava/lang/String; = "com.konka.hotkey.ACTION_3DAUTO_RETURN"


# instance fields
.field private final DISMISS_PROGRESSDIALOG:I

.field private IMG_HEIGHT:I

.field private IMG_WIDTH:I

.field private final StrNameId:I

.field private final StrValueIds:[I

.field private final TEXT_SIZE_NOR:F

.field private m3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;

.field private mViewContainer:Landroid/widget/LinearLayout;

.field private mViewLlAuto:Landroid/widget/LinearLayout;

.field private mViewSwitch:Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView;

.field private myHandler:Landroid/os/Handler;

.field private pd:Landroid/app/ProgressDialog;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity;->pd:Landroid/app/ProgressDialog;

    const/16 v0, 0x1ce

    iput v0, p0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity;->IMG_WIDTH:I

    const/16 v0, 0x3e

    iput v0, p0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity;->IMG_HEIGHT:I

    const/high16 v0, 0x41700000    # 15.0f

    iput v0, p0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity;->TEXT_SIZE_NOR:F

    const v0, 0x7f080023    # com.konka.hotkey.R.string.str_hk_3d_menu_auto

    iput v0, p0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity;->StrNameId:I

    const/4 v0, 0x1

    iput v0, p0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity;->DISMISS_PROGRESSDIALOG:I

    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity;->StrValueIds:[I

    new-instance v0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$1;

    invoke-direct {v0, p0}, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$1;-><init>(Lcom/konka/hotkey/video3dpip/Video3DAutoActivity;)V

    iput-object v0, p0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity;->myHandler:Landroid/os/Handler;

    return-void

    :array_0
    .array-data 4
        0x7f080002    # com.konka.hotkey.R.string.str_hk_state_off
        0x7f080001    # com.konka.hotkey.R.string.str_hk_state_on
    .end array-data
.end method

.method static synthetic access$0(Lcom/konka/hotkey/video3dpip/Video3DAutoActivity;)Landroid/app/ProgressDialog;
    .locals 1

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity;->pd:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$1(Lcom/konka/hotkey/video3dpip/Video3DAutoActivity;)I
    .locals 1

    iget v0, p0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity;->IMG_WIDTH:I

    return v0
.end method

.method static synthetic access$2(Lcom/konka/hotkey/video3dpip/Video3DAutoActivity;)I
    .locals 1

    iget v0, p0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity;->IMG_HEIGHT:I

    return v0
.end method

.method static synthetic access$3(Lcom/konka/hotkey/video3dpip/Video3DAutoActivity;)[I
    .locals 1

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity;->StrValueIds:[I

    return-object v0
.end method

.method static synthetic access$4(Lcom/konka/hotkey/video3dpip/Video3DAutoActivity;)Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView;
    .locals 1

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity;->mViewSwitch:Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView;

    return-object v0
.end method

.method static synthetic access$5(Lcom/konka/hotkey/video3dpip/Video3DAutoActivity;)Lcom/konka/kkinterface/tv/S3DDesk;
    .locals 1

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity;->m3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    return-object v0
.end method

.method static synthetic access$6(Lcom/konka/hotkey/video3dpip/Video3DAutoActivity;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity;->myHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private updateMenuPosition(Z)V
    .locals 4
    .param p1    # Z

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "POPPIP==>Video3DAuto==>IS_DOBULD_CHANNEL: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    const v2, 0x7f0a0012    # com.konka.hotkey.R.id.hk_3d_menu_auto_layout

    invoke-virtual {p0, v2}, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    if-eqz v1, :cond_3

    if-nez p1, :cond_1

    invoke-virtual {p0}, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v2

    invoke-virtual {p0}, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/konka/kkinterface/tv/CommonDesk;->is1280x720(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v2, 0x97

    iput v2, v1, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const-string v2, "DoubleChannel==>Video3DAuto==>updateMenuPosition: success!"

    invoke-static {v2}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    :goto_1
    return-void

    :cond_0
    const/16 v2, 0xdd

    iput v2, v1, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v2

    invoke-virtual {p0}, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/konka/kkinterface/tv/CommonDesk;->is1280x720(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/16 v2, 0xea

    iput v2, v1, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    goto :goto_0

    :cond_2
    const/16 v2, 0x166

    iput v2, v1, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    goto :goto_0

    :cond_3
    const-string v2, "DoubleChannel==>Video3DAuto==>updateMenuPosition: fail!"

    invoke-static {v2}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    goto :goto_1
.end method

.method private viewInit()V
    .locals 6

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity;->m3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/S3DDesk;->getSelfAdaptiveDetect()Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;

    move-result-object v2

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;->DB_ThreeD_Video_SELF_ADAPTIVE_DETECT_OFF:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;

    if-ne v2, v3, :cond_0

    const/4 v0, 0x0

    :goto_0
    new-instance v2, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$2;

    invoke-virtual {p0}, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, p0, v3, v0}, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$2;-><init>(Lcom/konka/hotkey/video3dpip/Video3DAutoActivity;Landroid/content/Context;I)V

    iput-object v2, p0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity;->mViewSwitch:Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView;

    iget-object v2, p0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity;->mViewContainer:Landroid/widget/LinearLayout;

    iget-object v3, p0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity;->mViewSwitch:Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView;

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    new-instance v1, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$KeyListener;

    invoke-direct {v1, p0}, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$KeyListener;-><init>(Lcom/konka/hotkey/video3dpip/Video3DAutoActivity;)V

    iget-object v2, p0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity;->mViewSwitch:Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView;

    invoke-virtual {v2, v1}, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity$S3DAutoView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    new-instance v2, Landroid/app/ProgressDialog;

    invoke-direct {v2, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity;->pd:Landroid/app/ProgressDialog;

    iget-object v2, p0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity;->pd:Landroid/app/ProgressDialog;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f080032    # com.konka.hotkey.R.string.str_hk_3d_auto_wait

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    return-void

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-static {p0}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getS3DManagerInstance()Lcom/konka/kkinterface/tv/S3DDesk;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity;->m3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    const v1, 0x7f030005    # com.konka.hotkey.R.layout.hk_video3d_auto_menu

    invoke-virtual {p0, v1}, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity;->setContentView(I)V

    const v1, 0x7f0a0012    # com.konka.hotkey.R.id.hk_3d_menu_auto_layout

    invoke-virtual {p0, v1}, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity;->mViewLlAuto:Landroid/widget/LinearLayout;

    const v1, 0x7f0a0014    # com.konka.hotkey.R.id.hk_3d_menu_auto_container

    invoke-virtual {p0, v1}, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity;->mViewContainer:Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/high16 v2, 0x7f070000    # com.konka.hotkey.R.dimen.TVHotkeyS3dEnItem_Width

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity;->IMG_WIDTH:I

    invoke-virtual {p0}, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070001    # com.konka.hotkey.R.dimen.TVHotkeyS3dEnItem_Height

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity;->IMG_HEIGHT:I

    invoke-direct {p0}, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity;->viewInit()V

    invoke-virtual {p0}, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "IS_DOBULE_CHANNEL"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity;->updateMenuPosition(Z)V

    return-void
.end method

.method protected onResume()V
    .locals 1

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity;->myHandler:Landroid/os/Handler;

    invoke-static {v0}, Lcom/konka/hotkey/LittleDownTimer;->setHandler(Landroid/os/Handler;)V

    invoke-static {}, Lcom/konka/hotkey/LittleDownTimer;->resumeMenuTime()V

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    return-void
.end method

.method protected onStart()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    const/high16 v0, 0x7f040000    # com.konka.hotkey.R.anim.anim_zoom_in

    const v1, 0x7f040001    # com.konka.hotkey.R.anim.anim_zoom_out

    invoke-virtual {p0, v0, v1}, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity;->overridePendingTransition(II)V

    return-void
.end method

.method protected onStop()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    invoke-virtual {p0}, Lcom/konka/hotkey/video3dpip/Video3DAutoActivity;->finish()V

    return-void
.end method

.method public onUserInteraction()V
    .locals 0

    invoke-static {}, Lcom/konka/hotkey/LittleDownTimer;->resetMenuTime()V

    invoke-super {p0}, Landroid/app/Activity;->onUserInteraction()V

    return-void
.end method
