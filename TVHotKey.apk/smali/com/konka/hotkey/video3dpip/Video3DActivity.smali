.class public Lcom/konka/hotkey/video3dpip/Video3DActivity;
.super Landroid/app/Activity;
.source "Video3DActivity.java"


# static fields
.field private static final ACTION_3DAUTO_RETURN:Ljava/lang/String; = "com.konka.hotkey.ACTION_3DAUTO_RETURN"

.field private static final ACTION_3DEN_RETURN:Ljava/lang/String; = "com.konka.hotkey.ACTION_3DEN_RETURN"

.field private static final ACTION_PIPSCSD_RETURN:Ljava/lang/String; = "com.konka.hotkey.ACTION_PIPSCSD_RETURN"

.field private static final LANGUAGE_EN:Ljava/lang/String; = "en"

.field private static final LANGUAGE_ZH:Ljava/lang/String; = "zh"


# instance fields
.field public final COMM_RET_AUTO:I

.field public final COMM_RET_EN:I

.field public final COMM_RET_SUBCHANNEL:I

.field private CURRENT_LANGUAGE:Ljava/lang/String;

.field private ITEM_HEIGHT:I

.field private ITEM_WIDTH:I

.field private TXT_SIZE_NOR:F

.field private TXT_SIZE_SEL:F

.field mReceiver:Landroid/content/BroadcastReceiver;

.field protected m_bIs3DClar:Z

.field protected m_bIs3DEnabled:Z

.field private m_bIsDoubleChannel:Z

.field protected m_bIsFramePacking:Z

.field private m_bIsKtvOrStorage:Z

.field private m_bIsPIPEnabled:Z

.field private m_bIsReceiverRegisted:Z

.field private m_bIsSupportSyncView:Z

.field private menu3DHolder:Lcom/konka/hotkey/video3dpip/MenuHolder3D;

.field private menuPIPHolder:Lcom/konka/hotkey/video3dpip/MenuHolderPIP;

.field private ms3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;

.field private myHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput v1, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->COMM_RET_EN:I

    const/4 v0, 0x1

    iput v0, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->COMM_RET_AUTO:I

    const/4 v0, 0x2

    iput v0, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->COMM_RET_SUBCHANNEL:I

    iput-boolean v1, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->m_bIsKtvOrStorage:Z

    iput-boolean v1, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->m_bIsPIPEnabled:Z

    iput-boolean v1, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->m_bIs3DEnabled:Z

    iput-boolean v1, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->m_bIs3DClar:Z

    iput-boolean v1, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->m_bIsFramePacking:Z

    iput-boolean v1, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->m_bIsSupportSyncView:Z

    const/high16 v0, 0x41600000    # 14.0f

    iput v0, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->TXT_SIZE_NOR:F

    const/high16 v0, 0x41800000    # 16.0f

    iput v0, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->TXT_SIZE_SEL:F

    new-instance v0, Lcom/konka/hotkey/video3dpip/Video3DActivity$1;

    invoke-direct {v0, p0}, Lcom/konka/hotkey/video3dpip/Video3DActivity$1;-><init>(Lcom/konka/hotkey/video3dpip/Video3DActivity;)V

    iput-object v0, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->myHandler:Landroid/os/Handler;

    iput-boolean v1, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->m_bIsReceiverRegisted:Z

    new-instance v0, Lcom/konka/hotkey/video3dpip/Video3DActivity$2;

    invoke-direct {v0, p0}, Lcom/konka/hotkey/video3dpip/Video3DActivity$2;-><init>(Lcom/konka/hotkey/video3dpip/Video3DActivity;)V

    iput-object v0, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method private checkSyncViewSupportable()Z
    .locals 6

    new-instance v0, Lcom/konka/kkimplements/common/IniEditor;

    invoke-direct {v0}, Lcom/konka/kkimplements/common/IniEditor;-><init>()V

    const-string v4, "/customercfg/panel/panel.ini"

    invoke-virtual {v0, v4}, Lcom/konka/kkimplements/common/IniEditor;->loadFile(Ljava/lang/String;)Z

    const-string v4, "panel:b3DPanel"

    const-string v5, "0"

    invoke-virtual {v0, v4, v5}, Lcom/konka/kkimplements/common/IniEditor;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v4, "panel:bSGPanelFlag"

    const-string v5, "0"

    invoke-virtual {v0, v4, v5}, Lcom/konka/kkimplements/common/IniEditor;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v4

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v4

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/CommonDesk;->getPipInfo()Lcom/konka/kkinterface/tv/CommonDesk$PipInfo;

    move-result-object v4

    iget v4, v4, Lcom/konka/kkinterface/tv/CommonDesk$PipInfo;->enableDualView:I

    if-eqz v4, :cond_0

    const-string v4, "1"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "0"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v3, 0x1

    :goto_0
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Video3DActivity==>isSyncViewSupportable, supportable = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    return v3

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private dip2px(F)I
    .locals 3
    .param p1    # F

    invoke-virtual {p0}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v0, v1, Landroid/util/DisplayMetrics;->density:F

    mul-float v1, p1, v0

    const/high16 v2, 0x3f000000    # 0.5f

    add-float/2addr v1, v2

    float-to-int v1, v1

    return v1
.end method

.method private doCheckDoubleChannel()V
    .locals 15

    const/16 v14, 0x8

    const/4 v13, 0x2

    const/4 v12, 0x1

    const/4 v11, 0x0

    const-string v0, "Activity==>doCheckDoubleChannel start."

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f07000a    # com.konka.hotkey.R.dimen.TVHotkeyS3dItem_Width

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->ITEM_WIDTH:I

    invoke-virtual {p0}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f07000b    # com.konka.hotkey.R.dimen.TVHotkeyS3dItem_Height

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->ITEM_HEIGHT:I

    invoke-virtual {p0}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080003    # com.konka.hotkey.R.string.str_sys_language

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->CURRENT_LANGUAGE:Ljava/lang/String;

    const-string v0, "en"

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->CURRENT_LANGUAGE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/high16 v0, 0x41600000    # 14.0f

    iput v0, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->TXT_SIZE_NOR:F

    const/high16 v0, 0x41800000    # 16.0f

    iput v0, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->TXT_SIZE_SEL:F

    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->refreshStateParams()V

    const v0, 0x7f0a0028    # com.konka.hotkey.R.id.pip3d_menu_container

    invoke-virtual {p0, v0}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/LinearLayout;

    const v0, 0x7f0a002a    # com.konka.hotkey.R.id.menu_doublechannel

    invoke-virtual {p0, v0}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/LinearLayout;

    const v0, 0x7f0a0029    # com.konka.hotkey.R.id.hk_3d_menu_container

    invoke-virtual {p0, v0}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/LinearLayout;

    const-string v0, "Activity==>doCheckDoubleChannel adding 3d menu."

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    const-string v0, "doCheckDoubleChannel==>3D:ITEM_WIDTH = %d, ITEM_HEIGHT = %d;"

    new-array v1, v13, [Ljava/lang/Object;

    iget v4, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->ITEM_WIDTH:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v11

    iget v4, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->ITEM_HEIGHT:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v12

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    new-instance v0, Lcom/konka/hotkey/video3dpip/MenuHolder3D;

    iget v2, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->ITEM_WIDTH:I

    iget v3, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->ITEM_HEIGHT:I

    iget v4, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->TXT_SIZE_NOR:F

    iget v5, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->TXT_SIZE_SEL:F

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/konka/hotkey/video3dpip/MenuHolder3D;-><init>(Lcom/konka/hotkey/video3dpip/Video3DActivity;IIFF)V

    iput-object v0, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->menu3DHolder:Lcom/konka/hotkey/video3dpip/MenuHolder3D;

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->menu3DHolder:Lcom/konka/hotkey/video3dpip/MenuHolder3D;

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->myHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->setHandler(Landroid/os/Handler;)V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->menu3DHolder:Lcom/konka/hotkey/video3dpip/MenuHolder3D;

    invoke-virtual {v0}, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->loadDataToUI()V

    const-string v0, "Activity==>doCheckDoubleChannel add 3d menu done."

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    new-instance v6, Lcom/konka/kkimplements/common/IniEditor;

    invoke-direct {v6}, Lcom/konka/kkimplements/common/IniEditor;-><init>()V

    const-string v0, "/customercfg/panel/panel.ini"

    invoke-virtual {v6, v0}, Lcom/konka/kkimplements/common/IniEditor;->loadFile(Ljava/lang/String;)Z

    const-string v0, "panel:b3DPanel"

    const-string v1, "0"

    invoke-virtual {v6, v0, v1}, Lcom/konka/kkimplements/common/IniEditor;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "XXXXX==========>>>doCheckDoubleChannel ["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v1, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->m_bIsDoubleChannel:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->m_bIsSupportSyncView:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->m_bIsDoubleChannel:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->m_bIsSupportSyncView:Z

    if-nez v0, :cond_4

    :cond_1
    invoke-virtual {v10, v14}, Landroid/widget/LinearLayout;->setVisibility(I)V

    const-string v0, "Activity==>doCheckDoubleChannel no pip menu."

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    :goto_1
    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->menu3DHolder:Lcom/konka/hotkey/video3dpip/MenuHolder3D;

    invoke-virtual {v0, v12}, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->updateItemStates(Z)V

    const-string v0, "Activity==>doCheckDoubleChannel done!"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    const-string v0, "0"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {v9, v14}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :cond_2
    return-void

    :cond_3
    const-string v0, "zh"

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->CURRENT_LANGUAGE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/high16 v0, 0x41a00000    # 20.0f

    iput v0, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->TXT_SIZE_NOR:F

    const/high16 v0, 0x41b00000    # 22.0f

    iput v0, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->TXT_SIZE_SEL:F

    goto/16 :goto_0

    :cond_4
    const-string v0, "Activity==>doCheckDoubleChannel adding menu pip."

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-virtual {v10, v11}, Landroid/widget/LinearLayout;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f07000c    # com.konka.hotkey.R.dimen.TVHotkeyPIPMenuItem_Width

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v2, v0

    iput v2, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->ITEM_WIDTH:I

    iget v0, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->ITEM_HEIGHT:I

    add-int/lit8 v3, v0, -0x23

    invoke-static {p0}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/CommonDesk;->isEnableAdvancedPipSyncview()Z

    move-result v0

    if-eqz v0, :cond_5

    const/high16 v0, 0x43400000    # 192.0f

    invoke-direct {p0, v0}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->dip2px(F)I

    move-result v2

    iput v2, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->ITEM_WIDTH:I

    invoke-direct {p0}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->checkSyncViewSupportable()Z

    move-result v0

    if-nez v0, :cond_5

    const/high16 v0, 0x43480000    # 200.0f

    invoke-direct {p0, v0}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->dip2px(F)I

    move-result v2

    iput v2, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->ITEM_WIDTH:I

    const-string v0, "0"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget v3, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->ITEM_HEIGHT:I

    :cond_5
    const-string v0, "doCheckDoubleChannel==>PIP:ITEM_WIDTH = %d, ITEM_HEIGHT = %d;"

    new-array v1, v13, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v11

    iget v4, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->ITEM_HEIGHT:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v12

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    new-instance v0, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;

    iget v4, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->TXT_SIZE_NOR:F

    iget v5, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->TXT_SIZE_SEL:F

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;-><init>(Lcom/konka/hotkey/video3dpip/Video3DActivity;IIFF)V

    iput-object v0, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->menuPIPHolder:Lcom/konka/hotkey/video3dpip/MenuHolderPIP;

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->menuPIPHolder:Lcom/konka/hotkey/video3dpip/MenuHolderPIP;

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->myHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->setHandler(Landroid/os/Handler;)V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->menuPIPHolder:Lcom/konka/hotkey/video3dpip/MenuHolderPIP;

    invoke-virtual {v0}, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->loadDataToUI()V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->menuPIPHolder:Lcom/konka/hotkey/video3dpip/MenuHolderPIP;

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->menu3DHolder:Lcom/konka/hotkey/video3dpip/MenuHolder3D;

    invoke-virtual {v1}, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->isMenuForbidden()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->initItemStates(Z)V

    const-string v0, "Activity==>doCheckDoubleChannel add pip menu done."

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method private isReadyShow3D(Landroid/content/Context;)Z
    .locals 10
    .param p1    # Landroid/content/Context;

    const/4 v8, 0x1

    const/4 v7, 0x0

    const-string v6, "activity"

    invoke-virtual {p1, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    const/4 v6, 0x2

    invoke-virtual {v0, v6}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v6

    invoke-interface {v6, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v1, v6, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v9, "The package of second task is ===="

    invoke-direct {v6, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-static {p0}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v6

    invoke-interface {v6}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v6

    invoke-interface {v6}, Lcom/konka/kkinterface/tv/CommonDesk;->getPipInfo()Lcom/konka/kkinterface/tv/CommonDesk$PipInfo;

    move-result-object v6

    iget v6, v6, Lcom/konka/kkinterface/tv/CommonDesk$PipInfo;->enablePip:I

    if-nez v6, :cond_2

    const-string v6, "browser"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1

    :cond_0
    :goto_0
    return v7

    :cond_1
    const-string v6, "launcher"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    const-string v6, "avenger"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    const-string v6, "metrolauncher"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    const-string v6, "epg"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    const-string v6, "karaoke"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    const-string v6, "activitydoublechannel"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    move v7, v8

    goto :goto_0

    :cond_2
    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_NONE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v6

    if-eqz v6, :cond_3

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/mstar/android/tvapi/common/TvManager;->getCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v2

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v9, "currInputSrc === "

    invoke-direct {v6, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    sget-object v6, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_KTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-eq v2, v6, :cond_4

    sget-object v6, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_STORAGE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-eq v2, v6, :cond_4

    sget-object v6, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_STORAGE2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-eq v2, v6, :cond_4

    move v4, v7

    :goto_2
    const-string v6, "browser"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_5

    move v7, v8

    goto :goto_0

    :cond_3
    :try_start_1
    const-string v6, "TvManger.getInstance() == null"

    invoke-static {v6}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :catch_0
    move-exception v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1

    :cond_4
    move v4, v8

    goto :goto_2

    :cond_5
    const-string v6, "launcher"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_6

    const-string v6, "avenger"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_6

    const-string v6, "metrolauncher"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_7

    :cond_6
    if-eqz v4, :cond_0

    :cond_7
    const-string v6, "epg"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    const-string v6, "karaoke"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    const-string v6, "activitydoublechannel"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    move v7, v8

    goto/16 :goto_0
.end method

.method private refreshStateParams()V
    .locals 8

    const/4 v5, 0x1

    const/4 v6, 0x0

    const-string v4, "Activity==>refreshStateParams start!"

    invoke-static {v4}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-static {p0}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v4

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v4

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/CommonDesk;->getPipInfo()Lcom/konka/kkinterface/tv/CommonDesk$PipInfo;

    move-result-object v4

    iget v4, v4, Lcom/konka/kkinterface/tv/CommonDesk$PipInfo;->enablePip:I

    if-eqz v4, :cond_2

    move v4, v5

    :goto_0
    iput-boolean v4, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->m_bIsDoubleChannel:Z

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tv/TvPipPopManager;->isPopEnabled()Z

    move-result v4

    if-nez v4, :cond_3

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tv/TvPipPopManager;->isPipEnabled()Z

    move-result v4

    if-nez v4, :cond_3

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tv/TvPipPopManager;->isDualViewEnabled()Z

    move-result v4

    if-nez v4, :cond_3

    move v4, v6

    :goto_1
    iput-boolean v4, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->m_bIsPIPEnabled:Z

    iput-boolean v5, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->m_bIs3DEnabled:Z

    iput-boolean v6, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->m_bIsFramePacking:Z

    iput-boolean v6, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->m_bIs3DClar:Z

    iput-boolean v6, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->m_bIsKtvOrStorage:Z

    iput-boolean v6, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->m_bIsPIPEnabled:Z

    :try_start_0
    iget-object v4, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->ms3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/S3DDesk;->getDisplay3DTo2DMode()Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;

    move-result-object v1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    if-eqz v4, :cond_8

    iget-object v4, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->ms3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/S3DDesk;->getDisplayFormat()Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    move-result-object v3

    sget-object v4, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->DB_ThreeD_Video_DISPLAYFORMAT_COUNT:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    if-eq v3, v4, :cond_0

    sget-object v4, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->DB_ThreeD_Video_DISPLAYFORMAT_NONE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    if-ne v3, v4, :cond_4

    sget-object v4, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;->DB_ThreeD_Video_3DTO2D_NONE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;

    if-ne v1, v4, :cond_4

    :cond_0
    move v4, v6

    :goto_2
    iput-boolean v4, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->m_bIs3DEnabled:Z

    sget-object v4, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->DB_ThreeD_Video_DISPLAYFORMAT_FRAME_PACKING:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    if-ne v3, v4, :cond_5

    move v4, v5

    :goto_3
    iput-boolean v4, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->m_bIsFramePacking:Z

    sget-object v4, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->DB_ThreeD_Video_DISPLAYFORMAT_FRAME_PACKING:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    if-eq v3, v4, :cond_6

    sget-object v4, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->DB_ThreeD_Video_DISPLAYFORMAT_NONE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    if-ne v3, v4, :cond_1

    sget-object v4, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;->DB_ThreeD_Video_3DTO2D_FRAME_PACKING:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;

    if-eq v1, v4, :cond_6

    :cond_1
    move v4, v6

    :goto_4
    iput-boolean v4, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->m_bIs3DClar:Z

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v0

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_KTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-eq v0, v4, :cond_7

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_STORAGE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-eq v0, v4, :cond_7

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_STORAGE2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-eq v0, v4, :cond_7

    move v4, v6

    :goto_5
    iput-boolean v4, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->m_bIsKtvOrStorage:Z

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v7, "Activity==>refreshStateParams: display3DTo2D = "

    invoke-direct {v4, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v7, "Activity==>refreshStateParams: eS3DMode = "

    invoke-direct {v4, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v7, "Activity==>refreshStateParams: currInputSrc = "

    invoke-direct {v4, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v7, "Activity==>refreshStateParams: m_bIsDoubleChannel = "

    invoke-direct {v4, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v7, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->m_bIsDoubleChannel:Z

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v7, "Activity==>refreshStateParams: m_bIsKtvOrStorage = "

    invoke-direct {v4, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v7, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->m_bIsKtvOrStorage:Z

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v7, "Activity==>refreshStateParams: m_bIs3DEnabled = "

    invoke-direct {v4, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v7, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->m_bIs3DEnabled:Z

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v7, "Activity==>refreshStateParams: m_bIs3DClar = "

    invoke-direct {v4, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v7, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->m_bIs3DClar:Z

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v7, "Activity==>refreshStateParams: m_bIsFramePacking = "

    invoke-direct {v4, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v7, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->m_bIsFramePacking:Z

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_6
    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tv/TvPipPopManager;->isPipEnabled()Z

    move-result v4

    if-nez v4, :cond_9

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tv/TvPipPopManager;->isPopEnabled()Z

    move-result v4

    if-nez v4, :cond_9

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tv/TvPipPopManager;->isDualViewEnabled()Z

    move-result v4

    if-nez v4, :cond_9

    :goto_7
    iput-boolean v6, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->m_bIsPIPEnabled:Z

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Activity==>refreshStateParams: m_bIsPIPEnabled = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v5, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->m_bIsPIPEnabled:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    const-string v4, "Activity==>refreshStateParams done!"

    invoke-static {v4}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    return-void

    :cond_2
    move v4, v6

    goto/16 :goto_0

    :cond_3
    move v4, v5

    goto/16 :goto_1

    :cond_4
    move v4, v5

    goto/16 :goto_2

    :cond_5
    move v4, v6

    goto/16 :goto_3

    :cond_6
    move v4, v5

    goto/16 :goto_4

    :cond_7
    move v4, v5

    goto/16 :goto_5

    :cond_8
    :try_start_1
    const-string v4, "TvManger.getInstance() == null"

    invoke-static {v4}, Lcom/konka/debuginfo/logPrint;->Fatal(Ljava/lang/String;)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_6

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_6

    :cond_9
    move v6, v5

    goto :goto_7
.end method


# virtual methods
.method public findFocusFor3DMenu()V
    .locals 3

    const-string v1, "Activity==>findFocusFor3DMenu start!"

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->menu3DHolder:Lcom/konka/hotkey/video3dpip/MenuHolder3D;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->menu3DHolder:Lcom/konka/hotkey/video3dpip/MenuHolder3D;

    invoke-virtual {v1}, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->getLastFocusItem()Lcom/konka/hotkey/view/Com3DPIPItem;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->requestFocus()Z

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateFocus()V

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v1, "Activity==>findFocusFor3DMenu done! ItemName="

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    const-string v1, "Activity==>findFocusFor3DMenu fail, getLastFocusItem null!!!"

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const-string v1, "Activity==>findFocusFor3DMenu fail, menu3DHolder is null!!!"

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public findFocusForPIPMenu()V
    .locals 3

    const-string v1, "Activity==>findFocusForPIPMenu start!"

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->menuPIPHolder:Lcom/konka/hotkey/video3dpip/MenuHolderPIP;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->menuPIPHolder:Lcom/konka/hotkey/video3dpip/MenuHolderPIP;

    invoke-virtual {v1}, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->getLastFocusItem()Lcom/konka/hotkey/view/Com3DPIPItem;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->requestFocus()Z

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateFocus()V

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v1, "Activity==>findFocusForPIPMenu done! ItemName="

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    const-string v1, "Activity==>findFocusForPIPMenu fail, getLastFocusItem null!!!"

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const-string v1, "Activity==>findFocusForPIPMenu fail, menuPIPHolder is null!!!"

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public forbidden3DMenu()V
    .locals 1

    const-string v0, "Activity==>forbidden3DMenu start!"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->menu3DHolder:Lcom/konka/hotkey/video3dpip/MenuHolder3D;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->menu3DHolder:Lcom/konka/hotkey/video3dpip/MenuHolder3D;

    invoke-virtual {v0}, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->setMenuForbidden()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Activity==>forbidden3DMenu, done."

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    const-string v0, "Activity==>forbidden3DMenu fail, menu3DHolder is null or did nothing!"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public forbiddenPIPMenu()V
    .locals 1

    const-string v0, "Activity==>forbiddenPIPMenu start!"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->menuPIPHolder:Lcom/konka/hotkey/video3dpip/MenuHolderPIP;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->menuPIPHolder:Lcom/konka/hotkey/video3dpip/MenuHolderPIP;

    invoke-virtual {v0}, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->setMenuForbidden()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Activity==>forbiddenPIPMenu, done."

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    const-string v0, "Activity==>forbiddenPIPMenu fail, menuPIPHolder is null or did nothing!"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public is3DClar()Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->m_bIs3DClar:Z

    return v0
.end method

.method public is3DEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->m_bIs3DEnabled:Z

    return v0
.end method

.method public is3DVisible(Landroid/content/Context;)Z
    .locals 5
    .param p1    # Landroid/content/Context;

    const/4 v3, 0x1

    const-string v2, "activity"

    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v1, v2, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const-string v4, "mm"

    invoke-virtual {v2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->menu3DHolder:Lcom/konka/hotkey/video3dpip/MenuHolder3D;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->menu3DHolder:Lcom/konka/hotkey/video3dpip/MenuHolder3D;

    invoke-virtual {v2}, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->setMenuForbidden()Z

    :cond_0
    const/4 v2, 0x0

    :goto_0
    return v2

    :cond_1
    move v2, v3

    goto :goto_0
.end method

.method public isDoubleChannel()Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->m_bIsSupportSyncView:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->m_bIsDoubleChannel:Z

    :cond_0
    iget-boolean v0, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->m_bIsDoubleChannel:Z

    return v0
.end method

.method public isFramePacking()Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->m_bIsFramePacking:Z

    return v0
.end method

.method public isKtvOrStorage()Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->m_bIsKtvOrStorage:Z

    return v0
.end method

.method public isPIPEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->m_bIsPIPEnabled:Z

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;

    const/4 v4, 0x0

    const-string v3, "Activity==>onCreate start."

    invoke-static {v3}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v3, 0x7f030007    # com.konka.hotkey.R.layout.hk_video3d_menu

    invoke-virtual {p0, v3}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->setContentView(I)V

    invoke-static {p0}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v0

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getS3DManagerInstance()Lcom/konka/kkinterface/tv/S3DDesk;

    move-result-object v3

    iput-object v3, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->ms3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/CommonDesk;->getPipInfo()Lcom/konka/kkinterface/tv/CommonDesk$PipInfo;

    move-result-object v2

    iget v3, v2, Lcom/konka/kkinterface/tv/CommonDesk$PipInfo;->enablePip:I

    if-eqz v3, :cond_1

    const/4 v3, 0x1

    :goto_0
    iput-boolean v3, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->m_bIsDoubleChannel:Z

    invoke-virtual {p0}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {p0}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    const-string v5, "IS_SUPPORT_SYNC_VIEW"

    invoke-virtual {v3, v5, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->m_bIsSupportSyncView:Z

    :cond_0
    const-string v3, "Activity==>onCreate done."

    invoke-static {v3}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    return-void

    :cond_1
    move v3, v4

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    iget-boolean v0, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->m_bIsReceiverRegisted:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    :cond_0
    invoke-static {}, Lcom/konka/hotkey/LittleDownTimer;->destroy()V

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    const-string v0, "Activity==>onDestroy!!!"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x4

    if-ne v0, p1, :cond_0

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->myHandler:Landroid/os/Handler;

    const/16 v1, 0x40a

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 0
    .param p1    # Landroid/content/Intent;

    invoke-super {p0, p1}, Landroid/app/Activity;->onNewIntent(Landroid/content/Intent;)V

    invoke-virtual {p0, p1}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->setIntent(Landroid/content/Intent;)V

    return-void
.end method

.method protected onPause()V
    .locals 1

    const/4 v0, 0x0

    invoke-static {v0}, Lcom/konka/hotkey/LittleDownTimer;->setIsStopMenu(Z)V

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    const-string v0, "Activity==>onPause!!!"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    return-void
.end method

.method protected onResume()V
    .locals 5

    const-string v3, "Activit==>onResume, start."

    invoke-static {v3}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-boolean v3, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->m_bIsReceiverRegisted:Z

    if-nez v3, :cond_0

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v3, "com.konka.hotkey.ACTION_3DAUTO_RETURN"

    invoke-virtual {v0, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v3, "com.konka.hotkey.ACTION_3DEN_RETURN"

    invoke-virtual {v0, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v3, "com.konka.hotkey.ACTION_PIPSCSD_RETURN"

    invoke-virtual {v0, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v3, v0}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->m_bIsReceiverRegisted:Z

    :cond_0
    invoke-direct {p0, p0}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->isReadyShow3D(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-direct {p0}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->doCheckDoubleChannel()V

    new-instance v1, Lcom/konka/kkimplements/common/IniEditor;

    invoke-direct {v1}, Lcom/konka/kkimplements/common/IniEditor;-><init>()V

    const-string v3, "/customercfg/panel/panel.ini"

    invoke-virtual {v1, v3}, Lcom/konka/kkimplements/common/IniEditor;->loadFile(Ljava/lang/String;)Z

    const-string v3, "panel:b3DPanel"

    const-string v4, "0"

    invoke-virtual {v1, v3, v4}, Lcom/konka/kkimplements/common/IniEditor;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->menu3DHolder:Lcom/konka/hotkey/video3dpip/MenuHolder3D;

    invoke-virtual {v3}, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->isMenuForbidden()Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "0"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {p0, p0}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->is3DVisible(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {p0}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->findFocusFor3DMenu()V

    :goto_0
    invoke-static {}, Lcom/konka/hotkey/LittleDownTimer;->getInstance()Lcom/konka/hotkey/LittleDownTimer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/konka/hotkey/LittleDownTimer;->start()V

    iget-object v3, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->myHandler:Landroid/os/Handler;

    invoke-static {v3}, Lcom/konka/hotkey/LittleDownTimer;->setHandler(Landroid/os/Handler;)V

    invoke-static {}, Lcom/konka/hotkey/LittleDownTimer;->resumeMenuTime()V

    :goto_1
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    const-string v3, "Activit==>onResume, done."

    invoke-static {v3}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->findFocusForPIPMenu()V

    goto :goto_0

    :cond_2
    invoke-static {p0}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v3

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v3

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/CommonDesk;->getPipInfo()Lcom/konka/kkinterface/tv/CommonDesk$PipInfo;

    move-result-object v3

    iget v3, v3, Lcom/konka/kkinterface/tv/CommonDesk$PipInfo;->enablePip:I

    if-nez v3, :cond_3

    const v3, 0x7f080040    # com.konka.hotkey.R.string.warning_notsupport_3d

    invoke-virtual {p0, p0, v3}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->popToast(Landroid/content/Context;I)V

    :goto_2
    invoke-virtual {p0}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->finish()V

    goto :goto_1

    :cond_3
    const v3, 0x7f080042    # com.konka.hotkey.R.string.warning_notsupport_3d_pip

    invoke-virtual {p0, p0, v3}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->popToast(Landroid/content/Context;I)V

    goto :goto_2
.end method

.method protected onStart()V
    .locals 3

    invoke-static {p0}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getDataBaseManagerInstance()Lcom/konka/kkinterface/tv/DataBaseDesk;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/DataBaseDesk;->SyncUserSettingDB()V

    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    invoke-virtual {p0}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "IS_SUPPORT_SYNC_VIEW"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->m_bIsSupportSyncView:Z

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "XXXXX==========>>>onStart ["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v1, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->m_bIsDoubleChannel:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->m_bIsSupportSyncView:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    return-void
.end method

.method protected onStop()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    invoke-virtual {p0}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->finish()V

    const-string v0, "ActivityXXXXXXXXXX========>>>onStop!!!"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    return-void
.end method

.method public onUserInteraction()V
    .locals 0

    invoke-static {}, Lcom/konka/hotkey/LittleDownTimer;->resetMenuTime()V

    invoke-super {p0}, Landroid/app/Activity;->onUserInteraction()V

    return-void
.end method

.method public popToast(Landroid/content/Context;I)V
    .locals 5
    .param p1    # Landroid/content/Context;
    .param p2    # I

    const/4 v4, 0x0

    const/4 v3, 0x1

    invoke-static {p1, p2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    const/16 v3, 0x11

    invoke-virtual {v1, v3, v4, v4}, Landroid/widget/Toast;->setGravity(III)V

    const/16 v3, 0x3e8

    invoke-virtual {v1, v3}, Landroid/widget/Toast;->setDuration(I)V

    invoke-virtual {v1}, Landroid/widget/Toast;->getView()Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    new-instance v0, Landroid/widget/ImageView;

    invoke-direct {v0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v0, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    return-void
.end method

.method public resume3DMenu()V
    .locals 1

    const-string v0, "Activity==>resume3DMenu start!"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->menu3DHolder:Lcom/konka/hotkey/video3dpip/MenuHolder3D;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->menu3DHolder:Lcom/konka/hotkey/video3dpip/MenuHolder3D;

    invoke-virtual {v0}, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->setMenuResume()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Activity==>resume3DMenu, done."

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    const-string v0, "Activity==>resume3DMenu fail, menu3DHolder is null or did nothing!"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public resumePIPMenu()V
    .locals 1

    const-string v0, "Activity==>resumePIPMenu start!"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->menuPIPHolder:Lcom/konka/hotkey/video3dpip/MenuHolderPIP;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->menuPIPHolder:Lcom/konka/hotkey/video3dpip/MenuHolderPIP;

    invoke-virtual {v0}, Lcom/konka/hotkey/video3dpip/MenuHolderPIP;->setMenuResume()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Activity==>resumePIPMenu, done."

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    const-string v0, "Activity==>resumePIPMenu fail, menuPIPHolder is null or did nothing!"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public set3DEnabled(Z)V
    .locals 2
    .param p1    # Z

    iput-boolean p1, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->m_bIs3DEnabled:Z

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Activity==>set3DEnabled, value="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    return-void
.end method

.method public setIs3DClar(Z)V
    .locals 2
    .param p1    # Z

    iput-boolean p1, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->m_bIs3DClar:Z

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Activity==>setIs3DClar, value="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    return-void
.end method

.method public setIsFramePacking(Z)V
    .locals 2
    .param p1    # Z

    iput-boolean p1, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->m_bIsFramePacking:Z

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Activity==>setIsFramePacking, value="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    return-void
.end method

.method public setPIPEnabled(Z)V
    .locals 2
    .param p1    # Z

    iput-boolean p1, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity;->m_bIsPIPEnabled:Z

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Activity==>setPIPEnabled, value="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    return-void
.end method
