.class Lcom/konka/hotkey/video3dpip/MenuHolder3D$7;
.super Lcom/konka/hotkey/view/Com3DPIPItem;
.source "MenuHolder3D.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/hotkey/video3dpip/MenuHolder3D;->addItem3DAuto()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/hotkey/video3dpip/MenuHolder3D;


# direct methods
.method constructor <init>(Lcom/konka/hotkey/video3dpip/MenuHolder3D;Landroid/content/Context;IIILjava/lang/String;FI)V
    .locals 8
    .param p2    # Landroid/content/Context;
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # Ljava/lang/String;
    .param p7    # F
    .param p8    # I

    iput-object p1, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D$7;->this$0:Lcom/konka/hotkey/video3dpip/MenuHolder3D;

    move-object v0, p0

    move-object v1, p2

    move v2, p3

    move v3, p4

    move v4, p5

    move-object v5, p6

    move v6, p7

    move/from16 v7, p8

    invoke-direct/range {v0 .. v7}, Lcom/konka/hotkey/view/Com3DPIPItem;-><init>(Landroid/content/Context;IIILjava/lang/String;FI)V

    return-void
.end method


# virtual methods
.method public doReturn()V
    .locals 1

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D$7;->this$0:Lcom/konka/hotkey/video3dpip/MenuHolder3D;

    invoke-virtual {v0}, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->dismiss()V

    return-void
.end method

.method public doUpdate()Z
    .locals 1

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D$7;->this$0:Lcom/konka/hotkey/video3dpip/MenuHolder3D;

    invoke-virtual {v0}, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->updateItemState3DAuto()V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/MenuHolder3D$7;->this$0:Lcom/konka/hotkey/video3dpip/MenuHolder3D;

    invoke-virtual {v0}, Lcom/konka/hotkey/video3dpip/MenuHolder3D;->display3DAuto()V

    const/4 v0, 0x1

    return v0
.end method

.method public setItemStateInUse()V
    .locals 0

    invoke-virtual {p0}, Lcom/konka/hotkey/video3dpip/MenuHolder3D$7;->setItemStateInUseWithoutDisabled()V

    return-void
.end method
