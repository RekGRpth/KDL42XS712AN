.class public Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;
.super Landroid/app/Activity;
.source "PIPSubChannelSelDlgActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity$InputItem;
    }
.end annotation


# static fields
.field private static final ACTION_PIPSCSD_RETURN:Ljava/lang/String; = "com.konka.hotkey.ACTION_PIPSCSD_RETURN"


# instance fields
.field private HEIGHT:I

.field private WIDTH:I

.field private channelDesk:Lcom/konka/kkinterface/tv/ChannelDesk;

.field private commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

.field private inputSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

.field private mInputSourceModes:[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

.field private mInputSourceName:[Ljava/lang/String;

.field private mViewItemContainer:Landroid/widget/LinearLayout;

.field private mViewItems:[Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity$InputItem;

.field private myHandler:Landroid/os/Handler;

.field private serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;->mViewItemContainer:Landroid/widget/LinearLayout;

    new-instance v0, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity$1;

    invoke-direct {v0, p0}, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity$1;-><init>(Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;)V

    iput-object v0, p0, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;->myHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$0(Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;)I
    .locals 1

    iget v0, p0, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;->WIDTH:I

    return v0
.end method

.method static synthetic access$1(Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;)I
    .locals 1

    iget v0, p0, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;->HEIGHT:I

    return v0
.end method

.method private dataConfigurate()V
    .locals 13

    iget-object v9, p0, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v9}, Lcom/konka/kkinterface/tv/CommonDesk;->getInputSourceInfo()Lcom/konka/kkinterface/tv/CommonDesk$InputSourceInfo;

    move-result-object v9

    iget v2, v9, Lcom/konka/kkinterface/tv/CommonDesk$InputSourceInfo;->sourceCountDTV:I

    iget-object v9, p0, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v9}, Lcom/konka/kkinterface/tv/CommonDesk;->getInputSourceInfo()Lcom/konka/kkinterface/tv/CommonDesk$InputSourceInfo;

    move-result-object v9

    iget v0, v9, Lcom/konka/kkinterface/tv/CommonDesk$InputSourceInfo;->sourceCountATV:I

    iget-object v9, p0, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v9}, Lcom/konka/kkinterface/tv/CommonDesk;->getInputSourceInfo()Lcom/konka/kkinterface/tv/CommonDesk$InputSourceInfo;

    move-result-object v9

    iget v1, v9, Lcom/konka/kkinterface/tv/CommonDesk$InputSourceInfo;->sourceCountAV:I

    iget-object v9, p0, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v9}, Lcom/konka/kkinterface/tv/CommonDesk;->getInputSourceInfo()Lcom/konka/kkinterface/tv/CommonDesk$InputSourceInfo;

    move-result-object v9

    iget v6, v9, Lcom/konka/kkinterface/tv/CommonDesk$InputSourceInfo;->sourceCountYPbPr:I

    iget-object v9, p0, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v9}, Lcom/konka/kkinterface/tv/CommonDesk;->getInputSourceInfo()Lcom/konka/kkinterface/tv/CommonDesk$InputSourceInfo;

    move-result-object v9

    iget v5, v9, Lcom/konka/kkinterface/tv/CommonDesk$InputSourceInfo;->sourceCountVGA:I

    iget-object v9, p0, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v9}, Lcom/konka/kkinterface/tv/CommonDesk;->getInputSourceInfo()Lcom/konka/kkinterface/tv/CommonDesk$InputSourceInfo;

    move-result-object v9

    iget v3, v9, Lcom/konka/kkinterface/tv/CommonDesk$InputSourceInfo;->sourceCountHDMI:I

    add-int v9, v2, v0

    add-int/2addr v9, v1

    add-int/2addr v9, v6

    add-int/2addr v9, v5

    add-int v4, v9, v3

    new-array v9, v4, [Ljava/lang/String;

    iput-object v9, p0, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;->mInputSourceName:[Ljava/lang/String;

    new-array v9, v4, [Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    iput-object v9, p0, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;->mInputSourceModes:[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    const/4 v7, 0x0

    const/4 v8, 0x0

    :goto_0
    if-lt v8, v2, :cond_0

    const/4 v8, 0x0

    :goto_1
    if-lt v8, v0, :cond_2

    const/4 v8, 0x0

    :goto_2
    if-lt v8, v1, :cond_4

    const/4 v8, 0x0

    :goto_3
    if-lt v8, v6, :cond_6

    const/4 v8, 0x0

    :goto_4
    if-lt v8, v5, :cond_8

    const/4 v8, 0x0

    :goto_5
    if-lt v8, v3, :cond_a

    return-void

    :cond_0
    const/4 v9, 0x1

    if-ne v2, v9, :cond_1

    iget-object v9, p0, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;->mInputSourceName:[Ljava/lang/String;

    invoke-virtual {p0}, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f08004b    # com.konka.hotkey.R.string.str_input_menu_dtv

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v9, v7

    :goto_6
    iget-object v9, p0, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;->mInputSourceModes:[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    sget-object v10, Lcom/konka/hotkey/utils/ConfigurationData;->DTVInputSourceModes:[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    aget-object v10, v10, v8

    aput-object v10, v9, v7

    add-int/lit8 v8, v8, 0x1

    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    :cond_1
    iget-object v9, p0, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;->mInputSourceName:[Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f08004b    # com.konka.hotkey.R.string.str_input_menu_dtv

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    add-int/lit8 v11, v8, 0x1

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v9, v7

    goto :goto_6

    :cond_2
    const/4 v9, 0x1

    if-ne v0, v9, :cond_3

    iget-object v9, p0, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;->mInputSourceName:[Ljava/lang/String;

    invoke-virtual {p0}, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f08004c    # com.konka.hotkey.R.string.str_input_menu_atv

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v9, v7

    :goto_7
    iget-object v9, p0, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;->mInputSourceModes:[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    sget-object v10, Lcom/konka/hotkey/utils/ConfigurationData;->ATVInputSourceModes:[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    aget-object v10, v10, v8

    aput-object v10, v9, v7

    add-int/lit8 v8, v8, 0x1

    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    :cond_3
    iget-object v9, p0, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;->mInputSourceName:[Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f08004c    # com.konka.hotkey.R.string.str_input_menu_atv

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    add-int/lit8 v11, v8, 0x1

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v9, v7

    goto :goto_7

    :cond_4
    const/4 v9, 0x1

    if-ne v1, v9, :cond_5

    iget-object v9, p0, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;->mInputSourceName:[Ljava/lang/String;

    invoke-virtual {p0}, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f08004d    # com.konka.hotkey.R.string.str_input_menu_av

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v9, v7

    :goto_8
    iget-object v9, p0, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;->mInputSourceModes:[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    sget-object v10, Lcom/konka/hotkey/utils/ConfigurationData;->AVInputSourceModes:[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    aget-object v10, v10, v8

    aput-object v10, v9, v7

    add-int/lit8 v8, v8, 0x1

    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_2

    :cond_5
    iget-object v9, p0, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;->mInputSourceName:[Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f08004d    # com.konka.hotkey.R.string.str_input_menu_av

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    add-int/lit8 v11, v8, 0x1

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v9, v7

    goto :goto_8

    :cond_6
    const/4 v9, 0x1

    if-ne v6, v9, :cond_7

    iget-object v9, p0, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;->mInputSourceName:[Ljava/lang/String;

    invoke-virtual {p0}, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f08004f    # com.konka.hotkey.R.string.str_input_menu_ypbpr

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v9, v7

    :goto_9
    iget-object v9, p0, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;->mInputSourceModes:[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    sget-object v10, Lcom/konka/hotkey/utils/ConfigurationData;->YPbPrInputSourceModes:[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    aget-object v10, v10, v8

    aput-object v10, v9, v7

    add-int/lit8 v8, v8, 0x1

    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_3

    :cond_7
    iget-object v9, p0, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;->mInputSourceName:[Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f08004f    # com.konka.hotkey.R.string.str_input_menu_ypbpr

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    add-int/lit8 v11, v8, 0x1

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v9, v7

    goto :goto_9

    :cond_8
    const/4 v9, 0x1

    if-ne v5, v9, :cond_9

    iget-object v9, p0, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;->mInputSourceName:[Ljava/lang/String;

    invoke-virtual {p0}, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f080050    # com.konka.hotkey.R.string.str_input_menu_vga

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v9, v7

    :goto_a
    iget-object v9, p0, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;->mInputSourceModes:[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    sget-object v10, Lcom/konka/hotkey/utils/ConfigurationData;->VGAInputSourceModes:[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    aget-object v10, v10, v8

    aput-object v10, v9, v7

    add-int/lit8 v8, v8, 0x1

    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_4

    :cond_9
    iget-object v9, p0, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;->mInputSourceName:[Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f080050    # com.konka.hotkey.R.string.str_input_menu_vga

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    add-int/lit8 v11, v8, 0x1

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v9, v7

    goto :goto_a

    :cond_a
    const/4 v9, 0x1

    if-ne v3, v9, :cond_b

    iget-object v9, p0, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;->mInputSourceName:[Ljava/lang/String;

    invoke-virtual {p0}, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f08004e    # com.konka.hotkey.R.string.str_input_menu_hdmi

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v9, v7

    :goto_b
    iget-object v9, p0, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;->mInputSourceModes:[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    sget-object v10, Lcom/konka/hotkey/utils/ConfigurationData;->HDMIInputSourceModes:[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    aget-object v10, v10, v8

    aput-object v10, v9, v7

    add-int/lit8 v8, v8, 0x1

    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_5

    :cond_b
    iget-object v9, p0, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;->mInputSourceName:[Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f08004e    # com.konka.hotkey.R.string.str_input_menu_hdmi

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    add-int/lit8 v11, v8, 0x1

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v9, v7

    goto :goto_b
.end method

.method private findFocus(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)V
    .locals 3
    .param p1    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-nez p1, :cond_0

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;->mViewItems:[Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity$InputItem;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;->mViewItems:[Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity$InputItem;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v1}, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity$InputItem;->requestFocus()Z

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;->mViewItems:[Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity$InputItem;

    array-length v1, v1

    if-lt v0, v1, :cond_1

    :goto_1
    return-void

    :cond_1
    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;->mViewItems:[Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity$InputItem;

    aget-object v1, v1, v0

    iget-object v1, v1, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity$InputItem;->mInputMode:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-ne p1, v1, :cond_2

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;->mViewItems:[Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity$InputItem;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity$InputItem;->requestFocus()Z

    goto :goto_1

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public addItem()V
    .locals 13

    new-instance v8, Landroid/widget/RelativeLayout;

    invoke-virtual {p0}, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v8, v0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    new-instance v7, Landroid/widget/RelativeLayout;

    invoke-virtual {p0}, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v7, v0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    new-instance v10, Landroid/widget/LinearLayout$LayoutParams;

    iget v0, p0, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;->WIDTH:I

    const/4 v1, -0x2

    invoke-direct {v10, v0, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v8, v10}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v7, v10}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const v0, 0x7f020077    # com.konka.hotkey.R.drawable.input_item_head

    invoke-virtual {v8, v0}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    const v0, 0x7f020076    # com.konka.hotkey.R.drawable.input_item_foot

    invoke-virtual {v7, v0}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;->mViewItemContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/mstar/android/tv/TvPipPopManager;->getSubWindowSourceList(Z)[I

    move-result-object v11

    const/4 v6, 0x0

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;->mInputSourceName:[Ljava/lang/String;

    array-length v0, v0

    new-array v0, v0, [Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity$InputItem;

    iput-object v0, p0, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;->mViewItems:[Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity$InputItem;

    const/4 v5, 0x0

    :goto_0
    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;->mInputSourceName:[Ljava/lang/String;

    array-length v0, v0

    if-lt v5, v0, :cond_0

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;->mViewItemContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    return-void

    :cond_0
    iget-object v12, p0, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;->mViewItems:[Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity$InputItem;

    new-instance v0, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity$InputItem;

    invoke-virtual {p0}, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;->mInputSourceName:[Ljava/lang/String;

    aget-object v3, v1, v5

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;->mInputSourceModes:[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    aget-object v4, v1, v5

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity$InputItem;-><init>(Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;Landroid/content/Context;Ljava/lang/String;Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;I)V

    aput-object v0, v12, v5

    const/4 v9, 0x0

    :goto_1
    array-length v0, v11

    if-lt v9, v0, :cond_2

    :goto_2
    if-eqz v6, :cond_1

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;->mViewItemContainer:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;->mViewItems:[Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity$InputItem;

    aget-object v1, v1, v5

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    :cond_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;->mInputSourceModes:[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    aget-object v0, v0, v5

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v0

    aget v1, v11, v9

    if-ne v0, v1, :cond_3

    const/4 v6, 0x1

    goto :goto_2

    :cond_3
    const/4 v6, 0x0

    add-int/lit8 v9, v9, 0x1

    goto :goto_1
.end method

.method public finish()V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "FromSubMenu"

    const-string v2, "INPUT_SOURCE"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "com.konka.hotkey.ACTION_PIPSCSD_RETURN"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;->sendBroadcast(Landroid/content/Intent;)V

    invoke-super {p0}, Landroid/app/Activity;->finish()V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f03000a    # com.konka.hotkey.R.layout.input_menu

    invoke-virtual {p0, v0}, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;->setContentView(I)V

    const v0, 0x7f0a002f    # com.konka.hotkey.R.id.input_item_container

    invoke-virtual {p0, v0}, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;->mViewItemContainer:Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getChannelManagerInstance()Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;->channelDesk:Lcom/konka/kkinterface/tv/ChannelDesk;

    invoke-virtual {p0}, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f07000f    # com.konka.hotkey.R.dimen.TVSettingInputItem_Width

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;->WIDTH:I

    invoke-virtual {p0}, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070010    # com.konka.hotkey.R.dimen.TVSettingInputItem_Height

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;->HEIGHT:I

    invoke-direct {p0}, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;->dataConfigurate()V

    invoke-virtual {p0}, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;->addItem()V

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tv/TvPipPopManager;->getSubInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;->inputSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;->inputSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-direct {p0, v0}, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;->findFocus(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x4

    if-ne v0, p1, :cond_0

    const-string v0, "on key down"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;->myHandler:Landroid/os/Handler;

    const/16 v1, 0x40a

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method protected onResume()V
    .locals 1

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;->myHandler:Landroid/os/Handler;

    invoke-static {v0}, Lcom/konka/hotkey/LittleDownTimer;->setHandler(Landroid/os/Handler;)V

    invoke-static {}, Lcom/konka/hotkey/LittleDownTimer;->resumeMenuTime()V

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    return-void
.end method

.method protected onStart()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    const/high16 v0, 0x7f040000    # com.konka.hotkey.R.anim.anim_zoom_in

    const v1, 0x7f040001    # com.konka.hotkey.R.anim.anim_zoom_out

    invoke-virtual {p0, v0, v1}, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;->overridePendingTransition(II)V

    return-void
.end method

.method protected onStop()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    invoke-virtual {p0}, Lcom/konka/hotkey/video3dpip/PIPSubChannelSelDlgActivity;->finish()V

    const-string v0, "ActivityXXXXXXXXXX========>>>onStop!!!"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    return-void
.end method

.method public onUserInteraction()V
    .locals 0

    invoke-static {}, Lcom/konka/hotkey/LittleDownTimer;->resetMenuTime()V

    invoke-super {p0}, Landroid/app/Activity;->onUserInteraction()V

    return-void
.end method

.method public setSubChannel(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)V
    .locals 14
    .param p1    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-static {}, Lcom/mstar/android/tv/TvCommonManager;->getInstance()Lcom/mstar/android/tv/TvCommonManager;

    move-result-object v12

    invoke-virtual {v12}, Lcom/mstar/android/tv/TvCommonManager;->getCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v4

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "Test by zhoujun ----- setSubChannel------------inputSource === "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "Test by zhoujun ----- setSubChannel------------subSrc === "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v12

    invoke-virtual {v12}, Lcom/mstar/android/tv/TvPipPopManager;->isPipEnabled()Z

    move-result v12

    if-eqz v12, :cond_3

    const/4 v6, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v12

    if-eqz v12, :cond_2

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v12

    invoke-virtual {v12}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v12

    invoke-virtual {v12}, Lcom/mstar/android/tvapi/common/PictureManager;->getPanelWidthHeight()Lcom/mstar/android/tvapi/common/vo/PanelProperty;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    :goto_0
    const/16 v9, 0x780

    const/16 v3, 0x438

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v8, 0x0

    const/4 v2, 0x0

    if-eqz v6, :cond_0

    iget v9, v6, Lcom/mstar/android/tvapi/common/vo/PanelProperty;->width:I

    iget v3, v6, Lcom/mstar/android/tvapi/common/vo/PanelProperty;->height:I

    :cond_0
    div-int/lit8 v8, v9, 0x3

    div-int/lit8 v3, v3, 0x3

    move v2, v3

    sub-int v10, v9, v8

    const/4 v11, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;-><init>()V

    iput v10, v0, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->x:I

    iput v11, v0, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->y:I

    iput v8, v0, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->width:I

    iput v2, v0, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->height:I

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v12

    invoke-virtual {v12, v4, p1, v0}, Lcom/mstar/android/tv/TvPipPopManager;->enablePipTV(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;Lcom/mstar/android/tvapi/common/vo/VideoWindowType;)Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;

    :goto_1
    move-object v4, p1

    :cond_1
    return-void

    :cond_2
    :try_start_1
    const-string v12, "TvManger.getInstance() == null"

    invoke-static {v12}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :cond_3
    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v12

    invoke-virtual {v12}, Lcom/mstar/android/tv/TvPipPopManager;->isPopEnabled()Z

    move-result v12

    if-eqz v12, :cond_4

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v12

    invoke-virtual {v12, v4, p1}, Lcom/mstar/android/tv/TvPipPopManager;->enablePopTV(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;

    goto :goto_1

    :cond_4
    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v12

    invoke-virtual {v12}, Lcom/mstar/android/tv/TvPipPopManager;->isDualViewEnabled()Z

    move-result v12

    if-eqz v12, :cond_1

    const/4 v6, 0x0

    :try_start_2
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v12

    if-eqz v12, :cond_6

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v12

    invoke-virtual {v12}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v12

    invoke-virtual {v12}, Lcom/mstar/android/tvapi/common/PictureManager;->getPanelWidthHeight()Lcom/mstar/android/tvapi/common/vo/PanelProperty;
    :try_end_2
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object v6

    :goto_2
    const/16 v9, 0x780

    const/16 v3, 0x438

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v8, 0x0

    const/4 v2, 0x0

    if-eqz v6, :cond_5

    iget v9, v6, Lcom/mstar/android/tvapi/common/vo/PanelProperty;->width:I

    iget v3, v6, Lcom/mstar/android/tvapi/common/vo/PanelProperty;->height:I

    :cond_5
    move v8, v9

    move v2, v3

    new-instance v5, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;

    invoke-direct {v5}, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;-><init>()V

    new-instance v7, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;

    invoke-direct {v7}, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;-><init>()V

    iput v10, v7, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->x:I

    iput v10, v5, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->x:I

    iput v11, v7, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->y:I

    iput v11, v5, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->y:I

    iput v8, v7, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->width:I

    iput v8, v5, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->width:I

    iput v2, v7, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->height:I

    iput v2, v5, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->height:I

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v12

    invoke-virtual {v12, v4, p1, v5, v7}, Lcom/mstar/android/tv/TvPipPopManager;->enable3dDualView(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;Lcom/mstar/android/tvapi/common/vo/VideoWindowType;Lcom/mstar/android/tvapi/common/vo/VideoWindowType;)Z

    goto :goto_1

    :cond_6
    :try_start_3
    const-string v12, "TvManger.getInstance() == null"

    invoke-static {v12}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V
    :try_end_3
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_2

    :catch_1
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_2
.end method
