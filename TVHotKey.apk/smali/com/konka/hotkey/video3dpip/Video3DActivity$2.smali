.class Lcom/konka/hotkey/video3dpip/Video3DActivity$2;
.super Landroid/content/BroadcastReceiver;
.source "Video3DActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/hotkey/video3dpip/Video3DActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/hotkey/video3dpip/Video3DActivity;


# direct methods
.method constructor <init>(Lcom/konka/hotkey/video3dpip/Video3DActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity$2;->this$0:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.konka.hotkey.ACTION_3DAUTO_RETURN"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "com.konka.hotkey.ACTION_3DEN_RETURN"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_0
    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity$2;->this$0:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    invoke-virtual {v1}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->resume3DMenu()V

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity$2;->this$0:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    invoke-virtual {v1}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->findFocusFor3DMenu()V

    :cond_1
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Activity==>onReceive, action = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    return-void

    :cond_2
    const-string v1, "com.konka.hotkey.ACTION_PIPSCSD_RETURN"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity$2;->this$0:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    invoke-virtual {v1}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->resumePIPMenu()V

    iget-object v1, p0, Lcom/konka/hotkey/video3dpip/Video3DActivity$2;->this$0:Lcom/konka/hotkey/video3dpip/Video3DActivity;

    invoke-virtual {v1}, Lcom/konka/hotkey/video3dpip/Video3DActivity;->findFocusForPIPMenu()V

    goto :goto_0
.end method
