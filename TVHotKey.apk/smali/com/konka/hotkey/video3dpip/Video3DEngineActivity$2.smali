.class Lcom/konka/hotkey/video3dpip/Video3DEngineActivity$2;
.super Lcom/konka/hotkey/view/S3dEnItem;
.source "Video3DEngineActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;->addItemSequence()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;


# direct methods
.method constructor <init>(Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;Landroid/app/Activity;Landroid/content/Context;II[I)V
    .locals 6
    .param p2    # Landroid/app/Activity;
    .param p3    # Landroid/content/Context;
    .param p4    # I
    .param p5    # I
    .param p6    # [I

    iput-object p1, p0, Lcom/konka/hotkey/video3dpip/Video3DEngineActivity$2;->this$0:Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move v3, p4

    move v4, p5

    move-object v5, p6

    invoke-direct/range {v0 .. v5}, Lcom/konka/hotkey/view/S3dEnItem;-><init>(Landroid/app/Activity;Landroid/content/Context;II[I)V

    return-void
.end method


# virtual methods
.method public doUpdate()Z
    .locals 3

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/Video3DEngineActivity$2;->this$0:Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;

    # getter for: Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;->itemSequence:Lcom/konka/hotkey/view/S3dEnItem;
    invoke-static {v0}, Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;->access$0(Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;)Lcom/konka/hotkey/view/S3dEnItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/hotkey/view/S3dEnItem;->getValueIndex()I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/Video3DEngineActivity$2;->this$0:Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;

    # getter for: Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;->mS3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;
    invoke-static {v0}, Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;->access$1(Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;)Lcom/konka/kkinterface/tv/S3DDesk;

    move-result-object v0

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;->DB_ThreeD_Video_LRVIEWSWITCH_NOTEXCHANGE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;

    invoke-interface {v0, v1}, Lcom/konka/kkinterface/tv/S3DDesk;->setLRViewSwitch(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;)Z

    :cond_0
    :goto_0
    return v2

    :cond_1
    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/Video3DEngineActivity$2;->this$0:Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;

    # getter for: Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;->itemSequence:Lcom/konka/hotkey/view/S3dEnItem;
    invoke-static {v0}, Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;->access$0(Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;)Lcom/konka/hotkey/view/S3dEnItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/hotkey/view/S3dEnItem;->getValueIndex()I

    move-result v0

    if-ne v2, v0, :cond_0

    iget-object v0, p0, Lcom/konka/hotkey/video3dpip/Video3DEngineActivity$2;->this$0:Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;

    # getter for: Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;->mS3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;
    invoke-static {v0}, Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;->access$1(Lcom/konka/hotkey/video3dpip/Video3DEngineActivity;)Lcom/konka/kkinterface/tv/S3DDesk;

    move-result-object v0

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;->DB_ThreeD_Video_LRVIEWSWITCH_EXCHANGE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;

    invoke-interface {v0, v1}, Lcom/konka/kkinterface/tv/S3DDesk;->setLRViewSwitch(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;)Z

    goto :goto_0
.end method
