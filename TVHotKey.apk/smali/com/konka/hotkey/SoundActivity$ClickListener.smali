.class Lcom/konka/hotkey/SoundActivity$ClickListener;
.super Ljava/lang/Object;
.source "SoundActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/hotkey/SoundActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ClickListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/hotkey/SoundActivity;


# direct methods
.method constructor <init>(Lcom/konka/hotkey/SoundActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/hotkey/SoundActivity$ClickListener;->this$0:Lcom/konka/hotkey/SoundActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    move-object v1, p1

    check-cast v1, Lcom/konka/hotkey/SoundActivity$SoundItem;

    invoke-virtual {v1}, Lcom/konka/hotkey/SoundActivity$SoundItem;->isRunning()Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/konka/hotkey/SoundActivity$ClickListener;->this$0:Lcom/konka/hotkey/SoundActivity;

    invoke-virtual {v2}, Lcom/konka/hotkey/SoundActivity;->recoverItemNorBg()V

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/konka/hotkey/SoundActivity$SoundItem;->setRunning(Z)V

    invoke-virtual {v1}, Lcom/konka/hotkey/SoundActivity$SoundItem;->doUpdate()Z

    iget-object v2, p0, Lcom/konka/hotkey/SoundActivity$ClickListener;->this$0:Lcom/konka/hotkey/SoundActivity;

    # getter for: Lcom/konka/hotkey/SoundActivity;->audioSkin:Lcom/mstar/android/tv/TvAudioManager;
    invoke-static {v2}, Lcom/konka/hotkey/SoundActivity;->access$0(Lcom/konka/hotkey/SoundActivity;)Lcom/mstar/android/tv/TvAudioManager;

    move-result-object v2

    if-eqz v2, :cond_1

    const-string v2, "audio skin is not null"

    invoke-static {v2}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/konka/hotkey/SoundActivity$SoundItem;->doUpdate()Z

    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.konka.tv.hotkey.service.UNMUTE"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/hotkey/SoundActivity$ClickListener;->this$0:Lcom/konka/hotkey/SoundActivity;

    invoke-virtual {v2, v0}, Lcom/konka/hotkey/SoundActivity;->sendBroadcast(Landroid/content/Intent;)V

    :cond_1
    iget-object v2, p0, Lcom/konka/hotkey/SoundActivity$ClickListener;->this$0:Lcom/konka/hotkey/SoundActivity;

    invoke-virtual {v2}, Lcom/konka/hotkey/SoundActivity;->finish()V

    goto :goto_0
.end method
