.class public abstract Lcom/konka/hotkey/interfaces/PIPMenuBase;
.super Lcom/konka/hotkey/interfaces/MenuBaseParams;
.source "PIPMenuBase.java"

# interfaces
.implements Lcom/konka/hotkey/interfaces/MenuBase;


# static fields
.field protected static final HKDC_NORS:I = 0x5

.field protected static final HKDC_PIP:I = 0x2

.field protected static final HKDC_POP:I = 0x1

.field protected static final HKDC_SCFS:I = 0x4

.field protected static final HKDC_SCSEL:I = 0x3

.field protected static final HKDC_SYNCW:I = 0x0

.field protected static final ItemName_NormalScreen:Ljava/lang/String; = "ItemName_NormalScreen"

.field protected static final ItemName_PIP:Ljava/lang/String; = "ItemName_PIP"

.field protected static final ItemName_POP:Ljava/lang/String; = "ItemName_POP"

.field protected static final ItemName_SubChannelSelection:Ljava/lang/String; = "ItemName_SubChannelSelection"

.field protected static final ItemName_SubFullScreen:Ljava/lang/String; = "ItemName_SubFullScreen"

.field protected static final ItemName_SyncWatching:Ljava/lang/String; = "ItemName_SyncWatching"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/konka/hotkey/interfaces/MenuBaseParams;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract displayNormalScreen()V
.end method

.method public abstract displayPIP()Z
.end method

.method public abstract displayPOP()Z
.end method

.method public abstract displaySubChannelFullScreen()V
.end method

.method public abstract displaySyncWatching()Z
.end method

.method public abstract doUpdateMenuStateNotSupport(Lcom/konka/hotkey/view/Com3DPIPItem;)V
.end method

.method public abstract showSubChannelSelectionDialog()V
.end method

.method public abstract updateItemStateNormalScreen(Z)V
.end method

.method public abstract updateItemStatePIP(Z)V
.end method

.method public abstract updateItemStatePOP(Z)V
.end method

.method public abstract updateItemStateSubChannelFullScreen(Z)V
.end method

.method public abstract updateItemStateSubChannelSelection(Z)V
.end method

.method public abstract updateItemStateSyncWatching(Z)V
.end method
