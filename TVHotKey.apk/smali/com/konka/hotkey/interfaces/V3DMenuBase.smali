.class public abstract Lcom/konka/hotkey/interfaces/V3DMenuBase;
.super Lcom/konka/hotkey/interfaces/MenuBaseParams;
.source "V3DMenuBase.java"

# interfaces
.implements Lcom/konka/hotkey/interfaces/MenuBase;


# static fields
.field protected static final HK3D_2TO3:I = 0x4

.field protected static final HK3D_3TO2:I = 0x3

.field protected static final HK3D_AUTO:I = 0x6

.field protected static final HK3D_CLAR:I = 0x5

.field protected static final HK3D_EN:I = 0x7

.field protected static final HK3D_LR:I = 0x0

.field protected static final HK3D_OFF:I = 0x2

.field protected static final HK3D_TB:I = 0x1

.field protected static final ItemName_2To3:Ljava/lang/String; = "ItemName_2To3"

.field protected static final ItemName_3To2:Ljava/lang/String; = "ItemName_3To2"

.field protected static final ItemName_Auto:Ljava/lang/String; = "ItemName_Auto"

.field protected static final ItemName_Clar:Ljava/lang/String; = "ItemName_Clar"

.field protected static final ItemName_En:Ljava/lang/String; = "ItemName_En"

.field protected static final ItemName_LR:Ljava/lang/String; = "ItemName_LR"

.field protected static final ItemName_Off:Ljava/lang/String; = "ItemName_Off"

.field protected static final ItemName_TB:Ljava/lang/String; = "ItemName_TB"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/konka/hotkey/interfaces/MenuBaseParams;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract display3D2To3()V
.end method

.method public abstract display3D3To2()V
.end method

.method public abstract display3DAuto()V
.end method

.method public abstract display3DClar()V
.end method

.method public abstract display3DEn()V
.end method

.method public abstract display3DLR()V
.end method

.method public abstract display3DOff()V
.end method

.method public abstract display3DTB()V
.end method

.method public abstract updateItemState3D2To3(Z)V
.end method

.method public abstract updateItemState3D3To2(Z)V
.end method

.method public abstract updateItemState3DAuto()V
.end method

.method public abstract updateItemState3DClar(Z)V
.end method

.method public abstract updateItemState3DEn()V
.end method

.method public abstract updateItemState3DLR(Z)V
.end method

.method public abstract updateItemState3DOff(Z)V
.end method

.method public abstract updateItemState3DTB(Z)V
.end method
