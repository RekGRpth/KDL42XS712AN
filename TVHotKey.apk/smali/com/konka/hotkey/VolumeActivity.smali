.class public Lcom/konka/hotkey/VolumeActivity;
.super Landroid/app/Activity;
.source "VolumeActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/hotkey/VolumeActivity$SeekBarChangeListener;
    }
.end annotation


# static fields
.field private static final ACTION_VOICE_CONTROL_VOLUME:Ljava/lang/String; = "com.konka.hotkey.VOICE_CONTROL_VOLUME"

.field public static final CMD_ERROR:I = -0x1

.field public static final CMD_VOLUME_DECREASE:I = 0x49

.field public static final CMD_VOLUME_INCREASE:I = 0x48

.field public static final CMD_VOLUME_SET_TO_MUTE:I = 0x46

.field public static final CMD_VOLUME_SET_TO_VALUE:I = 0x47

.field private static instance:Lcom/konka/hotkey/VolumeActivity;

.field public static mbIsRunning:Z


# instance fields
.field private audioSkin:Lcom/mstar/android/tv/TvAudioManager;

.field private broadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mViewIvHundred:Landroid/widget/ImageView;

.field private mViewSbSoundBar:Landroid/widget/SeekBar;

.field private mViewTvSoundVal:Landroid/widget/TextView;

.field private myHandler:Landroid/os/Handler;

.field private receiveKeyCode:I

.field private volume:I

.field private volumeSetter:Ljava/lang/Thread;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/konka/hotkey/VolumeActivity;->instance:Lcom/konka/hotkey/VolumeActivity;

    const/4 v0, 0x0

    sput-boolean v0, Lcom/konka/hotkey/VolumeActivity;->mbIsRunning:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    new-instance v0, Lcom/konka/hotkey/VolumeActivity$1;

    invoke-direct {v0, p0}, Lcom/konka/hotkey/VolumeActivity$1;-><init>(Lcom/konka/hotkey/VolumeActivity;)V

    iput-object v0, p0, Lcom/konka/hotkey/VolumeActivity;->myHandler:Landroid/os/Handler;

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/konka/hotkey/VolumeActivity$2;

    invoke-direct {v1, p0}, Lcom/konka/hotkey/VolumeActivity$2;-><init>(Lcom/konka/hotkey/VolumeActivity;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/konka/hotkey/VolumeActivity;->volumeSetter:Ljava/lang/Thread;

    new-instance v0, Lcom/konka/hotkey/VolumeActivity$3;

    invoke-direct {v0, p0}, Lcom/konka/hotkey/VolumeActivity$3;-><init>(Lcom/konka/hotkey/VolumeActivity;)V

    iput-object v0, p0, Lcom/konka/hotkey/VolumeActivity;->broadcastReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$0(Lcom/konka/hotkey/VolumeActivity;ILandroid/content/Intent;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/konka/hotkey/VolumeActivity;->executeCmd(ILandroid/content/Intent;)V

    return-void
.end method

.method static synthetic access$1(Lcom/konka/hotkey/VolumeActivity;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/konka/hotkey/VolumeActivity;->mViewTvSoundVal:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$2(Lcom/konka/hotkey/VolumeActivity;I)V
    .locals 0

    iput p1, p0, Lcom/konka/hotkey/VolumeActivity;->volume:I

    return-void
.end method

.method static synthetic access$3(Lcom/konka/hotkey/VolumeActivity;)I
    .locals 1

    iget v0, p0, Lcom/konka/hotkey/VolumeActivity;->volume:I

    return v0
.end method

.method static synthetic access$4(Lcom/konka/hotkey/VolumeActivity;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/hotkey/VolumeActivity;->showVolume(I)V

    return-void
.end method

.method private executeCmd(ILandroid/content/Intent;)V
    .locals 2
    .param p1    # I
    .param p2    # Landroid/content/Intent;

    sparse-switch p1, :sswitch_data_0

    :goto_0
    :sswitch_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "volume: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/konka/hotkey/VolumeActivity;->volume:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    return-void

    :sswitch_1
    const-string v0, "volume"

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/konka/hotkey/VolumeActivity;->setVolume(I)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p0}, Lcom/konka/hotkey/VolumeActivity;->plusVolume()V

    invoke-virtual {p0}, Lcom/konka/hotkey/VolumeActivity;->plusVolume()V

    invoke-virtual {p0}, Lcom/konka/hotkey/VolumeActivity;->plusVolume()V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p0}, Lcom/konka/hotkey/VolumeActivity;->decVolume()V

    invoke-virtual {p0}, Lcom/konka/hotkey/VolumeActivity;->decVolume()V

    invoke-virtual {p0}, Lcom/konka/hotkey/VolumeActivity;->decVolume()V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x1 -> :sswitch_0
        0x46 -> :sswitch_0
        0x47 -> :sswitch_1
        0x48 -> :sswitch_2
        0x49 -> :sswitch_3
    .end sparse-switch
.end method

.method public static getInstance()Lcom/konka/hotkey/VolumeActivity;
    .locals 1

    sget-object v0, Lcom/konka/hotkey/VolumeActivity;->instance:Lcom/konka/hotkey/VolumeActivity;

    return-object v0
.end method

.method private setVolume(I)V
    .locals 1
    .param p1    # I

    iput p1, p0, Lcom/konka/hotkey/VolumeActivity;->volume:I

    iget v0, p0, Lcom/konka/hotkey/VolumeActivity;->volume:I

    invoke-direct {p0, v0}, Lcom/konka/hotkey/VolumeActivity;->showVolume(I)V

    return-void
.end method

.method private showVolume(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/konka/hotkey/VolumeActivity;->mViewSbSoundBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, p1}, Landroid/widget/SeekBar;->setProgress(I)V

    return-void
.end method


# virtual methods
.method public decVolume()V
    .locals 3

    iget v0, p0, Lcom/konka/hotkey/VolumeActivity;->volume:I

    add-int/lit8 v0, v0, -0x1

    if-gez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput v0, p0, Lcom/konka/hotkey/VolumeActivity;->volume:I

    iget v1, p0, Lcom/konka/hotkey/VolumeActivity;->volume:I

    invoke-direct {p0, v1}, Lcom/konka/hotkey/VolumeActivity;->showVolume(I)V

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "decVolume success============================volume="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/konka/hotkey/VolumeActivity;->volume:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v3, 0x7f030008    # com.konka.hotkey.R.layout.hk_volume_menu

    invoke-virtual {p0, v3}, Lcom/konka/hotkey/VolumeActivity;->setContentView(I)V

    invoke-static {}, Lcom/mstar/android/tv/TvAudioManager;->getInstance()Lcom/mstar/android/tv/TvAudioManager;

    move-result-object v3

    iput-object v3, p0, Lcom/konka/hotkey/VolumeActivity;->audioSkin:Lcom/mstar/android/tv/TvAudioManager;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "OnCreat###############get volume====="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p0, Lcom/konka/hotkey/VolumeActivity;->volume:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/konka/hotkey/VolumeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "KeyCode"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    iput v3, p0, Lcom/konka/hotkey/VolumeActivity;->receiveKeyCode:I

    sput-object p0, Lcom/konka/hotkey/VolumeActivity;->instance:Lcom/konka/hotkey/VolumeActivity;

    const v3, 0x7f0a002d    # com.konka.hotkey.R.id.hk_sound_menu_tv_volum

    invoke-virtual {p0, v3}, Lcom/konka/hotkey/VolumeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/konka/hotkey/VolumeActivity;->mViewTvSoundVal:Landroid/widget/TextView;

    const v3, 0x7f0a002b    # com.konka.hotkey.R.id.hk_sound_menu_seekbar

    invoke-virtual {p0, v3}, Lcom/konka/hotkey/VolumeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/SeekBar;

    iput-object v3, p0, Lcom/konka/hotkey/VolumeActivity;->mViewSbSoundBar:Landroid/widget/SeekBar;

    const v3, 0x7f0a002c    # com.konka.hotkey.R.id.hk_sound_menu_volume100

    invoke-virtual {p0, v3}, Lcom/konka/hotkey/VolumeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/konka/hotkey/VolumeActivity;->mViewIvHundred:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/konka/hotkey/VolumeActivity;->mViewIvHundred:Landroid/widget/ImageView;

    new-instance v4, Lcom/konka/hotkey/VolumeActivity$4;

    invoke-direct {v4, p0}, Lcom/konka/hotkey/VolumeActivity$4;-><init>(Lcom/konka/hotkey/VolumeActivity;)V

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v2, Lcom/konka/hotkey/VolumeActivity$SeekBarChangeListener;

    invoke-direct {v2, p0}, Lcom/konka/hotkey/VolumeActivity$SeekBarChangeListener;-><init>(Lcom/konka/hotkey/VolumeActivity;)V

    iget-object v3, p0, Lcom/konka/hotkey/VolumeActivity;->mViewSbSoundBar:Landroid/widget/SeekBar;

    invoke-virtual {v3, v2}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    new-instance v0, Landroid/content/IntentFilter;

    const-string v3, "com.konka.hotkey.VOICE_CONTROL_VOLUME"

    invoke-direct {v0, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/hotkey/VolumeActivity;->broadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v3, v0}, Lcom/konka/hotkey/VolumeActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    iget v3, p0, Lcom/konka/hotkey/VolumeActivity;->receiveKeyCode:I

    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, Lcom/konka/hotkey/VolumeActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    invoke-virtual {p0}, Lcom/konka/hotkey/VolumeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "CommandId"

    const/4 v5, -0x1

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    invoke-virtual {p0}, Lcom/konka/hotkey/VolumeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-direct {p0, v1, v3}, Lcom/konka/hotkey/VolumeActivity;->executeCmd(ILandroid/content/Intent;)V

    iget v3, p0, Lcom/konka/hotkey/VolumeActivity;->volume:I

    invoke-direct {p0, v3}, Lcom/konka/hotkey/VolumeActivity;->showVolume(I)V

    invoke-static {}, Lcom/konka/hotkey/LittleDownTimer;->getInstance()Lcom/konka/hotkey/LittleDownTimer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/konka/hotkey/LittleDownTimer;->start()V

    iget-object v3, p0, Lcom/konka/hotkey/VolumeActivity;->myHandler:Landroid/os/Handler;

    invoke-static {v3}, Lcom/konka/hotkey/LittleDownTimer;->setHandler(Landroid/os/Handler;)V

    return-void
.end method

.method protected onDestroy()V
    .locals 3

    const/4 v2, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "onDestroy###############volume====="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/konka/hotkey/VolumeActivity;->volume:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    sput-object v2, Lcom/konka/hotkey/VolumeActivity;->instance:Lcom/konka/hotkey/VolumeActivity;

    iput-object v2, p0, Lcom/konka/hotkey/VolumeActivity;->volumeSetter:Ljava/lang/Thread;

    invoke-static {}, Lcom/konka/hotkey/LittleDownTimer;->destroy()V

    iget-object v0, p0, Lcom/konka/hotkey/VolumeActivity;->broadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/konka/hotkey/VolumeActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "OnKeyDown============================"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    const/16 v0, 0x19

    if-ne v0, p1, :cond_1

    invoke-virtual {p0}, Lcom/konka/hotkey/VolumeActivity;->decVolume()V

    :cond_0
    :goto_0
    const/4 v0, 0x1

    return v0

    :cond_1
    const/16 v0, 0x18

    if-ne v0, p1, :cond_2

    invoke-virtual {p0}, Lcom/konka/hotkey/VolumeActivity;->plusVolume()V

    goto :goto_0

    :cond_2
    const/4 v0, 0x4

    if-ne v0, p1, :cond_0

    invoke-virtual {p0}, Lcom/konka/hotkey/VolumeActivity;->finish()V

    goto :goto_0
.end method

.method protected onResume()V
    .locals 2

    invoke-static {}, Lcom/konka/hotkey/LittleDownTimer;->resumeMenuTime()V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "onResume###############volume====="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/konka/hotkey/VolumeActivity;->volume:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    return-void
.end method

.method protected onStart()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    const/4 v0, 0x1

    sput-boolean v0, Lcom/konka/hotkey/VolumeActivity;->mbIsRunning:Z

    iget-object v0, p0, Lcom/konka/hotkey/VolumeActivity;->volumeSetter:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method protected onStop()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    const/4 v0, 0x0

    sput-boolean v0, Lcom/konka/hotkey/VolumeActivity;->mbIsRunning:Z

    return-void
.end method

.method public onUserInteraction()V
    .locals 0

    invoke-static {}, Lcom/konka/hotkey/LittleDownTimer;->resetMenuTime()V

    invoke-super {p0}, Landroid/app/Activity;->onUserInteraction()V

    return-void
.end method

.method public plusVolume()V
    .locals 3

    iget v0, p0, Lcom/konka/hotkey/VolumeActivity;->volume:I

    add-int/lit8 v0, v0, 0x1

    const/16 v1, 0x64

    if-le v0, v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iput v0, p0, Lcom/konka/hotkey/VolumeActivity;->volume:I

    iget v1, p0, Lcom/konka/hotkey/VolumeActivity;->volume:I

    invoke-direct {p0, v1}, Lcom/konka/hotkey/VolumeActivity;->showVolume(I)V

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "plusVolume success============================volume="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/konka/hotkey/VolumeActivity;->volume:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    goto :goto_0
.end method
