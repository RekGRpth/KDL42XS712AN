.class Lcom/konka/hotkey/ZoomActivity$FocusChangeListener;
.super Ljava/lang/Object;
.source "ZoomActivity.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/hotkey/ZoomActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "FocusChangeListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/hotkey/ZoomActivity;


# direct methods
.method constructor <init>(Lcom/konka/hotkey/ZoomActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/hotkey/ZoomActivity$FocusChangeListener;->this$0:Lcom/konka/hotkey/ZoomActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 3
    .param p1    # Landroid/view/View;
    .param p2    # Z

    move-object v0, p1

    check-cast v0, Lcom/konka/hotkey/ZoomActivity$ZoomItem;

    if-eqz p2, :cond_0

    iget-object v1, p0, Lcom/konka/hotkey/ZoomActivity$FocusChangeListener;->this$0:Lcom/konka/hotkey/ZoomActivity;

    # getter for: Lcom/konka/hotkey/ZoomActivity;->SIZE_SEL:F
    invoke-static {v1}, Lcom/konka/hotkey/ZoomActivity;->access$3(Lcom/konka/hotkey/ZoomActivity;)F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/konka/hotkey/ZoomActivity$ZoomItem;->getFocus(F)V

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "get: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, v0, Lcom/konka/hotkey/ZoomActivity$ZoomItem;->meMode:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/konka/hotkey/ZoomActivity$FocusChangeListener;->this$0:Lcom/konka/hotkey/ZoomActivity;

    # getter for: Lcom/konka/hotkey/ZoomActivity;->SIZE_NOR:F
    invoke-static {v1}, Lcom/konka/hotkey/ZoomActivity;->access$4(Lcom/konka/hotkey/ZoomActivity;)F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/konka/hotkey/ZoomActivity$ZoomItem;->lostFocus(F)V

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Lost: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, v0, Lcom/konka/hotkey/ZoomActivity$ZoomItem;->meMode:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    goto :goto_0
.end method
