.class public Lcom/konka/hotkey/SoundActivity;
.super Landroid/app/Activity;
.source "SoundActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/hotkey/SoundActivity$ClickListener;,
        Lcom/konka/hotkey/SoundActivity$FocusChangeListener;,
        Lcom/konka/hotkey/SoundActivity$KeyListener;,
        Lcom/konka/hotkey/SoundActivity$SoundItem;
    }
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$mstar$android$tvapi$common$vo$EnumSoundMode:[I


# instance fields
.field private CURRENT_LANGUAGE:Ljava/lang/String;

.field private IMG_HEIGHT:I

.field private IMG_WIDTH:I

.field private final ITEM_MOVIE:I

.field private final ITEM_MUSIC:I

.field private final ITEM_NORMAL:I

.field private final ITEM_SPORT:I

.field private final ITEM_USER:I

.field private final LANGUAGE_EN:Ljava/lang/String;

.field private final LANGUAGE_ZH:Ljava/lang/String;

.field private SIZE_NOR:F

.field private SIZE_SEL:F

.field private audioSkin:Lcom/mstar/android/tv/TvAudioManager;

.field private mViewLlContainer:Landroid/widget/LinearLayout;

.field private mViewSiMovie:Lcom/konka/hotkey/SoundActivity$SoundItem;

.field private mViewSiMusic:Lcom/konka/hotkey/SoundActivity$SoundItem;

.field private mViewSiNormal:Lcom/konka/hotkey/SoundActivity$SoundItem;

.field private mViewSiSport:Lcom/konka/hotkey/SoundActivity$SoundItem;

.field private mViewSiUser:Lcom/konka/hotkey/SoundActivity$SoundItem;

.field private meSoundMode:Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;

.field private myHandler:Landroid/os/Handler;

.field private final strId:[I


# direct methods
.method static synthetic $SWITCH_TABLE$com$mstar$android$tvapi$common$vo$EnumSoundMode()[I
    .locals 3

    sget-object v0, Lcom/konka/hotkey/SoundActivity;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$EnumSoundMode:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;->values()[Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;->E_MOVIE:Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_7

    :goto_1
    :try_start_1
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;->E_MUSIC:Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_6

    :goto_2
    :try_start_2
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;->E_NUM:Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_5

    :goto_3
    :try_start_3
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;->E_ONSITE1:Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_4

    :goto_4
    :try_start_4
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;->E_ONSITE2:Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_3

    :goto_5
    :try_start_5
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;->E_SPORTS:Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_2

    :goto_6
    :try_start_6
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;->E_STANDARD:Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_1

    :goto_7
    :try_start_7
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;->E_USER:Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_0

    :goto_8
    sput-object v0, Lcom/konka/hotkey/SoundActivity;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$EnumSoundMode:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_8

    :catch_1
    move-exception v1

    goto :goto_7

    :catch_2
    move-exception v1

    goto :goto_6

    :catch_3
    move-exception v1

    goto :goto_5

    :catch_4
    move-exception v1

    goto :goto_4

    :catch_5
    move-exception v1

    goto :goto_3

    :catch_6
    move-exception v1

    goto :goto_2

    :catch_7
    move-exception v1

    goto :goto_1
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;->E_STANDARD:Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;

    iput-object v0, p0, Lcom/konka/hotkey/SoundActivity;->meSoundMode:Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;

    const/4 v0, 0x0

    iput v0, p0, Lcom/konka/hotkey/SoundActivity;->ITEM_NORMAL:I

    const/4 v0, 0x1

    iput v0, p0, Lcom/konka/hotkey/SoundActivity;->ITEM_MUSIC:I

    const/4 v0, 0x2

    iput v0, p0, Lcom/konka/hotkey/SoundActivity;->ITEM_MOVIE:I

    const/4 v0, 0x3

    iput v0, p0, Lcom/konka/hotkey/SoundActivity;->ITEM_SPORT:I

    const/4 v0, 0x4

    iput v0, p0, Lcom/konka/hotkey/SoundActivity;->ITEM_USER:I

    const/16 v0, 0x85

    iput v0, p0, Lcom/konka/hotkey/SoundActivity;->IMG_WIDTH:I

    const/16 v0, 0x86

    iput v0, p0, Lcom/konka/hotkey/SoundActivity;->IMG_HEIGHT:I

    const-string v0, "en"

    iput-object v0, p0, Lcom/konka/hotkey/SoundActivity;->LANGUAGE_EN:Ljava/lang/String;

    const-string v0, "zh"

    iput-object v0, p0, Lcom/konka/hotkey/SoundActivity;->LANGUAGE_ZH:Ljava/lang/String;

    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/konka/hotkey/SoundActivity;->strId:[I

    new-instance v0, Lcom/konka/hotkey/SoundActivity$1;

    invoke-direct {v0, p0}, Lcom/konka/hotkey/SoundActivity$1;-><init>(Lcom/konka/hotkey/SoundActivity;)V

    iput-object v0, p0, Lcom/konka/hotkey/SoundActivity;->myHandler:Landroid/os/Handler;

    return-void

    :array_0
    .array-data 4
        0x7f08000a    # com.konka.hotkey.R.string.str_hk_sound_menu_normal
        0x7f08000b    # com.konka.hotkey.R.string.str_hk_sound_menu_music
        0x7f08000c    # com.konka.hotkey.R.string.str_hk_sound_menu_movie
        0x7f08000d    # com.konka.hotkey.R.string.str_hk_sound_menu_sport
        0x7f08000e    # com.konka.hotkey.R.string.str_hk_sound_menu_user
    .end array-data
.end method

.method static synthetic access$0(Lcom/konka/hotkey/SoundActivity;)Lcom/mstar/android/tv/TvAudioManager;
    .locals 1

    iget-object v0, p0, Lcom/konka/hotkey/SoundActivity;->audioSkin:Lcom/mstar/android/tv/TvAudioManager;

    return-object v0
.end method

.method static synthetic access$1(Lcom/konka/hotkey/SoundActivity;)F
    .locals 1

    iget v0, p0, Lcom/konka/hotkey/SoundActivity;->SIZE_SEL:F

    return v0
.end method

.method static synthetic access$2(Lcom/konka/hotkey/SoundActivity;)F
    .locals 1

    iget v0, p0, Lcom/konka/hotkey/SoundActivity;->SIZE_NOR:F

    return v0
.end method

.method private findFocus(Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;)V
    .locals 3
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;

    const/4 v2, 0x1

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/konka/hotkey/SoundActivity;->mViewSiNormal:Lcom/konka/hotkey/SoundActivity$SoundItem;

    if-eqz v0, :cond_1

    const-string v0, "in find focus mode is null"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/hotkey/SoundActivity;->mViewSiNormal:Lcom/konka/hotkey/SoundActivity$SoundItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/SoundActivity$SoundItem;->requestFocus()Z

    :cond_0
    :goto_0
    invoke-static {}, Lcom/konka/hotkey/SoundActivity;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$EnumSoundMode()[I

    move-result-object v0

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    iget-object v0, p0, Lcom/konka/hotkey/SoundActivity;->mViewSiNormal:Lcom/konka/hotkey/SoundActivity$SoundItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/SoundActivity$SoundItem;->requestFocus()Z

    iget-object v0, p0, Lcom/konka/hotkey/SoundActivity;->mViewSiNormal:Lcom/konka/hotkey/SoundActivity$SoundItem;

    invoke-virtual {v0, v2}, Lcom/konka/hotkey/SoundActivity$SoundItem;->setRunning(Z)V

    :goto_1
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/konka/hotkey/SoundActivity;->finish()V

    goto :goto_0

    :pswitch_0
    iget-object v0, p0, Lcom/konka/hotkey/SoundActivity;->mViewSiMovie:Lcom/konka/hotkey/SoundActivity$SoundItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/SoundActivity$SoundItem;->requestFocus()Z

    iget-object v0, p0, Lcom/konka/hotkey/SoundActivity;->mViewSiMovie:Lcom/konka/hotkey/SoundActivity$SoundItem;

    invoke-virtual {v0, v2}, Lcom/konka/hotkey/SoundActivity$SoundItem;->setRunning(Z)V

    goto :goto_1

    :pswitch_1
    iget-object v0, p0, Lcom/konka/hotkey/SoundActivity;->mViewSiMusic:Lcom/konka/hotkey/SoundActivity$SoundItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/SoundActivity$SoundItem;->requestFocus()Z

    iget-object v0, p0, Lcom/konka/hotkey/SoundActivity;->mViewSiMusic:Lcom/konka/hotkey/SoundActivity$SoundItem;

    invoke-virtual {v0, v2}, Lcom/konka/hotkey/SoundActivity$SoundItem;->setRunning(Z)V

    goto :goto_1

    :pswitch_2
    iget-object v0, p0, Lcom/konka/hotkey/SoundActivity;->mViewSiSport:Lcom/konka/hotkey/SoundActivity$SoundItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/SoundActivity$SoundItem;->requestFocus()Z

    iget-object v0, p0, Lcom/konka/hotkey/SoundActivity;->mViewSiSport:Lcom/konka/hotkey/SoundActivity$SoundItem;

    invoke-virtual {v0, v2}, Lcom/konka/hotkey/SoundActivity$SoundItem;->setRunning(Z)V

    goto :goto_1

    :pswitch_3
    iget-object v0, p0, Lcom/konka/hotkey/SoundActivity;->mViewSiUser:Lcom/konka/hotkey/SoundActivity$SoundItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/SoundActivity$SoundItem;->requestFocus()Z

    iget-object v0, p0, Lcom/konka/hotkey/SoundActivity;->mViewSiUser:Lcom/konka/hotkey/SoundActivity$SoundItem;

    invoke-virtual {v0, v2}, Lcom/konka/hotkey/SoundActivity$SoundItem;->setRunning(Z)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private listenerInit()V
    .locals 4

    new-instance v1, Lcom/konka/hotkey/SoundActivity$FocusChangeListener;

    invoke-direct {v1, p0}, Lcom/konka/hotkey/SoundActivity$FocusChangeListener;-><init>(Lcom/konka/hotkey/SoundActivity;)V

    new-instance v0, Lcom/konka/hotkey/SoundActivity$ClickListener;

    invoke-direct {v0, p0}, Lcom/konka/hotkey/SoundActivity$ClickListener;-><init>(Lcom/konka/hotkey/SoundActivity;)V

    new-instance v2, Lcom/konka/hotkey/SoundActivity$KeyListener;

    invoke-direct {v2, p0}, Lcom/konka/hotkey/SoundActivity$KeyListener;-><init>(Lcom/konka/hotkey/SoundActivity;)V

    iget-object v3, p0, Lcom/konka/hotkey/SoundActivity;->mViewSiNormal:Lcom/konka/hotkey/SoundActivity$SoundItem;

    invoke-virtual {v3, v0}, Lcom/konka/hotkey/SoundActivity$SoundItem;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lcom/konka/hotkey/SoundActivity;->mViewSiNormal:Lcom/konka/hotkey/SoundActivity$SoundItem;

    invoke-virtual {v3, v1}, Lcom/konka/hotkey/SoundActivity$SoundItem;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v3, p0, Lcom/konka/hotkey/SoundActivity;->mViewSiNormal:Lcom/konka/hotkey/SoundActivity$SoundItem;

    invoke-virtual {v3, v2}, Lcom/konka/hotkey/SoundActivity$SoundItem;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v3, p0, Lcom/konka/hotkey/SoundActivity;->mViewSiMusic:Lcom/konka/hotkey/SoundActivity$SoundItem;

    invoke-virtual {v3, v0}, Lcom/konka/hotkey/SoundActivity$SoundItem;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lcom/konka/hotkey/SoundActivity;->mViewSiMusic:Lcom/konka/hotkey/SoundActivity$SoundItem;

    invoke-virtual {v3, v1}, Lcom/konka/hotkey/SoundActivity$SoundItem;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v3, p0, Lcom/konka/hotkey/SoundActivity;->mViewSiMusic:Lcom/konka/hotkey/SoundActivity$SoundItem;

    invoke-virtual {v3, v2}, Lcom/konka/hotkey/SoundActivity$SoundItem;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v3, p0, Lcom/konka/hotkey/SoundActivity;->mViewSiMovie:Lcom/konka/hotkey/SoundActivity$SoundItem;

    invoke-virtual {v3, v0}, Lcom/konka/hotkey/SoundActivity$SoundItem;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lcom/konka/hotkey/SoundActivity;->mViewSiMovie:Lcom/konka/hotkey/SoundActivity$SoundItem;

    invoke-virtual {v3, v1}, Lcom/konka/hotkey/SoundActivity$SoundItem;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v3, p0, Lcom/konka/hotkey/SoundActivity;->mViewSiMovie:Lcom/konka/hotkey/SoundActivity$SoundItem;

    invoke-virtual {v3, v2}, Lcom/konka/hotkey/SoundActivity$SoundItem;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v3, p0, Lcom/konka/hotkey/SoundActivity;->mViewSiUser:Lcom/konka/hotkey/SoundActivity$SoundItem;

    invoke-virtual {v3, v0}, Lcom/konka/hotkey/SoundActivity$SoundItem;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lcom/konka/hotkey/SoundActivity;->mViewSiUser:Lcom/konka/hotkey/SoundActivity$SoundItem;

    invoke-virtual {v3, v1}, Lcom/konka/hotkey/SoundActivity$SoundItem;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v3, p0, Lcom/konka/hotkey/SoundActivity;->mViewSiUser:Lcom/konka/hotkey/SoundActivity$SoundItem;

    invoke-virtual {v3, v2}, Lcom/konka/hotkey/SoundActivity$SoundItem;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v3, p0, Lcom/konka/hotkey/SoundActivity;->mViewSiSport:Lcom/konka/hotkey/SoundActivity$SoundItem;

    invoke-virtual {v3, v0}, Lcom/konka/hotkey/SoundActivity$SoundItem;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lcom/konka/hotkey/SoundActivity;->mViewSiSport:Lcom/konka/hotkey/SoundActivity$SoundItem;

    invoke-virtual {v3, v1}, Lcom/konka/hotkey/SoundActivity$SoundItem;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v3, p0, Lcom/konka/hotkey/SoundActivity;->mViewSiSport:Lcom/konka/hotkey/SoundActivity$SoundItem;

    invoke-virtual {v3, v2}, Lcom/konka/hotkey/SoundActivity$SoundItem;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    return-void
.end method


# virtual methods
.method public focusAnticlockwise(Lcom/konka/hotkey/SoundActivity$SoundItem;)V
    .locals 1
    .param p1    # Lcom/konka/hotkey/SoundActivity$SoundItem;

    invoke-virtual {p1}, Lcom/konka/hotkey/SoundActivity$SoundItem;->getPos()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/konka/hotkey/SoundActivity;->mViewSiUser:Lcom/konka/hotkey/SoundActivity$SoundItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/SoundActivity$SoundItem;->requestFocus()Z

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/konka/hotkey/SoundActivity;->mViewSiNormal:Lcom/konka/hotkey/SoundActivity$SoundItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/SoundActivity$SoundItem;->requestFocus()Z

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/konka/hotkey/SoundActivity;->mViewSiMusic:Lcom/konka/hotkey/SoundActivity$SoundItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/SoundActivity$SoundItem;->requestFocus()Z

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/konka/hotkey/SoundActivity;->mViewSiMovie:Lcom/konka/hotkey/SoundActivity$SoundItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/SoundActivity$SoundItem;->requestFocus()Z

    goto :goto_0

    :pswitch_4
    iget-object v0, p0, Lcom/konka/hotkey/SoundActivity;->mViewSiSport:Lcom/konka/hotkey/SoundActivity$SoundItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/SoundActivity$SoundItem;->requestFocus()Z

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public focusClockwise(Lcom/konka/hotkey/SoundActivity$SoundItem;)V
    .locals 1
    .param p1    # Lcom/konka/hotkey/SoundActivity$SoundItem;

    invoke-virtual {p1}, Lcom/konka/hotkey/SoundActivity$SoundItem;->getPos()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/konka/hotkey/SoundActivity;->mViewSiMusic:Lcom/konka/hotkey/SoundActivity$SoundItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/SoundActivity$SoundItem;->requestFocus()Z

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/konka/hotkey/SoundActivity;->mViewSiMovie:Lcom/konka/hotkey/SoundActivity$SoundItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/SoundActivity$SoundItem;->requestFocus()Z

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/konka/hotkey/SoundActivity;->mViewSiSport:Lcom/konka/hotkey/SoundActivity$SoundItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/SoundActivity$SoundItem;->requestFocus()Z

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/konka/hotkey/SoundActivity;->mViewSiUser:Lcom/konka/hotkey/SoundActivity$SoundItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/SoundActivity$SoundItem;->requestFocus()Z

    goto :goto_0

    :pswitch_4
    iget-object v0, p0, Lcom/konka/hotkey/SoundActivity;->mViewSiNormal:Lcom/konka/hotkey/SoundActivity$SoundItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/SoundActivity$SoundItem;->requestFocus()Z

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f030004    # com.konka.hotkey.R.layout.hk_sound_menu

    invoke-virtual {p0, v0}, Lcom/konka/hotkey/SoundActivity;->setContentView(I)V

    const v0, 0x7f0a0011    # com.konka.hotkey.R.id.hk_sound_menu_container

    invoke-virtual {p0, v0}, Lcom/konka/hotkey/SoundActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/hotkey/SoundActivity;->mViewLlContainer:Landroid/widget/LinearLayout;

    invoke-static {}, Lcom/mstar/android/tv/TvAudioManager;->getInstance()Lcom/mstar/android/tv/TvAudioManager;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/hotkey/SoundActivity;->audioSkin:Lcom/mstar/android/tv/TvAudioManager;

    invoke-virtual {p0}, Lcom/konka/hotkey/SoundActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070006    # com.konka.hotkey.R.dimen.TVHotkeySoundModeItem_Width

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/konka/hotkey/SoundActivity;->IMG_WIDTH:I

    invoke-virtual {p0}, Lcom/konka/hotkey/SoundActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070007    # com.konka.hotkey.R.dimen.TVHotkeySoundModeItem_Height

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/konka/hotkey/SoundActivity;->IMG_HEIGHT:I

    invoke-virtual {p0}, Lcom/konka/hotkey/SoundActivity;->viewInit()V

    invoke-direct {p0}, Lcom/konka/hotkey/SoundActivity;->listenerInit()V

    iget-object v0, p0, Lcom/konka/hotkey/SoundActivity;->audioSkin:Lcom/mstar/android/tv/TvAudioManager;

    invoke-virtual {v0}, Lcom/mstar/android/tv/TvAudioManager;->getSoundMode()Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/hotkey/SoundActivity;->meSoundMode:Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Get init sound mode: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/hotkey/SoundActivity;->meSoundMode:Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/hotkey/SoundActivity;->meSoundMode:Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;

    invoke-direct {p0, v0}, Lcom/konka/hotkey/SoundActivity;->findFocus(Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;)V

    invoke-static {}, Lcom/konka/hotkey/LittleDownTimer;->getInstance()Lcom/konka/hotkey/LittleDownTimer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/hotkey/LittleDownTimer;->start()V

    return-void
.end method

.method protected onDestroy()V
    .locals 0

    invoke-static {}, Lcom/konka/hotkey/LittleDownTimer;->destroy()V

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method protected onResume()V
    .locals 1

    iget-object v0, p0, Lcom/konka/hotkey/SoundActivity;->myHandler:Landroid/os/Handler;

    invoke-static {v0}, Lcom/konka/hotkey/LittleDownTimer;->setHandler(Landroid/os/Handler;)V

    invoke-static {}, Lcom/konka/hotkey/LittleDownTimer;->resumeMenuTime()V

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    return-void
.end method

.method protected onStop()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    invoke-virtual {p0}, Lcom/konka/hotkey/SoundActivity;->finish()V

    return-void
.end method

.method public onUserInteraction()V
    .locals 0

    invoke-static {}, Lcom/konka/hotkey/LittleDownTimer;->resetMenuTime()V

    invoke-super {p0}, Landroid/app/Activity;->onUserInteraction()V

    return-void
.end method

.method public recoverItemNorBg()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/konka/hotkey/SoundActivity;->mViewSiNormal:Lcom/konka/hotkey/SoundActivity$SoundItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/SoundActivity$SoundItem;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/konka/hotkey/SoundActivity;->mViewSiNormal:Lcom/konka/hotkey/SoundActivity$SoundItem;

    iget v1, p0, Lcom/konka/hotkey/SoundActivity;->SIZE_NOR:F

    invoke-virtual {v0, v1}, Lcom/konka/hotkey/SoundActivity$SoundItem;->setNorBg(F)V

    iget-object v0, p0, Lcom/konka/hotkey/SoundActivity;->mViewSiNormal:Lcom/konka/hotkey/SoundActivity$SoundItem;

    invoke-virtual {v0, v2}, Lcom/konka/hotkey/SoundActivity$SoundItem;->setRunning(Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/konka/hotkey/SoundActivity;->mViewSiMusic:Lcom/konka/hotkey/SoundActivity$SoundItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/SoundActivity$SoundItem;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/konka/hotkey/SoundActivity;->mViewSiMusic:Lcom/konka/hotkey/SoundActivity$SoundItem;

    iget v1, p0, Lcom/konka/hotkey/SoundActivity;->SIZE_NOR:F

    invoke-virtual {v0, v1}, Lcom/konka/hotkey/SoundActivity$SoundItem;->setNorBg(F)V

    iget-object v0, p0, Lcom/konka/hotkey/SoundActivity;->mViewSiMusic:Lcom/konka/hotkey/SoundActivity$SoundItem;

    invoke-virtual {v0, v2}, Lcom/konka/hotkey/SoundActivity$SoundItem;->setRunning(Z)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/konka/hotkey/SoundActivity;->mViewSiMovie:Lcom/konka/hotkey/SoundActivity$SoundItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/SoundActivity$SoundItem;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/konka/hotkey/SoundActivity;->mViewSiMovie:Lcom/konka/hotkey/SoundActivity$SoundItem;

    iget v1, p0, Lcom/konka/hotkey/SoundActivity;->SIZE_NOR:F

    invoke-virtual {v0, v1}, Lcom/konka/hotkey/SoundActivity$SoundItem;->setNorBg(F)V

    iget-object v0, p0, Lcom/konka/hotkey/SoundActivity;->mViewSiMovie:Lcom/konka/hotkey/SoundActivity$SoundItem;

    invoke-virtual {v0, v2}, Lcom/konka/hotkey/SoundActivity$SoundItem;->setRunning(Z)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/konka/hotkey/SoundActivity;->mViewSiSport:Lcom/konka/hotkey/SoundActivity$SoundItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/SoundActivity$SoundItem;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/konka/hotkey/SoundActivity;->mViewSiSport:Lcom/konka/hotkey/SoundActivity$SoundItem;

    iget v1, p0, Lcom/konka/hotkey/SoundActivity;->SIZE_NOR:F

    invoke-virtual {v0, v1}, Lcom/konka/hotkey/SoundActivity$SoundItem;->setNorBg(F)V

    iget-object v0, p0, Lcom/konka/hotkey/SoundActivity;->mViewSiSport:Lcom/konka/hotkey/SoundActivity$SoundItem;

    invoke-virtual {v0, v2}, Lcom/konka/hotkey/SoundActivity$SoundItem;->setRunning(Z)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/konka/hotkey/SoundActivity;->mViewSiUser:Lcom/konka/hotkey/SoundActivity$SoundItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/SoundActivity$SoundItem;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/hotkey/SoundActivity;->mViewSiUser:Lcom/konka/hotkey/SoundActivity$SoundItem;

    iget v1, p0, Lcom/konka/hotkey/SoundActivity;->SIZE_NOR:F

    invoke-virtual {v0, v1}, Lcom/konka/hotkey/SoundActivity$SoundItem;->setNorBg(F)V

    iget-object v0, p0, Lcom/konka/hotkey/SoundActivity;->mViewSiUser:Lcom/konka/hotkey/SoundActivity$SoundItem;

    invoke-virtual {v0, v2}, Lcom/konka/hotkey/SoundActivity$SoundItem;->setRunning(Z)V

    goto :goto_0
.end method

.method public viewInit()V
    .locals 11

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v7, 0x0

    const/high16 v3, 0x41900000    # 18.0f

    const/high16 v2, 0x41800000    # 16.0f

    invoke-virtual {p0}, Lcom/konka/hotkey/SoundActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080003    # com.konka.hotkey.R.string.str_sys_language

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/hotkey/SoundActivity;->CURRENT_LANGUAGE:Ljava/lang/String;

    const-string v0, "en"

    iget-object v1, p0, Lcom/konka/hotkey/SoundActivity;->CURRENT_LANGUAGE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iput v2, p0, Lcom/konka/hotkey/SoundActivity;->SIZE_NOR:F

    iput v3, p0, Lcom/konka/hotkey/SoundActivity;->SIZE_SEL:F

    :goto_0
    new-instance v0, Lcom/konka/hotkey/SoundActivity$SoundItem;

    invoke-virtual {p0}, Lcom/konka/hotkey/SoundActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget v3, p0, Lcom/konka/hotkey/SoundActivity;->IMG_WIDTH:I

    iget v4, p0, Lcom/konka/hotkey/SoundActivity;->IMG_HEIGHT:I

    iget-object v1, p0, Lcom/konka/hotkey/SoundActivity;->strId:[I

    aget v5, v1, v7

    iget v6, p0, Lcom/konka/hotkey/SoundActivity;->SIZE_NOR:F

    sget-object v8, Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;->E_STANDARD:Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;

    move-object v1, p0

    invoke-direct/range {v0 .. v8}, Lcom/konka/hotkey/SoundActivity$SoundItem;-><init>(Lcom/konka/hotkey/SoundActivity;Landroid/content/Context;IIIFILcom/mstar/android/tvapi/common/vo/EnumSoundMode;)V

    iput-object v0, p0, Lcom/konka/hotkey/SoundActivity;->mViewSiNormal:Lcom/konka/hotkey/SoundActivity$SoundItem;

    new-instance v0, Lcom/konka/hotkey/SoundActivity$SoundItem;

    invoke-virtual {p0}, Lcom/konka/hotkey/SoundActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget v3, p0, Lcom/konka/hotkey/SoundActivity;->IMG_WIDTH:I

    iget v4, p0, Lcom/konka/hotkey/SoundActivity;->IMG_HEIGHT:I

    iget-object v1, p0, Lcom/konka/hotkey/SoundActivity;->strId:[I

    aget v5, v1, v9

    iget v6, p0, Lcom/konka/hotkey/SoundActivity;->SIZE_NOR:F

    sget-object v8, Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;->E_MUSIC:Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;

    move-object v1, p0

    move v7, v9

    invoke-direct/range {v0 .. v8}, Lcom/konka/hotkey/SoundActivity$SoundItem;-><init>(Lcom/konka/hotkey/SoundActivity;Landroid/content/Context;IIIFILcom/mstar/android/tvapi/common/vo/EnumSoundMode;)V

    iput-object v0, p0, Lcom/konka/hotkey/SoundActivity;->mViewSiMusic:Lcom/konka/hotkey/SoundActivity$SoundItem;

    new-instance v0, Lcom/konka/hotkey/SoundActivity$SoundItem;

    invoke-virtual {p0}, Lcom/konka/hotkey/SoundActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget v3, p0, Lcom/konka/hotkey/SoundActivity;->IMG_WIDTH:I

    iget v4, p0, Lcom/konka/hotkey/SoundActivity;->IMG_HEIGHT:I

    iget-object v1, p0, Lcom/konka/hotkey/SoundActivity;->strId:[I

    aget v5, v1, v10

    iget v6, p0, Lcom/konka/hotkey/SoundActivity;->SIZE_NOR:F

    sget-object v8, Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;->E_MOVIE:Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;

    move-object v1, p0

    move v7, v10

    invoke-direct/range {v0 .. v8}, Lcom/konka/hotkey/SoundActivity$SoundItem;-><init>(Lcom/konka/hotkey/SoundActivity;Landroid/content/Context;IIIFILcom/mstar/android/tvapi/common/vo/EnumSoundMode;)V

    iput-object v0, p0, Lcom/konka/hotkey/SoundActivity;->mViewSiMovie:Lcom/konka/hotkey/SoundActivity$SoundItem;

    new-instance v0, Lcom/konka/hotkey/SoundActivity$SoundItem;

    invoke-virtual {p0}, Lcom/konka/hotkey/SoundActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget v3, p0, Lcom/konka/hotkey/SoundActivity;->IMG_WIDTH:I

    iget v4, p0, Lcom/konka/hotkey/SoundActivity;->IMG_HEIGHT:I

    iget-object v1, p0, Lcom/konka/hotkey/SoundActivity;->strId:[I

    const/4 v5, 0x3

    aget v5, v1, v5

    iget v6, p0, Lcom/konka/hotkey/SoundActivity;->SIZE_NOR:F

    const/4 v7, 0x3

    sget-object v8, Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;->E_SPORTS:Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;

    move-object v1, p0

    invoke-direct/range {v0 .. v8}, Lcom/konka/hotkey/SoundActivity$SoundItem;-><init>(Lcom/konka/hotkey/SoundActivity;Landroid/content/Context;IIIFILcom/mstar/android/tvapi/common/vo/EnumSoundMode;)V

    iput-object v0, p0, Lcom/konka/hotkey/SoundActivity;->mViewSiSport:Lcom/konka/hotkey/SoundActivity$SoundItem;

    new-instance v0, Lcom/konka/hotkey/SoundActivity$SoundItem;

    invoke-virtual {p0}, Lcom/konka/hotkey/SoundActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget v3, p0, Lcom/konka/hotkey/SoundActivity;->IMG_WIDTH:I

    iget v4, p0, Lcom/konka/hotkey/SoundActivity;->IMG_HEIGHT:I

    iget-object v1, p0, Lcom/konka/hotkey/SoundActivity;->strId:[I

    const/4 v5, 0x4

    aget v5, v1, v5

    iget v6, p0, Lcom/konka/hotkey/SoundActivity;->SIZE_NOR:F

    const/4 v7, 0x4

    sget-object v8, Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;->E_USER:Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;

    move-object v1, p0

    invoke-direct/range {v0 .. v8}, Lcom/konka/hotkey/SoundActivity$SoundItem;-><init>(Lcom/konka/hotkey/SoundActivity;Landroid/content/Context;IIIFILcom/mstar/android/tvapi/common/vo/EnumSoundMode;)V

    iput-object v0, p0, Lcom/konka/hotkey/SoundActivity;->mViewSiUser:Lcom/konka/hotkey/SoundActivity$SoundItem;

    iget-object v0, p0, Lcom/konka/hotkey/SoundActivity;->mViewLlContainer:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/konka/hotkey/SoundActivity;->mViewSiNormal:Lcom/konka/hotkey/SoundActivity$SoundItem;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/konka/hotkey/SoundActivity;->mViewLlContainer:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/konka/hotkey/SoundActivity;->mViewSiMusic:Lcom/konka/hotkey/SoundActivity$SoundItem;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/konka/hotkey/SoundActivity;->mViewLlContainer:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/konka/hotkey/SoundActivity;->mViewSiMovie:Lcom/konka/hotkey/SoundActivity$SoundItem;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/konka/hotkey/SoundActivity;->mViewLlContainer:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/konka/hotkey/SoundActivity;->mViewSiSport:Lcom/konka/hotkey/SoundActivity$SoundItem;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/konka/hotkey/SoundActivity;->mViewLlContainer:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/konka/hotkey/SoundActivity;->mViewSiUser:Lcom/konka/hotkey/SoundActivity$SoundItem;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    return-void

    :cond_0
    iput v2, p0, Lcom/konka/hotkey/SoundActivity;->SIZE_NOR:F

    iput v3, p0, Lcom/konka/hotkey/SoundActivity;->SIZE_SEL:F

    goto/16 :goto_0
.end method
