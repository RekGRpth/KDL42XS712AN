.class Lcom/konka/hotkey/VolumeActivity$SeekBarChangeListener;
.super Ljava/lang/Object;
.source "VolumeActivity.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/hotkey/VolumeActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SeekBarChangeListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/hotkey/VolumeActivity;


# direct methods
.method constructor <init>(Lcom/konka/hotkey/VolumeActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/hotkey/VolumeActivity$SeekBarChangeListener;->this$0:Lcom/konka/hotkey/VolumeActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 3
    .param p1    # Landroid/widget/SeekBar;
    .param p2    # I
    .param p3    # Z

    iget-object v0, p0, Lcom/konka/hotkey/VolumeActivity$SeekBarChangeListener;->this$0:Lcom/konka/hotkey/VolumeActivity;

    # getter for: Lcom/konka/hotkey/VolumeActivity;->mViewTvSoundVal:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/hotkey/VolumeActivity;->access$1(Lcom/konka/hotkey/VolumeActivity;)Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/hotkey/VolumeActivity$SeekBarChangeListener;->this$0:Lcom/konka/hotkey/VolumeActivity;

    invoke-static {v0, p2}, Lcom/konka/hotkey/VolumeActivity;->access$2(Lcom/konka/hotkey/VolumeActivity;I)V

    return-void
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1    # Landroid/widget/SeekBar;

    invoke-static {}, Lcom/konka/hotkey/LittleDownTimer;->destroy()V

    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1    # Landroid/widget/SeekBar;

    invoke-static {}, Lcom/konka/hotkey/LittleDownTimer;->resumeMenuTime()V

    return-void
.end method
