.class Lcom/konka/hotkey/SoundActivity$SoundItem;
.super Lcom/konka/hotkey/view/HotkeyTextView;
.source "SoundActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/hotkey/SoundActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SoundItem"
.end annotation


# instance fields
.field public meMode:Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;

.field final synthetic this$0:Lcom/konka/hotkey/SoundActivity;


# direct methods
.method public constructor <init>(Lcom/konka/hotkey/SoundActivity;Landroid/content/Context;IIIFILcom/mstar/android/tvapi/common/vo/EnumSoundMode;)V
    .locals 7
    .param p2    # Landroid/content/Context;
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # F
    .param p7    # I
    .param p8    # Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;

    iput-object p1, p0, Lcom/konka/hotkey/SoundActivity$SoundItem;->this$0:Lcom/konka/hotkey/SoundActivity;

    move-object v0, p0

    move-object v1, p2

    move v2, p3

    move v3, p4

    move v4, p5

    move v5, p6

    move v6, p7

    invoke-direct/range {v0 .. v6}, Lcom/konka/hotkey/view/HotkeyTextView;-><init>(Landroid/content/Context;IIIFI)V

    iput-object p8, p0, Lcom/konka/hotkey/SoundActivity$SoundItem;->meMode:Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;

    return-void
.end method


# virtual methods
.method public doUpdate()Z
    .locals 2

    invoke-static {}, Lcom/mstar/android/tv/TvAudioManager;->getInstance()Lcom/mstar/android/tv/TvAudioManager;

    iget-object v0, p0, Lcom/konka/hotkey/SoundActivity$SoundItem;->this$0:Lcom/konka/hotkey/SoundActivity;

    # getter for: Lcom/konka/hotkey/SoundActivity;->audioSkin:Lcom/mstar/android/tv/TvAudioManager;
    invoke-static {v0}, Lcom/konka/hotkey/SoundActivity;->access$0(Lcom/konka/hotkey/SoundActivity;)Lcom/mstar/android/tv/TvAudioManager;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/hotkey/SoundActivity$SoundItem;->meMode:Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;

    invoke-virtual {v0, v1}, Lcom/mstar/android/tv/TvAudioManager;->setSoundMode(Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;)Z

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "click: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/hotkey/SoundActivity$SoundItem;->meMode:Lcom/mstar/android/tvapi/common/vo/EnumSoundMode;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    const/4 v0, 0x1

    return v0
.end method
