.class Lcom/konka/hotkey/CountdownActivity$1;
.super Landroid/os/Handler;
.source "CountdownActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/hotkey/CountdownActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/hotkey/CountdownActivity;


# direct methods
.method constructor <init>(Lcom/konka/hotkey/CountdownActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/hotkey/CountdownActivity$1;->this$0:Lcom/konka/hotkey/CountdownActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1    # Landroid/os/Message;

    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v1, p0, Lcom/konka/hotkey/CountdownActivity$1;->this$0:Lcom/konka/hotkey/CountdownActivity;

    # getter for: Lcom/konka/hotkey/CountdownActivity;->mViewTvCount:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/konka/hotkey/CountdownActivity;->access$0(Lcom/konka/hotkey/CountdownActivity;)Landroid/widget/TextView;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/konka/hotkey/CountdownActivity$1;->this$0:Lcom/konka/hotkey/CountdownActivity;

    # getter for: Lcom/konka/hotkey/CountdownActivity;->miSecCount:I
    invoke-static {v3}, Lcom/konka/hotkey/CountdownActivity;->access$1(Lcom/konka/hotkey/CountdownActivity;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_1
    iget-object v1, p0, Lcom/konka/hotkey/CountdownActivity$1;->this$0:Lcom/konka/hotkey/CountdownActivity;

    # getter for: Lcom/konka/hotkey/CountdownActivity;->timer:Ljava/util/Timer;
    invoke-static {v1}, Lcom/konka/hotkey/CountdownActivity;->access$2(Lcom/konka/hotkey/CountdownActivity;)Ljava/util/Timer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Timer;->cancel()V

    const-string v1, "Cancel enter sleep mode"

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/hotkey/CountdownActivity$1;->this$0:Lcom/konka/hotkey/CountdownActivity;

    invoke-virtual {v1}, Lcom/konka/hotkey/CountdownActivity;->finish()V

    goto :goto_0

    :pswitch_2
    const-string v1, "Start sleep Activity"

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/hotkey/CountdownActivity$1;->this$0:Lcom/konka/hotkey/CountdownActivity;

    # getter for: Lcom/konka/hotkey/CountdownActivity;->timer:Ljava/util/Timer;
    invoke-static {v1}, Lcom/konka/hotkey/CountdownActivity;->access$2(Lcom/konka/hotkey/CountdownActivity;)Ljava/util/Timer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Timer;->cancel()V

    iget-object v1, p0, Lcom/konka/hotkey/CountdownActivity$1;->this$0:Lcom/konka/hotkey/CountdownActivity;

    # getter for: Lcom/konka/hotkey/CountdownActivity;->isNoSignalStandby:Z
    invoke-static {v1}, Lcom/konka/hotkey/CountdownActivity;->access$3(Lcom/konka/hotkey/CountdownActivity;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/konka/hotkey/CountdownActivity$1;->this$0:Lcom/konka/hotkey/CountdownActivity;

    # getter for: Lcom/konka/hotkey/CountdownActivity;->mCurrentInputSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    invoke-static {v1}, Lcom/konka/hotkey/CountdownActivity;->access$4(Lcom/konka/hotkey/CountdownActivity;)Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v1

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_VGA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-ne v1, v2, :cond_0

    const-string v1, "Send message com.konka.tv.hotkey.VGA_NOSIGNALPOWER_OFF"

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.konka.tv.hotkey.VGA_NOSIGNALPOWER_OFF"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    :goto_1
    iget-object v1, p0, Lcom/konka/hotkey/CountdownActivity$1;->this$0:Lcom/konka/hotkey/CountdownActivity;

    invoke-virtual {v1, v0}, Lcom/konka/hotkey/CountdownActivity;->sendBroadcast(Landroid/content/Intent;)V

    iget-object v1, p0, Lcom/konka/hotkey/CountdownActivity$1;->this$0:Lcom/konka/hotkey/CountdownActivity;

    invoke-virtual {v1}, Lcom/konka/hotkey/CountdownActivity;->finish()V

    goto :goto_0

    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.konka.tv.hotkey.POWER_OFF"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
