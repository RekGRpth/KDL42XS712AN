.class public Lcom/konka/hotkey/volumeViewHolder;
.super Ljava/lang/Object;
.source "volumeViewHolder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/hotkey/volumeViewHolder$MyHandler;,
        Lcom/konka/hotkey/volumeViewHolder$UpdateAudioVolumeThread;
    }
.end annotation


# static fields
.field public static final AUTOEXIT_TIMEOUT:I = 0x7

.field private static final KILL_VIEW_FOR_VOL_MSG:I = 0x3e9

.field public static final SET_TIME_INTERVAL:I = 0x12c

.field public static final VOLUME_DECREASE:I = 0xde

.field public static final VOLUME_INCREASE:I = 0x6f

.field public static final VOLUME_SET:I = 0x14d

.field public static bIsVolumeShow:Z

.field public static iAutoExitCountDown:I

.field private static updateAudioVolumeThread:Lcom/konka/hotkey/volumeViewHolder$UpdateAudioVolumeThread;


# instance fields
.field private audioskin:Lcom/mstar/android/tv/TvAudioManager;

.field private barVol:Landroid/widget/SeekBar;

.field private iPreVolumeValue:I

.field private iVolumeValue:I

.field private mContext:Landroid/content/Context;

.field private myHandler:Lcom/konka/hotkey/volumeViewHolder$MyHandler;

.field private textView:Landroid/widget/TextView;

.field private volumeView:Landroid/view/View;

.field private wm:Landroid/view/WindowManager;

.field wmParams:Landroid/view/WindowManager$LayoutParams;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-boolean v0, Lcom/konka/hotkey/volumeViewHolder;->bIsVolumeShow:Z

    const/4 v0, 0x7

    sput v0, Lcom/konka/hotkey/volumeViewHolder;->iAutoExitCountDown:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1    # Landroid/content/Context;

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v2, p0, Lcom/konka/hotkey/volumeViewHolder;->mContext:Landroid/content/Context;

    iput-object v2, p0, Lcom/konka/hotkey/volumeViewHolder;->wm:Landroid/view/WindowManager;

    iput-object v2, p0, Lcom/konka/hotkey/volumeViewHolder;->wmParams:Landroid/view/WindowManager$LayoutParams;

    iput-object v2, p0, Lcom/konka/hotkey/volumeViewHolder;->audioskin:Lcom/mstar/android/tv/TvAudioManager;

    const/4 v0, 0x0

    iput v0, p0, Lcom/konka/hotkey/volumeViewHolder;->iVolumeValue:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/konka/hotkey/volumeViewHolder;->iPreVolumeValue:I

    iput-object v2, p0, Lcom/konka/hotkey/volumeViewHolder;->myHandler:Lcom/konka/hotkey/volumeViewHolder$MyHandler;

    iput-object p1, p0, Lcom/konka/hotkey/volumeViewHolder;->mContext:Landroid/content/Context;

    iget-object v0, p0, Lcom/konka/hotkey/volumeViewHolder;->mContext:Landroid/content/Context;

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iput-object v0, p0, Lcom/konka/hotkey/volumeViewHolder;->wm:Landroid/view/WindowManager;

    iget-object v0, p0, Lcom/konka/hotkey/volumeViewHolder;->audioskin:Lcom/mstar/android/tv/TvAudioManager;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tv/TvAudioManager;->getInstance()Lcom/mstar/android/tv/TvAudioManager;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/hotkey/volumeViewHolder;->audioskin:Lcom/mstar/android/tv/TvAudioManager;

    :cond_0
    new-instance v0, Lcom/konka/hotkey/volumeViewHolder$MyHandler;

    invoke-direct {v0, p0, v2}, Lcom/konka/hotkey/volumeViewHolder$MyHandler;-><init>(Lcom/konka/hotkey/volumeViewHolder;Lcom/konka/hotkey/volumeViewHolder$MyHandler;)V

    iput-object v0, p0, Lcom/konka/hotkey/volumeViewHolder;->myHandler:Lcom/konka/hotkey/volumeViewHolder$MyHandler;

    return-void
.end method

.method static synthetic access$0(Lcom/konka/hotkey/volumeViewHolder;)I
    .locals 1

    iget v0, p0, Lcom/konka/hotkey/volumeViewHolder;->iPreVolumeValue:I

    return v0
.end method

.method static synthetic access$1(Lcom/konka/hotkey/volumeViewHolder;)I
    .locals 1

    iget v0, p0, Lcom/konka/hotkey/volumeViewHolder;->iVolumeValue:I

    return v0
.end method

.method static synthetic access$2(Lcom/konka/hotkey/volumeViewHolder;I)V
    .locals 0

    iput p1, p0, Lcom/konka/hotkey/volumeViewHolder;->iPreVolumeValue:I

    return-void
.end method

.method static synthetic access$3(Lcom/konka/hotkey/volumeViewHolder;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/hotkey/volumeViewHolder;->adjustVolume(Z)V

    return-void
.end method

.method static synthetic access$4(Lcom/konka/hotkey/volumeViewHolder;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/hotkey/volumeViewHolder;->setVolumeValue(I)V

    return-void
.end method

.method private adjustVolume(Z)V
    .locals 7
    .param p1    # Z

    const/4 v6, 0x0

    const/16 v3, 0x64

    const/4 v5, 0x1

    sget-boolean v0, Lcom/konka/hotkey/volumeViewHolder;->bIsVolumeShow:Z

    const/4 v2, 0x7

    sput v2, Lcom/konka/hotkey/volumeViewHolder;->iAutoExitCountDown:I

    sget-boolean v2, Lcom/konka/hotkey/volumeViewHolder;->bIsVolumeShow:Z

    if-nez v2, :cond_0

    sput-boolean v5, Lcom/konka/hotkey/volumeViewHolder;->bIsVolumeShow:Z

    :cond_0
    invoke-direct {p0}, Lcom/konka/hotkey/volumeViewHolder;->startSetVolume()V

    if-nez p1, :cond_3

    iget v2, p0, Lcom/konka/hotkey/volumeViewHolder;->iVolumeValue:I

    if-lez v2, :cond_2

    iget v2, p0, Lcom/konka/hotkey/volumeViewHolder;->iVolumeValue:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/konka/hotkey/volumeViewHolder;->iVolumeValue:I

    :goto_0
    if-nez v0, :cond_5

    new-instance v2, Landroid/view/WindowManager$LayoutParams;

    const/16 v3, 0x1f4

    const/16 v4, 0xc8

    invoke-direct {v2, v3, v4}, Landroid/view/WindowManager$LayoutParams;-><init>(II)V

    iput-object v2, p0, Lcom/konka/hotkey/volumeViewHolder;->wmParams:Landroid/view/WindowManager$LayoutParams;

    iget-object v2, p0, Lcom/konka/hotkey/volumeViewHolder;->wmParams:Landroid/view/WindowManager$LayoutParams;

    const/16 v3, 0x7d6

    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->type:I

    iget-object v2, p0, Lcom/konka/hotkey/volumeViewHolder;->wmParams:Landroid/view/WindowManager$LayoutParams;

    iput v5, v2, Landroid/view/WindowManager$LayoutParams;->format:I

    iget-object v2, p0, Lcom/konka/hotkey/volumeViewHolder;->wm:Landroid/view/WindowManager;

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/konka/hotkey/volumeViewHolder;->mContext:Landroid/content/Context;

    const-string v3, "window"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/WindowManager;

    iput-object v2, p0, Lcom/konka/hotkey/volumeViewHolder;->wm:Landroid/view/WindowManager;

    :cond_1
    iget-object v2, p0, Lcom/konka/hotkey/volumeViewHolder;->mContext:Landroid/content/Context;

    const-string v3, "layout_inflater"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    const v2, 0x7f030008    # com.konka.hotkey.R.layout.hk_volume_menu

    invoke-virtual {v1, v2, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/konka/hotkey/volumeViewHolder;->volumeView:Landroid/view/View;

    iget-object v2, p0, Lcom/konka/hotkey/volumeViewHolder;->volumeView:Landroid/view/View;

    const v3, 0x7f0a002b    # com.konka.hotkey.R.id.hk_sound_menu_seekbar

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/SeekBar;

    iput-object v2, p0, Lcom/konka/hotkey/volumeViewHolder;->barVol:Landroid/widget/SeekBar;

    iget-object v2, p0, Lcom/konka/hotkey/volumeViewHolder;->volumeView:Landroid/view/View;

    const v3, 0x7f0a002d    # com.konka.hotkey.R.id.hk_sound_menu_tv_volum

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/konka/hotkey/volumeViewHolder;->textView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/konka/hotkey/volumeViewHolder;->barVol:Landroid/widget/SeekBar;

    iget v3, p0, Lcom/konka/hotkey/volumeViewHolder;->iVolumeValue:I

    invoke-virtual {v2, v3}, Landroid/widget/SeekBar;->setProgress(I)V

    iget-object v2, p0, Lcom/konka/hotkey/volumeViewHolder;->textView:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget v4, p0, Lcom/konka/hotkey/volumeViewHolder;->iVolumeValue:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/konka/hotkey/volumeViewHolder;->wm:Landroid/view/WindowManager;

    iget-object v3, p0, Lcom/konka/hotkey/volumeViewHolder;->volumeView:Landroid/view/View;

    iget-object v4, p0, Lcom/konka/hotkey/volumeViewHolder;->wmParams:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v2, v3, v4}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    sput-boolean v5, Lcom/konka/hotkey/volumeViewHolder;->bIsVolumeShow:Z

    new-instance v2, Lcom/konka/hotkey/volumeViewHolder$UpdateAudioVolumeThread;

    invoke-direct {v2, p0, v6}, Lcom/konka/hotkey/volumeViewHolder$UpdateAudioVolumeThread;-><init>(Lcom/konka/hotkey/volumeViewHolder;Lcom/konka/hotkey/volumeViewHolder$UpdateAudioVolumeThread;)V

    sput-object v2, Lcom/konka/hotkey/volumeViewHolder;->updateAudioVolumeThread:Lcom/konka/hotkey/volumeViewHolder$UpdateAudioVolumeThread;

    sget-object v2, Lcom/konka/hotkey/volumeViewHolder;->updateAudioVolumeThread:Lcom/konka/hotkey/volumeViewHolder$UpdateAudioVolumeThread;

    invoke-virtual {v2}, Lcom/konka/hotkey/volumeViewHolder$UpdateAudioVolumeThread;->start()V

    :goto_1
    return-void

    :cond_2
    const/4 v2, 0x0

    iput v2, p0, Lcom/konka/hotkey/volumeViewHolder;->iVolumeValue:I

    goto/16 :goto_0

    :cond_3
    iget v2, p0, Lcom/konka/hotkey/volumeViewHolder;->iVolumeValue:I

    if-ge v2, v3, :cond_4

    iget v2, p0, Lcom/konka/hotkey/volumeViewHolder;->iVolumeValue:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/konka/hotkey/volumeViewHolder;->iVolumeValue:I

    goto/16 :goto_0

    :cond_4
    iput v3, p0, Lcom/konka/hotkey/volumeViewHolder;->iVolumeValue:I

    goto/16 :goto_0

    :cond_5
    iget-object v2, p0, Lcom/konka/hotkey/volumeViewHolder;->barVol:Landroid/widget/SeekBar;

    iget v3, p0, Lcom/konka/hotkey/volumeViewHolder;->iVolumeValue:I

    invoke-virtual {v2, v3}, Landroid/widget/SeekBar;->setProgress(I)V

    iget-object v2, p0, Lcom/konka/hotkey/volumeViewHolder;->textView:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget v4, p0, Lcom/konka/hotkey/volumeViewHolder;->iVolumeValue:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/konka/hotkey/volumeViewHolder;->wm:Landroid/view/WindowManager;

    iget-object v3, p0, Lcom/konka/hotkey/volumeViewHolder;->volumeView:Landroid/view/View;

    iget-object v4, p0, Lcom/konka/hotkey/volumeViewHolder;->wmParams:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v2, v3, v4}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_1
.end method

.method private setVolumeValue(I)V
    .locals 11
    .param p1    # I

    const/4 v10, 0x0

    const v9, 0x7f0a002d    # com.konka.hotkey.R.id.hk_sound_menu_tv_volum

    const v8, 0x7f0a002b    # com.konka.hotkey.R.id.hk_sound_menu_seekbar

    const/4 v7, 0x1

    sget-boolean v0, Lcom/konka/hotkey/volumeViewHolder;->bIsVolumeShow:Z

    const/4 v4, 0x7

    sput v4, Lcom/konka/hotkey/volumeViewHolder;->iAutoExitCountDown:I

    sget-boolean v4, Lcom/konka/hotkey/volumeViewHolder;->bIsVolumeShow:Z

    if-nez v4, :cond_0

    sput-boolean v7, Lcom/konka/hotkey/volumeViewHolder;->bIsVolumeShow:Z

    :cond_0
    invoke-direct {p0}, Lcom/konka/hotkey/volumeViewHolder;->startSetVolume()V

    iput p1, p0, Lcom/konka/hotkey/volumeViewHolder;->iVolumeValue:I

    if-nez v0, :cond_2

    new-instance v4, Landroid/view/WindowManager$LayoutParams;

    const/16 v5, 0x1f4

    const/16 v6, 0xc8

    invoke-direct {v4, v5, v6}, Landroid/view/WindowManager$LayoutParams;-><init>(II)V

    iput-object v4, p0, Lcom/konka/hotkey/volumeViewHolder;->wmParams:Landroid/view/WindowManager$LayoutParams;

    iget-object v4, p0, Lcom/konka/hotkey/volumeViewHolder;->wmParams:Landroid/view/WindowManager$LayoutParams;

    const/16 v5, 0x7d6

    iput v5, v4, Landroid/view/WindowManager$LayoutParams;->type:I

    iget-object v4, p0, Lcom/konka/hotkey/volumeViewHolder;->wmParams:Landroid/view/WindowManager$LayoutParams;

    iput v7, v4, Landroid/view/WindowManager$LayoutParams;->format:I

    iget-object v4, p0, Lcom/konka/hotkey/volumeViewHolder;->wm:Landroid/view/WindowManager;

    if-nez v4, :cond_1

    iget-object v4, p0, Lcom/konka/hotkey/volumeViewHolder;->mContext:Landroid/content/Context;

    const-string v5, "window"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/WindowManager;

    iput-object v4, p0, Lcom/konka/hotkey/volumeViewHolder;->wm:Landroid/view/WindowManager;

    :cond_1
    iget-object v4, p0, Lcom/konka/hotkey/volumeViewHolder;->mContext:Landroid/content/Context;

    const-string v5, "layout_inflater"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    const v4, 0x7f030008    # com.konka.hotkey.R.layout.hk_volume_menu

    invoke-virtual {v2, v4, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/konka/hotkey/volumeViewHolder;->volumeView:Landroid/view/View;

    iget-object v4, p0, Lcom/konka/hotkey/volumeViewHolder;->volumeView:Landroid/view/View;

    invoke-virtual {v4, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/SeekBar;

    iget-object v4, p0, Lcom/konka/hotkey/volumeViewHolder;->volumeView:Landroid/view/View;

    invoke-virtual {v4, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iget v4, p0, Lcom/konka/hotkey/volumeViewHolder;->iVolumeValue:I

    invoke-virtual {v1, v4}, Landroid/widget/SeekBar;->setProgress(I)V

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget v5, p0, Lcom/konka/hotkey/volumeViewHolder;->iVolumeValue:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/konka/hotkey/volumeViewHolder;->wm:Landroid/view/WindowManager;

    iget-object v5, p0, Lcom/konka/hotkey/volumeViewHolder;->volumeView:Landroid/view/View;

    iget-object v6, p0, Lcom/konka/hotkey/volumeViewHolder;->wmParams:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v4, v5, v6}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    sput-boolean v7, Lcom/konka/hotkey/volumeViewHolder;->bIsVolumeShow:Z

    new-instance v4, Lcom/konka/hotkey/volumeViewHolder$UpdateAudioVolumeThread;

    invoke-direct {v4, p0, v10}, Lcom/konka/hotkey/volumeViewHolder$UpdateAudioVolumeThread;-><init>(Lcom/konka/hotkey/volumeViewHolder;Lcom/konka/hotkey/volumeViewHolder$UpdateAudioVolumeThread;)V

    sput-object v4, Lcom/konka/hotkey/volumeViewHolder;->updateAudioVolumeThread:Lcom/konka/hotkey/volumeViewHolder$UpdateAudioVolumeThread;

    sget-object v4, Lcom/konka/hotkey/volumeViewHolder;->updateAudioVolumeThread:Lcom/konka/hotkey/volumeViewHolder$UpdateAudioVolumeThread;

    invoke-virtual {v4}, Lcom/konka/hotkey/volumeViewHolder$UpdateAudioVolumeThread;->start()V

    :goto_0
    return-void

    :cond_2
    iget-object v4, p0, Lcom/konka/hotkey/volumeViewHolder;->volumeView:Landroid/view/View;

    invoke-virtual {v4, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/SeekBar;

    iget-object v4, p0, Lcom/konka/hotkey/volumeViewHolder;->volumeView:Landroid/view/View;

    invoke-virtual {v4, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iget v4, p0, Lcom/konka/hotkey/volumeViewHolder;->iVolumeValue:I

    invoke-virtual {v1, v4}, Landroid/widget/SeekBar;->setProgress(I)V

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget v5, p0, Lcom/konka/hotkey/volumeViewHolder;->iVolumeValue:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/konka/hotkey/volumeViewHolder;->wm:Landroid/view/WindowManager;

    iget-object v5, p0, Lcom/konka/hotkey/volumeViewHolder;->volumeView:Landroid/view/View;

    iget-object v6, p0, Lcom/konka/hotkey/volumeViewHolder;->wmParams:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v4, v5, v6}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method private startSetVolume()V
    .locals 4

    const/16 v3, 0x3e9

    iget-object v0, p0, Lcom/konka/hotkey/volumeViewHolder;->myHandler:Lcom/konka/hotkey/volumeViewHolder$MyHandler;

    invoke-virtual {v0, v3}, Lcom/konka/hotkey/volumeViewHolder$MyHandler;->removeMessages(I)V

    iget-object v0, p0, Lcom/konka/hotkey/volumeViewHolder;->myHandler:Lcom/konka/hotkey/volumeViewHolder$MyHandler;

    const-wide/16 v1, 0x1388

    invoke-virtual {v0, v3, v1, v2}, Lcom/konka/hotkey/volumeViewHolder$MyHandler;->sendEmptyMessageDelayed(IJ)Z

    return-void
.end method


# virtual methods
.method public closeView()V
    .locals 2

    const/4 v0, 0x0

    :try_start_0
    sput-boolean v0, Lcom/konka/hotkey/volumeViewHolder;->bIsVolumeShow:Z

    iget-object v0, p0, Lcom/konka/hotkey/volumeViewHolder;->wm:Landroid/view/WindowManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/hotkey/volumeViewHolder;->volumeView:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/hotkey/volumeViewHolder;->wm:Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/konka/hotkey/volumeViewHolder;->volumeView:Landroid/view/View;

    invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    :cond_0
    sget-object v0, Lcom/konka/hotkey/volumeViewHolder;->updateAudioVolumeThread:Lcom/konka/hotkey/volumeViewHolder$UpdateAudioVolumeThread;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/konka/hotkey/volumeViewHolder;->updateAudioVolumeThread:Lcom/konka/hotkey/volumeViewHolder$UpdateAudioVolumeThread;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/konka/hotkey/volumeViewHolder$UpdateAudioVolumeThread;->starting:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public getAudioSkin()Lcom/mstar/android/tv/TvAudioManager;
    .locals 1

    iget-object v0, p0, Lcom/konka/hotkey/volumeViewHolder;->audioskin:Lcom/mstar/android/tv/TvAudioManager;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tv/TvAudioManager;->getInstance()Lcom/mstar/android/tv/TvAudioManager;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/hotkey/volumeViewHolder;->audioskin:Lcom/mstar/android/tv/TvAudioManager;

    :cond_0
    iget-object v0, p0, Lcom/konka/hotkey/volumeViewHolder;->audioskin:Lcom/mstar/android/tv/TvAudioManager;

    return-object v0
.end method

.method public setVolume(I)V
    .locals 2
    .param p1    # I

    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    const/16 v1, 0x14d

    iput v1, v0, Landroid/os/Message;->what:I

    iput p1, v0, Landroid/os/Message;->arg1:I

    iget-object v1, p0, Lcom/konka/hotkey/volumeViewHolder;->myHandler:Lcom/konka/hotkey/volumeViewHolder$MyHandler;

    invoke-virtual {v1, v0}, Lcom/konka/hotkey/volumeViewHolder$MyHandler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public volumeDown()V
    .locals 2

    iget-object v0, p0, Lcom/konka/hotkey/volumeViewHolder;->myHandler:Lcom/konka/hotkey/volumeViewHolder$MyHandler;

    const/16 v1, 0xde

    invoke-virtual {v0, v1}, Lcom/konka/hotkey/volumeViewHolder$MyHandler;->sendEmptyMessage(I)Z

    return-void
.end method

.method public volumeUp()V
    .locals 2

    iget-object v0, p0, Lcom/konka/hotkey/volumeViewHolder;->myHandler:Lcom/konka/hotkey/volumeViewHolder$MyHandler;

    const/16 v1, 0x6f

    invoke-virtual {v0, v1}, Lcom/konka/hotkey/volumeViewHolder$MyHandler;->sendEmptyMessage(I)Z

    return-void
.end method
