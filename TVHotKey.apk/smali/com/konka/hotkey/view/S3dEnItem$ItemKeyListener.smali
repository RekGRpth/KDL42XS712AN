.class Lcom/konka/hotkey/view/S3dEnItem$ItemKeyListener;
.super Ljava/lang/Object;
.source "S3dEnItem.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/hotkey/view/S3dEnItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ItemKeyListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/hotkey/view/S3dEnItem;


# direct methods
.method constructor <init>(Lcom/konka/hotkey/view/S3dEnItem;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/hotkey/view/S3dEnItem$ItemKeyListener;->this$0:Lcom/konka/hotkey/view/S3dEnItem;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # Landroid/view/KeyEvent;

    const/4 v2, 0x0

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_1

    const/16 v0, 0x15

    if-ne p2, v0, :cond_1

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    iget-object v1, p0, Lcom/konka/hotkey/view/S3dEnItem$ItemKeyListener;->this$0:Lcom/konka/hotkey/view/S3dEnItem;

    # getter for: Lcom/konka/hotkey/view/S3dEnItem;->mLayoutContainer:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/konka/hotkey/view/S3dEnItem;->access$0(Lcom/konka/hotkey/view/S3dEnItem;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getId()I

    move-result v1

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnItem$ItemKeyListener;->this$0:Lcom/konka/hotkey/view/S3dEnItem;

    # getter for: Lcom/konka/hotkey/view/S3dEnItem;->mValueIndex:I
    invoke-static {v0}, Lcom/konka/hotkey/view/S3dEnItem;->access$5(Lcom/konka/hotkey/view/S3dEnItem;)I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-static {v0, v1}, Lcom/konka/hotkey/view/S3dEnItem;->access$6(Lcom/konka/hotkey/view/S3dEnItem;I)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnItem$ItemKeyListener;->this$0:Lcom/konka/hotkey/view/S3dEnItem;

    # getter for: Lcom/konka/hotkey/view/S3dEnItem;->mValueIndex:I
    invoke-static {v0}, Lcom/konka/hotkey/view/S3dEnItem;->access$5(Lcom/konka/hotkey/view/S3dEnItem;)I

    move-result v0

    if-gez v0, :cond_0

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnItem$ItemKeyListener;->this$0:Lcom/konka/hotkey/view/S3dEnItem;

    iget-object v1, p0, Lcom/konka/hotkey/view/S3dEnItem$ItemKeyListener;->this$0:Lcom/konka/hotkey/view/S3dEnItem;

    iget-object v1, v1, Lcom/konka/hotkey/view/S3dEnItem;->mStatus:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    invoke-static {v0, v1}, Lcom/konka/hotkey/view/S3dEnItem;->access$6(Lcom/konka/hotkey/view/S3dEnItem;I)V

    :cond_0
    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnItem$ItemKeyListener;->this$0:Lcom/konka/hotkey/view/S3dEnItem;

    iget-object v1, p0, Lcom/konka/hotkey/view/S3dEnItem$ItemKeyListener;->this$0:Lcom/konka/hotkey/view/S3dEnItem;

    # getter for: Lcom/konka/hotkey/view/S3dEnItem;->mValueIndex:I
    invoke-static {v1}, Lcom/konka/hotkey/view/S3dEnItem;->access$5(Lcom/konka/hotkey/view/S3dEnItem;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/konka/hotkey/view/S3dEnItem;->setValueIndex(I)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnItem$ItemKeyListener;->this$0:Lcom/konka/hotkey/view/S3dEnItem;

    iget-object v1, p0, Lcom/konka/hotkey/view/S3dEnItem$ItemKeyListener;->this$0:Lcom/konka/hotkey/view/S3dEnItem;

    # getter for: Lcom/konka/hotkey/view/S3dEnItem;->mValueIndex:I
    invoke-static {v1}, Lcom/konka/hotkey/view/S3dEnItem;->access$5(Lcom/konka/hotkey/view/S3dEnItem;)I

    move-result v1

    # invokes: Lcom/konka/hotkey/view/S3dEnItem;->refreshArrowsImg(I)V
    invoke-static {v0, v1}, Lcom/konka/hotkey/view/S3dEnItem;->access$7(Lcom/konka/hotkey/view/S3dEnItem;I)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnItem$ItemKeyListener;->this$0:Lcom/konka/hotkey/view/S3dEnItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/S3dEnItem;->doUpdate()Z

    :cond_1
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_3

    const/16 v0, 0x16

    if-ne p2, v0, :cond_3

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    iget-object v1, p0, Lcom/konka/hotkey/view/S3dEnItem$ItemKeyListener;->this$0:Lcom/konka/hotkey/view/S3dEnItem;

    # getter for: Lcom/konka/hotkey/view/S3dEnItem;->mLayoutContainer:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/konka/hotkey/view/S3dEnItem;->access$0(Lcom/konka/hotkey/view/S3dEnItem;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getId()I

    move-result v1

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnItem$ItemKeyListener;->this$0:Lcom/konka/hotkey/view/S3dEnItem;

    # getter for: Lcom/konka/hotkey/view/S3dEnItem;->mValueIndex:I
    invoke-static {v0}, Lcom/konka/hotkey/view/S3dEnItem;->access$5(Lcom/konka/hotkey/view/S3dEnItem;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {v0, v1}, Lcom/konka/hotkey/view/S3dEnItem;->access$6(Lcom/konka/hotkey/view/S3dEnItem;I)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnItem$ItemKeyListener;->this$0:Lcom/konka/hotkey/view/S3dEnItem;

    # getter for: Lcom/konka/hotkey/view/S3dEnItem;->mValueIndex:I
    invoke-static {v0}, Lcom/konka/hotkey/view/S3dEnItem;->access$5(Lcom/konka/hotkey/view/S3dEnItem;)I

    move-result v0

    iget-object v1, p0, Lcom/konka/hotkey/view/S3dEnItem$ItemKeyListener;->this$0:Lcom/konka/hotkey/view/S3dEnItem;

    iget-object v1, v1, Lcom/konka/hotkey/view/S3dEnItem;->mStatus:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-le v0, v1, :cond_2

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnItem$ItemKeyListener;->this$0:Lcom/konka/hotkey/view/S3dEnItem;

    invoke-static {v0, v2}, Lcom/konka/hotkey/view/S3dEnItem;->access$6(Lcom/konka/hotkey/view/S3dEnItem;I)V

    :cond_2
    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnItem$ItemKeyListener;->this$0:Lcom/konka/hotkey/view/S3dEnItem;

    iget-object v1, p0, Lcom/konka/hotkey/view/S3dEnItem$ItemKeyListener;->this$0:Lcom/konka/hotkey/view/S3dEnItem;

    # getter for: Lcom/konka/hotkey/view/S3dEnItem;->mValueIndex:I
    invoke-static {v1}, Lcom/konka/hotkey/view/S3dEnItem;->access$5(Lcom/konka/hotkey/view/S3dEnItem;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/konka/hotkey/view/S3dEnItem;->setValueIndex(I)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnItem$ItemKeyListener;->this$0:Lcom/konka/hotkey/view/S3dEnItem;

    iget-object v1, p0, Lcom/konka/hotkey/view/S3dEnItem$ItemKeyListener;->this$0:Lcom/konka/hotkey/view/S3dEnItem;

    # getter for: Lcom/konka/hotkey/view/S3dEnItem;->mValueIndex:I
    invoke-static {v1}, Lcom/konka/hotkey/view/S3dEnItem;->access$5(Lcom/konka/hotkey/view/S3dEnItem;)I

    move-result v1

    # invokes: Lcom/konka/hotkey/view/S3dEnItem;->refreshArrowsImg(I)V
    invoke-static {v0, v1}, Lcom/konka/hotkey/view/S3dEnItem;->access$7(Lcom/konka/hotkey/view/S3dEnItem;I)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnItem$ItemKeyListener;->this$0:Lcom/konka/hotkey/view/S3dEnItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/S3dEnItem;->doUpdate()Z

    :cond_3
    return v2
.end method
