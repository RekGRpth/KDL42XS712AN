.class public final enum Lcom/konka/hotkey/view/Com3DPIPItem$STATE;
.super Ljava/lang/Enum;
.source "Com3DPIPItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/hotkey/view/Com3DPIPItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "STATE"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/konka/hotkey/view/Com3DPIPItem$STATE;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum CURR_FOCUS:Lcom/konka/hotkey/view/Com3DPIPItem$STATE;

.field public static final enum CURR_INUSE:Lcom/konka/hotkey/view/Com3DPIPItem$STATE;

.field private static final synthetic ENUM$VALUES:[Lcom/konka/hotkey/view/Com3DPIPItem$STATE;

.field public static final enum FORBIDDEN:Lcom/konka/hotkey/view/Com3DPIPItem$STATE;

.field public static final enum NORMAL:Lcom/konka/hotkey/view/Com3DPIPItem$STATE;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/konka/hotkey/view/Com3DPIPItem$STATE;

    const-string v1, "CURR_FOCUS"

    invoke-direct {v0, v1, v2}, Lcom/konka/hotkey/view/Com3DPIPItem$STATE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/hotkey/view/Com3DPIPItem$STATE;->CURR_FOCUS:Lcom/konka/hotkey/view/Com3DPIPItem$STATE;

    new-instance v0, Lcom/konka/hotkey/view/Com3DPIPItem$STATE;

    const-string v1, "CURR_INUSE"

    invoke-direct {v0, v1, v3}, Lcom/konka/hotkey/view/Com3DPIPItem$STATE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/hotkey/view/Com3DPIPItem$STATE;->CURR_INUSE:Lcom/konka/hotkey/view/Com3DPIPItem$STATE;

    new-instance v0, Lcom/konka/hotkey/view/Com3DPIPItem$STATE;

    const-string v1, "FORBIDDEN"

    invoke-direct {v0, v1, v4}, Lcom/konka/hotkey/view/Com3DPIPItem$STATE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/hotkey/view/Com3DPIPItem$STATE;->FORBIDDEN:Lcom/konka/hotkey/view/Com3DPIPItem$STATE;

    new-instance v0, Lcom/konka/hotkey/view/Com3DPIPItem$STATE;

    const-string v1, "NORMAL"

    invoke-direct {v0, v1, v5}, Lcom/konka/hotkey/view/Com3DPIPItem$STATE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/hotkey/view/Com3DPIPItem$STATE;->NORMAL:Lcom/konka/hotkey/view/Com3DPIPItem$STATE;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/konka/hotkey/view/Com3DPIPItem$STATE;

    sget-object v1, Lcom/konka/hotkey/view/Com3DPIPItem$STATE;->CURR_FOCUS:Lcom/konka/hotkey/view/Com3DPIPItem$STATE;

    aput-object v1, v0, v2

    sget-object v1, Lcom/konka/hotkey/view/Com3DPIPItem$STATE;->CURR_INUSE:Lcom/konka/hotkey/view/Com3DPIPItem$STATE;

    aput-object v1, v0, v3

    sget-object v1, Lcom/konka/hotkey/view/Com3DPIPItem$STATE;->FORBIDDEN:Lcom/konka/hotkey/view/Com3DPIPItem$STATE;

    aput-object v1, v0, v4

    sget-object v1, Lcom/konka/hotkey/view/Com3DPIPItem$STATE;->NORMAL:Lcom/konka/hotkey/view/Com3DPIPItem$STATE;

    aput-object v1, v0, v5

    sput-object v0, Lcom/konka/hotkey/view/Com3DPIPItem$STATE;->ENUM$VALUES:[Lcom/konka/hotkey/view/Com3DPIPItem$STATE;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/konka/hotkey/view/Com3DPIPItem$STATE;
    .locals 1

    const-class v0, Lcom/konka/hotkey/view/Com3DPIPItem$STATE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/konka/hotkey/view/Com3DPIPItem$STATE;

    return-object v0
.end method

.method public static values()[Lcom/konka/hotkey/view/Com3DPIPItem$STATE;
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lcom/konka/hotkey/view/Com3DPIPItem$STATE;->ENUM$VALUES:[Lcom/konka/hotkey/view/Com3DPIPItem$STATE;

    array-length v1, v0

    new-array v2, v1, [Lcom/konka/hotkey/view/Com3DPIPItem$STATE;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
