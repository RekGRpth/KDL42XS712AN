.class Lcom/konka/hotkey/view/S3dEnEffectItem$SeekBarListener;
.super Ljava/lang/Object;
.source "S3dEnEffectItem.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/hotkey/view/S3dEnEffectItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SeekBarListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;


# direct methods
.method constructor <init>(Lcom/konka/hotkey/view/S3dEnEffectItem;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$SeekBarListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 2
    .param p1    # Landroid/widget/SeekBar;
    .param p2    # I
    .param p3    # Z

    invoke-virtual {p1}, Landroid/widget/SeekBar;->getId()I

    move-result v0

    iget-object v1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$SeekBarListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    # getter for: Lcom/konka/hotkey/view/S3dEnEffectItem;->mSbDepth:Landroid/widget/SeekBar;
    invoke-static {v1}, Lcom/konka/hotkey/view/S3dEnEffectItem;->access$0(Lcom/konka/hotkey/view/S3dEnEffectItem;)Landroid/widget/SeekBar;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getId()I

    move-result v1

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$SeekBarListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    # getter for: Lcom/konka/hotkey/view/S3dEnEffectItem;->mViewDepth:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/hotkey/view/S3dEnEffectItem;->access$1(Lcom/konka/hotkey/view/S3dEnEffectItem;)Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$SeekBarListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    invoke-static {v0, p2}, Lcom/konka/hotkey/view/S3dEnEffectItem;->access$2(Lcom/konka/hotkey/view/S3dEnEffectItem;I)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$SeekBarListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    # getter for: Lcom/konka/hotkey/view/S3dEnEffectItem;->mS3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;
    invoke-static {v0}, Lcom/konka/hotkey/view/S3dEnEffectItem;->access$3(Lcom/konka/hotkey/view/S3dEnEffectItem;)Lcom/konka/kkinterface/tv/S3DDesk;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$SeekBarListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    # getter for: Lcom/konka/hotkey/view/S3dEnEffectItem;->mDepthValue:I
    invoke-static {v1}, Lcom/konka/hotkey/view/S3dEnEffectItem;->access$4(Lcom/konka/hotkey/view/S3dEnEffectItem;)I

    move-result v1

    mul-int/lit8 v1, v1, 0x3

    invoke-interface {v0, v1}, Lcom/konka/kkinterface/tv/S3DDesk;->set3DDepthMode(I)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Landroid/widget/SeekBar;->getId()I

    move-result v0

    iget-object v1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$SeekBarListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    # getter for: Lcom/konka/hotkey/view/S3dEnEffectItem;->mSbOffset:Landroid/widget/SeekBar;
    invoke-static {v1}, Lcom/konka/hotkey/view/S3dEnEffectItem;->access$5(Lcom/konka/hotkey/view/S3dEnEffectItem;)Landroid/widget/SeekBar;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getId()I

    move-result v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$SeekBarListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    # getter for: Lcom/konka/hotkey/view/S3dEnEffectItem;->mViewOffset:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/hotkey/view/S3dEnEffectItem;->access$6(Lcom/konka/hotkey/view/S3dEnEffectItem;)Landroid/widget/TextView;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$SeekBarListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    invoke-static {v0, p2}, Lcom/konka/hotkey/view/S3dEnEffectItem;->access$7(Lcom/konka/hotkey/view/S3dEnEffectItem;I)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$SeekBarListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    # getter for: Lcom/konka/hotkey/view/S3dEnEffectItem;->mS3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;
    invoke-static {v0}, Lcom/konka/hotkey/view/S3dEnEffectItem;->access$3(Lcom/konka/hotkey/view/S3dEnEffectItem;)Lcom/konka/kkinterface/tv/S3DDesk;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$SeekBarListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    # getter for: Lcom/konka/hotkey/view/S3dEnEffectItem;->mOffsetValue:I
    invoke-static {v1}, Lcom/konka/hotkey/view/S3dEnEffectItem;->access$8(Lcom/konka/hotkey/view/S3dEnEffectItem;)I

    move-result v1

    mul-int/lit8 v1, v1, 0x3

    invoke-interface {v0, v1}, Lcom/konka/kkinterface/tv/S3DDesk;->set3DOffsetMode(I)Z

    goto :goto_0
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1    # Landroid/widget/SeekBar;

    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1    # Landroid/widget/SeekBar;

    return-void
.end method
