.class public Lcom/konka/hotkey/view/HotkeyTextView;
.super Landroid/widget/RelativeLayout;
.source "HotkeyTextView.java"

# interfaces
.implements Lcom/konka/hotkey/IUpdateSysData;


# instance fields
.field private TEXT_SIZE_NOR:F

.field private TEXT_SIZE_SEL:F

.field private mContext:Landroid/content/Context;

.field public mTextView:Landroid/widget/TextView;

.field private mbIsRunning:Z

.field private pos:I


# direct methods
.method public constructor <init>(Landroid/content/Context;IIIFI)V
    .locals 7
    .param p1    # Landroid/content/Context;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # F
    .param p6    # I

    const/4 v6, 0x5

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, -0x1

    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    const/high16 v2, 0x41a00000    # 20.0f

    iput v2, p0, Lcom/konka/hotkey/view/HotkeyTextView;->TEXT_SIZE_NOR:F

    const/high16 v2, 0x41d00000    # 26.0f

    iput v2, p0, Lcom/konka/hotkey/view/HotkeyTextView;->TEXT_SIZE_SEL:F

    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, p2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    iput-object p1, p0, Lcom/konka/hotkey/view/HotkeyTextView;->mContext:Landroid/content/Context;

    invoke-virtual {p0, v0}, Lcom/konka/hotkey/view/HotkeyTextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iput p6, p0, Lcom/konka/hotkey/view/HotkeyTextView;->pos:I

    invoke-virtual {p0, v5}, Lcom/konka/hotkey/view/HotkeyTextView;->setFocusable(Z)V

    invoke-virtual {p0, v5}, Lcom/konka/hotkey/view/HotkeyTextView;->setFocusableInTouchMode(Z)V

    const v2, 0x7f020074    # com.konka.hotkey.R.drawable.hotkey_textview_selector

    invoke-virtual {p0, v2}, Lcom/konka/hotkey/view/HotkeyTextView;->setBackgroundResource(I)V

    new-instance v2, Landroid/widget/TextView;

    invoke-direct {v2, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/konka/hotkey/view/HotkeyTextView;->mTextView:Landroid/widget/TextView;

    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v3, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v2, 0xd

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    iget-object v2, p0, Lcom/konka/hotkey/view/HotkeyTextView;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v2, p0, Lcom/konka/hotkey/view/HotkeyTextView;->mTextView:Landroid/widget/TextView;

    const/16 v3, 0x11

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setGravity(I)V

    iget-object v2, p0, Lcom/konka/hotkey/view/HotkeyTextView;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v2, p4}, Landroid/widget/TextView;->setText(I)V

    iget-object v2, p0, Lcom/konka/hotkey/view/HotkeyTextView;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v2, p5}, Landroid/widget/TextView;->setTextSize(F)V

    iget-object v2, p0, Lcom/konka/hotkey/view/HotkeyTextView;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->setSingleLine()V

    iget-object v2, p0, Lcom/konka/hotkey/view/HotkeyTextView;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v2, v6, v4, v6, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    iget-object v2, p0, Lcom/konka/hotkey/view/HotkeyTextView;->mTextView:Landroid/widget/TextView;

    invoke-virtual {p0, v2}, Lcom/konka/hotkey/view/HotkeyTextView;->addView(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public doUpdate()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public getFocus(F)V
    .locals 2
    .param p1    # F

    invoke-virtual {p0, p1}, Lcom/konka/hotkey/view/HotkeyTextView;->setSelBg(F)V

    iget-object v0, p0, Lcom/konka/hotkey/view/HotkeyTextView;->mTextView:Landroid/widget/TextView;

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    return-void
.end method

.method public getPos()I
    .locals 1

    iget v0, p0, Lcom/konka/hotkey/view/HotkeyTextView;->pos:I

    return v0
.end method

.method public isRunning()Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/hotkey/view/HotkeyTextView;->mbIsRunning:Z

    return v0
.end method

.method public lostFocus(F)V
    .locals 1
    .param p1    # F

    iget-boolean v0, p0, Lcom/konka/hotkey/view/HotkeyTextView;->mbIsRunning:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/konka/hotkey/view/HotkeyTextView;->setRunBg(F)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, p1}, Lcom/konka/hotkey/view/HotkeyTextView;->setNorBg(F)V

    goto :goto_0
.end method

.method public setNorBg(F)V
    .locals 3
    .param p1    # F

    iget-object v0, p0, Lcom/konka/hotkey/view/HotkeyTextView;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextSize(F)V

    iget-object v0, p0, Lcom/konka/hotkey/view/HotkeyTextView;->mTextView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/konka/hotkey/view/HotkeyTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f060002    # com.konka.hotkey.R.color.text_normal_col

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    return-void
.end method

.method public setRunBg(F)V
    .locals 3
    .param p1    # F

    iget-object v0, p0, Lcom/konka/hotkey/view/HotkeyTextView;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextSize(F)V

    iget-object v0, p0, Lcom/konka/hotkey/view/HotkeyTextView;->mTextView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/konka/hotkey/view/HotkeyTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/high16 v2, 0x7f060000    # com.konka.hotkey.R.color.text_running_col

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    return-void
.end method

.method public setRunning(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/konka/hotkey/view/HotkeyTextView;->mbIsRunning:Z

    return-void
.end method

.method public setSelBg(F)V
    .locals 3
    .param p1    # F

    iget-object v0, p0, Lcom/konka/hotkey/view/HotkeyTextView;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextSize(F)V

    iget-object v0, p0, Lcom/konka/hotkey/view/HotkeyTextView;->mTextView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/konka/hotkey/view/HotkeyTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f060001    # com.konka.hotkey.R.color.text_select_col

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    return-void
.end method
