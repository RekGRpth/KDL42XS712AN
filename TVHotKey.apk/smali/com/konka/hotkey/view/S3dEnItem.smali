.class public Lcom/konka/hotkey/view/S3dEnItem;
.super Ljava/lang/Object;
.source "S3dEnItem.java"

# interfaces
.implements Lcom/konka/hotkey/IUpdateSysData;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/hotkey/view/S3dEnItem$ArrowsClickListener;,
        Lcom/konka/hotkey/view/S3dEnItem$ItemClickListener;,
        Lcom/konka/hotkey/view/S3dEnItem$ItemFocusListener;,
        Lcom/konka/hotkey/view/S3dEnItem$ItemKeyListener;
    }
.end annotation


# instance fields
.field private bFocuse:Z

.field mActivity:Landroid/app/Activity;

.field private mArrowsNext:Landroid/widget/ImageView;

.field private mArrowsPre:Landroid/widget/ImageView;

.field mContext:Landroid/content/Context;

.field private mLayoutContainer:Landroid/widget/LinearLayout;

.field mStatus:[I

.field private mValueIndex:I

.field private mViewContent:Landroid/widget/TextView;

.field private mViewStatus:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/content/Context;II[I)V
    .locals 2
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/content/Context;
    .param p3    # I
    .param p4    # I
    .param p5    # [I

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/konka/hotkey/view/S3dEnItem;->mContext:Landroid/content/Context;

    iput-object v0, p0, Lcom/konka/hotkey/view/S3dEnItem;->mActivity:Landroid/app/Activity;

    iput-object v0, p0, Lcom/konka/hotkey/view/S3dEnItem;->mLayoutContainer:Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/hotkey/view/S3dEnItem;->mViewContent:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/hotkey/view/S3dEnItem;->mViewStatus:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/hotkey/view/S3dEnItem;->mArrowsPre:Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/konka/hotkey/view/S3dEnItem;->mArrowsNext:Landroid/widget/ImageView;

    iput v1, p0, Lcom/konka/hotkey/view/S3dEnItem;->mValueIndex:I

    iput-boolean v1, p0, Lcom/konka/hotkey/view/S3dEnItem;->bFocuse:Z

    iput-object v0, p0, Lcom/konka/hotkey/view/S3dEnItem;->mStatus:[I

    iput-object p1, p0, Lcom/konka/hotkey/view/S3dEnItem;->mActivity:Landroid/app/Activity;

    iput-object p2, p0, Lcom/konka/hotkey/view/S3dEnItem;->mContext:Landroid/content/Context;

    iput p3, p0, Lcom/konka/hotkey/view/S3dEnItem;->mValueIndex:I

    iput-object p5, p0, Lcom/konka/hotkey/view/S3dEnItem;->mStatus:[I

    invoke-virtual {p0, p4}, Lcom/konka/hotkey/view/S3dEnItem;->findView(I)V

    invoke-direct {p0}, Lcom/konka/hotkey/view/S3dEnItem;->updateUI()V

    invoke-direct {p0}, Lcom/konka/hotkey/view/S3dEnItem;->listenerInit()V

    iget v0, p0, Lcom/konka/hotkey/view/S3dEnItem;->mValueIndex:I

    invoke-direct {p0, v0}, Lcom/konka/hotkey/view/S3dEnItem;->refreshArrowsImg(I)V

    return-void
.end method

.method static synthetic access$0(Lcom/konka/hotkey/view/S3dEnItem;)Landroid/widget/LinearLayout;
    .locals 1

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnItem;->mLayoutContainer:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$1(Lcom/konka/hotkey/view/S3dEnItem;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/hotkey/view/S3dEnItem;->setArrowsVisibility(Z)V

    return-void
.end method

.method static synthetic access$2(Lcom/konka/hotkey/view/S3dEnItem;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnItem;->mViewContent:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$3(Lcom/konka/hotkey/view/S3dEnItem;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnItem;->mViewStatus:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$4(Lcom/konka/hotkey/view/S3dEnItem;)Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnItem;->mArrowsNext:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$5(Lcom/konka/hotkey/view/S3dEnItem;)I
    .locals 1

    iget v0, p0, Lcom/konka/hotkey/view/S3dEnItem;->mValueIndex:I

    return v0
.end method

.method static synthetic access$6(Lcom/konka/hotkey/view/S3dEnItem;I)V
    .locals 0

    iput p1, p0, Lcom/konka/hotkey/view/S3dEnItem;->mValueIndex:I

    return-void
.end method

.method static synthetic access$7(Lcom/konka/hotkey/view/S3dEnItem;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/hotkey/view/S3dEnItem;->refreshArrowsImg(I)V

    return-void
.end method

.method static synthetic access$8(Lcom/konka/hotkey/view/S3dEnItem;)Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnItem;->mArrowsPre:Landroid/widget/ImageView;

    return-object v0
.end method

.method private listenerInit()V
    .locals 2

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnItem;->mLayoutContainer:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/konka/hotkey/view/S3dEnItem$ItemKeyListener;

    invoke-direct {v1, p0}, Lcom/konka/hotkey/view/S3dEnItem$ItemKeyListener;-><init>(Lcom/konka/hotkey/view/S3dEnItem;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnItem;->mLayoutContainer:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/konka/hotkey/view/S3dEnItem$ItemClickListener;

    invoke-direct {v1, p0}, Lcom/konka/hotkey/view/S3dEnItem$ItemClickListener;-><init>(Lcom/konka/hotkey/view/S3dEnItem;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnItem;->mLayoutContainer:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/konka/hotkey/view/S3dEnItem$ItemFocusListener;

    invoke-direct {v1, p0}, Lcom/konka/hotkey/view/S3dEnItem$ItemFocusListener;-><init>(Lcom/konka/hotkey/view/S3dEnItem;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnItem;->mArrowsNext:Landroid/widget/ImageView;

    new-instance v1, Lcom/konka/hotkey/view/S3dEnItem$ArrowsClickListener;

    invoke-direct {v1, p0}, Lcom/konka/hotkey/view/S3dEnItem$ArrowsClickListener;-><init>(Lcom/konka/hotkey/view/S3dEnItem;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnItem;->mArrowsPre:Landroid/widget/ImageView;

    new-instance v1, Lcom/konka/hotkey/view/S3dEnItem$ArrowsClickListener;

    invoke-direct {v1, p0}, Lcom/konka/hotkey/view/S3dEnItem$ArrowsClickListener;-><init>(Lcom/konka/hotkey/view/S3dEnItem;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private refreshArrowsImg(I)V
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnItem;->mArrowsPre:Landroid/widget/ImageView;

    const v1, 0x7f020030    # com.konka.hotkey.R.drawable.hk_3d_menu_en_pre_sel

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnItem;->mArrowsNext:Landroid/widget/ImageView;

    const v1, 0x7f02002d    # com.konka.hotkey.R.drawable.hk_3d_menu_en_next_sel

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    return-void
.end method

.method private setArrowsVisibility(Z)V
    .locals 3
    .param p1    # Z

    const/4 v2, 0x4

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnItem;->mArrowsPre:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnItem;->mArrowsNext:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnItem;->mArrowsPre:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnItem;->mArrowsNext:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method private updateUI()V
    .locals 1

    iget v0, p0, Lcom/konka/hotkey/view/S3dEnItem;->mValueIndex:I

    invoke-virtual {p0, v0}, Lcom/konka/hotkey/view/S3dEnItem;->setValueIndex(I)V

    return-void
.end method


# virtual methods
.method public doUpdate()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public findView(I)V
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnItem;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0, p1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/hotkey/view/S3dEnItem;->mLayoutContainer:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnItem;->mLayoutContainer:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/hotkey/view/S3dEnItem;->mViewContent:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnItem;->mLayoutContainer:Landroid/widget/LinearLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/konka/hotkey/view/S3dEnItem;->mArrowsPre:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnItem;->mLayoutContainer:Landroid/widget/LinearLayout;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/hotkey/view/S3dEnItem;->mViewStatus:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnItem;->mLayoutContainer:Landroid/widget/LinearLayout;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/konka/hotkey/view/S3dEnItem;->mArrowsNext:Landroid/widget/ImageView;

    return-void
.end method

.method public getValueIndex()I
    .locals 1

    iget v0, p0, Lcom/konka/hotkey/view/S3dEnItem;->mValueIndex:I

    return v0
.end method

.method public isSelected()Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/hotkey/view/S3dEnItem;->bFocuse:Z

    return v0
.end method

.method public requestFocus()Z
    .locals 1

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnItem;->mLayoutContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->requestFocus()Z

    move-result v0

    return v0
.end method

.method public setSelected(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/konka/hotkey/view/S3dEnItem;->bFocuse:Z

    return-void
.end method

.method public setStatusFbd()V
    .locals 3

    const v2, 0x7f060003    # com.konka.hotkey.R.color.text_forbidden_col

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnItem;->mLayoutContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnItem;->mLayoutContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setFocusableInTouchMode(Z)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnItem;->mViewContent:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/hotkey/view/S3dEnItem;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnItem;->mViewStatus:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/hotkey/view/S3dEnItem;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    return-void
.end method

.method public setValueIndex(I)V
    .locals 2
    .param p1    # I

    iput p1, p0, Lcom/konka/hotkey/view/S3dEnItem;->mValueIndex:I

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnItem;->mViewStatus:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/hotkey/view/S3dEnItem;->mStatus:[I

    aget v1, v1, p1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    return-void
.end method
