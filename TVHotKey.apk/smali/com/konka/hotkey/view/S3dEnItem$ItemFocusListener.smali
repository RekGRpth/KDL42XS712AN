.class Lcom/konka/hotkey/view/S3dEnItem$ItemFocusListener;
.super Ljava/lang/Object;
.source "S3dEnItem.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/hotkey/view/S3dEnItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ItemFocusListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/hotkey/view/S3dEnItem;


# direct methods
.method constructor <init>(Lcom/konka/hotkey/view/S3dEnItem;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/hotkey/view/S3dEnItem$ItemFocusListener;->this$0:Lcom/konka/hotkey/view/S3dEnItem;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 6
    .param p1    # Landroid/view/View;
    .param p2    # Z

    const v5, 0x7f060002    # com.konka.hotkey.R.color.text_normal_col

    const v4, 0x7f060001    # com.konka.hotkey.R.color.text_select_col

    const/4 v3, 0x1

    const/4 v2, 0x0

    if-eqz p2, :cond_1

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    iget-object v1, p0, Lcom/konka/hotkey/view/S3dEnItem$ItemFocusListener;->this$0:Lcom/konka/hotkey/view/S3dEnItem;

    # getter for: Lcom/konka/hotkey/view/S3dEnItem;->mLayoutContainer:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/konka/hotkey/view/S3dEnItem;->access$0(Lcom/konka/hotkey/view/S3dEnItem;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getId()I

    move-result v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnItem$ItemFocusListener;->this$0:Lcom/konka/hotkey/view/S3dEnItem;

    # invokes: Lcom/konka/hotkey/view/S3dEnItem;->setArrowsVisibility(Z)V
    invoke-static {v0, v3}, Lcom/konka/hotkey/view/S3dEnItem;->access$1(Lcom/konka/hotkey/view/S3dEnItem;Z)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnItem$ItemFocusListener;->this$0:Lcom/konka/hotkey/view/S3dEnItem;

    # getter for: Lcom/konka/hotkey/view/S3dEnItem;->mViewContent:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/hotkey/view/S3dEnItem;->access$2(Lcom/konka/hotkey/view/S3dEnItem;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/hotkey/view/S3dEnItem$ItemFocusListener;->this$0:Lcom/konka/hotkey/view/S3dEnItem;

    iget-object v1, v1, Lcom/konka/hotkey/view/S3dEnItem;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnItem$ItemFocusListener;->this$0:Lcom/konka/hotkey/view/S3dEnItem;

    # getter for: Lcom/konka/hotkey/view/S3dEnItem;->mViewStatus:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/hotkey/view/S3dEnItem;->access$3(Lcom/konka/hotkey/view/S3dEnItem;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/hotkey/view/S3dEnItem$ItemFocusListener;->this$0:Lcom/konka/hotkey/view/S3dEnItem;

    iget-object v1, v1, Lcom/konka/hotkey/view/S3dEnItem;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_0
    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnItem$ItemFocusListener;->this$0:Lcom/konka/hotkey/view/S3dEnItem;

    invoke-virtual {v0, v3}, Lcom/konka/hotkey/view/S3dEnItem;->setSelected(Z)V

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    iget-object v1, p0, Lcom/konka/hotkey/view/S3dEnItem$ItemFocusListener;->this$0:Lcom/konka/hotkey/view/S3dEnItem;

    # getter for: Lcom/konka/hotkey/view/S3dEnItem;->mLayoutContainer:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/konka/hotkey/view/S3dEnItem;->access$0(Lcom/konka/hotkey/view/S3dEnItem;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getId()I

    move-result v1

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnItem$ItemFocusListener;->this$0:Lcom/konka/hotkey/view/S3dEnItem;

    # invokes: Lcom/konka/hotkey/view/S3dEnItem;->setArrowsVisibility(Z)V
    invoke-static {v0, v2}, Lcom/konka/hotkey/view/S3dEnItem;->access$1(Lcom/konka/hotkey/view/S3dEnItem;Z)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnItem$ItemFocusListener;->this$0:Lcom/konka/hotkey/view/S3dEnItem;

    # getter for: Lcom/konka/hotkey/view/S3dEnItem;->mViewContent:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/hotkey/view/S3dEnItem;->access$2(Lcom/konka/hotkey/view/S3dEnItem;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/hotkey/view/S3dEnItem$ItemFocusListener;->this$0:Lcom/konka/hotkey/view/S3dEnItem;

    iget-object v1, v1, Lcom/konka/hotkey/view/S3dEnItem;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnItem$ItemFocusListener;->this$0:Lcom/konka/hotkey/view/S3dEnItem;

    # getter for: Lcom/konka/hotkey/view/S3dEnItem;->mViewStatus:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/hotkey/view/S3dEnItem;->access$3(Lcom/konka/hotkey/view/S3dEnItem;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/hotkey/view/S3dEnItem$ItemFocusListener;->this$0:Lcom/konka/hotkey/view/S3dEnItem;

    iget-object v1, v1, Lcom/konka/hotkey/view/S3dEnItem;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_2
    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnItem$ItemFocusListener;->this$0:Lcom/konka/hotkey/view/S3dEnItem;

    invoke-virtual {v0, v2}, Lcom/konka/hotkey/view/S3dEnItem;->setSelected(Z)V

    goto :goto_0
.end method
