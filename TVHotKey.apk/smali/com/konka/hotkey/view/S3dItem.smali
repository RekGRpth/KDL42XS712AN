.class public Lcom/konka/hotkey/view/S3dItem;
.super Landroid/widget/RelativeLayout;
.source "S3dItem.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnFocusChangeListener;
.implements Lcom/konka/hotkey/IUpdateSysData;


# instance fields
.field private final IMG_FBD_ID:[I

.field private final IMG_NOR_ID:[I

.field private final IMG_RUN_ID:[I

.field private final IMG_SEL_ID:[I

.field private TEXT_SIZE_NOR:F

.field private TEXT_SIZE_SEL:F

.field private isRunning:Z

.field private mBgFbdId:I

.field private mBgNorId:I

.field private mBgRunId:I

.field private mBgSelId:I

.field private mContext:Landroid/content/Context;

.field mTextView:Landroid/widget/TextView;

.field private pos:I

.field private status:I

.field private final strTextId:[I


# direct methods
.method public constructor <init>(Landroid/content/Context;IIIFF)V
    .locals 7
    .param p1    # Landroid/content/Context;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # F
    .param p6    # F

    const/4 v6, 0x5

    const/4 v5, 0x1

    const/16 v3, 0x8

    const/4 v4, 0x0

    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    iput v4, p0, Lcom/konka/hotkey/view/S3dItem;->status:I

    iput-boolean v4, p0, Lcom/konka/hotkey/view/S3dItem;->isRunning:Z

    new-array v2, v3, [I

    fill-array-data v2, :array_0

    iput-object v2, p0, Lcom/konka/hotkey/view/S3dItem;->IMG_SEL_ID:[I

    new-array v2, v3, [I

    fill-array-data v2, :array_1

    iput-object v2, p0, Lcom/konka/hotkey/view/S3dItem;->IMG_NOR_ID:[I

    new-array v2, v3, [I

    fill-array-data v2, :array_2

    iput-object v2, p0, Lcom/konka/hotkey/view/S3dItem;->IMG_RUN_ID:[I

    new-array v2, v3, [I

    fill-array-data v2, :array_3

    iput-object v2, p0, Lcom/konka/hotkey/view/S3dItem;->IMG_FBD_ID:[I

    new-array v2, v3, [I

    fill-array-data v2, :array_4

    iput-object v2, p0, Lcom/konka/hotkey/view/S3dItem;->strTextId:[I

    iput-object p1, p0, Lcom/konka/hotkey/view/S3dItem;->mContext:Landroid/content/Context;

    iput p4, p0, Lcom/konka/hotkey/view/S3dItem;->pos:I

    iget-object v2, p0, Lcom/konka/hotkey/view/S3dItem;->IMG_NOR_ID:[I

    aget v2, v2, p4

    iput v2, p0, Lcom/konka/hotkey/view/S3dItem;->mBgNorId:I

    iget-object v2, p0, Lcom/konka/hotkey/view/S3dItem;->IMG_SEL_ID:[I

    aget v2, v2, p4

    iput v2, p0, Lcom/konka/hotkey/view/S3dItem;->mBgSelId:I

    iget-object v2, p0, Lcom/konka/hotkey/view/S3dItem;->IMG_RUN_ID:[I

    aget v2, v2, p4

    iput v2, p0, Lcom/konka/hotkey/view/S3dItem;->mBgRunId:I

    iget-object v2, p0, Lcom/konka/hotkey/view/S3dItem;->IMG_FBD_ID:[I

    aget v2, v2, p4

    iput v2, p0, Lcom/konka/hotkey/view/S3dItem;->mBgFbdId:I

    iput p5, p0, Lcom/konka/hotkey/view/S3dItem;->TEXT_SIZE_NOR:F

    iput p6, p0, Lcom/konka/hotkey/view/S3dItem;->TEXT_SIZE_SEL:F

    invoke-virtual {p0, v5}, Lcom/konka/hotkey/view/S3dItem;->setFocusable(Z)V

    invoke-virtual {p0, v5}, Lcom/konka/hotkey/view/S3dItem;->setFocusableInTouchMode(Z)V

    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x1

    invoke-direct {v0, p2, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0}, Lcom/konka/hotkey/view/S3dItem;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v2, Landroid/widget/TextView;

    invoke-direct {v2, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/konka/hotkey/view/S3dItem;->mTextView:Landroid/widget/TextView;

    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v2, 0xa4

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v2, 0xc

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    const/16 v2, 0xa

    invoke-virtual {v1, v4, v4, v4, v2}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    iget-object v2, p0, Lcom/konka/hotkey/view/S3dItem;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v2, p0, Lcom/konka/hotkey/view/S3dItem;->mTextView:Landroid/widget/TextView;

    const/16 v3, 0x11

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setGravity(I)V

    iget-object v2, p0, Lcom/konka/hotkey/view/S3dItem;->mTextView:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/konka/hotkey/view/S3dItem;->strTextId:[I

    aget v3, v3, p4

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    iget-object v2, p0, Lcom/konka/hotkey/view/S3dItem;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->setSingleLine()V

    iget-object v2, p0, Lcom/konka/hotkey/view/S3dItem;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v2, v6, v4, v6, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    iget-object v2, p0, Lcom/konka/hotkey/view/S3dItem;->mTextView:Landroid/widget/TextView;

    invoke-virtual {p0, v2}, Lcom/konka/hotkey/view/S3dItem;->addView(Landroid/view/View;)V

    invoke-virtual {p0, p0}, Lcom/konka/hotkey/view/S3dItem;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0, p0}, Lcom/konka/hotkey/view/S3dItem;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget v2, p0, Lcom/konka/hotkey/view/S3dItem;->mBgNorId:I

    invoke-virtual {p0, v2}, Lcom/konka/hotkey/view/S3dItem;->setBackgroundResource(I)V

    return-void

    nop

    :array_0
    .array-data 4
        0x7f020036    # com.konka.hotkey.R.drawable.hk_3d_menu_lr_sel
        0x7f02003e    # com.konka.hotkey.R.drawable.hk_3d_menu_ud_sel
        0x7f02003a    # com.konka.hotkey.R.drawable.hk_3d_menu_off_sel
        0x7f02000b    # com.konka.hotkey.R.drawable.hk_3d_menu_3t2_sel
        0x7f020007    # com.konka.hotkey.R.drawable.hk_3d_menu_2t3_sel
        0x7f02001b    # com.konka.hotkey.R.drawable.hk_3d_menu_clar_sel
        0x7f020013    # com.konka.hotkey.R.drawable.hk_3d_menu_auto_sel
        0x7f020032    # com.konka.hotkey.R.drawable.hk_3d_menu_en_sel
    .end array-data

    :array_1
    .array-data 4
        0x7f020034    # com.konka.hotkey.R.drawable.hk_3d_menu_lr_nor
        0x7f02003c    # com.konka.hotkey.R.drawable.hk_3d_menu_ud_nor
        0x7f020038    # com.konka.hotkey.R.drawable.hk_3d_menu_off_nor
        0x7f020009    # com.konka.hotkey.R.drawable.hk_3d_menu_3t2_nor
        0x7f020005    # com.konka.hotkey.R.drawable.hk_3d_menu_2t3_nor
        0x7f020019    # com.konka.hotkey.R.drawable.hk_3d_menu_clar_nor
        0x7f020011    # com.konka.hotkey.R.drawable.hk_3d_menu_auto_nor
        0x7f02002e    # com.konka.hotkey.R.drawable.hk_3d_menu_en_nor
    .end array-data

    :array_2
    .array-data 4
        0x7f020035    # com.konka.hotkey.R.drawable.hk_3d_menu_lr_run
        0x7f02003d    # com.konka.hotkey.R.drawable.hk_3d_menu_ud_run
        0x7f020039    # com.konka.hotkey.R.drawable.hk_3d_menu_off_run
        0x7f02000a    # com.konka.hotkey.R.drawable.hk_3d_menu_3t2_run
        0x7f020006    # com.konka.hotkey.R.drawable.hk_3d_menu_2t3_run
        0x7f02001a    # com.konka.hotkey.R.drawable.hk_3d_menu_clar_run
        0x7f020012    # com.konka.hotkey.R.drawable.hk_3d_menu_auto_run
        0x7f020031    # com.konka.hotkey.R.drawable.hk_3d_menu_en_run
    .end array-data

    :array_3
    .array-data 4
        0x7f020033    # com.konka.hotkey.R.drawable.hk_3d_menu_lr_fbd
        0x7f02003b    # com.konka.hotkey.R.drawable.hk_3d_menu_ud_fbd
        0x7f020037    # com.konka.hotkey.R.drawable.hk_3d_menu_off_fbd
        0x7f020008    # com.konka.hotkey.R.drawable.hk_3d_menu_3t2_fbd
        0x7f020004    # com.konka.hotkey.R.drawable.hk_3d_menu_2t3_fbd
        0x7f020018    # com.konka.hotkey.R.drawable.hk_3d_menu_clar_fbd
        0x7f020010    # com.konka.hotkey.R.drawable.hk_3d_menu_auto_fbd
        0x7f02002a    # com.konka.hotkey.R.drawable.hk_3d_menu_en_fbd
    .end array-data

    :array_4
    .array-data 4
        0x7f08001d    # com.konka.hotkey.R.string.str_hk_3d_menu_lr
        0x7f08001e    # com.konka.hotkey.R.string.str_hk_3d_menu_ud
        0x7f08001f    # com.konka.hotkey.R.string.str_hk_3d_menu_off
        0x7f080020    # com.konka.hotkey.R.string.str_hk_3d_menu_3t2
        0x7f080021    # com.konka.hotkey.R.string.str_hk_3d_menu_2t3
        0x7f080022    # com.konka.hotkey.R.string.str_hk_3d_menu_clar
        0x7f080023    # com.konka.hotkey.R.string.str_hk_3d_menu_auto
        0x7f080024    # com.konka.hotkey.R.string.str_hk_3d_menu_en
    .end array-data
.end method


# virtual methods
.method public doUpdate()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public getPos()I
    .locals 1

    iget v0, p0, Lcom/konka/hotkey/view/S3dItem;->pos:I

    return v0
.end method

.method public getStatus()I
    .locals 1

    iget v0, p0, Lcom/konka/hotkey/view/S3dItem;->status:I

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/konka/hotkey/view/S3dItem;->isRunning:Z

    invoke-virtual {p0}, Lcom/konka/hotkey/view/S3dItem;->doUpdate()Z

    return-void
.end method

.method public onFocusChange(Landroid/view/View;Z)V
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # Z

    if-eqz p2, :cond_0

    iget v0, p0, Lcom/konka/hotkey/view/S3dItem;->TEXT_SIZE_NOR:F

    invoke-virtual {p0, v0}, Lcom/konka/hotkey/view/S3dItem;->setSelStatus(F)V

    :goto_0
    return-void

    :cond_0
    iget-boolean v0, p0, Lcom/konka/hotkey/view/S3dItem;->isRunning:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/konka/hotkey/view/S3dItem;->TEXT_SIZE_NOR:F

    invoke-virtual {p0, v0}, Lcom/konka/hotkey/view/S3dItem;->setRunStatus(F)V

    goto :goto_0

    :cond_1
    iget v0, p0, Lcom/konka/hotkey/view/S3dItem;->TEXT_SIZE_NOR:F

    invoke-virtual {p0, v0}, Lcom/konka/hotkey/view/S3dItem;->setNorStatus(F)V

    goto :goto_0
.end method

.method public setFbdStatus(F)V
    .locals 4
    .param p1    # F

    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/konka/hotkey/view/S3dItem;->isRunning:Z

    iget v0, p0, Lcom/konka/hotkey/view/S3dItem;->mBgFbdId:I

    invoke-virtual {p0, v0}, Lcom/konka/hotkey/view/S3dItem;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dItem;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextSize(F)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dItem;->mTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/hotkey/view/S3dItem;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f060002    # com.konka.hotkey.R.color.text_normal_col

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-virtual {p0, v3}, Lcom/konka/hotkey/view/S3dItem;->setClickable(Z)V

    invoke-virtual {p0, v3}, Lcom/konka/hotkey/view/S3dItem;->setFocusable(Z)V

    invoke-virtual {p0, v3}, Lcom/konka/hotkey/view/S3dItem;->setFocusableInTouchMode(Z)V

    return-void
.end method

.method public setNorStatus(F)V
    .locals 4
    .param p1    # F

    const/4 v3, 0x1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/konka/hotkey/view/S3dItem;->isRunning:Z

    iget v0, p0, Lcom/konka/hotkey/view/S3dItem;->mBgNorId:I

    invoke-virtual {p0, v0}, Lcom/konka/hotkey/view/S3dItem;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dItem;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextSize(F)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dItem;->mTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/hotkey/view/S3dItem;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f060002    # com.konka.hotkey.R.color.text_normal_col

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-virtual {p0, v3}, Lcom/konka/hotkey/view/S3dItem;->setClickable(Z)V

    invoke-virtual {p0, v3}, Lcom/konka/hotkey/view/S3dItem;->setFocusable(Z)V

    invoke-virtual {p0, v3}, Lcom/konka/hotkey/view/S3dItem;->setFocusableInTouchMode(Z)V

    return-void
.end method

.method public setPos(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/konka/hotkey/view/S3dItem;->pos:I

    return-void
.end method

.method public setRunBg(F)V
    .locals 3
    .param p1    # F

    iget v0, p0, Lcom/konka/hotkey/view/S3dItem;->mBgRunId:I

    invoke-virtual {p0, v0}, Lcom/konka/hotkey/view/S3dItem;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dItem;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextSize(F)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dItem;->mTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/hotkey/view/S3dItem;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f060001    # com.konka.hotkey.R.color.text_select_col

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    return-void
.end method

.method public setRunStatus(F)V
    .locals 4
    .param p1    # F

    const/4 v3, 0x0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/konka/hotkey/view/S3dItem;->isRunning:Z

    iget v0, p0, Lcom/konka/hotkey/view/S3dItem;->mBgRunId:I

    invoke-virtual {p0, v0}, Lcom/konka/hotkey/view/S3dItem;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dItem;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextSize(F)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dItem;->mTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/hotkey/view/S3dItem;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/high16 v2, 0x7f060000    # com.konka.hotkey.R.color.text_running_col

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-virtual {p0, v3}, Lcom/konka/hotkey/view/S3dItem;->setClickable(Z)V

    invoke-virtual {p0, v3}, Lcom/konka/hotkey/view/S3dItem;->setFocusable(Z)V

    invoke-virtual {p0, v3}, Lcom/konka/hotkey/view/S3dItem;->setFocusableInTouchMode(Z)V

    return-void
.end method

.method public setSelStatus(F)V
    .locals 4
    .param p1    # F

    const/4 v3, 0x1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/konka/hotkey/view/S3dItem;->isRunning:Z

    iget v0, p0, Lcom/konka/hotkey/view/S3dItem;->mBgSelId:I

    invoke-virtual {p0, v0}, Lcom/konka/hotkey/view/S3dItem;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dItem;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextSize(F)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dItem;->mTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/hotkey/view/S3dItem;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f060001    # com.konka.hotkey.R.color.text_select_col

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-virtual {p0, v3}, Lcom/konka/hotkey/view/S3dItem;->setClickable(Z)V

    invoke-virtual {p0, v3}, Lcom/konka/hotkey/view/S3dItem;->setFocusable(Z)V

    invoke-virtual {p0, v3}, Lcom/konka/hotkey/view/S3dItem;->setFocusableInTouchMode(Z)V

    return-void
.end method

.method public setStatus(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/konka/hotkey/view/S3dItem;->status:I

    return-void
.end method
