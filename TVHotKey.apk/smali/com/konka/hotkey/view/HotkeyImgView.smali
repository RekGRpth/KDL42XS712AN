.class public Lcom/konka/hotkey/view/HotkeyImgView;
.super Landroid/widget/RelativeLayout;
.source "HotkeyImgView.java"

# interfaces
.implements Lcom/konka/hotkey/IUpdateSysData;


# instance fields
.field private final STATUS_FBD:I

.field private final STATUS_NOR:I

.field private final STATUS_RUN:I

.field private imgNorId:I

.field private imgRunId:I

.field private imgSelId:I

.field private mContext:Landroid/content/Context;

.field public mTextView:Landroid/widget/TextView;

.field private mbIsRunning:Z

.field private miPos:I

.field public status:I


# direct methods
.method public constructor <init>(Landroid/content/Context;IILjava/lang/String;IIIIZZ)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # I
    .param p3    # I
    .param p4    # Ljava/lang/String;
    .param p5    # I
    .param p6    # I
    .param p7    # I
    .param p8    # I
    .param p9    # Z
    .param p10    # Z

    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    const/4 v3, 0x0

    iput v3, p0, Lcom/konka/hotkey/view/HotkeyImgView;->STATUS_NOR:I

    const/4 v3, 0x2

    iput v3, p0, Lcom/konka/hotkey/view/HotkeyImgView;->STATUS_RUN:I

    const/4 v3, 0x3

    iput v3, p0, Lcom/konka/hotkey/view/HotkeyImgView;->STATUS_FBD:I

    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/konka/hotkey/view/HotkeyImgView;->mbIsRunning:Z

    const/4 v3, 0x0

    iput v3, p0, Lcom/konka/hotkey/view/HotkeyImgView;->status:I

    iput-object p1, p0, Lcom/konka/hotkey/view/HotkeyImgView;->mContext:Landroid/content/Context;

    iput p7, p0, Lcom/konka/hotkey/view/HotkeyImgView;->imgNorId:I

    iput p6, p0, Lcom/konka/hotkey/view/HotkeyImgView;->imgSelId:I

    move/from16 v0, p8

    iput v0, p0, Lcom/konka/hotkey/view/HotkeyImgView;->imgRunId:I

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v3, -0x1

    invoke-direct {v1, p2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v1}, Lcom/konka/hotkey/view/HotkeyImgView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iput p5, p0, Lcom/konka/hotkey/view/HotkeyImgView;->miPos:I

    move/from16 v0, p9

    invoke-virtual {p0, v0}, Lcom/konka/hotkey/view/HotkeyImgView;->setFocusable(Z)V

    move/from16 v0, p10

    invoke-virtual {p0, v0}, Lcom/konka/hotkey/view/HotkeyImgView;->setFocusableInTouchMode(Z)V

    new-instance v3, Landroid/widget/TextView;

    invoke-direct {v3, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/konka/hotkey/view/HotkeyImgView;->mTextView:Landroid/widget/TextView;

    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v3, 0xa4

    const/4 v4, -0x2

    invoke-direct {v2, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v3, 0xc

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0xa

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    iget-object v3, p0, Lcom/konka/hotkey/view/HotkeyImgView;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v3, p0, Lcom/konka/hotkey/view/HotkeyImgView;->mTextView:Landroid/widget/TextView;

    const/16 v4, 0x11

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setGravity(I)V

    iget-object v3, p0, Lcom/konka/hotkey/view/HotkeyImgView;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v3, p4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/hotkey/view/HotkeyImgView;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->setSingleLine()V

    iget-object v3, p0, Lcom/konka/hotkey/view/HotkeyImgView;->mTextView:Landroid/widget/TextView;

    const/4 v4, 0x5

    const/4 v5, 0x0

    const/4 v6, 0x5

    const/4 v7, 0x0

    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/widget/TextView;->setPadding(IIII)V

    iget-object v3, p0, Lcom/konka/hotkey/view/HotkeyImgView;->mTextView:Landroid/widget/TextView;

    invoke-virtual {p0, v3}, Lcom/konka/hotkey/view/HotkeyImgView;->addView(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public doUpdate()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public getFocus(F)V
    .locals 0
    .param p1    # F

    invoke-virtual {p0, p1}, Lcom/konka/hotkey/view/HotkeyImgView;->setSelBg(F)V

    return-void
.end method

.method public getPos()I
    .locals 1

    iget v0, p0, Lcom/konka/hotkey/view/HotkeyImgView;->miPos:I

    return v0
.end method

.method public getStatus()I
    .locals 1

    iget v0, p0, Lcom/konka/hotkey/view/HotkeyImgView;->status:I

    return v0
.end method

.method public isRunning()Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/hotkey/view/HotkeyImgView;->mbIsRunning:Z

    return v0
.end method

.method public lostFocus(F)V
    .locals 2
    .param p1    # F

    const/4 v0, 0x2

    iget v1, p0, Lcom/konka/hotkey/view/HotkeyImgView;->status:I

    if-ne v0, v1, :cond_0

    invoke-virtual {p0, p1}, Lcom/konka/hotkey/view/HotkeyImgView;->setRunBg(F)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, p1}, Lcom/konka/hotkey/view/HotkeyImgView;->setNorBg(F)V

    goto :goto_0
.end method

.method public setNorBg(F)V
    .locals 3
    .param p1    # F

    iget v0, p0, Lcom/konka/hotkey/view/HotkeyImgView;->imgNorId:I

    invoke-virtual {p0, v0}, Lcom/konka/hotkey/view/HotkeyImgView;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/konka/hotkey/view/HotkeyImgView;->mTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/hotkey/view/HotkeyImgView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f060002    # com.konka.hotkey.R.color.text_normal_col

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/konka/hotkey/view/HotkeyImgView;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextSize(F)V

    return-void
.end method

.method public setRunBg(F)V
    .locals 3
    .param p1    # F

    iget v0, p0, Lcom/konka/hotkey/view/HotkeyImgView;->imgRunId:I

    invoke-virtual {p0, v0}, Lcom/konka/hotkey/view/HotkeyImgView;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/konka/hotkey/view/HotkeyImgView;->mTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/hotkey/view/HotkeyImgView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/high16 v2, 0x7f060000    # com.konka.hotkey.R.color.text_running_col

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/konka/hotkey/view/HotkeyImgView;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextSize(F)V

    return-void
.end method

.method public setRunning(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/konka/hotkey/view/HotkeyImgView;->mbIsRunning:Z

    return-void
.end method

.method public setSelBg(F)V
    .locals 3
    .param p1    # F

    iget v0, p0, Lcom/konka/hotkey/view/HotkeyImgView;->imgSelId:I

    invoke-virtual {p0, v0}, Lcom/konka/hotkey/view/HotkeyImgView;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/konka/hotkey/view/HotkeyImgView;->mTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/hotkey/view/HotkeyImgView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f060001    # com.konka.hotkey.R.color.text_select_col

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/konka/hotkey/view/HotkeyImgView;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextSize(F)V

    return-void
.end method

.method public setStatus(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/konka/hotkey/view/HotkeyImgView;->status:I

    return-void
.end method

.method public setTextSize(F)V
    .locals 1
    .param p1    # F

    iget-object v0, p0, Lcom/konka/hotkey/view/HotkeyImgView;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextSize(F)V

    return-void
.end method
