.class Lcom/konka/hotkey/view/S3dEnEffectItem$ItemKeyListener;
.super Ljava/lang/Object;
.source "S3dEnEffectItem.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/hotkey/view/S3dEnEffectItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ItemKeyListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;


# direct methods
.method constructor <init>(Lcom/konka/hotkey/view/S3dEnEffectItem;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemKeyListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 7
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # Landroid/view/KeyEvent;

    const v6, 0x7f0a0021    # com.konka.hotkey.R.id.hk_3d_en_offset_container

    const v5, 0x7f0a001b    # com.konka.hotkey.R.id.hk_3d_en_depth_container

    const v4, 0x7f0a0016    # com.konka.hotkey.R.id.hk_3d_en_effect_container

    const/16 v3, 0xa

    const/4 v2, 0x0

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_1

    const/16 v0, 0x15

    if-ne p2, v0, :cond_1

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    if-ne v0, v4, :cond_4

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemKeyListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    # getter for: Lcom/konka/hotkey/view/S3dEnEffectItem;->mEffectIndex:I
    invoke-static {v0}, Lcom/konka/hotkey/view/S3dEnEffectItem;->access$20(Lcom/konka/hotkey/view/S3dEnEffectItem;)I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-static {v0, v1}, Lcom/konka/hotkey/view/S3dEnEffectItem;->access$21(Lcom/konka/hotkey/view/S3dEnEffectItem;I)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemKeyListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    # getter for: Lcom/konka/hotkey/view/S3dEnEffectItem;->mEffectIndex:I
    invoke-static {v0}, Lcom/konka/hotkey/view/S3dEnEffectItem;->access$20(Lcom/konka/hotkey/view/S3dEnEffectItem;)I

    move-result v0

    if-gez v0, :cond_0

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemKeyListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    iget-object v1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemKeyListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    iget-object v1, v1, Lcom/konka/hotkey/view/S3dEnEffectItem;->mEffectStatus:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    invoke-static {v0, v1}, Lcom/konka/hotkey/view/S3dEnEffectItem;->access$21(Lcom/konka/hotkey/view/S3dEnEffectItem;I)V

    :cond_0
    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemKeyListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    iget-object v1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemKeyListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    # getter for: Lcom/konka/hotkey/view/S3dEnEffectItem;->mEffectIndex:I
    invoke-static {v1}, Lcom/konka/hotkey/view/S3dEnEffectItem;->access$20(Lcom/konka/hotkey/view/S3dEnEffectItem;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/konka/hotkey/view/S3dEnEffectItem;->setEffectIndex(I)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemKeyListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    iget-object v1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemKeyListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    # getter for: Lcom/konka/hotkey/view/S3dEnEffectItem;->mEffectIndex:I
    invoke-static {v1}, Lcom/konka/hotkey/view/S3dEnEffectItem;->access$20(Lcom/konka/hotkey/view/S3dEnEffectItem;)I

    move-result v1

    # invokes: Lcom/konka/hotkey/view/S3dEnEffectItem;->refreshArrowsImg(I)V
    invoke-static {v0, v1}, Lcom/konka/hotkey/view/S3dEnEffectItem;->access$22(Lcom/konka/hotkey/view/S3dEnEffectItem;I)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemKeyListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    iget-object v1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemKeyListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    # getter for: Lcom/konka/hotkey/view/S3dEnEffectItem;->mEffectIndex:I
    invoke-static {v1}, Lcom/konka/hotkey/view/S3dEnEffectItem;->access$20(Lcom/konka/hotkey/view/S3dEnEffectItem;)I

    move-result v1

    # invokes: Lcom/konka/hotkey/view/S3dEnEffectItem;->setOffsetDepthVisibility(I)V
    invoke-static {v0, v1}, Lcom/konka/hotkey/view/S3dEnEffectItem;->access$23(Lcom/konka/hotkey/view/S3dEnEffectItem;I)V

    :cond_1
    :goto_0
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_3

    const/16 v0, 0x16

    if-ne p2, v0, :cond_3

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    if-ne v0, v4, :cond_8

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemKeyListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    # getter for: Lcom/konka/hotkey/view/S3dEnEffectItem;->mEffectIndex:I
    invoke-static {v0}, Lcom/konka/hotkey/view/S3dEnEffectItem;->access$20(Lcom/konka/hotkey/view/S3dEnEffectItem;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {v0, v1}, Lcom/konka/hotkey/view/S3dEnEffectItem;->access$21(Lcom/konka/hotkey/view/S3dEnEffectItem;I)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemKeyListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    # getter for: Lcom/konka/hotkey/view/S3dEnEffectItem;->mEffectIndex:I
    invoke-static {v0}, Lcom/konka/hotkey/view/S3dEnEffectItem;->access$20(Lcom/konka/hotkey/view/S3dEnEffectItem;)I

    move-result v0

    iget-object v1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemKeyListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    iget-object v1, v1, Lcom/konka/hotkey/view/S3dEnEffectItem;->mEffectStatus:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-le v0, v1, :cond_2

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemKeyListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    invoke-static {v0, v2}, Lcom/konka/hotkey/view/S3dEnEffectItem;->access$21(Lcom/konka/hotkey/view/S3dEnEffectItem;I)V

    :cond_2
    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemKeyListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    iget-object v1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemKeyListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    # getter for: Lcom/konka/hotkey/view/S3dEnEffectItem;->mEffectIndex:I
    invoke-static {v1}, Lcom/konka/hotkey/view/S3dEnEffectItem;->access$20(Lcom/konka/hotkey/view/S3dEnEffectItem;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/konka/hotkey/view/S3dEnEffectItem;->setEffectIndex(I)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemKeyListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    iget-object v1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemKeyListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    # getter for: Lcom/konka/hotkey/view/S3dEnEffectItem;->mEffectIndex:I
    invoke-static {v1}, Lcom/konka/hotkey/view/S3dEnEffectItem;->access$20(Lcom/konka/hotkey/view/S3dEnEffectItem;)I

    move-result v1

    # invokes: Lcom/konka/hotkey/view/S3dEnEffectItem;->refreshArrowsImg(I)V
    invoke-static {v0, v1}, Lcom/konka/hotkey/view/S3dEnEffectItem;->access$22(Lcom/konka/hotkey/view/S3dEnEffectItem;I)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemKeyListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    iget-object v1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemKeyListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    # getter for: Lcom/konka/hotkey/view/S3dEnEffectItem;->mEffectIndex:I
    invoke-static {v1}, Lcom/konka/hotkey/view/S3dEnEffectItem;->access$20(Lcom/konka/hotkey/view/S3dEnEffectItem;)I

    move-result v1

    # invokes: Lcom/konka/hotkey/view/S3dEnEffectItem;->setOffsetDepthVisibility(I)V
    invoke-static {v0, v1}, Lcom/konka/hotkey/view/S3dEnEffectItem;->access$23(Lcom/konka/hotkey/view/S3dEnEffectItem;I)V

    :cond_3
    :goto_1
    return v2

    :cond_4
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    if-ne v0, v5, :cond_6

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemKeyListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    # getter for: Lcom/konka/hotkey/view/S3dEnEffectItem;->mDepthValue:I
    invoke-static {v0}, Lcom/konka/hotkey/view/S3dEnEffectItem;->access$4(Lcom/konka/hotkey/view/S3dEnEffectItem;)I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-static {v0, v1}, Lcom/konka/hotkey/view/S3dEnEffectItem;->access$2(Lcom/konka/hotkey/view/S3dEnEffectItem;I)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemKeyListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    # getter for: Lcom/konka/hotkey/view/S3dEnEffectItem;->mDepthValue:I
    invoke-static {v0}, Lcom/konka/hotkey/view/S3dEnEffectItem;->access$4(Lcom/konka/hotkey/view/S3dEnEffectItem;)I

    move-result v0

    if-gez v0, :cond_5

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemKeyListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    invoke-static {v0, v2}, Lcom/konka/hotkey/view/S3dEnEffectItem;->access$2(Lcom/konka/hotkey/view/S3dEnEffectItem;I)V

    :cond_5
    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemKeyListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    iget-object v1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemKeyListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    # getter for: Lcom/konka/hotkey/view/S3dEnEffectItem;->mDepthValue:I
    invoke-static {v1}, Lcom/konka/hotkey/view/S3dEnEffectItem;->access$4(Lcom/konka/hotkey/view/S3dEnEffectItem;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/konka/hotkey/view/S3dEnEffectItem;->setDepthValue(I)V

    goto :goto_0

    :cond_6
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    if-ne v0, v6, :cond_1

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemKeyListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    # getter for: Lcom/konka/hotkey/view/S3dEnEffectItem;->mOffsetValue:I
    invoke-static {v0}, Lcom/konka/hotkey/view/S3dEnEffectItem;->access$8(Lcom/konka/hotkey/view/S3dEnEffectItem;)I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-static {v0, v1}, Lcom/konka/hotkey/view/S3dEnEffectItem;->access$7(Lcom/konka/hotkey/view/S3dEnEffectItem;I)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemKeyListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    # getter for: Lcom/konka/hotkey/view/S3dEnEffectItem;->mOffsetValue:I
    invoke-static {v0}, Lcom/konka/hotkey/view/S3dEnEffectItem;->access$8(Lcom/konka/hotkey/view/S3dEnEffectItem;)I

    move-result v0

    if-gez v0, :cond_7

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemKeyListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    invoke-static {v0, v2}, Lcom/konka/hotkey/view/S3dEnEffectItem;->access$7(Lcom/konka/hotkey/view/S3dEnEffectItem;I)V

    :cond_7
    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemKeyListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    iget-object v1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemKeyListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    # getter for: Lcom/konka/hotkey/view/S3dEnEffectItem;->mOffsetValue:I
    invoke-static {v1}, Lcom/konka/hotkey/view/S3dEnEffectItem;->access$8(Lcom/konka/hotkey/view/S3dEnEffectItem;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/konka/hotkey/view/S3dEnEffectItem;->setOffsetValue(I)V

    goto/16 :goto_0

    :cond_8
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    if-ne v0, v5, :cond_a

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemKeyListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    # getter for: Lcom/konka/hotkey/view/S3dEnEffectItem;->mDepthValue:I
    invoke-static {v0}, Lcom/konka/hotkey/view/S3dEnEffectItem;->access$4(Lcom/konka/hotkey/view/S3dEnEffectItem;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {v0, v1}, Lcom/konka/hotkey/view/S3dEnEffectItem;->access$2(Lcom/konka/hotkey/view/S3dEnEffectItem;I)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemKeyListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    # getter for: Lcom/konka/hotkey/view/S3dEnEffectItem;->mDepthValue:I
    invoke-static {v0}, Lcom/konka/hotkey/view/S3dEnEffectItem;->access$4(Lcom/konka/hotkey/view/S3dEnEffectItem;)I

    move-result v0

    if-le v0, v3, :cond_9

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemKeyListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    invoke-static {v0, v3}, Lcom/konka/hotkey/view/S3dEnEffectItem;->access$2(Lcom/konka/hotkey/view/S3dEnEffectItem;I)V

    :cond_9
    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemKeyListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    iget-object v1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemKeyListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    # getter for: Lcom/konka/hotkey/view/S3dEnEffectItem;->mDepthValue:I
    invoke-static {v1}, Lcom/konka/hotkey/view/S3dEnEffectItem;->access$4(Lcom/konka/hotkey/view/S3dEnEffectItem;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/konka/hotkey/view/S3dEnEffectItem;->setDepthValue(I)V

    goto :goto_1

    :cond_a
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    if-ne v0, v6, :cond_3

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemKeyListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    # getter for: Lcom/konka/hotkey/view/S3dEnEffectItem;->mOffsetValue:I
    invoke-static {v0}, Lcom/konka/hotkey/view/S3dEnEffectItem;->access$8(Lcom/konka/hotkey/view/S3dEnEffectItem;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {v0, v1}, Lcom/konka/hotkey/view/S3dEnEffectItem;->access$7(Lcom/konka/hotkey/view/S3dEnEffectItem;I)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemKeyListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    # getter for: Lcom/konka/hotkey/view/S3dEnEffectItem;->mOffsetValue:I
    invoke-static {v0}, Lcom/konka/hotkey/view/S3dEnEffectItem;->access$8(Lcom/konka/hotkey/view/S3dEnEffectItem;)I

    move-result v0

    if-le v0, v3, :cond_b

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemKeyListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    invoke-static {v0, v3}, Lcom/konka/hotkey/view/S3dEnEffectItem;->access$7(Lcom/konka/hotkey/view/S3dEnEffectItem;I)V

    :cond_b
    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemKeyListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    iget-object v1, p0, Lcom/konka/hotkey/view/S3dEnEffectItem$ItemKeyListener;->this$0:Lcom/konka/hotkey/view/S3dEnEffectItem;

    # getter for: Lcom/konka/hotkey/view/S3dEnEffectItem;->mOffsetValue:I
    invoke-static {v1}, Lcom/konka/hotkey/view/S3dEnEffectItem;->access$8(Lcom/konka/hotkey/view/S3dEnEffectItem;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/konka/hotkey/view/S3dEnEffectItem;->setOffsetValue(I)V

    goto/16 :goto_1
.end method
