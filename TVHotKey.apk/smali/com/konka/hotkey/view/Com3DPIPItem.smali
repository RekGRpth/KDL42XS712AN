.class public Lcom/konka/hotkey/view/Com3DPIPItem;
.super Landroid/widget/RelativeLayout;
.source "Com3DPIPItem.java"

# interfaces
.implements Lcom/konka/hotkey/IUpdateSysData;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/hotkey/view/Com3DPIPItem$STATE;
    }
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$konka$hotkey$view$Com3DPIPItem$STATE:[I


# instance fields
.field private isEnabled:Z

.field private isInUse:Z

.field private mContext:Landroid/content/Context;

.field private mCurrState:Lcom/konka/hotkey/view/Com3DPIPItem$STATE;

.field private m_fNameSizeCurrFocus:F

.field private m_fNameSizeCurrInUse:F

.field private m_fNameSizeForbidden:F

.field private m_fNameSizeNormal:F

.field private m_iImgResCurrFocus:I

.field private m_iImgResCurrInUse:I

.field private m_iImgResForbidden:I

.field private m_iImgResNormal:I

.field private m_iNameColorCurrFocus:I

.field private m_iNameColorCurrInUse:I

.field private m_iNameColorForbidden:I

.field private m_iNameColorNormal:I

.field private m_strNameCurrFocus:Ljava/lang/String;

.field private m_strNameCurrInUse:Ljava/lang/String;

.field private m_strNameForbidden:Ljava/lang/String;

.field private m_strNameNormal:Ljava/lang/String;

.field private m_tvName:Landroid/widget/TextView;


# direct methods
.method static synthetic $SWITCH_TABLE$com$konka$hotkey$view$Com3DPIPItem$STATE()[I
    .locals 3

    sget-object v0, Lcom/konka/hotkey/view/Com3DPIPItem;->$SWITCH_TABLE$com$konka$hotkey$view$Com3DPIPItem$STATE:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/konka/hotkey/view/Com3DPIPItem$STATE;->values()[Lcom/konka/hotkey/view/Com3DPIPItem$STATE;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/konka/hotkey/view/Com3DPIPItem$STATE;->CURR_FOCUS:Lcom/konka/hotkey/view/Com3DPIPItem$STATE;

    invoke-virtual {v1}, Lcom/konka/hotkey/view/Com3DPIPItem$STATE;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_3

    :goto_1
    :try_start_1
    sget-object v1, Lcom/konka/hotkey/view/Com3DPIPItem$STATE;->CURR_INUSE:Lcom/konka/hotkey/view/Com3DPIPItem$STATE;

    invoke-virtual {v1}, Lcom/konka/hotkey/view/Com3DPIPItem$STATE;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_2

    :goto_2
    :try_start_2
    sget-object v1, Lcom/konka/hotkey/view/Com3DPIPItem$STATE;->FORBIDDEN:Lcom/konka/hotkey/view/Com3DPIPItem$STATE;

    invoke-virtual {v1}, Lcom/konka/hotkey/view/Com3DPIPItem$STATE;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_1

    :goto_3
    :try_start_3
    sget-object v1, Lcom/konka/hotkey/view/Com3DPIPItem$STATE;->NORMAL:Lcom/konka/hotkey/view/Com3DPIPItem$STATE;

    invoke-virtual {v1}, Lcom/konka/hotkey/view/Com3DPIPItem$STATE;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_0

    :goto_4
    sput-object v0, Lcom/konka/hotkey/view/Com3DPIPItem;->$SWITCH_TABLE$com$konka$hotkey$view$Com3DPIPItem$STATE:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_4

    :catch_1
    move-exception v1

    goto :goto_3

    :catch_2
    move-exception v1

    goto :goto_2

    :catch_3
    move-exception v1

    goto :goto_1
.end method

.method public constructor <init>(Landroid/content/Context;IIILjava/lang/String;FI)V
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # Ljava/lang/String;
    .param p6    # F
    .param p7    # I

    const/4 v5, 0x5

    const/4 v4, -0x1

    const/4 v3, 0x0

    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    iput-boolean v3, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->isInUse:Z

    iput-object p1, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->mContext:Landroid/content/Context;

    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, p2, p3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p0, p7}, Lcom/konka/hotkey/view/Com3DPIPItem;->setGravity(I)V

    new-instance v2, Landroid/widget/TextView;

    invoke-direct {v2, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_tvName:Landroid/widget/TextView;

    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v2, -0x2

    invoke-direct {v1, v4, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v2, 0xc

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    const/16 v2, 0xa

    invoke-virtual {v1, v3, v3, v3, v2}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    iget-object v2, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_tvName:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v2, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_tvName:Landroid/widget/TextView;

    invoke-virtual {v2, p7}, Landroid/widget/TextView;->setGravity(I)V

    iget-object v2, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_tvName:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->setSingleLine()V

    iget-object v2, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_tvName:Landroid/widget/TextView;

    invoke-virtual {v2, v5, v3, v5, v3}, Landroid/widget/TextView;->setPadding(IIII)V

    iput p4, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_iImgResCurrFocus:I

    iput p4, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_iImgResCurrInUse:I

    iput p4, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_iImgResForbidden:I

    iput p4, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_iImgResNormal:I

    iget v2, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_iImgResNormal:I

    invoke-virtual {p0, v2}, Lcom/konka/hotkey/view/Com3DPIPItem;->setBackgroundResource(I)V

    iput-object p5, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_strNameCurrFocus:Ljava/lang/String;

    iput-object p5, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_strNameCurrInUse:Ljava/lang/String;

    iput-object p5, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_strNameForbidden:Ljava/lang/String;

    iput-object p5, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_strNameNormal:Ljava/lang/String;

    iget-object v2, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_tvName:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_strNameNormal:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iput p6, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_fNameSizeCurrFocus:F

    iput p6, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_fNameSizeCurrInUse:F

    iput p6, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_fNameSizeForbidden:F

    iput p6, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_fNameSizeNormal:F

    iget-object v2, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_tvName:Landroid/widget/TextView;

    iget v3, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_fNameSizeNormal:F

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextSize(F)V

    iput v4, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_iNameColorCurrFocus:I

    const v2, -0x669a00

    iput v2, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_iNameColorCurrInUse:I

    const v2, -0xcdcdce

    iput v2, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_iNameColorForbidden:I

    iput v4, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_iNameColorNormal:I

    iget-object v2, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_tvName:Landroid/widget/TextView;

    iget v3, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_iNameColorNormal:I

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v2, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_tvName:Landroid/widget/TextView;

    invoke-virtual {p0, v2}, Lcom/konka/hotkey/view/Com3DPIPItem;->addView(Landroid/view/View;)V

    invoke-virtual {p0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateNormal()V

    invoke-virtual {p0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setListeners()V

    return-void
.end method

.method static synthetic access$1(Lcom/konka/hotkey/view/Com3DPIPItem;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/hotkey/view/Com3DPIPItem;->gainFocus()V

    return-void
.end method

.method static synthetic access$2(Lcom/konka/hotkey/view/Com3DPIPItem;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/hotkey/view/Com3DPIPItem;->loseFocus()V

    return-void
.end method

.method private gainFocus()V
    .locals 0

    invoke-virtual {p0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateFocus()V

    return-void
.end method

.method private loseFocus()V
    .locals 1

    iget-boolean v0, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->isInUse:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateInUse()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->isInUse:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->isEnabled:Z

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateNormal()V

    goto :goto_0

    :cond_2
    iget-boolean v0, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->isInUse:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->isEnabled:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateForbidden()V

    goto :goto_0
.end method


# virtual methods
.method public doReturn()V
    .locals 0

    return-void
.end method

.method public doUpdate()Z
    .locals 1

    invoke-virtual {p0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateFocus()V

    const/4 v0, 0x1

    return v0
.end method

.method public getItemState()Lcom/konka/hotkey/view/Com3DPIPItem$STATE;
    .locals 1

    iget-object v0, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->mCurrState:Lcom/konka/hotkey/view/Com3DPIPItem$STATE;

    return-object v0
.end method

.method public getString(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->mContext:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    return-void
.end method

.method public isItemEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->isEnabled:Z

    return v0
.end method

.method public isItemInUse()Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->isInUse:Z

    return v0
.end method

.method public onItemClick()Z
    .locals 3

    invoke-virtual {p0}, Lcom/konka/hotkey/view/Com3DPIPItem;->getTag()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v1, "===>onItemClick: "

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/konka/hotkey/view/Com3DPIPItem;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/konka/hotkey/view/Com3DPIPItem;->doUpdate()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemStateInUse()V

    :cond_1
    return v0
.end method

.method public setImgResOfAllStates(IIII)V
    .locals 0
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    iput p1, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_iImgResCurrFocus:I

    iput p2, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_iImgResCurrInUse:I

    iput p3, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_iImgResForbidden:I

    iput p4, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_iImgResNormal:I

    return-void
.end method

.method public setImgResOfState(Lcom/konka/hotkey/view/Com3DPIPItem$STATE;I)V
    .locals 2
    .param p1    # Lcom/konka/hotkey/view/Com3DPIPItem$STATE;
    .param p2    # I

    invoke-static {}, Lcom/konka/hotkey/view/Com3DPIPItem;->$SWITCH_TABLE$com$konka$hotkey$view$Com3DPIPItem$STATE()[I

    move-result-object v0

    invoke-virtual {p1}, Lcom/konka/hotkey/view/Com3DPIPItem$STATE;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iput p2, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_iImgResCurrFocus:I

    goto :goto_0

    :pswitch_1
    iput p2, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_iImgResCurrInUse:I

    goto :goto_0

    :pswitch_2
    iput p2, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_iImgResForbidden:I

    goto :goto_0

    :pswitch_3
    iput p2, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_iImgResNormal:I

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public setItemEnabled(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->isEnabled:Z

    invoke-virtual {p0, p1}, Lcom/konka/hotkey/view/Com3DPIPItem;->setEnabled(Z)V

    invoke-virtual {p0, p1}, Lcom/konka/hotkey/view/Com3DPIPItem;->setClickable(Z)V

    invoke-virtual {p0, p1}, Lcom/konka/hotkey/view/Com3DPIPItem;->setFocusable(Z)V

    invoke-virtual {p0, p1}, Lcom/konka/hotkey/view/Com3DPIPItem;->setFocusableInTouchMode(Z)V

    return-void
.end method

.method public setItemInUse(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->isInUse:Z

    return-void
.end method

.method public setItemStateFocus()V
    .locals 2

    sget-object v0, Lcom/konka/hotkey/view/Com3DPIPItem$STATE;->CURR_FOCUS:Lcom/konka/hotkey/view/Com3DPIPItem$STATE;

    iput-object v0, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->mCurrState:Lcom/konka/hotkey/view/Com3DPIPItem$STATE;

    iget-boolean v0, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->isEnabled:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemEnabled(Z)V

    :cond_0
    iget-boolean v0, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->isInUse:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemInUse(Z)V

    :cond_1
    iget v0, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_iImgResCurrFocus:I

    invoke-virtual {p0, v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_tvName:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_strNameCurrFocus:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_tvName:Landroid/widget/TextView;

    iget v1, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_fNameSizeCurrFocus:F

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextSize(F)V

    iget-object v0, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_tvName:Landroid/widget/TextView;

    iget v1, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_iNameColorCurrFocus:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    return-void
.end method

.method public setItemStateForbidden()V
    .locals 2

    const/4 v1, 0x0

    sget-object v0, Lcom/konka/hotkey/view/Com3DPIPItem$STATE;->FORBIDDEN:Lcom/konka/hotkey/view/Com3DPIPItem$STATE;

    iput-object v0, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->mCurrState:Lcom/konka/hotkey/view/Com3DPIPItem$STATE;

    iget-boolean v0, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->isEnabled:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0, v1}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemEnabled(Z)V

    :cond_0
    iget-boolean v0, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->isInUse:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0, v1}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemInUse(Z)V

    :cond_1
    iget v0, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_iImgResForbidden:I

    invoke-virtual {p0, v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_tvName:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_strNameForbidden:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_tvName:Landroid/widget/TextView;

    iget v1, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_fNameSizeForbidden:F

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextSize(F)V

    iget-object v0, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_tvName:Landroid/widget/TextView;

    iget v1, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_iNameColorForbidden:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    return-void
.end method

.method public setItemStateInUse()V
    .locals 2

    sget-object v0, Lcom/konka/hotkey/view/Com3DPIPItem$STATE;->CURR_INUSE:Lcom/konka/hotkey/view/Com3DPIPItem$STATE;

    iput-object v0, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->mCurrState:Lcom/konka/hotkey/view/Com3DPIPItem$STATE;

    iget-boolean v0, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->isEnabled:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemEnabled(Z)V

    :cond_0
    iget-boolean v0, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->isInUse:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemInUse(Z)V

    :cond_1
    iget v0, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_iImgResCurrInUse:I

    invoke-virtual {p0, v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_tvName:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_strNameCurrInUse:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_tvName:Landroid/widget/TextView;

    iget v1, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_fNameSizeCurrInUse:F

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextSize(F)V

    iget-object v0, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_tvName:Landroid/widget/TextView;

    iget v1, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_iNameColorCurrInUse:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    return-void
.end method

.method public setItemStateInUseWithoutDisabled()V
    .locals 2

    iget v0, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_iImgResCurrInUse:I

    invoke-virtual {p0, v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_tvName:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_strNameCurrInUse:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_tvName:Landroid/widget/TextView;

    iget v1, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_fNameSizeCurrInUse:F

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextSize(F)V

    iget-object v0, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_tvName:Landroid/widget/TextView;

    iget v1, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_iNameColorCurrInUse:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    return-void
.end method

.method public setItemStateNormal()V
    .locals 2

    sget-object v0, Lcom/konka/hotkey/view/Com3DPIPItem$STATE;->NORMAL:Lcom/konka/hotkey/view/Com3DPIPItem$STATE;

    iput-object v0, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->mCurrState:Lcom/konka/hotkey/view/Com3DPIPItem$STATE;

    iget-boolean v0, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->isEnabled:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemEnabled(Z)V

    :cond_0
    iget-boolean v0, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->isInUse:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setItemInUse(Z)V

    :cond_1
    iget v0, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_iImgResNormal:I

    invoke-virtual {p0, v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_tvName:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_strNameNormal:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_tvName:Landroid/widget/TextView;

    iget v1, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_fNameSizeNormal:F

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextSize(F)V

    iget-object v0, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_tvName:Landroid/widget/TextView;

    iget v1, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_iNameColorNormal:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    return-void
.end method

.method public setListeners()V
    .locals 1

    new-instance v0, Lcom/konka/hotkey/view/Com3DPIPItem$1;

    invoke-direct {v0, p0}, Lcom/konka/hotkey/view/Com3DPIPItem$1;-><init>(Lcom/konka/hotkey/view/Com3DPIPItem;)V

    invoke-virtual {p0, v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    new-instance v0, Lcom/konka/hotkey/view/Com3DPIPItem$2;

    invoke-direct {v0, p0}, Lcom/konka/hotkey/view/Com3DPIPItem$2;-><init>(Lcom/konka/hotkey/view/Com3DPIPItem;)V

    invoke-virtual {p0, v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    new-instance v0, Lcom/konka/hotkey/view/Com3DPIPItem$3;

    invoke-direct {v0, p0}, Lcom/konka/hotkey/view/Com3DPIPItem$3;-><init>(Lcom/konka/hotkey/view/Com3DPIPItem;)V

    invoke-virtual {p0, v0}, Lcom/konka/hotkey/view/Com3DPIPItem;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public setNameColorOfAllStates(IIII)V
    .locals 0
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    iput p1, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_iNameColorCurrFocus:I

    iput p2, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_iNameColorCurrInUse:I

    iput p3, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_iNameColorForbidden:I

    iput p4, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_iNameColorNormal:I

    return-void
.end method

.method public setNameColorOfState(Lcom/konka/hotkey/view/Com3DPIPItem$STATE;I)V
    .locals 2
    .param p1    # Lcom/konka/hotkey/view/Com3DPIPItem$STATE;
    .param p2    # I

    invoke-static {}, Lcom/konka/hotkey/view/Com3DPIPItem;->$SWITCH_TABLE$com$konka$hotkey$view$Com3DPIPItem$STATE()[I

    move-result-object v0

    invoke-virtual {p1}, Lcom/konka/hotkey/view/Com3DPIPItem$STATE;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iput p2, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_iNameColorCurrFocus:I

    goto :goto_0

    :pswitch_1
    iput p2, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_iNameColorCurrInUse:I

    goto :goto_0

    :pswitch_2
    iput p2, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_iNameColorForbidden:I

    goto :goto_0

    :pswitch_3
    iput p2, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_iNameColorNormal:I

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public setNameOfAllStates(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_strNameCurrFocus:Ljava/lang/String;

    iput-object p2, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_strNameCurrInUse:Ljava/lang/String;

    iput-object p3, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_strNameForbidden:Ljava/lang/String;

    iput-object p4, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_strNameNormal:Ljava/lang/String;

    return-void
.end method

.method public setNameOfState(Lcom/konka/hotkey/view/Com3DPIPItem$STATE;Ljava/lang/String;)V
    .locals 2
    .param p1    # Lcom/konka/hotkey/view/Com3DPIPItem$STATE;
    .param p2    # Ljava/lang/String;

    invoke-static {}, Lcom/konka/hotkey/view/Com3DPIPItem;->$SWITCH_TABLE$com$konka$hotkey$view$Com3DPIPItem$STATE()[I

    move-result-object v0

    invoke-virtual {p1}, Lcom/konka/hotkey/view/Com3DPIPItem$STATE;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iput-object p2, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_strNameCurrFocus:Ljava/lang/String;

    goto :goto_0

    :pswitch_1
    iput-object p2, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_strNameCurrInUse:Ljava/lang/String;

    goto :goto_0

    :pswitch_2
    iput-object p2, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_strNameForbidden:Ljava/lang/String;

    goto :goto_0

    :pswitch_3
    iput-object p2, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_strNameNormal:Ljava/lang/String;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public setNameSizeOfAllStates(FFFF)V
    .locals 0
    .param p1    # F
    .param p2    # F
    .param p3    # F
    .param p4    # F

    iput p1, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_fNameSizeCurrFocus:F

    iput p2, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_fNameSizeCurrInUse:F

    iput p3, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_fNameSizeForbidden:F

    iput p4, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_fNameSizeNormal:F

    return-void
.end method

.method public setNameSizeOfState(Lcom/konka/hotkey/view/Com3DPIPItem$STATE;F)V
    .locals 2
    .param p1    # Lcom/konka/hotkey/view/Com3DPIPItem$STATE;
    .param p2    # F

    invoke-static {}, Lcom/konka/hotkey/view/Com3DPIPItem;->$SWITCH_TABLE$com$konka$hotkey$view$Com3DPIPItem$STATE()[I

    move-result-object v0

    invoke-virtual {p1}, Lcom/konka/hotkey/view/Com3DPIPItem$STATE;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iput p2, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_fNameSizeCurrFocus:F

    goto :goto_0

    :pswitch_1
    iput p2, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_fNameSizeCurrInUse:F

    goto :goto_0

    :pswitch_2
    iput p2, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_fNameSizeForbidden:F

    goto :goto_0

    :pswitch_3
    iput p2, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_fNameSizeNormal:F

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public setText(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_tvName:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    return-void
.end method

.method public setText(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;

    iget-object v0, p0, Lcom/konka/hotkey/view/Com3DPIPItem;->m_tvName:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
