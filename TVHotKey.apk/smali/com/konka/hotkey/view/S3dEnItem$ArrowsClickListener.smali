.class Lcom/konka/hotkey/view/S3dEnItem$ArrowsClickListener;
.super Ljava/lang/Object;
.source "S3dEnItem.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/hotkey/view/S3dEnItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ArrowsClickListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/hotkey/view/S3dEnItem;


# direct methods
.method constructor <init>(Lcom/konka/hotkey/view/S3dEnItem;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/hotkey/view/S3dEnItem$ArrowsClickListener;->this$0:Lcom/konka/hotkey/view/S3dEnItem;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    iget-object v1, p0, Lcom/konka/hotkey/view/S3dEnItem$ArrowsClickListener;->this$0:Lcom/konka/hotkey/view/S3dEnItem;

    # getter for: Lcom/konka/hotkey/view/S3dEnItem;->mArrowsNext:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/konka/hotkey/view/S3dEnItem;->access$4(Lcom/konka/hotkey/view/S3dEnItem;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ImageView;->getId()I

    move-result v1

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnItem$ArrowsClickListener;->this$0:Lcom/konka/hotkey/view/S3dEnItem;

    # getter for: Lcom/konka/hotkey/view/S3dEnItem;->mValueIndex:I
    invoke-static {v0}, Lcom/konka/hotkey/view/S3dEnItem;->access$5(Lcom/konka/hotkey/view/S3dEnItem;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {v0, v1}, Lcom/konka/hotkey/view/S3dEnItem;->access$6(Lcom/konka/hotkey/view/S3dEnItem;I)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnItem$ArrowsClickListener;->this$0:Lcom/konka/hotkey/view/S3dEnItem;

    # getter for: Lcom/konka/hotkey/view/S3dEnItem;->mValueIndex:I
    invoke-static {v0}, Lcom/konka/hotkey/view/S3dEnItem;->access$5(Lcom/konka/hotkey/view/S3dEnItem;)I

    move-result v0

    iget-object v1, p0, Lcom/konka/hotkey/view/S3dEnItem$ArrowsClickListener;->this$0:Lcom/konka/hotkey/view/S3dEnItem;

    iget-object v1, v1, Lcom/konka/hotkey/view/S3dEnItem;->mStatus:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-le v0, v1, :cond_0

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnItem$ArrowsClickListener;->this$0:Lcom/konka/hotkey/view/S3dEnItem;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/konka/hotkey/view/S3dEnItem;->access$6(Lcom/konka/hotkey/view/S3dEnItem;I)V

    :cond_0
    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnItem$ArrowsClickListener;->this$0:Lcom/konka/hotkey/view/S3dEnItem;

    iget-object v1, p0, Lcom/konka/hotkey/view/S3dEnItem$ArrowsClickListener;->this$0:Lcom/konka/hotkey/view/S3dEnItem;

    # getter for: Lcom/konka/hotkey/view/S3dEnItem;->mValueIndex:I
    invoke-static {v1}, Lcom/konka/hotkey/view/S3dEnItem;->access$5(Lcom/konka/hotkey/view/S3dEnItem;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/konka/hotkey/view/S3dEnItem;->setValueIndex(I)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnItem$ArrowsClickListener;->this$0:Lcom/konka/hotkey/view/S3dEnItem;

    iget-object v1, p0, Lcom/konka/hotkey/view/S3dEnItem$ArrowsClickListener;->this$0:Lcom/konka/hotkey/view/S3dEnItem;

    # getter for: Lcom/konka/hotkey/view/S3dEnItem;->mValueIndex:I
    invoke-static {v1}, Lcom/konka/hotkey/view/S3dEnItem;->access$5(Lcom/konka/hotkey/view/S3dEnItem;)I

    move-result v1

    # invokes: Lcom/konka/hotkey/view/S3dEnItem;->refreshArrowsImg(I)V
    invoke-static {v0, v1}, Lcom/konka/hotkey/view/S3dEnItem;->access$7(Lcom/konka/hotkey/view/S3dEnItem;I)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnItem$ArrowsClickListener;->this$0:Lcom/konka/hotkey/view/S3dEnItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/S3dEnItem;->doUpdate()Z

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    iget-object v1, p0, Lcom/konka/hotkey/view/S3dEnItem$ArrowsClickListener;->this$0:Lcom/konka/hotkey/view/S3dEnItem;

    # getter for: Lcom/konka/hotkey/view/S3dEnItem;->mArrowsPre:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/konka/hotkey/view/S3dEnItem;->access$8(Lcom/konka/hotkey/view/S3dEnItem;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ImageView;->getId()I

    move-result v1

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnItem$ArrowsClickListener;->this$0:Lcom/konka/hotkey/view/S3dEnItem;

    # getter for: Lcom/konka/hotkey/view/S3dEnItem;->mValueIndex:I
    invoke-static {v0}, Lcom/konka/hotkey/view/S3dEnItem;->access$5(Lcom/konka/hotkey/view/S3dEnItem;)I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-static {v0, v1}, Lcom/konka/hotkey/view/S3dEnItem;->access$6(Lcom/konka/hotkey/view/S3dEnItem;I)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnItem$ArrowsClickListener;->this$0:Lcom/konka/hotkey/view/S3dEnItem;

    # getter for: Lcom/konka/hotkey/view/S3dEnItem;->mValueIndex:I
    invoke-static {v0}, Lcom/konka/hotkey/view/S3dEnItem;->access$5(Lcom/konka/hotkey/view/S3dEnItem;)I

    move-result v0

    if-gez v0, :cond_3

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnItem$ArrowsClickListener;->this$0:Lcom/konka/hotkey/view/S3dEnItem;

    iget-object v1, p0, Lcom/konka/hotkey/view/S3dEnItem$ArrowsClickListener;->this$0:Lcom/konka/hotkey/view/S3dEnItem;

    iget-object v1, v1, Lcom/konka/hotkey/view/S3dEnItem;->mStatus:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    invoke-static {v0, v1}, Lcom/konka/hotkey/view/S3dEnItem;->access$6(Lcom/konka/hotkey/view/S3dEnItem;I)V

    :cond_3
    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnItem$ArrowsClickListener;->this$0:Lcom/konka/hotkey/view/S3dEnItem;

    iget-object v1, p0, Lcom/konka/hotkey/view/S3dEnItem$ArrowsClickListener;->this$0:Lcom/konka/hotkey/view/S3dEnItem;

    # getter for: Lcom/konka/hotkey/view/S3dEnItem;->mValueIndex:I
    invoke-static {v1}, Lcom/konka/hotkey/view/S3dEnItem;->access$5(Lcom/konka/hotkey/view/S3dEnItem;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/konka/hotkey/view/S3dEnItem;->setValueIndex(I)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnItem$ArrowsClickListener;->this$0:Lcom/konka/hotkey/view/S3dEnItem;

    iget-object v1, p0, Lcom/konka/hotkey/view/S3dEnItem$ArrowsClickListener;->this$0:Lcom/konka/hotkey/view/S3dEnItem;

    # getter for: Lcom/konka/hotkey/view/S3dEnItem;->mValueIndex:I
    invoke-static {v1}, Lcom/konka/hotkey/view/S3dEnItem;->access$5(Lcom/konka/hotkey/view/S3dEnItem;)I

    move-result v1

    # invokes: Lcom/konka/hotkey/view/S3dEnItem;->refreshArrowsImg(I)V
    invoke-static {v0, v1}, Lcom/konka/hotkey/view/S3dEnItem;->access$7(Lcom/konka/hotkey/view/S3dEnItem;I)V

    iget-object v0, p0, Lcom/konka/hotkey/view/S3dEnItem$ArrowsClickListener;->this$0:Lcom/konka/hotkey/view/S3dEnItem;

    invoke-virtual {v0}, Lcom/konka/hotkey/view/S3dEnItem;->doUpdate()Z

    goto :goto_0
.end method
