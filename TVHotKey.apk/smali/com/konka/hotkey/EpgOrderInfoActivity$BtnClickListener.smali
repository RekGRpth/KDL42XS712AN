.class Lcom/konka/hotkey/EpgOrderInfoActivity$BtnClickListener;
.super Ljava/lang/Object;
.source "EpgOrderInfoActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/hotkey/EpgOrderInfoActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "BtnClickListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/hotkey/EpgOrderInfoActivity;


# direct methods
.method constructor <init>(Lcom/konka/hotkey/EpgOrderInfoActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/hotkey/EpgOrderInfoActivity$BtnClickListener;->this$0:Lcom/konka/hotkey/EpgOrderInfoActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0a000d    # com.konka.hotkey.R.id.hk_order_info_btn_ok

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/konka/hotkey/EpgOrderInfoActivity$BtnClickListener;->this$0:Lcom/konka/hotkey/EpgOrderInfoActivity;

    invoke-static {v0, v2}, Lcom/konka/hotkey/EpgOrderInfoActivity;->access$6(Lcom/konka/hotkey/EpgOrderInfoActivity;Z)V

    iget-object v0, p0, Lcom/konka/hotkey/EpgOrderInfoActivity$BtnClickListener;->this$0:Lcom/konka/hotkey/EpgOrderInfoActivity;

    # getter for: Lcom/konka/hotkey/EpgOrderInfoActivity;->myHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/konka/hotkey/EpgOrderInfoActivity;->access$7(Lcom/konka/hotkey/EpgOrderInfoActivity;)Landroid/os/Handler;

    move-result-object v0

    const/16 v1, 0x2886

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0a000e    # com.konka.hotkey.R.id.hk_order_info_btn_cancel

    if-ne v0, v1, :cond_1

    const-string v0, "Click Btn Cancel"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/hotkey/EpgOrderInfoActivity$BtnClickListener;->this$0:Lcom/konka/hotkey/EpgOrderInfoActivity;

    invoke-static {v0, v2}, Lcom/konka/hotkey/EpgOrderInfoActivity;->access$6(Lcom/konka/hotkey/EpgOrderInfoActivity;Z)V

    iget-object v0, p0, Lcom/konka/hotkey/EpgOrderInfoActivity$BtnClickListener;->this$0:Lcom/konka/hotkey/EpgOrderInfoActivity;

    # getter for: Lcom/konka/hotkey/EpgOrderInfoActivity;->myHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/konka/hotkey/EpgOrderInfoActivity;->access$7(Lcom/konka/hotkey/EpgOrderInfoActivity;)Landroid/os/Handler;

    move-result-object v0

    const/16 v1, 0x2887

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_1
    return-void
.end method
