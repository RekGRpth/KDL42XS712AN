.class public final enum Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;
.super Ljava/lang/Enum;
.source "HotkeyBroadcastReceiver.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/hotkey/HotkeyBroadcastReceiver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "UpgradeInfo"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;

.field public static final enum UPGRADE_DATA_OVER:Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;

.field public static final enum UPGRADE_NO_FILE:Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;

.field public static final enum UPGRADE_OK:Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;

.field public static final enum UPGRADE_READ_FILE_FAIL:Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;

.field public static final enum UPGRADE_RW_SPI_FAIL:Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;

    const-string v1, "UPGRADE_OK"

    invoke-direct {v0, v1, v2}, Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;->UPGRADE_OK:Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;

    new-instance v0, Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;

    const-string v1, "UPGRADE_DATA_OVER"

    invoke-direct {v0, v1, v3}, Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;->UPGRADE_DATA_OVER:Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;

    new-instance v0, Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;

    const-string v1, "UPGRADE_NO_FILE"

    invoke-direct {v0, v1, v4}, Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;->UPGRADE_NO_FILE:Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;

    new-instance v0, Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;

    const-string v1, "UPGRADE_READ_FILE_FAIL"

    invoke-direct {v0, v1, v5}, Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;->UPGRADE_READ_FILE_FAIL:Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;

    new-instance v0, Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;

    const-string v1, "UPGRADE_RW_SPI_FAIL"

    invoke-direct {v0, v1, v6}, Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;->UPGRADE_RW_SPI_FAIL:Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;

    sget-object v1, Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;->UPGRADE_OK:Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;

    aput-object v1, v0, v2

    sget-object v1, Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;->UPGRADE_DATA_OVER:Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;

    aput-object v1, v0, v3

    sget-object v1, Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;->UPGRADE_NO_FILE:Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;

    aput-object v1, v0, v4

    sget-object v1, Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;->UPGRADE_READ_FILE_FAIL:Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;

    aput-object v1, v0, v5

    sget-object v1, Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;->UPGRADE_RW_SPI_FAIL:Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;

    aput-object v1, v0, v6

    sput-object v0, Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;->ENUM$VALUES:[Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;
    .locals 1

    const-class v0, Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;

    return-object v0
.end method

.method public static values()[Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;->ENUM$VALUES:[Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;

    array-length v1, v0

    new-array v2, v1, [Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
