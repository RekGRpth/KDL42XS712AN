.class public Lcom/konka/hotkey/utils/ConfigurationData;
.super Ljava/lang/Object;
.source "ConfigurationData.java"


# static fields
.field public static final ATVInputSourceModes:[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

.field public static final AVInputSourceModes:[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

.field public static final DTVInputSourceModes:[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

.field public static final HDMIInputSourceModes:[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

.field public static final VGAInputSourceModes:[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

.field public static final YPbPrInputSourceModes:[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-array v0, v5, [Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    aput-object v1, v0, v4

    sput-object v0, Lcom/konka/hotkey/utils/ConfigurationData;->DTVInputSourceModes:[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    new-array v0, v4, [Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_ATV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    aput-object v1, v0, v3

    sput-object v0, Lcom/konka/hotkey/utils/ConfigurationData;->ATVInputSourceModes:[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS3:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS4:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    aput-object v1, v0, v6

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS5:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS6:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    aput-object v2, v0, v1

    sput-object v0, Lcom/konka/hotkey/utils/ConfigurationData;->AVInputSourceModes:[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    new-array v0, v4, [Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_VGA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    aput-object v1, v0, v3

    sput-object v0, Lcom/konka/hotkey/utils/ConfigurationData;->VGAInputSourceModes:[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    new-array v0, v6, [Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_YPBPR:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_YPBPR2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_YPBPR3:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    aput-object v1, v0, v5

    sput-object v0, Lcom/konka/hotkey/utils/ConfigurationData;->YPbPrInputSourceModes:[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    new-array v0, v7, [Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI3:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI4:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    aput-object v1, v0, v6

    sput-object v0, Lcom/konka/hotkey/utils/ConfigurationData;->HDMIInputSourceModes:[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
