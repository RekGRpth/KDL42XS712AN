.class Lcom/konka/hotkey/PictureActivity$PictureItem;
.super Lcom/konka/hotkey/view/HotkeyTextView;
.source "PictureActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/hotkey/PictureActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "PictureItem"
.end annotation


# instance fields
.field public mode:Lcom/mstar/android/tvapi/common/vo/EnumPictureMode;

.field final synthetic this$0:Lcom/konka/hotkey/PictureActivity;


# direct methods
.method public constructor <init>(Lcom/konka/hotkey/PictureActivity;Landroid/content/Context;IIIFILcom/mstar/android/tvapi/common/vo/EnumPictureMode;)V
    .locals 7
    .param p2    # Landroid/content/Context;
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # F
    .param p7    # I
    .param p8    # Lcom/mstar/android/tvapi/common/vo/EnumPictureMode;

    iput-object p1, p0, Lcom/konka/hotkey/PictureActivity$PictureItem;->this$0:Lcom/konka/hotkey/PictureActivity;

    move-object v0, p0

    move-object v1, p2

    move v2, p3

    move v3, p4

    move v4, p5

    move v5, p6

    move v6, p7

    invoke-direct/range {v0 .. v6}, Lcom/konka/hotkey/view/HotkeyTextView;-><init>(Landroid/content/Context;IIIFI)V

    iput-object p8, p0, Lcom/konka/hotkey/PictureActivity$PictureItem;->mode:Lcom/mstar/android/tvapi/common/vo/EnumPictureMode;

    return-void
.end method


# virtual methods
.method public doUpdate()Z
    .locals 3

    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/konka/hotkey/PictureActivity$PictureItem;->setRunning(Z)V

    iget-object v0, p0, Lcom/konka/hotkey/PictureActivity$PictureItem;->this$0:Lcom/konka/hotkey/PictureActivity;

    iget-object v0, v0, Lcom/konka/hotkey/PictureActivity;->pictureSkin:Lcom/mstar/android/tv/TvPictureManager;

    iget-object v1, p0, Lcom/konka/hotkey/PictureActivity$PictureItem;->mode:Lcom/mstar/android/tvapi/common/vo/EnumPictureMode;

    invoke-virtual {v0, v1}, Lcom/mstar/android/tv/TvPictureManager;->setPictureModeIdx(Lcom/mstar/android/tvapi/common/vo/EnumPictureMode;)Z

    iget-object v0, p0, Lcom/konka/hotkey/PictureActivity$PictureItem;->this$0:Lcom/konka/hotkey/PictureActivity;

    # getter for: Lcom/konka/hotkey/PictureActivity;->pictureDesk:Lcom/konka/kkinterface/tv/PictureDesk;
    invoke-static {v0}, Lcom/konka/hotkey/PictureActivity;->access$2(Lcom/konka/hotkey/PictureActivity;)Lcom/konka/kkinterface/tv/PictureDesk;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/PictureDesk;->UpdateVideoItem()V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "click: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/hotkey/PictureActivity$PictureItem;->mode:Lcom/mstar/android/tvapi/common/vo/EnumPictureMode;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    return v2
.end method
