.class Lcom/konka/hotkey/ZoomActivity$ZoomItem;
.super Lcom/konka/hotkey/view/HotkeyTextView;
.source "ZoomActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/hotkey/ZoomActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ZoomItem"
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$konka$kkinterface$tv$DataBaseDesk$MAPI_VIDEO_ARC_Type:[I


# instance fields
.field private Indexnumber:Lcom/konka/hotkey/getIndex;

.field public meMode:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

.field final synthetic this$0:Lcom/konka/hotkey/ZoomActivity;


# direct methods
.method static synthetic $SWITCH_TABLE$com$konka$kkinterface$tv$DataBaseDesk$MAPI_VIDEO_ARC_Type()[I
    .locals 3

    sget-object v0, Lcom/konka/hotkey/ZoomActivity$ZoomItem;->$SWITCH_TABLE$com$konka$kkinterface$tv$DataBaseDesk$MAPI_VIDEO_ARC_Type:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_14x9:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_16

    :goto_1
    :try_start_1
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_16x9:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_15

    :goto_2
    :try_start_2
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_16x9_Combind:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v1

    const/16 v2, 0x13

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_14

    :goto_3
    :try_start_3
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_16x9_PanScan:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v1

    const/16 v2, 0x11

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_13

    :goto_4
    :try_start_4
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_16x9_PillarBox:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v1

    const/16 v2, 0x10

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_12

    :goto_5
    :try_start_5
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_4x3:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_11

    :goto_6
    :try_start_6
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_4x3_Combind:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v1

    const/16 v2, 0x12

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_10

    :goto_7
    :try_start_7
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_4x3_LetterBox:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v1

    const/16 v2, 0xf

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_f

    :goto_8
    :try_start_8
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_4x3_PanScan:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v1

    const/16 v2, 0xe

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_e

    :goto_9
    :try_start_9
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_AUTO:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_d

    :goto_a
    :try_start_a
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_DEFAULT:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_c

    :goto_b
    :try_start_b
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_DotByDot:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_b

    :goto_c
    :try_start_c
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_JustScan:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_a

    :goto_d
    :try_start_d
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_MAX:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v1

    const/16 v2, 0x17

    aput v2, v0, v1
    :try_end_d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d .. :try_end_d} :catch_9

    :goto_e
    :try_start_e
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_Movie:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v1

    const/16 v2, 0xc

    aput v2, v0, v1
    :try_end_e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_e .. :try_end_e} :catch_8

    :goto_f
    :try_start_f
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_Panorama:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_f .. :try_end_f} :catch_7

    :goto_10
    :try_start_10
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_Personal:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v1

    const/16 v2, 0xd

    aput v2, v0, v1
    :try_end_10
    .catch Ljava/lang/NoSuchFieldError; {:try_start_10 .. :try_end_10} :catch_6

    :goto_11
    :try_start_11
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_Subtitle:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v1

    const/16 v2, 0xb

    aput v2, v0, v1
    :try_end_11
    .catch Ljava/lang/NoSuchFieldError; {:try_start_11 .. :try_end_11} :catch_5

    :goto_12
    :try_start_12
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_Zoom1:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_12
    .catch Ljava/lang/NoSuchFieldError; {:try_start_12 .. :try_end_12} :catch_4

    :goto_13
    :try_start_13
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_Zoom2:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_13
    .catch Ljava/lang/NoSuchFieldError; {:try_start_13 .. :try_end_13} :catch_3

    :goto_14
    :try_start_14
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_Zoom_2x:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v1

    const/16 v2, 0x14

    aput v2, v0, v1
    :try_end_14
    .catch Ljava/lang/NoSuchFieldError; {:try_start_14 .. :try_end_14} :catch_2

    :goto_15
    :try_start_15
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_Zoom_3x:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v1

    const/16 v2, 0x15

    aput v2, v0, v1
    :try_end_15
    .catch Ljava/lang/NoSuchFieldError; {:try_start_15 .. :try_end_15} :catch_1

    :goto_16
    :try_start_16
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_Zoom_4x:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v1

    const/16 v2, 0x16

    aput v2, v0, v1
    :try_end_16
    .catch Ljava/lang/NoSuchFieldError; {:try_start_16 .. :try_end_16} :catch_0

    :goto_17
    sput-object v0, Lcom/konka/hotkey/ZoomActivity$ZoomItem;->$SWITCH_TABLE$com$konka$kkinterface$tv$DataBaseDesk$MAPI_VIDEO_ARC_Type:[I

    goto/16 :goto_0

    :catch_0
    move-exception v1

    goto :goto_17

    :catch_1
    move-exception v1

    goto :goto_16

    :catch_2
    move-exception v1

    goto :goto_15

    :catch_3
    move-exception v1

    goto :goto_14

    :catch_4
    move-exception v1

    goto :goto_13

    :catch_5
    move-exception v1

    goto :goto_12

    :catch_6
    move-exception v1

    goto :goto_11

    :catch_7
    move-exception v1

    goto :goto_10

    :catch_8
    move-exception v1

    goto :goto_f

    :catch_9
    move-exception v1

    goto :goto_e

    :catch_a
    move-exception v1

    goto :goto_d

    :catch_b
    move-exception v1

    goto/16 :goto_c

    :catch_c
    move-exception v1

    goto/16 :goto_b

    :catch_d
    move-exception v1

    goto/16 :goto_a

    :catch_e
    move-exception v1

    goto/16 :goto_9

    :catch_f
    move-exception v1

    goto/16 :goto_8

    :catch_10
    move-exception v1

    goto/16 :goto_7

    :catch_11
    move-exception v1

    goto/16 :goto_6

    :catch_12
    move-exception v1

    goto/16 :goto_5

    :catch_13
    move-exception v1

    goto/16 :goto_4

    :catch_14
    move-exception v1

    goto/16 :goto_3

    :catch_15
    move-exception v1

    goto/16 :goto_2

    :catch_16
    move-exception v1

    goto/16 :goto_1
.end method

.method public constructor <init>(Lcom/konka/hotkey/ZoomActivity;Landroid/content/Context;IIIFILcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;)V
    .locals 9
    .param p2    # Landroid/content/Context;
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # F
    .param p7    # I
    .param p8    # Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    iput-object p1, p0, Lcom/konka/hotkey/ZoomActivity$ZoomItem;->this$0:Lcom/konka/hotkey/ZoomActivity;

    move-object v1, p0

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    move/from16 v7, p7

    invoke-direct/range {v1 .. v7}, Lcom/konka/hotkey/view/HotkeyTextView;-><init>(Landroid/content/Context;IIIFI)V

    new-instance v1, Lcom/konka/hotkey/getIndex;

    invoke-direct {v1}, Lcom/konka/hotkey/getIndex;-><init>()V

    iput-object v1, p0, Lcom/konka/hotkey/ZoomActivity$ZoomItem;->Indexnumber:Lcom/konka/hotkey/getIndex;

    move-object/from16 v0, p8

    iput-object v0, p0, Lcom/konka/hotkey/ZoomActivity$ZoomItem;->meMode:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    const/4 v8, 0x0

    invoke-static {}, Lcom/konka/hotkey/ZoomActivity$ZoomItem;->$SWITCH_TABLE$com$konka$kkinterface$tv$DataBaseDesk$MAPI_VIDEO_ARC_Type()[I

    move-result-object v1

    iget-object v2, p0, Lcom/konka/hotkey/ZoomActivity$ZoomItem;->meMode:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    const/4 v8, 0x0

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/konka/hotkey/ZoomActivity$ZoomItem;->Indexnumber:Lcom/konka/hotkey/getIndex;

    invoke-virtual {v1, v8}, Lcom/konka/hotkey/getIndex;->setIndex1(I)I

    return-void

    :pswitch_1
    const/4 v8, 0x0

    goto :goto_0

    :pswitch_2
    const/4 v8, 0x1

    goto :goto_0

    :pswitch_3
    const/4 v8, 0x2

    goto :goto_0

    :pswitch_4
    # getter for: Lcom/konka/hotkey/ZoomActivity;->mInputMode:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    invoke-static {p1}, Lcom/konka/hotkey/ZoomActivity;->access$5(Lcom/konka/hotkey/ZoomActivity;)Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v1

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_VGA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-ne v1, v2, :cond_1

    const/4 v8, 0x2

    goto :goto_0

    :cond_1
    # getter for: Lcom/konka/hotkey/ZoomActivity;->mInputMode:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    invoke-static {p1}, Lcom/konka/hotkey/ZoomActivity;->access$5(Lcom/konka/hotkey/ZoomActivity;)Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v1

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-eq v1, v2, :cond_2

    # getter for: Lcom/konka/hotkey/ZoomActivity;->mInputMode:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    invoke-static {p1}, Lcom/konka/hotkey/ZoomActivity;->access$5(Lcom/konka/hotkey/ZoomActivity;)Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v1

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-eq v1, v2, :cond_2

    # getter for: Lcom/konka/hotkey/ZoomActivity;->mInputMode:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    invoke-static {p1}, Lcom/konka/hotkey/ZoomActivity;->access$5(Lcom/konka/hotkey/ZoomActivity;)Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v1

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI3:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-eq v1, v2, :cond_2

    # getter for: Lcom/konka/hotkey/ZoomActivity;->mInputMode:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    invoke-static {p1}, Lcom/konka/hotkey/ZoomActivity;->access$5(Lcom/konka/hotkey/ZoomActivity;)Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v1

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI4:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-ne v1, v2, :cond_0

    :cond_2
    const/4 v8, 0x3

    goto :goto_0

    :pswitch_5
    const/4 v8, 0x3

    goto :goto_0

    :pswitch_6
    const/4 v8, 0x4

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_3
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method public doUpdate()Z
    .locals 4

    const/4 v3, 0x1

    iget-object v1, p0, Lcom/konka/hotkey/ZoomActivity$ZoomItem;->Indexnumber:Lcom/konka/hotkey/getIndex;

    invoke-virtual {v1}, Lcom/konka/hotkey/getIndex;->getIndex2()I

    move-result v0

    invoke-virtual {p0, v3}, Lcom/konka/hotkey/ZoomActivity$ZoomItem;->setRunning(Z)V

    iget-object v1, p0, Lcom/konka/hotkey/ZoomActivity$ZoomItem;->this$0:Lcom/konka/hotkey/ZoomActivity;

    # getter for: Lcom/konka/hotkey/ZoomActivity;->pictureDesk:Lcom/konka/kkinterface/tv/PictureDesk;
    invoke-static {v1}, Lcom/konka/hotkey/ZoomActivity;->access$6(Lcom/konka/hotkey/ZoomActivity;)Lcom/konka/kkinterface/tv/PictureDesk;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/hotkey/ZoomActivity$ZoomItem;->this$0:Lcom/konka/hotkey/ZoomActivity;

    iget-object v2, v2, Lcom/konka/hotkey/ZoomActivity;->arcModes:[Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Lcom/konka/kkinterface/tv/PictureDesk;->SetVideoArc(Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;)Z

    iget-object v1, p0, Lcom/konka/hotkey/ZoomActivity$ZoomItem;->this$0:Lcom/konka/hotkey/ZoomActivity;

    # getter for: Lcom/konka/hotkey/ZoomActivity;->pictureDesk:Lcom/konka/kkinterface/tv/PictureDesk;
    invoke-static {v1}, Lcom/konka/hotkey/ZoomActivity;->access$6(Lcom/konka/hotkey/ZoomActivity;)Lcom/konka/kkinterface/tv/PictureDesk;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/hotkey/ZoomActivity$ZoomItem;->this$0:Lcom/konka/hotkey/ZoomActivity;

    # getter for: Lcom/konka/hotkey/ZoomActivity;->curPicMod:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;
    invoke-static {v2}, Lcom/konka/hotkey/ZoomActivity;->access$7(Lcom/konka/hotkey/ZoomActivity;)Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/konka/kkinterface/tv/PictureDesk;->SetPictureModeIdx(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;)Z

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "click: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/hotkey/ZoomActivity$ZoomItem;->meMode:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "index:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    return v3
.end method
