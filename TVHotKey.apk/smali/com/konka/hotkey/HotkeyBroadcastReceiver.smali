.class public Lcom/konka/hotkey/HotkeyBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "HotkeyBroadcastReceiver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;,
        Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeMode;
    }
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$konka$hotkey$HotkeyBroadcastReceiver$UpgradeInfo:[I = null

.field private static final ACTION_NO_OPERATION:Ljava/lang/String; = "com.konka.NO_OPERATION_SHUTDOWN"

.field private static final ACTION_NO_SIGNAL:Ljava/lang/String; = "com.konka.tv.hotkey.NO_SIGNAL"

.field private static final ACTION_START_COUNTDOWN_ACTIVITY:Ljava/lang/String; = "com.konka.tvsettings.STANDBY_NO_OPERATIONS"

.field private static final ACTION_TIME_OFF:Ljava/lang/String; = "com.android.tv.hotkey.TIMER_OFF_WARNING"

.field private static final ACTION_VOICE_CONTROL:Ljava/lang/String; = "konka.voice.control.action.VOLUMESET"

.field public static final CMD_ERROR:I = -0x1

.field public static final CMD_VOLUME_SET_TO_DECREASE:I = 0x49

.field public static final CMD_VOLUME_SET_TO_INCREASE:I = 0x48

.field public static final CMD_VOLUME_SET_TO_MUTE:I = 0x46

.field public static final CMD_VOLUME_SET_TO_VALUE:I = 0x47

.field public static final FLASH_BANK_ADDR:I = 0x1d

.field public static final FLASH_HDCPKEY_ADDR_START:I = 0x100

.field public static final FLASH_MACADDR_ADDR_START:I = 0x0

.field public static final FLASH_SERIALNUM_ADDR_START:I = 0x80

.field public static final HDCPKEY_DATA_LEN:I = 0x130

.field public static final MACADDR_DATA_LEN:I = 0x6

.field public static RECEIVE_COUNT_DOWN:Z = false

.field public static final SERIALNUM_DATA_LEN:I = 0x32

.field public static final SPI_BANK_SIZE:I = 0x10000

.field public static final SYSTEM_BANK_SIZE:I = 0x300

.field private static bMuteFreezeOnShow:Z

.field private static freezeFlag:Z

.field private static iBackLight:S

.field private static iSoundBalance:S

.field private static index:I

.field private static mMuteAndFreezeView:Landroid/view/View;

.field private static mParamsMuteAndFreeze:Landroid/view/WindowManager$LayoutParams;

.field private static macstr:Ljava/lang/String;

.field private static muteFlag:Z

.field private static offsetStr:Ljava/lang/String;

.field static toast:Landroid/widget/Toast;

.field private static wmMuteAndFreeze:Landroid/view/WindowManager;


# instance fields
.field private audioskin:Lcom/mstar/android/tv/TvAudioManager;

.field private pictureskin:Lcom/mstar/android/tv/TvPictureManager;


# direct methods
.method static synthetic $SWITCH_TABLE$com$konka$hotkey$HotkeyBroadcastReceiver$UpgradeInfo()[I
    .locals 3

    sget-object v0, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->$SWITCH_TABLE$com$konka$hotkey$HotkeyBroadcastReceiver$UpgradeInfo:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;->values()[Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;->UPGRADE_DATA_OVER:Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;

    invoke-virtual {v1}, Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_4

    :goto_1
    :try_start_1
    sget-object v1, Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;->UPGRADE_NO_FILE:Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;

    invoke-virtual {v1}, Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_3

    :goto_2
    :try_start_2
    sget-object v1, Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;->UPGRADE_OK:Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;

    invoke-virtual {v1}, Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :goto_3
    :try_start_3
    sget-object v1, Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;->UPGRADE_READ_FILE_FAIL:Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;

    invoke-virtual {v1}, Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_1

    :goto_4
    :try_start_4
    sget-object v1, Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;->UPGRADE_RW_SPI_FAIL:Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;

    invoke-virtual {v1}, Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_0

    :goto_5
    sput-object v0, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->$SWITCH_TABLE$com$konka$hotkey$HotkeyBroadcastReceiver$UpgradeInfo:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_5

    :catch_1
    move-exception v1

    goto :goto_4

    :catch_2
    move-exception v1

    goto :goto_3

    :catch_3
    move-exception v1

    goto :goto_2

    :catch_4
    move-exception v1

    goto :goto_1
.end method

.method static constructor <clinit>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    sput-boolean v1, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->muteFlag:Z

    sput-boolean v1, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->freezeFlag:Z

    sput-boolean v1, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->bMuteFreezeOnShow:Z

    const/4 v0, 0x1

    sput-boolean v0, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->RECEIVE_COUNT_DOWN:Z

    sput-object v2, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->mParamsMuteAndFreeze:Landroid/view/WindowManager$LayoutParams;

    sput-object v2, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->mMuteAndFreezeView:Landroid/view/View;

    sput-short v1, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->iSoundBalance:S

    sput-short v1, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->iBackLight:S

    sput-object v2, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->toast:Landroid/widget/Toast;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    iput-object v0, p0, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->audioskin:Lcom/mstar/android/tv/TvAudioManager;

    iput-object v0, p0, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->pictureskin:Lcom/mstar/android/tv/TvPictureManager;

    return-void
.end method

.method public static DetectFileFromUSB(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p0    # Ljava/lang/String;

    new-instance v3, Ljava/io/File;

    const-string v6, "/mnt/usb/"

    invoke-direct {v3, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v5, Ljava/lang/String;

    const-string v6, "/mnt/usb/"

    invoke-direct {v5, v6}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->isDirectory()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {v3}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    const/4 v2, 0x0

    :goto_0
    array-length v6, v1

    if-lt v2, v6, :cond_1

    :cond_0
    const/4 v6, 0x0

    :goto_1
    return-object v6

    :cond_1
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v7, v1, v2

    invoke-virtual {v7}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    new-instance v4, Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v6}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_2

    move-object v6, v5

    goto :goto_1

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public static Write_HDCPKey(Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeMode;Ljava/util/HashMap;[B)Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;
    .locals 29
    .param p0    # Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeMode;
    .param p2    # [B
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeMode;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Double;",
            ">;[B)",
            "Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;"
        }
    .end annotation

    sget-object v24, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v25, "Write_HDCPKey!"

    invoke-virtual/range {v24 .. v25}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    const/16 v24, 0x130

    move/from16 v0, v24

    new-array v3, v0, [B

    const/16 v17, 0x0

    sget-object v24, Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeMode;->UPGRADE_MODE_USB:Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeMode;

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    if-ne v0, v1, :cond_12

    const-string v24, "hdcpkey.bin"

    invoke-static/range {v24 .. v24}, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->DetectFileFromUSB(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    if-nez v9, :cond_0

    sget-object v24, Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;->UPGRADE_NO_FILE:Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;

    :goto_0
    return-object v24

    :cond_0
    :try_start_0
    new-instance v11, Ljava/io/FileInputStream;

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v25

    invoke-direct/range {v24 .. v25}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v25, "hdcpkey_index.bin"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-direct {v11, v0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    const/16 v24, 0x4

    move/from16 v0, v24

    new-array v4, v0, [B

    invoke-virtual {v11, v4}, Ljava/io/FileInputStream;->read([B)I

    invoke-virtual {v11}, Ljava/io/FileInputStream;->close()V

    const/4 v14, 0x0

    const/4 v13, 0x3

    :goto_1
    if-gez v13, :cond_1

    const-string v24, "index"

    int-to-double v0, v14

    move-wide/from16 v25, v0

    invoke-static/range {v25 .. v26}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v25

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    move-object/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v24, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v25, "hdcp key index: %d\n"

    const/16 v26, 0x1

    move/from16 v0, v26

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v28

    aput-object v28, v26, v27

    invoke-virtual/range {v24 .. v26}, Ljava/io/PrintStream;->printf(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintStream;

    new-instance v10, Ljava/io/FileInputStream;

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v25

    invoke-direct/range {v24 .. v25}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v25, "hdcpkey.bin"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-direct {v10, v0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/FileInputStream;->available()I

    move-result v18

    const/16 v24, 0x20

    move/from16 v0, v18

    move/from16 v1, v24

    if-ge v0, v1, :cond_3

    invoke-virtual {v10}, Ljava/io/FileInputStream;->close()V

    sget-object v24, Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;->UPGRADE_READ_FILE_FAIL:Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;

    goto :goto_0

    :cond_1
    mul-int/lit16 v14, v14, 0x100

    aget-byte v23, v4, v13

    if-gez v23, :cond_2

    move/from16 v0, v23

    add-int/lit16 v0, v0, 0x100

    move/from16 v23, v0

    :cond_2
    add-int v14, v14, v23

    add-int/lit8 v13, v13, -0x1

    goto :goto_1

    :cond_3
    const/16 v24, 0x0

    const/16 v25, 0x20

    move/from16 v0, v24

    move/from16 v1, v25

    invoke-virtual {v10, v3, v0, v1}, Ljava/io/FileInputStream;->read([BII)I

    const/16 v21, 0x0

    const/16 v13, 0x12

    :goto_2
    const/16 v24, 0x14

    move/from16 v0, v24

    if-lt v13, v0, :cond_4

    const-string v24, "size"

    move/from16 v0, v21

    int-to-double v0, v0

    move-wide/from16 v25, v0

    invoke-static/range {v25 .. v26}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v25

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    move-object/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move/from16 v17, v21

    const/16 v19, 0x0

    const/16 v13, 0x14

    :goto_3
    const/16 v24, 0x18

    move/from16 v0, v24

    if-lt v13, v0, :cond_6

    sget-object v24, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v25, "hdcp key max: %d\n"

    const/16 v26, 0x1

    move/from16 v0, v26

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v28

    aput-object v28, v26, v27

    invoke-virtual/range {v24 .. v26}, Ljava/io/PrintStream;->printf(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintStream;

    move/from16 v0, v19

    if-le v14, v0, :cond_8

    invoke-virtual {v10}, Ljava/io/FileInputStream;->close()V

    sget-object v24, Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;->UPGRADE_DATA_OVER:Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;

    goto/16 :goto_0

    :cond_4
    move/from16 v0, v21

    mul-int/lit16 v0, v0, 0x100

    move/from16 v24, v0

    move/from16 v0, v24

    int-to-short v0, v0

    move/from16 v21, v0

    aget-byte v23, v3, v13

    if-gez v23, :cond_5

    move/from16 v0, v23

    add-int/lit16 v0, v0, 0x100

    move/from16 v23, v0

    :cond_5
    add-int v24, v21, v23

    move/from16 v0, v24

    int-to-short v0, v0

    move/from16 v21, v0

    add-int/lit8 v13, v13, 0x1

    goto :goto_2

    :cond_6
    move/from16 v0, v19

    mul-int/lit16 v0, v0, 0x100

    move/from16 v19, v0

    aget-byte v23, v3, v13

    if-gez v23, :cond_7

    move/from16 v0, v23

    add-int/lit16 v0, v0, 0x100

    move/from16 v23, v0

    :cond_7
    add-int v19, v19, v23

    add-int/lit8 v13, v13, 0x1

    goto :goto_3

    :cond_8
    mul-int v24, v21, v14

    add-int/lit8 v20, v24, 0x20

    const-string v24, "offset"

    move/from16 v0, v20

    int-to-double v0, v0

    move-wide/from16 v25, v0

    invoke-static/range {v25 .. v26}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v25

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    move-object/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v24, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v25, "hdcp key offset: 0x%08x\n"

    const/16 v26, 0x1

    move/from16 v0, v26

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v28

    aput-object v28, v26, v27

    invoke-virtual/range {v24 .. v26}, Ljava/io/PrintStream;->printf(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintStream;

    add-int v24, v20, v21

    move/from16 v0, v18

    move/from16 v1, v24

    if-lt v0, v1, :cond_9

    const/16 v24, 0x130

    move/from16 v0, v21

    move/from16 v1, v24

    if-le v0, v1, :cond_a

    :cond_9
    invoke-virtual {v10}, Ljava/io/FileInputStream;->close()V

    sget-object v24, Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;->UPGRADE_READ_FILE_FAIL:Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;

    goto/16 :goto_0

    :cond_a
    add-int/lit8 v24, v20, -0x20

    move/from16 v0, v24

    int-to-long v0, v0

    move-wide/from16 v24, v0

    move-wide/from16 v0, v24

    invoke-virtual {v10, v0, v1}, Ljava/io/FileInputStream;->skip(J)J

    const/16 v24, 0x0

    move/from16 v0, v24

    move/from16 v1, v21

    invoke-virtual {v10, v3, v0, v1}, Ljava/io/FileInputStream;->read([BII)I

    const/4 v13, 0x0

    :goto_4
    move/from16 v0, v21

    if-lt v13, v0, :cond_c

    add-int/lit8 v14, v14, 0x1

    const/4 v13, 0x0

    :goto_5
    const/16 v24, 0x4

    move/from16 v0, v24

    if-lt v13, v0, :cond_e

    new-instance v12, Ljava/io/FileOutputStream;

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v25

    invoke-direct/range {v24 .. v25}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v25, "hdcpkey_index.bin"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-direct {v12, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v4}, Ljava/io/FileOutputStream;->write([B)V

    invoke-virtual {v12}, Ljava/io/FileOutputStream;->close()V

    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v24

    const-string v25, "sync"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3

    const-wide/16 v24, 0xbb8

    :try_start_1
    invoke-static/range {v24 .. v25}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3

    :goto_6
    const/4 v7, 0x0

    const/16 v22, 0x1

    :goto_7
    :try_start_2
    new-instance v11, Ljava/io/FileInputStream;

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v25

    invoke-direct/range {v24 .. v25}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v25, "hdcpkey_index.bin"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-direct {v11, v0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11}, Ljava/io/FileInputStream;->available()I

    move-result v16

    const/16 v24, 0x4

    move/from16 v0, v16

    move/from16 v1, v24

    if-ge v0, v1, :cond_f

    const/16 v22, 0x0

    :cond_b
    :goto_8
    invoke-virtual {v11}, Ljava/io/FileInputStream;->close()V

    if-nez v22, :cond_13

    add-int/lit8 v7, v7, 0x1

    const/16 v24, 0x5

    move/from16 v0, v24

    if-le v7, v0, :cond_11

    sget-object v24, Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;->UPGRADE_READ_FILE_FAIL:Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;

    goto/16 :goto_0

    :cond_c
    const/4 v15, 0x0

    :goto_9
    const/16 v24, 0x10

    move/from16 v0, v24

    if-lt v15, v0, :cond_d

    sget-object v24, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual/range {v24 .. v24}, Ljava/io/PrintStream;->println()V

    add-int/lit8 v13, v13, 0x10

    goto/16 :goto_4

    :cond_d
    sget-object v24, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v25, "%02X "

    const/16 v26, 0x1

    move/from16 v0, v26

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    add-int v28, v13, v15

    aget-byte v28, v3, v28

    invoke-static/range {v28 .. v28}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v28

    aput-object v28, v26, v27

    invoke-virtual/range {v24 .. v26}, Ljava/io/PrintStream;->printf(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintStream;

    add-int/lit8 v15, v15, 0x1

    goto :goto_9

    :cond_e
    rem-int/lit16 v0, v14, 0x100

    move/from16 v24, v0

    move/from16 v0, v24

    int-to-byte v0, v0

    move/from16 v24, v0

    aput-byte v24, v4, v13

    div-int/lit16 v14, v14, 0x100

    add-int/lit8 v13, v13, 0x1

    goto/16 :goto_5

    :catch_0
    move-exception v8

    invoke-virtual {v8}, Ljava/lang/InterruptedException;->printStackTrace()V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    goto :goto_6

    :catch_1
    move-exception v8

    invoke-virtual {v8}, Ljava/io/FileNotFoundException;->printStackTrace()V

    sget-object v24, Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;->UPGRADE_NO_FILE:Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;

    goto/16 :goto_0

    :cond_f
    :try_start_3
    move/from16 v0, v16

    new-array v5, v0, [B

    invoke-virtual {v11, v5}, Ljava/io/FileInputStream;->read([B)I

    const/4 v13, 0x0

    :goto_a
    const/16 v24, 0x4

    move/from16 v0, v24

    if-ge v13, v0, :cond_b

    aget-byte v24, v5, v13

    aget-byte v25, v4, v13
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    move/from16 v0, v24

    move/from16 v1, v25

    if-eq v0, v1, :cond_10

    const/16 v22, 0x0

    goto :goto_8

    :cond_10
    add-int/lit8 v13, v13, 0x1

    goto :goto_a

    :cond_11
    const-wide/16 v24, 0x7d0

    :try_start_4
    invoke-static/range {v24 .. v25}, Ljava/lang/Thread;->sleep(J)V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    goto/16 :goto_7

    :catch_2
    move-exception v8

    :try_start_5
    invoke-virtual {v8}, Ljava/lang/InterruptedException;->printStackTrace()V
    :try_end_5
    .catch Ljava/io/FileNotFoundException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    goto/16 :goto_7

    :catch_3
    move-exception v8

    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    sget-object v24, Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;->UPGRADE_READ_FILE_FAIL:Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;

    goto/16 :goto_0

    :cond_12
    sget-object v24, Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeMode;->UPGRADE_MODE_NET:Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeMode;

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    if-ne v0, v1, :cond_13

    move-object/from16 v0, p2

    array-length v0, v0

    move/from16 v17, v0

    const/4 v13, 0x0

    :goto_b
    move/from16 v0, v17

    if-lt v13, v0, :cond_14

    :cond_13
    const/16 v24, 0x300

    :try_start_6
    move/from16 v0, v24

    new-array v6, v0, [S

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v24

    const/16 v25, 0x1d

    const/16 v26, 0x300

    invoke-virtual/range {v24 .. v26}, Lcom/mstar/android/tvapi/common/TvManager;->readFromSpiFlashByBank(II)[S

    move-result-object v6

    const/4 v13, 0x0

    :goto_c
    const/16 v24, 0x130

    move/from16 v0, v24

    if-lt v13, v0, :cond_15

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v24

    const/16 v25, 0x1d

    move-object/from16 v0, v24

    move/from16 v1, v25

    invoke-virtual {v0, v1, v6}, Lcom/mstar/android/tvapi/common/TvManager;->writeToSpiFlashByBank(I[S)Z

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v24

    const/16 v25, 0x1d

    const/16 v26, 0x230

    invoke-virtual/range {v24 .. v26}, Lcom/mstar/android/tvapi/common/TvManager;->readFromSpiFlashByBank(II)[S

    move-result-object v6

    const/4 v13, 0x0

    :goto_d
    move/from16 v0, v17

    if-lt v13, v0, :cond_16

    invoke-static/range {p2 .. p2}, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->initSerialNum([B)V
    :try_end_6
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_6 .. :try_end_6} :catch_4

    sget-object v24, Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;->UPGRADE_OK:Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;

    goto/16 :goto_0

    :cond_14
    aget-byte v24, p2, v13

    aput-byte v24, v3, v13

    const/16 v24, 0x130

    move/from16 v0, v24

    if-gt v13, v0, :cond_13

    add-int/lit8 v13, v13, 0x1

    goto :goto_b

    :cond_15
    add-int/lit16 v0, v13, 0x100

    move/from16 v24, v0

    :try_start_7
    aget-byte v25, v3, v13

    aput-short v25, v6, v24

    add-int/lit8 v13, v13, 0x1

    goto :goto_c

    :cond_16
    add-int/lit16 v0, v13, 0x100

    move/from16 v24, v0

    aget-short v24, v6, v24

    move/from16 v0, v24

    int-to-byte v0, v0

    move/from16 v24, v0

    aput-byte v24, p2, v13
    :try_end_7
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_7 .. :try_end_7} :catch_4

    add-int/lit8 v13, v13, 0x1

    goto :goto_d

    :catch_4
    move-exception v8

    invoke-virtual {v8}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    sget-object v24, Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;->UPGRADE_RW_SPI_FAIL:Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;

    goto/16 :goto_0
.end method

.method public static Write_MACaddr(Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeMode;[B)Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;
    .locals 27
    .param p0    # Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeMode;
    .param p1    # [B

    sget-object v22, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v23, "Write_MACaddr!"

    invoke-virtual/range {v22 .. v23}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    const/16 v22, 0xc

    move/from16 v0, v22

    new-array v3, v0, [B

    sget-object v22, Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeMode;->UPGRADE_MODE_USB:Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeMode;

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    if-ne v0, v1, :cond_c

    const-string v22, "mac.bin"

    invoke-static/range {v22 .. v22}, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->DetectFileFromUSB(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    if-nez v11, :cond_0

    const-string v22, "Write_MACaddr==========="

    const-string v23, "No file"

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v22, Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;->UPGRADE_NO_FILE:Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;

    :goto_0
    return-object v22

    :cond_0
    :try_start_0
    new-instance v12, Ljava/io/FileInputStream;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-static {v11}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v23

    invoke-direct/range {v22 .. v23}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v23, "mac.bin"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-direct {v12, v0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12}, Ljava/io/FileInputStream;->available()I

    move-result v15

    const/16 v22, 0xc

    move/from16 v0, v22

    if-ge v15, v0, :cond_1

    invoke-virtual {v12}, Ljava/io/FileInputStream;->close()V

    sget-object v22, Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;->UPGRADE_READ_FILE_FAIL:Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;

    goto :goto_0

    :cond_1
    invoke-virtual {v12, v3}, Ljava/io/FileInputStream;->read([B)I

    sget-object v22, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v23, "Mac Addr %02X:%02X:%02X:%02X:%02X:%02X\n"

    const/16 v24, 0x6

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    const/16 v26, 0x0

    aget-byte v26, v3, v26

    invoke-static/range {v26 .. v26}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v26

    aput-object v26, v24, v25

    const/16 v25, 0x1

    const/16 v26, 0x1

    aget-byte v26, v3, v26

    invoke-static/range {v26 .. v26}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v26

    aput-object v26, v24, v25

    const/16 v25, 0x2

    const/16 v26, 0x2

    aget-byte v26, v3, v26

    invoke-static/range {v26 .. v26}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v26

    aput-object v26, v24, v25

    const/16 v25, 0x3

    const/16 v26, 0x3

    aget-byte v26, v3, v26

    invoke-static/range {v26 .. v26}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v26

    aput-object v26, v24, v25

    const/16 v25, 0x4

    const/16 v26, 0x4

    aget-byte v26, v3, v26

    invoke-static/range {v26 .. v26}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v26

    aput-object v26, v24, v25

    const/16 v25, 0x5

    const/16 v26, 0x5

    aget-byte v26, v3, v26

    invoke-static/range {v26 .. v26}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v26

    aput-object v26, v24, v25

    invoke-virtual/range {v22 .. v24}, Ljava/io/PrintStream;->printf(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintStream;

    const-wide/16 v8, 0x0

    const-wide/16 v17, 0x0

    const/16 v22, 0xc

    move/from16 v0, v22

    new-array v6, v0, [B

    const/4 v14, 0x0

    :goto_1
    const/16 v22, 0x6

    move/from16 v0, v22

    if-lt v14, v0, :cond_2

    const/4 v14, 0x6

    :goto_2
    const/16 v22, 0xc

    move/from16 v0, v22

    if-lt v14, v0, :cond_4

    cmp-long v22, v8, v17

    if-lez v22, :cond_6

    invoke-virtual {v12}, Ljava/io/FileInputStream;->close()V

    sget-object v22, Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;->UPGRADE_DATA_OVER:Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;

    goto/16 :goto_0

    :cond_2
    const-wide/16 v22, 0x100

    mul-long v8, v8, v22

    aget-byte v21, v3, v14

    aget-byte v22, v3, v14

    aput-byte v22, v6, v14

    if-gez v21, :cond_3

    move/from16 v0, v21

    add-int/lit16 v0, v0, 0x100

    move/from16 v21, v0

    :cond_3
    move/from16 v0, v21

    int-to-long v0, v0

    move-wide/from16 v22, v0

    add-long v8, v8, v22

    add-int/lit8 v14, v14, 0x1

    goto :goto_1

    :cond_4
    const-wide/16 v22, 0x100

    mul-long v17, v17, v22

    aget-byte v21, v3, v14

    aget-byte v22, v3, v14

    aput-byte v22, v6, v14

    if-gez v21, :cond_5

    move/from16 v0, v21

    add-int/lit16 v0, v0, 0x100

    move/from16 v21, v0

    :cond_5
    move/from16 v0, v21

    int-to-long v0, v0

    move-wide/from16 v22, v0

    add-long v17, v17, v22

    add-int/lit8 v14, v14, 0x1

    goto :goto_2

    :cond_6
    invoke-virtual {v12}, Ljava/io/FileInputStream;->close()V

    const-wide/16 v22, 0x1

    add-long v8, v8, v22

    const/4 v14, 0x5

    :goto_3
    if-gez v14, :cond_8

    new-instance v13, Ljava/io/FileOutputStream;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-static {v11}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v23

    invoke-direct/range {v22 .. v23}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v23, "mac.bin"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-direct {v13, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13, v6}, Ljava/io/FileOutputStream;->write([B)V

    invoke-virtual {v13}, Ljava/io/FileOutputStream;->close()V

    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v22

    const-string v23, "sync"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3

    const-wide/16 v22, 0xbb8

    :try_start_1
    invoke-static/range {v22 .. v23}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3

    :goto_4
    const/4 v7, 0x0

    const/16 v19, 0x1

    :goto_5
    :try_start_2
    new-instance v12, Ljava/io/FileInputStream;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-static {v11}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v23

    invoke-direct/range {v22 .. v23}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v23, "mac.bin"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-direct {v12, v0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12}, Ljava/io/FileInputStream;->available()I

    move-result v15

    const/16 v22, 0xc

    move/from16 v0, v22

    if-ge v15, v0, :cond_9

    const/16 v19, 0x0

    :cond_7
    :goto_6
    invoke-virtual {v12}, Ljava/io/FileInputStream;->close()V

    if-nez v19, :cond_d

    add-int/lit8 v7, v7, 0x1

    const/16 v22, 0x5

    move/from16 v0, v22

    if-le v7, v0, :cond_b

    sget-object v22, Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;->UPGRADE_READ_FILE_FAIL:Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;

    goto/16 :goto_0

    :cond_8
    const-wide/16 v22, 0x100

    rem-long v22, v8, v22

    move-wide/from16 v0, v22

    long-to-int v0, v0

    move/from16 v22, v0

    move/from16 v0, v22

    int-to-byte v0, v0

    move/from16 v22, v0

    aput-byte v22, v6, v14

    const-wide/16 v22, 0x100

    div-long v8, v8, v22

    add-int/lit8 v14, v14, -0x1

    goto :goto_3

    :catch_0
    move-exception v10

    invoke-virtual {v10}, Ljava/lang/InterruptedException;->printStackTrace()V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    goto :goto_4

    :catch_1
    move-exception v10

    invoke-virtual {v10}, Ljava/io/FileNotFoundException;->printStackTrace()V

    const-string v22, "Write_MACaddr===========1111111"

    const-string v23, "No file"

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v22, Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;->UPGRADE_NO_FILE:Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;

    goto/16 :goto_0

    :cond_9
    :try_start_3
    new-array v4, v15, [B

    invoke-virtual {v12, v4}, Ljava/io/FileInputStream;->read([B)I

    const/4 v14, 0x0

    :goto_7
    const/16 v22, 0xc

    move/from16 v0, v22

    if-ge v14, v0, :cond_7

    aget-byte v22, v4, v14

    aget-byte v23, v6, v14
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    move/from16 v0, v22

    move/from16 v1, v23

    if-eq v0, v1, :cond_a

    const/16 v19, 0x0

    goto :goto_6

    :cond_a
    add-int/lit8 v14, v14, 0x1

    goto :goto_7

    :cond_b
    const-wide/16 v22, 0x7d0

    :try_start_4
    invoke-static/range {v22 .. v23}, Ljava/lang/Thread;->sleep(J)V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    goto/16 :goto_5

    :catch_2
    move-exception v10

    :try_start_5
    invoke-virtual {v10}, Ljava/lang/InterruptedException;->printStackTrace()V
    :try_end_5
    .catch Ljava/io/FileNotFoundException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    goto/16 :goto_5

    :catch_3
    move-exception v10

    invoke-virtual {v10}, Ljava/io/IOException;->printStackTrace()V

    sget-object v22, Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;->UPGRADE_READ_FILE_FAIL:Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;

    goto/16 :goto_0

    :cond_c
    sget-object v22, Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeMode;->UPGRADE_MODE_NET:Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeMode;

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    if-ne v0, v1, :cond_d

    move-object/from16 v0, p1

    array-length v0, v0

    move/from16 v16, v0

    const/4 v14, 0x0

    :goto_8
    move/from16 v0, v16

    if-lt v14, v0, :cond_e

    :cond_d
    const/16 v22, 0x300

    :try_start_6
    move/from16 v0, v22

    new-array v5, v0, [S

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v22

    const/16 v23, 0x1d

    const/16 v24, 0x300

    invoke-virtual/range {v22 .. v24}, Lcom/mstar/android/tvapi/common/TvManager;->readFromSpiFlashByBank(II)[S

    move-result-object v5

    const/4 v14, 0x0

    :goto_9
    const/16 v22, 0x6

    move/from16 v0, v22

    if-lt v14, v0, :cond_f

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v22

    const/16 v23, 0x1d

    move-object/from16 v0, v22

    move/from16 v1, v23

    invoke-virtual {v0, v1, v5}, Lcom/mstar/android/tvapi/common/TvManager;->writeToSpiFlashByBank(I[S)Z

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v22

    const/16 v23, 0x1d

    const/16 v24, 0x6

    invoke-virtual/range {v22 .. v24}, Lcom/mstar/android/tvapi/common/TvManager;->readFromSpiFlashByBank(II)[S
    :try_end_6
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_6 .. :try_end_6} :catch_5

    move-result-object v5

    const/4 v14, 0x0

    :goto_a
    const/16 v22, 0x6

    move/from16 v0, v22

    if-lt v14, v0, :cond_10

    :try_start_7
    const-string v22, "%02X:%02X:%02X:%02X:%02X:%02X"

    const/16 v23, 0x6

    move/from16 v0, v23

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    const/16 v25, 0x0

    aget-byte v25, v3, v25

    invoke-static/range {v25 .. v25}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v25

    aput-object v25, v23, v24

    const/16 v24, 0x1

    const/16 v25, 0x1

    aget-byte v25, v3, v25

    invoke-static/range {v25 .. v25}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v25

    aput-object v25, v23, v24

    const/16 v24, 0x2

    const/16 v25, 0x2

    aget-byte v25, v3, v25

    invoke-static/range {v25 .. v25}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v25

    aput-object v25, v23, v24

    const/16 v24, 0x3

    const/16 v25, 0x3

    aget-byte v25, v3, v25

    invoke-static/range {v25 .. v25}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v25

    aput-object v25, v23, v24

    const/16 v24, 0x4

    const/16 v25, 0x4

    aget-byte v25, v3, v25

    invoke-static/range {v25 .. v25}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v25

    aput-object v25, v23, v24

    const/16 v24, 0x5

    const/16 v25, 0x5

    aget-byte v25, v3, v25

    invoke-static/range {v25 .. v25}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v25

    aput-object v25, v23, v24

    invoke-static/range {v22 .. v23}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v20

    sput-object v20, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->macstr:Ljava/lang/String;

    const-string v22, "setEnvironment"

    new-instance v23, Ljava/lang/StringBuilder;

    const-string v24, "ethaddr "

    invoke-direct/range {v23 .. v24}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v23

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v22

    const-string v23, "ethaddr"

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Lcom/mstar/android/tvapi/common/TvManager;->setEnvironment(Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_7
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_7 .. :try_end_7} :catch_4

    :goto_b
    sget-object v22, Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;->UPGRADE_OK:Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;

    goto/16 :goto_0

    :cond_e
    aget-byte v22, p1, v14

    aput-byte v22, v3, v14

    const/16 v22, 0x6

    move/from16 v0, v22

    if-gt v14, v0, :cond_d

    add-int/lit8 v14, v14, 0x1

    goto/16 :goto_8

    :cond_f
    add-int/lit8 v22, v14, 0x0

    :try_start_8
    aget-byte v23, v3, v14

    aput-short v23, v5, v22

    add-int/lit8 v14, v14, 0x1

    goto/16 :goto_9

    :cond_10
    add-int/lit8 v22, v14, 0x0

    aget-short v22, v5, v22

    move/from16 v0, v22

    int-to-byte v0, v0

    move/from16 v22, v0

    aput-byte v22, p1, v14

    add-int/lit8 v14, v14, 0x1

    goto/16 :goto_a

    :catch_4
    move-exception v10

    invoke-virtual {v10}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V
    :try_end_8
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_8 .. :try_end_8} :catch_5

    goto :goto_b

    :catch_5
    move-exception v10

    invoke-virtual {v10}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    sget-object v22, Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;->UPGRADE_RW_SPI_FAIL:Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;

    goto/16 :goto_0
.end method

.method public static get4K2KMode()Z
    .locals 2

    const/4 v0, 0x1

    const-string v1, "isIn4K2KMode"

    invoke-static {v1}, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->getValueByTvosCommon(Ljava/lang/String;)S

    move-result v1

    if-ne v0, v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getValueByTvosCommon(Ljava/lang/String;)S
    .locals 4
    .param p0    # Ljava/lang/String;

    const/4 v2, -0x1

    if-eqz p0, :cond_0

    const-string v3, ""

    if-ne p0, v3, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3, p0}, Lcom/mstar/android/tvapi/common/TvManager;->setTvosCommonCommand(Ljava/lang/String;)[S

    move-result-object v1

    if-eqz v1, :cond_0

    array-length v3, v1

    if-eqz v3, :cond_0

    const/4 v3, 0x0

    aget-short v2, v1, v3

    goto :goto_0

    :cond_2
    const-string v3, "TvManger.getInstance() == null"

    invoke-static {v3}, Lcom/konka/debuginfo/logPrint;->Fatal(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method private static initSerialNum([B)V
    .locals 10
    .param p0    # [B

    const/16 v9, 0x32

    const-string v0, "/data/misc/konka/SerialNum.txt"

    new-array v5, v9, [S

    fill-array-data v5, :array_0

    new-array v1, v9, [B

    array-length v6, p0

    if-le v6, v9, :cond_0

    const/16 v6, 0x32

    :cond_0
    const/4 v4, 0x0

    :goto_0
    if-lt v4, v6, :cond_1

    const/4 v4, 0x0

    :goto_1
    if-lt v4, v9, :cond_2

    :try_start_0
    new-instance v3, Ljava/io/FileOutputStream;

    const-string v8, "/data/misc/konka/SerialNum.txt"

    invoke-direct {v3, v8}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/io/FileOutputStream;->write([B)V

    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_2
    return-void

    :cond_1
    aget-byte v8, p0, v4

    aput-byte v8, v1, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_2
    aget-byte v8, v1, v4

    shl-int/lit8 v8, v8, 0x4

    int-to-byte v7, v8

    aget-byte v8, v1, v4

    shr-int/lit8 v8, v8, 0x4

    int-to-byte v8, v8

    aput-byte v8, v1, v4

    aget-byte v8, v1, v4

    or-int/2addr v8, v7

    int-to-byte v7, v8

    xor-int/lit8 v8, v7, 0x36

    int-to-byte v7, v8

    aget-short v8, v5, v4

    xor-int/2addr v8, v7

    int-to-byte v7, v8

    aput-byte v7, v1, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_2

    :catch_1
    move-exception v2

    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    nop

    :array_0
    .array-data 2
        0x6bs
        0xf7s
        0x6as
        0x3fs
        0x9as
        0x36s
        0x3ds
        0xefs
        0xfas
        0x4ds
        0x77s
        0xd0s
        0x6as
        0xd3s
        0x1bs
        0x5cs
        0x20s
        0x82s
        0x40s
        0xf2s
        0xd7s
        0xf2s
        0xa1s
        0x59s
        0x89s
        0x55s
        0x6fs
        0xdds
        0x45s
        0x9bs
        0x9s
        0xe4s
        0x10s
        0x4bs
        0x9s
        0xfes
        0x53s
        0x25s
        0xbes
        0xe5s
        0x50s
        0xe4s
        0x81s
        0x3as
        0x62s
        0xc7s
        0x9s
        0x5ds
        0x89s
        0x8s
    .end array-data
.end method

.method private isReadyShow3D(Landroid/content/Context;)Z
    .locals 9
    .param p1    # Landroid/content/Context;

    const/4 v7, 0x1

    const/4 v6, 0x0

    new-instance v2, Lcom/konka/kkimplements/common/IniEditor;

    invoke-direct {v2}, Lcom/konka/kkimplements/common/IniEditor;-><init>()V

    const-string v5, "/customercfg/panel/panel.ini"

    invoke-virtual {v2, v5}, Lcom/konka/kkimplements/common/IniEditor;->loadFile(Ljava/lang/String;)Z

    const-string v5, "panel:b3DPanel"

    const-string v8, "0"

    invoke-virtual {v2, v5, v8}, Lcom/konka/kkimplements/common/IniEditor;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v5, "0"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-static {p1}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v5

    invoke-interface {v5}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v5

    invoke-interface {v5}, Lcom/konka/kkinterface/tv/CommonDesk;->isEnableAdvancedPipSyncview()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-static {p1}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v5

    invoke-interface {v5}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v5

    invoke-interface {v5}, Lcom/konka/kkinterface/tv/CommonDesk;->getPipInfo()Lcom/konka/kkinterface/tv/CommonDesk$PipInfo;

    move-result-object v5

    iget v5, v5, Lcom/konka/kkinterface/tv/CommonDesk$PipInfo;->enablePip:I

    if-nez v5, :cond_1

    :cond_0
    move v5, v6

    :goto_0
    return v5

    :cond_1
    const-string v5, "activity"

    invoke-virtual {p1, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    const/4 v5, 0x2

    invoke-virtual {v0, v5}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v5

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v1, v5, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v8, "The package of top task is ===="

    invoke-direct {v5, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    sget-object v5, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->wmMuteAndFreeze:Landroid/view/WindowManager;

    if-nez v5, :cond_2

    const-string v5, "wmMuteAndFreeze is null"

    invoke-static {v5}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    const-string v5, "window"

    invoke-virtual {p1, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/WindowManager;

    sput-object v5, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->wmMuteAndFreeze:Landroid/view/WindowManager;

    :cond_2
    invoke-virtual {v1}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v8, "The top class name is ====="

    invoke-direct {v5, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    const-string v5, "com.konka.tvsettings.RootActivity"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v5

    const-string v8, "com.konka.tvsettings"

    invoke-virtual {v5, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_3

    const-string v5, "com.konka.tvsettings.screensaver.ScreenSaverActivity"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    const-string v5, "com.konka.hotkey.video3dpip.Video3DActivity"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    const-string v5, "com.konka.tvsettings.popup.SourceInfoActivity"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v7, "The top class name is , return false ===== "

    invoke-direct {v5, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    move v5, v6

    goto/16 :goto_0

    :cond_3
    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v5

    const-string v8, "browser"

    invoke-virtual {v5, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-static {p1}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v5

    invoke-interface {v5}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v5

    invoke-interface {v5}, Lcom/konka/kkinterface/tv/CommonDesk;->isEnableAdvancedPipSyncview()Z

    move-result v5

    if-eqz v5, :cond_4

    move v5, v7

    goto/16 :goto_0

    :cond_4
    move v5, v6

    goto/16 :goto_0

    :cond_5
    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v5

    const-string v8, "com.konka.avenger"

    invoke-virtual {v5, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_6

    move v5, v6

    goto/16 :goto_0

    :cond_6
    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v5

    const-string v8, "epg"

    invoke-virtual {v5, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_7

    move v5, v6

    goto/16 :goto_0

    :cond_7
    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v5

    const-string v8, "karaoke"

    invoke-virtual {v5, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_8

    move v5, v6

    goto/16 :goto_0

    :cond_8
    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v5

    const-string v8, "launcher"

    invoke-virtual {v5, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_9

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v5

    const-string v8, "avenger"

    invoke-virtual {v5, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_9

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v5

    const-string v8, "metrolauncher"

    invoke-virtual {v5, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_a

    :cond_9
    move v5, v6

    goto/16 :goto_0

    :cond_a
    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v5

    const-string v8, "doublechannelentry"

    invoke-virtual {v5, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_b

    move v5, v6

    goto/16 :goto_0

    :cond_b
    invoke-virtual {v1}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v5

    const-string v8, "PVRFullPageBrowserActivity"

    invoke-virtual {v5, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_c

    move v5, v6

    goto/16 :goto_0

    :cond_c
    invoke-virtual {v1}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v5

    const-string v8, "systemsetting"

    invoke-virtual {v5, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_d

    move v5, v6

    goto/16 :goto_0

    :cond_d
    move v5, v7

    goto/16 :goto_0
.end method

.method private isSupportSyncView(Landroid/content/Context;)Z
    .locals 6
    .param p1    # Landroid/content/Context;

    const/4 v3, 0x1

    const/4 v4, 0x0

    const-string v2, "activity"

    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v1, v2, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v5, "The package of top task is ==XXXXXXXXXX=="

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v2

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v2

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/CommonDesk;->isEnableAdvancedPipSyncview()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const-string v5, "browser"

    invoke-virtual {v2, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v2, v3

    :goto_0
    return v2

    :cond_0
    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const-string v5, "com.konka.avenger"

    invoke-virtual {v2, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    move v2, v4

    goto :goto_0

    :cond_1
    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const-string v5, "epg"

    invoke-virtual {v2, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    move v2, v4

    goto :goto_0

    :cond_2
    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const-string v5, "karaoke"

    invoke-virtual {v2, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    move v2, v4

    goto :goto_0

    :cond_3
    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const-string v5, "launcher"

    invoke-virtual {v2, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const-string v5, "avenger"

    invoke-virtual {v2, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const-string v5, "metrolauncher"

    invoke-virtual {v2, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_5

    :cond_4
    move v2, v4

    goto :goto_0

    :cond_5
    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const-string v5, "doublechannelentry"

    invoke-virtual {v2, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_6

    move v2, v4

    goto :goto_0

    :cond_6
    invoke-virtual {v1}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v2

    const-string v5, "PVRFullPageBrowserActivity"

    invoke-virtual {v2, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_7

    move v2, v4

    goto :goto_0

    :cond_7
    move v2, v3

    goto :goto_0

    :cond_8
    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const-string v5, "com.konka.mm"

    invoke-virtual {v2, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_9

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const-string v5, "com.konka.kkvideoplayer"

    invoke-virtual {v2, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_a

    :cond_9
    move v2, v3

    goto/16 :goto_0

    :cond_a
    move v2, v4

    goto/16 :goto_0
.end method

.method private procChannelUpDown(Landroid/content/Context;Ljava/lang/String;)V
    .locals 9
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;

    const/4 v8, 0x0

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/mstar/android/tv/TvPipPopManager;->isPipModeEnabled()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/mstar/android/tv/TvPipPopManager;->getSubInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v5

    sget-object v6, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_ATV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-eq v5, v6, :cond_0

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/mstar/android/tv/TvPipPopManager;->getSubInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v5

    sget-object v6, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-ne v5, v6, :cond_4

    :cond_0
    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v5

    sget-object v6, Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;->E_SUB_WINDOW:Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;

    invoke-virtual {v5, v6}, Lcom/mstar/android/tv/TvPipPopManager;->setPipDisplayFocusWindow(Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;)V

    const-string v5, "com.konka.CHANNEL_UP"

    invoke-virtual {p2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-static {p1}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v5

    invoke-interface {v5}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getChannelManagerInstance()Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v5

    invoke-interface {v5}, Lcom/konka/kkinterface/tv/ChannelDesk;->programUp()Z

    :cond_1
    :goto_0
    const/4 v3, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v5

    if-eqz v5, :cond_6

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/mstar/android/tvapi/common/PictureManager;->getPanelWidthHeight()Lcom/mstar/android/tvapi/common/vo/PanelProperty;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    :goto_1
    const/16 v2, 0x438

    if-eqz v3, :cond_2

    iget v2, v3, Lcom/mstar/android/tvapi/common/vo/PanelProperty;->height:I

    :cond_2
    div-int/lit8 v2, v2, 0x3

    invoke-static {p1}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v5

    invoke-interface {v5}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getChannelManagerInstance()Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v5

    invoke-interface {v5}, Lcom/konka/kkinterface/tv/ChannelDesk;->getCurrProgramInfo()Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    move-result-object v5

    iget v0, v5, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->number:I

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/mstar/android/tv/TvPipPopManager;->getSubInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v5

    sget-object v6, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_ATV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-ne v5, v6, :cond_3

    add-int/lit8 v0, v0, 0x1

    :cond_3
    const-string v5, "%d"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->toast:Landroid/widget/Toast;

    if-eqz v5, :cond_7

    sget-object v5, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->toast:Landroid/widget/Toast;

    invoke-virtual {v5, v4}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    :goto_2
    sget-object v5, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->toast:Landroid/widget/Toast;

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v5

    sget-object v6, Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;->E_MAIN_WINDOW:Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;

    invoke-virtual {v5, v6}, Lcom/mstar/android/tv/TvPipPopManager;->setPipDisplayFocusWindow(Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;)V

    :cond_4
    return-void

    :cond_5
    const-string v5, "com.konka.CHANNEL_DOWN"

    invoke-virtual {p2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-static {p1}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v5

    invoke-interface {v5}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getChannelManagerInstance()Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v5

    invoke-interface {v5}, Lcom/konka/kkinterface/tv/ChannelDesk;->programDown()Z

    goto :goto_0

    :cond_6
    :try_start_1
    const-string v5, "TvManger.getInstance() == null"

    invoke-static {v5}, Lcom/konka/debuginfo/logPrint;->Fatal(Ljava/lang/String;)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1

    :cond_7
    invoke-static {p1, v4, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v5

    sput-object v5, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->toast:Landroid/widget/Toast;

    sget-object v5, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->toast:Landroid/widget/Toast;

    const/16 v6, 0x35

    const/16 v7, 0x1e

    add-int/lit8 v8, v2, 0xa

    invoke-virtual {v5, v6, v7, v8}, Landroid/widget/Toast;->setGravity(III)V

    goto :goto_2
.end method

.method private setFreeze(Landroid/content/Context;Z)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Z

    iget-object v0, p0, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->pictureskin:Lcom/mstar/android/tv/TvPictureManager;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tv/TvPictureManager;->getInstance()Lcom/mstar/android/tv/TvPictureManager;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->pictureskin:Lcom/mstar/android/tv/TvPictureManager;

    :cond_0
    invoke-direct {p0, p1}, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->showMuteFreezeView(Landroid/content/Context;)V

    sget-boolean v0, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->freezeFlag:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->pictureskin:Lcom/mstar/android/tv/TvPictureManager;

    invoke-virtual {v0}, Lcom/mstar/android/tv/TvPictureManager;->freezeImage()Z

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->pictureskin:Lcom/mstar/android/tv/TvPictureManager;

    invoke-virtual {v0}, Lcom/mstar/android/tv/TvPictureManager;->unFreezeImage()Z

    goto :goto_0
.end method

.method private setMute(Landroid/content/Context;Z)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Z

    iget-object v0, p0, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->audioskin:Lcom/mstar/android/tv/TvAudioManager;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tv/TvAudioManager;->getInstance()Lcom/mstar/android/tv/TvAudioManager;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->audioskin:Lcom/mstar/android/tv/TvAudioManager;

    :cond_0
    invoke-direct {p0, p1}, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->showMuteFreezeView(Landroid/content/Context;)V

    return-void
.end method

.method private showMuteFreezeView(Landroid/content/Context;)V
    .locals 10
    .param p1    # Landroid/content/Context;

    const/4 v9, -0x2

    const v8, 0x7f0a0031    # com.konka.hotkey.R.id.iv_mute

    const v7, 0x7f0a0030    # com.konka.hotkey.R.id.iv_freeze

    const/4 v6, 0x1

    const/4 v5, 0x0

    sget-object v3, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->wmMuteAndFreeze:Landroid/view/WindowManager;

    if-nez v3, :cond_0

    const-string v3, "wmMuteAndFreeze is null"

    invoke-static {v3}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    const-string v3, "window"

    invoke-virtual {p1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/WindowManager;

    sput-object v3, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->wmMuteAndFreeze:Landroid/view/WindowManager;

    :cond_0
    sget-object v3, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->mMuteAndFreezeView:Landroid/view/View;

    if-nez v3, :cond_2

    const-string v3, "mMuteAndFreezeView is null"

    invoke-static {v3}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    sget-object v3, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->mParamsMuteAndFreeze:Landroid/view/WindowManager$LayoutParams;

    if-nez v3, :cond_1

    const-string v3, "mParamsMuteAndFreeze is null"

    invoke-static {v3}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    new-instance v3, Landroid/view/WindowManager$LayoutParams;

    invoke-direct {v3}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    sput-object v3, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->mParamsMuteAndFreeze:Landroid/view/WindowManager$LayoutParams;

    sget-object v3, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->mParamsMuteAndFreeze:Landroid/view/WindowManager$LayoutParams;

    const/16 v4, 0x7d6

    iput v4, v3, Landroid/view/WindowManager$LayoutParams;->type:I

    sget-object v3, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->mParamsMuteAndFreeze:Landroid/view/WindowManager$LayoutParams;

    iput v9, v3, Landroid/view/WindowManager$LayoutParams;->width:I

    sget-object v3, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->mParamsMuteAndFreeze:Landroid/view/WindowManager$LayoutParams;

    iput v9, v3, Landroid/view/WindowManager$LayoutParams;->height:I

    sget-object v3, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->mParamsMuteAndFreeze:Landroid/view/WindowManager$LayoutParams;

    iput v6, v3, Landroid/view/WindowManager$LayoutParams;->format:I

    sget-object v3, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->mParamsMuteAndFreeze:Landroid/view/WindowManager$LayoutParams;

    const/16 v4, 0x53

    iput v4, v3, Landroid/view/WindowManager$LayoutParams;->gravity:I

    :cond_1
    const-string v3, "layout_inflater"

    invoke-virtual {p1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    const v3, 0x7f03000b    # com.konka.hotkey.R.layout.muteandfreeze

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    sput-object v3, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->mMuteAndFreezeView:Landroid/view/View;

    :cond_2
    sget-boolean v3, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->muteFlag:Z

    if-eqz v3, :cond_5

    sget-boolean v3, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->freezeFlag:Z

    if-eqz v3, :cond_5

    sget-object v3, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->mMuteAndFreezeView:Landroid/view/View;

    invoke-virtual {v3, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    sget-object v3, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->mMuteAndFreezeView:Landroid/view/View;

    invoke-virtual {v3, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    sget-boolean v3, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->bMuteFreezeOnShow:Z

    if-eqz v3, :cond_4

    sget-object v3, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->wmMuteAndFreeze:Landroid/view/WindowManager;

    sget-object v4, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->mMuteAndFreezeView:Landroid/view/View;

    sget-object v5, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->mParamsMuteAndFreeze:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v3, v4, v5}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    :cond_3
    :goto_0
    return-void

    :cond_4
    const-string v3, "It should not been running here"

    invoke-static {v3}, Lcom/konka/debuginfo/logPrint;->Fatal(Ljava/lang/String;)V

    sput-boolean v6, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->bMuteFreezeOnShow:Z

    sget-object v3, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->wmMuteAndFreeze:Landroid/view/WindowManager;

    sget-object v4, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->mMuteAndFreezeView:Landroid/view/View;

    sget-object v5, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->mParamsMuteAndFreeze:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v3, v4, v5}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    :cond_5
    sget-boolean v3, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->muteFlag:Z

    if-eqz v3, :cond_7

    sget-boolean v3, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->freezeFlag:Z

    if-nez v3, :cond_7

    sget-object v3, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->mMuteAndFreezeView:Landroid/view/View;

    invoke-virtual {v3, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    sget-object v3, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->mMuteAndFreezeView:Landroid/view/View;

    invoke-virtual {v3, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    const/4 v3, 0x4

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    sget-boolean v3, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->bMuteFreezeOnShow:Z

    if-eqz v3, :cond_6

    sget-object v3, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->wmMuteAndFreeze:Landroid/view/WindowManager;

    sget-object v4, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->mMuteAndFreezeView:Landroid/view/View;

    sget-object v5, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->mParamsMuteAndFreeze:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v3, v4, v5}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    :cond_6
    sput-boolean v6, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->bMuteFreezeOnShow:Z

    sget-object v3, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->wmMuteAndFreeze:Landroid/view/WindowManager;

    sget-object v4, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->mMuteAndFreezeView:Landroid/view/View;

    sget-object v5, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->mParamsMuteAndFreeze:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v3, v4, v5}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    :cond_7
    sget-boolean v3, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->muteFlag:Z

    if-nez v3, :cond_9

    sget-boolean v3, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->freezeFlag:Z

    if-eqz v3, :cond_9

    sget-object v3, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->mMuteAndFreezeView:Landroid/view/View;

    invoke-virtual {v3, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    sget-object v3, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->mMuteAndFreezeView:Landroid/view/View;

    invoke-virtual {v3, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    sget-boolean v3, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->bMuteFreezeOnShow:Z

    if-eqz v3, :cond_8

    sget-object v3, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->wmMuteAndFreeze:Landroid/view/WindowManager;

    sget-object v4, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->mMuteAndFreezeView:Landroid/view/View;

    sget-object v5, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->mParamsMuteAndFreeze:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v3, v4, v5}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    :cond_8
    sput-boolean v6, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->bMuteFreezeOnShow:Z

    sget-object v3, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->wmMuteAndFreeze:Landroid/view/WindowManager;

    sget-object v4, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->mMuteAndFreezeView:Landroid/view/View;

    sget-object v5, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->mParamsMuteAndFreeze:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v3, v4, v5}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_0

    :cond_9
    sget-boolean v3, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->bMuteFreezeOnShow:Z

    if-eqz v3, :cond_3

    sput-boolean v5, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->bMuteFreezeOnShow:Z

    sget-object v3, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->wmMuteAndFreeze:Landroid/view/WindowManager;

    sget-object v4, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->mMuteAndFreezeView:Landroid/view/View;

    invoke-interface {v3, v4}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    goto/16 :goto_0
.end method

.method private voiceCtlVolumeFunc(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const/4 v5, -0x1

    const/4 v2, 0x0

    const-string v3, "CommandId"

    invoke-virtual {p2, v3, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "get command: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    sget-boolean v3, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->muteFlag:Z

    if-eqz v3, :cond_1

    :goto_1
    sput-boolean v2, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->muteFlag:Z

    sget-boolean v2, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->muteFlag:Z

    invoke-direct {p0, p1, v2}, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->setMute(Landroid/content/Context;Z)V

    goto :goto_0

    :cond_1
    const/4 v2, 0x1

    goto :goto_1

    :pswitch_1
    sget-boolean v3, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->muteFlag:Z

    if-eqz v3, :cond_2

    sput-boolean v2, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->muteFlag:Z

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    sget-boolean v3, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->muteFlag:Z

    invoke-direct {p0, v2, v3}, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->setMute(Landroid/content/Context;Z)V

    :cond_2
    const-string v2, "Arg"

    invoke-virtual {p2, v2, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    goto :goto_0

    :pswitch_2
    sget-boolean v3, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->muteFlag:Z

    if-eqz v3, :cond_0

    sput-boolean v2, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->muteFlag:Z

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    sget-boolean v3, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->muteFlag:Z

    invoke-direct {p0, v2, v3}, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->setMute(Landroid/content/Context;Z)V

    goto :goto_0

    :pswitch_3
    sget-boolean v3, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->muteFlag:Z

    if-eqz v3, :cond_0

    sput-boolean v2, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->muteFlag:Z

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    sget-boolean v3, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->muteFlag:Z

    invoke-direct {p0, v2, v3}, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->setMute(Landroid/content/Context;Z)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x46
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public Display(Landroid/content/Context;Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;I)V
    .locals 5
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;
    .param p3    # I

    move-object v1, p1

    const/4 v0, 0x0

    const-string v2, "Display,"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "UpgradeInfo========="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->$SWITCH_TABLE$com$konka$hotkey$HotkeyBroadcastReceiver$UpgradeInfo()[I

    move-result-object v2

    invoke-virtual {p2}, Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    :goto_0
    const/4 v2, 0x0

    invoke-static {p1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    return-void

    :pswitch_0
    packed-switch p3, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080058    # com.konka.hotkey.R.string.str_factory_upgradeok

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f080055    # com.konka.hotkey.R.string.str_factory_macaddr

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v3, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->macstr:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_3
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f080056    # com.konka.hotkey.R.string.str_factory_index

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f080057    # com.konka.hotkey.R.string.str_factory_offset

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->offsetStr:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_4
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080059    # com.konka.hotkey.R.string.str_factory_upgradenofile

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_5
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08005a    # com.konka.hotkey.R.string.str_factory_upgradereadfilefail

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_6
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08005b    # com.konka.hotkey.R.string.str_factory_upgradedataover

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_6
        :pswitch_4
        :pswitch_5
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 35
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    new-instance v30, Ljava/lang/StringBuilder;

    const-string v31, "========receive the broadcast===="

    invoke-direct/range {v30 .. v31}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v30

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    invoke-static/range {v30 .. v30}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    const-string v30, "konka.voice.control.action.VOLUMESET"

    move-object/from16 v0, v30

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_1

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v30

    move-object/from16 v0, p0

    move-object/from16 v1, v30

    move-object/from16 v2, p2

    invoke-direct {v0, v1, v2}, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->voiceCtlVolumeFunc(Landroid/content/Context;Landroid/content/Intent;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v30, "com.konka.tvsettings.STANDBY_NO_OPERATIONS"

    move-object/from16 v0, v30

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-nez v30, :cond_2

    const-string v30, "com.android.tv.hotkey.TIMER_OFF_WARNING"

    move-object/from16 v0, v30

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-nez v30, :cond_2

    const-string v30, "com.konka.NO_OPERATION_SHUTDOWN"

    move-object/from16 v0, v30

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-nez v30, :cond_2

    const-string v30, "com.konka.tv.hotkey.NO_SIGNAL"

    move-object/from16 v0, v30

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_3

    :cond_2
    const-string v30, "---------------COUNTDOWN----------------"

    invoke-static/range {v30 .. v30}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    new-instance v17, Landroid/content/Intent;

    const-string v30, "com.konka.hotkey.intent.action.CountdownActivity"

    move-object/from16 v0, v17

    move-object/from16 v1, v30

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v30, 0x10200000

    move-object/from16 v0, v17

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const-string v30, "action"

    move-object/from16 v0, v17

    move-object/from16 v1, v30

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v30, "CountDownValue"

    const-string v31, "CountDownValue"

    const/16 v32, 0x3c

    move-object/from16 v0, p2

    move-object/from16 v1, v31

    move/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v31

    move-object/from16 v0, v17

    move-object/from16 v1, v30

    move/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_3
    const-string v30, "com.konka.tv.hotkey.AUDIO"

    move-object/from16 v0, v30

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_4

    const-string v30, "---------------AUDIO----------------"

    invoke-static/range {v30 .. v30}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    new-instance v15, Landroid/content/Intent;

    const-string v30, "com.konka.hotkey.intent.action.SoundActivity"

    move-object/from16 v0, v30

    invoke-direct {v15, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v30, "KeyCode"

    const/16 v31, 0xfb

    move-object/from16 v0, v30

    move/from16 v1, v31

    invoke-virtual {v15, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/high16 v30, 0x10200000

    move/from16 v0, v30

    invoke-virtual {v15, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_4
    const-string v30, "com.konka.tv.hotkey.PICTURE"

    move-object/from16 v0, v30

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_5

    const-string v30, "---------------PICTURE----------------"

    invoke-static/range {v30 .. v30}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    new-instance v18, Landroid/content/Intent;

    const-string v30, "com.konka.hotkey.intent.action.PictureActivity"

    move-object/from16 v0, v18

    move-object/from16 v1, v30

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v30, 0x10200000

    move-object/from16 v0, v18

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_5
    const-string v30, "com.konka.tv.hotkey.ZOOM"

    move-object/from16 v0, v30

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_6

    const-string v30, "---------------ZOOM----------------"

    invoke-static/range {v30 .. v30}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    new-instance v20, Landroid/content/Intent;

    const-string v30, "com.konka.hotkey.intent.action.ZoomActivity"

    move-object/from16 v0, v20

    move-object/from16 v1, v30

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v30, "KeyCode"

    const/16 v31, 0x10e

    move-object/from16 v0, v20

    move-object/from16 v1, v30

    move/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/high16 v30, 0x10200000

    move-object/from16 v0, v20

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_6
    const-string v30, "com.mstar.tv.service.SLEEP"

    move-object/from16 v0, v30

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_7

    const-string v30, "---------------SLEEP----------------"

    invoke-static/range {v30 .. v30}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    new-instance v19, Landroid/content/Intent;

    const-string v30, "com.konka.hotkey.intent.action.SleepActivity"

    move-object/from16 v0, v19

    move-object/from16 v1, v30

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v30, 0x10200000

    move-object/from16 v0, v19

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_7
    const-string v30, "com.mstar.tv.service.POWER"

    move-object/from16 v0, v30

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_8

    const-string v30, "---------------POWER----------------"

    invoke-static/range {v30 .. v30}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    new-instance v19, Landroid/content/Intent;

    const-string v30, "com.konka.hotkey.intent.action.SleepActivity"

    move-object/from16 v0, v19

    move-object/from16 v1, v30

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v30, 0x10200000

    move-object/from16 v0, v19

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_8
    const-string v30, "com.konka.tv.hotkey.service.VOLUMEADD"

    move-object/from16 v0, v30

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_9

    const-string v30, "---------------VOLUMEADD----------------"

    invoke-static/range {v30 .. v30}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    sget-boolean v30, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->muteFlag:Z

    if-eqz v30, :cond_0

    const/16 v30, 0x0

    sput-boolean v30, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->muteFlag:Z

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v30

    sget-boolean v31, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->muteFlag:Z

    move-object/from16 v0, p0

    move-object/from16 v1, v30

    move/from16 v2, v31

    invoke-direct {v0, v1, v2}, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->setMute(Landroid/content/Context;Z)V

    goto/16 :goto_0

    :cond_9
    const-string v30, "com.konka.tv.hokey.service.VOLUMEDEC"

    move-object/from16 v0, v30

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_a

    const-string v30, "---------------VOLUMEDEC----------------"

    invoke-static/range {v30 .. v30}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    sget-boolean v30, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->muteFlag:Z

    if-eqz v30, :cond_0

    const/16 v30, 0x0

    sput-boolean v30, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->muteFlag:Z

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v30

    sget-boolean v31, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->muteFlag:Z

    move-object/from16 v0, p0

    move-object/from16 v1, v30

    move/from16 v2, v31

    invoke-direct {v0, v1, v2}, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->setMute(Landroid/content/Context;Z)V

    goto/16 :goto_0

    :cond_a
    const-string v30, "com.mstar.tv.service.EPGCOUNTDOWN"

    move-object/from16 v0, v30

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_b

    new-instance v30, Ljava/lang/StringBuilder;

    const-string v31, "here to start a activity=========="

    invoke-direct/range {v30 .. v31}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-boolean v31, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->RECEIVE_COUNT_DOWN:Z

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    invoke-static/range {v30 .. v30}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    sget-boolean v30, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->RECEIVE_COUNT_DOWN:Z

    if-eqz v30, :cond_0

    const-string v30, "leftTime"

    const/16 v31, 0x3c

    move-object/from16 v0, p2

    move-object/from16 v1, v30

    move/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v24

    new-instance v30, Ljava/lang/StringBuilder;

    const-string v31, "here to start a leftTime=========="

    invoke-direct/range {v30 .. v31}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v30

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    invoke-static/range {v30 .. v30}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    const/16 v30, 0xa

    move/from16 v0, v24

    move/from16 v1, v30

    if-ne v0, v1, :cond_0

    const/16 v30, 0x0

    sput-boolean v30, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->RECEIVE_COUNT_DOWN:Z

    new-instance v16, Landroid/content/Intent;

    const-string v30, "com.konka.hotkey.intent.action.EpgOrderInfoActivity"

    move-object/from16 v0, v16

    move-object/from16 v1, v30

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v30, 0x10200000

    move-object/from16 v0, v16

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    const-string v30, "here to start a activity=========="

    invoke-static/range {v30 .. v30}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_b
    const-string v30, "com.mstar.tv.service.3D"

    move-object/from16 v0, v30

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_f

    const-string v30, "---------------3D_MODE----------------"

    invoke-static/range {v30 .. v30}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-direct/range {p0 .. p1}, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->isReadyShow3D(Landroid/content/Context;)Z

    move-result v30

    if-eqz v30, :cond_c

    invoke-static {}, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->get4K2KMode()Z

    move-result v30

    if-nez v30, :cond_c

    new-instance v13, Landroid/content/Intent;

    const-string v30, "com.konka.hotkey.intent.action.3DActivity"

    move-object/from16 v0, v30

    invoke-direct {v13, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v30, "IS_SUPPORT_SYNC_VIEW"

    invoke-direct/range {p0 .. p1}, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->isSupportSyncView(Landroid/content/Context;)Z

    move-result v31

    move-object/from16 v0, v30

    move/from16 v1, v31

    invoke-virtual {v13, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const/high16 v30, 0x10200000

    move/from16 v0, v30

    invoke-virtual {v13, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_c
    invoke-static {}, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->get4K2KMode()Z

    move-result v30

    if-eqz v30, :cond_d

    const v30, 0x7f08005c    # com.konka.hotkey.R.string.warning_4k2knotsupport

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v30

    invoke-virtual {v0, v1, v2}, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->popToast(Landroid/content/Context;I)V

    goto/16 :goto_0

    :cond_d
    const-string v30, "activity"

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/ActivityManager;

    const/16 v30, 0x2

    move/from16 v0, v30

    invoke-virtual {v4, v0}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v30

    const/16 v31, 0x0

    invoke-interface/range {v30 .. v31}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v30

    check-cast v30, Landroid/app/ActivityManager$RunningTaskInfo;

    move-object/from16 v0, v30

    iget-object v5, v0, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v5}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v30

    const-string v31, "com.konka.tvsettings.RootActivity"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-nez v30, :cond_e

    invoke-virtual {v5}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v30

    const-string v31, "com.konka.tvsettings"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v30

    if-eqz v30, :cond_e

    const v30, 0x7f08005d    # com.konka.hotkey.R.string.warning_exitthemenu

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v30

    invoke-virtual {v0, v1, v2}, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->popToast(Landroid/content/Context;I)V

    goto/16 :goto_0

    :cond_e
    const v30, 0x7f080040    # com.konka.hotkey.R.string.warning_notsupport_3d

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v30

    invoke-virtual {v0, v1, v2}, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->popToast(Landroid/content/Context;I)V

    goto/16 :goto_0

    :cond_f
    const-string v30, "com.konka.tv.hotkey.service.MUTE"

    move-object/from16 v0, v30

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_10

    const-string v30, "---------------MUTE----------------"

    invoke-static/range {v30 .. v30}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    const/16 v30, 0x1

    sput-boolean v30, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->muteFlag:Z

    sget-boolean v30, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->muteFlag:Z

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v30

    invoke-direct {v0, v1, v2}, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->setMute(Landroid/content/Context;Z)V

    goto/16 :goto_0

    :cond_10
    const-string v30, "com.konka.tv.hotkey.service.FREEZE"

    move-object/from16 v0, v30

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_12

    const-string v30, "--------------FREEZE---------------"

    invoke-static/range {v30 .. v30}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    sget-boolean v30, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->freezeFlag:Z

    if-eqz v30, :cond_11

    const/16 v30, 0x0

    :goto_1
    sput-boolean v30, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->freezeFlag:Z

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v30

    sget-boolean v31, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->freezeFlag:Z

    move-object/from16 v0, p0

    move-object/from16 v1, v30

    move/from16 v2, v31

    invoke-direct {v0, v1, v2}, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->setFreeze(Landroid/content/Context;Z)V

    goto/16 :goto_0

    :cond_11
    const/16 v30, 0x1

    goto :goto_1

    :cond_12
    const-string v30, "com.konka.tv.hotkey.service.UNMUTE"

    move-object/from16 v0, v30

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_13

    const-string v30, "---------------UNMUTE----------------"

    invoke-static/range {v30 .. v30}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    const/16 v30, 0x0

    sput-boolean v30, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->muteFlag:Z

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v30

    const/16 v31, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v30

    move/from16 v2, v31

    invoke-direct {v0, v1, v2}, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->setMute(Landroid/content/Context;Z)V

    goto/16 :goto_0

    :cond_13
    const-string v30, "com.konka.tv.hotkey.service.UNFREEZE"

    move-object/from16 v0, v30

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_14

    const-string v30, "---------------UNFREEZE----------------"

    invoke-static/range {v30 .. v30}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    sget-boolean v30, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->freezeFlag:Z

    if-eqz v30, :cond_0

    const/16 v30, 0x0

    sput-boolean v30, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->freezeFlag:Z

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v30

    sget-boolean v31, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->freezeFlag:Z

    move-object/from16 v0, p0

    move-object/from16 v1, v30

    move/from16 v2, v31

    invoke-direct {v0, v1, v2}, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->setFreeze(Landroid/content/Context;Z)V

    goto/16 :goto_0

    :cond_14
    const-string v30, "com.konka.PAD_SOUNDOUTPUT_ENABLE"

    move-object/from16 v0, v30

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_15

    const-string v30, "---------------PAD_SOUNDOUTPUT_ENABLE----------------"

    invoke-static/range {v30 .. v30}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-static {}, Lcom/mstar/android/tv/TvCommonManager;->getInstance()Lcom/mstar/android/tv/TvCommonManager;

    move-result-object v30

    const/16 v31, 0x1

    invoke-virtual/range {v30 .. v31}, Lcom/mstar/android/tv/TvCommonManager;->setHPPortStatus(Z)V

    goto/16 :goto_0

    :cond_15
    const-string v30, "com.konka.PAD_SOUNDOUTPUT_DISABLE"

    move-object/from16 v0, v30

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_16

    const-string v30, "---------------PAD_SOUNDOUTPUT_DISABLE----------------"

    invoke-static/range {v30 .. v30}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-static {}, Lcom/mstar/android/tv/TvCommonManager;->getInstance()Lcom/mstar/android/tv/TvCommonManager;

    move-result-object v30

    const/16 v31, 0x0

    invoke-virtual/range {v30 .. v31}, Lcom/mstar/android/tv/TvCommonManager;->setHPPortStatus(Z)V

    goto/16 :goto_0

    :cond_16
    const-string v30, "com.android.server.tv.TIME_EVENT_LAST_MINUTE_WARN"

    move-object/from16 v0, v30

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_17

    const-string v30, "---------------PAD_SOUNDOUTPUT_DISABLE----------------"

    invoke-static/range {v30 .. v30}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    new-instance v29, Landroid/content/Intent;

    const-string v30, "com.konka.hotkey.intent.action.TVCounterActivity"

    invoke-direct/range {v29 .. v30}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v30, 0x10000000

    invoke-virtual/range {v29 .. v30}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-object/from16 v0, p1

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_17
    const-string v30, "com.konka.CHANNEL_UP"

    move-object/from16 v0, v30

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-nez v30, :cond_18

    const-string v30, "com.konka.CHANNEL_DOWN"

    move-object/from16 v0, v30

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_19

    :cond_18
    const-string v30, "---------------CHANNEL_UP/CHANNEL_DOWN for DualView/PIP----------------"

    invoke-static/range {v30 .. v30}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v3}, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->procChannelUpDown(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_19
    const-string v30, "com.konka.tv.hotkey.factorydefault"

    move-object/from16 v0, v30

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_1a

    const-string v30, "start factory default!!!"

    invoke-static/range {v30 .. v30}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-static/range {p1 .. p1}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v30

    invoke-interface/range {v30 .. v30}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getSettingManagerInstance()Lcom/konka/kkinterface/tv/SettingDesk;

    move-result-object v30

    invoke-interface/range {v30 .. v30}, Lcom/konka/kkinterface/tv/SettingDesk;->ExecFactoryDefault()V

    invoke-static {}, Lcom/mstar/android/tv/TvCommonManager;->getInstance()Lcom/mstar/android/tv/TvCommonManager;

    move-result-object v30

    const-string v31, "reboot"

    invoke-virtual/range {v30 .. v31}, Lcom/mstar/android/tv/TvCommonManager;->rebootSystem(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_1a
    const-string v30, "com.konka.tv.hotkey.sound_balance"

    move-object/from16 v0, v30

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_1e

    const-string v30, "hotkey set sound balance!!!"

    invoke-static/range {v30 .. v30}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-static/range {p1 .. p1}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v30

    invoke-interface/range {v30 .. v30}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getSoundManagerInstance()Lcom/konka/kkinterface/tv/SoundDesk;

    move-result-object v30

    invoke-interface/range {v30 .. v30}, Lcom/konka/kkinterface/tv/SoundDesk;->getBalance()S

    move-result v30

    sput-short v30, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->iSoundBalance:S

    sget-short v30, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->iSoundBalance:S

    if-nez v30, :cond_1c

    const/16 v30, 0x32

    sput-short v30, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->iSoundBalance:S

    :cond_1b
    :goto_2
    invoke-static/range {p1 .. p1}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v30

    invoke-interface/range {v30 .. v30}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getSoundManagerInstance()Lcom/konka/kkinterface/tv/SoundDesk;

    move-result-object v30

    sget-short v31, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->iSoundBalance:S

    invoke-interface/range {v30 .. v31}, Lcom/konka/kkinterface/tv/SoundDesk;->setBalance(S)Z

    new-instance v30, Ljava/lang/StringBuilder;

    const-string v31, "SoundBalance:"

    invoke-direct/range {v30 .. v31}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-short v31, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->iSoundBalance:S

    add-int/lit8 v31, v31, -0x32

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    const/16 v31, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    move/from16 v2, v31

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Landroid/widget/Toast;->show()V

    new-instance v22, Landroid/content/Intent;

    const-string v30, "com.konka.tv.hotkey.service.UNMUTE"

    move-object/from16 v0, v22

    move-object/from16 v1, v30

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_1c
    sget-short v30, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->iSoundBalance:S

    if-lez v30, :cond_1d

    sget-short v30, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->iSoundBalance:S

    const/16 v31, 0x32

    move/from16 v0, v30

    move/from16 v1, v31

    if-gt v0, v1, :cond_1d

    const/16 v30, 0x64

    sput-short v30, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->iSoundBalance:S

    goto :goto_2

    :cond_1d
    sget-short v30, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->iSoundBalance:S

    const/16 v31, 0x32

    move/from16 v0, v30

    move/from16 v1, v31

    if-le v0, v1, :cond_1b

    sget-short v30, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->iSoundBalance:S

    const/16 v31, 0x64

    move/from16 v0, v30

    move/from16 v1, v31

    if-gt v0, v1, :cond_1b

    const/16 v30, 0x0

    sput-short v30, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->iSoundBalance:S

    goto :goto_2

    :cond_1e
    const-string v30, "com.konka.tv.hotkey.backlight_test"

    move-object/from16 v0, v30

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_22

    const-string v30, "hotkey set backlight test"

    invoke-static/range {v30 .. v30}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-static/range {p1 .. p1}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v30

    invoke-interface/range {v30 .. v30}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getPictureManagerInstance()Lcom/konka/kkinterface/tv/PictureDesk;

    move-result-object v30

    invoke-interface/range {v30 .. v30}, Lcom/konka/kkinterface/tv/PictureDesk;->GetBacklight()S

    move-result v30

    sput-short v30, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->iBackLight:S

    sget-short v30, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->iBackLight:S

    if-nez v30, :cond_20

    const/16 v30, 0x32

    sput-short v30, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->iBackLight:S

    :cond_1f
    :goto_3
    invoke-static/range {p1 .. p1}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v30

    invoke-interface/range {v30 .. v30}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getPictureManagerInstance()Lcom/konka/kkinterface/tv/PictureDesk;

    move-result-object v30

    sget-short v31, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->iBackLight:S

    invoke-interface/range {v30 .. v31}, Lcom/konka/kkinterface/tv/PictureDesk;->SetBacklight(S)Z

    new-instance v30, Ljava/lang/StringBuilder;

    const-string v31, "BackLight:"

    invoke-direct/range {v30 .. v31}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-short v31, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->iBackLight:S

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    const/16 v31, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    move/from16 v2, v31

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    :cond_20
    sget-short v30, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->iBackLight:S

    if-lez v30, :cond_21

    sget-short v30, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->iBackLight:S

    const/16 v31, 0x32

    move/from16 v0, v30

    move/from16 v1, v31

    if-gt v0, v1, :cond_21

    const/16 v30, 0x64

    sput-short v30, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->iBackLight:S

    goto :goto_3

    :cond_21
    sget-short v30, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->iBackLight:S

    const/16 v31, 0x32

    move/from16 v0, v30

    move/from16 v1, v31

    if-le v0, v1, :cond_1f

    sget-short v30, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->iBackLight:S

    const/16 v31, 0x64

    move/from16 v0, v30

    move/from16 v1, v31

    if-gt v0, v1, :cond_1f

    const/16 v30, 0x0

    sput-short v30, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->iBackLight:S

    goto :goto_3

    :cond_22
    const-string v30, "com.konka.tv.hotkey.software_update"

    move-object/from16 v0, v30

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_23

    :try_start_0
    const-string v30, "setEnvironment"

    const-string v31, "upgrade_mode usb"

    invoke-static/range {v30 .. v31}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v30

    const-string v31, "upgrade_mode"

    const-string v32, "usb"

    invoke-virtual/range {v30 .. v32}, Lcom/mstar/android/tvapi/common/TvManager;->setEnvironment(Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_4
    const-string v30, "SoftWare_Update"

    const-string v31, "rebootsystem begin"

    invoke-static/range {v30 .. v31}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvCommonManager;->getInstance()Lcom/mstar/android/tv/TvCommonManager;

    move-result-object v30

    const-string v31, "reboot"

    invoke-virtual/range {v30 .. v31}, Lcom/mstar/android/tv/TvCommonManager;->rebootSystem(Ljava/lang/String;)V

    const-string v30, "SoftWare_Update"

    const-string v31, "rebootsystem end"

    invoke-static/range {v30 .. v31}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :catch_0
    move-exception v9

    invoke-virtual {v9}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_4

    :cond_23
    const-string v30, "com.konka.tv.hotkey.burn_mac"

    move-object/from16 v0, v30

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_24

    const/4 v12, 0x0

    const/16 v30, 0x6

    move/from16 v0, v30

    new-array v8, v0, [B

    sget-object v30, Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeMode;->UPGRADE_MODE_USB:Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeMode;

    move-object/from16 v0, v30

    invoke-static {v0, v8}, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->Write_MACaddr(Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeMode;[B)Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;

    move-result-object v12

    const/16 v30, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v30

    invoke-virtual {v0, v1, v12, v2}, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->Display(Landroid/content/Context;Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;I)V

    goto/16 :goto_0

    :cond_24
    const-string v30, "com.konka.tv.hotkey.burn_hdcpkey"

    move-object/from16 v0, v30

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_28

    const/4 v11, 0x0

    const/16 v30, 0x130

    move/from16 v0, v30

    new-array v7, v0, [B

    new-instance v26, Ljava/util/HashMap;

    invoke-direct/range {v26 .. v26}, Ljava/util/HashMap;-><init>()V

    sget-object v30, Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeMode;->UPGRADE_MODE_USB:Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeMode;

    move-object/from16 v0, v30

    move-object/from16 v1, v26

    invoke-static {v0, v1, v7}, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->Write_HDCPKey(Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeMode;Ljava/util/HashMap;[B)Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;

    move-result-object v11

    sget-object v30, Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;->UPGRADE_OK:Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;

    move-object/from16 v0, v30

    if-ne v11, v0, :cond_25

    const-string v30, "index"

    move-object/from16 v0, v26

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v30

    check-cast v30, Ljava/lang/Double;

    invoke-virtual/range {v30 .. v30}, Ljava/lang/Double;->intValue()I

    move-result v30

    sput v30, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->index:I

    const-string v30, "size"

    move-object/from16 v0, v26

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v30

    check-cast v30, Ljava/lang/Double;

    invoke-virtual/range {v30 .. v30}, Ljava/lang/Double;->intValue()I

    move-result v28

    const-string v30, "offset"

    move-object/from16 v0, v26

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v30

    check-cast v30, Ljava/lang/Double;

    invoke-virtual/range {v30 .. v30}, Ljava/lang/Double;->intValue()I

    move-result v27

    const/4 v10, 0x0

    :goto_5
    move/from16 v0, v28

    if-lt v10, v0, :cond_26

    const-string v30, "0x%08x"

    const/16 v31, 0x1

    move/from16 v0, v31

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v31, v0

    const/16 v32, 0x0

    invoke-static/range {v27 .. v27}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v33

    aput-object v33, v31, v32

    invoke-static/range {v30 .. v31}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v30

    sput-object v30, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->offsetStr:Ljava/lang/String;

    :cond_25
    const/16 v30, 0x2

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v30

    invoke-virtual {v0, v1, v11, v2}, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->Display(Landroid/content/Context;Lcom/konka/hotkey/HotkeyBroadcastReceiver$UpgradeInfo;I)V

    goto/16 :goto_0

    :cond_26
    const/16 v23, 0x0

    :goto_6
    const/16 v30, 0x10

    move/from16 v0, v23

    move/from16 v1, v30

    if-lt v0, v1, :cond_27

    sget-object v30, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual/range {v30 .. v30}, Ljava/io/PrintStream;->println()V

    add-int/lit8 v10, v10, 0x10

    goto :goto_5

    :cond_27
    sget-object v30, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v31, "%02X "

    const/16 v32, 0x1

    move/from16 v0, v32

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v32, v0

    const/16 v33, 0x0

    add-int v34, v10, v23

    aget-byte v34, v7, v34

    invoke-static/range {v34 .. v34}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v34

    aput-object v34, v32, v33

    invoke-virtual/range {v30 .. v32}, Ljava/io/PrintStream;->printf(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintStream;

    add-int/lit8 v23, v23, 0x1

    goto :goto_6

    :cond_28
    const-string v30, "com.konka.tv.hotkey.burn_sn"

    move-object/from16 v0, v30

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_29

    const-string v30, "start the activity=====android.settings.NETFACTORYAdjustViewHolder"

    invoke-static/range {v30 .. v30}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    new-instance v14, Landroid/content/Intent;

    const-string v30, "android.settings.NETFACTORYAdjustViewHolder"

    move-object/from16 v0, v30

    invoke-direct {v14, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v30, 0x10200000

    move/from16 v0, v30

    invoke-virtual {v14, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_29
    const-string v30, "com.konka.tv.hotkey.enter_factory"

    move-object/from16 v0, v30

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_2a

    const-string v30, "start the activity=====com.konka.factory.MainmenuActivity"

    invoke-static/range {v30 .. v30}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    new-instance v21, Landroid/content/Intent;

    invoke-direct/range {v21 .. v21}, Landroid/content/Intent;-><init>()V

    const-string v30, "com.konka.factory"

    const-string v31, "com.konka.factory.MainmenuActivity"

    move-object/from16 v0, v21

    move-object/from16 v1, v30

    move-object/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v30, 0x10200000

    move-object/from16 v0, v21

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_2a
    const-string v30, "com.konka.tv.hotkey.setmic"

    move-object/from16 v0, v30

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_2b

    new-instance v14, Landroid/content/Intent;

    const-string v30, "android.settings.SET_MICROPHONE"

    move-object/from16 v0, v30

    invoke-direct {v14, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v30, 0x10200000

    move/from16 v0, v30

    invoke-virtual {v14, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_2b
    const-string v30, "com.konka.tv.hotkey.language_setting"

    move-object/from16 v0, v30

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_2c

    new-instance v14, Landroid/content/Intent;

    const-string v30, "android.settings.INPUT_METHOD_SUBTYPE_SETTINGS"

    move-object/from16 v0, v30

    invoke-direct {v14, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v30, 0x10200000

    move/from16 v0, v30

    invoke-virtual {v14, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_2c
    const-string v30, "com.konka.tv.hotkey.net_setting"

    move-object/from16 v0, v30

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_2d

    new-instance v14, Landroid/content/Intent;

    const-string v30, "android.net.wifi.PICK_WIFI_NETWORK"

    move-object/from16 v0, v30

    invoke-direct {v14, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v30, 0x10200000

    move/from16 v0, v30

    invoke-virtual {v14, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_2d
    const-string v30, "com.konka.tv.hotkey.volume_test"

    move-object/from16 v0, v30

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_0

    const-string v30, "hotkey set volume test!!!!"

    invoke-static/range {v30 .. v30}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    const-string v30, "audio"

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Landroid/media/AudioManager;

    invoke-virtual/range {v25 .. v25}, Landroid/media/AudioManager;->getMasterVolume()I

    move-result v6

    if-ltz v6, :cond_2f

    const/16 v30, 0x1e

    move/from16 v0, v30

    if-ge v6, v0, :cond_2f

    const/16 v30, 0x1e

    const/16 v31, 0x1

    move-object/from16 v0, v25

    move/from16 v1, v30

    move/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Landroid/media/AudioManager;->setMasterVolume(II)V

    :cond_2e
    :goto_7
    const/16 v30, 0x0

    move-object/from16 v0, v25

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setMasterMute(Z)V

    goto/16 :goto_0

    :cond_2f
    const/16 v30, 0x1e

    move/from16 v0, v30

    if-lt v6, v0, :cond_30

    const/16 v30, 0x64

    move/from16 v0, v30

    if-ge v6, v0, :cond_30

    const/16 v30, 0x64

    const/16 v31, 0x1

    move-object/from16 v0, v25

    move/from16 v1, v30

    move/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Landroid/media/AudioManager;->setMasterVolume(II)V

    goto :goto_7

    :cond_30
    const/16 v30, 0x64

    move/from16 v0, v30

    if-ne v6, v0, :cond_2e

    const/16 v30, 0x0

    const/16 v31, 0x1

    move-object/from16 v0, v25

    move/from16 v1, v30

    move/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Landroid/media/AudioManager;->setMasterVolume(II)V

    goto :goto_7
.end method

.method public peekService(Landroid/content/Context;Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    invoke-super {p0, p1, p2}, Landroid/content/BroadcastReceiver;->peekService(Landroid/content/Context;Landroid/content/Intent;)Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public popToast(Landroid/content/Context;I)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # I

    const/4 v2, 0x0

    sget-object v0, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->toast:Landroid/widget/Toast;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->toast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V

    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, p2, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    sput-object v0, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->toast:Landroid/widget/Toast;

    sget-object v0, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->toast:Landroid/widget/Toast;

    const/16 v1, 0x11

    invoke-virtual {v0, v1, v2, v2}, Landroid/widget/Toast;->setGravity(III)V

    sget-object v0, Lcom/konka/hotkey/HotkeyBroadcastReceiver;->toast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method
