.class public Lcom/konka/hotkey/CountdownActivity;
.super Landroid/app/Activity;
.source "CountdownActivity.java"


# instance fields
.field private final CMD_CANCEL_COUNT_DOWN:I

.field private final CMD_ENTER_STANDBY_MODE:I

.field private final CMD_UPDATE_SEC_COUNT:I

.field private final TIME_PERIOD:I

.field private broadcastReceiver:Landroid/content/BroadcastReceiver;

.field private isNoSignalStandby:Z

.field private mCurrentInputSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

.field private mViewTvCount:Landroid/widget/TextView;

.field private mViewTvNote:Landroid/widget/TextView;

.field private miSecCount:I

.field private myHandler:Landroid/os/Handler;

.field private timer:Ljava/util/Timer;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/16 v0, 0x3c

    iput v0, p0, Lcom/konka/hotkey/CountdownActivity;->miSecCount:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/konka/hotkey/CountdownActivity;->broadcastReceiver:Landroid/content/BroadcastReceiver;

    const/16 v0, 0x3e8

    iput v0, p0, Lcom/konka/hotkey/CountdownActivity;->TIME_PERIOD:I

    const/4 v0, 0x1

    iput v0, p0, Lcom/konka/hotkey/CountdownActivity;->CMD_UPDATE_SEC_COUNT:I

    const/4 v0, 0x2

    iput v0, p0, Lcom/konka/hotkey/CountdownActivity;->CMD_CANCEL_COUNT_DOWN:I

    const/4 v0, 0x3

    iput v0, p0, Lcom/konka/hotkey/CountdownActivity;->CMD_ENTER_STANDBY_MODE:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/konka/hotkey/CountdownActivity;->isNoSignalStandby:Z

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_ATV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    iput-object v0, p0, Lcom/konka/hotkey/CountdownActivity;->mCurrentInputSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    new-instance v0, Lcom/konka/hotkey/CountdownActivity$1;

    invoke-direct {v0, p0}, Lcom/konka/hotkey/CountdownActivity$1;-><init>(Lcom/konka/hotkey/CountdownActivity;)V

    iput-object v0, p0, Lcom/konka/hotkey/CountdownActivity;->myHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$0(Lcom/konka/hotkey/CountdownActivity;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/konka/hotkey/CountdownActivity;->mViewTvCount:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1(Lcom/konka/hotkey/CountdownActivity;)I
    .locals 1

    iget v0, p0, Lcom/konka/hotkey/CountdownActivity;->miSecCount:I

    return v0
.end method

.method static synthetic access$2(Lcom/konka/hotkey/CountdownActivity;)Ljava/util/Timer;
    .locals 1

    iget-object v0, p0, Lcom/konka/hotkey/CountdownActivity;->timer:Ljava/util/Timer;

    return-object v0
.end method

.method static synthetic access$3(Lcom/konka/hotkey/CountdownActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/hotkey/CountdownActivity;->isNoSignalStandby:Z

    return v0
.end method

.method static synthetic access$4(Lcom/konka/hotkey/CountdownActivity;)Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    .locals 1

    iget-object v0, p0, Lcom/konka/hotkey/CountdownActivity;->mCurrentInputSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    return-object v0
.end method

.method static synthetic access$5(Lcom/konka/hotkey/CountdownActivity;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/konka/hotkey/CountdownActivity;->myHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$6(Lcom/konka/hotkey/CountdownActivity;I)V
    .locals 0

    iput p1, p0, Lcom/konka/hotkey/CountdownActivity;->miSecCount:I

    return-void
.end method


# virtual methods
.method public countTimerTask()Ljava/util/TimerTask;
    .locals 1

    new-instance v0, Lcom/konka/hotkey/CountdownActivity$3;

    invoke-direct {v0, p0}, Lcom/konka/hotkey/CountdownActivity$3;-><init>(Lcom/konka/hotkey/CountdownActivity;)V

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const/high16 v0, 0x7f030000    # com.konka.hotkey.R.layout.hk_countdown_menu

    invoke-virtual {p0, v0}, Lcom/konka/hotkey/CountdownActivity;->setContentView(I)V

    const v0, 0x7f0a0001    # com.konka.hotkey.R.id.hk_countdown_menu_note

    invoke-virtual {p0, v0}, Lcom/konka/hotkey/CountdownActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/hotkey/CountdownActivity;->mViewTvNote:Landroid/widget/TextView;

    const v0, 0x7f0a0002    # com.konka.hotkey.R.id.hk_countdown_menu_sec

    invoke-virtual {p0, v0}, Lcom/konka/hotkey/CountdownActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/hotkey/CountdownActivity;->mViewTvCount:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/konka/hotkey/CountdownActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/hotkey/CountdownActivity;->mCurrentInputSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {p0}, Lcom/konka/hotkey/CountdownActivity;->viewInit()V

    invoke-virtual {p0}, Lcom/konka/hotkey/CountdownActivity;->registerBroadcastReceiver()V

    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/konka/hotkey/CountdownActivity;->timer:Ljava/util/Timer;

    iget-object v0, p0, Lcom/konka/hotkey/CountdownActivity;->timer:Ljava/util/Timer;

    invoke-virtual {p0}, Lcom/konka/hotkey/CountdownActivity;->countTimerTask()Ljava/util/TimerTask;

    move-result-object v1

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x3e8

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    return-void
.end method

.method protected onDestroy()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    iget-object v0, p0, Lcom/konka/hotkey/CountdownActivity;->broadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/konka/hotkey/CountdownActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/konka/hotkey/CountdownActivity;->myHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method protected onStop()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    const-string v0, "XXXXXXXXXX==========>>>>>onStop"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/hotkey/CountdownActivity;->myHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method

.method public registerBroadcastReceiver()V
    .locals 2

    new-instance v1, Lcom/konka/hotkey/CountdownActivity$2;

    invoke-direct {v1, p0}, Lcom/konka/hotkey/CountdownActivity$2;-><init>(Lcom/konka/hotkey/CountdownActivity;)V

    iput-object v1, p0, Lcom/konka/hotkey/CountdownActivity;->broadcastReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "com.mstar.tv.service.VOLUMEADD"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "com.mstar.tv.service.VOLUMEDEC"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "com.mstar.tv.service.MUTE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "com.konka.GO_TO_HOME_PAGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "com.konka.START_SHOW_3D_DESKTOP"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "com.konka.SCREEN_RECEIVE_HOME_PAGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "com.konka.tv.action.SIGNAL_LOCK"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/hotkey/CountdownActivity;->broadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/konka/hotkey/CountdownActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method public viewInit()V
    .locals 6

    const/4 v5, 0x0

    const-string v0, ""

    invoke-virtual {p0}, Lcom/konka/hotkey/CountdownActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "action"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    const-string v1, "com.konka.NO_OPERATION_SHUTDOWN"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/konka/hotkey/CountdownActivity;->mViewTvNote:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, p0, Lcom/konka/hotkey/CountdownActivity;->mViewTvNote:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/konka/hotkey/CountdownActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f080011    # com.konka.hotkey.R.string.str_hk_sleep_menu_str3

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p0}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v3

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getSettingManagerInstance()Lcom/konka/kkinterface/tv/SettingDesk;

    move-result-object v3

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/SettingDesk;->getStandbyNoOperation()I

    move-result v3

    div-int/lit8 v3, v3, 0x3c

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/konka/hotkey/CountdownActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f080012    # com.konka.hotkey.R.string.str_hk_sleep_menu_str4

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {p0}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getSettingManagerInstance()Lcom/konka/kkinterface/tv/SettingDesk;

    move-result-object v1

    invoke-interface {v1, v5}, Lcom/konka/kkinterface/tv/SettingDesk;->setStandbyNoOperation(I)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz v0, :cond_2

    const-string v1, "com.konka.tv.hotkey.TIMER_OFF_WARNING"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/konka/hotkey/CountdownActivity;->mViewTvNote:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, p0, Lcom/konka/hotkey/CountdownActivity;->mViewTvNote:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/konka/hotkey/CountdownActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f080013    # com.konka.hotkey.R.string.str_hk_sleep_menu_str5

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_2
    if-eqz v0, :cond_0

    const-string v1, "com.konka.tv.hotkey.NO_SIGNAL"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/konka/hotkey/CountdownActivity;->isNoSignalStandby:Z

    iget-object v1, p0, Lcom/konka/hotkey/CountdownActivity;->mViewTvNote:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, p0, Lcom/konka/hotkey/CountdownActivity;->mViewTvNote:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/konka/hotkey/CountdownActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f080014    # com.konka.hotkey.R.string.str_hk_sleep_menu_str6

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/hotkey/CountdownActivity;->mCurrentInputSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_VGA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-ne v1, v2, :cond_3

    const/16 v1, 0x10

    iput v1, p0, Lcom/konka/hotkey/CountdownActivity;->miSecCount:I

    goto :goto_0

    :cond_3
    const/16 v1, 0x3c

    iput v1, p0, Lcom/konka/hotkey/CountdownActivity;->miSecCount:I

    goto :goto_0
.end method
