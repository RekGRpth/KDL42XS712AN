.class public interface abstract Lcom/konka/kkinterface/tv/DemoDesk;
.super Ljava/lang/Object;
.source "DemoDesk.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_DBC_TYPE;,
        Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_DCC_TYPE;,
        Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_DLC_TYPE;,
        Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_UCLEAR_TYPE;
    }
.end annotation


# virtual methods
.method public abstract forceThreadSleep(Z)V
.end method

.method public abstract get3DNRStatus()Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;
.end method

.method public abstract getDBCStatus()Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_DBC_TYPE;
.end method

.method public abstract getDCCStatus()Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_DCC_TYPE;
.end method

.method public abstract getDLCStatus()Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_DLC_TYPE;
.end method

.method public abstract getMWEStatus()Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;
.end method

.method public abstract getUClearStatus()Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_UCLEAR_TYPE;
.end method

.method public abstract set3DNR(Lcom/mstar/android/tvapi/common/vo/NoiseReduction$EnumNoiseReduction;)V
.end method

.method public abstract setDBCStatus(Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_DBC_TYPE;)V
.end method

.method public abstract setDCCStatus(Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_DCC_TYPE;)V
.end method

.method public abstract setDLCStatus(Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_DLC_TYPE;)V
.end method

.method public abstract setMWEStatus(Lcom/mstar/android/tvapi/common/vo/MweType$EnumMweType;)V
.end method

.method public abstract setUClearStatus(Lcom/konka/kkinterface/tv/DemoDesk$EN_MS_UCLEAR_TYPE;)V
.end method
