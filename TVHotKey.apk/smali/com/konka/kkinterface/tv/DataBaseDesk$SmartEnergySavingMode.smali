.class public final enum Lcom/konka/kkinterface/tv/DataBaseDesk$SmartEnergySavingMode;
.super Ljava/lang/Enum;
.source "DataBaseDesk.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/kkinterface/tv/DataBaseDesk;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SmartEnergySavingMode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/konka/kkinterface/tv/DataBaseDesk$SmartEnergySavingMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lcom/konka/kkinterface/tv/DataBaseDesk$SmartEnergySavingMode;

.field public static final enum MODE_DEMO:Lcom/konka/kkinterface/tv/DataBaseDesk$SmartEnergySavingMode;

.field public static final enum MODE_OFF:Lcom/konka/kkinterface/tv/DataBaseDesk$SmartEnergySavingMode;

.field public static final enum MODE_ON:Lcom/konka/kkinterface/tv/DataBaseDesk$SmartEnergySavingMode;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$SmartEnergySavingMode;

    const-string v1, "MODE_OFF"

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$SmartEnergySavingMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$SmartEnergySavingMode;->MODE_OFF:Lcom/konka/kkinterface/tv/DataBaseDesk$SmartEnergySavingMode;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$SmartEnergySavingMode;

    const-string v1, "MODE_ON"

    invoke-direct {v0, v1, v3}, Lcom/konka/kkinterface/tv/DataBaseDesk$SmartEnergySavingMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$SmartEnergySavingMode;->MODE_ON:Lcom/konka/kkinterface/tv/DataBaseDesk$SmartEnergySavingMode;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$SmartEnergySavingMode;

    const-string v1, "MODE_DEMO"

    invoke-direct {v0, v1, v4}, Lcom/konka/kkinterface/tv/DataBaseDesk$SmartEnergySavingMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$SmartEnergySavingMode;->MODE_DEMO:Lcom/konka/kkinterface/tv/DataBaseDesk$SmartEnergySavingMode;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/konka/kkinterface/tv/DataBaseDesk$SmartEnergySavingMode;

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$SmartEnergySavingMode;->MODE_OFF:Lcom/konka/kkinterface/tv/DataBaseDesk$SmartEnergySavingMode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$SmartEnergySavingMode;->MODE_ON:Lcom/konka/kkinterface/tv/DataBaseDesk$SmartEnergySavingMode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$SmartEnergySavingMode;->MODE_DEMO:Lcom/konka/kkinterface/tv/DataBaseDesk$SmartEnergySavingMode;

    aput-object v1, v0, v4

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$SmartEnergySavingMode;->ENUM$VALUES:[Lcom/konka/kkinterface/tv/DataBaseDesk$SmartEnergySavingMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/konka/kkinterface/tv/DataBaseDesk$SmartEnergySavingMode;
    .locals 1

    const-class v0, Lcom/konka/kkinterface/tv/DataBaseDesk$SmartEnergySavingMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/konka/kkinterface/tv/DataBaseDesk$SmartEnergySavingMode;

    return-object v0
.end method

.method public static values()[Lcom/konka/kkinterface/tv/DataBaseDesk$SmartEnergySavingMode;
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$SmartEnergySavingMode;->ENUM$VALUES:[Lcom/konka/kkinterface/tv/DataBaseDesk$SmartEnergySavingMode;

    array-length v1, v0

    new-array v2, v1, [Lcom/konka/kkinterface/tv/DataBaseDesk$SmartEnergySavingMode;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
