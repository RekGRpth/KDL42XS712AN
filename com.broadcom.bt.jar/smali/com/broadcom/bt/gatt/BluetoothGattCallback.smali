.class public abstract Lcom/broadcom/bt/gatt/BluetoothGattCallback;
.super Ljava/lang/Object;
.source "BluetoothGattCallback.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAppRegistered(I)V
    .locals 0
    .param p1    # I

    return-void
.end method

.method public onCharacteristicChanged(Lcom/broadcom/bt/gatt/BluetoothGattCharacteristic;)V
    .locals 0
    .param p1    # Lcom/broadcom/bt/gatt/BluetoothGattCharacteristic;

    return-void
.end method

.method public onCharacteristicRead(Lcom/broadcom/bt/gatt/BluetoothGattCharacteristic;I)V
    .locals 0
    .param p1    # Lcom/broadcom/bt/gatt/BluetoothGattCharacteristic;
    .param p2    # I

    return-void
.end method

.method public onCharacteristicWrite(Lcom/broadcom/bt/gatt/BluetoothGattCharacteristic;I)V
    .locals 0
    .param p1    # Lcom/broadcom/bt/gatt/BluetoothGattCharacteristic;
    .param p2    # I

    return-void
.end method

.method public onConnectionStateChange(Landroid/bluetooth/BluetoothDevice;II)V
    .locals 0
    .param p1    # Landroid/bluetooth/BluetoothDevice;
    .param p2    # I
    .param p3    # I

    return-void
.end method

.method public onDescriptorRead(Lcom/broadcom/bt/gatt/BluetoothGattDescriptor;I)V
    .locals 0
    .param p1    # Lcom/broadcom/bt/gatt/BluetoothGattDescriptor;
    .param p2    # I

    return-void
.end method

.method public onDescriptorWrite(Lcom/broadcom/bt/gatt/BluetoothGattDescriptor;I)V
    .locals 0
    .param p1    # Lcom/broadcom/bt/gatt/BluetoothGattDescriptor;
    .param p2    # I

    return-void
.end method

.method public onReliableWriteCompleted(Landroid/bluetooth/BluetoothDevice;I)V
    .locals 0
    .param p1    # Landroid/bluetooth/BluetoothDevice;
    .param p2    # I

    return-void
.end method

.method public onScanResult(Landroid/bluetooth/BluetoothDevice;I[B)V
    .locals 0
    .param p1    # Landroid/bluetooth/BluetoothDevice;
    .param p2    # I
    .param p3    # [B

    return-void
.end method

.method public onServicesDiscovered(Landroid/bluetooth/BluetoothDevice;I)V
    .locals 0
    .param p1    # Landroid/bluetooth/BluetoothDevice;
    .param p2    # I

    return-void
.end method
