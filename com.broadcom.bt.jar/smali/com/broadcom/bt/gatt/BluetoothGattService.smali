.class public Lcom/broadcom/bt/gatt/BluetoothGattService;
.super Ljava/lang/Object;
.source "BluetoothGattService.java"


# static fields
.field public static final SERVICE_TYPE_PRIMARY:I = 0x0

.field public static final SERVICE_TYPE_SECONDARY:I = 0x1


# instance fields
.field protected mCharacteristics:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/broadcom/bt/gatt/BluetoothGattCharacteristic;",
            ">;"
        }
    .end annotation
.end field

.field protected mDevice:Landroid/bluetooth/BluetoothDevice;

.field protected mHandles:I

.field protected mIncludedServices:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/broadcom/bt/gatt/BluetoothGattService;",
            ">;"
        }
    .end annotation
.end field

.field protected mInstanceId:I

.field protected mServiceType:I

.field protected mUuid:Ljava/util/UUID;


# direct methods
.method constructor <init>(Landroid/bluetooth/BluetoothDevice;Ljava/util/UUID;II)V
    .locals 1
    .param p1    # Landroid/bluetooth/BluetoothDevice;
    .param p2    # Ljava/util/UUID;
    .param p3    # I
    .param p4    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/broadcom/bt/gatt/BluetoothGattService;->mHandles:I

    iput-object p1, p0, Lcom/broadcom/bt/gatt/BluetoothGattService;->mDevice:Landroid/bluetooth/BluetoothDevice;

    iput-object p2, p0, Lcom/broadcom/bt/gatt/BluetoothGattService;->mUuid:Ljava/util/UUID;

    iput p3, p0, Lcom/broadcom/bt/gatt/BluetoothGattService;->mInstanceId:I

    iput p4, p0, Lcom/broadcom/bt/gatt/BluetoothGattService;->mServiceType:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/broadcom/bt/gatt/BluetoothGattService;->mCharacteristics:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/broadcom/bt/gatt/BluetoothGattService;->mIncludedServices:Ljava/util/List;

    return-void
.end method

.method constructor <init>(Ljava/util/UUID;I)V
    .locals 2
    .param p1    # Ljava/util/UUID;
    .param p2    # I

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v1, p0, Lcom/broadcom/bt/gatt/BluetoothGattService;->mHandles:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/broadcom/bt/gatt/BluetoothGattService;->mDevice:Landroid/bluetooth/BluetoothDevice;

    iput-object p1, p0, Lcom/broadcom/bt/gatt/BluetoothGattService;->mUuid:Ljava/util/UUID;

    iput v1, p0, Lcom/broadcom/bt/gatt/BluetoothGattService;->mInstanceId:I

    iput p2, p0, Lcom/broadcom/bt/gatt/BluetoothGattService;->mServiceType:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/broadcom/bt/gatt/BluetoothGattService;->mCharacteristics:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/broadcom/bt/gatt/BluetoothGattService;->mIncludedServices:Ljava/util/List;

    return-void
.end method


# virtual methods
.method addCharacteristic(Lcom/broadcom/bt/gatt/BluetoothGattCharacteristic;)V
    .locals 1
    .param p1    # Lcom/broadcom/bt/gatt/BluetoothGattCharacteristic;

    iget-object v0, p0, Lcom/broadcom/bt/gatt/BluetoothGattService;->mCharacteristics:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public getCharacteristic(Ljava/util/UUID;)Lcom/broadcom/bt/gatt/BluetoothGattCharacteristic;
    .locals 3
    .param p1    # Ljava/util/UUID;

    iget-object v2, p0, Lcom/broadcom/bt/gatt/BluetoothGattService;->mCharacteristics:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/broadcom/bt/gatt/BluetoothGattCharacteristic;

    invoke-virtual {v0}, Lcom/broadcom/bt/gatt/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/util/UUID;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method getCharacteristic(Ljava/util/UUID;I)Lcom/broadcom/bt/gatt/BluetoothGattCharacteristic;
    .locals 3
    .param p1    # Ljava/util/UUID;
    .param p2    # I

    iget-object v2, p0, Lcom/broadcom/bt/gatt/BluetoothGattService;->mCharacteristics:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/broadcom/bt/gatt/BluetoothGattCharacteristic;

    invoke-virtual {v0}, Lcom/broadcom/bt/gatt/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/util/UUID;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/broadcom/bt/gatt/BluetoothGattService;->mInstanceId:I

    if-ne v2, p2, :cond_0

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCharacteristics()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/broadcom/bt/gatt/BluetoothGattCharacteristic;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/broadcom/bt/gatt/BluetoothGattService;->mCharacteristics:Ljava/util/List;

    return-object v0
.end method

.method getDevice()Landroid/bluetooth/BluetoothDevice;
    .locals 1

    iget-object v0, p0, Lcom/broadcom/bt/gatt/BluetoothGattService;->mDevice:Landroid/bluetooth/BluetoothDevice;

    return-object v0
.end method

.method getHandles()I
    .locals 1

    iget v0, p0, Lcom/broadcom/bt/gatt/BluetoothGattService;->mHandles:I

    return v0
.end method

.method public getIncludedServices()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/broadcom/bt/gatt/BluetoothGattService;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/broadcom/bt/gatt/BluetoothGattService;->mIncludedServices:Ljava/util/List;

    return-object v0
.end method

.method public getInstanceId()I
    .locals 1

    iget v0, p0, Lcom/broadcom/bt/gatt/BluetoothGattService;->mInstanceId:I

    return v0
.end method

.method public getType()I
    .locals 1

    iget v0, p0, Lcom/broadcom/bt/gatt/BluetoothGattService;->mServiceType:I

    return v0
.end method

.method public getUuid()Ljava/util/UUID;
    .locals 1

    iget-object v0, p0, Lcom/broadcom/bt/gatt/BluetoothGattService;->mUuid:Ljava/util/UUID;

    return-object v0
.end method
