.class public Lcom/broadcom/bt/gatt/MutableBluetoothGattCharacteristic;
.super Lcom/broadcom/bt/gatt/BluetoothGattCharacteristic;
.source "MutableBluetoothGattCharacteristic.java"


# direct methods
.method public constructor <init>(Ljava/util/UUID;II)V
    .locals 6
    .param p1    # Ljava/util/UUID;
    .param p2    # I
    .param p3    # I

    const/4 v1, 0x0

    const/4 v3, 0x0

    move-object v0, p0

    move-object v2, p1

    move v4, p2

    move v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/broadcom/bt/gatt/BluetoothGattCharacteristic;-><init>(Lcom/broadcom/bt/gatt/BluetoothGattService;Ljava/util/UUID;III)V

    return-void
.end method


# virtual methods
.method public addDescriptor(Lcom/broadcom/bt/gatt/MutableBluetoothGattDescriptor;)V
    .locals 1
    .param p1    # Lcom/broadcom/bt/gatt/MutableBluetoothGattDescriptor;

    iget-object v0, p0, Lcom/broadcom/bt/gatt/MutableBluetoothGattCharacteristic;->mDescriptors:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {p1, p0}, Lcom/broadcom/bt/gatt/MutableBluetoothGattDescriptor;->setCharacteristic(Lcom/broadcom/bt/gatt/BluetoothGattCharacteristic;)V

    return-void
.end method

.method public setKeySize(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/broadcom/bt/gatt/MutableBluetoothGattCharacteristic;->mKeySize:I

    return-void
.end method

.method setService(Lcom/broadcom/bt/gatt/BluetoothGattService;)V
    .locals 0
    .param p1    # Lcom/broadcom/bt/gatt/BluetoothGattService;

    iput-object p1, p0, Lcom/broadcom/bt/gatt/MutableBluetoothGattCharacteristic;->mService:Lcom/broadcom/bt/gatt/BluetoothGattService;

    return-void
.end method
