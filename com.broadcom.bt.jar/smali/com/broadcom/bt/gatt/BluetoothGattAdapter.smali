.class public Lcom/broadcom/bt/gatt/BluetoothGattAdapter;
.super Ljava/lang/Object;
.source "BluetoothGattAdapter.java"


# static fields
.field public static final GATT:I = 0x7

.field public static final GATT_SERVER:I = 0x8


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static closeProfileProxy(ILandroid/bluetooth/BluetoothProfile;)V
    .locals 2
    .param p0    # I
    .param p1    # Landroid/bluetooth/BluetoothProfile;

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    packed-switch p0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    move-object v0, p1

    check-cast v0, Lcom/broadcom/bt/gatt/BluetoothGatt;

    invoke-virtual {v0}, Lcom/broadcom/bt/gatt/BluetoothGatt;->close()V

    goto :goto_0

    :pswitch_1
    move-object v1, p1

    check-cast v1, Lcom/broadcom/bt/gatt/BluetoothGattServer;

    invoke-virtual {v1}, Lcom/broadcom/bt/gatt/BluetoothGattServer;->close()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static getProfileProxy(Landroid/content/Context;Landroid/bluetooth/BluetoothProfile$ServiceListener;I)Z
    .locals 5
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/bluetooth/BluetoothProfile$ServiceListener;
    .param p2    # I

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    move v2, v3

    :goto_0
    return v2

    :cond_1
    const/4 v4, 0x7

    if-ne p2, v4, :cond_2

    new-instance v0, Lcom/broadcom/bt/gatt/BluetoothGatt;

    invoke-direct {v0, p0, p1}, Lcom/broadcom/bt/gatt/BluetoothGatt;-><init>(Landroid/content/Context;Landroid/bluetooth/BluetoothProfile$ServiceListener;)V

    goto :goto_0

    :cond_2
    const/16 v4, 0x8

    if-ne p2, v4, :cond_3

    new-instance v1, Lcom/broadcom/bt/gatt/BluetoothGattServer;

    invoke-direct {v1, p0, p1}, Lcom/broadcom/bt/gatt/BluetoothGattServer;-><init>(Landroid/content/Context;Landroid/bluetooth/BluetoothProfile$ServiceListener;)V

    goto :goto_0

    :cond_3
    move v2, v3

    goto :goto_0
.end method
