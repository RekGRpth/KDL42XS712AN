.class Lcom/broadcom/bt/gatt/BluetoothGatt$3;
.super Lcom/broadcom/bt/gatt/IBluetoothGattCallback$Stub;
.source "BluetoothGatt.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/broadcom/bt/gatt/BluetoothGatt;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/broadcom/bt/gatt/BluetoothGatt;


# direct methods
.method constructor <init>(Lcom/broadcom/bt/gatt/BluetoothGatt;)V
    .locals 0

    iput-object p1, p0, Lcom/broadcom/bt/gatt/BluetoothGatt$3;->this$0:Lcom/broadcom/bt/gatt/BluetoothGatt;

    invoke-direct {p0}, Lcom/broadcom/bt/gatt/IBluetoothGattCallback$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onCharacteristicRead(Ljava/lang/String;IIILandroid/os/ParcelUuid;ILandroid/os/ParcelUuid;[B)V
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # Landroid/os/ParcelUuid;
    .param p6    # I
    .param p7    # Landroid/os/ParcelUuid;
    .param p8    # [B

    const-string v3, "BtGatt.BluetoothGatt"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onCharacteristicRead() - Device="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " UUID="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/broadcom/bt/gatt/BluetoothGatt$3;->this$0:Lcom/broadcom/bt/gatt/BluetoothGatt;

    # getter for: Lcom/broadcom/bt/gatt/BluetoothGatt;->mAdapter:Landroid/bluetooth/BluetoothAdapter;
    invoke-static {v3}, Lcom/broadcom/bt/gatt/BluetoothGatt;->access$600(Lcom/broadcom/bt/gatt/BluetoothGatt;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v3

    invoke-virtual {v3, p1}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v1

    iget-object v3, p0, Lcom/broadcom/bt/gatt/BluetoothGatt$3;->this$0:Lcom/broadcom/bt/gatt/BluetoothGatt;

    invoke-virtual {p5}, Landroid/os/ParcelUuid;->getUuid()Ljava/util/UUID;

    move-result-object v4

    invoke-virtual {v3, v1, v4, p4, p3}, Lcom/broadcom/bt/gatt/BluetoothGatt;->getService(Landroid/bluetooth/BluetoothDevice;Ljava/util/UUID;II)Lcom/broadcom/bt/gatt/BluetoothGattService;

    move-result-object v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p7}, Landroid/os/ParcelUuid;->getUuid()Ljava/util/UUID;

    move-result-object v3

    invoke-virtual {v2, v3, p6}, Lcom/broadcom/bt/gatt/BluetoothGattService;->getCharacteristic(Ljava/util/UUID;I)Lcom/broadcom/bt/gatt/BluetoothGattCharacteristic;

    move-result-object v0

    if-eqz v0, :cond_0

    if-nez p2, :cond_2

    invoke-virtual {v0, p8}, Lcom/broadcom/bt/gatt/BluetoothGattCharacteristic;->setValue([B)Z

    :cond_2
    iget-object v3, p0, Lcom/broadcom/bt/gatt/BluetoothGatt$3;->this$0:Lcom/broadcom/bt/gatt/BluetoothGatt;

    # getter for: Lcom/broadcom/bt/gatt/BluetoothGatt;->mCallback:Lcom/broadcom/bt/gatt/BluetoothGattCallback;
    invoke-static {v3}, Lcom/broadcom/bt/gatt/BluetoothGatt;->access$500(Lcom/broadcom/bt/gatt/BluetoothGatt;)Lcom/broadcom/bt/gatt/BluetoothGattCallback;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/broadcom/bt/gatt/BluetoothGatt$3;->this$0:Lcom/broadcom/bt/gatt/BluetoothGatt;

    # getter for: Lcom/broadcom/bt/gatt/BluetoothGatt;->mCallback:Lcom/broadcom/bt/gatt/BluetoothGattCallback;
    invoke-static {v3}, Lcom/broadcom/bt/gatt/BluetoothGatt;->access$500(Lcom/broadcom/bt/gatt/BluetoothGatt;)Lcom/broadcom/bt/gatt/BluetoothGattCallback;

    move-result-object v3

    invoke-virtual {v3, v0, p2}, Lcom/broadcom/bt/gatt/BluetoothGattCallback;->onCharacteristicRead(Lcom/broadcom/bt/gatt/BluetoothGattCharacteristic;I)V

    goto :goto_0
.end method

.method public onCharacteristicWrite(Ljava/lang/String;IIILandroid/os/ParcelUuid;ILandroid/os/ParcelUuid;)V
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # Landroid/os/ParcelUuid;
    .param p6    # I
    .param p7    # Landroid/os/ParcelUuid;

    const-string v3, "BtGatt.BluetoothGatt"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onCharacteristicWrite() - Device="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " UUID="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/broadcom/bt/gatt/BluetoothGatt$3;->this$0:Lcom/broadcom/bt/gatt/BluetoothGatt;

    # getter for: Lcom/broadcom/bt/gatt/BluetoothGatt;->mAdapter:Landroid/bluetooth/BluetoothAdapter;
    invoke-static {v3}, Lcom/broadcom/bt/gatt/BluetoothGatt;->access$600(Lcom/broadcom/bt/gatt/BluetoothGatt;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v3

    invoke-virtual {v3, p1}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v1

    iget-object v3, p0, Lcom/broadcom/bt/gatt/BluetoothGatt$3;->this$0:Lcom/broadcom/bt/gatt/BluetoothGatt;

    invoke-virtual {p5}, Landroid/os/ParcelUuid;->getUuid()Ljava/util/UUID;

    move-result-object v4

    invoke-virtual {v3, v1, v4, p4, p3}, Lcom/broadcom/bt/gatt/BluetoothGatt;->getService(Landroid/bluetooth/BluetoothDevice;Ljava/util/UUID;II)Lcom/broadcom/bt/gatt/BluetoothGattService;

    move-result-object v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p7}, Landroid/os/ParcelUuid;->getUuid()Ljava/util/UUID;

    move-result-object v3

    invoke-virtual {v2, v3, p6}, Lcom/broadcom/bt/gatt/BluetoothGattService;->getCharacteristic(Ljava/util/UUID;I)Lcom/broadcom/bt/gatt/BluetoothGattCharacteristic;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v3, p0, Lcom/broadcom/bt/gatt/BluetoothGatt$3;->this$0:Lcom/broadcom/bt/gatt/BluetoothGatt;

    # getter for: Lcom/broadcom/bt/gatt/BluetoothGatt;->mCallback:Lcom/broadcom/bt/gatt/BluetoothGattCallback;
    invoke-static {v3}, Lcom/broadcom/bt/gatt/BluetoothGatt;->access$500(Lcom/broadcom/bt/gatt/BluetoothGatt;)Lcom/broadcom/bt/gatt/BluetoothGattCallback;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/broadcom/bt/gatt/BluetoothGatt$3;->this$0:Lcom/broadcom/bt/gatt/BluetoothGatt;

    # getter for: Lcom/broadcom/bt/gatt/BluetoothGatt;->mCallback:Lcom/broadcom/bt/gatt/BluetoothGattCallback;
    invoke-static {v3}, Lcom/broadcom/bt/gatt/BluetoothGatt;->access$500(Lcom/broadcom/bt/gatt/BluetoothGatt;)Lcom/broadcom/bt/gatt/BluetoothGattCallback;

    move-result-object v3

    invoke-virtual {v3, v0, p2}, Lcom/broadcom/bt/gatt/BluetoothGattCallback;->onCharacteristicWrite(Lcom/broadcom/bt/gatt/BluetoothGattCharacteristic;I)V

    goto :goto_0
.end method

.method public onClientConnectionState(BBZLjava/lang/String;)V
    .locals 3
    .param p1    # B
    .param p2    # B
    .param p3    # Z
    .param p4    # Ljava/lang/String;

    const-string v0, "BtGatt.BluetoothGatt"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onClientConnectionState() - status="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " clientIf="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " device="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/broadcom/bt/gatt/BluetoothGatt$3;->this$0:Lcom/broadcom/bt/gatt/BluetoothGatt;

    # getter for: Lcom/broadcom/bt/gatt/BluetoothGatt;->mCallback:Lcom/broadcom/bt/gatt/BluetoothGattCallback;
    invoke-static {v0}, Lcom/broadcom/bt/gatt/BluetoothGatt;->access$500(Lcom/broadcom/bt/gatt/BluetoothGatt;)Lcom/broadcom/bt/gatt/BluetoothGattCallback;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/broadcom/bt/gatt/BluetoothGatt$3;->this$0:Lcom/broadcom/bt/gatt/BluetoothGatt;

    # getter for: Lcom/broadcom/bt/gatt/BluetoothGatt;->mCallback:Lcom/broadcom/bt/gatt/BluetoothGattCallback;
    invoke-static {v0}, Lcom/broadcom/bt/gatt/BluetoothGatt;->access$500(Lcom/broadcom/bt/gatt/BluetoothGatt;)Lcom/broadcom/bt/gatt/BluetoothGattCallback;

    move-result-object v1

    iget-object v0, p0, Lcom/broadcom/bt/gatt/BluetoothGatt$3;->this$0:Lcom/broadcom/bt/gatt/BluetoothGatt;

    # getter for: Lcom/broadcom/bt/gatt/BluetoothGatt;->mAdapter:Landroid/bluetooth/BluetoothAdapter;
    invoke-static {v0}, Lcom/broadcom/bt/gatt/BluetoothGatt;->access$600(Lcom/broadcom/bt/gatt/BluetoothGatt;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    invoke-virtual {v0, p4}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v2

    if-eqz p3, :cond_1

    const/4 v0, 0x2

    :goto_0
    invoke-virtual {v1, v2, p1, v0}, Lcom/broadcom/bt/gatt/BluetoothGattCallback;->onConnectionStateChange(Landroid/bluetooth/BluetoothDevice;II)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onClientRegistered(BB)V
    .locals 3
    .param p1    # B
    .param p2    # B

    const-string v0, "BtGatt.BluetoothGatt"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onClientRegistered() - status="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " clientIf="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/broadcom/bt/gatt/BluetoothGatt$3;->this$0:Lcom/broadcom/bt/gatt/BluetoothGatt;

    # setter for: Lcom/broadcom/bt/gatt/BluetoothGatt;->mClientIf:B
    invoke-static {v0, p2}, Lcom/broadcom/bt/gatt/BluetoothGatt;->access$402(Lcom/broadcom/bt/gatt/BluetoothGatt;B)B

    iget-object v0, p0, Lcom/broadcom/bt/gatt/BluetoothGatt$3;->this$0:Lcom/broadcom/bt/gatt/BluetoothGatt;

    # getter for: Lcom/broadcom/bt/gatt/BluetoothGatt;->mCallback:Lcom/broadcom/bt/gatt/BluetoothGattCallback;
    invoke-static {v0}, Lcom/broadcom/bt/gatt/BluetoothGatt;->access$500(Lcom/broadcom/bt/gatt/BluetoothGatt;)Lcom/broadcom/bt/gatt/BluetoothGattCallback;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/broadcom/bt/gatt/BluetoothGatt$3;->this$0:Lcom/broadcom/bt/gatt/BluetoothGatt;

    # getter for: Lcom/broadcom/bt/gatt/BluetoothGatt;->mCallback:Lcom/broadcom/bt/gatt/BluetoothGattCallback;
    invoke-static {v0}, Lcom/broadcom/bt/gatt/BluetoothGatt;->access$500(Lcom/broadcom/bt/gatt/BluetoothGatt;)Lcom/broadcom/bt/gatt/BluetoothGattCallback;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/broadcom/bt/gatt/BluetoothGattCallback;->onAppRegistered(I)V

    :cond_0
    return-void
.end method

.method public onDescriptorRead(Ljava/lang/String;IIILandroid/os/ParcelUuid;ILandroid/os/ParcelUuid;Landroid/os/ParcelUuid;[B)V
    .locals 8
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # Landroid/os/ParcelUuid;
    .param p6    # I
    .param p7    # Landroid/os/ParcelUuid;
    .param p8    # Landroid/os/ParcelUuid;
    .param p9    # [B

    const-string v5, "BtGatt.BluetoothGatt"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "onDescriptorRead() - Device="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " UUID="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v5, p0, Lcom/broadcom/bt/gatt/BluetoothGatt$3;->this$0:Lcom/broadcom/bt/gatt/BluetoothGatt;

    # getter for: Lcom/broadcom/bt/gatt/BluetoothGatt;->mAdapter:Landroid/bluetooth/BluetoothAdapter;
    invoke-static {v5}, Lcom/broadcom/bt/gatt/BluetoothGatt;->access$600(Lcom/broadcom/bt/gatt/BluetoothGatt;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v5

    invoke-virtual {v5, p1}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v3

    iget-object v5, p0, Lcom/broadcom/bt/gatt/BluetoothGatt$3;->this$0:Lcom/broadcom/bt/gatt/BluetoothGatt;

    invoke-virtual {p5}, Landroid/os/ParcelUuid;->getUuid()Ljava/util/UUID;

    move-result-object v6

    invoke-virtual {v5, v3, v6, p4, p3}, Lcom/broadcom/bt/gatt/BluetoothGatt;->getService(Landroid/bluetooth/BluetoothDevice;Ljava/util/UUID;II)Lcom/broadcom/bt/gatt/BluetoothGattService;

    move-result-object v4

    if-nez v4, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p7}, Landroid/os/ParcelUuid;->getUuid()Ljava/util/UUID;

    move-result-object v5

    invoke-virtual {v4, v5, p6}, Lcom/broadcom/bt/gatt/BluetoothGattService;->getCharacteristic(Ljava/util/UUID;I)Lcom/broadcom/bt/gatt/BluetoothGattCharacteristic;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual/range {p8 .. p8}, Landroid/os/ParcelUuid;->getUuid()Ljava/util/UUID;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/broadcom/bt/gatt/BluetoothGattCharacteristic;->getDescriptor(Ljava/util/UUID;)Lcom/broadcom/bt/gatt/BluetoothGattDescriptor;

    move-result-object v2

    if-eqz v2, :cond_0

    if-nez p2, :cond_2

    move-object/from16 v0, p9

    invoke-virtual {v2, v0}, Lcom/broadcom/bt/gatt/BluetoothGattDescriptor;->setValue([B)Z

    :cond_2
    iget-object v5, p0, Lcom/broadcom/bt/gatt/BluetoothGatt$3;->this$0:Lcom/broadcom/bt/gatt/BluetoothGatt;

    # getter for: Lcom/broadcom/bt/gatt/BluetoothGatt;->mCallback:Lcom/broadcom/bt/gatt/BluetoothGattCallback;
    invoke-static {v5}, Lcom/broadcom/bt/gatt/BluetoothGatt;->access$500(Lcom/broadcom/bt/gatt/BluetoothGatt;)Lcom/broadcom/bt/gatt/BluetoothGattCallback;

    move-result-object v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/broadcom/bt/gatt/BluetoothGatt$3;->this$0:Lcom/broadcom/bt/gatt/BluetoothGatt;

    # getter for: Lcom/broadcom/bt/gatt/BluetoothGatt;->mCallback:Lcom/broadcom/bt/gatt/BluetoothGattCallback;
    invoke-static {v5}, Lcom/broadcom/bt/gatt/BluetoothGatt;->access$500(Lcom/broadcom/bt/gatt/BluetoothGatt;)Lcom/broadcom/bt/gatt/BluetoothGattCallback;

    move-result-object v5

    invoke-virtual {v5, v2, p2}, Lcom/broadcom/bt/gatt/BluetoothGattCallback;->onDescriptorRead(Lcom/broadcom/bt/gatt/BluetoothGattDescriptor;I)V

    goto :goto_0
.end method

.method public onDescriptorWrite(Ljava/lang/String;IIILandroid/os/ParcelUuid;ILandroid/os/ParcelUuid;Landroid/os/ParcelUuid;)V
    .locals 7
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # Landroid/os/ParcelUuid;
    .param p6    # I
    .param p7    # Landroid/os/ParcelUuid;
    .param p8    # Landroid/os/ParcelUuid;

    const-string v4, "BtGatt.BluetoothGatt"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onDescriptorWrite() - Device="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " UUID="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/broadcom/bt/gatt/BluetoothGatt$3;->this$0:Lcom/broadcom/bt/gatt/BluetoothGatt;

    # getter for: Lcom/broadcom/bt/gatt/BluetoothGatt;->mAdapter:Landroid/bluetooth/BluetoothAdapter;
    invoke-static {v4}, Lcom/broadcom/bt/gatt/BluetoothGatt;->access$600(Lcom/broadcom/bt/gatt/BluetoothGatt;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v4

    invoke-virtual {v4, p1}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v2

    iget-object v4, p0, Lcom/broadcom/bt/gatt/BluetoothGatt$3;->this$0:Lcom/broadcom/bt/gatt/BluetoothGatt;

    invoke-virtual {p5}, Landroid/os/ParcelUuid;->getUuid()Ljava/util/UUID;

    move-result-object v5

    invoke-virtual {v4, v2, v5, p4, p3}, Lcom/broadcom/bt/gatt/BluetoothGatt;->getService(Landroid/bluetooth/BluetoothDevice;Ljava/util/UUID;II)Lcom/broadcom/bt/gatt/BluetoothGattService;

    move-result-object v3

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p7}, Landroid/os/ParcelUuid;->getUuid()Ljava/util/UUID;

    move-result-object v4

    invoke-virtual {v3, v4, p6}, Lcom/broadcom/bt/gatt/BluetoothGattService;->getCharacteristic(Ljava/util/UUID;I)Lcom/broadcom/bt/gatt/BluetoothGattCharacteristic;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p8}, Landroid/os/ParcelUuid;->getUuid()Ljava/util/UUID;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/broadcom/bt/gatt/BluetoothGattCharacteristic;->getDescriptor(Ljava/util/UUID;)Lcom/broadcom/bt/gatt/BluetoothGattDescriptor;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v4, p0, Lcom/broadcom/bt/gatt/BluetoothGatt$3;->this$0:Lcom/broadcom/bt/gatt/BluetoothGatt;

    # getter for: Lcom/broadcom/bt/gatt/BluetoothGatt;->mCallback:Lcom/broadcom/bt/gatt/BluetoothGattCallback;
    invoke-static {v4}, Lcom/broadcom/bt/gatt/BluetoothGatt;->access$500(Lcom/broadcom/bt/gatt/BluetoothGatt;)Lcom/broadcom/bt/gatt/BluetoothGattCallback;

    move-result-object v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/broadcom/bt/gatt/BluetoothGatt$3;->this$0:Lcom/broadcom/bt/gatt/BluetoothGatt;

    # getter for: Lcom/broadcom/bt/gatt/BluetoothGatt;->mCallback:Lcom/broadcom/bt/gatt/BluetoothGattCallback;
    invoke-static {v4}, Lcom/broadcom/bt/gatt/BluetoothGatt;->access$500(Lcom/broadcom/bt/gatt/BluetoothGatt;)Lcom/broadcom/bt/gatt/BluetoothGattCallback;

    move-result-object v4

    invoke-virtual {v4, v1, p2}, Lcom/broadcom/bt/gatt/BluetoothGattCallback;->onDescriptorWrite(Lcom/broadcom/bt/gatt/BluetoothGattDescriptor;I)V

    goto :goto_0
.end method

.method public onGetCharacteristic(Ljava/lang/String;IILandroid/os/ParcelUuid;ILandroid/os/ParcelUuid;I)V
    .locals 7
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # I
    .param p4    # Landroid/os/ParcelUuid;
    .param p5    # I
    .param p6    # Landroid/os/ParcelUuid;
    .param p7    # I

    const-string v0, "BtGatt.BluetoothGatt"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onGetCharacteristic() - Device="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " UUID="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/broadcom/bt/gatt/BluetoothGatt$3;->this$0:Lcom/broadcom/bt/gatt/BluetoothGatt;

    # getter for: Lcom/broadcom/bt/gatt/BluetoothGatt;->mAdapter:Landroid/bluetooth/BluetoothAdapter;
    invoke-static {v0}, Lcom/broadcom/bt/gatt/BluetoothGatt;->access$600(Lcom/broadcom/bt/gatt/BluetoothGatt;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v6

    iget-object v0, p0, Lcom/broadcom/bt/gatt/BluetoothGatt$3;->this$0:Lcom/broadcom/bt/gatt/BluetoothGatt;

    invoke-virtual {p4}, Landroid/os/ParcelUuid;->getUuid()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v0, v6, v2, p3, p2}, Lcom/broadcom/bt/gatt/BluetoothGatt;->getService(Landroid/bluetooth/BluetoothDevice;Ljava/util/UUID;II)Lcom/broadcom/bt/gatt/BluetoothGattService;

    move-result-object v1

    if-eqz v1, :cond_0

    new-instance v0, Lcom/broadcom/bt/gatt/BluetoothGattCharacteristic;

    invoke-virtual {p6}, Landroid/os/ParcelUuid;->getUuid()Ljava/util/UUID;

    move-result-object v2

    const/4 v5, 0x0

    move v3, p5

    move v4, p7

    invoke-direct/range {v0 .. v5}, Lcom/broadcom/bt/gatt/BluetoothGattCharacteristic;-><init>(Lcom/broadcom/bt/gatt/BluetoothGattService;Ljava/util/UUID;III)V

    invoke-virtual {v1, v0}, Lcom/broadcom/bt/gatt/BluetoothGattService;->addCharacteristic(Lcom/broadcom/bt/gatt/BluetoothGattCharacteristic;)V

    :cond_0
    return-void
.end method

.method public onGetDescriptor(Ljava/lang/String;IILandroid/os/ParcelUuid;ILandroid/os/ParcelUuid;Landroid/os/ParcelUuid;)V
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # I
    .param p4    # Landroid/os/ParcelUuid;
    .param p5    # I
    .param p6    # Landroid/os/ParcelUuid;
    .param p7    # Landroid/os/ParcelUuid;

    const-string v3, "BtGatt.BluetoothGatt"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onGetDescriptor() - Device="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " UUID="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/broadcom/bt/gatt/BluetoothGatt$3;->this$0:Lcom/broadcom/bt/gatt/BluetoothGatt;

    # getter for: Lcom/broadcom/bt/gatt/BluetoothGatt;->mAdapter:Landroid/bluetooth/BluetoothAdapter;
    invoke-static {v3}, Lcom/broadcom/bt/gatt/BluetoothGatt;->access$600(Lcom/broadcom/bt/gatt/BluetoothGatt;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v3

    invoke-virtual {v3, p1}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v1

    iget-object v3, p0, Lcom/broadcom/bt/gatt/BluetoothGatt$3;->this$0:Lcom/broadcom/bt/gatt/BluetoothGatt;

    invoke-virtual {p4}, Landroid/os/ParcelUuid;->getUuid()Ljava/util/UUID;

    move-result-object v4

    invoke-virtual {v3, v1, v4, p3, p2}, Lcom/broadcom/bt/gatt/BluetoothGatt;->getService(Landroid/bluetooth/BluetoothDevice;Ljava/util/UUID;II)Lcom/broadcom/bt/gatt/BluetoothGattService;

    move-result-object v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p6}, Landroid/os/ParcelUuid;->getUuid()Ljava/util/UUID;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/broadcom/bt/gatt/BluetoothGattService;->getCharacteristic(Ljava/util/UUID;)Lcom/broadcom/bt/gatt/BluetoothGattCharacteristic;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v3, Lcom/broadcom/bt/gatt/BluetoothGattDescriptor;

    invoke-virtual {p7}, Landroid/os/ParcelUuid;->getUuid()Ljava/util/UUID;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct {v3, v0, v4, v5}, Lcom/broadcom/bt/gatt/BluetoothGattDescriptor;-><init>(Lcom/broadcom/bt/gatt/BluetoothGattCharacteristic;Ljava/util/UUID;I)V

    invoke-virtual {v0, v3}, Lcom/broadcom/bt/gatt/BluetoothGattCharacteristic;->addDescriptor(Lcom/broadcom/bt/gatt/BluetoothGattDescriptor;)V

    goto :goto_0
.end method

.method public onGetService(Ljava/lang/String;IILandroid/os/ParcelUuid;)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # I
    .param p4    # Landroid/os/ParcelUuid;

    const-string v1, "BtGatt.BluetoothGatt"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onGetService() - Device="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " UUID="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/broadcom/bt/gatt/BluetoothGatt$3;->this$0:Lcom/broadcom/bt/gatt/BluetoothGatt;

    # getter for: Lcom/broadcom/bt/gatt/BluetoothGatt;->mAdapter:Landroid/bluetooth/BluetoothAdapter;
    invoke-static {v1}, Lcom/broadcom/bt/gatt/BluetoothGatt;->access$600(Lcom/broadcom/bt/gatt/BluetoothGatt;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v0

    iget-object v1, p0, Lcom/broadcom/bt/gatt/BluetoothGatt$3;->this$0:Lcom/broadcom/bt/gatt/BluetoothGatt;

    # getter for: Lcom/broadcom/bt/gatt/BluetoothGatt;->mServices:Ljava/util/List;
    invoke-static {v1}, Lcom/broadcom/bt/gatt/BluetoothGatt;->access$700(Lcom/broadcom/bt/gatt/BluetoothGatt;)Ljava/util/List;

    move-result-object v1

    new-instance v2, Lcom/broadcom/bt/gatt/BluetoothGattService;

    invoke-virtual {p4}, Landroid/os/ParcelUuid;->getUuid()Ljava/util/UUID;

    move-result-object v3

    invoke-direct {v2, v0, v3, p3, p2}, Lcom/broadcom/bt/gatt/BluetoothGattService;-><init>(Landroid/bluetooth/BluetoothDevice;Ljava/util/UUID;II)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public onNotify(Ljava/lang/String;IILandroid/os/ParcelUuid;ILandroid/os/ParcelUuid;[B)V
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # I
    .param p4    # Landroid/os/ParcelUuid;
    .param p5    # I
    .param p6    # Landroid/os/ParcelUuid;
    .param p7    # [B

    const-string v3, "BtGatt.BluetoothGatt"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onNotify() - Device="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " UUID="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/broadcom/bt/gatt/BluetoothGatt$3;->this$0:Lcom/broadcom/bt/gatt/BluetoothGatt;

    # getter for: Lcom/broadcom/bt/gatt/BluetoothGatt;->mAdapter:Landroid/bluetooth/BluetoothAdapter;
    invoke-static {v3}, Lcom/broadcom/bt/gatt/BluetoothGatt;->access$600(Lcom/broadcom/bt/gatt/BluetoothGatt;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v3

    invoke-virtual {v3, p1}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v1

    iget-object v3, p0, Lcom/broadcom/bt/gatt/BluetoothGatt$3;->this$0:Lcom/broadcom/bt/gatt/BluetoothGatt;

    invoke-virtual {p4}, Landroid/os/ParcelUuid;->getUuid()Ljava/util/UUID;

    move-result-object v4

    invoke-virtual {v3, v1, v4, p3, p2}, Lcom/broadcom/bt/gatt/BluetoothGatt;->getService(Landroid/bluetooth/BluetoothDevice;Ljava/util/UUID;II)Lcom/broadcom/bt/gatt/BluetoothGattService;

    move-result-object v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p6}, Landroid/os/ParcelUuid;->getUuid()Ljava/util/UUID;

    move-result-object v3

    invoke-virtual {v2, v3, p5}, Lcom/broadcom/bt/gatt/BluetoothGattService;->getCharacteristic(Ljava/util/UUID;I)Lcom/broadcom/bt/gatt/BluetoothGattCharacteristic;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p7}, Lcom/broadcom/bt/gatt/BluetoothGattCharacteristic;->setValue([B)Z

    iget-object v3, p0, Lcom/broadcom/bt/gatt/BluetoothGatt$3;->this$0:Lcom/broadcom/bt/gatt/BluetoothGatt;

    # getter for: Lcom/broadcom/bt/gatt/BluetoothGatt;->mCallback:Lcom/broadcom/bt/gatt/BluetoothGattCallback;
    invoke-static {v3}, Lcom/broadcom/bt/gatt/BluetoothGatt;->access$500(Lcom/broadcom/bt/gatt/BluetoothGatt;)Lcom/broadcom/bt/gatt/BluetoothGattCallback;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/broadcom/bt/gatt/BluetoothGatt$3;->this$0:Lcom/broadcom/bt/gatt/BluetoothGatt;

    # getter for: Lcom/broadcom/bt/gatt/BluetoothGatt;->mCallback:Lcom/broadcom/bt/gatt/BluetoothGattCallback;
    invoke-static {v3}, Lcom/broadcom/bt/gatt/BluetoothGatt;->access$500(Lcom/broadcom/bt/gatt/BluetoothGatt;)Lcom/broadcom/bt/gatt/BluetoothGattCallback;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/broadcom/bt/gatt/BluetoothGattCallback;->onCharacteristicChanged(Lcom/broadcom/bt/gatt/BluetoothGattCharacteristic;)V

    goto :goto_0
.end method

.method public onScanResult(Ljava/lang/String;I[B)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # [B

    const-string v0, "BtGatt.BluetoothGatt"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onScanResult() - Device="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " RSSI="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/broadcom/bt/gatt/BluetoothGatt$3;->this$0:Lcom/broadcom/bt/gatt/BluetoothGatt;

    # getter for: Lcom/broadcom/bt/gatt/BluetoothGatt;->mCallback:Lcom/broadcom/bt/gatt/BluetoothGattCallback;
    invoke-static {v0}, Lcom/broadcom/bt/gatt/BluetoothGatt;->access$500(Lcom/broadcom/bt/gatt/BluetoothGatt;)Lcom/broadcom/bt/gatt/BluetoothGattCallback;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/broadcom/bt/gatt/BluetoothGatt$3;->this$0:Lcom/broadcom/bt/gatt/BluetoothGatt;

    # getter for: Lcom/broadcom/bt/gatt/BluetoothGatt;->mCallback:Lcom/broadcom/bt/gatt/BluetoothGattCallback;
    invoke-static {v0}, Lcom/broadcom/bt/gatt/BluetoothGatt;->access$500(Lcom/broadcom/bt/gatt/BluetoothGatt;)Lcom/broadcom/bt/gatt/BluetoothGattCallback;

    move-result-object v0

    iget-object v1, p0, Lcom/broadcom/bt/gatt/BluetoothGatt$3;->this$0:Lcom/broadcom/bt/gatt/BluetoothGatt;

    # getter for: Lcom/broadcom/bt/gatt/BluetoothGatt;->mAdapter:Landroid/bluetooth/BluetoothAdapter;
    invoke-static {v1}, Lcom/broadcom/bt/gatt/BluetoothGatt;->access$600(Lcom/broadcom/bt/gatt/BluetoothGatt;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v1

    invoke-virtual {v0, v1, p2, p3}, Lcom/broadcom/bt/gatt/BluetoothGattCallback;->onScanResult(Landroid/bluetooth/BluetoothDevice;I[B)V

    :cond_0
    return-void
.end method

.method public onSearchComplete(Ljava/lang/String;I)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # I

    const-string v1, "BtGatt.BluetoothGatt"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onSearchComplete() = Device="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " Status="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/broadcom/bt/gatt/BluetoothGatt$3;->this$0:Lcom/broadcom/bt/gatt/BluetoothGatt;

    # getter for: Lcom/broadcom/bt/gatt/BluetoothGatt;->mCallback:Lcom/broadcom/bt/gatt/BluetoothGattCallback;
    invoke-static {v1}, Lcom/broadcom/bt/gatt/BluetoothGatt;->access$500(Lcom/broadcom/bt/gatt/BluetoothGatt;)Lcom/broadcom/bt/gatt/BluetoothGattCallback;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/broadcom/bt/gatt/BluetoothGatt$3;->this$0:Lcom/broadcom/bt/gatt/BluetoothGatt;

    # getter for: Lcom/broadcom/bt/gatt/BluetoothGatt;->mAdapter:Landroid/bluetooth/BluetoothAdapter;
    invoke-static {v1}, Lcom/broadcom/bt/gatt/BluetoothGatt;->access$600(Lcom/broadcom/bt/gatt/BluetoothGatt;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v0

    iget-object v1, p0, Lcom/broadcom/bt/gatt/BluetoothGatt$3;->this$0:Lcom/broadcom/bt/gatt/BluetoothGatt;

    # getter for: Lcom/broadcom/bt/gatt/BluetoothGatt;->mCallback:Lcom/broadcom/bt/gatt/BluetoothGattCallback;
    invoke-static {v1}, Lcom/broadcom/bt/gatt/BluetoothGatt;->access$500(Lcom/broadcom/bt/gatt/BluetoothGatt;)Lcom/broadcom/bt/gatt/BluetoothGattCallback;

    move-result-object v1

    invoke-virtual {v1, v0, p2}, Lcom/broadcom/bt/gatt/BluetoothGattCallback;->onServicesDiscovered(Landroid/bluetooth/BluetoothDevice;I)V

    :cond_0
    return-void
.end method
