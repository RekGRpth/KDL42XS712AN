.class public Lcom/broadcom/bt/gatt/MutableBluetoothGattDescriptor;
.super Lcom/broadcom/bt/gatt/BluetoothGattDescriptor;
.source "MutableBluetoothGattDescriptor.java"


# direct methods
.method public constructor <init>(Ljava/util/UUID;I)V
    .locals 1
    .param p1    # Ljava/util/UUID;
    .param p2    # I

    const/4 v0, 0x0

    invoke-direct {p0, v0, p1, p2}, Lcom/broadcom/bt/gatt/BluetoothGattDescriptor;-><init>(Lcom/broadcom/bt/gatt/BluetoothGattCharacteristic;Ljava/util/UUID;I)V

    return-void
.end method


# virtual methods
.method setCharacteristic(Lcom/broadcom/bt/gatt/BluetoothGattCharacteristic;)V
    .locals 0
    .param p1    # Lcom/broadcom/bt/gatt/BluetoothGattCharacteristic;

    iput-object p1, p0, Lcom/broadcom/bt/gatt/MutableBluetoothGattDescriptor;->mCharacteristic:Lcom/broadcom/bt/gatt/BluetoothGattCharacteristic;

    return-void
.end method
