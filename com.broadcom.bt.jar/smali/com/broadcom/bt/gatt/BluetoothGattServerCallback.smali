.class public abstract Lcom/broadcom/bt/gatt/BluetoothGattServerCallback;
.super Ljava/lang/Object;
.source "BluetoothGattServerCallback.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAppRegistered(I)V
    .locals 0
    .param p1    # I

    return-void
.end method

.method public onCharacteristicReadRequest(Landroid/bluetooth/BluetoothDevice;IILcom/broadcom/bt/gatt/BluetoothGattCharacteristic;)V
    .locals 0
    .param p1    # Landroid/bluetooth/BluetoothDevice;
    .param p2    # I
    .param p3    # I
    .param p4    # Lcom/broadcom/bt/gatt/BluetoothGattCharacteristic;

    return-void
.end method

.method public onCharacteristicWriteRequest(Landroid/bluetooth/BluetoothDevice;ILcom/broadcom/bt/gatt/BluetoothGattCharacteristic;ZZI[B)V
    .locals 0
    .param p1    # Landroid/bluetooth/BluetoothDevice;
    .param p2    # I
    .param p3    # Lcom/broadcom/bt/gatt/BluetoothGattCharacteristic;
    .param p4    # Z
    .param p5    # Z
    .param p6    # I
    .param p7    # [B

    return-void
.end method

.method public onConnectionStateChange(Landroid/bluetooth/BluetoothDevice;II)V
    .locals 0
    .param p1    # Landroid/bluetooth/BluetoothDevice;
    .param p2    # I
    .param p3    # I

    return-void
.end method

.method public onDescriptorReadRequest(Landroid/bluetooth/BluetoothDevice;IILcom/broadcom/bt/gatt/BluetoothGattDescriptor;)V
    .locals 0
    .param p1    # Landroid/bluetooth/BluetoothDevice;
    .param p2    # I
    .param p3    # I
    .param p4    # Lcom/broadcom/bt/gatt/BluetoothGattDescriptor;

    return-void
.end method

.method public onDescriptorWriteRequest(Landroid/bluetooth/BluetoothDevice;ILcom/broadcom/bt/gatt/BluetoothGattDescriptor;ZZI[B)V
    .locals 0
    .param p1    # Landroid/bluetooth/BluetoothDevice;
    .param p2    # I
    .param p3    # Lcom/broadcom/bt/gatt/BluetoothGattDescriptor;
    .param p4    # Z
    .param p5    # Z
    .param p6    # I
    .param p7    # [B

    return-void
.end method

.method public onExecuteWrite(Landroid/bluetooth/BluetoothDevice;IZ)V
    .locals 0
    .param p1    # Landroid/bluetooth/BluetoothDevice;
    .param p2    # I
    .param p3    # Z

    return-void
.end method

.method public onScanResult(Landroid/bluetooth/BluetoothDevice;I[B)V
    .locals 0
    .param p1    # Landroid/bluetooth/BluetoothDevice;
    .param p2    # I
    .param p3    # [B

    return-void
.end method

.method public onServiceAdded(ILcom/broadcom/bt/gatt/BluetoothGattService;)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/broadcom/bt/gatt/BluetoothGattService;

    return-void
.end method
