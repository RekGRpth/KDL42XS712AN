.class Lcom/broadcom/bt/gatt/BluetoothGatt$2;
.super Ljava/lang/Object;
.source "BluetoothGatt.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/broadcom/bt/gatt/BluetoothGatt;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/broadcom/bt/gatt/BluetoothGatt;


# direct methods
.method constructor <init>(Lcom/broadcom/bt/gatt/BluetoothGatt;)V
    .locals 0

    iput-object p1, p0, Lcom/broadcom/bt/gatt/BluetoothGatt$2;->this$0:Lcom/broadcom/bt/gatt/BluetoothGatt;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3
    .param p1    # Landroid/content/ComponentName;
    .param p2    # Landroid/os/IBinder;

    const-string v0, "BtGatt.BluetoothGatt"

    const-string v1, "Proxy object connected"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/broadcom/bt/gatt/BluetoothGatt$2;->this$0:Lcom/broadcom/bt/gatt/BluetoothGatt;

    invoke-static {p2}, Lcom/broadcom/bt/gatt/IBluetoothGatt$Stub;->asInterface(Landroid/os/IBinder;)Lcom/broadcom/bt/gatt/IBluetoothGatt;

    move-result-object v1

    # setter for: Lcom/broadcom/bt/gatt/BluetoothGatt;->mService:Lcom/broadcom/bt/gatt/IBluetoothGatt;
    invoke-static {v0, v1}, Lcom/broadcom/bt/gatt/BluetoothGatt;->access$102(Lcom/broadcom/bt/gatt/BluetoothGatt;Lcom/broadcom/bt/gatt/IBluetoothGatt;)Lcom/broadcom/bt/gatt/IBluetoothGatt;

    iget-object v0, p0, Lcom/broadcom/bt/gatt/BluetoothGatt$2;->this$0:Lcom/broadcom/bt/gatt/BluetoothGatt;

    # getter for: Lcom/broadcom/bt/gatt/BluetoothGatt;->mServiceListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;
    invoke-static {v0}, Lcom/broadcom/bt/gatt/BluetoothGatt;->access$300(Lcom/broadcom/bt/gatt/BluetoothGatt;)Landroid/bluetooth/BluetoothProfile$ServiceListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/broadcom/bt/gatt/BluetoothGatt$2;->this$0:Lcom/broadcom/bt/gatt/BluetoothGatt;

    # getter for: Lcom/broadcom/bt/gatt/BluetoothGatt;->mServiceListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;
    invoke-static {v0}, Lcom/broadcom/bt/gatt/BluetoothGatt;->access$300(Lcom/broadcom/bt/gatt/BluetoothGatt;)Landroid/bluetooth/BluetoothProfile$ServiceListener;

    move-result-object v0

    const/4 v1, 0x7

    iget-object v2, p0, Lcom/broadcom/bt/gatt/BluetoothGatt$2;->this$0:Lcom/broadcom/bt/gatt/BluetoothGatt;

    invoke-interface {v0, v1, v2}, Landroid/bluetooth/BluetoothProfile$ServiceListener;->onServiceConnected(ILandroid/bluetooth/BluetoothProfile;)V

    :cond_0
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1    # Landroid/content/ComponentName;

    const-string v0, "BtGatt.BluetoothGatt"

    const-string v1, "Proxy object disconnected"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/broadcom/bt/gatt/BluetoothGatt$2;->this$0:Lcom/broadcom/bt/gatt/BluetoothGatt;

    const/4 v1, 0x0

    # setter for: Lcom/broadcom/bt/gatt/BluetoothGatt;->mService:Lcom/broadcom/bt/gatt/IBluetoothGatt;
    invoke-static {v0, v1}, Lcom/broadcom/bt/gatt/BluetoothGatt;->access$102(Lcom/broadcom/bt/gatt/BluetoothGatt;Lcom/broadcom/bt/gatt/IBluetoothGatt;)Lcom/broadcom/bt/gatt/IBluetoothGatt;

    iget-object v0, p0, Lcom/broadcom/bt/gatt/BluetoothGatt$2;->this$0:Lcom/broadcom/bt/gatt/BluetoothGatt;

    # getter for: Lcom/broadcom/bt/gatt/BluetoothGatt;->mServiceListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;
    invoke-static {v0}, Lcom/broadcom/bt/gatt/BluetoothGatt;->access$300(Lcom/broadcom/bt/gatt/BluetoothGatt;)Landroid/bluetooth/BluetoothProfile$ServiceListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/broadcom/bt/gatt/BluetoothGatt$2;->this$0:Lcom/broadcom/bt/gatt/BluetoothGatt;

    # getter for: Lcom/broadcom/bt/gatt/BluetoothGatt;->mServiceListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;
    invoke-static {v0}, Lcom/broadcom/bt/gatt/BluetoothGatt;->access$300(Lcom/broadcom/bt/gatt/BluetoothGatt;)Landroid/bluetooth/BluetoothProfile$ServiceListener;

    move-result-object v0

    const/4 v1, 0x7

    invoke-interface {v0, v1}, Landroid/bluetooth/BluetoothProfile$ServiceListener;->onServiceDisconnected(I)V

    :cond_0
    return-void
.end method
