.class public Lcom/broadcom/bt/map/FolderInfo;
.super Ljava/lang/Object;
.source "FolderInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/broadcom/bt/map/FolderInfo;",
            ">;"
        }
    .end annotation
.end field

.field public static final MODE_READ_ONLY:J = 0x1L

.field public static final VIRTUAL_FOLDER_DELETED:Ljava/lang/String; = "deleted"

.field public static final VIRTUAL_FOLDER_DRAFT:Ljava/lang/String; = "draft"

.field public static final VIRTUAL_FOLDER_INBOX:Ljava/lang/String; = "inbox"

.field public static final VIRTUAL_FOLDER_OUTBOX:Ljava/lang/String; = "outbox"

.field public static final VIRTUAL_FOLDER_SENT:Ljava/lang/String; = "sent"


# instance fields
.field public mCreatedDateTimeMS:Ljava/lang/String;

.field public mFolderId:Ljava/lang/String;

.field public mFolderName:Ljava/lang/String;

.field public mFolderSizeBytes:J

.field public mMode:J

.field public mVirtualName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/broadcom/bt/map/FolderInfo$1;

    invoke-direct {v0}, Lcom/broadcom/bt/map/FolderInfo$1;-><init>()V

    sput-object v0, Lcom/broadcom/bt/map/FolderInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const-wide/16 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/broadcom/bt/map/FolderInfo;->mVirtualName:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/broadcom/bt/map/FolderInfo;->mFolderName:Ljava/lang/String;

    iput-wide v1, p0, Lcom/broadcom/bt/map/FolderInfo;->mFolderSizeBytes:J

    const-string v0, ""

    iput-object v0, p0, Lcom/broadcom/bt/map/FolderInfo;->mCreatedDateTimeMS:Ljava/lang/String;

    iput-wide v1, p0, Lcom/broadcom/bt/map/FolderInfo;->mMode:J

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3
    .param p1    # Landroid/os/Parcel;

    const-wide/16 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/broadcom/bt/map/FolderInfo;->mVirtualName:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/broadcom/bt/map/FolderInfo;->mFolderName:Ljava/lang/String;

    iput-wide v1, p0, Lcom/broadcom/bt/map/FolderInfo;->mFolderSizeBytes:J

    const-string v0, ""

    iput-object v0, p0, Lcom/broadcom/bt/map/FolderInfo;->mCreatedDateTimeMS:Ljava/lang/String;

    iput-wide v1, p0, Lcom/broadcom/bt/map/FolderInfo;->mMode:J

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/broadcom/bt/map/FolderInfo;->mFolderName:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/broadcom/bt/map/FolderInfo;->mFolderSizeBytes:J

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/broadcom/bt/map/FolderInfo;->mCreatedDateTimeMS:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/broadcom/bt/map/FolderInfo;->mMode:J

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;J)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # J
    .param p6    # Ljava/lang/String;
    .param p7    # J

    const-wide/16 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/broadcom/bt/map/FolderInfo;->mVirtualName:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/broadcom/bt/map/FolderInfo;->mFolderName:Ljava/lang/String;

    iput-wide v1, p0, Lcom/broadcom/bt/map/FolderInfo;->mFolderSizeBytes:J

    const-string v0, ""

    iput-object v0, p0, Lcom/broadcom/bt/map/FolderInfo;->mCreatedDateTimeMS:Ljava/lang/String;

    iput-wide v1, p0, Lcom/broadcom/bt/map/FolderInfo;->mMode:J

    iput-object p1, p0, Lcom/broadcom/bt/map/FolderInfo;->mFolderId:Ljava/lang/String;

    iput-object p3, p0, Lcom/broadcom/bt/map/FolderInfo;->mVirtualName:Ljava/lang/String;

    iput-object p3, p0, Lcom/broadcom/bt/map/FolderInfo;->mFolderName:Ljava/lang/String;

    iput-wide p4, p0, Lcom/broadcom/bt/map/FolderInfo;->mFolderSizeBytes:J

    iput-object p6, p0, Lcom/broadcom/bt/map/FolderInfo;->mCreatedDateTimeMS:Ljava/lang/String;

    iput-wide p7, p0, Lcom/broadcom/bt/map/FolderInfo;->mMode:J

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public dumpState(Ljava/lang/StringBuilder;Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/StringBuilder;
    .param p2    # Ljava/lang/String;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "Name    :"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/broadcom/bt/map/FolderInfo;->mFolderName:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "\n"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "Size    : "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v0, p0, Lcom/broadcom/bt/map/FolderInfo;->mFolderSizeBytes:J

    invoke-virtual {p1, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v0, "\n"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "Created : "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/broadcom/bt/map/FolderInfo;->mCreatedDateTimeMS:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "\n"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "Mode : "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v0, p0, Lcom/broadcom/bt/map/FolderInfo;->mMode:J

    invoke-virtual {p1, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v0, "\n"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    return-void

    :cond_0
    iget-object v0, p0, Lcom/broadcom/bt/map/FolderInfo;->mFolderName:Ljava/lang/String;

    goto :goto_0
.end method

.method public isReadOnly()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget-object v0, p0, Lcom/broadcom/bt/map/FolderInfo;->mFolderName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-wide v0, p0, Lcom/broadcom/bt/map/FolderInfo;->mFolderSizeBytes:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    iget-object v0, p0, Lcom/broadcom/bt/map/FolderInfo;->mCreatedDateTimeMS:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-wide v0, p0, Lcom/broadcom/bt/map/FolderInfo;->mMode:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    return-void
.end method
