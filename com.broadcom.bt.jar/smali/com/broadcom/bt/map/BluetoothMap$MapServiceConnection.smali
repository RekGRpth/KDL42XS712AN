.class Lcom/broadcom/bt/map/BluetoothMap$MapServiceConnection;
.super Ljava/lang/Object;
.source "BluetoothMap.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/broadcom/bt/map/BluetoothMap;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MapServiceConnection"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/broadcom/bt/map/BluetoothMap;


# direct methods
.method private constructor <init>(Lcom/broadcom/bt/map/BluetoothMap;)V
    .locals 0

    iput-object p1, p0, Lcom/broadcom/bt/map/BluetoothMap$MapServiceConnection;->this$0:Lcom/broadcom/bt/map/BluetoothMap;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/broadcom/bt/map/BluetoothMap;Lcom/broadcom/bt/map/BluetoothMap$1;)V
    .locals 0
    .param p1    # Lcom/broadcom/bt/map/BluetoothMap;
    .param p2    # Lcom/broadcom/bt/map/BluetoothMap$1;

    invoke-direct {p0, p1}, Lcom/broadcom/bt/map/BluetoothMap$MapServiceConnection;-><init>(Lcom/broadcom/bt/map/BluetoothMap;)V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 5
    .param p1    # Landroid/content/ComponentName;
    .param p2    # Landroid/os/IBinder;

    const/4 v4, 0x0

    const-string v1, "BtMap.BluetoothMap"

    const-string v2, "BluetoothMap Proxy object connected"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/broadcom/bt/map/BluetoothMap$MapServiceConnection;->this$0:Lcom/broadcom/bt/map/BluetoothMap;

    monitor-enter v2

    :try_start_0
    iget-object v1, p0, Lcom/broadcom/bt/map/BluetoothMap$MapServiceConnection;->this$0:Lcom/broadcom/bt/map/BluetoothMap;

    invoke-static {p2}, Lcom/broadcom/bt/map/IBluetoothMap$Stub;->asInterface(Landroid/os/IBinder;)Lcom/broadcom/bt/map/IBluetoothMap;

    move-result-object v3

    # setter for: Lcom/broadcom/bt/map/BluetoothMap;->mService:Lcom/broadcom/bt/map/IBluetoothMap;
    invoke-static {v1, v3}, Lcom/broadcom/bt/map/BluetoothMap;->access$002(Lcom/broadcom/bt/map/BluetoothMap;Lcom/broadcom/bt/map/IBluetoothMap;)Lcom/broadcom/bt/map/IBluetoothMap;

    iget-object v1, p0, Lcom/broadcom/bt/map/BluetoothMap$MapServiceConnection;->this$0:Lcom/broadcom/bt/map/BluetoothMap;

    const/4 v3, 0x2

    # setter for: Lcom/broadcom/bt/map/BluetoothMap;->mBindingState:I
    invoke-static {v1, v3}, Lcom/broadcom/bt/map/BluetoothMap;->access$102(Lcom/broadcom/bt/map/BluetoothMap;I)I

    iget-object v1, p0, Lcom/broadcom/bt/map/BluetoothMap$MapServiceConnection;->this$0:Lcom/broadcom/bt/map/BluetoothMap;

    # getter for: Lcom/broadcom/bt/map/BluetoothMap;->mPendingClose:Z
    invoke-static {v1}, Lcom/broadcom/bt/map/BluetoothMap;->access$200(Lcom/broadcom/bt/map/BluetoothMap;)Z

    move-result v0

    iget-object v1, p0, Lcom/broadcom/bt/map/BluetoothMap$MapServiceConnection;->this$0:Lcom/broadcom/bt/map/BluetoothMap;

    const/4 v3, 0x0

    # setter for: Lcom/broadcom/bt/map/BluetoothMap;->mPendingClose:Z
    invoke-static {v1, v3}, Lcom/broadcom/bt/map/BluetoothMap;->access$202(Lcom/broadcom/bt/map/BluetoothMap;Z)Z

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v1, p0, Lcom/broadcom/bt/map/BluetoothMap$MapServiceConnection;->this$0:Lcom/broadcom/bt/map/BluetoothMap;

    # getter for: Lcom/broadcom/bt/map/BluetoothMap;->mServiceListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;
    invoke-static {v1}, Lcom/broadcom/bt/map/BluetoothMap;->access$300(Lcom/broadcom/bt/map/BluetoothMap;)Landroid/bluetooth/BluetoothProfile$ServiceListener;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/broadcom/bt/map/BluetoothMap$MapServiceConnection;->this$0:Lcom/broadcom/bt/map/BluetoothMap;

    # getter for: Lcom/broadcom/bt/map/BluetoothMap;->mServiceListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;
    invoke-static {v1}, Lcom/broadcom/bt/map/BluetoothMap;->access$300(Lcom/broadcom/bt/map/BluetoothMap;)Landroid/bluetooth/BluetoothProfile$ServiceListener;

    move-result-object v1

    const/16 v2, 0x3e8

    iget-object v3, p0, Lcom/broadcom/bt/map/BluetoothMap$MapServiceConnection;->this$0:Lcom/broadcom/bt/map/BluetoothMap;

    invoke-interface {v1, v2, v3}, Landroid/bluetooth/BluetoothProfile$ServiceListener;->onServiceConnected(ILandroid/bluetooth/BluetoothProfile;)V

    :cond_0
    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/broadcom/bt/map/BluetoothMap$MapServiceConnection;->this$0:Lcom/broadcom/bt/map/BluetoothMap;

    invoke-virtual {v1}, Lcom/broadcom/bt/map/BluetoothMap;->close()V

    iget-object v1, p0, Lcom/broadcom/bt/map/BluetoothMap$MapServiceConnection;->this$0:Lcom/broadcom/bt/map/BluetoothMap;

    # setter for: Lcom/broadcom/bt/map/BluetoothMap;->mPendingClose:Z
    invoke-static {v1, v4}, Lcom/broadcom/bt/map/BluetoothMap;->access$202(Lcom/broadcom/bt/map/BluetoothMap;Z)Z

    :cond_1
    return-void

    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 3
    .param p1    # Landroid/content/ComponentName;

    const-string v0, "BtMap.BluetoothMap"

    const-string v1, "BluetoothMap Proxy object disconnected"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/broadcom/bt/map/BluetoothMap$MapServiceConnection;->this$0:Lcom/broadcom/bt/map/BluetoothMap;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/broadcom/bt/map/BluetoothMap$MapServiceConnection;->this$0:Lcom/broadcom/bt/map/BluetoothMap;

    const/4 v2, 0x0

    # setter for: Lcom/broadcom/bt/map/BluetoothMap;->mService:Lcom/broadcom/bt/map/IBluetoothMap;
    invoke-static {v0, v2}, Lcom/broadcom/bt/map/BluetoothMap;->access$002(Lcom/broadcom/bt/map/BluetoothMap;Lcom/broadcom/bt/map/IBluetoothMap;)Lcom/broadcom/bt/map/IBluetoothMap;

    iget-object v0, p0, Lcom/broadcom/bt/map/BluetoothMap$MapServiceConnection;->this$0:Lcom/broadcom/bt/map/BluetoothMap;

    const/4 v2, 0x0

    # setter for: Lcom/broadcom/bt/map/BluetoothMap;->mBindingState:I
    invoke-static {v0, v2}, Lcom/broadcom/bt/map/BluetoothMap;->access$102(Lcom/broadcom/bt/map/BluetoothMap;I)I

    iget-object v0, p0, Lcom/broadcom/bt/map/BluetoothMap$MapServiceConnection;->this$0:Lcom/broadcom/bt/map/BluetoothMap;

    # getter for: Lcom/broadcom/bt/map/BluetoothMap;->mServiceListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;
    invoke-static {v0}, Lcom/broadcom/bt/map/BluetoothMap;->access$300(Lcom/broadcom/bt/map/BluetoothMap;)Landroid/bluetooth/BluetoothProfile$ServiceListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/broadcom/bt/map/BluetoothMap$MapServiceConnection;->this$0:Lcom/broadcom/bt/map/BluetoothMap;

    # getter for: Lcom/broadcom/bt/map/BluetoothMap;->mServiceListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;
    invoke-static {v0}, Lcom/broadcom/bt/map/BluetoothMap;->access$300(Lcom/broadcom/bt/map/BluetoothMap;)Landroid/bluetooth/BluetoothProfile$ServiceListener;

    move-result-object v0

    const/16 v2, 0x3e8

    invoke-interface {v0, v2}, Landroid/bluetooth/BluetoothProfile$ServiceListener;->onServiceDisconnected(I)V

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
