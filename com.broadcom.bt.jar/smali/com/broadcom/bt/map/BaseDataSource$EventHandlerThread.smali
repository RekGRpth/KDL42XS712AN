.class public Lcom/broadcom/bt/map/BaseDataSource$EventHandlerThread;
.super Ljava/lang/Thread;
.source "BaseDataSource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/broadcom/bt/map/BaseDataSource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "EventHandlerThread"
.end annotation


# instance fields
.field public mHandler:Lcom/broadcom/bt/map/BaseDataSource$EventHandler;

.field final synthetic this$0:Lcom/broadcom/bt/map/BaseDataSource;


# direct methods
.method public constructor <init>(Lcom/broadcom/bt/map/BaseDataSource;)V
    .locals 1

    iput-object p1, p0, Lcom/broadcom/bt/map/BaseDataSource$EventHandlerThread;->this$0:Lcom/broadcom/bt/map/BaseDataSource;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Lcom/broadcom/bt/map/BaseDataSource$EventHandlerThread;->setPriority(I)V

    return-void
.end method


# virtual methods
.method public finish()V
    .locals 2

    iget-object v1, p0, Lcom/broadcom/bt/map/BaseDataSource$EventHandlerThread;->mHandler:Lcom/broadcom/bt/map/BaseDataSource$EventHandler;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/broadcom/bt/map/BaseDataSource$EventHandlerThread;->mHandler:Lcom/broadcom/bt/map/BaseDataSource$EventHandler;

    invoke-virtual {v1}, Lcom/broadcom/bt/map/BaseDataSource$EventHandler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    :cond_0
    return-void
.end method

.method public run()V
    .locals 2

    invoke-static {}, Landroid/os/Looper;->prepare()V

    new-instance v0, Lcom/broadcom/bt/map/BaseDataSource$EventHandler;

    iget-object v1, p0, Lcom/broadcom/bt/map/BaseDataSource$EventHandlerThread;->this$0:Lcom/broadcom/bt/map/BaseDataSource;

    invoke-direct {v0, v1}, Lcom/broadcom/bt/map/BaseDataSource$EventHandler;-><init>(Lcom/broadcom/bt/map/BaseDataSource;)V

    iput-object v0, p0, Lcom/broadcom/bt/map/BaseDataSource$EventHandlerThread;->mHandler:Lcom/broadcom/bt/map/BaseDataSource$EventHandler;

    invoke-static {}, Landroid/os/Looper;->loop()V

    return-void
.end method
