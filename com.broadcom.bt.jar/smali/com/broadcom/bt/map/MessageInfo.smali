.class public Lcom/broadcom/bt/map/MessageInfo;
.super Ljava/lang/Object;
.source "MessageInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/broadcom/bt/map/MessageInfo;",
            ">;"
        }
    .end annotation
.end field

.field public static final IOP_MAX_SUBJECT_LENGTH:I = 0x14

.field private static final IOP_SUBJECT_TRAILER:Ljava/lang/String; = " ..."

.field private static final IOP_SUBJECT_TRAILER_LENGTH:I

.field public static final MAP_MSG_MASK_ATTACHMENT_SIZE:I = 0x400

.field public static final MAP_MSG_MASK_DATETIME:I = 0x2

.field public static final MAP_MSG_MASK_PRIORITY:I = 0x800

.field public static final MAP_MSG_MASK_PROTECTED:I = 0x4000

.field public static final MAP_MSG_MASK_READ:I = 0x1000

.field public static final MAP_MSG_MASK_RECEPTION_STATUS:I = 0x100

.field public static final MAP_MSG_MASK_RECIPIENT_ADDRESSING:I = 0x20

.field public static final MAP_MSG_MASK_RECIPIENT_NAME:I = 0x10

.field public static final MAP_MSG_MASK_SENDER_ADDRESSING:I = 0x8

.field public static final MAP_MSG_MASK_SENDER_NAME:I = 0x4

.field public static final MAP_MSG_MASK_SENT:I = 0x2000

.field public static final MAP_MSG_MASK_SIZE:I = 0x80

.field public static final MAP_MSG_MASK_SUBJECT:I = 0x1

.field public static final MAP_MSG_MASK_TEXT:I = 0x200

.field public static final MAP_MSG_MASK_TYPE:I = 0x40

.field public static final MAP_MSG_REPLYTO_ADDRESSING:I = 0x8000

.field private static final MAX_SUBJECT_LENGTH_ALLOWED:I = 0xff

.field public static final MSG_RECEPTION_STATUS_COMPLETE:B = 0x0t

.field public static final MSG_RECEPTION_STATUS_FRACTION:B = 0x1t

.field public static final MSG_RECEPTION_STATUS_NOTIFICATION:B = 0x2t

.field public static final MSG_TYPE_EMAIL:B = 0x1t

.field public static final MSG_TYPE_MMS:B = 0x8t

.field public static final MSG_TYPE_SMS_CDMA:B = 0x4t

.field public static final MSG_TYPE_SMS_GSM:B = 0x2t

.field private static final TAG:Ljava/lang/String; = "BtMap.MessageInfo"

.field private static final UTF8_DATA_FIRST_2_BYTES_MASK:B = -0x40t

.field private static final UTF8_DATA_FIRST_2_BYTES_VALUE:B = -0x80t


# instance fields
.field public mAttachmentSize:I

.field public mDateTime:Ljava/lang/String;

.field public mIsHighPriority:Z

.field public mIsOutbound:Z

.field public mIsProtected:Z

.field public mIsRead:Z

.field public mIsSent:Z

.field public mIsText:Z

.field public mMsgHandle:Ljava/lang/String;

.field public mMsgSize:I

.field public mMsgType:B

.field public mParameterMask:J

.field public mReceptionStatus:B

.field public mRecipientAddressing:Ljava/lang/String;

.field public mRecipientName:Ljava/lang/String;

.field public mReplyToAddressing:Ljava/lang/String;

.field public mSenderAddressing:Ljava/lang/String;

.field public mSenderName:Ljava/lang/String;

.field public mSubject:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, " ..."

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    sput v0, Lcom/broadcom/bt/map/MessageInfo;->IOP_SUBJECT_TRAILER_LENGTH:I

    new-instance v0, Lcom/broadcom/bt/map/MessageInfo$1;

    invoke-direct {v0}, Lcom/broadcom/bt/map/MessageInfo$1;-><init>()V

    sput-object v0, Lcom/broadcom/bt/map/MessageInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mParameterMask:J

    iput v2, p0, Lcom/broadcom/bt/map/MessageInfo;->mMsgSize:I

    iput v2, p0, Lcom/broadcom/bt/map/MessageInfo;->mAttachmentSize:I

    iput-boolean v2, p0, Lcom/broadcom/bt/map/MessageInfo;->mIsText:Z

    iput-boolean v2, p0, Lcom/broadcom/bt/map/MessageInfo;->mIsHighPriority:Z

    iput-boolean v2, p0, Lcom/broadcom/bt/map/MessageInfo;->mIsRead:Z

    iput-boolean v2, p0, Lcom/broadcom/bt/map/MessageInfo;->mIsSent:Z

    iput-boolean v2, p0, Lcom/broadcom/bt/map/MessageInfo;->mIsProtected:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mMsgHandle:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mSubject:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mDateTime:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mSenderName:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mSenderAddressing:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mRecipientName:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mRecipientAddressing:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mReplyToAddressing:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(IIIZZZZZLjava/lang/String;BBLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # Z
    .param p5    # Z
    .param p6    # Z
    .param p7    # Z
    .param p8    # Z
    .param p9    # Ljava/lang/String;
    .param p10    # B
    .param p11    # B
    .param p12    # Ljava/lang/String;
    .param p13    # Ljava/lang/String;
    .param p14    # Ljava/lang/String;
    .param p15    # Ljava/lang/String;
    .param p16    # Ljava/lang/String;
    .param p17    # Ljava/lang/String;
    .param p18    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v1, 0x0

    iput-wide v1, p0, Lcom/broadcom/bt/map/MessageInfo;->mParameterMask:J

    const/4 v1, 0x0

    iput v1, p0, Lcom/broadcom/bt/map/MessageInfo;->mMsgSize:I

    const/4 v1, 0x0

    iput v1, p0, Lcom/broadcom/bt/map/MessageInfo;->mAttachmentSize:I

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/broadcom/bt/map/MessageInfo;->mIsText:Z

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/broadcom/bt/map/MessageInfo;->mIsHighPriority:Z

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/broadcom/bt/map/MessageInfo;->mIsRead:Z

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/broadcom/bt/map/MessageInfo;->mIsSent:Z

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/broadcom/bt/map/MessageInfo;->mIsProtected:Z

    const-string v1, ""

    iput-object v1, p0, Lcom/broadcom/bt/map/MessageInfo;->mMsgHandle:Ljava/lang/String;

    const-string v1, ""

    iput-object v1, p0, Lcom/broadcom/bt/map/MessageInfo;->mSubject:Ljava/lang/String;

    const-string v1, ""

    iput-object v1, p0, Lcom/broadcom/bt/map/MessageInfo;->mDateTime:Ljava/lang/String;

    const-string v1, ""

    iput-object v1, p0, Lcom/broadcom/bt/map/MessageInfo;->mSenderName:Ljava/lang/String;

    const-string v1, ""

    iput-object v1, p0, Lcom/broadcom/bt/map/MessageInfo;->mSenderAddressing:Ljava/lang/String;

    const-string v1, ""

    iput-object v1, p0, Lcom/broadcom/bt/map/MessageInfo;->mRecipientName:Ljava/lang/String;

    const-string v1, ""

    iput-object v1, p0, Lcom/broadcom/bt/map/MessageInfo;->mRecipientAddressing:Ljava/lang/String;

    const-string v1, ""

    iput-object v1, p0, Lcom/broadcom/bt/map/MessageInfo;->mReplyToAddressing:Ljava/lang/String;

    int-to-long v1, p1

    iput-wide v1, p0, Lcom/broadcom/bt/map/MessageInfo;->mParameterMask:J

    iput p2, p0, Lcom/broadcom/bt/map/MessageInfo;->mMsgSize:I

    iput p3, p0, Lcom/broadcom/bt/map/MessageInfo;->mAttachmentSize:I

    iput-boolean p4, p0, Lcom/broadcom/bt/map/MessageInfo;->mIsText:Z

    iput-boolean p5, p0, Lcom/broadcom/bt/map/MessageInfo;->mIsHighPriority:Z

    iput-boolean p6, p0, Lcom/broadcom/bt/map/MessageInfo;->mIsRead:Z

    iput-boolean p7, p0, Lcom/broadcom/bt/map/MessageInfo;->mIsSent:Z

    iput-boolean p8, p0, Lcom/broadcom/bt/map/MessageInfo;->mIsProtected:Z

    iput-object p9, p0, Lcom/broadcom/bt/map/MessageInfo;->mMsgHandle:Ljava/lang/String;

    iput-byte p10, p0, Lcom/broadcom/bt/map/MessageInfo;->mMsgType:B

    iput-byte p11, p0, Lcom/broadcom/bt/map/MessageInfo;->mReceptionStatus:B

    iput-object p12, p0, Lcom/broadcom/bt/map/MessageInfo;->mSubject:Ljava/lang/String;

    move-object/from16 v0, p13

    iput-object v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mDateTime:Ljava/lang/String;

    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mSenderName:Ljava/lang/String;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mSenderAddressing:Ljava/lang/String;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mRecipientName:Ljava/lang/String;

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mRecipientAddressing:Ljava/lang/String;

    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mReplyToAddressing:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 4
    .param p1    # Landroid/os/Parcel;

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mParameterMask:J

    iput v2, p0, Lcom/broadcom/bt/map/MessageInfo;->mMsgSize:I

    iput v2, p0, Lcom/broadcom/bt/map/MessageInfo;->mAttachmentSize:I

    iput-boolean v2, p0, Lcom/broadcom/bt/map/MessageInfo;->mIsText:Z

    iput-boolean v2, p0, Lcom/broadcom/bt/map/MessageInfo;->mIsHighPriority:Z

    iput-boolean v2, p0, Lcom/broadcom/bt/map/MessageInfo;->mIsRead:Z

    iput-boolean v2, p0, Lcom/broadcom/bt/map/MessageInfo;->mIsSent:Z

    iput-boolean v2, p0, Lcom/broadcom/bt/map/MessageInfo;->mIsProtected:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mMsgHandle:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mSubject:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mDateTime:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mSenderName:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mSenderAddressing:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mRecipientName:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mRecipientAddressing:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mReplyToAddressing:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    long-to-int v0, v0

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mParameterMask:J

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    long-to-int v0, v0

    iput v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mMsgSize:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    long-to-int v0, v0

    iput v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mAttachmentSize:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_0

    iput-boolean v3, p0, Lcom/broadcom/bt/map/MessageInfo;->mIsText:Z

    :goto_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_1

    iput-boolean v3, p0, Lcom/broadcom/bt/map/MessageInfo;->mIsHighPriority:Z

    :goto_1
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_2

    iput-boolean v3, p0, Lcom/broadcom/bt/map/MessageInfo;->mIsRead:Z

    :goto_2
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_3

    iput-boolean v3, p0, Lcom/broadcom/bt/map/MessageInfo;->mIsSent:Z

    :goto_3
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_4

    iput-boolean v3, p0, Lcom/broadcom/bt/map/MessageInfo;->mIsProtected:Z

    :goto_4
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mMsgHandle:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    iput-byte v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mMsgType:B

    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    iput-byte v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mReceptionStatus:B

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mSubject:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mDateTime:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mSenderName:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mSenderAddressing:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mRecipientName:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mRecipientAddressing:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mReplyToAddressing:Ljava/lang/String;

    return-void

    :cond_0
    iput-boolean v2, p0, Lcom/broadcom/bt/map/MessageInfo;->mIsText:Z

    goto :goto_0

    :cond_1
    iput-boolean v2, p0, Lcom/broadcom/bt/map/MessageInfo;->mIsHighPriority:Z

    goto :goto_1

    :cond_2
    iput-boolean v2, p0, Lcom/broadcom/bt/map/MessageInfo;->mIsRead:Z

    goto :goto_2

    :cond_3
    iput-boolean v2, p0, Lcom/broadcom/bt/map/MessageInfo;->mIsSent:Z

    goto :goto_3

    :cond_4
    iput-boolean v2, p0, Lcom/broadcom/bt/map/MessageInfo;->mIsProtected:Z

    goto :goto_4
.end method

.method private getMaxLengthOfCompletedSubject([BI)I
    .locals 4
    .param p1    # [B
    .param p2    # I

    const/4 v1, 0x0

    move v0, p2

    array-length v2, p1

    if-gt v2, p2, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    :goto_1
    if-lez v0, :cond_0

    const/16 v2, -0x80

    aget-byte v3, p1, v0

    and-int/lit8 v3, v3, -0x40

    if-ne v2, v3, :cond_2

    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    :cond_2
    move v1, v0

    goto :goto_0
.end method


# virtual methods
.method public addRecipientAddress(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mRecipientAddressing:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mRecipientAddressing:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/broadcom/bt/map/MessageInfo;->mRecipientAddressing:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mRecipientAddressing:Ljava/lang/String;

    goto :goto_0

    :cond_1
    iput-object p1, p0, Lcom/broadcom/bt/map/MessageInfo;->mRecipientAddressing:Ljava/lang/String;

    goto :goto_0
.end method

.method public addReplyToAddress(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mReplyToAddressing:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mReplyToAddressing:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/broadcom/bt/map/MessageInfo;->mReplyToAddressing:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mReplyToAddressing:Ljava/lang/String;

    goto :goto_0

    :cond_1
    iput-object p1, p0, Lcom/broadcom/bt/map/MessageInfo;->mReplyToAddressing:Ljava/lang/String;

    goto :goto_0
.end method

.method public addSenderAddress(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mSenderAddressing:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mSenderAddressing:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/broadcom/bt/map/MessageInfo;->mSenderAddressing:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mSenderAddressing:Ljava/lang/String;

    goto :goto_0

    :cond_1
    iput-object p1, p0, Lcom/broadcom/bt/map/MessageInfo;->mSenderAddressing:Ljava/lang/String;

    goto :goto_0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public dumpState(Ljava/lang/StringBuilder;Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/StringBuilder;
    .param p2    # Ljava/lang/String;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "msg_handle  = "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/broadcom/bt/map/MessageInfo;->mMsgHandle:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "\n"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "msg_type = "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-byte v1, p0, Lcom/broadcom/bt/map/MessageInfo;->mMsgType:B

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, "\n"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "msg_size = "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/broadcom/bt/map/MessageInfo;->mMsgSize:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, "\n"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "attachment_size = "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/broadcom/bt/map/MessageInfo;->mAttachmentSize:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, "\n"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "parameter_mask = "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/broadcom/bt/map/MessageInfo;->mParameterMask:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v0, "\n"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "istext = "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/broadcom/bt/map/MessageInfo;->mIsText:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v0, "\n"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "highpriority = "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/broadcom/bt/map/MessageInfo;->mIsHighPriority:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v0, "\n"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "read = "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/broadcom/bt/map/MessageInfo;->mIsRead:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v0, "\n"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "sent = "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/broadcom/bt/map/MessageInfo;->mIsSent:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v0, "\n"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "is_protected = "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/broadcom/bt/map/MessageInfo;->mIsProtected:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v0, "\n"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "reception_status = "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-byte v1, p0, Lcom/broadcom/bt/map/MessageInfo;->mReceptionStatus:B

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, "\n"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "subject = "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mSubject:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "\n"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "date_time = "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/broadcom/bt/map/MessageInfo;->mDateTime:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "\n"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "sender_name = "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mSenderName:Ljava/lang/String;

    if-nez v0, :cond_1

    const-string v0, ""

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "\n"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "sender_addressing = "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mSenderAddressing:Ljava/lang/String;

    if-nez v0, :cond_2

    const-string v0, ""

    :goto_2
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "\n"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "recepient_name = "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mRecipientName:Ljava/lang/String;

    if-nez v0, :cond_3

    const-string v0, ""

    :goto_3
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "\n"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "recipient_addressing = "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mRecipientAddressing:Ljava/lang/String;

    if-nez v0, :cond_4

    const-string v0, ""

    :goto_4
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "\n"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "replyto_addressing = "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mReplyToAddressing:Ljava/lang/String;

    if-nez v0, :cond_5

    const-string v0, ""

    :goto_5
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "\n"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    return-void

    :cond_0
    iget-object v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mSubject:Ljava/lang/String;

    goto/16 :goto_0

    :cond_1
    iget-object v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mSenderName:Ljava/lang/String;

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mSenderAddressing:Ljava/lang/String;

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mRecipientName:Ljava/lang/String;

    goto :goto_3

    :cond_4
    iget-object v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mRecipientAddressing:Ljava/lang/String;

    goto :goto_4

    :cond_5
    iget-object v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mReplyToAddressing:Ljava/lang/String;

    goto :goto_5
.end method

.method public htmlEncode()V
    .locals 1

    iget-object v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mSubject:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mSubject:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->htmlEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mSubject:Ljava/lang/String;

    :cond_0
    iget-object v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mSenderAddressing:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mSenderAddressing:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->htmlEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mSenderAddressing:Ljava/lang/String;

    :cond_1
    iget-object v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mSenderName:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mSenderName:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->htmlEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mSenderName:Ljava/lang/String;

    :cond_2
    iget-object v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mReplyToAddressing:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mReplyToAddressing:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->htmlEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mReplyToAddressing:Ljava/lang/String;

    :cond_3
    iget-object v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mRecipientAddressing:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mRecipientAddressing:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->htmlEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mRecipientAddressing:Ljava/lang/String;

    :cond_4
    iget-object v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mRecipientName:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mRecipientName:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->htmlEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mRecipientName:Ljava/lang/String;

    :cond_5
    return-void
.end method

.method public setMsgRecipientNameInfo(Lcom/broadcom/bt/map/PersonInfo;)V
    .locals 3
    .param p1    # Lcom/broadcom/bt/map/PersonInfo;

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    const-string v0, "BtMap.MessageInfo"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setMsgRecipientNameInfo - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/broadcom/bt/map/PersonInfo;->mFamilyName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/broadcom/bt/map/PersonInfo;->mGivenName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/broadcom/bt/map/PersonInfo;->getDisplayName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mRecipientName:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mRecipientName:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/broadcom/bt/map/MessageInfo;->mRecipientName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/broadcom/bt/map/PersonInfo;->getDisplayName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mRecipientName:Ljava/lang/String;

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lcom/broadcom/bt/map/PersonInfo;->getDisplayName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mRecipientName:Ljava/lang/String;

    goto :goto_0
.end method

.method public setMsgReplyToNameInfo(Lcom/broadcom/bt/map/PersonInfo;)V
    .locals 3
    .param p1    # Lcom/broadcom/bt/map/PersonInfo;

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    const-string v0, "BtMap.MessageInfo"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setMsgReplyToNameInfo - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/broadcom/bt/map/PersonInfo;->mFamilyName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/broadcom/bt/map/PersonInfo;->mGivenName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/broadcom/bt/map/PersonInfo;->getDisplayName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setMsgSenderNameInfo(Lcom/broadcom/bt/map/PersonInfo;)V
    .locals 3
    .param p1    # Lcom/broadcom/bt/map/PersonInfo;

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    const-string v0, "BtMap.MessageInfo"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setMsgSenderNameInfo - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/broadcom/bt/map/PersonInfo;->mFamilyName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/broadcom/bt/map/PersonInfo;->mGivenName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/broadcom/bt/map/PersonInfo;->getDisplayName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mSenderName:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mSenderName:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/broadcom/bt/map/MessageInfo;->mSenderName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/broadcom/bt/map/PersonInfo;->getDisplayName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mSenderName:Ljava/lang/String;

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lcom/broadcom/bt/map/PersonInfo;->getDisplayName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mSenderName:Ljava/lang/String;

    goto :goto_0
.end method

.method public setSubject(Ljava/lang/String;I)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # I

    const/4 v3, 0x0

    if-nez p1, :cond_0

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/broadcom/bt/map/MessageInfo;->mSubject:Ljava/lang/String;

    :goto_0
    return-void

    :cond_0
    if-lez p2, :cond_1

    const/16 v1, 0x14

    if-le p2, v1, :cond_2

    :cond_1
    const/16 p2, 0x14

    :cond_2
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-gt v0, p2, :cond_3

    iput-object p1, p0, Lcom/broadcom/bt/map/MessageInfo;->mSubject:Ljava/lang/String;

    goto :goto_0

    :cond_3
    sget v1, Lcom/broadcom/bt/map/MessageInfo;->IOP_SUBJECT_TRAILER_LENGTH:I

    if-le p2, v1, :cond_4

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget v2, Lcom/broadcom/bt/map/MessageInfo;->IOP_SUBJECT_TRAILER_LENGTH:I

    sub-int v2, p2, v2

    invoke-virtual {p1, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ..."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/broadcom/bt/map/MessageInfo;->mSubject:Ljava/lang/String;

    goto :goto_0

    :cond_4
    invoke-virtual {p1, v3, p2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/broadcom/bt/map/MessageInfo;->mSubject:Ljava/lang/String;

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0xc8

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {p0, v0}, Lcom/broadcom/bt/map/MessageInfo;->toString(Ljava/lang/StringBuilder;)V

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public toString(Ljava/lang/StringBuilder;)V
    .locals 1
    .param p1    # Ljava/lang/StringBuilder;

    const-string v0, ""

    invoke-virtual {p0, p1, v0}, Lcom/broadcom/bt/map/MessageInfo;->dumpState(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 4
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    const/4 v3, 0x0

    const/4 v2, 0x1

    iget-wide v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mParameterMask:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    iget v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mMsgSize:I

    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    iget v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mAttachmentSize:I

    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    iget-boolean v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mIsText:Z

    if-ne v0, v2, :cond_0

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeByte(B)V

    :goto_0
    iget-boolean v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mIsHighPriority:Z

    if-ne v0, v2, :cond_1

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeByte(B)V

    :goto_1
    iget-boolean v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mIsRead:Z

    if-ne v0, v2, :cond_2

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeByte(B)V

    :goto_2
    iget-boolean v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mIsSent:Z

    if-ne v0, v2, :cond_3

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeByte(B)V

    :goto_3
    iget-boolean v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mIsProtected:Z

    if-ne v0, v2, :cond_4

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeByte(B)V

    :goto_4
    iget-object v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mMsgHandle:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-byte v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mMsgType:B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    iget-byte v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mReceptionStatus:B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    iget-object v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mSubject:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mDateTime:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mSenderName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mSenderAddressing:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mRecipientName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mRecipientAddressing:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/broadcom/bt/map/MessageInfo;->mReplyToAddressing:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void

    :cond_0
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeByte(B)V

    goto :goto_0

    :cond_1
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeByte(B)V

    goto :goto_1

    :cond_2
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeByte(B)V

    goto :goto_2

    :cond_3
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeByte(B)V

    goto :goto_3

    :cond_4
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeByte(B)V

    goto :goto_4
.end method
