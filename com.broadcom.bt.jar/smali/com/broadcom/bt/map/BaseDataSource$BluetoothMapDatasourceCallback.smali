.class public Lcom/broadcom/bt/map/BaseDataSource$BluetoothMapDatasourceCallback;
.super Lcom/broadcom/bt/map/IBluetoothMapDatasourceCallback$Stub;
.source "BaseDataSource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/broadcom/bt/map/BaseDataSource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "BluetoothMapDatasourceCallback"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/broadcom/bt/map/BaseDataSource;


# direct methods
.method public constructor <init>(Lcom/broadcom/bt/map/BaseDataSource;)V
    .locals 0

    iput-object p1, p0, Lcom/broadcom/bt/map/BaseDataSource$BluetoothMapDatasourceCallback;->this$0:Lcom/broadcom/bt/map/BaseDataSource;

    invoke-direct {p0}, Lcom/broadcom/bt/map/IBluetoothMapDatasourceCallback$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onClientConnectionStateChanged(Z)V
    .locals 1
    .param p1    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/broadcom/bt/map/BaseDataSource$BluetoothMapDatasourceCallback;->this$0:Lcom/broadcom/bt/map/BaseDataSource;

    # setter for: Lcom/broadcom/bt/map/BaseDataSource;->mHasClientConnected:Z
    invoke-static {v0, p1}, Lcom/broadcom/bt/map/BaseDataSource;->access$302(Lcom/broadcom/bt/map/BaseDataSource;Z)Z

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/broadcom/bt/map/BaseDataSource$BluetoothMapDatasourceCallback;->this$0:Lcom/broadcom/bt/map/BaseDataSource;

    iget-object v0, v0, Lcom/broadcom/bt/map/BaseDataSource;->mEventHandlerThread:Lcom/broadcom/bt/map/BaseDataSource$EventHandlerThread;

    iget-object v0, v0, Lcom/broadcom/bt/map/BaseDataSource$EventHandlerThread;->mHandler:Lcom/broadcom/bt/map/BaseDataSource$EventHandler;

    invoke-virtual {v0, p1}, Lcom/broadcom/bt/map/BaseDataSource$EventHandler;->setClientConnectionStateChanged(Z)V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public onClientRegistrationChanged(Z)V
    .locals 1
    .param p1    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/broadcom/bt/map/BaseDataSource$BluetoothMapDatasourceCallback;->this$0:Lcom/broadcom/bt/map/BaseDataSource;

    # setter for: Lcom/broadcom/bt/map/BaseDataSource;->mHasClientsRegistered:Z
    invoke-static {v0, p1}, Lcom/broadcom/bt/map/BaseDataSource;->access$402(Lcom/broadcom/bt/map/BaseDataSource;Z)Z

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onGetFolderListing(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;)V
    .locals 1
    .param p1    # Lcom/broadcom/bt/map/RequestId;
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/broadcom/bt/map/BaseDataSource$BluetoothMapDatasourceCallback;->this$0:Lcom/broadcom/bt/map/BaseDataSource;

    iget-object v0, v0, Lcom/broadcom/bt/map/BaseDataSource;->mEventHandlerThread:Lcom/broadcom/bt/map/BaseDataSource$EventHandlerThread;

    iget-object v0, v0, Lcom/broadcom/bt/map/BaseDataSource$EventHandlerThread;->mHandler:Lcom/broadcom/bt/map/BaseDataSource$EventHandler;

    invoke-virtual {v0, p1, p2}, Lcom/broadcom/bt/map/BaseDataSource$EventHandler;->getFolderListing(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;)V

    return-void
.end method

.method public onGetMessage(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZB)V
    .locals 7
    .param p1    # Lcom/broadcom/bt/map/RequestId;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Z
    .param p6    # B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/broadcom/bt/map/BaseDataSource$BluetoothMapDatasourceCallback;->this$0:Lcom/broadcom/bt/map/BaseDataSource;

    iget-object v0, v0, Lcom/broadcom/bt/map/BaseDataSource;->mEventHandlerThread:Lcom/broadcom/bt/map/BaseDataSource$EventHandlerThread;

    iget-object v0, v0, Lcom/broadcom/bt/map/BaseDataSource$EventHandlerThread;->mHandler:Lcom/broadcom/bt/map/BaseDataSource$EventHandler;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move v6, p6

    invoke-virtual/range {v0 .. v6}, Lcom/broadcom/bt/map/BaseDataSource$EventHandler;->getMessage(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZB)V

    return-void
.end method

.method public onGetMessageListing(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;Lcom/broadcom/bt/map/MessageListFilter;JZ)V
    .locals 7
    .param p1    # Lcom/broadcom/bt/map/RequestId;
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/broadcom/bt/map/MessageListFilter;
    .param p4    # J
    .param p6    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/broadcom/bt/map/BaseDataSource$BluetoothMapDatasourceCallback;->this$0:Lcom/broadcom/bt/map/BaseDataSource;

    iget-object v0, v0, Lcom/broadcom/bt/map/BaseDataSource;->mEventHandlerThread:Lcom/broadcom/bt/map/BaseDataSource$EventHandlerThread;

    iget-object v0, v0, Lcom/broadcom/bt/map/BaseDataSource$EventHandlerThread;->mHandler:Lcom/broadcom/bt/map/BaseDataSource$EventHandler;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-wide v4, p4

    move v6, p6

    invoke-virtual/range {v0 .. v6}, Lcom/broadcom/bt/map/BaseDataSource$EventHandler;->getMsgListing(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;Lcom/broadcom/bt/map/MessageListFilter;JZ)V

    return-void
.end method

.method public onPushMessage(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZI)V
    .locals 8
    .param p1    # Lcom/broadcom/bt/map/RequestId;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Z
    .param p6    # Z
    .param p7    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/broadcom/bt/map/BaseDataSource$BluetoothMapDatasourceCallback;->this$0:Lcom/broadcom/bt/map/BaseDataSource;

    iget-object v0, v0, Lcom/broadcom/bt/map/BaseDataSource;->mEventHandlerThread:Lcom/broadcom/bt/map/BaseDataSource$EventHandlerThread;

    iget-object v0, v0, Lcom/broadcom/bt/map/BaseDataSource$EventHandlerThread;->mHandler:Lcom/broadcom/bt/map/BaseDataSource$EventHandler;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move v6, p6

    move v7, p7

    invoke-virtual/range {v0 .. v7}, Lcom/broadcom/bt/map/BaseDataSource$EventHandler;->pushMessage(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZI)V

    return-void
.end method

.method public onSetMessageStatus(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;IZ)V
    .locals 1
    .param p1    # Lcom/broadcom/bt/map/RequestId;
    .param p2    # Ljava/lang/String;
    .param p3    # I
    .param p4    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/broadcom/bt/map/BaseDataSource$BluetoothMapDatasourceCallback;->this$0:Lcom/broadcom/bt/map/BaseDataSource;

    iget-object v0, v0, Lcom/broadcom/bt/map/BaseDataSource;->mEventHandlerThread:Lcom/broadcom/bt/map/BaseDataSource$EventHandlerThread;

    iget-object v0, v0, Lcom/broadcom/bt/map/BaseDataSource$EventHandlerThread;->mHandler:Lcom/broadcom/bt/map/BaseDataSource$EventHandler;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/broadcom/bt/map/BaseDataSource$EventHandler;->setMessageStatus(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;IZ)V

    return-void
.end method

.method public onStartCompleted(Z)V
    .locals 1
    .param p1    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/broadcom/bt/map/BaseDataSource$BluetoothMapDatasourceCallback;->this$0:Lcom/broadcom/bt/map/BaseDataSource;

    iget-object v0, v0, Lcom/broadcom/bt/map/BaseDataSource;->mEventHandlerThread:Lcom/broadcom/bt/map/BaseDataSource$EventHandlerThread;

    iget-object v0, v0, Lcom/broadcom/bt/map/BaseDataSource$EventHandlerThread;->mHandler:Lcom/broadcom/bt/map/BaseDataSource$EventHandler;

    invoke-virtual {v0, p1}, Lcom/broadcom/bt/map/BaseDataSource$EventHandler;->onStartCompleted(Z)V

    return-void
.end method

.method public onStopCompleted(Z)V
    .locals 1
    .param p1    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/broadcom/bt/map/BaseDataSource$BluetoothMapDatasourceCallback;->this$0:Lcom/broadcom/bt/map/BaseDataSource;

    iget-object v0, v0, Lcom/broadcom/bt/map/BaseDataSource;->mEventHandlerThread:Lcom/broadcom/bt/map/BaseDataSource$EventHandlerThread;

    iget-object v0, v0, Lcom/broadcom/bt/map/BaseDataSource$EventHandlerThread;->mHandler:Lcom/broadcom/bt/map/BaseDataSource$EventHandler;

    invoke-virtual {v0, p1}, Lcom/broadcom/bt/map/BaseDataSource$EventHandler;->onStopCompleted(Z)V

    return-void
.end method

.method public onUpdateInbox()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/broadcom/bt/map/BaseDataSource$BluetoothMapDatasourceCallback;->this$0:Lcom/broadcom/bt/map/BaseDataSource;

    iget-object v0, v0, Lcom/broadcom/bt/map/BaseDataSource;->mEventHandlerThread:Lcom/broadcom/bt/map/BaseDataSource$EventHandlerThread;

    iget-object v0, v0, Lcom/broadcom/bt/map/BaseDataSource$EventHandlerThread;->mHandler:Lcom/broadcom/bt/map/BaseDataSource$EventHandler;

    invoke-virtual {v0}, Lcom/broadcom/bt/map/BaseDataSource$EventHandler;->updateInbox()V

    return-void
.end method
