.class Lcom/broadcom/bt/map/BaseDataSource$EventReceiver;
.super Landroid/content/BroadcastReceiver;
.source "BaseDataSource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/broadcom/bt/map/BaseDataSource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EventReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/broadcom/bt/map/BaseDataSource;


# direct methods
.method private constructor <init>(Lcom/broadcom/bt/map/BaseDataSource;)V
    .locals 0

    iput-object p1, p0, Lcom/broadcom/bt/map/BaseDataSource$EventReceiver;->this$0:Lcom/broadcom/bt/map/BaseDataSource;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/broadcom/bt/map/BaseDataSource;Lcom/broadcom/bt/map/BaseDataSource$1;)V
    .locals 0
    .param p1    # Lcom/broadcom/bt/map/BaseDataSource;
    .param p2    # Lcom/broadcom/bt/map/BaseDataSource$1;

    invoke-direct {p0, p1}, Lcom/broadcom/bt/map/BaseDataSource$EventReceiver;-><init>(Lcom/broadcom/bt/map/BaseDataSource;)V

    return-void
.end method

.method static synthetic access$600(Lcom/broadcom/bt/map/BaseDataSource$EventReceiver;Landroid/content/Context;Landroid/os/Handler;)V
    .locals 0
    .param p0    # Lcom/broadcom/bt/map/BaseDataSource$EventReceiver;
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/os/Handler;

    invoke-direct {p0, p1, p2}, Lcom/broadcom/bt/map/BaseDataSource$EventReceiver;->init(Landroid/content/Context;Landroid/os/Handler;)V

    return-void
.end method

.method private init(Landroid/content/Context;Landroid/os/Handler;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/os/Handler;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "broadcom.bluetooth.map.START"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "broadcom.bluetooth.map.STOP"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/broadcom/bt/map/BaseDataSource$EventReceiver;->this$0:Lcom/broadcom/bt/map/BaseDataSource;

    iget-object v1, v1, Lcom/broadcom/bt/map/BaseDataSource;->mContext:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-virtual {v1, p0, v0, v2, p2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v3, "broadcom.bluetooth.map.START"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "BtMap.BaseDataSource"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/broadcom/bt/map/BaseDataSource$EventReceiver;->this$0:Lcom/broadcom/bt/map/BaseDataSource;

    invoke-virtual {v5}, Lcom/broadcom/bt/map/BaseDataSource;->getDatasourceId()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/broadcom/bt/util/StringUtil;->toNonNullString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ": received start action..."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/broadcom/bt/map/BaseDataSource$EventReceiver;->this$0:Lcom/broadcom/bt/map/BaseDataSource;

    monitor-enter v4

    :try_start_0
    iget-object v3, p0, Lcom/broadcom/bt/map/BaseDataSource$EventReceiver;->this$0:Lcom/broadcom/bt/map/BaseDataSource;

    iget-boolean v3, v3, Lcom/broadcom/bt/map/BaseDataSource;->mIsStarted:Z

    if-eqz v3, :cond_0

    const-string v3, "broadcom.bluetooth.map.STOP"

    iget-object v5, p0, Lcom/broadcom/bt/map/BaseDataSource$EventReceiver;->this$0:Lcom/broadcom/bt/map/BaseDataSource;

    iget-object v5, v5, Lcom/broadcom/bt/map/BaseDataSource;->mPendingRequest:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "BtMap.BaseDataSource"

    const-string v5, "DataSource already started...Ignoring start action..."

    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    iget-object v3, p0, Lcom/broadcom/bt/map/BaseDataSource$EventReceiver;->this$0:Lcom/broadcom/bt/map/BaseDataSource;

    iget-object v3, v3, Lcom/broadcom/bt/map/BaseDataSource;->mEventHandlerThread:Lcom/broadcom/bt/map/BaseDataSource$EventHandlerThread;

    iget-object v3, v3, Lcom/broadcom/bt/map/BaseDataSource$EventHandlerThread;->mHandler:Lcom/broadcom/bt/map/BaseDataSource$EventHandler;

    invoke-virtual {v3}, Lcom/broadcom/bt/map/BaseDataSource$EventHandler;->cancelDeferredStop()V

    monitor-exit v4

    :goto_1
    return-void

    :cond_0
    const-string v3, "BtMap.BaseDataSource"

    const-string v5, "Deferring start request..."

    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/broadcom/bt/map/BaseDataSource$EventReceiver;->this$0:Lcom/broadcom/bt/map/BaseDataSource;

    iget-object v3, v3, Lcom/broadcom/bt/map/BaseDataSource;->mEventHandlerThread:Lcom/broadcom/bt/map/BaseDataSource$EventHandlerThread;

    iget-object v3, v3, Lcom/broadcom/bt/map/BaseDataSource$EventHandlerThread;->mHandler:Lcom/broadcom/bt/map/BaseDataSource$EventHandler;

    invoke-virtual {v3}, Lcom/broadcom/bt/map/BaseDataSource$EventHandler;->deferStart()V

    goto :goto_0

    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    :cond_1
    const-string v3, "broadcom.bluetooth.map.STOP"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    const-string v3, "BtMap.BaseDataSource"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/broadcom/bt/map/BaseDataSource$EventReceiver;->this$0:Lcom/broadcom/bt/map/BaseDataSource;

    invoke-virtual {v5}, Lcom/broadcom/bt/map/BaseDataSource;->getDatasourceId()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/broadcom/bt/util/StringUtil;->toNonNullString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ": received stop action..."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/broadcom/bt/map/BaseDataSource$EventReceiver;->this$0:Lcom/broadcom/bt/map/BaseDataSource;

    monitor-enter v4

    :try_start_1
    iget-object v3, p0, Lcom/broadcom/bt/map/BaseDataSource$EventReceiver;->this$0:Lcom/broadcom/bt/map/BaseDataSource;

    iget-boolean v3, v3, Lcom/broadcom/bt/map/BaseDataSource;->mIsStarted:Z

    if-eqz v3, :cond_4

    const-string v3, "broadcom.bluetooth.map.STOP"

    iget-object v5, p0, Lcom/broadcom/bt/map/BaseDataSource$EventReceiver;->this$0:Lcom/broadcom/bt/map/BaseDataSource;

    iget-object v5, v5, Lcom/broadcom/bt/map/BaseDataSource;->mPendingRequest:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    const-string v3, "BtMap.BaseDataSource"

    const-string v5, "Stopping DataSource..."

    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v5, p0, Lcom/broadcom/bt/map/BaseDataSource$EventReceiver;->this$0:Lcom/broadcom/bt/map/BaseDataSource;

    monitor-enter v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    iget-object v3, p0, Lcom/broadcom/bt/map/BaseDataSource$EventReceiver;->this$0:Lcom/broadcom/bt/map/BaseDataSource;

    const-string v6, "broadcom.bluetooth.map.STOP"

    iput-object v6, v3, Lcom/broadcom/bt/map/BaseDataSource;->mPendingRequest:Ljava/lang/String;

    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    :try_start_3
    iget-object v3, p0, Lcom/broadcom/bt/map/BaseDataSource$EventReceiver;->this$0:Lcom/broadcom/bt/map/BaseDataSource;

    iget-object v3, v3, Lcom/broadcom/bt/map/BaseDataSource;->mEventHandlerThread:Lcom/broadcom/bt/map/BaseDataSource$EventHandlerThread;

    iget-object v3, v3, Lcom/broadcom/bt/map/BaseDataSource$EventHandlerThread;->mHandler:Lcom/broadcom/bt/map/BaseDataSource$EventHandler;

    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Lcom/broadcom/bt/map/BaseDataSource$EventHandler;->onStopCompleted(Z)V

    :cond_2
    :goto_2
    iget-object v3, p0, Lcom/broadcom/bt/map/BaseDataSource$EventReceiver;->this$0:Lcom/broadcom/bt/map/BaseDataSource;

    iget-object v3, v3, Lcom/broadcom/bt/map/BaseDataSource;->mEventHandlerThread:Lcom/broadcom/bt/map/BaseDataSource$EventHandlerThread;

    iget-object v3, v3, Lcom/broadcom/bt/map/BaseDataSource$EventHandlerThread;->mHandler:Lcom/broadcom/bt/map/BaseDataSource$EventHandler;

    invoke-virtual {v3}, Lcom/broadcom/bt/map/BaseDataSource$EventHandler;->cancelDeferredStart()V

    monitor-exit v4

    goto :goto_1

    :catchall_1
    move-exception v3

    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v3

    :catchall_2
    move-exception v3

    :try_start_4
    monitor-exit v5
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    :try_start_5
    throw v3

    :cond_3
    const-string v3, "BtMap.BaseDataSource"

    const-string v5, "Stop already pending...Ignoring stop..."

    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :cond_4
    const-string v3, "BtMap.BaseDataSource"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "DataSource state: mIsStarted="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/broadcom/bt/map/BaseDataSource$EventReceiver;->this$0:Lcom/broadcom/bt/map/BaseDataSource;

    iget-boolean v6, v6, Lcom/broadcom/bt/map/BaseDataSource;->mIsStarted:Z

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", mPendingRequest="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/broadcom/bt/map/BaseDataSource$EventReceiver;->this$0:Lcom/broadcom/bt/map/BaseDataSource;

    iget-object v6, v6, Lcom/broadcom/bt/map/BaseDataSource;->mPendingRequest:Ljava/lang/String;

    invoke-static {v6}, Lcom/broadcom/bt/util/StringUtil;->toNonNullString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v3, "broadcom.bluetooth.map.START"

    iget-object v5, p0, Lcom/broadcom/bt/map/BaseDataSource$EventReceiver;->this$0:Lcom/broadcom/bt/map/BaseDataSource;

    iget-object v5, v5, Lcom/broadcom/bt/map/BaseDataSource;->mPendingRequest:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/broadcom/bt/map/BaseDataSource$EventReceiver;->this$0:Lcom/broadcom/bt/map/BaseDataSource;

    iget-object v3, v3, Lcom/broadcom/bt/map/BaseDataSource;->mEventHandlerThread:Lcom/broadcom/bt/map/BaseDataSource$EventHandlerThread;

    iget-object v3, v3, Lcom/broadcom/bt/map/BaseDataSource$EventHandlerThread;->mHandler:Lcom/broadcom/bt/map/BaseDataSource$EventHandler;

    invoke-virtual {v3}, Lcom/broadcom/bt/map/BaseDataSource$EventHandler;->deferStop()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_2

    :cond_5
    const-string v3, ""

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    const-string v3, ""

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_6

    const-string v3, "BtMap.BaseDataSource"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Received  "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", but remote address not found.. Ignoring disconnect request..."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :cond_6
    iget-object v3, p0, Lcom/broadcom/bt/map/BaseDataSource$EventReceiver;->this$0:Lcom/broadcom/bt/map/BaseDataSource;

    iget-object v2, v3, Lcom/broadcom/bt/map/BaseDataSource;->mConnectedDevice:Landroid/bluetooth/BluetoothDevice;

    if-eqz v2, :cond_7

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    const-string v3, "BtMap.BaseDataSource"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Disconnecting connected MAP client "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/broadcom/bt/map/BaseDataSource$EventReceiver;->this$0:Lcom/broadcom/bt/map/BaseDataSource;

    invoke-virtual {v3, v1}, Lcom/broadcom/bt/map/BaseDataSource;->disconnect(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_7
    const-string v3, "BtMap.BaseDataSource"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Remote device "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " is not connected MAP client.. Ignoring disconnect request..."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :cond_8
    const-string v3, "BtMap.BaseDataSource"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Provider received unhandled event "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1
.end method
