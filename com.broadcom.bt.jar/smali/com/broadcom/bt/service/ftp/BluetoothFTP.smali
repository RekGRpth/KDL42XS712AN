.class public final Lcom/broadcom/bt/service/ftp/BluetoothFTP;
.super Ljava/lang/Object;
.source "BluetoothFTP.java"

# interfaces
.implements Landroid/bluetooth/BluetoothProfile;


# static fields
.field public static final ACTION_CONNECTION_STATE_CHANGED:Ljava/lang/String; = "android.broadcom.ftpserver.CONNECTION_STATE_CHANGED"

.field public static final ACTION_ON_FTS_ACCESS_REQUEST:Ljava/lang/String; = "android.broadcom.ftpserver.ON_FTS_REQUEST_REQUEST"

.field public static final ACTION_ON_FTS_CLOSED:Ljava/lang/String; = "android.broadcom.ftpserver.ON_FTPS_CLOSED"

.field public static final ACTION_ON_FTS_DEL_COMPLETE:Ljava/lang/String; = "android.broadcom.ftpserver.ON_FTS_DEL_COMPLETE"

.field public static final ACTION_ON_FTS_GET_COMPLETE:Ljava/lang/String; = "android.broadcom.ftpserver.ON_FTS_GET_COMPLETE"

.field public static final ACTION_ON_FTS_OPENED:Ljava/lang/String; = "android.broadcom.ftpserver.ON_FTS_OPENED"

.field public static final ACTION_ON_FTS_PUT_COMPLETE:Ljava/lang/String; = "android.broadcom.ftpserver.ON_FTPS_PUT_COMPLETE"

.field public static final ACTION_ON_FTS_XFR_PROGRESS:Ljava/lang/String; = "android.broadcom.ftpserver.ON_FTS_XFR_PROGRESS"

.field static final ACTION_PREFIX:Ljava/lang/String; = "android.broadcom.ftpserver."

.field static final ACTION_PREFIX_LENGTH:I

.field public static final BLUETOOTH_ADMIN_PERM:Ljava/lang/String; = "android.permission.BLUETOOTH_ADMIN"

.field public static final BLUETOOTH_PERM:Ljava/lang/String; = "android.permission.BLUETOOTH"

.field private static final DBG:Z = true

.field public static final EXTRA_BYTES_TRANSFERRED:Ljava/lang/String; = "BYTES_TRANSFERRED"

.field public static final EXTRA_FILEPATH:Ljava/lang/String; = "FILEPATH"

.field public static final EXTRA_OPERATION:Ljava/lang/String; = "android.broadcom.ftpserver.extra.OPERATION"

.field public static final EXTRA_RMT_DEV_ADDR:Ljava/lang/String; = "android.broadcom.ftpserver.extra.RMT_DEV_ADDR"

.field public static final EXTRA_RMT_DEV_NAME:Ljava/lang/String; = "android.broadcom.ftpserver.extra.RMT_DEV_NAME"

.field public static final EXTRA_STATUS:Ljava/lang/String; = "STATUS"

.field public static final EXTRA_TOTAL_BYTES:Ljava/lang/String; = "TOTAL_BYTES"

.field public static final FTPS_OPER_CHG_DIR:B = 0x5t

.field public static final FTPS_OPER_COPY:B = 0x7t

.field public static final FTPS_OPER_DEL_DIR:B = 0x4t

.field public static final FTPS_OPER_DEL_FILE:B = 0x3t

.field public static final FTPS_OPER_GET:B = 0x2t

.field public static final FTPS_OPER_MK_DIR:B = 0x6t

.field public static final FTPS_OPER_MOVE:B = 0x8t

.field public static final FTPS_OPER_PUT:B = 0x1t

.field public static final FTPS_OPER_SET_PERM:B = 0x9t

.field public static final FTPS_STATUS_FAIL:I = 0x1

.field public static final FTPS_STATUS_OK:I = 0x0

.field public static final FTP_EXTRA_PREFIX:Ljava/lang/String; = "android.broadcom.ftpserver.extra."

.field public static final FTP_SERVER:I = 0x7

.field public static final SERVICE_NAME:Ljava/lang/String; = "bluetooth_ftp"

.field private static final TAG:Ljava/lang/String; = "BluetoothFTP"


# instance fields
.field private mAdapter:Landroid/bluetooth/BluetoothAdapter;

.field private final mBluetoothStateChangeCallback:Landroid/bluetooth/IBluetoothStateChangeCallback;

.field private final mCallback:Lcom/broadcom/bt/service/ftp/IBluetoothFTPCallback;

.field private mConnection:Landroid/content/ServiceConnection;

.field private mContext:Landroid/content/Context;

.field private mService:Lcom/broadcom/bt/service/ftp/IBluetoothFTP;

.field private mServiceListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;

.field private mgr:Landroid/bluetooth/IBluetoothManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "android.broadcom.ftpserver."

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    sput v0, Lcom/broadcom/bt/service/ftp/BluetoothFTP;->ACTION_PREFIX_LENGTH:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/bluetooth/BluetoothProfile$ServiceListener;)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/bluetooth/BluetoothProfile$ServiceListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v1, Lcom/broadcom/bt/service/ftp/BluetoothFTP$1;

    invoke-direct {v1, p0}, Lcom/broadcom/bt/service/ftp/BluetoothFTP$1;-><init>(Lcom/broadcom/bt/service/ftp/BluetoothFTP;)V

    iput-object v1, p0, Lcom/broadcom/bt/service/ftp/BluetoothFTP;->mConnection:Landroid/content/ServiceConnection;

    new-instance v1, Lcom/broadcom/bt/service/ftp/BluetoothFTP$2;

    invoke-direct {v1, p0}, Lcom/broadcom/bt/service/ftp/BluetoothFTP$2;-><init>(Lcom/broadcom/bt/service/ftp/BluetoothFTP;)V

    iput-object v1, p0, Lcom/broadcom/bt/service/ftp/BluetoothFTP;->mBluetoothStateChangeCallback:Landroid/bluetooth/IBluetoothStateChangeCallback;

    new-instance v1, Lcom/broadcom/bt/service/ftp/BluetoothFTP$3;

    invoke-direct {v1, p0}, Lcom/broadcom/bt/service/ftp/BluetoothFTP$3;-><init>(Lcom/broadcom/bt/service/ftp/BluetoothFTP;)V

    iput-object v1, p0, Lcom/broadcom/bt/service/ftp/BluetoothFTP;->mCallback:Lcom/broadcom/bt/service/ftp/IBluetoothFTPCallback;

    iput-object p1, p0, Lcom/broadcom/bt/service/ftp/BluetoothFTP;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/broadcom/bt/service/ftp/BluetoothFTP;->mServiceListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;

    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v1

    iput-object v1, p0, Lcom/broadcom/bt/service/ftp/BluetoothFTP;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    const-string v1, "bluetooth_manager"

    invoke-static {v1}, Landroid/os/ServiceManager;->checkService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Landroid/bluetooth/IBluetoothManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/bluetooth/IBluetoothManager;

    move-result-object v1

    iput-object v1, p0, Lcom/broadcom/bt/service/ftp/BluetoothFTP;->mgr:Landroid/bluetooth/IBluetoothManager;

    iget-object v1, p0, Lcom/broadcom/bt/service/ftp/BluetoothFTP;->mgr:Landroid/bluetooth/IBluetoothManager;

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/broadcom/bt/service/ftp/BluetoothFTP;->mgr:Landroid/bluetooth/IBluetoothManager;

    iget-object v2, p0, Lcom/broadcom/bt/service/ftp/BluetoothFTP;->mBluetoothStateChangeCallback:Landroid/bluetooth/IBluetoothStateChangeCallback;

    invoke-interface {v1, v2}, Landroid/bluetooth/IBluetoothManager;->registerStateChangeCallback(Landroid/bluetooth/IBluetoothStateChangeCallback;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/broadcom/bt/service/ftp/IBluetoothFTP;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/broadcom/bt/service/ftp/BluetoothFTP;->mConnection:Landroid/content/ServiceConnection;

    const/4 v3, 0x0

    invoke-virtual {p1, v1, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "BluetoothFTP"

    const-string v2, "Could not bind to Bluetooth Headset Service"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    return-void

    :catch_0
    move-exception v0

    const-string v1, "BluetoothFTP"

    const-string v2, ""

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/broadcom/bt/service/ftp/BluetoothFTP;)Lcom/broadcom/bt/service/ftp/IBluetoothFTP;
    .locals 1
    .param p0    # Lcom/broadcom/bt/service/ftp/BluetoothFTP;

    iget-object v0, p0, Lcom/broadcom/bt/service/ftp/BluetoothFTP;->mService:Lcom/broadcom/bt/service/ftp/IBluetoothFTP;

    return-object v0
.end method

.method static synthetic access$002(Lcom/broadcom/bt/service/ftp/BluetoothFTP;Lcom/broadcom/bt/service/ftp/IBluetoothFTP;)Lcom/broadcom/bt/service/ftp/IBluetoothFTP;
    .locals 0
    .param p0    # Lcom/broadcom/bt/service/ftp/BluetoothFTP;
    .param p1    # Lcom/broadcom/bt/service/ftp/IBluetoothFTP;

    iput-object p1, p0, Lcom/broadcom/bt/service/ftp/BluetoothFTP;->mService:Lcom/broadcom/bt/service/ftp/IBluetoothFTP;

    return-object p1
.end method

.method static synthetic access$100(Lcom/broadcom/bt/service/ftp/BluetoothFTP;)Landroid/bluetooth/BluetoothProfile$ServiceListener;
    .locals 1
    .param p0    # Lcom/broadcom/bt/service/ftp/BluetoothFTP;

    iget-object v0, p0, Lcom/broadcom/bt/service/ftp/BluetoothFTP;->mServiceListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;

    return-object v0
.end method

.method static synthetic access$200(Lcom/broadcom/bt/service/ftp/BluetoothFTP;)Landroid/content/ServiceConnection;
    .locals 1
    .param p0    # Lcom/broadcom/bt/service/ftp/BluetoothFTP;

    iget-object v0, p0, Lcom/broadcom/bt/service/ftp/BluetoothFTP;->mConnection:Landroid/content/ServiceConnection;

    return-object v0
.end method

.method static synthetic access$300(Lcom/broadcom/bt/service/ftp/BluetoothFTP;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/broadcom/bt/service/ftp/BluetoothFTP;

    iget-object v0, p0, Lcom/broadcom/bt/service/ftp/BluetoothFTP;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public static getProfileProxy(Landroid/content/Context;Landroid/bluetooth/BluetoothProfile$ServiceListener;)Z
    .locals 4
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/bluetooth/BluetoothProfile$ServiceListener;

    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v2, "BluetoothFTP"

    const-string v3, "BluetoothAdapter is null."

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x0

    :goto_0
    return v2

    :cond_0
    new-instance v1, Lcom/broadcom/bt/service/ftp/BluetoothFTP;

    invoke-direct {v1, p0, p1}, Lcom/broadcom/bt/service/ftp/BluetoothFTP;-><init>(Landroid/content/Context;Landroid/bluetooth/BluetoothProfile$ServiceListener;)V

    const/4 v2, 0x1

    goto :goto_0
.end method

.method private isDisabled()Z
    .locals 2

    iget-object v0, p0, Lcom/broadcom/bt/service/ftp/BluetoothFTP;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->getState()I

    move-result v0

    const/16 v1, 0xa

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isEnabled()Z
    .locals 2

    iget-object v0, p0, Lcom/broadcom/bt/service/ftp/BluetoothFTP;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->getState()I

    move-result v0

    const/16 v1, 0xc

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isValidDevice(Landroid/bluetooth/BluetoothDevice;)Z
    .locals 2
    .param p1    # Landroid/bluetooth/BluetoothDevice;

    const/4 v0, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/bluetooth/BluetoothAdapter;->checkBluetoothAddress(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static log(Ljava/lang/String;)V
    .locals 1
    .param p0    # Ljava/lang/String;

    const-string v0, "BluetoothFTP"

    invoke-static {v0, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method


# virtual methods
.method public declared-synchronized close()V
    .locals 5

    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/broadcom/bt/service/ftp/BluetoothFTP;->mgr:Landroid/bluetooth/IBluetoothManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v2, :cond_0

    :try_start_1
    iget-object v2, p0, Lcom/broadcom/bt/service/ftp/BluetoothFTP;->mgr:Landroid/bluetooth/IBluetoothManager;

    iget-object v3, p0, Lcom/broadcom/bt/service/ftp/BluetoothFTP;->mBluetoothStateChangeCallback:Landroid/bluetooth/IBluetoothStateChangeCallback;

    invoke-interface {v2, v3}, Landroid/bluetooth/IBluetoothManager;->registerStateChangeCallback(Landroid/bluetooth/IBluetoothStateChangeCallback;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_0
    :goto_0
    :try_start_2
    iget-object v3, p0, Lcom/broadcom/bt/service/ftp/BluetoothFTP;->mConnection:Landroid/content/ServiceConnection;

    monitor-enter v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    iget-object v2, p0, Lcom/broadcom/bt/service/ftp/BluetoothFTP;->mService:Lcom/broadcom/bt/service/ftp/IBluetoothFTP;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/broadcom/bt/service/ftp/BluetoothFTP;->mCallback:Lcom/broadcom/bt/service/ftp/IBluetoothFTPCallback;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    if-eqz v2, :cond_1

    :try_start_4
    iget-object v2, p0, Lcom/broadcom/bt/service/ftp/BluetoothFTP;->mService:Lcom/broadcom/bt/service/ftp/IBluetoothFTP;

    iget-object v4, p0, Lcom/broadcom/bt/service/ftp/BluetoothFTP;->mCallback:Lcom/broadcom/bt/service/ftp/IBluetoothFTPCallback;

    invoke-interface {v2, v4}, Lcom/broadcom/bt/service/ftp/IBluetoothFTP;->unregisterCallback(Lcom/broadcom/bt/service/ftp/IBluetoothFTPCallback;)V

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/broadcom/bt/service/ftp/BluetoothFTP;->mService:Lcom/broadcom/bt/service/ftp/IBluetoothFTP;

    iget-object v2, p0, Lcom/broadcom/bt/service/ftp/BluetoothFTP;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/broadcom/bt/service/ftp/BluetoothFTP;->mConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v2, v4}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :cond_1
    :goto_1
    :try_start_5
    monitor-exit v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    const/4 v2, 0x0

    :try_start_6
    iput-object v2, p0, Lcom/broadcom/bt/service/ftp/BluetoothFTP;->mServiceListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    monitor-exit p0

    return-void

    :catch_0
    move-exception v0

    :try_start_7
    const-string v2, "BluetoothFTP"

    const-string v3, ""

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    :catch_1
    move-exception v1

    :try_start_8
    const-string v2, "BluetoothFTP"

    const-string v4, ""

    invoke-static {v2, v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    :catchall_1
    move-exception v2

    monitor-exit v3
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    :try_start_9
    throw v2
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0
.end method

.method public ftpServerAccessRsp(BZLjava/lang/String;)V
    .locals 4
    .param p1    # B
    .param p2    # Z
    .param p3    # Ljava/lang/String;

    :try_start_0
    const-string v1, "TAG"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ftpServerAccessRsp("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/broadcom/bt/service/ftp/BluetoothFTP;->mService:Lcom/broadcom/bt/service/ftp/IBluetoothFTP;

    invoke-interface {v1, p1, p2, p3}, Lcom/broadcom/bt/service/ftp/IBluetoothFTP;->ftpServerAccessRsp(BZLjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "BluetoothFTP"

    const-string v2, "Error calling ftpServerAccessRsp"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public ftpServerAuthenRsp(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    :try_start_0
    iget-object v1, p0, Lcom/broadcom/bt/service/ftp/BluetoothFTP;->mService:Lcom/broadcom/bt/service/ftp/IBluetoothFTP;

    invoke-interface {v1, p1, p2}, Lcom/broadcom/bt/service/ftp/IBluetoothFTP;->ftpServerAuthenRsp(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "BluetoothFTP"

    const-string v2, "Error calling ftpServerAuthenRsp"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public getConnectedDevices()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/bluetooth/BluetoothDevice;",
            ">;"
        }
    .end annotation

    const-string v1, "getConnectedDevices()"

    invoke-static {v1}, Lcom/broadcom/bt/service/ftp/BluetoothFTP;->log(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/broadcom/bt/service/ftp/BluetoothFTP;->mService:Lcom/broadcom/bt/service/ftp/IBluetoothFTP;

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/broadcom/bt/service/ftp/BluetoothFTP;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/broadcom/bt/service/ftp/BluetoothFTP;->mService:Lcom/broadcom/bt/service/ftp/IBluetoothFTP;

    invoke-interface {v1}, Lcom/broadcom/bt/service/ftp/IBluetoothFTP;->getConnectedDevices()Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    const-string v1, "BluetoothFTP"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Stack:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    new-instance v3, Ljava/lang/Throwable;

    invoke-direct {v3}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v3}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/broadcom/bt/service/ftp/BluetoothFTP;->mService:Lcom/broadcom/bt/service/ftp/IBluetoothFTP;

    if-nez v1, :cond_1

    const-string v1, "BluetoothFTP"

    const-string v2, "Proxy not attached to service"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0
.end method

.method public getConnectionState(Landroid/bluetooth/BluetoothDevice;)I
    .locals 5
    .param p1    # Landroid/bluetooth/BluetoothDevice;

    const/4 v1, 0x0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getState("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/broadcom/bt/service/ftp/BluetoothFTP;->log(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/broadcom/bt/service/ftp/BluetoothFTP;->mService:Lcom/broadcom/bt/service/ftp/IBluetoothFTP;

    if-eqz v2, :cond_1

    invoke-direct {p0}, Lcom/broadcom/bt/service/ftp/BluetoothFTP;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-direct {p0, p1}, Lcom/broadcom/bt/service/ftp/BluetoothFTP;->isValidDevice(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v2

    if-eqz v2, :cond_1

    :try_start_0
    iget-object v2, p0, Lcom/broadcom/bt/service/ftp/BluetoothFTP;->mService:Lcom/broadcom/bt/service/ftp/IBluetoothFTP;

    invoke-interface {v2, p1}, Lcom/broadcom/bt/service/ftp/IBluetoothFTP;->getConnectionState(Landroid/bluetooth/BluetoothDevice;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :cond_0
    :goto_0
    return v1

    :catch_0
    move-exception v0

    const-string v2, "BluetoothFTP"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Stack:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    new-instance v4, Ljava/lang/Throwable;

    invoke-direct {v4}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v4}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/broadcom/bt/service/ftp/BluetoothFTP;->mService:Lcom/broadcom/bt/service/ftp/IBluetoothFTP;

    if-nez v2, :cond_0

    const-string v2, "BluetoothFTP"

    const-string v3, "Proxy not attached to service"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getDevicesMatchingConnectionStates([I)Ljava/util/List;
    .locals 4
    .param p1    # [I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([I)",
            "Ljava/util/List",
            "<",
            "Landroid/bluetooth/BluetoothDevice;",
            ">;"
        }
    .end annotation

    const-string v1, "getDevicesMatchingStates()"

    invoke-static {v1}, Lcom/broadcom/bt/service/ftp/BluetoothFTP;->log(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/broadcom/bt/service/ftp/BluetoothFTP;->mService:Lcom/broadcom/bt/service/ftp/IBluetoothFTP;

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/broadcom/bt/service/ftp/BluetoothFTP;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/broadcom/bt/service/ftp/BluetoothFTP;->mService:Lcom/broadcom/bt/service/ftp/IBluetoothFTP;

    invoke-interface {v1, p1}, Lcom/broadcom/bt/service/ftp/IBluetoothFTP;->getDevicesMatchingConnectionStates([I)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    const-string v1, "BluetoothFTP"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Stack:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    new-instance v3, Ljava/lang/Throwable;

    invoke-direct {v3}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v3}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/broadcom/bt/service/ftp/BluetoothFTP;->mService:Lcom/broadcom/bt/service/ftp/IBluetoothFTP;

    if-nez v1, :cond_1

    const-string v1, "BluetoothFTP"

    const-string v2, "Proxy not attached to service"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0
.end method

.method protected init(Landroid/os/IBinder;)Z
    .locals 3
    .param p1    # Landroid/os/IBinder;

    :try_start_0
    invoke-static {p1}, Lcom/broadcom/bt/service/ftp/IBluetoothFTP$Stub;->asInterface(Landroid/os/IBinder;)Lcom/broadcom/bt/service/ftp/IBluetoothFTP;

    move-result-object v1

    iput-object v1, p0, Lcom/broadcom/bt/service/ftp/BluetoothFTP;->mService:Lcom/broadcom/bt/service/ftp/IBluetoothFTP;

    iget-object v1, p0, Lcom/broadcom/bt/service/ftp/BluetoothFTP;->mCallback:Lcom/broadcom/bt/service/ftp/IBluetoothFTPCallback;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/broadcom/bt/service/ftp/BluetoothFTP;->mService:Lcom/broadcom/bt/service/ftp/IBluetoothFTP;

    iget-object v2, p0, Lcom/broadcom/bt/service/ftp/BluetoothFTP;->mCallback:Lcom/broadcom/bt/service/ftp/IBluetoothFTPCallback;

    invoke-interface {v1, v2}, Lcom/broadcom/bt/service/ftp/IBluetoothFTP;->registerCallback(Lcom/broadcom/bt/service/ftp/IBluetoothFTPCallback;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    const/4 v1, 0x1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    const-string v1, "BluetoothFTP"

    const-string v2, "Unable to initialize proxy with service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public requiresAccessProcessing()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public setAccess(IZLjava/lang/Object;)V
    .locals 1
    .param p1    # I
    .param p2    # Z
    .param p3    # Ljava/lang/Object;

    int-to-byte v0, p1

    check-cast p3, Ljava/lang/String;

    invoke-virtual {p0, v0, p2, p3}, Lcom/broadcom/bt/service/ftp/BluetoothFTP;->ftpServerAccessRsp(BZLjava/lang/String;)V

    return-void
.end method
