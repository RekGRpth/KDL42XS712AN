.class Lcom/broadcom/bt/service/ftp/BluetoothFTP$3;
.super Lcom/broadcom/bt/service/ftp/IBluetoothFTPCallback$Stub;
.source "BluetoothFTP.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/broadcom/bt/service/ftp/BluetoothFTP;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/broadcom/bt/service/ftp/BluetoothFTP;


# direct methods
.method constructor <init>(Lcom/broadcom/bt/service/ftp/BluetoothFTP;)V
    .locals 0

    iput-object p1, p0, Lcom/broadcom/bt/service/ftp/BluetoothFTP$3;->this$0:Lcom/broadcom/bt/service/ftp/BluetoothFTP;

    invoke-direct {p0}, Lcom/broadcom/bt/service/ftp/IBluetoothFTPCallback$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onFtpServerAccessRequested(Ljava/lang/String;ILjava/lang/String;BLjava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # Ljava/lang/String;
    .param p4    # B
    .param p5    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    return-void
.end method

.method public onFtpServerAuthen(Ljava/lang/String;BZ)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # B
    .param p3    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    return-void
.end method

.method public onFtpServerClosed()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/broadcom/bt/service/ftp/BluetoothFTP$3;->this$0:Lcom/broadcom/bt/service/ftp/BluetoothFTP;

    # getter for: Lcom/broadcom/bt/service/ftp/BluetoothFTP;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/broadcom/bt/service/ftp/BluetoothFTP;->access$300(Lcom/broadcom/bt/service/ftp/BluetoothFTP;)Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.broadcom.ftpserver.ON_FTPS_CLOSED"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "android.permission.BLUETOOTH"

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->sendOrderedBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    return-void
.end method

.method public onFtpServerDelCompleted(Ljava/lang/String;B)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.broadcom.ftpserver.ON_FTS_DEL_COMPLETE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "FILEPATH"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "STATUS"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;B)Landroid/content/Intent;

    iget-object v1, p0, Lcom/broadcom/bt/service/ftp/BluetoothFTP$3;->this$0:Lcom/broadcom/bt/service/ftp/BluetoothFTP;

    # getter for: Lcom/broadcom/bt/service/ftp/BluetoothFTP;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/broadcom/bt/service/ftp/BluetoothFTP;->access$300(Lcom/broadcom/bt/service/ftp/BluetoothFTP;)Landroid/content/Context;

    move-result-object v1

    const-string v2, "android.permission.BLUETOOTH"

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendOrderedBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    return-void
.end method

.method public onFtpServerEnabled()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    return-void
.end method

.method public onFtpServerFileTransferInProgress(II)V
    .locals 3
    .param p1    # I
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.broadcom.ftpserver.ON_FTS_XFR_PROGRESS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "TOTAL_BYTES"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "BYTES_TRANSFERRED"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v1, p0, Lcom/broadcom/bt/service/ftp/BluetoothFTP$3;->this$0:Lcom/broadcom/bt/service/ftp/BluetoothFTP;

    # getter for: Lcom/broadcom/bt/service/ftp/BluetoothFTP;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/broadcom/bt/service/ftp/BluetoothFTP;->access$300(Lcom/broadcom/bt/service/ftp/BluetoothFTP;)Landroid/content/Context;

    move-result-object v1

    const-string v2, "android.permission.BLUETOOTH"

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendOrderedBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    return-void
.end method

.method public onFtpServerGetCompleted(Ljava/lang/String;B)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.broadcom.ftpserver.ON_FTPS_PUT_COMPLETE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "FILEPATH"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "STATUS"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;B)Landroid/content/Intent;

    iget-object v1, p0, Lcom/broadcom/bt/service/ftp/BluetoothFTP$3;->this$0:Lcom/broadcom/bt/service/ftp/BluetoothFTP;

    # getter for: Lcom/broadcom/bt/service/ftp/BluetoothFTP;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/broadcom/bt/service/ftp/BluetoothFTP;->access$300(Lcom/broadcom/bt/service/ftp/BluetoothFTP;)Landroid/content/Context;

    move-result-object v1

    const-string v2, "android.permission.BLUETOOTH"

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendOrderedBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    return-void
.end method

.method public onFtpServerOpened(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.broadcom.ftpserver.ON_FTS_OPENED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "android.broadcom.ftpserver.extra.RMT_DEV_ADDR"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/broadcom/bt/service/ftp/BluetoothFTP$3;->this$0:Lcom/broadcom/bt/service/ftp/BluetoothFTP;

    # getter for: Lcom/broadcom/bt/service/ftp/BluetoothFTP;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/broadcom/bt/service/ftp/BluetoothFTP;->access$300(Lcom/broadcom/bt/service/ftp/BluetoothFTP;)Landroid/content/Context;

    move-result-object v1

    const-string v2, "android.permission.BLUETOOTH"

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendOrderedBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    return-void
.end method

.method public onFtpServerPutCompleted(Ljava/lang/String;B)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.broadcom.ftpserver.ON_FTPS_PUT_COMPLETE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "FILEPATH"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "STATUS"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;B)Landroid/content/Intent;

    iget-object v1, p0, Lcom/broadcom/bt/service/ftp/BluetoothFTP$3;->this$0:Lcom/broadcom/bt/service/ftp/BluetoothFTP;

    # getter for: Lcom/broadcom/bt/service/ftp/BluetoothFTP;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/broadcom/bt/service/ftp/BluetoothFTP;->access$300(Lcom/broadcom/bt/service/ftp/BluetoothFTP;)Landroid/content/Context;

    move-result-object v1

    const-string v2, "android.permission.BLUETOOTH"

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendOrderedBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    return-void
.end method
