.class Lcom/broadcom/bt/service/sap/BluetoothSap$1;
.super Landroid/bluetooth/IBluetoothStateChangeCallback$Stub;
.source "BluetoothSap.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/broadcom/bt/service/sap/BluetoothSap;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/broadcom/bt/service/sap/BluetoothSap;


# direct methods
.method constructor <init>(Lcom/broadcom/bt/service/sap/BluetoothSap;)V
    .locals 0

    iput-object p1, p0, Lcom/broadcom/bt/service/sap/BluetoothSap$1;->this$0:Lcom/broadcom/bt/service/sap/BluetoothSap;

    invoke-direct {p0}, Landroid/bluetooth/IBluetoothStateChangeCallback$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onBluetoothStateChange(Z)V
    .locals 5
    .param p1    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    if-eqz p1, :cond_1

    const-string v1, "Bluetoothsap"

    const-string v2, "onBluetoothStateChange(on) call bindService"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/broadcom/bt/service/sap/BluetoothSap$1;->this$0:Lcom/broadcom/bt/service/sap/BluetoothSap;

    # getter for: Lcom/broadcom/bt/service/sap/BluetoothSap;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/broadcom/bt/service/sap/BluetoothSap;->access$100(Lcom/broadcom/bt/service/sap/BluetoothSap;)Landroid/content/Context;

    move-result-object v1

    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/broadcom/bt/service/sap/IBluetoothSap;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/broadcom/bt/service/sap/BluetoothSap$1;->this$0:Lcom/broadcom/bt/service/sap/BluetoothSap;

    # getter for: Lcom/broadcom/bt/service/sap/BluetoothSap;->mConnection:Landroid/content/ServiceConnection;
    invoke-static {v3}, Lcom/broadcom/bt/service/sap/BluetoothSap;->access$000(Lcom/broadcom/bt/service/sap/BluetoothSap;)Landroid/content/ServiceConnection;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "Bluetoothsap"

    const-string v2, "Could not bind to Bluetooth SAP Service"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const-string v1, "Bluetoothsap"

    const-string v2, "BluetoothSap(), bindService called"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_1
    const-string v1, "Bluetoothsap"

    const-string v2, "Unbinding service..."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/broadcom/bt/service/sap/BluetoothSap$1;->this$0:Lcom/broadcom/bt/service/sap/BluetoothSap;

    # getter for: Lcom/broadcom/bt/service/sap/BluetoothSap;->mConnection:Landroid/content/ServiceConnection;
    invoke-static {v1}, Lcom/broadcom/bt/service/sap/BluetoothSap;->access$000(Lcom/broadcom/bt/service/sap/BluetoothSap;)Landroid/content/ServiceConnection;

    move-result-object v2

    monitor-enter v2

    :try_start_0
    iget-object v1, p0, Lcom/broadcom/bt/service/sap/BluetoothSap$1;->this$0:Lcom/broadcom/bt/service/sap/BluetoothSap;

    const/4 v3, 0x0

    # setter for: Lcom/broadcom/bt/service/sap/BluetoothSap;->mSapService:Lcom/broadcom/bt/service/sap/IBluetoothSap;
    invoke-static {v1, v3}, Lcom/broadcom/bt/service/sap/BluetoothSap;->access$202(Lcom/broadcom/bt/service/sap/BluetoothSap;Lcom/broadcom/bt/service/sap/IBluetoothSap;)Lcom/broadcom/bt/service/sap/IBluetoothSap;

    iget-object v1, p0, Lcom/broadcom/bt/service/sap/BluetoothSap$1;->this$0:Lcom/broadcom/bt/service/sap/BluetoothSap;

    # getter for: Lcom/broadcom/bt/service/sap/BluetoothSap;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/broadcom/bt/service/sap/BluetoothSap;->access$100(Lcom/broadcom/bt/service/sap/BluetoothSap;)Landroid/content/Context;

    move-result-object v1

    iget-object v3, p0, Lcom/broadcom/bt/service/sap/BluetoothSap$1;->this$0:Lcom/broadcom/bt/service/sap/BluetoothSap;

    # getter for: Lcom/broadcom/bt/service/sap/BluetoothSap;->mConnection:Landroid/content/ServiceConnection;
    invoke-static {v3}, Lcom/broadcom/bt/service/sap/BluetoothSap;->access$000(Lcom/broadcom/bt/service/sap/BluetoothSap;)Landroid/content/ServiceConnection;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_1
    :try_start_1
    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    :catch_0
    move-exception v0

    :try_start_2
    const-string v1, "Bluetoothsap"

    const-string v3, ""

    invoke-static {v1, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method
