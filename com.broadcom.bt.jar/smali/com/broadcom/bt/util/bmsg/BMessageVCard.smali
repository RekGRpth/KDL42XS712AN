.class public Lcom/broadcom/bt/util/bmsg/BMessageVCard;
.super Lcom/broadcom/bt/util/bmsg/BMessageBase;
.source "BMessageVCard.java"


# static fields
.field private static final ERR_CHECK:Z = true

.field private static final TAG:Ljava/lang/String; = "BMessageVCard"


# instance fields
.field private mParent:Lcom/broadcom/bt/util/bmsg/BMessageBase;

.field private mPreviousVCard:Lcom/broadcom/bt/util/bmsg/BMessageVCard;


# direct methods
.method constructor <init>(Lcom/broadcom/bt/util/bmsg/BMessageBase;I)V
    .locals 0
    .param p1    # Lcom/broadcom/bt/util/bmsg/BMessageBase;
    .param p2    # I

    invoke-direct {p0}, Lcom/broadcom/bt/util/bmsg/BMessageBase;-><init>()V

    iput-object p1, p0, Lcom/broadcom/bt/util/bmsg/BMessageVCard;->mParent:Lcom/broadcom/bt/util/bmsg/BMessageBase;

    invoke-virtual {p0, p2}, Lcom/broadcom/bt/util/bmsg/BMessageVCard;->setNativeRef(I)Z

    return-void
.end method

.method private constructor <init>(Lcom/broadcom/bt/util/bmsg/BMessageVCard;I)V
    .locals 1
    .param p1    # Lcom/broadcom/bt/util/bmsg/BMessageVCard;
    .param p2    # I

    iget-object v0, p1, Lcom/broadcom/bt/util/bmsg/BMessageVCard;->mParent:Lcom/broadcom/bt/util/bmsg/BMessageBase;

    invoke-direct {p0, v0, p2}, Lcom/broadcom/bt/util/bmsg/BMessageVCard;-><init>(Lcom/broadcom/bt/util/bmsg/BMessageBase;I)V

    iput-object p1, p0, Lcom/broadcom/bt/util/bmsg/BMessageVCard;->mPreviousVCard:Lcom/broadcom/bt/util/bmsg/BMessageVCard;

    return-void
.end method


# virtual methods
.method public addProperty(BLjava/lang/String;Ljava/lang/String;)Lcom/broadcom/bt/util/bmsg/BMessageVCardProperty;
    .locals 5
    .param p1    # B
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/broadcom/bt/util/bmsg/BMessageVCard;->isNativeCreated()Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-object v1

    :cond_1
    if-ltz p1, :cond_2

    const/4 v2, 0x3

    if-le p1, v2, :cond_3

    :cond_2
    const-string v2, "BMessageVCard"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Invalid vCard property: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    iget v2, p0, Lcom/broadcom/bt/util/bmsg/BMessageVCard;->mNativeObjectRef:I

    invoke-static {v2, p1, p2, p3}, Lcom/broadcom/bt/util/bmsg/BMessageManager;->addBvCardProp(IBLjava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-lez v0, :cond_0

    new-instance v1, Lcom/broadcom/bt/util/bmsg/BMessageVCardProperty;

    invoke-direct {v1, p0, v0}, Lcom/broadcom/bt/util/bmsg/BMessageVCardProperty;-><init>(Lcom/broadcom/bt/util/bmsg/BMessageVCard;I)V

    goto :goto_0
.end method

.method public getNextvCard()Lcom/broadcom/bt/util/bmsg/BMessageVCard;
    .locals 2

    invoke-virtual {p0}, Lcom/broadcom/bt/util/bmsg/BMessageVCard;->isNativeCreated()Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/broadcom/bt/util/bmsg/BMessageVCard;->mNativeObjectRef:I

    invoke-static {v1}, Lcom/broadcom/bt/util/bmsg/BMessageManager;->getBvCardNext(I)I

    move-result v0

    if-lez v0, :cond_0

    new-instance v1, Lcom/broadcom/bt/util/bmsg/BMessageVCard;

    invoke-direct {v1, p0, v0}, Lcom/broadcom/bt/util/bmsg/BMessageVCard;-><init>(Lcom/broadcom/bt/util/bmsg/BMessageVCard;I)V

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getProperty(B)Lcom/broadcom/bt/util/bmsg/BMessageVCardProperty;
    .locals 5
    .param p1    # B

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/broadcom/bt/util/bmsg/BMessageVCard;->isNativeCreated()Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-object v1

    :cond_1
    if-ltz p1, :cond_2

    const/4 v2, 0x3

    if-le p1, v2, :cond_3

    :cond_2
    const-string v2, "BMessageVCard"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Invalid vCard property: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    iget v2, p0, Lcom/broadcom/bt/util/bmsg/BMessageVCard;->mNativeObjectRef:I

    invoke-static {v2, p1}, Lcom/broadcom/bt/util/bmsg/BMessageManager;->getBvCardProp(IB)I

    move-result v0

    if-lez v0, :cond_0

    new-instance v1, Lcom/broadcom/bt/util/bmsg/BMessageVCardProperty;

    invoke-direct {v1, p0, v0}, Lcom/broadcom/bt/util/bmsg/BMessageVCardProperty;-><init>(Lcom/broadcom/bt/util/bmsg/BMessageVCard;I)V

    goto :goto_0
.end method

.method public getVersion()B
    .locals 1

    invoke-virtual {p0}, Lcom/broadcom/bt/util/bmsg/BMessageVCard;->isNativeCreated()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/broadcom/bt/util/bmsg/BMessageVCard;->mNativeObjectRef:I

    invoke-static {v0}, Lcom/broadcom/bt/util/bmsg/BMessageManager;->getBvCardVer(I)B

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public setVersion(B)V
    .locals 1
    .param p1    # B

    invoke-virtual {p0}, Lcom/broadcom/bt/util/bmsg/BMessageVCard;->isNativeCreated()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/broadcom/bt/util/bmsg/BMessageVCard;->mNativeObjectRef:I

    invoke-static {v0, p1}, Lcom/broadcom/bt/util/bmsg/BMessageManager;->setBvCardVer(IB)V

    :cond_0
    return-void
.end method
