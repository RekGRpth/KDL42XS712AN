.class public abstract Lcom/broadcom/bt/util/mime4j/field/address/Address;
.super Ljava/lang/Object;
.source "Address.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method final addMailboxesTo(Ljava/util/ArrayList;)V
    .locals 0
    .param p1    # Ljava/util/ArrayList;

    invoke-virtual {p0, p1}, Lcom/broadcom/bt/util/mime4j/field/address/Address;->doAddMailboxesTo(Ljava/util/ArrayList;)V

    return-void
.end method

.method protected abstract doAddMailboxesTo(Ljava/util/ArrayList;)V
.end method
