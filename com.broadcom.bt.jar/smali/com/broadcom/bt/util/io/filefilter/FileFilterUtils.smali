.class public Lcom/broadcom/bt/util/io/filefilter/FileFilterUtils;
.super Ljava/lang/Object;
.source "FileFilterUtils.java"


# static fields
.field private static cvsFilter:Lcom/broadcom/bt/util/io/filefilter/IOFileFilter;

.field private static svnFilter:Lcom/broadcom/bt/util/io/filefilter/IOFileFilter;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static ageFileFilter(J)Lcom/broadcom/bt/util/io/filefilter/IOFileFilter;
    .locals 1
    .param p0    # J

    new-instance v0, Lcom/broadcom/bt/util/io/filefilter/AgeFileFilter;

    invoke-direct {v0, p0, p1}, Lcom/broadcom/bt/util/io/filefilter/AgeFileFilter;-><init>(J)V

    return-object v0
.end method

.method public static ageFileFilter(JZ)Lcom/broadcom/bt/util/io/filefilter/IOFileFilter;
    .locals 1
    .param p0    # J
    .param p2    # Z

    new-instance v0, Lcom/broadcom/bt/util/io/filefilter/AgeFileFilter;

    invoke-direct {v0, p0, p1, p2}, Lcom/broadcom/bt/util/io/filefilter/AgeFileFilter;-><init>(JZ)V

    return-object v0
.end method

.method public static ageFileFilter(Ljava/io/File;)Lcom/broadcom/bt/util/io/filefilter/IOFileFilter;
    .locals 1
    .param p0    # Ljava/io/File;

    new-instance v0, Lcom/broadcom/bt/util/io/filefilter/AgeFileFilter;

    invoke-direct {v0, p0}, Lcom/broadcom/bt/util/io/filefilter/AgeFileFilter;-><init>(Ljava/io/File;)V

    return-object v0
.end method

.method public static ageFileFilter(Ljava/io/File;Z)Lcom/broadcom/bt/util/io/filefilter/IOFileFilter;
    .locals 1
    .param p0    # Ljava/io/File;
    .param p1    # Z

    new-instance v0, Lcom/broadcom/bt/util/io/filefilter/AgeFileFilter;

    invoke-direct {v0, p0, p1}, Lcom/broadcom/bt/util/io/filefilter/AgeFileFilter;-><init>(Ljava/io/File;Z)V

    return-object v0
.end method

.method public static ageFileFilter(Ljava/util/Date;)Lcom/broadcom/bt/util/io/filefilter/IOFileFilter;
    .locals 1
    .param p0    # Ljava/util/Date;

    new-instance v0, Lcom/broadcom/bt/util/io/filefilter/AgeFileFilter;

    invoke-direct {v0, p0}, Lcom/broadcom/bt/util/io/filefilter/AgeFileFilter;-><init>(Ljava/util/Date;)V

    return-object v0
.end method

.method public static ageFileFilter(Ljava/util/Date;Z)Lcom/broadcom/bt/util/io/filefilter/IOFileFilter;
    .locals 1
    .param p0    # Ljava/util/Date;
    .param p1    # Z

    new-instance v0, Lcom/broadcom/bt/util/io/filefilter/AgeFileFilter;

    invoke-direct {v0, p0, p1}, Lcom/broadcom/bt/util/io/filefilter/AgeFileFilter;-><init>(Ljava/util/Date;Z)V

    return-object v0
.end method

.method public static andFileFilter(Lcom/broadcom/bt/util/io/filefilter/IOFileFilter;Lcom/broadcom/bt/util/io/filefilter/IOFileFilter;)Lcom/broadcom/bt/util/io/filefilter/IOFileFilter;
    .locals 1
    .param p0    # Lcom/broadcom/bt/util/io/filefilter/IOFileFilter;
    .param p1    # Lcom/broadcom/bt/util/io/filefilter/IOFileFilter;

    new-instance v0, Lcom/broadcom/bt/util/io/filefilter/AndFileFilter;

    invoke-direct {v0, p0, p1}, Lcom/broadcom/bt/util/io/filefilter/AndFileFilter;-><init>(Lcom/broadcom/bt/util/io/filefilter/IOFileFilter;Lcom/broadcom/bt/util/io/filefilter/IOFileFilter;)V

    return-object v0
.end method

.method public static asFileFilter(Ljava/io/FileFilter;)Lcom/broadcom/bt/util/io/filefilter/IOFileFilter;
    .locals 1
    .param p0    # Ljava/io/FileFilter;

    new-instance v0, Lcom/broadcom/bt/util/io/filefilter/DelegateFileFilter;

    invoke-direct {v0, p0}, Lcom/broadcom/bt/util/io/filefilter/DelegateFileFilter;-><init>(Ljava/io/FileFilter;)V

    return-object v0
.end method

.method public static asFileFilter(Ljava/io/FilenameFilter;)Lcom/broadcom/bt/util/io/filefilter/IOFileFilter;
    .locals 1
    .param p0    # Ljava/io/FilenameFilter;

    new-instance v0, Lcom/broadcom/bt/util/io/filefilter/DelegateFileFilter;

    invoke-direct {v0, p0}, Lcom/broadcom/bt/util/io/filefilter/DelegateFileFilter;-><init>(Ljava/io/FilenameFilter;)V

    return-object v0
.end method

.method public static directoryFileFilter()Lcom/broadcom/bt/util/io/filefilter/IOFileFilter;
    .locals 1

    sget-object v0, Lcom/broadcom/bt/util/io/filefilter/DirectoryFileFilter;->DIRECTORY:Lcom/broadcom/bt/util/io/filefilter/IOFileFilter;

    return-object v0
.end method

.method public static falseFileFilter()Lcom/broadcom/bt/util/io/filefilter/IOFileFilter;
    .locals 1

    sget-object v0, Lcom/broadcom/bt/util/io/filefilter/FalseFileFilter;->FALSE:Lcom/broadcom/bt/util/io/filefilter/IOFileFilter;

    return-object v0
.end method

.method public static fileFileFilter()Lcom/broadcom/bt/util/io/filefilter/IOFileFilter;
    .locals 1

    sget-object v0, Lcom/broadcom/bt/util/io/filefilter/FileFileFilter;->FILE:Lcom/broadcom/bt/util/io/filefilter/IOFileFilter;

    return-object v0
.end method

.method public static makeCVSAware(Lcom/broadcom/bt/util/io/filefilter/IOFileFilter;)Lcom/broadcom/bt/util/io/filefilter/IOFileFilter;
    .locals 2
    .param p0    # Lcom/broadcom/bt/util/io/filefilter/IOFileFilter;

    sget-object v0, Lcom/broadcom/bt/util/io/filefilter/FileFilterUtils;->cvsFilter:Lcom/broadcom/bt/util/io/filefilter/IOFileFilter;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/broadcom/bt/util/io/filefilter/FileFilterUtils;->directoryFileFilter()Lcom/broadcom/bt/util/io/filefilter/IOFileFilter;

    move-result-object v0

    const-string v1, "CVS"

    invoke-static {v1}, Lcom/broadcom/bt/util/io/filefilter/FileFilterUtils;->nameFileFilter(Ljava/lang/String;)Lcom/broadcom/bt/util/io/filefilter/IOFileFilter;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/broadcom/bt/util/io/filefilter/FileFilterUtils;->andFileFilter(Lcom/broadcom/bt/util/io/filefilter/IOFileFilter;Lcom/broadcom/bt/util/io/filefilter/IOFileFilter;)Lcom/broadcom/bt/util/io/filefilter/IOFileFilter;

    move-result-object v0

    invoke-static {v0}, Lcom/broadcom/bt/util/io/filefilter/FileFilterUtils;->notFileFilter(Lcom/broadcom/bt/util/io/filefilter/IOFileFilter;)Lcom/broadcom/bt/util/io/filefilter/IOFileFilter;

    move-result-object v0

    sput-object v0, Lcom/broadcom/bt/util/io/filefilter/FileFilterUtils;->cvsFilter:Lcom/broadcom/bt/util/io/filefilter/IOFileFilter;

    :cond_0
    if-nez p0, :cond_1

    sget-object v0, Lcom/broadcom/bt/util/io/filefilter/FileFilterUtils;->cvsFilter:Lcom/broadcom/bt/util/io/filefilter/IOFileFilter;

    :goto_0
    return-object v0

    :cond_1
    sget-object v0, Lcom/broadcom/bt/util/io/filefilter/FileFilterUtils;->cvsFilter:Lcom/broadcom/bt/util/io/filefilter/IOFileFilter;

    invoke-static {p0, v0}, Lcom/broadcom/bt/util/io/filefilter/FileFilterUtils;->andFileFilter(Lcom/broadcom/bt/util/io/filefilter/IOFileFilter;Lcom/broadcom/bt/util/io/filefilter/IOFileFilter;)Lcom/broadcom/bt/util/io/filefilter/IOFileFilter;

    move-result-object v0

    goto :goto_0
.end method

.method public static makeDirectoryOnly(Lcom/broadcom/bt/util/io/filefilter/IOFileFilter;)Lcom/broadcom/bt/util/io/filefilter/IOFileFilter;
    .locals 2
    .param p0    # Lcom/broadcom/bt/util/io/filefilter/IOFileFilter;

    if-nez p0, :cond_0

    sget-object v0, Lcom/broadcom/bt/util/io/filefilter/DirectoryFileFilter;->DIRECTORY:Lcom/broadcom/bt/util/io/filefilter/IOFileFilter;

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/broadcom/bt/util/io/filefilter/AndFileFilter;

    sget-object v1, Lcom/broadcom/bt/util/io/filefilter/DirectoryFileFilter;->DIRECTORY:Lcom/broadcom/bt/util/io/filefilter/IOFileFilter;

    invoke-direct {v0, v1, p0}, Lcom/broadcom/bt/util/io/filefilter/AndFileFilter;-><init>(Lcom/broadcom/bt/util/io/filefilter/IOFileFilter;Lcom/broadcom/bt/util/io/filefilter/IOFileFilter;)V

    goto :goto_0
.end method

.method public static makeFileOnly(Lcom/broadcom/bt/util/io/filefilter/IOFileFilter;)Lcom/broadcom/bt/util/io/filefilter/IOFileFilter;
    .locals 2
    .param p0    # Lcom/broadcom/bt/util/io/filefilter/IOFileFilter;

    if-nez p0, :cond_0

    sget-object v0, Lcom/broadcom/bt/util/io/filefilter/FileFileFilter;->FILE:Lcom/broadcom/bt/util/io/filefilter/IOFileFilter;

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/broadcom/bt/util/io/filefilter/AndFileFilter;

    sget-object v1, Lcom/broadcom/bt/util/io/filefilter/FileFileFilter;->FILE:Lcom/broadcom/bt/util/io/filefilter/IOFileFilter;

    invoke-direct {v0, v1, p0}, Lcom/broadcom/bt/util/io/filefilter/AndFileFilter;-><init>(Lcom/broadcom/bt/util/io/filefilter/IOFileFilter;Lcom/broadcom/bt/util/io/filefilter/IOFileFilter;)V

    goto :goto_0
.end method

.method public static makeSVNAware(Lcom/broadcom/bt/util/io/filefilter/IOFileFilter;)Lcom/broadcom/bt/util/io/filefilter/IOFileFilter;
    .locals 2
    .param p0    # Lcom/broadcom/bt/util/io/filefilter/IOFileFilter;

    sget-object v0, Lcom/broadcom/bt/util/io/filefilter/FileFilterUtils;->svnFilter:Lcom/broadcom/bt/util/io/filefilter/IOFileFilter;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/broadcom/bt/util/io/filefilter/FileFilterUtils;->directoryFileFilter()Lcom/broadcom/bt/util/io/filefilter/IOFileFilter;

    move-result-object v0

    const-string v1, ".svn"

    invoke-static {v1}, Lcom/broadcom/bt/util/io/filefilter/FileFilterUtils;->nameFileFilter(Ljava/lang/String;)Lcom/broadcom/bt/util/io/filefilter/IOFileFilter;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/broadcom/bt/util/io/filefilter/FileFilterUtils;->andFileFilter(Lcom/broadcom/bt/util/io/filefilter/IOFileFilter;Lcom/broadcom/bt/util/io/filefilter/IOFileFilter;)Lcom/broadcom/bt/util/io/filefilter/IOFileFilter;

    move-result-object v0

    invoke-static {v0}, Lcom/broadcom/bt/util/io/filefilter/FileFilterUtils;->notFileFilter(Lcom/broadcom/bt/util/io/filefilter/IOFileFilter;)Lcom/broadcom/bt/util/io/filefilter/IOFileFilter;

    move-result-object v0

    sput-object v0, Lcom/broadcom/bt/util/io/filefilter/FileFilterUtils;->svnFilter:Lcom/broadcom/bt/util/io/filefilter/IOFileFilter;

    :cond_0
    if-nez p0, :cond_1

    sget-object v0, Lcom/broadcom/bt/util/io/filefilter/FileFilterUtils;->svnFilter:Lcom/broadcom/bt/util/io/filefilter/IOFileFilter;

    :goto_0
    return-object v0

    :cond_1
    sget-object v0, Lcom/broadcom/bt/util/io/filefilter/FileFilterUtils;->svnFilter:Lcom/broadcom/bt/util/io/filefilter/IOFileFilter;

    invoke-static {p0, v0}, Lcom/broadcom/bt/util/io/filefilter/FileFilterUtils;->andFileFilter(Lcom/broadcom/bt/util/io/filefilter/IOFileFilter;Lcom/broadcom/bt/util/io/filefilter/IOFileFilter;)Lcom/broadcom/bt/util/io/filefilter/IOFileFilter;

    move-result-object v0

    goto :goto_0
.end method

.method public static nameFileFilter(Ljava/lang/String;)Lcom/broadcom/bt/util/io/filefilter/IOFileFilter;
    .locals 1
    .param p0    # Ljava/lang/String;

    new-instance v0, Lcom/broadcom/bt/util/io/filefilter/NameFileFilter;

    invoke-direct {v0, p0}, Lcom/broadcom/bt/util/io/filefilter/NameFileFilter;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static notFileFilter(Lcom/broadcom/bt/util/io/filefilter/IOFileFilter;)Lcom/broadcom/bt/util/io/filefilter/IOFileFilter;
    .locals 1
    .param p0    # Lcom/broadcom/bt/util/io/filefilter/IOFileFilter;

    new-instance v0, Lcom/broadcom/bt/util/io/filefilter/NotFileFilter;

    invoke-direct {v0, p0}, Lcom/broadcom/bt/util/io/filefilter/NotFileFilter;-><init>(Lcom/broadcom/bt/util/io/filefilter/IOFileFilter;)V

    return-object v0
.end method

.method public static orFileFilter(Lcom/broadcom/bt/util/io/filefilter/IOFileFilter;Lcom/broadcom/bt/util/io/filefilter/IOFileFilter;)Lcom/broadcom/bt/util/io/filefilter/IOFileFilter;
    .locals 1
    .param p0    # Lcom/broadcom/bt/util/io/filefilter/IOFileFilter;
    .param p1    # Lcom/broadcom/bt/util/io/filefilter/IOFileFilter;

    new-instance v0, Lcom/broadcom/bt/util/io/filefilter/OrFileFilter;

    invoke-direct {v0, p0, p1}, Lcom/broadcom/bt/util/io/filefilter/OrFileFilter;-><init>(Lcom/broadcom/bt/util/io/filefilter/IOFileFilter;Lcom/broadcom/bt/util/io/filefilter/IOFileFilter;)V

    return-object v0
.end method

.method public static prefixFileFilter(Ljava/lang/String;)Lcom/broadcom/bt/util/io/filefilter/IOFileFilter;
    .locals 1
    .param p0    # Ljava/lang/String;

    new-instance v0, Lcom/broadcom/bt/util/io/filefilter/PrefixFileFilter;

    invoke-direct {v0, p0}, Lcom/broadcom/bt/util/io/filefilter/PrefixFileFilter;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static sizeFileFilter(J)Lcom/broadcom/bt/util/io/filefilter/IOFileFilter;
    .locals 1
    .param p0    # J

    new-instance v0, Lcom/broadcom/bt/util/io/filefilter/SizeFileFilter;

    invoke-direct {v0, p0, p1}, Lcom/broadcom/bt/util/io/filefilter/SizeFileFilter;-><init>(J)V

    return-object v0
.end method

.method public static sizeFileFilter(JZ)Lcom/broadcom/bt/util/io/filefilter/IOFileFilter;
    .locals 1
    .param p0    # J
    .param p2    # Z

    new-instance v0, Lcom/broadcom/bt/util/io/filefilter/SizeFileFilter;

    invoke-direct {v0, p0, p1, p2}, Lcom/broadcom/bt/util/io/filefilter/SizeFileFilter;-><init>(JZ)V

    return-object v0
.end method

.method public static sizeRangeFileFilter(JJ)Lcom/broadcom/bt/util/io/filefilter/IOFileFilter;
    .locals 5
    .param p0    # J
    .param p2    # J

    new-instance v1, Lcom/broadcom/bt/util/io/filefilter/SizeFileFilter;

    const/4 v2, 0x1

    invoke-direct {v1, p0, p1, v2}, Lcom/broadcom/bt/util/io/filefilter/SizeFileFilter;-><init>(JZ)V

    new-instance v0, Lcom/broadcom/bt/util/io/filefilter/SizeFileFilter;

    const-wide/16 v2, 0x1

    add-long/2addr v2, p2

    const/4 v4, 0x0

    invoke-direct {v0, v2, v3, v4}, Lcom/broadcom/bt/util/io/filefilter/SizeFileFilter;-><init>(JZ)V

    new-instance v2, Lcom/broadcom/bt/util/io/filefilter/AndFileFilter;

    invoke-direct {v2, v1, v0}, Lcom/broadcom/bt/util/io/filefilter/AndFileFilter;-><init>(Lcom/broadcom/bt/util/io/filefilter/IOFileFilter;Lcom/broadcom/bt/util/io/filefilter/IOFileFilter;)V

    return-object v2
.end method

.method public static suffixFileFilter(Ljava/lang/String;)Lcom/broadcom/bt/util/io/filefilter/IOFileFilter;
    .locals 1
    .param p0    # Ljava/lang/String;

    new-instance v0, Lcom/broadcom/bt/util/io/filefilter/SuffixFileFilter;

    invoke-direct {v0, p0}, Lcom/broadcom/bt/util/io/filefilter/SuffixFileFilter;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static trueFileFilter()Lcom/broadcom/bt/util/io/filefilter/IOFileFilter;
    .locals 1

    sget-object v0, Lcom/broadcom/bt/util/io/filefilter/TrueFileFilter;->TRUE:Lcom/broadcom/bt/util/io/filefilter/IOFileFilter;

    return-object v0
.end method
