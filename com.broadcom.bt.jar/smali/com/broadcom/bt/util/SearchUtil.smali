.class public Lcom/broadcom/bt/util/SearchUtil;
.super Ljava/lang/Object;
.source "SearchUtil.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static binarySearch([II)I
    .locals 6
    .param p0    # [I
    .param p1    # I

    array-length v5, p0

    add-int/lit8 v1, v5, -0x1

    const/4 v0, 0x0

    move v4, v1

    :cond_0
    add-int v5, v0, v4

    div-int/lit8 v2, v5, 0x2

    aget v3, p0, v2

    if-ne v3, p1, :cond_1

    :goto_0
    return v2

    :cond_1
    if-ge v3, p1, :cond_3

    add-int/lit8 v0, v2, 0x1

    :cond_2
    :goto_1
    if-le v0, v4, :cond_0

    const/4 v2, -0x1

    goto :goto_0

    :cond_3
    if-le v3, p1, :cond_2

    add-int/lit8 v4, v2, -0x1

    goto :goto_1
.end method

.method public static indexOf([II)I
    .locals 2
    .param p0    # [I
    .param p1    # I

    const/4 v0, 0x0

    :goto_0
    array-length v1, p0

    if-ge v0, v1, :cond_1

    aget v1, p0, v0

    if-ne p1, v1, :cond_0

    :goto_1
    return v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public static search([Ljava/lang/String;Ljava/lang/String;)I
    .locals 2
    .param p0    # [Ljava/lang/String;
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x0

    :goto_0
    array-length v1, p0

    if-ge v0, v1, :cond_1

    aget-object v1, p0, v0

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_1
    return v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method
