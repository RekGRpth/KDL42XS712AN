.class public final Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync;
.super Ljava/lang/Object;
.source "Bluetooth3DSync.java"

# interfaces
.implements Landroid/bluetooth/BluetoothProfile;


# static fields
.field public static final ACTION_ON_ASSOC_NOTIFICATION:Ljava/lang/String; = "android.broadcom.bt3dsync.ON_3DS_ASSOC_NOTIFICATION"

.field public static final ACTION_ON_BATTERY_LEVEL_CHANGED:Ljava/lang/String; = "android.broadcom.bt3dsync.ON_3DS_BATTERY_LEVEL_CHANGED"

.field public static final ACTION_ON_FRAME_PERIOD_CHANGED:Ljava/lang/String; = "android.broadcom.bt3dsync.ON_3DS_FRAME_PERIOD_CHANGED"

.field public static final ACTION_ON_MASTER_SYNC_STATUS_CHANGED:Ljava/lang/String; = "android.broadcom.bt3dsync.ON_3DS_MASTER_SYNC_STATUS_CHANGED"

.field static final ACTION_PREFIX:Ljava/lang/String; = "android.broadcom.bt3dsync."

.field public static final BT3D_EXTRA_PREFIX:Ljava/lang/String; = "android.broadcom.bt3dsync.extra."

.field public static final BT3D_MODE_IDLE:I = 0x0

.field public static final BT3D_MODE_MASTER:I = 0x1

.field public static final BT3D_MODE_SHOWROOM:I = 0x3

.field public static final BT3D_MODE_SLAVE:I = 0x2

.field public static final BT_3D_SYNC:I = 0x8

.field private static final DBG:Z = true

.field public static final EXTRA_BATTERY_LEVEL:Ljava/lang/String; = "android.broadcom.bt3dsync.extra.BATTERY_LEVEL"

.field public static final EXTRA_FRAME_PERIOD:Ljava/lang/String; = "android.broadcom.bt3dsync.extra.FRAME_PERIOD"

.field public static final EXTRA_SYNC_STATUS:Ljava/lang/String; = "android.broadcom.bt3dsync.extra.SYNC_STATUS"

.field public static final SERVICE_NAME:Ljava/lang/String; = "bluetooth_3d_sync"

.field private static final TAG:Ljava/lang/String; = "Bluetooth3DSync"


# instance fields
.field private mAdapter:Landroid/bluetooth/BluetoothAdapter;

.field private final mBluetoothStateChangeCallback:Landroid/bluetooth/IBluetoothStateChangeCallback;

.field private mConnection:Landroid/content/ServiceConnection;

.field private mContext:Landroid/content/Context;

.field private mService:Lcom/broadcom/bt/bt3dsync/IBluetooth3DSync;

.field private mServiceListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;

.field private mgr:Landroid/bluetooth/IBluetoothManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/bluetooth/BluetoothProfile$ServiceListener;)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/bluetooth/BluetoothProfile$ServiceListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v1, Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync$1;

    invoke-direct {v1, p0}, Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync$1;-><init>(Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync;)V

    iput-object v1, p0, Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync;->mConnection:Landroid/content/ServiceConnection;

    new-instance v1, Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync$2;

    invoke-direct {v1, p0}, Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync$2;-><init>(Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync;)V

    iput-object v1, p0, Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync;->mBluetoothStateChangeCallback:Landroid/bluetooth/IBluetoothStateChangeCallback;

    iput-object p1, p0, Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync;->mServiceListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;

    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v1

    iput-object v1, p0, Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    const-string v1, "bluetooth_manager"

    invoke-static {v1}, Landroid/os/ServiceManager;->checkService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Landroid/bluetooth/IBluetoothManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/bluetooth/IBluetoothManager;

    move-result-object v1

    iput-object v1, p0, Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync;->mgr:Landroid/bluetooth/IBluetoothManager;

    iget-object v1, p0, Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync;->mgr:Landroid/bluetooth/IBluetoothManager;

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync;->mgr:Landroid/bluetooth/IBluetoothManager;

    iget-object v2, p0, Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync;->mBluetoothStateChangeCallback:Landroid/bluetooth/IBluetoothStateChangeCallback;

    invoke-interface {v1, v2}, Landroid/bluetooth/IBluetoothManager;->registerStateChangeCallback(Landroid/bluetooth/IBluetoothStateChangeCallback;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/broadcom/bt/bt3dsync/IBluetooth3DSync;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync;->mConnection:Landroid/content/ServiceConnection;

    const/4 v3, 0x0

    invoke-virtual {p1, v1, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "Bluetooth3DSync"

    const-string v2, "Could not bind to Bluetooth 3D Sync Service"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    return-void

    :catch_0
    move-exception v0

    const-string v1, "Bluetooth3DSync"

    const-string v2, ""

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync;)Lcom/broadcom/bt/bt3dsync/IBluetooth3DSync;
    .locals 1
    .param p0    # Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync;

    iget-object v0, p0, Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync;->mService:Lcom/broadcom/bt/bt3dsync/IBluetooth3DSync;

    return-object v0
.end method

.method static synthetic access$002(Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync;Lcom/broadcom/bt/bt3dsync/IBluetooth3DSync;)Lcom/broadcom/bt/bt3dsync/IBluetooth3DSync;
    .locals 0
    .param p0    # Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync;
    .param p1    # Lcom/broadcom/bt/bt3dsync/IBluetooth3DSync;

    iput-object p1, p0, Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync;->mService:Lcom/broadcom/bt/bt3dsync/IBluetooth3DSync;

    return-object p1
.end method

.method static synthetic access$100(Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync;)Landroid/bluetooth/BluetoothProfile$ServiceListener;
    .locals 1
    .param p0    # Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync;

    iget-object v0, p0, Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync;->mServiceListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;

    return-object v0
.end method

.method static synthetic access$200(Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync;)Landroid/content/ServiceConnection;
    .locals 1
    .param p0    # Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync;

    iget-object v0, p0, Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync;->mConnection:Landroid/content/ServiceConnection;

    return-object v0
.end method

.method static synthetic access$300(Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync;

    iget-object v0, p0, Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public static closeProfileProxy(Landroid/bluetooth/BluetoothProfile;)V
    .locals 1
    .param p0    # Landroid/bluetooth/BluetoothProfile;

    if-nez p0, :cond_0

    :goto_0
    return-void

    :cond_0
    move-object v0, p0

    check-cast v0, Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync;

    invoke-virtual {v0}, Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync;->close()V

    goto :goto_0
.end method

.method public static getProfileProxy(Landroid/content/Context;Landroid/bluetooth/BluetoothProfile$ServiceListener;)Z
    .locals 4
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/bluetooth/BluetoothProfile$ServiceListener;

    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v2, "Bluetooth3DSync"

    const-string v3, "BluetoothAdapter is null."

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x0

    :goto_0
    return v2

    :cond_0
    new-instance v1, Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync;

    invoke-direct {v1, p0, p1}, Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync;-><init>(Landroid/content/Context;Landroid/bluetooth/BluetoothProfile$ServiceListener;)V

    const/4 v2, 0x1

    goto :goto_0
.end method

.method private isDisabled()Z
    .locals 2

    iget-object v0, p0, Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->getState()I

    move-result v0

    const/16 v1, 0xa

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isEnabled()Z
    .locals 2

    iget-object v0, p0, Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->getState()I

    move-result v0

    const/16 v1, 0xc

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isValidDevice(Landroid/bluetooth/BluetoothDevice;)Z
    .locals 2
    .param p1    # Landroid/bluetooth/BluetoothDevice;

    const/4 v0, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/bluetooth/BluetoothAdapter;->checkBluetoothAddress(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public broadcast3DData(IIIIIB)V
    .locals 8
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # B

    iget-object v0, p0, Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync;->mService:Lcom/broadcom/bt/bt3dsync/IBluetooth3DSync;

    if-nez v0, :cond_0

    const-string v0, "Bluetooth3DSync"

    const-string v1, "Proxy not attached to service"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync;->mService:Lcom/broadcom/bt/bt3dsync/IBluetooth3DSync;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-interface/range {v0 .. v6}, Lcom/broadcom/bt/bt3dsync/IBluetooth3DSync;->broadcast3DData(IIIIIB)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v7

    const-string v0, "Bluetooth3DSync"

    const-string v1, "Error calling broadcast3DData()"

    invoke-static {v0, v1, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public declared-synchronized close()V
    .locals 5

    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync;->mgr:Landroid/bluetooth/IBluetoothManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v2, :cond_0

    :try_start_1
    iget-object v2, p0, Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync;->mgr:Landroid/bluetooth/IBluetoothManager;

    iget-object v3, p0, Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync;->mBluetoothStateChangeCallback:Landroid/bluetooth/IBluetoothStateChangeCallback;

    invoke-interface {v2, v3}, Landroid/bluetooth/IBluetoothManager;->registerStateChangeCallback(Landroid/bluetooth/IBluetoothStateChangeCallback;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_0
    :goto_0
    :try_start_2
    iget-object v3, p0, Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync;->mConnection:Landroid/content/ServiceConnection;

    monitor-enter v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    iget-object v2, p0, Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync;->mService:Lcom/broadcom/bt/bt3dsync/IBluetooth3DSync;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    if-eqz v2, :cond_1

    const/4 v2, 0x0

    :try_start_4
    iput-object v2, p0, Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync;->mService:Lcom/broadcom/bt/bt3dsync/IBluetooth3DSync;

    iget-object v2, p0, Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync;->mConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v2, v4}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :cond_1
    :goto_1
    :try_start_5
    monitor-exit v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    const/4 v2, 0x0

    :try_start_6
    iput-object v2, p0, Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync;->mServiceListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    monitor-exit p0

    return-void

    :catch_0
    move-exception v0

    :try_start_7
    const-string v2, "Bluetooth3DSync"

    const-string v3, ""

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    :catch_1
    move-exception v1

    :try_start_8
    const-string v2, "Bluetooth3DSync"

    const-string v4, ""

    invoke-static {v2, v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    :catchall_1
    move-exception v2

    monitor-exit v3
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    :try_start_9
    throw v2
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0
.end method

.method public getConnectedDevices()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/bluetooth/BluetoothDevice;",
            ">;"
        }
    .end annotation

    const-string v1, "Bluetooth3DSync"

    const-string v2, "getConnectedDevices()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync;->mService:Lcom/broadcom/bt/bt3dsync/IBluetooth3DSync;

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync;->mService:Lcom/broadcom/bt/bt3dsync/IBluetooth3DSync;

    invoke-interface {v1}, Lcom/broadcom/bt/bt3dsync/IBluetooth3DSync;->getConnectedDevices()Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    const-string v1, "Bluetooth3DSync"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Stack:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    new-instance v3, Ljava/lang/Throwable;

    invoke-direct {v3}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v3}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync;->mService:Lcom/broadcom/bt/bt3dsync/IBluetooth3DSync;

    if-nez v1, :cond_1

    const-string v1, "Bluetooth3DSync"

    const-string v2, "Proxy not attached to service"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0
.end method

.method public getConnectionState(Landroid/bluetooth/BluetoothDevice;)I
    .locals 5
    .param p1    # Landroid/bluetooth/BluetoothDevice;

    const/4 v1, 0x0

    const-string v2, "Bluetooth3DSync"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getState("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync;->mService:Lcom/broadcom/bt/bt3dsync/IBluetooth3DSync;

    if-eqz v2, :cond_1

    invoke-direct {p0}, Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-direct {p0, p1}, Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync;->isValidDevice(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v2

    if-eqz v2, :cond_1

    :try_start_0
    iget-object v2, p0, Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync;->mService:Lcom/broadcom/bt/bt3dsync/IBluetooth3DSync;

    invoke-interface {v2, p1}, Lcom/broadcom/bt/bt3dsync/IBluetooth3DSync;->getConnectionState(Landroid/bluetooth/BluetoothDevice;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :cond_0
    :goto_0
    return v1

    :catch_0
    move-exception v0

    const-string v2, "Bluetooth3DSync"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Stack:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    new-instance v4, Ljava/lang/Throwable;

    invoke-direct {v4}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v4}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync;->mService:Lcom/broadcom/bt/bt3dsync/IBluetooth3DSync;

    if-nez v2, :cond_0

    const-string v2, "Bluetooth3DSync"

    const-string v3, "Proxy not attached to service"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getDevicesMatchingConnectionStates([I)Ljava/util/List;
    .locals 4
    .param p1    # [I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([I)",
            "Ljava/util/List",
            "<",
            "Landroid/bluetooth/BluetoothDevice;",
            ">;"
        }
    .end annotation

    const-string v1, "Bluetooth3DSync"

    const-string v2, "getDevicesMatchingStates()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync;->mService:Lcom/broadcom/bt/bt3dsync/IBluetooth3DSync;

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync;->mService:Lcom/broadcom/bt/bt3dsync/IBluetooth3DSync;

    invoke-interface {v1, p1}, Lcom/broadcom/bt/bt3dsync/IBluetooth3DSync;->getDevicesMatchingConnectionStates([I)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    const-string v1, "Bluetooth3DSync"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Stack:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    new-instance v3, Ljava/lang/Throwable;

    invoke-direct {v3}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v3}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync;->mService:Lcom/broadcom/bt/bt3dsync/IBluetooth3DSync;

    if-nez v1, :cond_1

    const-string v1, "Bluetooth3DSync"

    const-string v2, "Proxy not attached to service"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0
.end method

.method protected init(Landroid/os/IBinder;)Z
    .locals 3
    .param p1    # Landroid/os/IBinder;

    :try_start_0
    invoke-static {p1}, Lcom/broadcom/bt/bt3dsync/IBluetooth3DSync$Stub;->asInterface(Landroid/os/IBinder;)Lcom/broadcom/bt/bt3dsync/IBluetooth3DSync;

    move-result-object v1

    iput-object v1, p0, Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync;->mService:Lcom/broadcom/bt/bt3dsync/IBluetooth3DSync;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    const-string v1, "Bluetooth3DSync"

    const-string v2, "Unable to initialize proxy with service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public set3DMode(ILandroid/bluetooth/BluetoothDevice;)V
    .locals 3
    .param p1    # I
    .param p2    # Landroid/bluetooth/BluetoothDevice;

    iget-object v1, p0, Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync;->mService:Lcom/broadcom/bt/bt3dsync/IBluetooth3DSync;

    if-nez v1, :cond_0

    const-string v1, "Bluetooth3DSync"

    const-string v2, "Proxy not attached to service"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync;->mService:Lcom/broadcom/bt/bt3dsync/IBluetooth3DSync;

    invoke-interface {v1, p1, p2}, Lcom/broadcom/bt/bt3dsync/IBluetooth3DSync;->set3DMode(ILandroid/bluetooth/BluetoothDevice;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "Bluetooth3DSync"

    const-string v2, "Error calling set3DMode()"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
