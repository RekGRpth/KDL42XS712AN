.class Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync$1;
.super Ljava/lang/Object;
.source "Bluetooth3DSync.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync;


# direct methods
.method constructor <init>(Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync;)V
    .locals 0

    iput-object p1, p0, Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync$1;->this$0:Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3
    .param p1    # Landroid/content/ComponentName;
    .param p2    # Landroid/os/IBinder;

    const-string v0, "Bluetooth3DSync"

    const-string v1, "Proxy object connected"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync$1;->this$0:Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync;

    invoke-static {p2}, Lcom/broadcom/bt/bt3dsync/IBluetooth3DSync$Stub;->asInterface(Landroid/os/IBinder;)Lcom/broadcom/bt/bt3dsync/IBluetooth3DSync;

    move-result-object v1

    # setter for: Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync;->mService:Lcom/broadcom/bt/bt3dsync/IBluetooth3DSync;
    invoke-static {v0, v1}, Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync;->access$002(Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync;Lcom/broadcom/bt/bt3dsync/IBluetooth3DSync;)Lcom/broadcom/bt/bt3dsync/IBluetooth3DSync;

    iget-object v0, p0, Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync$1;->this$0:Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync;

    # getter for: Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync;->mServiceListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;
    invoke-static {v0}, Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync;->access$100(Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync;)Landroid/bluetooth/BluetoothProfile$ServiceListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync$1;->this$0:Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync;

    # getter for: Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync;->mServiceListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;
    invoke-static {v0}, Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync;->access$100(Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync;)Landroid/bluetooth/BluetoothProfile$ServiceListener;

    move-result-object v0

    const/16 v1, 0x8

    iget-object v2, p0, Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync$1;->this$0:Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync;

    invoke-interface {v0, v1, v2}, Landroid/bluetooth/BluetoothProfile$ServiceListener;->onServiceConnected(ILandroid/bluetooth/BluetoothProfile;)V

    :cond_0
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1    # Landroid/content/ComponentName;

    const-string v0, "Bluetooth3DSync"

    const-string v1, "Proxy object disconnected"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync$1;->this$0:Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync;

    const/4 v1, 0x0

    # setter for: Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync;->mService:Lcom/broadcom/bt/bt3dsync/IBluetooth3DSync;
    invoke-static {v0, v1}, Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync;->access$002(Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync;Lcom/broadcom/bt/bt3dsync/IBluetooth3DSync;)Lcom/broadcom/bt/bt3dsync/IBluetooth3DSync;

    iget-object v0, p0, Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync$1;->this$0:Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync;

    # getter for: Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync;->mServiceListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;
    invoke-static {v0}, Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync;->access$100(Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync;)Landroid/bluetooth/BluetoothProfile$ServiceListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync$1;->this$0:Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync;

    # getter for: Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync;->mServiceListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;
    invoke-static {v0}, Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync;->access$100(Lcom/broadcom/bt/bt3dsync/Bluetooth3DSync;)Landroid/bluetooth/BluetoothProfile$ServiceListener;

    move-result-object v0

    const/16 v1, 0x8

    invoke-interface {v0, v1}, Landroid/bluetooth/BluetoothProfile$ServiceListener;->onServiceDisconnected(I)V

    :cond_0
    return-void
.end method
