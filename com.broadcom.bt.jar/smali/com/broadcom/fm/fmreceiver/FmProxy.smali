.class public final Lcom/broadcom/fm/fmreceiver/FmProxy;
.super Ljava/lang/Object;
.source "FmProxy.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/broadcom/fm/fmreceiver/FmProxy$EventCallbackHandler;,
        Lcom/broadcom/fm/fmreceiver/FmProxy$FmBroadcastReceiver;,
        Lcom/broadcom/fm/fmreceiver/FmProxy$FmReceiverCallback;
    }
.end annotation


# static fields
.field public static final ACTION_ON_AUDIO_MODE:Ljava/lang/String; = "com.broadcom.bt.app.fm.action.ON_AUDIO_MODE"

.field public static final ACTION_ON_AUDIO_PATH:Ljava/lang/String; = "com.broadcom.bt.app.fm.action.ON_AUDIO_PATH"

.field public static final ACTION_ON_AUDIO_QUAL:Ljava/lang/String; = "com.broadcom.bt.app.fm.action.ON_AUDIO_QUAL"

.field public static final ACTION_ON_EST_NFL:Ljava/lang/String; = "com.broadcom.bt.app.fm.action.ON_EST_NFL"

.field public static final ACTION_ON_RDS_DATA:Ljava/lang/String; = "com.broadcom.bt.app.fm.action.ON_RDS_DATA"

.field public static final ACTION_ON_RDS_MODE:Ljava/lang/String; = "com.broadcom.bt.app.fm.action.ON_RDS_MODE"

.field public static final ACTION_ON_SEEK_CMPL:Ljava/lang/String; = "com.broadcom.bt.app.fm.action.ON_SEEK_CMPL"

.field public static final ACTION_ON_STATUS:Ljava/lang/String; = "com.broadcom.bt.app.fm.action.ON_STATUS"

.field public static final ACTION_ON_VOL:Ljava/lang/String; = "ON_VOL"

.field public static final ACTION_ON_WRLD_RGN:Ljava/lang/String; = "com.broadcom.bt.app.fm.action.ON_WRLD_RGN"

.field private static final ACTION_PREFIX:Ljava/lang/String; = "com.broadcom.bt.app.fm.action."

.field private static final ACTION_PREFIX_LENGTH:I

.field public static final AF_MODE_DEFAULT:I = 0x0

.field public static final AF_MODE_OFF:I = 0x0

.field public static final AF_MODE_ON:I = 0x1

.field public static final AUDIO_MODE_AUTO:I = 0x0

.field public static final AUDIO_MODE_BLEND:I = 0x3

.field public static final AUDIO_MODE_MONO:I = 0x2

.field public static final AUDIO_MODE_STEREO:I = 0x1

.field public static final AUDIO_MODE_SWITCH:I = 0x3

.field public static final AUDIO_PATH_DIGITAL:I = 0x3

.field public static final AUDIO_PATH_NONE:I = 0x0

.field public static final AUDIO_PATH_SPEAKER:I = 0x1

.field public static final AUDIO_PATH_WIRE_HEADSET:I = 0x2

.field public static final AUDIO_QUALITY_BLEND:I = 0x4

.field public static final AUDIO_QUALITY_MONO:I = 0x2

.field public static final AUDIO_QUALITY_STEREO:I = 0x1

.field public static final BLUETOOTH_PERM:Ljava/lang/String; = "android.permission.BLUETOOTH"

.field private static final D:Z = true

.field public static final DEEMPHASIS_50U:I = 0x0

.field public static final DEEMPHASIS_75U:I = 0x40

.field public static final DEEMPHASIS_TIME_DEFAULT:I = 0x40

.field public static final DEFAULT_BROADCAST_RECEIVER_PRIORITY:I = 0xc8

.field public static final EXTRA_ALT_FREQ_MODE:Ljava/lang/String; = "ALT_FREQ_MODE"

.field public static final EXTRA_AUDIO_MODE:Ljava/lang/String; = "AUDIO_MODE"

.field public static final EXTRA_AUDIO_PATH:Ljava/lang/String; = "AUDIO_PATH"

.field public static final EXTRA_FREQ:Ljava/lang/String; = "FREQ"

.field public static final EXTRA_MUTED:Ljava/lang/String; = "MUTED"

.field public static final EXTRA_NFL:Ljava/lang/String; = "NFL"

.field public static final EXTRA_RADIO_ON:Ljava/lang/String; = "RADIO_ON"

.field public static final EXTRA_RDS_DATA_TYPE:Ljava/lang/String; = "RDS_DATA_TYPE"

.field public static final EXTRA_RDS_IDX:Ljava/lang/String; = "RDS_IDX"

.field public static final EXTRA_RDS_MODE:Ljava/lang/String; = "RDS_MODE"

.field public static final EXTRA_RDS_PRGM_SVC:Ljava/lang/String; = "RDS_PRGM_SVC"

.field public static final EXTRA_RDS_PRGM_TYPE:Ljava/lang/String; = "RDS_PRGM_TYPE"

.field public static final EXTRA_RDS_PRGM_TYPE_NAME:Ljava/lang/String; = "RDS_PRGM_TYPE_NAME"

.field public static final EXTRA_RDS_TXT:Ljava/lang/String; = "RDS_TXT"

.field public static final EXTRA_RSSI:Ljava/lang/String; = "RSSI"

.field public static final EXTRA_SNR:Ljava/lang/String; = "SNR"

.field public static final EXTRA_STATUS:Ljava/lang/String; = "STATUS"

.field public static final EXTRA_SUCCESS:Ljava/lang/String; = "RDS_SUCCESS"

.field public static final EXTRA_VOL:Ljava/lang/String; = "VOL"

.field public static final EXTRA_WRLD_RGN:Ljava/lang/String; = "WRLD_RGN"

.field public static final FM_MAX_SNR_THRESHOLD:I = 0x1f

.field public static final FM_MIN_SNR_THRESHOLD:I = 0x0

.field public static final FM_RECEIVER_PERM:Ljava/lang/String; = "android.permission.ACCESS_FM_RECEIVER"

.field public static final FM_VOLUME_MAX:I = 0xff

.field public static final FREQ_STEP_100KHZ:I = 0x0

.field public static final FREQ_STEP_50KHZ:I = 0x10

.field public static final FREQ_STEP_DEFAULT:I = 0x0

.field public static final FUNCTIONALITY_DEFAULT:I = 0x0

.field public static final FUNC_AF:I = 0x40

.field public static final FUNC_RBDS:I = 0x20

.field public static final FUNC_RDS:I = 0x10

.field public static final FUNC_REGION_DEFAULT:I = 0x0

.field public static final FUNC_REGION_EUR:I = 0x1

.field public static final FUNC_REGION_JP:I = 0x2

.field public static final FUNC_REGION_JP_II:I = 0x3

.field public static final FUNC_REGION_NA:I = 0x0

.field public static final FUNC_SOFTMUTE:I = 0x100

.field public static final LIVE_AUDIO_QUALITY_DEFAULT:Z = false

.field public static final MIN_SIGNAL_STRENGTH_DEFAULT:I = 0x69

.field public static final NFL_DEFAULT:I = 0x1

.field public static final NFL_FINE:I = 0x2

.field public static final NFL_LOW:I = 0x0

.field public static final NFL_MED:I = 0x1

.field public static final RDS_COND_NONE:I = 0x0

.field public static final RDS_COND_PTY:I = 0x1

.field public static final RDS_COND_PTY_VAL:I = 0x0

.field public static final RDS_COND_TP:I = 0x2

.field public static final RDS_FEATURE_PS:I = 0x4

.field public static final RDS_FEATURE_PTY:I = 0x8

.field public static final RDS_FEATURE_PTYN:I = 0x20

.field public static final RDS_FEATURE_RT:I = 0x40

.field public static final RDS_FEATURE_TP:I = 0x10

.field public static final RDS_MODE_DEFAULT_ON:I = 0x1

.field public static final RDS_MODE_OFF:I = 0x0

.field public static final RDS_MODE_RBDS_ON:I = 0x3

.field public static final RDS_MODE_RDS_ON:I = 0x2

.field public static final SCAN_MODE_DOWN:I = 0x0

.field public static final SCAN_MODE_FAST:I = 0x1

.field public static final SCAN_MODE_FULL:I = 0x82

.field public static final SCAN_MODE_NORMAL:I = 0x0

.field public static final SCAN_MODE_UP:I = 0x80

.field public static final SIGNAL_POLL_INTERVAL_DEFAULT:I = 0x64

.field public static final STATUS_FAIL:I = 0x1

.field public static final STATUS_ILLEGAL_COMMAND:I = 0x3

.field public static final STATUS_ILLEGAL_PARAMETERS:I = 0x4

.field public static final STATUS_OK:I = 0x0

.field public static final STATUS_SERVER_FAIL:I = 0x2

.field private static final TAG:Ljava/lang/String; = "FmProxy"


# instance fields
.field protected mBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mCallback:Lcom/broadcom/fm/fmreceiver/IFmReceiverCallback;

.field private mConnection:Landroid/content/ServiceConnection;

.field protected mContext:Landroid/content/Context;

.field protected mEventCallbackHandler:Lcom/broadcom/fm/fmreceiver/FmProxy$EventCallbackHandler;

.field private mEventHandler:Lcom/broadcom/fm/fmreceiver/IFmReceiverEventHandler;

.field protected mIsAvailable:Z

.field protected mProxyAvailCb:Lcom/broadcom/fm/fmreceiver/IFmProxyCallback;

.field protected mReceiverPriority:I

.field private mService:Lcom/broadcom/fm/fmreceiver/IFmReceiverService;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "com.broadcom.bt.app.fm.action."

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    sput v0, Lcom/broadcom/fm/fmreceiver/FmProxy;->ACTION_PREFIX_LENGTH:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/broadcom/fm/fmreceiver/IFmProxyCallback;)V
    .locals 5
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/broadcom/fm/fmreceiver/IFmProxyCallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/broadcom/fm/fmreceiver/FmProxy;->mEventHandler:Lcom/broadcom/fm/fmreceiver/IFmReceiverEventHandler;

    const/16 v1, 0xc8

    iput v1, p0, Lcom/broadcom/fm/fmreceiver/FmProxy;->mReceiverPriority:I

    new-instance v1, Lcom/broadcom/fm/fmreceiver/FmProxy$1;

    invoke-direct {v1, p0}, Lcom/broadcom/fm/fmreceiver/FmProxy$1;-><init>(Lcom/broadcom/fm/fmreceiver/FmProxy;)V

    iput-object v1, p0, Lcom/broadcom/fm/fmreceiver/FmProxy;->mConnection:Landroid/content/ServiceConnection;

    const-string v1, "FmProxy"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "FmProxy object created obj ="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput-object p1, p0, Lcom/broadcom/fm/fmreceiver/FmProxy;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/broadcom/fm/fmreceiver/FmProxy;->mProxyAvailCb:Lcom/broadcom/fm/fmreceiver/IFmProxyCallback;

    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    if-nez v0, :cond_1

    const-string v1, "FmProxy"

    const-string v2, "BluetoothAdapter is null."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/broadcom/fm/fmreceiver/FmProxy;->mContext:Landroid/content/Context;

    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/broadcom/fm/fmreceiver/IFmReceiverService;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/broadcom/fm/fmreceiver/FmProxy;->mConnection:Landroid/content/ServiceConnection;

    const/4 v4, 0x1

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "FmProxy"

    const-string v2, "Could not bind to IFmReceiverService Service"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method static synthetic access$100(Lcom/broadcom/fm/fmreceiver/FmProxy;)Lcom/broadcom/fm/fmreceiver/IFmReceiverEventHandler;
    .locals 1
    .param p0    # Lcom/broadcom/fm/fmreceiver/FmProxy;

    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmProxy;->mEventHandler:Lcom/broadcom/fm/fmreceiver/IFmReceiverEventHandler;

    return-object v0
.end method

.method static synthetic access$200()I
    .locals 1

    sget v0, Lcom/broadcom/fm/fmreceiver/FmProxy;->ACTION_PREFIX_LENGTH:I

    return v0
.end method

.method static synthetic access$302(Lcom/broadcom/fm/fmreceiver/FmProxy;Lcom/broadcom/fm/fmreceiver/IFmReceiverService;)Lcom/broadcom/fm/fmreceiver/IFmReceiverService;
    .locals 0
    .param p0    # Lcom/broadcom/fm/fmreceiver/FmProxy;
    .param p1    # Lcom/broadcom/fm/fmreceiver/IFmReceiverService;

    iput-object p1, p0, Lcom/broadcom/fm/fmreceiver/FmProxy;->mService:Lcom/broadcom/fm/fmreceiver/IFmReceiverService;

    return-object p1
.end method

.method protected static actionsEqual(Ljava/lang/String;Ljava/lang/String;I)Z
    .locals 2
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .param p2    # I

    if-ne p0, p1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    sub-int v1, v0, p2

    invoke-virtual {p0, p2, p1, p2, v1}, Ljava/lang/String;->regionMatches(ILjava/lang/String;II)Z

    move-result v1

    goto :goto_0
.end method

.method public static createFilter(Landroid/content/IntentFilter;)Landroid/content/IntentFilter;
    .locals 1
    .param p0    # Landroid/content/IntentFilter;

    if-nez p0, :cond_0

    new-instance p0, Landroid/content/IntentFilter;

    invoke-direct {p0}, Landroid/content/IntentFilter;-><init>()V

    :cond_0
    const-string v0, "com.broadcom.bt.app.fm.action.ON_AUDIO_MODE"

    invoke-virtual {p0, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v0, "com.broadcom.bt.app.fm.action.ON_AUDIO_PATH"

    invoke-virtual {p0, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v0, "com.broadcom.bt.app.fm.action.ON_AUDIO_QUAL"

    invoke-virtual {p0, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v0, "com.broadcom.bt.app.fm.action.ON_EST_NFL"

    invoke-virtual {p0, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v0, "com.broadcom.bt.app.fm.action.ON_RDS_DATA"

    invoke-virtual {p0, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v0, "com.broadcom.bt.app.fm.action.ON_RDS_MODE"

    invoke-virtual {p0, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v0, "com.broadcom.bt.app.fm.action.ON_SEEK_CMPL"

    invoke-virtual {p0, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v0, "com.broadcom.bt.app.fm.action.ON_STATUS"

    invoke-virtual {p0, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v0, "ON_VOL"

    invoke-virtual {p0, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v0, "com.broadcom.bt.app.fm.action.ON_WRLD_RGN"

    invoke-virtual {p0, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    return-object p0
.end method

.method public static getProxy(Landroid/content/Context;Lcom/broadcom/fm/fmreceiver/IFmProxyCallback;)Z
    .locals 6
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/broadcom/fm/fmreceiver/IFmProxyCallback;

    const/4 v2, 0x0

    const/4 v0, 0x0

    :try_start_0
    new-instance v1, Lcom/broadcom/fm/fmreceiver/FmProxy;

    invoke-direct {v1, p0, p1}, Lcom/broadcom/fm/fmreceiver/FmProxy;-><init>(Landroid/content/Context;Lcom/broadcom/fm/fmreceiver/IFmProxyCallback;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v4, 0x1

    move-object v0, v1

    :goto_0
    return v4

    :catch_0
    move-exception v3

    const-string v4, "FmProxy"

    const-string v5, "Unable to get FM Proxy"

    invoke-static {v4, v5, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v4, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected baseFinalize()V
    .locals 0

    invoke-virtual {p0}, Lcom/broadcom/fm/fmreceiver/FmProxy;->finish()V

    return-void
.end method

.method public declared-synchronized baseFinish()V
    .locals 3

    monitor-enter p0

    :try_start_0
    const-string v0, "FmProxy"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "finish() mContext = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/broadcom/fm/fmreceiver/FmProxy;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmProxy;->mEventCallbackHandler:Lcom/broadcom/fm/fmreceiver/FmProxy$EventCallbackHandler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmProxy;->mEventCallbackHandler:Lcom/broadcom/fm/fmreceiver/FmProxy$EventCallbackHandler;

    invoke-virtual {v0}, Lcom/broadcom/fm/fmreceiver/FmProxy$EventCallbackHandler;->finish()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmProxy;->mEventCallbackHandler:Lcom/broadcom/fm/fmreceiver/FmProxy$EventCallbackHandler;

    :cond_0
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmProxy;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmProxy;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/broadcom/fm/fmreceiver/FmProxy;->mConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmProxy;->mContext:Landroid/content/Context;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized cleanupFmService()I
    .locals 4

    monitor-enter p0

    const/4 v1, 0x2

    :try_start_0
    iget-object v2, p0, Lcom/broadcom/fm/fmreceiver/FmProxy;->mService:Lcom/broadcom/fm/fmreceiver/IFmReceiverService;

    invoke-interface {v2}, Lcom/broadcom/fm/fmreceiver/IFmReceiverService;->cleanupFmService()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    :try_start_1
    const-string v2, "FmProxy"

    const-string v3, "cleanup triggered"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return v1

    :catch_0
    move-exception v0

    :try_start_2
    const-string v2, "FmProxy"

    const-string v3, "cleanupFmService() failed"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public declared-synchronized estimateNoiseFloorLevel(I)I
    .locals 4
    .param p1    # I

    monitor-enter p0

    const/4 v1, 0x2

    :try_start_0
    iget-object v2, p0, Lcom/broadcom/fm/fmreceiver/FmProxy;->mService:Lcom/broadcom/fm/fmreceiver/IFmReceiverService;

    invoke-interface {v2, p1}, Lcom/broadcom/fm/fmreceiver/IFmReceiverService;->estimateNoiseFloorLevel(I)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    :goto_0
    monitor-exit p0

    return v1

    :catch_0
    move-exception v0

    :try_start_1
    const-string v2, "FmProxy"

    const-string v3, "estimateNoiseFloorLevel() failed"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method protected finalize()V
    .locals 0

    invoke-virtual {p0}, Lcom/broadcom/fm/fmreceiver/FmProxy;->baseFinalize()V

    return-void
.end method

.method public declared-synchronized finish()V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/broadcom/fm/fmreceiver/FmProxy;->mEventHandler:Lcom/broadcom/fm/fmreceiver/IFmReceiverEventHandler;

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/broadcom/fm/fmreceiver/FmProxy;->mEventHandler:Lcom/broadcom/fm/fmreceiver/IFmReceiverEventHandler;

    :cond_0
    iget-object v1, p0, Lcom/broadcom/fm/fmreceiver/FmProxy;->mCallback:Lcom/broadcom/fm/fmreceiver/IFmReceiverCallback;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/broadcom/fm/fmreceiver/FmProxy;->mService:Lcom/broadcom/fm/fmreceiver/IFmReceiverService;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_1

    :try_start_1
    iget-object v1, p0, Lcom/broadcom/fm/fmreceiver/FmProxy;->mService:Lcom/broadcom/fm/fmreceiver/IFmReceiverService;

    iget-object v2, p0, Lcom/broadcom/fm/fmreceiver/FmProxy;->mCallback:Lcom/broadcom/fm/fmreceiver/IFmReceiverCallback;

    invoke-interface {v1, v2}, Lcom/broadcom/fm/fmreceiver/IFmReceiverService;->unregisterCallback(Lcom/broadcom/fm/fmreceiver/IFmReceiverCallback;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    const/4 v1, 0x0

    :try_start_2
    iput-object v1, p0, Lcom/broadcom/fm/fmreceiver/FmProxy;->mCallback:Lcom/broadcom/fm/fmreceiver/IFmReceiverCallback;

    :cond_1
    invoke-virtual {p0}, Lcom/broadcom/fm/fmreceiver/FmProxy;->baseFinish()V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/broadcom/fm/fmreceiver/FmProxy;->mContext:Landroid/content/Context;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/broadcom/fm/fmreceiver/FmProxy;->mService:Lcom/broadcom/fm/fmreceiver/IFmReceiverService;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit p0

    return-void

    :catch_0
    move-exception v0

    :try_start_3
    const-string v1, "FmProxy"

    const-string v2, "Unable to unregister callback"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method protected declared-synchronized finishEventCallbackHandler()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmProxy;->mEventCallbackHandler:Lcom/broadcom/fm/fmreceiver/FmProxy$EventCallbackHandler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmProxy;->mEventCallbackHandler:Lcom/broadcom/fm/fmreceiver/FmProxy$EventCallbackHandler;

    invoke-virtual {v0}, Lcom/broadcom/fm/fmreceiver/FmProxy$EventCallbackHandler;->finish()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmProxy;->mEventCallbackHandler:Lcom/broadcom/fm/fmreceiver/FmProxy$EventCallbackHandler;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getIsMute()Z
    .locals 4

    const/4 v1, 0x0

    :try_start_0
    iget-object v2, p0, Lcom/broadcom/fm/fmreceiver/FmProxy;->mService:Lcom/broadcom/fm/fmreceiver/IFmReceiverService;

    invoke-interface {v2}, Lcom/broadcom/fm/fmreceiver/IFmReceiverService;->getIsMute()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    const-string v2, "FmProxy"

    const-string v3, "getIsMute() failed"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public getMonoStereoMode()I
    .locals 4

    const/4 v1, 0x0

    :try_start_0
    iget-object v2, p0, Lcom/broadcom/fm/fmreceiver/FmProxy;->mService:Lcom/broadcom/fm/fmreceiver/IFmReceiverService;

    invoke-interface {v2}, Lcom/broadcom/fm/fmreceiver/IFmReceiverService;->getMonoStereoMode()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    const-string v2, "FmProxy"

    const-string v3, "getMonoStereoMode() failed"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public getRadioIsOn()Z
    .locals 4

    const/4 v1, 0x0

    :try_start_0
    iget-object v2, p0, Lcom/broadcom/fm/fmreceiver/FmProxy;->mService:Lcom/broadcom/fm/fmreceiver/IFmReceiverService;

    invoke-interface {v2}, Lcom/broadcom/fm/fmreceiver/IFmReceiverService;->getRadioIsOn()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    const-string v2, "FmProxy"

    const-string v3, "getRadioIsOn() failed"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public declared-synchronized getStatus()I
    .locals 4

    monitor-enter p0

    const/4 v1, 0x2

    :try_start_0
    iget-object v2, p0, Lcom/broadcom/fm/fmreceiver/FmProxy;->mService:Lcom/broadcom/fm/fmreceiver/IFmReceiverService;

    invoke-interface {v2}, Lcom/broadcom/fm/fmreceiver/IFmReceiverService;->getStatus()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    :goto_0
    monitor-exit p0

    return v1

    :catch_0
    move-exception v0

    :try_start_1
    const-string v2, "FmProxy"

    const-string v3, "getStatus() failed"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public getTunedFrequency()I
    .locals 4

    const/4 v1, 0x0

    :try_start_0
    iget-object v2, p0, Lcom/broadcom/fm/fmreceiver/FmProxy;->mService:Lcom/broadcom/fm/fmreceiver/IFmReceiverService;

    invoke-interface {v2}, Lcom/broadcom/fm/fmreceiver/IFmReceiverService;->getTunedFrequency()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    const-string v2, "FmProxy"

    const-string v3, "getTunedFrequency() failed"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method protected init(Landroid/os/IBinder;)Z
    .locals 3
    .param p1    # Landroid/os/IBinder;

    :try_start_0
    invoke-static {p1}, Lcom/broadcom/fm/fmreceiver/IFmReceiverService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/broadcom/fm/fmreceiver/IFmReceiverService;

    move-result-object v1

    iput-object v1, p0, Lcom/broadcom/fm/fmreceiver/FmProxy;->mService:Lcom/broadcom/fm/fmreceiver/IFmReceiverService;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    const-string v1, "FmProxy"

    const-string v2, "Unable to initialize BluetoothFM proxy with service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected declared-synchronized initEventCallbackHandler()Landroid/os/Handler;
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmProxy;->mEventCallbackHandler:Lcom/broadcom/fm/fmreceiver/FmProxy$EventCallbackHandler;

    if-nez v0, :cond_0

    new-instance v0, Lcom/broadcom/fm/fmreceiver/FmProxy$EventCallbackHandler;

    invoke-direct {v0, p0}, Lcom/broadcom/fm/fmreceiver/FmProxy$EventCallbackHandler;-><init>(Lcom/broadcom/fm/fmreceiver/FmProxy;)V

    iput-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmProxy;->mEventCallbackHandler:Lcom/broadcom/fm/fmreceiver/FmProxy$EventCallbackHandler;

    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmProxy;->mEventCallbackHandler:Lcom/broadcom/fm/fmreceiver/FmProxy$EventCallbackHandler;

    invoke-virtual {v0}, Lcom/broadcom/fm/fmreceiver/FmProxy$EventCallbackHandler;->start()V

    :goto_0
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmProxy;->mEventCallbackHandler:Lcom/broadcom/fm/fmreceiver/FmProxy$EventCallbackHandler;

    iget-object v0, v0, Lcom/broadcom/fm/fmreceiver/FmProxy$EventCallbackHandler;->mHandler:Landroid/os/Handler;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    const-wide/16 v0, 0x64

    :try_start_1
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0

    :cond_0
    :try_start_2
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmProxy;->mEventCallbackHandler:Lcom/broadcom/fm/fmreceiver/FmProxy$EventCallbackHandler;

    iget-object v0, v0, Lcom/broadcom/fm/fmreceiver/FmProxy$EventCallbackHandler;->mHandler:Landroid/os/Handler;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized muteAudio(Z)I
    .locals 4
    .param p1    # Z

    monitor-enter p0

    const/4 v1, 0x2

    :try_start_0
    iget-object v2, p0, Lcom/broadcom/fm/fmreceiver/FmProxy;->mService:Lcom/broadcom/fm/fmreceiver/IFmReceiverService;

    invoke-interface {v2, p1}, Lcom/broadcom/fm/fmreceiver/IFmReceiverService;->muteAudio(Z)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    :goto_0
    monitor-exit p0

    return v1

    :catch_0
    move-exception v0

    :try_start_1
    const-string v2, "FmProxy"

    const-string v3, "muteAudio() failed"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public declared-synchronized registerEventHandler(Lcom/broadcom/fm/fmreceiver/IFmReceiverEventHandler;)V
    .locals 3
    .param p1    # Lcom/broadcom/fm/fmreceiver/IFmReceiverEventHandler;

    monitor-enter p0

    :try_start_0
    const-string v0, "FmProxy"

    const-string v1, "registerEventHandler()"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    const/4 v1, 0x1

    const/16 v2, 0xc8

    invoke-virtual {p0, p1, v0, v1, v2}, Lcom/broadcom/fm/fmreceiver/FmProxy;->registerEventHandler(Lcom/broadcom/fm/fmreceiver/IFmReceiverEventHandler;Landroid/content/IntentFilter;ZI)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized registerEventHandler(Lcom/broadcom/fm/fmreceiver/IFmReceiverEventHandler;Landroid/content/IntentFilter;Landroid/os/Handler;I)V
    .locals 3
    .param p1    # Lcom/broadcom/fm/fmreceiver/IFmReceiverEventHandler;
    .param p2    # Landroid/content/IntentFilter;
    .param p3    # Landroid/os/Handler;
    .param p4    # I

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/broadcom/fm/fmreceiver/FmProxy;->mEventHandler:Lcom/broadcom/fm/fmreceiver/IFmReceiverEventHandler;

    iget-object v1, p0, Lcom/broadcom/fm/fmreceiver/FmProxy;->mCallback:Lcom/broadcom/fm/fmreceiver/IFmReceiverCallback;

    if-nez v1, :cond_0

    new-instance v1, Lcom/broadcom/fm/fmreceiver/FmProxy$FmReceiverCallback;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/broadcom/fm/fmreceiver/FmProxy$FmReceiverCallback;-><init>(Lcom/broadcom/fm/fmreceiver/FmProxy;Lcom/broadcom/fm/fmreceiver/FmProxy$1;)V

    iput-object v1, p0, Lcom/broadcom/fm/fmreceiver/FmProxy;->mCallback:Lcom/broadcom/fm/fmreceiver/IFmReceiverCallback;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v1, p0, Lcom/broadcom/fm/fmreceiver/FmProxy;->mService:Lcom/broadcom/fm/fmreceiver/IFmReceiverService;

    iget-object v2, p0, Lcom/broadcom/fm/fmreceiver/FmProxy;->mCallback:Lcom/broadcom/fm/fmreceiver/IFmReceiverCallback;

    invoke-interface {v1, v2}, Lcom/broadcom/fm/fmreceiver/IFmReceiverService;->registerCallback(Lcom/broadcom/fm/fmreceiver/IFmReceiverCallback;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :catch_0
    move-exception v0

    :try_start_2
    const-string v1, "FmProxy"

    const-string v2, "Error registering callback handler"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized registerEventHandler(Lcom/broadcom/fm/fmreceiver/IFmReceiverEventHandler;Landroid/content/IntentFilter;ZI)V
    .locals 2
    .param p1    # Lcom/broadcom/fm/fmreceiver/IFmReceiverEventHandler;
    .param p2    # Landroid/content/IntentFilter;
    .param p3    # Z
    .param p4    # I

    monitor-enter p0

    const/4 v0, 0x0

    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p0, p1, v0, v1, p4}, Lcom/broadcom/fm/fmreceiver/FmProxy;->registerEventHandler(Lcom/broadcom/fm/fmreceiver/IFmReceiverEventHandler;Landroid/content/IntentFilter;Landroid/os/Handler;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public requiresAccessProcessing()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public seekRdsStation(III)I
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # I

    const/16 v0, 0x69

    invoke-virtual {p0, p1, v0, p2, p3}, Lcom/broadcom/fm/fmreceiver/FmProxy;->seekRdsStation(IIII)I

    move-result v0

    return v0
.end method

.method public declared-synchronized seekRdsStation(IIII)I
    .locals 4
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    monitor-enter p0

    const/4 v1, 0x2

    :try_start_0
    iget-object v2, p0, Lcom/broadcom/fm/fmreceiver/FmProxy;->mService:Lcom/broadcom/fm/fmreceiver/IFmReceiverService;

    invoke-interface {v2, p1, p2, p3, p4}, Lcom/broadcom/fm/fmreceiver/IFmReceiverService;->seekRdsStation(IIII)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    :goto_0
    monitor-exit p0

    return v1

    :catch_0
    move-exception v0

    :try_start_1
    const-string v2, "FmProxy"

    const-string v3, "seekRdsStation() failed"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public seekStation(I)I
    .locals 1
    .param p1    # I

    const/16 v0, 0x69

    invoke-virtual {p0, p1, v0}, Lcom/broadcom/fm/fmreceiver/FmProxy;->seekStation(II)I

    move-result v0

    return v0
.end method

.method public declared-synchronized seekStation(II)I
    .locals 4
    .param p1    # I
    .param p2    # I

    monitor-enter p0

    const/4 v1, 0x2

    :try_start_0
    iget-object v2, p0, Lcom/broadcom/fm/fmreceiver/FmProxy;->mService:Lcom/broadcom/fm/fmreceiver/IFmReceiverService;

    invoke-interface {v2, p1, p2}, Lcom/broadcom/fm/fmreceiver/IFmReceiverService;->seekStation(II)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    :goto_0
    monitor-exit p0

    return v1

    :catch_0
    move-exception v0

    :try_start_1
    const-string v2, "FmProxy"

    const-string v3, "seekStation() failed"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public declared-synchronized seekStationAbort()I
    .locals 4

    monitor-enter p0

    const/4 v1, 0x2

    :try_start_0
    iget-object v2, p0, Lcom/broadcom/fm/fmreceiver/FmProxy;->mService:Lcom/broadcom/fm/fmreceiver/IFmReceiverService;

    invoke-interface {v2}, Lcom/broadcom/fm/fmreceiver/IFmReceiverService;->seekStationAbort()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    :goto_0
    monitor-exit p0

    return v1

    :catch_0
    move-exception v0

    :try_start_1
    const-string v2, "FmProxy"

    const-string v3, "seekStationAbort() failed"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public declared-synchronized seekStationCombo(IIIIIZII)I
    .locals 11
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # Z
    .param p7    # I
    .param p8    # I

    monitor-enter p0

    const/4 v10, 0x2

    :try_start_0
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmProxy;->mService:Lcom/broadcom/fm/fmreceiver/IFmReceiverService;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move/from16 v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    invoke-interface/range {v0 .. v8}, Lcom/broadcom/fm/fmreceiver/IFmReceiverService;->seekStationCombo(IIIIIZII)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v10

    :goto_0
    monitor-exit p0

    return v10

    :catch_0
    move-exception v9

    :try_start_1
    const-string v0, "FmProxy"

    const-string v1, "seekStation() failed"

    invoke-static {v0, v1, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setAudioMode(I)I
    .locals 4
    .param p1    # I

    monitor-enter p0

    const/4 v1, 0x2

    :try_start_0
    iget-object v2, p0, Lcom/broadcom/fm/fmreceiver/FmProxy;->mService:Lcom/broadcom/fm/fmreceiver/IFmReceiverService;

    invoke-interface {v2, p1}, Lcom/broadcom/fm/fmreceiver/IFmReceiverService;->setAudioMode(I)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    :goto_0
    monitor-exit p0

    return v1

    :catch_0
    move-exception v0

    :try_start_1
    const-string v2, "FmProxy"

    const-string v3, "setAudioMode() failed"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public declared-synchronized setAudioPath(I)I
    .locals 4
    .param p1    # I

    monitor-enter p0

    const/4 v1, 0x2

    :try_start_0
    iget-object v2, p0, Lcom/broadcom/fm/fmreceiver/FmProxy;->mService:Lcom/broadcom/fm/fmreceiver/IFmReceiverService;

    invoke-interface {v2, p1}, Lcom/broadcom/fm/fmreceiver/IFmReceiverService;->setAudioPath(I)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    :goto_0
    monitor-exit p0

    return v1

    :catch_0
    move-exception v0

    :try_start_1
    const-string v2, "FmProxy"

    const-string v3, "setAudioPath() failed"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public declared-synchronized setFMVolume(I)I
    .locals 4
    .param p1    # I

    monitor-enter p0

    const/4 v1, 0x2

    :try_start_0
    iget-object v2, p0, Lcom/broadcom/fm/fmreceiver/FmProxy;->mService:Lcom/broadcom/fm/fmreceiver/IFmReceiverService;

    invoke-interface {v2, p1}, Lcom/broadcom/fm/fmreceiver/IFmReceiverService;->setFMVolume(I)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    :goto_0
    monitor-exit p0

    return v1

    :catch_0
    move-exception v0

    :try_start_1
    const-string v2, "FmProxy"

    const-string v3, "setFMVolume() failed"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public declared-synchronized setLiveAudioPolling(ZI)I
    .locals 4
    .param p1    # Z
    .param p2    # I

    monitor-enter p0

    const/4 v1, 0x2

    :try_start_0
    iget-object v2, p0, Lcom/broadcom/fm/fmreceiver/FmProxy;->mService:Lcom/broadcom/fm/fmreceiver/IFmReceiverService;

    invoke-interface {v2, p1, p2}, Lcom/broadcom/fm/fmreceiver/IFmReceiverService;->setLiveAudioPolling(ZI)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    :goto_0
    monitor-exit p0

    return v1

    :catch_0
    move-exception v0

    :try_start_1
    const-string v2, "FmProxy"

    const-string v3, "setLiveAudioPolling() failed"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public declared-synchronized setRdsMode(IIII)I
    .locals 4
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    monitor-enter p0

    const/4 v1, 0x2

    :try_start_0
    iget-object v2, p0, Lcom/broadcom/fm/fmreceiver/FmProxy;->mService:Lcom/broadcom/fm/fmreceiver/IFmReceiverService;

    invoke-interface {v2, p1, p2, p3, p4}, Lcom/broadcom/fm/fmreceiver/IFmReceiverService;->setRdsMode(IIII)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    :goto_0
    monitor-exit p0

    return v1

    :catch_0
    move-exception v0

    :try_start_1
    const-string v2, "FmProxy"

    const-string v3, "setRdsMode() failed"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public declared-synchronized setSnrThreshold(I)I
    .locals 4
    .param p1    # I

    monitor-enter p0

    const/4 v1, 0x2

    :try_start_0
    iget-object v2, p0, Lcom/broadcom/fm/fmreceiver/FmProxy;->mService:Lcom/broadcom/fm/fmreceiver/IFmReceiverService;

    invoke-interface {v2, p1}, Lcom/broadcom/fm/fmreceiver/IFmReceiverService;->setSnrThreshold(I)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    :goto_0
    monitor-exit p0

    return v1

    :catch_0
    move-exception v0

    :try_start_1
    const-string v2, "FmProxy"

    const-string v3, "setSnrThreshold() failed"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public declared-synchronized setStepSize(I)I
    .locals 4
    .param p1    # I

    monitor-enter p0

    const/4 v1, 0x2

    :try_start_0
    iget-object v2, p0, Lcom/broadcom/fm/fmreceiver/FmProxy;->mService:Lcom/broadcom/fm/fmreceiver/IFmReceiverService;

    invoke-interface {v2, p1}, Lcom/broadcom/fm/fmreceiver/IFmReceiverService;->setStepSize(I)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    :goto_0
    monitor-exit p0

    return v1

    :catch_0
    move-exception v0

    :try_start_1
    const-string v2, "FmProxy"

    const-string v3, "setStepSize() failed"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public declared-synchronized setWorldRegion(II)I
    .locals 4
    .param p1    # I
    .param p2    # I

    monitor-enter p0

    const/4 v1, 0x2

    :try_start_0
    iget-object v2, p0, Lcom/broadcom/fm/fmreceiver/FmProxy;->mService:Lcom/broadcom/fm/fmreceiver/IFmReceiverService;

    invoke-interface {v2, p1, p2}, Lcom/broadcom/fm/fmreceiver/IFmReceiverService;->setWorldRegion(II)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    :goto_0
    monitor-exit p0

    return v1

    :catch_0
    move-exception v0

    :try_start_1
    const-string v2, "FmProxy"

    const-string v3, "setWorldRegion() failed"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public declared-synchronized tuneRadio(I)I
    .locals 4
    .param p1    # I

    monitor-enter p0

    const/4 v1, 0x2

    :try_start_0
    iget-object v2, p0, Lcom/broadcom/fm/fmreceiver/FmProxy;->mService:Lcom/broadcom/fm/fmreceiver/IFmReceiverService;

    invoke-interface {v2, p1}, Lcom/broadcom/fm/fmreceiver/IFmReceiverService;->tuneRadio(I)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    :goto_0
    monitor-exit p0

    return v1

    :catch_0
    move-exception v0

    :try_start_1
    const-string v2, "FmProxy"

    const-string v3, "tuneRadio() failed"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public declared-synchronized turnOffRadio()I
    .locals 5

    monitor-enter p0

    const/4 v1, 0x2

    :try_start_0
    iget-object v3, p0, Lcom/broadcom/fm/fmreceiver/FmProxy;->mService:Lcom/broadcom/fm/fmreceiver/IFmReceiverService;

    invoke-interface {v3}, Lcom/broadcom/fm/fmreceiver/IFmReceiverService;->turnOffRadio()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    move v2, v1

    :goto_0
    monitor-exit p0

    return v2

    :catch_0
    move-exception v0

    :try_start_1
    const-string v3, "FmProxy"

    const-string v4, "turnOffRadio() failed"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v2, v1

    goto :goto_0

    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method public declared-synchronized turnOnRadio(ILjava/lang/String;)I
    .locals 5
    .param p1    # I
    .param p2    # Ljava/lang/String;

    monitor-enter p0

    const/4 v1, 0x2

    :try_start_0
    const-string v2, "FmProxy"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Fmproxy"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "mService"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/broadcom/fm/fmreceiver/FmProxy;->mService:Lcom/broadcom/fm/fmreceiver/IFmReceiverService;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v2, p0, Lcom/broadcom/fm/fmreceiver/FmProxy;->mService:Lcom/broadcom/fm/fmreceiver/IFmReceiverService;

    invoke-virtual {p2}, Ljava/lang/String;->toCharArray()[C

    move-result-object v3

    invoke-interface {v2, p1, v3}, Lcom/broadcom/fm/fmreceiver/IFmReceiverService;->turnOnRadio(I[C)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    :goto_0
    monitor-exit p0

    return v1

    :catch_0
    move-exception v0

    :try_start_2
    const-string v2, "FmProxy"

    const-string v3, "turnOnRadio() failed"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public turnOnRadio(Ljava/lang/String;)I
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1}, Lcom/broadcom/fm/fmreceiver/FmProxy;->turnOnRadio(ILjava/lang/String;)I

    move-result v0

    return v0
.end method

.method public declared-synchronized unregisterEventHandler()V
    .locals 3

    monitor-enter p0

    :try_start_0
    const-string v1, "FmProxy"

    const-string v2, "unregisterEventHandler()"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/broadcom/fm/fmreceiver/FmProxy;->mEventHandler:Lcom/broadcom/fm/fmreceiver/IFmReceiverEventHandler;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v1, p0, Lcom/broadcom/fm/fmreceiver/FmProxy;->mService:Lcom/broadcom/fm/fmreceiver/IFmReceiverService;

    iget-object v2, p0, Lcom/broadcom/fm/fmreceiver/FmProxy;->mCallback:Lcom/broadcom/fm/fmreceiver/IFmReceiverCallback;

    invoke-interface {v1, v2}, Lcom/broadcom/fm/fmreceiver/IFmReceiverService;->unregisterCallback(Lcom/broadcom/fm/fmreceiver/IFmReceiverCallback;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :catch_0
    move-exception v0

    :try_start_2
    const-string v1, "FmProxy"

    const-string v2, "Unable to unregister callback"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method
