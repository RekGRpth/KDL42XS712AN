.class Lcom/broadcom/fm/fmreceiver/FmProxy$1;
.super Ljava/lang/Object;
.source "FmProxy.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/broadcom/fm/fmreceiver/FmProxy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/broadcom/fm/fmreceiver/FmProxy;


# direct methods
.method constructor <init>(Lcom/broadcom/fm/fmreceiver/FmProxy;)V
    .locals 0

    iput-object p1, p0, Lcom/broadcom/fm/fmreceiver/FmProxy$1;->this$0:Lcom/broadcom/fm/fmreceiver/FmProxy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3
    .param p1    # Landroid/content/ComponentName;
    .param p2    # Landroid/os/IBinder;

    const-string v0, "FmProxy"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Fm proxy onServiceConnected() name = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", service = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmProxy$1;->this$0:Lcom/broadcom/fm/fmreceiver/FmProxy;

    invoke-virtual {v0, p2}, Lcom/broadcom/fm/fmreceiver/FmProxy;->init(Landroid/os/IBinder;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmProxy$1;->this$0:Lcom/broadcom/fm/fmreceiver/FmProxy;

    iget-object v0, v0, Lcom/broadcom/fm/fmreceiver/FmProxy;->mProxyAvailCb:Lcom/broadcom/fm/fmreceiver/IFmProxyCallback;

    if-eqz v0, :cond_1

    :cond_0
    const-string v0, "FmProxy"

    const-string v1, "Unable to create proxy"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmProxy$1;->this$0:Lcom/broadcom/fm/fmreceiver/FmProxy;

    iget-object v0, v0, Lcom/broadcom/fm/fmreceiver/FmProxy;->mProxyAvailCb:Lcom/broadcom/fm/fmreceiver/IFmProxyCallback;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmProxy$1;->this$0:Lcom/broadcom/fm/fmreceiver/FmProxy;

    iget-object v0, v0, Lcom/broadcom/fm/fmreceiver/FmProxy;->mProxyAvailCb:Lcom/broadcom/fm/fmreceiver/IFmProxyCallback;

    iget-object v1, p0, Lcom/broadcom/fm/fmreceiver/FmProxy$1;->this$0:Lcom/broadcom/fm/fmreceiver/FmProxy;

    invoke-interface {v0, v1}, Lcom/broadcom/fm/fmreceiver/IFmProxyCallback;->onProxyAvailable(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmProxy$1;->this$0:Lcom/broadcom/fm/fmreceiver/FmProxy;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/broadcom/fm/fmreceiver/FmProxy;->mProxyAvailCb:Lcom/broadcom/fm/fmreceiver/IFmProxyCallback;

    :cond_2
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1    # Landroid/content/ComponentName;

    const-string v0, "FmProxy"

    const-string v1, "Fm Proxy object disconnected"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmProxy$1;->this$0:Lcom/broadcom/fm/fmreceiver/FmProxy;

    const/4 v1, 0x0

    # setter for: Lcom/broadcom/fm/fmreceiver/FmProxy;->mService:Lcom/broadcom/fm/fmreceiver/IFmReceiverService;
    invoke-static {v0, v1}, Lcom/broadcom/fm/fmreceiver/FmProxy;->access$302(Lcom/broadcom/fm/fmreceiver/FmProxy;Lcom/broadcom/fm/fmreceiver/IFmReceiverService;)Lcom/broadcom/fm/fmreceiver/IFmReceiverService;

    return-void
.end method
