.class public Lorg/apache/harmony/xnet/provider/jsse/OpenSSLDSAParams;
.super Ljava/lang/Object;
.source "OpenSSLDSAParams.java"

# interfaces
.implements Ljava/security/interfaces/DSAParams;
.implements Ljava/security/spec/AlgorithmParameterSpec;


# instance fields
.field private fetchedParams:Z

.field private g:Ljava/math/BigInteger;

.field private key:Lorg/apache/harmony/xnet/provider/jsse/OpenSSLKey;

.field private p:Ljava/math/BigInteger;

.field private q:Ljava/math/BigInteger;

.field private x:Ljava/math/BigInteger;

.field private y:Ljava/math/BigInteger;


# direct methods
.method constructor <init>(Lorg/apache/harmony/xnet/provider/jsse/OpenSSLKey;)V
    .locals 0
    .param p1    # Lorg/apache/harmony/xnet/provider/jsse/OpenSSLKey;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLDSAParams;->key:Lorg/apache/harmony/xnet/provider/jsse/OpenSSLKey;

    return-void
.end method

.method private final declared-synchronized ensureReadParams()V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLDSAParams;->fetchedParams:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iget-object v1, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLDSAParams;->key:Lorg/apache/harmony/xnet/provider/jsse/OpenSSLKey;

    invoke-virtual {v1}, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLKey;->getPkeyContext()I

    move-result v1

    invoke-static {v1}, Lorg/apache/harmony/xnet/provider/jsse/NativeCrypto;->get_DSA_params(I)[[B

    move-result-object v0

    new-instance v1, Ljava/math/BigInteger;

    const/4 v2, 0x0

    aget-object v2, v0, v2

    invoke-direct {v1, v2}, Ljava/math/BigInteger;-><init>([B)V

    iput-object v1, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLDSAParams;->g:Ljava/math/BigInteger;

    new-instance v1, Ljava/math/BigInteger;

    const/4 v2, 0x1

    aget-object v2, v0, v2

    invoke-direct {v1, v2}, Ljava/math/BigInteger;-><init>([B)V

    iput-object v1, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLDSAParams;->p:Ljava/math/BigInteger;

    new-instance v1, Ljava/math/BigInteger;

    const/4 v2, 0x2

    aget-object v2, v0, v2

    invoke-direct {v1, v2}, Ljava/math/BigInteger;-><init>([B)V

    iput-object v1, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLDSAParams;->q:Ljava/math/BigInteger;

    const/4 v1, 0x3

    aget-object v1, v0, v1

    if-eqz v1, :cond_1

    new-instance v1, Ljava/math/BigInteger;

    const/4 v2, 0x3

    aget-object v2, v0, v2

    invoke-direct {v1, v2}, Ljava/math/BigInteger;-><init>([B)V

    iput-object v1, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLDSAParams;->y:Ljava/math/BigInteger;

    :cond_1
    const/4 v1, 0x4

    aget-object v1, v0, v1

    if-eqz v1, :cond_2

    new-instance v1, Ljava/math/BigInteger;

    const/4 v2, 0x4

    aget-object v2, v0, v2

    invoke-direct {v1, v2}, Ljava/math/BigInteger;-><init>([B)V

    iput-object v1, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLDSAParams;->x:Ljava/math/BigInteger;

    :cond_2
    const/4 v1, 0x1

    iput-boolean v1, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLDSAParams;->fetchedParams:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1    # Ljava/lang/Object;

    const/4 v2, 0x0

    const/4 v1, 0x1

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    instance-of v3, p1, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLDSAParams;

    if-eqz v3, :cond_2

    move-object v0, p1

    check-cast v0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLDSAParams;

    iget-object v3, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLDSAParams;->key:Lorg/apache/harmony/xnet/provider/jsse/OpenSSLKey;

    invoke-virtual {v0}, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLDSAParams;->getOpenSSLKey()Lorg/apache/harmony/xnet/provider/jsse/OpenSSLKey;

    move-result-object v4

    if-eq v3, v4, :cond_0

    :cond_2
    instance-of v3, p1, Ljava/security/interfaces/DSAParams;

    if-nez v3, :cond_3

    move v1, v2

    goto :goto_0

    :cond_3
    invoke-direct {p0}, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLDSAParams;->ensureReadParams()V

    move-object v0, p1

    check-cast v0, Ljava/security/interfaces/DSAParams;

    iget-object v3, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLDSAParams;->g:Ljava/math/BigInteger;

    invoke-interface {v0}, Ljava/security/interfaces/DSAParams;->getG()Ljava/math/BigInteger;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/math/BigInteger;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLDSAParams;->p:Ljava/math/BigInteger;

    invoke-interface {v0}, Ljava/security/interfaces/DSAParams;->getP()Ljava/math/BigInteger;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/math/BigInteger;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLDSAParams;->q:Ljava/math/BigInteger;

    invoke-interface {v0}, Ljava/security/interfaces/DSAParams;->getQ()Ljava/math/BigInteger;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/math/BigInteger;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_4
    move v1, v2

    goto :goto_0
.end method

.method public getG()Ljava/math/BigInteger;
    .locals 1

    invoke-direct {p0}, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLDSAParams;->ensureReadParams()V

    iget-object v0, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLDSAParams;->g:Ljava/math/BigInteger;

    return-object v0
.end method

.method getOpenSSLKey()Lorg/apache/harmony/xnet/provider/jsse/OpenSSLKey;
    .locals 1

    iget-object v0, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLDSAParams;->key:Lorg/apache/harmony/xnet/provider/jsse/OpenSSLKey;

    return-object v0
.end method

.method public getP()Ljava/math/BigInteger;
    .locals 1

    invoke-direct {p0}, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLDSAParams;->ensureReadParams()V

    iget-object v0, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLDSAParams;->p:Ljava/math/BigInteger;

    return-object v0
.end method

.method public getQ()Ljava/math/BigInteger;
    .locals 1

    invoke-direct {p0}, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLDSAParams;->ensureReadParams()V

    iget-object v0, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLDSAParams;->q:Ljava/math/BigInteger;

    return-object v0
.end method

.method getX()Ljava/math/BigInteger;
    .locals 1

    invoke-direct {p0}, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLDSAParams;->ensureReadParams()V

    iget-object v0, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLDSAParams;->x:Ljava/math/BigInteger;

    return-object v0
.end method

.method getY()Ljava/math/BigInteger;
    .locals 1

    invoke-direct {p0}, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLDSAParams;->ensureReadParams()V

    iget-object v0, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLDSAParams;->y:Ljava/math/BigInteger;

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    invoke-direct {p0}, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLDSAParams;->ensureReadParams()V

    iget-object v0, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLDSAParams;->g:Ljava/math/BigInteger;

    invoke-virtual {v0}, Ljava/math/BigInteger;->hashCode()I

    move-result v0

    iget-object v1, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLDSAParams;->p:Ljava/math/BigInteger;

    invoke-virtual {v1}, Ljava/math/BigInteger;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    iget-object v1, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLDSAParams;->q:Ljava/math/BigInteger;

    invoke-virtual {v1}, Ljava/math/BigInteger;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const/16 v2, 0x10

    invoke-direct {p0}, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLDSAParams;->ensureReadParams()V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "OpenSSLDSAParams{"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "G="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLDSAParams;->g:Ljava/math/BigInteger;

    invoke-virtual {v1, v2}, Ljava/math/BigInteger;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ",P="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLDSAParams;->p:Ljava/math/BigInteger;

    invoke-virtual {v1, v2}, Ljava/math/BigInteger;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ",Q="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lorg/apache/harmony/xnet/provider/jsse/OpenSSLDSAParams;->q:Ljava/math/BigInteger;

    invoke-virtual {v1, v2}, Ljava/math/BigInteger;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
