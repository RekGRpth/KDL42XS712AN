.class public Lorg/apache/harmony/xnet/provider/jsse/OpenSSLRandom;
.super Ljava/security/SecureRandomSpi;
.source "OpenSSLRandom.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x760c2179bb8f6dacL


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/security/SecureRandomSpi;-><init>()V

    return-void
.end method


# virtual methods
.method protected engineGenerateSeed(I)[B
    .locals 1
    .param p1    # I

    new-array v0, p1, [B

    invoke-static {v0}, Lorg/apache/harmony/xnet/provider/jsse/NativeCrypto;->RAND_bytes([B)V

    return-object v0
.end method

.method protected engineNextBytes([B)V
    .locals 0
    .param p1    # [B

    invoke-static {p1}, Lorg/apache/harmony/xnet/provider/jsse/NativeCrypto;->RAND_bytes([B)V

    return-void
.end method

.method protected engineSetSeed([B)V
    .locals 0
    .param p1    # [B

    invoke-static {p1}, Lorg/apache/harmony/xnet/provider/jsse/NativeCrypto;->RAND_seed([B)V

    return-void
.end method
