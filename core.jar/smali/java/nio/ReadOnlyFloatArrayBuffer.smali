.class final Ljava/nio/ReadOnlyFloatArrayBuffer;
.super Ljava/nio/FloatArrayBuffer;
.source "ReadOnlyFloatArrayBuffer.java"


# direct methods
.method constructor <init>(I[FI)V
    .locals 0
    .param p1    # I
    .param p2    # [F
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Ljava/nio/FloatArrayBuffer;-><init>(I[FI)V

    return-void
.end method

.method static copy(Ljava/nio/FloatArrayBuffer;I)Ljava/nio/ReadOnlyFloatArrayBuffer;
    .locals 4
    .param p0    # Ljava/nio/FloatArrayBuffer;
    .param p1    # I

    new-instance v0, Ljava/nio/ReadOnlyFloatArrayBuffer;

    invoke-virtual {p0}, Ljava/nio/FloatArrayBuffer;->capacity()I

    move-result v1

    iget-object v2, p0, Ljava/nio/FloatArrayBuffer;->backingArray:[F

    iget v3, p0, Ljava/nio/FloatArrayBuffer;->offset:I

    invoke-direct {v0, v1, v2, v3}, Ljava/nio/ReadOnlyFloatArrayBuffer;-><init>(I[FI)V

    iget v1, p0, Ljava/nio/FloatArrayBuffer;->limit:I

    iput v1, v0, Ljava/nio/ReadOnlyFloatArrayBuffer;->limit:I

    invoke-virtual {p0}, Ljava/nio/FloatArrayBuffer;->position()I

    move-result v1

    iput v1, v0, Ljava/nio/ReadOnlyFloatArrayBuffer;->position:I

    iput p1, v0, Ljava/nio/ReadOnlyFloatArrayBuffer;->mark:I

    return-object v0
.end method


# virtual methods
.method public asReadOnlyBuffer()Ljava/nio/FloatBuffer;
    .locals 1

    invoke-virtual {p0}, Ljava/nio/ReadOnlyFloatArrayBuffer;->duplicate()Ljava/nio/FloatBuffer;

    move-result-object v0

    return-object v0
.end method

.method public compact()Ljava/nio/FloatBuffer;
    .locals 1

    new-instance v0, Ljava/nio/ReadOnlyBufferException;

    invoke-direct {v0}, Ljava/nio/ReadOnlyBufferException;-><init>()V

    throw v0
.end method

.method public duplicate()Ljava/nio/FloatBuffer;
    .locals 1

    iget v0, p0, Ljava/nio/ReadOnlyFloatArrayBuffer;->mark:I

    invoke-static {p0, v0}, Ljava/nio/ReadOnlyFloatArrayBuffer;->copy(Ljava/nio/FloatArrayBuffer;I)Ljava/nio/ReadOnlyFloatArrayBuffer;

    move-result-object v0

    return-object v0
.end method

.method public isReadOnly()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protectedArray()[F
    .locals 1

    new-instance v0, Ljava/nio/ReadOnlyBufferException;

    invoke-direct {v0}, Ljava/nio/ReadOnlyBufferException;-><init>()V

    throw v0
.end method

.method protectedArrayOffset()I
    .locals 1

    new-instance v0, Ljava/nio/ReadOnlyBufferException;

    invoke-direct {v0}, Ljava/nio/ReadOnlyBufferException;-><init>()V

    throw v0
.end method

.method protectedHasArray()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public put(F)Ljava/nio/FloatBuffer;
    .locals 1
    .param p1    # F

    new-instance v0, Ljava/nio/ReadOnlyBufferException;

    invoke-direct {v0}, Ljava/nio/ReadOnlyBufferException;-><init>()V

    throw v0
.end method

.method public put(IF)Ljava/nio/FloatBuffer;
    .locals 1
    .param p1    # I
    .param p2    # F

    new-instance v0, Ljava/nio/ReadOnlyBufferException;

    invoke-direct {v0}, Ljava/nio/ReadOnlyBufferException;-><init>()V

    throw v0
.end method

.method public put(Ljava/nio/FloatBuffer;)Ljava/nio/FloatBuffer;
    .locals 1
    .param p1    # Ljava/nio/FloatBuffer;

    new-instance v0, Ljava/nio/ReadOnlyBufferException;

    invoke-direct {v0}, Ljava/nio/ReadOnlyBufferException;-><init>()V

    throw v0
.end method

.method public final put([FII)Ljava/nio/FloatBuffer;
    .locals 1
    .param p1    # [F
    .param p2    # I
    .param p3    # I

    new-instance v0, Ljava/nio/ReadOnlyBufferException;

    invoke-direct {v0}, Ljava/nio/ReadOnlyBufferException;-><init>()V

    throw v0
.end method

.method public slice()Ljava/nio/FloatBuffer;
    .locals 5

    new-instance v0, Ljava/nio/ReadOnlyFloatArrayBuffer;

    invoke-virtual {p0}, Ljava/nio/ReadOnlyFloatArrayBuffer;->remaining()I

    move-result v1

    iget-object v2, p0, Ljava/nio/ReadOnlyFloatArrayBuffer;->backingArray:[F

    iget v3, p0, Ljava/nio/ReadOnlyFloatArrayBuffer;->offset:I

    iget v4, p0, Ljava/nio/ReadOnlyFloatArrayBuffer;->position:I

    add-int/2addr v3, v4

    invoke-direct {v0, v1, v2, v3}, Ljava/nio/ReadOnlyFloatArrayBuffer;-><init>(I[FI)V

    return-object v0
.end method
