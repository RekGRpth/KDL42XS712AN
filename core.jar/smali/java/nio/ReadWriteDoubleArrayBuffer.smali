.class final Ljava/nio/ReadWriteDoubleArrayBuffer;
.super Ljava/nio/DoubleArrayBuffer;
.source "ReadWriteDoubleArrayBuffer.java"


# direct methods
.method constructor <init>(I)V
    .locals 0
    .param p1    # I

    invoke-direct {p0, p1}, Ljava/nio/DoubleArrayBuffer;-><init>(I)V

    return-void
.end method

.method constructor <init>(I[DI)V
    .locals 0
    .param p1    # I
    .param p2    # [D
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Ljava/nio/DoubleArrayBuffer;-><init>(I[DI)V

    return-void
.end method

.method constructor <init>([D)V
    .locals 0
    .param p1    # [D

    invoke-direct {p0, p1}, Ljava/nio/DoubleArrayBuffer;-><init>([D)V

    return-void
.end method

.method static copy(Ljava/nio/DoubleArrayBuffer;I)Ljava/nio/ReadWriteDoubleArrayBuffer;
    .locals 4
    .param p0    # Ljava/nio/DoubleArrayBuffer;
    .param p1    # I

    new-instance v0, Ljava/nio/ReadWriteDoubleArrayBuffer;

    invoke-virtual {p0}, Ljava/nio/DoubleArrayBuffer;->capacity()I

    move-result v1

    iget-object v2, p0, Ljava/nio/DoubleArrayBuffer;->backingArray:[D

    iget v3, p0, Ljava/nio/DoubleArrayBuffer;->offset:I

    invoke-direct {v0, v1, v2, v3}, Ljava/nio/ReadWriteDoubleArrayBuffer;-><init>(I[DI)V

    iget v1, p0, Ljava/nio/DoubleArrayBuffer;->limit:I

    iput v1, v0, Ljava/nio/ReadWriteDoubleArrayBuffer;->limit:I

    invoke-virtual {p0}, Ljava/nio/DoubleArrayBuffer;->position()I

    move-result v1

    iput v1, v0, Ljava/nio/ReadWriteDoubleArrayBuffer;->position:I

    iput p1, v0, Ljava/nio/ReadWriteDoubleArrayBuffer;->mark:I

    return-object v0
.end method


# virtual methods
.method public asReadOnlyBuffer()Ljava/nio/DoubleBuffer;
    .locals 1

    iget v0, p0, Ljava/nio/ReadWriteDoubleArrayBuffer;->mark:I

    invoke-static {p0, v0}, Ljava/nio/ReadOnlyDoubleArrayBuffer;->copy(Ljava/nio/DoubleArrayBuffer;I)Ljava/nio/ReadOnlyDoubleArrayBuffer;

    move-result-object v0

    return-object v0
.end method

.method public compact()Ljava/nio/DoubleBuffer;
    .locals 5

    iget-object v0, p0, Ljava/nio/ReadWriteDoubleArrayBuffer;->backingArray:[D

    iget v1, p0, Ljava/nio/ReadWriteDoubleArrayBuffer;->position:I

    iget v2, p0, Ljava/nio/ReadWriteDoubleArrayBuffer;->offset:I

    add-int/2addr v1, v2

    iget-object v2, p0, Ljava/nio/ReadWriteDoubleArrayBuffer;->backingArray:[D

    iget v3, p0, Ljava/nio/ReadWriteDoubleArrayBuffer;->offset:I

    invoke-virtual {p0}, Ljava/nio/ReadWriteDoubleArrayBuffer;->remaining()I

    move-result v4

    invoke-static {v0, v1, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget v0, p0, Ljava/nio/ReadWriteDoubleArrayBuffer;->limit:I

    iget v1, p0, Ljava/nio/ReadWriteDoubleArrayBuffer;->position:I

    sub-int/2addr v0, v1

    iput v0, p0, Ljava/nio/ReadWriteDoubleArrayBuffer;->position:I

    iget v0, p0, Ljava/nio/ReadWriteDoubleArrayBuffer;->capacity:I

    iput v0, p0, Ljava/nio/ReadWriteDoubleArrayBuffer;->limit:I

    const/4 v0, -0x1

    iput v0, p0, Ljava/nio/ReadWriteDoubleArrayBuffer;->mark:I

    return-object p0
.end method

.method public duplicate()Ljava/nio/DoubleBuffer;
    .locals 1

    iget v0, p0, Ljava/nio/ReadWriteDoubleArrayBuffer;->mark:I

    invoke-static {p0, v0}, Ljava/nio/ReadWriteDoubleArrayBuffer;->copy(Ljava/nio/DoubleArrayBuffer;I)Ljava/nio/ReadWriteDoubleArrayBuffer;

    move-result-object v0

    return-object v0
.end method

.method public isReadOnly()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protectedArray()[D
    .locals 1

    iget-object v0, p0, Ljava/nio/ReadWriteDoubleArrayBuffer;->backingArray:[D

    return-object v0
.end method

.method protectedArrayOffset()I
    .locals 1

    iget v0, p0, Ljava/nio/ReadWriteDoubleArrayBuffer;->offset:I

    return v0
.end method

.method protectedHasArray()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public put(D)Ljava/nio/DoubleBuffer;
    .locals 4
    .param p1    # D

    iget v0, p0, Ljava/nio/ReadWriteDoubleArrayBuffer;->position:I

    iget v1, p0, Ljava/nio/ReadWriteDoubleArrayBuffer;->limit:I

    if-ne v0, v1, :cond_0

    new-instance v0, Ljava/nio/BufferOverflowException;

    invoke-direct {v0}, Ljava/nio/BufferOverflowException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Ljava/nio/ReadWriteDoubleArrayBuffer;->backingArray:[D

    iget v1, p0, Ljava/nio/ReadWriteDoubleArrayBuffer;->offset:I

    iget v2, p0, Ljava/nio/ReadWriteDoubleArrayBuffer;->position:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Ljava/nio/ReadWriteDoubleArrayBuffer;->position:I

    add-int/2addr v1, v2

    aput-wide p1, v0, v1

    return-object p0
.end method

.method public put(ID)Ljava/nio/DoubleBuffer;
    .locals 2
    .param p1    # I
    .param p2    # D

    invoke-virtual {p0, p1}, Ljava/nio/ReadWriteDoubleArrayBuffer;->checkIndex(I)V

    iget-object v0, p0, Ljava/nio/ReadWriteDoubleArrayBuffer;->backingArray:[D

    iget v1, p0, Ljava/nio/ReadWriteDoubleArrayBuffer;->offset:I

    add-int/2addr v1, p1

    aput-wide p2, v0, v1

    return-object p0
.end method

.method public put([DII)Ljava/nio/DoubleBuffer;
    .locals 3
    .param p1    # [D
    .param p2    # I
    .param p3    # I

    invoke-virtual {p0}, Ljava/nio/ReadWriteDoubleArrayBuffer;->remaining()I

    move-result v0

    if-le p3, v0, :cond_0

    new-instance v0, Ljava/nio/BufferOverflowException;

    invoke-direct {v0}, Ljava/nio/BufferOverflowException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Ljava/nio/ReadWriteDoubleArrayBuffer;->backingArray:[D

    iget v1, p0, Ljava/nio/ReadWriteDoubleArrayBuffer;->offset:I

    iget v2, p0, Ljava/nio/ReadWriteDoubleArrayBuffer;->position:I

    add-int/2addr v1, v2

    invoke-static {p1, p2, v0, v1, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget v0, p0, Ljava/nio/ReadWriteDoubleArrayBuffer;->position:I

    add-int/2addr v0, p3

    iput v0, p0, Ljava/nio/ReadWriteDoubleArrayBuffer;->position:I

    return-object p0
.end method

.method public slice()Ljava/nio/DoubleBuffer;
    .locals 5

    new-instance v0, Ljava/nio/ReadWriteDoubleArrayBuffer;

    invoke-virtual {p0}, Ljava/nio/ReadWriteDoubleArrayBuffer;->remaining()I

    move-result v1

    iget-object v2, p0, Ljava/nio/ReadWriteDoubleArrayBuffer;->backingArray:[D

    iget v3, p0, Ljava/nio/ReadWriteDoubleArrayBuffer;->offset:I

    iget v4, p0, Ljava/nio/ReadWriteDoubleArrayBuffer;->position:I

    add-int/2addr v3, v4

    invoke-direct {v0, v1, v2, v3}, Ljava/nio/ReadWriteDoubleArrayBuffer;-><init>(I[DI)V

    return-object v0
.end method
