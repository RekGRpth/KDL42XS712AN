.class public Ljava/util/concurrent/ForkJoinPool;
.super Ljava/util/concurrent/AbstractExecutorService;
.source "ForkJoinPool.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ljava/util/concurrent/ForkJoinPool$ManagedBlocker;,
        Ljava/util/concurrent/ForkJoinPool$InvokeAll;,
        Ljava/util/concurrent/ForkJoinPool$DefaultForkJoinWorkerThreadFactory;,
        Ljava/util/concurrent/ForkJoinPool$ForkJoinWorkerThreadFactory;
    }
.end annotation


# static fields
.field private static final ABASE:J

.field private static final AC_MASK:J = -0x1000000000000L

.field private static final AC_SHIFT:I = 0x30

.field private static final AC_UNIT:J = 0x1000000000000L

.field private static final ASHIFT:I

.field private static final EC_SHIFT:I = 0x10

.field private static final EC_UNIT:I = 0x10000

.field private static final E_MASK:I = 0x7fffffff

.field private static final INITIAL_QUEUE_CAPACITY:I = 0x8

.field private static final INT_SIGN:I = -0x80000000

.field private static final MAXIMUM_QUEUE_CAPACITY:I = 0x1000000

.field private static final MAX_ID:I = 0x7fff

.field private static final SG_UNIT:I = 0x10000

.field private static final SHORT_SIGN:I = 0x8000

.field private static final SHRINK_RATE:J = 0xee6b2800L

.field private static final SMASK:I = 0xffff

.field private static final STOP_BIT:J = 0x80000000L

.field private static final ST_SHIFT:I = 0x1f

.field private static final TC_MASK:J = 0xffff00000000L

.field private static final TC_SHIFT:I = 0x20

.field private static final TC_UNIT:J = 0x100000000L

.field private static final UAC_MASK:I = -0x10000

.field private static final UAC_SHIFT:I = 0x10

.field private static final UAC_UNIT:I = 0x10000

.field private static final UNSAFE:Lsun/misc/Unsafe;

.field private static final UTC_MASK:I = 0xffff

.field private static final UTC_SHIFT:I = 0x0

.field private static final UTC_UNIT:I = 0x1

.field private static final blockedCountOffset:J

.field private static final ctlOffset:J

.field public static final defaultForkJoinWorkerThreadFactory:Ljava/util/concurrent/ForkJoinPool$ForkJoinWorkerThreadFactory;

.field private static final modifyThreadPermission:Ljava/lang/RuntimePermission;

.field private static final nextWorkerNumberOffset:J

.field private static final poolNumberGenerator:Ljava/util/concurrent/atomic/AtomicInteger;

.field private static final quiescerCountOffset:J

.field private static final scanGuardOffset:J

.field private static final stealCountOffset:J

.field static final workerSeedGenerator:Ljava/util/Random;


# instance fields
.field volatile blockedCount:I

.field volatile ctl:J

.field private final factory:Ljava/util/concurrent/ForkJoinPool$ForkJoinWorkerThreadFactory;

.field final locallyFifo:Z

.field private nextWorkerIndex:I

.field private volatile nextWorkerNumber:I

.field final parallelism:I

.field volatile queueBase:I

.field queueTop:I

.field volatile quiescerCount:I

.field volatile scanGuard:I

.field volatile shutdown:Z

.field private volatile stealCount:J

.field private final submissionLock:Ljava/util/concurrent/locks/ReentrantLock;

.field private submissionQueue:[Ljava/util/concurrent/ForkJoinTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/util/concurrent/ForkJoinTask",
            "<*>;"
        }
    .end annotation
.end field

.field private final termination:Ljava/util/concurrent/locks/Condition;

.field final ueh:Ljava/lang/Thread$UncaughtExceptionHandler;

.field private final workerNamePrefix:Ljava/lang/String;

.field workers:[Ljava/util/concurrent/ForkJoinWorkerThread;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    new-instance v4, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v4}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    sput-object v4, Ljava/util/concurrent/ForkJoinPool;->poolNumberGenerator:Ljava/util/concurrent/atomic/AtomicInteger;

    new-instance v4, Ljava/util/Random;

    invoke-direct {v4}, Ljava/util/Random;-><init>()V

    sput-object v4, Ljava/util/concurrent/ForkJoinPool;->workerSeedGenerator:Ljava/util/Random;

    new-instance v4, Ljava/lang/RuntimePermission;

    const-string v5, "modifyThread"

    invoke-direct {v4, v5}, Ljava/lang/RuntimePermission;-><init>(Ljava/lang/String;)V

    sput-object v4, Ljava/util/concurrent/ForkJoinPool;->modifyThreadPermission:Ljava/lang/RuntimePermission;

    new-instance v4, Ljava/util/concurrent/ForkJoinPool$DefaultForkJoinWorkerThreadFactory;

    invoke-direct {v4}, Ljava/util/concurrent/ForkJoinPool$DefaultForkJoinWorkerThreadFactory;-><init>()V

    sput-object v4, Ljava/util/concurrent/ForkJoinPool;->defaultForkJoinWorkerThreadFactory:Ljava/util/concurrent/ForkJoinPool$ForkJoinWorkerThreadFactory;

    :try_start_0
    invoke-static {}, Lsun/misc/Unsafe;->getUnsafe()Lsun/misc/Unsafe;

    move-result-object v4

    sput-object v4, Ljava/util/concurrent/ForkJoinPool;->UNSAFE:Lsun/misc/Unsafe;

    const-class v2, Ljava/util/concurrent/ForkJoinPool;

    sget-object v4, Ljava/util/concurrent/ForkJoinPool;->UNSAFE:Lsun/misc/Unsafe;

    const-string v5, "ctl"

    invoke-virtual {v2, v5}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v5

    invoke-virtual {v4, v5}, Lsun/misc/Unsafe;->objectFieldOffset(Ljava/lang/reflect/Field;)J

    move-result-wide v4

    sput-wide v4, Ljava/util/concurrent/ForkJoinPool;->ctlOffset:J

    sget-object v4, Ljava/util/concurrent/ForkJoinPool;->UNSAFE:Lsun/misc/Unsafe;

    const-string v5, "stealCount"

    invoke-virtual {v2, v5}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v5

    invoke-virtual {v4, v5}, Lsun/misc/Unsafe;->objectFieldOffset(Ljava/lang/reflect/Field;)J

    move-result-wide v4

    sput-wide v4, Ljava/util/concurrent/ForkJoinPool;->stealCountOffset:J

    sget-object v4, Ljava/util/concurrent/ForkJoinPool;->UNSAFE:Lsun/misc/Unsafe;

    const-string v5, "blockedCount"

    invoke-virtual {v2, v5}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v5

    invoke-virtual {v4, v5}, Lsun/misc/Unsafe;->objectFieldOffset(Ljava/lang/reflect/Field;)J

    move-result-wide v4

    sput-wide v4, Ljava/util/concurrent/ForkJoinPool;->blockedCountOffset:J

    sget-object v4, Ljava/util/concurrent/ForkJoinPool;->UNSAFE:Lsun/misc/Unsafe;

    const-string v5, "quiescerCount"

    invoke-virtual {v2, v5}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v5

    invoke-virtual {v4, v5}, Lsun/misc/Unsafe;->objectFieldOffset(Ljava/lang/reflect/Field;)J

    move-result-wide v4

    sput-wide v4, Ljava/util/concurrent/ForkJoinPool;->quiescerCountOffset:J

    sget-object v4, Ljava/util/concurrent/ForkJoinPool;->UNSAFE:Lsun/misc/Unsafe;

    const-string v5, "scanGuard"

    invoke-virtual {v2, v5}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v5

    invoke-virtual {v4, v5}, Lsun/misc/Unsafe;->objectFieldOffset(Ljava/lang/reflect/Field;)J

    move-result-wide v4

    sput-wide v4, Ljava/util/concurrent/ForkJoinPool;->scanGuardOffset:J

    sget-object v4, Ljava/util/concurrent/ForkJoinPool;->UNSAFE:Lsun/misc/Unsafe;

    const-string v5, "nextWorkerNumber"

    invoke-virtual {v2, v5}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v5

    invoke-virtual {v4, v5}, Lsun/misc/Unsafe;->objectFieldOffset(Ljava/lang/reflect/Field;)J

    move-result-wide v4

    sput-wide v4, Ljava/util/concurrent/ForkJoinPool;->nextWorkerNumberOffset:J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const-class v0, [Ljava/util/concurrent/ForkJoinTask;

    sget-object v4, Ljava/util/concurrent/ForkJoinPool;->UNSAFE:Lsun/misc/Unsafe;

    invoke-virtual {v4, v0}, Lsun/misc/Unsafe;->arrayBaseOffset(Ljava/lang/Class;)I

    move-result v4

    int-to-long v4, v4

    sput-wide v4, Ljava/util/concurrent/ForkJoinPool;->ABASE:J

    sget-object v4, Ljava/util/concurrent/ForkJoinPool;->UNSAFE:Lsun/misc/Unsafe;

    invoke-virtual {v4, v0}, Lsun/misc/Unsafe;->arrayIndexScale(Ljava/lang/Class;)I

    move-result v3

    add-int/lit8 v4, v3, -0x1

    and-int/2addr v4, v3

    if-eqz v4, :cond_0

    new-instance v4, Ljava/lang/Error;

    const-string v5, "data type scale not a power of two"

    invoke-direct {v4, v5}, Ljava/lang/Error;-><init>(Ljava/lang/String;)V

    throw v4

    :catch_0
    move-exception v1

    new-instance v4, Ljava/lang/Error;

    invoke-direct {v4, v1}, Ljava/lang/Error;-><init>(Ljava/lang/Throwable;)V

    throw v4

    :cond_0
    invoke-static {v3}, Ljava/lang/Integer;->numberOfLeadingZeros(I)I

    move-result v4

    rsub-int/lit8 v4, v4, 0x1f

    sput v4, Ljava/util/concurrent/ForkJoinPool;->ASHIFT:I

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Runtime;->availableProcessors()I

    move-result v0

    sget-object v1, Ljava/util/concurrent/ForkJoinPool;->defaultForkJoinWorkerThreadFactory:Ljava/util/concurrent/ForkJoinPool$ForkJoinWorkerThreadFactory;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {p0, v0, v1, v2, v3}, Ljava/util/concurrent/ForkJoinPool;-><init>(ILjava/util/concurrent/ForkJoinPool$ForkJoinWorkerThreadFactory;Ljava/lang/Thread$UncaughtExceptionHandler;Z)V

    return-void
.end method

.method public constructor <init>(I)V
    .locals 3
    .param p1    # I

    sget-object v0, Ljava/util/concurrent/ForkJoinPool;->defaultForkJoinWorkerThreadFactory:Ljava/util/concurrent/ForkJoinPool$ForkJoinWorkerThreadFactory;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {p0, p1, v0, v1, v2}, Ljava/util/concurrent/ForkJoinPool;-><init>(ILjava/util/concurrent/ForkJoinPool$ForkJoinWorkerThreadFactory;Ljava/lang/Thread$UncaughtExceptionHandler;Z)V

    return-void
.end method

.method public constructor <init>(ILjava/util/concurrent/ForkJoinPool$ForkJoinWorkerThreadFactory;Ljava/lang/Thread$UncaughtExceptionHandler;Z)V
    .locals 11
    .param p1    # I
    .param p2    # Ljava/util/concurrent/ForkJoinPool$ForkJoinWorkerThreadFactory;
    .param p3    # Ljava/lang/Thread$UncaughtExceptionHandler;
    .param p4    # Z

    const/16 v10, 0x7fff

    invoke-direct {p0}, Ljava/util/concurrent/AbstractExecutorService;-><init>()V

    invoke-static {}, Ljava/util/concurrent/ForkJoinPool;->checkPermission()V

    if-nez p2, :cond_0

    new-instance v4, Ljava/lang/NullPointerException;

    invoke-direct {v4}, Ljava/lang/NullPointerException;-><init>()V

    throw v4

    :cond_0
    if-lez p1, :cond_1

    if-le p1, v10, :cond_2

    :cond_1
    new-instance v4, Ljava/lang/IllegalArgumentException;

    invoke-direct {v4}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v4

    :cond_2
    iput p1, p0, Ljava/util/concurrent/ForkJoinPool;->parallelism:I

    iput-object p2, p0, Ljava/util/concurrent/ForkJoinPool;->factory:Ljava/util/concurrent/ForkJoinPool$ForkJoinWorkerThreadFactory;

    iput-object p3, p0, Ljava/util/concurrent/ForkJoinPool;->ueh:Ljava/lang/Thread$UncaughtExceptionHandler;

    iput-boolean p4, p0, Ljava/util/concurrent/ForkJoinPool;->locallyFifo:Z

    neg-int v4, p1

    int-to-long v1, v4

    const/16 v4, 0x30

    shl-long v4, v1, v4

    const-wide/high16 v6, -0x1000000000000L

    and-long/2addr v4, v6

    const/16 v6, 0x20

    shl-long v6, v1, v6

    const-wide v8, 0xffff00000000L

    and-long/2addr v6, v8

    or-long/2addr v4, v6

    iput-wide v4, p0, Ljava/util/concurrent/ForkJoinPool;->ctl:J

    const/16 v4, 0x8

    new-array v4, v4, [Ljava/util/concurrent/ForkJoinTask;

    iput-object v4, p0, Ljava/util/concurrent/ForkJoinPool;->submissionQueue:[Ljava/util/concurrent/ForkJoinTask;

    shl-int/lit8 v0, p1, 0x1

    if-lt v0, v10, :cond_3

    const/16 v0, 0x7fff

    :goto_0
    add-int/lit8 v4, v0, 0x1

    new-array v4, v4, [Ljava/util/concurrent/ForkJoinWorkerThread;

    iput-object v4, p0, Ljava/util/concurrent/ForkJoinPool;->workers:[Ljava/util/concurrent/ForkJoinWorkerThread;

    new-instance v4, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v4}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v4, p0, Ljava/util/concurrent/ForkJoinPool;->submissionLock:Ljava/util/concurrent/locks/ReentrantLock;

    iget-object v4, p0, Ljava/util/concurrent/ForkJoinPool;->submissionLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v4}, Ljava/util/concurrent/locks/ReentrantLock;->newCondition()Ljava/util/concurrent/locks/Condition;

    move-result-object v4

    iput-object v4, p0, Ljava/util/concurrent/ForkJoinPool;->termination:Ljava/util/concurrent/locks/Condition;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "ForkJoinPool-"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v4, Ljava/util/concurrent/ForkJoinPool;->poolNumberGenerator:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v4, "-worker-"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Ljava/util/concurrent/ForkJoinPool;->workerNamePrefix:Ljava/lang/String;

    return-void

    :cond_3
    ushr-int/lit8 v4, v0, 0x1

    or-int/2addr v0, v4

    ushr-int/lit8 v4, v0, 0x2

    or-int/2addr v0, v4

    ushr-int/lit8 v4, v0, 0x4

    or-int/2addr v0, v4

    ushr-int/lit8 v4, v0, 0x8

    or-int/2addr v0, v4

    goto :goto_0
.end method

.method private addSubmission(Ljava/util/concurrent/ForkJoinTask;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/ForkJoinTask",
            "<*>;)V"
        }
    .end annotation

    iget-object v0, p0, Ljava/util/concurrent/ForkJoinPool;->submissionLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    :try_start_0
    iget-object v2, p0, Ljava/util/concurrent/ForkJoinPool;->submissionQueue:[Ljava/util/concurrent/ForkJoinTask;

    if-eqz v2, :cond_0

    iget v3, p0, Ljava/util/concurrent/ForkJoinPool;->queueTop:I

    array-length v6, v2

    add-int/lit8 v1, v6, -0x1

    and-int v6, v3, v1

    sget v7, Ljava/util/concurrent/ForkJoinPool;->ASHIFT:I

    shl-int/2addr v6, v7

    int-to-long v6, v6

    sget-wide v8, Ljava/util/concurrent/ForkJoinPool;->ABASE:J

    add-long v4, v6, v8

    sget-object v6, Ljava/util/concurrent/ForkJoinPool;->UNSAFE:Lsun/misc/Unsafe;

    invoke-virtual {v6, v2, v4, v5, p1}, Lsun/misc/Unsafe;->putOrderedObject(Ljava/lang/Object;JLjava/lang/Object;)V

    add-int/lit8 v6, v3, 0x1

    iput v6, p0, Ljava/util/concurrent/ForkJoinPool;->queueTop:I

    iget v6, p0, Ljava/util/concurrent/ForkJoinPool;->queueBase:I

    sub-int v6, v3, v6

    if-ne v6, v1, :cond_0

    invoke-direct {p0}, Ljava/util/concurrent/ForkJoinPool;->growSubmissionQueue()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    invoke-virtual {p0}, Ljava/util/concurrent/ForkJoinPool;->signalWork()V

    return-void

    :catchall_0
    move-exception v6

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v6
.end method

.method private addWorker()V
    .locals 15

    const/4 v9, 0x0

    const/4 v10, 0x0

    :try_start_0
    iget-object v0, p0, Ljava/util/concurrent/ForkJoinPool;->factory:Ljava/util/concurrent/ForkJoinPool$ForkJoinWorkerThreadFactory;

    invoke-interface {v0, p0}, Ljava/util/concurrent/ForkJoinPool$ForkJoinWorkerThreadFactory;->newThread(Ljava/util/concurrent/ForkJoinPool;)Ljava/util/concurrent/ForkJoinWorkerThread;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v10

    :goto_0
    if-nez v10, :cond_2

    :cond_0
    sget-object v0, Ljava/util/concurrent/ForkJoinPool;->UNSAFE:Lsun/misc/Unsafe;

    sget-wide v2, Ljava/util/concurrent/ForkJoinPool;->ctlOffset:J

    iget-wide v4, p0, Ljava/util/concurrent/ForkJoinPool;->ctl:J

    const-wide/high16 v6, 0x1000000000000L

    sub-long v6, v4, v6

    const-wide/high16 v11, -0x1000000000000L

    and-long/2addr v6, v11

    const-wide v11, 0x100000000L

    sub-long v11, v4, v11

    const-wide v13, 0xffff00000000L

    and-long/2addr v11, v13

    or-long/2addr v6, v11

    const-wide v11, 0xffffffffL

    and-long/2addr v11, v4

    or-long/2addr v6, v11

    move-object v1, p0

    invoke-virtual/range {v0 .. v7}, Lsun/misc/Unsafe;->compareAndSwapLong(Ljava/lang/Object;JJJ)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Ljava/util/concurrent/ForkJoinPool;->tryTerminate(Z)Z

    move-result v0

    if-nez v0, :cond_1

    if-eqz v9, :cond_1

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    instance-of v0, v0, Ljava/util/concurrent/ForkJoinWorkerThread;

    if-nez v0, :cond_1

    invoke-static {v9}, Llibcore/util/SneakyThrow;->sneakyThrow(Ljava/lang/Throwable;)V

    :cond_1
    :goto_1
    return-void

    :catch_0
    move-exception v8

    move-object v9, v8

    goto :goto_0

    :cond_2
    invoke-virtual {v10}, Ljava/util/concurrent/ForkJoinWorkerThread;->start()V

    goto :goto_1
.end method

.method private awaitBlocker(Ljava/util/concurrent/ForkJoinPool$ManagedBlocker;)V
    .locals 1
    .param p1    # Ljava/util/concurrent/ForkJoinPool$ManagedBlocker;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    :cond_0
    invoke-interface {p1}, Ljava/util/concurrent/ForkJoinPool$ManagedBlocker;->isReleasable()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-direct {p0}, Ljava/util/concurrent/ForkJoinPool;->tryPreBlock()Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_1
    :try_start_0
    invoke-interface {p1}, Ljava/util/concurrent/ForkJoinPool$ManagedBlocker;->isReleasable()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-interface {p1}, Ljava/util/concurrent/ForkJoinPool$ManagedBlocker;->block()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    :cond_2
    invoke-direct {p0}, Ljava/util/concurrent/ForkJoinPool;->postBlock()V

    :cond_3
    return-void

    :catchall_0
    move-exception v0

    invoke-direct {p0}, Ljava/util/concurrent/ForkJoinPool;->postBlock()V

    throw v0
.end method

.method private cancelSubmissions()V
    .locals 3

    :cond_0
    :goto_0
    iget v1, p0, Ljava/util/concurrent/ForkJoinPool;->queueBase:I

    iget v2, p0, Ljava/util/concurrent/ForkJoinPool;->queueTop:I

    if-eq v1, v2, :cond_1

    invoke-virtual {p0}, Ljava/util/concurrent/ForkJoinPool;->pollSubmission()Ljava/util/concurrent/ForkJoinTask;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {v0, v1}, Ljava/util/concurrent/ForkJoinTask;->cancel(Z)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private static checkPermission()V
    .locals 2

    invoke-static {}, Ljava/lang/System;->getSecurityManager()Ljava/lang/SecurityManager;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v1, Ljava/util/concurrent/ForkJoinPool;->modifyThreadPermission:Ljava/lang/RuntimePermission;

    invoke-virtual {v0, v1}, Ljava/lang/SecurityManager;->checkPermission(Ljava/security/Permission;)V

    :cond_0
    return-void
.end method

.method private forkOrSubmit(Ljava/util/concurrent/ForkJoinTask;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/ForkJoinTask",
            "<TT;>;)V"
        }
    .end annotation

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iget-boolean v2, p0, Ljava/util/concurrent/ForkJoinPool;->shutdown:Z

    if-eqz v2, :cond_0

    new-instance v2, Ljava/util/concurrent/RejectedExecutionException;

    invoke-direct {v2}, Ljava/util/concurrent/RejectedExecutionException;-><init>()V

    throw v2

    :cond_0
    instance-of v2, v0, Ljava/util/concurrent/ForkJoinWorkerThread;

    if-eqz v2, :cond_1

    move-object v1, v0

    check-cast v1, Ljava/util/concurrent/ForkJoinWorkerThread;

    iget-object v2, v1, Ljava/util/concurrent/ForkJoinWorkerThread;->pool:Ljava/util/concurrent/ForkJoinPool;

    if-ne v2, p0, :cond_1

    invoke-virtual {v1, p1}, Ljava/util/concurrent/ForkJoinWorkerThread;->pushTask(Ljava/util/concurrent/ForkJoinTask;)V

    :goto_0
    return-void

    :cond_1
    invoke-direct {p0, p1}, Ljava/util/concurrent/ForkJoinPool;->addSubmission(Ljava/util/concurrent/ForkJoinTask;)V

    goto :goto_0
.end method

.method private growSubmissionQueue()V
    .locals 17

    move-object/from16 v0, p0

    iget-object v2, v0, Ljava/util/concurrent/ForkJoinPool;->submissionQueue:[Ljava/util/concurrent/ForkJoinTask;

    if-eqz v2, :cond_0

    array-length v1, v2

    shl-int/lit8 v11, v1, 0x1

    :goto_0
    const/high16 v1, 0x1000000

    if-le v11, v1, :cond_1

    new-instance v1, Ljava/util/concurrent/RejectedExecutionException;

    const-string v6, "Queue capacity exceeded"

    invoke-direct {v1, v6}, Ljava/util/concurrent/RejectedExecutionException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    const/16 v11, 0x8

    goto :goto_0

    :cond_1
    const/16 v1, 0x8

    if-ge v11, v1, :cond_2

    const/16 v11, 0x8

    :cond_2
    new-array v10, v11, [Ljava/util/concurrent/ForkJoinTask;

    move-object/from16 v0, p0

    iput-object v10, v0, Ljava/util/concurrent/ForkJoinPool;->submissionQueue:[Ljava/util/concurrent/ForkJoinTask;

    add-int/lit8 v8, v11, -0x1

    move-object/from16 v0, p0

    iget v12, v0, Ljava/util/concurrent/ForkJoinPool;->queueTop:I

    if-eqz v2, :cond_4

    array-length v1, v2

    add-int/lit8 v9, v1, -0x1

    if-ltz v9, :cond_4

    move-object/from16 v0, p0

    iget v7, v0, Ljava/util/concurrent/ForkJoinPool;->queueBase:I

    :goto_1
    if-eq v7, v12, :cond_4

    and-int v1, v7, v9

    sget v6, Ljava/util/concurrent/ForkJoinPool;->ASHIFT:I

    shl-int/2addr v1, v6

    int-to-long v13, v1

    sget-wide v15, Ljava/util/concurrent/ForkJoinPool;->ABASE:J

    add-long v3, v13, v15

    sget-object v1, Ljava/util/concurrent/ForkJoinPool;->UNSAFE:Lsun/misc/Unsafe;

    invoke-virtual {v1, v2, v3, v4}, Lsun/misc/Unsafe;->getObjectVolatile(Ljava/lang/Object;J)Ljava/lang/Object;

    move-result-object v5

    if-eqz v5, :cond_3

    sget-object v1, Ljava/util/concurrent/ForkJoinPool;->UNSAFE:Lsun/misc/Unsafe;

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Lsun/misc/Unsafe;->compareAndSwapObject(Ljava/lang/Object;JLjava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    sget-object v1, Ljava/util/concurrent/ForkJoinPool;->UNSAFE:Lsun/misc/Unsafe;

    and-int v6, v7, v8

    sget v13, Ljava/util/concurrent/ForkJoinPool;->ASHIFT:I

    shl-int/2addr v6, v13

    int-to-long v13, v6

    sget-wide v15, Ljava/util/concurrent/ForkJoinPool;->ABASE:J

    add-long/2addr v13, v15

    invoke-virtual {v1, v10, v13, v14, v5}, Lsun/misc/Unsafe;->putObjectVolatile(Ljava/lang/Object;JLjava/lang/Object;)V

    :cond_3
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    :cond_4
    return-void
.end method

.method private idleAwaitWork(Ljava/util/concurrent/ForkJoinWorkerThread;JJI)V
    .locals 11
    .param p1    # Ljava/util/concurrent/ForkJoinWorkerThread;
    .param p2    # J
    .param p4    # J
    .param p6    # I

    iget v1, p1, Ljava/util/concurrent/ForkJoinWorkerThread;->eventCount:I

    move/from16 v0, p6

    if-ne v1, v0, :cond_3

    iget-boolean v1, p0, Ljava/util/concurrent/ForkJoinPool;->shutdown:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    invoke-direct {p0, v1}, Ljava/util/concurrent/ForkJoinPool;->tryTerminate(Z)Z

    :cond_0
    invoke-static {}, Ljava/util/concurrent/ForkJoinTask;->helpExpungeStaleExceptions()V

    :cond_1
    :goto_0
    iget-wide v1, p0, Ljava/util/concurrent/ForkJoinPool;->ctl:J

    cmp-long v1, v1, p2

    if-nez v1, :cond_3

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v9

    const/4 v1, 0x1

    iput-boolean v1, p1, Ljava/util/concurrent/ForkJoinWorkerThread;->parked:Z

    iget v1, p1, Ljava/util/concurrent/ForkJoinWorkerThread;->eventCount:I

    move/from16 v0, p6

    if-ne v1, v0, :cond_2

    const-wide v1, 0xee6b2800L

    invoke-static {p0, v1, v2}, Ljava/util/concurrent/locks/LockSupport;->parkNanos(Ljava/lang/Object;J)V

    :cond_2
    const/4 v1, 0x0

    iput-boolean v1, p1, Ljava/util/concurrent/ForkJoinWorkerThread;->parked:Z

    iget v1, p1, Ljava/util/concurrent/ForkJoinWorkerThread;->eventCount:I

    move/from16 v0, p6

    if-eq v1, v0, :cond_4

    :cond_3
    :goto_1
    return-void

    :cond_4
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v1

    sub-long/2addr v1, v9

    const-wide v3, 0xd693a400L

    cmp-long v1, v1, v3

    if-gez v1, :cond_5

    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    goto :goto_0

    :cond_5
    sget-object v1, Ljava/util/concurrent/ForkJoinPool;->UNSAFE:Lsun/misc/Unsafe;

    sget-wide v3, Ljava/util/concurrent/ForkJoinPool;->ctlOffset:J

    move-object v2, p0

    move-wide v5, p2

    move-wide v7, p4

    invoke-virtual/range {v1 .. v8}, Lsun/misc/Unsafe;->compareAndSwapLong(Ljava/lang/Object;JJJ)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    iput-boolean v1, p1, Ljava/util/concurrent/ForkJoinWorkerThread;->terminate:Z

    long-to-int v1, p2

    const/high16 v2, 0x10000

    add-int/2addr v1, v2

    const v2, 0x7fffffff

    and-int/2addr v1, v2

    iput v1, p1, Ljava/util/concurrent/ForkJoinWorkerThread;->eventCount:I

    goto :goto_1
.end method

.method public static managedBlock(Ljava/util/concurrent/ForkJoinPool$ManagedBlocker;)V
    .locals 3
    .param p0    # Ljava/util/concurrent/ForkJoinPool$ManagedBlocker;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    instance-of v2, v0, Ljava/util/concurrent/ForkJoinWorkerThread;

    if-eqz v2, :cond_1

    move-object v1, v0

    check-cast v1, Ljava/util/concurrent/ForkJoinWorkerThread;

    iget-object v2, v1, Ljava/util/concurrent/ForkJoinWorkerThread;->pool:Ljava/util/concurrent/ForkJoinPool;

    invoke-direct {v2, p0}, Ljava/util/concurrent/ForkJoinPool;->awaitBlocker(Ljava/util/concurrent/ForkJoinPool$ManagedBlocker;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-interface {p0}, Ljava/util/concurrent/ForkJoinPool$ManagedBlocker;->isReleasable()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-interface {p0}, Ljava/util/concurrent/ForkJoinPool$ManagedBlocker;->block()Z

    move-result v2

    if-eqz v2, :cond_1

    goto :goto_0
.end method

.method private postBlock()V
    .locals 12

    :cond_0
    sget-object v0, Ljava/util/concurrent/ForkJoinPool;->UNSAFE:Lsun/misc/Unsafe;

    sget-wide v2, Ljava/util/concurrent/ForkJoinPool;->ctlOffset:J

    iget-wide v4, p0, Ljava/util/concurrent/ForkJoinPool;->ctl:J

    const-wide/high16 v6, 0x1000000000000L

    add-long/2addr v6, v4

    move-object v1, p0

    invoke-virtual/range {v0 .. v7}, Lsun/misc/Unsafe;->compareAndSwapLong(Ljava/lang/Object;JJJ)Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_1
    sget-object v6, Ljava/util/concurrent/ForkJoinPool;->UNSAFE:Lsun/misc/Unsafe;

    sget-wide v8, Ljava/util/concurrent/ForkJoinPool;->blockedCountOffset:J

    iget v10, p0, Ljava/util/concurrent/ForkJoinPool;->blockedCount:I

    add-int/lit8 v11, v10, -0x1

    move-object v7, p0

    invoke-virtual/range {v6 .. v11}, Lsun/misc/Unsafe;->compareAndSwapInt(Ljava/lang/Object;JII)Z

    move-result v0

    if-eqz v0, :cond_1

    return-void
.end method

.method private scan(Ljava/util/concurrent/ForkJoinWorkerThread;I)Z
    .locals 22
    .param p1    # Ljava/util/concurrent/ForkJoinWorkerThread;
    .param p2    # I

    move-object/from16 v0, p0

    iget v10, v0, Ljava/util/concurrent/ForkJoinPool;->scanGuard:I

    move-object/from16 v0, p0

    iget v2, v0, Ljava/util/concurrent/ForkJoinPool;->parallelism:I

    rsub-int/lit8 v7, p2, 0x1

    if-ne v2, v7, :cond_1

    move-object/from16 v0, p0

    iget v2, v0, Ljava/util/concurrent/ForkJoinPool;->blockedCount:I

    if-nez v2, :cond_1

    const/4 v14, 0x0

    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Ljava/util/concurrent/ForkJoinPool;->workers:[Ljava/util/concurrent/ForkJoinWorkerThread;

    move-object/from16 v17, v0

    if-eqz v17, :cond_0

    move-object/from16 v0, v17

    array-length v2, v0

    if-gt v2, v14, :cond_2

    :cond_0
    const/4 v2, 0x0

    :goto_1
    return v2

    :cond_1
    const v2, 0xffff

    and-int v14, v10, v2

    goto :goto_0

    :cond_2
    move-object/from16 v0, p1

    iget v15, v0, Ljava/util/concurrent/ForkJoinWorkerThread;->seed:I

    move v13, v15

    add-int v2, v14, v14

    neg-int v12, v2

    :goto_2
    add-int v2, v14, v14

    if-gt v12, v2, :cond_7

    and-int v2, v13, v14

    aget-object v16, v17, v2

    if-eqz v16, :cond_5

    move-object/from16 v0, v16

    iget v8, v0, Ljava/util/concurrent/ForkJoinWorkerThread;->queueBase:I

    move-object/from16 v0, v16

    iget v2, v0, Ljava/util/concurrent/ForkJoinWorkerThread;->queueTop:I

    if-eq v8, v2, :cond_5

    move-object/from16 v0, v16

    iget-object v3, v0, Ljava/util/concurrent/ForkJoinWorkerThread;->queue:[Ljava/util/concurrent/ForkJoinTask;

    if-eqz v3, :cond_5

    array-length v2, v3

    add-int/lit8 v2, v2, -0x1

    and-int v11, v2, v8

    if-ltz v11, :cond_5

    sget v2, Ljava/util/concurrent/ForkJoinPool;->ASHIFT:I

    shl-int v2, v11, v2

    int-to-long v0, v2

    move-wide/from16 v18, v0

    sget-wide v20, Ljava/util/concurrent/ForkJoinPool;->ABASE:J

    add-long v4, v18, v20

    aget-object v6, v3, v11

    if-eqz v6, :cond_4

    move-object/from16 v0, v16

    iget v2, v0, Ljava/util/concurrent/ForkJoinWorkerThread;->queueBase:I

    if-ne v2, v8, :cond_4

    sget-object v2, Ljava/util/concurrent/ForkJoinPool;->UNSAFE:Lsun/misc/Unsafe;

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Lsun/misc/Unsafe;->compareAndSwapObject(Ljava/lang/Object;JLjava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    add-int/lit8 v2, v8, 0x1

    move-object/from16 v0, v16

    iput v2, v0, Ljava/util/concurrent/ForkJoinWorkerThread;->queueBase:I

    move-object/from16 v0, v16

    iget v7, v0, Ljava/util/concurrent/ForkJoinWorkerThread;->queueTop:I

    sub-int v9, v2, v7

    move-object/from16 v0, p1

    iget v2, v0, Ljava/util/concurrent/ForkJoinWorkerThread;->poolIndex:I

    move-object/from16 v0, v16

    iput v2, v0, Ljava/util/concurrent/ForkJoinWorkerThread;->stealHint:I

    if-eqz v9, :cond_3

    invoke-virtual/range {p0 .. p0}, Ljava/util/concurrent/ForkJoinPool;->signalWork()V

    :cond_3
    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Ljava/util/concurrent/ForkJoinWorkerThread;->execTask(Ljava/util/concurrent/ForkJoinTask;)V

    :cond_4
    shl-int/lit8 v2, v15, 0xd

    xor-int/2addr v15, v2

    ushr-int/lit8 v2, v15, 0x11

    xor-int/2addr v15, v2

    shl-int/lit8 v2, v15, 0x5

    xor-int/2addr v2, v15

    move-object/from16 v0, p1

    iput v2, v0, Ljava/util/concurrent/ForkJoinWorkerThread;->seed:I

    const/4 v2, 0x0

    goto :goto_1

    :cond_5
    if-gez v12, :cond_6

    shl-int/lit8 v2, v15, 0xd

    xor-int/2addr v15, v2

    ushr-int/lit8 v2, v15, 0x11

    xor-int/2addr v15, v2

    shl-int/lit8 v2, v15, 0x5

    xor-int/2addr v15, v2

    move v13, v15

    :goto_3
    add-int/lit8 v12, v12, 0x1

    goto :goto_2

    :cond_6
    add-int/lit8 v13, v13, 0x1

    goto :goto_3

    :cond_7
    move-object/from16 v0, p0

    iget v2, v0, Ljava/util/concurrent/ForkJoinPool;->scanGuard:I

    if-eq v2, v10, :cond_8

    const/4 v2, 0x0

    goto/16 :goto_1

    :cond_8
    move-object/from16 v0, p0

    iget v8, v0, Ljava/util/concurrent/ForkJoinPool;->queueBase:I

    move-object/from16 v0, p0

    iget v2, v0, Ljava/util/concurrent/ForkJoinPool;->queueTop:I

    if-eq v8, v2, :cond_a

    move-object/from16 v0, p0

    iget-object v3, v0, Ljava/util/concurrent/ForkJoinPool;->submissionQueue:[Ljava/util/concurrent/ForkJoinTask;

    if-eqz v3, :cond_a

    array-length v2, v3

    add-int/lit8 v2, v2, -0x1

    and-int v11, v2, v8

    if-ltz v11, :cond_a

    sget v2, Ljava/util/concurrent/ForkJoinPool;->ASHIFT:I

    shl-int v2, v11, v2

    int-to-long v0, v2

    move-wide/from16 v18, v0

    sget-wide v20, Ljava/util/concurrent/ForkJoinPool;->ABASE:J

    add-long v4, v18, v20

    aget-object v6, v3, v11

    if-eqz v6, :cond_9

    move-object/from16 v0, p0

    iget v2, v0, Ljava/util/concurrent/ForkJoinPool;->queueBase:I

    if-ne v2, v8, :cond_9

    sget-object v2, Ljava/util/concurrent/ForkJoinPool;->UNSAFE:Lsun/misc/Unsafe;

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Lsun/misc/Unsafe;->compareAndSwapObject(Ljava/lang/Object;JLjava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    add-int/lit8 v2, v8, 0x1

    move-object/from16 v0, p0

    iput v2, v0, Ljava/util/concurrent/ForkJoinPool;->queueBase:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Ljava/util/concurrent/ForkJoinWorkerThread;->execTask(Ljava/util/concurrent/ForkJoinTask;)V

    :cond_9
    const/4 v2, 0x0

    goto/16 :goto_1

    :cond_a
    const/4 v2, 0x1

    goto/16 :goto_1
.end method

.method private startTerminating()V
    .locals 8

    const/4 v7, 0x1

    invoke-direct {p0}, Ljava/util/concurrent/ForkJoinPool;->cancelSubmissions()V

    const/4 v3, 0x0

    :goto_0
    const/4 v6, 0x3

    if-ge v3, v6, :cond_3

    iget-object v5, p0, Ljava/util/concurrent/ForkJoinPool;->workers:[Ljava/util/concurrent/ForkJoinWorkerThread;

    if-eqz v5, :cond_2

    move-object v0, v5

    array-length v2, v0

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v2, :cond_1

    aget-object v4, v0, v1

    if-eqz v4, :cond_0

    iput-boolean v7, v4, Ljava/util/concurrent/ForkJoinWorkerThread;->terminate:Z

    if-lez v3, :cond_0

    invoke-virtual {v4}, Ljava/util/concurrent/ForkJoinWorkerThread;->cancelTasks()V

    if-le v3, v7, :cond_0

    invoke-virtual {v4}, Ljava/util/concurrent/ForkJoinWorkerThread;->isInterrupted()Z

    move-result v6

    if-nez v6, :cond_0

    :try_start_0
    invoke-virtual {v4}, Ljava/util/concurrent/ForkJoinWorkerThread;->interrupt()V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    invoke-direct {p0}, Ljava/util/concurrent/ForkJoinPool;->terminateWaiters()V

    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :catch_0
    move-exception v6

    goto :goto_2

    :cond_3
    return-void
.end method

.method private terminateWaiters()V
    .locals 18

    move-object/from16 v0, p0

    iget-object v13, v0, Ljava/util/concurrent/ForkJoinPool;->workers:[Ljava/util/concurrent/ForkJoinWorkerThread;

    if-eqz v13, :cond_1

    array-length v11, v13

    :cond_0
    :goto_0
    move-object/from16 v0, p0

    iget-wide v5, v0, Ljava/util/concurrent/ForkJoinPool;->ctl:J

    long-to-int v9, v5

    xor-int/lit8 v1, v9, -0x1

    const v2, 0xffff

    and-int v10, v1, v2

    if-ge v10, v11, :cond_1

    aget-object v12, v13, v10

    if-eqz v12, :cond_1

    iget v1, v12, Ljava/util/concurrent/ForkJoinWorkerThread;->eventCount:I

    const v2, 0x7fffffff

    and-int/2addr v2, v9

    if-ne v1, v2, :cond_1

    sget-object v1, Ljava/util/concurrent/ForkJoinPool;->UNSAFE:Lsun/misc/Unsafe;

    sget-wide v3, Ljava/util/concurrent/ForkJoinPool;->ctlOffset:J

    iget v2, v12, Ljava/util/concurrent/ForkJoinWorkerThread;->nextWait:I

    const v7, 0x7fffffff

    and-int/2addr v2, v7

    int-to-long v7, v2

    const-wide/high16 v14, 0x1000000000000L

    add-long/2addr v14, v5

    const-wide/high16 v16, -0x1000000000000L

    and-long v14, v14, v16

    or-long/2addr v7, v14

    const-wide v14, 0xffff80000000L

    and-long/2addr v14, v5

    or-long/2addr v7, v14

    move-object/from16 v2, p0

    invoke-virtual/range {v1 .. v8}, Lsun/misc/Unsafe;->compareAndSwapLong(Ljava/lang/Object;JJJ)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iput-boolean v1, v12, Ljava/util/concurrent/ForkJoinWorkerThread;->terminate:Z

    const/high16 v1, 0x10000

    add-int/2addr v1, v9

    iput v1, v12, Ljava/util/concurrent/ForkJoinWorkerThread;->eventCount:I

    iget-boolean v1, v12, Ljava/util/concurrent/ForkJoinWorkerThread;->parked:Z

    if-eqz v1, :cond_0

    sget-object v1, Ljava/util/concurrent/ForkJoinPool;->UNSAFE:Lsun/misc/Unsafe;

    invoke-virtual {v1, v12}, Lsun/misc/Unsafe;->unpark(Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method private tryAwaitWork(Ljava/util/concurrent/ForkJoinWorkerThread;J)Z
    .locals 28
    .param p1    # Ljava/util/concurrent/ForkJoinWorkerThread;
    .param p2    # J

    move-object/from16 v0, p1

    iget v0, v0, Ljava/util/concurrent/ForkJoinWorkerThread;->eventCount:I

    move/from16 v26, v0

    move-wide/from16 v0, p2

    long-to-int v2, v0

    move-object/from16 v0, p1

    iput v2, v0, Ljava/util/concurrent/ForkJoinWorkerThread;->nextWait:I

    const v2, 0x7fffffff

    and-int v2, v2, v26

    int-to-long v2, v2

    const-wide/high16 v4, 0x1000000000000L

    sub-long v4, p2, v4

    const-wide v6, -0x100000000L

    and-long/2addr v4, v6

    or-long v8, v2, v4

    move-object/from16 v0, p0

    iget-wide v2, v0, Ljava/util/concurrent/ForkJoinPool;->ctl:J

    cmp-long v2, v2, p2

    if-nez v2, :cond_0

    sget-object v2, Ljava/util/concurrent/ForkJoinPool;->UNSAFE:Lsun/misc/Unsafe;

    sget-wide v4, Ljava/util/concurrent/ForkJoinPool;->ctlOffset:J

    move-object/from16 v3, p0

    move-wide/from16 v6, p2

    invoke-virtual/range {v2 .. v9}, Lsun/misc/Unsafe;->compareAndSwapLong(Ljava/lang/Object;JJJ)Z

    move-result v2

    if-nez v2, :cond_2

    :cond_0
    move-object/from16 v0, p0

    iget-wide v0, v0, Ljava/util/concurrent/ForkJoinPool;->ctl:J

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    long-to-int v2, v0

    move-wide/from16 v0, p2

    long-to-int v3, v0

    if-eq v2, v3, :cond_1

    const-wide/high16 v2, -0x1000000000000L

    and-long v2, v2, v18

    const-wide/high16 v4, -0x1000000000000L

    and-long v4, v4, p2

    cmp-long v2, v2, v4

    if-ltz v2, :cond_1

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    :cond_2
    move-object/from16 v0, p1

    iget v0, v0, Ljava/util/concurrent/ForkJoinWorkerThread;->stealCount:I

    move/from16 v24, v0

    :cond_3
    :goto_1
    if-eqz v24, :cond_5

    move-object/from16 v0, p0

    iget-wide v14, v0, Ljava/util/concurrent/ForkJoinPool;->stealCount:J

    sget-object v10, Ljava/util/concurrent/ForkJoinPool;->UNSAFE:Lsun/misc/Unsafe;

    sget-wide v12, Ljava/util/concurrent/ForkJoinPool;->stealCountOffset:J

    move/from16 v0, v24

    int-to-long v2, v0

    add-long v16, v14, v2

    move-object/from16 v11, p0

    invoke-virtual/range {v10 .. v17}, Lsun/misc/Unsafe;->compareAndSwapLong(Ljava/lang/Object;JJJ)Z

    move-result v2

    if-eqz v2, :cond_4

    const/16 v24, 0x0

    move/from16 v0, v24

    move-object/from16 v1, p1

    iput v0, v1, Ljava/util/concurrent/ForkJoinWorkerThread;->stealCount:I

    goto :goto_1

    :cond_4
    move-object/from16 v0, p1

    iget v2, v0, Ljava/util/concurrent/ForkJoinWorkerThread;->eventCount:I

    move/from16 v0, v26

    if-eq v2, v0, :cond_3

    const/4 v2, 0x1

    goto :goto_0

    :cond_5
    move-object/from16 v0, p0

    iget-boolean v2, v0, Ljava/util/concurrent/ForkJoinPool;->shutdown:Z

    if-eqz v2, :cond_6

    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Ljava/util/concurrent/ForkJoinPool;->tryTerminate(Z)Z

    move-result v2

    if-nez v2, :cond_7

    :cond_6
    move-wide/from16 v0, p2

    long-to-int v2, v0

    if-eqz v2, :cond_7

    move-object/from16 v0, p0

    iget v2, v0, Ljava/util/concurrent/ForkJoinPool;->parallelism:I

    const/16 v3, 0x30

    shr-long v3, v8, v3

    long-to-int v3, v3

    add-int/2addr v2, v3

    if-nez v2, :cond_7

    move-object/from16 v0, p0

    iget v2, v0, Ljava/util/concurrent/ForkJoinPool;->blockedCount:I

    if-nez v2, :cond_7

    move-object/from16 v0, p0

    iget v2, v0, Ljava/util/concurrent/ForkJoinPool;->quiescerCount:I

    if-nez v2, :cond_7

    move-object/from16 v6, p0

    move-object/from16 v7, p1

    move-wide/from16 v10, p2

    move/from16 v12, v26

    invoke-direct/range {v6 .. v12}, Ljava/util/concurrent/ForkJoinPool;->idleAwaitWork(Ljava/util/concurrent/ForkJoinWorkerThread;JJI)V

    :cond_7
    const/16 v23, 0x0

    :goto_2
    move-object/from16 v0, p1

    iget v2, v0, Ljava/util/concurrent/ForkJoinWorkerThread;->eventCount:I

    move/from16 v0, v26

    if-eq v2, v0, :cond_8

    const/4 v2, 0x1

    goto :goto_0

    :cond_8
    if-nez v23, :cond_f

    move-object/from16 v0, p0

    iget v0, v0, Ljava/util/concurrent/ForkJoinPool;->scanGuard:I

    move/from16 v20, v0

    const v2, 0xffff

    and-int v22, v20, v2

    move-object/from16 v0, p0

    iget-object v0, v0, Ljava/util/concurrent/ForkJoinPool;->workers:[Ljava/util/concurrent/ForkJoinWorkerThread;

    move-object/from16 v27, v0

    if-eqz v27, :cond_b

    move-object/from16 v0, v27

    array-length v2, v0

    move/from16 v0, v22

    if-ge v0, v2, :cond_b

    const/16 v23, 0x1

    const/16 v21, 0x0

    :goto_3
    move/from16 v0, v21

    move/from16 v1, v22

    if-gt v0, v1, :cond_b

    aget-object v25, v27, v21

    if-eqz v25, :cond_a

    move-object/from16 v0, v25

    iget v2, v0, Ljava/util/concurrent/ForkJoinWorkerThread;->queueBase:I

    move-object/from16 v0, v25

    iget v3, v0, Ljava/util/concurrent/ForkJoinWorkerThread;->queueTop:I

    if-eq v2, v3, :cond_9

    invoke-direct/range {p0 .. p0}, Ljava/util/concurrent/ForkJoinPool;->tryReleaseWaiter()Z

    move-result v2

    if-nez v2, :cond_9

    const/16 v23, 0x0

    :cond_9
    move-object/from16 v0, p1

    iget v2, v0, Ljava/util/concurrent/ForkJoinWorkerThread;->eventCount:I

    move/from16 v0, v26

    if-eq v2, v0, :cond_a

    const/4 v2, 0x1

    goto/16 :goto_0

    :cond_a
    add-int/lit8 v21, v21, 0x1

    goto :goto_3

    :cond_b
    move-object/from16 v0, p0

    iget v2, v0, Ljava/util/concurrent/ForkJoinPool;->scanGuard:I

    move/from16 v0, v20

    if-ne v2, v0, :cond_c

    move-object/from16 v0, p0

    iget v2, v0, Ljava/util/concurrent/ForkJoinPool;->queueBase:I

    move-object/from16 v0, p0

    iget v3, v0, Ljava/util/concurrent/ForkJoinPool;->queueTop:I

    if-eq v2, v3, :cond_d

    invoke-direct/range {p0 .. p0}, Ljava/util/concurrent/ForkJoinPool;->tryReleaseWaiter()Z

    move-result v2

    if-nez v2, :cond_d

    :cond_c
    const/16 v23, 0x0

    :cond_d
    if-nez v23, :cond_e

    invoke-static {}, Ljava/lang/Thread;->yield()V

    goto :goto_2

    :cond_e
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    goto :goto_2

    :cond_f
    const/4 v2, 0x1

    move-object/from16 v0, p1

    iput-boolean v2, v0, Ljava/util/concurrent/ForkJoinWorkerThread;->parked:Z

    move-object/from16 v0, p1

    iget v2, v0, Ljava/util/concurrent/ForkJoinWorkerThread;->eventCount:I

    move/from16 v0, v26

    if-eq v2, v0, :cond_10

    const/4 v2, 0x0

    move-object/from16 v0, p1

    iput-boolean v2, v0, Ljava/util/concurrent/ForkJoinWorkerThread;->parked:Z

    const/4 v2, 0x1

    goto/16 :goto_0

    :cond_10
    invoke-static/range {p0 .. p0}, Ljava/util/concurrent/locks/LockSupport;->park(Ljava/lang/Object;)V

    const/16 v23, 0x0

    move/from16 v0, v23

    move-object/from16 v1, p1

    iput-boolean v0, v1, Ljava/util/concurrent/ForkJoinWorkerThread;->parked:Z

    goto/16 :goto_2
.end method

.method private tryPreBlock()Z
    .locals 22

    move-object/from16 v0, p0

    iget v5, v0, Ljava/util/concurrent/ForkJoinPool;->blockedCount:I

    sget-object v1, Ljava/util/concurrent/ForkJoinPool;->UNSAFE:Lsun/misc/Unsafe;

    sget-wide v3, Ljava/util/concurrent/ForkJoinPool;->blockedCountOffset:J

    add-int/lit8 v6, v5, 0x1

    move-object/from16 v2, p0

    invoke-virtual/range {v1 .. v6}, Lsun/misc/Unsafe;->compareAndSwapInt(Ljava/lang/Object;JII)Z

    move-result v1

    if-eqz v1, :cond_2

    move-object/from16 v0, p0

    iget v0, v0, Ljava/util/concurrent/ForkJoinPool;->parallelism:I

    move/from16 v17, v0

    :cond_0
    move-object/from16 v0, p0

    iget-wide v10, v0, Ljava/util/concurrent/ForkJoinPool;->ctl:J

    const/16 v1, 0x20

    ushr-long v1, v10, v1

    long-to-int v0, v1

    move/from16 v19, v0

    long-to-int v15, v10

    if-gez v15, :cond_3

    :cond_1
    sget-object v1, Ljava/util/concurrent/ForkJoinPool;->UNSAFE:Lsun/misc/Unsafe;

    sget-wide v3, Ljava/util/concurrent/ForkJoinPool;->blockedCountOffset:J

    move-object/from16 v0, p0

    iget v5, v0, Ljava/util/concurrent/ForkJoinPool;->blockedCount:I

    add-int/lit8 v6, v5, -0x1

    move-object/from16 v2, p0

    invoke-virtual/range {v1 .. v6}, Lsun/misc/Unsafe;->compareAndSwapInt(Ljava/lang/Object;JII)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_3
    shr-int/lit8 v14, v19, 0x10

    if-gtz v14, :cond_5

    if-eqz v15, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Ljava/util/concurrent/ForkJoinPool;->workers:[Ljava/util/concurrent/ForkJoinWorkerThread;

    move-object/from16 v21, v0

    if-eqz v21, :cond_5

    xor-int/lit8 v1, v15, -0x1

    const v2, 0xffff

    and-int v16, v1, v2

    move-object/from16 v0, v21

    array-length v1, v0

    move/from16 v0, v16

    if-ge v0, v1, :cond_5

    aget-object v20, v21, v16

    if-eqz v20, :cond_5

    move-object/from16 v0, v20

    iget v1, v0, Ljava/util/concurrent/ForkJoinWorkerThread;->nextWait:I

    const v2, 0x7fffffff

    and-int/2addr v1, v2

    int-to-long v1, v1

    const-wide v3, -0x100000000L

    and-long/2addr v3, v10

    or-long v12, v1, v3

    move-object/from16 v0, v20

    iget v1, v0, Ljava/util/concurrent/ForkJoinWorkerThread;->eventCount:I

    if-ne v1, v15, :cond_1

    sget-object v6, Ljava/util/concurrent/ForkJoinPool;->UNSAFE:Lsun/misc/Unsafe;

    sget-wide v8, Ljava/util/concurrent/ForkJoinPool;->ctlOffset:J

    move-object/from16 v7, p0

    invoke-virtual/range {v6 .. v13}, Lsun/misc/Unsafe;->compareAndSwapLong(Ljava/lang/Object;JJJ)Z

    move-result v1

    if-eqz v1, :cond_1

    const/high16 v1, 0x10000

    add-int/2addr v1, v15

    const v2, 0x7fffffff

    and-int/2addr v1, v2

    move-object/from16 v0, v20

    iput v1, v0, Ljava/util/concurrent/ForkJoinWorkerThread;->eventCount:I

    move-object/from16 v0, v20

    iget-boolean v1, v0, Ljava/util/concurrent/ForkJoinWorkerThread;->parked:Z

    if-eqz v1, :cond_4

    sget-object v1, Ljava/util/concurrent/ForkJoinPool;->UNSAFE:Lsun/misc/Unsafe;

    move-object/from16 v0, v20

    invoke-virtual {v1, v0}, Lsun/misc/Unsafe;->unpark(Ljava/lang/Object;)V

    :cond_4
    const/4 v1, 0x1

    goto :goto_0

    :cond_5
    ushr-int/lit8 v1, v19, 0x0

    int-to-short v0, v1

    move/from16 v18, v0

    if-ltz v18, :cond_6

    add-int v1, v14, v17

    const/4 v2, 0x1

    if-le v1, v2, :cond_6

    const-wide/high16 v1, 0x1000000000000L

    sub-long v1, v10, v1

    const-wide/high16 v3, -0x1000000000000L

    and-long/2addr v1, v3

    const-wide v3, 0xffffffffffffL

    and-long/2addr v3, v10

    or-long v12, v1, v3

    sget-object v6, Ljava/util/concurrent/ForkJoinPool;->UNSAFE:Lsun/misc/Unsafe;

    sget-wide v8, Ljava/util/concurrent/ForkJoinPool;->ctlOffset:J

    move-object/from16 v7, p0

    invoke-virtual/range {v6 .. v13}, Lsun/misc/Unsafe;->compareAndSwapLong(Ljava/lang/Object;JJJ)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    goto/16 :goto_0

    :cond_6
    add-int v1, v18, v17

    const/16 v2, 0x7fff

    if-ge v1, v2, :cond_1

    const-wide v1, 0x100000000L

    add-long/2addr v1, v10

    const-wide v3, 0xffff00000000L

    and-long/2addr v1, v3

    const-wide v3, -0xffff00000001L

    and-long/2addr v3, v10

    or-long v12, v1, v3

    sget-object v6, Ljava/util/concurrent/ForkJoinPool;->UNSAFE:Lsun/misc/Unsafe;

    sget-wide v8, Ljava/util/concurrent/ForkJoinPool;->ctlOffset:J

    move-object/from16 v7, p0

    invoke-virtual/range {v6 .. v13}, Lsun/misc/Unsafe;->compareAndSwapLong(Ljava/lang/Object;JJJ)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-direct/range {p0 .. p0}, Ljava/util/concurrent/ForkJoinPool;->addWorker()V

    const/4 v1, 0x1

    goto/16 :goto_0
.end method

.method private tryReleaseWaiter()Z
    .locals 15

    const v14, 0x7fffffff

    iget-wide v4, p0, Ljava/util/concurrent/ForkJoinPool;->ctl:J

    long-to-int v8, v4

    if-lez v8, :cond_2

    const/16 v0, 0x30

    shr-long v0, v4, v0

    long-to-int v0, v0

    if-gez v0, :cond_2

    iget-object v11, p0, Ljava/util/concurrent/ForkJoinPool;->workers:[Ljava/util/concurrent/ForkJoinWorkerThread;

    if-eqz v11, :cond_2

    xor-int/lit8 v0, v8, -0x1

    const v1, 0xffff

    and-int v9, v0, v1

    array-length v0, v11

    if-ge v9, v0, :cond_2

    aget-object v10, v11, v9

    if-eqz v10, :cond_2

    iget v0, v10, Ljava/util/concurrent/ForkJoinWorkerThread;->nextWait:I

    and-int/2addr v0, v14

    int-to-long v0, v0

    const-wide/high16 v2, 0x1000000000000L

    add-long/2addr v2, v4

    const-wide v12, -0x100000000L

    and-long/2addr v2, v12

    or-long v6, v0, v2

    iget v0, v10, Ljava/util/concurrent/ForkJoinWorkerThread;->eventCount:I

    if-ne v0, v8, :cond_0

    sget-object v0, Ljava/util/concurrent/ForkJoinPool;->UNSAFE:Lsun/misc/Unsafe;

    sget-wide v2, Ljava/util/concurrent/ForkJoinPool;->ctlOffset:J

    move-object v1, p0

    invoke-virtual/range {v0 .. v7}, Lsun/misc/Unsafe;->compareAndSwapLong(Ljava/lang/Object;JJJ)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    const/high16 v0, 0x10000

    add-int/2addr v0, v8

    and-int/2addr v0, v14

    iput v0, v10, Ljava/util/concurrent/ForkJoinWorkerThread;->eventCount:I

    iget-boolean v0, v10, Ljava/util/concurrent/ForkJoinWorkerThread;->parked:Z

    if-eqz v0, :cond_2

    sget-object v0, Ljava/util/concurrent/ForkJoinPool;->UNSAFE:Lsun/misc/Unsafe;

    invoke-virtual {v0, v10}, Lsun/misc/Unsafe;->unpark(Ljava/lang/Object;)V

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private tryTerminate(Z)Z
    .locals 12
    .param p1    # Z

    const-wide v10, 0x80000000L

    const/4 v9, 0x0

    :cond_0
    :goto_0
    iget-wide v4, p0, Ljava/util/concurrent/ForkJoinPool;->ctl:J

    and-long v0, v4, v10

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_4

    if-nez p1, :cond_3

    const/16 v0, 0x30

    shr-long v0, v4, v0

    long-to-int v0, v0

    iget v1, p0, Ljava/util/concurrent/ForkJoinPool;->parallelism:I

    neg-int v1, v1

    if-eq v0, v1, :cond_1

    move v0, v9

    :goto_1
    return v0

    :cond_1
    iget-boolean v0, p0, Ljava/util/concurrent/ForkJoinPool;->shutdown:Z

    if-eqz v0, :cond_2

    iget v0, p0, Ljava/util/concurrent/ForkJoinPool;->blockedCount:I

    if-nez v0, :cond_2

    iget v0, p0, Ljava/util/concurrent/ForkJoinPool;->quiescerCount:I

    if-nez v0, :cond_2

    iget v0, p0, Ljava/util/concurrent/ForkJoinPool;->queueBase:I

    iget v1, p0, Ljava/util/concurrent/ForkJoinPool;->queueTop:I

    if-eq v0, v1, :cond_3

    :cond_2
    iget-wide v0, p0, Ljava/util/concurrent/ForkJoinPool;->ctl:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    move v0, v9

    goto :goto_1

    :cond_3
    sget-object v0, Ljava/util/concurrent/ForkJoinPool;->UNSAFE:Lsun/misc/Unsafe;

    sget-wide v2, Ljava/util/concurrent/ForkJoinPool;->ctlOffset:J

    or-long v6, v4, v10

    move-object v1, p0

    invoke-virtual/range {v0 .. v7}, Lsun/misc/Unsafe;->compareAndSwapLong(Ljava/lang/Object;JJJ)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Ljava/util/concurrent/ForkJoinPool;->startTerminating()V

    goto :goto_0

    :cond_4
    const/16 v0, 0x20

    ushr-long v0, v4, v0

    long-to-int v0, v0

    int-to-short v0, v0

    iget v1, p0, Ljava/util/concurrent/ForkJoinPool;->parallelism:I

    neg-int v1, v1

    if-ne v0, v1, :cond_5

    iget-object v8, p0, Ljava/util/concurrent/ForkJoinPool;->submissionLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v8}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    :try_start_0
    iget-object v0, p0, Ljava/util/concurrent/ForkJoinPool;->termination:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signalAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v8}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    :cond_5
    const/4 v0, 0x1

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-virtual {v8}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method


# virtual methods
.method final addActiveCount(I)V
    .locals 10
    .param p1    # I

    int-to-long v0, p1

    const/16 v2, 0x30

    shl-long v8, v0, v2

    :cond_0
    sget-object v0, Ljava/util/concurrent/ForkJoinPool;->UNSAFE:Lsun/misc/Unsafe;

    sget-wide v2, Ljava/util/concurrent/ForkJoinPool;->ctlOffset:J

    iget-wide v4, p0, Ljava/util/concurrent/ForkJoinPool;->ctl:J

    add-long v6, v4, v8

    move-object v1, p0

    invoke-virtual/range {v0 .. v7}, Lsun/misc/Unsafe;->compareAndSwapLong(Ljava/lang/Object;JJJ)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void
.end method

.method final addQuiescerCount(I)V
    .locals 6
    .param p1    # I

    :cond_0
    sget-object v0, Ljava/util/concurrent/ForkJoinPool;->UNSAFE:Lsun/misc/Unsafe;

    sget-wide v2, Ljava/util/concurrent/ForkJoinPool;->quiescerCountOffset:J

    iget v4, p0, Ljava/util/concurrent/ForkJoinPool;->quiescerCount:I

    add-int v5, v4, p1

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Lsun/misc/Unsafe;->compareAndSwapInt(Ljava/lang/Object;JII)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void
.end method

.method public awaitTermination(JLjava/util/concurrent/TimeUnit;)Z
    .locals 5
    .param p1    # J
    .param p3    # Ljava/util/concurrent/TimeUnit;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    invoke-virtual {p3, p1, p2}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v1

    iget-object v0, p0, Ljava/util/concurrent/ForkJoinPool;->submissionLock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    :goto_0
    :try_start_0
    invoke-virtual {p0}, Ljava/util/concurrent/ForkJoinPool;->isTerminated()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    :goto_1
    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    return v3

    :cond_0
    const-wide/16 v3, 0x0

    cmp-long v3, v1, v3

    if-gtz v3, :cond_1

    const/4 v3, 0x0

    goto :goto_1

    :cond_1
    :try_start_1
    iget-object v3, p0, Ljava/util/concurrent/ForkJoinPool;->termination:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v3, v1, v2}, Ljava/util/concurrent/locks/Condition;->awaitNanos(J)J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v1

    goto :goto_0

    :catchall_0
    move-exception v3

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v3
.end method

.method final deregisterWorker(Ljava/util/concurrent/ForkJoinWorkerThread;Ljava/lang/Throwable;)V
    .locals 26
    .param p1    # Ljava/util/concurrent/ForkJoinWorkerThread;
    .param p2    # Ljava/lang/Throwable;

    move-object/from16 v0, p1

    iget v0, v0, Ljava/util/concurrent/ForkJoinWorkerThread;->poolIndex:I

    move/from16 v22, v0

    move-object/from16 v0, p1

    iget v0, v0, Ljava/util/concurrent/ForkJoinWorkerThread;->stealCount:I

    move/from16 v23, v0

    const/16 v24, 0x0

    :cond_0
    if-nez v24, :cond_2

    move-object/from16 v0, p0

    iget v6, v0, Ljava/util/concurrent/ForkJoinPool;->scanGuard:I

    const/high16 v2, 0x10000

    and-int/2addr v2, v6

    if-nez v2, :cond_2

    sget-object v2, Ljava/util/concurrent/ForkJoinPool;->UNSAFE:Lsun/misc/Unsafe;

    sget-wide v4, Ljava/util/concurrent/ForkJoinPool;->scanGuardOffset:J

    const/high16 v3, 0x10000

    or-int v7, v6, v3

    move-object/from16 v3, p0

    invoke-virtual/range {v2 .. v7}, Lsun/misc/Unsafe;->compareAndSwapInt(Ljava/lang/Object;JII)Z

    move-result v2

    if-eqz v2, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Ljava/util/concurrent/ForkJoinPool;->workers:[Ljava/util/concurrent/ForkJoinWorkerThread;

    move-object/from16 v25, v0

    if-eqz v25, :cond_1

    if-ltz v22, :cond_1

    move-object/from16 v0, v25

    array-length v2, v0

    move/from16 v0, v22

    if-ge v0, v2, :cond_1

    aget-object v2, v25, v22

    move-object/from16 v0, p1

    if-ne v2, v0, :cond_1

    const/4 v2, 0x0

    aput-object v2, v25, v22

    :cond_1
    move/from16 v0, v22

    move-object/from16 v1, p0

    iput v0, v1, Ljava/util/concurrent/ForkJoinPool;->nextWorkerIndex:I

    const/high16 v2, 0x10000

    add-int/2addr v2, v7

    move-object/from16 v0, p0

    iput v2, v0, Ljava/util/concurrent/ForkJoinPool;->scanGuard:I

    const/16 v24, 0x1

    :cond_2
    const/4 v2, 0x1

    move/from16 v0, v24

    if-ne v0, v2, :cond_3

    sget-object v8, Ljava/util/concurrent/ForkJoinPool;->UNSAFE:Lsun/misc/Unsafe;

    sget-wide v10, Ljava/util/concurrent/ForkJoinPool;->ctlOffset:J

    move-object/from16 v0, p0

    iget-wide v12, v0, Ljava/util/concurrent/ForkJoinPool;->ctl:J

    const-wide/high16 v2, 0x1000000000000L

    sub-long v2, v12, v2

    const-wide/high16 v4, -0x1000000000000L

    and-long/2addr v2, v4

    const-wide v4, 0x100000000L

    sub-long v4, v12, v4

    const-wide v14, 0xffff00000000L

    and-long/2addr v4, v14

    or-long/2addr v2, v4

    const-wide v4, 0xffffffffL

    and-long/2addr v4, v12

    or-long v14, v2, v4

    move-object/from16 v9, p0

    invoke-virtual/range {v8 .. v15}, Lsun/misc/Unsafe;->compareAndSwapLong(Ljava/lang/Object;JJJ)Z

    move-result v2

    if-eqz v2, :cond_3

    const/16 v24, 0x2

    :cond_3
    if-eqz v23, :cond_4

    sget-object v14, Ljava/util/concurrent/ForkJoinPool;->UNSAFE:Lsun/misc/Unsafe;

    sget-wide v16, Ljava/util/concurrent/ForkJoinPool;->stealCountOffset:J

    move-object/from16 v0, p0

    iget-wide v0, v0, Ljava/util/concurrent/ForkJoinPool;->stealCount:J

    move-wide/from16 v18, v0

    move/from16 v0, v23

    int-to-long v2, v0

    add-long v20, v18, v2

    move-object/from16 v15, p0

    invoke-virtual/range {v14 .. v21}, Lsun/misc/Unsafe;->compareAndSwapLong(Ljava/lang/Object;JJJ)Z

    move-result v2

    if-eqz v2, :cond_4

    const/16 v23, 0x0

    :cond_4
    const/4 v2, 0x2

    move/from16 v0, v24

    if-ne v0, v2, :cond_0

    if-nez v23, :cond_0

    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Ljava/util/concurrent/ForkJoinPool;->tryTerminate(Z)Z

    move-result v2

    if-nez v2, :cond_5

    if-eqz p2, :cond_6

    invoke-virtual/range {p0 .. p0}, Ljava/util/concurrent/ForkJoinPool;->signalWork()V

    :cond_5
    :goto_0
    return-void

    :cond_6
    invoke-direct/range {p0 .. p0}, Ljava/util/concurrent/ForkJoinPool;->tryReleaseWaiter()Z

    goto :goto_0
.end method

.method protected drainTasksTo(Ljava/util/Collection;)I
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<-",
            "Ljava/util/concurrent/ForkJoinTask",
            "<*>;>;)I"
        }
    .end annotation

    const/4 v1, 0x0

    :cond_0
    :goto_0
    iget v7, p0, Ljava/util/concurrent/ForkJoinPool;->queueBase:I

    iget v8, p0, Ljava/util/concurrent/ForkJoinPool;->queueTop:I

    if-eq v7, v8, :cond_1

    invoke-virtual {p0}, Ljava/util/concurrent/ForkJoinPool;->pollSubmission()Ljava/util/concurrent/ForkJoinTask;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-interface {p1, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    iget-wide v7, p0, Ljava/util/concurrent/ForkJoinPool;->ctl:J

    const/16 v9, 0x20

    ushr-long/2addr v7, v9

    long-to-int v7, v7

    int-to-short v7, v7

    iget v8, p0, Ljava/util/concurrent/ForkJoinPool;->parallelism:I

    neg-int v8, v8

    if-le v7, v8, :cond_3

    iget-object v6, p0, Ljava/util/concurrent/ForkJoinPool;->workers:[Ljava/util/concurrent/ForkJoinWorkerThread;

    if-eqz v6, :cond_3

    move-object v0, v6

    array-length v3, v0

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v3, :cond_3

    aget-object v5, v0, v2

    if-eqz v5, :cond_2

    invoke-virtual {v5, p1}, Ljava/util/concurrent/ForkJoinWorkerThread;->drainTasksTo(Ljava/util/Collection;)I

    move-result v7

    add-int/2addr v1, v7

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_3
    return v1
.end method

.method public execute(Ljava/lang/Runnable;)V
    .locals 2
    .param p1    # Ljava/lang/Runnable;

    if-nez p1, :cond_0

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-direct {v1}, Ljava/lang/NullPointerException;-><init>()V

    throw v1

    :cond_0
    instance-of v1, p1, Ljava/util/concurrent/ForkJoinTask;

    if-eqz v1, :cond_1

    move-object v0, p1

    check-cast v0, Ljava/util/concurrent/ForkJoinTask;

    :goto_0
    invoke-direct {p0, v0}, Ljava/util/concurrent/ForkJoinPool;->forkOrSubmit(Ljava/util/concurrent/ForkJoinTask;)V

    return-void

    :cond_1
    const/4 v1, 0x0

    invoke-static {p1, v1}, Ljava/util/concurrent/ForkJoinTask;->adapt(Ljava/lang/Runnable;Ljava/lang/Object;)Ljava/util/concurrent/ForkJoinTask;

    move-result-object v0

    goto :goto_0
.end method

.method public execute(Ljava/util/concurrent/ForkJoinTask;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/ForkJoinTask",
            "<*>;)V"
        }
    .end annotation

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0, p1}, Ljava/util/concurrent/ForkJoinPool;->forkOrSubmit(Ljava/util/concurrent/ForkJoinTask;)V

    return-void
.end method

.method public getActiveThreadCount()I
    .locals 5

    iget v1, p0, Ljava/util/concurrent/ForkJoinPool;->parallelism:I

    iget-wide v2, p0, Ljava/util/concurrent/ForkJoinPool;->ctl:J

    const/16 v4, 0x30

    shr-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v1, v2

    iget v2, p0, Ljava/util/concurrent/ForkJoinPool;->blockedCount:I

    add-int v0, v1, v2

    if-gtz v0, :cond_0

    const/4 v0, 0x0

    :cond_0
    return v0
.end method

.method public getAsyncMode()Z
    .locals 1

    iget-boolean v0, p0, Ljava/util/concurrent/ForkJoinPool;->locallyFifo:Z

    return v0
.end method

.method public getFactory()Ljava/util/concurrent/ForkJoinPool$ForkJoinWorkerThreadFactory;
    .locals 1

    iget-object v0, p0, Ljava/util/concurrent/ForkJoinPool;->factory:Ljava/util/concurrent/ForkJoinPool$ForkJoinWorkerThreadFactory;

    return-object v0
.end method

.method public getParallelism()I
    .locals 1

    iget v0, p0, Ljava/util/concurrent/ForkJoinPool;->parallelism:I

    return v0
.end method

.method public getPoolSize()I
    .locals 4

    iget v0, p0, Ljava/util/concurrent/ForkJoinPool;->parallelism:I

    iget-wide v1, p0, Ljava/util/concurrent/ForkJoinPool;->ctl:J

    const/16 v3, 0x20

    ushr-long/2addr v1, v3

    long-to-int v1, v1

    int-to-short v1, v1

    add-int/2addr v0, v1

    return v0
.end method

.method public getQueuedSubmissionCount()I
    .locals 2

    iget v0, p0, Ljava/util/concurrent/ForkJoinPool;->queueBase:I

    neg-int v0, v0

    iget v1, p0, Ljava/util/concurrent/ForkJoinPool;->queueTop:I

    add-int/2addr v0, v1

    return v0
.end method

.method public getQueuedTaskCount()J
    .locals 10

    const-wide/16 v1, 0x0

    iget-wide v7, p0, Ljava/util/concurrent/ForkJoinPool;->ctl:J

    const/16 v9, 0x20

    ushr-long/2addr v7, v9

    long-to-int v7, v7

    int-to-short v7, v7

    iget v8, p0, Ljava/util/concurrent/ForkJoinPool;->parallelism:I

    neg-int v8, v8

    if-le v7, v8, :cond_1

    iget-object v6, p0, Ljava/util/concurrent/ForkJoinPool;->workers:[Ljava/util/concurrent/ForkJoinWorkerThread;

    if-eqz v6, :cond_1

    move-object v0, v6

    array-length v4, v0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v4, :cond_1

    aget-object v5, v0, v3

    if-eqz v5, :cond_0

    iget v7, v5, Ljava/util/concurrent/ForkJoinWorkerThread;->queueBase:I

    iget v8, v5, Ljava/util/concurrent/ForkJoinWorkerThread;->queueTop:I

    sub-int/2addr v7, v8

    int-to-long v7, v7

    sub-long/2addr v1, v7

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    return-wide v1
.end method

.method public getRunningThreadCount()I
    .locals 5

    iget v1, p0, Ljava/util/concurrent/ForkJoinPool;->parallelism:I

    iget-wide v2, p0, Ljava/util/concurrent/ForkJoinPool;->ctl:J

    const/16 v4, 0x30

    shr-long/2addr v2, v4

    long-to-int v2, v2

    add-int v0, v1, v2

    if-gtz v0, :cond_0

    const/4 v0, 0x0

    :cond_0
    return v0
.end method

.method public getStealCount()J
    .locals 2

    iget-wide v0, p0, Ljava/util/concurrent/ForkJoinPool;->stealCount:J

    return-wide v0
.end method

.method public getUncaughtExceptionHandler()Ljava/lang/Thread$UncaughtExceptionHandler;
    .locals 1

    iget-object v0, p0, Ljava/util/concurrent/ForkJoinPool;->ueh:Ljava/lang/Thread$UncaughtExceptionHandler;

    return-object v0
.end method

.method public hasQueuedSubmissions()Z
    .locals 2

    iget v0, p0, Ljava/util/concurrent/ForkJoinPool;->queueBase:I

    iget v1, p0, Ljava/util/concurrent/ForkJoinPool;->queueTop:I

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final idlePerActive()I
    .locals 5

    iget v1, p0, Ljava/util/concurrent/ForkJoinPool;->parallelism:I

    iget-wide v2, p0, Ljava/util/concurrent/ForkJoinPool;->ctl:J

    const/16 v4, 0x30

    shr-long/2addr v2, v4

    long-to-int v2, v2

    add-int v0, v1, v2

    ushr-int/lit8 v1, v1, 0x1

    if-le v0, v1, :cond_0

    const/4 v2, 0x0

    :goto_0
    return v2

    :cond_0
    ushr-int/lit8 v1, v1, 0x1

    if-le v0, v1, :cond_1

    const/4 v2, 0x1

    goto :goto_0

    :cond_1
    ushr-int/lit8 v1, v1, 0x1

    if-le v0, v1, :cond_2

    const/4 v2, 0x2

    goto :goto_0

    :cond_2
    ushr-int/lit8 v1, v1, 0x1

    if-le v0, v1, :cond_3

    const/4 v2, 0x4

    goto :goto_0

    :cond_3
    const/16 v2, 0x8

    goto :goto_0
.end method

.method public invoke(Ljava/util/concurrent/ForkJoinTask;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/ForkJoinTask",
            "<TT;>;)TT;"
        }
    .end annotation

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    if-nez p1, :cond_0

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-direct {v1}, Ljava/lang/NullPointerException;-><init>()V

    throw v1

    :cond_0
    iget-boolean v1, p0, Ljava/util/concurrent/ForkJoinPool;->shutdown:Z

    if-eqz v1, :cond_1

    new-instance v1, Ljava/util/concurrent/RejectedExecutionException;

    invoke-direct {v1}, Ljava/util/concurrent/RejectedExecutionException;-><init>()V

    throw v1

    :cond_1
    instance-of v1, v0, Ljava/util/concurrent/ForkJoinWorkerThread;

    if-eqz v1, :cond_2

    check-cast v0, Ljava/util/concurrent/ForkJoinWorkerThread;

    iget-object v1, v0, Ljava/util/concurrent/ForkJoinWorkerThread;->pool:Ljava/util/concurrent/ForkJoinPool;

    if-ne v1, p0, :cond_2

    invoke-virtual {p1}, Ljava/util/concurrent/ForkJoinTask;->invoke()Ljava/lang/Object;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_2
    invoke-direct {p0, p1}, Ljava/util/concurrent/ForkJoinPool;->addSubmission(Ljava/util/concurrent/ForkJoinTask;)V

    invoke-virtual {p1}, Ljava/util/concurrent/ForkJoinTask;->join()Ljava/lang/Object;

    move-result-object v1

    goto :goto_0
.end method

.method public invokeAll(Ljava/util/Collection;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Collection",
            "<+",
            "Ljava/util/concurrent/Callable",
            "<TT;>;>;)",
            "Ljava/util/List",
            "<",
            "Ljava/util/concurrent/Future",
            "<TT;>;>;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v4

    invoke-direct {v0, v4}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/Callable;

    invoke-static {v3}, Ljava/util/concurrent/ForkJoinTask;->adapt(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/ForkJoinTask;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    new-instance v4, Ljava/util/concurrent/ForkJoinPool$InvokeAll;

    invoke-direct {v4, v0}, Ljava/util/concurrent/ForkJoinPool$InvokeAll;-><init>(Ljava/util/ArrayList;)V

    invoke-virtual {p0, v4}, Ljava/util/concurrent/ForkJoinPool;->invoke(Ljava/util/concurrent/ForkJoinTask;)Ljava/lang/Object;

    move-object v1, v0

    return-object v1
.end method

.method final isAtLeastTerminating()Z
    .locals 4

    iget-wide v0, p0, Ljava/util/concurrent/ForkJoinPool;->ctl:J

    const-wide v2, 0x80000000L

    and-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isQuiescent()Z
    .locals 4

    iget v0, p0, Ljava/util/concurrent/ForkJoinPool;->parallelism:I

    iget-wide v1, p0, Ljava/util/concurrent/ForkJoinPool;->ctl:J

    const/16 v3, 0x30

    shr-long/2addr v1, v3

    long-to-int v1, v1

    add-int/2addr v0, v1

    iget v1, p0, Ljava/util/concurrent/ForkJoinPool;->blockedCount:I

    add-int/2addr v0, v1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isShutdown()Z
    .locals 1

    iget-boolean v0, p0, Ljava/util/concurrent/ForkJoinPool;->shutdown:Z

    return v0
.end method

.method public isTerminated()Z
    .locals 6

    iget-wide v0, p0, Ljava/util/concurrent/ForkJoinPool;->ctl:J

    const-wide v2, 0x80000000L

    and-long/2addr v2, v0

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    const/16 v2, 0x20

    ushr-long v2, v0, v2

    long-to-int v2, v2

    int-to-short v2, v2

    iget v3, p0, Ljava/util/concurrent/ForkJoinPool;->parallelism:I

    neg-int v3, v3

    if-ne v2, v3, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public isTerminating()Z
    .locals 6

    iget-wide v0, p0, Ljava/util/concurrent/ForkJoinPool;->ctl:J

    const-wide v2, 0x80000000L

    and-long/2addr v2, v0

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    const/16 v2, 0x20

    ushr-long v2, v0, v2

    long-to-int v2, v2

    int-to-short v2, v2

    iget v3, p0, Ljava/util/concurrent/ForkJoinPool;->parallelism:I

    neg-int v3, v3

    if-eq v2, v3, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method protected newTaskFor(Ljava/lang/Runnable;Ljava/lang/Object;)Ljava/util/concurrent/RunnableFuture;
    .locals 1
    .param p1    # Ljava/lang/Runnable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Runnable;",
            "TT;)",
            "Ljava/util/concurrent/RunnableFuture",
            "<TT;>;"
        }
    .end annotation

    invoke-static {p1, p2}, Ljava/util/concurrent/ForkJoinTask;->adapt(Ljava/lang/Runnable;Ljava/lang/Object;)Ljava/util/concurrent/ForkJoinTask;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/RunnableFuture;

    return-object v0
.end method

.method protected newTaskFor(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/RunnableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Callable",
            "<TT;>;)",
            "Ljava/util/concurrent/RunnableFuture",
            "<TT;>;"
        }
    .end annotation

    invoke-static {p1}, Ljava/util/concurrent/ForkJoinTask;->adapt(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/ForkJoinTask;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/RunnableFuture;

    return-object v0
.end method

.method final nextWorkerName()Ljava/lang/String;
    .locals 6

    :cond_0
    sget-object v0, Ljava/util/concurrent/ForkJoinPool;->UNSAFE:Lsun/misc/Unsafe;

    sget-wide v2, Ljava/util/concurrent/ForkJoinPool;->nextWorkerNumberOffset:J

    iget v4, p0, Ljava/util/concurrent/ForkJoinPool;->nextWorkerNumber:I

    add-int/lit8 v5, v4, 0x1

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Lsun/misc/Unsafe;->compareAndSwapInt(Ljava/lang/Object;JII)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Ljava/util/concurrent/ForkJoinPool;->workerNamePrefix:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected pollSubmission()Ljava/util/concurrent/ForkJoinTask;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/concurrent/ForkJoinTask",
            "<*>;"
        }
    .end annotation

    const/4 v5, 0x0

    :cond_0
    iget v6, p0, Ljava/util/concurrent/ForkJoinPool;->queueBase:I

    iget v0, p0, Ljava/util/concurrent/ForkJoinPool;->queueTop:I

    if-eq v6, v0, :cond_1

    iget-object v1, p0, Ljava/util/concurrent/ForkJoinPool;->submissionQueue:[Ljava/util/concurrent/ForkJoinTask;

    if-eqz v1, :cond_1

    array-length v0, v1

    add-int/lit8 v0, v0, -0x1

    and-int v7, v0, v6

    if-ltz v7, :cond_1

    sget v0, Ljava/util/concurrent/ForkJoinPool;->ASHIFT:I

    shl-int v0, v7, v0

    int-to-long v8, v0

    sget-wide v10, Ljava/util/concurrent/ForkJoinPool;->ABASE:J

    add-long v2, v8, v10

    aget-object v4, v1, v7

    if-eqz v4, :cond_0

    iget v0, p0, Ljava/util/concurrent/ForkJoinPool;->queueBase:I

    if-ne v0, v6, :cond_0

    sget-object v0, Ljava/util/concurrent/ForkJoinPool;->UNSAFE:Lsun/misc/Unsafe;

    invoke-virtual/range {v0 .. v5}, Lsun/misc/Unsafe;->compareAndSwapObject(Ljava/lang/Object;JLjava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    add-int/lit8 v0, v6, 0x1

    iput v0, p0, Ljava/util/concurrent/ForkJoinPool;->queueBase:I

    :goto_0
    return-object v4

    :cond_1
    move-object v4, v5

    goto :goto_0
.end method

.method final registerWorker(Ljava/util/concurrent/ForkJoinWorkerThread;)I
    .locals 14
    .param p1    # Ljava/util/concurrent/ForkJoinWorkerThread;

    :cond_0
    iget v4, p0, Ljava/util/concurrent/ForkJoinPool;->scanGuard:I

    const/high16 v0, 0x10000

    and-int/2addr v0, v4

    if-nez v0, :cond_6

    sget-object v0, Ljava/util/concurrent/ForkJoinPool;->UNSAFE:Lsun/misc/Unsafe;

    sget-wide v2, Ljava/util/concurrent/ForkJoinPool;->scanGuardOffset:J

    const/high16 v1, 0x10000

    or-int v5, v4, v1

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Lsun/misc/Unsafe;->compareAndSwapInt(Ljava/lang/Object;JII)Z

    move-result v0

    if-eqz v0, :cond_6

    iget v8, p0, Ljava/util/concurrent/ForkJoinPool;->nextWorkerIndex:I

    :try_start_0
    iget-object v13, p0, Ljava/util/concurrent/ForkJoinPool;->workers:[Ljava/util/concurrent/ForkJoinWorkerThread;

    if-eqz v13, :cond_4

    array-length v11, v13

    if-ltz v8, :cond_1

    if-ge v8, v11, :cond_1

    aget-object v0, v13, v8

    if-eqz v0, :cond_3

    :cond_1
    const/4 v8, 0x0

    :goto_0
    if-ge v8, v11, :cond_2

    aget-object v0, v13, v8

    if-eqz v0, :cond_2

    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    :cond_2
    if-ne v8, v11, :cond_3

    shl-int/lit8 v0, v11, 0x1

    invoke-static {v13, v0}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v13

    check-cast v13, [Ljava/util/concurrent/ForkJoinWorkerThread;

    iput-object v13, p0, Ljava/util/concurrent/ForkJoinPool;->workers:[Ljava/util/concurrent/ForkJoinWorkerThread;

    :cond_3
    aput-object p1, v13, v8

    add-int/lit8 v0, v8, 0x1

    iput v0, p0, Ljava/util/concurrent/ForkJoinPool;->nextWorkerIndex:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const v0, 0xffff

    and-int v10, v4, v0

    if-le v8, v10, :cond_5

    shl-int/lit8 v0, v10, 0x1

    add-int/lit8 v0, v0, 0x1

    const v1, 0xffff

    and-int v4, v0, v1

    :cond_4
    :goto_1
    iput v4, p0, Ljava/util/concurrent/ForkJoinPool;->scanGuard:I

    return v8

    :cond_5
    const/high16 v0, 0x20000

    add-int/2addr v4, v0

    goto :goto_1

    :catchall_0
    move-exception v0

    iput v4, p0, Ljava/util/concurrent/ForkJoinPool;->scanGuard:I

    throw v0

    :cond_6
    iget-object v13, p0, Ljava/util/concurrent/ForkJoinPool;->workers:[Ljava/util/concurrent/ForkJoinWorkerThread;

    if-eqz v13, :cond_0

    move-object v6, v13

    array-length v9, v6

    const/4 v7, 0x0

    :goto_2
    if-ge v7, v9, :cond_0

    aget-object v12, v6, v7

    if-eqz v12, :cond_7

    iget v0, v12, Ljava/util/concurrent/ForkJoinWorkerThread;->queueBase:I

    iget v1, v12, Ljava/util/concurrent/ForkJoinWorkerThread;->queueTop:I

    if-eq v0, v1, :cond_7

    invoke-direct {p0}, Ljava/util/concurrent/ForkJoinPool;->tryReleaseWaiter()Z

    move-result v0

    if-nez v0, :cond_0

    :cond_7
    add-int/lit8 v7, v7, 0x1

    goto :goto_2
.end method

.method public shutdown()V
    .locals 1

    invoke-static {}, Ljava/util/concurrent/ForkJoinPool;->checkPermission()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Ljava/util/concurrent/ForkJoinPool;->shutdown:Z

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Ljava/util/concurrent/ForkJoinPool;->tryTerminate(Z)Z

    return-void
.end method

.method public shutdownNow()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x1

    invoke-static {}, Ljava/util/concurrent/ForkJoinPool;->checkPermission()V

    iput-boolean v0, p0, Ljava/util/concurrent/ForkJoinPool;->shutdown:Z

    invoke-direct {p0, v0}, Ljava/util/concurrent/ForkJoinPool;->tryTerminate(Z)Z

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method final signalWork()V
    .locals 22

    :cond_0
    move-object/from16 v0, p0

    iget-wide v5, v0, Ljava/util/concurrent/ForkJoinPool;->ctl:J

    long-to-int v0, v5

    move/from16 v17, v0

    const/16 v1, 0x20

    ushr-long v1, v5, v1

    long-to-int v0, v1

    move/from16 v19, v0

    or-int v1, v17, v19

    const v2, -0x7fff8000

    and-int/2addr v1, v2

    const v2, -0x7fff8000

    if-ne v1, v2, :cond_1

    if-ltz v17, :cond_1

    if-lez v17, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Ljava/util/concurrent/ForkJoinPool;->workers:[Ljava/util/concurrent/ForkJoinWorkerThread;

    move-object/from16 v21, v0

    if-eqz v21, :cond_1

    xor-int/lit8 v1, v17, -0x1

    const v2, 0xffff

    and-int v18, v1, v2

    move-object/from16 v0, v21

    array-length v1, v0

    move/from16 v0, v18

    if-ge v0, v1, :cond_1

    aget-object v20, v21, v18

    if-nez v20, :cond_2

    :cond_1
    :goto_0
    return-void

    :cond_2
    move-object/from16 v0, v20

    iget v1, v0, Ljava/util/concurrent/ForkJoinWorkerThread;->nextWait:I

    const v2, 0x7fffffff

    and-int/2addr v1, v2

    int-to-long v1, v1

    const/high16 v3, 0x10000

    add-int v3, v3, v19

    int-to-long v3, v3

    const/16 v9, 0x20

    shl-long/2addr v3, v9

    or-long v7, v1, v3

    move-object/from16 v0, v20

    iget v1, v0, Ljava/util/concurrent/ForkJoinWorkerThread;->eventCount:I

    move/from16 v0, v17

    if-ne v1, v0, :cond_0

    sget-object v1, Ljava/util/concurrent/ForkJoinPool;->UNSAFE:Lsun/misc/Unsafe;

    sget-wide v3, Ljava/util/concurrent/ForkJoinPool;->ctlOffset:J

    move-object/from16 v2, p0

    invoke-virtual/range {v1 .. v8}, Lsun/misc/Unsafe;->compareAndSwapLong(Ljava/lang/Object;JJJ)Z

    move-result v1

    if-eqz v1, :cond_0

    const/high16 v1, 0x10000

    add-int v1, v1, v17

    const v2, 0x7fffffff

    and-int/2addr v1, v2

    move-object/from16 v0, v20

    iput v1, v0, Ljava/util/concurrent/ForkJoinWorkerThread;->eventCount:I

    move-object/from16 v0, v20

    iget-boolean v1, v0, Ljava/util/concurrent/ForkJoinWorkerThread;->parked:Z

    if-eqz v1, :cond_1

    sget-object v1, Ljava/util/concurrent/ForkJoinPool;->UNSAFE:Lsun/misc/Unsafe;

    move-object/from16 v0, v20

    invoke-virtual {v1, v0}, Lsun/misc/Unsafe;->unpark(Ljava/lang/Object;)V

    goto :goto_0

    :cond_3
    sget-object v9, Ljava/util/concurrent/ForkJoinPool;->UNSAFE:Lsun/misc/Unsafe;

    sget-wide v11, Ljava/util/concurrent/ForkJoinPool;->ctlOffset:J

    add-int/lit8 v1, v19, 0x1

    const v2, 0xffff

    and-int/2addr v1, v2

    const/high16 v2, 0x10000

    add-int v2, v2, v19

    const/high16 v3, -0x10000

    and-int/2addr v2, v3

    or-int/2addr v1, v2

    int-to-long v1, v1

    const/16 v3, 0x20

    shl-long v15, v1, v3

    move-object/from16 v10, p0

    move-wide v13, v5

    invoke-virtual/range {v9 .. v16}, Lsun/misc/Unsafe;->compareAndSwapLong(Ljava/lang/Object;JJJ)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct/range {p0 .. p0}, Ljava/util/concurrent/ForkJoinPool;->addWorker()V

    goto :goto_0
.end method

.method public submit(Ljava/lang/Runnable;)Ljava/util/concurrent/ForkJoinTask;
    .locals 2
    .param p1    # Ljava/lang/Runnable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Runnable;",
            ")",
            "Ljava/util/concurrent/ForkJoinTask",
            "<*>;"
        }
    .end annotation

    if-nez p1, :cond_0

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-direct {v1}, Ljava/lang/NullPointerException;-><init>()V

    throw v1

    :cond_0
    instance-of v1, p1, Ljava/util/concurrent/ForkJoinTask;

    if-eqz v1, :cond_1

    move-object v0, p1

    check-cast v0, Ljava/util/concurrent/ForkJoinTask;

    :goto_0
    invoke-direct {p0, v0}, Ljava/util/concurrent/ForkJoinPool;->forkOrSubmit(Ljava/util/concurrent/ForkJoinTask;)V

    return-object v0

    :cond_1
    const/4 v1, 0x0

    invoke-static {p1, v1}, Ljava/util/concurrent/ForkJoinTask;->adapt(Ljava/lang/Runnable;Ljava/lang/Object;)Ljava/util/concurrent/ForkJoinTask;

    move-result-object v0

    goto :goto_0
.end method

.method public submit(Ljava/lang/Runnable;Ljava/lang/Object;)Ljava/util/concurrent/ForkJoinTask;
    .locals 2
    .param p1    # Ljava/lang/Runnable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Runnable;",
            "TT;)",
            "Ljava/util/concurrent/ForkJoinTask",
            "<TT;>;"
        }
    .end annotation

    if-nez p1, :cond_0

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-direct {v1}, Ljava/lang/NullPointerException;-><init>()V

    throw v1

    :cond_0
    invoke-static {p1, p2}, Ljava/util/concurrent/ForkJoinTask;->adapt(Ljava/lang/Runnable;Ljava/lang/Object;)Ljava/util/concurrent/ForkJoinTask;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/util/concurrent/ForkJoinPool;->forkOrSubmit(Ljava/util/concurrent/ForkJoinTask;)V

    return-object v0
.end method

.method public submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/ForkJoinTask;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Callable",
            "<TT;>;)",
            "Ljava/util/concurrent/ForkJoinTask",
            "<TT;>;"
        }
    .end annotation

    if-nez p1, :cond_0

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-direct {v1}, Ljava/lang/NullPointerException;-><init>()V

    throw v1

    :cond_0
    invoke-static {p1}, Ljava/util/concurrent/ForkJoinTask;->adapt(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/ForkJoinTask;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/util/concurrent/ForkJoinPool;->forkOrSubmit(Ljava/util/concurrent/ForkJoinTask;)V

    return-object v0
.end method

.method public submit(Ljava/util/concurrent/ForkJoinTask;)Ljava/util/concurrent/ForkJoinTask;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/ForkJoinTask",
            "<TT;>;)",
            "Ljava/util/concurrent/ForkJoinTask",
            "<TT;>;"
        }
    .end annotation

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0, p1}, Ljava/util/concurrent/ForkJoinPool;->forkOrSubmit(Ljava/util/concurrent/ForkJoinTask;)V

    return-object p1
.end method

.method public bridge synthetic submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;
    .locals 1
    .param p1    # Ljava/lang/Runnable;

    invoke-virtual {p0, p1}, Ljava/util/concurrent/ForkJoinPool;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/ForkJoinTask;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic submit(Ljava/lang/Runnable;Ljava/lang/Object;)Ljava/util/concurrent/Future;
    .locals 1
    .param p1    # Ljava/lang/Runnable;
    .param p2    # Ljava/lang/Object;

    invoke-virtual {p0, p1, p2}, Ljava/util/concurrent/ForkJoinPool;->submit(Ljava/lang/Runnable;Ljava/lang/Object;)Ljava/util/concurrent/ForkJoinTask;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;
    .locals 1
    .param p1    # Ljava/util/concurrent/Callable;

    invoke-virtual {p0, p1}, Ljava/util/concurrent/ForkJoinPool;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/ForkJoinTask;

    move-result-object v0

    return-object v0
.end method

.method final timedAwaitJoin(Ljava/util/concurrent/ForkJoinTask;J)V
    .locals 12
    .param p2    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/ForkJoinTask",
            "<*>;J)V"
        }
    .end annotation

    const-wide v10, 0x80000000L

    const-wide/16 v8, 0x0

    :cond_0
    iget v6, p1, Ljava/util/concurrent/ForkJoinTask;->status:I

    if-ltz v6, :cond_1

    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    iget-wide v6, p0, Ljava/util/concurrent/ForkJoinPool;->ctl:J

    and-long/2addr v6, v10

    cmp-long v6, v6, v8

    if-eqz v6, :cond_2

    invoke-virtual {p1}, Ljava/util/concurrent/ForkJoinTask;->cancelIgnoringExceptions()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-direct {p0}, Ljava/util/concurrent/ForkJoinPool;->tryPreBlock()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    :goto_1
    iget v6, p1, Ljava/util/concurrent/ForkJoinTask;->status:I

    if-ltz v6, :cond_3

    sget-object v6, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v6, p2, p3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    cmp-long v6, v2, v8

    if-gtz v6, :cond_4

    :cond_3
    :goto_2
    invoke-direct {p0}, Ljava/util/concurrent/ForkJoinPool;->postBlock()V

    goto :goto_0

    :cond_4
    invoke-virtual {p1, v2, v3}, Ljava/util/concurrent/ForkJoinTask;->tryAwaitDone(J)V

    iget v6, p1, Ljava/util/concurrent/ForkJoinTask;->status:I

    if-ltz v6, :cond_3

    iget-wide v6, p0, Ljava/util/concurrent/ForkJoinPool;->ctl:J

    and-long/2addr v6, v10

    cmp-long v6, v6, v8

    if-eqz v6, :cond_5

    invoke-virtual {p1}, Ljava/util/concurrent/ForkJoinTask;->cancelIgnoringExceptions()V

    goto :goto_2

    :cond_5
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v4

    sub-long v6, v4, v0

    sub-long/2addr p2, v6

    move-wide v0, v4

    goto :goto_1
.end method

.method public toString()Ljava/lang/String;
    .locals 18

    invoke-virtual/range {p0 .. p0}, Ljava/util/concurrent/ForkJoinPool;->getStealCount()J

    move-result-wide v11

    invoke-virtual/range {p0 .. p0}, Ljava/util/concurrent/ForkJoinPool;->getQueuedTaskCount()J

    move-result-wide v8

    invoke-virtual/range {p0 .. p0}, Ljava/util/concurrent/ForkJoinPool;->getQueuedSubmissionCount()I

    move-result v14

    int-to-long v6, v14

    move-object/from16 v0, p0

    iget v5, v0, Ljava/util/concurrent/ForkJoinPool;->parallelism:I

    move-object/from16 v0, p0

    iget-wide v2, v0, Ljava/util/concurrent/ForkJoinPool;->ctl:J

    const/16 v14, 0x20

    ushr-long v14, v2, v14

    long-to-int v14, v14

    int-to-short v14, v14

    add-int v13, v5, v14

    const/16 v14, 0x30

    shr-long v14, v2, v14

    long-to-int v14, v14

    add-int v10, v5, v14

    if-gez v10, :cond_0

    const/4 v10, 0x0

    :cond_0
    move-object/from16 v0, p0

    iget v14, v0, Ljava/util/concurrent/ForkJoinPool;->blockedCount:I

    add-int v1, v10, v14

    const-wide v14, 0x80000000L

    and-long/2addr v14, v2

    const-wide/16 v16, 0x0

    cmp-long v14, v14, v16

    if-eqz v14, :cond_2

    if-nez v13, :cond_1

    const-string v4, "Terminated"

    :goto_0
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-super/range {p0 .. p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "["

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ", parallelism = "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ", size = "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ", active = "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ", running = "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ", steals = "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ", tasks = "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ", submissions = "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "]"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    return-object v14

    :cond_1
    const-string v4, "Terminating"

    goto :goto_0

    :cond_2
    move-object/from16 v0, p0

    iget-boolean v14, v0, Ljava/util/concurrent/ForkJoinPool;->shutdown:Z

    if-eqz v14, :cond_3

    const-string v4, "Shutting down"

    :goto_1
    goto :goto_0

    :cond_3
    const-string v4, "Running"

    goto :goto_1
.end method

.method final tryAwaitJoin(Ljava/util/concurrent/ForkJoinTask;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/ForkJoinTask",
            "<*>;)V"
        }
    .end annotation

    const-wide/16 v4, 0x0

    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    iget v0, p1, Ljava/util/concurrent/ForkJoinTask;->status:I

    if-ltz v0, :cond_0

    invoke-direct {p0}, Ljava/util/concurrent/ForkJoinPool;->tryPreBlock()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1, v4, v5}, Ljava/util/concurrent/ForkJoinTask;->tryAwaitDone(J)V

    invoke-direct {p0}, Ljava/util/concurrent/ForkJoinPool;->postBlock()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-wide v0, p0, Ljava/util/concurrent/ForkJoinPool;->ctl:J

    const-wide v2, 0x80000000L

    and-long/2addr v0, v2

    cmp-long v0, v0, v4

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Ljava/util/concurrent/ForkJoinTask;->cancelIgnoringExceptions()V

    goto :goto_0
.end method

.method final work(Ljava/util/concurrent/ForkJoinWorkerThread;)V
    .locals 6
    .param p1    # Ljava/util/concurrent/ForkJoinWorkerThread;

    const/4 v3, 0x0

    :cond_0
    :goto_0
    iget-boolean v4, p1, Ljava/util/concurrent/ForkJoinWorkerThread;->terminate:Z

    if-nez v4, :cond_2

    iget-wide v1, p0, Ljava/util/concurrent/ForkJoinPool;->ctl:J

    long-to-int v4, v1

    if-ltz v4, :cond_2

    if-nez v3, :cond_1

    const/16 v4, 0x30

    shr-long v4, v1, v4

    long-to-int v0, v4

    if-gtz v0, :cond_1

    invoke-direct {p0, p1, v0}, Ljava/util/concurrent/ForkJoinPool;->scan(Ljava/util/concurrent/ForkJoinWorkerThread;I)Z

    move-result v3

    goto :goto_0

    :cond_1
    invoke-direct {p0, p1, v1, v2}, Ljava/util/concurrent/ForkJoinPool;->tryAwaitWork(Ljava/util/concurrent/ForkJoinWorkerThread;J)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v3, 0x0

    goto :goto_0

    :cond_2
    return-void
.end method
