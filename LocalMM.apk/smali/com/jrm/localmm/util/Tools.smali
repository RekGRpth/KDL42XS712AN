.class public Lcom/jrm/localmm/util/Tools;
.super Ljava/lang/Object;
.source "Tools.java"


# static fields
.field private static appSkin:Lcom/mstar/android/tv/TvCommonManager;

.field private static audioTrackSettingOptTextOne:[Ljava/lang/String;

.field private static audioTrackSettingOptTextTwo:[Ljava/lang/String;

.field private static inputSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

.field private static playSettingOptTextOne:[Ljava/lang/String;

.field private static playSettingOptTextTwo:[Ljava/lang/String;

.field private static subTitleSettingOptTextOne:[Ljava/lang/String;

.field private static subTitleSettingOptTextTwo:[Ljava/lang/String;

.field private static subtitleLanguageTypeOne:[Ljava/lang/String;

.field private static subtitleLanguageTypeTwo:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/jrm/localmm/util/Tools;->subTitleSettingOptTextOne:[Ljava/lang/String;

    sput-object v0, Lcom/jrm/localmm/util/Tools;->subTitleSettingOptTextTwo:[Ljava/lang/String;

    sput-object v0, Lcom/jrm/localmm/util/Tools;->playSettingOptTextOne:[Ljava/lang/String;

    sput-object v0, Lcom/jrm/localmm/util/Tools;->playSettingOptTextTwo:[Ljava/lang/String;

    sput-object v0, Lcom/jrm/localmm/util/Tools;->audioTrackSettingOptTextOne:[Ljava/lang/String;

    sput-object v0, Lcom/jrm/localmm/util/Tools;->audioTrackSettingOptTextTwo:[Ljava/lang/String;

    sput-object v0, Lcom/jrm/localmm/util/Tools;->subtitleLanguageTypeOne:[Ljava/lang/String;

    sput-object v0, Lcom/jrm/localmm/util/Tools;->subtitleLanguageTypeTwo:[Ljava/lang/String;

    sput-object v0, Lcom/jrm/localmm/util/Tools;->appSkin:Lcom/mstar/android/tv/TvCommonManager;

    sput-object v0, Lcom/jrm/localmm/util/Tools;->inputSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000()Lcom/mstar/android/tv/TvCommonManager;
    .locals 1

    sget-object v0, Lcom/jrm/localmm/util/Tools;->appSkin:Lcom/mstar/android/tv/TvCommonManager;

    return-object v0
.end method

.method static synthetic access$002(Lcom/mstar/android/tv/TvCommonManager;)Lcom/mstar/android/tv/TvCommonManager;
    .locals 0
    .param p0    # Lcom/mstar/android/tv/TvCommonManager;

    sput-object p0, Lcom/jrm/localmm/util/Tools;->appSkin:Lcom/mstar/android/tv/TvCommonManager;

    return-object p0
.end method

.method static synthetic access$100()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    .locals 1

    sget-object v0, Lcom/jrm/localmm/util/Tools;->inputSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    return-object v0
.end method

.method static synthetic access$102(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    .locals 0
    .param p0    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    sput-object p0, Lcom/jrm/localmm/util/Tools;->inputSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    return-object p0
.end method

.method public static changeSource(Z)V
    .locals 2
    .param p0    # Z

    new-instance v0, Lcom/jrm/localmm/util/Tools$1;

    invoke-direct {v0, p0}, Lcom/jrm/localmm/util/Tools$1;-><init>(Z)V

    new-instance v1, Ljava/lang/Thread;

    invoke-direct {v1, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method public static destroyAllSettingOpt()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/jrm/localmm/util/Tools;->playSettingOptTextOne:[Ljava/lang/String;

    sput-object v0, Lcom/jrm/localmm/util/Tools;->subTitleSettingOptTextOne:[Ljava/lang/String;

    sput-object v0, Lcom/jrm/localmm/util/Tools;->audioTrackSettingOptTextOne:[Ljava/lang/String;

    sput-object v0, Lcom/jrm/localmm/util/Tools;->playSettingOptTextTwo:[Ljava/lang/String;

    sput-object v0, Lcom/jrm/localmm/util/Tools;->subTitleSettingOptTextTwo:[Ljava/lang/String;

    sput-object v0, Lcom/jrm/localmm/util/Tools;->audioTrackSettingOptTextTwo:[Ljava/lang/String;

    return-void
.end method

.method public static formatDuration(J)Ljava/lang/String;
    .locals 12
    .param p0    # J

    const-wide/16 v10, 0x3c

    const-wide/16 v8, 0x3e8

    div-long v6, p0, v8

    const-wide/16 v8, 0x0

    cmp-long v8, v6, v8

    if-gtz v8, :cond_0

    const-string v8, "--:--:--"

    :goto_0
    return-object v8

    :cond_0
    div-long v8, v6, v10

    rem-long v2, v8, v10

    div-long v8, v6, v10

    div-long v0, v8, v10

    rem-long v4, v6, v10

    const-string v8, "%02d:%02d:%02d"

    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x2

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    goto :goto_0
.end method

.method public static formatDuration2(I)Ljava/lang/String;
    .locals 7
    .param p0    # I

    div-int/lit16 v2, p0, 0x3e8

    if-nez v2, :cond_0

    if-lez p0, :cond_0

    const/4 v2, 0x1

    :cond_0
    div-int/lit8 v0, v2, 0x3c

    rem-int/lit8 v1, v2, 0x3c

    const-string v3, "%02d:%02d"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method public static formatSize(Ljava/math/BigInteger;)Ljava/lang/String;
    .locals 11
    .param p0    # Ljava/math/BigInteger;

    const-wide/32 v9, 0x40000000

    const-wide/32 v7, 0x100000

    const-wide v5, 0x10000000000L

    const-wide/16 v3, 0x400

    const/4 v2, -0x1

    invoke-static {v3, v4}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/math/BigInteger;->compareTo(Ljava/math/BigInteger;)I

    move-result v0

    if-ne v0, v2, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/math/BigInteger;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "B"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {v7, v8}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/math/BigInteger;->compareTo(Ljava/math/BigInteger;)I

    move-result v0

    if-ne v0, v2, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v3, v4}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v1

    invoke-virtual {p0, v1}, Ljava/math/BigInteger;->divide(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v1

    invoke-virtual {v1}, Ljava/math/BigInteger;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "KB"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-static {v9, v10}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/math/BigInteger;->compareTo(Ljava/math/BigInteger;)I

    move-result v0

    if-ne v0, v2, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v7, v8}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v1

    invoke-virtual {p0, v1}, Ljava/math/BigInteger;->divide(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v1

    invoke-virtual {v1}, Ljava/math/BigInteger;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "MB"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    invoke-static {v5, v6}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/math/BigInteger;->compareTo(Ljava/math/BigInteger;)I

    move-result v0

    if-ne v0, v2, :cond_3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v9, v10}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v1

    invoke-virtual {p0, v1}, Ljava/math/BigInteger;->divide(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v1

    invoke-virtual {v1}, Ljava/math/BigInteger;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "GB"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_3
    invoke-static {v5, v6}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v0

    invoke-static {v3, v4}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/math/BigInteger;->multiply(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/math/BigInteger;->compareTo(Ljava/math/BigInteger;)I

    move-result v0

    if-ne v0, v2, :cond_4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v5, v6}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v1

    invoke-virtual {p0, v1}, Ljava/math/BigInteger;->divide(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v1

    invoke-virtual {v1}, Ljava/math/BigInteger;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "TB"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_4
    const-string v0, "N/A"

    goto/16 :goto_0
.end method

.method public static getAudioTackSettingOpt(II)Ljava/lang/String;
    .locals 2
    .param p0    # I
    .param p1    # I

    const/4 v1, 0x4

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    if-ltz p0, :cond_1

    if-ge p0, v1, :cond_1

    sget-object v0, Lcom/jrm/localmm/util/Tools;->audioTrackSettingOptTextOne:[Ljava/lang/String;

    aget-object v0, v0, p0

    :goto_0
    return-object v0

    :cond_0
    if-ltz p0, :cond_1

    if-ge p0, v1, :cond_1

    sget-object v0, Lcom/jrm/localmm/util/Tools;->audioTrackSettingOptTextTwo:[Ljava/lang/String;

    aget-object v0, v0, p0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getPlaySettingOpt(II)Ljava/lang/String;
    .locals 3
    .param p0    # I
    .param p1    # I

    const/4 v2, 0x3

    const/4 v0, 0x0

    const/4 v1, 0x1

    if-ne p1, v1, :cond_1

    if-ltz p0, :cond_0

    if-ge p0, v2, :cond_0

    sget-object v1, Lcom/jrm/localmm/util/Tools;->playSettingOptTextOne:[Ljava/lang/String;

    if-eqz v1, :cond_0

    sget-object v0, Lcom/jrm/localmm/util/Tools;->playSettingOptTextOne:[Ljava/lang/String;

    aget-object v0, v0, p0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    if-ltz p0, :cond_0

    if-ge p0, v2, :cond_0

    sget-object v1, Lcom/jrm/localmm/util/Tools;->playSettingOptTextTwo:[Ljava/lang/String;

    if-eqz v1, :cond_0

    sget-object v0, Lcom/jrm/localmm/util/Tools;->playSettingOptTextTwo:[Ljava/lang/String;

    aget-object v0, v0, p0

    goto :goto_0
.end method

.method public static getSubtitleLanguage(II)Ljava/lang/String;
    .locals 2
    .param p0    # I
    .param p1    # I

    const/4 v1, 0x1

    if-ne p1, v1, :cond_0

    sget-object v1, Lcom/jrm/localmm/util/Tools;->subtitleLanguageTypeOne:[Ljava/lang/String;

    if-eqz v1, :cond_1

    sget-object v1, Lcom/jrm/localmm/util/Tools;->subtitleLanguageTypeOne:[Ljava/lang/String;

    array-length v0, v1

    if-ge p0, v0, :cond_1

    if-ltz p0, :cond_1

    sget-object v1, Lcom/jrm/localmm/util/Tools;->subtitleLanguageTypeOne:[Ljava/lang/String;

    aget-object v1, v1, p0

    :goto_0
    return-object v1

    :cond_0
    sget-object v1, Lcom/jrm/localmm/util/Tools;->subtitleLanguageTypeOne:[Ljava/lang/String;

    if-eqz v1, :cond_1

    sget-object v1, Lcom/jrm/localmm/util/Tools;->subtitleLanguageTypeOne:[Ljava/lang/String;

    array-length v0, v1

    if-ge p0, v0, :cond_1

    if-ltz p0, :cond_1

    sget-object v1, Lcom/jrm/localmm/util/Tools;->subtitleLanguageTypeOne:[Ljava/lang/String;

    aget-object v1, v1, p0

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static getSubtitleLanguageTypes(I)[Ljava/lang/String;
    .locals 1
    .param p0    # I

    const/4 v0, 0x1

    if-ne p0, v0, :cond_0

    sget-object v0, Lcom/jrm/localmm/util/Tools;->subtitleLanguageTypeOne:[Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/jrm/localmm/util/Tools;->subtitleLanguageTypeTwo:[Ljava/lang/String;

    goto :goto_0
.end method

.method public static getSubtitleSettingOptValue(II)Ljava/lang/String;
    .locals 2
    .param p0    # I
    .param p1    # I

    const/16 v1, 0xa

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    if-ltz p0, :cond_1

    if-ge p0, v1, :cond_1

    sget-object v0, Lcom/jrm/localmm/util/Tools;->subTitleSettingOptTextOne:[Ljava/lang/String;

    aget-object v0, v0, p0

    :goto_0
    return-object v0

    :cond_0
    if-ltz p0, :cond_1

    if-ge p0, v1, :cond_1

    sget-object v0, Lcom/jrm/localmm/util/Tools;->subTitleSettingOptTextTwo:[Ljava/lang/String;

    aget-object v0, v0, p0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getUSBMountedPath()Ljava/lang/String;
    .locals 1

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static initAudioTackSettingOpt(Landroid/content/Context;I)[Ljava/lang/String;
    .locals 2
    .param p0    # Landroid/content/Context;
    .param p1    # I

    const v1, 0x7f050009    # com.jrm.localmm.R.array.audio_track_setting_opt

    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    sget-object v0, Lcom/jrm/localmm/util/Tools;->audioTrackSettingOptTextOne:[Ljava/lang/String;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jrm/localmm/util/Tools;->audioTrackSettingOptTextOne:[Ljava/lang/String;

    :cond_0
    sget-object v0, Lcom/jrm/localmm/util/Tools;->audioTrackSettingOptTextOne:[Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_1
    sget-object v0, Lcom/jrm/localmm/util/Tools;->audioTrackSettingOptTextTwo:[Ljava/lang/String;

    if-nez v0, :cond_2

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jrm/localmm/util/Tools;->audioTrackSettingOptTextTwo:[Ljava/lang/String;

    :cond_2
    sget-object v0, Lcom/jrm/localmm/util/Tools;->audioTrackSettingOptTextTwo:[Ljava/lang/String;

    goto :goto_0
.end method

.method public static initPlaySettingOpt(Landroid/content/Context;I)[Ljava/lang/String;
    .locals 2
    .param p0    # Landroid/content/Context;
    .param p1    # I

    const v1, 0x7f050008    # com.jrm.localmm.R.array.play_setting_opt

    sget-object v0, Lcom/jrm/localmm/util/Tools;->playSettingOptTextOne:[Ljava/lang/String;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jrm/localmm/util/Tools;->playSettingOptTextOne:[Ljava/lang/String;

    :cond_0
    sget-object v0, Lcom/jrm/localmm/util/Tools;->playSettingOptTextTwo:[Ljava/lang/String;

    if-nez v0, :cond_1

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jrm/localmm/util/Tools;->playSettingOptTextTwo:[Ljava/lang/String;

    :cond_1
    const/4 v0, 0x1

    if-ne p1, v0, :cond_2

    sget-object v0, Lcom/jrm/localmm/util/Tools;->playSettingOptTextOne:[Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_2
    sget-object v0, Lcom/jrm/localmm/util/Tools;->playSettingOptTextTwo:[Ljava/lang/String;

    goto :goto_0
.end method

.method public static initSubtitleSettingOpt(Landroid/content/Context;I)[Ljava/lang/String;
    .locals 2
    .param p0    # Landroid/content/Context;
    .param p1    # I

    const v1, 0x7f050007    # com.jrm.localmm.R.array.subtitle_setting_opt

    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    sget-object v0, Lcom/jrm/localmm/util/Tools;->subTitleSettingOptTextOne:[Ljava/lang/String;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jrm/localmm/util/Tools;->subTitleSettingOptTextOne:[Ljava/lang/String;

    :cond_0
    sget-object v0, Lcom/jrm/localmm/util/Tools;->subTitleSettingOptTextOne:[Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_1
    sget-object v0, Lcom/jrm/localmm/util/Tools;->subTitleSettingOptTextTwo:[Ljava/lang/String;

    if-nez v0, :cond_2

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/jrm/localmm/util/Tools;->subTitleSettingOptTextTwo:[Ljava/lang/String;

    :cond_2
    sget-object v0, Lcom/jrm/localmm/util/Tools;->subTitleSettingOptTextTwo:[Ljava/lang/String;

    goto :goto_0
.end method

.method public static isFileExist(Ljava/io/File;)Z
    .locals 1
    .param p0    # Ljava/io/File;

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Ljava/io/File;->exists()Z

    move-result v0

    goto :goto_0
.end method

.method public static isFileExist(Ljava/lang/String;)Z
    .locals 1
    .param p0    # Ljava/lang/String;

    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/jrm/localmm/util/Tools;->isFileExist(Ljava/io/File;)Z

    move-result v0

    return v0
.end method

.method public static isMediaFile(I)Z
    .locals 1
    .param p0    # I

    const/4 v0, 0x2

    if-eq v0, p0, :cond_0

    const/4 v0, 0x3

    if-eq v0, p0, :cond_0

    const/4 v0, 0x4

    if-ne v0, p0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isNetWorkConnected(Landroid/content/Context;)Z
    .locals 9
    .param p0    # Landroid/content/Context;

    const/4 v6, 0x1

    const/4 v5, 0x0

    :try_start_0
    const-string v7, "connectivity"

    invoke-virtual {p0, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v4

    if-nez v4, :cond_1

    :cond_0
    :goto_0
    return v5

    :cond_1
    invoke-virtual {v4}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v2

    invoke-virtual {v4}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v7

    sget-object v8, Landroid/net/NetworkInfo$State;->CONNECTED:Landroid/net/NetworkInfo$State;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-ne v7, v8, :cond_2

    move v3, v6

    :goto_1
    if-eqz v2, :cond_0

    if-eqz v3, :cond_0

    move v5, v6

    goto :goto_0

    :cond_2
    move v3, v5

    goto :goto_1

    :catch_0
    move-exception v1

    const-string v6, "Tools"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "network exception."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static setAudioTackSettingOpt(Landroid/content/Context;ILjava/lang/String;I)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # I

    const v2, 0x7f0600c5    # com.jrm.localmm.R.string.video_size_unknown

    const/4 v1, 0x4

    const/4 v0, 0x1

    if-ne p3, v0, :cond_2

    if-ltz p1, :cond_0

    if-ge p1, v1, :cond_0

    if-eqz p2, :cond_1

    sget-object v0, Lcom/jrm/localmm/util/Tools;->audioTrackSettingOptTextOne:[Ljava/lang/String;

    aput-object p2, v0, p1

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v0, Lcom/jrm/localmm/util/Tools;->audioTrackSettingOptTextOne:[Ljava/lang/String;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, p1

    goto :goto_0

    :cond_2
    if-ltz p1, :cond_0

    if-ge p1, v1, :cond_0

    if-eqz p2, :cond_3

    sget-object v0, Lcom/jrm/localmm/util/Tools;->audioTrackSettingOptTextTwo:[Ljava/lang/String;

    aput-object p2, v0, p1

    goto :goto_0

    :cond_3
    sget-object v0, Lcom/jrm/localmm/util/Tools;->audioTrackSettingOptTextTwo:[Ljava/lang/String;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, p1

    goto :goto_0
.end method

.method public static setPlaySettingOpt(ILjava/lang/String;I)V
    .locals 2
    .param p0    # I
    .param p1    # Ljava/lang/String;
    .param p2    # I

    const/4 v1, 0x2

    const/4 v0, 0x1

    if-ne p2, v0, :cond_2

    if-ltz p0, :cond_0

    if-ge p0, v1, :cond_0

    sget-object v0, Lcom/jrm/localmm/util/Tools;->playSettingOptTextOne:[Ljava/lang/String;

    aput-object p1, v0, p0

    :cond_0
    :goto_0
    if-ne p0, v1, :cond_1

    sget-object v0, Lcom/jrm/localmm/util/Tools;->playSettingOptTextOne:[Ljava/lang/String;

    aput-object p1, v0, p0

    sget-object v0, Lcom/jrm/localmm/util/Tools;->playSettingOptTextTwo:[Ljava/lang/String;

    aput-object p1, v0, p0

    :cond_1
    return-void

    :cond_2
    if-ltz p0, :cond_0

    if-ge p0, v1, :cond_0

    sget-object v0, Lcom/jrm/localmm/util/Tools;->playSettingOptTextTwo:[Ljava/lang/String;

    aput-object p1, v0, p0

    goto :goto_0
.end method

.method public static setSubtitleLanguageType([Ljava/lang/String;I)V
    .locals 1
    .param p0    # [Ljava/lang/String;
    .param p1    # I

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    sput-object p0, Lcom/jrm/localmm/util/Tools;->subtitleLanguageTypeOne:[Ljava/lang/String;

    :goto_0
    return-void

    :cond_0
    sput-object p0, Lcom/jrm/localmm/util/Tools;->subtitleLanguageTypeTwo:[Ljava/lang/String;

    goto :goto_0
.end method

.method public static setSubtitleSettingOpt(ILjava/lang/String;I)V
    .locals 2
    .param p0    # I
    .param p1    # Ljava/lang/String;
    .param p2    # I

    const/16 v1, 0xa

    const/4 v0, 0x1

    if-ne p2, v0, :cond_1

    if-ltz p0, :cond_0

    if-ge p0, v1, :cond_0

    sget-object v0, Lcom/jrm/localmm/util/Tools;->subTitleSettingOptTextOne:[Ljava/lang/String;

    aput-object p1, v0, p0

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-ltz p0, :cond_0

    if-ge p0, v1, :cond_0

    sget-object v0, Lcom/jrm/localmm/util/Tools;->subTitleSettingOptTextTwo:[Ljava/lang/String;

    aput-object p1, v0, p0

    goto :goto_0
.end method

.method public static startMediascanner(Landroid/content/Context;)V
    .locals 3
    .param p0    # Landroid/content/Context;

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "action_media_scanner_start"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    const-string v1, "Tools"

    const-string v2, "startMediascanner"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public static stopMediascanner(Landroid/content/Context;)V
    .locals 3
    .param p0    # Landroid/content/Context;

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "action_media_scanner_stop"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    const-string v1, "Tools"

    const-string v2, "stopMediascanner"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method
