.class public Lcom/jrm/localmm/util/VerticalSeekBar;
.super Landroid/widget/SeekBar;
.source "VerticalSeekBar.java"


# instance fields
.field private bottom:I

.field private height:I

.field private left:I

.field private mHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

.field private right:I

.field private top:I

.field private width:I

.field private xPos:F

.field private yPos:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1    # Landroid/content/Context;

    const/high16 v2, -0x40800000    # -1.0f

    const/4 v1, -0x1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/widget/SeekBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/16 v0, 0x96

    iput v0, p0, Lcom/jrm/localmm/util/VerticalSeekBar;->height:I

    const/16 v0, 0x1d

    iput v0, p0, Lcom/jrm/localmm/util/VerticalSeekBar;->width:I

    iput v2, p0, Lcom/jrm/localmm/util/VerticalSeekBar;->xPos:F

    iput v2, p0, Lcom/jrm/localmm/util/VerticalSeekBar;->yPos:F

    iput v1, p0, Lcom/jrm/localmm/util/VerticalSeekBar;->top:I

    iput v1, p0, Lcom/jrm/localmm/util/VerticalSeekBar;->bottom:I

    iput v1, p0, Lcom/jrm/localmm/util/VerticalSeekBar;->left:I

    iput v1, p0, Lcom/jrm/localmm/util/VerticalSeekBar;->right:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/high16 v2, -0x40800000    # -1.0f

    const/4 v1, -0x1

    const v0, 0x101007b    # android.R.attr.seekBarStyle

    invoke-direct {p0, p1, p2, v0}, Landroid/widget/SeekBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/16 v0, 0x96

    iput v0, p0, Lcom/jrm/localmm/util/VerticalSeekBar;->height:I

    const/16 v0, 0x1d

    iput v0, p0, Lcom/jrm/localmm/util/VerticalSeekBar;->width:I

    iput v2, p0, Lcom/jrm/localmm/util/VerticalSeekBar;->xPos:F

    iput v2, p0, Lcom/jrm/localmm/util/VerticalSeekBar;->yPos:F

    iput v1, p0, Lcom/jrm/localmm/util/VerticalSeekBar;->top:I

    iput v1, p0, Lcom/jrm/localmm/util/VerticalSeekBar;->bottom:I

    iput v1, p0, Lcom/jrm/localmm/util/VerticalSeekBar;->left:I

    iput v1, p0, Lcom/jrm/localmm/util/VerticalSeekBar;->right:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/high16 v2, -0x40800000    # -1.0f

    const/4 v1, -0x1

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/SeekBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/16 v0, 0x96

    iput v0, p0, Lcom/jrm/localmm/util/VerticalSeekBar;->height:I

    const/16 v0, 0x1d

    iput v0, p0, Lcom/jrm/localmm/util/VerticalSeekBar;->width:I

    iput v2, p0, Lcom/jrm/localmm/util/VerticalSeekBar;->xPos:F

    iput v2, p0, Lcom/jrm/localmm/util/VerticalSeekBar;->yPos:F

    iput v1, p0, Lcom/jrm/localmm/util/VerticalSeekBar;->top:I

    iput v1, p0, Lcom/jrm/localmm/util/VerticalSeekBar;->bottom:I

    iput v1, p0, Lcom/jrm/localmm/util/VerticalSeekBar;->left:I

    iput v1, p0, Lcom/jrm/localmm/util/VerticalSeekBar;->right:I

    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 2
    .param p1    # Landroid/graphics/Canvas;

    const/high16 v0, -0x3d4c0000    # -90.0f

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->rotate(F)V

    iget v0, p0, Lcom/jrm/localmm/util/VerticalSeekBar;->height:I

    neg-int v0, v0

    int-to-float v0, v0

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    invoke-super {p0, p1}, Landroid/widget/SeekBar;->onDraw(Landroid/graphics/Canvas;)V

    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/util/VerticalSeekBar;->setMax(I)V

    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 0
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    invoke-super/range {p0 .. p5}, Landroid/widget/SeekBar;->onLayout(ZIIII)V

    iput p2, p0, Lcom/jrm/localmm/util/VerticalSeekBar;->left:I

    iput p4, p0, Lcom/jrm/localmm/util/VerticalSeekBar;->right:I

    iput p3, p0, Lcom/jrm/localmm/util/VerticalSeekBar;->top:I

    iput p5, p0, Lcom/jrm/localmm/util/VerticalSeekBar;->bottom:I

    return-void
.end method

.method protected declared-synchronized onMeasure(II)V
    .locals 3
    .param p1    # I
    .param p2    # I

    monitor-enter p0

    :try_start_0
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    iput v0, p0, Lcom/jrm/localmm/util/VerticalSeekBar;->height:I

    iget v1, p0, Lcom/jrm/localmm/util/VerticalSeekBar;->width:I

    iget v2, p0, Lcom/jrm/localmm/util/VerticalSeekBar;->height:I

    invoke-virtual {p0, v1, v2}, Lcom/jrm/localmm/util/VerticalSeekBar;->setMeasuredDimension(II)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method protected onSizeChanged(IIII)V
    .locals 0
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    invoke-super {p0, p2, p1, p3, p4}, Landroid/widget/SeekBar;->onSizeChanged(IIII)V

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1    # Landroid/view/MotionEvent;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    iput v3, p0, Lcom/jrm/localmm/util/VerticalSeekBar;->xPos:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    invoke-virtual {p0}, Lcom/jrm/localmm/util/VerticalSeekBar;->getTop()I

    move-result v4

    int-to-float v4, v4

    add-float/2addr v3, v4

    iput v3, p0, Lcom/jrm/localmm/util/VerticalSeekBar;->yPos:F

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/jrm/localmm/util/VerticalSeekBar;->getBottom()I

    move-result v3

    int-to-float v3, v3

    iget v4, p0, Lcom/jrm/localmm/util/VerticalSeekBar;->yPos:F

    sub-float/2addr v3, v4

    const/high16 v4, 0x40000000    # 2.0f

    mul-float/2addr v3, v4

    invoke-virtual {p0}, Lcom/jrm/localmm/util/VerticalSeekBar;->getBottom()I

    move-result v4

    invoke-virtual {p0}, Lcom/jrm/localmm/util/VerticalSeekBar;->getTop()I

    move-result v5

    sub-int/2addr v4, v5

    int-to-float v4, v4

    cmpl-float v3, v3, v4

    if-lez v3, :cond_1

    invoke-virtual {p0}, Lcom/jrm/localmm/util/VerticalSeekBar;->getBottom()I

    move-result v3

    int-to-float v3, v3

    iget v4, p0, Lcom/jrm/localmm/util/VerticalSeekBar;->yPos:F

    sub-float/2addr v3, v4

    invoke-virtual {p0}, Lcom/jrm/localmm/util/VerticalSeekBar;->getBottom()I

    move-result v4

    invoke-virtual {p0}, Lcom/jrm/localmm/util/VerticalSeekBar;->getTop()I

    move-result v5

    sub-int/2addr v4, v5

    add-int/lit8 v4, v4, -0x1e

    int-to-float v4, v4

    div-float v2, v3, v4

    :goto_0
    const-string v3, "onTouchEvent"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "xPos= "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/jrm/localmm/util/VerticalSeekBar;->xPos:F

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v3, "onTouchEvent"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "yPos= "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/jrm/localmm/util/VerticalSeekBar;->yPos:F

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v3, "onTouchEvent"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "this.getBottom()= "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/jrm/localmm/util/VerticalSeekBar;->getBottom()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v3, "onTouchEvent"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "this.getTop()= "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/jrm/localmm/util/VerticalSeekBar;->getTop()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v3, "onTouchEvent"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "progress= "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    neg-float v3, v2

    invoke-virtual {p0}, Lcom/jrm/localmm/util/VerticalSeekBar;->getBottom()I

    move-result v4

    invoke-virtual {p0}, Lcom/jrm/localmm/util/VerticalSeekBar;->getTop()I

    move-result v5

    sub-int/2addr v4, v5

    int-to-float v4, v4

    mul-float/2addr v3, v4

    const/high16 v4, 0x41a00000    # 20.0f

    add-float v1, v3, v4

    const-string v3, "onTouchEvent"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "offset= "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    float-to-int v5, v1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/high16 v3, 0x41200000    # 10.0f

    mul-float/2addr v3, v2

    float-to-int v0, v3

    const-string v3, "onTouchEvent"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Progress = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/util/VerticalSeekBar;->setProgress(I)V

    iget-object v3, p0, Lcom/jrm/localmm/util/VerticalSeekBar;->mHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/jrm/localmm/util/VerticalSeekBar;->mHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v3, v0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVoice(I)V

    :cond_0
    const/4 v3, 0x1

    return v3

    :cond_1
    invoke-virtual {p0}, Lcom/jrm/localmm/util/VerticalSeekBar;->getBottom()I

    move-result v3

    int-to-float v3, v3

    iget v4, p0, Lcom/jrm/localmm/util/VerticalSeekBar;->yPos:F

    sub-float/2addr v3, v4

    const/high16 v4, 0x41f00000    # 30.0f

    sub-float/2addr v3, v4

    invoke-virtual {p0}, Lcom/jrm/localmm/util/VerticalSeekBar;->getBottom()I

    move-result v4

    invoke-virtual {p0}, Lcom/jrm/localmm/util/VerticalSeekBar;->getTop()I

    move-result v5

    sub-int/2addr v4, v5

    int-to-float v4, v4

    div-float v2, v3, v4

    goto/16 :goto_0
.end method

.method public setHolder(Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;)V
    .locals 0
    .param p1    # Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    iput-object p1, p0, Lcom/jrm/localmm/util/VerticalSeekBar;->mHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    return-void
.end method
