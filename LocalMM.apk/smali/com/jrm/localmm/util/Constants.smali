.class public Lcom/jrm/localmm/util/Constants;
.super Ljava/lang/Object;
.source "Constants.java"


# static fields
.field public static aFlag:Z

.field public static abFlag:Z

.field public static isExit:Z

.field public static mVideoSubtitleNo:I

.field public static subTitleSettingName:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-boolean v0, Lcom/jrm/localmm/util/Constants;->abFlag:Z

    sput-boolean v0, Lcom/jrm/localmm/util/Constants;->aFlag:Z

    sput v0, Lcom/jrm/localmm/util/Constants;->mVideoSubtitleNo:I

    const/16 v0, 0xa

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/jrm/localmm/util/Constants;->subTitleSettingName:[I

    const/4 v0, 0x1

    sput-boolean v0, Lcom/jrm/localmm/util/Constants;->isExit:Z

    return-void

    :array_0
    .array-data 4
        0x7f060079    # com.jrm.localmm.R.string.subtitle_type
        0x7f06007a    # com.jrm.localmm.R.string.subtitle_out_path
        0x7f06007b    # com.jrm.localmm.R.string.subtitle_in_no
        0x7f06007c    # com.jrm.localmm.R.string.subtitle_language
        0x7f06007e    # com.jrm.localmm.R.string.subtitle_font
        0x7f06007f    # com.jrm.localmm.R.string.subtitle_font_size
        0x7f06007d    # com.jrm.localmm.R.string.subtitle_color
        0x7f060080    # com.jrm.localmm.R.string.subtitle_frame
        0x7f060081    # com.jrm.localmm.R.string.subtitle_area
        0x7f060082    # com.jrm.localmm.R.string.subtitle_adjust
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
