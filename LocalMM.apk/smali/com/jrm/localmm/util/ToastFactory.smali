.class public Lcom/jrm/localmm/util/ToastFactory;
.super Ljava/lang/Object;
.source "ToastFactory.java"


# static fields
.field private static context:Landroid/content/Context;

.field private static toast:Landroid/widget/Toast;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/jrm/localmm/util/ToastFactory;->context:Landroid/content/Context;

    sput-object v0, Lcom/jrm/localmm/util/ToastFactory;->toast:Landroid/widget/Toast;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getToast(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # I

    const/4 v2, 0x0

    sget-object v0, Lcom/jrm/localmm/util/ToastFactory;->context:Landroid/content/Context;

    if-ne v0, p0, :cond_0

    sget-object v0, Lcom/jrm/localmm/util/ToastFactory;->toast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "not create"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :goto_0
    invoke-static {p0, p1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    sput-object v0, Lcom/jrm/localmm/util/ToastFactory;->toast:Landroid/widget/Toast;

    sget-object v0, Lcom/jrm/localmm/util/ToastFactory;->toast:Landroid/widget/Toast;

    invoke-virtual {v0, p2, v2, v2}, Landroid/widget/Toast;->setGravity(III)V

    sget-object v0, Lcom/jrm/localmm/util/ToastFactory;->toast:Landroid/widget/Toast;

    return-object v0

    :cond_0
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "create toast"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    sput-object p0, Lcom/jrm/localmm/util/ToastFactory;->context:Landroid/content/Context;

    goto :goto_0
.end method
