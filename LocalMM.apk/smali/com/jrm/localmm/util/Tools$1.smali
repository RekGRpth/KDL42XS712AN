.class final Lcom/jrm/localmm/util/Tools$1;
.super Ljava/lang/Object;
.source "Tools.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/jrm/localmm/util/Tools;->changeSource(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$isEnter:Z


# direct methods
.method constructor <init>(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/jrm/localmm/util/Tools$1;->val$isEnter:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    # getter for: Lcom/jrm/localmm/util/Tools;->appSkin:Lcom/mstar/android/tv/TvCommonManager;
    invoke-static {}, Lcom/jrm/localmm/util/Tools;->access$000()Lcom/mstar/android/tv/TvCommonManager;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tv/TvCommonManager;->getInstance()Lcom/mstar/android/tv/TvCommonManager;

    move-result-object v0

    # setter for: Lcom/jrm/localmm/util/Tools;->appSkin:Lcom/mstar/android/tv/TvCommonManager;
    invoke-static {v0}, Lcom/jrm/localmm/util/Tools;->access$002(Lcom/mstar/android/tv/TvCommonManager;)Lcom/mstar/android/tv/TvCommonManager;

    :cond_0
    # getter for: Lcom/jrm/localmm/util/Tools;->appSkin:Lcom/mstar/android/tv/TvCommonManager;
    invoke-static {}, Lcom/jrm/localmm/util/Tools;->access$000()Lcom/mstar/android/tv/TvCommonManager;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/jrm/localmm/util/Tools$1;->val$isEnter:Z

    if-eqz v0, :cond_2

    # getter for: Lcom/jrm/localmm/util/Tools;->appSkin:Lcom/mstar/android/tv/TvCommonManager;
    invoke-static {}, Lcom/jrm/localmm/util/Tools;->access$000()Lcom/mstar/android/tv/TvCommonManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tv/TvCommonManager;->getCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v0

    # setter for: Lcom/jrm/localmm/util/Tools;->inputSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    invoke-static {v0}, Lcom/jrm/localmm/util/Tools;->access$102(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    # getter for: Lcom/jrm/localmm/util/Tools;->inputSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    invoke-static {}, Lcom/jrm/localmm/util/Tools;->access$100()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_STORAGE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-eq v0, v1, :cond_1

    # getter for: Lcom/jrm/localmm/util/Tools;->appSkin:Lcom/mstar/android/tv/TvCommonManager;
    invoke-static {}, Lcom/jrm/localmm/util/Tools;->access$000()Lcom/mstar/android/tv/TvCommonManager;

    move-result-object v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_STORAGE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v0, v1}, Lcom/mstar/android/tv/TvCommonManager;->setInputSource(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    # getter for: Lcom/jrm/localmm/util/Tools;->inputSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    invoke-static {}, Lcom/jrm/localmm/util/Tools;->access$100()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_STORAGE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-eq v0, v1, :cond_1

    # getter for: Lcom/jrm/localmm/util/Tools;->appSkin:Lcom/mstar/android/tv/TvCommonManager;
    invoke-static {}, Lcom/jrm/localmm/util/Tools;->access$000()Lcom/mstar/android/tv/TvCommonManager;

    move-result-object v0

    # getter for: Lcom/jrm/localmm/util/Tools;->inputSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    invoke-static {}, Lcom/jrm/localmm/util/Tools;->access$100()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/mstar/android/tv/TvCommonManager;->setInputSource(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)V

    goto :goto_0
.end method
