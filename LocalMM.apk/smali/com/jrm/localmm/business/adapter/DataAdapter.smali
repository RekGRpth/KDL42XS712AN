.class public Lcom/jrm/localmm/business/adapter/DataAdapter;
.super Landroid/widget/BaseAdapter;
.source "DataAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/jrm/localmm/business/adapter/DataAdapter$1;,
        Lcom/jrm/localmm/business/adapter/DataAdapter$MyOnHoverListener;,
        Lcom/jrm/localmm/business/adapter/DataAdapter$ItemViewHolder;
    }
.end annotation


# instance fields
.field private handler:Landroid/os/Handler;

.field private itemHeight:I

.field private layoutInflater:Landroid/view/LayoutInflater;

.field private list:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/jrm/localmm/business/data/BaseData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/os/Handler;Ljava/util/List;)V
    .locals 1
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/os/Handler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Landroid/os/Handler;",
            "Ljava/util/List",
            "<",
            "Lcom/jrm/localmm/business/data/BaseData;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/jrm/localmm/business/adapter/DataAdapter;->itemHeight:I

    if-eqz p3, :cond_0

    iput-object p3, p0, Lcom/jrm/localmm/business/adapter/DataAdapter;->list:Ljava/util/List;

    :goto_0
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/jrm/localmm/business/adapter/DataAdapter;->layoutInflater:Landroid/view/LayoutInflater;

    iput-object p2, p0, Lcom/jrm/localmm/business/adapter/DataAdapter;->handler:Landroid/os/Handler;

    return-void

    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/jrm/localmm/business/adapter/DataAdapter;->list:Ljava/util/List;

    goto :goto_0
.end method

.method static synthetic access$100(Lcom/jrm/localmm/business/adapter/DataAdapter;)I
    .locals 1
    .param p0    # Lcom/jrm/localmm/business/adapter/DataAdapter;

    iget v0, p0, Lcom/jrm/localmm/business/adapter/DataAdapter;->itemHeight:I

    return v0
.end method

.method static synthetic access$102(Lcom/jrm/localmm/business/adapter/DataAdapter;I)I
    .locals 0
    .param p0    # Lcom/jrm/localmm/business/adapter/DataAdapter;
    .param p1    # I

    iput p1, p0, Lcom/jrm/localmm/business/adapter/DataAdapter;->itemHeight:I

    return p1
.end method

.method static synthetic access$200(Lcom/jrm/localmm/business/adapter/DataAdapter;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/jrm/localmm/business/adapter/DataAdapter;

    iget-object v0, p0, Lcom/jrm/localmm/business/adapter/DataAdapter;->handler:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/jrm/localmm/business/adapter/DataAdapter;->list:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/jrm/localmm/business/adapter/DataAdapter;->list:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1    # I

    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    const/4 v6, 0x0

    const/4 v5, 0x6

    const/4 v0, 0x0

    if-nez p2, :cond_1

    iget-object v3, p0, Lcom/jrm/localmm/business/adapter/DataAdapter;->layoutInflater:Landroid/view/LayoutInflater;

    const v4, 0x7f030013    # com.jrm.localmm.R.layout.radio_selection_mode_list_item

    invoke-virtual {v3, v4, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    new-instance v3, Lcom/jrm/localmm/business/adapter/DataAdapter$MyOnHoverListener;

    invoke-direct {v3, p0}, Lcom/jrm/localmm/business/adapter/DataAdapter$MyOnHoverListener;-><init>(Lcom/jrm/localmm/business/adapter/DataAdapter;)V

    invoke-virtual {p2, v3}, Landroid/view/View;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    new-instance v0, Lcom/jrm/localmm/business/adapter/DataAdapter$ItemViewHolder;

    invoke-direct {v0, v6}, Lcom/jrm/localmm/business/adapter/DataAdapter$ItemViewHolder;-><init>(Lcom/jrm/localmm/business/adapter/DataAdapter$1;)V

    const/high16 v3, 0x7f090000    # com.jrm.localmm.R.radio_selection_mode_list_item.iconImage

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, v0, Lcom/jrm/localmm/business/adapter/DataAdapter$ItemViewHolder;->iconImage:Landroid/widget/ImageView;

    const v3, 0x7f090001    # com.jrm.localmm.R.radio_selection_mode_list_item.nameText

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v0, Lcom/jrm/localmm/business/adapter/DataAdapter$ItemViewHolder;->nameText:Landroid/widget/TextView;

    const v3, 0x7f090002    # com.jrm.localmm.R.radio_selection_mode_list_item.formatText

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v0, Lcom/jrm/localmm/business/adapter/DataAdapter$ItemViewHolder;->formatText:Landroid/widget/TextView;

    const v3, 0x7f090003    # com.jrm.localmm.R.radio_selection_mode_list_item.descriptionText

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v0, Lcom/jrm/localmm/business/adapter/DataAdapter$ItemViewHolder;->descriptionText:Landroid/widget/TextView;

    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :goto_0
    iget-object v3, p0, Lcom/jrm/localmm/business/adapter/DataAdapter;->list:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jrm/localmm/business/data/BaseData;

    if-eqz v1, :cond_0

    iget-object v3, v0, Lcom/jrm/localmm/business/adapter/DataAdapter$ItemViewHolder;->iconImage:Landroid/widget/ImageView;

    invoke-virtual {v1}, Lcom/jrm/localmm/business/data/BaseData;->getIcon()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    invoke-virtual {v1}, Lcom/jrm/localmm/business/data/BaseData;->getType()I

    move-result v2

    if-ne v5, v2, :cond_2

    iget-object v3, v0, Lcom/jrm/localmm/business/adapter/DataAdapter$ItemViewHolder;->nameText:Landroid/widget/TextView;

    const v4, 0x7f060002    # com.jrm.localmm.R.string.back

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    :goto_1
    const/4 v3, 0x5

    if-ne v3, v2, :cond_3

    iget-object v3, v0, Lcom/jrm/localmm/business/adapter/DataAdapter$ItemViewHolder;->formatText:Landroid/widget/TextView;

    const v4, 0x7f060003    # com.jrm.localmm.R.string.folder

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    :goto_2
    invoke-virtual {v1}, Lcom/jrm/localmm/business/data/BaseData;->getDescription()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_4

    if-eq v5, v2, :cond_4

    iget-object v3, v0, Lcom/jrm/localmm/business/adapter/DataAdapter$ItemViewHolder;->descriptionText:Landroid/widget/TextView;

    invoke-virtual {v1}, Lcom/jrm/localmm/business/data/BaseData;->getDescription()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_3
    const/16 v3, 0x9

    if-ne p1, v3, :cond_0

    invoke-virtual {p2}, Landroid/view/View;->getY()F

    move-result v3

    float-to-int v3, v3

    div-int/lit8 v3, v3, 0x9

    iput v3, p0, Lcom/jrm/localmm/business/adapter/DataAdapter;->itemHeight:I

    :cond_0
    return-object p2

    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jrm/localmm/business/adapter/DataAdapter$ItemViewHolder;

    goto :goto_0

    :cond_2
    iget-object v3, v0, Lcom/jrm/localmm/business/adapter/DataAdapter$ItemViewHolder;->nameText:Landroid/widget/TextView;

    invoke-virtual {v1}, Lcom/jrm/localmm/business/data/BaseData;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_3
    iget-object v3, v0, Lcom/jrm/localmm/business/adapter/DataAdapter$ItemViewHolder;->formatText:Landroid/widget/TextView;

    invoke-virtual {v1}, Lcom/jrm/localmm/business/data/BaseData;->getFormat()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    :cond_4
    iget-object v3, v0, Lcom/jrm/localmm/business/adapter/DataAdapter$ItemViewHolder;->descriptionText:Landroid/widget/TextView;

    const-string v4, ""

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3
.end method
