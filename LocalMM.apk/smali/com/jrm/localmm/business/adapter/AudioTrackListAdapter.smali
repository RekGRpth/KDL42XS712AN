.class public Lcom/jrm/localmm/business/adapter/AudioTrackListAdapter;
.super Landroid/widget/BaseAdapter;
.source "AudioTrackListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/jrm/localmm/business/adapter/AudioTrackListAdapter$ViewHolder;
    }
.end annotation


# instance fields
.field private context:Landroid/content/Context;

.field private count:I

.field private holder:Lcom/jrm/localmm/business/adapter/AudioTrackListAdapter$ViewHolder;

.field private mInflater:Landroid/view/LayoutInflater;

.field private optionData:[Ljava/lang/String;

.field private settingData:[I

.field private videoPlayActivity:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;


# direct methods
.method public constructor <init>(Landroid/content/Context;[I[Ljava/lang/String;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # [I
    .param p3    # [Ljava/lang/String;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    :try_start_0
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/jrm/localmm/business/adapter/AudioTrackListAdapter;->mInflater:Landroid/view/LayoutInflater;

    iput-object p1, p0, Lcom/jrm/localmm/business/adapter/AudioTrackListAdapter;->context:Landroid/content/Context;

    iput-object p2, p0, Lcom/jrm/localmm/business/adapter/AudioTrackListAdapter;->settingData:[I

    iput-object p3, p0, Lcom/jrm/localmm/business/adapter/AudioTrackListAdapter;->optionData:[Ljava/lang/String;

    check-cast p1, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    iput-object p1, p0, Lcom/jrm/localmm/business/adapter/AudioTrackListAdapter;->videoPlayActivity:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    iget-object v0, p0, Lcom/jrm/localmm/business/adapter/AudioTrackListAdapter;->videoPlayActivity:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getAudioTrackCount()I

    move-result v0

    iput v0, p0, Lcom/jrm/localmm/business/adapter/AudioTrackListAdapter;->count:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/jrm/localmm/business/adapter/AudioTrackListAdapter;->settingData:[I

    array-length v0, v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1    # I

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1    # I

    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    const v7, 0x7f080092    # com.jrm.localmm.R.id.rightImageView

    const v6, 0x7f080091    # com.jrm.localmm.R.id.settingOptionTextView

    const v5, 0x7f080090    # com.jrm.localmm.R.id.settingNameTextView

    const v4, 0x7f080033    # com.jrm.localmm.R.id.leftImageView

    const/4 v3, 0x4

    if-nez p2, :cond_2

    iget-object v0, p0, Lcom/jrm/localmm/business/adapter/AudioTrackListAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f030017    # com.jrm.localmm.R.layout.subtitle_setting_list_item

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    new-instance v0, Lcom/jrm/localmm/business/adapter/AudioTrackListAdapter$ViewHolder;

    invoke-direct {v0}, Lcom/jrm/localmm/business/adapter/AudioTrackListAdapter$ViewHolder;-><init>()V

    iput-object v0, p0, Lcom/jrm/localmm/business/adapter/AudioTrackListAdapter;->holder:Lcom/jrm/localmm/business/adapter/AudioTrackListAdapter$ViewHolder;

    iget-object v1, p0, Lcom/jrm/localmm/business/adapter/AudioTrackListAdapter;->holder:Lcom/jrm/localmm/business/adapter/AudioTrackListAdapter$ViewHolder;

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/jrm/localmm/business/adapter/AudioTrackListAdapter$ViewHolder;->settingNameTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/jrm/localmm/business/adapter/AudioTrackListAdapter;->holder:Lcom/jrm/localmm/business/adapter/AudioTrackListAdapter$ViewHolder;

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lcom/jrm/localmm/business/adapter/AudioTrackListAdapter$ViewHolder;->leftImageView:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/jrm/localmm/business/adapter/AudioTrackListAdapter;->holder:Lcom/jrm/localmm/business/adapter/AudioTrackListAdapter$ViewHolder;

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/jrm/localmm/business/adapter/AudioTrackListAdapter$ViewHolder;->settingOptionTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/jrm/localmm/business/adapter/AudioTrackListAdapter;->holder:Lcom/jrm/localmm/business/adapter/AudioTrackListAdapter$ViewHolder;

    invoke-virtual {p2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lcom/jrm/localmm/business/adapter/AudioTrackListAdapter$ViewHolder;->rightImageView:Landroid/widget/ImageView;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/business/adapter/AudioTrackListAdapter;->holder:Lcom/jrm/localmm/business/adapter/AudioTrackListAdapter$ViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/business/adapter/AudioTrackListAdapter$ViewHolder;->leftImageView:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/jrm/localmm/business/adapter/AudioTrackListAdapter;->holder:Lcom/jrm/localmm/business/adapter/AudioTrackListAdapter$ViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/business/adapter/AudioTrackListAdapter$ViewHolder;->rightImageView:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_0
    iget-object v0, p0, Lcom/jrm/localmm/business/adapter/AudioTrackListAdapter;->holder:Lcom/jrm/localmm/business/adapter/AudioTrackListAdapter$ViewHolder;

    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :goto_0
    iget-object v0, p0, Lcom/jrm/localmm/business/adapter/AudioTrackListAdapter;->holder:Lcom/jrm/localmm/business/adapter/AudioTrackListAdapter$ViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/business/adapter/AudioTrackListAdapter$ViewHolder;->settingNameTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/jrm/localmm/business/adapter/AudioTrackListAdapter;->context:Landroid/content/Context;

    iget-object v2, p0, Lcom/jrm/localmm/business/adapter/AudioTrackListAdapter;->settingData:[I

    aget v2, v2, p1

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/jrm/localmm/business/adapter/AudioTrackListAdapter;->holder:Lcom/jrm/localmm/business/adapter/AudioTrackListAdapter$ViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/business/adapter/AudioTrackListAdapter$ViewHolder;->settingOptionTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/jrm/localmm/business/adapter/AudioTrackListAdapter;->optionData:[Ljava/lang/String;

    aget-object v1, v1, p1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    if-nez p1, :cond_1

    iget v0, p0, Lcom/jrm/localmm/business/adapter/AudioTrackListAdapter;->count:I

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/jrm/localmm/business/adapter/AudioTrackListAdapter;->holder:Lcom/jrm/localmm/business/adapter/AudioTrackListAdapter$ViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/business/adapter/AudioTrackListAdapter$ViewHolder;->settingOptionTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/jrm/localmm/business/adapter/AudioTrackListAdapter;->videoPlayActivity:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f060058    # com.jrm.localmm.R.string.no_video_track

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    return-object p2

    :cond_2
    iget-object v1, p0, Lcom/jrm/localmm/business/adapter/AudioTrackListAdapter;->holder:Lcom/jrm/localmm/business/adapter/AudioTrackListAdapter$ViewHolder;

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/jrm/localmm/business/adapter/AudioTrackListAdapter$ViewHolder;->settingNameTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/jrm/localmm/business/adapter/AudioTrackListAdapter;->holder:Lcom/jrm/localmm/business/adapter/AudioTrackListAdapter$ViewHolder;

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lcom/jrm/localmm/business/adapter/AudioTrackListAdapter$ViewHolder;->leftImageView:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/jrm/localmm/business/adapter/AudioTrackListAdapter;->holder:Lcom/jrm/localmm/business/adapter/AudioTrackListAdapter$ViewHolder;

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/jrm/localmm/business/adapter/AudioTrackListAdapter$ViewHolder;->settingOptionTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/jrm/localmm/business/adapter/AudioTrackListAdapter;->holder:Lcom/jrm/localmm/business/adapter/AudioTrackListAdapter$ViewHolder;

    invoke-virtual {p2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lcom/jrm/localmm/business/adapter/AudioTrackListAdapter$ViewHolder;->rightImageView:Landroid/widget/ImageView;

    goto :goto_0
.end method
