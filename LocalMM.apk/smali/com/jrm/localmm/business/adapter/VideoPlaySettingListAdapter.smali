.class public Lcom/jrm/localmm/business/adapter/VideoPlaySettingListAdapter;
.super Landroid/widget/BaseAdapter;
.source "VideoPlaySettingListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/jrm/localmm/business/adapter/VideoPlaySettingListAdapter$ViewHolder;
    }
.end annotation


# instance fields
.field private context:Landroid/content/Context;

.field private holder:Lcom/jrm/localmm/business/adapter/VideoPlaySettingListAdapter$ViewHolder;

.field private mInflater:Landroid/view/LayoutInflater;

.field private optionData:[Ljava/lang/String;

.field private settingData:[I


# direct methods
.method public constructor <init>(Landroid/content/Context;[I[Ljava/lang/String;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # [I
    .param p3    # [Ljava/lang/String;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    :try_start_0
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/jrm/localmm/business/adapter/VideoPlaySettingListAdapter;->mInflater:Landroid/view/LayoutInflater;

    iput-object p1, p0, Lcom/jrm/localmm/business/adapter/VideoPlaySettingListAdapter;->context:Landroid/content/Context;

    iput-object p2, p0, Lcom/jrm/localmm/business/adapter/VideoPlaySettingListAdapter;->settingData:[I

    iput-object p3, p0, Lcom/jrm/localmm/business/adapter/VideoPlaySettingListAdapter;->optionData:[Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/jrm/localmm/business/adapter/VideoPlaySettingListAdapter;->settingData:[I

    array-length v0, v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1    # I

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1    # I

    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    const v7, 0x7f080092    # com.jrm.localmm.R.id.rightImageView

    const v6, 0x7f080091    # com.jrm.localmm.R.id.settingOptionTextView

    const v5, 0x7f080090    # com.jrm.localmm.R.id.settingNameTextView

    const v4, 0x7f080033    # com.jrm.localmm.R.id.leftImageView

    const/4 v3, 0x4

    if-nez p2, :cond_1

    iget-object v0, p0, Lcom/jrm/localmm/business/adapter/VideoPlaySettingListAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f030021    # com.jrm.localmm.R.layout.video_play_setting_list_item

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    new-instance v0, Lcom/jrm/localmm/business/adapter/VideoPlaySettingListAdapter$ViewHolder;

    invoke-direct {v0}, Lcom/jrm/localmm/business/adapter/VideoPlaySettingListAdapter$ViewHolder;-><init>()V

    iput-object v0, p0, Lcom/jrm/localmm/business/adapter/VideoPlaySettingListAdapter;->holder:Lcom/jrm/localmm/business/adapter/VideoPlaySettingListAdapter$ViewHolder;

    iget-object v1, p0, Lcom/jrm/localmm/business/adapter/VideoPlaySettingListAdapter;->holder:Lcom/jrm/localmm/business/adapter/VideoPlaySettingListAdapter$ViewHolder;

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/jrm/localmm/business/adapter/VideoPlaySettingListAdapter$ViewHolder;->settingNameTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/jrm/localmm/business/adapter/VideoPlaySettingListAdapter;->holder:Lcom/jrm/localmm/business/adapter/VideoPlaySettingListAdapter$ViewHolder;

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lcom/jrm/localmm/business/adapter/VideoPlaySettingListAdapter$ViewHolder;->leftImageView:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/jrm/localmm/business/adapter/VideoPlaySettingListAdapter;->holder:Lcom/jrm/localmm/business/adapter/VideoPlaySettingListAdapter$ViewHolder;

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/jrm/localmm/business/adapter/VideoPlaySettingListAdapter$ViewHolder;->settingOptionTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/jrm/localmm/business/adapter/VideoPlaySettingListAdapter;->holder:Lcom/jrm/localmm/business/adapter/VideoPlaySettingListAdapter$ViewHolder;

    invoke-virtual {p2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lcom/jrm/localmm/business/adapter/VideoPlaySettingListAdapter$ViewHolder;->rightImageView:Landroid/widget/ImageView;

    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/business/adapter/VideoPlaySettingListAdapter;->holder:Lcom/jrm/localmm/business/adapter/VideoPlaySettingListAdapter$ViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/business/adapter/VideoPlaySettingListAdapter$ViewHolder;->settingOptionTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/jrm/localmm/business/adapter/VideoPlaySettingListAdapter;->holder:Lcom/jrm/localmm/business/adapter/VideoPlaySettingListAdapter$ViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/business/adapter/VideoPlaySettingListAdapter$ViewHolder;->leftImageView:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/jrm/localmm/business/adapter/VideoPlaySettingListAdapter;->holder:Lcom/jrm/localmm/business/adapter/VideoPlaySettingListAdapter$ViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/business/adapter/VideoPlaySettingListAdapter$ViewHolder;->rightImageView:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_0
    iget-object v0, p0, Lcom/jrm/localmm/business/adapter/VideoPlaySettingListAdapter;->holder:Lcom/jrm/localmm/business/adapter/VideoPlaySettingListAdapter$ViewHolder;

    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :goto_0
    iget-object v0, p0, Lcom/jrm/localmm/business/adapter/VideoPlaySettingListAdapter;->holder:Lcom/jrm/localmm/business/adapter/VideoPlaySettingListAdapter$ViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/business/adapter/VideoPlaySettingListAdapter$ViewHolder;->settingNameTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/jrm/localmm/business/adapter/VideoPlaySettingListAdapter;->context:Landroid/content/Context;

    iget-object v2, p0, Lcom/jrm/localmm/business/adapter/VideoPlaySettingListAdapter;->settingData:[I

    aget v2, v2, p1

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/jrm/localmm/business/adapter/VideoPlaySettingListAdapter;->holder:Lcom/jrm/localmm/business/adapter/VideoPlaySettingListAdapter$ViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/business/adapter/VideoPlaySettingListAdapter$ViewHolder;->settingOptionTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/jrm/localmm/business/adapter/VideoPlaySettingListAdapter;->optionData:[Ljava/lang/String;

    aget-object v1, v1, p1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-object p2

    :cond_1
    iget-object v1, p0, Lcom/jrm/localmm/business/adapter/VideoPlaySettingListAdapter;->holder:Lcom/jrm/localmm/business/adapter/VideoPlaySettingListAdapter$ViewHolder;

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/jrm/localmm/business/adapter/VideoPlaySettingListAdapter$ViewHolder;->settingNameTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/jrm/localmm/business/adapter/VideoPlaySettingListAdapter;->holder:Lcom/jrm/localmm/business/adapter/VideoPlaySettingListAdapter$ViewHolder;

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lcom/jrm/localmm/business/adapter/VideoPlaySettingListAdapter$ViewHolder;->leftImageView:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/jrm/localmm/business/adapter/VideoPlaySettingListAdapter;->holder:Lcom/jrm/localmm/business/adapter/VideoPlaySettingListAdapter$ViewHolder;

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/jrm/localmm/business/adapter/VideoPlaySettingListAdapter$ViewHolder;->settingOptionTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/jrm/localmm/business/adapter/VideoPlaySettingListAdapter;->holder:Lcom/jrm/localmm/business/adapter/VideoPlaySettingListAdapter$ViewHolder;

    invoke-virtual {p2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lcom/jrm/localmm/business/adapter/VideoPlaySettingListAdapter$ViewHolder;->rightImageView:Landroid/widget/ImageView;

    goto :goto_0
.end method
