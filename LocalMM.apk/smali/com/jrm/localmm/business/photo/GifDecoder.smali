.class public Lcom/jrm/localmm/business/photo/GifDecoder;
.super Ljava/lang/Object;
.source "GifDecoder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/jrm/localmm/business/photo/GifDecoder$GifFrame;
    }
.end annotation


# instance fields
.field protected act:[I

.field protected bgColor:I

.field protected bgIndex:I

.field protected block:[B

.field protected blockSize:I

.field protected delay:I

.field protected dispose:I

.field protected frameCount:I

.field protected frameindex:I

.field protected frames:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Lcom/jrm/localmm/business/photo/GifDecoder$GifFrame;",
            ">;"
        }
    .end annotation
.end field

.field protected gct:[I

.field protected gctFlag:Z

.field protected gctSize:I

.field protected height:I

.field protected ih:I

.field protected image:Landroid/graphics/Bitmap;

.field protected in:Ljava/io/InputStream;

.field protected interlace:Z

.field protected iw:I

.field protected ix:I

.field protected iy:I

.field protected lastBgColor:I

.field protected lastDispose:I

.field protected lastImage:Landroid/graphics/Bitmap;

.field protected lct:[I

.field protected lctFlag:Z

.field protected lctSize:I

.field protected loopCount:I

.field protected lrh:I

.field protected lrw:I

.field protected lrx:I

.field protected lry:I

.field protected pixelAspect:I

.field protected pixelStack:[B

.field protected pixels:[B

.field protected prefix:[S

.field protected status:I

.field protected suffix:[B

.field protected transIndex:I

.field protected transparency:Z

.field protected width:I


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->loopCount:I

    iput v1, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->frameindex:I

    const/16 v0, 0x100

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->block:[B

    iput v1, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->blockSize:I

    iput v1, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->dispose:I

    iput v1, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->lastDispose:I

    iput-boolean v1, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->transparency:Z

    iput v1, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->delay:I

    return-void
.end method


# virtual methods
.method protected decodeImageData()V
    .locals 25

    const/4 v2, -0x1

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jrm/localmm/business/photo/GifDecoder;->iw:I

    move/from16 v23, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/jrm/localmm/business/photo/GifDecoder;->ih:I

    move/from16 v24, v0

    mul-int v17, v23, v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jrm/localmm/business/photo/GifDecoder;->pixels:[B

    move-object/from16 v23, v0

    if-eqz v23, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jrm/localmm/business/photo/GifDecoder;->pixels:[B

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    array-length v0, v0

    move/from16 v23, v0

    move/from16 v0, v23

    move/from16 v1, v17

    if-ge v0, v1, :cond_1

    :cond_0
    move/from16 v0, v17

    new-array v0, v0, [B

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/jrm/localmm/business/photo/GifDecoder;->pixels:[B

    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jrm/localmm/business/photo/GifDecoder;->prefix:[S

    move-object/from16 v23, v0

    if-nez v23, :cond_2

    const/16 v23, 0x1000

    move/from16 v0, v23

    new-array v0, v0, [S

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/jrm/localmm/business/photo/GifDecoder;->prefix:[S

    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jrm/localmm/business/photo/GifDecoder;->suffix:[B

    move-object/from16 v23, v0

    if-nez v23, :cond_3

    const/16 v23, 0x1000

    move/from16 v0, v23

    new-array v0, v0, [B

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/jrm/localmm/business/photo/GifDecoder;->suffix:[B

    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jrm/localmm/business/photo/GifDecoder;->pixelStack:[B

    move-object/from16 v23, v0

    if-nez v23, :cond_4

    const/16 v23, 0x1001

    move/from16 v0, v23

    new-array v0, v0, [B

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/jrm/localmm/business/photo/GifDecoder;->pixelStack:[B

    :cond_4
    invoke-virtual/range {p0 .. p0}, Lcom/jrm/localmm/business/photo/GifDecoder;->read()I

    move-result v11

    const/16 v23, 0x1

    shl-int v6, v23, v11

    add-int/lit8 v13, v6, 0x1

    add-int/lit8 v3, v6, 0x2

    move/from16 v18, v2

    add-int/lit8 v9, v11, 0x1

    const/16 v23, 0x1

    shl-int v23, v23, v9

    add-int/lit8 v8, v23, -0x1

    const/4 v7, 0x0

    :goto_0
    if-ge v7, v6, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jrm/localmm/business/photo/GifDecoder;->prefix:[S

    move-object/from16 v23, v0

    const/16 v24, 0x0

    aput-short v24, v23, v7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jrm/localmm/business/photo/GifDecoder;->suffix:[B

    move-object/from16 v23, v0

    int-to-byte v0, v7

    move/from16 v24, v0

    aput-byte v24, v23, v7

    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    :cond_5
    const/4 v4, 0x0

    move/from16 v19, v4

    move/from16 v21, v4

    move v14, v4

    move v10, v4

    move v5, v4

    move v12, v4

    const/4 v15, 0x0

    move/from16 v20, v19

    move/from16 v22, v21

    :goto_1
    move/from16 v0, v17

    if-ge v15, v0, :cond_11

    if-nez v22, :cond_12

    if-ge v5, v9, :cond_8

    if-nez v10, :cond_7

    invoke-virtual/range {p0 .. p0}, Lcom/jrm/localmm/business/photo/GifDecoder;->readBlock()I

    move-result v10

    if-gtz v10, :cond_6

    move/from16 v21, v22

    :goto_2
    move/from16 v15, v20

    :goto_3
    move/from16 v0, v17

    if-ge v15, v0, :cond_10

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jrm/localmm/business/photo/GifDecoder;->pixels:[B

    move-object/from16 v23, v0

    const/16 v24, 0x0

    aput-byte v24, v23, v15

    add-int/lit8 v15, v15, 0x1

    goto :goto_3

    :cond_6
    const/4 v4, 0x0

    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jrm/localmm/business/photo/GifDecoder;->block:[B

    move-object/from16 v23, v0

    aget-byte v23, v23, v4

    move/from16 v0, v23

    and-int/lit16 v0, v0, 0xff

    move/from16 v23, v0

    shl-int v23, v23, v5

    add-int v12, v12, v23

    add-int/lit8 v5, v5, 0x8

    add-int/lit8 v4, v4, 0x1

    add-int/lit8 v10, v10, -0x1

    goto :goto_1

    :cond_8
    and-int v7, v12, v8

    shr-int/2addr v12, v9

    sub-int/2addr v5, v9

    if-gt v7, v3, :cond_11

    if-ne v7, v13, :cond_9

    move/from16 v21, v22

    goto :goto_2

    :cond_9
    if-ne v7, v6, :cond_a

    add-int/lit8 v9, v11, 0x1

    const/16 v23, 0x1

    shl-int v23, v23, v9

    add-int/lit8 v8, v23, -0x1

    add-int/lit8 v3, v6, 0x2

    move/from16 v18, v2

    goto :goto_1

    :cond_a
    move/from16 v0, v18

    if-ne v0, v2, :cond_b

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jrm/localmm/business/photo/GifDecoder;->pixelStack:[B

    move-object/from16 v23, v0

    add-int/lit8 v21, v22, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jrm/localmm/business/photo/GifDecoder;->suffix:[B

    move-object/from16 v24, v0

    aget-byte v24, v24, v7

    aput-byte v24, v23, v22

    move/from16 v18, v7

    move v14, v7

    move/from16 v22, v21

    goto :goto_1

    :cond_b
    move/from16 v16, v7

    if-ne v7, v3, :cond_c

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jrm/localmm/business/photo/GifDecoder;->pixelStack:[B

    move-object/from16 v23, v0

    add-int/lit8 v21, v22, 0x1

    int-to-byte v0, v14

    move/from16 v24, v0

    aput-byte v24, v23, v22

    move/from16 v7, v18

    move/from16 v22, v21

    :cond_c
    :goto_4
    if-le v7, v6, :cond_d

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jrm/localmm/business/photo/GifDecoder;->pixelStack:[B

    move-object/from16 v23, v0

    add-int/lit8 v21, v22, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jrm/localmm/business/photo/GifDecoder;->suffix:[B

    move-object/from16 v24, v0

    aget-byte v24, v24, v7

    aput-byte v24, v23, v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jrm/localmm/business/photo/GifDecoder;->prefix:[S

    move-object/from16 v23, v0

    aget-short v7, v23, v7

    move/from16 v22, v21

    goto :goto_4

    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jrm/localmm/business/photo/GifDecoder;->suffix:[B

    move-object/from16 v23, v0

    aget-byte v23, v23, v7

    move/from16 v0, v23

    and-int/lit16 v14, v0, 0xff

    const/16 v23, 0x1000

    move/from16 v0, v23

    if-lt v3, v0, :cond_e

    move/from16 v21, v22

    goto/16 :goto_2

    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jrm/localmm/business/photo/GifDecoder;->pixelStack:[B

    move-object/from16 v23, v0

    add-int/lit8 v21, v22, 0x1

    int-to-byte v0, v14

    move/from16 v24, v0

    aput-byte v24, v23, v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jrm/localmm/business/photo/GifDecoder;->prefix:[S

    move-object/from16 v23, v0

    move/from16 v0, v18

    int-to-short v0, v0

    move/from16 v24, v0

    aput-short v24, v23, v3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jrm/localmm/business/photo/GifDecoder;->suffix:[B

    move-object/from16 v23, v0

    int-to-byte v0, v14

    move/from16 v24, v0

    aput-byte v24, v23, v3

    add-int/lit8 v3, v3, 0x1

    and-int v23, v3, v8

    if-nez v23, :cond_f

    const/16 v23, 0x1000

    move/from16 v0, v23

    if-ge v3, v0, :cond_f

    add-int/lit8 v9, v9, 0x1

    add-int/2addr v8, v3

    :cond_f
    move/from16 v18, v16

    :goto_5
    add-int/lit8 v21, v21, -0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jrm/localmm/business/photo/GifDecoder;->pixels:[B

    move-object/from16 v23, v0

    add-int/lit8 v19, v20, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jrm/localmm/business/photo/GifDecoder;->pixelStack:[B

    move-object/from16 v24, v0

    aget-byte v24, v24, v21

    aput-byte v24, v23, v20

    add-int/lit8 v15, v15, 0x1

    move/from16 v20, v19

    move/from16 v22, v21

    goto/16 :goto_1

    :cond_10
    return-void

    :cond_11
    move/from16 v21, v22

    goto/16 :goto_2

    :cond_12
    move/from16 v21, v22

    goto :goto_5
.end method

.method public err()Z
    .locals 1

    iget v0, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->status:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getFrame(I)Landroid/graphics/Bitmap;
    .locals 2
    .param p1    # I

    const/4 v0, 0x0

    if-ltz p1, :cond_0

    iget v1, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->frameCount:I

    if-ge p1, v1, :cond_0

    iget-object v1, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->frames:Ljava/util/Vector;

    invoke-virtual {v1, p1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jrm/localmm/business/photo/GifDecoder$GifFrame;

    iget-object v0, v1, Lcom/jrm/localmm/business/photo/GifDecoder$GifFrame;->image:Landroid/graphics/Bitmap;

    :cond_0
    return-object v0
.end method

.method public getFrameCount()I
    .locals 1

    iget v0, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->frameCount:I

    return v0
.end method

.method public getImage()Landroid/graphics/Bitmap;
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/business/photo/GifDecoder;->getFrame(I)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public init()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    iput v0, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->status:I

    iput v0, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->frameCount:I

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->frames:Ljava/util/Vector;

    iput-object v1, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->gct:[I

    iput-object v1, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->lct:[I

    return-void
.end method

.method public nextBitmap()Landroid/graphics/Bitmap;
    .locals 2

    iget v0, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->frameindex:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->frameindex:I

    iget v0, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->frameindex:I

    iget v1, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->frameCount:I

    add-int/lit8 v1, v1, -0x1

    if-le v0, v1, :cond_0

    const/4 v0, 0x0

    iput v0, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->frameindex:I

    :cond_0
    iget-object v0, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->frames:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-gtz v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->frames:Ljava/util/Vector;

    iget v1, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->frameindex:I

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jrm/localmm/business/photo/GifDecoder$GifFrame;

    iget-object v0, v0, Lcom/jrm/localmm/business/photo/GifDecoder$GifFrame;->image:Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method public nextDelay()I
    .locals 2

    iget-object v0, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->frames:Ljava/util/Vector;

    iget v1, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->frameindex:I

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jrm/localmm/business/photo/GifDecoder$GifFrame;

    iget v0, v0, Lcom/jrm/localmm/business/photo/GifDecoder$GifFrame;->delay:I

    return v0
.end method

.method protected read()I
    .locals 3

    const/4 v0, 0x0

    :try_start_0
    iget-object v2, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->in:Ljava/io/InputStream;

    invoke-virtual {v2}, Ljava/io/InputStream;->read()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_0
    return v0

    :catch_0
    move-exception v1

    const/4 v2, 0x1

    iput v2, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->status:I

    goto :goto_0
.end method

.method public read(Ljava/io/InputStream;)I
    .locals 4
    .param p1    # Ljava/io/InputStream;

    const/4 v3, 0x2

    invoke-virtual {p0}, Lcom/jrm/localmm/business/photo/GifDecoder;->init()V

    if-eqz p1, :cond_2

    iput-object p1, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->in:Ljava/io/InputStream;

    invoke-virtual {p0}, Lcom/jrm/localmm/business/photo/GifDecoder;->readHeader()V

    iget v1, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->width:I

    iget v2, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->height:I

    mul-int/2addr v1, v2

    const v2, 0x1fa400

    if-lt v1, v2, :cond_0

    iput v3, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->status:I

    :cond_0
    invoke-virtual {p0}, Lcom/jrm/localmm/business/photo/GifDecoder;->err()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p0}, Lcom/jrm/localmm/business/photo/GifDecoder;->readContents()V

    iget v1, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->frameCount:I

    if-gez v1, :cond_1

    const/4 v1, 0x1

    iput v1, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->status:I

    :cond_1
    :goto_0
    :try_start_0
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    iget v1, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->status:I

    return v1

    :cond_2
    iput v3, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->status:I

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method protected readBlock()I
    .locals 6

    invoke-virtual {p0}, Lcom/jrm/localmm/business/photo/GifDecoder;->read()I

    move-result v3

    iput v3, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->blockSize:I

    const/4 v2, 0x0

    iget v3, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->blockSize:I

    if-lez v3, :cond_1

    const/4 v0, 0x0

    :goto_0
    :try_start_0
    iget v3, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->blockSize:I

    if-ge v2, v3, :cond_0

    iget-object v3, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->in:Ljava/io/InputStream;

    iget-object v4, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->block:[B

    iget v5, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->blockSize:I

    sub-int/2addr v5, v2

    invoke-virtual {v3, v4, v2, v5}, Ljava/io/InputStream;->read([BII)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    const/4 v3, -0x1

    if-ne v0, v3, :cond_2

    :cond_0
    :goto_1
    iget v3, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->blockSize:I

    if-ge v2, v3, :cond_1

    const/4 v3, 0x1

    iput v3, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->status:I

    :cond_1
    return v2

    :cond_2
    add-int/2addr v2, v0

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method protected readColorTable(I)[I
    .locals 14
    .param p1    # I

    mul-int/lit8 v9, p1, 0x3

    const/4 v11, 0x0

    new-array v1, v9, [B

    const/4 v8, 0x0

    :try_start_0
    iget-object v12, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->in:Ljava/io/InputStream;

    invoke-virtual {v12, v1}, Ljava/io/InputStream;->read([B)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v8

    :goto_0
    if-ge v8, v9, :cond_1

    const/4 v12, 0x1

    iput v12, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->status:I

    :cond_0
    return-object v11

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    :cond_1
    const/16 v12, 0x100

    new-array v11, v12, [I

    const/4 v4, 0x0

    const/4 v6, 0x0

    move v7, v6

    move v5, v4

    :goto_1
    if-ge v5, p1, :cond_0

    add-int/lit8 v6, v7, 0x1

    aget-byte v12, v1, v7

    and-int/lit16 v10, v12, 0xff

    add-int/lit8 v7, v6, 0x1

    aget-byte v12, v1, v6

    and-int/lit16 v3, v12, 0xff

    add-int/lit8 v6, v7, 0x1

    aget-byte v12, v1, v7

    and-int/lit16 v0, v12, 0xff

    add-int/lit8 v4, v5, 0x1

    const/high16 v12, -0x1000000

    shl-int/lit8 v13, v10, 0x10

    or-int/2addr v12, v13

    shl-int/lit8 v13, v3, 0x8

    or-int/2addr v12, v13

    or-int/2addr v12, v0

    aput v12, v11, v5

    move v7, v6

    move v5, v4

    goto :goto_1
.end method

.method protected readContents()V
    .locals 6

    const/4 v2, 0x0

    :goto_0
    :sswitch_0
    if-nez v2, :cond_2

    invoke-virtual {p0}, Lcom/jrm/localmm/business/photo/GifDecoder;->err()Z

    move-result v4

    if-nez v4, :cond_2

    invoke-virtual {p0}, Lcom/jrm/localmm/business/photo/GifDecoder;->read()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    const/4 v4, 0x1

    iput v4, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->status:I

    goto :goto_0

    :sswitch_1
    invoke-virtual {p0}, Lcom/jrm/localmm/business/photo/GifDecoder;->readImage()V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p0}, Lcom/jrm/localmm/business/photo/GifDecoder;->read()I

    move-result v1

    sparse-switch v1, :sswitch_data_1

    invoke-virtual {p0}, Lcom/jrm/localmm/business/photo/GifDecoder;->skip()V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p0}, Lcom/jrm/localmm/business/photo/GifDecoder;->readGraphicControlExt()V

    goto :goto_0

    :sswitch_4
    invoke-virtual {p0}, Lcom/jrm/localmm/business/photo/GifDecoder;->readBlock()I

    const-string v0, ""

    const/4 v3, 0x0

    :goto_1
    const/16 v4, 0xb

    if-ge v3, v4, :cond_0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->block:[B

    aget-byte v5, v5, v3

    int-to-char v5, v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_0
    const-string v4, "NETSCAPE2.0"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {p0}, Lcom/jrm/localmm/business/photo/GifDecoder;->readNetscapeExt()V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/jrm/localmm/business/photo/GifDecoder;->skip()V

    goto :goto_0

    :sswitch_5
    const/4 v2, 0x1

    goto :goto_0

    :cond_2
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x21 -> :sswitch_2
        0x2c -> :sswitch_1
        0x3b -> :sswitch_5
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0xf9 -> :sswitch_3
        0xff -> :sswitch_4
    .end sparse-switch
.end method

.method protected readGraphicControlExt()V
    .locals 3

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/jrm/localmm/business/photo/GifDecoder;->read()I

    invoke-virtual {p0}, Lcom/jrm/localmm/business/photo/GifDecoder;->read()I

    move-result v0

    and-int/lit8 v2, v0, 0x1c

    shr-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->dispose:I

    iget v2, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->dispose:I

    if-nez v2, :cond_0

    iput v1, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->dispose:I

    :cond_0
    and-int/lit8 v2, v0, 0x1

    if-eqz v2, :cond_1

    :goto_0
    iput-boolean v1, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->transparency:Z

    invoke-virtual {p0}, Lcom/jrm/localmm/business/photo/GifDecoder;->readShort()I

    move-result v1

    mul-int/lit8 v1, v1, 0xa

    iput v1, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->delay:I

    invoke-virtual {p0}, Lcom/jrm/localmm/business/photo/GifDecoder;->read()I

    move-result v1

    iput v1, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->transIndex:I

    invoke-virtual {p0}, Lcom/jrm/localmm/business/photo/GifDecoder;->read()I

    return-void

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected readHeader()V
    .locals 5

    const-string v1, ""

    const/4 v0, 0x0

    :goto_0
    const/4 v2, 0x6

    if-ge v0, v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/jrm/localmm/business/photo/GifDecoder;->read()I

    move-result v3

    int-to-char v3, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    const-string v2, "GifDecoder"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "******id*****"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "GIF"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    const/4 v2, 0x1

    iput v2, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->status:I

    :cond_1
    :goto_1
    return-void

    :cond_2
    invoke-virtual {p0}, Lcom/jrm/localmm/business/photo/GifDecoder;->readLSD()V

    iget-boolean v2, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->gctFlag:Z

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/jrm/localmm/business/photo/GifDecoder;->err()Z

    move-result v2

    if-nez v2, :cond_1

    iget v2, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->gctSize:I

    invoke-virtual {p0, v2}, Lcom/jrm/localmm/business/photo/GifDecoder;->readColorTable(I)[I

    move-result-object v2

    iput-object v2, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->gct:[I

    iget-object v2, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->gct:[I

    iget v3, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->bgIndex:I

    aget v2, v2, v3

    iput v2, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->bgColor:I

    goto :goto_1
.end method

.method protected readImage()V
    .locals 6

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/jrm/localmm/business/photo/GifDecoder;->readShort()I

    move-result v2

    iput v2, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->ix:I

    invoke-virtual {p0}, Lcom/jrm/localmm/business/photo/GifDecoder;->readShort()I

    move-result v2

    iput v2, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->iy:I

    invoke-virtual {p0}, Lcom/jrm/localmm/business/photo/GifDecoder;->readShort()I

    move-result v2

    iput v2, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->iw:I

    invoke-virtual {p0}, Lcom/jrm/localmm/business/photo/GifDecoder;->readShort()I

    move-result v2

    iput v2, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->ih:I

    invoke-virtual {p0}, Lcom/jrm/localmm/business/photo/GifDecoder;->read()I

    move-result v0

    and-int/lit16 v2, v0, 0x80

    if-eqz v2, :cond_4

    move v2, v3

    :goto_0
    iput-boolean v2, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->lctFlag:Z

    and-int/lit8 v2, v0, 0x40

    if-eqz v2, :cond_5

    move v2, v3

    :goto_1
    iput-boolean v2, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->interlace:Z

    const/4 v2, 0x2

    and-int/lit8 v5, v0, 0x7

    shl-int/2addr v2, v5

    iput v2, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->lctSize:I

    iget-boolean v2, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->lctFlag:Z

    if-eqz v2, :cond_6

    iget v2, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->lctSize:I

    invoke-virtual {p0, v2}, Lcom/jrm/localmm/business/photo/GifDecoder;->readColorTable(I)[I

    move-result-object v2

    iput-object v2, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->lct:[I

    iget-object v2, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->lct:[I

    iput-object v2, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->act:[I

    :cond_0
    :goto_2
    const/4 v1, 0x0

    iget-boolean v2, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->transparency:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->act:[I

    iget v5, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->transIndex:I

    aget v1, v2, v5

    iget-object v2, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->act:[I

    iget v5, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->transIndex:I

    aput v4, v2, v5

    :cond_1
    iget-object v2, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->act:[I

    if-nez v2, :cond_2

    iput v3, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->status:I

    :cond_2
    invoke-virtual {p0}, Lcom/jrm/localmm/business/photo/GifDecoder;->err()Z

    move-result v2

    if-eqz v2, :cond_7

    :cond_3
    :goto_3
    return-void

    :cond_4
    move v2, v4

    goto :goto_0

    :cond_5
    move v2, v4

    goto :goto_1

    :cond_6
    iget-object v2, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->gct:[I

    iput-object v2, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->act:[I

    iget v2, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->bgIndex:I

    iget v5, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->transIndex:I

    if-ne v2, v5, :cond_0

    iput v4, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->bgColor:I

    goto :goto_2

    :cond_7
    invoke-virtual {p0}, Lcom/jrm/localmm/business/photo/GifDecoder;->decodeImageData()V

    invoke-virtual {p0}, Lcom/jrm/localmm/business/photo/GifDecoder;->skip()V

    invoke-virtual {p0}, Lcom/jrm/localmm/business/photo/GifDecoder;->err()Z

    move-result v2

    if-nez v2, :cond_3

    iget v2, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->frameCount:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->frameCount:I

    iget v2, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->width:I

    iget v3, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->height:I

    sget-object v4, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->image:Landroid/graphics/Bitmap;

    invoke-virtual {p0}, Lcom/jrm/localmm/business/photo/GifDecoder;->setPixels()V

    iget-object v2, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->frames:Ljava/util/Vector;

    new-instance v3, Lcom/jrm/localmm/business/photo/GifDecoder$GifFrame;

    iget-object v4, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->image:Landroid/graphics/Bitmap;

    iget v5, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->delay:I

    invoke-direct {v3, p0, v4, v5}, Lcom/jrm/localmm/business/photo/GifDecoder$GifFrame;-><init>(Lcom/jrm/localmm/business/photo/GifDecoder;Landroid/graphics/Bitmap;I)V

    invoke-virtual {v2, v3}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    iget-boolean v2, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->transparency:Z

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->act:[I

    iget v3, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->transIndex:I

    aput v1, v2, v3

    :cond_8
    invoke-virtual {p0}, Lcom/jrm/localmm/business/photo/GifDecoder;->resetFrame()V

    goto :goto_3
.end method

.method protected readLSD()V
    .locals 3

    invoke-virtual {p0}, Lcom/jrm/localmm/business/photo/GifDecoder;->readShort()I

    move-result v1

    iput v1, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->width:I

    invoke-virtual {p0}, Lcom/jrm/localmm/business/photo/GifDecoder;->readShort()I

    move-result v1

    iput v1, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->height:I

    invoke-virtual {p0}, Lcom/jrm/localmm/business/photo/GifDecoder;->read()I

    move-result v0

    and-int/lit16 v1, v0, 0x80

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    iput-boolean v1, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->gctFlag:Z

    const/4 v1, 0x2

    and-int/lit8 v2, v0, 0x7

    shl-int/2addr v1, v2

    iput v1, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->gctSize:I

    invoke-virtual {p0}, Lcom/jrm/localmm/business/photo/GifDecoder;->read()I

    move-result v1

    iput v1, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->bgIndex:I

    invoke-virtual {p0}, Lcom/jrm/localmm/business/photo/GifDecoder;->read()I

    move-result v1

    iput v1, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->pixelAspect:I

    return-void

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected readNetscapeExt()V
    .locals 5

    const/4 v4, 0x1

    :cond_0
    invoke-virtual {p0}, Lcom/jrm/localmm/business/photo/GifDecoder;->readBlock()I

    iget-object v2, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->block:[B

    const/4 v3, 0x0

    aget-byte v2, v2, v3

    if-ne v2, v4, :cond_1

    iget-object v2, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->block:[B

    aget-byte v2, v2, v4

    and-int/lit16 v0, v2, 0xff

    iget-object v2, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->block:[B

    const/4 v3, 0x2

    aget-byte v2, v2, v3

    and-int/lit16 v1, v2, 0xff

    shl-int/lit8 v2, v1, 0x8

    or-int/2addr v2, v0

    iput v2, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->loopCount:I

    :cond_1
    iget v2, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->blockSize:I

    if-lez v2, :cond_2

    invoke-virtual {p0}, Lcom/jrm/localmm/business/photo/GifDecoder;->err()Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_2
    return-void
.end method

.method protected readShort()I
    .locals 2

    invoke-virtual {p0}, Lcom/jrm/localmm/business/photo/GifDecoder;->read()I

    move-result v0

    invoke-virtual {p0}, Lcom/jrm/localmm/business/photo/GifDecoder;->read()I

    move-result v1

    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    return v0
.end method

.method public reset()V
    .locals 2

    iget-object v1, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->in:Ljava/io/InputStream;

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->in:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method protected resetFrame()V
    .locals 2

    const/4 v1, 0x0

    iget v0, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->dispose:I

    iput v0, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->lastDispose:I

    iget v0, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->ix:I

    iput v0, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->lrx:I

    iget v0, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->iy:I

    iput v0, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->lry:I

    iget v0, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->iw:I

    iput v0, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->lrw:I

    iget v0, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->ih:I

    iput v0, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->lrh:I

    iget-object v0, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->image:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->lastImage:Landroid/graphics/Bitmap;

    iget v0, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->bgColor:I

    iput v0, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->lastBgColor:I

    iput v1, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->dispose:I

    iput-boolean v1, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->transparency:Z

    iput v1, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->delay:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->lct:[I

    return-void
.end method

.method protected setPixels()V
    .locals 25

    move-object/from16 v0, p0

    iget v2, v0, Lcom/jrm/localmm/business/photo/GifDecoder;->width:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/jrm/localmm/business/photo/GifDecoder;->height:I

    mul-int/2addr v2, v4

    new-array v3, v2, [I

    move-object/from16 v0, p0

    iget v2, v0, Lcom/jrm/localmm/business/photo/GifDecoder;->lastDispose:I

    if-lez v2, :cond_4

    move-object/from16 v0, p0

    iget v2, v0, Lcom/jrm/localmm/business/photo/GifDecoder;->lastDispose:I

    const/4 v4, 0x3

    if-ne v2, v4, :cond_0

    move-object/from16 v0, p0

    iget v2, v0, Lcom/jrm/localmm/business/photo/GifDecoder;->frameCount:I

    add-int/lit8 v19, v2, -0x2

    if-lez v19, :cond_2

    add-int/lit8 v2, v19, -0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/jrm/localmm/business/photo/GifDecoder;->getFrame(I)Landroid/graphics/Bitmap;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/jrm/localmm/business/photo/GifDecoder;->lastImage:Landroid/graphics/Bitmap;

    :cond_0
    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/jrm/localmm/business/photo/GifDecoder;->lastImage:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/jrm/localmm/business/photo/GifDecoder;->lastImage:Landroid/graphics/Bitmap;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget v5, v0, Lcom/jrm/localmm/business/photo/GifDecoder;->width:I

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget v8, v0, Lcom/jrm/localmm/business/photo/GifDecoder;->width:I

    move-object/from16 v0, p0

    iget v9, v0, Lcom/jrm/localmm/business/photo/GifDecoder;->height:I

    invoke-virtual/range {v2 .. v9}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    move-object/from16 v0, p0

    iget v2, v0, Lcom/jrm/localmm/business/photo/GifDecoder;->lastDispose:I

    const/4 v4, 0x2

    if-ne v2, v4, :cond_4

    const/4 v10, 0x0

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/jrm/localmm/business/photo/GifDecoder;->transparency:Z

    if-nez v2, :cond_1

    move-object/from16 v0, p0

    iget v10, v0, Lcom/jrm/localmm/business/photo/GifDecoder;->lastBgColor:I

    :cond_1
    const/4 v13, 0x0

    :goto_1
    move-object/from16 v0, p0

    iget v2, v0, Lcom/jrm/localmm/business/photo/GifDecoder;->lrh:I

    if-ge v13, v2, :cond_4

    move-object/from16 v0, p0

    iget v2, v0, Lcom/jrm/localmm/business/photo/GifDecoder;->lry:I

    add-int/2addr v2, v13

    move-object/from16 v0, p0

    iget v4, v0, Lcom/jrm/localmm/business/photo/GifDecoder;->width:I

    mul-int/2addr v2, v4

    move-object/from16 v0, p0

    iget v4, v0, Lcom/jrm/localmm/business/photo/GifDecoder;->lrx:I

    add-int v20, v2, v4

    move-object/from16 v0, p0

    iget v2, v0, Lcom/jrm/localmm/business/photo/GifDecoder;->lrw:I

    add-int v21, v20, v2

    move/from16 v17, v20

    :goto_2
    move/from16 v0, v17

    move/from16 v1, v21

    if-ge v0, v1, :cond_3

    aput v10, v3, v17

    add-int/lit8 v17, v17, 0x1

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/jrm/localmm/business/photo/GifDecoder;->lastImage:Landroid/graphics/Bitmap;

    goto :goto_0

    :cond_3
    add-int/lit8 v13, v13, 0x1

    goto :goto_1

    :cond_4
    const/16 v22, 0x1

    const/16 v15, 0x8

    const/4 v14, 0x0

    const/4 v13, 0x0

    :goto_3
    move-object/from16 v0, p0

    iget v2, v0, Lcom/jrm/localmm/business/photo/GifDecoder;->ih:I

    if-ge v13, v2, :cond_a

    move/from16 v18, v13

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/jrm/localmm/business/photo/GifDecoder;->interlace:Z

    if-eqz v2, :cond_6

    move-object/from16 v0, p0

    iget v2, v0, Lcom/jrm/localmm/business/photo/GifDecoder;->ih:I

    if-lt v14, v2, :cond_5

    add-int/lit8 v22, v22, 0x1

    packed-switch v22, :pswitch_data_0

    :cond_5
    :goto_4
    move/from16 v18, v14

    add-int/2addr v14, v15

    :cond_6
    move-object/from16 v0, p0

    iget v2, v0, Lcom/jrm/localmm/business/photo/GifDecoder;->iy:I

    add-int v18, v18, v2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/jrm/localmm/business/photo/GifDecoder;->height:I

    move/from16 v0, v18

    if-ge v0, v2, :cond_9

    move-object/from16 v0, p0

    iget v2, v0, Lcom/jrm/localmm/business/photo/GifDecoder;->width:I

    mul-int v17, v18, v2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/jrm/localmm/business/photo/GifDecoder;->ix:I

    add-int v12, v17, v2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/jrm/localmm/business/photo/GifDecoder;->iw:I

    add-int v11, v12, v2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/jrm/localmm/business/photo/GifDecoder;->width:I

    add-int v2, v2, v17

    if-ge v2, v11, :cond_7

    move-object/from16 v0, p0

    iget v2, v0, Lcom/jrm/localmm/business/photo/GifDecoder;->width:I

    add-int v11, v17, v2

    :cond_7
    move-object/from16 v0, p0

    iget v2, v0, Lcom/jrm/localmm/business/photo/GifDecoder;->iw:I

    mul-int v23, v13, v2

    move/from16 v24, v23

    :goto_5
    if-ge v12, v11, :cond_9

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/jrm/localmm/business/photo/GifDecoder;->pixels:[B

    add-int/lit8 v23, v24, 0x1

    aget-byte v2, v2, v24

    and-int/lit16 v0, v2, 0xff

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/jrm/localmm/business/photo/GifDecoder;->act:[I

    aget v10, v2, v16

    if-eqz v10, :cond_8

    aput v10, v3, v12

    :cond_8
    add-int/lit8 v12, v12, 0x1

    move/from16 v24, v23

    goto :goto_5

    :pswitch_0
    const/4 v14, 0x4

    goto :goto_4

    :pswitch_1
    const/4 v14, 0x2

    const/4 v15, 0x4

    goto :goto_4

    :pswitch_2
    const/4 v14, 0x1

    const/4 v15, 0x2

    goto :goto_4

    :cond_9
    add-int/lit8 v13, v13, 0x1

    goto :goto_3

    :cond_a
    move-object/from16 v0, p0

    iget v2, v0, Lcom/jrm/localmm/business/photo/GifDecoder;->width:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/jrm/localmm/business/photo/GifDecoder;->height:I

    sget-object v5, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v2, v4, v5}, Landroid/graphics/Bitmap;->createBitmap([IIILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/jrm/localmm/business/photo/GifDecoder;->image:Landroid/graphics/Bitmap;

    return-void

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected skip()V
    .locals 1

    :cond_0
    invoke-virtual {p0}, Lcom/jrm/localmm/business/photo/GifDecoder;->readBlock()I

    iget v0, p0, Lcom/jrm/localmm/business/photo/GifDecoder;->blockSize:I

    if-lez v0, :cond_1

    invoke-virtual {p0}, Lcom/jrm/localmm/business/photo/GifDecoder;->err()Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_1
    return-void
.end method
