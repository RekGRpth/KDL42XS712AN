.class public Lcom/jrm/localmm/business/photo/MyInputStream;
.super Ljava/io/BufferedInputStream;
.source "MyInputStream.java"


# instance fields
.field private imagePath:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/io/InputStream;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/io/InputStream;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0, p1}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    iput-object p2, p0, Lcom/jrm/localmm/business/photo/MyInputStream;->imagePath:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public declared-synchronized reset()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v4, -0x1

    monitor-enter p0

    :try_start_0
    const-string v2, "MyInputStream"

    const-string v3, "***********reset()************"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v2, Ljava/net/URL;

    iget-object v3, p0, Lcom/jrm/localmm/business/photo/MyInputStream;->imagePath:Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/net/URL;->openStream()Ljava/io/InputStream;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/jrm/localmm/business/photo/MyInputStream;->close()V

    iput-object v0, p0, Lcom/jrm/localmm/business/photo/MyInputStream;->in:Ljava/io/InputStream;

    const/4 v2, 0x0

    iput v2, p0, Lcom/jrm/localmm/business/photo/MyInputStream;->count:I

    const/4 v2, 0x0

    iput v2, p0, Lcom/jrm/localmm/business/photo/MyInputStream;->marklimit:I

    const/4 v2, -0x1

    iput v2, p0, Lcom/jrm/localmm/business/photo/MyInputStream;->markpos:I

    const/4 v2, 0x0

    iput v2, p0, Lcom/jrm/localmm/business/photo/MyInputStream;->pos:I

    const/16 v2, 0x2000

    new-array v2, v2, [B

    iput-object v2, p0, Lcom/jrm/localmm/business/photo/MyInputStream;->buf:[B

    iget-object v2, p0, Lcom/jrm/localmm/business/photo/MyInputStream;->in:Ljava/io/InputStream;

    iget-object v3, p0, Lcom/jrm/localmm/business/photo/MyInputStream;->buf:[B

    invoke-virtual {v2, v3}, Ljava/io/InputStream;->read([B)I

    move-result v1

    if-lez v1, :cond_0

    const/4 v2, -0x1

    iput v2, p0, Lcom/jrm/localmm/business/photo/MyInputStream;->markpos:I

    const/4 v2, 0x0

    iput v2, p0, Lcom/jrm/localmm/business/photo/MyInputStream;->pos:I

    iput v1, p0, Lcom/jrm/localmm/business/photo/MyInputStream;->count:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :cond_0
    if-nez v1, :cond_1

    :try_start_1
    new-instance v2, Ljava/io/IOException;

    const-string v3, "The network is not available!"

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    :cond_1
    if-ne v4, v1, :cond_2

    :try_start_2
    new-instance v2, Ljava/io/IOException;

    const-string v3, "EOF!"

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_2
    new-instance v2, Ljava/io/IOException;

    const-string v3, "Unknown error!"

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_3
    new-instance v2, Ljava/io/IOException;

    const-string v3, "Initialize inputStream  fail!"

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
.end method
