.class public Lcom/jrm/localmm/business/data/ReturnStack;
.super Ljava/lang/Object;
.source "ReturnStack.java"


# instance fields
.field private rdList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/jrm/localmm/business/data/ReturnData;",
            ">;"
        }
    .end annotation
.end field

.field private topPosition:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/jrm/localmm/business/data/ReturnStack;->rdList:Ljava/util/List;

    const/4 v0, -0x1

    iput v0, p0, Lcom/jrm/localmm/business/data/ReturnStack;->topPosition:I

    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    :goto_0
    iget v0, p0, Lcom/jrm/localmm/business/data/ReturnStack;->topPosition:I

    if-ltz v0, :cond_0

    invoke-virtual {p0}, Lcom/jrm/localmm/business/data/ReturnStack;->pop()Lcom/jrm/localmm/business/data/ReturnData;

    goto :goto_0

    :cond_0
    return-void
.end method

.method public getTankage()I
    .locals 1

    iget v0, p0, Lcom/jrm/localmm/business/data/ReturnStack;->topPosition:I

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public pop()Lcom/jrm/localmm/business/data/ReturnData;
    .locals 3

    iget v0, p0, Lcom/jrm/localmm/business/data/ReturnStack;->topPosition:I

    if-gez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/jrm/localmm/business/data/ReturnStack;->rdList:Ljava/util/List;

    iget v1, p0, Lcom/jrm/localmm/business/data/ReturnStack;->topPosition:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jrm/localmm/business/data/ReturnData;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v1, p0, Lcom/jrm/localmm/business/data/ReturnStack;->rdList:Ljava/util/List;

    iget v2, p0, Lcom/jrm/localmm/business/data/ReturnStack;->topPosition:I

    invoke-interface {v1, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    iget v1, p0, Lcom/jrm/localmm/business/data/ReturnStack;->topPosition:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/jrm/localmm/business/data/ReturnStack;->topPosition:I

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/jrm/localmm/business/data/ReturnStack;->rdList:Ljava/util/List;

    iget v2, p0, Lcom/jrm/localmm/business/data/ReturnStack;->topPosition:I

    invoke-interface {v1, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    iget v1, p0, Lcom/jrm/localmm/business/data/ReturnStack;->topPosition:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/jrm/localmm/business/data/ReturnStack;->topPosition:I

    throw v0
.end method

.method public push(Lcom/jrm/localmm/business/data/ReturnData;)V
    .locals 1
    .param p1    # Lcom/jrm/localmm/business/data/ReturnData;

    iget-object v0, p0, Lcom/jrm/localmm/business/data/ReturnStack;->rdList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget v0, p0, Lcom/jrm/localmm/business/data/ReturnStack;->topPosition:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/jrm/localmm/business/data/ReturnStack;->topPosition:I

    return-void
.end method
