.class public Lcom/jrm/localmm/business/data/ReturnData;
.super Ljava/lang/Object;
.source "ReturnData.java"


# instance fields
.field private id:Ljava/lang/String;

.field private page:I

.field private position:I

.field private total:I


# direct methods
.method public constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/jrm/localmm/business/data/ReturnData;->id:Ljava/lang/String;

    iput p2, p0, Lcom/jrm/localmm/business/data/ReturnData;->page:I

    iput p3, p0, Lcom/jrm/localmm/business/data/ReturnData;->position:I

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/jrm/localmm/business/data/ReturnData;->id:Ljava/lang/String;

    iput p2, p0, Lcom/jrm/localmm/business/data/ReturnData;->page:I

    iput p3, p0, Lcom/jrm/localmm/business/data/ReturnData;->total:I

    iput p4, p0, Lcom/jrm/localmm/business/data/ReturnData;->position:I

    return-void
.end method


# virtual methods
.method public getId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jrm/localmm/business/data/ReturnData;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getPage()I
    .locals 1

    iget v0, p0, Lcom/jrm/localmm/business/data/ReturnData;->page:I

    return v0
.end method

.method public getPosition()I
    .locals 1

    iget v0, p0, Lcom/jrm/localmm/business/data/ReturnData;->position:I

    return v0
.end method

.method public getTotal()I
    .locals 1

    iget v0, p0, Lcom/jrm/localmm/business/data/ReturnData;->total:I

    return v0
.end method
