.class public Lcom/jrm/localmm/business/data/BaseData;
.super Ljava/lang/Object;
.source "BaseData.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/jrm/localmm/business/data/BaseData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private _duration:I

.field private album:J

.field private artist:Ljava/lang/String;

.field private description:Ljava/lang/String;

.field private duration:Ljava/lang/String;

.field private format:Ljava/lang/String;

.field private icon:I

.field private id:J

.field private modifyTime:J

.field private name:Ljava/lang/String;

.field private parentPath:Ljava/lang/String;

.field private path:Ljava/lang/String;

.field private size:Ljava/lang/String;

.field private title:Ljava/lang/String;

.field private type:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/jrm/localmm/business/data/BaseData$1;

    invoke-direct {v0}, Lcom/jrm/localmm/business/data/BaseData$1;-><init>()V

    sput-object v0, Lcom/jrm/localmm/business/data/BaseData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/jrm/localmm/business/data/BaseData;->id:J

    return-void
.end method

.method public constructor <init>(I)V
    .locals 2
    .param p1    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/jrm/localmm/business/data/BaseData;->id:J

    iput p1, p0, Lcom/jrm/localmm/business/data/BaseData;->type:I

    const/4 v0, 0x2

    if-ne v0, p1, :cond_1

    const v0, 0x7f020020    # com.jrm.localmm.R.drawable.icon_file_pic

    iput v0, p0, Lcom/jrm/localmm/business/data/BaseData;->icon:I

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x3

    if-ne v0, p1, :cond_2

    const v0, 0x7f020022    # com.jrm.localmm.R.drawable.icon_file_song

    iput v0, p0, Lcom/jrm/localmm/business/data/BaseData;->icon:I

    goto :goto_0

    :cond_2
    const/4 v0, 0x4

    if-ne v0, p1, :cond_3

    const v0, 0x7f020023    # com.jrm.localmm.R.drawable.icon_file_video

    iput v0, p0, Lcom/jrm/localmm/business/data/BaseData;->icon:I

    goto :goto_0

    :cond_3
    const/4 v0, 0x1

    if-ne v0, p1, :cond_4

    const v0, 0x7f02001e    # com.jrm.localmm.R.drawable.icon_file_file

    iput v0, p0, Lcom/jrm/localmm/business/data/BaseData;->icon:I

    goto :goto_0

    :cond_4
    const/4 v0, 0x5

    if-ne v0, p1, :cond_5

    const v0, 0x7f02001f    # com.jrm.localmm.R.drawable.icon_file_folder

    iput v0, p0, Lcom/jrm/localmm/business/data/BaseData;->icon:I

    goto :goto_0

    :cond_5
    const/4 v0, 0x6

    if-ne v0, p1, :cond_0

    const v0, 0x7f020021    # com.jrm.localmm.R.drawable.icon_file_return

    iput v0, p0, Lcom/jrm/localmm/business/data/BaseData;->icon:I

    goto :goto_0
.end method

.method static synthetic access$002(Lcom/jrm/localmm/business/data/BaseData;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/jrm/localmm/business/data/BaseData;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/jrm/localmm/business/data/BaseData;->name:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$102(Lcom/jrm/localmm/business/data/BaseData;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/jrm/localmm/business/data/BaseData;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/jrm/localmm/business/data/BaseData;->path:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$202(Lcom/jrm/localmm/business/data/BaseData;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/jrm/localmm/business/data/BaseData;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/jrm/localmm/business/data/BaseData;->size:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$302(Lcom/jrm/localmm/business/data/BaseData;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/jrm/localmm/business/data/BaseData;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/jrm/localmm/business/data/BaseData;->format:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$402(Lcom/jrm/localmm/business/data/BaseData;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/jrm/localmm/business/data/BaseData;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/jrm/localmm/business/data/BaseData;->artist:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$502(Lcom/jrm/localmm/business/data/BaseData;J)J
    .locals 0
    .param p0    # Lcom/jrm/localmm/business/data/BaseData;
    .param p1    # J

    iput-wide p1, p0, Lcom/jrm/localmm/business/data/BaseData;->modifyTime:J

    return-wide p1
.end method

.method static synthetic access$602(Lcom/jrm/localmm/business/data/BaseData;J)J
    .locals 0
    .param p0    # Lcom/jrm/localmm/business/data/BaseData;
    .param p1    # J

    iput-wide p1, p0, Lcom/jrm/localmm/business/data/BaseData;->id:J

    return-wide p1
.end method

.method static synthetic access$702(Lcom/jrm/localmm/business/data/BaseData;J)J
    .locals 0
    .param p0    # Lcom/jrm/localmm/business/data/BaseData;
    .param p1    # J

    iput-wide p1, p0, Lcom/jrm/localmm/business/data/BaseData;->album:J

    return-wide p1
.end method

.method static synthetic access$802(Lcom/jrm/localmm/business/data/BaseData;I)I
    .locals 0
    .param p0    # Lcom/jrm/localmm/business/data/BaseData;
    .param p1    # I

    iput p1, p0, Lcom/jrm/localmm/business/data/BaseData;->_duration:I

    return p1
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getAlbum()J
    .locals 2

    iget-wide v0, p0, Lcom/jrm/localmm/business/data/BaseData;->album:J

    return-wide v0
.end method

.method public getArtist()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jrm/localmm/business/data/BaseData;->artist:Ljava/lang/String;

    return-object v0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jrm/localmm/business/data/BaseData;->description:Ljava/lang/String;

    return-object v0
.end method

.method public getDuration()I
    .locals 1

    iget v0, p0, Lcom/jrm/localmm/business/data/BaseData;->_duration:I

    return v0
.end method

.method public getDuration2()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jrm/localmm/business/data/BaseData;->duration:Ljava/lang/String;

    return-object v0
.end method

.method public getFormat()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jrm/localmm/business/data/BaseData;->format:Ljava/lang/String;

    return-object v0
.end method

.method public getIcon()I
    .locals 1

    iget v0, p0, Lcom/jrm/localmm/business/data/BaseData;->icon:I

    return v0
.end method

.method public getId()J
    .locals 2

    iget-wide v0, p0, Lcom/jrm/localmm/business/data/BaseData;->id:J

    return-wide v0
.end method

.method public getModifyTime()J
    .locals 2

    iget-wide v0, p0, Lcom/jrm/localmm/business/data/BaseData;->modifyTime:J

    return-wide v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jrm/localmm/business/data/BaseData;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getParentPath()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jrm/localmm/business/data/BaseData;->parentPath:Ljava/lang/String;

    return-object v0
.end method

.method public getPath()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jrm/localmm/business/data/BaseData;->path:Ljava/lang/String;

    return-object v0
.end method

.method public getSize()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jrm/localmm/business/data/BaseData;->size:Ljava/lang/String;

    return-object v0
.end method

.method public getType()I
    .locals 1

    iget v0, p0, Lcom/jrm/localmm/business/data/BaseData;->type:I

    return v0
.end method

.method public setAlbum(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/jrm/localmm/business/data/BaseData;->album:J

    return-void
.end method

.method public setArtist(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/jrm/localmm/business/data/BaseData;->artist:Ljava/lang/String;

    return-void
.end method

.method public setDescription(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/jrm/localmm/business/data/BaseData;->description:Ljava/lang/String;

    return-void
.end method

.method public setFormat(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/jrm/localmm/business/data/BaseData;->format:Ljava/lang/String;

    return-void
.end method

.method public setId(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/jrm/localmm/business/data/BaseData;->id:J

    return-void
.end method

.method public setModifyTime(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/jrm/localmm/business/data/BaseData;->modifyTime:J

    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/jrm/localmm/business/data/BaseData;->name:Ljava/lang/String;

    return-void
.end method

.method public setParentPath(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/jrm/localmm/business/data/BaseData;->parentPath:Ljava/lang/String;

    return-void
.end method

.method public setPath(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/jrm/localmm/business/data/BaseData;->path:Ljava/lang/String;

    return-void
.end method

.method public setSize(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/jrm/localmm/business/data/BaseData;->size:Ljava/lang/String;

    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/jrm/localmm/business/data/BaseData;->title:Ljava/lang/String;

    return-void
.end method

.method public setType(I)V
    .locals 1
    .param p1    # I

    iput p1, p0, Lcom/jrm/localmm/business/data/BaseData;->type:I

    const/4 v0, 0x5

    if-ne v0, p1, :cond_1

    const v0, 0x7f02001f    # com.jrm.localmm.R.drawable.icon_file_folder

    iput v0, p0, Lcom/jrm/localmm/business/data/BaseData;->icon:I

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x6

    if-ne v0, p1, :cond_2

    const v0, 0x7f020021    # com.jrm.localmm.R.drawable.icon_file_return

    iput v0, p0, Lcom/jrm/localmm/business/data/BaseData;->icon:I

    goto :goto_0

    :cond_2
    const/4 v0, 0x2

    if-ne v0, p1, :cond_3

    const v0, 0x7f020020    # com.jrm.localmm.R.drawable.icon_file_pic

    iput v0, p0, Lcom/jrm/localmm/business/data/BaseData;->icon:I

    goto :goto_0

    :cond_3
    const/4 v0, 0x3

    if-ne v0, p1, :cond_4

    const v0, 0x7f020022    # com.jrm.localmm.R.drawable.icon_file_song

    iput v0, p0, Lcom/jrm/localmm/business/data/BaseData;->icon:I

    goto :goto_0

    :cond_4
    const/4 v0, 0x4

    if-ne v0, p1, :cond_5

    const v0, 0x7f020023    # com.jrm.localmm.R.drawable.icon_file_video

    iput v0, p0, Lcom/jrm/localmm/business/data/BaseData;->icon:I

    goto :goto_0

    :cond_5
    const/4 v0, 0x1

    if-ne v0, p1, :cond_0

    const v0, 0x7f02001e    # com.jrm.localmm.R.drawable.icon_file_file

    iput v0, p0, Lcom/jrm/localmm/business/data/BaseData;->icon:I

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget-object v0, p0, Lcom/jrm/localmm/business/data/BaseData;->name:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/jrm/localmm/business/data/BaseData;->path:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/jrm/localmm/business/data/BaseData;->size:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/jrm/localmm/business/data/BaseData;->format:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/jrm/localmm/business/data/BaseData;->artist:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-wide v0, p0, Lcom/jrm/localmm/business/data/BaseData;->modifyTime:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    iget-wide v0, p0, Lcom/jrm/localmm/business/data/BaseData;->id:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    iget-wide v0, p0, Lcom/jrm/localmm/business/data/BaseData;->album:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    iget v0, p0, Lcom/jrm/localmm/business/data/BaseData;->_duration:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
