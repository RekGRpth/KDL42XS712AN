.class public Lcom/jrm/localmm/business/data/MediaServerDisplay;
.super Ljava/lang/Object;
.source "MediaServerDisplay.java"


# instance fields
.field private deviceInfo:Landroid/net/dlna/DeviceInfo;

.field private isConnected:Z

.field private mediaServerController:Landroid/net/dlna/MediaServerController;


# direct methods
.method public constructor <init>(Landroid/net/dlna/MediaServerController;Z)V
    .locals 2
    .param p1    # Landroid/net/dlna/MediaServerController;
    .param p2    # Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/jrm/localmm/business/data/MediaServerDisplay;->mediaServerController:Landroid/net/dlna/MediaServerController;

    :try_start_0
    iget-object v1, p0, Lcom/jrm/localmm/business/data/MediaServerDisplay;->mediaServerController:Landroid/net/dlna/MediaServerController;

    invoke-interface {v1}, Landroid/net/dlna/MediaServerController;->GetDeviceInfo()Landroid/net/dlna/DeviceInfo;

    move-result-object v1

    iput-object v1, p0, Lcom/jrm/localmm/business/data/MediaServerDisplay;->deviceInfo:Landroid/net/dlna/DeviceInfo;
    :try_end_0
    .catch Landroid/net/dlna/ActionUnsupportedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/net/dlna/HostUnreachableException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    invoke-virtual {p0, p2}, Lcom/jrm/localmm/business/data/MediaServerDisplay;->setConnected(Z)V

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/net/dlna/ActionUnsupportedException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Landroid/net/dlna/HostUnreachableException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 6
    .param p1    # Ljava/lang/Object;

    const/4 v3, 0x0

    if-ne p0, p1, :cond_1

    const/4 v3, 0x1

    :cond_0
    :goto_0
    return v3

    :cond_1
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    if-ne v4, v5, :cond_0

    move-object v1, p1

    check-cast v1, Lcom/jrm/localmm/business/data/MediaServerDisplay;

    :try_start_0
    invoke-virtual {v1}, Lcom/jrm/localmm/business/data/MediaServerDisplay;->getMediaServerController()Landroid/net/dlna/MediaServerController;

    move-result-object v4

    invoke-interface {v4}, Landroid/net/dlna/MediaServerController;->GetDeviceInfo()Landroid/net/dlna/DeviceInfo;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/dlna/DeviceInfo;->getUDN()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/jrm/localmm/business/data/MediaServerDisplay;->deviceInfo:Landroid/net/dlna/DeviceInfo;

    invoke-virtual {v5}, Landroid/net/dlna/DeviceInfo;->getUDN()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/net/dlna/ActionUnsupportedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/net/dlna/HostUnreachableException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v3

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/net/dlna/ActionUnsupportedException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Landroid/net/dlna/HostUnreachableException;->printStackTrace()V

    goto :goto_0
.end method

.method public getFriendlyName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jrm/localmm/business/data/MediaServerDisplay;->deviceInfo:Landroid/net/dlna/DeviceInfo;

    invoke-virtual {v0}, Landroid/net/dlna/DeviceInfo;->getFriendlyName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMediaServerController()Landroid/net/dlna/MediaServerController;
    .locals 1

    iget-object v0, p0, Lcom/jrm/localmm/business/data/MediaServerDisplay;->mediaServerController:Landroid/net/dlna/MediaServerController;

    return-object v0
.end method

.method public getSerilNumber()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jrm/localmm/business/data/MediaServerDisplay;->deviceInfo:Landroid/net/dlna/DeviceInfo;

    invoke-virtual {v0}, Landroid/net/dlna/DeviceInfo;->getSerialNumber()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setConnected(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/jrm/localmm/business/data/MediaServerDisplay;->isConnected:Z

    return-void
.end method
