.class public Lcom/jrm/localmm/business/video/BreakPointDBAdapter;
.super Ljava/lang/Object;
.source "BreakPointDBAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/jrm/localmm/business/video/BreakPointDBAdapter$DBOpenHelper;
    }
.end annotation


# instance fields
.field private db:Landroid/database/sqlite/SQLiteDatabase;

.field private dbOpenHelper:Lcom/jrm/localmm/business/video/BreakPointDBAdapter$DBOpenHelper;

.field private xContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/jrm/localmm/business/video/BreakPointDBAdapter;->xContext:Landroid/content/Context;

    return-void
.end method

.method private ConvertToInfo(Landroid/database/Cursor;)[Lcom/jrm/localmm/business/video/BreakPointInfo;
    .locals 6
    .param p1    # Landroid/database/Cursor;

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-nez v3, :cond_2

    :cond_0
    const/4 v0, 0x0

    :cond_1
    return-object v0

    :cond_2
    new-array v0, v2, [Lcom/jrm/localmm/business/video/BreakPointInfo;

    const-string v3, "db_action"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "breakPoint len:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    array-length v5, v0

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_3

    new-instance v3, Lcom/jrm/localmm/business/video/BreakPointInfo;

    invoke-direct {v3}, Lcom/jrm/localmm/business/video/BreakPointInfo;-><init>()V

    aput-object v3, v0, v1

    aget-object v3, v0, v1

    const-string v4, "path"

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/jrm/localmm/business/video/BreakPointInfo;->Path:Ljava/lang/String;

    aget-object v3, v0, v1

    const-string v4, "time"

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    iput v4, v3, Lcom/jrm/localmm/business/video/BreakPointInfo;->Time:I

    aget-object v3, v0, v1

    const-string v4, "size"

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/jrm/localmm/business/video/BreakPointInfo;->Size:Ljava/lang/String;

    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    const/16 v3, 0x64

    if-lt v2, v3, :cond_1

    const/4 v1, 0x0

    :goto_1
    const/16 v3, 0xa

    if-ge v1, v3, :cond_1

    aget-object v3, v0, v1

    iget-object v3, v3, Lcom/jrm/localmm/business/video/BreakPointInfo;->Path:Ljava/lang/String;

    aget-object v4, v0, v1

    iget-object v4, v4, Lcom/jrm/localmm/business/video/BreakPointInfo;->Size:Ljava/lang/String;

    invoke-virtual {p0, v3, v4}, Lcom/jrm/localmm/business/video/BreakPointDBAdapter;->deleteOneData(Ljava/lang/String;Ljava/lang/String;)J

    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method


# virtual methods
.method public close()V
    .locals 1

    iget-object v0, p0, Lcom/jrm/localmm/business/video/BreakPointDBAdapter;->db:Landroid/database/sqlite/SQLiteDatabase;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/business/video/BreakPointDBAdapter;->db:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/jrm/localmm/business/video/BreakPointDBAdapter;->db:Landroid/database/sqlite/SQLiteDatabase;

    :cond_0
    return-void
.end method

.method public deleteOneData(Ljava/lang/String;Ljava/lang/String;)J
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    iget-object v0, p0, Lcom/jrm/localmm/business/video/BreakPointDBAdapter;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "breakpoint"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "path=\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " and "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "size"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method public insert(Lcom/jrm/localmm/business/video/BreakPointInfo;)J
    .locals 4
    .param p1    # Lcom/jrm/localmm/business/video/BreakPointInfo;

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "path"

    iget-object v2, p1, Lcom/jrm/localmm/business/video/BreakPointInfo;->Path:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "time"

    iget v2, p1, Lcom/jrm/localmm/business/video/BreakPointInfo;->Time:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "size"

    iget-object v2, p1, Lcom/jrm/localmm/business/video/BreakPointInfo;->Size:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/jrm/localmm/business/video/BreakPointDBAdapter;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "breakpoint"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v1

    return-wide v1
.end method

.method public open()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/database/sqlite/SQLiteException;
        }
    .end annotation

    new-instance v1, Lcom/jrm/localmm/business/video/BreakPointDBAdapter$DBOpenHelper;

    iget-object v2, p0, Lcom/jrm/localmm/business/video/BreakPointDBAdapter;->xContext:Landroid/content/Context;

    const-string v3, "breakpoint.db"

    const/4 v4, 0x0

    const/4 v5, 0x1

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/jrm/localmm/business/video/BreakPointDBAdapter$DBOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    iput-object v1, p0, Lcom/jrm/localmm/business/video/BreakPointDBAdapter;->dbOpenHelper:Lcom/jrm/localmm/business/video/BreakPointDBAdapter$DBOpenHelper;

    :try_start_0
    iget-object v1, p0, Lcom/jrm/localmm/business/video/BreakPointDBAdapter;->dbOpenHelper:Lcom/jrm/localmm/business/video/BreakPointDBAdapter$DBOpenHelper;

    invoke-virtual {v1}, Lcom/jrm/localmm/business/video/BreakPointDBAdapter$DBOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    iput-object v1, p0, Lcom/jrm/localmm/business/video/BreakPointDBAdapter;->db:Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    iget-object v1, p0, Lcom/jrm/localmm/business/video/BreakPointDBAdapter;->dbOpenHelper:Lcom/jrm/localmm/business/video/BreakPointDBAdapter$DBOpenHelper;

    invoke-virtual {v1}, Lcom/jrm/localmm/business/video/BreakPointDBAdapter$DBOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    iput-object v1, p0, Lcom/jrm/localmm/business/video/BreakPointDBAdapter;->db:Landroid/database/sqlite/SQLiteDatabase;

    goto :goto_0
.end method

.method public queryAllData()[Lcom/jrm/localmm/business/video/BreakPointInfo;
    .locals 9

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/jrm/localmm/business/video/BreakPointDBAdapter;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "breakpoint"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "path"

    aput-object v5, v2, v4

    const/4 v4, 0x1

    const-string v5, "time"

    aput-object v5, v2, v4

    const/4 v4, 0x2

    const-string v5, "size"

    aput-object v5, v2, v4

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    move-object v7, v3

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    invoke-direct {p0, v8}, Lcom/jrm/localmm/business/video/BreakPointDBAdapter;->ConvertToInfo(Landroid/database/Cursor;)[Lcom/jrm/localmm/business/video/BreakPointInfo;

    move-result-object v0

    return-object v0
.end method

.method public queryOneData(Ljava/lang/String;Ljava/lang/String;)[Lcom/jrm/localmm/business/video/BreakPointInfo;
    .locals 9
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/jrm/localmm/business/video/BreakPointDBAdapter;->db:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "breakpoint"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v5, "path"

    aput-object v5, v2, v3

    const/4 v3, 0x1

    const-string v5, "time"

    aput-object v5, v2, v3

    const/4 v3, 0x2

    const-string v5, "size"

    aput-object v5, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "path=\'"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "\'"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " and "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "size"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "\'"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "\'"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v5, v4

    move-object v6, v4

    move-object v7, v4

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    invoke-direct {p0, v8}, Lcom/jrm/localmm/business/video/BreakPointDBAdapter;->ConvertToInfo(Landroid/database/Cursor;)[Lcom/jrm/localmm/business/video/BreakPointInfo;

    move-result-object v0

    return-object v0
.end method
