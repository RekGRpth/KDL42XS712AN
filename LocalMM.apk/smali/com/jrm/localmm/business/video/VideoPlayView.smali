.class public Lcom/jrm/localmm/business/video/VideoPlayView;
.super Landroid/view/SurfaceView;
.source "VideoPlayView.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "NewApi"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/jrm/localmm/business/video/VideoPlayView$playerCallback;
    }
.end annotation


# instance fields
.field private TAG:Ljava/lang/String;

.field private currentVoice:F

.field private endSeekTime:J

.field private isVoiceOpen:Z

.field private mAudioManager:Landroid/media/AudioManager;

.field private mBufferingUpdateListener:Landroid/media/MediaPlayer$OnBufferingUpdateListener;

.field private mCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

.field private mCurrentState:I

.field private mDuration:I

.field private mErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

.field private mInfoListener:Landroid/media/MediaPlayer$OnInfoListener;

.field private mMMediaPlayer:Lcom/mstar/android/media/MMediaPlayer;

.field private mMMediaPlayerSeekCompleteListener:Landroid/media/MediaPlayer$OnSeekCompleteListener;

.field mPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

.field mSHCallback:Landroid/view/SurfaceHolder$Callback;

.field private mSeekWhenPrepared:I

.field mSizeChangedListener:Landroid/media/MediaPlayer$OnVideoSizeChangedListener;

.field private mSurfaceHeight:I

.field private mSurfaceHolder:Landroid/view/SurfaceHolder;

.field private mSurfaceWidth:I

.field private mTargetState:I

.field private mTimedTextListener:Landroid/media/MediaPlayer$OnTimedTextListener;

.field private mUri:Landroid/net/Uri;

.field private mVideoHeight:I

.field private mVideoWidth:I

.field private myPlayerCallback:Lcom/jrm/localmm/business/video/VideoPlayView$playerCallback;

.field private startSeekTime:J

.field private startTime:J

.field private viewId:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1    # Landroid/content/Context;

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;)V

    const-string v0, "VideoPlayView"

    iput-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->TAG:Ljava/lang/String;

    iput v2, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mCurrentState:I

    iput v2, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mTargetState:I

    iput-object v1, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    iput-object v1, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mMMediaPlayer:Lcom/mstar/android/media/MMediaPlayer;

    iput-object v1, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->myPlayerCallback:Lcom/jrm/localmm/business/video/VideoPlayView$playerCallback;

    iput-object v1, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mAudioManager:Landroid/media/AudioManager;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->isVoiceOpen:Z

    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->currentVoice:F

    iput v2, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->viewId:I

    new-instance v0, Lcom/jrm/localmm/business/video/VideoPlayView$2;

    invoke-direct {v0, p0}, Lcom/jrm/localmm/business/video/VideoPlayView$2;-><init>(Lcom/jrm/localmm/business/video/VideoPlayView;)V

    iput-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mSizeChangedListener:Landroid/media/MediaPlayer$OnVideoSizeChangedListener;

    new-instance v0, Lcom/jrm/localmm/business/video/VideoPlayView$3;

    invoke-direct {v0, p0}, Lcom/jrm/localmm/business/video/VideoPlayView$3;-><init>(Lcom/jrm/localmm/business/video/VideoPlayView;)V

    iput-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

    new-instance v0, Lcom/jrm/localmm/business/video/VideoPlayView$4;

    invoke-direct {v0, p0}, Lcom/jrm/localmm/business/video/VideoPlayView$4;-><init>(Lcom/jrm/localmm/business/video/VideoPlayView;)V

    iput-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    new-instance v0, Lcom/jrm/localmm/business/video/VideoPlayView$5;

    invoke-direct {v0, p0}, Lcom/jrm/localmm/business/video/VideoPlayView$5;-><init>(Lcom/jrm/localmm/business/video/VideoPlayView;)V

    iput-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    new-instance v0, Lcom/jrm/localmm/business/video/VideoPlayView$6;

    invoke-direct {v0, p0}, Lcom/jrm/localmm/business/video/VideoPlayView$6;-><init>(Lcom/jrm/localmm/business/video/VideoPlayView;)V

    iput-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mBufferingUpdateListener:Landroid/media/MediaPlayer$OnBufferingUpdateListener;

    new-instance v0, Lcom/jrm/localmm/business/video/VideoPlayView$7;

    invoke-direct {v0, p0}, Lcom/jrm/localmm/business/video/VideoPlayView$7;-><init>(Lcom/jrm/localmm/business/video/VideoPlayView;)V

    iput-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mInfoListener:Landroid/media/MediaPlayer$OnInfoListener;

    new-instance v0, Lcom/jrm/localmm/business/video/VideoPlayView$8;

    invoke-direct {v0, p0}, Lcom/jrm/localmm/business/video/VideoPlayView$8;-><init>(Lcom/jrm/localmm/business/video/VideoPlayView;)V

    iput-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mTimedTextListener:Landroid/media/MediaPlayer$OnTimedTextListener;

    new-instance v0, Lcom/jrm/localmm/business/video/VideoPlayView$9;

    invoke-direct {v0, p0}, Lcom/jrm/localmm/business/video/VideoPlayView$9;-><init>(Lcom/jrm/localmm/business/video/VideoPlayView;)V

    iput-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mMMediaPlayerSeekCompleteListener:Landroid/media/MediaPlayer$OnSeekCompleteListener;

    new-instance v0, Lcom/jrm/localmm/business/video/VideoPlayView$10;

    invoke-direct {v0, p0}, Lcom/jrm/localmm/business/video/VideoPlayView$10;-><init>(Lcom/jrm/localmm/business/video/VideoPlayView;)V

    iput-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mSHCallback:Landroid/view/SurfaceHolder$Callback;

    const-string v0, "audio"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mAudioManager:Landroid/media/AudioManager;

    invoke-direct {p0}, Lcom/jrm/localmm/business/video/VideoPlayView;->initVideoView()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/jrm/localmm/business/video/VideoPlayView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const-string v0, "audio"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mAudioManager:Landroid/media/AudioManager;

    invoke-direct {p0}, Lcom/jrm/localmm/business/video/VideoPlayView;->initVideoView()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const-string v0, "VideoPlayView"

    iput-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->TAG:Ljava/lang/String;

    iput v2, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mCurrentState:I

    iput v2, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mTargetState:I

    iput-object v1, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    iput-object v1, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mMMediaPlayer:Lcom/mstar/android/media/MMediaPlayer;

    iput-object v1, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->myPlayerCallback:Lcom/jrm/localmm/business/video/VideoPlayView$playerCallback;

    iput-object v1, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mAudioManager:Landroid/media/AudioManager;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->isVoiceOpen:Z

    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->currentVoice:F

    iput v2, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->viewId:I

    new-instance v0, Lcom/jrm/localmm/business/video/VideoPlayView$2;

    invoke-direct {v0, p0}, Lcom/jrm/localmm/business/video/VideoPlayView$2;-><init>(Lcom/jrm/localmm/business/video/VideoPlayView;)V

    iput-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mSizeChangedListener:Landroid/media/MediaPlayer$OnVideoSizeChangedListener;

    new-instance v0, Lcom/jrm/localmm/business/video/VideoPlayView$3;

    invoke-direct {v0, p0}, Lcom/jrm/localmm/business/video/VideoPlayView$3;-><init>(Lcom/jrm/localmm/business/video/VideoPlayView;)V

    iput-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

    new-instance v0, Lcom/jrm/localmm/business/video/VideoPlayView$4;

    invoke-direct {v0, p0}, Lcom/jrm/localmm/business/video/VideoPlayView$4;-><init>(Lcom/jrm/localmm/business/video/VideoPlayView;)V

    iput-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    new-instance v0, Lcom/jrm/localmm/business/video/VideoPlayView$5;

    invoke-direct {v0, p0}, Lcom/jrm/localmm/business/video/VideoPlayView$5;-><init>(Lcom/jrm/localmm/business/video/VideoPlayView;)V

    iput-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    new-instance v0, Lcom/jrm/localmm/business/video/VideoPlayView$6;

    invoke-direct {v0, p0}, Lcom/jrm/localmm/business/video/VideoPlayView$6;-><init>(Lcom/jrm/localmm/business/video/VideoPlayView;)V

    iput-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mBufferingUpdateListener:Landroid/media/MediaPlayer$OnBufferingUpdateListener;

    new-instance v0, Lcom/jrm/localmm/business/video/VideoPlayView$7;

    invoke-direct {v0, p0}, Lcom/jrm/localmm/business/video/VideoPlayView$7;-><init>(Lcom/jrm/localmm/business/video/VideoPlayView;)V

    iput-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mInfoListener:Landroid/media/MediaPlayer$OnInfoListener;

    new-instance v0, Lcom/jrm/localmm/business/video/VideoPlayView$8;

    invoke-direct {v0, p0}, Lcom/jrm/localmm/business/video/VideoPlayView$8;-><init>(Lcom/jrm/localmm/business/video/VideoPlayView;)V

    iput-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mTimedTextListener:Landroid/media/MediaPlayer$OnTimedTextListener;

    new-instance v0, Lcom/jrm/localmm/business/video/VideoPlayView$9;

    invoke-direct {v0, p0}, Lcom/jrm/localmm/business/video/VideoPlayView$9;-><init>(Lcom/jrm/localmm/business/video/VideoPlayView;)V

    iput-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mMMediaPlayerSeekCompleteListener:Landroid/media/MediaPlayer$OnSeekCompleteListener;

    new-instance v0, Lcom/jrm/localmm/business/video/VideoPlayView$10;

    invoke-direct {v0, p0}, Lcom/jrm/localmm/business/video/VideoPlayView$10;-><init>(Lcom/jrm/localmm/business/video/VideoPlayView;)V

    iput-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mSHCallback:Landroid/view/SurfaceHolder$Callback;

    const-string v0, "audio"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mAudioManager:Landroid/media/AudioManager;

    invoke-direct {p0}, Lcom/jrm/localmm/business/video/VideoPlayView;->initVideoView()V

    return-void
.end method

.method static synthetic access$000(Lcom/jrm/localmm/business/video/VideoPlayView;)Lcom/mstar/android/media/MMediaPlayer;
    .locals 1
    .param p0    # Lcom/jrm/localmm/business/video/VideoPlayView;

    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mMMediaPlayer:Lcom/mstar/android/media/MMediaPlayer;

    return-object v0
.end method

.method static synthetic access$002(Lcom/jrm/localmm/business/video/VideoPlayView;Lcom/mstar/android/media/MMediaPlayer;)Lcom/mstar/android/media/MMediaPlayer;
    .locals 0
    .param p0    # Lcom/jrm/localmm/business/video/VideoPlayView;
    .param p1    # Lcom/mstar/android/media/MMediaPlayer;

    iput-object p1, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mMMediaPlayer:Lcom/mstar/android/media/MMediaPlayer;

    return-object p1
.end method

.method static synthetic access$100(Lcom/jrm/localmm/business/video/VideoPlayView;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/jrm/localmm/business/video/VideoPlayView;

    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/jrm/localmm/business/video/VideoPlayView;)I
    .locals 1
    .param p0    # Lcom/jrm/localmm/business/video/VideoPlayView;

    iget v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mSurfaceHeight:I

    return v0
.end method

.method static synthetic access$1002(Lcom/jrm/localmm/business/video/VideoPlayView;I)I
    .locals 0
    .param p0    # Lcom/jrm/localmm/business/video/VideoPlayView;
    .param p1    # I

    iput p1, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mSurfaceHeight:I

    return p1
.end method

.method static synthetic access$1100(Lcom/jrm/localmm/business/video/VideoPlayView;)Landroid/media/AudioManager;
    .locals 1
    .param p0    # Lcom/jrm/localmm/business/video/VideoPlayView;

    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mAudioManager:Landroid/media/AudioManager;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/jrm/localmm/business/video/VideoPlayView;)J
    .locals 2
    .param p0    # Lcom/jrm/localmm/business/video/VideoPlayView;

    iget-wide v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->endSeekTime:J

    return-wide v0
.end method

.method static synthetic access$1202(Lcom/jrm/localmm/business/video/VideoPlayView;J)J
    .locals 0
    .param p0    # Lcom/jrm/localmm/business/video/VideoPlayView;
    .param p1    # J

    iput-wide p1, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->endSeekTime:J

    return-wide p1
.end method

.method static synthetic access$1300(Lcom/jrm/localmm/business/video/VideoPlayView;)J
    .locals 2
    .param p0    # Lcom/jrm/localmm/business/video/VideoPlayView;

    iget-wide v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->startSeekTime:J

    return-wide v0
.end method

.method static synthetic access$1402(Lcom/jrm/localmm/business/video/VideoPlayView;Landroid/view/SurfaceHolder;)Landroid/view/SurfaceHolder;
    .locals 0
    .param p0    # Lcom/jrm/localmm/business/video/VideoPlayView;
    .param p1    # Landroid/view/SurfaceHolder;

    iput-object p1, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    return-object p1
.end method

.method static synthetic access$1500(Lcom/jrm/localmm/business/video/VideoPlayView;)V
    .locals 0
    .param p0    # Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-direct {p0}, Lcom/jrm/localmm/business/video/VideoPlayView;->openPlayer()V

    return-void
.end method

.method static synthetic access$1600(Lcom/jrm/localmm/business/video/VideoPlayView;Z)V
    .locals 0
    .param p0    # Lcom/jrm/localmm/business/video/VideoPlayView;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/jrm/localmm/business/video/VideoPlayView;->release(Z)V

    return-void
.end method

.method static synthetic access$200(Lcom/jrm/localmm/business/video/VideoPlayView;)I
    .locals 1
    .param p0    # Lcom/jrm/localmm/business/video/VideoPlayView;

    iget v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mVideoWidth:I

    return v0
.end method

.method static synthetic access$202(Lcom/jrm/localmm/business/video/VideoPlayView;I)I
    .locals 0
    .param p0    # Lcom/jrm/localmm/business/video/VideoPlayView;
    .param p1    # I

    iput p1, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mVideoWidth:I

    return p1
.end method

.method static synthetic access$300(Lcom/jrm/localmm/business/video/VideoPlayView;)I
    .locals 1
    .param p0    # Lcom/jrm/localmm/business/video/VideoPlayView;

    iget v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mVideoHeight:I

    return v0
.end method

.method static synthetic access$302(Lcom/jrm/localmm/business/video/VideoPlayView;I)I
    .locals 0
    .param p0    # Lcom/jrm/localmm/business/video/VideoPlayView;
    .param p1    # I

    iput p1, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mVideoHeight:I

    return p1
.end method

.method static synthetic access$402(Lcom/jrm/localmm/business/video/VideoPlayView;I)I
    .locals 0
    .param p0    # Lcom/jrm/localmm/business/video/VideoPlayView;
    .param p1    # I

    iput p1, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mCurrentState:I

    return p1
.end method

.method static synthetic access$500(Lcom/jrm/localmm/business/video/VideoPlayView;)Lcom/jrm/localmm/business/video/VideoPlayView$playerCallback;
    .locals 1
    .param p0    # Lcom/jrm/localmm/business/video/VideoPlayView;

    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->myPlayerCallback:Lcom/jrm/localmm/business/video/VideoPlayView$playerCallback;

    return-object v0
.end method

.method static synthetic access$600(Lcom/jrm/localmm/business/video/VideoPlayView;)I
    .locals 1
    .param p0    # Lcom/jrm/localmm/business/video/VideoPlayView;

    iget v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->viewId:I

    return v0
.end method

.method static synthetic access$700(Lcom/jrm/localmm/business/video/VideoPlayView;)I
    .locals 1
    .param p0    # Lcom/jrm/localmm/business/video/VideoPlayView;

    iget v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mSeekWhenPrepared:I

    return v0
.end method

.method static synthetic access$800(Lcom/jrm/localmm/business/video/VideoPlayView;)I
    .locals 1
    .param p0    # Lcom/jrm/localmm/business/video/VideoPlayView;

    iget v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mTargetState:I

    return v0
.end method

.method static synthetic access$802(Lcom/jrm/localmm/business/video/VideoPlayView;I)I
    .locals 0
    .param p0    # Lcom/jrm/localmm/business/video/VideoPlayView;
    .param p1    # I

    iput p1, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mTargetState:I

    return p1
.end method

.method static synthetic access$900(Lcom/jrm/localmm/business/video/VideoPlayView;)I
    .locals 1
    .param p0    # Lcom/jrm/localmm/business/video/VideoPlayView;

    iget v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mSurfaceWidth:I

    return v0
.end method

.method static synthetic access$902(Lcom/jrm/localmm/business/video/VideoPlayView;I)I
    .locals 0
    .param p0    # Lcom/jrm/localmm/business/video/VideoPlayView;
    .param p1    # I

    iput p1, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mSurfaceWidth:I

    return p1
.end method

.method private errorCallback(I)V
    .locals 4
    .param p1    # I

    const/4 v0, -0x1

    iput v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mCurrentState:I

    iput v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mTargetState:I

    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->myPlayerCallback:Lcom/jrm/localmm/business/video/VideoPlayView$playerCallback;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->myPlayerCallback:Lcom/jrm/localmm/business/video/VideoPlayView$playerCallback;

    iget-object v1, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mMMediaPlayer:Lcom/mstar/android/media/MMediaPlayer;

    const/4 v2, 0x1

    iget v3, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->viewId:I

    invoke-interface {v0, v1, v2, p1, v3}, Lcom/jrm/localmm/business/video/VideoPlayView$playerCallback;->onError(Landroid/media/MediaPlayer;III)Z

    :cond_0
    return-void
.end method

.method private initVideoView()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    iput v2, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mVideoWidth:I

    iput v2, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mVideoHeight:I

    invoke-virtual {p0}, Lcom/jrm/localmm/business/video/VideoPlayView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    iget-object v1, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mSHCallback:Landroid/view/SurfaceHolder$Callback;

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    invoke-virtual {p0, v3}, Lcom/jrm/localmm/business/video/VideoPlayView;->setFocusable(Z)V

    invoke-virtual {p0, v3}, Lcom/jrm/localmm/business/video/VideoPlayView;->setFocusableInTouchMode(Z)V

    invoke-virtual {p0}, Lcom/jrm/localmm/business/video/VideoPlayView;->requestFocus()Z

    iput v2, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mCurrentState:I

    iput v2, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mTargetState:I

    return-void
.end method

.method private openPlayer()V
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    iget-object v2, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mUri:Landroid/net/Uri;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.android.music.musicservicecommand"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "command"

    const-string v3, "pause"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/jrm/localmm/business/video/VideoPlayView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    iget-object v2, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->myPlayerCallback:Lcom/jrm/localmm/business/video/VideoPlayView$playerCallback;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->myPlayerCallback:Lcom/jrm/localmm/business/video/VideoPlayView$playerCallback;

    invoke-interface {v2}, Lcom/jrm/localmm/business/video/VideoPlayView$playerCallback;->onCloseMusic()V

    :cond_2
    invoke-direct {p0, v5}, Lcom/jrm/localmm/business/video/VideoPlayView;->release(Z)V

    :try_start_0
    new-instance v2, Lcom/mstar/android/media/MMediaPlayer;

    invoke-direct {v2}, Lcom/mstar/android/media/MMediaPlayer;-><init>()V

    iput-object v2, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mMMediaPlayer:Lcom/mstar/android/media/MMediaPlayer;

    const/4 v2, -0x1

    iput v2, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mDuration:I

    iget-object v2, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mMMediaPlayer:Lcom/mstar/android/media/MMediaPlayer;

    iget-object v3, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

    invoke-virtual {v2, v3}, Lcom/mstar/android/media/MMediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    iget-object v2, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mMMediaPlayer:Lcom/mstar/android/media/MMediaPlayer;

    iget-object v3, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mSizeChangedListener:Landroid/media/MediaPlayer$OnVideoSizeChangedListener;

    invoke-virtual {v2, v3}, Lcom/mstar/android/media/MMediaPlayer;->setOnVideoSizeChangedListener(Landroid/media/MediaPlayer$OnVideoSizeChangedListener;)V

    iget-object v2, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mMMediaPlayer:Lcom/mstar/android/media/MMediaPlayer;

    iget-object v3, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    invoke-virtual {v2, v3}, Lcom/mstar/android/media/MMediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    iget-object v2, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mMMediaPlayer:Lcom/mstar/android/media/MMediaPlayer;

    iget-object v3, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    invoke-virtual {v2, v3}, Lcom/mstar/android/media/MMediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    iget-object v2, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mMMediaPlayer:Lcom/mstar/android/media/MMediaPlayer;

    iget-object v3, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mBufferingUpdateListener:Landroid/media/MediaPlayer$OnBufferingUpdateListener;

    invoke-virtual {v2, v3}, Lcom/mstar/android/media/MMediaPlayer;->setOnBufferingUpdateListener(Landroid/media/MediaPlayer$OnBufferingUpdateListener;)V

    iget-object v2, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mMMediaPlayer:Lcom/mstar/android/media/MMediaPlayer;

    iget-object v3, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mInfoListener:Landroid/media/MediaPlayer$OnInfoListener;

    invoke-virtual {v2, v3}, Lcom/mstar/android/media/MMediaPlayer;->setOnInfoListener(Landroid/media/MediaPlayer$OnInfoListener;)V

    iget-object v2, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mMMediaPlayer:Lcom/mstar/android/media/MMediaPlayer;

    iget-object v3, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mTimedTextListener:Landroid/media/MediaPlayer$OnTimedTextListener;

    invoke-virtual {v2, v3}, Lcom/mstar/android/media/MMediaPlayer;->setOnTimedTextListener(Landroid/media/MediaPlayer$OnTimedTextListener;)V

    iget-object v2, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mMMediaPlayer:Lcom/mstar/android/media/MMediaPlayer;

    iget-object v3, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mMMediaPlayerSeekCompleteListener:Landroid/media/MediaPlayer$OnSeekCompleteListener;

    invoke-virtual {v2, v3}, Lcom/mstar/android/media/MMediaPlayer;->setOnSeekCompleteListener(Landroid/media/MediaPlayer$OnSeekCompleteListener;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->startTime:J

    iget-object v2, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mMMediaPlayer:Lcom/mstar/android/media/MMediaPlayer;

    invoke-virtual {p0}, Lcom/jrm/localmm/business/video/VideoPlayView;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mUri:Landroid/net/Uri;

    invoke-virtual {v2, v3, v4}, Lcom/mstar/android/media/MMediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V

    iget-object v2, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mMMediaPlayer:Lcom/mstar/android/media/MMediaPlayer;

    iget-object v3, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    invoke-virtual {v2, v3}, Lcom/mstar/android/media/MMediaPlayer;->setDisplay(Landroid/view/SurfaceHolder;)V

    :cond_3
    iget v2, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->viewId:I

    if-ne v2, v6, :cond_5

    iget-object v2, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mMMediaPlayer:Lcom/mstar/android/media/MMediaPlayer;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Lcom/mstar/android/media/MMediaPlayer;->setAudioStreamType(I)V

    :cond_4
    :goto_1
    iget-object v2, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mMMediaPlayer:Lcom/mstar/android/media/MMediaPlayer;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/mstar/android/media/MMediaPlayer;->setScreenOnWhilePlaying(Z)V

    iget-object v2, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "***********prepareAsync: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mMMediaPlayer:Lcom/mstar/android/media/MMediaPlayer;

    invoke-virtual {v2}, Lcom/mstar/android/media/MMediaPlayer;->prepareAsync()V

    iget-object v2, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->TAG:Ljava/lang/String;

    const-string v3, "*******prepareAsync  end*****"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x1

    iput v2, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mCurrentState:I

    const/4 v2, 0x2

    iput v2, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mTargetState:I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_3

    goto/16 :goto_0

    :catch_0
    move-exception v0

    iget-object v2, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unable to open content: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mUri:Landroid/net/Uri;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    invoke-direct {p0, v5}, Lcom/jrm/localmm/business/video/VideoPlayView;->errorCallback(I)V

    goto/16 :goto_0

    :cond_5
    :try_start_1
    iget-object v2, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mMMediaPlayer:Lcom/mstar/android/media/MMediaPlayer;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/mstar/android/media/MMediaPlayer;->setAudioStreamType(I)V

    iget-object v2, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mAudioManager:Landroid/media/AudioManager;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->TAG:Ljava/lang/String;

    const-string v3, "*********DualAudioOn******openPlayer***"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mAudioManager:Landroid/media/AudioManager;

    const-string v3, "DualAudioOn"

    invoke-virtual {v2, v3}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_3

    goto :goto_1

    :catch_1
    move-exception v0

    iget-object v2, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unable to open content: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mUri:Landroid/net/Uri;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    invoke-direct {p0, v5}, Lcom/jrm/localmm/business/video/VideoPlayView;->errorCallback(I)V

    goto/16 :goto_0

    :catch_2
    move-exception v0

    iget-object v2, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unable to open content: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mUri:Landroid/net/Uri;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    invoke-direct {p0, v5}, Lcom/jrm/localmm/business/video/VideoPlayView;->errorCallback(I)V

    goto/16 :goto_0

    :catch_3
    move-exception v0

    iget-object v2, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unable to open content: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mUri:Landroid/net/Uri;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    invoke-direct {p0, v5}, Lcom/jrm/localmm/business/video/VideoPlayView;->errorCallback(I)V

    goto/16 :goto_0
.end method

.method private release(Z)V
    .locals 4
    .param p1    # Z

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "***********surfaceDestroyed1"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mTargetState:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mTargetState:I

    if-nez v0, :cond_1

    :goto_1
    return-void

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    iput v1, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mCurrentState:I

    if-eqz p1, :cond_2

    iput v1, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mTargetState:I

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/jrm/localmm/business/video/VideoPlayView$11;

    invoke-direct {v1, p0}, Lcom/jrm/localmm/business/video/VideoPlayView$11;-><init>(Lcom/jrm/localmm/business/video/VideoPlayView;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mMMediaPlayer:Lcom/mstar/android/media/MMediaPlayer;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->TAG:Ljava/lang/String;

    const-string v1, "***********openPlayer"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mMMediaPlayer:Lcom/mstar/android/media/MMediaPlayer;

    invoke-virtual {v0}, Lcom/mstar/android/media/MMediaPlayer;->release()V

    :cond_3
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mMMediaPlayer:Lcom/mstar/android/media/MMediaPlayer;

    goto :goto_1
.end method


# virtual methods
.method public addVoice(Z)V
    .locals 3
    .param p1    # Z

    const v2, 0x3dcccccd    # 0.1f

    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mMMediaPlayer:Lcom/mstar/android/media/MMediaPlayer;

    if-eqz v0, :cond_1

    if-eqz p1, :cond_2

    iget v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->currentVoice:F

    const/high16 v1, 0x3f800000    # 1.0f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    iget v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->currentVoice:F

    add-float/2addr v0, v2

    iput v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->currentVoice:F

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "******currentVoice*******"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->currentVoice:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mMMediaPlayer:Lcom/mstar/android/media/MMediaPlayer;

    iget v1, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->currentVoice:F

    iget v2, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->currentVoice:F

    invoke-virtual {v0, v1, v2}, Lcom/mstar/android/media/MMediaPlayer;->setVolume(FF)V

    :cond_1
    return-void

    :cond_2
    iget v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->currentVoice:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    iget v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->currentVoice:F

    sub-float/2addr v0, v2

    iput v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->currentVoice:F

    goto :goto_0
.end method

.method public getAllSubtitleTrackInfo()Lcom/mstar/android/media/SubtitleTrackInfo;
    .locals 2

    invoke-virtual {p0}, Lcom/jrm/localmm/business/video/VideoPlayView;->isInPlaybackState()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "****************"

    const-string v1, "***********mMMediaPlayer.getAllSubtitleTrackInfo()**************"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mMMediaPlayer:Lcom/mstar/android/media/MMediaPlayer;

    invoke-virtual {v0}, Lcom/mstar/android/media/MMediaPlayer;->getAllSubtitleTrackInfo()Lcom/mstar/android/media/SubtitleTrackInfo;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getAudioCodecType()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/jrm/localmm/business/video/VideoPlayView;->isInPlaybackState()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mMMediaPlayer:Lcom/mstar/android/media/MMediaPlayer;

    invoke-virtual {v0}, Lcom/mstar/android/media/MMediaPlayer;->getAudioCodecType()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getAudioTrackInfo(Z)Lcom/mstar/android/media/AudioTrackInfo;
    .locals 2
    .param p1    # Z

    invoke-virtual {p0}, Lcom/jrm/localmm/business/video/VideoPlayView;->isInPlaybackState()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->TAG:Ljava/lang/String;

    const-string v1, "***getAudioTrackInfo**"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mMMediaPlayer:Lcom/mstar/android/media/MMediaPlayer;

    invoke-virtual {v0, p1}, Lcom/mstar/android/media/MMediaPlayer;->getAudioTrackInfo(Z)Lcom/mstar/android/media/AudioTrackInfo;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCurrentPosition()I
    .locals 1

    invoke-virtual {p0}, Lcom/jrm/localmm/business/video/VideoPlayView;->isInPlaybackState()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mMMediaPlayer:Lcom/mstar/android/media/MMediaPlayer;

    invoke-virtual {v0}, Lcom/mstar/android/media/MMediaPlayer;->getCurrentPosition()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getDuration()I
    .locals 1

    invoke-virtual {p0}, Lcom/jrm/localmm/business/video/VideoPlayView;->isInPlaybackState()Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mDuration:I

    if-lez v0, :cond_0

    iget v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mDuration:I

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mMMediaPlayer:Lcom/mstar/android/media/MMediaPlayer;

    invoke-virtual {v0}, Lcom/mstar/android/media/MMediaPlayer;->getDuration()I

    move-result v0

    iput v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mDuration:I

    iget v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mDuration:I

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    iput v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mDuration:I

    iget v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mDuration:I

    goto :goto_0
.end method

.method public getMMediaPlayer()Lcom/mstar/android/media/MMediaPlayer;
    .locals 1

    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mMMediaPlayer:Lcom/mstar/android/media/MMediaPlayer;

    return-object v0
.end method

.method public getPlayMode()I
    .locals 1

    invoke-virtual {p0}, Lcom/jrm/localmm/business/video/VideoPlayView;->isInPlaybackState()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mMMediaPlayer:Lcom/mstar/android/media/MMediaPlayer;

    invoke-virtual {v0}, Lcom/mstar/android/media/MMediaPlayer;->getPlayMode()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x40

    goto :goto_0
.end method

.method public getStartTime()J
    .locals 2

    iget-wide v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->startTime:J

    return-wide v0
.end method

.method public getSubtitleData()Ljava/lang/String;
    .locals 2

    const-string v0, ""

    iget-object v1, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mMMediaPlayer:Lcom/mstar/android/media/MMediaPlayer;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mMMediaPlayer:Lcom/mstar/android/media/MMediaPlayer;

    invoke-virtual {v1}, Lcom/mstar/android/media/MMediaPlayer;->getSubtitleData()Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public getSubtitleTrackInfo(I)Lcom/mstar/android/media/SubtitleTrackInfo;
    .locals 1
    .param p1    # I

    invoke-virtual {p0}, Lcom/jrm/localmm/business/video/VideoPlayView;->isInPlaybackState()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mMMediaPlayer:Lcom/mstar/android/media/MMediaPlayer;

    invoke-virtual {v0, p1}, Lcom/mstar/android/media/MMediaPlayer;->getSubtitleTrackInfo(I)Lcom/mstar/android/media/SubtitleTrackInfo;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getVideoInfo()Lcom/mstar/android/media/VideoCodecInfo;
    .locals 1

    invoke-virtual {p0}, Lcom/jrm/localmm/business/video/VideoPlayView;->isInPlaybackState()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mMMediaPlayer:Lcom/mstar/android/media/MMediaPlayer;

    invoke-virtual {v0}, Lcom/mstar/android/media/MMediaPlayer;->getVideoInfo()Lcom/mstar/android/media/VideoCodecInfo;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getVoice()F
    .locals 2

    iget v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->currentVoice:F

    const/high16 v1, 0x41200000    # 10.0f

    mul-float/2addr v0, v1

    return v0
.end method

.method public isInPlaybackState()Z
    .locals 3

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mMMediaPlayer:Lcom/mstar/android/media/MMediaPlayer;

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mCurrentState:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    iget v1, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mCurrentState:I

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mCurrentState:I

    if-eq v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isMVCSource()Z
    .locals 7

    const/4 v2, 0x1

    const/4 v1, 0x0

    const/16 v6, 0x1b

    invoke-virtual {p0}, Lcom/jrm/localmm/business/video/VideoPlayView;->isInPlaybackState()Z

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    iget-object v3, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mMMediaPlayer:Lcom/mstar/android/media/MMediaPlayer;

    invoke-virtual {v3, v2, v2}, Lcom/mstar/android/media/MMediaPlayer;->getMetadata(ZZ)Landroid/media/Metadata;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, v6}, Landroid/media/Metadata;->has(I)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "**VIDEO_CODEC***"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0, v6}, Landroid/media/Metadata;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    invoke-virtual {v0, v6}, Landroid/media/Metadata;->has(I)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "MVC"

    invoke-virtual {v0, v6}, Landroid/media/Metadata;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v1, v2

    goto :goto_0
.end method

.method public isPlaying()Z
    .locals 1

    invoke-virtual {p0}, Lcom/jrm/localmm/business/video/VideoPlayView;->isInPlaybackState()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mMMediaPlayer:Lcom/mstar/android/media/MMediaPlayer;

    invoke-virtual {v0}, Lcom/mstar/android/media/MMediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public offSubtitleTrack()V
    .locals 1

    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mMMediaPlayer:Lcom/mstar/android/media/MMediaPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mMMediaPlayer:Lcom/mstar/android/media/MMediaPlayer;

    invoke-virtual {v0}, Lcom/mstar/android/media/MMediaPlayer;->offSubtitleTrack()V

    :cond_0
    return-void
.end method

.method public onSubtitleTrack()V
    .locals 1

    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mMMediaPlayer:Lcom/mstar/android/media/MMediaPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mMMediaPlayer:Lcom/mstar/android/media/MMediaPlayer;

    invoke-virtual {v0}, Lcom/mstar/android/media/MMediaPlayer;->onSubtitleTrack()V

    :cond_0
    return-void
.end method

.method public pause()V
    .locals 2

    const/4 v1, 0x4

    invoke-virtual {p0}, Lcom/jrm/localmm/business/video/VideoPlayView;->isInPlaybackState()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mMMediaPlayer:Lcom/mstar/android/media/MMediaPlayer;

    invoke-virtual {v0}, Lcom/mstar/android/media/MMediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mMMediaPlayer:Lcom/mstar/android/media/MMediaPlayer;

    invoke-virtual {v0}, Lcom/mstar/android/media/MMediaPlayer;->pause()V

    iput v1, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mCurrentState:I

    :cond_0
    iput v1, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mTargetState:I

    return-void
.end method

.method public seekTo(I)V
    .locals 2
    .param p1    # I

    invoke-virtual {p0}, Lcom/jrm/localmm/business/video/VideoPlayView;->isInPlaybackState()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->startSeekTime:J

    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mMMediaPlayer:Lcom/mstar/android/media/MMediaPlayer;

    invoke-virtual {v0, p1}, Lcom/mstar/android/media/MMediaPlayer;->seekTo(I)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mSeekWhenPrepared:I

    :goto_0
    return-void

    :cond_0
    iput p1, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mSeekWhenPrepared:I

    goto :goto_0
.end method

.method public setAudioTrack(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mMMediaPlayer:Lcom/mstar/android/media/MMediaPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mMMediaPlayer:Lcom/mstar/android/media/MMediaPlayer;

    invoke-virtual {v0, p1}, Lcom/mstar/android/media/MMediaPlayer;->setAudioTrack(I)V

    :cond_0
    return-void
.end method

.method public setPlayMode(I)Z
    .locals 3
    .param p1    # I

    const/4 v0, 0x0

    const/16 v1, -0x20

    if-lt p1, v1, :cond_0

    const/16 v1, 0x20

    if-le p1, v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lcom/jrm/localmm/business/video/VideoPlayView;->isInPlaybackState()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "****setPlayMode***"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mMMediaPlayer:Lcom/mstar/android/media/MMediaPlayer;

    invoke-virtual {v0, p1}, Lcom/mstar/android/media/MMediaPlayer;->setPlayMode(I)Z

    move-result v0

    goto :goto_0
.end method

.method public setPlayerCallbackListener(Lcom/jrm/localmm/business/video/VideoPlayView$playerCallback;)V
    .locals 0
    .param p1    # Lcom/jrm/localmm/business/video/VideoPlayView$playerCallback;

    iput-object p1, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->myPlayerCallback:Lcom/jrm/localmm/business/video/VideoPlayView$playerCallback;

    return-void
.end method

.method public setSubtitleDataSource(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mMMediaPlayer:Lcom/mstar/android/media/MMediaPlayer;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/jrm/localmm/business/video/VideoPlayView$12;

    invoke-direct {v1, p0, p1}, Lcom/jrm/localmm/business/video/VideoPlayView$12;-><init>(Lcom/jrm/localmm/business/video/VideoPlayView;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    :cond_0
    return-void
.end method

.method public setSubtitleDisplay(Landroid/view/SurfaceHolder;)V
    .locals 1
    .param p1    # Landroid/view/SurfaceHolder;

    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mMMediaPlayer:Lcom/mstar/android/media/MMediaPlayer;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mMMediaPlayer:Lcom/mstar/android/media/MMediaPlayer;

    invoke-virtual {v0, p1}, Lcom/mstar/android/media/MMediaPlayer;->setSubtitleDisplay(Landroid/view/SurfaceHolder;)V

    :cond_0
    return-void
.end method

.method public setSubtitleSync(I)I
    .locals 1
    .param p1    # I

    invoke-virtual {p0}, Lcom/jrm/localmm/business/video/VideoPlayView;->isInPlaybackState()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mMMediaPlayer:Lcom/mstar/android/media/MMediaPlayer;

    invoke-virtual {v0, p1}, Lcom/mstar/android/media/MMediaPlayer;->setSubtitleSync(I)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setSubtitleTrack(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mMMediaPlayer:Lcom/mstar/android/media/MMediaPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mMMediaPlayer:Lcom/mstar/android/media/MMediaPlayer;

    invoke-virtual {v0, p1}, Lcom/mstar/android/media/MMediaPlayer;->setSubtitleTrack(I)V

    :cond_0
    return-void
.end method

.method public setVideoPath(Ljava/lang/String;I)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # I

    iput p2, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->viewId:I

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mUri:Landroid/net/Uri;

    const/4 v0, 0x0

    iput v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mSeekWhenPrepared:I

    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "***********setVideoURI:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mUri:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/jrm/localmm/business/video/VideoPlayView;->openPlayer()V

    invoke-virtual {p0}, Lcom/jrm/localmm/business/video/VideoPlayView;->requestLayout()V

    invoke-virtual {p0}, Lcom/jrm/localmm/business/video/VideoPlayView;->invalidate()V

    return-void
.end method

.method public setVoice(I)V
    .locals 3
    .param p1    # I

    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mMMediaPlayer:Lcom/mstar/android/media/MMediaPlayer;

    if-eqz v0, :cond_1

    if-ltz p1, :cond_0

    const/16 v0, 0xa

    if-gt p1, v0, :cond_0

    int-to-float v0, p1

    const v1, 0x3dcccccd    # 0.1f

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->currentVoice:F

    :cond_0
    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "******currentVoice*******"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->currentVoice:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mMMediaPlayer:Lcom/mstar/android/media/MMediaPlayer;

    iget v1, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->currentVoice:F

    iget v2, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->currentVoice:F

    invoke-virtual {v0, v1, v2}, Lcom/mstar/android/media/MMediaPlayer;->setVolume(FF)V

    :cond_1
    return-void
.end method

.method public setVoice(Z)V
    .locals 3
    .param p1    # Z

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mMMediaPlayer:Lcom/mstar/android/media/MMediaPlayer;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mMMediaPlayer:Lcom/mstar/android/media/MMediaPlayer;

    iget v1, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->currentVoice:F

    iget v2, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->currentVoice:F

    invoke-virtual {v0, v1, v2}, Lcom/mstar/android/media/MMediaPlayer;->setVolume(FF)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->isVoiceOpen:Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mMMediaPlayer:Lcom/mstar/android/media/MMediaPlayer;

    invoke-virtual {v0, v1, v1}, Lcom/mstar/android/media/MMediaPlayer;->setVolume(FF)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->isVoiceOpen:Z

    goto :goto_0
.end method

.method public start()V
    .locals 2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/jrm/localmm/business/video/VideoPlayView;->isInPlaybackState()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mMMediaPlayer:Lcom/mstar/android/media/MMediaPlayer;

    invoke-virtual {v0}, Lcom/mstar/android/media/MMediaPlayer;->start()V

    iput v1, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mCurrentState:I

    :cond_0
    iput v1, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mTargetState:I

    return-void
.end method

.method public stopPlayback()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mMMediaPlayer:Lcom/mstar/android/media/MMediaPlayer;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mTargetState:I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mMMediaPlayer:Lcom/mstar/android/media/MMediaPlayer;

    invoke-virtual {v0}, Lcom/mstar/android/media/MMediaPlayer;->stop()V

    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->TAG:Ljava/lang/String;

    const-string v1, "***********stopPlayback: next"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mMMediaPlayer:Lcom/mstar/android/media/MMediaPlayer;

    invoke-virtual {v0}, Lcom/mstar/android/media/MMediaPlayer;->release()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mMMediaPlayer:Lcom/mstar/android/media/MMediaPlayer;

    iput v2, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mCurrentState:I

    iput v2, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mTargetState:I

    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mAudioManager:Landroid/media/AudioManager;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->viewId:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->TAG:Ljava/lang/String;

    const-string v1, "*********DualAudioOff******stopPlayback: next***"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mAudioManager:Landroid/media/AudioManager;

    const-string v1, "DualAudioOff"

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public stopPlayer()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mAudioManager:Landroid/media/AudioManager;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->viewId:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->TAG:Ljava/lang/String;

    const-string v1, "*********DualAudioOff******stopplayer***"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mAudioManager:Landroid/media/AudioManager;

    const-string v1, "DualAudioOff"

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mMMediaPlayer:Lcom/mstar/android/media/MMediaPlayer;

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mTargetState:I

    if-eqz v0, :cond_1

    iput v2, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mCurrentState:I

    iput v2, p0, Lcom/jrm/localmm/business/video/VideoPlayView;->mTargetState:I

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/jrm/localmm/business/video/VideoPlayView$1;

    invoke-direct {v1, p0}, Lcom/jrm/localmm/business/video/VideoPlayView$1;-><init>(Lcom/jrm/localmm/business/video/VideoPlayView;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    :cond_1
    return-void
.end method
