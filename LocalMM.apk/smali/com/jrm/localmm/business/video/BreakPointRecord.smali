.class public Lcom/jrm/localmm/business/video/BreakPointRecord;
.super Ljava/lang/Object;
.source "BreakPointRecord.java"


# static fields
.field private static dbAdapter:Lcom/jrm/localmm/business/video/BreakPointDBAdapter;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static addData(Ljava/lang/String;ILjava/lang/String;Landroid/content/Context;)V
    .locals 6
    .param p0    # Ljava/lang/String;
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # Landroid/content/Context;

    if-gtz p1, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v3, Lcom/jrm/localmm/business/video/BreakPointRecord;->dbAdapter:Lcom/jrm/localmm/business/video/BreakPointDBAdapter;

    if-nez v3, :cond_1

    new-instance v3, Lcom/jrm/localmm/business/video/BreakPointDBAdapter;

    invoke-direct {v3, p3}, Lcom/jrm/localmm/business/video/BreakPointDBAdapter;-><init>(Landroid/content/Context;)V

    sput-object v3, Lcom/jrm/localmm/business/video/BreakPointRecord;->dbAdapter:Lcom/jrm/localmm/business/video/BreakPointDBAdapter;

    sget-object v3, Lcom/jrm/localmm/business/video/BreakPointRecord;->dbAdapter:Lcom/jrm/localmm/business/video/BreakPointDBAdapter;

    invoke-virtual {v3}, Lcom/jrm/localmm/business/video/BreakPointDBAdapter;->open()V

    :cond_1
    invoke-virtual {p0}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v3, v4}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object p0

    const-string v3, "BreakPointRecord"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "****************Base64*****addData*******"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/jrm/localmm/business/video/BreakPointInfo;

    invoke-direct {v0}, Lcom/jrm/localmm/business/video/BreakPointInfo;-><init>()V

    iput-object p0, v0, Lcom/jrm/localmm/business/video/BreakPointInfo;->Path:Ljava/lang/String;

    iput p1, v0, Lcom/jrm/localmm/business/video/BreakPointInfo;->Time:I

    iput-object p2, v0, Lcom/jrm/localmm/business/video/BreakPointInfo;->Size:Ljava/lang/String;

    sget-object v3, Lcom/jrm/localmm/business/video/BreakPointRecord;->dbAdapter:Lcom/jrm/localmm/business/video/BreakPointDBAdapter;

    invoke-virtual {v3, p0, p2}, Lcom/jrm/localmm/business/video/BreakPointDBAdapter;->deleteOneData(Ljava/lang/String;Ljava/lang/String;)J

    sget-object v3, Lcom/jrm/localmm/business/video/BreakPointRecord;->dbAdapter:Lcom/jrm/localmm/business/video/BreakPointDBAdapter;

    invoke-virtual {v3, v0}, Lcom/jrm/localmm/business/video/BreakPointDBAdapter;->insert(Lcom/jrm/localmm/business/video/BreakPointInfo;)J

    move-result-wide v1

    const-wide/16 v3, -0x1

    cmp-long v3, v1, v3

    if-nez v3, :cond_2

    const-string v3, "BreakPointRecord"

    const-string v4, "********add breakPoint fail!********"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    sget-object v3, Lcom/jrm/localmm/business/video/BreakPointRecord;->dbAdapter:Lcom/jrm/localmm/business/video/BreakPointDBAdapter;

    invoke-virtual {v3}, Lcom/jrm/localmm/business/video/BreakPointDBAdapter;->queryAllData()[Lcom/jrm/localmm/business/video/BreakPointInfo;

    goto :goto_0

    :cond_2
    const-string v3, "BreakPointRecord"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "********add breakPoint success!******** ID: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public static closeDB()V
    .locals 1

    sget-object v0, Lcom/jrm/localmm/business/video/BreakPointRecord;->dbAdapter:Lcom/jrm/localmm/business/video/BreakPointDBAdapter;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/jrm/localmm/business/video/BreakPointRecord;->dbAdapter:Lcom/jrm/localmm/business/video/BreakPointDBAdapter;

    invoke-virtual {v0}, Lcom/jrm/localmm/business/video/BreakPointDBAdapter;->close()V

    const/4 v0, 0x0

    sput-object v0, Lcom/jrm/localmm/business/video/BreakPointRecord;->dbAdapter:Lcom/jrm/localmm/business/video/BreakPointDBAdapter;

    :cond_0
    return-void
.end method

.method public static getBreakPointFlag(Landroid/content/Context;)Z
    .locals 9
    .param p0    # Landroid/content/Context;

    const/4 v5, 0x1

    const/4 v6, 0x0

    :try_start_0
    const-string v7, "a.txt"

    invoke-virtual {p0, v7}, Landroid/content/Context;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;

    move-result-object v2

    new-instance v4, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v4}, Ljava/io/ByteArrayOutputStream;-><init>()V

    const/16 v7, 0x400

    new-array v0, v7, [B

    const/4 v3, -0x1

    :goto_0
    invoke-virtual {v2, v0}, Ljava/io/FileInputStream;->read([B)I

    move-result v3

    const/4 v7, -0x1

    if-eq v3, v7, :cond_1

    const/4 v7, 0x0

    invoke-virtual {v4, v0, v7, v3}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    goto :goto_0

    :catch_0
    move-exception v1

    :cond_0
    :goto_1
    return v5

    :cond_1
    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->close()V

    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V

    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->toString()Ljava/lang/String;

    move-result-object v7

    const-string v8, "true"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v7

    if-nez v7, :cond_0

    move v5, v6

    goto :goto_1
.end method

.method public static getData(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)I
    .locals 5
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/content/Context;

    const/4 v1, 0x0

    sget-object v2, Lcom/jrm/localmm/business/video/BreakPointRecord;->dbAdapter:Lcom/jrm/localmm/business/video/BreakPointDBAdapter;

    if-nez v2, :cond_0

    new-instance v2, Lcom/jrm/localmm/business/video/BreakPointDBAdapter;

    invoke-direct {v2, p2}, Lcom/jrm/localmm/business/video/BreakPointDBAdapter;-><init>(Landroid/content/Context;)V

    sput-object v2, Lcom/jrm/localmm/business/video/BreakPointRecord;->dbAdapter:Lcom/jrm/localmm/business/video/BreakPointDBAdapter;

    sget-object v2, Lcom/jrm/localmm/business/video/BreakPointRecord;->dbAdapter:Lcom/jrm/localmm/business/video/BreakPointDBAdapter;

    invoke-virtual {v2}, Lcom/jrm/localmm/business/video/BreakPointDBAdapter;->open()V

    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    invoke-static {v2, v1}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object p0

    const-string v2, "BreakPointRecord"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "****************Base64******getData******"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v2, Lcom/jrm/localmm/business/video/BreakPointRecord;->dbAdapter:Lcom/jrm/localmm/business/video/BreakPointDBAdapter;

    invoke-virtual {v2, p0, p1}, Lcom/jrm/localmm/business/video/BreakPointDBAdapter;->queryOneData(Ljava/lang/String;Ljava/lang/String;)[Lcom/jrm/localmm/business/video/BreakPointInfo;

    move-result-object v0

    if-eqz v0, :cond_1

    aget-object v1, v0, v1

    iget v1, v1, Lcom/jrm/localmm/business/video/BreakPointInfo;->Time:I

    :cond_1
    return v1
.end method

.method public static setBreakPointFlag(Landroid/content/Context;Ljava/lang/String;)V
    .locals 4
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;

    :try_start_0
    const-string v2, "a.txt"

    const/4 v3, 0x1

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/FileOutputStream;->write([B)V

    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method
