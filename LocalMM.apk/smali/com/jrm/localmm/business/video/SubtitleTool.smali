.class public Lcom/jrm/localmm/business/video/SubtitleTool;
.super Ljava/lang/Object;
.source "SubtitleTool.java"


# static fields
.field private static SUBTITLE_FORMATE_ARRAY:[Ljava/lang/String;

.field public static currentPipSubSize:F

.field public static currentSubSize:F


# instance fields
.field private VideoPath:Ljava/lang/String;

.field private cuurentSubtitleType:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    sput-object v0, Lcom/jrm/localmm/business/video/SubtitleTool;->SUBTITLE_FORMATE_ARRAY:[Ljava/lang/String;

    const/high16 v0, 0x41200000    # 10.0f

    sput v0, Lcom/jrm/localmm/business/video/SubtitleTool;->currentPipSubSize:F

    const/high16 v0, 0x420c0000    # 35.0f

    sput v0, Lcom/jrm/localmm/business/video/SubtitleTool;->currentSubSize:F

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v2, p0, Lcom/jrm/localmm/business/video/SubtitleTool;->cuurentSubtitleType:I

    const-string v0, ""

    iput-object v0, p0, Lcom/jrm/localmm/business/video/SubtitleTool;->VideoPath:Ljava/lang/String;

    iput-object p1, p0, Lcom/jrm/localmm/business/video/SubtitleTool;->VideoPath:Ljava/lang/String;

    sget-object v0, Lcom/jrm/localmm/business/video/SubtitleTool;->SUBTITLE_FORMATE_ARRAY:[Ljava/lang/String;

    const-string v1, ""

    aput-object v1, v0, v2

    sget-object v0, Lcom/jrm/localmm/business/video/SubtitleTool;->SUBTITLE_FORMATE_ARRAY:[Ljava/lang/String;

    const/4 v1, 0x1

    const-string v2, ".srt"

    aput-object v2, v0, v1

    sget-object v0, Lcom/jrm/localmm/business/video/SubtitleTool;->SUBTITLE_FORMATE_ARRAY:[Ljava/lang/String;

    const/4 v1, 0x2

    const-string v2, ".ssa"

    aput-object v2, v0, v1

    sget-object v0, Lcom/jrm/localmm/business/video/SubtitleTool;->SUBTITLE_FORMATE_ARRAY:[Ljava/lang/String;

    const/4 v1, 0x3

    const-string v2, ".ass"

    aput-object v2, v0, v1

    sget-object v0, Lcom/jrm/localmm/business/video/SubtitleTool;->SUBTITLE_FORMATE_ARRAY:[Ljava/lang/String;

    const/4 v1, 0x4

    const-string v2, ".smi"

    aput-object v2, v0, v1

    sget-object v0, Lcom/jrm/localmm/business/video/SubtitleTool;->SUBTITLE_FORMATE_ARRAY:[Ljava/lang/String;

    const/4 v1, 0x5

    const-string v2, ".txt"

    aput-object v2, v0, v1

    sget-object v0, Lcom/jrm/localmm/business/video/SubtitleTool;->SUBTITLE_FORMATE_ARRAY:[Ljava/lang/String;

    const/4 v1, 0x6

    const-string v2, ".idx"

    aput-object v2, v0, v1

    sget-object v0, Lcom/jrm/localmm/business/video/SubtitleTool;->SUBTITLE_FORMATE_ARRAY:[Ljava/lang/String;

    const/4 v1, 0x7

    const-string v2, ".sub"

    aput-object v2, v0, v1

    return-void
.end method

.method private FilterFormat(Ljava/util/ArrayList;I)Ljava/util/ArrayList;
    .locals 5
    .param p2    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;I)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v0, v3, :cond_1

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/jrm/localmm/business/video/SubtitleTool;->SUBTITLE_FORMATE_ARRAY:[Ljava/lang/String;

    aget-object v4, v4, p2

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method public static getCurrentSubSize()F
    .locals 1

    sget v0, Lcom/jrm/localmm/business/video/SubtitleTool;->currentSubSize:F

    return v0
.end method

.method private scan([Ljava/io/File;Ljava/util/ArrayList;)V
    .locals 8
    .param p1    # [Ljava/io/File;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/io/File;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    move-object v1, p1

    array-length v5, v1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v5, :cond_3

    aget-object v2, v1, v3

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2}, Ljava/io/File;->isDirectory()Z

    move-result v6

    if-eqz v6, :cond_1

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    const/4 v4, 0x1

    :goto_1
    sget-object v6, Lcom/jrm/localmm/business/video/SubtitleTool;->SUBTITLE_FORMATE_ARRAY:[Ljava/lang/String;

    array-length v6, v6

    if-ge v4, v6, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v6

    sget-object v7, Lcom/jrm/localmm/business/video/SubtitleTool;->SUBTITLE_FORMATE_ARRAY:[Ljava/lang/String;

    aget-object v7, v7, v4

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_3
    return-void
.end method

.method public static setCurrentPIPSubSize(F)V
    .locals 1
    .param p0    # F

    sput p0, Lcom/jrm/localmm/business/video/SubtitleTool;->currentPipSubSize:F

    const/high16 v0, 0x41700000    # 15.0f

    cmpl-float v0, p0, v0

    if-nez v0, :cond_0

    const/high16 v0, 0x42340000    # 45.0f

    sput v0, Lcom/jrm/localmm/business/video/SubtitleTool;->currentSubSize:F

    :goto_0
    return-void

    :cond_0
    const/high16 v0, 0x41200000    # 10.0f

    cmpl-float v0, p0, v0

    if-nez v0, :cond_1

    const/high16 v0, 0x420c0000    # 35.0f

    sput v0, Lcom/jrm/localmm/business/video/SubtitleTool;->currentSubSize:F

    goto :goto_0

    :cond_1
    const/high16 v0, 0x41c80000    # 25.0f

    sput v0, Lcom/jrm/localmm/business/video/SubtitleTool;->currentSubSize:F

    goto :goto_0
.end method


# virtual methods
.method public getSubtitlePathList(I)Ljava/util/ArrayList;
    .locals 6
    .param p1    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iget-object v4, p0, Lcom/jrm/localmm/business/video/SubtitleTool;->VideoPath:Ljava/lang/String;

    sget-object v5, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v4

    add-int/lit8 v1, v4, 0x1

    iget-object v4, p0, Lcom/jrm/localmm/business/video/SubtitleTool;->VideoPath:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {v4, v5, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v4

    invoke-direct {p0, v4, v2}, Lcom/jrm/localmm/business/video/SubtitleTool;->scan([Ljava/io/File;Ljava/util/ArrayList;)V

    if-nez p1, :cond_0

    :goto_0
    return-object v2

    :cond_0
    invoke-direct {p0, v2, p1}, Lcom/jrm/localmm/business/video/SubtitleTool;->FilterFormat(Ljava/util/ArrayList;I)Ljava/util/ArrayList;

    move-result-object v2

    goto :goto_0
.end method
