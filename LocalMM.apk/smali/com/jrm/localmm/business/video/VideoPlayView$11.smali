.class Lcom/jrm/localmm/business/video/VideoPlayView$11;
.super Ljava/lang/Object;
.source "VideoPlayView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/jrm/localmm/business/video/VideoPlayView;->release(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/localmm/business/video/VideoPlayView;


# direct methods
.method constructor <init>(Lcom/jrm/localmm/business/video/VideoPlayView;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/localmm/business/video/VideoPlayView$11;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    iget-object v2, p0, Lcom/jrm/localmm/business/video/VideoPlayView$11;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    # getter for: Lcom/jrm/localmm/business/video/VideoPlayView;->mMMediaPlayer:Lcom/mstar/android/media/MMediaPlayer;
    invoke-static {v2}, Lcom/jrm/localmm/business/video/VideoPlayView;->access$000(Lcom/jrm/localmm/business/video/VideoPlayView;)Lcom/mstar/android/media/MMediaPlayer;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/jrm/localmm/business/video/VideoPlayView$11;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    # getter for: Lcom/jrm/localmm/business/video/VideoPlayView;->mMMediaPlayer:Lcom/mstar/android/media/MMediaPlayer;
    invoke-static {v2}, Lcom/jrm/localmm/business/video/VideoPlayView;->access$000(Lcom/jrm/localmm/business/video/VideoPlayView;)Lcom/mstar/android/media/MMediaPlayer;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/media/MMediaPlayer;->isPlaying()Z

    move-result v2

    if-eqz v2, :cond_0

    :try_start_0
    iget-object v2, p0, Lcom/jrm/localmm/business/video/VideoPlayView$11;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    # getter for: Lcom/jrm/localmm/business/video/VideoPlayView;->mMMediaPlayer:Lcom/mstar/android/media/MMediaPlayer;
    invoke-static {v2}, Lcom/jrm/localmm/business/video/VideoPlayView;->access$000(Lcom/jrm/localmm/business/video/VideoPlayView;)Lcom/mstar/android/media/MMediaPlayer;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/media/MMediaPlayer;->stop()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/jrm/localmm/business/video/VideoPlayView$11;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    # getter for: Lcom/jrm/localmm/business/video/VideoPlayView;->mMMediaPlayer:Lcom/mstar/android/media/MMediaPlayer;
    invoke-static {v2}, Lcom/jrm/localmm/business/video/VideoPlayView;->access$000(Lcom/jrm/localmm/business/video/VideoPlayView;)Lcom/mstar/android/media/MMediaPlayer;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/jrm/localmm/business/video/VideoPlayView$11;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    # getter for: Lcom/jrm/localmm/business/video/VideoPlayView;->TAG:Ljava/lang/String;
    invoke-static {v2}, Lcom/jrm/localmm/business/video/VideoPlayView;->access$100(Lcom/jrm/localmm/business/video/VideoPlayView;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "*****release start*****"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/jrm/localmm/business/video/VideoPlayView$11;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    # getter for: Lcom/jrm/localmm/business/video/VideoPlayView;->mMMediaPlayer:Lcom/mstar/android/media/MMediaPlayer;
    invoke-static {v2}, Lcom/jrm/localmm/business/video/VideoPlayView;->access$000(Lcom/jrm/localmm/business/video/VideoPlayView;)Lcom/mstar/android/media/MMediaPlayer;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/media/MMediaPlayer;->release()V

    iget-object v2, p0, Lcom/jrm/localmm/business/video/VideoPlayView$11;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    # getter for: Lcom/jrm/localmm/business/video/VideoPlayView;->TAG:Ljava/lang/String;
    invoke-static {v2}, Lcom/jrm/localmm/business/video/VideoPlayView;->access$100(Lcom/jrm/localmm/business/video/VideoPlayView;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "*****release end*****"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    iget-object v2, p0, Lcom/jrm/localmm/business/video/VideoPlayView$11;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    const/4 v3, 0x0

    # setter for: Lcom/jrm/localmm/business/video/VideoPlayView;->mMMediaPlayer:Lcom/mstar/android/media/MMediaPlayer;
    invoke-static {v2, v3}, Lcom/jrm/localmm/business/video/VideoPlayView;->access$002(Lcom/jrm/localmm/business/video/VideoPlayView;Lcom/mstar/android/media/MMediaPlayer;)Lcom/mstar/android/media/MMediaPlayer;

    return-void

    :catch_0
    move-exception v0

    iget-object v2, p0, Lcom/jrm/localmm/business/video/VideoPlayView$11;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    # getter for: Lcom/jrm/localmm/business/video/VideoPlayView;->TAG:Ljava/lang/String;
    invoke-static {v2}, Lcom/jrm/localmm/business/video/VideoPlayView;->access$100(Lcom/jrm/localmm/business/video/VideoPlayView;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "stop fail! please try again!"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-wide/16 v2, 0x7d0

    :try_start_1
    invoke-virtual {p0, v2, v3}, Ljava/lang/Object;->wait(J)V

    iget-object v2, p0, Lcom/jrm/localmm/business/video/VideoPlayView$11;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    # getter for: Lcom/jrm/localmm/business/video/VideoPlayView;->mMMediaPlayer:Lcom/mstar/android/media/MMediaPlayer;
    invoke-static {v2}, Lcom/jrm/localmm/business/video/VideoPlayView;->access$000(Lcom/jrm/localmm/business/video/VideoPlayView;)Lcom/mstar/android/media/MMediaPlayer;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/media/MMediaPlayer;->stop()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method
