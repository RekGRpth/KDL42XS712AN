.class Lcom/jrm/localmm/business/video/VideoPlayView$10;
.super Ljava/lang/Object;
.source "VideoPlayView.java"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jrm/localmm/business/video/VideoPlayView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/localmm/business/video/VideoPlayView;


# direct methods
.method constructor <init>(Lcom/jrm/localmm/business/video/VideoPlayView;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/localmm/business/video/VideoPlayView$10;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 7
    .param p1    # Landroid/view/SurfaceHolder;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/jrm/localmm/business/video/VideoPlayView$10;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    # setter for: Lcom/jrm/localmm/business/video/VideoPlayView;->mSurfaceHolder:Landroid/view/SurfaceHolder;
    invoke-static {v4, p1}, Lcom/jrm/localmm/business/video/VideoPlayView;->access$1402(Lcom/jrm/localmm/business/video/VideoPlayView;Landroid/view/SurfaceHolder;)Landroid/view/SurfaceHolder;

    iget-object v4, p0, Lcom/jrm/localmm/business/video/VideoPlayView$10;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    # getter for: Lcom/jrm/localmm/business/video/VideoPlayView;->TAG:Ljava/lang/String;
    invoke-static {v4}, Lcom/jrm/localmm/business/video/VideoPlayView;->access$100(Lcom/jrm/localmm/business/video/VideoPlayView;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "*************surfaceChanged************"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/jrm/localmm/business/video/VideoPlayView$10;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    # setter for: Lcom/jrm/localmm/business/video/VideoPlayView;->mSurfaceWidth:I
    invoke-static {v4, p3}, Lcom/jrm/localmm/business/video/VideoPlayView;->access$902(Lcom/jrm/localmm/business/video/VideoPlayView;I)I

    iget-object v4, p0, Lcom/jrm/localmm/business/video/VideoPlayView$10;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    # setter for: Lcom/jrm/localmm/business/video/VideoPlayView;->mSurfaceHeight:I
    invoke-static {v4, p4}, Lcom/jrm/localmm/business/video/VideoPlayView;->access$1002(Lcom/jrm/localmm/business/video/VideoPlayView;I)I

    iget-object v4, p0, Lcom/jrm/localmm/business/video/VideoPlayView$10;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    # getter for: Lcom/jrm/localmm/business/video/VideoPlayView;->mTargetState:I
    invoke-static {v4}, Lcom/jrm/localmm/business/video/VideoPlayView;->access$800(Lcom/jrm/localmm/business/video/VideoPlayView;)I

    move-result v4

    const/4 v5, 0x3

    if-ne v4, v5, :cond_2

    move v1, v2

    :goto_0
    iget-object v4, p0, Lcom/jrm/localmm/business/video/VideoPlayView$10;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    # getter for: Lcom/jrm/localmm/business/video/VideoPlayView;->mVideoWidth:I
    invoke-static {v4}, Lcom/jrm/localmm/business/video/VideoPlayView;->access$200(Lcom/jrm/localmm/business/video/VideoPlayView;)I

    move-result v4

    if-ne v4, p3, :cond_3

    iget-object v4, p0, Lcom/jrm/localmm/business/video/VideoPlayView$10;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    # getter for: Lcom/jrm/localmm/business/video/VideoPlayView;->mVideoHeight:I
    invoke-static {v4}, Lcom/jrm/localmm/business/video/VideoPlayView;->access$300(Lcom/jrm/localmm/business/video/VideoPlayView;)I

    move-result v4

    if-ne v4, p4, :cond_3

    move v0, v2

    :goto_1
    iget-object v2, p0, Lcom/jrm/localmm/business/video/VideoPlayView$10;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    # getter for: Lcom/jrm/localmm/business/video/VideoPlayView;->mMMediaPlayer:Lcom/mstar/android/media/MMediaPlayer;
    invoke-static {v2}, Lcom/jrm/localmm/business/video/VideoPlayView;->access$000(Lcom/jrm/localmm/business/video/VideoPlayView;)Lcom/mstar/android/media/MMediaPlayer;

    move-result-object v2

    if-eqz v2, :cond_1

    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    iget-object v2, p0, Lcom/jrm/localmm/business/video/VideoPlayView$10;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    # getter for: Lcom/jrm/localmm/business/video/VideoPlayView;->mSeekWhenPrepared:I
    invoke-static {v2}, Lcom/jrm/localmm/business/video/VideoPlayView;->access$700(Lcom/jrm/localmm/business/video/VideoPlayView;)I

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/jrm/localmm/business/video/VideoPlayView$10;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    iget-object v3, p0, Lcom/jrm/localmm/business/video/VideoPlayView$10;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    # getter for: Lcom/jrm/localmm/business/video/VideoPlayView;->mSeekWhenPrepared:I
    invoke-static {v3}, Lcom/jrm/localmm/business/video/VideoPlayView;->access$700(Lcom/jrm/localmm/business/video/VideoPlayView;)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/jrm/localmm/business/video/VideoPlayView;->seekTo(I)V

    :cond_0
    iget-object v2, p0, Lcom/jrm/localmm/business/video/VideoPlayView$10;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-virtual {v2}, Lcom/jrm/localmm/business/video/VideoPlayView;->start()V

    :cond_1
    return-void

    :cond_2
    move v1, v3

    goto :goto_0

    :cond_3
    move v0, v3

    goto :goto_1
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 1
    .param p1    # Landroid/view/SurfaceHolder;

    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView$10;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    # setter for: Lcom/jrm/localmm/business/video/VideoPlayView;->mSurfaceHolder:Landroid/view/SurfaceHolder;
    invoke-static {v0, p1}, Lcom/jrm/localmm/business/video/VideoPlayView;->access$1402(Lcom/jrm/localmm/business/video/VideoPlayView;Landroid/view/SurfaceHolder;)Landroid/view/SurfaceHolder;

    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView$10;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    # invokes: Lcom/jrm/localmm/business/video/VideoPlayView;->openPlayer()V
    invoke-static {v0}, Lcom/jrm/localmm/business/video/VideoPlayView;->access$1500(Lcom/jrm/localmm/business/video/VideoPlayView;)V

    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 3
    .param p1    # Landroid/view/SurfaceHolder;

    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView$10;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    const/4 v1, 0x0

    # setter for: Lcom/jrm/localmm/business/video/VideoPlayView;->mSurfaceHolder:Landroid/view/SurfaceHolder;
    invoke-static {v0, v1}, Lcom/jrm/localmm/business/video/VideoPlayView;->access$1402(Lcom/jrm/localmm/business/video/VideoPlayView;Landroid/view/SurfaceHolder;)Landroid/view/SurfaceHolder;

    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView$10;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    # getter for: Lcom/jrm/localmm/business/video/VideoPlayView;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/jrm/localmm/business/video/VideoPlayView;->access$100(Lcom/jrm/localmm/business/video/VideoPlayView;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "*************surfaceDestroyed************"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/jrm/localmm/business/video/VideoPlayView$10;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    # getter for: Lcom/jrm/localmm/business/video/VideoPlayView;->viewId:I
    invoke-static {v2}, Lcom/jrm/localmm/business/video/VideoPlayView;->access$600(Lcom/jrm/localmm/business/video/VideoPlayView;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView$10;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    # getter for: Lcom/jrm/localmm/business/video/VideoPlayView;->mAudioManager:Landroid/media/AudioManager;
    invoke-static {v0}, Lcom/jrm/localmm/business/video/VideoPlayView;->access$1100(Lcom/jrm/localmm/business/video/VideoPlayView;)Landroid/media/AudioManager;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView$10;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    # getter for: Lcom/jrm/localmm/business/video/VideoPlayView;->viewId:I
    invoke-static {v0}, Lcom/jrm/localmm/business/video/VideoPlayView;->access$600(Lcom/jrm/localmm/business/video/VideoPlayView;)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView$10;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    # getter for: Lcom/jrm/localmm/business/video/VideoPlayView;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/jrm/localmm/business/video/VideoPlayView;->access$100(Lcom/jrm/localmm/business/video/VideoPlayView;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "*********DualAudioOff******surfaceDestroyed***"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView$10;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    # getter for: Lcom/jrm/localmm/business/video/VideoPlayView;->mAudioManager:Landroid/media/AudioManager;
    invoke-static {v0}, Lcom/jrm/localmm/business/video/VideoPlayView;->access$1100(Lcom/jrm/localmm/business/video/VideoPlayView;)Landroid/media/AudioManager;

    move-result-object v0

    const-string v1, "DualAudioOff"

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView$10;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    # getter for: Lcom/jrm/localmm/business/video/VideoPlayView;->viewId:I
    invoke-static {v0}, Lcom/jrm/localmm/business/video/VideoPlayView;->access$600(Lcom/jrm/localmm/business/video/VideoPlayView;)I

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView$10;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    const/4 v1, 0x1

    # invokes: Lcom/jrm/localmm/business/video/VideoPlayView;->release(Z)V
    invoke-static {v0, v1}, Lcom/jrm/localmm/business/video/VideoPlayView;->access$1600(Lcom/jrm/localmm/business/video/VideoPlayView;Z)V

    :cond_1
    return-void
.end method
