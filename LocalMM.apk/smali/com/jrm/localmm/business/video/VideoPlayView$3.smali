.class Lcom/jrm/localmm/business/video/VideoPlayView$3;
.super Ljava/lang/Object;
.source "VideoPlayView.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnPreparedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jrm/localmm/business/video/VideoPlayView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/localmm/business/video/VideoPlayView;


# direct methods
.method constructor <init>(Lcom/jrm/localmm/business/video/VideoPlayView;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/localmm/business/video/VideoPlayView$3;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPrepared(Landroid/media/MediaPlayer;)V
    .locals 5
    .param p1    # Landroid/media/MediaPlayer;

    const/4 v4, 0x3

    iget-object v1, p0, Lcom/jrm/localmm/business/video/VideoPlayView$3;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    const/4 v2, 0x2

    # setter for: Lcom/jrm/localmm/business/video/VideoPlayView;->mCurrentState:I
    invoke-static {v1, v2}, Lcom/jrm/localmm/business/video/VideoPlayView;->access$402(Lcom/jrm/localmm/business/video/VideoPlayView;I)I

    iget-object v1, p0, Lcom/jrm/localmm/business/video/VideoPlayView$3;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    # getter for: Lcom/jrm/localmm/business/video/VideoPlayView;->TAG:Ljava/lang/String;
    invoke-static {v1}, Lcom/jrm/localmm/business/video/VideoPlayView;->access$100(Lcom/jrm/localmm/business/video/VideoPlayView;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "******onPrepared*myPlayerCallback*****"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/jrm/localmm/business/video/VideoPlayView$3;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    # getter for: Lcom/jrm/localmm/business/video/VideoPlayView;->myPlayerCallback:Lcom/jrm/localmm/business/video/VideoPlayView$playerCallback;
    invoke-static {v3}, Lcom/jrm/localmm/business/video/VideoPlayView;->access$500(Lcom/jrm/localmm/business/video/VideoPlayView;)Lcom/jrm/localmm/business/video/VideoPlayView$playerCallback;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/jrm/localmm/business/video/VideoPlayView$3;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    # getter for: Lcom/jrm/localmm/business/video/VideoPlayView;->myPlayerCallback:Lcom/jrm/localmm/business/video/VideoPlayView$playerCallback;
    invoke-static {v1}, Lcom/jrm/localmm/business/video/VideoPlayView;->access$500(Lcom/jrm/localmm/business/video/VideoPlayView;)Lcom/jrm/localmm/business/video/VideoPlayView$playerCallback;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/jrm/localmm/business/video/VideoPlayView$3;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    # getter for: Lcom/jrm/localmm/business/video/VideoPlayView;->myPlayerCallback:Lcom/jrm/localmm/business/video/VideoPlayView$playerCallback;
    invoke-static {v1}, Lcom/jrm/localmm/business/video/VideoPlayView;->access$500(Lcom/jrm/localmm/business/video/VideoPlayView;)Lcom/jrm/localmm/business/video/VideoPlayView$playerCallback;

    move-result-object v1

    iget-object v2, p0, Lcom/jrm/localmm/business/video/VideoPlayView$3;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    # getter for: Lcom/jrm/localmm/business/video/VideoPlayView;->mMMediaPlayer:Lcom/mstar/android/media/MMediaPlayer;
    invoke-static {v2}, Lcom/jrm/localmm/business/video/VideoPlayView;->access$000(Lcom/jrm/localmm/business/video/VideoPlayView;)Lcom/mstar/android/media/MMediaPlayer;

    move-result-object v2

    iget-object v3, p0, Lcom/jrm/localmm/business/video/VideoPlayView$3;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    # getter for: Lcom/jrm/localmm/business/video/VideoPlayView;->viewId:I
    invoke-static {v3}, Lcom/jrm/localmm/business/video/VideoPlayView;->access$600(Lcom/jrm/localmm/business/video/VideoPlayView;)I

    move-result v3

    invoke-interface {v1, v2, v3}, Lcom/jrm/localmm/business/video/VideoPlayView$playerCallback;->onPrepared(Landroid/media/MediaPlayer;I)V

    :cond_0
    iget-object v1, p0, Lcom/jrm/localmm/business/video/VideoPlayView$3;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getVideoWidth()I

    move-result v2

    # setter for: Lcom/jrm/localmm/business/video/VideoPlayView;->mVideoWidth:I
    invoke-static {v1, v2}, Lcom/jrm/localmm/business/video/VideoPlayView;->access$202(Lcom/jrm/localmm/business/video/VideoPlayView;I)I

    iget-object v1, p0, Lcom/jrm/localmm/business/video/VideoPlayView$3;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getVideoHeight()I

    move-result v2

    # setter for: Lcom/jrm/localmm/business/video/VideoPlayView;->mVideoHeight:I
    invoke-static {v1, v2}, Lcom/jrm/localmm/business/video/VideoPlayView;->access$302(Lcom/jrm/localmm/business/video/VideoPlayView;I)I

    iget-object v1, p0, Lcom/jrm/localmm/business/video/VideoPlayView$3;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    # getter for: Lcom/jrm/localmm/business/video/VideoPlayView;->mSeekWhenPrepared:I
    invoke-static {v1}, Lcom/jrm/localmm/business/video/VideoPlayView;->access$700(Lcom/jrm/localmm/business/video/VideoPlayView;)I

    move-result v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/jrm/localmm/business/video/VideoPlayView$3;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-virtual {v1, v0}, Lcom/jrm/localmm/business/video/VideoPlayView;->seekTo(I)V

    :cond_1
    iget-object v1, p0, Lcom/jrm/localmm/business/video/VideoPlayView$3;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    # getter for: Lcom/jrm/localmm/business/video/VideoPlayView;->mVideoWidth:I
    invoke-static {v1}, Lcom/jrm/localmm/business/video/VideoPlayView;->access$200(Lcom/jrm/localmm/business/video/VideoPlayView;)I

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/jrm/localmm/business/video/VideoPlayView$3;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    # getter for: Lcom/jrm/localmm/business/video/VideoPlayView;->mVideoHeight:I
    invoke-static {v1}, Lcom/jrm/localmm/business/video/VideoPlayView;->access$300(Lcom/jrm/localmm/business/video/VideoPlayView;)I

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/jrm/localmm/business/video/VideoPlayView$3;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    # getter for: Lcom/jrm/localmm/business/video/VideoPlayView;->TAG:Ljava/lang/String;
    invoke-static {v1}, Lcom/jrm/localmm/business/video/VideoPlayView;->access$100(Lcom/jrm/localmm/business/video/VideoPlayView;)Ljava/lang/String;

    move-result-object v2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "video size: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/jrm/localmm/business/video/VideoPlayView$3;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    # getter for: Lcom/jrm/localmm/business/video/VideoPlayView;->mVideoWidth:I
    invoke-static {v3}, Lcom/jrm/localmm/business/video/VideoPlayView;->access$200(Lcom/jrm/localmm/business/video/VideoPlayView;)I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "/"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/jrm/localmm/business/video/VideoPlayView$3;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    # getter for: Lcom/jrm/localmm/business/video/VideoPlayView;->mVideoHeight:I
    invoke-static {v3}, Lcom/jrm/localmm/business/video/VideoPlayView;->access$300(Lcom/jrm/localmm/business/video/VideoPlayView;)I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " mTargetState == STATE_PLAYING ="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v1, p0, Lcom/jrm/localmm/business/video/VideoPlayView$3;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    # getter for: Lcom/jrm/localmm/business/video/VideoPlayView;->mTargetState:I
    invoke-static {v1}, Lcom/jrm/localmm/business/video/VideoPlayView;->access$800(Lcom/jrm/localmm/business/video/VideoPlayView;)I

    move-result v1

    if-ne v1, v4, :cond_3

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/jrm/localmm/business/video/VideoPlayView$3;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    # getter for: Lcom/jrm/localmm/business/video/VideoPlayView;->mSurfaceWidth:I
    invoke-static {v1}, Lcom/jrm/localmm/business/video/VideoPlayView;->access$900(Lcom/jrm/localmm/business/video/VideoPlayView;)I

    move-result v1

    iget-object v2, p0, Lcom/jrm/localmm/business/video/VideoPlayView$3;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    # getter for: Lcom/jrm/localmm/business/video/VideoPlayView;->mVideoWidth:I
    invoke-static {v2}, Lcom/jrm/localmm/business/video/VideoPlayView;->access$200(Lcom/jrm/localmm/business/video/VideoPlayView;)I

    move-result v2

    if-ne v1, v2, :cond_2

    iget-object v1, p0, Lcom/jrm/localmm/business/video/VideoPlayView$3;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    # getter for: Lcom/jrm/localmm/business/video/VideoPlayView;->mSurfaceHeight:I
    invoke-static {v1}, Lcom/jrm/localmm/business/video/VideoPlayView;->access$1000(Lcom/jrm/localmm/business/video/VideoPlayView;)I

    move-result v1

    iget-object v2, p0, Lcom/jrm/localmm/business/video/VideoPlayView$3;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    # getter for: Lcom/jrm/localmm/business/video/VideoPlayView;->mVideoHeight:I
    invoke-static {v2}, Lcom/jrm/localmm/business/video/VideoPlayView;->access$300(Lcom/jrm/localmm/business/video/VideoPlayView;)I

    move-result v2

    if-ne v1, v2, :cond_2

    iget-object v1, p0, Lcom/jrm/localmm/business/video/VideoPlayView$3;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    # getter for: Lcom/jrm/localmm/business/video/VideoPlayView;->mTargetState:I
    invoke-static {v1}, Lcom/jrm/localmm/business/video/VideoPlayView;->access$800(Lcom/jrm/localmm/business/video/VideoPlayView;)I

    move-result v1

    if-ne v1, v4, :cond_2

    iget-object v1, p0, Lcom/jrm/localmm/business/video/VideoPlayView$3;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-virtual {v1}, Lcom/jrm/localmm/business/video/VideoPlayView;->start()V

    :cond_2
    :goto_1
    return-void

    :cond_3
    const/4 v1, 0x0

    goto :goto_0

    :cond_4
    iget-object v1, p0, Lcom/jrm/localmm/business/video/VideoPlayView$3;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    # getter for: Lcom/jrm/localmm/business/video/VideoPlayView;->mTargetState:I
    invoke-static {v1}, Lcom/jrm/localmm/business/video/VideoPlayView;->access$800(Lcom/jrm/localmm/business/video/VideoPlayView;)I

    move-result v1

    if-ne v1, v4, :cond_2

    iget-object v1, p0, Lcom/jrm/localmm/business/video/VideoPlayView$3;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-virtual {v1}, Lcom/jrm/localmm/business/video/VideoPlayView;->start()V

    goto :goto_1
.end method
