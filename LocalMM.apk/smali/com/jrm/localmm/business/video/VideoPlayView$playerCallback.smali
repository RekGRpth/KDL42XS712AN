.class public interface abstract Lcom/jrm/localmm/business/video/VideoPlayView$playerCallback;
.super Ljava/lang/Object;
.source "VideoPlayView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jrm/localmm/business/video/VideoPlayView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "playerCallback"
.end annotation


# virtual methods
.method public abstract onBufferingUpdate(Landroid/media/MediaPlayer;I)V
.end method

.method public abstract onCloseMusic()V
.end method

.method public abstract onCompletion(Landroid/media/MediaPlayer;I)V
.end method

.method public abstract onError(Landroid/media/MediaPlayer;III)Z
.end method

.method public abstract onInfo(Landroid/media/MediaPlayer;III)Z
.end method

.method public abstract onPrepared(Landroid/media/MediaPlayer;I)V
.end method

.method public abstract onSeekComplete(Landroid/media/MediaPlayer;I)V
.end method

.method public abstract onUpdateSubtitle(Ljava/lang/String;)V
.end method
