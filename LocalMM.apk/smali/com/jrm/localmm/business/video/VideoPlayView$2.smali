.class Lcom/jrm/localmm/business/video/VideoPlayView$2;
.super Ljava/lang/Object;
.source "VideoPlayView.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnVideoSizeChangedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jrm/localmm/business/video/VideoPlayView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/localmm/business/video/VideoPlayView;


# direct methods
.method constructor <init>(Lcom/jrm/localmm/business/video/VideoPlayView;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/localmm/business/video/VideoPlayView$2;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onVideoSizeChanged(Landroid/media/MediaPlayer;II)V
    .locals 3
    .param p1    # Landroid/media/MediaPlayer;
    .param p2    # I
    .param p3    # I

    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView$2;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getVideoWidth()I

    move-result v1

    # setter for: Lcom/jrm/localmm/business/video/VideoPlayView;->mVideoWidth:I
    invoke-static {v0, v1}, Lcom/jrm/localmm/business/video/VideoPlayView;->access$202(Lcom/jrm/localmm/business/video/VideoPlayView;I)I

    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView$2;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getVideoHeight()I

    move-result v1

    # setter for: Lcom/jrm/localmm/business/video/VideoPlayView;->mVideoHeight:I
    invoke-static {v0, v1}, Lcom/jrm/localmm/business/video/VideoPlayView;->access$302(Lcom/jrm/localmm/business/video/VideoPlayView;I)I

    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView$2;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    # getter for: Lcom/jrm/localmm/business/video/VideoPlayView;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/jrm/localmm/business/video/VideoPlayView;->access$100(Lcom/jrm/localmm/business/video/VideoPlayView;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mVideoWidth: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/jrm/localmm/business/video/VideoPlayView$2;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    # getter for: Lcom/jrm/localmm/business/video/VideoPlayView;->mVideoWidth:I
    invoke-static {v2}, Lcom/jrm/localmm/business/video/VideoPlayView;->access$200(Lcom/jrm/localmm/business/video/VideoPlayView;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mVideoHeight "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/jrm/localmm/business/video/VideoPlayView$2;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    # getter for: Lcom/jrm/localmm/business/video/VideoPlayView;->mVideoHeight:I
    invoke-static {v2}, Lcom/jrm/localmm/business/video/VideoPlayView;->access$300(Lcom/jrm/localmm/business/video/VideoPlayView;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView$2;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    # getter for: Lcom/jrm/localmm/business/video/VideoPlayView;->mVideoWidth:I
    invoke-static {v0}, Lcom/jrm/localmm/business/video/VideoPlayView;->access$200(Lcom/jrm/localmm/business/video/VideoPlayView;)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView$2;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    # getter for: Lcom/jrm/localmm/business/video/VideoPlayView;->mVideoHeight:I
    invoke-static {v0}, Lcom/jrm/localmm/business/video/VideoPlayView;->access$300(Lcom/jrm/localmm/business/video/VideoPlayView;)I

    move-result v0

    if-eqz v0, :cond_0

    :cond_0
    return-void
.end method
