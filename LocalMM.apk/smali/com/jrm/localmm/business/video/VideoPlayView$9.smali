.class Lcom/jrm/localmm/business/video/VideoPlayView$9;
.super Ljava/lang/Object;
.source "VideoPlayView.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnSeekCompleteListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jrm/localmm/business/video/VideoPlayView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/localmm/business/video/VideoPlayView;


# direct methods
.method constructor <init>(Lcom/jrm/localmm/business/video/VideoPlayView;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/localmm/business/video/VideoPlayView$9;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSeekComplete(Landroid/media/MediaPlayer;)V
    .locals 6
    .param p1    # Landroid/media/MediaPlayer;

    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView$9;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    # setter for: Lcom/jrm/localmm/business/video/VideoPlayView;->endSeekTime:J
    invoke-static {v0, v1, v2}, Lcom/jrm/localmm/business/video/VideoPlayView;->access$1202(Lcom/jrm/localmm/business/video/VideoPlayView;J)J

    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView$9;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    # getter for: Lcom/jrm/localmm/business/video/VideoPlayView;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/jrm/localmm/business/video/VideoPlayView;->access$100(Lcom/jrm/localmm/business/video/VideoPlayView;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ">>>SeekComplete>>>>>>seek time : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/jrm/localmm/business/video/VideoPlayView$9;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    # getter for: Lcom/jrm/localmm/business/video/VideoPlayView;->endSeekTime:J
    invoke-static {v2}, Lcom/jrm/localmm/business/video/VideoPlayView;->access$1200(Lcom/jrm/localmm/business/video/VideoPlayView;)J

    move-result-wide v2

    iget-object v4, p0, Lcom/jrm/localmm/business/video/VideoPlayView$9;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    # getter for: Lcom/jrm/localmm/business/video/VideoPlayView;->startSeekTime:J
    invoke-static {v4}, Lcom/jrm/localmm/business/video/VideoPlayView;->access$1300(Lcom/jrm/localmm/business/video/VideoPlayView;)J

    move-result-wide v4

    sub-long/2addr v2, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ms"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView$9;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    # getter for: Lcom/jrm/localmm/business/video/VideoPlayView;->myPlayerCallback:Lcom/jrm/localmm/business/video/VideoPlayView$playerCallback;
    invoke-static {v0}, Lcom/jrm/localmm/business/video/VideoPlayView;->access$500(Lcom/jrm/localmm/business/video/VideoPlayView;)Lcom/jrm/localmm/business/video/VideoPlayView$playerCallback;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView$9;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    # getter for: Lcom/jrm/localmm/business/video/VideoPlayView;->myPlayerCallback:Lcom/jrm/localmm/business/video/VideoPlayView$playerCallback;
    invoke-static {v0}, Lcom/jrm/localmm/business/video/VideoPlayView;->access$500(Lcom/jrm/localmm/business/video/VideoPlayView;)Lcom/jrm/localmm/business/video/VideoPlayView$playerCallback;

    move-result-object v0

    iget-object v1, p0, Lcom/jrm/localmm/business/video/VideoPlayView$9;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    # getter for: Lcom/jrm/localmm/business/video/VideoPlayView;->viewId:I
    invoke-static {v1}, Lcom/jrm/localmm/business/video/VideoPlayView;->access$600(Lcom/jrm/localmm/business/video/VideoPlayView;)I

    move-result v1

    invoke-interface {v0, p1, v1}, Lcom/jrm/localmm/business/video/VideoPlayView$playerCallback;->onSeekComplete(Landroid/media/MediaPlayer;I)V

    :cond_0
    return-void
.end method
