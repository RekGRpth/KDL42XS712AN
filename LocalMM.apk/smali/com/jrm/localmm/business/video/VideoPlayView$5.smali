.class Lcom/jrm/localmm/business/video/VideoPlayView$5;
.super Ljava/lang/Object;
.source "VideoPlayView.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnErrorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jrm/localmm/business/video/VideoPlayView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/localmm/business/video/VideoPlayView;


# direct methods
.method constructor <init>(Lcom/jrm/localmm/business/video/VideoPlayView;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/localmm/business/video/VideoPlayView$5;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Landroid/media/MediaPlayer;II)Z
    .locals 5
    .param p1    # Landroid/media/MediaPlayer;
    .param p2    # I
    .param p3    # I

    const/4 v4, 0x1

    const/4 v3, -0x1

    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView$5;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    # getter for: Lcom/jrm/localmm/business/video/VideoPlayView;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/jrm/localmm/business/video/VideoPlayView;->access$100(Lcom/jrm/localmm/business/video/VideoPlayView;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView$5;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    # setter for: Lcom/jrm/localmm/business/video/VideoPlayView;->mCurrentState:I
    invoke-static {v0, v3}, Lcom/jrm/localmm/business/video/VideoPlayView;->access$402(Lcom/jrm/localmm/business/video/VideoPlayView;I)I

    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView$5;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    # setter for: Lcom/jrm/localmm/business/video/VideoPlayView;->mTargetState:I
    invoke-static {v0, v3}, Lcom/jrm/localmm/business/video/VideoPlayView;->access$802(Lcom/jrm/localmm/business/video/VideoPlayView;I)I

    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView$5;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    # getter for: Lcom/jrm/localmm/business/video/VideoPlayView;->myPlayerCallback:Lcom/jrm/localmm/business/video/VideoPlayView$playerCallback;
    invoke-static {v0}, Lcom/jrm/localmm/business/video/VideoPlayView;->access$500(Lcom/jrm/localmm/business/video/VideoPlayView;)Lcom/jrm/localmm/business/video/VideoPlayView$playerCallback;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/business/video/VideoPlayView$5;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    # getter for: Lcom/jrm/localmm/business/video/VideoPlayView;->myPlayerCallback:Lcom/jrm/localmm/business/video/VideoPlayView$playerCallback;
    invoke-static {v0}, Lcom/jrm/localmm/business/video/VideoPlayView;->access$500(Lcom/jrm/localmm/business/video/VideoPlayView;)Lcom/jrm/localmm/business/video/VideoPlayView$playerCallback;

    move-result-object v0

    iget-object v1, p0, Lcom/jrm/localmm/business/video/VideoPlayView$5;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    # getter for: Lcom/jrm/localmm/business/video/VideoPlayView;->mMMediaPlayer:Lcom/mstar/android/media/MMediaPlayer;
    invoke-static {v1}, Lcom/jrm/localmm/business/video/VideoPlayView;->access$000(Lcom/jrm/localmm/business/video/VideoPlayView;)Lcom/mstar/android/media/MMediaPlayer;

    move-result-object v1

    iget-object v2, p0, Lcom/jrm/localmm/business/video/VideoPlayView$5;->this$0:Lcom/jrm/localmm/business/video/VideoPlayView;

    # getter for: Lcom/jrm/localmm/business/video/VideoPlayView;->viewId:I
    invoke-static {v2}, Lcom/jrm/localmm/business/video/VideoPlayView;->access$600(Lcom/jrm/localmm/business/video/VideoPlayView;)I

    move-result v2

    invoke-interface {v0, v1, p2, p3, v2}, Lcom/jrm/localmm/business/video/VideoPlayView$playerCallback;->onError(Landroid/media/MediaPlayer;III)Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_0
    return v4
.end method
