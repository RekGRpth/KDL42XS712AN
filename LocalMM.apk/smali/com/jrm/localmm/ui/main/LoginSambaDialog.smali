.class public Lcom/jrm/localmm/ui/main/LoginSambaDialog;
.super Landroid/app/Dialog;
.source "LoginSambaDialog.java"


# instance fields
.field private cancel:Landroid/widget/Button;

.field private confirm:Landroid/widget/Button;

.field private handler:Landroid/os/Handler;

.field private isDismiss:Z

.field private onKeyListener:Landroid/view/View$OnKeyListener;

.field private onTouchListener:Landroid/view/View$OnTouchListener;

.field private password:Landroid/widget/EditText;

.field private user:Landroid/widget/EditText;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/LoginSambaDialog;->user:Landroid/widget/EditText;

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/LoginSambaDialog;->password:Landroid/widget/EditText;

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/LoginSambaDialog;->confirm:Landroid/widget/Button;

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/LoginSambaDialog;->cancel:Landroid/widget/Button;

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/LoginSambaDialog;->handler:Landroid/os/Handler;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/jrm/localmm/ui/main/LoginSambaDialog;->isDismiss:Z

    new-instance v0, Lcom/jrm/localmm/ui/main/LoginSambaDialog$1;

    invoke-direct {v0, p0}, Lcom/jrm/localmm/ui/main/LoginSambaDialog$1;-><init>(Lcom/jrm/localmm/ui/main/LoginSambaDialog;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/LoginSambaDialog;->onKeyListener:Landroid/view/View$OnKeyListener;

    new-instance v0, Lcom/jrm/localmm/ui/main/LoginSambaDialog$2;

    invoke-direct {v0, p0}, Lcom/jrm/localmm/ui/main/LoginSambaDialog$2;-><init>(Lcom/jrm/localmm/ui/main/LoginSambaDialog;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/LoginSambaDialog;->onTouchListener:Landroid/view/View$OnTouchListener;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/os/Handler;

    const/4 v0, 0x0

    invoke-direct {p0, p1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/LoginSambaDialog;->user:Landroid/widget/EditText;

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/LoginSambaDialog;->password:Landroid/widget/EditText;

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/LoginSambaDialog;->confirm:Landroid/widget/Button;

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/LoginSambaDialog;->cancel:Landroid/widget/Button;

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/LoginSambaDialog;->handler:Landroid/os/Handler;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/jrm/localmm/ui/main/LoginSambaDialog;->isDismiss:Z

    new-instance v0, Lcom/jrm/localmm/ui/main/LoginSambaDialog$1;

    invoke-direct {v0, p0}, Lcom/jrm/localmm/ui/main/LoginSambaDialog$1;-><init>(Lcom/jrm/localmm/ui/main/LoginSambaDialog;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/LoginSambaDialog;->onKeyListener:Landroid/view/View$OnKeyListener;

    new-instance v0, Lcom/jrm/localmm/ui/main/LoginSambaDialog$2;

    invoke-direct {v0, p0}, Lcom/jrm/localmm/ui/main/LoginSambaDialog$2;-><init>(Lcom/jrm/localmm/ui/main/LoginSambaDialog;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/LoginSambaDialog;->onTouchListener:Landroid/view/View$OnTouchListener;

    iput-object p2, p0, Lcom/jrm/localmm/ui/main/LoginSambaDialog;->handler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/jrm/localmm/ui/main/LoginSambaDialog;I)V
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/main/LoginSambaDialog;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/jrm/localmm/ui/main/LoginSambaDialog;->responseButton(I)V

    return-void
.end method

.method static synthetic access$100(Lcom/jrm/localmm/ui/main/LoginSambaDialog;)Z
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/main/LoginSambaDialog;

    iget-boolean v0, p0, Lcom/jrm/localmm/ui/main/LoginSambaDialog;->isDismiss:Z

    return v0
.end method

.method static synthetic access$102(Lcom/jrm/localmm/ui/main/LoginSambaDialog;Z)Z
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/main/LoginSambaDialog;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/jrm/localmm/ui/main/LoginSambaDialog;->isDismiss:Z

    return p1
.end method

.method private findViews()V
    .locals 2

    const v0, 0x7f080088    # com.jrm.localmm.R.id.samba_user_name

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/main/LoginSambaDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/LoginSambaDialog;->user:Landroid/widget/EditText;

    const v0, 0x7f080089    # com.jrm.localmm.R.id.samba_user_pass

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/main/LoginSambaDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/LoginSambaDialog;->password:Landroid/widget/EditText;

    const v0, 0x7f08008a    # com.jrm.localmm.R.id.samba_login

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/main/LoginSambaDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/LoginSambaDialog;->confirm:Landroid/widget/Button;

    const v0, 0x7f08008b    # com.jrm.localmm.R.id.samba_cancel

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/main/LoginSambaDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/LoginSambaDialog;->cancel:Landroid/widget/Button;

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/LoginSambaDialog;->user:Landroid/widget/EditText;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFocusable(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/LoginSambaDialog;->user:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/LoginSambaDialog;->confirm:Landroid/widget/Button;

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/LoginSambaDialog;->onKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/LoginSambaDialog;->cancel:Landroid/widget/Button;

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/LoginSambaDialog;->onKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/LoginSambaDialog;->confirm:Landroid/widget/Button;

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/LoginSambaDialog;->onTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/LoginSambaDialog;->cancel:Landroid/widget/Button;

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/LoginSambaDialog;->onTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    return-void
.end method

.method private responseButton(I)V
    .locals 7
    .param p1    # I

    new-instance v1, Landroid/os/Message;

    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v4, p0, Lcom/jrm/localmm/ui/main/LoginSambaDialog;->user:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "\\"

    const-string v5, "\\\\"

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/LoginSambaDialog;->password:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v4, "loginSamba2"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "user: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " pass: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v4, "USERNAME"

    invoke-virtual {v0, v4, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "PASSWORD"

    invoke-virtual {v0, v4, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    const/4 v4, 0x1

    iput v4, v1, Landroid/os/Message;->what:I

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/LoginSambaDialog;->handler:Landroid/os/Handler;

    invoke-virtual {v4, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    :pswitch_1
    new-instance v1, Landroid/os/Message;

    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    const/4 v4, 0x0

    iput v4, v1, Landroid/os/Message;->what:I

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/LoginSambaDialog;->handler:Landroid/os/Handler;

    invoke-virtual {v4, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7f08008a
        :pswitch_0    # com.jrm.localmm.R.id.samba_login
        :pswitch_1    # com.jrm.localmm.R.id.samba_cancel
    .end packed-switch
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 10
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    const v6, 0x7f030015    # com.jrm.localmm.R.layout.samba_input

    invoke-virtual {p0, v6}, Lcom/jrm/localmm/ui/main/LoginSambaDialog;->setContentView(I)V

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/main/LoginSambaDialog;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Window;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v6

    invoke-interface {v6}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    invoke-virtual {v0, v2}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    iget v6, v2, Landroid/graphics/Point;->x:I

    int-to-double v6, v6

    const-wide/high16 v8, 0x3fd0000000000000L    # 0.25

    mul-double/2addr v6, v8

    double-to-int v4, v6

    iget v6, v2, Landroid/graphics/Point;->y:I

    int-to-double v6, v6

    const-wide v8, 0x3fd6666666666666L    # 0.35

    mul-double/2addr v6, v8

    double-to-int v1, v6

    const/4 v6, 0x0

    invoke-virtual {v3, v6}, Landroid/view/Window;->setTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {v3, v4, v1}, Landroid/view/Window;->setLayout(II)V

    const/16 v6, 0x11

    invoke-virtual {v3, v6}, Landroid/view/Window;->setGravity(I)V

    invoke-virtual {v3}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    const/4 v6, 0x0

    iput-boolean v6, p0, Lcom/jrm/localmm/ui/main/LoginSambaDialog;->isDismiss:Z

    invoke-direct {p0}, Lcom/jrm/localmm/ui/main/LoginSambaDialog;->findViews()V

    return-void
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v1, 0x4

    if-ne p1, v1, :cond_0

    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    const/4 v1, 0x0

    iput v1, v0, Landroid/os/Message;->what:I

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/LoginSambaDialog;->handler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Dialog;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v1

    return v1
.end method
