.class Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;
.super Landroid/os/Handler;
.source "FileBrowserActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jrm/localmm/ui/main/FileBrowserActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;


# direct methods
.method constructor <init>(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 11
    .param p1    # Landroid/os/Message;

    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x4

    const/4 v7, 0x1

    const/4 v6, 0x0

    iget v4, p1, Landroid/os/Message;->what:I

    if-ne v4, v7, :cond_2

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    iget-object v5, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    iget v5, v5, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->dataSource:I

    # setter for: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->lastDataSource:I
    invoke-static {v4, v5}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$602(Lcom/jrm/localmm/ui/main/FileBrowserActivity;I)I

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->lastDataSource:I
    invoke-static {v4}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$600(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)I

    move-result v4

    if-ne v4, v8, :cond_0

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->holder:Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;
    invoke-static {v4}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$700(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->openTypeChange()V

    :cond_0
    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    iput v7, v4, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->dataSource:I

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->holder:Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;
    invoke-static {v4}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$700(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;

    move-result-object v4

    invoke-virtual {v4, v7}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->showScanStatus(Z)V

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    const v5, 0x7f060024    # com.jrm.localmm.R.string.loading_top

    # invokes: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->showScanMessage(I)V
    invoke-static {v4, v5}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$800(Lcom/jrm/localmm/ui/main/FileBrowserActivity;I)V

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->topDataBrowser:Lcom/jrm/localmm/ui/main/TopDataBrowser;
    invoke-static {v4}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$900(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)Lcom/jrm/localmm/ui/main/TopDataBrowser;

    move-result-object v4

    invoke-virtual {v4}, Lcom/jrm/localmm/ui/main/TopDataBrowser;->refresh()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget v4, p1, Landroid/os/Message;->what:I

    if-ne v4, v9, :cond_3

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    iput v9, v4, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->dataSource:I

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    const v5, 0x7f060025    # com.jrm.localmm.R.string.loading_usb_device

    # invokes: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->showScanMessage(I)V
    invoke-static {v4, v5}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$800(Lcom/jrm/localmm/ui/main/FileBrowserActivity;I)V

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->localDataBrowser:Lcom/jrm/localmm/ui/main/LocalDataBrowser;
    invoke-static {v4}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$300(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)Lcom/jrm/localmm/ui/main/LocalDataBrowser;

    move-result-object v4

    const/4 v5, -0x1

    iget-object v6, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    iget v6, v6, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->dataType:I

    invoke-virtual {v4, v5, v6}, Lcom/jrm/localmm/ui/main/LocalDataBrowser;->browser(II)V

    goto :goto_0

    :cond_3
    iget v4, p1, Landroid/os/Message;->what:I

    if-ne v4, v10, :cond_4

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    iget-object v5, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    iget v5, v5, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->dataSource:I

    # setter for: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->lastDataSource:I
    invoke-static {v4, v5}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$602(Lcom/jrm/localmm/ui/main/FileBrowserActivity;I)I

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    iput v10, v4, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->dataSource:I

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    const v5, 0x7f06002a    # com.jrm.localmm.R.string.loading_samba_device

    # invokes: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->showScanMessage(I)V
    invoke-static {v4, v5}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$800(Lcom/jrm/localmm/ui/main/FileBrowserActivity;I)V

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    iput-boolean v6, v4, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->canResponse:Z

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->sambaDataBrowser:Lcom/jrm/localmm/ui/main/SambaDataBrowser;
    invoke-static {v4}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$500(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)Lcom/jrm/localmm/ui/main/SambaDataBrowser;

    move-result-object v4

    const/4 v5, -0x1

    iget-object v6, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    iget v6, v6, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->dataType:I

    invoke-virtual {v4, v5, v6}, Lcom/jrm/localmm/ui/main/SambaDataBrowser;->browser(II)V

    goto :goto_0

    :cond_4
    iget v4, p1, Landroid/os/Message;->what:I

    if-ne v4, v8, :cond_5

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    iget-object v5, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    iget v5, v5, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->dataSource:I

    # setter for: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->lastDataSource:I
    invoke-static {v4, v5}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$602(Lcom/jrm/localmm/ui/main/FileBrowserActivity;I)I

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    iput v8, v4, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->dataSource:I

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    const v5, 0x7f060027    # com.jrm.localmm.R.string.loading_dlna_device

    # invokes: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->showScanMessage(I)V
    invoke-static {v4, v5}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$800(Lcom/jrm/localmm/ui/main/FileBrowserActivity;I)V

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->dlnaDataBrowser:Lcom/jrm/localmm/ui/main/DlnaDataBrowser;
    invoke-static {v4}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$400(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)Lcom/jrm/localmm/ui/main/DlnaDataBrowser;

    move-result-object v4

    const/4 v5, -0x1

    iget-object v6, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    iget v6, v6, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->dataType:I

    invoke-virtual {v4, v5, v6}, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->browser(II)V

    goto :goto_0

    :cond_5
    iget v4, p1, Landroid/os/Message;->what:I

    const/4 v5, 0x5

    if-ne v4, v5, :cond_a

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    # invokes: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->dismissScanMessage()V
    invoke-static {v4}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$1000(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)V

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->desDataList:Ljava/util/List;
    invoke-static {v4}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$1100(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->clear()V

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->desDataList:Ljava/util/List;
    invoke-static {v4}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$1100(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)Ljava/util/List;

    move-result-object v4

    iget-object v5, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->sourceData:Ljava/util/List;
    invoke-static {v5}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$1200(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)Ljava/util/List;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->adapter:Lcom/jrm/localmm/business/adapter/DataAdapter;
    invoke-static {v4}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$1300(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)Lcom/jrm/localmm/business/adapter/DataAdapter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/jrm/localmm/business/adapter/DataAdapter;->notifyDataSetChanged()V

    const-string v3, ""

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->lastDataSource:I
    invoke-static {v4}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$600(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)I

    move-result v4

    if-eq v4, v7, :cond_6

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->lastDataSource:I
    invoke-static {v4}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$600(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)I

    move-result v4

    if-ne v4, v8, :cond_8

    :cond_6
    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->holder:Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;
    invoke-static {v4}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$700(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;

    move-result-object v4

    iget-object v4, v4, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->listView:Landroid/widget/ListView;

    invoke-virtual {v4, v6}, Landroid/widget/ListView;->setSelection(I)V

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    invoke-virtual {v4}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f050006    # com.jrm.localmm.R.array.data_source

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v4

    aget-object v3, v4, v6

    :cond_7
    :goto_1
    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->holder:Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;
    invoke-static {v4}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$700(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->setDisplayTip(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->holder:Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;
    invoke-static {v4}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$700(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;

    move-result-object v4

    iget v5, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v4, v5}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->setCurrentPageNum(I)V

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->holder:Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;
    invoke-static {v4}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$700(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;

    move-result-object v4

    iget v5, p1, Landroid/os/Message;->arg2:I

    invoke-virtual {v4, v5}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->setTotalPageNum(I)V

    goto/16 :goto_0

    :cond_8
    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->lastDataSource:I
    invoke-static {v4}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$600(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)I

    move-result v4

    if-ne v4, v8, :cond_9

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->holder:Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;
    invoke-static {v4}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$700(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;

    move-result-object v4

    iget-object v4, v4, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->listView:Landroid/widget/ListView;

    invoke-virtual {v4, v6}, Landroid/widget/ListView;->setSelection(I)V

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    invoke-virtual {v4}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f050006    # com.jrm.localmm.R.array.data_source

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v4

    aget-object v3, v4, v6

    goto :goto_1

    :cond_9
    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->lastDataSource:I
    invoke-static {v4}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$600(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)I

    move-result v4

    if-ne v4, v8, :cond_7

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->holder:Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;
    invoke-static {v4}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$700(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;

    move-result-object v4

    iget-object v4, v4, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->listView:Landroid/widget/ListView;

    invoke-virtual {v4, v6}, Landroid/widget/ListView;->setSelection(I)V

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    invoke-virtual {v4}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f050006    # com.jrm.localmm.R.array.data_source

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v4

    aget-object v3, v4, v6

    goto :goto_1

    :cond_a
    iget v4, p1, Landroid/os/Message;->what:I

    const/4 v5, 0x6

    if-ne v4, v5, :cond_b

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    # invokes: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->dismissScanMessage()V
    invoke-static {v4}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$1000(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)V

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->desDataList:Ljava/util/List;
    invoke-static {v4}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$1100(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->clear()V

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->desDataList:Ljava/util/List;
    invoke-static {v4}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$1100(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)Ljava/util/List;

    move-result-object v4

    iget-object v5, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->sourceData:Ljava/util/List;
    invoke-static {v5}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$1200(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)Ljava/util/List;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->adapter:Lcom/jrm/localmm/business/adapter/DataAdapter;
    invoke-static {v4}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$1300(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)Lcom/jrm/localmm/business/adapter/DataAdapter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/jrm/localmm/business/adapter/DataAdapter;->notifyDataSetChanged()V

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string v4, "current_index"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->holder:Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;
    invoke-static {v4}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$700(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;

    move-result-object v4

    iget-object v4, v4, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->listView:Landroid/widget/ListView;

    invoke-virtual {v4, v1}, Landroid/widget/ListView;->setSelection(I)V

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->holder:Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;
    invoke-static {v4}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$700(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;

    move-result-object v4

    const-string v5, "current_page"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->setCurrentPageNum(I)V

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->holder:Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;
    invoke-static {v4}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$700(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;

    move-result-object v4

    const-string v5, "total_page"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->setTotalPageNum(I)V

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    # invokes: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->showHits(I)V
    invoke-static {v4, v1}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$100(Lcom/jrm/localmm/ui/main/FileBrowserActivity;I)V

    goto/16 :goto_0

    :cond_b
    iget v4, p1, Landroid/os/Message;->what:I

    const/4 v5, 0x7

    if-ne v4, v5, :cond_c

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    # invokes: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->dismissScanMessage()V
    invoke-static {v4}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$1000(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)V

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->holder:Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;
    invoke-static {v4}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$700(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;

    move-result-object v4

    invoke-virtual {v4, v7, v6}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->setLeftFocus(IZ)V

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->holder:Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;
    invoke-static {v4}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$700(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->closeTypeChange()V

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    iput v7, v4, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->tmpType:I

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->desDataList:Ljava/util/List;
    invoke-static {v4}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$1100(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->clear()V

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->desDataList:Ljava/util/List;
    invoke-static {v4}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$1100(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)Ljava/util/List;

    move-result-object v4

    iget-object v5, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->sourceData:Ljava/util/List;
    invoke-static {v5}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$1200(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)Ljava/util/List;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->adapter:Lcom/jrm/localmm/business/adapter/DataAdapter;
    invoke-static {v4}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$1300(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)Lcom/jrm/localmm/business/adapter/DataAdapter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/jrm/localmm/business/adapter/DataAdapter;->notifyDataSetChanged()V

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string v4, "current_index"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->holder:Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;
    invoke-static {v4}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$700(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;

    move-result-object v4

    iget-object v4, v4, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->listView:Landroid/widget/ListView;

    invoke-virtual {v4, v1}, Landroid/widget/ListView;->setSelection(I)V

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->holder:Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;
    invoke-static {v4}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$700(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;

    move-result-object v4

    const-string v5, "current_page"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->setCurrentPageNum(I)V

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->holder:Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;
    invoke-static {v4}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$700(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;

    move-result-object v4

    const-string v5, "total_page"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->setTotalPageNum(I)V

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    # invokes: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->showHits(I)V
    invoke-static {v4, v1}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$100(Lcom/jrm/localmm/ui/main/FileBrowserActivity;I)V

    goto/16 :goto_0

    :cond_c
    iget v4, p1, Landroid/os/Message;->what:I

    const/16 v5, 0x8

    if-ne v4, v5, :cond_d

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    # invokes: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->dismissScanMessage()V
    invoke-static {v4}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$1000(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)V

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->desDataList:Ljava/util/List;
    invoke-static {v4}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$1100(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->clear()V

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->desDataList:Ljava/util/List;
    invoke-static {v4}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$1100(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)Ljava/util/List;

    move-result-object v4

    iget-object v5, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->sourceData:Ljava/util/List;
    invoke-static {v5}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$1200(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)Ljava/util/List;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->adapter:Lcom/jrm/localmm/business/adapter/DataAdapter;
    invoke-static {v4}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$1300(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)Lcom/jrm/localmm/business/adapter/DataAdapter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/jrm/localmm/business/adapter/DataAdapter;->notifyDataSetChanged()V

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string v4, "current_index"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->holder:Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;
    invoke-static {v4}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$700(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;

    move-result-object v4

    iget-object v4, v4, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->listView:Landroid/widget/ListView;

    invoke-virtual {v4, v1}, Landroid/widget/ListView;->setSelection(I)V

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->holder:Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;
    invoke-static {v4}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$700(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;

    move-result-object v4

    iget-object v4, v4, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->listView:Landroid/widget/ListView;

    invoke-virtual {v4, v1}, Landroid/widget/ListView;->setSelection(I)V

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->holder:Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;
    invoke-static {v4}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$700(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;

    move-result-object v4

    const-string v5, "current_page"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->setCurrentPageNum(I)V

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->holder:Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;
    invoke-static {v4}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$700(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;

    move-result-object v4

    const-string v5, "total_page"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->setTotalPageNum(I)V

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    # invokes: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->showHits(I)V
    invoke-static {v4, v1}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$100(Lcom/jrm/localmm/ui/main/FileBrowserActivity;I)V

    goto/16 :goto_0

    :cond_d
    iget v4, p1, Landroid/os/Message;->what:I

    const/16 v5, 0x9

    if-ne v4, v5, :cond_e

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    iget-object v5, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    iget v5, v5, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->dataType:I

    # invokes: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->switchMediaType(I)V
    invoke-static {v4, v5}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$1400(Lcom/jrm/localmm/ui/main/FileBrowserActivity;I)V

    goto/16 :goto_0

    :cond_e
    iget v4, p1, Landroid/os/Message;->what:I

    const/16 v5, 0xa

    if-ne v4, v5, :cond_f

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    iget v5, p1, Landroid/os/Message;->arg1:I

    # invokes: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->processExceptions(I)V
    invoke-static {v4, v5}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$1500(Lcom/jrm/localmm/ui/main/FileBrowserActivity;I)V

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->holder:Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;
    invoke-static {v4}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$700(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;

    move-result-object v4

    invoke-virtual {v4, v6}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->showScanStatus(Z)V

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->holder:Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;
    invoke-static {v4}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$700(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;

    move-result-object v4

    invoke-virtual {v4, v6, v6}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->updateScanStatusText(ZI)V

    goto/16 :goto_0

    :cond_f
    iget v4, p1, Landroid/os/Message;->what:I

    const/16 v5, 0xb

    if-ne v4, v5, :cond_11

    iget v4, p1, Landroid/os/Message;->arg2:I

    const/16 v5, 0x11

    if-ne v4, v5, :cond_10

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    iget v5, p1, Landroid/os/Message;->arg1:I

    # invokes: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->showScanMessage(I)V
    invoke-static {v4, v5}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$800(Lcom/jrm/localmm/ui/main/FileBrowserActivity;I)V

    goto/16 :goto_0

    :cond_10
    iget v4, p1, Landroid/os/Message;->arg2:I

    const/16 v5, 0x12

    if-ne v4, v5, :cond_1

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    # invokes: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->dismissScanMessage()V
    invoke-static {v4}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$1000(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)V

    goto/16 :goto_0

    :cond_11
    iget v4, p1, Landroid/os/Message;->what:I

    const/16 v5, 0xf

    if-ne v4, v5, :cond_13

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    iput-boolean v7, v4, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->canResponse:Z

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    # invokes: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->release()V
    invoke-static {v4}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$1600(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)V

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    iget v4, v4, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->dataSource:I

    if-ne v4, v8, :cond_12

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->holder:Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;
    invoke-static {v4}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$700(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->openTypeChange()V

    :cond_12
    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    iget-object v5, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    iget v5, v5, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->dataSource:I

    # setter for: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->lastDataSource:I
    invoke-static {v4, v5}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$602(Lcom/jrm/localmm/ui/main/FileBrowserActivity;I)I

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    iput v7, v4, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->dataSource:I

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->holder:Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;
    invoke-static {v4}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$700(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;

    move-result-object v4

    invoke-virtual {v4, v7}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->showScanStatus(Z)V

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->topDataBrowser:Lcom/jrm/localmm/ui/main/TopDataBrowser;
    invoke-static {v4}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$900(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)Lcom/jrm/localmm/ui/main/TopDataBrowser;

    move-result-object v4

    invoke-virtual {v4}, Lcom/jrm/localmm/ui/main/TopDataBrowser;->refresh()V

    goto/16 :goto_0

    :cond_13
    iget v4, p1, Landroid/os/Message;->what:I

    const/16 v5, 0x14

    if-ne v4, v5, :cond_16

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    iget v4, v4, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->dataSource:I

    if-ne v4, v9, :cond_14

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->localDataBrowser:Lcom/jrm/localmm/ui/main/LocalDataBrowser;
    invoke-static {v4}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$300(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)Lcom/jrm/localmm/ui/main/LocalDataBrowser;

    move-result-object v4

    const/4 v5, 0x5

    invoke-virtual {v4, v5}, Lcom/jrm/localmm/ui/main/LocalDataBrowser;->refresh(I)V

    goto/16 :goto_0

    :cond_14
    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    iget v4, v4, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->dataSource:I

    if-ne v4, v8, :cond_15

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->dlnaDataBrowser:Lcom/jrm/localmm/ui/main/DlnaDataBrowser;
    invoke-static {v4}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$400(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)Lcom/jrm/localmm/ui/main/DlnaDataBrowser;

    move-result-object v4

    const/4 v5, 0x5

    invoke-virtual {v4, v5}, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->refresh(I)V

    goto/16 :goto_0

    :cond_15
    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    iget v4, v4, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->dataSource:I

    if-ne v4, v10, :cond_1

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->sambaDataBrowser:Lcom/jrm/localmm/ui/main/SambaDataBrowser;
    invoke-static {v4}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$500(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)Lcom/jrm/localmm/ui/main/SambaDataBrowser;

    move-result-object v4

    const/4 v5, 0x5

    invoke-virtual {v4, v5}, Lcom/jrm/localmm/ui/main/SambaDataBrowser;->refresh(I)V

    goto/16 :goto_0

    :cond_16
    iget v4, p1, Landroid/os/Message;->what:I

    const/16 v5, 0x15

    if-ne v4, v5, :cond_19

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    iget v4, v4, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->dataSource:I

    if-ne v4, v9, :cond_17

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->localDataBrowser:Lcom/jrm/localmm/ui/main/LocalDataBrowser;
    invoke-static {v4}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$300(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)Lcom/jrm/localmm/ui/main/LocalDataBrowser;

    move-result-object v4

    const/4 v5, 0x6

    invoke-virtual {v4, v5}, Lcom/jrm/localmm/ui/main/LocalDataBrowser;->refresh(I)V

    goto/16 :goto_0

    :cond_17
    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    iget v4, v4, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->dataSource:I

    if-ne v4, v8, :cond_18

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->dlnaDataBrowser:Lcom/jrm/localmm/ui/main/DlnaDataBrowser;
    invoke-static {v4}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$400(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)Lcom/jrm/localmm/ui/main/DlnaDataBrowser;

    move-result-object v4

    const/4 v5, 0x6

    invoke-virtual {v4, v5}, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->refresh(I)V

    goto/16 :goto_0

    :cond_18
    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    iget v4, v4, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->dataSource:I

    if-ne v4, v10, :cond_1

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->sambaDataBrowser:Lcom/jrm/localmm/ui/main/SambaDataBrowser;
    invoke-static {v4}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$500(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)Lcom/jrm/localmm/ui/main/SambaDataBrowser;

    move-result-object v4

    const/4 v5, 0x6

    invoke-virtual {v4, v5}, Lcom/jrm/localmm/ui/main/SambaDataBrowser;->refresh(I)V

    goto/16 :goto_0

    :cond_19
    iget v4, p1, Landroid/os/Message;->what:I

    const/16 v5, 0x13

    if-ne v4, v5, :cond_1a

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    iget-boolean v4, v4, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->canResponse:Z

    if-eqz v4, :cond_1

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "adapter.position"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->holder:Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;
    invoke-static {v4}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$700(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;

    move-result-object v4

    invoke-virtual {v4, v7, v2}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->setListViewFocus(ZI)V

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    # invokes: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->showHits(I)V
    invoke-static {v4, v2}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$100(Lcom/jrm/localmm/ui/main/FileBrowserActivity;I)V

    goto/16 :goto_0

    :cond_1a
    iget v4, p1, Landroid/os/Message;->what:I

    const/16 v5, 0x64

    if-ne v4, v5, :cond_1

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    iget v4, v4, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->dataSource:I

    if-ne v4, v9, :cond_1b

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->localDataBrowser:Lcom/jrm/localmm/ui/main/LocalDataBrowser;
    invoke-static {v4}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$300(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)Lcom/jrm/localmm/ui/main/LocalDataBrowser;

    move-result-object v4

    invoke-virtual {v4}, Lcom/jrm/localmm/ui/main/LocalDataBrowser;->startPlayer()V

    goto/16 :goto_0

    :cond_1b
    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    iget v4, v4, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->dataSource:I

    if-ne v4, v8, :cond_1c

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->dlnaDataBrowser:Lcom/jrm/localmm/ui/main/DlnaDataBrowser;
    invoke-static {v4}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$400(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)Lcom/jrm/localmm/ui/main/DlnaDataBrowser;

    move-result-object v4

    invoke-virtual {v4}, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->startPlayer()V

    goto/16 :goto_0

    :cond_1c
    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    iget v4, v4, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->dataSource:I

    if-ne v4, v10, :cond_1

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$5;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->sambaDataBrowser:Lcom/jrm/localmm/ui/main/SambaDataBrowser;
    invoke-static {v4}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$500(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)Lcom/jrm/localmm/ui/main/SambaDataBrowser;

    move-result-object v4

    invoke-virtual {v4}, Lcom/jrm/localmm/ui/main/SambaDataBrowser;->startPlayer()V

    goto/16 :goto_0
.end method
