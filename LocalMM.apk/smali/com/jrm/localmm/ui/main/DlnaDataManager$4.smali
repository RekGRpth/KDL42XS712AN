.class Lcom/jrm/localmm/ui/main/DlnaDataManager$4;
.super Ljava/lang/Object;
.source "DlnaDataManager.java"

# interfaces
.implements Lcom/jrm/localmm/ui/main/PingDeviceListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/jrm/localmm/ui/main/DlnaDataManager;->enterDirectory(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/localmm/ui/main/DlnaDataManager;


# direct methods
.method constructor <init>(Lcom/jrm/localmm/ui/main/DlnaDataManager;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager$4;->this$0:Lcom/jrm/localmm/ui/main/DlnaDataManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFinish(Z)V
    .locals 4
    .param p1    # Z

    const/4 v3, 0x1

    if-eqz p1, :cond_0

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager$4;->this$0:Lcom/jrm/localmm/ui/main/DlnaDataManager;

    const/4 v2, 0x0

    # setter for: Lcom/jrm/localmm/ui/main/DlnaDataManager;->position:I
    invoke-static {v1, v2}, Lcom/jrm/localmm/ui/main/DlnaDataManager;->access$702(Lcom/jrm/localmm/ui/main/DlnaDataManager;I)I

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager$4;->this$0:Lcom/jrm/localmm/ui/main/DlnaDataManager;

    # setter for: Lcom/jrm/localmm/ui/main/DlnaDataManager;->currentPage:I
    invoke-static {v1, v3}, Lcom/jrm/localmm/ui/main/DlnaDataManager;->access$802(Lcom/jrm/localmm/ui/main/DlnaDataManager;I)I

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/jrm/localmm/ui/main/DlnaDataManager$BrowseRunnable;

    iget-object v2, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager$4;->this$0:Lcom/jrm/localmm/ui/main/DlnaDataManager;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/jrm/localmm/ui/main/DlnaDataManager$BrowseRunnable;-><init>(Lcom/jrm/localmm/ui/main/DlnaDataManager;Lcom/jrm/localmm/ui/main/DlnaDataManager$1;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager$4;->this$0:Lcom/jrm/localmm/ui/main/DlnaDataManager;

    # setter for: Lcom/jrm/localmm/ui/main/DlnaDataManager;->state:I
    invoke-static {v1, v3}, Lcom/jrm/localmm/ui/main/DlnaDataManager;->access$502(Lcom/jrm/localmm/ui/main/DlnaDataManager;I)I

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager$4;->this$0:Lcom/jrm/localmm/ui/main/DlnaDataManager;

    # getter for: Lcom/jrm/localmm/ui/main/DlnaDataManager;->refreshUIListener:Lcom/jrm/localmm/ui/main/RefreshUIListener;
    invoke-static {v1}, Lcom/jrm/localmm/ui/main/DlnaDataManager;->access$1000(Lcom/jrm/localmm/ui/main/DlnaDataManager;)Lcom/jrm/localmm/ui/main/RefreshUIListener;

    move-result-object v1

    invoke-interface {v1, v3}, Lcom/jrm/localmm/ui/main/RefreshUIListener;->onFailed(I)V

    goto :goto_0
.end method
