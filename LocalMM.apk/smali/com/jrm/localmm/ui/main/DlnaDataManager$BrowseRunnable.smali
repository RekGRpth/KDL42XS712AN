.class Lcom/jrm/localmm/ui/main/DlnaDataManager$BrowseRunnable;
.super Ljava/lang/Object;
.source "DlnaDataManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jrm/localmm/ui/main/DlnaDataManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BrowseRunnable"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/localmm/ui/main/DlnaDataManager;


# direct methods
.method private constructor <init>(Lcom/jrm/localmm/ui/main/DlnaDataManager;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager$BrowseRunnable;->this$0:Lcom/jrm/localmm/ui/main/DlnaDataManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/jrm/localmm/ui/main/DlnaDataManager;Lcom/jrm/localmm/ui/main/DlnaDataManager$1;)V
    .locals 0
    .param p1    # Lcom/jrm/localmm/ui/main/DlnaDataManager;
    .param p2    # Lcom/jrm/localmm/ui/main/DlnaDataManager$1;

    invoke-direct {p0, p1}, Lcom/jrm/localmm/ui/main/DlnaDataManager$BrowseRunnable;-><init>(Lcom/jrm/localmm/ui/main/DlnaDataManager;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 13

    const/4 v12, 0x1

    const/4 v11, 0x0

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager$BrowseRunnable;->this$0:Lcom/jrm/localmm/ui/main/DlnaDataManager;

    # getter for: Lcom/jrm/localmm/ui/main/DlnaDataManager;->mediaServerDisplay:Lcom/jrm/localmm/business/data/MediaServerDisplay;
    invoke-static {v1}, Lcom/jrm/localmm/ui/main/DlnaDataManager;->access$600(Lcom/jrm/localmm/ui/main/DlnaDataManager;)Lcom/jrm/localmm/business/data/MediaServerDisplay;

    move-result-object v1

    invoke-virtual {v1}, Lcom/jrm/localmm/business/data/MediaServerDisplay;->getMediaServerController()Landroid/net/dlna/MediaServerController;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager$BrowseRunnable;->this$0:Lcom/jrm/localmm/ui/main/DlnaDataManager;

    const/4 v2, 0x2

    # setter for: Lcom/jrm/localmm/ui/main/DlnaDataManager;->state:I
    invoke-static {v1, v2}, Lcom/jrm/localmm/ui/main/DlnaDataManager;->access$502(Lcom/jrm/localmm/ui/main/DlnaDataManager;I)I

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager$BrowseRunnable;->this$0:Lcom/jrm/localmm/ui/main/DlnaDataManager;

    # getter for: Lcom/jrm/localmm/ui/main/DlnaDataManager;->currentPath:Ljava/lang/String;
    invoke-static {v1}, Lcom/jrm/localmm/ui/main/DlnaDataManager;->access$1100(Lcom/jrm/localmm/ui/main/DlnaDataManager;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Landroid/net/dlna/BrowseFlag;->BROWSE_DIRECT_CHILDREN:Landroid/net/dlna/BrowseFlag;

    const-string v3, "res,res@size"

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-interface/range {v0 .. v6}, Landroid/net/dlna/MediaServerController;->Browse_Ex(Ljava/lang/String;Landroid/net/dlna/BrowseFlag;Ljava/lang/String;IILjava/lang/String;)Landroid/net/dlna/BrowseResult;

    move-result-object v7

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager$BrowseRunnable;->this$0:Lcom/jrm/localmm/ui/main/DlnaDataManager;

    # getter for: Lcom/jrm/localmm/ui/main/DlnaDataManager;->resourceList:Ljava/util/List;
    invoke-static {v1}, Lcom/jrm/localmm/ui/main/DlnaDataManager;->access$1200(Lcom/jrm/localmm/ui/main/DlnaDataManager;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->clear()V

    if-eqz v7, :cond_1

    invoke-virtual {v7}, Landroid/net/dlna/BrowseResult;->getShareObjects()Ljava/util/ArrayList;

    move-result-object v10

    if-eqz v10, :cond_1

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager$BrowseRunnable;->this$0:Lcom/jrm/localmm/ui/main/DlnaDataManager;

    # getter for: Lcom/jrm/localmm/ui/main/DlnaDataManager;->resourceList:Ljava/util/List;
    invoke-static {v1}, Lcom/jrm/localmm/ui/main/DlnaDataManager;->access$1200(Lcom/jrm/localmm/ui/main/DlnaDataManager;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v10}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_1
    iget-object v1, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager$BrowseRunnable;->this$0:Lcom/jrm/localmm/ui/main/DlnaDataManager;

    # getter for: Lcom/jrm/localmm/ui/main/DlnaDataManager;->resourceList:Ljava/util/List;
    invoke-static {v1}, Lcom/jrm/localmm/ui/main/DlnaDataManager;->access$1200(Lcom/jrm/localmm/ui/main/DlnaDataManager;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v9

    if-nez v9, :cond_2

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager$BrowseRunnable;->this$0:Lcom/jrm/localmm/ui/main/DlnaDataManager;

    const/4 v2, 0x1

    # setter for: Lcom/jrm/localmm/ui/main/DlnaDataManager;->totalPage:I
    invoke-static {v1, v2}, Lcom/jrm/localmm/ui/main/DlnaDataManager;->access$1302(Lcom/jrm/localmm/ui/main/DlnaDataManager;I)I

    :goto_1
    iget-object v1, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager$BrowseRunnable;->this$0:Lcom/jrm/localmm/ui/main/DlnaDataManager;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/jrm/localmm/ui/main/DlnaDataManager;->getCurrentPage(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v8

    const-string v1, "DlnaDataManager"

    const-string v2, "BrowseRunnable resource exception"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager$BrowseRunnable;->this$0:Lcom/jrm/localmm/ui/main/DlnaDataManager;

    # getter for: Lcom/jrm/localmm/ui/main/DlnaDataManager;->refreshUIListener:Lcom/jrm/localmm/ui/main/RefreshUIListener;
    invoke-static {v1}, Lcom/jrm/localmm/ui/main/DlnaDataManager;->access$1000(Lcom/jrm/localmm/ui/main/DlnaDataManager;)Lcom/jrm/localmm/ui/main/RefreshUIListener;

    move-result-object v1

    invoke-interface {v1, v12}, Lcom/jrm/localmm/ui/main/RefreshUIListener;->onFailed(I)V

    goto :goto_0

    :cond_2
    :try_start_1
    iget-object v1, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager$BrowseRunnable;->this$0:Lcom/jrm/localmm/ui/main/DlnaDataManager;

    div-int/lit8 v2, v9, 0x9

    # setter for: Lcom/jrm/localmm/ui/main/DlnaDataManager;->totalPage:I
    invoke-static {v1, v2}, Lcom/jrm/localmm/ui/main/DlnaDataManager;->access$1302(Lcom/jrm/localmm/ui/main/DlnaDataManager;I)I

    iget-object v2, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager$BrowseRunnable;->this$0:Lcom/jrm/localmm/ui/main/DlnaDataManager;

    rem-int/lit8 v1, v9, 0x9

    if-nez v1, :cond_3

    move v1, v11

    :goto_2
    # += operator for: Lcom/jrm/localmm/ui/main/DlnaDataManager;->totalPage:I
    invoke-static {v2, v1}, Lcom/jrm/localmm/ui/main/DlnaDataManager;->access$1312(Lcom/jrm/localmm/ui/main/DlnaDataManager;I)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :cond_3
    move v1, v12

    goto :goto_2
.end method
