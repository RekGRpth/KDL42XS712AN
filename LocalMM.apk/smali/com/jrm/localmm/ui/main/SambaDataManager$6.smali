.class Lcom/jrm/localmm/ui/main/SambaDataManager$6;
.super Ljava/lang/Object;
.source "SambaDataManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/jrm/localmm/ui/main/SambaDataManager;->enterDirectory(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/localmm/ui/main/SambaDataManager;


# direct methods
.method constructor <init>(Lcom/jrm/localmm/ui/main/SambaDataManager;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/localmm/ui/main/SambaDataManager$6;->this$0:Lcom/jrm/localmm/ui/main/SambaDataManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/SambaDataManager$6;->this$0:Lcom/jrm/localmm/ui/main/SambaDataManager;

    # getter for: Lcom/jrm/localmm/ui/main/SambaDataManager;->loginSambaListener:Lcom/jrm/localmm/ui/main/LoginSambaListener;
    invoke-static {v1}, Lcom/jrm/localmm/ui/main/SambaDataManager;->access$400(Lcom/jrm/localmm/ui/main/SambaDataManager;)Lcom/jrm/localmm/ui/main/LoginSambaListener;

    move-result-object v1

    const/16 v2, 0xc

    invoke-interface {v1, v2}, Lcom/jrm/localmm/ui/main/LoginSambaListener;->onEnd(I)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/SambaDataManager$6;->this$0:Lcom/jrm/localmm/ui/main/SambaDataManager;

    # getter for: Lcom/jrm/localmm/ui/main/SambaDataManager;->smbDevice:Landroid/net/samba/SmbDevice;
    invoke-static {v1}, Lcom/jrm/localmm/ui/main/SambaDataManager;->access$000(Lcom/jrm/localmm/ui/main/SambaDataManager;)Landroid/net/samba/SmbDevice;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/samba/SmbDevice;->unmount()I

    move-result v0

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/SambaDataManager$6;->this$0:Lcom/jrm/localmm/ui/main/SambaDataManager;

    # getter for: Lcom/jrm/localmm/ui/main/SambaDataManager;->unmountListener:Lcom/jrm/localmm/ui/main/UnMountListener;
    invoke-static {v1}, Lcom/jrm/localmm/ui/main/SambaDataManager;->access$100(Lcom/jrm/localmm/ui/main/SambaDataManager;)Lcom/jrm/localmm/ui/main/UnMountListener;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/jrm/localmm/ui/main/UnMountListener;->onFinish(I)V

    return-void
.end method
