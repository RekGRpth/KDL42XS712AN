.class Lcom/jrm/localmm/ui/main/SambaDataManager$8$1;
.super Ljava/lang/Object;
.source "SambaDataManager.java"

# interfaces
.implements Lcom/jrm/localmm/ui/main/PingDeviceListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/jrm/localmm/ui/main/SambaDataManager$8;->handleMessage(Landroid/os/Message;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/jrm/localmm/ui/main/SambaDataManager$8;


# direct methods
.method constructor <init>(Lcom/jrm/localmm/ui/main/SambaDataManager$8;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/localmm/ui/main/SambaDataManager$8$1;->this$1:Lcom/jrm/localmm/ui/main/SambaDataManager$8;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFinish(Z)V
    .locals 4
    .param p1    # Z

    if-eqz p1, :cond_0

    new-instance v0, Lcom/jrm/localmm/ui/main/SambaDataManager$LoginStatus;

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/SambaDataManager$8$1;->this$1:Lcom/jrm/localmm/ui/main/SambaDataManager$8;

    iget-object v1, v1, Lcom/jrm/localmm/ui/main/SambaDataManager$8;->this$0:Lcom/jrm/localmm/ui/main/SambaDataManager;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/jrm/localmm/ui/main/SambaDataManager$LoginStatus;-><init>(Lcom/jrm/localmm/ui/main/SambaDataManager;Lcom/jrm/localmm/ui/main/SambaDataManager$1;)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/SambaDataManager$8$1;->this$1:Lcom/jrm/localmm/ui/main/SambaDataManager$8;

    iget-object v1, v1, Lcom/jrm/localmm/ui/main/SambaDataManager$8;->this$0:Lcom/jrm/localmm/ui/main/SambaDataManager;

    iget-object v2, p0, Lcom/jrm/localmm/ui/main/SambaDataManager$8$1;->this$1:Lcom/jrm/localmm/ui/main/SambaDataManager$8;

    iget-object v2, v2, Lcom/jrm/localmm/ui/main/SambaDataManager$8;->this$0:Lcom/jrm/localmm/ui/main/SambaDataManager;

    # getter for: Lcom/jrm/localmm/ui/main/SambaDataManager;->usr:Ljava/lang/String;
    invoke-static {v2}, Lcom/jrm/localmm/ui/main/SambaDataManager;->access$1200(Lcom/jrm/localmm/ui/main/SambaDataManager;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/jrm/localmm/ui/main/SambaDataManager$8$1;->this$1:Lcom/jrm/localmm/ui/main/SambaDataManager$8;

    iget-object v3, v3, Lcom/jrm/localmm/ui/main/SambaDataManager$8;->this$0:Lcom/jrm/localmm/ui/main/SambaDataManager;

    # getter for: Lcom/jrm/localmm/ui/main/SambaDataManager;->pwd:Ljava/lang/String;
    invoke-static {v3}, Lcom/jrm/localmm/ui/main/SambaDataManager;->access$1300(Lcom/jrm/localmm/ui/main/SambaDataManager;)Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/jrm/localmm/ui/main/SambaDataManager;->login(Ljava/lang/String;Ljava/lang/String;Lcom/jrm/localmm/ui/main/SambaDataManager$LoginStatus;)V
    invoke-static {v1, v2, v3, v0}, Lcom/jrm/localmm/ui/main/SambaDataManager;->access$1600(Lcom/jrm/localmm/ui/main/SambaDataManager;Ljava/lang/String;Ljava/lang/String;Lcom/jrm/localmm/ui/main/SambaDataManager$LoginStatus;)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/jrm/localmm/ui/main/SambaDataManager$8$1;->this$1:Lcom/jrm/localmm/ui/main/SambaDataManager$8;

    iget-object v1, v1, Lcom/jrm/localmm/ui/main/SambaDataManager$8;->this$0:Lcom/jrm/localmm/ui/main/SambaDataManager;

    # getter for: Lcom/jrm/localmm/ui/main/SambaDataManager;->refreshUIListener:Lcom/jrm/localmm/ui/main/RefreshUIListener;
    invoke-static {v1}, Lcom/jrm/localmm/ui/main/SambaDataManager;->access$300(Lcom/jrm/localmm/ui/main/SambaDataManager;)Lcom/jrm/localmm/ui/main/RefreshUIListener;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Lcom/jrm/localmm/ui/main/RefreshUIListener;->onFailed(I)V

    goto :goto_0
.end method
