.class Lcom/jrm/localmm/ui/main/SambaDataManager$FindHostRunnable;
.super Ljava/lang/Object;
.source "SambaDataManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jrm/localmm/ui/main/SambaDataManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FindHostRunnable"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/localmm/ui/main/SambaDataManager;


# direct methods
.method private constructor <init>(Lcom/jrm/localmm/ui/main/SambaDataManager;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/localmm/ui/main/SambaDataManager$FindHostRunnable;->this$0:Lcom/jrm/localmm/ui/main/SambaDataManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/jrm/localmm/ui/main/SambaDataManager;Lcom/jrm/localmm/ui/main/SambaDataManager$1;)V
    .locals 0
    .param p1    # Lcom/jrm/localmm/ui/main/SambaDataManager;
    .param p2    # Lcom/jrm/localmm/ui/main/SambaDataManager$1;

    invoke-direct {p0, p1}, Lcom/jrm/localmm/ui/main/SambaDataManager$FindHostRunnable;-><init>(Lcom/jrm/localmm/ui/main/SambaDataManager;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/SambaDataManager$FindHostRunnable;->this$0:Lcom/jrm/localmm/ui/main/SambaDataManager;

    new-instance v1, Landroid/net/samba/SmbClient;

    invoke-direct {v1}, Landroid/net/samba/SmbClient;-><init>()V

    # setter for: Lcom/jrm/localmm/ui/main/SambaDataManager;->smbClient:Landroid/net/samba/SmbClient;
    invoke-static {v0, v1}, Lcom/jrm/localmm/ui/main/SambaDataManager;->access$1802(Lcom/jrm/localmm/ui/main/SambaDataManager;Landroid/net/samba/SmbClient;)Landroid/net/samba/SmbClient;

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/SambaDataManager$FindHostRunnable;->this$0:Lcom/jrm/localmm/ui/main/SambaDataManager;

    # getter for: Lcom/jrm/localmm/ui/main/SambaDataManager;->smbClient:Landroid/net/samba/SmbClient;
    invoke-static {v0}, Lcom/jrm/localmm/ui/main/SambaDataManager;->access$1800(Lcom/jrm/localmm/ui/main/SambaDataManager;)Landroid/net/samba/SmbClient;

    move-result-object v0

    const/16 v1, 0x3e8

    invoke-virtual {v0, v1}, Landroid/net/samba/SmbClient;->SetPingTimeout(I)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/SambaDataManager$FindHostRunnable;->this$0:Lcom/jrm/localmm/ui/main/SambaDataManager;

    # getter for: Lcom/jrm/localmm/ui/main/SambaDataManager;->smbClient:Landroid/net/samba/SmbClient;
    invoke-static {v0}, Lcom/jrm/localmm/ui/main/SambaDataManager;->access$1800(Lcom/jrm/localmm/ui/main/SambaDataManager;)Landroid/net/samba/SmbClient;

    move-result-object v0

    new-instance v1, Lcom/jrm/localmm/ui/main/SambaDataManager$FindHostRunnable$1;

    invoke-direct {v1, p0}, Lcom/jrm/localmm/ui/main/SambaDataManager$FindHostRunnable$1;-><init>(Lcom/jrm/localmm/ui/main/SambaDataManager$FindHostRunnable;)V

    invoke-virtual {v0, v1}, Landroid/net/samba/SmbClient;->setOnRecvMsgListener(Landroid/net/samba/OnRecvMsgListener;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/SambaDataManager$FindHostRunnable;->this$0:Lcom/jrm/localmm/ui/main/SambaDataManager;

    # getter for: Lcom/jrm/localmm/ui/main/SambaDataManager;->loginSambaListener:Lcom/jrm/localmm/ui/main/LoginSambaListener;
    invoke-static {v0}, Lcom/jrm/localmm/ui/main/SambaDataManager;->access$400(Lcom/jrm/localmm/ui/main/SambaDataManager;)Lcom/jrm/localmm/ui/main/LoginSambaListener;

    move-result-object v0

    const/16 v1, 0xd

    invoke-interface {v0, v1}, Lcom/jrm/localmm/ui/main/LoginSambaListener;->onEnd(I)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/SambaDataManager$FindHostRunnable;->this$0:Lcom/jrm/localmm/ui/main/SambaDataManager;

    # getter for: Lcom/jrm/localmm/ui/main/SambaDataManager;->smbClient:Landroid/net/samba/SmbClient;
    invoke-static {v0}, Lcom/jrm/localmm/ui/main/SambaDataManager;->access$1800(Lcom/jrm/localmm/ui/main/SambaDataManager;)Landroid/net/samba/SmbClient;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/samba/SmbClient;->updateSmbDeviceList()V

    return-void
.end method
