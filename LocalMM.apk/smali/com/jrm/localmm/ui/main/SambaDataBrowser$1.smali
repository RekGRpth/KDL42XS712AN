.class Lcom/jrm/localmm/ui/main/SambaDataBrowser$1;
.super Ljava/lang/Object;
.source "SambaDataBrowser.java"

# interfaces
.implements Lcom/jrm/localmm/ui/main/RefreshUIListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jrm/localmm/ui/main/SambaDataBrowser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/localmm/ui/main/SambaDataBrowser;


# direct methods
.method constructor <init>(Lcom/jrm/localmm/ui/main/SambaDataBrowser;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/localmm/ui/main/SambaDataBrowser$1;->this$0:Lcom/jrm/localmm/ui/main/SambaDataBrowser;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailed(I)V
    .locals 8
    .param p1    # I

    const/4 v7, 0x5

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const-string v1, "SambaDataBrowser"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onFailed, code : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/SambaDataBrowser$1;->this$0:Lcom/jrm/localmm/ui/main/SambaDataBrowser;

    # getter for: Lcom/jrm/localmm/ui/main/SambaDataBrowser;->handler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/jrm/localmm/ui/main/SambaDataBrowser;->access$200(Lcom/jrm/localmm/ui/main/SambaDataBrowser;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    const/16 v1, 0xa

    iput v1, v0, Landroid/os/Message;->what:I

    if-ne p1, v4, :cond_1

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/SambaDataBrowser$1;->this$0:Lcom/jrm/localmm/ui/main/SambaDataBrowser;

    invoke-virtual {v1}, Lcom/jrm/localmm/ui/main/SambaDataBrowser;->release()V

    iput v4, v0, Landroid/os/Message;->arg1:I

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/jrm/localmm/ui/main/SambaDataBrowser$1;->this$0:Lcom/jrm/localmm/ui/main/SambaDataBrowser;

    # getter for: Lcom/jrm/localmm/ui/main/SambaDataBrowser;->handler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/jrm/localmm/ui/main/SambaDataBrowser;->access$200(Lcom/jrm/localmm/ui/main/SambaDataBrowser;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void

    :cond_1
    if-ne p1, v5, :cond_2

    iput v5, v0, Landroid/os/Message;->arg1:I

    goto :goto_0

    :cond_2
    if-ne p1, v6, :cond_3

    iput v6, v0, Landroid/os/Message;->arg1:I

    goto :goto_0

    :cond_3
    if-ne p1, v7, :cond_0

    iput v7, v0, Landroid/os/Message;->arg1:I

    goto :goto_0
.end method

.method public onFinish(Ljava/util/List;IIILjava/util/List;)V
    .locals 5
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/jrm/localmm/business/data/BaseData;",
            ">;III",
            "Ljava/util/List",
            "<",
            "Lcom/jrm/localmm/business/data/BaseData;",
            ">;)V"
        }
    .end annotation

    const-string v2, "SambaDataBrowser"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onFinish, currentPage : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " totalPage : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " position : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/jrm/localmm/ui/main/SambaDataBrowser$1;->this$0:Lcom/jrm/localmm/ui/main/SambaDataBrowser;

    # setter for: Lcom/jrm/localmm/ui/main/SambaDataBrowser;->focusPosition:I
    invoke-static {v2, p4}, Lcom/jrm/localmm/ui/main/SambaDataBrowser;->access$002(Lcom/jrm/localmm/ui/main/SambaDataBrowser;I)I

    iget-object v2, p0, Lcom/jrm/localmm/ui/main/SambaDataBrowser$1;->this$0:Lcom/jrm/localmm/ui/main/SambaDataBrowser;

    # getter for: Lcom/jrm/localmm/ui/main/SambaDataBrowser;->dataList:Ljava/util/List;
    invoke-static {v2}, Lcom/jrm/localmm/ui/main/SambaDataBrowser;->access$100(Lcom/jrm/localmm/ui/main/SambaDataBrowser;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->clear()V

    iget-object v2, p0, Lcom/jrm/localmm/ui/main/SambaDataBrowser$1;->this$0:Lcom/jrm/localmm/ui/main/SambaDataBrowser;

    # getter for: Lcom/jrm/localmm/ui/main/SambaDataBrowser;->dataList:Ljava/util/List;
    invoke-static {v2}, Lcom/jrm/localmm/ui/main/SambaDataBrowser;->access$100(Lcom/jrm/localmm/ui/main/SambaDataBrowser;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    iget-object v2, p0, Lcom/jrm/localmm/ui/main/SambaDataBrowser$1;->this$0:Lcom/jrm/localmm/ui/main/SambaDataBrowser;

    # getter for: Lcom/jrm/localmm/ui/main/SambaDataBrowser;->handler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/jrm/localmm/ui/main/SambaDataBrowser;->access$200(Lcom/jrm/localmm/ui/main/SambaDataBrowser;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    const/16 v2, 0x8

    iput v2, v1, Landroid/os/Message;->what:I

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "current_page"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "total_page"

    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "current_index"

    invoke-virtual {v0, v2, p4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v2, p0, Lcom/jrm/localmm/ui/main/SambaDataBrowser$1;->this$0:Lcom/jrm/localmm/ui/main/SambaDataBrowser;

    # getter for: Lcom/jrm/localmm/ui/main/SambaDataBrowser;->handler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/jrm/localmm/ui/main/SambaDataBrowser;->access$200(Lcom/jrm/localmm/ui/main/SambaDataBrowser;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method
