.class Lcom/jrm/localmm/ui/main/DlnaDataBrowser$1;
.super Ljava/lang/Object;
.source "DlnaDataBrowser.java"

# interfaces
.implements Lcom/jrm/localmm/ui/main/RefreshUIListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jrm/localmm/ui/main/DlnaDataBrowser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/localmm/ui/main/DlnaDataBrowser;


# direct methods
.method constructor <init>(Lcom/jrm/localmm/ui/main/DlnaDataBrowser;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/localmm/ui/main/DlnaDataBrowser$1;->this$0:Lcom/jrm/localmm/ui/main/DlnaDataBrowser;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailed(I)V
    .locals 5
    .param p1    # I

    const/4 v4, 0x1

    const-string v1, "DlnaDataBrowser"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onFailed, code : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/DlnaDataBrowser$1;->this$0:Lcom/jrm/localmm/ui/main/DlnaDataBrowser;

    invoke-virtual {v1}, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->release()V

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/DlnaDataBrowser$1;->this$0:Lcom/jrm/localmm/ui/main/DlnaDataBrowser;

    # getter for: Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->handler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->access$500(Lcom/jrm/localmm/ui/main/DlnaDataBrowser;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    const/16 v1, 0xa

    iput v1, v0, Landroid/os/Message;->what:I

    if-ne p1, v4, :cond_0

    iput v4, v0, Landroid/os/Message;->arg1:I

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/DlnaDataBrowser$1;->this$0:Lcom/jrm/localmm/ui/main/DlnaDataBrowser;

    # getter for: Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->handler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->access$500(Lcom/jrm/localmm/ui/main/DlnaDataBrowser;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_0
    return-void
.end method

.method public onFinish(Ljava/util/List;IIILjava/util/List;)V
    .locals 5
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/jrm/localmm/business/data/BaseData;",
            ">;III",
            "Ljava/util/List",
            "<",
            "Lcom/jrm/localmm/business/data/BaseData;",
            ">;)V"
        }
    .end annotation

    const-string v2, "DlnaDataBrowser"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onFinish, currentPage : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " totalPage : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " focus position : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " current list size:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " total list size:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {p5}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/jrm/localmm/ui/main/DlnaDataBrowser$1;->this$0:Lcom/jrm/localmm/ui/main/DlnaDataBrowser;

    # setter for: Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->focusPosition:I
    invoke-static {v2, p4}, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->access$002(Lcom/jrm/localmm/ui/main/DlnaDataBrowser;I)I

    iget-object v2, p0, Lcom/jrm/localmm/ui/main/DlnaDataBrowser$1;->this$0:Lcom/jrm/localmm/ui/main/DlnaDataBrowser;

    # getter for: Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->data:Ljava/util/List;
    invoke-static {v2}, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->access$100(Lcom/jrm/localmm/ui/main/DlnaDataBrowser;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->clear()V

    iget-object v2, p0, Lcom/jrm/localmm/ui/main/DlnaDataBrowser$1;->this$0:Lcom/jrm/localmm/ui/main/DlnaDataBrowser;

    # getter for: Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->data:Ljava/util/List;
    invoke-static {v2}, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->access$100(Lcom/jrm/localmm/ui/main/DlnaDataBrowser;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    iget-object v2, p0, Lcom/jrm/localmm/ui/main/DlnaDataBrowser$1;->this$0:Lcom/jrm/localmm/ui/main/DlnaDataBrowser;

    # getter for: Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->totalDataList:Ljava/util/List;
    invoke-static {v2}, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->access$200(Lcom/jrm/localmm/ui/main/DlnaDataBrowser;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->clear()V

    iget-object v2, p0, Lcom/jrm/localmm/ui/main/DlnaDataBrowser$1;->this$0:Lcom/jrm/localmm/ui/main/DlnaDataBrowser;

    # getter for: Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->totalDataList:Ljava/util/List;
    invoke-static {v2}, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->access$200(Lcom/jrm/localmm/ui/main/DlnaDataBrowser;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, p5}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    iget-object v2, p0, Lcom/jrm/localmm/ui/main/DlnaDataBrowser$1;->this$0:Lcom/jrm/localmm/ui/main/DlnaDataBrowser;

    # setter for: Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->currentPageIndex:I
    invoke-static {v2, p2}, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->access$302(Lcom/jrm/localmm/ui/main/DlnaDataBrowser;I)I

    iget-object v2, p0, Lcom/jrm/localmm/ui/main/DlnaDataBrowser$1;->this$0:Lcom/jrm/localmm/ui/main/DlnaDataBrowser;

    # setter for: Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->totalPageIndex:I
    invoke-static {v2, p3}, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->access$402(Lcom/jrm/localmm/ui/main/DlnaDataBrowser;I)I

    const-string v2, "DlnaDataBrowser"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onFinish, data size:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/DlnaDataBrowser$1;->this$0:Lcom/jrm/localmm/ui/main/DlnaDataBrowser;

    # getter for: Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->data:Ljava/util/List;
    invoke-static {v4}, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->access$100(Lcom/jrm/localmm/ui/main/DlnaDataBrowser;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " total data size:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/DlnaDataBrowser$1;->this$0:Lcom/jrm/localmm/ui/main/DlnaDataBrowser;

    # getter for: Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->totalDataList:Ljava/util/List;
    invoke-static {v4}, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->access$200(Lcom/jrm/localmm/ui/main/DlnaDataBrowser;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/jrm/localmm/ui/main/DlnaDataBrowser$1;->this$0:Lcom/jrm/localmm/ui/main/DlnaDataBrowser;

    # getter for: Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->handler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->access$500(Lcom/jrm/localmm/ui/main/DlnaDataBrowser;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    const/4 v2, 0x7

    iput v2, v1, Landroid/os/Message;->what:I

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "current_page"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "total_page"

    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "current_index"

    invoke-virtual {v0, v2, p4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v2, p0, Lcom/jrm/localmm/ui/main/DlnaDataBrowser$1;->this$0:Lcom/jrm/localmm/ui/main/DlnaDataBrowser;

    # getter for: Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->handler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->access$500(Lcom/jrm/localmm/ui/main/DlnaDataBrowser;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method
