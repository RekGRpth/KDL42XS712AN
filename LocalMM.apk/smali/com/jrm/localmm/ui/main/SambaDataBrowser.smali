.class public Lcom/jrm/localmm/ui/main/SambaDataBrowser;
.super Ljava/lang/Object;
.source "SambaDataBrowser.java"


# instance fields
.field private activity:Landroid/app/Activity;

.field private browserType:I

.field private dataList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/jrm/localmm/business/data/BaseData;",
            ">;"
        }
    .end annotation
.end field

.field private focusPosition:I

.field private handler:Landroid/os/Handler;

.field private loginSambaListener:Lcom/jrm/localmm/ui/main/LoginSambaListener;

.field private mediaType:I

.field private refreshUIListener:Lcom/jrm/localmm/ui/main/RefreshUIListener;

.field private sambaDataManager:Lcom/jrm/localmm/ui/main/SambaDataManager;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/os/Handler;Ljava/util/List;)V
    .locals 1
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/os/Handler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Landroid/os/Handler;",
            "Ljava/util/List",
            "<",
            "Lcom/jrm/localmm/business/data/BaseData;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/jrm/localmm/ui/main/SambaDataBrowser;->browserType:I

    new-instance v0, Lcom/jrm/localmm/ui/main/SambaDataBrowser$1;

    invoke-direct {v0, p0}, Lcom/jrm/localmm/ui/main/SambaDataBrowser$1;-><init>(Lcom/jrm/localmm/ui/main/SambaDataBrowser;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/SambaDataBrowser;->refreshUIListener:Lcom/jrm/localmm/ui/main/RefreshUIListener;

    new-instance v0, Lcom/jrm/localmm/ui/main/SambaDataBrowser$2;

    invoke-direct {v0, p0}, Lcom/jrm/localmm/ui/main/SambaDataBrowser$2;-><init>(Lcom/jrm/localmm/ui/main/SambaDataBrowser;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/SambaDataBrowser;->loginSambaListener:Lcom/jrm/localmm/ui/main/LoginSambaListener;

    iput-object p1, p0, Lcom/jrm/localmm/ui/main/SambaDataBrowser;->activity:Landroid/app/Activity;

    iput-object p2, p0, Lcom/jrm/localmm/ui/main/SambaDataBrowser;->handler:Landroid/os/Handler;

    iput-object p3, p0, Lcom/jrm/localmm/ui/main/SambaDataBrowser;->dataList:Ljava/util/List;

    return-void
.end method

.method static synthetic access$000(Lcom/jrm/localmm/ui/main/SambaDataBrowser;)I
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/main/SambaDataBrowser;

    iget v0, p0, Lcom/jrm/localmm/ui/main/SambaDataBrowser;->focusPosition:I

    return v0
.end method

.method static synthetic access$002(Lcom/jrm/localmm/ui/main/SambaDataBrowser;I)I
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/main/SambaDataBrowser;
    .param p1    # I

    iput p1, p0, Lcom/jrm/localmm/ui/main/SambaDataBrowser;->focusPosition:I

    return p1
.end method

.method static synthetic access$100(Lcom/jrm/localmm/ui/main/SambaDataBrowser;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/main/SambaDataBrowser;

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/SambaDataBrowser;->dataList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$200(Lcom/jrm/localmm/ui/main/SambaDataBrowser;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/main/SambaDataBrowser;

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/SambaDataBrowser;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$300(Lcom/jrm/localmm/ui/main/SambaDataBrowser;)I
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/main/SambaDataBrowser;

    iget v0, p0, Lcom/jrm/localmm/ui/main/SambaDataBrowser;->mediaType:I

    return v0
.end method

.method static synthetic access$400(Lcom/jrm/localmm/ui/main/SambaDataBrowser;II)V
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/main/SambaDataBrowser;
    .param p1    # I
    .param p2    # I

    invoke-direct {p0, p1, p2}, Lcom/jrm/localmm/ui/main/SambaDataBrowser;->startPlayer(II)V

    return-void
.end method

.method private processDownKeyEvent(I)Z
    .locals 2
    .param p1    # I

    iget v0, p0, Lcom/jrm/localmm/ui/main/SambaDataBrowser;->focusPosition:I

    const/16 v1, 0x9

    if-ne v0, v1, :cond_0

    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/main/SambaDataBrowser;->refresh(I)V

    :goto_0
    const/4 v0, 0x1

    return v0

    :cond_0
    iput p1, p0, Lcom/jrm/localmm/ui/main/SambaDataBrowser;->focusPosition:I

    goto :goto_0
.end method

.method private processEnterKeyEvent(I)Z
    .locals 8
    .param p1    # I

    const/4 v6, 0x1

    const/4 v5, 0x0

    iput p1, p0, Lcom/jrm/localmm/ui/main/SambaDataBrowser;->focusPosition:I

    if-nez p1, :cond_2

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/SambaDataBrowser;->handler:Landroid/os/Handler;

    invoke-virtual {v4}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v2

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/SambaDataBrowser;->dataList:Ljava/util/List;

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/jrm/localmm/business/data/BaseData;

    invoke-virtual {v4}, Lcom/jrm/localmm/business/data/BaseData;->getDescription()Ljava/lang/String;

    move-result-object v1

    const-string v4, "top"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/main/SambaDataBrowser;->release()V

    iput v6, v2, Landroid/os/Message;->what:I

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/SambaDataBrowser;->handler:Landroid/os/Handler;

    invoke-virtual {v4, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_0
    :goto_0
    move v4, v6

    :goto_1
    return v4

    :cond_1
    const-string v4, "samba"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/16 v4, 0xb

    iput v4, v2, Landroid/os/Message;->what:I

    const v4, 0x7f06002b    # com.jrm.localmm.R.string.loading_samba_resource

    iput v4, v2, Landroid/os/Message;->arg1:I

    const/16 v4, 0x11

    iput v4, v2, Landroid/os/Message;->arg2:I

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/SambaDataBrowser;->handler:Landroid/os/Handler;

    invoke-virtual {v4, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    iget v4, p0, Lcom/jrm/localmm/ui/main/SambaDataBrowser;->browserType:I

    invoke-virtual {p0, v5, v4}, Lcom/jrm/localmm/ui/main/SambaDataBrowser;->browser(II)V

    goto :goto_0

    :cond_2
    iget-object v4, p0, Lcom/jrm/localmm/ui/main/SambaDataBrowser;->dataList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge p1, v4, :cond_3

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/SambaDataBrowser;->dataList:Ljava/util/List;

    invoke-interface {v4, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jrm/localmm/business/data/BaseData;

    invoke-virtual {v0}, Lcom/jrm/localmm/business/data/BaseData;->getType()I

    move-result v4

    iput v4, p0, Lcom/jrm/localmm/ui/main/SambaDataBrowser;->mediaType:I

    const/4 v4, 0x5

    iget v5, p0, Lcom/jrm/localmm/ui/main/SambaDataBrowser;->mediaType:I

    if-ne v4, v5, :cond_4

    iget v4, p0, Lcom/jrm/localmm/ui/main/SambaDataBrowser;->browserType:I

    invoke-virtual {p0, p1, v4}, Lcom/jrm/localmm/ui/main/SambaDataBrowser;->browser(II)V

    goto :goto_0

    :cond_3
    const-string v4, "SambaDataBrowser"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "processEnterKeyEvent, positon : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v4, v5

    goto :goto_1

    :cond_4
    iget v4, p0, Lcom/jrm/localmm/ui/main/SambaDataBrowser;->mediaType:I

    invoke-static {v4}, Lcom/jrm/localmm/util/Tools;->isMediaFile(I)Z

    move-result v4

    if-eqz v4, :cond_5

    new-instance v3, Lcom/jrm/localmm/ui/main/SambaDataBrowser$3;

    invoke-direct {v3, p0}, Lcom/jrm/localmm/ui/main/SambaDataBrowser$3;-><init>(Lcom/jrm/localmm/ui/main/SambaDataBrowser;)V

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/SambaDataBrowser;->sambaDataManager:Lcom/jrm/localmm/ui/main/SambaDataManager;

    invoke-virtual {v4, v3}, Lcom/jrm/localmm/ui/main/SambaDataManager;->pingDevice(Lcom/jrm/localmm/ui/main/PingDeviceListener;)V

    goto :goto_0

    :cond_5
    iget-object v4, p0, Lcom/jrm/localmm/ui/main/SambaDataBrowser;->handler:Landroid/os/Handler;

    invoke-virtual {v4}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v2

    const/16 v4, 0xa

    iput v4, v2, Landroid/os/Message;->what:I

    const/16 v4, 0x10

    iput v4, v2, Landroid/os/Message;->arg1:I

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/SambaDataBrowser;->handler:Landroid/os/Handler;

    invoke-virtual {v4, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0
.end method

.method private processUpKeyEvent(I)Z
    .locals 1
    .param p1    # I

    iget v0, p0, Lcom/jrm/localmm/ui/main/SambaDataBrowser;->focusPosition:I

    if-nez v0, :cond_0

    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/main/SambaDataBrowser;->refresh(I)V

    :goto_0
    const/4 v0, 0x1

    return v0

    :cond_0
    iput p1, p0, Lcom/jrm/localmm/ui/main/SambaDataBrowser;->focusPosition:I

    goto :goto_0
.end method

.method private startPlayer(II)V
    .locals 5

    const/4 v4, 0x2

    invoke-static {}, Lcom/jrm/localmm/ui/MediaContainerApplication;->getInstance()Lcom/jrm/localmm/ui/MediaContainerApplication;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/jrm/localmm/ui/MediaContainerApplication;->hasMedia(I)Z

    move-result v0

    if-eqz v0, :cond_6

    iget v0, p0, Lcom/jrm/localmm/ui/main/SambaDataBrowser;->browserType:I

    if-eq v0, p1, :cond_1

    iget v0, p0, Lcom/jrm/localmm/ui/main/SambaDataBrowser;->browserType:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/SambaDataBrowser;->sambaDataManager:Lcom/jrm/localmm/ui/main/SambaDataManager;

    neg-int v1, p1

    invoke-virtual {v0, v1, p2}, Lcom/jrm/localmm/ui/main/SambaDataManager;->getMediaFile(II)I

    move-result v0

    :goto_0
    const-string v1, "SambaDataBrowser"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "startPlayer, index : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    if-ne v4, p1, :cond_2

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/jrm/localmm/ui/main/SambaDataBrowser;->activity:Landroid/app/Activity;

    const-class v3, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    :cond_0
    :goto_1
    const-string v2, "com.jrm.index"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v0, "sourceFrom"

    const/16 v2, 0x11

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    if-ne v4, p1, :cond_5

    sget-boolean v0, Lcom/jrm/localmm/util/Constants;->isExit:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/SambaDataBrowser;->activity:Landroid/app/Activity;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    :goto_2
    return-void

    :cond_1
    iget-object v0, p0, Lcom/jrm/localmm/ui/main/SambaDataBrowser;->sambaDataManager:Lcom/jrm/localmm/ui/main/SambaDataManager;

    invoke-virtual {v0, p1, p2}, Lcom/jrm/localmm/ui/main/SambaDataManager;->getMediaFile(II)I

    move-result v0

    goto :goto_0

    :cond_2
    const/4 v2, 0x3

    if-ne v2, p1, :cond_3

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/jrm/localmm/ui/main/SambaDataBrowser;->activity:Landroid/app/Activity;

    const-class v3, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    goto :goto_1

    :cond_3
    const/4 v2, 0x4

    if-ne v2, p1, :cond_0

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/jrm/localmm/ui/main/SambaDataBrowser;->activity:Landroid/app/Activity;

    const-class v3, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    goto :goto_1

    :cond_4
    iput p2, p0, Lcom/jrm/localmm/ui/main/SambaDataBrowser;->focusPosition:I

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/SambaDataBrowser;->handler:Landroid/os/Handler;

    const/16 v1, 0x64

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_2

    :cond_5
    iget-object v0, p0, Lcom/jrm/localmm/ui/main/SambaDataBrowser;->activity:Landroid/app/Activity;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_2

    :cond_6
    const-string v0, "SambaDataBrowser"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Does not has specified type : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " of media."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method


# virtual methods
.method protected browser(II)V
    .locals 4
    .param p1    # I
    .param p2    # I

    iput p2, p0, Lcom/jrm/localmm/ui/main/SambaDataBrowser;->browserType:I

    const-string v0, "SambaDataBrowser"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "index : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " type : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/SambaDataBrowser;->sambaDataManager:Lcom/jrm/localmm/ui/main/SambaDataManager;

    if-nez v0, :cond_0

    new-instance v0, Lcom/jrm/localmm/ui/main/SambaDataManager;

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/SambaDataBrowser;->activity:Landroid/app/Activity;

    iget-object v2, p0, Lcom/jrm/localmm/ui/main/SambaDataBrowser;->loginSambaListener:Lcom/jrm/localmm/ui/main/LoginSambaListener;

    iget-object v3, p0, Lcom/jrm/localmm/ui/main/SambaDataBrowser;->refreshUIListener:Lcom/jrm/localmm/ui/main/RefreshUIListener;

    invoke-direct {v0, v1, v2, v3}, Lcom/jrm/localmm/ui/main/SambaDataManager;-><init>(Landroid/app/Activity;Lcom/jrm/localmm/ui/main/LoginSambaListener;Lcom/jrm/localmm/ui/main/RefreshUIListener;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/SambaDataBrowser;->sambaDataManager:Lcom/jrm/localmm/ui/main/SambaDataManager;

    :cond_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/main/SambaDataBrowser;->sambaDataManager:Lcom/jrm/localmm/ui/main/SambaDataManager;

    invoke-virtual {v0, p1, p2}, Lcom/jrm/localmm/ui/main/SambaDataManager;->browser(II)V

    return-void
.end method

.method protected processKeyDown(II)Z
    .locals 3
    .param p1    # I
    .param p2    # I

    const-string v0, "SambaDataBrowser"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "keyCode : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " position : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v0, 0x42

    if-ne p1, v0, :cond_0

    invoke-direct {p0, p2}, Lcom/jrm/localmm/ui/main/SambaDataBrowser;->processEnterKeyEvent(I)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x13

    if-ne p1, v0, :cond_1

    invoke-direct {p0, p2}, Lcom/jrm/localmm/ui/main/SambaDataBrowser;->processUpKeyEvent(I)Z

    move-result v0

    goto :goto_0

    :cond_1
    const/16 v0, 0x14

    if-ne p1, v0, :cond_2

    invoke-direct {p0, p2}, Lcom/jrm/localmm/ui/main/SambaDataBrowser;->processDownKeyEvent(I)Z

    move-result v0

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected refresh(I)V
    .locals 3
    .param p1    # I

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eq p1, v1, :cond_0

    const/4 v0, 0x2

    if-eq p1, v0, :cond_0

    const/4 v0, 0x3

    if-eq p1, v0, :cond_0

    const/4 v0, 0x4

    if-ne p1, v0, :cond_2

    :cond_0
    iput p1, p0, Lcom/jrm/localmm/ui/main/SambaDataBrowser;->browserType:I

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/SambaDataBrowser;->sambaDataManager:Lcom/jrm/localmm/ui/main/SambaDataManager;

    invoke-virtual {v0, v2, p1}, Lcom/jrm/localmm/ui/main/SambaDataManager;->getCurrentPage(II)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    const/4 v0, 0x5

    if-eq p1, v0, :cond_3

    const/4 v0, 0x7

    if-ne p1, v0, :cond_4

    :cond_3
    iget-object v0, p0, Lcom/jrm/localmm/ui/main/SambaDataBrowser;->sambaDataManager:Lcom/jrm/localmm/ui/main/SambaDataManager;

    const/4 v1, -0x1

    invoke-virtual {v0, v1, v2}, Lcom/jrm/localmm/ui/main/SambaDataManager;->getCurrentPage(II)V

    goto :goto_0

    :cond_4
    const/4 v0, 0x6

    if-eq p1, v0, :cond_5

    const/16 v0, 0x8

    if-ne p1, v0, :cond_1

    :cond_5
    iget-object v0, p0, Lcom/jrm/localmm/ui/main/SambaDataBrowser;->sambaDataManager:Lcom/jrm/localmm/ui/main/SambaDataManager;

    invoke-virtual {v0, v1, v2}, Lcom/jrm/localmm/ui/main/SambaDataManager;->getCurrentPage(II)V

    goto :goto_0
.end method

.method protected release()V
    .locals 1

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/SambaDataBrowser;->sambaDataManager:Lcom/jrm/localmm/ui/main/SambaDataManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/SambaDataBrowser;->sambaDataManager:Lcom/jrm/localmm/ui/main/SambaDataManager;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/main/SambaDataManager;->release()V

    :cond_0
    return-void
.end method

.method public startPlayer()V
    .locals 2

    iget v0, p0, Lcom/jrm/localmm/ui/main/SambaDataBrowser;->mediaType:I

    iget v1, p0, Lcom/jrm/localmm/ui/main/SambaDataBrowser;->focusPosition:I

    invoke-direct {p0, v0, v1}, Lcom/jrm/localmm/ui/main/SambaDataBrowser;->startPlayer(II)V

    return-void
.end method

.method protected unmount()V
    .locals 1

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/SambaDataBrowser;->sambaDataManager:Lcom/jrm/localmm/ui/main/SambaDataManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/SambaDataBrowser;->sambaDataManager:Lcom/jrm/localmm/ui/main/SambaDataManager;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/main/SambaDataManager;->unmount()V

    :cond_0
    return-void
.end method
