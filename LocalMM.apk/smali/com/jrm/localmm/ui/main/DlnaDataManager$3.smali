.class Lcom/jrm/localmm/ui/main/DlnaDataManager$3;
.super Ljava/lang/Object;
.source "DlnaDataManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/jrm/localmm/ui/main/DlnaDataManager;->pingDevice(Lcom/jrm/localmm/ui/main/PingDeviceListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/localmm/ui/main/DlnaDataManager;

.field final synthetic val$listener:Lcom/jrm/localmm/ui/main/PingDeviceListener;


# direct methods
.method constructor <init>(Lcom/jrm/localmm/ui/main/DlnaDataManager;Lcom/jrm/localmm/ui/main/PingDeviceListener;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager$3;->this$0:Lcom/jrm/localmm/ui/main/DlnaDataManager;

    iput-object p2, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager$3;->val$listener:Lcom/jrm/localmm/ui/main/PingDeviceListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager$3;->this$0:Lcom/jrm/localmm/ui/main/DlnaDataManager;

    # getter for: Lcom/jrm/localmm/ui/main/DlnaDataManager;->mediaServerDisplay:Lcom/jrm/localmm/business/data/MediaServerDisplay;
    invoke-static {v5}, Lcom/jrm/localmm/ui/main/DlnaDataManager;->access$600(Lcom/jrm/localmm/ui/main/DlnaDataManager;)Lcom/jrm/localmm/business/data/MediaServerDisplay;

    move-result-object v5

    invoke-virtual {v5}, Lcom/jrm/localmm/business/data/MediaServerDisplay;->getMediaServerController()Landroid/net/dlna/MediaServerController;

    move-result-object v3

    :try_start_0
    invoke-interface {v3}, Landroid/net/dlna/MediaServerController;->GetDeviceInfo()Landroid/net/dlna/DeviceInfo;

    move-result-object v5

    invoke-virtual {v5}, Landroid/net/dlna/DeviceInfo;->getAddress()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v0

    const/16 v5, 0x3e8

    invoke-virtual {v0, v5}, Ljava/net/InetAddress;->isReachable(I)Z

    move-result v4

    const-string v5, "DlnaDataManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "host ip : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " result : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v5, p0, Lcom/jrm/localmm/ui/main/DlnaDataManager$3;->val$listener:Lcom/jrm/localmm/ui/main/PingDeviceListener;

    invoke-interface {v5, v4}, Lcom/jrm/localmm/ui/main/PingDeviceListener;->onFinish(Z)V

    return-void

    :catch_0
    move-exception v1

    const/4 v4, 0x0

    goto :goto_0
.end method
