.class Lcom/jrm/localmm/ui/main/SambaDataManager$1;
.super Ljava/lang/Object;
.source "SambaDataManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/jrm/localmm/ui/main/SambaDataManager;->pingDevice(Lcom/jrm/localmm/ui/main/PingDeviceListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/localmm/ui/main/SambaDataManager;

.field final synthetic val$listener:Lcom/jrm/localmm/ui/main/PingDeviceListener;


# direct methods
.method constructor <init>(Lcom/jrm/localmm/ui/main/SambaDataManager;Lcom/jrm/localmm/ui/main/PingDeviceListener;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/localmm/ui/main/SambaDataManager$1;->this$0:Lcom/jrm/localmm/ui/main/SambaDataManager;

    iput-object p2, p0, Lcom/jrm/localmm/ui/main/SambaDataManager$1;->val$listener:Lcom/jrm/localmm/ui/main/PingDeviceListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    const/4 v3, 0x0

    :try_start_0
    iget-object v4, p0, Lcom/jrm/localmm/ui/main/SambaDataManager$1;->this$0:Lcom/jrm/localmm/ui/main/SambaDataManager;

    # getter for: Lcom/jrm/localmm/ui/main/SambaDataManager;->smbDevice:Landroid/net/samba/SmbDevice;
    invoke-static {v4}, Lcom/jrm/localmm/ui/main/SambaDataManager;->access$000(Lcom/jrm/localmm/ui/main/SambaDataManager;)Landroid/net/samba/SmbDevice;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/samba/SmbDevice;->getAddress()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v2

    const/16 v4, 0x3e8

    invoke-virtual {v2, v4}, Ljava/net/InetAddress;->isReachable(I)Z

    move-result v3

    const-string v4, "SambaDataManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "host ip : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " result : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v4, p0, Lcom/jrm/localmm/ui/main/SambaDataManager$1;->val$listener:Lcom/jrm/localmm/ui/main/PingDeviceListener;

    invoke-interface {v4, v3}, Lcom/jrm/localmm/ui/main/PingDeviceListener;->onFinish(Z)V

    return-void

    :catch_0
    move-exception v0

    const/4 v3, 0x0

    goto :goto_0
.end method
