.class Lcom/jrm/localmm/ui/main/FileBrowserActivity$6;
.super Landroid/content/BroadcastReceiver;
.source "FileBrowserActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jrm/localmm/ui/main/FileBrowserActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;


# direct methods
.method constructor <init>(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$6;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const/4 v4, 0x2

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v3, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$6;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    iget v3, v3, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->dataSource:I

    if-ne v3, v4, :cond_0

    iget-object v3, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$6;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->localDataBrowser:Lcom/jrm/localmm/ui/main/LocalDataBrowser;
    invoke-static {v3}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$300(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)Lcom/jrm/localmm/ui/main/LocalDataBrowser;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/jrm/localmm/ui/main/LocalDataBrowser;->updateUSBDevice(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v3, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$6;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    iget v3, v3, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->dataSource:I

    if-ne v3, v4, :cond_0

    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$6;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->localDataBrowser:Lcom/jrm/localmm/ui/main/LocalDataBrowser;
    invoke-static {v3}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$300(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)Lcom/jrm/localmm/ui/main/LocalDataBrowser;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/jrm/localmm/ui/main/LocalDataBrowser;->updateUSBDevice(Ljava/lang/String;)V

    goto :goto_0
.end method
