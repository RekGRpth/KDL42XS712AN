.class Lcom/jrm/localmm/ui/main/SambaDataBrowser$2;
.super Ljava/lang/Object;
.source "SambaDataBrowser.java"

# interfaces
.implements Lcom/jrm/localmm/ui/main/LoginSambaListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jrm/localmm/ui/main/SambaDataBrowser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/localmm/ui/main/SambaDataBrowser;


# direct methods
.method constructor <init>(Lcom/jrm/localmm/ui/main/SambaDataBrowser;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/localmm/ui/main/SambaDataBrowser$2;->this$0:Lcom/jrm/localmm/ui/main/SambaDataBrowser;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEnd(I)V
    .locals 3
    .param p1    # I

    const/16 v2, 0x12

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/SambaDataBrowser$2;->this$0:Lcom/jrm/localmm/ui/main/SambaDataBrowser;

    # getter for: Lcom/jrm/localmm/ui/main/SambaDataBrowser;->handler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/jrm/localmm/ui/main/SambaDataBrowser;->access$200(Lcom/jrm/localmm/ui/main/SambaDataBrowser;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    const/16 v1, 0xb

    iput v1, v0, Landroid/os/Message;->what:I

    const/16 v1, 0x11

    iput v1, v0, Landroid/os/Message;->arg2:I

    packed-switch p1, :pswitch_data_0

    :goto_0
    :pswitch_0
    iget-object v1, p0, Lcom/jrm/localmm/ui/main/SambaDataBrowser$2;->this$0:Lcom/jrm/localmm/ui/main/SambaDataBrowser;

    # getter for: Lcom/jrm/localmm/ui/main/SambaDataBrowser;->handler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/jrm/localmm/ui/main/SambaDataBrowser;->access$200(Lcom/jrm/localmm/ui/main/SambaDataBrowser;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void

    :pswitch_1
    const v1, 0x7f060034    # com.jrm.localmm.R.string.login_samba

    iput v1, v0, Landroid/os/Message;->arg1:I

    goto :goto_0

    :pswitch_2
    const v1, 0x7f060035    # com.jrm.localmm.R.string.logging_out_samba

    iput v1, v0, Landroid/os/Message;->arg1:I

    goto :goto_0

    :pswitch_3
    const v1, 0x7f06002a    # com.jrm.localmm.R.string.loading_samba_device

    iput v1, v0, Landroid/os/Message;->arg1:I

    goto :goto_0

    :pswitch_4
    const v1, 0x7f06002b    # com.jrm.localmm.R.string.loading_samba_resource

    iput v1, v0, Landroid/os/Message;->arg1:I

    goto :goto_0

    :pswitch_5
    iput v2, v0, Landroid/os/Message;->arg2:I

    goto :goto_0

    :pswitch_6
    const/16 v1, 0xa

    iput v1, v0, Landroid/os/Message;->what:I

    const/4 v1, 0x4

    iput v1, v0, Landroid/os/Message;->arg1:I

    goto :goto_0

    :pswitch_7
    iput v2, v0, Landroid/os/Message;->arg2:I

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method
