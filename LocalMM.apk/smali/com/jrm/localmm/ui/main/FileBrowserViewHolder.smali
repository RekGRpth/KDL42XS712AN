.class public Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;
.super Ljava/lang/Object;
.source "FileBrowserViewHolder.java"


# instance fields
.field private activity:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

.field protected currentPageNumText:Landroid/widget/TextView;

.field protected displayTip:Landroid/widget/TextView;

.field private handler:Landroid/os/Handler;

.field protected listView:Landroid/widget/ListView;

.field private onClickListener:Landroid/view/View$OnClickListener;

.field private onHoverListener:Landroid/view/View$OnHoverListener;

.field protected otherTitleImage:Landroid/widget/ImageView;

.field protected otherTitleText:Landroid/widget/TextView;

.field protected pageDown:Landroid/widget/ImageView;

.field protected pageUp:Landroid/widget/ImageView;

.field protected pictureTitleImage:Landroid/widget/ImageView;

.field protected pictureTitleText:Landroid/widget/TextView;

.field protected progressScanner:Landroid/widget/ProgressBar;

.field protected scannerStateText:Landroid/widget/TextView;

.field protected selectedItemPosition:I

.field protected songTitleImage:Landroid/widget/ImageView;

.field protected songTitleText:Landroid/widget/TextView;

.field protected totalPageNumText:Landroid/widget/TextView;

.field protected videoTitleImage:Landroid/widget/ImageView;

.field protected videoTitleText:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Lcom/jrm/localmm/ui/main/FileBrowserActivity;Landroid/os/Handler;)V
    .locals 1
    .param p1    # Lcom/jrm/localmm/ui/main/FileBrowserActivity;
    .param p2    # Landroid/os/Handler;

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->listView:Landroid/widget/ListView;

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->currentPageNumText:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->totalPageNumText:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->scannerStateText:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->displayTip:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->pictureTitleText:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->songTitleText:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->videoTitleText:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->otherTitleText:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->pictureTitleImage:Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->songTitleImage:Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->videoTitleImage:Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->otherTitleImage:Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->pageUp:Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->pageDown:Landroid/widget/ImageView;

    const/4 v0, 0x0

    iput v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->selectedItemPosition:I

    new-instance v0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder$1;

    invoke-direct {v0, p0}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder$1;-><init>(Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->onClickListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder$2;

    invoke-direct {v0, p0}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder$2;-><init>(Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->onHoverListener:Landroid/view/View$OnHoverListener;

    iput-object p1, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->activity:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    iput-object p2, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->handler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;)Lcom/jrm/localmm/ui/main/FileBrowserActivity;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->activity:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    return-object v0
.end method

.method static synthetic access$100(Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$200(Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;I)V
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->changeFocus(I)V

    return-void
.end method

.method private changeFocus(I)V
    .locals 6
    .param p1    # I

    const/4 v5, 0x4

    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->listView:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setFocusable(Z)V

    packed-switch p1, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->activity:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    iget v0, v0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->tmpType:I

    invoke-virtual {p0, v0, v1}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->changeLeftFocus(II)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->activity:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    iput v1, v0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->tmpType:I

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->activity:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    iget v0, v0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->tmpType:I

    invoke-virtual {p0, v0, v3}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->changeLeftFocus(II)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->activity:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    iput v3, v0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->tmpType:I

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->activity:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    iget v0, v0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->tmpType:I

    invoke-virtual {p0, v0, v4}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->changeLeftFocus(II)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->activity:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    iput v4, v0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->tmpType:I

    goto :goto_0

    :pswitch_4
    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->activity:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    iget v0, v0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->tmpType:I

    invoke-virtual {p0, v0, v5}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->changeLeftFocus(II)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->activity:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    iput v5, v0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->tmpType:I

    goto :goto_0

    :pswitch_5
    invoke-direct {p0, v2}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->setLeftFocusable(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->listView:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->requestFocus()Z

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->listView:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->activity:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    const/16 v1, 0x16

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->sendKeyEvent(I)V

    goto :goto_0

    :pswitch_6
    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->displayTip:Landroid/widget/TextView;

    const v1, 0x7f06001d    # com.jrm.localmm.R.string.page_up

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    :pswitch_7
    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->displayTip:Landroid/widget/TextView;

    const v1, 0x7f06001e    # com.jrm.localmm.R.string.page_down

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7f080007
        :pswitch_1    # com.jrm.localmm.R.id.otherTitleImage
        :pswitch_0    # com.jrm.localmm.R.id.otherTitleText
        :pswitch_2    # com.jrm.localmm.R.id.pictureTitleImage
        :pswitch_0    # com.jrm.localmm.R.id.pictureTitleText
        :pswitch_3    # com.jrm.localmm.R.id.songTitleImage
        :pswitch_0    # com.jrm.localmm.R.id.songTitleText
        :pswitch_4    # com.jrm.localmm.R.id.videoTitleImage
        :pswitch_0    # com.jrm.localmm.R.id.videoTitleText
        :pswitch_0    # com.jrm.localmm.R.id.progress_scan
        :pswitch_0    # com.jrm.localmm.R.id.scanner_id
        :pswitch_6    # com.jrm.localmm.R.id.leftArrowImg
        :pswitch_0    # com.jrm.localmm.R.id.videoCurrentPageNumText
        :pswitch_0    # com.jrm.localmm.R.id.videoTotalPageNumText
        :pswitch_7    # com.jrm.localmm.R.id.rightArrowImg
        :pswitch_0    # com.jrm.localmm.R.id.videoSelectLinearLayout
        :pswitch_5    # com.jrm.localmm.R.id.videoFileList
    .end packed-switch
.end method

.method private changeOtherImageStyle(ZI)V
    .locals 2
    .param p1    # Z
    .param p2    # I

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->otherTitleImage:Landroid/widget/ImageView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->otherTitleImage:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->activity:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    invoke-virtual {v1}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->otherTitleImage:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->requestFocus()Z

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->otherTitleImage:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->otherTitleImage:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->activity:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    invoke-virtual {v1}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method private changeOtherTextStyle(II)V
    .locals 2
    .param p1    # I
    .param p2    # I

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->otherTitleText:Landroid/widget/TextView;

    int-to-float v1, p1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextSize(F)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->otherTitleText:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setTextColor(I)V

    return-void
.end method

.method private changePictureImageStyle(ZI)V
    .locals 2
    .param p1    # Z
    .param p2    # I

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->pictureTitleImage:Landroid/widget/ImageView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->pictureTitleImage:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->activity:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    invoke-virtual {v1}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->pictureTitleImage:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->requestFocus()Z

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->pictureTitleImage:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->pictureTitleImage:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->activity:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    invoke-virtual {v1}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method private changePictureTextStyle(II)V
    .locals 2
    .param p1    # I
    .param p2    # I

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->pictureTitleText:Landroid/widget/TextView;

    int-to-float v1, p1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextSize(F)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->pictureTitleText:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setTextColor(I)V

    return-void
.end method

.method private changeSongImageStyle(ZI)V
    .locals 2
    .param p1    # Z
    .param p2    # I

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->songTitleImage:Landroid/widget/ImageView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->songTitleImage:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->activity:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    invoke-virtual {v1}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->songTitleImage:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->requestFocus()Z

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->songTitleImage:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->songTitleImage:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->activity:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    invoke-virtual {v1}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method private changeSongTextStyle(II)V
    .locals 2
    .param p1    # I
    .param p2    # I

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->songTitleText:Landroid/widget/TextView;

    int-to-float v1, p1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextSize(F)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->songTitleText:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setTextColor(I)V

    return-void
.end method

.method private changeVideoImageStyle(ZI)V
    .locals 2
    .param p1    # Z
    .param p2    # I

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->videoTitleImage:Landroid/widget/ImageView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->videoTitleImage:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->activity:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    invoke-virtual {v1}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->videoTitleImage:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->requestFocus()Z

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->videoTitleImage:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->videoTitleImage:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->activity:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    invoke-virtual {v1}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method private changeVideoTextStyle(II)V
    .locals 2
    .param p1    # I
    .param p2    # I

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->videoTitleText:Landroid/widget/TextView;

    int-to-float v1, p1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextSize(F)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->videoTitleText:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setTextColor(I)V

    return-void
.end method

.method private setLeftFocusable(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->otherTitleImage:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->pictureTitleImage:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->songTitleImage:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->videoTitleImage:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setFocusable(Z)V

    return-void
.end method


# virtual methods
.method protected changeLeftFocus(II)V
    .locals 7
    .param p1    # I
    .param p2    # I

    const/16 v6, 0x14

    const/4 v5, 0x0

    const/4 v4, -0x1

    const v3, -0x333334

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->activity:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    iget v0, v0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->dataSource:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x2

    if-ne p1, v0, :cond_3

    const v0, 0x7f02002a    # com.jrm.localmm.R.drawable.icon_pic_s

    invoke-direct {p0, v5, v0}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->changePictureImageStyle(ZI)V

    invoke-direct {p0, v6, v3}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->changePictureTextStyle(II)V

    :cond_2
    :goto_1
    const/4 v0, 0x2

    if-ne p2, v0, :cond_6

    const v0, 0x7f020029    # com.jrm.localmm.R.drawable.icon_pic_foucs

    invoke-direct {p0, v2, v0}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->changePictureImageStyle(ZI)V

    const/16 v0, 0x19

    invoke-direct {p0, v0, v4}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->changePictureTextStyle(II)V

    const v0, 0x7f060016    # com.jrm.localmm.R.string.picture

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->setDisplayTip(I)V

    goto :goto_0

    :cond_3
    const/4 v0, 0x3

    if-ne p1, v0, :cond_4

    const v0, 0x7f02002e    # com.jrm.localmm.R.drawable.icon_song_s

    invoke-direct {p0, v5, v0}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->changeSongImageStyle(ZI)V

    invoke-direct {p0, v6, v3}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->changeSongTextStyle(II)V

    goto :goto_1

    :cond_4
    const/4 v0, 0x4

    if-ne p1, v0, :cond_5

    const v0, 0x7f020033    # com.jrm.localmm.R.drawable.icon_video_s

    invoke-direct {p0, v5, v0}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->changeVideoImageStyle(ZI)V

    invoke-direct {p0, v6, v3}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->changeVideoTextStyle(II)V

    goto :goto_1

    :cond_5
    if-ne p1, v2, :cond_2

    const v0, 0x7f020026    # com.jrm.localmm.R.drawable.icon_other_s

    invoke-direct {p0, v5, v0}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->changeOtherImageStyle(ZI)V

    invoke-direct {p0, v6, v3}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->changeOtherTextStyle(II)V

    goto :goto_1

    :cond_6
    const/4 v0, 0x3

    if-ne p2, v0, :cond_7

    const v0, 0x7f02002d    # com.jrm.localmm.R.drawable.icon_song_foucs

    invoke-direct {p0, v2, v0}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->changeSongImageStyle(ZI)V

    const/16 v0, 0x19

    invoke-direct {p0, v0, v4}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->changeSongTextStyle(II)V

    const v0, 0x7f060017    # com.jrm.localmm.R.string.song

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->setDisplayTip(I)V

    goto :goto_0

    :cond_7
    const/4 v0, 0x4

    if-ne p2, v0, :cond_8

    const v0, 0x7f020032    # com.jrm.localmm.R.drawable.icon_video_foucs

    invoke-direct {p0, v2, v0}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->changeVideoImageStyle(ZI)V

    const/16 v0, 0x19

    invoke-direct {p0, v0, v4}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->changeVideoTextStyle(II)V

    const v0, 0x7f060018    # com.jrm.localmm.R.string.video

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->setDisplayTip(I)V

    goto :goto_0

    :cond_8
    if-ne p2, v2, :cond_0

    const v0, 0x7f020025    # com.jrm.localmm.R.drawable.icon_other_foucs

    invoke-direct {p0, v2, v0}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->changeOtherImageStyle(ZI)V

    const/16 v0, 0x19

    invoke-direct {p0, v0, v4}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->changeOtherTextStyle(II)V

    const v0, 0x7f06001c    # com.jrm.localmm.R.string.all

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->setDisplayTip(I)V

    goto/16 :goto_0
.end method

.method protected closeTypeChange()V
    .locals 5

    const/16 v4, 0x14

    const v3, -0x333334

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->activity:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    iget v0, v0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->dataType:I

    invoke-virtual {p0, v0, v2}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->changeLeftFocus(II)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->activity:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    const/4 v1, 0x1

    iput v1, v0, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->dataType:I

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->pictureTitleImage:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setClickable(Z)V

    const v0, 0x7f020028    # com.jrm.localmm.R.drawable.icon_pic_close

    invoke-direct {p0, v2, v0}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->changePictureImageStyle(ZI)V

    invoke-direct {p0, v4, v3}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->changePictureTextStyle(II)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->songTitleImage:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setClickable(Z)V

    const v0, 0x7f02002c    # com.jrm.localmm.R.drawable.icon_song_close

    invoke-direct {p0, v2, v0}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->changeSongImageStyle(ZI)V

    invoke-direct {p0, v4, v3}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->changeSongTextStyle(II)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->videoTitleImage:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setClickable(Z)V

    const v0, 0x7f020031    # com.jrm.localmm.R.drawable.icon_video_close

    invoke-direct {p0, v2, v0}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->changeVideoImageStyle(ZI)V

    invoke-direct {p0, v4, v3}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->changeVideoTextStyle(II)V

    return-void
.end method

.method protected findViews()V
    .locals 2

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->activity:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    const v1, 0x7f080016    # com.jrm.localmm.R.id.videoFileList

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->listView:Landroid/widget/ListView;

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->listView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->onHoverListener:Landroid/view/View$OnHoverListener;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->activity:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    const v1, 0x7f08000f    # com.jrm.localmm.R.id.progress_scan

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->progressScanner:Landroid/widget/ProgressBar;

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->activity:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    const v1, 0x7f080010    # com.jrm.localmm.R.id.scanner_id

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->scannerStateText:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->activity:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    const v1, 0x7f080012    # com.jrm.localmm.R.id.videoCurrentPageNumText

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->currentPageNumText:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->activity:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    const v1, 0x7f080013    # com.jrm.localmm.R.id.videoTotalPageNumText

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->totalPageNumText:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->activity:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    const v1, 0x7f080011    # com.jrm.localmm.R.id.leftArrowImg

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->pageUp:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->pageUp:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->onClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->pageUp:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->onHoverListener:Landroid/view/View$OnHoverListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->activity:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    const v1, 0x7f080014    # com.jrm.localmm.R.id.rightArrowImg

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->pageDown:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->pageDown:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->onClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->pageDown:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->onHoverListener:Landroid/view/View$OnHoverListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->activity:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    const v1, 0x7f080017    # com.jrm.localmm.R.id.displayInfor

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->displayTip:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->activity:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    const v1, 0x7f080009    # com.jrm.localmm.R.id.pictureTitleImage

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->pictureTitleImage:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->activity:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    const v1, 0x7f08000a    # com.jrm.localmm.R.id.pictureTitleText

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->pictureTitleText:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->pictureTitleImage:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->onClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->pictureTitleImage:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->onHoverListener:Landroid/view/View$OnHoverListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->activity:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    const v1, 0x7f08000b    # com.jrm.localmm.R.id.songTitleImage

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->songTitleImage:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->activity:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    const v1, 0x7f08000c    # com.jrm.localmm.R.id.songTitleText

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->songTitleText:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->songTitleImage:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->onClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->songTitleImage:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->onHoverListener:Landroid/view/View$OnHoverListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->activity:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    const v1, 0x7f08000d    # com.jrm.localmm.R.id.videoTitleImage

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->videoTitleImage:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->activity:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    const v1, 0x7f08000e    # com.jrm.localmm.R.id.videoTitleText

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->videoTitleText:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->videoTitleImage:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->onClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->videoTitleImage:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->onHoverListener:Landroid/view/View$OnHoverListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->activity:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    const v1, 0x7f080007    # com.jrm.localmm.R.id.otherTitleImage

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->otherTitleImage:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->activity:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    const v1, 0x7f080008    # com.jrm.localmm.R.id.otherTitleText

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->otherTitleText:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->otherTitleImage:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->onClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->otherTitleImage:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->onHoverListener:Landroid/view/View$OnHoverListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    return-void
.end method

.method protected openTypeChange()V
    .locals 5

    const/16 v4, 0x14

    const/4 v3, 0x1

    const/4 v2, 0x0

    const v1, -0x333334

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->pictureTitleImage:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setClickable(Z)V

    const v0, 0x7f02002a    # com.jrm.localmm.R.drawable.icon_pic_s

    invoke-direct {p0, v2, v0}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->changePictureImageStyle(ZI)V

    invoke-direct {p0, v4, v1}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->changePictureTextStyle(II)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->songTitleImage:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setClickable(Z)V

    const v0, 0x7f02002e    # com.jrm.localmm.R.drawable.icon_song_s

    invoke-direct {p0, v2, v0}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->changeSongImageStyle(ZI)V

    invoke-direct {p0, v4, v1}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->changeSongTextStyle(II)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->videoTitleImage:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setClickable(Z)V

    const v0, 0x7f020033    # com.jrm.localmm.R.drawable.icon_video_s

    invoke-direct {p0, v2, v0}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->changeVideoImageStyle(ZI)V

    invoke-direct {p0, v4, v1}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->changeVideoTextStyle(II)V

    return-void
.end method

.method protected setCurrentPageNum(I)V
    .locals 3
    .param p1    # I

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->currentPageNumText:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method protected setDisplayTip(I)V
    .locals 2
    .param p1    # I

    if-lez p1, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->displayTip:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->activity:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    invoke-virtual {v1, p1}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->displayTip:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method protected setDisplayTip(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->displayTip:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->displayTip:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method protected setLeftFocus(IZ)V
    .locals 6
    .param p1    # I
    .param p2    # Z

    const/4 v5, 0x0

    const/4 v4, -0x1

    const v3, -0x333334

    const/4 v2, 0x1

    const/16 v1, 0x19

    const/4 v0, 0x2

    if-ne p1, v0, :cond_2

    if-eqz p2, :cond_1

    const v0, 0x7f020029    # com.jrm.localmm.R.drawable.icon_pic_foucs

    invoke-direct {p0, v2, v0}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->changePictureImageStyle(ZI)V

    invoke-direct {p0, v1, v4}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->changePictureTextStyle(II)V

    :goto_0
    const v0, 0x7f060016    # com.jrm.localmm.R.string.picture

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->setDisplayTip(I)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    const v0, 0x7f020027    # com.jrm.localmm.R.drawable.icon_pic

    invoke-direct {p0, v5, v0}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->changePictureImageStyle(ZI)V

    invoke-direct {p0, v1, v3}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->changePictureTextStyle(II)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x3

    if-ne p1, v0, :cond_4

    if-eqz p2, :cond_3

    const v0, 0x7f02002d    # com.jrm.localmm.R.drawable.icon_song_foucs

    invoke-direct {p0, v2, v0}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->changeSongImageStyle(ZI)V

    invoke-direct {p0, v1, v4}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->changeSongTextStyle(II)V

    :goto_2
    const v0, 0x7f060017    # com.jrm.localmm.R.string.song

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->setDisplayTip(I)V

    goto :goto_1

    :cond_3
    const v0, 0x7f02002b    # com.jrm.localmm.R.drawable.icon_song

    invoke-direct {p0, v5, v0}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->changeSongImageStyle(ZI)V

    invoke-direct {p0, v1, v3}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->changeSongTextStyle(II)V

    goto :goto_2

    :cond_4
    const/4 v0, 0x4

    if-ne p1, v0, :cond_6

    if-eqz p2, :cond_5

    const v0, 0x7f020032    # com.jrm.localmm.R.drawable.icon_video_foucs

    invoke-direct {p0, v2, v0}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->changeVideoImageStyle(ZI)V

    invoke-direct {p0, v1, v4}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->changeVideoTextStyle(II)V

    :goto_3
    const v0, 0x7f060018    # com.jrm.localmm.R.string.video

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->setDisplayTip(I)V

    goto :goto_1

    :cond_5
    const v0, 0x7f020030    # com.jrm.localmm.R.drawable.icon_video

    invoke-direct {p0, v5, v0}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->changeVideoImageStyle(ZI)V

    invoke-direct {p0, v1, v3}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->changeVideoTextStyle(II)V

    goto :goto_3

    :cond_6
    if-ne p1, v2, :cond_0

    if-eqz p2, :cond_7

    const v0, 0x7f020025    # com.jrm.localmm.R.drawable.icon_other_foucs

    invoke-direct {p0, v2, v0}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->changeOtherImageStyle(ZI)V

    invoke-direct {p0, v1, v4}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->changeOtherTextStyle(II)V

    :goto_4
    const v0, 0x7f06001c    # com.jrm.localmm.R.string.all

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->setDisplayTip(I)V

    goto :goto_1

    :cond_7
    const v0, 0x7f020024    # com.jrm.localmm.R.drawable.icon_other

    invoke-direct {p0, v5, v0}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->changeOtherImageStyle(ZI)V

    invoke-direct {p0, v1, v3}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->changeOtherTextStyle(II)V

    goto :goto_4
.end method

.method protected setListViewFocus(ZI)V
    .locals 2
    .param p1    # Z
    .param p2    # I

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->listView:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->requestFocus()Z

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->listView:Landroid/widget/ListView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->listView:Landroid/widget/ListView;

    invoke-virtual {v0, p2}, Landroid/widget/ListView;->setSelection(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->listView:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setFocusable(Z)V

    goto :goto_0
.end method

.method protected setTotalPageNum(I)V
    .locals 3
    .param p1    # I

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->totalPageNumText:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method protected showScanStatus(Z)V
    .locals 2
    .param p1    # Z

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->progressScanner:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->progressScanner:Landroid/widget/ProgressBar;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0
.end method

.method protected updateScanStatusText(ZI)V
    .locals 2
    .param p1    # Z
    .param p2    # I

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->scannerStateText:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->scannerStateText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->activity:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    invoke-virtual {v1, p2}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->scannerStateText:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method
