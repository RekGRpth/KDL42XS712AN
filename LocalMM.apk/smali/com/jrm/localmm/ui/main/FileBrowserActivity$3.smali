.class Lcom/jrm/localmm/ui/main/FileBrowserActivity$3;
.super Ljava/lang/Object;
.source "FileBrowserActivity.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jrm/localmm/ui/main/FileBrowserActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;


# direct methods
.method constructor <init>(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$3;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 8
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/view/MotionEvent;

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/16 v4, 0x8

    const/4 v3, 0x7

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v0, v1

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$3;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    iget-boolean v1, v1, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->canResponse:Z

    if-eqz v1, :cond_6

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_0
    const/4 v1, 0x0

    :goto_1
    return v1

    :pswitch_0
    iget-object v1, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$3;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    # setter for: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->motionY:I
    invoke-static {v1, v0}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$202(Lcom/jrm/localmm/ui/main/FileBrowserActivity;I)I

    goto :goto_0

    :pswitch_1
    iget-object v1, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$3;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->motionY:I
    invoke-static {v1}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$200(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)I

    move-result v1

    if-le v0, v1, :cond_3

    add-int/lit8 v1, v0, -0x32

    iget-object v2, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$3;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->motionY:I
    invoke-static {v2}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$200(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)I

    move-result v2

    if-le v1, v2, :cond_3

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$3;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    iget v1, v1, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->dataSource:I

    if-ne v1, v5, :cond_1

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$3;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->localDataBrowser:Lcom/jrm/localmm/ui/main/LocalDataBrowser;
    invoke-static {v1}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$300(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)Lcom/jrm/localmm/ui/main/LocalDataBrowser;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/jrm/localmm/ui/main/LocalDataBrowser;->refresh(I)V

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$3;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    iget v1, v1, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->dataSource:I

    if-ne v1, v7, :cond_2

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$3;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->dlnaDataBrowser:Lcom/jrm/localmm/ui/main/DlnaDataBrowser;
    invoke-static {v1}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$400(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)Lcom/jrm/localmm/ui/main/DlnaDataBrowser;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->refresh(I)V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$3;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    iget v1, v1, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->dataSource:I

    if-ne v1, v6, :cond_0

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$3;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->sambaDataBrowser:Lcom/jrm/localmm/ui/main/SambaDataBrowser;
    invoke-static {v1}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$500(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)Lcom/jrm/localmm/ui/main/SambaDataBrowser;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/jrm/localmm/ui/main/SambaDataBrowser;->refresh(I)V

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$3;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->motionY:I
    invoke-static {v1}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$200(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)I

    move-result v1

    if-ge v0, v1, :cond_0

    add-int/lit8 v1, v0, 0x32

    iget-object v2, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$3;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->motionY:I
    invoke-static {v2}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$200(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)I

    move-result v2

    if-ge v1, v2, :cond_0

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$3;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    iget v1, v1, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->dataSource:I

    if-ne v1, v5, :cond_4

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$3;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->localDataBrowser:Lcom/jrm/localmm/ui/main/LocalDataBrowser;
    invoke-static {v1}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$300(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)Lcom/jrm/localmm/ui/main/LocalDataBrowser;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/jrm/localmm/ui/main/LocalDataBrowser;->refresh(I)V

    goto :goto_0

    :cond_4
    iget-object v1, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$3;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    iget v1, v1, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->dataSource:I

    if-ne v1, v7, :cond_5

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$3;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->dlnaDataBrowser:Lcom/jrm/localmm/ui/main/DlnaDataBrowser;
    invoke-static {v1}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$400(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)Lcom/jrm/localmm/ui/main/DlnaDataBrowser;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->refresh(I)V

    goto :goto_0

    :cond_5
    iget-object v1, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$3;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    iget v1, v1, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->dataSource:I

    if-ne v1, v6, :cond_0

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/FileBrowserActivity$3;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserActivity;->sambaDataBrowser:Lcom/jrm/localmm/ui/main/SambaDataBrowser;
    invoke-static {v1}, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->access$500(Lcom/jrm/localmm/ui/main/FileBrowserActivity;)Lcom/jrm/localmm/ui/main/SambaDataBrowser;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/jrm/localmm/ui/main/SambaDataBrowser;->refresh(I)V

    goto/16 :goto_0

    :cond_6
    const/4 v1, 0x1

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
