.class public Lcom/jrm/localmm/ui/main/LocalDataBrowser;
.super Ljava/lang/Object;
.source "LocalDataBrowser.java"


# instance fields
.field private activity:Landroid/app/Activity;

.field private activityType:I

.field protected data:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/jrm/localmm/business/data/BaseData;",
            ">;"
        }
    .end annotation
.end field

.field private focusPosition:I

.field private handler:Landroid/os/Handler;

.field private localDataManager:Lcom/jrm/localmm/ui/main/LocalDataManager;

.field private mediaType:I

.field private refreshUIListener:Lcom/jrm/localmm/ui/main/RefreshUIListener;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/os/Handler;Ljava/util/List;)V
    .locals 1
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/os/Handler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Landroid/os/Handler;",
            "Ljava/util/List",
            "<",
            "Lcom/jrm/localmm/business/data/BaseData;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/jrm/localmm/ui/main/LocalDataBrowser$1;

    invoke-direct {v0, p0}, Lcom/jrm/localmm/ui/main/LocalDataBrowser$1;-><init>(Lcom/jrm/localmm/ui/main/LocalDataBrowser;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/LocalDataBrowser;->refreshUIListener:Lcom/jrm/localmm/ui/main/RefreshUIListener;

    iput-object p1, p0, Lcom/jrm/localmm/ui/main/LocalDataBrowser;->activity:Landroid/app/Activity;

    iput-object p2, p0, Lcom/jrm/localmm/ui/main/LocalDataBrowser;->handler:Landroid/os/Handler;

    iput-object p3, p0, Lcom/jrm/localmm/ui/main/LocalDataBrowser;->data:Ljava/util/List;

    return-void
.end method

.method static synthetic access$002(Lcom/jrm/localmm/ui/main/LocalDataBrowser;I)I
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/main/LocalDataBrowser;
    .param p1    # I

    iput p1, p0, Lcom/jrm/localmm/ui/main/LocalDataBrowser;->focusPosition:I

    return p1
.end method

.method static synthetic access$100(Lcom/jrm/localmm/ui/main/LocalDataBrowser;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/main/LocalDataBrowser;

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/LocalDataBrowser;->handler:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method protected browser(II)V
    .locals 3
    .param p1    # I
    .param p2    # I

    iput p2, p0, Lcom/jrm/localmm/ui/main/LocalDataBrowser;->activityType:I

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/LocalDataBrowser;->localDataManager:Lcom/jrm/localmm/ui/main/LocalDataManager;

    if-nez v0, :cond_0

    new-instance v0, Lcom/jrm/localmm/ui/main/LocalDataManager;

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/LocalDataBrowser;->activity:Landroid/app/Activity;

    iget-object v2, p0, Lcom/jrm/localmm/ui/main/LocalDataBrowser;->refreshUIListener:Lcom/jrm/localmm/ui/main/RefreshUIListener;

    invoke-direct {v0, v1, v2}, Lcom/jrm/localmm/ui/main/LocalDataManager;-><init>(Landroid/app/Activity;Lcom/jrm/localmm/ui/main/RefreshUIListener;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/LocalDataBrowser;->localDataManager:Lcom/jrm/localmm/ui/main/LocalDataManager;

    :cond_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/main/LocalDataBrowser;->localDataManager:Lcom/jrm/localmm/ui/main/LocalDataManager;

    invoke-virtual {v0, p1, p2}, Lcom/jrm/localmm/ui/main/LocalDataManager;->browser(II)V

    return-void
.end method

.method protected processKeyDown(II)Z
    .locals 11
    .param p1    # I
    .param p2    # I

    const/16 v10, 0x11

    const/16 v9, 0xb

    const/4 v8, 0x5

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/16 v4, 0x42

    if-ne p1, v4, :cond_7

    if-nez p2, :cond_2

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/LocalDataBrowser;->data:Ljava/util/List;

    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/jrm/localmm/business/data/BaseData;

    invoke-virtual {v4}, Lcom/jrm/localmm/business/data/BaseData;->getDescription()Ljava/lang/String;

    move-result-object v0

    const-string v4, "top"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/LocalDataBrowser;->handler:Landroid/os/Handler;

    invoke-virtual {v4, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_0
    :goto_0
    move v4, v5

    :goto_1
    return v4

    :cond_1
    const-string v4, "local"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/LocalDataBrowser;->handler:Landroid/os/Handler;

    invoke-virtual {v4}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v2

    iput v9, v2, Landroid/os/Message;->what:I

    const v4, 0x7f060026    # com.jrm.localmm.R.string.loading_local_resource

    iput v4, v2, Landroid/os/Message;->arg1:I

    iput v10, v2, Landroid/os/Message;->arg2:I

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/LocalDataBrowser;->handler:Landroid/os/Handler;

    invoke-virtual {v4, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    iget v4, p0, Lcom/jrm/localmm/ui/main/LocalDataBrowser;->activityType:I

    invoke-virtual {p0, v6, v4}, Lcom/jrm/localmm/ui/main/LocalDataBrowser;->browser(II)V

    goto :goto_0

    :cond_2
    iput v6, p0, Lcom/jrm/localmm/ui/main/LocalDataBrowser;->mediaType:I

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/LocalDataBrowser;->data:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge p2, v4, :cond_4

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/LocalDataBrowser;->data:Ljava/util/List;

    invoke-interface {v4, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jrm/localmm/business/data/BaseData;

    invoke-virtual {v1}, Lcom/jrm/localmm/business/data/BaseData;->getType()I

    move-result v4

    iput v4, p0, Lcom/jrm/localmm/ui/main/LocalDataBrowser;->mediaType:I

    :goto_2
    const/4 v4, 0x2

    iget v6, p0, Lcom/jrm/localmm/ui/main/LocalDataBrowser;->mediaType:I

    if-eq v4, v6, :cond_3

    const/4 v4, 0x3

    iget v6, p0, Lcom/jrm/localmm/ui/main/LocalDataBrowser;->mediaType:I

    if-eq v4, v6, :cond_3

    const/4 v4, 0x4

    iget v6, p0, Lcom/jrm/localmm/ui/main/LocalDataBrowser;->mediaType:I

    if-ne v4, v6, :cond_5

    :cond_3
    iget v4, p0, Lcom/jrm/localmm/ui/main/LocalDataBrowser;->mediaType:I

    invoke-virtual {p0, v4, p2}, Lcom/jrm/localmm/ui/main/LocalDataBrowser;->startPlayer(II)V

    goto :goto_0

    :cond_4
    const-string v4, "LocalDataBrowser"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "processKeyDown, positon : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :cond_5
    iget v4, p0, Lcom/jrm/localmm/ui/main/LocalDataBrowser;->mediaType:I

    if-ne v8, v4, :cond_6

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/LocalDataBrowser;->handler:Landroid/os/Handler;

    invoke-virtual {v4}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v3

    iput v9, v3, Landroid/os/Message;->what:I

    const v4, 0x7f060026    # com.jrm.localmm.R.string.loading_local_resource

    iput v4, v3, Landroid/os/Message;->arg1:I

    iput v10, v3, Landroid/os/Message;->arg2:I

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/LocalDataBrowser;->handler:Landroid/os/Handler;

    invoke-virtual {v4, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    iget v4, p0, Lcom/jrm/localmm/ui/main/LocalDataBrowser;->activityType:I

    invoke-virtual {p0, p2, v4}, Lcom/jrm/localmm/ui/main/LocalDataBrowser;->browser(II)V

    goto/16 :goto_0

    :cond_6
    iget-object v4, p0, Lcom/jrm/localmm/ui/main/LocalDataBrowser;->handler:Landroid/os/Handler;

    invoke-virtual {v4}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v2

    const/16 v4, 0xa

    iput v4, v2, Landroid/os/Message;->what:I

    const/16 v4, 0x10

    iput v4, v2, Landroid/os/Message;->arg1:I

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/LocalDataBrowser;->handler:Landroid/os/Handler;

    invoke-virtual {v4, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    :cond_7
    const/16 v4, 0x13

    if-ne p1, v4, :cond_9

    iget v4, p0, Lcom/jrm/localmm/ui/main/LocalDataBrowser;->focusPosition:I

    if-nez v4, :cond_8

    invoke-virtual {p0, v8}, Lcom/jrm/localmm/ui/main/LocalDataBrowser;->refresh(I)V

    :goto_3
    move v4, v5

    goto/16 :goto_1

    :cond_8
    iput p2, p0, Lcom/jrm/localmm/ui/main/LocalDataBrowser;->focusPosition:I

    goto :goto_3

    :cond_9
    const/16 v4, 0x14

    if-ne p1, v4, :cond_b

    iget v4, p0, Lcom/jrm/localmm/ui/main/LocalDataBrowser;->focusPosition:I

    const/16 v6, 0x9

    if-ne v4, v6, :cond_a

    const/4 v4, 0x6

    invoke-virtual {p0, v4}, Lcom/jrm/localmm/ui/main/LocalDataBrowser;->refresh(I)V

    :goto_4
    move v4, v5

    goto/16 :goto_1

    :cond_a
    iput p2, p0, Lcom/jrm/localmm/ui/main/LocalDataBrowser;->focusPosition:I

    goto :goto_4

    :cond_b
    move v4, v6

    goto/16 :goto_1
.end method

.method protected refresh(I)V
    .locals 4
    .param p1    # I

    const/4 v1, 0x1

    const/4 v3, 0x0

    const/4 v0, 0x5

    if-eq p1, v0, :cond_0

    const/4 v0, 0x7

    if-ne p1, v0, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/main/LocalDataBrowser;->localDataManager:Lcom/jrm/localmm/ui/main/LocalDataManager;

    const/4 v1, -0x1

    invoke-virtual {v0, v1, v3}, Lcom/jrm/localmm/ui/main/LocalDataManager;->getCurrentPage(II)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    const/4 v0, 0x6

    if-eq p1, v0, :cond_3

    const/16 v0, 0x8

    if-ne p1, v0, :cond_4

    :cond_3
    iget-object v0, p0, Lcom/jrm/localmm/ui/main/LocalDataBrowser;->localDataManager:Lcom/jrm/localmm/ui/main/LocalDataManager;

    invoke-virtual {v0, v1, v3}, Lcom/jrm/localmm/ui/main/LocalDataManager;->getCurrentPage(II)V

    goto :goto_0

    :cond_4
    if-eq p1, v1, :cond_5

    const/4 v0, 0x3

    if-eq p1, v0, :cond_5

    const/4 v0, 0x4

    if-eq p1, v0, :cond_5

    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    :cond_5
    iput p1, p0, Lcom/jrm/localmm/ui/main/LocalDataBrowser;->activityType:I

    const-string v0, "LocalDataBrowser"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "LocalDataBrowser activityType:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/jrm/localmm/ui/main/LocalDataBrowser;->activityType:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/LocalDataBrowser;->localDataManager:Lcom/jrm/localmm/ui/main/LocalDataManager;

    invoke-virtual {v0, v3, p1}, Lcom/jrm/localmm/ui/main/LocalDataManager;->getCurrentPage(II)V

    goto :goto_0
.end method

.method public startPlayer()V
    .locals 2

    iget v0, p0, Lcom/jrm/localmm/ui/main/LocalDataBrowser;->mediaType:I

    iget v1, p0, Lcom/jrm/localmm/ui/main/LocalDataBrowser;->focusPosition:I

    invoke-virtual {p0, v0, v1}, Lcom/jrm/localmm/ui/main/LocalDataBrowser;->startPlayer(II)V

    return-void
.end method

.method protected startPlayer(II)V
    .locals 5

    const/4 v4, 0x2

    const-string v0, "LocalDataBrowser"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "*****startPlayer****"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/jrm/localmm/ui/MediaContainerApplication;->getInstance()Lcom/jrm/localmm/ui/MediaContainerApplication;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/jrm/localmm/ui/MediaContainerApplication;->hasMedia(I)Z

    move-result v0

    if-eqz v0, :cond_6

    iget v0, p0, Lcom/jrm/localmm/ui/main/LocalDataBrowser;->activityType:I

    if-eq v0, p1, :cond_1

    iget v0, p0, Lcom/jrm/localmm/ui/main/LocalDataBrowser;->activityType:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/LocalDataBrowser;->localDataManager:Lcom/jrm/localmm/ui/main/LocalDataManager;

    neg-int v1, p1

    invoke-virtual {v0, v1, p2}, Lcom/jrm/localmm/ui/main/LocalDataManager;->getMediaFile(II)I

    move-result v0

    :goto_0
    const-string v1, "LocalDataBrowser"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "startPlayer, index : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    if-ne v4, p1, :cond_2

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/jrm/localmm/ui/main/LocalDataBrowser;->activity:Landroid/app/Activity;

    const-class v3, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    :cond_0
    :goto_1
    const-string v2, "com.jrm.index"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    if-ne v4, p1, :cond_5

    sget-boolean v0, Lcom/jrm/localmm/util/Constants;->isExit:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/LocalDataBrowser;->activity:Landroid/app/Activity;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    :goto_2
    return-void

    :cond_1
    iget-object v0, p0, Lcom/jrm/localmm/ui/main/LocalDataBrowser;->localDataManager:Lcom/jrm/localmm/ui/main/LocalDataManager;

    invoke-virtual {v0, p1, p2}, Lcom/jrm/localmm/ui/main/LocalDataManager;->getMediaFile(II)I

    move-result v0

    goto :goto_0

    :cond_2
    const/4 v2, 0x3

    if-ne v2, p1, :cond_3

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/jrm/localmm/ui/main/LocalDataBrowser;->activity:Landroid/app/Activity;

    const-class v3, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    goto :goto_1

    :cond_3
    const/4 v2, 0x4

    if-ne v2, p1, :cond_0

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/jrm/localmm/ui/main/LocalDataBrowser;->activity:Landroid/app/Activity;

    const-class v3, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    goto :goto_1

    :cond_4
    iput p2, p0, Lcom/jrm/localmm/ui/main/LocalDataBrowser;->focusPosition:I

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/LocalDataBrowser;->handler:Landroid/os/Handler;

    const/16 v1, 0x64

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_2

    :cond_5
    iget-object v0, p0, Lcom/jrm/localmm/ui/main/LocalDataBrowser;->activity:Landroid/app/Activity;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_2

    :cond_6
    const-string v0, "LocalDataBrowser"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Does not has specified type : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " of media."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method protected updateUSBDevice(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/LocalDataBrowser;->localDataManager:Lcom/jrm/localmm/ui/main/LocalDataManager;

    invoke-virtual {v0, p1}, Lcom/jrm/localmm/ui/main/LocalDataManager;->showUSBDevice(Ljava/lang/String;)V

    return-void
.end method
