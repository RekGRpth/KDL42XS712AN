.class Lcom/jrm/localmm/ui/main/LocalDataBrowser$1;
.super Ljava/lang/Object;
.source "LocalDataBrowser.java"

# interfaces
.implements Lcom/jrm/localmm/ui/main/RefreshUIListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jrm/localmm/ui/main/LocalDataBrowser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/localmm/ui/main/LocalDataBrowser;


# direct methods
.method constructor <init>(Lcom/jrm/localmm/ui/main/LocalDataBrowser;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/localmm/ui/main/LocalDataBrowser$1;->this$0:Lcom/jrm/localmm/ui/main/LocalDataBrowser;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailed(I)V
    .locals 2
    .param p1    # I

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/LocalDataBrowser$1;->this$0:Lcom/jrm/localmm/ui/main/LocalDataBrowser;

    # getter for: Lcom/jrm/localmm/ui/main/LocalDataBrowser;->handler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/jrm/localmm/ui/main/LocalDataBrowser;->access$100(Lcom/jrm/localmm/ui/main/LocalDataBrowser;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    const/16 v1, 0xc

    if-ne p1, v1, :cond_0

    const/16 v1, 0xb

    iput v1, v0, Landroid/os/Message;->what:I

    const/16 v1, 0x11

    iput v1, v0, Landroid/os/Message;->arg2:I

    const v1, 0x7f060025    # com.jrm.localmm.R.string.loading_usb_device

    iput v1, v0, Landroid/os/Message;->arg1:I

    :cond_0
    iget-object v1, p0, Lcom/jrm/localmm/ui/main/LocalDataBrowser$1;->this$0:Lcom/jrm/localmm/ui/main/LocalDataBrowser;

    # getter for: Lcom/jrm/localmm/ui/main/LocalDataBrowser;->handler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/jrm/localmm/ui/main/LocalDataBrowser;->access$100(Lcom/jrm/localmm/ui/main/LocalDataBrowser;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public onFinish(Ljava/util/List;IIILjava/util/List;)V
    .locals 3
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/jrm/localmm/business/data/BaseData;",
            ">;III",
            "Ljava/util/List",
            "<",
            "Lcom/jrm/localmm/business/data/BaseData;",
            ">;)V"
        }
    .end annotation

    iget-object v2, p0, Lcom/jrm/localmm/ui/main/LocalDataBrowser$1;->this$0:Lcom/jrm/localmm/ui/main/LocalDataBrowser;

    # setter for: Lcom/jrm/localmm/ui/main/LocalDataBrowser;->focusPosition:I
    invoke-static {v2, p4}, Lcom/jrm/localmm/ui/main/LocalDataBrowser;->access$002(Lcom/jrm/localmm/ui/main/LocalDataBrowser;I)I

    iget-object v2, p0, Lcom/jrm/localmm/ui/main/LocalDataBrowser$1;->this$0:Lcom/jrm/localmm/ui/main/LocalDataBrowser;

    iget-object v2, v2, Lcom/jrm/localmm/ui/main/LocalDataBrowser;->data:Ljava/util/List;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/jrm/localmm/ui/main/LocalDataBrowser$1;->this$0:Lcom/jrm/localmm/ui/main/LocalDataBrowser;

    iget-object v2, v2, Lcom/jrm/localmm/ui/main/LocalDataBrowser;->data:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    :cond_0
    iget-object v2, p0, Lcom/jrm/localmm/ui/main/LocalDataBrowser$1;->this$0:Lcom/jrm/localmm/ui/main/LocalDataBrowser;

    iget-object v2, v2, Lcom/jrm/localmm/ui/main/LocalDataBrowser;->data:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    iget-object v2, p0, Lcom/jrm/localmm/ui/main/LocalDataBrowser$1;->this$0:Lcom/jrm/localmm/ui/main/LocalDataBrowser;

    # getter for: Lcom/jrm/localmm/ui/main/LocalDataBrowser;->handler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/jrm/localmm/ui/main/LocalDataBrowser;->access$100(Lcom/jrm/localmm/ui/main/LocalDataBrowser;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    const/4 v2, 0x6

    iput v2, v1, Landroid/os/Message;->what:I

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "current_page"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "total_page"

    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "current_index"

    invoke-virtual {v0, v2, p4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v2, p0, Lcom/jrm/localmm/ui/main/LocalDataBrowser$1;->this$0:Lcom/jrm/localmm/ui/main/LocalDataBrowser;

    # getter for: Lcom/jrm/localmm/ui/main/LocalDataBrowser;->handler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/jrm/localmm/ui/main/LocalDataBrowser;->access$100(Lcom/jrm/localmm/ui/main/LocalDataBrowser;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method
