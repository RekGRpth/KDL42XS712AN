.class Lcom/jrm/localmm/ui/main/BaseDataManager$2;
.super Ljava/lang/Object;
.source "BaseDataManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/jrm/localmm/ui/main/BaseDataManager;->startScan(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/localmm/ui/main/BaseDataManager;

.field final synthetic val$path:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/jrm/localmm/ui/main/BaseDataManager;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/localmm/ui/main/BaseDataManager$2;->this$0:Lcom/jrm/localmm/ui/main/BaseDataManager;

    iput-object p2, p0, Lcom/jrm/localmm/ui/main/BaseDataManager$2;->val$path:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/BaseDataManager$2;->val$path:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v1

    array-length v1, v1

    if-lez v1, :cond_1

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/BaseDataManager$2;->this$0:Lcom/jrm/localmm/ui/main/BaseDataManager;

    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    # invokes: Lcom/jrm/localmm/ui/main/BaseDataManager;->scan([Ljava/io/File;)V
    invoke-static {v1, v2}, Lcom/jrm/localmm/ui/main/BaseDataManager;->access$000(Lcom/jrm/localmm/ui/main/BaseDataManager;[Ljava/io/File;)V

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/jrm/localmm/ui/main/BaseDataManager$2;->this$0:Lcom/jrm/localmm/ui/main/BaseDataManager;

    invoke-virtual {v1}, Lcom/jrm/localmm/ui/main/BaseDataManager;->onFinish()V

    return-void

    :cond_1
    iget-object v1, p0, Lcom/jrm/localmm/ui/main/BaseDataManager$2;->this$0:Lcom/jrm/localmm/ui/main/BaseDataManager;

    # getter for: Lcom/jrm/localmm/ui/main/BaseDataManager;->mediaContainer:Lcom/jrm/localmm/ui/MediaContainerApplication;
    invoke-static {v1}, Lcom/jrm/localmm/ui/main/BaseDataManager;->access$100(Lcom/jrm/localmm/ui/main/BaseDataManager;)Lcom/jrm/localmm/ui/MediaContainerApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/jrm/localmm/ui/MediaContainerApplication;->clearAll()V

    goto :goto_0
.end method
