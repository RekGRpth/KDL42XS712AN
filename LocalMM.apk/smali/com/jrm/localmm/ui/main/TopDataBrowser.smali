.class public Lcom/jrm/localmm/ui/main/TopDataBrowser;
.super Ljava/lang/Object;
.source "TopDataBrowser.java"


# instance fields
.field private activity:Landroid/app/Activity;

.field private data:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/jrm/localmm/business/data/BaseData;",
            ">;"
        }
    .end annotation
.end field

.field private handler:Landroid/os/Handler;

.field private source:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/os/Handler;Ljava/util/List;)V
    .locals 1
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/os/Handler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Landroid/os/Handler;",
            "Ljava/util/List",
            "<",
            "Lcom/jrm/localmm/business/data/BaseData;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/TopDataBrowser;->source:[Ljava/lang/String;

    iput-object p1, p0, Lcom/jrm/localmm/ui/main/TopDataBrowser;->activity:Landroid/app/Activity;

    iput-object p2, p0, Lcom/jrm/localmm/ui/main/TopDataBrowser;->handler:Landroid/os/Handler;

    iput-object p3, p0, Lcom/jrm/localmm/ui/main/TopDataBrowser;->data:Ljava/util/List;

    return-void
.end method

.method private browser(I)V
    .locals 5
    .param p1    # I

    const/16 v4, 0xd

    const/16 v3, 0xa

    const/4 v2, 0x4

    if-nez p1, :cond_1

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/TopDataBrowser;->handler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v1, 0x1

    if-ne p1, v1, :cond_3

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/TopDataBrowser;->activity:Landroid/app/Activity;

    invoke-static {v1}, Lcom/jrm/localmm/util/Tools;->isNetWorkConnected(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/TopDataBrowser;->handler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/jrm/localmm/ui/main/TopDataBrowser;->handler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    iput v3, v0, Landroid/os/Message;->what:I

    iput v4, v0, Landroid/os/Message;->arg1:I

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/TopDataBrowser;->handler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    :cond_3
    const/4 v1, 0x2

    if-ne p1, v1, :cond_0

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/TopDataBrowser;->activity:Landroid/app/Activity;

    invoke-static {v1}, Lcom/jrm/localmm/util/Tools;->isNetWorkConnected(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/TopDataBrowser;->handler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    :cond_4
    iget-object v1, p0, Lcom/jrm/localmm/ui/main/TopDataBrowser;->handler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    iput v3, v0, Landroid/os/Message;->what:I

    iput v4, v0, Landroid/os/Message;->arg1:I

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/TopDataBrowser;->handler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method


# virtual methods
.method protected processKeyDown(II)Z
    .locals 3
    .param p1    # I
    .param p2    # I

    const-string v0, "TopDataBrowser"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "position : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " keyCode : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v0, 0x42

    if-ne p1, v0, :cond_0

    invoke-direct {p0, p2}, Lcom/jrm/localmm/ui/main/TopDataBrowser;->browser(I)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected refresh()V
    .locals 12

    const/4 v11, 0x5

    const/4 v10, 0x1

    iget-object v6, p0, Lcom/jrm/localmm/ui/main/TopDataBrowser;->data:Ljava/util/List;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/jrm/localmm/ui/main/TopDataBrowser;->data:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->clear()V

    :cond_0
    iget-object v6, p0, Lcom/jrm/localmm/ui/main/TopDataBrowser;->source:[Ljava/lang/String;

    if-nez v6, :cond_1

    iget-object v6, p0, Lcom/jrm/localmm/ui/main/TopDataBrowser;->activity:Landroid/app/Activity;

    invoke-virtual {v6}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f050006    # com.jrm.localmm.R.array.data_source

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/jrm/localmm/ui/main/TopDataBrowser;->source:[Ljava/lang/String;

    :cond_1
    const-string v6, "TopDataBrowser"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "source[0] : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/jrm/localmm/ui/main/TopDataBrowser;->source:[Ljava/lang/String;

    const/4 v9, 0x0

    aget-object v8, v8, v9

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/TopDataBrowser;->source:[Ljava/lang/String;

    array-length v4, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v1, v0, v2

    new-instance v3, Lcom/jrm/localmm/business/data/BaseData;

    invoke-direct {v3, v11}, Lcom/jrm/localmm/business/data/BaseData;-><init>(I)V

    invoke-virtual {v3, v1}, Lcom/jrm/localmm/business/data/BaseData;->setName(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/jrm/localmm/ui/main/TopDataBrowser;->data:Ljava/util/List;

    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    iget-object v6, p0, Lcom/jrm/localmm/ui/main/TopDataBrowser;->handler:Landroid/os/Handler;

    invoke-virtual {v6}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v5

    iput v11, v5, Landroid/os/Message;->what:I

    iput v10, v5, Landroid/os/Message;->arg1:I

    iput v10, v5, Landroid/os/Message;->arg2:I

    iget-object v6, p0, Lcom/jrm/localmm/ui/main/TopDataBrowser;->handler:Landroid/os/Handler;

    invoke-virtual {v6, v5}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method
