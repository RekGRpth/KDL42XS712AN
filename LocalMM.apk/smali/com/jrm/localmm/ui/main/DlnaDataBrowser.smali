.class public Lcom/jrm/localmm/ui/main/DlnaDataBrowser;
.super Ljava/lang/Object;
.source "DlnaDataBrowser.java"


# instance fields
.field private activity:Landroid/app/Activity;

.field private currentPageIndex:I

.field private data:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/jrm/localmm/business/data/BaseData;",
            ">;"
        }
    .end annotation
.end field

.field private dlnaDataManager:Lcom/jrm/localmm/ui/main/DlnaDataManager;

.field private focusPosition:I

.field private handler:Landroid/os/Handler;

.field private isOnDevice:Z

.field private mediaType:I

.field private refreshUIListener:Lcom/jrm/localmm/ui/main/RefreshUIListener;

.field private totalDataList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/jrm/localmm/business/data/BaseData;",
            ">;"
        }
    .end annotation
.end field

.field private totalPageIndex:I


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/os/Handler;Ljava/util/List;)V
    .locals 3
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/os/Handler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Landroid/os/Handler;",
            "Ljava/util/List",
            "<",
            "Lcom/jrm/localmm/business/data/BaseData;",
            ">;)V"
        }
    .end annotation

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->currentPageIndex:I

    iput v0, p0, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->totalPageIndex:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->isOnDevice:Z

    new-instance v0, Lcom/jrm/localmm/ui/main/DlnaDataBrowser$1;

    invoke-direct {v0, p0}, Lcom/jrm/localmm/ui/main/DlnaDataBrowser$1;-><init>(Lcom/jrm/localmm/ui/main/DlnaDataBrowser;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->refreshUIListener:Lcom/jrm/localmm/ui/main/RefreshUIListener;

    iput-object p1, p0, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->activity:Landroid/app/Activity;

    iput-object p2, p0, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->handler:Landroid/os/Handler;

    iput-object p3, p0, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->data:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->totalDataList:Ljava/util/List;

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->totalDataList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    const-string v0, "DlnaDataBrowser"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DlnaDataBrowser constructor function, data size:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method static synthetic access$000(Lcom/jrm/localmm/ui/main/DlnaDataBrowser;)I
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/main/DlnaDataBrowser;

    iget v0, p0, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->focusPosition:I

    return v0
.end method

.method static synthetic access$002(Lcom/jrm/localmm/ui/main/DlnaDataBrowser;I)I
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/main/DlnaDataBrowser;
    .param p1    # I

    iput p1, p0, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->focusPosition:I

    return p1
.end method

.method static synthetic access$100(Lcom/jrm/localmm/ui/main/DlnaDataBrowser;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/main/DlnaDataBrowser;

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->data:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$200(Lcom/jrm/localmm/ui/main/DlnaDataBrowser;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/main/DlnaDataBrowser;

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->totalDataList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$302(Lcom/jrm/localmm/ui/main/DlnaDataBrowser;I)I
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/main/DlnaDataBrowser;
    .param p1    # I

    iput p1, p0, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->currentPageIndex:I

    return p1
.end method

.method static synthetic access$402(Lcom/jrm/localmm/ui/main/DlnaDataBrowser;I)I
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/main/DlnaDataBrowser;
    .param p1    # I

    iput p1, p0, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->totalPageIndex:I

    return p1
.end method

.method static synthetic access$500(Lcom/jrm/localmm/ui/main/DlnaDataBrowser;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/main/DlnaDataBrowser;

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$600(Lcom/jrm/localmm/ui/main/DlnaDataBrowser;)I
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/main/DlnaDataBrowser;

    iget v0, p0, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->mediaType:I

    return v0
.end method

.method static synthetic access$700(Lcom/jrm/localmm/ui/main/DlnaDataBrowser;II)V
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/main/DlnaDataBrowser;
    .param p1    # I
    .param p2    # I

    invoke-direct {p0, p1, p2}, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->startPlayer(II)V

    return-void
.end method

.method private processDownKeyEvent(I)Z
    .locals 2
    .param p1    # I

    iget v0, p0, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->focusPosition:I

    const/16 v1, 0x9

    if-ne v0, v1, :cond_0

    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->refresh(I)V

    :goto_0
    const/4 v0, 0x1

    return v0

    :cond_0
    iput p1, p0, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->focusPosition:I

    goto :goto_0
.end method

.method private processEnterKeyEvent(I)Z
    .locals 11
    .param p1    # I

    const v10, 0x7f060028    # com.jrm.localmm.R.string.loading_dlna_resource

    const/16 v9, 0x11

    const/16 v8, 0xb

    const/4 v6, 0x1

    const/4 v5, 0x0

    iput p1, p0, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->focusPosition:I

    if-nez p1, :cond_2

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->handler:Landroid/os/Handler;

    invoke-virtual {v4}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v2

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->data:Ljava/util/List;

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/jrm/localmm/business/data/BaseData;

    invoke-virtual {v4}, Lcom/jrm/localmm/business/data/BaseData;->getDescription()Ljava/lang/String;

    move-result-object v0

    const-string v4, "top"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->release()V

    iput-boolean v6, p0, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->isOnDevice:Z

    iput v6, v2, Landroid/os/Message;->what:I

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->handler:Landroid/os/Handler;

    invoke-virtual {v4, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_0
    :goto_0
    move v4, v6

    :goto_1
    return v4

    :cond_1
    const-string v4, "dlna"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    iput v8, v2, Landroid/os/Message;->what:I

    iput v10, v2, Landroid/os/Message;->arg1:I

    iput v9, v2, Landroid/os/Message;->arg2:I

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->handler:Landroid/os/Handler;

    invoke-virtual {v4, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    invoke-virtual {p0, v5, v5}, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->browser(II)V

    goto :goto_0

    :cond_2
    iget-object v4, p0, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->data:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge p1, v4, :cond_3

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->data:Ljava/util/List;

    invoke-interface {v4, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jrm/localmm/business/data/BaseData;

    invoke-virtual {v1}, Lcom/jrm/localmm/business/data/BaseData;->getType()I

    move-result v4

    iput v4, p0, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->mediaType:I

    const/4 v4, 0x5

    iget v7, p0, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->mediaType:I

    if-ne v4, v7, :cond_5

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->handler:Landroid/os/Handler;

    invoke-virtual {v4}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v2

    iput v8, v2, Landroid/os/Message;->what:I

    iget-boolean v4, p0, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->isOnDevice:Z

    if-eqz v4, :cond_4

    const v4, 0x7f060027    # com.jrm.localmm.R.string.loading_dlna_device

    iput v4, v2, Landroid/os/Message;->arg1:I

    iput-boolean v5, p0, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->isOnDevice:Z

    :goto_2
    iput v9, v2, Landroid/os/Message;->arg2:I

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->handler:Landroid/os/Handler;

    invoke-virtual {v4, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    invoke-virtual {p0, p1, v5}, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->browser(II)V

    goto :goto_0

    :cond_3
    const-string v4, "DlnaDataBrowser"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "processEnterKeyEvent, positon : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v4, v5

    goto :goto_1

    :cond_4
    iput v10, v2, Landroid/os/Message;->arg1:I

    goto :goto_2

    :cond_5
    iget v4, p0, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->mediaType:I

    invoke-static {v4}, Lcom/jrm/localmm/util/Tools;->isMediaFile(I)Z

    move-result v4

    if-eqz v4, :cond_6

    new-instance v3, Lcom/jrm/localmm/ui/main/DlnaDataBrowser$2;

    invoke-direct {v3, p0}, Lcom/jrm/localmm/ui/main/DlnaDataBrowser$2;-><init>(Lcom/jrm/localmm/ui/main/DlnaDataBrowser;)V

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->dlnaDataManager:Lcom/jrm/localmm/ui/main/DlnaDataManager;

    invoke-virtual {v4, v3}, Lcom/jrm/localmm/ui/main/DlnaDataManager;->pingDevice(Lcom/jrm/localmm/ui/main/PingDeviceListener;)V

    goto/16 :goto_0

    :cond_6
    iget-object v4, p0, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->handler:Landroid/os/Handler;

    invoke-virtual {v4}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v2

    const/16 v4, 0xa

    iput v4, v2, Landroid/os/Message;->what:I

    const/16 v4, 0x10

    iput v4, v2, Landroid/os/Message;->arg1:I

    iget-object v4, p0, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->handler:Landroid/os/Handler;

    invoke-virtual {v4, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0
.end method

.method private processUpKeyEvent(I)Z
    .locals 1
    .param p1    # I

    iget v0, p0, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->focusPosition:I

    if-nez v0, :cond_0

    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->refresh(I)V

    :goto_0
    const/4 v0, 0x1

    return v0

    :cond_0
    iput p1, p0, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->focusPosition:I

    goto :goto_0
.end method

.method private startPlayer(II)V
    .locals 7

    const/4 v6, 0x3

    const/4 v5, 0x2

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const-string v1, "DlnaDataBrowser"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "startplayer function, data size:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->data:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->totalDataList:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    const-string v1, "DlnaDataBrowser"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "startplayer function, local size:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "DlnaDataBrowser"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "current page,"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->currentPageIndex:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ",position:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v1, p0, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->currentPageIndex:I

    if-lez v1, :cond_0

    iget v1, p0, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->currentPageIndex:I

    add-int/lit8 v1, v1, -0x1

    mul-int/lit8 v1, v1, 0x9

    add-int/2addr p2, v1

    :cond_0
    const-string v1, "DlnaDataBrowser"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "current position:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-lez p2, :cond_2

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-gt p2, v1, :cond_2

    add-int/lit8 v1, p2, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jrm/localmm/business/data/BaseData;

    if-eqz v0, :cond_2

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    if-ne v5, p1, :cond_3

    const-string v1, "DlnaDataBrowser"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Try to play:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/jrm/localmm/business/data/BaseData;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->totalDataList:Ljava/util/List;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->activity:Landroid/app/Activity;

    const-class v3, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    invoke-direct {v0, v1, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    :goto_0
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v3, "com.jrm.arraylist"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    if-eq v6, p1, :cond_1

    if-ne v5, p1, :cond_5

    :cond_1
    const-string v3, "com.jrm.index"

    add-int/lit8 v4, p2, -0x1

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :goto_1
    const-string v3, "sourceFrom"

    const/16 v4, 0x10

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    const-string v1, "DlnaDataBrowser"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "start player from dlna, position : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " media list size : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-ne v5, p1, :cond_7

    sget-boolean v1, Lcom/jrm/localmm/util/Constants;->isExit:Z

    if-eqz v1, :cond_6

    const-string v1, "DlnaDataBrowser"

    const-string v2, "Start from here Constants.isExit is true"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->activity:Landroid/app/Activity;

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    :cond_2
    :goto_2
    return-void

    :cond_3
    if-ne v6, p1, :cond_4

    const-string v0, "DlnaDataBrowser"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "startplayer function, data size:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->data:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->totalDataList:Ljava/util/List;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    const-string v0, "DlnaDataBrowser"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "startplayer function, media file size:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "DlnaDataBrowser"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "startplayer function, media file size:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->activity:Landroid/app/Activity;

    const-class v3, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

    invoke-direct {v0, v1, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    goto/16 :goto_0

    :cond_4
    const/4 v3, 0x4

    if-ne v3, p1, :cond_8

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->activity:Landroid/app/Activity;

    const-class v3, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    invoke-direct {v0, v1, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    goto/16 :goto_0

    :cond_5
    const-string v3, "com.jrm.index"

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto/16 :goto_1

    :cond_6
    const-string v0, "DlnaDataBrowser"

    const-string v1, "Start from here Constants.isExit is false"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->handler:Landroid/os/Handler;

    const/16 v1, 0x64

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_2

    :cond_7
    iget-object v1, p0, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->activity:Landroid/app/Activity;

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_2

    :cond_8
    move-object v0, v1

    goto/16 :goto_0
.end method


# virtual methods
.method protected browser(II)V
    .locals 3
    .param p1    # I
    .param p2    # I

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->dlnaDataManager:Lcom/jrm/localmm/ui/main/DlnaDataManager;

    if-nez v0, :cond_0

    new-instance v0, Lcom/jrm/localmm/ui/main/DlnaDataManager;

    iget-object v1, p0, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->activity:Landroid/app/Activity;

    iget-object v2, p0, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->refreshUIListener:Lcom/jrm/localmm/ui/main/RefreshUIListener;

    invoke-direct {v0, v1, v2}, Lcom/jrm/localmm/ui/main/DlnaDataManager;-><init>(Landroid/app/Activity;Lcom/jrm/localmm/ui/main/RefreshUIListener;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->dlnaDataManager:Lcom/jrm/localmm/ui/main/DlnaDataManager;

    :cond_0
    const-string v0, "DlnaDataBrowser"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "index : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " type : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->dlnaDataManager:Lcom/jrm/localmm/ui/main/DlnaDataManager;

    invoke-virtual {v0, p1, p2}, Lcom/jrm/localmm/ui/main/DlnaDataManager;->browser(II)V

    return-void
.end method

.method protected processKeyDown(II)Z
    .locals 3
    .param p1    # I
    .param p2    # I

    const-string v0, "DlnaDataBrowser"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "processKeyDown, keyCode : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " position : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v0, 0x42

    if-ne p1, v0, :cond_0

    invoke-direct {p0, p2}, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->processEnterKeyEvent(I)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x13

    if-ne p1, v0, :cond_1

    invoke-direct {p0, p2}, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->processUpKeyEvent(I)Z

    move-result v0

    goto :goto_0

    :cond_1
    const/16 v0, 0x14

    if-ne p1, v0, :cond_2

    invoke-direct {p0, p2}, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->processDownKeyEvent(I)Z

    move-result v0

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected refresh(I)V
    .locals 2
    .param p1    # I

    const/4 v0, 0x5

    if-eq p1, v0, :cond_0

    const/4 v0, 0x7

    if-ne p1, v0, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->dlnaDataManager:Lcom/jrm/localmm/ui/main/DlnaDataManager;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/main/DlnaDataManager;->getCurrentPage(I)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    const/4 v0, 0x6

    if-eq p1, v0, :cond_3

    const/16 v0, 0x8

    if-ne p1, v0, :cond_1

    :cond_3
    iget-object v0, p0, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->dlnaDataManager:Lcom/jrm/localmm/ui/main/DlnaDataManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/main/DlnaDataManager;->getCurrentPage(I)V

    goto :goto_0
.end method

.method protected release()V
    .locals 1

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->dlnaDataManager:Lcom/jrm/localmm/ui/main/DlnaDataManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->dlnaDataManager:Lcom/jrm/localmm/ui/main/DlnaDataManager;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/main/DlnaDataManager;->release()V

    :cond_0
    return-void
.end method

.method public startPlayer()V
    .locals 2

    iget v0, p0, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->mediaType:I

    iget v1, p0, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->focusPosition:I

    invoke-direct {p0, v0, v1}, Lcom/jrm/localmm/ui/main/DlnaDataBrowser;->startPlayer(II)V

    return-void
.end method
