.class Lcom/jrm/localmm/ui/main/FileBrowserViewHolder$2;
.super Ljava/lang/Object;
.source "FileBrowserViewHolder.java"

# interfaces
.implements Landroid/view/View$OnHoverListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;


# direct methods
.method constructor <init>(Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder$2;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onHover(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/view/MotionEvent;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder$2;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->activity:Lcom/jrm/localmm/ui/main/FileBrowserActivity;
    invoke-static {v2}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->access$000(Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;)Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    move-result-object v2

    iget-boolean v2, v2, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->canResponse:Z

    if-eqz v2, :cond_1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return v1

    :pswitch_0
    iget-object v2, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder$2;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    # invokes: Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->changeFocus(I)V
    invoke-static {v2, v3}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->access$200(Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;I)V

    goto :goto_0

    :pswitch_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    const v3, 0x7f080016    # com.jrm.localmm.R.id.videoFileList

    if-eq v2, v3, :cond_0

    iget-object v2, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder$2;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;

    iget-object v3, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder$2;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->activity:Lcom/jrm/localmm/ui/main/FileBrowserActivity;
    invoke-static {v3}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->access$000(Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;)Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    move-result-object v3

    iget v3, v3, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->tmpType:I

    invoke-virtual {v2, v3, v1}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->changeLeftFocus(II)V

    iget-object v2, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder$2;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;

    iget-object v3, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder$2;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->activity:Lcom/jrm/localmm/ui/main/FileBrowserActivity;
    invoke-static {v3}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->access$000(Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;)Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    move-result-object v3

    iget v3, v3, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->dataType:I

    invoke-virtual {v2, v3, v1}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->setLeftFocus(IZ)V

    iget-object v2, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder$2;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->activity:Lcom/jrm/localmm/ui/main/FileBrowserActivity;
    invoke-static {v2}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->access$000(Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;)Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder$2;->this$0:Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;

    # getter for: Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->activity:Lcom/jrm/localmm/ui/main/FileBrowserActivity;
    invoke-static {v3}, Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;->access$000(Lcom/jrm/localmm/ui/main/FileBrowserViewHolder;)Lcom/jrm/localmm/ui/main/FileBrowserActivity;

    move-result-object v3

    iget v3, v3, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->dataType:I

    iput v3, v2, Lcom/jrm/localmm/ui/main/FileBrowserActivity;->tmpType:I

    goto :goto_0

    :cond_1
    const/4 v1, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x9
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
