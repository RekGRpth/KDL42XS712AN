.class Lcom/jrm/localmm/ui/main/BaseDataManager$1;
.super Ljava/lang/Object;
.source "BaseDataManager.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jrm/localmm/ui/main/BaseDataManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/jrm/localmm/business/data/BaseData;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/localmm/ui/main/BaseDataManager;


# direct methods
.method constructor <init>(Lcom/jrm/localmm/ui/main/BaseDataManager;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/localmm/ui/main/BaseDataManager$1;->this$0:Lcom/jrm/localmm/ui/main/BaseDataManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/jrm/localmm/business/data/BaseData;Lcom/jrm/localmm/business/data/BaseData;)I
    .locals 5
    .param p1    # Lcom/jrm/localmm/business/data/BaseData;
    .param p2    # Lcom/jrm/localmm/business/data/BaseData;

    invoke-virtual {p1}, Lcom/jrm/localmm/business/data/BaseData;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lcom/jrm/localmm/business/data/BaseData;->getName()Ljava/lang/String;

    move-result-object v2

    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    sget-object v3, Ljava/util/Locale;->CHINA:Ljava/util/Locale;

    invoke-static {v3}, Ljava/text/Collator;->getInstance(Ljava/util/Locale;)Ljava/text/Collator;

    move-result-object v0

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    :goto_0
    return v3

    :cond_0
    const-string v3, "comparator"

    const-string v4, "lName != null && rName != null is false"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v3, 0x0

    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p1, Lcom/jrm/localmm/business/data/BaseData;

    check-cast p2, Lcom/jrm/localmm/business/data/BaseData;

    invoke-virtual {p0, p1, p2}, Lcom/jrm/localmm/ui/main/BaseDataManager$1;->compare(Lcom/jrm/localmm/business/data/BaseData;Lcom/jrm/localmm/business/data/BaseData;)I

    move-result v0

    return v0
.end method
