.class public Lcom/jrm/localmm/ui/MediaContainerApplication;
.super Landroid/app/Application;
.source "MediaContainerApplication.java"


# static fields
.field private static instance:Lcom/jrm/localmm/ui/MediaContainerApplication;


# instance fields
.field private allFileList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/jrm/localmm/business/data/BaseData;",
            ">;"
        }
    .end annotation
.end field

.field private allFolderList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/jrm/localmm/business/data/BaseData;",
            ">;"
        }
    .end annotation
.end field

.field private allPictureFileList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/jrm/localmm/business/data/BaseData;",
            ">;"
        }
    .end annotation
.end field

.field private allSongFileList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/jrm/localmm/business/data/BaseData;",
            ">;"
        }
    .end annotation
.end field

.field private allVideoFileList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/jrm/localmm/business/data/BaseData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    iput-object v0, p0, Lcom/jrm/localmm/ui/MediaContainerApplication;->allFileList:Ljava/util/List;

    iput-object v0, p0, Lcom/jrm/localmm/ui/MediaContainerApplication;->allFolderList:Ljava/util/List;

    iput-object v0, p0, Lcom/jrm/localmm/ui/MediaContainerApplication;->allPictureFileList:Ljava/util/List;

    iput-object v0, p0, Lcom/jrm/localmm/ui/MediaContainerApplication;->allSongFileList:Ljava/util/List;

    iput-object v0, p0, Lcom/jrm/localmm/ui/MediaContainerApplication;->allVideoFileList:Ljava/util/List;

    return-void
.end method

.method public static getInstance()Lcom/jrm/localmm/ui/MediaContainerApplication;
    .locals 1

    sget-object v0, Lcom/jrm/localmm/ui/MediaContainerApplication;->instance:Lcom/jrm/localmm/ui/MediaContainerApplication;

    return-object v0
.end method


# virtual methods
.method public final clearAll()V
    .locals 1

    iget-object v0, p0, Lcom/jrm/localmm/ui/MediaContainerApplication;->allFileList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/MediaContainerApplication;->allFolderList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/MediaContainerApplication;->allPictureFileList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/MediaContainerApplication;->allSongFileList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/MediaContainerApplication;->allVideoFileList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    return-void
.end method

.method public final getMediaData(I)Ljava/util/List;
    .locals 2
    .param p1    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lcom/jrm/localmm/business/data/BaseData;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-object v0

    :pswitch_0
    iget-object v1, p0, Lcom/jrm/localmm/ui/MediaContainerApplication;->allFileList:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    :pswitch_1
    iget-object v1, p0, Lcom/jrm/localmm/ui/MediaContainerApplication;->allPictureFileList:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    :pswitch_2
    iget-object v1, p0, Lcom/jrm/localmm/ui/MediaContainerApplication;->allSongFileList:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    :pswitch_3
    iget-object v1, p0, Lcom/jrm/localmm/ui/MediaContainerApplication;->allVideoFileList:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    :pswitch_4
    iget-object v1, p0, Lcom/jrm/localmm/ui/MediaContainerApplication;->allFolderList:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final hasMedia(I)Z
    .locals 2
    .param p1    # I

    const/4 v0, 0x0

    packed-switch p1, :pswitch_data_0

    :goto_0
    return v0

    :pswitch_0
    iget-object v1, p0, Lcom/jrm/localmm/ui/MediaContainerApplication;->allPictureFileList:Ljava/util/List;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/jrm/localmm/ui/MediaContainerApplication;->allPictureFileList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_1
    iget-object v1, p0, Lcom/jrm/localmm/ui/MediaContainerApplication;->allSongFileList:Ljava/util/List;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/jrm/localmm/ui/MediaContainerApplication;->allSongFileList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_3

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_2
    iget-object v1, p0, Lcom/jrm/localmm/ui/MediaContainerApplication;->allVideoFileList:Ljava/util/List;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/jrm/localmm/ui/MediaContainerApplication;->allVideoFileList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_5

    :cond_4
    const/4 v0, 0x0

    goto :goto_0

    :cond_5
    const/4 v0, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onCreate()V
    .locals 1

    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    sput-object p0, Lcom/jrm/localmm/ui/MediaContainerApplication;->instance:Lcom/jrm/localmm/ui/MediaContainerApplication;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/jrm/localmm/ui/MediaContainerApplication;->allFileList:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/jrm/localmm/ui/MediaContainerApplication;->allFolderList:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/jrm/localmm/ui/MediaContainerApplication;->allPictureFileList:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/jrm/localmm/ui/MediaContainerApplication;->allSongFileList:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/jrm/localmm/ui/MediaContainerApplication;->allVideoFileList:Ljava/util/List;

    return-void
.end method

.method public final putMediaData(ILjava/util/List;)V
    .locals 1
    .param p1    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Lcom/jrm/localmm/business/data/BaseData;",
            ">;)V"
        }
    .end annotation

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/MediaContainerApplication;->allFileList:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/jrm/localmm/ui/MediaContainerApplication;->allPictureFileList:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/jrm/localmm/ui/MediaContainerApplication;->allSongFileList:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/jrm/localmm/ui/MediaContainerApplication;->allVideoFileList:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    :pswitch_4
    iget-object v0, p0, Lcom/jrm/localmm/ui/MediaContainerApplication;->allFolderList:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
