.class Lcom/jrm/localmm/ui/music/MusicPlayerListener$1;
.super Ljava/lang/Object;
.source "MusicPlayerListener.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/jrm/localmm/ui/music/MusicPlayerListener;->addMusicListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/localmm/ui/music/MusicPlayerListener;


# direct methods
.method constructor <init>(Lcom/jrm/localmm/ui/music/MusicPlayerListener;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener$1;->this$0:Lcom/jrm/localmm/ui/music/MusicPlayerListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    const/4 v2, 0x1

    sput v2, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->state:I

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener$1;->this$0:Lcom/jrm/localmm/ui/music/MusicPlayerListener;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->setViewDefault()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener$1;->this$0:Lcom/jrm/localmm/ui/music/MusicPlayerListener;

    # getter for: Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;
    invoke-static {v0}, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->access$000(Lcom/jrm/localmm/ui/music/MusicPlayerListener;)Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    move-result-object v0

    iget-object v0, v0, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicPlay:Landroid/widget/ImageView;

    const v1, 0x7f020069    # com.jrm.localmm.R.drawable.player_icon_pause

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener$1;->this$0:Lcom/jrm/localmm/ui/music/MusicPlayerListener;

    # getter for: Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;
    invoke-static {v0}, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->access$000(Lcom/jrm/localmm/ui/music/MusicPlayerListener;)Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    move-result-object v0

    iget-object v0, v0, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicPre:Lcom/jrm/localmm/ui/music/RepeatingImageButton;

    const v1, 0x7f02006e    # com.jrm.localmm.R.drawable.player_icon_previous_focus

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/music/RepeatingImageButton;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener$1;->this$0:Lcom/jrm/localmm/ui/music/MusicPlayerListener;

    # getter for: Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerActivity:Lcom/jrm/localmm/ui/music/MusicPlayerActivity;
    invoke-static {v0}, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->access$100(Lcom/jrm/localmm/ui/music/MusicPlayerListener;)Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

    move-result-object v0

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->isNextMusic:Z

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener$1;->this$0:Lcom/jrm/localmm/ui/music/MusicPlayerListener;

    # getter for: Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerActivity:Lcom/jrm/localmm/ui/music/MusicPlayerActivity;
    invoke-static {v0}, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->access$100(Lcom/jrm/localmm/ui/music/MusicPlayerListener;)Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

    move-result-object v0

    iput-boolean v2, v0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->clickable:Z

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener$1;->this$0:Lcom/jrm/localmm/ui/music/MusicPlayerListener;

    # getter for: Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerActivity:Lcom/jrm/localmm/ui/music/MusicPlayerActivity;
    invoke-static {v0}, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->access$100(Lcom/jrm/localmm/ui/music/MusicPlayerListener;)Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

    move-result-object v0

    iget-object v0, v0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->musicPlayHandle:Landroid/os/Handler;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener$1;->this$0:Lcom/jrm/localmm/ui/music/MusicPlayerListener;

    # getter for: Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerActivity:Lcom/jrm/localmm/ui/music/MusicPlayerActivity;
    invoke-static {v0}, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->access$100(Lcom/jrm/localmm/ui/music/MusicPlayerListener;)Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->processPlayCompletion()V

    sput-boolean v2, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->isPlaying:Z

    return-void
.end method
