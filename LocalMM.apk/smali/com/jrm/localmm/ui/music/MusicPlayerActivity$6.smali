.class Lcom/jrm/localmm/ui/music/MusicPlayerActivity$6;
.super Ljava/lang/Object;
.source "MusicPlayerActivity.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jrm/localmm/ui/music/MusicPlayerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/localmm/ui/music/MusicPlayerActivity;


# direct methods
.method constructor <init>(Lcom/jrm/localmm/ui/music/MusicPlayerActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity$6;->this$0:Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3
    .param p1    # Landroid/content/ComponentName;
    .param p2    # Landroid/os/IBinder;

    invoke-static {p2}, Lcom/jrm/localmm/ui/music/IMediaService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/jrm/localmm/ui/music/IMediaService;

    move-result-object v1

    sput-object v1, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mService:Lcom/jrm/localmm/ui/music/IMediaService;

    sget-object v1, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mService:Lcom/jrm/localmm/ui/music/IMediaService;

    if-nez v1, :cond_0

    const-string v1, "MusicPlayerActivity"

    const-string v2, "ServiceConnection:::onServiceConnected"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    :try_start_0
    sget-object v1, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mService:Lcom/jrm/localmm/ui/music/IMediaService;

    iget-object v2, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity$6;->this$0:Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->musicStatusListener:Lcom/jrm/localmm/ui/music/IMusicStatusListener;
    invoke-static {v2}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->access$1300(Lcom/jrm/localmm/ui/music/MusicPlayerActivity;)Lcom/jrm/localmm/ui/music/IMusicStatusListener;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/jrm/localmm/ui/music/IMediaService;->setMusicStatusListener(Lcom/jrm/localmm/ui/music/IMusicStatusListener;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity$6;->this$0:Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->musicPlayHandle:Landroid/os/Handler;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 1
    .param p1    # Landroid/content/ComponentName;

    const/4 v0, 0x0

    sput-object v0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mService:Lcom/jrm/localmm/ui/music/IMediaService;

    return-void
.end method
