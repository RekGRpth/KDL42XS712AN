.class Lcom/jrm/localmm/ui/music/MediaService$ServiceStub;
.super Lcom/jrm/localmm/ui/music/IMediaService$Stub;
.source "MediaService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jrm/localmm/ui/music/MediaService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ServiceStub"
.end annotation


# instance fields
.field mService:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/jrm/localmm/ui/music/MediaService;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/jrm/localmm/ui/music/MediaService;


# direct methods
.method constructor <init>(Lcom/jrm/localmm/ui/music/MediaService;Lcom/jrm/localmm/ui/music/MediaService;)V
    .locals 1
    .param p2    # Lcom/jrm/localmm/ui/music/MediaService;

    iput-object p1, p0, Lcom/jrm/localmm/ui/music/MediaService$ServiceStub;->this$0:Lcom/jrm/localmm/ui/music/MediaService;

    invoke-direct {p0}, Lcom/jrm/localmm/ui/music/IMediaService$Stub;-><init>()V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/music/MediaService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    return-void
.end method


# virtual methods
.method public continuePlay()V
    .locals 1

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MediaService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jrm/localmm/ui/music/MediaService;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/music/MediaService;->start()V

    return-void
.end method

.method public getAudioSessionId()I
    .locals 1

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MediaService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jrm/localmm/ui/music/MediaService;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/music/MediaService;->getAudioSessionId()I

    move-result v0

    return v0
.end method

.method public getCurrentPosition()J
    .locals 2

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MediaService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jrm/localmm/ui/music/MediaService;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/music/MediaService;->getCurrentPosition()J

    move-result-wide v0

    return-wide v0
.end method

.method public getDuration()J
    .locals 2

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MediaService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jrm/localmm/ui/music/MediaService;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/music/MediaService;->getDuration()J

    move-result-wide v0

    return-wide v0
.end method

.method public initPlayer(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;

    const-string v0, "MediaService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "initPlayer, current thread id : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getId()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MediaService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jrm/localmm/ui/music/MediaService;

    invoke-virtual {v0, p1}, Lcom/jrm/localmm/ui/music/MediaService;->openPlayer(Ljava/lang/String;)V

    return-void
.end method

.method public isPlaying()Z
    .locals 1

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MediaService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MediaService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jrm/localmm/ui/music/MediaService;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/music/MediaService;->isPlaying()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public next(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MediaService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jrm/localmm/ui/music/MediaService;

    invoke-virtual {v0, p1}, Lcom/jrm/localmm/ui/music/MediaService;->next(Ljava/lang/String;)V

    return-void
.end method

.method public pause()V
    .locals 1

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MediaService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jrm/localmm/ui/music/MediaService;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/music/MediaService;->pause()V

    return-void
.end method

.method public playerToPosiztion(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MediaService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jrm/localmm/ui/music/MediaService;

    invoke-virtual {v0, p1}, Lcom/jrm/localmm/ui/music/MediaService;->seekTo(I)V

    return-void
.end method

.method public setMusicStatusListener(Lcom/jrm/localmm/ui/music/IMusicStatusListener;)V
    .locals 1
    .param p1    # Lcom/jrm/localmm/ui/music/IMusicStatusListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MediaService$ServiceStub;->this$0:Lcom/jrm/localmm/ui/music/MediaService;

    # setter for: Lcom/jrm/localmm/ui/music/MediaService;->mMusicStatusListener:Lcom/jrm/localmm/ui/music/IMusicStatusListener;
    invoke-static {v0, p1}, Lcom/jrm/localmm/ui/music/MediaService;->access$402(Lcom/jrm/localmm/ui/music/MediaService;Lcom/jrm/localmm/ui/music/IMusicStatusListener;)Lcom/jrm/localmm/ui/music/IMusicStatusListener;

    return-void
.end method

.method public stop()V
    .locals 1

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MediaService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jrm/localmm/ui/music/MediaService;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/music/MediaService;->stop()V

    return-void
.end method
