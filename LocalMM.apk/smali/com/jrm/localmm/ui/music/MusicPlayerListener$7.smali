.class Lcom/jrm/localmm/ui/music/MusicPlayerListener$7;
.super Ljava/lang/Object;
.source "MusicPlayerListener.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jrm/localmm/ui/music/MusicPlayerListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/localmm/ui/music/MusicPlayerListener;


# direct methods
.method constructor <init>(Lcom/jrm/localmm/ui/music/MusicPlayerListener;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener$7;->this$0:Lcom/jrm/localmm/ui/music/MusicPlayerListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 0
    .param p1    # Landroid/widget/SeekBar;
    .param p2    # I
    .param p3    # Z

    return-void
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1    # Landroid/widget/SeekBar;

    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 2
    .param p1    # Landroid/widget/SeekBar;

    invoke-virtual {p1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v0

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener$7;->this$0:Lcom/jrm/localmm/ui/music/MusicPlayerListener;

    # getter for: Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerActivity:Lcom/jrm/localmm/ui/music/MusicPlayerActivity;
    invoke-static {v1}, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->access$100(Lcom/jrm/localmm/ui/music/MusicPlayerListener;)Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->seekTo(I)V

    return-void
.end method
