.class Lcom/jrm/localmm/ui/music/MusicPlayerActivity$3;
.super Landroid/os/Handler;
.source "MusicPlayerActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jrm/localmm/ui/music/MusicPlayerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/localmm/ui/music/MusicPlayerActivity;


# direct methods
.method constructor <init>(Lcom/jrm/localmm/ui/music/MusicPlayerActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity$3;->this$0:Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1    # Landroid/os/Message;

    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    :try_start_0
    sget-object v1, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mService:Lcom/jrm/localmm/ui/music/IMediaService;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mService:Lcom/jrm/localmm/ui/music/IMediaService;

    invoke-interface {v1}, Lcom/jrm/localmm/ui/music/IMediaService;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mService:Lcom/jrm/localmm/ui/music/IMediaService;

    invoke-interface {v1}, Lcom/jrm/localmm/ui/music/IMediaService;->getCurrentPosition()J

    move-result-wide v1

    long-to-int v1, v1

    # setter for: Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->currentTime:I
    invoke-static {v1}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->access$402(I)I

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity$3;->this$0:Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;
    invoke-static {v1}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->access$500(Lcom/jrm/localmm/ui/music/MusicPlayerActivity;)Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    move-result-object v1

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->playTime:Landroid/widget/TextView;

    # getter for: Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->currentTime:I
    invoke-static {}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->access$400()I

    move-result v2

    invoke-static {v2}, Lcom/jrm/localmm/util/Tools;->formatDuration2(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    sget v1, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->countTime:I

    sget-object v2, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mService:Lcom/jrm/localmm/ui/music/IMediaService;

    invoke-interface {v2}, Lcom/jrm/localmm/ui/music/IMediaService;->getDuration()J

    move-result-wide v2

    long-to-int v2, v2

    if-eq v1, v2, :cond_1

    sget-object v1, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mService:Lcom/jrm/localmm/ui/music/IMediaService;

    invoke-interface {v1}, Lcom/jrm/localmm/ui/music/IMediaService;->getDuration()J

    move-result-wide v1

    long-to-int v1, v1

    sput v1, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->countTime:I

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity$3;->this$0:Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;
    invoke-static {v1}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->access$500(Lcom/jrm/localmm/ui/music/MusicPlayerActivity;)Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    move-result-object v1

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->durationTime:Landroid/widget/TextView;

    sget v2, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->countTime:I

    invoke-static {v2}, Lcom/jrm/localmm/util/Tools;->formatDuration2(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity$3;->this$0:Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;
    invoke-static {v1}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->access$500(Lcom/jrm/localmm/ui/music/MusicPlayerActivity;)Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    move-result-object v1

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->seekBar:Landroid/widget/SeekBar;

    sget v2, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->countTime:I

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setMax(I)V

    :cond_1
    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity$3;->this$0:Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;
    invoke-static {v1}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->access$500(Lcom/jrm/localmm/ui/music/MusicPlayerActivity;)Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    move-result-object v1

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->seekBar:Landroid/widget/SeekBar;

    # getter for: Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->currentTime:I
    invoke-static {}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->access$400()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setProgress(I)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity$3;->this$0:Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

    iget-boolean v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->isLrcShow:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity$3;->this$0:Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

    # invokes: Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->setLRCIndex()V
    invoke-static {v1}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->access$600(Lcom/jrm/localmm/ui/music/MusicPlayerActivity;)V

    :cond_2
    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity$3;->this$0:Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->musicPlayHandle:Landroid/os/Handler;

    const/4 v2, 0x7

    const-wide/16 v3, 0x1f4

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    :pswitch_2
    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity$3;->this$0:Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;
    invoke-static {v1}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->access$500(Lcom/jrm/localmm/ui/music/MusicPlayerActivity;)Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    move-result-object v1

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->mLyricView:Lcom/jrm/localmm/ui/music/LyricView;

    # getter for: Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->index:I
    invoke-static {}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->access$700()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/jrm/localmm/ui/music/LyricView;->SetIndex(I)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity$3;->this$0:Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;
    invoke-static {v1}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->access$500(Lcom/jrm/localmm/ui/music/MusicPlayerActivity;)Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    move-result-object v1

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->mLyricView:Lcom/jrm/localmm/ui/music/LyricView;

    invoke-virtual {v1}, Lcom/jrm/localmm/ui/music/LyricView;->invalidate()V

    goto/16 :goto_0

    :pswitch_3
    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity$3;->this$0:Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

    # invokes: Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->initPlay()V
    invoke-static {v1}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->access$800(Lcom/jrm/localmm/ui/music/MusicPlayerActivity;)V

    goto/16 :goto_0

    :pswitch_4
    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity$3;->this$0:Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

    # invokes: Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->processPlayPrepare()V
    invoke-static {v1}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->access$900(Lcom/jrm/localmm/ui/music/MusicPlayerActivity;)V

    goto/16 :goto_0

    :pswitch_5
    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity$3;->this$0:Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mMusicPlayerListener:Lcom/jrm/localmm/ui/music/MusicPlayerListener;
    invoke-static {v1}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->access$1000(Lcom/jrm/localmm/ui/music/MusicPlayerActivity;)Lcom/jrm/localmm/ui/music/MusicPlayerListener;

    move-result-object v1

    invoke-virtual {v1}, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->registerListeners()V

    goto/16 :goto_0

    :pswitch_6
    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity$3;->this$0:Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

    invoke-virtual {v1}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->finish()V

    goto/16 :goto_0

    :pswitch_7
    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity$3;->this$0:Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mMusicPlayerListener:Lcom/jrm/localmm/ui/music/MusicPlayerListener;
    invoke-static {v1}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->access$1000(Lcom/jrm/localmm/ui/music/MusicPlayerActivity;)Lcom/jrm/localmm/ui/music/MusicPlayerListener;

    move-result-object v1

    sget-object v2, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->musicList:Ljava/util/List;

    sget v3, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->currentPosition:I

    invoke-virtual {v1, v2, v3}, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->getPicOfAlbum(Ljava/util/List;I)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity$3;->this$0:Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->mMusicPlayerListener:Lcom/jrm/localmm/ui/music/MusicPlayerListener;
    invoke-static {v1}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->access$1000(Lcom/jrm/localmm/ui/music/MusicPlayerActivity;)Lcom/jrm/localmm/ui/music/MusicPlayerListener;

    move-result-object v1

    sget-object v2, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->musicList:Ljava/util/List;

    sget v3, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->currentPosition:I

    invoke-virtual {v1, v2, v3}, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->setSongsContent(Ljava/util/List;I)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method
