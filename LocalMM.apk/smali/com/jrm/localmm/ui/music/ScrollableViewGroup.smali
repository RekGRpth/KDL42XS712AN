.class public Lcom/jrm/localmm/ui/music/ScrollableViewGroup;
.super Landroid/view/ViewGroup;
.source "ScrollableViewGroup.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/jrm/localmm/ui/music/ScrollableViewGroup$OnCurrentViewChangedListener;
    }
.end annotation


# instance fields
.field private mCurrentScreen:I

.field private mDefaultScreen:I

.field private mFirstLayout:Z

.field private mNextScreen:I

.field private mOnCurrentViewChangedListener:Lcom/jrm/localmm/ui/music/ScrollableViewGroup$OnCurrentViewChangedListener;

.field private mPaintFlag:I

.field private mScroller:Landroid/widget/Scroller;

.field private mTouchState:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->mNextScreen:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->mFirstLayout:Z

    const/4 v0, 0x0

    iput v0, p0, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->mPaintFlag:I

    invoke-direct {p0}, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->initViewGroup()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->mNextScreen:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->mFirstLayout:Z

    const/4 v0, 0x0

    iput v0, p0, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->mPaintFlag:I

    invoke-direct {p0}, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->initViewGroup()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->mNextScreen:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->mFirstLayout:Z

    const/4 v0, 0x0

    iput v0, p0, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->mPaintFlag:I

    invoke-direct {p0}, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->initViewGroup()V

    return-void
.end method

.method private clearChildrenCache()V
    .locals 4

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->getChildCount()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    invoke-virtual {p0, v1}, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    instance-of v3, v2, Landroid/view/ViewGroup;

    if-eqz v3, :cond_0

    check-cast v2, Landroid/view/ViewGroup;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->setAlwaysDrawnWithCacheEnabled(Z)V

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private enableChildrenCache()V
    .locals 5

    const/4 v4, 0x1

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->getChildCount()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    invoke-virtual {p0, v1}, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    instance-of v3, v2, Landroid/view/ViewGroup;

    if-eqz v3, :cond_0

    check-cast v2, Landroid/view/ViewGroup;

    invoke-virtual {v2, v4}, Landroid/view/ViewGroup;->setAlwaysDrawnWithCacheEnabled(Z)V

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private initViewGroup()V
    .locals 2

    new-instance v0, Landroid/widget/Scroller;

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->mScroller:Landroid/widget/Scroller;

    iget v0, p0, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->mDefaultScreen:I

    iput v0, p0, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->mCurrentScreen:I

    return-void
.end method

.method private snapToScreen(I)V
    .locals 11
    .param p1    # I

    const/4 v6, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->enableChildrenCache()V

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->getChildCount()I

    move-result v0

    add-int/lit8 v10, v0, -0x1

    move v9, p1

    if-gez p1, :cond_2

    move p1, v10

    const/4 v0, -0x1

    iput v0, p0, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->mPaintFlag:I

    :goto_1
    iget v0, p0, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->mCurrentScreen:I

    if-eq p1, v0, :cond_4

    :goto_2
    iput p1, p0, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->mNextScreen:I

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->getFocusedChild()Landroid/view/View;

    move-result-object v7

    if-eqz v7, :cond_1

    if-eqz v6, :cond_1

    iget v0, p0, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->mCurrentScreen:I

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    if-ne v7, v0, :cond_1

    invoke-virtual {v7}, Landroid/view/View;->clearFocus()V

    :cond_1
    invoke-virtual {p0}, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->getWidth()I

    move-result v0

    mul-int v8, v9, v0

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->getScrollX()I

    move-result v0

    sub-int v3, v8, v0

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->getScrollX()I

    move-result v1

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v4

    mul-int/lit8 v5, v4, 0x2

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/widget/Scroller;->startScroll(IIIII)V

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->invalidate()V

    goto :goto_0

    :cond_2
    if-le p1, v10, :cond_3

    const/4 p1, 0x0

    iput v6, p0, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->mPaintFlag:I

    goto :goto_1

    :cond_3
    iput v2, p0, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->mPaintFlag:I

    goto :goto_1

    :cond_4
    move v6, v2

    goto :goto_2
.end method


# virtual methods
.method public computeScroll()V
    .locals 9

    const/4 v8, 0x0

    const/4 v7, -0x1

    iget-object v5, p0, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v5}, Landroid/widget/Scroller;->computeScrollOffset()Z

    move-result v5

    if-eqz v5, :cond_4

    iget-object v5, p0, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v5}, Landroid/widget/Scroller;->getCurrX()I

    move-result v0

    iget-object v5, p0, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v5}, Landroid/widget/Scroller;->getCurrY()I

    move-result v1

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->getScrollX()I

    move-result v3

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->getScrollY()I

    move-result v4

    if-ne v0, v3, :cond_0

    if-eq v1, v4, :cond_3

    :cond_0
    iget-object v5, p0, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v5}, Landroid/widget/Scroller;->getCurrX()I

    move-result v5

    iget-object v6, p0, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v6}, Landroid/widget/Scroller;->getCurrY()I

    move-result v6

    invoke-virtual {p0, v5, v6}, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->scrollTo(II)V

    :cond_1
    :goto_0
    iget-object v5, p0, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->mOnCurrentViewChangedListener:Lcom/jrm/localmm/ui/music/ScrollableViewGroup$OnCurrentViewChangedListener;

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->mOnCurrentViewChangedListener:Lcom/jrm/localmm/ui/music/ScrollableViewGroup$OnCurrentViewChangedListener;

    iget v6, p0, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->mCurrentScreen:I

    invoke-interface {v5, p0, v6}, Lcom/jrm/localmm/ui/music/ScrollableViewGroup$OnCurrentViewChangedListener;->onCurrentViewChanged(Landroid/view/View;I)V

    :cond_2
    return-void

    :cond_3
    invoke-virtual {p0}, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->invalidate()V

    goto :goto_0

    :cond_4
    iget v5, p0, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->mNextScreen:I

    if-eq v5, v7, :cond_1

    iget v5, p0, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->mNextScreen:I

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->getChildCount()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v5

    invoke-static {v8, v5}, Ljava/lang/Math;->max(II)I

    move-result v5

    iput v5, p0, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->mCurrentScreen:I

    iput v7, p0, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->mNextScreen:I

    iput v8, p0, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->mPaintFlag:I

    invoke-direct {p0}, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->clearChildrenCache()V

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->getScrollX()I

    move-result v3

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->getScrollY()I

    move-result v4

    iget v5, p0, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->mCurrentScreen:I

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->getWidth()I

    move-result v6

    mul-int v2, v5, v6

    if-eq v3, v2, :cond_1

    invoke-virtual {p0, v2, v4}, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->scrollTo(II)V

    goto :goto_0
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 12
    .param p1    # Landroid/graphics/Canvas;

    const/4 v9, 0x0

    const/4 v8, 0x1

    iget v10, p0, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->mTouchState:I

    if-eq v10, v8, :cond_1

    iget v10, p0, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->mNextScreen:I

    const/4 v11, -0x1

    if-ne v10, v11, :cond_1

    move v3, v8

    :goto_0
    if-eqz v3, :cond_2

    iget v8, p0, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->mCurrentScreen:I

    invoke-virtual {p0, v8}, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->getDrawingTime()J

    move-result-wide v9

    invoke-virtual {p0, p1, v8, v9, v10}, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    :cond_0
    :goto_1
    return-void

    :cond_1
    move v3, v9

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->getDrawingTime()J

    move-result-wide v1

    iget v10, p0, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->mNextScreen:I

    if-ltz v10, :cond_6

    iget v10, p0, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->mNextScreen:I

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->getChildCount()I

    move-result v11

    if-ge v10, v11, :cond_6

    iget v10, p0, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->mCurrentScreen:I

    iget v11, p0, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->mNextScreen:I

    sub-int/2addr v10, v11

    invoke-static {v10}, Ljava/lang/Math;->abs(I)I

    move-result v10

    if-eq v10, v8, :cond_3

    iget v8, p0, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->mPaintFlag:I

    if-eqz v8, :cond_6

    :cond_3
    iget v8, p0, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->mCurrentScreen:I

    invoke-virtual {p0, v8}, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    iget v8, p0, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->mNextScreen:I

    invoke-virtual {p0, v8}, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    invoke-virtual {p0, p1, v6, v1, v2}, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    iget v8, p0, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->mPaintFlag:I

    if-nez v8, :cond_4

    invoke-virtual {p0, p1, v7, v1, v2}, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    goto :goto_1

    :cond_4
    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    iget v8, p0, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->mPaintFlag:I

    if-gez v8, :cond_5

    invoke-virtual {v7}, Landroid/view/View;->getDrawingCache()Landroid/graphics/Bitmap;

    move-result-object v8

    invoke-virtual {v7}, Landroid/view/View;->getWidth()I

    move-result v9

    neg-int v9, v9

    int-to-float v9, v9

    invoke-virtual {v7}, Landroid/view/View;->getTop()I

    move-result v10

    int-to-float v10, v10

    invoke-virtual {p1, v8, v9, v10, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_1

    :cond_5
    invoke-virtual {v7}, Landroid/view/View;->getDrawingCache()Landroid/graphics/Bitmap;

    move-result-object v8

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->getWidth()I

    move-result v9

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->getChildCount()I

    move-result v10

    mul-int/2addr v9, v10

    int-to-float v9, v9

    invoke-virtual {v7}, Landroid/view/View;->getTop()I

    move-result v10

    int-to-float v10, v10

    invoke-virtual {p1, v8, v9, v10, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_1

    :cond_6
    invoke-virtual {p0}, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->getChildCount()I

    move-result v0

    const/4 v4, 0x0

    :goto_2
    if-ge v4, v0, :cond_7

    invoke-virtual {p0, v4}, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    invoke-virtual {p0, p1, v8, v1, v2}, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    :cond_7
    iget v8, p0, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->mPaintFlag:I

    if-eqz v8, :cond_0

    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    iget v8, p0, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->mPaintFlag:I

    if-gez v8, :cond_8

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->getChildCount()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-virtual {p0, v8}, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    invoke-virtual {v7}, Landroid/view/View;->getDrawingCache()Landroid/graphics/Bitmap;

    move-result-object v8

    invoke-virtual {v7}, Landroid/view/View;->getWidth()I

    move-result v9

    neg-int v9, v9

    int-to-float v9, v9

    invoke-virtual {v7}, Landroid/view/View;->getTop()I

    move-result v10

    int-to-float v10, v10

    invoke-virtual {p1, v8, v9, v10, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_1

    :cond_8
    invoke-virtual {p0, v9}, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    invoke-virtual {v7}, Landroid/view/View;->getDrawingCache()Landroid/graphics/Bitmap;

    move-result-object v8

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->getWidth()I

    move-result v9

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->getChildCount()I

    move-result v10

    mul-int/2addr v9, v10

    int-to-float v9, v9

    invoke-virtual {v7}, Landroid/view/View;->getTop()I

    move-result v10

    int-to-float v10, v10

    invoke-virtual {p1, v8, v9, v10, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_1
.end method

.method protected onLayout(ZIIII)V
    .locals 8
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->getChildCount()I

    move-result v3

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v3, :cond_1

    invoke-virtual {p0, v4}, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v5

    const/16 v6, 0x8

    if-eq v5, v6, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    const/4 v5, 0x0

    add-int v6, v1, v2

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v7

    invoke-virtual {v0, v1, v5, v6, v7}, Landroid/view/View;->layout(IIII)V

    add-int/2addr v1, v2

    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method protected onMeasure(II)V
    .locals 5
    .param p1    # I
    .param p2    # I

    const/4 v4, 0x0

    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->onMeasure(II)V

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->getChildCount()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    invoke-virtual {p0, v1}, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Landroid/view/View;->measure(II)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    iget-boolean v3, p0, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->mFirstLayout:Z

    if-eqz v3, :cond_1

    iget v3, p0, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->mCurrentScreen:I

    mul-int/2addr v3, v2

    invoke-virtual {p0, v3, v4}, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->scrollTo(II)V

    iput-boolean v4, p0, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->mFirstLayout:Z

    :cond_1
    return-void
.end method

.method protected onRequestFocusInDescendants(ILandroid/graphics/Rect;)Z
    .locals 3
    .param p1    # I
    .param p2    # Landroid/graphics/Rect;

    iget v1, p0, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->mNextScreen:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    iget v0, p0, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->mNextScreen:I

    :goto_0
    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Landroid/view/View;->requestFocus(ILandroid/graphics/Rect;)Z

    const/4 v1, 0x0

    return v1

    :cond_0
    iget v0, p0, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->mCurrentScreen:I

    goto :goto_0
.end method

.method public requestChildRectangleOnScreen(Landroid/view/View;Landroid/graphics/Rect;Z)Z
    .locals 2
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/graphics/Rect;
    .param p3    # Z

    invoke-virtual {p0, p1}, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v0

    iget v1, p0, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->mCurrentScreen:I

    if-ne v0, v1, :cond_0

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v1}, Landroid/widget/Scroller;->isFinished()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    invoke-direct {p0, v0}, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->snapToScreen(I)V

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setCurrentView(I)V
    .locals 4
    .param p1    # I

    const/4 v3, 0x0

    const-string v0, "qqqq"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "here:view "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->mCurrentScreen:I

    iget v0, p0, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->mCurrentScreen:I

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->getWidth()I

    move-result v1

    mul-int/2addr v0, v1

    invoke-virtual {p0, v0, v3}, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->scrollTo(II)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->mOnCurrentViewChangedListener:Lcom/jrm/localmm/ui/music/ScrollableViewGroup$OnCurrentViewChangedListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->mOnCurrentViewChangedListener:Lcom/jrm/localmm/ui/music/ScrollableViewGroup$OnCurrentViewChangedListener;

    iget v1, p0, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->mCurrentScreen:I

    invoke-interface {v0, p0, v1}, Lcom/jrm/localmm/ui/music/ScrollableViewGroup$OnCurrentViewChangedListener;->onCurrentViewChanged(Landroid/view/View;I)V

    :cond_0
    invoke-virtual {p0}, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->invalidate()V

    return-void
.end method
