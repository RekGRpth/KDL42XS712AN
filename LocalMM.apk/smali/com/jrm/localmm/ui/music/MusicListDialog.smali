.class public Lcom/jrm/localmm/ui/music/MusicListDialog;
.super Landroid/app/Dialog;
.source "MusicListDialog.java"


# instance fields
.field private activity:Landroid/app/Activity;

.field private handler:Landroid/os/Handler;

.field private list:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/jrm/localmm/business/data/BaseData;",
            ">;"
        }
    .end annotation
.end field

.field private listData:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field

.field private mPosition:I

.field private musicList:Landroid/widget/ListView;

.field private selected:I

.field private simpleAdapter:Landroid/widget/SimpleAdapter;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/os/Handler;)V
    .locals 1
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/os/Handler;

    invoke-direct {p0, p1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/jrm/localmm/ui/music/MusicListDialog;->selected:I

    iput-object p1, p0, Lcom/jrm/localmm/ui/music/MusicListDialog;->activity:Landroid/app/Activity;

    iput-object p2, p0, Lcom/jrm/localmm/ui/music/MusicListDialog;->handler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/jrm/localmm/ui/music/MusicListDialog;)I
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/music/MusicListDialog;

    iget v0, p0, Lcom/jrm/localmm/ui/music/MusicListDialog;->selected:I

    return v0
.end method

.method static synthetic access$002(Lcom/jrm/localmm/ui/music/MusicListDialog;I)I
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/music/MusicListDialog;
    .param p1    # I

    iput p1, p0, Lcom/jrm/localmm/ui/music/MusicListDialog;->selected:I

    return p1
.end method

.method static synthetic access$100(Lcom/jrm/localmm/ui/music/MusicListDialog;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/music/MusicListDialog;

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicListDialog;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$200(Lcom/jrm/localmm/ui/music/MusicListDialog;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/music/MusicListDialog;

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicListDialog;->list:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$300(Lcom/jrm/localmm/ui/music/MusicListDialog;)Ljava/util/ArrayList;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/music/MusicListDialog;

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicListDialog;->listData:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$400(Lcom/jrm/localmm/ui/music/MusicListDialog;)Landroid/widget/SimpleAdapter;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/music/MusicListDialog;

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicListDialog;->simpleAdapter:Landroid/widget/SimpleAdapter;

    return-object v0
.end method

.method private addListener()V
    .locals 2

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicListDialog;->musicList:Landroid/widget/ListView;

    new-instance v1, Lcom/jrm/localmm/ui/music/MusicListDialog$1;

    invoke-direct {v1, p0}, Lcom/jrm/localmm/ui/music/MusicListDialog$1;-><init>(Lcom/jrm/localmm/ui/music/MusicListDialog;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicListDialog;->musicList:Landroid/widget/ListView;

    new-instance v1, Lcom/jrm/localmm/ui/music/MusicListDialog$2;

    invoke-direct {v1, p0}, Lcom/jrm/localmm/ui/music/MusicListDialog$2;-><init>(Lcom/jrm/localmm/ui/music/MusicListDialog;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    return-void
.end method

.method private findView()V
    .locals 3

    const/4 v2, 0x1

    const v0, 0x7f0800ad    # com.jrm.localmm.R.id.VidoFilename

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/music/MusicListDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/jrm/localmm/ui/music/MusicListDialog;->musicList:Landroid/widget/ListView;

    invoke-direct {p0}, Lcom/jrm/localmm/ui/music/MusicListDialog;->getVideoName()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicListDialog;->musicList:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setDividerHeight(I)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicListDialog;->musicList:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->requestFocus()Z

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicListDialog;->musicList:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicListDialog;->musicList:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicListDialog;->musicList:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setFocusableInTouchMode(Z)V

    iget v0, p0, Lcom/jrm/localmm/ui/music/MusicListDialog;->mPosition:I

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/music/MusicListDialog;->setSelection(I)V

    return-void
.end method

.method private getVideoName()V
    .locals 12

    const/4 v10, 0x1

    const/4 v11, 0x0

    const-string v6, "videoname"

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/jrm/localmm/ui/music/MusicListDialog;->listData:Ljava/util/ArrayList;

    sget-object v0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->musicList:Ljava/util/List;

    iput-object v0, p0, Lcom/jrm/localmm/ui/music/MusicListDialog;->list:Ljava/util/List;

    const/4 v7, 0x0

    :goto_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicListDialog;->list:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v7, v0, :cond_1

    new-instance v9, Ljava/util/HashMap;

    invoke-direct {v9}, Ljava/util/HashMap;-><init>()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicListDialog;->list:Ljava/util/List;

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jrm/localmm/business/data/BaseData;

    invoke-virtual {v0}, Lcom/jrm/localmm/business/data/BaseData;->getName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "videoname"

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicListDialog;->list:Ljava/util/List;

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jrm/localmm/business/data/BaseData;

    invoke-virtual {v0}, Lcom/jrm/localmm/business/data/BaseData;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_1
    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicListDialog;->listData:Ljava/util/ArrayList;

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicListDialog;->list:Ljava/util/List;

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jrm/localmm/business/data/BaseData;

    invoke-virtual {v0}, Lcom/jrm/localmm/business/data/BaseData;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v0, "videoname"

    const-string v1, "/"

    invoke-virtual {v8, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v8, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_1
    new-instance v0, Landroid/widget/SimpleAdapter;

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicListDialog;->activity:Landroid/app/Activity;

    iget-object v2, p0, Lcom/jrm/localmm/ui/music/MusicListDialog;->listData:Ljava/util/ArrayList;

    const v3, 0x7f03001c    # com.jrm.localmm.R.layout.video_list_item

    new-array v4, v10, [Ljava/lang/String;

    const-string v5, "videoname"

    aput-object v5, v4, v11

    new-array v5, v10, [I

    const v10, 0x7f0800a5    # com.jrm.localmm.R.id.videoName

    aput v10, v5, v11

    invoke-direct/range {v0 .. v5}, Landroid/widget/SimpleAdapter;-><init>(Landroid/content/Context;Ljava/util/List;I[Ljava/lang/String;[I)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/music/MusicListDialog;->simpleAdapter:Landroid/widget/SimpleAdapter;

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicListDialog;->musicList:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicListDialog;->simpleAdapter:Landroid/widget/SimpleAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method private showToast(I)V
    .locals 3
    .param p1    # I

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicListDialog;->activity:Landroid/app/Activity;

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicListDialog;->activity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 10
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    const v6, 0x7f03001e    # com.jrm.localmm.R.layout.video_play_list

    invoke-virtual {p0, v6}, Lcom/jrm/localmm/ui/music/MusicListDialog;->setContentView(I)V

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/music/MusicListDialog;->getWindow()Landroid/view/Window;

    move-result-object v3

    const/4 v6, 0x0

    invoke-virtual {v3, v6}, Landroid/view/Window;->setTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {v3}, Landroid/view/Window;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v6

    invoke-interface {v6}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    invoke-virtual {v0, v2}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    const-string v6, "MusicListDialog"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "x : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, v2, Landroid/graphics/Point;->x:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " y : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, v2, Landroid/graphics/Point;->y:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v6, v2, Landroid/graphics/Point;->x:I

    int-to-double v6, v6

    const-wide v8, 0x3fd999999999999aL    # 0.4

    mul-double/2addr v6, v8

    double-to-int v4, v6

    iget v6, v2, Landroid/graphics/Point;->y:I

    int-to-double v6, v6

    const-wide v8, 0x3fe6666666666666L    # 0.7

    mul-double/2addr v6, v8

    double-to-int v1, v6

    invoke-virtual {v3, v4, v1}, Landroid/view/Window;->setLayout(II)V

    const/16 v6, 0x11

    invoke-virtual {v3, v6}, Landroid/view/Window;->setGravity(I)V

    invoke-virtual {v3}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    const v6, 0x106000d    # android.R.color.transparent

    invoke-virtual {v3, v6}, Landroid/view/Window;->setBackgroundDrawableResource(I)V

    sget v6, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->currentPosition:I

    iput v6, p0, Lcom/jrm/localmm/ui/music/MusicListDialog;->mPosition:I

    invoke-direct {p0}, Lcom/jrm/localmm/ui/music/MusicListDialog;->findView()V

    iget-object v6, p0, Lcom/jrm/localmm/ui/music/MusicListDialog;->musicList:Landroid/widget/ListView;

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Landroid/widget/ListView;->setClickable(Z)V

    const-string v6, "MusicListDialog"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "listview is focused : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/jrm/localmm/ui/music/MusicListDialog;->musicList:Landroid/widget/ListView;

    invoke-virtual {v8}, Landroid/widget/ListView;->isFocused()Z

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/jrm/localmm/ui/music/MusicListDialog;->addListener()V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/16 v0, 0x7e

    if-eq v0, p1, :cond_0

    const/16 v0, 0x7f

    if-eq v0, p1, :cond_0

    const/16 v0, 0x57

    if-eq v0, p1, :cond_0

    const/16 v0, 0x58

    if-ne v0, p1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    invoke-super {p0, p1, p2}, Landroid/app/Dialog;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/16 v1, 0x7e

    if-eq v1, p1, :cond_0

    const/16 v1, 0x7f

    if-eq v1, p1, :cond_0

    const/16 v1, 0x57

    if-eq v1, p1, :cond_0

    const/16 v1, 0x58

    if-ne v1, p1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/16 v1, 0x42

    if-ne p1, v1, :cond_3

    iget v1, p0, Lcom/jrm/localmm/ui/music/MusicListDialog;->selected:I

    sput v1, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->currentPosition:I

    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/jrm/localmm/ui/music/MusicListDialog$3;

    invoke-direct {v2, p0}, Lcom/jrm/localmm/ui/music/MusicListDialog$3;-><init>(Lcom/jrm/localmm/ui/music/MusicListDialog;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/music/MusicListDialog;->dismiss()V

    :cond_2
    :goto_1
    invoke-super {p0, p1, p2}, Landroid/app/Dialog;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v1

    goto :goto_0

    :cond_3
    const/16 v1, 0x52

    if-ne p1, v1, :cond_4

    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicListDialog;->activity:Landroid/app/Activity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f06000d    # com.jrm.localmm.R.string.exit_title

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    const v1, 0x7f06000e    # com.jrm.localmm.R.string.exit_confirm

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicListDialog;->activity:Landroid/app/Activity;

    const v2, 0x7f060007    # com.jrm.localmm.R.string.exit_ok

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/jrm/localmm/ui/music/MusicListDialog$4;

    invoke-direct {v2, p0}, Lcom/jrm/localmm/ui/music/MusicListDialog$4;-><init>(Lcom/jrm/localmm/ui/music/MusicListDialog;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicListDialog;->activity:Landroid/app/Activity;

    const v2, 0x7f060008    # com.jrm.localmm.R.string.exit_cancel

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/jrm/localmm/ui/music/MusicListDialog$5;

    invoke-direct {v2, p0}, Lcom/jrm/localmm/ui/music/MusicListDialog$5;-><init>(Lcom/jrm/localmm/ui/music/MusicListDialog;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNeutralButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    new-instance v1, Lcom/jrm/localmm/ui/music/MusicListDialog$6;

    invoke-direct {v1, p0}, Lcom/jrm/localmm/ui/music/MusicListDialog$6;-><init>(Lcom/jrm/localmm/ui/music/MusicListDialog;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto :goto_1

    :cond_4
    const/16 v1, 0x13

    if-ne p1, v1, :cond_5

    iget v1, p0, Lcom/jrm/localmm/ui/music/MusicListDialog;->selected:I

    if-nez v1, :cond_2

    const v1, 0x7f06000f    # com.jrm.localmm.R.string.the_first_file

    invoke-direct {p0, v1}, Lcom/jrm/localmm/ui/music/MusicListDialog;->showToast(I)V

    goto :goto_1

    :cond_5
    const/16 v1, 0x14

    if-ne p1, v1, :cond_6

    iget v1, p0, Lcom/jrm/localmm/ui/music/MusicListDialog;->selected:I

    iget-object v2, p0, Lcom/jrm/localmm/ui/music/MusicListDialog;->list:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ne v1, v2, :cond_2

    const v1, 0x7f060010    # com.jrm.localmm.R.string.the_last_file

    invoke-direct {p0, v1}, Lcom/jrm/localmm/ui/music/MusicListDialog;->showToast(I)V

    goto :goto_1

    :cond_6
    const/4 v1, 0x4

    if-ne p1, v1, :cond_2

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/music/MusicListDialog;->dismiss()V

    goto :goto_1
.end method

.method protected setSelection(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicListDialog;->musicList:Landroid/widget/ListView;

    invoke-virtual {v0, p1}, Landroid/widget/ListView;->setSelection(I)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicListDialog;->simpleAdapter:Landroid/widget/SimpleAdapter;

    invoke-virtual {v0}, Landroid/widget/SimpleAdapter;->notifyDataSetChanged()V

    return-void
.end method
