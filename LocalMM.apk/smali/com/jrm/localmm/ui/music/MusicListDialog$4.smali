.class Lcom/jrm/localmm/ui/music/MusicListDialog$4;
.super Ljava/lang/Object;
.source "MusicListDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/jrm/localmm/ui/music/MusicListDialog;->onKeyUp(ILandroid/view/KeyEvent;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/localmm/ui/music/MusicListDialog;


# direct methods
.method constructor <init>(Lcom/jrm/localmm/ui/music/MusicListDialog;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/localmm/ui/music/MusicListDialog$4;->this$0:Lcom/jrm/localmm/ui/music/MusicListDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicListDialog$4;->this$0:Lcom/jrm/localmm/ui/music/MusicListDialog;

    # getter for: Lcom/jrm/localmm/ui/music/MusicListDialog;->list:Ljava/util/List;
    invoke-static {v0}, Lcom/jrm/localmm/ui/music/MusicListDialog;->access$200(Lcom/jrm/localmm/ui/music/MusicListDialog;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicListDialog$4;->this$0:Lcom/jrm/localmm/ui/music/MusicListDialog;

    # getter for: Lcom/jrm/localmm/ui/music/MusicListDialog;->selected:I
    invoke-static {v1}, Lcom/jrm/localmm/ui/music/MusicListDialog;->access$000(Lcom/jrm/localmm/ui/music/MusicListDialog;)I

    move-result v1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicListDialog$4;->this$0:Lcom/jrm/localmm/ui/music/MusicListDialog;

    # getter for: Lcom/jrm/localmm/ui/music/MusicListDialog;->listData:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/jrm/localmm/ui/music/MusicListDialog;->access$300(Lcom/jrm/localmm/ui/music/MusicListDialog;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicListDialog$4;->this$0:Lcom/jrm/localmm/ui/music/MusicListDialog;

    # getter for: Lcom/jrm/localmm/ui/music/MusicListDialog;->selected:I
    invoke-static {v1}, Lcom/jrm/localmm/ui/music/MusicListDialog;->access$000(Lcom/jrm/localmm/ui/music/MusicListDialog;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicListDialog$4;->this$0:Lcom/jrm/localmm/ui/music/MusicListDialog;

    # getter for: Lcom/jrm/localmm/ui/music/MusicListDialog;->simpleAdapter:Landroid/widget/SimpleAdapter;
    invoke-static {v0}, Lcom/jrm/localmm/ui/music/MusicListDialog;->access$400(Lcom/jrm/localmm/ui/music/MusicListDialog;)Landroid/widget/SimpleAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/SimpleAdapter;->notifyDataSetChanged()V

    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    return-void
.end method
