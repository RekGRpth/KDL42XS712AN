.class public Lcom/jrm/localmm/ui/music/MusicPlayerListener;
.super Ljava/lang/Object;
.source "MusicPlayerListener.java"


# static fields
.field public static currentPlayMode:I

.field protected static isPlaying:Z


# instance fields
.field private mMusicDetailInfoDialog:Lcom/jrm/localmm/ui/music/MusicDetailInfoDialog;

.field private mMusicPlayerActivity:Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

.field private mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

.field private musicListDialog:Lcom/jrm/localmm/ui/music/MusicListDialog;

.field private seekBarChageListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x2

    sput v0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->currentPlayMode:I

    const/4 v0, 0x1

    sput-boolean v0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->isPlaying:Z

    return-void
.end method

.method public constructor <init>(Lcom/jrm/localmm/ui/music/MusicPlayerActivity;Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;)V
    .locals 1
    .param p1    # Lcom/jrm/localmm/ui/music/MusicPlayerActivity;
    .param p2    # Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/jrm/localmm/ui/music/MusicPlayerListener$7;

    invoke-direct {v0, p0}, Lcom/jrm/localmm/ui/music/MusicPlayerListener$7;-><init>(Lcom/jrm/localmm/ui/music/MusicPlayerListener;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->seekBarChageListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    iput-object p1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerActivity:Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

    iput-object p2, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    return-void
.end method

.method static synthetic access$000(Lcom/jrm/localmm/ui/music/MusicPlayerListener;)Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/music/MusicPlayerListener;

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    return-object v0
.end method

.method static synthetic access$100(Lcom/jrm/localmm/ui/music/MusicPlayerListener;)Lcom/jrm/localmm/ui/music/MusicPlayerActivity;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/music/MusicPlayerListener;

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerActivity:Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

    return-object v0
.end method

.method private setPlayMode(I)V
    .locals 5
    .param p1    # I

    iget-object v2, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerActivity:Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

    const-string v3, "localMM"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "playMode"

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method private showMusicInfo(I)V
    .locals 3
    .param p1    # I

    sget-object v1, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->musicList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge p1, v1, :cond_0

    if-ltz p1, :cond_0

    sget-object v1, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->musicList:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jrm/localmm/business/data/BaseData;

    new-instance v1, Lcom/jrm/localmm/ui/music/MusicDetailInfoDialog;

    iget-object v2, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerActivity:Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

    invoke-direct {v1, v2, v0}, Lcom/jrm/localmm/ui/music/MusicDetailInfoDialog;-><init>(Landroid/app/Activity;Lcom/jrm/localmm/business/data/BaseData;)V

    iput-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicDetailInfoDialog:Lcom/jrm/localmm/ui/music/MusicDetailInfoDialog;

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicDetailInfoDialog:Lcom/jrm/localmm/ui/music/MusicDetailInfoDialog;

    invoke-virtual {v1}, Lcom/jrm/localmm/ui/music/MusicDetailInfoDialog;->show()V

    :cond_0
    return-void
.end method


# virtual methods
.method protected addMusicListener()V
    .locals 2

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->seekBar:Landroid/widget/SeekBar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setClickable(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->seekBar:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->seekBarChageListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerActivity:Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

    iget-object v0, v0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->musicPlayHandle:Landroid/os/Handler;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicPre:Lcom/jrm/localmm/ui/music/RepeatingImageButton;

    new-instance v1, Lcom/jrm/localmm/ui/music/MusicPlayerListener$1;

    invoke-direct {v1, p0}, Lcom/jrm/localmm/ui/music/MusicPlayerListener$1;-><init>(Lcom/jrm/localmm/ui/music/MusicPlayerListener;)V

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/music/RepeatingImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicPlay:Landroid/widget/ImageView;

    new-instance v1, Lcom/jrm/localmm/ui/music/MusicPlayerListener$2;

    invoke-direct {v1, p0}, Lcom/jrm/localmm/ui/music/MusicPlayerListener$2;-><init>(Lcom/jrm/localmm/ui/music/MusicPlayerListener;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicNext:Lcom/jrm/localmm/ui/music/RepeatingImageButton;

    new-instance v1, Lcom/jrm/localmm/ui/music/MusicPlayerListener$3;

    invoke-direct {v1, p0}, Lcom/jrm/localmm/ui/music/MusicPlayerListener$3;-><init>(Lcom/jrm/localmm/ui/music/MusicPlayerListener;)V

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/music/RepeatingImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicCir:Landroid/widget/ImageView;

    new-instance v1, Lcom/jrm/localmm/ui/music/MusicPlayerListener$4;

    invoke-direct {v1, p0}, Lcom/jrm/localmm/ui/music/MusicPlayerListener$4;-><init>(Lcom/jrm/localmm/ui/music/MusicPlayerListener;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicList:Landroid/widget/ImageView;

    new-instance v1, Lcom/jrm/localmm/ui/music/MusicPlayerListener$5;

    invoke-direct {v1, p0}, Lcom/jrm/localmm/ui/music/MusicPlayerListener$5;-><init>(Lcom/jrm/localmm/ui/music/MusicPlayerListener;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicInfo:Landroid/widget/ImageView;

    new-instance v1, Lcom/jrm/localmm/ui/music/MusicPlayerListener$6;

    invoke-direct {v1, p0}, Lcom/jrm/localmm/ui/music/MusicPlayerListener$6;-><init>(Lcom/jrm/localmm/ui/music/MusicPlayerListener;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method protected changePlayMode()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x2

    const/4 v1, 0x1

    sget v0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->currentPlayMode:I

    if-nez v0, :cond_1

    sput v1, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->currentPlayMode:I

    invoke-direct {p0, v1}, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->setPlayMode(I)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicCir:Landroid/widget/ImageView;

    const v1, 0x7f020071    # com.jrm.localmm.R.drawable.player_icon_random_focus

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget v0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->currentPlayMode:I

    if-ne v0, v1, :cond_2

    sput v2, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->currentPlayMode:I

    invoke-direct {p0, v2}, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->setPlayMode(I)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicCir:Landroid/widget/ImageView;

    const v1, 0x7f020063    # com.jrm.localmm.R.drawable.player_icon_loop_focus

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    :cond_2
    sget v0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->currentPlayMode:I

    if-ne v0, v2, :cond_0

    sput v3, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->currentPlayMode:I

    invoke-direct {p0, v3}, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->setPlayMode(I)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicCir:Landroid/widget/ImageView;

    const v1, 0x7f02007b    # com.jrm.localmm.R.drawable.player_icon_singles_focus

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method protected drapLeft()V
    .locals 5

    const/4 v4, 0x2

    const/4 v2, 0x0

    const/4 v3, 0x1

    sget v1, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->state:I

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    const/4 v1, 0x7

    sput v1, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->state:I

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicPre:Lcom/jrm/localmm/ui/music/RepeatingImageButton;

    invoke-virtual {v1, v2}, Lcom/jrm/localmm/ui/music/RepeatingImageButton;->setFocusable(Z)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicPre:Lcom/jrm/localmm/ui/music/RepeatingImageButton;

    const v2, 0x7f02006d    # com.jrm.localmm.R.drawable.player_icon_previous

    invoke-virtual {v1, v2}, Lcom/jrm/localmm/ui/music/RepeatingImageButton;->setBackgroundResource(I)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicInfo:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicInfo:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->requestFocus()Z

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicInfo:Landroid/widget/ImageView;

    const v2, 0x7f02005d    # com.jrm.localmm.R.drawable.player_icon_infor_focus

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    :pswitch_2
    sput v3, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->state:I

    sget-boolean v1, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->isPlaying:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicPlay:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicPlay:Landroid/widget/ImageView;

    const v2, 0x7f020069    # com.jrm.localmm.R.drawable.player_icon_pause

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    :goto_1
    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicPre:Lcom/jrm/localmm/ui/music/RepeatingImageButton;

    invoke-virtual {v1, v3}, Lcom/jrm/localmm/ui/music/RepeatingImageButton;->setFocusable(Z)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicPre:Lcom/jrm/localmm/ui/music/RepeatingImageButton;

    invoke-virtual {v1}, Lcom/jrm/localmm/ui/music/RepeatingImageButton;->requestFocus()Z

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicPre:Lcom/jrm/localmm/ui/music/RepeatingImageButton;

    const v2, 0x7f02006e    # com.jrm.localmm.R.drawable.player_icon_previous_focus

    invoke-virtual {v1, v2}, Lcom/jrm/localmm/ui/music/RepeatingImageButton;->setBackgroundResource(I)V

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicPlay:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicPlay:Landroid/widget/ImageView;

    const v2, 0x7f02006b    # com.jrm.localmm.R.drawable.player_icon_play

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_1

    :pswitch_3
    sput v4, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->state:I

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicNext:Lcom/jrm/localmm/ui/music/RepeatingImageButton;

    invoke-virtual {v1, v2}, Lcom/jrm/localmm/ui/music/RepeatingImageButton;->setFocusable(Z)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicNext:Lcom/jrm/localmm/ui/music/RepeatingImageButton;

    const v2, 0x7f020066    # com.jrm.localmm.R.drawable.player_icon_next

    invoke-virtual {v1, v2}, Lcom/jrm/localmm/ui/music/RepeatingImageButton;->setBackgroundResource(I)V

    sget-boolean v1, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->isPlaying:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicPlay:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicPlay:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->requestFocus()Z

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicPlay:Landroid/widget/ImageView;

    const v2, 0x7f02006a    # com.jrm.localmm.R.drawable.player_icon_pause_focus

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto/16 :goto_0

    :cond_2
    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicPlay:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicPlay:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->requestFocus()Z

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicPlay:Landroid/widget/ImageView;

    const v2, 0x7f02006c    # com.jrm.localmm.R.drawable.player_icon_play_focus

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto/16 :goto_0

    :pswitch_4
    const/4 v1, 0x3

    sput v1, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->state:I

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicCir:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setFocusable(Z)V

    sget v0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->currentPlayMode:I

    if-nez v0, :cond_4

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicCir:Landroid/widget/ImageView;

    const v2, 0x7f02007a    # com.jrm.localmm.R.drawable.player_icon_singles

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    :cond_3
    :goto_2
    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicNext:Lcom/jrm/localmm/ui/music/RepeatingImageButton;

    invoke-virtual {v1, v3}, Lcom/jrm/localmm/ui/music/RepeatingImageButton;->setFocusable(Z)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicNext:Lcom/jrm/localmm/ui/music/RepeatingImageButton;

    invoke-virtual {v1}, Lcom/jrm/localmm/ui/music/RepeatingImageButton;->requestFocus()Z

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicNext:Lcom/jrm/localmm/ui/music/RepeatingImageButton;

    const v2, 0x7f020067    # com.jrm.localmm.R.drawable.player_icon_next_focus

    invoke-virtual {v1, v2}, Lcom/jrm/localmm/ui/music/RepeatingImageButton;->setBackgroundResource(I)V

    goto/16 :goto_0

    :cond_4
    if-ne v0, v3, :cond_5

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicCir:Landroid/widget/ImageView;

    const v2, 0x7f020070    # com.jrm.localmm.R.drawable.player_icon_random

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_2

    :cond_5
    if-ne v0, v4, :cond_3

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicCir:Landroid/widget/ImageView;

    const v2, 0x7f020062    # com.jrm.localmm.R.drawable.player_icon_loop

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_2

    :pswitch_5
    const/4 v1, 0x4

    sput v1, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->state:I

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicList:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicList:Landroid/widget/ImageView;

    const v2, 0x7f020060    # com.jrm.localmm.R.drawable.player_icon_list

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicCir:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicCir:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->requestFocus()Z

    sget v0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->currentPlayMode:I

    if-nez v0, :cond_6

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicCir:Landroid/widget/ImageView;

    const v2, 0x7f02007b    # com.jrm.localmm.R.drawable.player_icon_singles_focus

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto/16 :goto_0

    :cond_6
    if-ne v0, v3, :cond_7

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicCir:Landroid/widget/ImageView;

    const v2, 0x7f020071    # com.jrm.localmm.R.drawable.player_icon_random_focus

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto/16 :goto_0

    :cond_7
    if-ne v0, v4, :cond_0

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicCir:Landroid/widget/ImageView;

    const v2, 0x7f020063    # com.jrm.localmm.R.drawable.player_icon_loop_focus

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto/16 :goto_0

    :pswitch_6
    const/4 v1, 0x5

    sput v1, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->state:I

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicInfo:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicInfo:Landroid/widget/ImageView;

    const v2, 0x7f02005c    # com.jrm.localmm.R.drawable.player_icon_infor

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicList:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicList:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->requestFocus()Z

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicList:Landroid/widget/ImageView;

    const v2, 0x7f020061    # com.jrm.localmm.R.drawable.player_icon_list_focus

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_6
    .end packed-switch
.end method

.method protected drapRight()V
    .locals 6

    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    sget v1, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->state:I

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    sput v5, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->state:I

    sget-boolean v1, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->isPlaying:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicPlay:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicPlay:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->requestFocus()Z

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicPlay:Landroid/widget/ImageView;

    const v2, 0x7f02006a    # com.jrm.localmm.R.drawable.player_icon_pause_focus

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    :goto_1
    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicPre:Lcom/jrm/localmm/ui/music/RepeatingImageButton;

    invoke-virtual {v1, v4}, Lcom/jrm/localmm/ui/music/RepeatingImageButton;->setFocusable(Z)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicPre:Lcom/jrm/localmm/ui/music/RepeatingImageButton;

    const v2, 0x7f02006d    # com.jrm.localmm.R.drawable.player_icon_previous

    invoke-virtual {v1, v2}, Lcom/jrm/localmm/ui/music/RepeatingImageButton;->setBackgroundResource(I)V

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicPlay:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicPlay:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->requestFocus()Z

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicPlay:Landroid/widget/ImageView;

    const v2, 0x7f02006c    # com.jrm.localmm.R.drawable.player_icon_play_focus

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_1

    :pswitch_2
    const/4 v1, 0x3

    sput v1, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->state:I

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicNext:Lcom/jrm/localmm/ui/music/RepeatingImageButton;

    invoke-virtual {v1, v3}, Lcom/jrm/localmm/ui/music/RepeatingImageButton;->setFocusable(Z)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicNext:Lcom/jrm/localmm/ui/music/RepeatingImageButton;

    invoke-virtual {v1}, Lcom/jrm/localmm/ui/music/RepeatingImageButton;->requestFocus()Z

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicNext:Lcom/jrm/localmm/ui/music/RepeatingImageButton;

    const v2, 0x7f020067    # com.jrm.localmm.R.drawable.player_icon_next_focus

    invoke-virtual {v1, v2}, Lcom/jrm/localmm/ui/music/RepeatingImageButton;->setBackgroundResource(I)V

    const-string v1, "MusicPlayerListener"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "OPTION_STATE_PLAY : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-boolean v3, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->isPlaying:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-boolean v1, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->isPlaying:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicPlay:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicPlay:Landroid/widget/ImageView;

    const v2, 0x7f020069    # com.jrm.localmm.R.drawable.player_icon_pause

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto/16 :goto_0

    :cond_2
    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicPlay:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicPlay:Landroid/widget/ImageView;

    const v2, 0x7f02006b    # com.jrm.localmm.R.drawable.player_icon_play

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto/16 :goto_0

    :pswitch_3
    const/4 v1, 0x4

    sput v1, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->state:I

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicCir:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicCir:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->requestFocus()Z

    sget v0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->currentPlayMode:I

    if-nez v0, :cond_4

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicCir:Landroid/widget/ImageView;

    const v2, 0x7f02007b    # com.jrm.localmm.R.drawable.player_icon_singles_focus

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    :cond_3
    :goto_2
    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicNext:Lcom/jrm/localmm/ui/music/RepeatingImageButton;

    invoke-virtual {v1, v4}, Lcom/jrm/localmm/ui/music/RepeatingImageButton;->setFocusable(Z)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicNext:Lcom/jrm/localmm/ui/music/RepeatingImageButton;

    const v2, 0x7f020066    # com.jrm.localmm.R.drawable.player_icon_next

    invoke-virtual {v1, v2}, Lcom/jrm/localmm/ui/music/RepeatingImageButton;->setBackgroundResource(I)V

    goto/16 :goto_0

    :cond_4
    if-ne v0, v3, :cond_5

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicCir:Landroid/widget/ImageView;

    const v2, 0x7f020071    # com.jrm.localmm.R.drawable.player_icon_random_focus

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_2

    :cond_5
    if-ne v0, v5, :cond_3

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicCir:Landroid/widget/ImageView;

    const v2, 0x7f020063    # com.jrm.localmm.R.drawable.player_icon_loop_focus

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_2

    :pswitch_4
    const/4 v1, 0x5

    sput v1, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->state:I

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicList:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicList:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->requestFocus()Z

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicList:Landroid/widget/ImageView;

    const v2, 0x7f020061    # com.jrm.localmm.R.drawable.player_icon_list_focus

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicCir:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setFocusable(Z)V

    sget v0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->currentPlayMode:I

    if-nez v0, :cond_6

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicCir:Landroid/widget/ImageView;

    const v2, 0x7f02007a    # com.jrm.localmm.R.drawable.player_icon_singles

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto/16 :goto_0

    :cond_6
    if-ne v0, v3, :cond_7

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicCir:Landroid/widget/ImageView;

    const v2, 0x7f020070    # com.jrm.localmm.R.drawable.player_icon_random

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto/16 :goto_0

    :cond_7
    if-ne v0, v5, :cond_0

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicCir:Landroid/widget/ImageView;

    const v2, 0x7f020062    # com.jrm.localmm.R.drawable.player_icon_loop

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto/16 :goto_0

    :pswitch_5
    const/4 v1, 0x7

    sput v1, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->state:I

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicInfo:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicInfo:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->requestFocus()Z

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicInfo:Landroid/widget/ImageView;

    const v2, 0x7f02005d    # com.jrm.localmm.R.drawable.player_icon_infor_focus

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicList:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicList:Landroid/widget/ImageView;

    const v2, 0x7f020060    # com.jrm.localmm.R.drawable.player_icon_list

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto/16 :goto_0

    :pswitch_6
    sput v3, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->state:I

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicPre:Lcom/jrm/localmm/ui/music/RepeatingImageButton;

    invoke-virtual {v1, v3}, Lcom/jrm/localmm/ui/music/RepeatingImageButton;->setFocusable(Z)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicPre:Lcom/jrm/localmm/ui/music/RepeatingImageButton;

    invoke-virtual {v1}, Lcom/jrm/localmm/ui/music/RepeatingImageButton;->requestFocus()Z

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicPre:Lcom/jrm/localmm/ui/music/RepeatingImageButton;

    const v2, 0x7f02006e    # com.jrm.localmm.R.drawable.player_icon_previous_focus

    invoke-virtual {v1, v2}, Lcom/jrm/localmm/ui/music/RepeatingImageButton;->setBackgroundResource(I)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicInfo:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicInfo:Landroid/widget/ImageView;

    const v2, 0x7f02005c    # com.jrm.localmm.R.drawable.player_icon_infor

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_6
    .end packed-switch
.end method

.method protected getPicOfAlbum(Ljava/util/List;I)V
    .locals 7
    .param p2    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/jrm/localmm/business/data/BaseData;",
            ">;I)V"
        }
    .end annotation

    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jrm/localmm/business/data/BaseData;

    invoke-virtual {v0}, Lcom/jrm/localmm/business/data/BaseData;->getId()J

    move-result-wide v1

    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jrm/localmm/business/data/BaseData;

    invoke-virtual {v0}, Lcom/jrm/localmm/business/data/BaseData;->getAlbum()J

    move-result-wide v3

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerActivity:Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

    const/4 v5, 0x1

    invoke-static/range {v0 .. v5}, Lcom/jrm/localmm/util/MusicUtils;->getArtwork(Landroid/content/Context;JJZ)Landroid/graphics/Bitmap;

    move-result-object v6

    if-eqz v6, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicImageAlbum:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :cond_0
    return-void
.end method

.method public initPlayMode()V
    .locals 3

    sget v0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->currentPlayMode:I

    if-nez v0, :cond_1

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicCir:Landroid/widget/ImageView;

    const v2, 0x7f02007a    # com.jrm.localmm.R.drawable.player_icon_singles

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicCir:Landroid/widget/ImageView;

    const v2, 0x7f020070    # com.jrm.localmm.R.drawable.player_icon_random

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    :cond_2
    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicCir:Landroid/widget/ImageView;

    const v2, 0x7f020062    # com.jrm.localmm.R.drawable.player_icon_loop

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method public registerListeners()V
    .locals 5

    const/4 v4, 0x1

    const v3, 0x7f02006b    # com.jrm.localmm.R.drawable.player_icon_play

    const v2, 0x7f020069    # com.jrm.localmm.R.drawable.player_icon_pause

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->setViewDefault()V

    sget v0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->state:I

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerActivity:Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

    iget-boolean v0, v0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->isPrepared:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerActivity:Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

    iput-boolean v1, v0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->isPrepared:Z

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicPre:Lcom/jrm/localmm/ui/music/RepeatingImageButton;

    const v1, 0x7f02006e    # com.jrm.localmm.R.drawable.player_icon_previous_focus

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/music/RepeatingImageButton;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerActivity:Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->preMusic()V

    sput-boolean v4, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->isPlaying:Z

    goto :goto_0

    :pswitch_1
    const-string v0, "MusicPlayerListener"

    const-string v1, "OPTION_STATE_PLAY:::::pauseMusic::::::::"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerActivity:Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->pauseMusic()V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerActivity:Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

    iget-boolean v0, v0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->isPrepared:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerActivity:Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

    iput-boolean v1, v0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->isPrepared:Z

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicNext:Lcom/jrm/localmm/ui/music/RepeatingImageButton;

    const v1, 0x7f020067    # com.jrm.localmm.R.drawable.player_icon_next_focus

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/music/RepeatingImageButton;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerActivity:Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->nextMusic()V

    sput-boolean v4, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->isPlaying:Z

    goto :goto_0

    :pswitch_3
    sget-boolean v0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->isPlaying:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicPlay:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    :goto_1
    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicPlay:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->changePlayMode()V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicPlay:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_1

    :pswitch_4
    sget-boolean v0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->isPlaying:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicPlay:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    :goto_2
    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicPlay:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    new-instance v0, Lcom/jrm/localmm/ui/music/MusicListDialog;

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerActivity:Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

    iget-object v2, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerActivity:Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

    iget-object v2, v2, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->musicPlayHandle:Landroid/os/Handler;

    invoke-direct {v0, v1, v2}, Lcom/jrm/localmm/ui/music/MusicListDialog;-><init>(Landroid/app/Activity;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->musicListDialog:Lcom/jrm/localmm/ui/music/MusicListDialog;

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->musicListDialog:Lcom/jrm/localmm/ui/music/MusicListDialog;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/music/MusicListDialog;->show()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicList:Landroid/widget/ImageView;

    const v1, 0x7f020061    # com.jrm.localmm.R.drawable.player_icon_list_focus

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto/16 :goto_0

    :cond_2
    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicPlay:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_2

    :pswitch_5
    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerActivity:Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

    iput v1, v0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->scrollstate:I

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->scrollView:Lcom/jrm/localmm/ui/music/ScrollableViewGroup;

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerActivity:Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

    iget v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->scrollstate:I

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/music/ScrollableViewGroup;->setCurrentView(I)V

    goto/16 :goto_0

    :pswitch_6
    sget-boolean v0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->isPlaying:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicPlay:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    :goto_3
    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicPlay:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    sget v0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->currentPosition:I

    invoke-direct {p0, v0}, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->showMusicInfo(I)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicInfo:Landroid/widget/ImageView;

    const v1, 0x7f02005d    # com.jrm.localmm.R.drawable.player_icon_infor_focus

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto/16 :goto_0

    :cond_3
    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicPlay:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method protected setSongsContent(Ljava/util/List;I)V
    .locals 6
    .param p2    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/jrm/localmm/business/data/BaseData;",
            ">;I)V"
        }
    .end annotation

    const v5, 0x7f060055    # com.jrm.localmm.R.string.current_song

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->currentMusicArtist:Landroid/widget/TextView;

    const-string v2, "   "

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jrm/localmm/business/data/BaseData;

    invoke-virtual {v1}, Lcom/jrm/localmm/business/data/BaseData;->getArtist()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v2, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->currentMusicArtist:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerActivity:Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

    invoke-virtual {v3}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f060054    # com.jrm.localmm.R.string.singer

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jrm/localmm/business/data/BaseData;

    invoke-virtual {v1}, Lcom/jrm/localmm/business/data/BaseData;->getArtist()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jrm/localmm/business/data/BaseData;

    invoke-virtual {v1}, Lcom/jrm/localmm/business/data/BaseData;->getName()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v2, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->currentMusicTitle:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerActivity:Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

    invoke-virtual {v3}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jrm/localmm/business/data/BaseData;

    invoke-virtual {v1}, Lcom/jrm/localmm/business/data/BaseData;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->currentMusicNum:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    add-int/lit8 v3, p2, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void

    :cond_1
    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jrm/localmm/business/data/BaseData;

    invoke-virtual {v1}, Lcom/jrm/localmm/business/data/BaseData;->getPath()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->currentMusicTitle:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerActivity:Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

    invoke-virtual {v3}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v0, v3}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method protected setViewDefault()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicNext:Lcom/jrm/localmm/ui/music/RepeatingImageButton;

    const v1, 0x7f020066    # com.jrm.localmm.R.drawable.player_icon_next

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/music/RepeatingImageButton;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicNext:Lcom/jrm/localmm/ui/music/RepeatingImageButton;

    invoke-virtual {v0, v2}, Lcom/jrm/localmm/ui/music/RepeatingImageButton;->setFocusable(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicPre:Lcom/jrm/localmm/ui/music/RepeatingImageButton;

    const v1, 0x7f02006d    # com.jrm.localmm.R.drawable.player_icon_previous

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/music/RepeatingImageButton;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicPre:Lcom/jrm/localmm/ui/music/RepeatingImageButton;

    invoke-virtual {v0, v2}, Lcom/jrm/localmm/ui/music/RepeatingImageButton;->setFocusable(Z)V

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->initPlayMode()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicCir:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicList:Landroid/widget/ImageView;

    const v1, 0x7f020060    # com.jrm.localmm.R.drawable.player_icon_list

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicList:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicInfo:Landroid/widget/ImageView;

    const v1, 0x7f02005c    # com.jrm.localmm.R.drawable.player_icon_infor

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicPlayerViewHolder:Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/music/MusicPlayerViewHolder;->bt_MusicInfo:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicDetailInfoDialog:Lcom/jrm/localmm/ui/music/MusicDetailInfoDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicDetailInfoDialog:Lcom/jrm/localmm/ui/music/MusicDetailInfoDialog;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/music/MusicDetailInfoDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->mMusicDetailInfoDialog:Lcom/jrm/localmm/ui/music/MusicDetailInfoDialog;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/music/MusicDetailInfoDialog;->dismiss()V

    :cond_0
    return-void
.end method

.method protected updateListViewSelection()V
    .locals 2

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->musicListDialog:Lcom/jrm/localmm/ui/music/MusicListDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerListener;->musicListDialog:Lcom/jrm/localmm/ui/music/MusicListDialog;

    sget v1, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->currentPosition:I

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/music/MusicListDialog;->setSelection(I)V

    :cond_0
    return-void
.end method
