.class Lcom/jrm/localmm/ui/music/MusicPlayerActivity$DiskChangeReceiver;
.super Landroid/content/BroadcastReceiver;
.source "MusicPlayerActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jrm/localmm/ui/music/MusicPlayerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DiskChangeReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/localmm/ui/music/MusicPlayerActivity;


# direct methods
.method private constructor <init>(Lcom/jrm/localmm/ui/music/MusicPlayerActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity$DiskChangeReceiver;->this$0:Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/jrm/localmm/ui/music/MusicPlayerActivity;Lcom/jrm/localmm/ui/music/MusicPlayerActivity$1;)V
    .locals 0
    .param p1    # Lcom/jrm/localmm/ui/music/MusicPlayerActivity;
    .param p2    # Lcom/jrm/localmm/ui/music/MusicPlayerActivity$1;

    invoke-direct {p0, p1}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity$DiskChangeReceiver;-><init>(Lcom/jrm/localmm/ui/music/MusicPlayerActivity;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const/4 v6, 0x0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x7

    invoke-virtual {v3, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    const-string v3, "MusicPlayerActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "DiskChangeReceiver: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v3, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    sget-object v3, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->musicList:Ljava/util/List;

    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/jrm/localmm/business/data/BaseData;

    invoke-virtual {v3}, Lcom/jrm/localmm/business/data/BaseData;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity$DiskChangeReceiver;->this$0:Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

    const v4, 0x7f06003e    # com.jrm.localmm.R.string.disk_eject

    invoke-static {v3, v4, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    const/16 v3, 0x11

    invoke-virtual {v2, v3, v6, v6}, Landroid/widget/Toast;->setGravity(III)V

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    iget-object v3, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity$DiskChangeReceiver;->this$0:Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

    invoke-virtual {v3}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->finish()V

    :cond_0
    return-void
.end method
