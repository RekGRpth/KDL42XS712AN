.class public Lcom/jrm/localmm/ui/music/MediaService;
.super Landroid/app/Service;
.source "MediaService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/jrm/localmm/ui/music/MediaService$ServiceStub;
    }
.end annotation


# instance fields
.field private final mBinder:Landroid/os/IBinder;

.field private mCanPause:Z

.field private mCanSeekBack:Z

.field private mCanSeekForward:Z

.field private mCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

.field private mCurrentState:I

.field private mDuration:I

.field private mErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

.field private mInfoListener:Landroid/media/MediaPlayer$OnInfoListener;

.field private mMusicStatusListener:Lcom/jrm/localmm/ui/music/IMusicStatusListener;

.field private mPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

.field private mSeekCompleteListener:Landroid/media/MediaPlayer$OnSeekCompleteListener;

.field private mSeekWhenPrepared:I

.field private mTargetState:I

.field private player:Lcom/mstar/android/media/MMediaPlayer;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    new-instance v0, Lcom/jrm/localmm/ui/music/MediaService$ServiceStub;

    invoke-direct {v0, p0, p0}, Lcom/jrm/localmm/ui/music/MediaService$ServiceStub;-><init>(Lcom/jrm/localmm/ui/music/MediaService;Lcom/jrm/localmm/ui/music/MediaService;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/music/MediaService;->mBinder:Landroid/os/IBinder;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/jrm/localmm/ui/music/MediaService;->player:Lcom/mstar/android/media/MMediaPlayer;

    iput v1, p0, Lcom/jrm/localmm/ui/music/MediaService;->mCurrentState:I

    iput v1, p0, Lcom/jrm/localmm/ui/music/MediaService;->mTargetState:I

    new-instance v0, Lcom/jrm/localmm/ui/music/MediaService$1;

    invoke-direct {v0, p0}, Lcom/jrm/localmm/ui/music/MediaService$1;-><init>(Lcom/jrm/localmm/ui/music/MediaService;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/music/MediaService;->mInfoListener:Landroid/media/MediaPlayer$OnInfoListener;

    new-instance v0, Lcom/jrm/localmm/ui/music/MediaService$2;

    invoke-direct {v0, p0}, Lcom/jrm/localmm/ui/music/MediaService$2;-><init>(Lcom/jrm/localmm/ui/music/MediaService;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/music/MediaService;->mErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    new-instance v0, Lcom/jrm/localmm/ui/music/MediaService$3;

    invoke-direct {v0, p0}, Lcom/jrm/localmm/ui/music/MediaService$3;-><init>(Lcom/jrm/localmm/ui/music/MediaService;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/music/MediaService;->mPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

    new-instance v0, Lcom/jrm/localmm/ui/music/MediaService$4;

    invoke-direct {v0, p0}, Lcom/jrm/localmm/ui/music/MediaService$4;-><init>(Lcom/jrm/localmm/ui/music/MediaService;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/music/MediaService;->mCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    new-instance v0, Lcom/jrm/localmm/ui/music/MediaService$5;

    invoke-direct {v0, p0}, Lcom/jrm/localmm/ui/music/MediaService$5;-><init>(Lcom/jrm/localmm/ui/music/MediaService;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/music/MediaService;->mSeekCompleteListener:Landroid/media/MediaPlayer$OnSeekCompleteListener;

    return-void
.end method

.method static synthetic access$002(Lcom/jrm/localmm/ui/music/MediaService;I)I
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/music/MediaService;
    .param p1    # I

    iput p1, p0, Lcom/jrm/localmm/ui/music/MediaService;->mCurrentState:I

    return p1
.end method

.method static synthetic access$102(Lcom/jrm/localmm/ui/music/MediaService;I)I
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/music/MediaService;
    .param p1    # I

    iput p1, p0, Lcom/jrm/localmm/ui/music/MediaService;->mTargetState:I

    return p1
.end method

.method static synthetic access$200(Lcom/jrm/localmm/ui/music/MediaService;)Lcom/mstar/android/media/MMediaPlayer;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/music/MediaService;

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MediaService;->player:Lcom/mstar/android/media/MMediaPlayer;

    return-object v0
.end method

.method static synthetic access$202(Lcom/jrm/localmm/ui/music/MediaService;Lcom/mstar/android/media/MMediaPlayer;)Lcom/mstar/android/media/MMediaPlayer;
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/music/MediaService;
    .param p1    # Lcom/mstar/android/media/MMediaPlayer;

    iput-object p1, p0, Lcom/jrm/localmm/ui/music/MediaService;->player:Lcom/mstar/android/media/MMediaPlayer;

    return-object p1
.end method

.method static synthetic access$300(Lcom/jrm/localmm/ui/music/MediaService;Landroid/media/MediaPlayer;II)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/music/MediaService;
    .param p1    # Landroid/media/MediaPlayer;
    .param p2    # I
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Lcom/jrm/localmm/ui/music/MediaService;->processErrorUnknown(Landroid/media/MediaPlayer;II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/jrm/localmm/ui/music/MediaService;)Lcom/jrm/localmm/ui/music/IMusicStatusListener;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/music/MediaService;

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MediaService;->mMusicStatusListener:Lcom/jrm/localmm/ui/music/IMusicStatusListener;

    return-object v0
.end method

.method static synthetic access$402(Lcom/jrm/localmm/ui/music/MediaService;Lcom/jrm/localmm/ui/music/IMusicStatusListener;)Lcom/jrm/localmm/ui/music/IMusicStatusListener;
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/music/MediaService;
    .param p1    # Lcom/jrm/localmm/ui/music/IMusicStatusListener;

    iput-object p1, p0, Lcom/jrm/localmm/ui/music/MediaService;->mMusicStatusListener:Lcom/jrm/localmm/ui/music/IMusicStatusListener;

    return-object p1
.end method

.method static synthetic access$502(Lcom/jrm/localmm/ui/music/MediaService;Z)Z
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/music/MediaService;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/jrm/localmm/ui/music/MediaService;->mCanPause:Z

    return p1
.end method

.method static synthetic access$602(Lcom/jrm/localmm/ui/music/MediaService;Z)Z
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/music/MediaService;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/jrm/localmm/ui/music/MediaService;->mCanSeekBack:Z

    return p1
.end method

.method static synthetic access$702(Lcom/jrm/localmm/ui/music/MediaService;Z)Z
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/music/MediaService;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/jrm/localmm/ui/music/MediaService;->mCanSeekForward:Z

    return p1
.end method

.method private isInPlaybackState()Z
    .locals 3

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MediaService;->player:Lcom/mstar/android/media/MMediaPlayer;

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/jrm/localmm/ui/music/MediaService;->mCurrentState:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    iget v1, p0, Lcom/jrm/localmm/ui/music/MediaService;->mCurrentState:I

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/jrm/localmm/ui/music/MediaService;->mCurrentState:I

    if-eq v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private processErrorUnknown(Landroid/media/MediaPlayer;II)Ljava/lang/String;
    .locals 3
    .param p1    # Landroid/media/MediaPlayer;
    .param p2    # I
    .param p3    # I

    const v0, 0x7f06006a    # com.jrm.localmm.R.string.video_media_error_unknown

    sparse-switch p3, :sswitch_data_0

    :goto_0
    invoke-virtual {p0}, Lcom/jrm/localmm/ui/music/MediaService;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    return-object v1

    :sswitch_0
    const v0, 0x7f06006b    # com.jrm.localmm.R.string.video_media_error_malformed

    goto :goto_0

    :sswitch_1
    const v0, 0x7f06006c    # com.jrm.localmm.R.string.video_media_error_io

    goto :goto_0

    :sswitch_2
    const v0, 0x7f06006d    # com.jrm.localmm.R.string.video_media_error_unsupported

    goto :goto_0

    :sswitch_3
    const v0, 0x7f06006e    # com.jrm.localmm.R.string.video_media_error_format_unsupport

    goto :goto_0

    :sswitch_4
    const v0, 0x7f06006f    # com.jrm.localmm.R.string.video_media_error_not_connected

    goto :goto_0

    :sswitch_5
    const v0, 0x7f060070    # com.jrm.localmm.R.string.video_media_error_audio_unsupport

    goto :goto_0

    :sswitch_6
    const v0, 0x7f060071    # com.jrm.localmm.R.string.video_media_error_video_unsupport

    goto :goto_0

    :sswitch_7
    const v0, 0x7f060072    # com.jrm.localmm.R.string.video_media_error_no_license

    goto :goto_0

    :sswitch_8
    const v0, 0x7f060073    # com.jrm.localmm.R.string.video_media_error_license_expired

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x138b -> :sswitch_3
        -0x138a -> :sswitch_6
        -0x1389 -> :sswitch_5
        -0x7d2 -> :sswitch_8
        -0x7d1 -> :sswitch_7
        -0x3f2 -> :sswitch_2
        -0x3ef -> :sswitch_0
        -0x3ec -> :sswitch_1
        -0x3e9 -> :sswitch_4
    .end sparse-switch
.end method

.method private release(Z)V
    .locals 2
    .param p1    # Z

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MediaService;->player:Lcom/mstar/android/media/MMediaPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MediaService;->player:Lcom/mstar/android/media/MMediaPlayer;

    invoke-virtual {v0}, Lcom/mstar/android/media/MMediaPlayer;->release()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/jrm/localmm/ui/music/MediaService;->player:Lcom/mstar/android/media/MMediaPlayer;

    iput v1, p0, Lcom/jrm/localmm/ui/music/MediaService;->mCurrentState:I

    if-eqz p1, :cond_0

    iput v1, p0, Lcom/jrm/localmm/ui/music/MediaService;->mTargetState:I

    :cond_0
    return-void
.end method


# virtual methods
.method public getAudioSessionId()I
    .locals 1

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MediaService;->player:Lcom/mstar/android/media/MMediaPlayer;

    invoke-virtual {v0}, Lcom/mstar/android/media/MMediaPlayer;->getAudioSessionId()I

    move-result v0

    return v0
.end method

.method public getCurrentPosition()J
    .locals 2

    invoke-direct {p0}, Lcom/jrm/localmm/ui/music/MediaService;->isInPlaybackState()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MediaService;->player:Lcom/mstar/android/media/MMediaPlayer;

    invoke-virtual {v0}, Lcom/mstar/android/media/MMediaPlayer;->getCurrentPosition()I

    move-result v0

    int-to-long v0, v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method public getDuration()J
    .locals 2

    invoke-direct {p0}, Lcom/jrm/localmm/ui/music/MediaService;->isInPlaybackState()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MediaService;->player:Lcom/mstar/android/media/MMediaPlayer;

    invoke-virtual {v0}, Lcom/mstar/android/media/MMediaPlayer;->getDuration()I

    move-result v0

    iput v0, p0, Lcom/jrm/localmm/ui/music/MediaService;->mDuration:I

    iget v0, p0, Lcom/jrm/localmm/ui/music/MediaService;->mDuration:I

    int-to-long v0, v0

    :goto_0
    return-wide v0

    :cond_0
    const/4 v0, -0x1

    iput v0, p0, Lcom/jrm/localmm/ui/music/MediaService;->mDuration:I

    iget v0, p0, Lcom/jrm/localmm/ui/music/MediaService;->mDuration:I

    int-to-long v0, v0

    goto :goto_0
.end method

.method public isPlaying()Z
    .locals 1

    invoke-direct {p0}, Lcom/jrm/localmm/ui/music/MediaService;->isInPlaybackState()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MediaService;->player:Lcom/mstar/android/media/MMediaPlayer;

    invoke-virtual {v0}, Lcom/mstar/android/media/MMediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public next(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/music/MediaService;->stop()V

    invoke-virtual {p0, p1}, Lcom/jrm/localmm/ui/music/MediaService;->openPlayer(Ljava/lang/String;)V

    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1    # Landroid/content/Intent;

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MediaService;->mBinder:Landroid/os/IBinder;

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    const/4 v1, 0x0

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MediaService;->player:Lcom/mstar/android/media/MMediaPlayer;

    if-nez v0, :cond_0

    new-instance v0, Lcom/mstar/android/media/MMediaPlayer;

    invoke-direct {v0}, Lcom/mstar/android/media/MMediaPlayer;-><init>()V

    iput-object v0, p0, Lcom/jrm/localmm/ui/music/MediaService;->player:Lcom/mstar/android/media/MMediaPlayer;

    iput v1, p0, Lcom/jrm/localmm/ui/music/MediaService;->mCurrentState:I

    iput v1, p0, Lcom/jrm/localmm/ui/music/MediaService;->mTargetState:I

    :cond_0
    return-void
.end method

.method public onDestroy()V
    .locals 2

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    const-string v0, "MediaService"

    const-string v1, "onDestroy player"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/music/MediaService;->stop()V

    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 2
    .param p1    # Landroid/content/Intent;
    .param p2    # I
    .param p3    # I

    const-string v0, "MediaService"

    const-string v1, "onStartCommand player"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    move-result v0

    return v0
.end method

.method public openPlayer(Ljava/lang/String;)V
    .locals 10
    .param p1    # Ljava/lang/String;

    const/4 v9, 0x0

    const/4 v8, -0x1

    new-instance v3, Landroid/content/Intent;

    const-string v4, "com.android.music.musicservicecommand"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v4, "command"

    const-string v5, "pause"

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v3}, Lcom/jrm/localmm/ui/music/MediaService;->sendBroadcast(Landroid/content/Intent;)V

    const-string v4, "MediaService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "openPlayer, current thread id : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Thread;->getId()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/music/MediaService;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f060074    # com.jrm.localmm.R.string.video_media_other_error_unknown

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v9}, Lcom/jrm/localmm/ui/music/MediaService;->release(Z)V

    :try_start_0
    new-instance v4, Lcom/mstar/android/media/MMediaPlayer;

    invoke-direct {v4}, Lcom/mstar/android/media/MMediaPlayer;-><init>()V

    iput-object v4, p0, Lcom/jrm/localmm/ui/music/MediaService;->player:Lcom/mstar/android/media/MMediaPlayer;

    iget-object v4, p0, Lcom/jrm/localmm/ui/music/MediaService;->player:Lcom/mstar/android/media/MMediaPlayer;

    invoke-virtual {v4}, Lcom/mstar/android/media/MMediaPlayer;->reset()V

    const/4 v4, 0x0

    iput v4, p0, Lcom/jrm/localmm/ui/music/MediaService;->mSeekWhenPrepared:I

    iget-object v4, p0, Lcom/jrm/localmm/ui/music/MediaService;->player:Lcom/mstar/android/media/MMediaPlayer;

    iget-object v5, p0, Lcom/jrm/localmm/ui/music/MediaService;->mErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    invoke-virtual {v4, v5}, Lcom/mstar/android/media/MMediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    iget-object v4, p0, Lcom/jrm/localmm/ui/music/MediaService;->player:Lcom/mstar/android/media/MMediaPlayer;

    iget-object v5, p0, Lcom/jrm/localmm/ui/music/MediaService;->mSeekCompleteListener:Landroid/media/MediaPlayer$OnSeekCompleteListener;

    invoke-virtual {v4, v5}, Lcom/mstar/android/media/MMediaPlayer;->setOnSeekCompleteListener(Landroid/media/MediaPlayer$OnSeekCompleteListener;)V

    iget-object v4, p0, Lcom/jrm/localmm/ui/music/MediaService;->player:Lcom/mstar/android/media/MMediaPlayer;

    iget-object v5, p0, Lcom/jrm/localmm/ui/music/MediaService;->mCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    invoke-virtual {v4, v5}, Lcom/mstar/android/media/MMediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    iget-object v4, p0, Lcom/jrm/localmm/ui/music/MediaService;->player:Lcom/mstar/android/media/MMediaPlayer;

    iget-object v5, p0, Lcom/jrm/localmm/ui/music/MediaService;->mInfoListener:Landroid/media/MediaPlayer$OnInfoListener;

    invoke-virtual {v4, v5}, Lcom/mstar/android/media/MMediaPlayer;->setOnInfoListener(Landroid/media/MediaPlayer$OnInfoListener;)V

    iget-object v4, p0, Lcom/jrm/localmm/ui/music/MediaService;->player:Lcom/mstar/android/media/MMediaPlayer;

    iget-object v5, p0, Lcom/jrm/localmm/ui/music/MediaService;->mPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

    invoke-virtual {v4, v5}, Lcom/mstar/android/media/MMediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    iget-object v4, p0, Lcom/jrm/localmm/ui/music/MediaService;->player:Lcom/mstar/android/media/MMediaPlayer;

    invoke-virtual {v4, p1}, Lcom/mstar/android/media/MMediaPlayer;->setDataSource(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/jrm/localmm/ui/music/MediaService;->player:Lcom/mstar/android/media/MMediaPlayer;

    invoke-virtual {v4}, Lcom/mstar/android/media/MMediaPlayer;->prepareAsync()V

    const/4 v4, 0x1

    iput v4, p0, Lcom/jrm/localmm/ui/music/MediaService;->mCurrentState:I
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_6
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_8

    :goto_0
    return-void

    :catch_0
    move-exception v0

    iput v8, p0, Lcom/jrm/localmm/ui/music/MediaService;->mCurrentState:I

    iput v8, p0, Lcom/jrm/localmm/ui/music/MediaService;->mTargetState:I

    :try_start_1
    iget-object v4, p0, Lcom/jrm/localmm/ui/music/MediaService;->mMusicStatusListener:Lcom/jrm/localmm/ui/music/IMusicStatusListener;

    invoke-interface {v4, v2}, Lcom/jrm/localmm/ui/music/IMusicStatusListener;->musicPlayException(Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    :catch_2
    move-exception v0

    iput v8, p0, Lcom/jrm/localmm/ui/music/MediaService;->mCurrentState:I

    iput v8, p0, Lcom/jrm/localmm/ui/music/MediaService;->mTargetState:I

    :try_start_2
    iget-object v4, p0, Lcom/jrm/localmm/ui/music/MediaService;->mMusicStatusListener:Lcom/jrm/localmm/ui/music/IMusicStatusListener;

    invoke-interface {v4, v2}, Lcom/jrm/localmm/ui/music/IMusicStatusListener;->musicPlayException(Ljava/lang/String;)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_3

    goto :goto_0

    :catch_3
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    :catch_4
    move-exception v0

    iput v8, p0, Lcom/jrm/localmm/ui/music/MediaService;->mCurrentState:I

    iput v8, p0, Lcom/jrm/localmm/ui/music/MediaService;->mTargetState:I

    :try_start_3
    iget-object v4, p0, Lcom/jrm/localmm/ui/music/MediaService;->mMusicStatusListener:Lcom/jrm/localmm/ui/music/IMusicStatusListener;

    invoke-interface {v4, v2}, Lcom/jrm/localmm/ui/music/IMusicStatusListener;->musicPlayException(Ljava/lang/String;)V
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_5

    goto :goto_0

    :catch_5
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    :catch_6
    move-exception v0

    iput v8, p0, Lcom/jrm/localmm/ui/music/MediaService;->mCurrentState:I

    iput v8, p0, Lcom/jrm/localmm/ui/music/MediaService;->mTargetState:I

    :try_start_4
    iget-object v4, p0, Lcom/jrm/localmm/ui/music/MediaService;->mMusicStatusListener:Lcom/jrm/localmm/ui/music/IMusicStatusListener;

    invoke-interface {v4, v2}, Lcom/jrm/localmm/ui/music/IMusicStatusListener;->musicPlayException(Ljava/lang/String;)V
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_7

    goto :goto_0

    :catch_7
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    :catch_8
    move-exception v0

    iput v8, p0, Lcom/jrm/localmm/ui/music/MediaService;->mCurrentState:I

    iput v8, p0, Lcom/jrm/localmm/ui/music/MediaService;->mTargetState:I

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :try_start_5
    iget-object v4, p0, Lcom/jrm/localmm/ui/music/MediaService;->mMusicStatusListener:Lcom/jrm/localmm/ui/music/IMusicStatusListener;

    invoke-interface {v4, v2}, Lcom/jrm/localmm/ui/music/IMusicStatusListener;->musicPlayException(Ljava/lang/String;)V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_5} :catch_9

    goto :goto_0

    :catch_9
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public pause()V
    .locals 6

    const/4 v5, 0x4

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/music/MediaService;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f060074    # com.jrm.localmm.R.string.video_media_other_error_unknown

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0}, Lcom/jrm/localmm/ui/music/MediaService;->isInPlaybackState()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/jrm/localmm/ui/music/MediaService;->player:Lcom/mstar/android/media/MMediaPlayer;

    invoke-virtual {v3}, Lcom/mstar/android/media/MMediaPlayer;->isPlaying()Z

    move-result v3

    if-eqz v3, :cond_0

    :try_start_0
    iget-object v3, p0, Lcom/jrm/localmm/ui/music/MediaService;->player:Lcom/mstar/android/media/MMediaPlayer;

    invoke-virtual {v3}, Lcom/mstar/android/media/MMediaPlayer;->pause()V

    const/4 v3, 0x4

    iput v3, p0, Lcom/jrm/localmm/ui/music/MediaService;->mCurrentState:I
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    :cond_0
    :goto_0
    iput v5, p0, Lcom/jrm/localmm/ui/music/MediaService;->mTargetState:I

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    :try_start_1
    iget-object v3, p0, Lcom/jrm/localmm/ui/music/MediaService;->mMusicStatusListener:Lcom/jrm/localmm/ui/music/IMusicStatusListener;

    invoke-interface {v3, v2}, Lcom/jrm/localmm/ui/music/IMusicStatusListener;->musicPlayException(Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :try_start_2
    iget-object v3, p0, Lcom/jrm/localmm/ui/music/MediaService;->mMusicStatusListener:Lcom/jrm/localmm/ui/music/IMusicStatusListener;

    invoke-interface {v3, v2}, Lcom/jrm/localmm/ui/music/IMusicStatusListener;->musicPlayException(Ljava/lang/String;)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_3

    goto :goto_0

    :catch_3
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public seekTo(I)V
    .locals 7
    .param p1    # I

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/music/MediaService;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f060074    # com.jrm.localmm.R.string.video_media_other_error_unknown

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0}, Lcom/jrm/localmm/ui/music/MediaService;->isInPlaybackState()Z

    move-result v3

    if-eqz v3, :cond_0

    if-ltz p1, :cond_0

    int-to-long v3, p1

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/music/MediaService;->getDuration()J

    move-result-wide v5

    cmp-long v3, v3, v5

    if-gtz v3, :cond_0

    const/4 v3, 0x0

    :try_start_0
    iput v3, p0, Lcom/jrm/localmm/ui/music/MediaService;->mSeekWhenPrepared:I

    iget-object v3, p0, Lcom/jrm/localmm/ui/music/MediaService;->player:Lcom/mstar/android/media/MMediaPlayer;

    invoke-virtual {v3, p1}, Lcom/mstar/android/media/MMediaPlayer;->seekTo(I)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    :try_start_1
    iget-object v3, p0, Lcom/jrm/localmm/ui/music/MediaService;->mMusicStatusListener:Lcom/jrm/localmm/ui/music/IMusicStatusListener;

    invoke-interface {v3, v2}, Lcom/jrm/localmm/ui/music/IMusicStatusListener;->musicPlayException(Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :try_start_2
    iget-object v3, p0, Lcom/jrm/localmm/ui/music/MediaService;->mMusicStatusListener:Lcom/jrm/localmm/ui/music/IMusicStatusListener;

    invoke-interface {v3, v2}, Lcom/jrm/localmm/ui/music/IMusicStatusListener;->musicPlayException(Ljava/lang/String;)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_3

    goto :goto_0

    :catch_3
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    :cond_0
    iput p1, p0, Lcom/jrm/localmm/ui/music/MediaService;->mSeekWhenPrepared:I

    goto :goto_0
.end method

.method public start()V
    .locals 5

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/music/MediaService;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f060074    # com.jrm.localmm.R.string.video_media_other_error_unknown

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0}, Lcom/jrm/localmm/ui/music/MediaService;->isInPlaybackState()Z

    move-result v3

    if-nez v3, :cond_0

    :goto_0
    return-void

    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/jrm/localmm/ui/music/MediaService;->player:Lcom/mstar/android/media/MMediaPlayer;

    invoke-virtual {v3}, Lcom/mstar/android/media/MMediaPlayer;->start()V

    const/4 v3, 0x3

    iput v3, p0, Lcom/jrm/localmm/ui/music/MediaService;->mCurrentState:I

    const/4 v3, 0x3

    iput v3, p0, Lcom/jrm/localmm/ui/music/MediaService;->mTargetState:I
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    :goto_1
    :try_start_1
    iget-object v3, p0, Lcom/jrm/localmm/ui/music/MediaService;->mMusicStatusListener:Lcom/jrm/localmm/ui/music/IMusicStatusListener;

    invoke-interface {v3}, Lcom/jrm/localmm/ui/music/IMusicStatusListener;->handleSongSpectrum()V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    :try_start_2
    iget-object v3, p0, Lcom/jrm/localmm/ui/music/MediaService;->mMusicStatusListener:Lcom/jrm/localmm/ui/music/IMusicStatusListener;

    invoke-interface {v3, v2}, Lcom/jrm/localmm/ui/music/IMusicStatusListener;->musicPlayException(Ljava/lang/String;)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_0

    :catch_2
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    :catch_3
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :try_start_3
    iget-object v3, p0, Lcom/jrm/localmm/ui/music/MediaService;->mMusicStatusListener:Lcom/jrm/localmm/ui/music/IMusicStatusListener;

    invoke-interface {v3, v2}, Lcom/jrm/localmm/ui/music/IMusicStatusListener;->musicPlayException(Ljava/lang/String;)V
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_4

    goto :goto_1

    :catch_4
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1
.end method

.method public stop()V
    .locals 5

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/music/MediaService;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f060074    # com.jrm.localmm.R.string.video_media_other_error_unknown

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/jrm/localmm/ui/music/MediaService;->player:Lcom/mstar/android/media/MMediaPlayer;

    if-eqz v3, :cond_0

    const/4 v3, 0x0

    :try_start_0
    iput v3, p0, Lcom/jrm/localmm/ui/music/MediaService;->mCurrentState:I

    const/4 v3, 0x0

    iput v3, p0, Lcom/jrm/localmm/ui/music/MediaService;->mTargetState:I

    iget-object v3, p0, Lcom/jrm/localmm/ui/music/MediaService;->player:Lcom/mstar/android/media/MMediaPlayer;

    invoke-virtual {v3}, Lcom/mstar/android/media/MMediaPlayer;->stop()V

    iget-object v3, p0, Lcom/jrm/localmm/ui/music/MediaService;->player:Lcom/mstar/android/media/MMediaPlayer;

    invoke-virtual {v3}, Lcom/mstar/android/media/MMediaPlayer;->release()V

    const/4 v3, 0x0

    iput-object v3, p0, Lcom/jrm/localmm/ui/music/MediaService;->player:Lcom/mstar/android/media/MMediaPlayer;

    const-string v3, "MediaService"

    const-string v4, "stop player done"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    :try_start_1
    iget-object v3, p0, Lcom/jrm/localmm/ui/music/MediaService;->mMusicStatusListener:Lcom/jrm/localmm/ui/music/IMusicStatusListener;

    invoke-interface {v3, v2}, Lcom/jrm/localmm/ui/music/IMusicStatusListener;->musicPlayException(Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :try_start_2
    iget-object v3, p0, Lcom/jrm/localmm/ui/music/MediaService;->mMusicStatusListener:Lcom/jrm/localmm/ui/music/IMusicStatusListener;

    invoke-interface {v3, v2}, Lcom/jrm/localmm/ui/music/IMusicStatusListener;->musicPlayException(Ljava/lang/String;)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_3

    goto :goto_0

    :catch_3
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method
