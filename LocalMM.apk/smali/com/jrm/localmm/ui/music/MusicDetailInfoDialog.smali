.class public Lcom/jrm/localmm/ui/music/MusicDetailInfoDialog;
.super Landroid/app/Dialog;
.source "MusicDetailInfoDialog.java"


# instance fields
.field private activity:Landroid/app/Activity;

.field private artistName:Landroid/widget/TextView;

.field private duration:Landroid/widget/TextView;

.field private fileFormat:Landroid/widget/TextView;

.field private fileName:Landroid/widget/TextView;

.field private filePath:Landroid/widget/TextView;

.field private musicFile:Lcom/jrm/localmm/business/data/BaseData;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/jrm/localmm/business/data/BaseData;)V
    .locals 0
    .param p1    # Landroid/app/Activity;
    .param p2    # Lcom/jrm/localmm/business/data/BaseData;

    invoke-direct {p0, p1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/jrm/localmm/ui/music/MusicDetailInfoDialog;->activity:Landroid/app/Activity;

    iput-object p2, p0, Lcom/jrm/localmm/ui/music/MusicDetailInfoDialog;->musicFile:Lcom/jrm/localmm/business/data/BaseData;

    return-void
.end method

.method private findViews()V
    .locals 1

    const v0, 0x7f08003e    # com.jrm.localmm.R.id.file_name

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/music/MusicDetailInfoDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/jrm/localmm/ui/music/MusicDetailInfoDialog;->fileName:Landroid/widget/TextView;

    const v0, 0x7f080044    # com.jrm.localmm.R.id.artist_name

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/music/MusicDetailInfoDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/jrm/localmm/ui/music/MusicDetailInfoDialog;->artistName:Landroid/widget/TextView;

    const v0, 0x7f080047    # com.jrm.localmm.R.id.file_format

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/music/MusicDetailInfoDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/jrm/localmm/ui/music/MusicDetailInfoDialog;->fileFormat:Landroid/widget/TextView;

    const v0, 0x7f080041    # com.jrm.localmm.R.id.duration

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/music/MusicDetailInfoDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/jrm/localmm/ui/music/MusicDetailInfoDialog;->duration:Landroid/widget/TextView;

    const v0, 0x7f08004a    # com.jrm.localmm.R.id.file_path

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/music/MusicDetailInfoDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/jrm/localmm/ui/music/MusicDetailInfoDialog;->filePath:Landroid/widget/TextView;

    return-void
.end method

.method private showMusicInfo()V
    .locals 4

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicDetailInfoDialog;->musicFile:Lcom/jrm/localmm/business/data/BaseData;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicDetailInfoDialog;->musicFile:Lcom/jrm/localmm/business/data/BaseData;

    invoke-virtual {v1}, Lcom/jrm/localmm/business/data/BaseData;->getPath()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicDetailInfoDialog;->musicFile:Lcom/jrm/localmm/business/data/BaseData;

    invoke-virtual {v1}, Lcom/jrm/localmm/business/data/BaseData;->getName()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicDetailInfoDialog;->fileName:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/jrm/localmm/ui/music/MusicDetailInfoDialog;->musicFile:Lcom/jrm/localmm/business/data/BaseData;

    invoke-virtual {v2}, Lcom/jrm/localmm/business/data/BaseData;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicDetailInfoDialog;->musicFile:Lcom/jrm/localmm/business/data/BaseData;

    invoke-virtual {v1}, Lcom/jrm/localmm/business/data/BaseData;->getArtist()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicDetailInfoDialog;->artistName:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/jrm/localmm/ui/music/MusicDetailInfoDialog;->musicFile:Lcom/jrm/localmm/business/data/BaseData;

    invoke-virtual {v2}, Lcom/jrm/localmm/business/data/BaseData;->getArtist()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicDetailInfoDialog;->fileFormat:Landroid/widget/TextView;

    const-string v2, "."

    invoke-virtual {v0, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicDetailInfoDialog;->musicFile:Lcom/jrm/localmm/business/data/BaseData;

    invoke-virtual {v1}, Lcom/jrm/localmm/business/data/BaseData;->getDuration2()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicDetailInfoDialog;->duration:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/jrm/localmm/ui/music/MusicDetailInfoDialog;->musicFile:Lcom/jrm/localmm/business/data/BaseData;

    invoke-virtual {v2}, Lcom/jrm/localmm/business/data/BaseData;->getDuration2()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_2
    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicDetailInfoDialog;->filePath:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/jrm/localmm/ui/music/MusicDetailInfoDialog;->musicFile:Lcom/jrm/localmm/business/data/BaseData;

    invoke-virtual {v2}, Lcom/jrm/localmm/business/data/BaseData;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicDetailInfoDialog;->fileName:Landroid/widget/TextView;

    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicDetailInfoDialog;->artistName:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/jrm/localmm/ui/music/MusicDetailInfoDialog;->activity:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f060053    # com.jrm.localmm.R.string.unknown

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_3
    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicDetailInfoDialog;->duration:Landroid/widget/TextView;

    sget v2, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->countTime:I

    invoke-static {v2}, Lcom/jrm/localmm/util/Tools;->formatDuration2(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 10
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    const v6, 0x7f03000b    # com.jrm.localmm.R.layout.music_info

    invoke-virtual {p0, v6}, Lcom/jrm/localmm/ui/music/MusicDetailInfoDialog;->setContentView(I)V

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/music/MusicDetailInfoDialog;->getWindow()Landroid/view/Window;

    move-result-object v3

    const/4 v6, 0x0

    invoke-virtual {v3, v6}, Landroid/view/Window;->setTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {v3}, Landroid/view/Window;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v6

    invoke-interface {v6}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    invoke-virtual {v0, v2}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    const-string v6, "MusicDetailInfoDialog"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "x : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, v2, Landroid/graphics/Point;->x:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " y : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, v2, Landroid/graphics/Point;->y:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v6, v2, Landroid/graphics/Point;->x:I

    int-to-double v6, v6

    const-wide v8, 0x3fd999999999999aL    # 0.4

    mul-double/2addr v6, v8

    double-to-int v4, v6

    iget v6, v2, Landroid/graphics/Point;->y:I

    int-to-double v6, v6

    const-wide v8, 0x3fe6666666666666L    # 0.7

    mul-double/2addr v6, v8

    double-to-int v1, v6

    invoke-virtual {v3, v4, v1}, Landroid/view/Window;->setLayout(II)V

    const/16 v6, 0x11

    invoke-virtual {v3, v6}, Landroid/view/Window;->setGravity(I)V

    invoke-virtual {v3}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v5

    const/high16 v6, 0x3f800000    # 1.0f

    iput v6, v5, Landroid/view/WindowManager$LayoutParams;->screenBrightness:F

    invoke-virtual {v3, v5}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    const v6, 0x106000d    # android.R.color.transparent

    invoke-virtual {v3, v6}, Landroid/view/Window;->setBackgroundDrawableResource(I)V

    invoke-direct {p0}, Lcom/jrm/localmm/ui/music/MusicDetailInfoDialog;->findViews()V

    invoke-direct {p0}, Lcom/jrm/localmm/ui/music/MusicDetailInfoDialog;->showMusicInfo()V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v0, 0x1

    return v0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v0, 0x1

    if-nez p1, :cond_0

    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Landroid/app/Dialog;->cancel()V

    goto :goto_0
.end method
