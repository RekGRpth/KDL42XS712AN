.class Lcom/jrm/localmm/ui/music/MusicPlayerActivity$2;
.super Lcom/jrm/localmm/ui/music/IMusicStatusListener$Stub;
.source "MusicPlayerActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jrm/localmm/ui/music/MusicPlayerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/localmm/ui/music/MusicPlayerActivity;


# direct methods
.method constructor <init>(Lcom/jrm/localmm/ui/music/MusicPlayerActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity$2;->this$0:Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

    invoke-direct {p0}, Lcom/jrm/localmm/ui/music/IMusicStatusListener$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessageInfo(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity$2;->this$0:Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

    invoke-virtual {v1}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f060071    # com.jrm.localmm.R.string.video_media_error_video_unsupport

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    if-ne p1, v1, :cond_0

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity$2;->this$0:Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

    # setter for: Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->isVideoSupport:Z
    invoke-static {v1, v3}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->access$102(Lcom/jrm/localmm/ui/music/MusicPlayerActivity;Z)Z

    :cond_0
    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity$2;->this$0:Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

    invoke-virtual {v1}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f060070    # com.jrm.localmm.R.string.video_media_error_audio_unsupport

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    if-ne p1, v1, :cond_1

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity$2;->this$0:Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

    # setter for: Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->isAudioSupport:Z
    invoke-static {v1, v3}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->access$202(Lcom/jrm/localmm/ui/music/MusicPlayerActivity;Z)Z

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity$2;->this$0:Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

    const/16 v2, 0x11

    invoke-static {v1, p1, v2}, Lcom/jrm/localmm/util/ToastFactory;->getToast(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_1
    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity$2;->this$0:Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->isVideoSupport:Z
    invoke-static {v1}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->access$100(Lcom/jrm/localmm/ui/music/MusicPlayerActivity;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity$2;->this$0:Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->isAudioSupport:Z
    invoke-static {v1}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->access$200(Lcom/jrm/localmm/ui/music/MusicPlayerActivity;)Z

    move-result v1

    if-nez v1, :cond_2

    sget-object v1, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->musicList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x1

    if-gt v1, v2, :cond_3

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity$2;->this$0:Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

    # invokes: Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->exitMusicPaly()V
    invoke-static {v1}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->access$300(Lcom/jrm/localmm/ui/music/MusicPlayerActivity;)V

    :cond_2
    :goto_0
    const-string v1, "MusicPlayerActivity"

    const-string v2, "IMusicStatusListener.Stub, handleMessageInfo"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_3
    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity$2;->this$0:Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

    iget-object v1, v1, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->musicPlayHandle:Landroid/os/Handler;

    const/4 v2, 0x7

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity$2;->this$0:Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

    invoke-virtual {v1}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->processPlayCompletion()V

    goto :goto_0
.end method

.method public handleSongSpectrum()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v0, "MusicPlayerActivity"

    const-string v1, "IMusicStatusListener.Stub, handleSongSpectrum"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public musicCompleted()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v0, "MusicPlayerActivity"

    const-string v1, "IMusicStatusListener.Stub, musicCompleted"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity$2;->this$0:Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

    iget-object v0, v0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->musicPlayHandle:Landroid/os/Handler;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity$2;->this$0:Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->processPlayCompletion()V

    return-void
.end method

.method public musicPlayErrorWithMsg(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v0, "MusicPlayerActivity"

    const-string v1, "IMusicStatusListener.Stub, musicPlayErrorWithMsg"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity$2;->this$0:Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

    # invokes: Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->exceptionProcess(Ljava/lang/String;)V
    invoke-static {v0, p1}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->access$000(Lcom/jrm/localmm/ui/music/MusicPlayerActivity;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity$2;->this$0:Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

    iget-object v0, v0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->musicPlayHandle:Landroid/os/Handler;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    return-void
.end method

.method public musicPlayException(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v0, "MusicPlayerActivity"

    const-string v1, "IMusicStatusListener.Stub, musicPlayException"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity$2;->this$0:Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

    # invokes: Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->exceptionProcess(Ljava/lang/String;)V
    invoke-static {v0, p1}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->access$000(Lcom/jrm/localmm/ui/music/MusicPlayerActivity;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity$2;->this$0:Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

    iget-object v0, v0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->musicPlayHandle:Landroid/os/Handler;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    return-void
.end method

.method public musicPrepared()V
    .locals 2

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/jrm/localmm/ui/music/MusicPlayerActivity$2$1;

    invoke-direct {v1, p0}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity$2$1;-><init>(Lcom/jrm/localmm/ui/music/MusicPlayerActivity$2;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method public musicSeekCompleted()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v0, "MusicPlayerActivity"

    const-string v1, "IMusicStatusListener.Stub, musicSeekCompleted"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method
