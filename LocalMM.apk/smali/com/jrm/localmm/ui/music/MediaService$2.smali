.class Lcom/jrm/localmm/ui/music/MediaService$2;
.super Ljava/lang/Object;
.source "MediaService.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnErrorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jrm/localmm/ui/music/MediaService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/localmm/ui/music/MediaService;


# direct methods
.method constructor <init>(Lcom/jrm/localmm/ui/music/MediaService;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/localmm/ui/music/MediaService$2;->this$0:Lcom/jrm/localmm/ui/music/MediaService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Landroid/media/MediaPlayer;II)Z
    .locals 6
    .param p1    # Landroid/media/MediaPlayer;
    .param p2    # I
    .param p3    # I

    const/4 v5, -0x1

    const-string v2, "MediaService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onError, what : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " extra : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/jrm/localmm/ui/music/MediaService$2;->this$0:Lcom/jrm/localmm/ui/music/MediaService;

    # setter for: Lcom/jrm/localmm/ui/music/MediaService;->mCurrentState:I
    invoke-static {v2, v5}, Lcom/jrm/localmm/ui/music/MediaService;->access$002(Lcom/jrm/localmm/ui/music/MediaService;I)I

    iget-object v2, p0, Lcom/jrm/localmm/ui/music/MediaService$2;->this$0:Lcom/jrm/localmm/ui/music/MediaService;

    # setter for: Lcom/jrm/localmm/ui/music/MediaService;->mTargetState:I
    invoke-static {v2, v5}, Lcom/jrm/localmm/ui/music/MediaService;->access$102(Lcom/jrm/localmm/ui/music/MediaService;I)I

    const-string v1, ""

    sparse-switch p2, :sswitch_data_0

    const-string v2, "MediaService"

    const-string v3, "Play Error::: default error!"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/jrm/localmm/ui/music/MediaService$2;->this$0:Lcom/jrm/localmm/ui/music/MediaService;

    invoke-virtual {v2}, Lcom/jrm/localmm/ui/music/MediaService;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f060074    # com.jrm.localmm.R.string.video_media_other_error_unknown

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    :goto_0
    :try_start_0
    iget-object v2, p0, Lcom/jrm/localmm/ui/music/MediaService$2;->this$0:Lcom/jrm/localmm/ui/music/MediaService;

    # getter for: Lcom/jrm/localmm/ui/music/MediaService;->mMusicStatusListener:Lcom/jrm/localmm/ui/music/IMusicStatusListener;
    invoke-static {v2}, Lcom/jrm/localmm/ui/music/MediaService;->access$400(Lcom/jrm/localmm/ui/music/MediaService;)Lcom/jrm/localmm/ui/music/IMusicStatusListener;

    move-result-object v2

    invoke-interface {v2, v1}, Lcom/jrm/localmm/ui/music/IMusicStatusListener;->musicPlayErrorWithMsg(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    const/4 v2, 0x0

    return v2

    :sswitch_0
    const-string v2, "MediaService"

    const-string v3, "Play Error::: MEDIA_ERROR_SERVER_DIED"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/jrm/localmm/ui/music/MediaService$2;->this$0:Lcom/jrm/localmm/ui/music/MediaService;

    # getter for: Lcom/jrm/localmm/ui/music/MediaService;->player:Lcom/mstar/android/media/MMediaPlayer;
    invoke-static {v2}, Lcom/jrm/localmm/ui/music/MediaService;->access$200(Lcom/jrm/localmm/ui/music/MediaService;)Lcom/mstar/android/media/MMediaPlayer;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/media/MMediaPlayer;->release()V

    iget-object v2, p0, Lcom/jrm/localmm/ui/music/MediaService$2;->this$0:Lcom/jrm/localmm/ui/music/MediaService;

    const/4 v3, 0x0

    # setter for: Lcom/jrm/localmm/ui/music/MediaService;->player:Lcom/mstar/android/media/MMediaPlayer;
    invoke-static {v2, v3}, Lcom/jrm/localmm/ui/music/MediaService;->access$202(Lcom/jrm/localmm/ui/music/MediaService;Lcom/mstar/android/media/MMediaPlayer;)Lcom/mstar/android/media/MMediaPlayer;

    iget-object v2, p0, Lcom/jrm/localmm/ui/music/MediaService$2;->this$0:Lcom/jrm/localmm/ui/music/MediaService;

    new-instance v3, Lcom/mstar/android/media/MMediaPlayer;

    invoke-direct {v3}, Lcom/mstar/android/media/MMediaPlayer;-><init>()V

    # setter for: Lcom/jrm/localmm/ui/music/MediaService;->player:Lcom/mstar/android/media/MMediaPlayer;
    invoke-static {v2, v3}, Lcom/jrm/localmm/ui/music/MediaService;->access$202(Lcom/jrm/localmm/ui/music/MediaService;Lcom/mstar/android/media/MMediaPlayer;)Lcom/mstar/android/media/MMediaPlayer;

    iget-object v2, p0, Lcom/jrm/localmm/ui/music/MediaService$2;->this$0:Lcom/jrm/localmm/ui/music/MediaService;

    invoke-virtual {v2}, Lcom/jrm/localmm/ui/music/MediaService;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f060076    # com.jrm.localmm.R.string.video_media_error_server_died

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :sswitch_1
    const-string v2, "MediaService"

    const-string v3, "Play Error::: MEDIA_ERROR_UNKNOWN"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/jrm/localmm/ui/music/MediaService$2;->this$0:Lcom/jrm/localmm/ui/music/MediaService;

    # invokes: Lcom/jrm/localmm/ui/music/MediaService;->processErrorUnknown(Landroid/media/MediaPlayer;II)Ljava/lang/String;
    invoke-static {v2, p1, p2, p3}, Lcom/jrm/localmm/ui/music/MediaService;->access$300(Lcom/jrm/localmm/ui/music/MediaService;Landroid/media/MediaPlayer;II)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :sswitch_2
    const-string v2, "MediaService"

    const-string v3, "Play Error::: MEDIA_ERROR_NOT_VALID_FOR_PROGRESSIVE_PLAYBACK"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/jrm/localmm/ui/music/MediaService$2;->this$0:Lcom/jrm/localmm/ui/music/MediaService;

    invoke-virtual {v2}, Lcom/jrm/localmm/ui/music/MediaService;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f060075    # com.jrm.localmm.R.string.video_media_error_not_valid

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x64 -> :sswitch_0
        0xc8 -> :sswitch_2
    .end sparse-switch
.end method
