.class public Lcom/jrm/localmm/ui/music/LyricContent;
.super Ljava/lang/Object;
.source "LyricContent.java"


# instance fields
.field private Lyric:Ljava/lang/String;

.field private LyricTime:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getLyric()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/LyricContent;->Lyric:Ljava/lang/String;

    return-object v0
.end method

.method public getLyricTime()I
    .locals 1

    iget v0, p0, Lcom/jrm/localmm/ui/music/LyricContent;->LyricTime:I

    return v0
.end method

.method public setLyric(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/jrm/localmm/ui/music/LyricContent;->Lyric:Ljava/lang/String;

    return-void
.end method

.method public setLyricTime(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/jrm/localmm/ui/music/LyricContent;->LyricTime:I

    return-void
.end method
