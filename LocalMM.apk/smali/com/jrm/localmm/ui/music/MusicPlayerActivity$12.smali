.class Lcom/jrm/localmm/ui/music/MusicPlayerActivity$12;
.super Ljava/lang/Object;
.source "MusicPlayerActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->changeSource()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/localmm/ui/music/MusicPlayerActivity;


# direct methods
.method constructor <init>(Lcom/jrm/localmm/ui/music/MusicPlayerActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity$12;->this$0:Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity$12;->this$0:Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->appSkin:Lcom/mstar/android/tv/TvCommonManager;
    invoke-static {v0}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->access$1500(Lcom/jrm/localmm/ui/music/MusicPlayerActivity;)Lcom/mstar/android/tv/TvCommonManager;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity$12;->this$0:Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

    invoke-static {}, Lcom/mstar/android/tv/TvCommonManager;->getInstance()Lcom/mstar/android/tv/TvCommonManager;

    move-result-object v1

    # setter for: Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->appSkin:Lcom/mstar/android/tv/TvCommonManager;
    invoke-static {v0, v1}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->access$1502(Lcom/jrm/localmm/ui/music/MusicPlayerActivity;Lcom/mstar/android/tv/TvCommonManager;)Lcom/mstar/android/tv/TvCommonManager;

    :cond_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity$12;->this$0:Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->appSkin:Lcom/mstar/android/tv/TvCommonManager;
    invoke-static {v0}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->access$1500(Lcom/jrm/localmm/ui/music/MusicPlayerActivity;)Lcom/mstar/android/tv/TvCommonManager;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity$12;->this$0:Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

    iget-object v1, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity$12;->this$0:Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->appSkin:Lcom/mstar/android/tv/TvCommonManager;
    invoke-static {v1}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->access$1500(Lcom/jrm/localmm/ui/music/MusicPlayerActivity;)Lcom/mstar/android/tv/TvCommonManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tv/TvCommonManager;->getCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v1

    # setter for: Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->inputSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    invoke-static {v0, v1}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->access$1602(Lcom/jrm/localmm/ui/music/MusicPlayerActivity;Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity$12;->this$0:Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->inputSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    invoke-static {v0}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->access$1600(Lcom/jrm/localmm/ui/music/MusicPlayerActivity;)Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_STORAGE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/jrm/localmm/ui/music/MusicPlayerActivity$12;->this$0:Lcom/jrm/localmm/ui/music/MusicPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->appSkin:Lcom/mstar/android/tv/TvCommonManager;
    invoke-static {v0}, Lcom/jrm/localmm/ui/music/MusicPlayerActivity;->access$1500(Lcom/jrm/localmm/ui/music/MusicPlayerActivity;)Lcom/mstar/android/tv/TvCommonManager;

    move-result-object v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_STORAGE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v0, v1}, Lcom/mstar/android/tv/TvCommonManager;->setInputSource(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)V

    const-string v0, "MusicPlayerActivity"

    const-string v1, "already SetInputSource"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    return-void
.end method
