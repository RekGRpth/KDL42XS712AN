.class Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$11;
.super Landroid/content/BroadcastReceiver;
.source "PhotoPlayerActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;


# direct methods
.method constructor <init>(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$11;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$11;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mSourceFrom:I
    invoke-static {v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$3900(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)I

    move-result v0

    const/16 v1, 0x11

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$11;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mSourceFrom:I
    invoke-static {v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$3900(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)I

    move-result v0

    const/16 v1, 0x10

    if-ne v0, v1, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$11;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$11;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    const v2, 0x7f060011    # com.jrm.localmm.R.string.net_disconnect

    invoke-virtual {v1, v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->showToastAtCenter(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$11;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$11;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mFileInputStream:Ljava/io/FileInputStream;
    invoke-static {v1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$4100(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Ljava/io/FileInputStream;

    move-result-object v1

    # invokes: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->closeSilently(Ljava/io/Closeable;)V
    invoke-static {v0, v1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$4200(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;Ljava/io/Closeable;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$11;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->finish()V

    :cond_1
    return-void
.end method
