.class Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$8;
.super Ljava/lang/Object;
.source "PhotoPlayerActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->decodeGif(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;


# direct methods
.method constructor <init>(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$8;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    const/4 v1, 0x3

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$8;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCurrentView:I
    invoke-static {v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$1200(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)I

    move-result v0

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$8;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;
    invoke-static {v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$300(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    move-result-object v0

    iget-object v0, v0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->mScrollView:Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->setCurrentView(I)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$8;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # setter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCurrentView:I
    invoke-static {v0, v1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$1202(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;I)I

    :cond_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$8;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;
    invoke-static {v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$300(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    move-result-object v0

    iget-object v1, v0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->photo_name:Landroid/widget/TextView;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$8;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    const v3, 0x7f060044    # com.jrm.localmm.R.string.current_pic

    invoke-virtual {v2, v3}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$8;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoFileList:Ljava/util/List;
    invoke-static {v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$1600(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Ljava/util/List;

    move-result-object v0

    iget-object v3, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$8;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCurrentPosition:I
    invoke-static {v3}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$1500(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)I

    move-result v3

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jrm/localmm/business/data/BaseData;

    invoke-virtual {v0}, Lcom/jrm/localmm/business/data/BaseData;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
