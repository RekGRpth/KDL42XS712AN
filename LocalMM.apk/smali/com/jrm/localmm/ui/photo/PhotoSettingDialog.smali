.class public Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;
.super Landroid/app/Dialog;
.source "PhotoSettingDialog.java"


# instance fields
.field private adapter:Lcom/jrm/localmm/business/adapter/VideoPlaySettingListAdapter;

.field private close:Ljava/lang/String;

.field private context:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

.field private isOpen:Z

.field private mHandler:Landroid/os/Handler;

.field private onkeyListenter:Landroid/view/View$OnKeyListener;

.field private open:Ljava/lang/String;

.field private playSettingListView:Landroid/widget/ListView;

.field private time:I


# direct methods
.method public constructor <init>(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;IZLandroid/os/Handler;)V
    .locals 1
    .param p1    # Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;
    .param p2    # I
    .param p3    # Z
    .param p4    # Landroid/os/Handler;

    invoke-direct {p0, p1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    new-instance v0, Lcom/jrm/localmm/ui/photo/PhotoSettingDialog$1;

    invoke-direct {v0, p0}, Lcom/jrm/localmm/ui/photo/PhotoSettingDialog$1;-><init>(Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;->onkeyListenter:Landroid/view/View$OnKeyListener;

    iput-object p1, p0, Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;->context:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    div-int/lit16 v0, p2, 0x3e8

    iput v0, p0, Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;->time:I

    iput-boolean p3, p0, Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;->isOpen:Z

    iput-object p4, p0, Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;)Landroid/widget/ListView;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;->playSettingListView:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;)Z
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;

    iget-boolean v0, p0, Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;->isOpen:Z

    return v0
.end method

.method static synthetic access$102(Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;Z)Z
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;->isOpen:Z

    return p1
.end method

.method static synthetic access$200(Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;->close:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;->open:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;)I
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;

    iget v0, p0, Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;->time:I

    return v0
.end method

.method static synthetic access$408(Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;)I
    .locals 2
    .param p0    # Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;

    iget v0, p0, Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;->time:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;->time:I

    return v0
.end method

.method static synthetic access$410(Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;)I
    .locals 2
    .param p0    # Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;

    iget v0, p0, Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;->time:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;->time:I

    return v0
.end method

.method static synthetic access$500(Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;)Lcom/jrm/localmm/business/adapter/VideoPlaySettingListAdapter;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;->adapter:Lcom/jrm/localmm/business/adapter/VideoPlaySettingListAdapter;

    return-object v0
.end method

.method static synthetic access$600(Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;->mHandler:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public dismiss()V
    .locals 0

    invoke-super {p0}, Landroid/app/Dialog;->dismiss()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 12
    .param p1    # Landroid/os/Bundle;

    const/4 v11, 0x0

    const/4 v10, 0x0

    const/4 v9, 0x1

    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    const v5, 0x7f030020    # com.jrm.localmm.R.layout.video_play_setting_dialog

    invoke-virtual {p0, v5}, Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;->setContentView(I)V

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v5

    invoke-interface {v5}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    const v5, 0x7f0800af    # com.jrm.localmm.R.id.videoPlaySettingListView

    invoke-virtual {p0, v5}, Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ListView;

    iput-object v5, p0, Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;->playSettingListView:Landroid/widget/ListView;

    new-instance v5, Lcom/jrm/localmm/business/adapter/VideoPlaySettingListAdapter;

    iget-object v6, p0, Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;->context:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    sget-object v7, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->playSettingName:[I

    iget-object v8, p0, Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;->context:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    invoke-static {v8, v9}, Lcom/jrm/localmm/util/Tools;->initPlaySettingOpt(Landroid/content/Context;I)[Ljava/lang/String;

    move-result-object v8

    invoke-direct {v5, v6, v7, v8}, Lcom/jrm/localmm/business/adapter/VideoPlaySettingListAdapter;-><init>(Landroid/content/Context;[I[Ljava/lang/String;)V

    iput-object v5, p0, Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;->adapter:Lcom/jrm/localmm/business/adapter/VideoPlaySettingListAdapter;

    iget-object v5, p0, Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;->context:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    const v6, 0x7f0600c3    # com.jrm.localmm.R.string.play_setting_0_value_1

    invoke-virtual {v5, v6}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;->open:Ljava/lang/String;

    iget-object v5, p0, Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;->context:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    const v6, 0x7f0600c4    # com.jrm.localmm.R.string.play_setting_0_value_2

    invoke-virtual {v5, v6}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;->close:Ljava/lang/String;

    iget-boolean v5, p0, Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;->isOpen:Z

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;->open:Ljava/lang/String;

    invoke-static {v10, v5, v9}, Lcom/jrm/localmm/util/Tools;->setPlaySettingOpt(ILjava/lang/String;I)V

    :goto_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;->time:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v9, v5, v9}, Lcom/jrm/localmm/util/Tools;->setPlaySettingOpt(ILjava/lang/String;I)V

    iget-object v5, p0, Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;->playSettingListView:Landroid/widget/ListView;

    invoke-virtual {v5, v11}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    iget-object v5, p0, Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;->playSettingListView:Landroid/widget/ListView;

    iget-object v6, p0, Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;->adapter:Lcom/jrm/localmm/business/adapter/VideoPlaySettingListAdapter;

    invoke-virtual {v5, v6}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    invoke-virtual {v2, v11}, Landroid/view/Window;->setTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v5

    int-to-double v5, v5

    const-wide v7, 0x3fdb851eb851eb85L    # 0.43

    mul-double/2addr v5, v7

    double-to-int v3, v5

    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v5

    int-to-double v5, v5

    const-wide v7, 0x3fe199999999999aL    # 0.55

    mul-double/2addr v5, v7

    double-to-int v1, v5

    invoke-virtual {v2, v3, v1}, Landroid/view/Window;->setLayout(II)V

    const/16 v5, 0x11

    invoke-virtual {v2, v5}, Landroid/view/Window;->setGravity(I)V

    invoke-virtual {v2}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    iget-object v5, p0, Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;->playSettingListView:Landroid/widget/ListView;

    iget-object v6, p0, Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;->onkeyListenter:Landroid/view/View$OnKeyListener;

    invoke-virtual {v5, v6}, Landroid/widget/ListView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    return-void

    :cond_0
    iget-object v5, p0, Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;->close:Ljava/lang/String;

    invoke-static {v10, v5, v9}, Lcom/jrm/localmm/util/Tools;->setPlaySettingOpt(ILjava/lang/String;I)V

    goto :goto_0
.end method
