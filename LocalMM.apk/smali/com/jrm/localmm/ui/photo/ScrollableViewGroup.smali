.class public Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;
.super Landroid/view/ViewGroup;
.source "ScrollableViewGroup.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/jrm/localmm/ui/photo/ScrollableViewGroup$OnCurrentViewChangedListener;
    }
.end annotation


# instance fields
.field private mAllowLongPress:Z

.field private mCurrentScreen:I

.field private mDefaultScreen:I

.field private mFirstLayout:Z

.field private mLastMotionX:F

.field private mLastMotionY:F

.field private mNextScreen:I

.field private mOnCurrentViewChangedListener:Lcom/jrm/localmm/ui/photo/ScrollableViewGroup$OnCurrentViewChangedListener;

.field private mPaintFlag:I

.field private mScroller:Landroid/widget/Scroller;

.field private mTouchSlop:I

.field private mTouchState:I

.field private mVelocityTracker:Landroid/view/VelocityTracker;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mNextScreen:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mFirstLayout:Z

    const/4 v0, 0x0

    iput v0, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mPaintFlag:I

    invoke-direct {p0}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->initViewGroup()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mNextScreen:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mFirstLayout:Z

    const/4 v0, 0x0

    iput v0, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mPaintFlag:I

    invoke-direct {p0}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->initViewGroup()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mNextScreen:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mFirstLayout:Z

    const/4 v0, 0x0

    iput v0, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mPaintFlag:I

    invoke-direct {p0}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->initViewGroup()V

    return-void
.end method

.method private initViewGroup()V
    .locals 3

    new-instance v1, Landroid/widget/Scroller;

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mScroller:Landroid/widget/Scroller;

    iget v1, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mDefaultScreen:I

    iput v1, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mCurrentScreen:I

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v1

    iput v1, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mTouchSlop:I

    return-void
.end method

.method private snapToDestination()V
    .locals 6

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->getScrollX()I

    move-result v4

    shr-int/lit8 v5, v0, 0x1

    add-int v1, v4, v5

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->getChildCount()I

    move-result v2

    if-gez v1, :cond_0

    const/4 v3, -0x1

    :goto_0
    invoke-virtual {p0, v3}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->snapToScreen(I)V

    return-void

    :cond_0
    mul-int v4, v0, v2

    if-le v1, v4, :cond_1

    move v3, v2

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->getScrollX()I

    move-result v4

    div-int/lit8 v5, v0, 0x2

    add-int/2addr v4, v5

    div-int v3, v4, v0

    goto :goto_0
.end method


# virtual methods
.method clearChildrenCache()V
    .locals 4

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->getChildCount()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    invoke-virtual {p0, v1}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    instance-of v3, v2, Landroid/view/ViewGroup;

    if-eqz v3, :cond_0

    check-cast v2, Landroid/view/ViewGroup;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->setAlwaysDrawnWithCacheEnabled(Z)V

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public computeScroll()V
    .locals 9

    const/4 v8, 0x0

    const/4 v7, -0x1

    iget-object v5, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v5}, Landroid/widget/Scroller;->computeScrollOffset()Z

    move-result v5

    if-eqz v5, :cond_4

    iget-object v5, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v5}, Landroid/widget/Scroller;->getCurrX()I

    move-result v0

    iget-object v5, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v5}, Landroid/widget/Scroller;->getCurrY()I

    move-result v1

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->getScrollX()I

    move-result v3

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->getScrollY()I

    move-result v4

    if-ne v0, v3, :cond_0

    if-eq v1, v4, :cond_3

    :cond_0
    iget-object v5, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v5}, Landroid/widget/Scroller;->getCurrX()I

    move-result v5

    iget-object v6, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v6}, Landroid/widget/Scroller;->getCurrY()I

    move-result v6

    invoke-virtual {p0, v5, v6}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->scrollTo(II)V

    :cond_1
    :goto_0
    iget-object v5, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mOnCurrentViewChangedListener:Lcom/jrm/localmm/ui/photo/ScrollableViewGroup$OnCurrentViewChangedListener;

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mOnCurrentViewChangedListener:Lcom/jrm/localmm/ui/photo/ScrollableViewGroup$OnCurrentViewChangedListener;

    iget v6, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mCurrentScreen:I

    invoke-interface {v5, p0, v6}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup$OnCurrentViewChangedListener;->onCurrentViewChanged(Landroid/view/View;I)V

    :cond_2
    return-void

    :cond_3
    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->invalidate()V

    goto :goto_0

    :cond_4
    iget v5, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mNextScreen:I

    if-eq v5, v7, :cond_1

    iget v5, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mNextScreen:I

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->getChildCount()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v5

    invoke-static {v8, v5}, Ljava/lang/Math;->max(II)I

    move-result v5

    iput v5, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mCurrentScreen:I

    iput v7, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mNextScreen:I

    iput v8, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mPaintFlag:I

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->clearChildrenCache()V

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->getScrollX()I

    move-result v3

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->getScrollY()I

    move-result v4

    iget v5, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mCurrentScreen:I

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->getWidth()I

    move-result v6

    mul-int v2, v5, v6

    if-eq v3, v2, :cond_1

    invoke-virtual {p0, v2, v4}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->scrollTo(II)V

    goto :goto_0
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 12
    .param p1    # Landroid/graphics/Canvas;

    const/4 v9, 0x0

    const/4 v8, 0x1

    iget v10, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mTouchState:I

    if-eq v10, v8, :cond_1

    iget v10, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mNextScreen:I

    const/4 v11, -0x1

    if-ne v10, v11, :cond_1

    move v3, v8

    :goto_0
    if-eqz v3, :cond_2

    iget v8, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mCurrentScreen:I

    invoke-virtual {p0, v8}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->getDrawingTime()J

    move-result-wide v9

    invoke-virtual {p0, p1, v8, v9, v10}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    :cond_0
    :goto_1
    return-void

    :cond_1
    move v3, v9

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->getDrawingTime()J

    move-result-wide v1

    iget v10, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mNextScreen:I

    if-ltz v10, :cond_6

    iget v10, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mNextScreen:I

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->getChildCount()I

    move-result v11

    if-ge v10, v11, :cond_6

    iget v10, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mCurrentScreen:I

    iget v11, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mNextScreen:I

    sub-int/2addr v10, v11

    invoke-static {v10}, Ljava/lang/Math;->abs(I)I

    move-result v10

    if-eq v10, v8, :cond_3

    iget v8, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mPaintFlag:I

    if-eqz v8, :cond_6

    :cond_3
    iget v8, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mCurrentScreen:I

    invoke-virtual {p0, v8}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    iget v8, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mNextScreen:I

    invoke-virtual {p0, v8}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    invoke-virtual {p0, p1, v6, v1, v2}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    iget v8, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mPaintFlag:I

    if-nez v8, :cond_4

    invoke-virtual {p0, p1, v7, v1, v2}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    goto :goto_1

    :cond_4
    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    iget v8, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mPaintFlag:I

    if-gez v8, :cond_5

    invoke-virtual {v7}, Landroid/view/View;->getDrawingCache()Landroid/graphics/Bitmap;

    move-result-object v8

    invoke-virtual {v7}, Landroid/view/View;->getWidth()I

    move-result v9

    neg-int v9, v9

    int-to-float v9, v9

    invoke-virtual {v7}, Landroid/view/View;->getTop()I

    move-result v10

    int-to-float v10, v10

    invoke-virtual {p1, v8, v9, v10, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_1

    :cond_5
    invoke-virtual {v7}, Landroid/view/View;->getDrawingCache()Landroid/graphics/Bitmap;

    move-result-object v8

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->getWidth()I

    move-result v9

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->getChildCount()I

    move-result v10

    mul-int/2addr v9, v10

    int-to-float v9, v9

    invoke-virtual {v7}, Landroid/view/View;->getTop()I

    move-result v10

    int-to-float v10, v10

    invoke-virtual {p1, v8, v9, v10, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_1

    :cond_6
    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->getChildCount()I

    move-result v0

    const/4 v4, 0x0

    :goto_2
    if-ge v4, v0, :cond_7

    invoke-virtual {p0, v4}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    invoke-virtual {p0, p1, v8, v1, v2}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    :cond_7
    iget v8, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mPaintFlag:I

    if-eqz v8, :cond_0

    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    iget v8, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mPaintFlag:I

    if-gez v8, :cond_8

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->getChildCount()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-virtual {p0, v8}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    invoke-virtual {v7}, Landroid/view/View;->getDrawingCache()Landroid/graphics/Bitmap;

    move-result-object v8

    invoke-virtual {v7}, Landroid/view/View;->getWidth()I

    move-result v9

    neg-int v9, v9

    int-to-float v9, v9

    invoke-virtual {v7}, Landroid/view/View;->getTop()I

    move-result v10

    int-to-float v10, v10

    invoke-virtual {p1, v8, v9, v10, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_1

    :cond_8
    invoke-virtual {p0, v9}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    invoke-virtual {v7}, Landroid/view/View;->getDrawingCache()Landroid/graphics/Bitmap;

    move-result-object v8

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->getWidth()I

    move-result v9

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->getChildCount()I

    move-result v10

    mul-int/2addr v9, v10

    int-to-float v9, v9

    invoke-virtual {v7}, Landroid/view/View;->getTop()I

    move-result v10

    int-to-float v10, v10

    invoke-virtual {p1, v8, v9, v10, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_1
.end method

.method public dispatchUnhandledMove(Landroid/view/View;I)Z
    .locals 3
    .param p1    # Landroid/view/View;
    .param p2    # I

    const/4 v0, 0x1

    const/16 v1, 0x11

    if-ne p2, v1, :cond_0

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->getCurrentView()I

    move-result v1

    if-lez v1, :cond_1

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->getCurrentView()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v1}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->snapToScreen(I)V

    :goto_0
    return v0

    :cond_0
    const/16 v1, 0x42

    if-ne p2, v1, :cond_1

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->getCurrentView()I

    move-result v1

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->getChildCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ge v1, v2, :cond_1

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->getCurrentView()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p0, v1}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->snapToScreen(I)V

    goto :goto_0

    :cond_1
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->dispatchUnhandledMove(Landroid/view/View;I)Z

    move-result v0

    goto :goto_0
.end method

.method enableChildrenCache()V
    .locals 5

    const/4 v4, 0x1

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->getChildCount()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    invoke-virtual {p0, v1}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    instance-of v3, v2, Landroid/view/ViewGroup;

    if-eqz v3, :cond_0

    check-cast v2, Landroid/view/ViewGroup;

    invoke-virtual {v2, v4}, Landroid/view/ViewGroup;->setAlwaysDrawnWithCacheEnabled(Z)V

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public getCurrentView()I
    .locals 1

    iget v0, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mCurrentScreen:I

    return v0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 12
    .param p1    # Landroid/view/MotionEvent;

    const/4 v11, 0x1

    const/4 v10, 0x0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v9, 0x2

    if-ne v0, v9, :cond_1

    iget v9, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mTouchState:I

    if-eqz v9, :cond_1

    :cond_0
    :goto_0
    return v11

    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    packed-switch v0, :pswitch_data_0

    :cond_2
    :goto_1
    iget v9, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mTouchState:I

    if-nez v9, :cond_0

    move v11, v10

    goto :goto_0

    :pswitch_0
    iget v9, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mLastMotionX:F

    sub-float v9, v3, v9

    invoke-static {v9}, Ljava/lang/Math;->abs(F)F

    move-result v9

    float-to-int v4, v9

    iget v9, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mLastMotionY:F

    sub-float v9, v6, v9

    invoke-static {v9}, Ljava/lang/Math;->abs(F)F

    move-result v9

    float-to-int v7, v9

    iget v2, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mTouchSlop:I

    if-le v4, v2, :cond_5

    move v5, v11

    :goto_2
    if-le v7, v2, :cond_6

    move v8, v11

    :goto_3
    if-nez v5, :cond_3

    if-eqz v8, :cond_2

    :cond_3
    if-eqz v5, :cond_4

    iput v11, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mTouchState:I

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->enableChildrenCache()V

    :cond_4
    iget-boolean v9, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mAllowLongPress:Z

    if-eqz v9, :cond_2

    iput-boolean v10, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mAllowLongPress:Z

    iget v9, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mCurrentScreen:I

    invoke-virtual {p0, v9}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->cancelLongPress()V

    goto :goto_1

    :cond_5
    move v5, v10

    goto :goto_2

    :cond_6
    move v8, v10

    goto :goto_3

    :pswitch_1
    iput v3, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mLastMotionX:F

    iput v6, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mLastMotionY:F

    iput-boolean v11, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mAllowLongPress:Z

    iget-object v9, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v9}, Landroid/widget/Scroller;->isFinished()Z

    move-result v9

    if-eqz v9, :cond_7

    move v9, v10

    :goto_4
    iput v9, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mTouchState:I

    goto :goto_1

    :cond_7
    move v9, v11

    goto :goto_4

    :pswitch_2
    iput v10, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mTouchState:I

    iput-boolean v10, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mAllowLongPress:Z

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method protected onLayout(ZIIII)V
    .locals 8
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->getChildCount()I

    move-result v3

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v3, :cond_1

    invoke-virtual {p0, v4}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v5

    const/16 v6, 0x8

    if-eq v5, v6, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    const/4 v5, 0x0

    add-int v6, v1, v2

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v7

    invoke-virtual {v0, v1, v5, v6, v7}, Landroid/view/View;->layout(IIII)V

    add-int/2addr v1, v2

    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method protected onMeasure(II)V
    .locals 5
    .param p1    # I
    .param p2    # I

    const/4 v4, 0x0

    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->onMeasure(II)V

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->getChildCount()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    invoke-virtual {p0, v1}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Landroid/view/View;->measure(II)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    iget-boolean v3, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mFirstLayout:Z

    if-eqz v3, :cond_1

    iget v3, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mCurrentScreen:I

    mul-int/2addr v3, v2

    invoke-virtual {p0, v3, v4}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->scrollTo(II)V

    iput-boolean v4, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mFirstLayout:Z

    :cond_1
    return-void
.end method

.method protected onRequestFocusInDescendants(ILandroid/graphics/Rect;)Z
    .locals 3
    .param p1    # I
    .param p2    # Landroid/graphics/Rect;

    iget v1, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mNextScreen:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    iget v0, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mNextScreen:I

    :goto_0
    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Landroid/view/View;->requestFocus(ILandroid/graphics/Rect;)Z

    const/4 v1, 0x0

    return v1

    :cond_0
    iget v0, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mCurrentScreen:I

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 10
    .param p1    # Landroid/view/MotionEvent;

    const/4 v9, 0x1

    const/4 v8, 0x0

    iget-object v6, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-nez v6, :cond_0

    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v6

    iput-object v6, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mVelocityTracker:Landroid/view/VelocityTracker;

    :cond_0
    iget-object v6, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v6, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    packed-switch v0, :pswitch_data_0

    :cond_1
    :goto_0
    return v9

    :pswitch_0
    iget-object v6, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v6}, Landroid/widget/Scroller;->isFinished()Z

    move-result v6

    if-nez v6, :cond_2

    iget-object v6, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v6}, Landroid/widget/Scroller;->abortAnimation()V

    :cond_2
    iput v5, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mLastMotionX:F

    goto :goto_0

    :pswitch_1
    iget v6, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mTouchState:I

    if-ne v6, v9, :cond_1

    iget v6, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mLastMotionX:F

    sub-float/2addr v6, v5

    float-to-int v2, v6

    iput v5, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mLastMotionX:F

    if-gez v2, :cond_4

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->getScrollX()I

    move-result v6

    if-gtz v6, :cond_3

    const/4 v6, -0x1

    iput v6, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mPaintFlag:I

    :cond_3
    invoke-virtual {p0, v2, v8}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->scrollBy(II)V

    goto :goto_0

    :cond_4
    if-lez v2, :cond_1

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->getChildCount()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-virtual {p0, v6}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/View;->getRight()I

    move-result v6

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->getScrollX()I

    move-result v7

    sub-int/2addr v6, v7

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->getWidth()I

    move-result v7

    sub-int v1, v6, v7

    if-gtz v1, :cond_5

    iput v9, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mPaintFlag:I

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->getWidth()I

    move-result v6

    shl-int/lit8 v6, v6, 0x1

    add-int/2addr v1, v6

    :cond_5
    if-lez v1, :cond_1

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v6

    invoke-virtual {p0, v6, v8}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->scrollBy(II)V

    goto :goto_0

    :pswitch_2
    iget v6, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mTouchState:I

    if-ne v6, v9, :cond_6

    iget-object v3, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v3}, Landroid/view/VelocityTracker;->getXVelocity()F

    move-result v6

    float-to-int v4, v6

    const/16 v6, 0x3e8

    if-le v4, v6, :cond_7

    iget v6, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mCurrentScreen:I

    if-lez v6, :cond_7

    iget v6, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mCurrentScreen:I

    add-int/lit8 v6, v6, -0x1

    invoke-virtual {p0, v6}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->snapToScreen(I)V

    :goto_1
    iget-object v6, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-eqz v6, :cond_6

    iget-object v6, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v6}, Landroid/view/VelocityTracker;->recycle()V

    const/4 v6, 0x0

    iput-object v6, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mVelocityTracker:Landroid/view/VelocityTracker;

    :cond_6
    iput v8, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mTouchState:I

    goto/16 :goto_0

    :cond_7
    const/16 v6, -0x3e8

    if-ge v4, v6, :cond_8

    iget v6, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mCurrentScreen:I

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->getChildCount()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    if-ge v6, v7, :cond_8

    iget v6, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mCurrentScreen:I

    add-int/lit8 v6, v6, 0x1

    invoke-virtual {p0, v6}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->snapToScreen(I)V

    goto :goto_1

    :cond_8
    invoke-direct {p0}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->snapToDestination()V

    goto :goto_1

    :pswitch_3
    iput v8, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mTouchState:I

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method public requestChildRectangleOnScreen(Landroid/view/View;Landroid/graphics/Rect;Z)Z
    .locals 2
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/graphics/Rect;
    .param p3    # Z

    invoke-virtual {p0, p1}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v0

    iget v1, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mCurrentScreen:I

    if-ne v0, v1, :cond_0

    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v1}, Landroid/widget/Scroller;->isFinished()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->snapToScreen(I)V

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setCurrentView(I)V
    .locals 4
    .param p1    # I

    const/4 v3, 0x0

    const-string v0, "qqqq"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "here:view "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mCurrentScreen:I

    iget v0, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mCurrentScreen:I

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->getWidth()I

    move-result v1

    mul-int/2addr v0, v1

    invoke-virtual {p0, v0, v3}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->scrollTo(II)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mOnCurrentViewChangedListener:Lcom/jrm/localmm/ui/photo/ScrollableViewGroup$OnCurrentViewChangedListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mOnCurrentViewChangedListener:Lcom/jrm/localmm/ui/photo/ScrollableViewGroup$OnCurrentViewChangedListener;

    iget v1, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mCurrentScreen:I

    invoke-interface {v0, p0, v1}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup$OnCurrentViewChangedListener;->onCurrentViewChanged(Landroid/view/View;I)V

    :cond_0
    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->invalidate()V

    return-void
.end method

.method public snapToScreen(I)V
    .locals 11
    .param p1    # I

    const/4 v6, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->enableChildrenCache()V

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->getChildCount()I

    move-result v0

    add-int/lit8 v10, v0, -0x1

    move v9, p1

    if-gez p1, :cond_2

    move p1, v10

    const/4 v0, -0x1

    iput v0, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mPaintFlag:I

    :goto_1
    iget v0, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mCurrentScreen:I

    if-eq p1, v0, :cond_4

    :goto_2
    iput p1, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mNextScreen:I

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->getFocusedChild()Landroid/view/View;

    move-result-object v7

    if-eqz v7, :cond_1

    if-eqz v6, :cond_1

    iget v0, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mCurrentScreen:I

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    if-ne v7, v0, :cond_1

    invoke-virtual {v7}, Landroid/view/View;->clearFocus()V

    :cond_1
    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->getWidth()I

    move-result v0

    mul-int v8, v9, v0

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->getScrollX()I

    move-result v0

    sub-int v3, v8, v0

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->getScrollX()I

    move-result v1

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v4

    mul-int/lit8 v5, v4, 0x2

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/widget/Scroller;->startScroll(IIIII)V

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->invalidate()V

    goto :goto_0

    :cond_2
    if-le p1, v10, :cond_3

    const/4 p1, 0x0

    iput v6, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mPaintFlag:I

    goto :goto_1

    :cond_3
    iput v2, p0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->mPaintFlag:I

    goto :goto_1

    :cond_4
    move v6, v2

    goto :goto_2
.end method
