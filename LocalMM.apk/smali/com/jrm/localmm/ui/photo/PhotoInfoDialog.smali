.class public Lcom/jrm/localmm/ui/photo/PhotoInfoDialog;
.super Landroid/app/Dialog;
.source "PhotoInfoDialog.java"


# instance fields
.field private mDuration:Landroid/widget/TextView;

.field private mFileFormat:Landroid/widget/TextView;

.field private mFileName:Landroid/widget/TextView;

.field private mFileSize:Landroid/widget/TextView;

.field private mFormat:Ljava/text/SimpleDateFormat;

.field private mPhotoFile:Lcom/jrm/localmm/business/data/BaseData;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/jrm/localmm/business/data/BaseData;)V
    .locals 1
    .param p1    # Landroid/app/Activity;
    .param p2    # Lcom/jrm/localmm/business/data/BaseData;

    invoke-direct {p0, p1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    new-instance v0, Ljava/text/SimpleDateFormat;

    invoke-direct {v0}, Ljava/text/SimpleDateFormat;-><init>()V

    iput-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoInfoDialog;->mFormat:Ljava/text/SimpleDateFormat;

    iput-object p2, p0, Lcom/jrm/localmm/ui/photo/PhotoInfoDialog;->mPhotoFile:Lcom/jrm/localmm/business/data/BaseData;

    return-void
.end method

.method private findViews()V
    .locals 1

    const v0, 0x7f08003e    # com.jrm.localmm.R.id.file_name

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/photo/PhotoInfoDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoInfoDialog;->mFileName:Landroid/widget/TextView;

    const v0, 0x7f080067    # com.jrm.localmm.R.id.file_zise

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/photo/PhotoInfoDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoInfoDialog;->mFileSize:Landroid/widget/TextView;

    const v0, 0x7f080047    # com.jrm.localmm.R.id.file_format

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/photo/PhotoInfoDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoInfoDialog;->mFileFormat:Landroid/widget/TextView;

    const v0, 0x7f080041    # com.jrm.localmm.R.id.duration

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/photo/PhotoInfoDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoInfoDialog;->mDuration:Landroid/widget/TextView;

    return-void
.end method

.method private updatePhotoInfo()V
    .locals 5

    iget-object v3, p0, Lcom/jrm/localmm/ui/photo/PhotoInfoDialog;->mPhotoFile:Lcom/jrm/localmm/business/data/BaseData;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/jrm/localmm/ui/photo/PhotoInfoDialog;->mFileName:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/jrm/localmm/ui/photo/PhotoInfoDialog;->mPhotoFile:Lcom/jrm/localmm/business/data/BaseData;

    invoke-virtual {v4}, Lcom/jrm/localmm/business/data/BaseData;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/jrm/localmm/ui/photo/PhotoInfoDialog;->mFileSize:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/jrm/localmm/ui/photo/PhotoInfoDialog;->mPhotoFile:Lcom/jrm/localmm/business/data/BaseData;

    invoke-virtual {v4}, Lcom/jrm/localmm/business/data/BaseData;->getSize()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/jrm/localmm/ui/photo/PhotoInfoDialog;->mFileFormat:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/jrm/localmm/ui/photo/PhotoInfoDialog;->mPhotoFile:Lcom/jrm/localmm/business/data/BaseData;

    invoke-virtual {v4}, Lcom/jrm/localmm/business/data/BaseData;->getFormat()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/jrm/localmm/ui/photo/PhotoInfoDialog;->mPhotoFile:Lcom/jrm/localmm/business/data/BaseData;

    invoke-virtual {v3}, Lcom/jrm/localmm/business/data/BaseData;->getModifyTime()J

    move-result-wide v1

    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, v1, v2}, Ljava/util/Date;-><init>(J)V

    const-wide/16 v3, 0x0

    cmp-long v3, v1, v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/jrm/localmm/ui/photo/PhotoInfoDialog;->mDuration:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/jrm/localmm/ui/photo/PhotoInfoDialog;->mFormat:Ljava/text/SimpleDateFormat;

    invoke-virtual {v4, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 10
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    const-string v6, "PhotoInfoDialog"

    const-string v7, "onCreate"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const v6, 0x7f03000f    # com.jrm.localmm.R.layout.photo_info

    invoke-virtual {p0, v6}, Lcom/jrm/localmm/ui/photo/PhotoInfoDialog;->setContentView(I)V

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/PhotoInfoDialog;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Window;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v6

    invoke-interface {v6}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    const/4 v6, 0x0

    invoke-virtual {v3, v6}, Landroid/view/Window;->setTitle(Ljava/lang/CharSequence;)V

    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    invoke-virtual {v0, v2}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    iget v6, v2, Landroid/graphics/Point;->x:I

    int-to-double v6, v6

    const-wide v8, 0x3fd999999999999aL    # 0.4

    mul-double/2addr v6, v8

    double-to-int v4, v6

    iget v6, v2, Landroid/graphics/Point;->y:I

    int-to-double v6, v6

    const-wide v8, 0x3fe6666666666666L    # 0.7

    mul-double/2addr v6, v8

    double-to-int v1, v6

    invoke-virtual {v3, v4, v1}, Landroid/view/Window;->setLayout(II)V

    const/16 v6, 0x11

    invoke-virtual {v3, v6}, Landroid/view/Window;->setGravity(I)V

    invoke-virtual {v3}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v5

    const/high16 v6, 0x3f800000    # 1.0f

    iput v6, v5, Landroid/view/WindowManager$LayoutParams;->screenBrightness:F

    invoke-virtual {v3, v5}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    const v6, 0x106000d    # android.R.color.transparent

    invoke-virtual {v3, v6}, Landroid/view/Window;->setBackgroundDrawableResource(I)V

    iget-object v6, p0, Lcom/jrm/localmm/ui/photo/PhotoInfoDialog;->mFormat:Ljava/text/SimpleDateFormat;

    const-string v7, "yyyy-MM-dd  HH:mm:ss"

    invoke-virtual {v6, v7}, Ljava/text/SimpleDateFormat;->applyPattern(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/jrm/localmm/ui/photo/PhotoInfoDialog;->findViews()V

    return-void
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/PhotoInfoDialog;->cancel()V

    invoke-super {p0, p1, p2}, Landroid/app/Dialog;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onStart()V
    .locals 0

    invoke-super {p0}, Landroid/app/Dialog;->onStart()V

    invoke-direct {p0}, Lcom/jrm/localmm/ui/photo/PhotoInfoDialog;->updatePhotoInfo()V

    return-void
.end method
