.class Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$1;
.super Landroid/os/Handler;
.source "PhotoPlayerActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;


# direct methods
.method constructor <init>(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1    # Landroid/os/Message;

    const/4 v2, 0x0

    iget v1, p1, Landroid/os/Message;->what:I

    sparse-switch v1, :sswitch_data_0

    :cond_0
    :goto_0
    return-void

    :sswitch_0
    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPPTPlayer:Z
    invoke-static {v1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    const/4 v2, 0x1

    # invokes: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->moveNextOrPrevious(I)V
    invoke-static {v1, v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$100(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;I)V

    goto :goto_0

    :sswitch_1
    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    const-string v2, "Set wallpaper Success!"

    # invokes: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->showToastAtBottom(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$200(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;Ljava/lang/String;)V

    goto :goto_0

    :sswitch_2
    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;
    invoke-static {v1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$300(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    move-result-object v1

    iget-object v1, v1, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->mSurfaceView:Lcom/jrm/localmm/ui/photo/ImageSurfaceView;

    invoke-virtual {v1}, Lcom/jrm/localmm/ui/photo/ImageSurfaceView;->updateView()Z

    goto :goto_0

    :sswitch_3
    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # invokes: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->dismissProgressDialog()V
    invoke-static {v1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$400(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    const v2, 0x7f060047    # com.jrm.localmm.R.string.picture_decoding

    # invokes: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->showProgressDialog(I)V
    invoke-static {v1, v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$500(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;I)V

    goto :goto_0

    :sswitch_4
    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # invokes: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->dismissProgressDialog()V
    invoke-static {v1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$400(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)V

    goto :goto_0

    :sswitch_5
    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # invokes: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->hideController()V
    invoke-static {v1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$600(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)V

    goto :goto_0

    :sswitch_6
    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # invokes: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->playFirstPhoto()V
    invoke-static {v1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$700(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)V

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    const-string v2, "isOpen"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    # setter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->isAnimationOpened:Z
    invoke-static {v1, v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$802(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;Z)Z

    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    const-string v2, "time"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    # setter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->slideTime:I
    invoke-static {v1, v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$902(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;I)I

    goto :goto_0

    :sswitch_8
    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # invokes: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->Play4K2KPhoto()Z
    invoke-static {v1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$1000(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Z

    goto :goto_0

    :sswitch_9
    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # invokes: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->RestoreCurrentView()Z
    invoke-static {v1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$1100(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Z

    goto :goto_0

    :sswitch_a
    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    invoke-virtual {v1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->finish()V

    goto :goto_0

    :sswitch_b
    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCurrentView:I
    invoke-static {v1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$1200(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)I

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;
    invoke-static {v1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$300(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    move-result-object v1

    iget-object v1, v1, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->mScrollView:Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;

    invoke-virtual {v1, v2}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->setCurrentView(I)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$1;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # setter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCurrentView:I
    invoke-static {v1, v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$1202(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;I)I

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_7
        0xa -> :sswitch_6
        0xb -> :sswitch_0
        0xc -> :sswitch_1
        0xd -> :sswitch_2
        0xe -> :sswitch_3
        0xf -> :sswitch_4
        0x10 -> :sswitch_5
        0x16 -> :sswitch_a
        0x25 -> :sswitch_b
        0xab -> :sswitch_8
        0xba -> :sswitch_9
    .end sparse-switch
.end method
