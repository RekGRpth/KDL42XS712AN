.class Lcom/jrm/localmm/ui/photo/PhotoSettingDialog$1;
.super Ljava/lang/Object;
.source "PhotoSettingDialog.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;


# direct methods
.method constructor <init>(Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/localmm/ui/photo/PhotoSettingDialog$1;->this$0:Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 8
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # Landroid/view/KeyEvent;

    const/4 v7, 0x0

    const/4 v6, 0x1

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    :goto_0
    return v7

    :pswitch_0
    iget-object v3, p0, Lcom/jrm/localmm/ui/photo/PhotoSettingDialog$1;->this$0:Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;->playSettingListView:Landroid/widget/ListView;
    invoke-static {v3}, Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;->access$000(Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;)Landroid/widget/ListView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/ListView;->getSelectedItemPosition()I

    move-result v2

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    new-instance v1, Landroid/os/Message;

    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    const/16 v3, 0x9

    iput v3, v1, Landroid/os/Message;->what:I

    packed-switch p2, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    const-string v3, "***********"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "*********position********"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    packed-switch v2, :pswitch_data_2

    :cond_0
    :goto_1
    iget-object v3, p0, Lcom/jrm/localmm/ui/photo/PhotoSettingDialog$1;->this$0:Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;->adapter:Lcom/jrm/localmm/business/adapter/VideoPlaySettingListAdapter;
    invoke-static {v3}, Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;->access$500(Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;)Lcom/jrm/localmm/business/adapter/VideoPlaySettingListAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/jrm/localmm/business/adapter/VideoPlaySettingListAdapter;->notifyDataSetChanged()V

    const-string v3, "time"

    iget-object v4, p0, Lcom/jrm/localmm/ui/photo/PhotoSettingDialog$1;->this$0:Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;->time:I
    invoke-static {v4}, Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;->access$400(Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;)I

    move-result v4

    mul-int/lit16 v4, v4, 0x3e8

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v3, "isOpen"

    iget-object v4, p0, Lcom/jrm/localmm/ui/photo/PhotoSettingDialog$1;->this$0:Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;->isOpen:Z
    invoke-static {v4}, Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;->access$100(Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;)Z

    move-result v4

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v3, p0, Lcom/jrm/localmm/ui/photo/PhotoSettingDialog$1;->this$0:Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;->mHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;->access$600(Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;)Landroid/os/Handler;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    :pswitch_2
    iget-object v3, p0, Lcom/jrm/localmm/ui/photo/PhotoSettingDialog$1;->this$0:Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;->isOpen:Z
    invoke-static {v3}, Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;->access$100(Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/jrm/localmm/ui/photo/PhotoSettingDialog$1;->this$0:Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;

    # setter for: Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;->isOpen:Z
    invoke-static {v3, v7}, Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;->access$102(Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;Z)Z

    iget-object v3, p0, Lcom/jrm/localmm/ui/photo/PhotoSettingDialog$1;->this$0:Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;->close:Ljava/lang/String;
    invoke-static {v3}, Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;->access$200(Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v7, v3, v6}, Lcom/jrm/localmm/util/Tools;->setPlaySettingOpt(ILjava/lang/String;I)V

    goto :goto_1

    :cond_1
    iget-object v3, p0, Lcom/jrm/localmm/ui/photo/PhotoSettingDialog$1;->this$0:Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;

    # setter for: Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;->isOpen:Z
    invoke-static {v3, v6}, Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;->access$102(Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;Z)Z

    iget-object v3, p0, Lcom/jrm/localmm/ui/photo/PhotoSettingDialog$1;->this$0:Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;->open:Ljava/lang/String;
    invoke-static {v3}, Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;->access$300(Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v7, v3, v6}, Lcom/jrm/localmm/util/Tools;->setPlaySettingOpt(ILjava/lang/String;I)V

    goto :goto_1

    :pswitch_3
    iget-object v3, p0, Lcom/jrm/localmm/ui/photo/PhotoSettingDialog$1;->this$0:Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;->time:I
    invoke-static {v3}, Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;->access$400(Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;)I

    move-result v3

    if-le v3, v6, :cond_0

    iget-object v3, p0, Lcom/jrm/localmm/ui/photo/PhotoSettingDialog$1;->this$0:Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;

    # operator-- for: Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;->time:I
    invoke-static {v3}, Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;->access$410(Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;)I

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/jrm/localmm/ui/photo/PhotoSettingDialog$1;->this$0:Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;->time:I
    invoke-static {v4}, Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;->access$400(Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v6, v3, v6}, Lcom/jrm/localmm/util/Tools;->setPlaySettingOpt(ILjava/lang/String;I)V

    goto :goto_1

    :pswitch_4
    const-string v3, "***********"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "*********position********"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    packed-switch v2, :pswitch_data_3

    :cond_2
    :goto_2
    iget-object v3, p0, Lcom/jrm/localmm/ui/photo/PhotoSettingDialog$1;->this$0:Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;->adapter:Lcom/jrm/localmm/business/adapter/VideoPlaySettingListAdapter;
    invoke-static {v3}, Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;->access$500(Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;)Lcom/jrm/localmm/business/adapter/VideoPlaySettingListAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/jrm/localmm/business/adapter/VideoPlaySettingListAdapter;->notifyDataSetChanged()V

    const-string v3, "time"

    iget-object v4, p0, Lcom/jrm/localmm/ui/photo/PhotoSettingDialog$1;->this$0:Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;->time:I
    invoke-static {v4}, Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;->access$400(Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;)I

    move-result v4

    mul-int/lit16 v4, v4, 0x3e8

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v3, "isOpen"

    iget-object v4, p0, Lcom/jrm/localmm/ui/photo/PhotoSettingDialog$1;->this$0:Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;->isOpen:Z
    invoke-static {v4}, Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;->access$100(Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;)Z

    move-result v4

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v3, p0, Lcom/jrm/localmm/ui/photo/PhotoSettingDialog$1;->this$0:Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;->mHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;->access$600(Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;)Landroid/os/Handler;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    :pswitch_5
    iget-object v3, p0, Lcom/jrm/localmm/ui/photo/PhotoSettingDialog$1;->this$0:Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;->isOpen:Z
    invoke-static {v3}, Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;->access$100(Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/jrm/localmm/ui/photo/PhotoSettingDialog$1;->this$0:Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;

    # setter for: Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;->isOpen:Z
    invoke-static {v3, v7}, Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;->access$102(Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;Z)Z

    iget-object v3, p0, Lcom/jrm/localmm/ui/photo/PhotoSettingDialog$1;->this$0:Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;->close:Ljava/lang/String;
    invoke-static {v3}, Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;->access$200(Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v7, v3, v6}, Lcom/jrm/localmm/util/Tools;->setPlaySettingOpt(ILjava/lang/String;I)V

    goto :goto_2

    :cond_3
    iget-object v3, p0, Lcom/jrm/localmm/ui/photo/PhotoSettingDialog$1;->this$0:Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;

    # setter for: Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;->isOpen:Z
    invoke-static {v3, v6}, Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;->access$102(Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;Z)Z

    iget-object v3, p0, Lcom/jrm/localmm/ui/photo/PhotoSettingDialog$1;->this$0:Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;->open:Ljava/lang/String;
    invoke-static {v3}, Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;->access$300(Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v7, v3, v6}, Lcom/jrm/localmm/util/Tools;->setPlaySettingOpt(ILjava/lang/String;I)V

    goto :goto_2

    :pswitch_6
    iget-object v3, p0, Lcom/jrm/localmm/ui/photo/PhotoSettingDialog$1;->this$0:Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;->time:I
    invoke-static {v3}, Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;->access$400(Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;)I

    move-result v3

    const/16 v4, 0xa

    if-ge v3, v4, :cond_2

    iget-object v3, p0, Lcom/jrm/localmm/ui/photo/PhotoSettingDialog$1;->this$0:Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;

    # operator++ for: Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;->time:I
    invoke-static {v3}, Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;->access$408(Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;)I

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/jrm/localmm/ui/photo/PhotoSettingDialog$1;->this$0:Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;->time:I
    invoke-static {v4}, Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;->access$400(Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v6, v3, v6}, Lcom/jrm/localmm/util/Tools;->setPlaySettingOpt(ILjava/lang/String;I)V

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x15
        :pswitch_1
        :pswitch_4
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method
