.class Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$4;
.super Ljava/lang/Object;
.source "PhotoPlayerActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->playFirstPhoto()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;


# direct methods
.method constructor <init>(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$4;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$4;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoFileList:Ljava/util/List;
    invoke-static {v1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$1600(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$4;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCurrentPosition:I
    invoke-static {v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$1500(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)I

    move-result v2

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jrm/localmm/business/data/BaseData;

    invoke-virtual {v1}, Lcom/jrm/localmm/business/data/BaseData;->getPath()Ljava/lang/String;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "gif"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$4;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # invokes: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->decodeGif(Ljava/lang/String;)V
    invoke-static {v1, v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$1700(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$4;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # invokes: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->initBitmap()Z
    invoke-static {v1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$1800(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$4;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    new-instance v2, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$4$1;

    invoke-direct {v2, p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$4$1;-><init>(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$4;)V

    invoke-virtual {v1, v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method
