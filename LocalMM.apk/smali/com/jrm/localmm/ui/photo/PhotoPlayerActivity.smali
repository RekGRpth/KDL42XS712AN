.class public Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;
.super Landroid/app/Activity;
.source "PhotoPlayerActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$DiskChangeReceiver;,
        Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;
    }
.end annotation


# static fields
.field protected static is4K2KMode:Z

.field private static mThread:Ljava/lang/Thread;

.field private static options:Landroid/graphics/BitmapFactory$Options;

.field protected static panel4k2kmode:Z


# instance fields
.field private animationArray:[I

.field private animationNum:I

.field private fristShowPicture:Z

.field private is:Ljava/io/InputStream;

.field private isAnimationOpened:Z

.field protected isDefaultPhoto:Z

.field private isMPO:Z

.field private isOnPause:Z

.field private isShowMsg:Z

.field private mBitmapCache:Lcom/jrm/localmm/ui/photo/RotateBitmap;

.field private mCanResponse:Z

.field private mCanSetWallpaper:Z

.field private mCurrentPosition:I

.field private mCurrentView:I

.field private mDiskChangeReceiver:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$DiskChangeReceiver;

.field private mFileInputStream:Ljava/io/FileInputStream;

.field private mHandler:Landroid/os/Handler;

.field private mIsControllerShow:Z

.field private mIsPlaying:Z

.field mNetDisconnectReceiver:Landroid/content/BroadcastReceiver;

.field private mPPTPlayer:Z

.field private mPhotoFileList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/jrm/localmm/business/data/BaseData;",
            ">;"
        }
    .end annotation
.end field

.field private mPhotoInfoDialog:Lcom/jrm/localmm/ui/photo/PhotoInfoDialog;

.field private mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

.field private mPlayControllerLayout:Landroid/widget/LinearLayout;

.field private mPreCurrentView:I

.field private mProgressDialog:Landroid/app/ProgressDialog;

.field private mSourceChangeReceiver:Landroid/content/BroadcastReceiver;

.field private mSourceFrom:I

.field private mState:I

.field private mWindowHeight:I

.field private mWindowWidth:I

.field private mZoomTimes:I

.field private slideTime:I

.field private str4K2KPhotoPath:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x0

    new-instance v0, Ljava/lang/Thread;

    invoke-direct {v0}, Ljava/lang/Thread;-><init>()V

    sput-object v0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mThread:Ljava/lang/Thread;

    sput-boolean v1, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->is4K2KMode:Z

    sput-boolean v1, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->panel4k2kmode:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    new-instance v0, Lcom/jrm/localmm/ui/photo/RotateBitmap;

    invoke-direct {v0, v3}, Lcom/jrm/localmm/ui/photo/RotateBitmap;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mBitmapCache:Lcom/jrm/localmm/ui/photo/RotateBitmap;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoFileList:Ljava/util/List;

    iput-object v3, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mFileInputStream:Ljava/io/FileInputStream;

    iput-object v3, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->is:Ljava/io/InputStream;

    iput-boolean v2, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mIsControllerShow:Z

    iput-boolean v2, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCanSetWallpaper:Z

    iput-boolean v2, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCanResponse:Z

    iput-boolean v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPPTPlayer:Z

    iput-boolean v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mIsPlaying:Z

    const/4 v0, 0x2

    iput v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mState:I

    iput v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCurrentPosition:I

    iput v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mZoomTimes:I

    iput v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mWindowWidth:I

    iput v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mWindowHeight:I

    iput v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCurrentView:I

    iput v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPreCurrentView:I

    iput-boolean v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->isShowMsg:Z

    iput-boolean v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->isOnPause:Z

    iput-boolean v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->isMPO:Z

    iput-boolean v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->isAnimationOpened:Z

    const/16 v0, 0xbb8

    iput v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->slideTime:I

    iput-boolean v2, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->fristShowPicture:Z

    const/16 v0, 0xc

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->animationArray:[I

    iput-boolean v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->isDefaultPhoto:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->str4K2KPhotoPath:Ljava/lang/String;

    new-instance v0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$1;

    invoke-direct {v0, p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$1;-><init>(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$11;

    invoke-direct {v0, p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$11;-><init>(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mNetDisconnectReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$12;

    invoke-direct {v0, p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$12;-><init>(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mSourceChangeReceiver:Landroid/content/BroadcastReceiver;

    return-void

    :array_0
    .array-data 4
        0x7f04000b    # com.jrm.localmm.R.anim.photo_scale_translate_left_bottom
        0x7f04000c    # com.jrm.localmm.R.anim.photo_scale_translate_left_top
        0x7f04000e    # com.jrm.localmm.R.anim.photo_scale_translate_right_top
        0x7f04000d    # com.jrm.localmm.R.anim.photo_scale_translate_right_bottom
        0x7f040007    # com.jrm.localmm.R.anim.photo_push_down_in
        0x7f040008    # com.jrm.localmm.R.anim.photo_push_left_in
        0x7f040009    # com.jrm.localmm.R.anim.photo_push_right_in
        0x7f04000a    # com.jrm.localmm.R.anim.photo_push_up_in
        0x7f040010    # com.jrm.localmm.R.anim.photo_zoom_enter
        0x7f040006    # com.jrm.localmm.R.anim.photo_hyperspace_out
        0x7f040011    # com.jrm.localmm.R.anim.photo_zoom_exit
        0x7f04000f    # com.jrm.localmm.R.anim.photo_wave_scale
    .end array-data
.end method

.method private Play4K2KPhoto()Z
    .locals 5

    const/4 v4, 0x2

    const/4 v0, 0x1

    const/4 v1, 0x0

    const-string v2, "PhotoPlayerActivity"

    const-string v3, "******Play4K2KPhoto******"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    iget-object v2, v2, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->mSurfaceView4K2K:Lcom/jrm/localmm/ui/photo/SurfaceView4K2K;

    invoke-virtual {v2}, Lcom/jrm/localmm/ui/photo/SurfaceView4K2K;->destroyDrawingCache()V

    iget-object v2, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    iget-object v2, v2, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->mSurfaceView4K2K:Lcom/jrm/localmm/ui/photo/SurfaceView4K2K;

    iget-object v3, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->str4K2KPhotoPath:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/jrm/localmm/ui/photo/SurfaceView4K2K;->setImagePath(Ljava/lang/String;)Z

    move-result v2

    if-ne v2, v0, :cond_0

    const-string v2, "PhotoPlayerActivity"

    const-string v3, "******Draw 4k2k photo img******"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, ""

    iput-object v2, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->str4K2KPhotoPath:Ljava/lang/String;

    iget v2, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCurrentView:I

    iput v2, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPreCurrentView:I

    iput v4, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCurrentView:I

    iget-object v2, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    iget-object v2, v2, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->mScrollView:Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;

    invoke-virtual {v2, v4}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->setCurrentView(I)V

    iget-object v2, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    iget-object v2, v2, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->mSurfaceView4K2K:Lcom/jrm/localmm/ui/photo/SurfaceView4K2K;

    invoke-virtual {v2}, Lcom/jrm/localmm/ui/photo/SurfaceView4K2K;->drawImage()V

    iget-object v2, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    iget-object v2, v2, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->mSurfaceView4K2K:Lcom/jrm/localmm/ui/photo/SurfaceView4K2K;

    invoke-virtual {v2, v1}, Lcom/jrm/localmm/ui/photo/SurfaceView4K2K;->setVisibility(I)V

    invoke-direct {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->dismissProgressDialog()V

    const-string v1, "PhotoPlayerActivity"

    const-string v2, "******play 4k2k photo finished******"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method private PlayProcess()V
    .locals 4

    const/16 v3, 0xb

    const/4 v2, 0x1

    iget v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCurrentView:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCurrentView:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCurrentView:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    const v0, 0x7f06003f    # com.jrm.localmm.R.string.photo_3D_toast

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->showToastAtBottom(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-boolean v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPPTPlayer:Z

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->stopPPTPlayer()V

    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    iget-boolean v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPPTPlayer:Z

    invoke-virtual {v0, v2, v1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->setPhotoPlaySelect(ZZ)V

    goto :goto_0

    :cond_2
    iget v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCurrentPosition:I

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoFileList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_3

    iput-boolean v2, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPPTPlayer:Z

    invoke-direct {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->hideControlDelay()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_1

    :cond_3
    iget v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCurrentPosition:I

    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoFileList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ne v0, v1, :cond_1

    iput-boolean v2, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPPTPlayer:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCurrentPosition:I

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_1
.end method

.method private RestoreCurrentView()Z
    .locals 2

    iget v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCurrentView:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPreCurrentView:I

    iput v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCurrentView:I

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->mScrollView:Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;

    iget v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCurrentView:I

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->setCurrentView(I)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Z
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    iget-boolean v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPPTPlayer:Z

    return v0
.end method

.method static synthetic access$100(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;I)V
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->moveNextOrPrevious(I)V

    return-void
.end method

.method static synthetic access$1000(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Z
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    invoke-direct {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->Play4K2KPhoto()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1100(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Z
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    invoke-direct {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->RestoreCurrentView()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1200(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)I
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    iget v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCurrentView:I

    return v0
.end method

.method static synthetic access$1202(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;I)I
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;
    .param p1    # I

    iput p1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCurrentView:I

    return p1
.end method

.method static synthetic access$1500(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)I
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    iget v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCurrentPosition:I

    return v0
.end method

.method static synthetic access$1600(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoFileList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->decodeGif(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1800(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Z
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    invoke-direct {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->initBitmap()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1900(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Lcom/jrm/localmm/ui/photo/RotateBitmap;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mBitmapCache:Lcom/jrm/localmm/ui/photo/RotateBitmap;

    return-object v0
.end method

.method static synthetic access$200(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->showToastAtBottom(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$2000(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Z
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    iget-boolean v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCanSetWallpaper:Z

    return v0
.end method

.method static synthetic access$2002(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;Z)Z
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCanSetWallpaper:Z

    return p1
.end method

.method static synthetic access$2100(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Z
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    iget-boolean v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mIsControllerShow:Z

    return v0
.end method

.method static synthetic access$2300(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)V
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    invoke-direct {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->showController()V

    return-void
.end method

.method static synthetic access$2400(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)V
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    invoke-direct {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->hideControlDelay()V

    return-void
.end method

.method static synthetic access$2500(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Z
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    iget-boolean v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCanResponse:Z

    return v0
.end method

.method static synthetic access$2600(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Z
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    iget-boolean v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mIsPlaying:Z

    return v0
.end method

.method static synthetic access$2700(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)V
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    invoke-direct {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->cancleDelayHide()V

    return-void
.end method

.method static synthetic access$2800(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)V
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    invoke-direct {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->stopPPTPlayer()V

    return-void
.end method

.method static synthetic access$2902(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;I)I
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;
    .param p1    # I

    iput p1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mState:I

    return p1
.end method

.method static synthetic access$300(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    return-object v0
.end method

.method static synthetic access$3000(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)V
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    invoke-direct {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->PlayProcess()V

    return-void
.end method

.method static synthetic access$3100(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)V
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    invoke-direct {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->zoomIn()V

    return-void
.end method

.method static synthetic access$3200(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)V
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    invoke-direct {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->zoomOut()V

    return-void
.end method

.method static synthetic access$3300(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)V
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    invoke-direct {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->rotateImageLeft()V

    return-void
.end method

.method static synthetic access$3400(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)V
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    invoke-direct {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->rotateImageRight()V

    return-void
.end method

.method static synthetic access$3500(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)V
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    invoke-direct {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->showPhotoInfo()V

    return-void
.end method

.method static synthetic access$3600(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)V
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    invoke-direct {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->setPhoto2Wallpaper()V

    return-void
.end method

.method static synthetic access$3700(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)V
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    invoke-direct {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->setPhotoThreeD()V

    return-void
.end method

.method static synthetic access$3800(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)V
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    invoke-direct {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->sbowPhotoSetting()V

    return-void
.end method

.method static synthetic access$3900(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)I
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    iget v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mSourceFrom:I

    return v0
.end method

.method static synthetic access$400(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)V
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    invoke-direct {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->dismissProgressDialog()V

    return-void
.end method

.method static synthetic access$4000(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)V
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    invoke-direct {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->startPhotoAnimation()V

    return-void
.end method

.method static synthetic access$4100(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Ljava/io/FileInputStream;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mFileInputStream:Ljava/io/FileInputStream;

    return-object v0
.end method

.method static synthetic access$4200(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;Ljava/io/Closeable;)V
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;
    .param p1    # Ljava/io/Closeable;

    invoke-direct {p0, p1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->closeSilently(Ljava/io/Closeable;)V

    return-void
.end method

.method static synthetic access$500(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;I)V
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->showProgressDialog(I)V

    return-void
.end method

.method static synthetic access$600(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)V
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    invoke-direct {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->hideController()V

    return-void
.end method

.method static synthetic access$700(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)V
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    invoke-direct {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->playFirstPhoto()V

    return-void
.end method

.method static synthetic access$800(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Z
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    iget-boolean v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->isAnimationOpened:Z

    return v0
.end method

.method static synthetic access$802(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;Z)Z
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->isAnimationOpened:Z

    return p1
.end method

.method static synthetic access$900(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)I
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    iget v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->slideTime:I

    return v0
.end method

.method static synthetic access$902(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;I)I
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;
    .param p1    # I

    iput p1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->slideTime:I

    return p1
.end method

.method private cancleDelayHide()V
    .locals 2

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPlayControllerLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->requestFocus()Z

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPlayControllerLayout:Landroid/widget/LinearLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    return-void
.end method

.method private closeSilently(Ljava/io/Closeable;)V
    .locals 1
    .param p1    # Ljava/io/Closeable;

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    :try_start_0
    invoke-interface {p1}, Ljava/io/Closeable;->close()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private computeSampleSizeLarger(DD)I
    .locals 6
    .param p1    # D
    .param p3    # D

    const-wide/high16 v2, 0x409e000000000000L    # 1920.0

    div-double v2, p1, v2

    const-wide v4, 0x4090e00000000000L    # 1080.0

    div-double v4, p3, v4

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(DD)D

    move-result-wide v0

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    cmpg-double v2, v0, v2

    if-gtz v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const-wide/high16 v2, 0x4010000000000000L    # 4.0

    cmpg-double v2, v0, v2

    if-gez v2, :cond_1

    const/4 v2, 0x2

    goto :goto_0

    :cond_1
    const-wide/high16 v2, 0x4020000000000000L    # 8.0

    cmpg-double v2, v0, v2

    if-gez v2, :cond_2

    const/4 v2, 0x4

    goto :goto_0

    :cond_2
    const/16 v2, 0x8

    goto :goto_0
.end method

.method private decodeBitmapFailed(I)V
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCanResponse:Z

    new-instance v0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$13;

    invoke-direct {v0, p0, p1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$13;-><init>(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;I)V

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method private decodeBitmapFromLocal(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 8

    const/4 v1, 0x0

    const/4 v7, 0x1

    invoke-static {p1}, Lcom/jrm/localmm/util/Tools;->isFileExist(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iput-boolean v7, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCanResponse:Z

    :goto_0
    return-object v1

    :cond_0
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    sput-object v0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->options:Landroid/graphics/BitmapFactory$Options;

    :try_start_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mFileInputStream:Ljava/io/FileInputStream;

    invoke-direct {p0, v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->closeSilently(Ljava/io/Closeable;)V

    new-instance v0, Ljava/io/FileInputStream;

    invoke-direct {v0, p1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mFileInputStream:Ljava/io/FileInputStream;

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mFileInputStream:Ljava/io/FileInputStream;

    invoke-virtual {v0}, Ljava/io/FileInputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mFileInputStream:Ljava/io/FileInputStream;

    invoke-direct {p0, v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->closeSilently(Ljava/io/Closeable;)V

    const v0, 0x7f060048    # com.jrm.localmm.R.string.picture_decode_failed

    invoke-direct {p0, v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->decodeBitmapFailed(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mFileInputStream:Ljava/io/FileInputStream;

    invoke-direct {p0, v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->closeSilently(Ljava/io/Closeable;)V

    goto :goto_0

    :cond_1
    :try_start_1
    sget-object v2, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->options:Landroid/graphics/BitmapFactory$Options;

    const/4 v3, 0x0

    iput-boolean v3, v2, Landroid/graphics/BitmapFactory$Options;->inPurgeable:Z

    sget-object v2, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->options:Landroid/graphics/BitmapFactory$Options;

    const/4 v3, 0x1

    iput-boolean v3, v2, Landroid/graphics/BitmapFactory$Options;->inInputShareable:Z

    sget-object v2, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->options:Landroid/graphics/BitmapFactory$Options;

    const/4 v3, 0x1

    iput-boolean v3, v2, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    const-string v2, "."

    invoke-virtual {p1, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {p1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "mpo"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    :goto_1
    const/4 v2, 0x0

    sget-object v3, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->options:Landroid/graphics/BitmapFactory$Options;

    invoke-static {v0, v2, v3}, Landroid/graphics/BitmapFactory;->decodeFileDescriptor(Ljava/io/FileDescriptor;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    const-string v2, "PhotoPlayerActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "options "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->options:Landroid/graphics/BitmapFactory$Options;

    iget v4, v4, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->options:Landroid/graphics/BitmapFactory$Options;

    iget v4, v4, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x0

    sput-boolean v2, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->is4K2KMode:Z

    sget-object v2, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->options:Landroid/graphics/BitmapFactory$Options;

    iget v2, v2, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    const/16 v3, 0x870

    if-lt v2, v3, :cond_3

    sget-object v2, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->options:Landroid/graphics/BitmapFactory$Options;

    iget v2, v2, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    const/16 v3, 0xf00

    if-lt v2, v3, :cond_3

    iput-object p1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->str4K2KPhotoPath:Ljava/lang/String;

    const/4 v0, 0x1

    sput-boolean v0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->is4K2KMode:Z

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mFileInputStream:Ljava/io/FileInputStream;

    invoke-direct {p0, v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->closeSilently(Ljava/io/Closeable;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCanResponse:Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mFileInputStream:Ljava/io/FileInputStream;

    invoke-direct {p0, v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->closeSilently(Ljava/io/Closeable;)V

    goto/16 :goto_0

    :cond_2
    const/4 v2, 0x1

    :try_start_2
    iput-boolean v2, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->isMPO:Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    :catch_0
    move-exception v0

    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    sget-boolean v0, Lcom/jrm/localmm/util/Constants;->isExit:Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mFileInputStream:Ljava/io/FileInputStream;

    invoke-direct {p0, v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->closeSilently(Ljava/io/Closeable;)V

    goto/16 :goto_0

    :cond_3
    :try_start_4
    sget-object v2, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->options:Landroid/graphics/BitmapFactory$Options;

    invoke-direct {p0, v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->isLargerThanLimit(Landroid/graphics/BitmapFactory$Options;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mFileInputStream:Ljava/io/FileInputStream;

    invoke-direct {p0, v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->closeSilently(Ljava/io/Closeable;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCanResponse:Z

    const-string v0, "PhotoPlayerActivity"

    const-string v2, "**show default photo**"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->setDefaultPhoto()Landroid/graphics/Bitmap;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v1

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mFileInputStream:Ljava/io/FileInputStream;

    invoke-direct {p0, v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->closeSilently(Ljava/io/Closeable;)V

    goto/16 :goto_0

    :cond_4
    :try_start_5
    sget-object v2, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->options:Landroid/graphics/BitmapFactory$Options;

    invoke-direct {p0, v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->isErrorPix(Landroid/graphics/BitmapFactory$Options;)Z

    move-result v2

    if-eqz v2, :cond_6

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mFileInputStream:Ljava/io/FileInputStream;

    invoke-direct {p0, v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->closeSilently(Ljava/io/Closeable;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCanResponse:Z

    iget-boolean v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->isOnPause:Z

    if-nez v0, :cond_5

    const v0, 0x7f060048    # com.jrm.localmm.R.string.picture_decode_failed

    invoke-direct {p0, v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->decodeBitmapFailed(I)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :cond_5
    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mFileInputStream:Ljava/io/FileInputStream;

    invoke-direct {p0, v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->closeSilently(Ljava/io/Closeable;)V

    goto/16 :goto_0

    :cond_6
    :try_start_6
    sget-object v2, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->options:Landroid/graphics/BitmapFactory$Options;

    sget-object v3, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->options:Landroid/graphics/BitmapFactory$Options;

    iget v3, v3, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    int-to-double v3, v3

    sget-object v5, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->options:Landroid/graphics/BitmapFactory$Options;

    iget v5, v5, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    int-to-double v5, v5

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->computeSampleSizeLarger(DD)I

    move-result v3

    iput v3, v2, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    const-string v2, "PhotoPlayerActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "options.inSampleSize : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->options:Landroid/graphics/BitmapFactory$Options;

    iget v4, v4, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v2, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->options:Landroid/graphics/BitmapFactory$Options;

    const/4 v3, 0x0

    iput-boolean v3, v2, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    if-eqz v0, :cond_c

    const/4 v2, 0x0

    sget-object v3, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->options:Landroid/graphics/BitmapFactory$Options;

    invoke-static {v0, v2, v3}, Landroid/graphics/BitmapFactory;->decodeFileDescriptor(Ljava/io/FileDescriptor;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_8

    iget v2, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mWindowWidth:I

    const/4 v3, 0x1

    invoke-direct {p0, v0, v2, v3}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->resizeDownIfTooBig(Landroid/graphics/Bitmap;IZ)Landroid/graphics/Bitmap;
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move-result-object v0

    :cond_7
    :goto_2
    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mFileInputStream:Ljava/io/FileInputStream;

    invoke-direct {p0, v1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->closeSilently(Ljava/io/Closeable;)V

    :goto_3
    iput-boolean v7, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCanResponse:Z

    sget-object v1, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->options:Landroid/graphics/BitmapFactory$Options;

    if-eqz v1, :cond_b

    sget-object v1, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->options:Landroid/graphics/BitmapFactory$Options;

    iget-object v1, v1, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    if-eq v1, v2, :cond_b

    invoke-direct {p0, v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->ensureGLCompatibleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v1

    goto/16 :goto_0

    :cond_8
    :try_start_7
    iget-boolean v2, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->isOnPause:Z

    if-nez v2, :cond_7

    const v2, 0x7f060046    # com.jrm.localmm.R.string.picture_can_not_decode

    invoke-direct {p0, v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->decodeBitmapFailed(I)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mFileInputStream:Ljava/io/FileInputStream;

    invoke-direct {p0, v1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->closeSilently(Ljava/io/Closeable;)V

    throw v0

    :cond_9
    :try_start_8
    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mFileInputStream:Ljava/io/FileInputStream;

    invoke-direct {p0, v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->closeSilently(Ljava/io/Closeable;)V

    new-instance v0, Ljava/io/FileInputStream;

    invoke-direct {v0, p1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mFileInputStream:Ljava/io/FileInputStream;

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mFileInputStream:Ljava/io/FileInputStream;

    const/4 v1, 0x0

    sget-object v2, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->options:Landroid/graphics/BitmapFactory$Options;

    invoke-static {v0, v1, v2}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    if-nez v0, :cond_a

    const v0, 0x7f060046    # com.jrm.localmm.R.string.picture_can_not_decode

    invoke-direct {p0, v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->decodeBitmapFailed(I)V

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->setDefaultPhoto()Landroid/graphics/Bitmap;
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    move-result-object v1

    :try_start_9
    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mFileInputStream:Ljava/io/FileInputStream;

    invoke-direct {p0, v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->closeSilently(Ljava/io/Closeable;)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mFileInputStream:Ljava/io/FileInputStream;

    invoke-direct {p0, v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->closeSilently(Ljava/io/Closeable;)V

    goto/16 :goto_0

    :cond_a
    :try_start_a
    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mFileInputStream:Ljava/io/FileInputStream;

    invoke-direct {p0, v1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->closeSilently(Ljava/io/Closeable;)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mFileInputStream:Ljava/io/FileInputStream;

    invoke-direct {p0, v1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->closeSilently(Ljava/io/Closeable;)V

    goto :goto_3

    :catch_1
    move-exception v0

    :try_start_b
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    const v0, 0x7f060046    # com.jrm.localmm.R.string.picture_can_not_decode

    invoke-direct {p0, v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->decodeBitmapFailed(I)V

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->setDefaultPhoto()Landroid/graphics/Bitmap;
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    move-result-object v1

    :try_start_c
    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mFileInputStream:Ljava/io/FileInputStream;

    invoke-direct {p0, v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->closeSilently(Ljava/io/Closeable;)V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mFileInputStream:Ljava/io/FileInputStream;

    invoke-direct {p0, v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->closeSilently(Ljava/io/Closeable;)V

    goto/16 :goto_0

    :catchall_1
    move-exception v0

    :try_start_d
    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mFileInputStream:Ljava/io/FileInputStream;

    invoke-direct {p0, v1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->closeSilently(Ljava/io/Closeable;)V

    throw v0
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    :cond_b
    move-object v1, v0

    goto/16 :goto_0

    :cond_c
    move-object v0, v1

    goto/16 :goto_2
.end method

.method private decodeBitmapFromNet(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 9

    const v8, 0x7f060048    # com.jrm.localmm.R.string.picture_decode_failed

    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :try_start_0
    invoke-direct {p0, v1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->closeSilently(Ljava/io/Closeable;)V

    invoke-direct {p0, v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->closeSilently(Ljava/io/Closeable;)V

    new-instance v1, Ljava/net/URL;

    invoke-direct {v1, p1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/net/URL;->openStream()Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_6
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    if-nez v3, :cond_1

    const v1, 0x7f060048    # com.jrm.localmm.R.string.picture_decode_failed

    :try_start_1
    invoke-direct {p0, v1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->decodeBitmapFailed(I)V
    :try_end_1
    .catch Ljava/net/MalformedURLException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    invoke-direct {p0, v3}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->closeSilently(Ljava/io/Closeable;)V

    invoke-direct {p0, v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->closeSilently(Ljava/io/Closeable;)V

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    :try_start_2
    new-instance v2, Lcom/jrm/localmm/business/photo/MyInputStream;

    invoke-direct {v2, v3, p1}, Lcom/jrm/localmm/business/photo/MyInputStream;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/net/MalformedURLException; {:try_start_2 .. :try_end_2} :catch_7
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :try_start_3
    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    sput-object v1, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->options:Landroid/graphics/BitmapFactory$Options;

    sget-object v1, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->options:Landroid/graphics/BitmapFactory$Options;

    sget-object v4, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    iput-object v4, v1, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    sget-object v1, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->options:Landroid/graphics/BitmapFactory$Options;

    const/4 v4, 0x1

    iput-boolean v4, v1, Landroid/graphics/BitmapFactory$Options;->inPurgeable:Z

    sget-object v1, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->options:Landroid/graphics/BitmapFactory$Options;

    const/4 v4, 0x1

    iput-boolean v4, v1, Landroid/graphics/BitmapFactory$Options;->inInputShareable:Z

    sget-object v1, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->options:Landroid/graphics/BitmapFactory$Options;

    const/4 v4, 0x1

    iput-boolean v4, v1, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    const-string v1, "."

    invoke-virtual {p1, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    const-string v4, "mpo"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    :goto_1
    const/4 v1, 0x0

    sget-object v4, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->options:Landroid/graphics/BitmapFactory$Options;

    invoke-static {v2, v1, v4}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    const-string v1, "PhotoPlayerActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "from dlna, options "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->options:Landroid/graphics/BitmapFactory$Options;

    iget v5, v5, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->options:Landroid/graphics/BitmapFactory$Options;

    iget v5, v5, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-boolean v1, Lcom/jrm/localmm/util/Constants;->isExit:Z
    :try_end_3
    .catch Ljava/net/MalformedURLException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    if-eqz v1, :cond_3

    invoke-direct {p0, v3}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->closeSilently(Ljava/io/Closeable;)V

    invoke-direct {p0, v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->closeSilently(Ljava/io/Closeable;)V

    goto :goto_0

    :cond_2
    const/4 v1, 0x1

    :try_start_4
    iput-boolean v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->isMPO:Z
    :try_end_4
    .catch Ljava/net/MalformedURLException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    goto :goto_1

    :catch_0
    move-exception v1

    :goto_2
    :try_start_5
    const-string v4, "PhotoPlayerActivity"

    const-string v5, "MalformedURLException in decodeBitmap"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v1}, Ljava/net/MalformedURLException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    invoke-direct {p0, v3}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->closeSilently(Ljava/io/Closeable;)V

    invoke-direct {p0, v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->closeSilently(Ljava/io/Closeable;)V

    :goto_3
    sget-boolean v1, Lcom/jrm/localmm/util/Constants;->isExit:Z

    if-nez v1, :cond_0

    invoke-direct {p0, v8}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->decodeBitmapFailed(I)V

    goto/16 :goto_0

    :cond_3
    :try_start_6
    sget-object v1, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->options:Landroid/graphics/BitmapFactory$Options;

    invoke-direct {p0, v1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->isLargerThanLimit(Landroid/graphics/BitmapFactory$Options;)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-direct {p0, v3}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->closeSilently(Ljava/io/Closeable;)V

    invoke-direct {p0, v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->closeSilently(Ljava/io/Closeable;)V

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCanResponse:Z

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->setDefaultPhoto()Landroid/graphics/Bitmap;
    :try_end_6
    .catch Ljava/net/MalformedURLException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    move-result-object v0

    invoke-direct {p0, v3}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->closeSilently(Ljava/io/Closeable;)V

    invoke-direct {p0, v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->closeSilently(Ljava/io/Closeable;)V

    goto/16 :goto_0

    :cond_4
    :try_start_7
    sget-object v1, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->options:Landroid/graphics/BitmapFactory$Options;

    invoke-direct {p0, v1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->isErrorPix(Landroid/graphics/BitmapFactory$Options;)Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-direct {p0, v3}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->closeSilently(Ljava/io/Closeable;)V

    invoke-direct {p0, v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->closeSilently(Ljava/io/Closeable;)V

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCanResponse:Z

    iget-boolean v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->isOnPause:Z

    if-nez v1, :cond_5

    const v1, 0x7f060048    # com.jrm.localmm.R.string.picture_decode_failed

    invoke-direct {p0, v1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->decodeBitmapFailed(I)V
    :try_end_7
    .catch Ljava/net/MalformedURLException; {:try_start_7 .. :try_end_7} :catch_0
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    :cond_5
    invoke-direct {p0, v3}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->closeSilently(Ljava/io/Closeable;)V

    invoke-direct {p0, v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->closeSilently(Ljava/io/Closeable;)V

    goto/16 :goto_0

    :cond_6
    :try_start_8
    sget-object v1, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->options:Landroid/graphics/BitmapFactory$Options;

    sget-object v4, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->options:Landroid/graphics/BitmapFactory$Options;

    iget v4, v4, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    int-to-double v4, v4

    sget-object v6, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->options:Landroid/graphics/BitmapFactory$Options;

    iget v6, v6, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    int-to-double v6, v6

    invoke-direct {p0, v4, v5, v6, v7}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->computeSampleSizeLarger(DD)I

    move-result v4

    iput v4, v1, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    sget-object v1, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->options:Landroid/graphics/BitmapFactory$Options;

    const/4 v4, 0x0

    iput-boolean v4, v1, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    invoke-direct {p0, v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->closeSilently(Ljava/io/Closeable;)V

    new-instance v1, Ljava/net/URL;

    invoke-direct {v1, p1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/net/URL;->openStream()Ljava/io/InputStream;
    :try_end_8
    .catch Ljava/net/MalformedURLException; {:try_start_8 .. :try_end_8} :catch_0
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    move-result-object v4

    if-nez v4, :cond_7

    const v1, 0x7f060048    # com.jrm.localmm.R.string.picture_decode_failed

    :try_start_9
    invoke-direct {p0, v1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->decodeBitmapFailed(I)V
    :try_end_9
    .catch Ljava/net/MalformedURLException; {:try_start_9 .. :try_end_9} :catch_8
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_4
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    invoke-direct {p0, v4}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->closeSilently(Ljava/io/Closeable;)V

    invoke-direct {p0, v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->closeSilently(Ljava/io/Closeable;)V

    goto/16 :goto_0

    :cond_7
    :try_start_a
    new-instance v3, Lcom/jrm/localmm/business/photo/MyInputStream;

    invoke-direct {v3, v4, p1}, Lcom/jrm/localmm/business/photo/MyInputStream;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V
    :try_end_a
    .catch Ljava/net/MalformedURLException; {:try_start_a .. :try_end_a} :catch_8
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_4
    .catchall {:try_start_a .. :try_end_a} :catchall_3

    :try_start_b
    const-string v1, "PhotoPlayerActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mIs : "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "PhotoPlayerActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "*****mIs*******"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v3}, Lcom/jrm/localmm/business/photo/MyInputStream;->markSupported()Z

    move-result v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    sget-object v2, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->options:Landroid/graphics/BitmapFactory$Options;

    invoke-static {v3, v1, v2}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v1

    if-nez v1, :cond_9

    const-string v2, "PhotoPlayerActivity"

    const-string v5, "BitmapFactory.decodeStream return null"

    invoke-static {v2, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v2, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->isOnPause:Z
    :try_end_b
    .catch Ljava/net/MalformedURLException; {:try_start_b .. :try_end_b} :catch_9
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_5
    .catchall {:try_start_b .. :try_end_b} :catchall_4

    if-eqz v2, :cond_8

    invoke-direct {p0, v4}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->closeSilently(Ljava/io/Closeable;)V

    invoke-direct {p0, v3}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->closeSilently(Ljava/io/Closeable;)V

    goto/16 :goto_0

    :cond_8
    const v2, 0x7f060048    # com.jrm.localmm.R.string.picture_decode_failed

    :try_start_c
    invoke-direct {p0, v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->decodeBitmapFailed(I)V

    :cond_9
    invoke-direct {p0, v4}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->closeSilently(Ljava/io/Closeable;)V

    invoke-direct {p0, v3}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->closeSilently(Ljava/io/Closeable;)V

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCanResponse:Z
    :try_end_c
    .catch Ljava/net/MalformedURLException; {:try_start_c .. :try_end_c} :catch_9
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_5
    .catchall {:try_start_c .. :try_end_c} :catchall_4

    invoke-direct {p0, v4}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->closeSilently(Ljava/io/Closeable;)V

    invoke-direct {p0, v3}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->closeSilently(Ljava/io/Closeable;)V

    move-object v0, v1

    goto/16 :goto_0

    :catch_1
    move-exception v1

    move-object v2, v0

    move-object v3, v0

    :goto_4
    :try_start_d
    const-string v4, "PhotoPlayerActivity"

    const-string v5, "IOException in decodeBitmap"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_2

    invoke-direct {p0, v3}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->closeSilently(Ljava/io/Closeable;)V

    invoke-direct {p0, v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->closeSilently(Ljava/io/Closeable;)V

    goto/16 :goto_3

    :catchall_0
    move-exception v1

    move-object v2, v0

    move-object v3, v0

    move-object v0, v1

    :goto_5
    invoke-direct {p0, v3}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->closeSilently(Ljava/io/Closeable;)V

    invoke-direct {p0, v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->closeSilently(Ljava/io/Closeable;)V

    throw v0

    :catchall_1
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    goto :goto_5

    :catchall_2
    move-exception v0

    goto :goto_5

    :catchall_3
    move-exception v0

    move-object v3, v4

    goto :goto_5

    :catchall_4
    move-exception v0

    move-object v2, v3

    move-object v3, v4

    goto :goto_5

    :catch_2
    move-exception v1

    move-object v2, v0

    goto :goto_4

    :catch_3
    move-exception v1

    goto :goto_4

    :catch_4
    move-exception v1

    move-object v3, v4

    goto :goto_4

    :catch_5
    move-exception v1

    move-object v2, v3

    move-object v3, v4

    goto :goto_4

    :catch_6
    move-exception v1

    move-object v2, v0

    move-object v3, v0

    goto/16 :goto_2

    :catch_7
    move-exception v1

    move-object v2, v0

    goto/16 :goto_2

    :catch_8
    move-exception v1

    move-object v3, v4

    goto/16 :goto_2

    :catch_9
    move-exception v1

    move-object v2, v3

    move-object v3, v4

    goto/16 :goto_2
.end method

.method private decodeGif(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mHandler:Landroid/os/Handler;

    const/16 v1, 0xe

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    new-instance v0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$8;

    invoke-direct {v0, p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$8;-><init>(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)V

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$9;

    invoke-direct {v1, p0, p1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$9;-><init>(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method private dismissProgressDialog()V
    .locals 2

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "PhotoPlayerActivity"

    const-string v1, "dismissProgressDialog"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    :cond_0
    return-void
.end method

.method private drapLeft()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x1

    iget v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mState:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    const/16 v0, 0xb

    iput v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mState:I

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    invoke-virtual {v0, v3}, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->setPhotoPreSelect(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    invoke-virtual {v0, v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->setPhotoSettingSelect(Z)V

    goto :goto_0

    :pswitch_1
    iput v2, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mState:I

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    iget-boolean v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPPTPlayer:Z

    invoke-virtual {v0, v3, v1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->setPhotoPlaySelect(ZZ)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    invoke-virtual {v0, v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->setPhotoPreSelect(Z)V

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x2

    iput v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mState:I

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    invoke-virtual {v0, v3}, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->setPhotoNextSelect(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    iget-boolean v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPPTPlayer:Z

    invoke-virtual {v0, v2, v1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->setPhotoPlaySelect(ZZ)V

    goto :goto_0

    :pswitch_3
    const/4 v0, 0x3

    iput v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mState:I

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    invoke-virtual {v0, v3}, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->setPhotoEnlargeSelect(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    invoke-virtual {v0, v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->setPhotoNextSelect(Z)V

    goto :goto_0

    :pswitch_4
    const/4 v0, 0x4

    iput v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mState:I

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    invoke-virtual {v0, v3}, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->setPhotoNarrowSelect(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    invoke-virtual {v0, v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->setPhotoEnlargeSelect(Z)V

    goto :goto_0

    :pswitch_5
    const/4 v0, 0x5

    iput v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mState:I

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    invoke-virtual {v0, v3}, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->setPhotoTurnLeftSelect(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    invoke-virtual {v0, v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->setPhotoNarrowSelect(Z)V

    goto :goto_0

    :pswitch_6
    const/4 v0, 0x6

    iput v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mState:I

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    invoke-virtual {v0, v3}, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->setPhotoTurnRightSelect(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    invoke-virtual {v0, v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->setPhotoTurnLeftSelect(Z)V

    goto :goto_0

    :pswitch_7
    const/4 v0, 0x7

    iput v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mState:I

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    invoke-virtual {v0, v3}, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->setPhotoInfoSelect(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    invoke-virtual {v0, v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->setPhotoTurnRightSelect(Z)V

    goto :goto_0

    :pswitch_8
    const/16 v0, 0x8

    iput v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mState:I

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    invoke-virtual {v0, v3}, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->setPhotoWallpaperSelect(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    invoke-virtual {v0, v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->setPhotoInfoSelect(Z)V

    goto/16 :goto_0

    :pswitch_9
    const/16 v0, 0x9

    iput v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mState:I

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    invoke-virtual {v0, v3}, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->setPhoto3DSelect(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    invoke-virtual {v0, v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->setPhotoWallpaperSelect(Z)V

    goto/16 :goto_0

    :pswitch_a
    const/16 v0, 0xa

    iput v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mState:I

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    invoke-virtual {v0, v3}, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->setPhotoSettingSelect(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    invoke-virtual {v0, v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->setPhoto3DSelect(Z)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method

.method private drapRight()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x1

    iget v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mState:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    const/4 v0, 0x2

    iput v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mState:I

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    invoke-virtual {v0, v3}, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->setPhotoPreSelect(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    iget-boolean v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPPTPlayer:Z

    invoke-virtual {v0, v2, v1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->setPhotoPlaySelect(ZZ)V

    goto :goto_0

    :pswitch_1
    const/4 v0, 0x3

    iput v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mState:I

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    iget-boolean v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPPTPlayer:Z

    invoke-virtual {v0, v3, v1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->setPhotoPlaySelect(ZZ)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    invoke-virtual {v0, v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->setPhotoNextSelect(Z)V

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x4

    iput v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mState:I

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    invoke-virtual {v0, v3}, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->setPhotoNextSelect(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    invoke-virtual {v0, v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->setPhotoEnlargeSelect(Z)V

    goto :goto_0

    :pswitch_3
    const/4 v0, 0x5

    iput v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mState:I

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    invoke-virtual {v0, v3}, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->setPhotoEnlargeSelect(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    invoke-virtual {v0, v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->setPhotoNarrowSelect(Z)V

    goto :goto_0

    :pswitch_4
    const/4 v0, 0x6

    iput v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mState:I

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    invoke-virtual {v0, v3}, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->setPhotoNarrowSelect(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    invoke-virtual {v0, v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->setPhotoTurnLeftSelect(Z)V

    goto :goto_0

    :pswitch_5
    const/4 v0, 0x7

    iput v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mState:I

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    invoke-virtual {v0, v3}, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->setPhotoTurnLeftSelect(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    invoke-virtual {v0, v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->setPhotoTurnRightSelect(Z)V

    goto :goto_0

    :pswitch_6
    const/16 v0, 0x8

    iput v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mState:I

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    invoke-virtual {v0, v3}, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->setPhotoTurnRightSelect(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    invoke-virtual {v0, v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->setPhotoInfoSelect(Z)V

    goto :goto_0

    :pswitch_7
    const/16 v0, 0x9

    iput v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mState:I

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    invoke-virtual {v0, v3}, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->setPhotoInfoSelect(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    invoke-virtual {v0, v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->setPhotoWallpaperSelect(Z)V

    goto :goto_0

    :pswitch_8
    const/16 v0, 0xa

    iput v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mState:I

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    invoke-virtual {v0, v3}, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->setPhotoWallpaperSelect(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    invoke-virtual {v0, v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->setPhoto3DSelect(Z)V

    goto/16 :goto_0

    :pswitch_9
    const/16 v0, 0xb

    iput v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mState:I

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    invoke-virtual {v0, v3}, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->setPhoto3DSelect(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    invoke-virtual {v0, v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->setPhotoSettingSelect(Z)V

    goto/16 :goto_0

    :pswitch_a
    iput v2, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mState:I

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    invoke-virtual {v0, v3}, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->setPhotoSettingSelect(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    invoke-virtual {v0, v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->setPhotoPreSelect(Z)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method

.method private ensureGLCompatibleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 6

    const/4 v1, 0x1

    const/4 v2, 0x0

    const-string v3, "PhotoPlayerActivity"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "***is**"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    if-nez p1, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v0

    if-eqz v0, :cond_3

    :cond_0
    :goto_2
    return-object p1

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    :cond_3
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-virtual {p1, v0, v2}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V

    invoke-static {}, Ljava/lang/System;->gc()V

    const-string v4, "PhotoPlayerActivity"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "***bitmap**"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    if-nez p1, :cond_4

    move v0, v1

    :goto_3
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, " "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    if-nez v3, :cond_5

    :goto_4
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object p1, v3

    goto :goto_2

    :cond_4
    move v0, v2

    goto :goto_3

    :cond_5
    move v1, v2

    goto :goto_4
.end method

.method private findView()V
    .locals 1

    const v0, 0x7f080068    # com.jrm.localmm.R.id.photo_suspension_layout

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPlayControllerLayout:Landroid/widget/LinearLayout;

    new-instance v0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    invoke-direct {v0, p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;-><init>(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->findViews()V

    invoke-direct {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->hideControlDelay()V

    return-void
.end method

.method private getConfig(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap$Config;
    .locals 1
    .param p1    # Landroid/graphics/Bitmap;

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    :cond_0
    return-object v0
.end method

.method private hideControlDelay()V
    .locals 4

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x10

    const-wide/16 v2, 0xbb8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    return-void
.end method

.method private hideController()V
    .locals 2

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setSystemUiVisibility(I)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPlayControllerLayout:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPlayControllerLayout:Landroid/widget/LinearLayout;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mIsControllerShow:Z

    :goto_0
    return-void

    :cond_0
    const-string v0, "PhotoPlayerActivity"

    const-string v1, "playControlLayout is null ptr!!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private initBitmap()Z
    .locals 7

    const/16 v6, 0xe

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mBitmapCache:Lcom/jrm/localmm/ui/photo/RotateBitmap;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mBitmapCache:Lcom/jrm/localmm/ui/photo/RotateBitmap;

    invoke-virtual {v2}, Lcom/jrm/localmm/ui/photo/RotateBitmap;->recycle()V

    invoke-static {}, Ljava/lang/System;->gc()V

    :cond_0
    iget-object v2, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoFileList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    iget v2, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCurrentPosition:I

    if-ltz v2, :cond_9

    iget v2, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCurrentPosition:I

    if-ge v2, v0, :cond_9

    iget-boolean v2, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->isShowMsg:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_1
    iput-boolean v4, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->isShowMsg:Z

    iput-boolean v3, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->isMPO:Z

    iget-object v2, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoFileList:Ljava/util/List;

    iget v5, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCurrentPosition:I

    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/jrm/localmm/business/data/BaseData;

    invoke-virtual {v2}, Lcom/jrm/localmm/business/data/BaseData;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->decodeBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v1

    sget-boolean v2, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->is4K2KMode:Z

    if-ne v2, v4, :cond_3

    if-nez v1, :cond_3

    const-string v2, "PhotoPlayerActivity"

    const-string v5, "******Draw 4k2k photo******"

    invoke-static {v2, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    iget-object v2, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mHandler:Landroid/os/Handler;

    const/16 v5, 0xab

    invoke-virtual {v2, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    sget-boolean v2, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->panel4k2kmode:Z

    if-nez v2, :cond_2

    sput-boolean v4, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->panel4k2kmode:Z

    sget-object v2, Lcom/mstar/android/MDisplay$PanelMode;->E_PANELMODE_4K2K_15HZ:Lcom/mstar/android/MDisplay$PanelMode;

    invoke-static {v2}, Lcom/mstar/android/MDisplay;->setPanelMode(Lcom/mstar/android/MDisplay$PanelMode;)V

    :cond_2
    move v2, v3

    :goto_0
    return v2

    :cond_3
    sget-boolean v2, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->panel4k2kmode:Z

    if-ne v2, v4, :cond_4

    sput-boolean v3, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->panel4k2kmode:Z

    sget-object v2, Lcom/mstar/android/MDisplay$PanelMode;->E_PANELMODE_NORMAL:Lcom/mstar/android/MDisplay$PanelMode;

    invoke-static {v2}, Lcom/mstar/android/MDisplay;->setPanelMode(Lcom/mstar/android/MDisplay$PanelMode;)V

    iget-object v2, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mHandler:Landroid/os/Handler;

    const/16 v5, 0xba

    invoke-virtual {v2, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_4
    const-string v2, "PhotoPlayerActivity"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "******tempBitmap******"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    sget-boolean v2, Lcom/jrm/localmm/util/Constants;->isExit:Z

    if-nez v2, :cond_5

    if-eqz p0, :cond_8

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->isFinishing()Z

    move-result v2

    if-nez v2, :cond_8

    :cond_5
    iget v2, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCurrentView:I

    if-nez v2, :cond_7

    const-string v2, "PhotoPlayerActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "******mBitmapCache.setBitmap******"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mBitmapCache:Lcom/jrm/localmm/ui/photo/RotateBitmap;

    invoke-virtual {v2, v1}, Lcom/jrm/localmm/ui/photo/RotateBitmap;->setBitmap(Landroid/graphics/Bitmap;)V

    :cond_6
    :goto_1
    iget-object v2, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mHandler:Landroid/os/Handler;

    const/16 v3, 0xf

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    const/4 v1, 0x0

    invoke-static {}, Ljava/lang/System;->gc()V

    move v2, v4

    goto :goto_0

    :cond_7
    iget v2, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCurrentView:I

    if-ne v2, v4, :cond_6

    iget-object v2, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    iget-object v2, v2, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->mSurfaceView:Lcom/jrm/localmm/ui/photo/ImageSurfaceView;

    invoke-virtual {v2, v1}, Lcom/jrm/localmm/ui/photo/ImageSurfaceView;->setBitmap(Landroid/graphics/Bitmap;)V

    iget-object v2, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mHandler:Landroid/os/Handler;

    const/16 v3, 0xd

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_1

    :cond_8
    const-string v2, "PhotoPlayerActivity"

    const-string v4, "********isFinishing*********"

    invoke-static {v2, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    invoke-static {}, Ljava/lang/System;->gc()V

    move v2, v3

    goto/16 :goto_0

    :cond_9
    move v2, v3

    goto/16 :goto_0
.end method

.method private isErrorPix(Landroid/graphics/BitmapFactory$Options;)Z
    .locals 1
    .param p1    # Landroid/graphics/BitmapFactory$Options;

    iget v0, p1, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    if-lez v0, :cond_0

    iget v0, p1, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    if-gtz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isLargerThanLimit(Landroid/graphics/BitmapFactory$Options;)Z
    .locals 4
    .param p1    # Landroid/graphics/BitmapFactory$Options;

    iget v2, p1, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iget v3, p1, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    mul-int/2addr v2, v3

    int-to-long v0, v2

    const-wide/32 v2, 0x7e90000

    cmp-long v2, v0, v2

    if-gtz v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x1

    goto :goto_0
.end method

.method private moveNextOrPrevious(I)V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->mGifView:Lcom/jrm/localmm/ui/photo/GifView;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/photo/GifView;->setStop()V

    sget-object v0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCurrentView:I

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCurrentView:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    iget v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCurrentView:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const v0, 0x7f06003f    # com.jrm.localmm.R.string.photo_3D_toast

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->showToastAtBottom(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    iget-boolean v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCanResponse:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCurrentPosition:I

    add-int/2addr v0, p1

    const/4 v1, -0x1

    if-gt v0, v1, :cond_3

    const v0, 0x7f060041    # com.jrm.localmm.R.string.first_photo

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->showToastAtCenter(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoFileList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lt v0, v1, :cond_4

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoFileList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    const v0, 0x7f060042    # com.jrm.localmm.R.string.last_photo

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->showToastAtCenter(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->stopPPTPlayer()V

    goto :goto_0

    :cond_4
    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->mImageView:Lcom/jrm/localmm/ui/photo/ImageViewTouch;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/jrm/localmm/ui/photo/ImageViewTouch;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->mImageView:Lcom/jrm/localmm/ui/photo/ImageViewTouch;

    invoke-virtual {v1}, Lcom/jrm/localmm/ui/photo/ImageViewTouch;->clear()V

    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->mImageView:Lcom/jrm/localmm/ui/photo/ImageViewTouch;

    iget v1, v1, Lcom/jrm/localmm/ui/photo/ImageViewTouch;->mRotateCounter:I

    if-nez v1, :cond_5

    iget v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mZoomTimes:I

    if-eqz v1, :cond_6

    :cond_5
    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->mImageView:Lcom/jrm/localmm/ui/photo/ImageViewTouch;

    invoke-virtual {v1}, Lcom/jrm/localmm/ui/photo/ImageViewTouch;->resetRotateCounter()V

    :cond_6
    iput v3, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mZoomTimes:I

    iput v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCurrentPosition:I

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoFileList:Ljava/util/List;

    iget v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCurrentPosition:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jrm/localmm/business/data/BaseData;

    invoke-virtual {v0}, Lcom/jrm/localmm/business/data/BaseData;->getPath()Ljava/lang/String;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "gif"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_7

    iget v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCurrentView:I

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->mScrollView:Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;

    invoke-virtual {v1, v3}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->setCurrentView(I)V

    iput v3, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCurrentView:I

    :cond_7
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$10;

    invoke-direct {v2, p0, v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$10;-><init>(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;Ljava/lang/String;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    sput-object v1, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mThread:Ljava/lang/Thread;

    sget-object v0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    iget-object v1, v0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->photo_name:Landroid/widget/TextView;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const v2, 0x7f060044    # com.jrm.localmm.R.string.current_pic

    invoke-virtual {p0, v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoFileList:Ljava/util/List;

    iget v3, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCurrentPosition:I

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jrm/localmm/business/data/BaseData;

    invoke-virtual {v0}, Lcom/jrm/localmm/business/data/BaseData;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0
.end method

.method private playFirstPhoto()V
    .locals 4

    sget-boolean v0, Lcom/jrm/localmm/util/Constants;->isExit:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCurrentPosition:I

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCurrentPosition:I

    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoFileList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lt v0, v1, :cond_1

    :cond_0
    const-string v0, "PhotoPlayerActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "******playFirstPhoto*****"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/jrm/localmm/util/Constants;->isExit:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_1
    sget-object v0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "PhotoPlayerActivity"

    const-string v1, "****playFirstPhoto****mThread.isAlive()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mSourceFrom:I

    const/16 v1, 0x10

    if-eq v0, v1, :cond_3

    sget-object v0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->options:Landroid/graphics/BitmapFactory$Options;

    invoke-virtual {v0}, Landroid/graphics/BitmapFactory$Options;->requestCancelDecode()V

    iget-boolean v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->isShowMsg:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mHandler:Landroid/os/Handler;

    const/16 v1, 0xe

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->isShowMsg:Z

    :cond_2
    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mHandler:Landroid/os/Handler;

    const/16 v1, 0xa

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    :cond_3
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$4;

    invoke-direct {v1, p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$4;-><init>(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    sput-object v0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mThread:Ljava/lang/Thread;

    sget-object v0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method

.method private registerListeners()V
    .locals 4

    const v3, 0x7f06004c    # com.jrm.localmm.R.string.photo_GIF_toast

    const v2, 0x7f06003f    # com.jrm.localmm.R.string.photo_3D_toast

    const/4 v1, 0x3

    iget v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mState:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-boolean v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPPTPlayer:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->stopPPTPlayer()V

    :cond_0
    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->moveNextOrPrevious(I)V

    goto :goto_0

    :pswitch_1
    invoke-direct {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->PlayProcess()V

    goto :goto_0

    :pswitch_2
    iget-boolean v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPPTPlayer:Z

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->stopPPTPlayer()V

    :cond_1
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->moveNextOrPrevious(I)V

    goto :goto_0

    :pswitch_3
    iget v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCurrentView:I

    if-nez v0, :cond_2

    invoke-direct {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->zoomIn()V

    goto :goto_0

    :cond_2
    iget v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCurrentView:I

    if-ne v0, v1, :cond_3

    invoke-virtual {p0, v3}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->showToastAtBottom(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    invoke-virtual {p0, v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->showToastAtBottom(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_4
    iget v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCurrentView:I

    if-nez v0, :cond_4

    invoke-direct {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->zoomOut()V

    goto :goto_0

    :cond_4
    iget v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCurrentView:I

    if-ne v0, v1, :cond_5

    invoke-virtual {p0, v3}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->showToastAtBottom(Ljava/lang/String;)V

    goto :goto_0

    :cond_5
    invoke-virtual {p0, v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->showToastAtBottom(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_5
    iget v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCurrentView:I

    if-nez v0, :cond_6

    invoke-direct {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->rotateImageLeft()V

    goto :goto_0

    :cond_6
    iget v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCurrentView:I

    if-ne v0, v1, :cond_7

    invoke-virtual {p0, v3}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->showToastAtBottom(Ljava/lang/String;)V

    goto :goto_0

    :cond_7
    invoke-virtual {p0, v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->showToastAtBottom(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_6
    iget v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCurrentView:I

    if-nez v0, :cond_8

    invoke-direct {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->rotateImageRight()V

    goto :goto_0

    :cond_8
    iget v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCurrentView:I

    if-ne v0, v1, :cond_9

    invoke-virtual {p0, v3}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->showToastAtBottom(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_9
    invoke-virtual {p0, v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->showToastAtBottom(Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_7
    iget-boolean v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCanSetWallpaper:Z

    if-eqz v0, :cond_a

    invoke-direct {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->setPhoto2Wallpaper()V

    goto/16 :goto_0

    :cond_a
    iget v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCurrentView:I

    if-ne v0, v1, :cond_b

    invoke-virtual {p0, v3}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->showToastAtBottom(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_b
    invoke-virtual {p0, v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->showToastAtBottom(Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_8
    invoke-direct {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->showPhotoInfo()V

    goto/16 :goto_0

    :pswitch_9
    invoke-direct {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->setPhotoThreeD()V

    goto/16 :goto_0

    :pswitch_a
    invoke-direct {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->sbowPhotoSetting()V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_8
        :pswitch_7
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method

.method private resizeBitmapByScale(Landroid/graphics/Bitmap;FZ)Landroid/graphics/Bitmap;
    .locals 7
    .param p1    # Landroid/graphics/Bitmap;
    .param p2    # F
    .param p3    # Z

    const/4 v6, 0x0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v5, p2

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v4

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v5, p2

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v1

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    if-ne v4, v5, :cond_0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    if-ne v1, v5, :cond_0

    :goto_0
    return-object p1

    :cond_0
    invoke-direct {p0, p1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->getConfig(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap$Config;

    move-result-object v5

    invoke-static {v4, v1, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v3

    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {v0, p2, p2}, Landroid/graphics/Canvas;->scale(FF)V

    new-instance v2, Landroid/graphics/Paint;

    const/4 v5, 0x6

    invoke-direct {v2, v5}, Landroid/graphics/Paint;-><init>(I)V

    invoke-virtual {v0, p1, v6, v6, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    if-eqz p3, :cond_1

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V

    :cond_1
    move-object p1, v3

    goto :goto_0
.end method

.method private resizeDownIfTooBig(Landroid/graphics/Bitmap;IZ)Landroid/graphics/Bitmap;
    .locals 6

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    int-to-float v2, p2

    int-to-float v3, v0

    div-float/2addr v2, v3

    int-to-float v3, p2

    int-to-float v4, v1

    div-float/2addr v3, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v2

    const-string v3, "PhotoPlayerActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "srcWidth : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " srcHeight : "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " scale : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/high16 v0, 0x3f000000    # 0.5f

    cmpl-float v0, v2, v0

    if-lez v0, :cond_0

    :goto_0
    return-object p1

    :cond_0
    invoke-direct {p0, p1, v2, p3}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->resizeBitmapByScale(Landroid/graphics/Bitmap;FZ)Landroid/graphics/Bitmap;

    move-result-object p1

    goto :goto_0
.end method

.method private rotateImageLeft()V
    .locals 2

    iget-boolean v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPPTPlayer:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->stopPPTPlayer()V

    :cond_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->mImageView:Lcom/jrm/localmm/ui/photo/ImageViewTouch;

    iget v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mZoomTimes:I

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/photo/ImageViewTouch;->rotateImageLeft(I)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mZoomTimes:I

    return-void
.end method

.method private rotateImageRight()V
    .locals 2

    iget-boolean v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPPTPlayer:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->stopPPTPlayer()V

    :cond_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->mImageView:Lcom/jrm/localmm/ui/photo/ImageViewTouch;

    iget v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mZoomTimes:I

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/photo/ImageViewTouch;->rotateImageRight(I)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mZoomTimes:I

    return-void
.end method

.method private sbowPhotoSetting()V
    .locals 4

    new-instance v0, Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;

    iget v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->slideTime:I

    iget-boolean v2, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->isAnimationOpened:Z

    iget-object v3, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;-><init>(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;IZLandroid/os/Handler;)V

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/photo/PhotoSettingDialog;->show()V

    return-void
.end method

.method private setPhoto2Wallpaper()V
    .locals 3

    const v2, 0x7f060040    # com.jrm.localmm.R.string.photo_set_wallpaper

    iget-boolean v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPPTPlayer:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->stopPPTPlayer()V

    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x1010355    # android.R.attr.alertDialogIcon

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {p0, v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {p0, v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/high16 v1, 0x1040000    # android.R.string.cancel

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x104000a    # android.R.string.ok

    invoke-virtual {p0, v1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$7;

    invoke-direct {v2, p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$7;-><init>(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    return-void
.end method

.method private setPhotoThreeD()V
    .locals 3

    const/4 v1, 0x1

    const/4 v2, 0x0

    sget-object v0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->isMPO:Z

    if-nez v0, :cond_2

    const v0, 0x7f060049    # com.jrm.localmm.R.string.picture_can_not_set3d

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->showToastAtBottom(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    iget-boolean v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPPTPlayer:Z

    if-eqz v0, :cond_3

    invoke-direct {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->stopPPTPlayer()V

    :cond_3
    iget v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCurrentView:I

    if-nez v0, :cond_4

    invoke-direct {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->hideController()V

    iput v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCurrentView:I

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->mImageView:Lcom/jrm/localmm/ui/photo/ImageViewTouch;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/photo/ImageViewTouch;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->cleanBitmapArray()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->mImageView:Lcom/jrm/localmm/ui/photo/ImageViewTouch;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/photo/ImageViewTouch;->clear()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->mImageView:Lcom/jrm/localmm/ui/photo/ImageViewTouch;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/photo/ImageViewTouch;->resetRotateCounter()V

    iput v2, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mZoomTimes:I

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$5;

    invoke-direct {v1, p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$5;-><init>(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    sput-object v0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mThread:Ljava/lang/Thread;

    sget-object v0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->mScrollView:Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;

    iget v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCurrentView:I

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->setCurrentView(I)V

    goto :goto_0

    :cond_4
    iget v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCurrentView:I

    if-ne v0, v1, :cond_0

    iput v2, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCurrentView:I

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->mSurfaceView:Lcom/jrm/localmm/ui/photo/ImageSurfaceView;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/photo/ImageSurfaceView;->setNormal()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->mSurfaceView:Lcom/jrm/localmm/ui/photo/ImageSurfaceView;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/photo/ImageSurfaceView;->destroyDrawingCache()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->mSurfaceView:Lcom/jrm/localmm/ui/photo/ImageSurfaceView;

    iget v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mWindowWidth:I

    iget v2, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mWindowHeight:I

    invoke-virtual {v0, v1, v2}, Lcom/jrm/localmm/ui/photo/ImageSurfaceView;->cleanView(II)V

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$6;

    invoke-direct {v1, p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$6;-><init>(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    sput-object v0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mThread:Ljava/lang/Thread;

    sget-object v0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->mScrollView:Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;

    iget v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCurrentView:I

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->setCurrentView(I)V

    goto/16 :goto_0
.end method

.method private showController()V
    .locals 3

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPlayControllerLayout:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPlayControllerLayout:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPlayControllerLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->requestFocus()Z

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPlayControllerLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    iput-boolean v2, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mIsControllerShow:Z

    :goto_0
    return-void

    :cond_0
    const-string v0, "PhotoPlayerActivity"

    const-string v1, "playControlLayout is null ptr==="

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private showPhotoInfo()V
    .locals 3

    iget-boolean v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPPTPlayer:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->stopPPTPlayer()V

    :cond_0
    const-string v0, "PhotoPlayerActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mCurrentPosition : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCurrentPosition:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " size : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoFileList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCurrentPosition:I

    if-ltz v0, :cond_1

    iget v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCurrentPosition:I

    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoFileList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    new-instance v1, Lcom/jrm/localmm/ui/photo/PhotoInfoDialog;

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoFileList:Ljava/util/List;

    iget v2, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCurrentPosition:I

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jrm/localmm/business/data/BaseData;

    invoke-direct {v1, p0, v0}, Lcom/jrm/localmm/ui/photo/PhotoInfoDialog;-><init>(Landroid/app/Activity;Lcom/jrm/localmm/business/data/BaseData;)V

    iput-object v1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoInfoDialog:Lcom/jrm/localmm/ui/photo/PhotoInfoDialog;

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoInfoDialog:Lcom/jrm/localmm/ui/photo/PhotoInfoDialog;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/photo/PhotoInfoDialog;->show()V

    :cond_1
    return-void
.end method

.method private showProgressDialog(I)V
    .locals 2
    .param p1    # I

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {p0, p1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    :cond_0
    return-void
.end method

.method private showToast(Ljava/lang/String;II)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # I

    invoke-static {p0, p1, p2}, Lcom/jrm/localmm/util/ToastFactory;->getToast(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method

.method private showToastAtBottom(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    const/16 v0, 0x50

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->showToast(Ljava/lang/String;II)V

    return-void
.end method

.method private startPhotoAnimation()V
    .locals 6

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v2

    iget v4, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->animationNum:I

    add-int/lit8 v4, v4, -0x1

    int-to-double v4, v4

    mul-double/2addr v2, v4

    const-wide/16 v4, 0x0

    add-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    long-to-int v1, v2

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->animationArray:[I

    aget v3, v3, v1

    invoke-static {v2, v3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iget-object v2, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    iget-object v2, v2, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->mImageView:Lcom/jrm/localmm/ui/photo/ImageViewTouch;

    invoke-virtual {v2, v0}, Lcom/jrm/localmm/ui/photo/ImageViewTouch;->startAnimation(Landroid/view/animation/Animation;)V

    return-void
.end method

.method private stopPPTPlayer()V
    .locals 3

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPPTPlayer:Z

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mHandler:Landroid/os/Handler;

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mState:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->setPhotoPlaySelect(ZZ)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    invoke-virtual {v0, v2, v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->setPhotoPlaySelect(ZZ)V

    goto :goto_0
.end method

.method private zoomIn()V
    .locals 2

    iget-boolean v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPPTPlayer:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->stopPPTPlayer()V

    :cond_0
    iget v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mZoomTimes:I

    const/4 v1, 0x3

    if-lt v0, v1, :cond_1

    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mZoomTimes:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mZoomTimes:I

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->mImageView:Lcom/jrm/localmm/ui/photo/ImageViewTouch;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/photo/ImageViewTouch;->zoomIn()V

    goto :goto_0
.end method

.method private zoomOut()V
    .locals 2

    iget-boolean v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPPTPlayer:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->stopPPTPlayer()V

    :cond_0
    iget v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mZoomTimes:I

    const/4 v1, -0x3

    if-gt v0, v1, :cond_1

    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mZoomTimes:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mZoomTimes:I

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->mImageView:Lcom/jrm/localmm/ui/photo/ImageViewTouch;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/photo/ImageViewTouch;->zoomOut()V

    goto :goto_0
.end method


# virtual methods
.method protected cleanBitmapArray()V
    .locals 1

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mBitmapCache:Lcom/jrm/localmm/ui/photo/RotateBitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mBitmapCache:Lcom/jrm/localmm/ui/photo/RotateBitmap;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/photo/RotateBitmap;->recycle()V

    :cond_0
    return-void
.end method

.method public decodeBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 4

    const/4 v3, 0x0

    const-string v0, "PhotoPlayerActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "decodeBitmap, url : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput-boolean v3, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCanResponse:Z

    iput-boolean v3, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->isDefaultPhoto:Z

    iget v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mSourceFrom:I

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    invoke-direct {p0, p1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->decodeBitmapFromNet(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0, p1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->decodeBitmapFromLocal(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 11
    .param p1    # Landroid/os/Bundle;

    const/16 v10, 0x10

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->getWindow()Landroid/view/Window;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v8}, Landroid/view/View;->setSystemUiVisibility(I)V

    const v5, 0x7f030011    # com.jrm.localmm.R.layout.photo_show

    invoke-virtual {p0, v5}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->setContentView(I)V

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const-string v6, "com.jrm.index"

    invoke-virtual {v5, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    iput v5, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCurrentPosition:I

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const-string v6, "sourceFrom"

    invoke-virtual {v5, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    iput v5, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mSourceFrom:I

    iget v5, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mSourceFrom:I

    if-ne v5, v10, :cond_0

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const-string v6, "com.jrm.arraylist"

    invoke-virtual {v5, v6}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v5

    iput-object v5, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoFileList:Ljava/util/List;

    :goto_0
    invoke-direct {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->findView()V

    iget-object v5, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    iget-boolean v6, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPPTPlayer:Z

    invoke-virtual {v5, v8, v6}, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->setPhotoPlaySelect(ZZ)V

    iput v9, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mState:I

    iget-object v5, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    iget-object v5, v5, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->mImageView:Lcom/jrm/localmm/ui/photo/ImageViewTouch;

    invoke-virtual {v5, v8}, Lcom/jrm/localmm/ui/photo/ImageViewTouch;->setEnableTrackballScroll(Z)V

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const-string v6, "com.jrm.index"

    invoke-virtual {v5, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    iput v5, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCurrentPosition:I

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const-string v6, "sourceFrom"

    invoke-virtual {v5, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    iput v5, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mSourceFrom:I

    iget v5, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mSourceFrom:I

    if-ne v5, v10, :cond_1

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const-string v6, "com.jrm.arraylist"

    invoke-virtual {v5, v6}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v5

    iput-object v5, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoFileList:Ljava/util/List;

    :goto_1
    iget-object v5, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    iget-object v6, v5, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->photo_name:Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const v7, 0x7f060044    # com.jrm.localmm.R.string.current_pic

    invoke-virtual {p0, v7}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v5, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoFileList:Ljava/util/List;

    iget v8, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCurrentPosition:I

    invoke-interface {v5, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/jrm/localmm/business/data/BaseData;

    invoke-virtual {v5}, Lcom/jrm/localmm/business/data/BaseData;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v4

    invoke-interface {v4}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    invoke-virtual {v0, v2}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    iget v5, v2, Landroid/graphics/Point;->x:I

    iput v5, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mWindowWidth:I

    iget v5, v2, Landroid/graphics/Point;->y:I

    iput v5, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mWindowHeight:I

    new-instance v1, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;

    const/4 v5, 0x0

    invoke-direct {v1, p0, v5}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$PhotoImageViewClickListener;-><init>(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$1;)V

    iget-object v5, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    invoke-virtual {v5, v1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v3, Landroid/content/IntentFilter;

    const-string v5, "source.switch.from.storage"

    invoke-direct {v3, v5}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mSourceChangeReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v5, v3}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void

    :cond_0
    invoke-static {}, Lcom/jrm/localmm/ui/MediaContainerApplication;->getInstance()Lcom/jrm/localmm/ui/MediaContainerApplication;

    move-result-object v5

    invoke-virtual {v5, v9}, Lcom/jrm/localmm/ui/MediaContainerApplication;->getMediaData(I)Ljava/util/List;

    move-result-object v5

    iput-object v5, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoFileList:Ljava/util/List;

    goto/16 :goto_0

    :cond_1
    invoke-static {}, Lcom/jrm/localmm/ui/MediaContainerApplication;->getInstance()Lcom/jrm/localmm/ui/MediaContainerApplication;

    move-result-object v5

    invoke-virtual {v5, v9}, Lcom/jrm/localmm/ui/MediaContainerApplication;->getMediaData(I)Ljava/util/List;

    move-result-object v5

    iput-object v5, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoFileList:Ljava/util/List;

    goto :goto_1
.end method

.method protected onDestroy()V
    .locals 2

    invoke-direct {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->dismissProgressDialog()V

    invoke-direct {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->stopPPTPlayer()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->mImageView:Lcom/jrm/localmm/ui/photo/ImageViewTouch;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/photo/ImageViewTouch;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->cleanBitmapArray()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mSourceChangeReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const-string v0, "PhotoPlayerActivity"

    const-string v1, "********onDestroy*******"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    sput-boolean v0, Lcom/jrm/localmm/util/Constants;->isExit:Z

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/16 v0, 0xb2

    if-ne p1, v0, :cond_0

    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 6
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v5, 0x4

    const/4 v0, 0x0

    const/4 v1, 0x1

    const-string v2, "PhotoPlayerActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "*********onKeyUp*********"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-ne p1, v5, :cond_4

    iget-object v2, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoFileList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    sget-boolean v2, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->is4K2KMode:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPlayControllerLayout:Landroid/widget/LinearLayout;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPlayControllerLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iput-boolean v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mIsControllerShow:Z

    :cond_0
    iget-object v2, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    iget-object v2, v2, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->mSurfaceView4K2K:Lcom/jrm/localmm/ui/photo/SurfaceView4K2K;

    invoke-virtual {v2, v5}, Lcom/jrm/localmm/ui/photo/SurfaceView4K2K;->setVisibility(I)V

    sput-boolean v0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->is4K2KMode:Z

    sput-boolean v0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->panel4k2kmode:Z

    sget-object v0, Lcom/mstar/android/MDisplay$PanelMode;->E_PANELMODE_NORMAL:Lcom/mstar/android/MDisplay$PanelMode;

    invoke-static {v0}, Lcom/mstar/android/MDisplay;->setPanelMode(Lcom/mstar/android/MDisplay$PanelMode;)V

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->finish()V

    :cond_1
    sget-object v0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x16

    const-wide/16 v3, 0x3e8

    invoke-virtual {v0, v2, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    :cond_2
    :goto_0
    return v1

    :cond_3
    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->finish()V

    goto :goto_0

    :cond_4
    iget-boolean v2, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCanResponse:Z

    if-eqz v2, :cond_2

    if-eqz p1, :cond_2

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-ne v2, v1, :cond_2

    iget-boolean v2, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mIsControllerShow:Z

    if-eqz v2, :cond_8

    sparse-switch p1, :sswitch_data_0

    sget-object v0, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string v2, "default is click!!"

    invoke-virtual {v0, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0

    :sswitch_0
    invoke-direct {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->cancleDelayHide()V

    iget-boolean v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPPTPlayer:Z

    if-eqz v0, :cond_5

    invoke-direct {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->stopPPTPlayer()V

    :cond_5
    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->moveNextOrPrevious(I)V

    invoke-direct {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->hideControlDelay()V

    goto :goto_0

    :sswitch_1
    invoke-direct {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->cancleDelayHide()V

    iget-boolean v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPPTPlayer:Z

    if-eqz v0, :cond_6

    invoke-direct {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->stopPPTPlayer()V

    :cond_6
    invoke-direct {p0, v1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->moveNextOrPrevious(I)V

    invoke-direct {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->hideControlDelay()V

    goto :goto_0

    :sswitch_2
    invoke-direct {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->cancleDelayHide()V

    invoke-direct {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->drapLeft()V

    invoke-direct {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->hideControlDelay()V

    goto :goto_0

    :sswitch_3
    invoke-direct {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->cancleDelayHide()V

    invoke-direct {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->drapRight()V

    invoke-direct {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->hideControlDelay()V

    goto :goto_0

    :sswitch_4
    invoke-direct {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->cancleDelayHide()V

    invoke-direct {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->registerListeners()V

    invoke-direct {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->hideControlDelay()V

    goto :goto_0

    :sswitch_5
    invoke-direct {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->cancleDelayHide()V

    iget-object v2, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    iget-object v2, v2, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->mImageView:Lcom/jrm/localmm/ui/photo/ImageViewTouch;

    iget-object v3, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    iget-object v3, v3, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->mImageView:Lcom/jrm/localmm/ui/photo/ImageViewTouch;

    invoke-virtual {v3}, Lcom/jrm/localmm/ui/photo/ImageViewTouch;->getMoveFlag()Z

    move-result v3

    if-nez v3, :cond_7

    move v0, v1

    :cond_7
    invoke-virtual {v2, v0}, Lcom/jrm/localmm/ui/photo/ImageViewTouch;->setMoveFlag(Z)V

    invoke-direct {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->hideControlDelay()V

    goto :goto_0

    :cond_8
    sparse-switch p1, :sswitch_data_1

    goto :goto_0

    :sswitch_6
    invoke-direct {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->showController()V

    invoke-direct {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->hideControlDelay()V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x13 -> :sswitch_0
        0x14 -> :sswitch_1
        0x15 -> :sswitch_2
        0x16 -> :sswitch_3
        0x17 -> :sswitch_4
        0x42 -> :sswitch_4
        0x52 -> :sswitch_5
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x17 -> :sswitch_6
        0x42 -> :sswitch_6
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 4

    const/4 v3, 0x1

    const-string v0, "PhotoPlayerActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "********onPause*******"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/jrm/localmm/util/Constants;->isExit:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iput-boolean v3, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->isOnPause:Z

    sget-object v0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->options:Landroid/graphics/BitmapFactory$Options;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->options:Landroid/graphics/BitmapFactory$Options;

    invoke-virtual {v0}, Landroid/graphics/BitmapFactory$Options;->requestCancelDecode()V

    :cond_0
    iget v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCurrentView:I

    if-ne v0, v3, :cond_1

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->mSurfaceView:Lcom/jrm/localmm/ui/photo/ImageSurfaceView;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/photo/ImageSurfaceView;->setNormal()V

    :cond_1
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$2;

    invoke-direct {v1, p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$2;-><init>(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mDiskChangeReceiver:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$DiskChangeReceiver;

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mNetDisconnectReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mFileInputStream:Ljava/io/FileInputStream;

    invoke-direct {p0, v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->closeSilently(Ljava/io/Closeable;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->is:Ljava/io/InputStream;

    invoke-direct {p0, v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->closeSilently(Ljava/io/Closeable;)V

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    return-void
.end method

.method protected onResume()V
    .locals 7

    const/4 v6, 0x0

    const-string v3, "PhotoPlayerActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "********onResume*******"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-boolean v5, Lcom/jrm/localmm/util/Constants;->isExit:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iput-boolean v6, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->isOnPause:Z

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    const-string v3, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v3, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v3, "file"

    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    new-instance v3, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$DiskChangeReceiver;

    const/4 v4, 0x0

    invoke-direct {v3, p0, v4}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$DiskChangeReceiver;-><init>(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$1;)V

    iput-object v3, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mDiskChangeReceiver:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$DiskChangeReceiver;

    iget-object v3, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mDiskChangeReceiver:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$DiskChangeReceiver;

    invoke-virtual {p0, v3, v1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "com.mstar.localmm.network.disconnect"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mNetDisconnectReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v3, v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    new-instance v3, Ljava/lang/Thread;

    new-instance v4, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$3;

    invoke-direct {v4, p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$3;-><init>(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)V

    invoke-direct {v3, v4}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v3}, Ljava/lang/Thread;->start()V

    iget-boolean v3, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->fristShowPicture:Z

    if-eqz v3, :cond_2

    iput-boolean v6, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->fristShowPicture:Z

    iget-object v3, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoFileList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->finish()V

    :goto_0
    return-void

    :cond_0
    iget v3, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCurrentPosition:I

    if-gt v0, v3, :cond_1

    add-int/lit8 v3, v0, -0x1

    iput v3, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCurrentPosition:I

    :cond_1
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->isShowMsg:Z

    sput-boolean v6, Lcom/jrm/localmm/util/Constants;->isExit:Z

    invoke-direct {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->playFirstPhoto()V

    :goto_1
    iget-object v3, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->animationArray:[I

    array-length v3, v3

    iput v3, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->animationNum:I

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->showController()V

    goto :goto_1
.end method

.method protected onStart()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    return-void
.end method

.method protected onStop()V
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->mImageView:Lcom/jrm/localmm/ui/photo/ImageViewTouch;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/photo/ImageViewTouch;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->mImageView:Lcom/jrm/localmm/ui/photo/ImageViewTouch;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/photo/ImageViewTouch;->clear()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->mImageView:Lcom/jrm/localmm/ui/photo/ImageViewTouch;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/photo/ImageViewTouch;->resetRotateCounter()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->mGifView:Lcom/jrm/localmm/ui/photo/GifView;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/photo/GifView;->setStop()V

    sget-boolean v0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->is4K2KMode:Z

    if-eqz v0, :cond_0

    sput-boolean v2, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->is4K2KMode:Z

    sput-boolean v2, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->panel4k2kmode:Z

    iput v2, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCurrentView:I

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoPlayerHolder:Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->mScrollView:Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;

    invoke-virtual {v0, v2}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->setCurrentView(I)V

    sget-object v0, Lcom/mstar/android/MDisplay$PanelMode;->E_PANELMODE_NORMAL:Lcom/mstar/android/MDisplay$PanelMode;

    invoke-static {v0}, Lcom/mstar/android/MDisplay;->setPanelMode(Lcom/mstar/android/MDisplay$PanelMode;)V

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->finish()V

    :cond_0
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1    # Landroid/view/MotionEvent;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mIsControllerShow:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->showController()V

    invoke-direct {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->hideControlDelay()V

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setSystemUiVisibility(I)V

    :cond_0
    const-string v0, "PhotoPlayerActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "********onTouchEvent*******"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0, p1}, Landroid/app/Activity;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public setDefaultPhoto()Landroid/graphics/Bitmap;
    .locals 5

    const/4 v0, 0x0

    const/4 v4, 0x1

    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    sget-object v2, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    iput-object v2, v1, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    iput-boolean v4, v1, Landroid/graphics/BitmapFactory$Options;->inPurgeable:Z

    iput-boolean v4, v1, Landroid/graphics/BitmapFactory$Options;->inInputShareable:Z

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f02001a    # com.jrm.localmm.R.drawable.default_bg

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v2

    if-nez v2, :cond_0

    :goto_0
    return-object v0

    :cond_0
    iput-boolean v4, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->isDefaultPhoto:Z

    invoke-static {v2, v0, v1}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public showToastAtCenter(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    const/16 v0, 0x11

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->showToast(Ljava/lang/String;II)V

    return-void
.end method
