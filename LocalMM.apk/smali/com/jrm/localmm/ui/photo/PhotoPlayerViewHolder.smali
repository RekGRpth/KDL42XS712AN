.class public Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;
.super Ljava/lang/Object;
.source "PhotoPlayerViewHolder.java"


# static fields
.field public static playSettingName:[I


# instance fields
.field protected bt_Photo3D:Landroid/widget/ImageView;

.field protected bt_PhotoSetting:Landroid/widget/ImageView;

.field protected bt_PhotoWallpaper:Landroid/widget/ImageView;

.field protected bt_photoEnlarge:Landroid/widget/ImageView;

.field protected bt_photoInfo:Landroid/widget/ImageView;

.field protected bt_photoNarrow:Landroid/widget/ImageView;

.field protected bt_photoNext:Landroid/widget/ImageView;

.field protected bt_photoPlay:Landroid/widget/ImageView;

.field protected bt_photoPre:Landroid/widget/ImageView;

.field protected bt_photoTLeft:Landroid/widget/ImageView;

.field protected bt_photoTRight:Landroid/widget/ImageView;

.field protected mGifView:Lcom/jrm/localmm/ui/photo/GifView;

.field protected mImageView:Lcom/jrm/localmm/ui/photo/ImageViewTouch;

.field private mPhotoPlayerActivity:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

.field protected mScrollView:Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;

.field protected mSurfaceView:Lcom/jrm/localmm/ui/photo/ImageSurfaceView;

.field protected mSurfaceView4K2K:Lcom/jrm/localmm/ui/photo/SurfaceView4K2K;

.field protected photo_name:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->playSettingName:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x7f06004b    # com.jrm.localmm.R.string.picture_animation_setting
        0x7f06004a    # com.jrm.localmm.R.string.picture_playing_time
    .end array-data
.end method

.method public constructor <init>(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)V
    .locals 0
    .param p1    # Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->mPhotoPlayerActivity:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    return-void
.end method


# virtual methods
.method findViews()V
    .locals 2

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->mPhotoPlayerActivity:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    const v1, 0x7f08005d    # com.jrm.localmm.R.id.ViewFlipper

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;

    iput-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->mScrollView:Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->mScrollView:Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->setCurrentView(I)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->mScrollView:Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;

    new-instance v1, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder$1;

    invoke-direct {v1, p0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder$1;-><init>(Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;)V

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/photo/ScrollableViewGroup;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->mPhotoPlayerActivity:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    const v1, 0x7f080069    # com.jrm.localmm.R.id.photo_name_display

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->photo_name:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->mPhotoPlayerActivity:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    const v1, 0x7f080032    # com.jrm.localmm.R.id.image

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/jrm/localmm/ui/photo/ImageViewTouch;

    iput-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->mImageView:Lcom/jrm/localmm/ui/photo/ImageViewTouch;

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->mPhotoPlayerActivity:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    const v1, 0x7f080031    # com.jrm.localmm.R.id.imageSurfaceView

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/jrm/localmm/ui/photo/ImageSurfaceView;

    iput-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->mSurfaceView:Lcom/jrm/localmm/ui/photo/ImageSurfaceView;

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->mPhotoPlayerActivity:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    const v1, 0x7f080030    # com.jrm.localmm.R.id.gifView

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/jrm/localmm/ui/photo/GifView;

    iput-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->mGifView:Lcom/jrm/localmm/ui/photo/GifView;

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->mPhotoPlayerActivity:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    const v1, 0x7f080096    # com.jrm.localmm.R.id.SurfaceView4K2K

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/jrm/localmm/ui/photo/SurfaceView4K2K;

    iput-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->mSurfaceView4K2K:Lcom/jrm/localmm/ui/photo/SurfaceView4K2K;

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->mPhotoPlayerActivity:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    const v1, 0x7f08006a    # com.jrm.localmm.R.id.player_previous

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->bt_photoPre:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->mPhotoPlayerActivity:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    const v1, 0x7f08006b    # com.jrm.localmm.R.id.photo_play

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->bt_photoPlay:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->bt_photoPlay:Landroid/widget/ImageView;

    const v1, 0x7f02006c    # com.jrm.localmm.R.drawable.player_icon_play_focus

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->mPhotoPlayerActivity:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    const v1, 0x7f08006c    # com.jrm.localmm.R.id.photo_next

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->bt_photoNext:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->mPhotoPlayerActivity:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    const v1, 0x7f08006d    # com.jrm.localmm.R.id.photo_enlarge

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->bt_photoEnlarge:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->mPhotoPlayerActivity:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    const v1, 0x7f08006e    # com.jrm.localmm.R.id.photo_narrow

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->bt_photoNarrow:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->mPhotoPlayerActivity:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    const v1, 0x7f08006f    # com.jrm.localmm.R.id.photo_turn_left

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->bt_photoTLeft:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->mPhotoPlayerActivity:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    const v1, 0x7f080071    # com.jrm.localmm.R.id.photo_info

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->bt_photoInfo:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->mPhotoPlayerActivity:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    const v1, 0x7f080070    # com.jrm.localmm.R.id.photo_turn_right

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->bt_photoTRight:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->mPhotoPlayerActivity:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    const v1, 0x7f080072    # com.jrm.localmm.R.id.photo_wallpaper

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->bt_PhotoWallpaper:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->mPhotoPlayerActivity:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    const v1, 0x7f080073    # com.jrm.localmm.R.id.photo_3d

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->bt_Photo3D:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->mPhotoPlayerActivity:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    const v1, 0x7f080074    # com.jrm.localmm.R.id.photo_setting

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->bt_PhotoSetting:Landroid/widget/ImageView;

    return-void
.end method

.method public setAllUnSelect(ZZ)V
    .locals 1
    .param p1    # Z
    .param p2    # Z

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->setPhotoPreSelect(Z)V

    invoke-virtual {p0, v0, p1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->setPhotoPlaySelect(ZZ)V

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->setPhotoNextSelect(Z)V

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->setPhotoEnlargeSelect(Z)V

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->setPhotoNarrowSelect(Z)V

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->setPhotoTurnLeftSelect(Z)V

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->setPhotoTurnRightSelect(Z)V

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->setPhotoInfoSelect(Z)V

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->setPhotoWallpaperSelect(Z)V

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->setPhoto3DSelect(Z)V

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->setPhotoSettingSelect(Z)V

    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1    # Landroid/view/View$OnClickListener;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->bt_photoPre:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->bt_photoPlay:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->bt_photoNext:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->bt_photoEnlarge:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->bt_photoNarrow:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->bt_photoTLeft:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->bt_photoInfo:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->bt_photoTRight:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->bt_PhotoWallpaper:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->bt_Photo3D:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->bt_PhotoSetting:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->mImageView:Lcom/jrm/localmm/ui/photo/ImageViewTouch;

    invoke-virtual {v0, p1}, Lcom/jrm/localmm/ui/photo/ImageViewTouch;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->mSurfaceView:Lcom/jrm/localmm/ui/photo/ImageSurfaceView;

    invoke-virtual {v0, p1}, Lcom/jrm/localmm/ui/photo/ImageSurfaceView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->mGifView:Lcom/jrm/localmm/ui/photo/GifView;

    invoke-virtual {v0, p1}, Lcom/jrm/localmm/ui/photo/GifView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    return-void
.end method

.method public setPhoto3DSelect(Z)V
    .locals 2
    .param p1    # Z

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->bt_Photo3D:Landroid/widget/ImageView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->bt_Photo3D:Landroid/widget/ImageView;

    const v1, 0x7f020045    # com.jrm.localmm.R.drawable.player_icon_3d_focus

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->bt_Photo3D:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->bt_Photo3D:Landroid/widget/ImageView;

    const v1, 0x7f020044    # com.jrm.localmm.R.drawable.player_icon_3d

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method public setPhotoEnlargeSelect(Z)V
    .locals 2
    .param p1    # Z

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->bt_photoEnlarge:Landroid/widget/ImageView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->bt_photoEnlarge:Landroid/widget/ImageView;

    const v1, 0x7f020049    # com.jrm.localmm.R.drawable.player_icon_amplification_focus

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->bt_photoEnlarge:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->bt_photoEnlarge:Landroid/widget/ImageView;

    const v1, 0x7f020048    # com.jrm.localmm.R.drawable.player_icon_amplification

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method public setPhotoInfoSelect(Z)V
    .locals 2
    .param p1    # Z

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->bt_photoInfo:Landroid/widget/ImageView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->bt_photoInfo:Landroid/widget/ImageView;

    const v1, 0x7f02005d    # com.jrm.localmm.R.drawable.player_icon_infor_focus

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->bt_photoInfo:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->bt_photoInfo:Landroid/widget/ImageView;

    const v1, 0x7f02005c    # com.jrm.localmm.R.drawable.player_icon_infor

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method public setPhotoNarrowSelect(Z)V
    .locals 2
    .param p1    # Z

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->bt_photoNarrow:Landroid/widget/ImageView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->bt_photoNarrow:Landroid/widget/ImageView;

    const v1, 0x7f020065    # com.jrm.localmm.R.drawable.player_icon_narrow_focus

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->bt_photoNarrow:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->bt_photoNarrow:Landroid/widget/ImageView;

    const v1, 0x7f020064    # com.jrm.localmm.R.drawable.player_icon_narrow

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method public setPhotoNextSelect(Z)V
    .locals 2
    .param p1    # Z

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->bt_photoNext:Landroid/widget/ImageView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->bt_photoNext:Landroid/widget/ImageView;

    const v1, 0x7f020067    # com.jrm.localmm.R.drawable.player_icon_next_focus

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->bt_photoNext:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->bt_photoNext:Landroid/widget/ImageView;

    const v1, 0x7f020066    # com.jrm.localmm.R.drawable.player_icon_next

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method public setPhotoPlaySelect(ZZ)V
    .locals 2
    .param p1    # Z
    .param p2    # Z

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->bt_photoPlay:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setFocusable(Z)V

    if-eqz p1, :cond_1

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->bt_photoPlay:Landroid/widget/ImageView;

    const v1, 0x7f02006a    # com.jrm.localmm.R.drawable.player_icon_pause_focus

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->bt_photoPlay:Landroid/widget/ImageView;

    const v1, 0x7f02006c    # com.jrm.localmm.R.drawable.player_icon_play_focus

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    :cond_1
    if-eqz p2, :cond_2

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->bt_photoPlay:Landroid/widget/ImageView;

    const v1, 0x7f020069    # com.jrm.localmm.R.drawable.player_icon_pause

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->bt_photoPlay:Landroid/widget/ImageView;

    const v1, 0x7f02006b    # com.jrm.localmm.R.drawable.player_icon_play

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method public setPhotoPreSelect(Z)V
    .locals 2
    .param p1    # Z

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->bt_photoPre:Landroid/widget/ImageView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->bt_photoPre:Landroid/widget/ImageView;

    const v1, 0x7f02006e    # com.jrm.localmm.R.drawable.player_icon_previous_focus

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->bt_photoPre:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->bt_photoPre:Landroid/widget/ImageView;

    const v1, 0x7f02006d    # com.jrm.localmm.R.drawable.player_icon_previous

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method public setPhotoSettingSelect(Z)V
    .locals 2
    .param p1    # Z

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->bt_PhotoSetting:Landroid/widget/ImageView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->bt_PhotoSetting:Landroid/widget/ImageView;

    const v1, 0x7f020079    # com.jrm.localmm.R.drawable.player_icon_setting_foucs

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->bt_PhotoSetting:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->bt_PhotoSetting:Landroid/widget/ImageView;

    const v1, 0x7f020078    # com.jrm.localmm.R.drawable.player_icon_setting

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method public setPhotoTurnLeftSelect(Z)V
    .locals 2
    .param p1    # Z

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->bt_photoTLeft:Landroid/widget/ImageView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->bt_photoTLeft:Landroid/widget/ImageView;

    const v1, 0x7f02005f    # com.jrm.localmm.R.drawable.player_icon_left_focus

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->bt_photoTLeft:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->bt_photoTLeft:Landroid/widget/ImageView;

    const v1, 0x7f02005e    # com.jrm.localmm.R.drawable.player_icon_left

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method public setPhotoTurnRightSelect(Z)V
    .locals 2
    .param p1    # Z

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->bt_photoTRight:Landroid/widget/ImageView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->bt_photoTRight:Landroid/widget/ImageView;

    const v1, 0x7f020075    # com.jrm.localmm.R.drawable.player_icon_right_focus

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->bt_photoTRight:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->bt_photoTRight:Landroid/widget/ImageView;

    const v1, 0x7f020074    # com.jrm.localmm.R.drawable.player_icon_right

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method public setPhotoWallpaperSelect(Z)V
    .locals 2
    .param p1    # Z

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->bt_PhotoWallpaper:Landroid/widget/ImageView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->bt_PhotoWallpaper:Landroid/widget/ImageView;

    const v1, 0x7f020077    # com.jrm.localmm.R.drawable.player_icon_scene_focus

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->bt_PhotoWallpaper:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerViewHolder;->bt_PhotoWallpaper:Landroid/widget/ImageView;

    const v1, 0x7f020076    # com.jrm.localmm.R.drawable.player_icon_scene

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0
.end method
