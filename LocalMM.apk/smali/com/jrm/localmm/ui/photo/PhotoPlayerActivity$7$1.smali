.class Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$7$1;
.super Ljava/lang/Object;
.source "PhotoPlayerActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$7;->onClick(Landroid/content/DialogInterface;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$7;


# direct methods
.method constructor <init>(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$7;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$7$1;->this$1:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$7;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    iget-object v4, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$7$1;->this$1:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$7;

    iget-object v4, v4, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$7;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    const/4 v5, 0x0

    # setter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCanSetWallpaper:Z
    invoke-static {v4, v5}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$2002(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;Z)Z

    iget-object v4, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$7$1;->this$1:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$7;

    iget-object v4, v4, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$7;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoFileList:Ljava/util/List;
    invoke-static {v4}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$1600(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Ljava/util/List;

    move-result-object v4

    iget-object v5, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$7$1;->this$1:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$7;

    iget-object v5, v5, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$7;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCurrentPosition:I
    invoke-static {v5}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$1500(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)I

    move-result v5

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/jrm/localmm/business/data/BaseData;

    invoke-virtual {v4}, Lcom/jrm/localmm/business/data/BaseData;->getPath()Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$7$1;->this$1:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$7;

    iget-object v4, v4, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$7;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    const-string v5, "wallpaper"

    invoke-virtual {v4, v5}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/WallpaperManager;

    iget-object v4, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$7$1;->this$1:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$7;

    iget-object v4, v4, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$7;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    invoke-virtual {v4, v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->decodeBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v4, "PhotoPlayerActivity"

    const-string v5, "Couldn\'t get bitmap for path!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    iget-object v4, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$7$1;->this$1:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$7;

    iget-object v4, v4, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$7;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    const/4 v5, 0x1

    # setter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mCanSetWallpaper:Z
    invoke-static {v4, v5}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$2002(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;Z)Z

    return-void

    :cond_0
    :try_start_0
    invoke-virtual {v3, v0}, Landroid/app/WallpaperManager;->setBitmap(Landroid/graphics/Bitmap;)V

    iget-object v4, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$7$1;->this$1:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$7;

    iget-object v4, v4, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$7;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$2100(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Landroid/os/Handler;

    move-result-object v4

    const/16 v5, 0xc

    invoke-virtual {v4, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    const-string v4, "PhotoPlayerActivity"

    const-string v5, "Failed to set wallpaper."

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
