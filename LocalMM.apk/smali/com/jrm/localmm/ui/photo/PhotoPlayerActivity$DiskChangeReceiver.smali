.class Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$DiskChangeReceiver;
.super Landroid/content/BroadcastReceiver;
.source "PhotoPlayerActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DiskChangeReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;


# direct methods
.method private constructor <init>(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$DiskChangeReceiver;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$1;)V
    .locals 0
    .param p1    # Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;
    .param p2    # Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$1;

    invoke-direct {p0, p1}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$DiskChangeReceiver;-><init>(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x7

    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$DiskChangeReceiver;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mPhotoFileList:Ljava/util/List;
    invoke-static {v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$1600(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Ljava/util/List;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/jrm/localmm/business/data/BaseData;

    invoke-virtual {v2}, Lcom/jrm/localmm/business/data/BaseData;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$DiskChangeReceiver;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    iget-object v3, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$DiskChangeReceiver;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    const v4, 0x7f06003e    # com.jrm.localmm.R.string.disk_eject

    invoke-virtual {v3, v4}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->showToastAtCenter(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$DiskChangeReceiver;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    iget-object v3, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$DiskChangeReceiver;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->mFileInputStream:Ljava/io/FileInputStream;
    invoke-static {v3}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$4100(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)Ljava/io/FileInputStream;

    move-result-object v3

    # invokes: Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->closeSilently(Ljava/io/Closeable;)V
    invoke-static {v2, v3}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->access$4200(Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;Ljava/io/Closeable;)V

    iget-object v2, p0, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity$DiskChangeReceiver;->this$0:Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    invoke-virtual {v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->finish()V

    :cond_0
    return-void
.end method
