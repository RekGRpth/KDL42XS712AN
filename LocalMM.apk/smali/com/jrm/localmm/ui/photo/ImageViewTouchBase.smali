.class public Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;
.super Landroid/widget/ImageView;
.source "ImageViewTouchBase.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/jrm/localmm/ui/photo/ImageViewTouchBase$Recycler;
    }
.end annotation


# instance fields
.field protected mBaseMatrix:Landroid/graphics/Matrix;

.field protected final mBitmapDisplayed:Lcom/jrm/localmm/ui/photo/RotateBitmap;

.field private mCanMove:Z

.field private final mDisplayMatrix:Landroid/graphics/Matrix;

.field protected mHandler:Landroid/os/Handler;

.field private final mMatrixValues:[F

.field private mOnLayoutRunnable:Ljava/lang/Runnable;

.field private mRecycler:Lcom/jrm/localmm/ui/photo/ImageViewTouchBase$Recycler;

.field public mRotateCounter:I

.field protected mSuppMatrix:Landroid/graphics/Matrix;

.field mThisHeight:I

.field mThisWidth:I

.field private mViewHeight:F

.field private mViewWidth:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1    # Landroid/content/Context;

    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, -0x1

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mBaseMatrix:Landroid/graphics/Matrix;

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mSuppMatrix:Landroid/graphics/Matrix;

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mDisplayMatrix:Landroid/graphics/Matrix;

    const/16 v0, 0x9

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mMatrixValues:[F

    new-instance v0, Lcom/jrm/localmm/ui/photo/RotateBitmap;

    invoke-direct {v0, v4}, Lcom/jrm/localmm/ui/photo/RotateBitmap;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mBitmapDisplayed:Lcom/jrm/localmm/ui/photo/RotateBitmap;

    iput v2, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mThisWidth:I

    iput v2, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mThisHeight:I

    iput v1, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mViewWidth:F

    iput v1, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mViewHeight:F

    iput v3, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mRotateCounter:I

    iput-boolean v3, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mCanMove:Z

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mHandler:Landroid/os/Handler;

    iput-object v4, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mOnLayoutRunnable:Ljava/lang/Runnable;

    invoke-direct {p0}, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->init()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, -0x1

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mBaseMatrix:Landroid/graphics/Matrix;

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mSuppMatrix:Landroid/graphics/Matrix;

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mDisplayMatrix:Landroid/graphics/Matrix;

    const/16 v0, 0x9

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mMatrixValues:[F

    new-instance v0, Lcom/jrm/localmm/ui/photo/RotateBitmap;

    invoke-direct {v0, v4}, Lcom/jrm/localmm/ui/photo/RotateBitmap;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mBitmapDisplayed:Lcom/jrm/localmm/ui/photo/RotateBitmap;

    iput v2, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mThisWidth:I

    iput v2, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mThisHeight:I

    iput v1, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mViewWidth:F

    iput v1, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mViewHeight:F

    iput v3, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mRotateCounter:I

    iput-boolean v3, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mCanMove:Z

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mHandler:Landroid/os/Handler;

    iput-object v4, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mOnLayoutRunnable:Ljava/lang/Runnable;

    invoke-direct {p0}, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->init()V

    return-void
.end method

.method private getProperBaseMatrix(Lcom/jrm/localmm/ui/photo/RotateBitmap;Landroid/graphics/Matrix;)V
    .locals 10
    .param p1    # Lcom/jrm/localmm/ui/photo/RotateBitmap;
    .param p2    # Landroid/graphics/Matrix;

    const/high16 v8, 0x40400000    # 3.0f

    const/high16 v9, 0x40000000    # 2.0f

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->getWidth()I

    move-result v7

    int-to-float v4, v7

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->getHeight()I

    move-result v7

    int-to-float v3, v7

    invoke-virtual {p1}, Lcom/jrm/localmm/ui/photo/RotateBitmap;->getWidth()I

    move-result v7

    int-to-float v5, v7

    invoke-virtual {p1}, Lcom/jrm/localmm/ui/photo/RotateBitmap;->getHeight()I

    move-result v7

    int-to-float v0, v7

    invoke-virtual {p2}, Landroid/graphics/Matrix;->reset()V

    div-float v7, v4, v5

    invoke-static {v7, v8}, Ljava/lang/Math;->min(FF)F

    move-result v6

    div-float v7, v3, v0

    invoke-static {v7, v8}, Ljava/lang/Math;->min(FF)F

    move-result v1

    invoke-static {v6, v1}, Ljava/lang/Math;->min(FF)F

    move-result v2

    invoke-virtual {p1}, Lcom/jrm/localmm/ui/photo/RotateBitmap;->getRotateMatrix()Landroid/graphics/Matrix;

    move-result-object v7

    invoke-virtual {p2, v7}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    invoke-virtual {p2, v2, v2}, Landroid/graphics/Matrix;->postScale(FF)Z

    mul-float v7, v5, v2

    sub-float v7, v4, v7

    div-float/2addr v7, v9

    mul-float v8, v0, v2

    sub-float v8, v3, v8

    div-float/2addr v8, v9

    invoke-virtual {p2, v7, v8}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    return-void
.end method

.method private init()V
    .locals 1

    sget-object v0, Landroid/widget/ImageView$ScaleType;->MATRIX:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    return-void
.end method

.method private moveDirection(I)V
    .locals 7
    .param p1    # I

    const/high16 v6, 0x42c80000    # 100.0f

    const/4 v5, 0x0

    const/4 v0, 0x0

    const/4 v1, 0x0

    iget-object v3, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mBitmapDisplayed:Lcom/jrm/localmm/ui/photo/RotateBitmap;

    invoke-virtual {v3}, Lcom/jrm/localmm/ui/photo/RotateBitmap;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v3

    if-nez v3, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v2, Landroid/graphics/RectF;

    iget-object v3, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mBitmapDisplayed:Lcom/jrm/localmm/ui/photo/RotateBitmap;

    invoke-virtual {v3}, Lcom/jrm/localmm/ui/photo/RotateBitmap;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mBitmapDisplayed:Lcom/jrm/localmm/ui/photo/RotateBitmap;

    invoke-virtual {v4}, Lcom/jrm/localmm/ui/photo/RotateBitmap;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    int-to-float v4, v4

    invoke-direct {v2, v5, v5, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    iget-object v3, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mDisplayMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v3, v2}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    packed-switch p1, :pswitch_data_0

    sget-object v3, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string v4, "default is click!!"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_1
    :goto_1
    iget-object v3, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mDisplayMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v3, v0, v1}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    goto :goto_0

    :pswitch_0
    iget v3, v2, Landroid/graphics/RectF;->bottom:F

    iget v4, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mViewHeight:F

    add-float/2addr v4, v6

    cmpl-float v3, v3, v4

    if-lez v3, :cond_2

    const/high16 v1, -0x3d380000    # -100.0f

    goto :goto_1

    :cond_2
    iget v3, v2, Landroid/graphics/RectF;->bottom:F

    iget v4, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mViewHeight:F

    cmpl-float v3, v3, v4

    if-lez v3, :cond_1

    iget v3, v2, Landroid/graphics/RectF;->bottom:F

    neg-float v3, v3

    iget v4, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mViewHeight:F

    add-float v1, v3, v4

    goto :goto_1

    :pswitch_1
    iget v3, v2, Landroid/graphics/RectF;->top:F

    add-float/2addr v3, v6

    cmpl-float v3, v5, v3

    if-lez v3, :cond_3

    const/high16 v1, 0x42c80000    # 100.0f

    goto :goto_1

    :cond_3
    iget v3, v2, Landroid/graphics/RectF;->top:F

    cmpl-float v3, v5, v3

    if-lez v3, :cond_1

    iget v3, v2, Landroid/graphics/RectF;->top:F

    neg-float v1, v3

    goto :goto_1

    :pswitch_2
    iget v3, v2, Landroid/graphics/RectF;->right:F

    iget v4, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mViewWidth:F

    add-float/2addr v4, v6

    cmpl-float v3, v3, v4

    if-lez v3, :cond_4

    const/high16 v0, -0x3d380000    # -100.0f

    goto :goto_1

    :cond_4
    iget v3, v2, Landroid/graphics/RectF;->right:F

    iget v4, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mViewWidth:F

    cmpl-float v3, v3, v4

    if-lez v3, :cond_1

    iget v3, v2, Landroid/graphics/RectF;->right:F

    neg-float v3, v3

    iget v4, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mViewWidth:F

    add-float v0, v3, v4

    goto :goto_1

    :pswitch_3
    iget v3, v2, Landroid/graphics/RectF;->left:F

    add-float/2addr v3, v6

    cmpl-float v3, v5, v3

    if-lez v3, :cond_5

    const/high16 v0, 0x42c80000    # 100.0f

    goto :goto_1

    :cond_5
    iget v3, v2, Landroid/graphics/RectF;->left:F

    cmpl-float v3, v5, v3

    if-lez v3, :cond_1

    iget v3, v2, Landroid/graphics/RectF;->left:F

    neg-float v0, v3

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x13
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private prepareDisplayMatrix()V
    .locals 14

    iget-object v10, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mBitmapDisplayed:Lcom/jrm/localmm/ui/photo/RotateBitmap;

    invoke-virtual {v10}, Lcom/jrm/localmm/ui/photo/RotateBitmap;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v10

    invoke-virtual {v10}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v10

    int-to-float v5, v10

    iget-object v10, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mBitmapDisplayed:Lcom/jrm/localmm/ui/photo/RotateBitmap;

    invoke-virtual {v10}, Lcom/jrm/localmm/ui/photo/RotateBitmap;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v10

    invoke-virtual {v10}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v10

    int-to-float v4, v10

    iget v10, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mRotateCounter:I

    invoke-static {v10}, Ljava/lang/Math;->abs(I)I

    move-result v10

    rem-int/lit8 v10, v10, 0x2

    const/4 v11, 0x1

    if-ne v10, v11, :cond_2

    iget v10, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mViewWidth:F

    div-float/2addr v10, v4

    const/high16 v11, 0x40400000    # 3.0f

    invoke-static {v10, v11}, Ljava/lang/Math;->min(FF)F

    move-result v9

    iget v10, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mViewHeight:F

    div-float/2addr v10, v5

    const/high16 v11, 0x40400000    # 3.0f

    invoke-static {v10, v11}, Ljava/lang/Math;->min(FF)F

    move-result v3

    :goto_0
    invoke-static {v9, v3}, Ljava/lang/Math;->min(FF)F

    move-result v7

    iget-object v10, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mBaseMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v10}, Landroid/graphics/Matrix;->reset()V

    iget-object v10, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mBaseMatrix:Landroid/graphics/Matrix;

    new-instance v11, Landroid/graphics/Matrix;

    invoke-direct {v11}, Landroid/graphics/Matrix;-><init>()V

    invoke-virtual {v10, v11}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    iget-object v10, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mBaseMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v10, v7, v7}, Landroid/graphics/Matrix;->postScale(FF)Z

    iget-object v10, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mBaseMatrix:Landroid/graphics/Matrix;

    iget v11, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mViewWidth:F

    mul-float v12, v5, v7

    sub-float/2addr v11, v12

    const/high16 v12, 0x40000000    # 2.0f

    div-float/2addr v11, v12

    iget v12, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mViewHeight:F

    mul-float v13, v4, v7

    sub-float/2addr v12, v13

    const/high16 v13, 0x40000000    # 2.0f

    div-float/2addr v12, v13

    invoke-virtual {v10, v11, v12}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    iget-object v10, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mDisplayMatrix:Landroid/graphics/Matrix;

    iget-object v11, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mBaseMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v10, v11}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    iget-object v10, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mDisplayMatrix:Landroid/graphics/Matrix;

    iget-object v11, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mSuppMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v10, v11}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    new-instance v6, Landroid/graphics/RectF;

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-direct {v6, v10, v11, v5, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    iget-object v10, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mDisplayMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v10, v6}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    invoke-virtual {v6}, Landroid/graphics/RectF;->height()F

    move-result v2

    invoke-virtual {v6}, Landroid/graphics/RectF;->width()F

    move-result v8

    const/4 v0, 0x0

    const/4 v1, 0x0

    iget v10, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mViewHeight:F

    cmpg-float v10, v2, v10

    if-gez v10, :cond_3

    iget v10, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mViewHeight:F

    sub-float/2addr v10, v2

    const/high16 v11, 0x40000000    # 2.0f

    div-float/2addr v10, v11

    iget v11, v6, Landroid/graphics/RectF;->top:F

    sub-float v1, v10, v11

    :cond_0
    :goto_1
    iget v10, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mViewWidth:F

    cmpg-float v10, v8, v10

    if-gez v10, :cond_5

    iget v10, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mViewWidth:F

    sub-float/2addr v10, v8

    const/high16 v11, 0x40000000    # 2.0f

    div-float/2addr v10, v11

    iget v11, v6, Landroid/graphics/RectF;->left:F

    sub-float v0, v10, v11

    :cond_1
    :goto_2
    iget-object v10, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mDisplayMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v10, v0, v1}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    return-void

    :cond_2
    iget v10, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mViewWidth:F

    div-float/2addr v10, v5

    const/high16 v11, 0x40400000    # 3.0f

    invoke-static {v10, v11}, Ljava/lang/Math;->min(FF)F

    move-result v9

    iget v10, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mViewHeight:F

    div-float/2addr v10, v4

    const/high16 v11, 0x40400000    # 3.0f

    invoke-static {v10, v11}, Ljava/lang/Math;->min(FF)F

    move-result v3

    goto/16 :goto_0

    :cond_3
    iget v10, v6, Landroid/graphics/RectF;->top:F

    const/4 v11, 0x0

    cmpl-float v10, v10, v11

    if-lez v10, :cond_4

    iget v10, v6, Landroid/graphics/RectF;->top:F

    neg-float v1, v10

    goto :goto_1

    :cond_4
    iget v10, v6, Landroid/graphics/RectF;->bottom:F

    iget v11, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mViewHeight:F

    cmpg-float v10, v10, v11

    if-gez v10, :cond_0

    iget v10, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mViewHeight:F

    iget v11, v6, Landroid/graphics/RectF;->bottom:F

    sub-float v1, v10, v11

    goto :goto_1

    :cond_5
    iget v10, v6, Landroid/graphics/RectF;->left:F

    const/4 v11, 0x0

    cmpl-float v10, v10, v11

    if-lez v10, :cond_6

    iget v10, v6, Landroid/graphics/RectF;->left:F

    neg-float v0, v10

    goto :goto_2

    :cond_6
    iget v10, v6, Landroid/graphics/RectF;->right:F

    iget v11, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mViewWidth:F

    cmpg-float v10, v10, v11

    if-gez v10, :cond_1

    iget v10, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mViewWidth:F

    iget v11, v6, Landroid/graphics/RectF;->right:F

    sub-float v0, v10, v11

    goto :goto_2
.end method

.method private set2Original(I)V
    .locals 9
    .param p1    # I

    const/high16 v5, 0x40000000    # 2.0f

    const/high16 v8, 0x3fa00000    # 1.25f

    const/high16 v7, 0x3f800000    # 1.0f

    const v6, 0x3f4ccccd    # 0.8f

    iget v4, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mViewWidth:F

    div-float v0, v4, v5

    iget v4, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mViewHeight:F

    div-float v1, v4, v5

    iget-object v4, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mSuppMatrix:Landroid/graphics/Matrix;

    const/4 v5, 0x0

    invoke-virtual {p0, v4, v5}, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->getValue(Landroid/graphics/Matrix;I)F

    move-result v3

    const/4 v4, 0x0

    cmpl-float v4, v3, v4

    if-lez v4, :cond_2

    iget-object v4, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mSuppMatrix:Landroid/graphics/Matrix;

    div-float v5, v7, v3

    div-float v6, v7, v3

    invoke-virtual {v4, v5, v6, v0, v1}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    :cond_0
    invoke-direct {p0}, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->prepareDisplayMatrix()V

    iget-object v4, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mDisplayMatrix:Landroid/graphics/Matrix;

    invoke-virtual {p0, v4}, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->setImageMatrix(Landroid/graphics/Matrix;)V

    :cond_1
    return-void

    :cond_2
    if-lez p1, :cond_3

    const/4 v2, 0x0

    :goto_0
    if-ge v2, p1, :cond_0

    iget-object v4, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mSuppMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v4, v6, v6, v0, v1}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_3
    if-gez p1, :cond_1

    const/4 v2, 0x0

    :goto_1
    neg-int v4, p1

    if-ge v2, v4, :cond_0

    iget-object v4, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mSuppMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v4, v8, v8, v0, v1}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method private setImageBitmap(Landroid/graphics/Bitmap;I)V
    .locals 3
    .param p1    # Landroid/graphics/Bitmap;
    .param p2    # I

    invoke-super {p0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/Drawable;->setDither(Z)V

    :cond_0
    iget-object v2, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mBitmapDisplayed:Lcom/jrm/localmm/ui/photo/RotateBitmap;

    invoke-virtual {v2}, Lcom/jrm/localmm/ui/photo/RotateBitmap;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    iget-object v2, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mBitmapDisplayed:Lcom/jrm/localmm/ui/photo/RotateBitmap;

    invoke-virtual {v2, p1}, Lcom/jrm/localmm/ui/photo/RotateBitmap;->setBitmap(Landroid/graphics/Bitmap;)V

    iget-object v2, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mBitmapDisplayed:Lcom/jrm/localmm/ui/photo/RotateBitmap;

    invoke-virtual {v2, p2}, Lcom/jrm/localmm/ui/photo/RotateBitmap;->setRotation(I)V

    if-eqz v1, :cond_1

    if-eq v1, p1, :cond_1

    iget-object v2, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mRecycler:Lcom/jrm/localmm/ui/photo/ImageViewTouchBase$Recycler;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mRecycler:Lcom/jrm/localmm/ui/photo/ImageViewTouchBase$Recycler;

    invoke-interface {v2, v1}, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase$Recycler;->recycle(Landroid/graphics/Bitmap;)V

    :cond_1
    return-void
.end method


# virtual methods
.method protected center(ZZ)V
    .locals 12
    .param p1    # Z
    .param p2    # Z

    const/high16 v11, 0x40000000    # 2.0f

    const/4 v10, 0x0

    iget-object v8, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mBitmapDisplayed:Lcom/jrm/localmm/ui/photo/RotateBitmap;

    invoke-virtual {v8}, Lcom/jrm/localmm/ui/photo/RotateBitmap;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v8

    if-nez v8, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->getImageViewMatrix()Landroid/graphics/Matrix;

    move-result-object v3

    new-instance v4, Landroid/graphics/RectF;

    iget-object v8, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mBitmapDisplayed:Lcom/jrm/localmm/ui/photo/RotateBitmap;

    invoke-virtual {v8}, Lcom/jrm/localmm/ui/photo/RotateBitmap;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v8

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    int-to-float v8, v8

    iget-object v9, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mBitmapDisplayed:Lcom/jrm/localmm/ui/photo/RotateBitmap;

    invoke-virtual {v9}, Lcom/jrm/localmm/ui/photo/RotateBitmap;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v9

    invoke-virtual {v9}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    int-to-float v9, v9

    invoke-direct {v4, v10, v10, v8, v9}, Landroid/graphics/RectF;-><init>(FFFF)V

    invoke-virtual {v3, v4}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v2

    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v7

    const/4 v0, 0x0

    const/4 v1, 0x0

    if-eqz p2, :cond_1

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->getHeight()I

    move-result v5

    int-to-float v8, v5

    cmpg-float v8, v2, v8

    if-gez v8, :cond_3

    int-to-float v8, v5

    sub-float/2addr v8, v2

    div-float/2addr v8, v11

    iget v9, v4, Landroid/graphics/RectF;->top:F

    sub-float v1, v8, v9

    :cond_1
    :goto_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->getWidth()I

    move-result v6

    int-to-float v8, v6

    cmpg-float v8, v7, v8

    if-gez v8, :cond_5

    int-to-float v8, v6

    sub-float/2addr v8, v7

    div-float/2addr v8, v11

    iget v9, v4, Landroid/graphics/RectF;->left:F

    sub-float v0, v8, v9

    :cond_2
    :goto_2
    invoke-virtual {p0, v0, v1}, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->postTranslate(FF)V

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->getImageViewMatrix()Landroid/graphics/Matrix;

    move-result-object v8

    invoke-virtual {p0, v8}, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->setImageMatrix(Landroid/graphics/Matrix;)V

    goto :goto_0

    :cond_3
    iget v8, v4, Landroid/graphics/RectF;->top:F

    cmpl-float v8, v8, v10

    if-lez v8, :cond_4

    iget v8, v4, Landroid/graphics/RectF;->top:F

    neg-float v1, v8

    goto :goto_1

    :cond_4
    iget v8, v4, Landroid/graphics/RectF;->bottom:F

    int-to-float v9, v5

    cmpg-float v8, v8, v9

    if-gez v8, :cond_1

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->getHeight()I

    move-result v8

    int-to-float v8, v8

    iget v9, v4, Landroid/graphics/RectF;->bottom:F

    sub-float v1, v8, v9

    goto :goto_1

    :cond_5
    iget v8, v4, Landroid/graphics/RectF;->left:F

    cmpl-float v8, v8, v10

    if-lez v8, :cond_6

    iget v8, v4, Landroid/graphics/RectF;->left:F

    neg-float v0, v8

    goto :goto_2

    :cond_6
    iget v8, v4, Landroid/graphics/RectF;->right:F

    int-to-float v9, v6

    cmpg-float v8, v8, v9

    if-gez v8, :cond_2

    int-to-float v8, v6

    iget v9, v4, Landroid/graphics/RectF;->right:F

    sub-float v0, v8, v9

    goto :goto_2
.end method

.method public clear()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->setImageBitmapResetBase(Landroid/graphics/Bitmap;Z)V

    return-void
.end method

.method protected getImageViewMatrix()Landroid/graphics/Matrix;
    .locals 2

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mDisplayMatrix:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mBaseMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mDisplayMatrix:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mSuppMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mDisplayMatrix:Landroid/graphics/Matrix;

    return-object v0
.end method

.method public getMoveFlag()Z
    .locals 1

    iget-boolean v0, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mCanMove:Z

    return v0
.end method

.method public getScale()F
    .locals 1

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mSuppMatrix:Landroid/graphics/Matrix;

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->getScale(Landroid/graphics/Matrix;)F

    move-result v0

    return v0
.end method

.method protected getScale(Landroid/graphics/Matrix;)F
    .locals 1
    .param p1    # Landroid/graphics/Matrix;

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->getValue(Landroid/graphics/Matrix;I)F

    move-result v0

    return v0
.end method

.method protected getValue(Landroid/graphics/Matrix;I)F
    .locals 1
    .param p1    # Landroid/graphics/Matrix;
    .param p2    # I

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mMatrixValues:[F

    invoke-virtual {p1, v0}, Landroid/graphics/Matrix;->getValues([F)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mMatrixValues:[F

    aget v0, v0, p2

    return v0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const-string v0, "ImageViewTouchBase"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onKeyDown, keyCode : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mCanMove : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mCanMove:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p2}, Landroid/view/KeyEvent;->startTracking()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/ImageView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 5
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v1, 0x1

    const-string v0, "ImageViewTouchBase"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onKeyUp, keyCode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " mCanMove : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mCanMove:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    invoke-virtual {p2}, Landroid/view/KeyEvent;->isTracking()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Landroid/view/KeyEvent;->isCanceled()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->getScale()F

    move-result v0

    cmpl-float v0, v0, v4

    if-lez v0, :cond_0

    invoke-virtual {p0, v4}, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->zoomTo(F)V

    :goto_0
    return v1

    :cond_0
    const/16 v0, 0x52

    if-ne p1, v0, :cond_1

    iget-boolean v0, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mCanMove:Z

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mCanMove:Z

    :cond_1
    iget-boolean v0, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mCanMove:Z

    if-eqz v0, :cond_3

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const-string v0, "ImageViewTouchBase"

    const-string v2, "move up"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v0, 0x13

    invoke-direct {p0, v0}, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->moveDirection(I)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mDisplayMatrix:Landroid/graphics/Matrix;

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->setImageMatrix(Landroid/graphics/Matrix;)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :pswitch_1
    const-string v0, "ImageViewTouchBase"

    const-string v2, "move down"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v0, 0x14

    invoke-direct {p0, v0}, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->moveDirection(I)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mDisplayMatrix:Landroid/graphics/Matrix;

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->setImageMatrix(Landroid/graphics/Matrix;)V

    goto :goto_0

    :pswitch_2
    const-string v0, "ImageViewTouchBase"

    const-string v2, "move left"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v0, 0x15

    invoke-direct {p0, v0}, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->moveDirection(I)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mDisplayMatrix:Landroid/graphics/Matrix;

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->setImageMatrix(Landroid/graphics/Matrix;)V

    goto :goto_0

    :pswitch_3
    const-string v0, "ImageViewTouchBase"

    const-string v2, "move right"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v0, 0x16

    invoke-direct {p0, v0}, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->moveDirection(I)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mDisplayMatrix:Landroid/graphics/Matrix;

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->setImageMatrix(Landroid/graphics/Matrix;)V

    goto :goto_0

    :cond_3
    invoke-super {p0, p1, p2}, Landroid/widget/ImageView;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x13
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected onLayout(ZIIII)V
    .locals 3
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    invoke-super/range {p0 .. p5}, Landroid/widget/ImageView;->onLayout(ZIIII)V

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->getWidth()I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mViewWidth:F

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->getHeight()I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mViewHeight:F

    sub-int v1, p4, p2

    iput v1, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mThisWidth:I

    sub-int v1, p5, p3

    iput v1, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mThisHeight:I

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mOnLayoutRunnable:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mOnLayoutRunnable:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    :cond_0
    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mBitmapDisplayed:Lcom/jrm/localmm/ui/photo/RotateBitmap;

    invoke-virtual {v1}, Lcom/jrm/localmm/ui/photo/RotateBitmap;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mBitmapDisplayed:Lcom/jrm/localmm/ui/photo/RotateBitmap;

    iget-object v2, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mBaseMatrix:Landroid/graphics/Matrix;

    invoke-direct {p0, v1, v2}, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->getProperBaseMatrix(Lcom/jrm/localmm/ui/photo/RotateBitmap;Landroid/graphics/Matrix;)V

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->getImageViewMatrix()Landroid/graphics/Matrix;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->setImageMatrix(Landroid/graphics/Matrix;)V

    :cond_1
    return-void
.end method

.method protected postTranslate(FF)V
    .locals 1
    .param p1    # F
    .param p2    # F

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mSuppMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    return-void
.end method

.method public resetRotateCounter()V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mRotateCounter:I

    return-void
.end method

.method public rotateImageLeft(I)V
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mBitmapDisplayed:Lcom/jrm/localmm/ui/photo/RotateBitmap;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/photo/RotateBitmap;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, p1}, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->set2Original(I)V

    iget v0, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mRotateCounter:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mRotateCounter:I

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mSuppMatrix:Landroid/graphics/Matrix;

    const/high16 v1, -0x3d4c0000    # -90.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->postRotate(F)Z

    invoke-direct {p0}, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->prepareDisplayMatrix()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mDisplayMatrix:Landroid/graphics/Matrix;

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->setImageMatrix(Landroid/graphics/Matrix;)V

    goto :goto_0
.end method

.method public rotateImageRight(I)V
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mBitmapDisplayed:Lcom/jrm/localmm/ui/photo/RotateBitmap;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/photo/RotateBitmap;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, p1}, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->set2Original(I)V

    iget v0, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mRotateCounter:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mRotateCounter:I

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mSuppMatrix:Landroid/graphics/Matrix;

    const/high16 v1, 0x42b40000    # 90.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->postRotate(F)Z

    invoke-direct {p0}, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->prepareDisplayMatrix()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mDisplayMatrix:Landroid/graphics/Matrix;

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->setImageMatrix(Landroid/graphics/Matrix;)V

    goto :goto_0
.end method

.method public setImageBitmap(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1    # Landroid/graphics/Bitmap;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->setImageBitmap(Landroid/graphics/Bitmap;I)V

    return-void
.end method

.method public setImageBitmapResetBase(Landroid/graphics/Bitmap;Z)V
    .locals 2
    .param p1    # Landroid/graphics/Bitmap;
    .param p2    # Z

    new-instance v0, Lcom/jrm/localmm/ui/photo/RotateBitmap;

    invoke-direct {v0, p1}, Lcom/jrm/localmm/ui/photo/RotateBitmap;-><init>(Landroid/graphics/Bitmap;)V

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p2, v1}, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->setImageRotateBitmapResetBase(Lcom/jrm/localmm/ui/photo/RotateBitmap;ZLcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)V

    return-void
.end method

.method public setImageRotateBitmapResetBase(Lcom/jrm/localmm/ui/photo/RotateBitmap;ZLcom/jrm/localmm/ui/photo/PhotoPlayerActivity;)V
    .locals 5
    .param p1    # Lcom/jrm/localmm/ui/photo/RotateBitmap;
    .param p2    # Z
    .param p3    # Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->getWidth()I

    move-result v1

    const-string v2, "ImageViewTouchBase"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "showBitmap, viewWidth : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-gtz v1, :cond_0

    new-instance v2, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase$1;

    invoke-direct {v2, p0, p1, p2}, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase$1;-><init>(Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;Lcom/jrm/localmm/ui/photo/RotateBitmap;Z)V

    iput-object v2, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mOnLayoutRunnable:Ljava/lang/Runnable;

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/jrm/localmm/ui/photo/RotateBitmap;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    const-string v2, "ImageViewTouchBase"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "showBitmap, viewWidth : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v0, :cond_3

    iget-object v2, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mBaseMatrix:Landroid/graphics/Matrix;

    invoke-direct {p0, p1, v2}, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->getProperBaseMatrix(Lcom/jrm/localmm/ui/photo/RotateBitmap;Landroid/graphics/Matrix;)V

    invoke-virtual {p1}, Lcom/jrm/localmm/ui/photo/RotateBitmap;->getRotation()I

    move-result v2

    invoke-direct {p0, v0, v2}, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->setImageBitmap(Landroid/graphics/Bitmap;I)V

    if-eqz p3, :cond_1

    iget-boolean v2, p3, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->isDefaultPhoto:Z

    if-eqz v2, :cond_1

    const v2, 0x7f060043    # com.jrm.localmm.R.string.can_not_decode

    invoke-virtual {p3, v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p3, v2}, Lcom/jrm/localmm/ui/photo/PhotoPlayerActivity;->showToastAtCenter(Ljava/lang/String;)V

    :cond_1
    const/4 v0, 0x0

    invoke-static {}, Ljava/lang/System;->gc()V

    :goto_1
    if-eqz p2, :cond_2

    iget-object v2, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mSuppMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v2}, Landroid/graphics/Matrix;->reset()V

    :cond_2
    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->getImageViewMatrix()Landroid/graphics/Matrix;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->setImageMatrix(Landroid/graphics/Matrix;)V

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mBaseMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v2}, Landroid/graphics/Matrix;->reset()V

    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_1
.end method

.method public setMoveFlag(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mCanMove:Z

    return-void
.end method

.method public zoomIn()V
    .locals 1

    const/high16 v0, 0x3fa00000    # 1.25f

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->zoomIn(F)V

    return-void
.end method

.method protected zoomIn(F)V
    .locals 4
    .param p1    # F

    const/high16 v3, 0x40000000    # 2.0f

    iget-object v2, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mBitmapDisplayed:Lcom/jrm/localmm/ui/photo/RotateBitmap;

    invoke-virtual {v2}, Lcom/jrm/localmm/ui/photo/RotateBitmap;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    if-nez v2, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->getWidth()I

    move-result v2

    int-to-float v2, v2

    div-float v0, v2, v3

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float v1, v2, v3

    iget-object v2, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mSuppMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v2, p1, p1, v0, v1}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    invoke-direct {p0}, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->prepareDisplayMatrix()V

    iget-object v2, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mDisplayMatrix:Landroid/graphics/Matrix;

    invoke-virtual {p0, v2}, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->setImageMatrix(Landroid/graphics/Matrix;)V

    goto :goto_0
.end method

.method public zoomOut()V
    .locals 1

    const/high16 v0, 0x3fa00000    # 1.25f

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->zoomOut(F)V

    return-void
.end method

.method protected zoomOut(F)V
    .locals 5
    .param p1    # F

    const/high16 v3, 0x40000000    # 2.0f

    const/high16 v4, 0x3f800000    # 1.0f

    iget-object v2, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mBitmapDisplayed:Lcom/jrm/localmm/ui/photo/RotateBitmap;

    invoke-virtual {v2}, Lcom/jrm/localmm/ui/photo/RotateBitmap;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    if-nez v2, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->getWidth()I

    move-result v2

    int-to-float v2, v2

    div-float v0, v2, v3

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float v1, v2, v3

    iget-object v2, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mSuppMatrix:Landroid/graphics/Matrix;

    div-float v3, v4, p1

    div-float/2addr v4, p1

    invoke-virtual {v2, v3, v4, v0, v1}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    invoke-direct {p0}, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->prepareDisplayMatrix()V

    iget-object v2, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mDisplayMatrix:Landroid/graphics/Matrix;

    invoke-virtual {p0, v2}, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->setImageMatrix(Landroid/graphics/Matrix;)V

    goto :goto_0
.end method

.method public zoomTo(F)V
    .locals 4
    .param p1    # F

    const/high16 v3, 0x40000000    # 2.0f

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->getWidth()I

    move-result v2

    int-to-float v2, v2

    div-float v0, v2, v3

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float v1, v2, v3

    invoke-virtual {p0, p1, v0, v1}, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->zoomTo(FFF)V

    return-void
.end method

.method protected zoomTo(FFF)V
    .locals 4
    .param p1    # F
    .param p2    # F
    .param p3    # F

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->getScale()F

    move-result v1

    div-float v0, p1, v1

    iget-object v2, p0, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->mSuppMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v2, v0, v0, p2, p3}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->getImageViewMatrix()Landroid/graphics/Matrix;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->setImageMatrix(Landroid/graphics/Matrix;)V

    invoke-virtual {p0, v3, v3}, Lcom/jrm/localmm/ui/photo/ImageViewTouchBase;->center(ZZ)V

    return-void
.end method
