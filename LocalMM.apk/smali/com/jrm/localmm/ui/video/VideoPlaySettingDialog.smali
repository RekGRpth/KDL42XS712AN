.class public Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;
.super Landroid/app/Dialog;
.source "VideoPlaySettingDialog.java"


# instance fields
.field private adapter:Lcom/jrm/localmm/business/adapter/VideoPlaySettingListAdapter;

.field private context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

.field private mVideoPlayView:Lcom/jrm/localmm/business/video/VideoPlayView;

.field private onkeyListenter:Landroid/view/View$OnKeyListener;

.field private playSettingAudioTrackDialog:Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;

.field private playSettingListView:Landroid/widget/ListView;

.field private threeDSettingDialog:Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;

.field private viewId:I


# direct methods
.method public constructor <init>(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;ILjava/lang/String;Lcom/jrm/localmm/business/video/VideoPlayView;)V
    .locals 6
    .param p1    # Lcom/jrm/localmm/ui/video/VideoPlayerActivity;
    .param p2    # I
    .param p3    # Ljava/lang/String;
    .param p4    # Lcom/jrm/localmm/business/video/VideoPlayView;

    const v5, 0x7f0600c4    # com.jrm.localmm.R.string.play_setting_0_value_2

    const/4 v4, 0x2

    const/4 v3, 0x1

    invoke-direct {p0, p1, p2}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    iput v3, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->viewId:I

    new-instance v0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog$3;

    invoke-direct {v0, p0}, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog$3;-><init>(Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->onkeyListenter:Landroid/view/View$OnKeyListener;

    iput-object p1, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    iput-object p4, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->mVideoPlayView:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-virtual {p1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getVideoPlayHolder()Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getViewId()I

    move-result v0

    iput v0, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->viewId:I

    new-instance v0, Lcom/jrm/localmm/business/adapter/VideoPlaySettingListAdapter;

    sget-object v1, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->playSettingName:[I

    iget v2, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->viewId:I

    invoke-static {p1, v2}, Lcom/jrm/localmm/util/Tools;->initPlaySettingOpt(Landroid/content/Context;I)[Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, p1, v1, v2}, Lcom/jrm/localmm/business/adapter/VideoPlaySettingListAdapter;-><init>(Landroid/content/Context;[I[Ljava/lang/String;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->adapter:Lcom/jrm/localmm/business/adapter/VideoPlaySettingListAdapter;

    invoke-virtual {p1, v5}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->viewId:I

    invoke-static {v3, v0, v1}, Lcom/jrm/localmm/util/Tools;->setPlaySettingOpt(ILjava/lang/String;I)V

    invoke-virtual {p1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getBreakPointFlag()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0600c3    # com.jrm.localmm.R.string.play_setting_0_value_1

    invoke-virtual {p1, v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->viewId:I

    invoke-static {v4, v0, v1}, Lcom/jrm/localmm/util/Tools;->setPlaySettingOpt(ILjava/lang/String;I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1, v5}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->viewId:I

    invoke-static {v4, v0, v1}, Lcom/jrm/localmm/util/Tools;->setPlaySettingOpt(ILjava/lang/String;I)V

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;)Lcom/jrm/localmm/ui/video/VideoPlayerActivity;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    return-object v0
.end method

.method static synthetic access$100(Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;)I
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;

    iget v0, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->viewId:I

    return v0
.end method

.method static synthetic access$200(Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;)Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->threeDSettingDialog:Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;

    return-object v0
.end method

.method static synthetic access$202(Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;)Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;
    .param p1    # Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;

    iput-object p1, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->threeDSettingDialog:Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;

    return-object p1
.end method

.method static synthetic access$300(Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;)V
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;

    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->changeBreakPointFlag()V

    return-void
.end method

.method static synthetic access$400(Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;)Lcom/jrm/localmm/business/adapter/VideoPlaySettingListAdapter;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->adapter:Lcom/jrm/localmm/business/adapter/VideoPlaySettingListAdapter;

    return-object v0
.end method

.method static synthetic access$500(Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;)Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->playSettingAudioTrackDialog:Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;

    return-object v0
.end method

.method static synthetic access$502(Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;)Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;
    .param p1    # Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;

    iput-object p1, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->playSettingAudioTrackDialog:Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;

    return-object p1
.end method

.method static synthetic access$600(Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;)Landroid/widget/ListView;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->playSettingListView:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$700(Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;)V
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;

    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->change3DState()V

    return-void
.end method

.method static synthetic access$800(Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;)V
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;

    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->changeSubtitleStateLeft()V

    return-void
.end method

.method static synthetic access$900(Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;)V
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;

    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->changeSubtitleStateRight()V

    return-void
.end method

.method private change3DState()V
    .locals 6

    const v4, 0x7f0600c4    # com.jrm.localmm.R.string.play_setting_0_value_2

    const v3, 0x7f0600c3    # com.jrm.localmm.R.string.play_setting_0_value_1

    const/4 v5, 0x0

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    iget-object v2, v2, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->s3dSkin:Lcom/mstar/android/tv/TvS3DManager;

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->viewId:I

    invoke-static {v5, v2}, Lcom/jrm/localmm/util/Tools;->getPlaySettingOpt(II)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v2, v3}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v2, v4}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget v3, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->viewId:I

    invoke-static {v5, v2, v3}, Lcom/jrm/localmm/util/Tools;->setPlaySettingOpt(ILjava/lang/String;I)V

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    iget-object v2, v2, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->s3dSkin:Lcom/mstar/android/tv/TvS3DManager;

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;->E_ThreeD_Video_DISPLAYFORMAT_NONE:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    invoke-virtual {v2, v3}, Lcom/mstar/android/tv/TvS3DManager;->setDisplayFormat(Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v2, v4}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getVideoPlayHolder()Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getDualVideoMode()Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v2, v3}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget v3, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->viewId:I

    invoke-static {v5, v2, v3}, Lcom/jrm/localmm/util/Tools;->setPlaySettingOpt(ILjava/lang/String;I)V

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    iget-object v2, v2, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->displayFormat:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;->E_ThreeD_Video_DISPLAYFORMAT_NONE:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;->E_ThreeD_Video_DISPLAYFORMAT_AUTO:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    iput-object v3, v2, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->displayFormat:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    :cond_2
    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    iget-object v2, v2, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->s3dSkin:Lcom/mstar/android/tv/TvS3DManager;

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    iget-object v3, v3, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->displayFormat:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    invoke-virtual {v2, v3}, Lcom/mstar/android/tv/TvS3DManager;->setDisplayFormat(Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;)Z

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v3}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0600a2    # com.jrm.localmm.R.string.can_not_open_3D

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    const/16 v2, 0x11

    invoke-virtual {v1, v2, v5, v5}, Landroid/widget/Toast;->setGravity(III)V

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method private changeBreakPointFlag()V
    .locals 5

    const v4, 0x7f0600c4    # com.jrm.localmm.R.string.play_setting_0_value_2

    const v2, 0x7f0600c3    # com.jrm.localmm.R.string.play_setting_0_value_1

    const/4 v3, 0x2

    iget v1, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->viewId:I

    invoke-static {v3, v1}, Lcom/jrm/localmm/util/Tools;->getPlaySettingOpt(II)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v1, v2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v1, v4}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget v2, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->viewId:I

    invoke-static {v3, v1, v2}, Lcom/jrm/localmm/util/Tools;->setPlaySettingOpt(ILjava/lang/String;I)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    const-string v2, "false"

    invoke-static {v1, v2}, Lcom/jrm/localmm/business/video/BreakPointRecord;->setBreakPointFlag(Landroid/content/Context;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->setBreakPointFlag(Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v1, v4}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v1, v2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget v2, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->viewId:I

    invoke-static {v3, v1, v2}, Lcom/jrm/localmm/util/Tools;->setPlaySettingOpt(ILjava/lang/String;I)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    const-string v2, "true"

    invoke-static {v1, v2}, Lcom/jrm/localmm/business/video/BreakPointRecord;->setBreakPointFlag(Landroid/content/Context;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->setBreakPointFlag(Z)V

    goto :goto_0
.end method

.method private changeSubtitleStateLeft()V
    .locals 7

    const v6, 0x7f0600c8    # com.jrm.localmm.R.string.subtitle_setting_0_value_2

    const v5, 0x7f0600c7    # com.jrm.localmm.R.string.subtitle_setting_0_value_1

    const v3, 0x7f0600c4    # com.jrm.localmm.R.string.play_setting_0_value_2

    const/4 v4, 0x1

    iget v2, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->viewId:I

    invoke-static {v4, v2}, Lcom/jrm/localmm/util/Tools;->getPlaySettingOpt(II)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v2, v5}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v2, v3}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget v3, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->viewId:I

    invoke-static {v4, v2, v3}, Lcom/jrm/localmm/util/Tools;->setPlaySettingOpt(ILjava/lang/String;I)V

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->mVideoPlayView:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-virtual {v2}, Lcom/jrm/localmm/business/video/VideoPlayView;->offSubtitleTrack()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v2, v6}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v2, v5}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget v3, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->viewId:I

    invoke-static {v4, v2, v3}, Lcom/jrm/localmm/util/Tools;->setPlaySettingOpt(ILjava/lang/String;I)V

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->mVideoPlayView:Lcom/jrm/localmm/business/video/VideoPlayView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/jrm/localmm/business/video/VideoPlayView;->setSubtitleDataSource(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->mVideoPlayView:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-virtual {v2}, Lcom/jrm/localmm/business/video/VideoPlayView;->offSubtitleTrack()V

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->mVideoPlayView:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-virtual {v2}, Lcom/jrm/localmm/business/video/VideoPlayView;->getAllSubtitleTrackInfo()Lcom/mstar/android/media/SubtitleTrackInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/mstar/android/media/SubtitleTrackInfo;->getAllInternalSubtitleCount()I

    move-result v2

    sput v2, Lcom/jrm/localmm/util/Constants;->mVideoSubtitleNo:I

    sget v2, Lcom/jrm/localmm/util/Constants;->mVideoSubtitleNo:I

    if-lez v2, :cond_0

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->mVideoPlayView:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-virtual {v2}, Lcom/jrm/localmm/business/video/VideoPlayView;->onSubtitleTrack()V

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->mVideoPlayView:Lcom/jrm/localmm/business/video/VideoPlayView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/jrm/localmm/business/video/VideoPlayView;->setSubtitleTrack(I)V

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v2, v3}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v2, v6}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget v3, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->viewId:I

    invoke-static {v4, v2, v3}, Lcom/jrm/localmm/util/Tools;->setPlaySettingOpt(ILjava/lang/String;I)V

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->mVideoPlayView:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-virtual {v2}, Lcom/jrm/localmm/business/video/VideoPlayView;->onSubtitleTrack()V

    goto :goto_0
.end method

.method private changeSubtitleStateRight()V
    .locals 7

    const v6, 0x7f0600c8    # com.jrm.localmm.R.string.subtitle_setting_0_value_2

    const v5, 0x7f0600c7    # com.jrm.localmm.R.string.subtitle_setting_0_value_1

    const v3, 0x7f0600c4    # com.jrm.localmm.R.string.play_setting_0_value_2

    const/4 v4, 0x1

    iget v2, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->viewId:I

    invoke-static {v4, v2}, Lcom/jrm/localmm/util/Tools;->getPlaySettingOpt(II)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v2, v3}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v2, v5}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget v3, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->viewId:I

    invoke-static {v4, v2, v3}, Lcom/jrm/localmm/util/Tools;->setPlaySettingOpt(ILjava/lang/String;I)V

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->mVideoPlayView:Lcom/jrm/localmm/business/video/VideoPlayView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/jrm/localmm/business/video/VideoPlayView;->setSubtitleDataSource(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->mVideoPlayView:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-virtual {v2}, Lcom/jrm/localmm/business/video/VideoPlayView;->getAllSubtitleTrackInfo()Lcom/mstar/android/media/SubtitleTrackInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/mstar/android/media/SubtitleTrackInfo;->getAllInternalSubtitleCount()I

    move-result v2

    sput v2, Lcom/jrm/localmm/util/Constants;->mVideoSubtitleNo:I

    sget v2, Lcom/jrm/localmm/util/Constants;->mVideoSubtitleNo:I

    if-lez v2, :cond_0

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->mVideoPlayView:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-virtual {v2}, Lcom/jrm/localmm/business/video/VideoPlayView;->onSubtitleTrack()V

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->mVideoPlayView:Lcom/jrm/localmm/business/video/VideoPlayView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/jrm/localmm/business/video/VideoPlayView;->setSubtitleTrack(I)V

    const/4 v2, 0x2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    const v5, 0x7f0600ae    # com.jrm.localmm.R.string.subtitle_2_value_2

    invoke-virtual {v4, v5}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "0"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget v4, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->viewId:I

    invoke-static {v2, v3, v4}, Lcom/jrm/localmm/util/Tools;->setSubtitleSettingOpt(ILjava/lang/String;I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v2, v5}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v2, v6}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget v3, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->viewId:I

    invoke-static {v4, v2, v3}, Lcom/jrm/localmm/util/Tools;->setPlaySettingOpt(ILjava/lang/String;I)V

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->mVideoPlayView:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-virtual {v2}, Lcom/jrm/localmm/business/video/VideoPlayView;->offSubtitleTrack()V

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v2, v6}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v2, v3}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget v3, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->viewId:I

    invoke-static {v4, v2, v3}, Lcom/jrm/localmm/util/Tools;->setPlaySettingOpt(ILjava/lang/String;I)V

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->mVideoPlayView:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-virtual {v2}, Lcom/jrm/localmm/business/video/VideoPlayView;->offSubtitleTrack()V

    goto :goto_0
.end method

.method private setListeners()V
    .locals 2

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->playSettingListView:Landroid/widget/ListView;

    new-instance v1, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog$2;

    invoke-direct {v1, p0}, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog$2;-><init>(Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->playSettingListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->onkeyListenter:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    return-void
.end method


# virtual methods
.method public dismiss()V
    .locals 1

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->threeDSettingDialog:Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->threeDSettingDialog:Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;->dismiss()V

    :cond_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->playSettingAudioTrackDialog:Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->playSettingAudioTrackDialog:Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->dismiss()V

    :cond_1
    invoke-super {p0}, Landroid/app/Dialog;->dismiss()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 9
    .param p1    # Landroid/os/Bundle;

    const/4 v7, 0x0

    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    const v5, 0x7f030020    # com.jrm.localmm.R.layout.video_play_setting_dialog

    invoke-virtual {p0, v5}, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->setContentView(I)V

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v5

    invoke-interface {v5}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    const v5, 0x7f0800af    # com.jrm.localmm.R.id.videoPlaySettingListView

    invoke-virtual {p0, v5}, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ListView;

    iput-object v5, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->playSettingListView:Landroid/widget/ListView;

    iget-object v5, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->playSettingListView:Landroid/widget/ListView;

    invoke-virtual {v5, v7}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    iget-object v5, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    iget-object v5, v5, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->s3dSkin:Lcom/mstar/android/tv/TvS3DManager;

    if-eqz v5, :cond_0

    new-instance v5, Ljava/lang/Thread;

    new-instance v6, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog$1;

    invoke-direct {v6, p0}, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog$1;-><init>(Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;)V

    invoke-direct {v5, v6}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v5}, Ljava/lang/Thread;->start()V

    :cond_0
    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->setListeners()V

    invoke-virtual {v2, v7}, Landroid/view/Window;->setTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v5

    int-to-double v5, v5

    const-wide v7, 0x3fdb851eb851eb85L    # 0.43

    mul-double/2addr v5, v7

    double-to-int v3, v5

    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v5

    int-to-double v5, v5

    const-wide v7, 0x3fe199999999999aL    # 0.55

    mul-double/2addr v5, v7

    double-to-int v1, v5

    invoke-virtual {v2, v3, v1}, Landroid/view/Window;->setLayout(II)V

    const/16 v5, 0x11

    invoke-virtual {v2, v5}, Landroid/view/Window;->setGravity(I)V

    invoke-virtual {v2}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/16 v0, 0x7e

    if-eq v0, p1, :cond_0

    const/16 v0, 0x7f

    if-eq v0, p1, :cond_0

    const/16 v0, 0x57

    if-eq v0, p1, :cond_0

    const/16 v0, 0x58

    if-ne v0, p1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    invoke-super {p0, p1, p2}, Landroid/app/Dialog;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/16 v0, 0x7e

    if-eq v0, p1, :cond_0

    const/16 v0, 0x7f

    if-eq v0, p1, :cond_0

    const/16 v0, 0x57

    if-eq v0, p1, :cond_0

    const/16 v0, 0x58

    if-ne v0, p1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    invoke-super {p0, p1, p2}, Landroid/app/Dialog;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public show()V
    .locals 2

    invoke-super {p0}, Landroid/app/Dialog;->show()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->playSettingListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->adapter:Lcom/jrm/localmm/business/adapter/VideoPlaySettingListAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method
