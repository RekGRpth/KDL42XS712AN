.class Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog$1;
.super Ljava/lang/Object;
.source "PlaySettingSubtitleDialog.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->setListeners()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;


# direct methods
.method constructor <init>(Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog$1;->this$0:Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    const/4 v2, 0x0

    packed-switch p3, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog$1;->this$0:Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;

    # getter for: Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->adapter:Lcom/jrm/localmm/business/adapter/SubtitleSettingListAdapter;
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->access$1200(Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;)Lcom/jrm/localmm/business/adapter/SubtitleSettingListAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/jrm/localmm/business/adapter/SubtitleSettingListAdapter;->notifyDataSetChanged()V

    return-void

    :pswitch_1
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog$1;->this$0:Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;

    # getter for: Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->isInnerSubtitle:Z
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->access$000(Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog$1;->this$0:Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog$1;->this$0:Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;

    # getter for: Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->subtitlePosition:I
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->access$100(Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;)I

    move-result v1

    # invokes: Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->changeSubtitleLanguage(IZ)V
    invoke-static {v0, v1, v2}, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->access$200(Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;IZ)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog$1;->this$0:Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->dismiss()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog$1;->this$0:Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;

    # getter for: Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->access$300(Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;)Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getVideoHandler()Landroid/os/Handler;

    move-result-object v0

    const/16 v1, 0x72

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog$1;->this$0:Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;

    # getter for: Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->isInnerSubtitle:Z
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->access$000(Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog$1;->this$0:Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;

    # invokes: Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->changeSubtitleLanguage(Z)V
    invoke-static {v0, v2}, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->access$400(Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;Z)V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog$1;->this$0:Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;

    # invokes: Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->changeSubtitleFont(Z)V
    invoke-static {v0, v2}, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->access$500(Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;Z)V

    goto :goto_0

    :pswitch_4
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog$1;->this$0:Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;

    # invokes: Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->changeSubtitleFontSize(Z)V
    invoke-static {v0, v2}, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->access$600(Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;Z)V

    goto :goto_0

    :pswitch_5
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog$1;->this$0:Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;

    # invokes: Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->changeSubtitleColor(Z)V
    invoke-static {v0, v2}, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->access$700(Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;Z)V

    goto :goto_0

    :pswitch_6
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog$1;->this$0:Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;

    # invokes: Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->changeSubtitleClip()V
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->access$800(Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;)V

    goto :goto_0

    :pswitch_7
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog$1;->this$0:Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;

    # invokes: Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->changeSubtitlePostion()V
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->access$900(Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;)V

    goto :goto_0

    :pswitch_8
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog$1;->this$0:Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog$1;->this$0:Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;

    # getter for: Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->subtitleTime:I
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->access$1000(Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;)I

    move-result v1

    # invokes: Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->changeTimeOrder(I)V
    invoke-static {v0, v1}, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->access$1100(Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method
