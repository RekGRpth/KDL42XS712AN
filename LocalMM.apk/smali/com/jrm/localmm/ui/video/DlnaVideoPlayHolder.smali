.class public Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;
.super Ljava/lang/Object;
.source "DlnaVideoPlayHolder.java"


# instance fields
.field protected bt_videoPlay:Landroid/widget/ImageView;

.field protected bt_videoRewind:Landroid/widget/ImageView;

.field protected bt_videoTime:Landroid/widget/ImageView;

.field protected bt_videoWind:Landroid/widget/ImageView;

.field protected current_time_video:Landroid/widget/TextView;

.field protected mVideoPlayView:Lcom/jrm/localmm/business/video/VideoPlayView;

.field protected mbNotSeek:Z

.field protected playControlLayout:Landroid/widget/LinearLayout;

.field protected total_time_video:Landroid/widget/TextView;

.field private videoPlayActivity:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

.field protected videoPlayerTextView:Lcom/jrm/localmm/ui/video/BorderTextViews;

.field protected videoSeekBar:Landroid/widget/SeekBar;

.field protected video_name:Landroid/widget/TextView;

.field protected video_playSpeed:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;)V
    .locals 1
    .param p1    # Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->mbNotSeek:Z

    iput-object p1, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->videoPlayActivity:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->findViews()V

    return-void
.end method


# virtual methods
.method public SetVideoPlaySelect(ZZ)V
    .locals 2
    .param p1    # Z
    .param p2    # Z

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->bt_videoPlay:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setFocusable(Z)V

    if-eqz p1, :cond_1

    invoke-virtual {p0, p2}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->setAllUnSelect(Z)V

    const/4 v0, 0x2

    sput v0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->state:I

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->bt_videoPlay:Landroid/widget/ImageView;

    const v1, 0x7f02006a    # com.jrm.localmm.R.drawable.player_icon_pause_focus

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->bt_videoPlay:Landroid/widget/ImageView;

    const v1, 0x7f02006c    # com.jrm.localmm.R.drawable.player_icon_play_focus

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    :cond_1
    if-eqz p2, :cond_2

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->bt_videoPlay:Landroid/widget/ImageView;

    const v1, 0x7f020069    # com.jrm.localmm.R.drawable.player_icon_pause

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->bt_videoPlay:Landroid/widget/ImageView;

    const v1, 0x7f02006b    # com.jrm.localmm.R.drawable.player_icon_play

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method public SetVideoRewindSelect(Z)V
    .locals 2
    .param p1    # Z

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->bt_videoRewind:Landroid/widget/ImageView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->bt_videoRewind:Landroid/widget/ImageView;

    const v1, 0x7f020073    # com.jrm.localmm.R.drawable.player_icon_rewind_focus

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->bt_videoRewind:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->bt_videoRewind:Landroid/widget/ImageView;

    const v1, 0x7f020072    # com.jrm.localmm.R.drawable.player_icon_rewind

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method public SetVideoTimeSelect(Z)V
    .locals 2
    .param p1    # Z

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->bt_videoTime:Landroid/widget/ImageView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->bt_videoTime:Landroid/widget/ImageView;

    const v1, 0x7f02007d    # com.jrm.localmm.R.drawable.player_icon_time_focus

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->bt_videoTime:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->bt_videoTime:Landroid/widget/ImageView;

    const v1, 0x7f02007c    # com.jrm.localmm.R.drawable.player_icon_time

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method public SetVideoWindSelect(Z)V
    .locals 2
    .param p1    # Z

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->bt_videoWind:Landroid/widget/ImageView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->bt_videoWind:Landroid/widget/ImageView;

    const v1, 0x7f020081    # com.jrm.localmm.R.drawable.player_icon_wind_focus

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->bt_videoWind:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->bt_videoWind:Landroid/widget/ImageView;

    const v1, 0x7f020080    # com.jrm.localmm.R.drawable.player_icon_wind

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method findViews()V
    .locals 2

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->videoPlayActivity:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    const v1, 0x7f08001b    # com.jrm.localmm.R.id.dlna_video_suspension_layout

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->playControlLayout:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->videoPlayActivity:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    const v1, 0x7f08001f    # com.jrm.localmm.R.id.dlna_video_play

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->bt_videoPlay:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->videoPlayActivity:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    const v1, 0x7f08001e    # com.jrm.localmm.R.id.dlna_video_rewind

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->bt_videoRewind:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->videoPlayActivity:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    const v1, 0x7f080020    # com.jrm.localmm.R.id.dlna_video_wind

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->bt_videoWind:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->videoPlayActivity:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    const v1, 0x7f080021    # com.jrm.localmm.R.id.dlna_video_time

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->bt_videoTime:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->videoPlayActivity:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    const v1, 0x7f080022    # com.jrm.localmm.R.id.dlna_control_timer_current

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->current_time_video:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->videoPlayActivity:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    const v1, 0x7f080024    # com.jrm.localmm.R.id.dlna_control_timer_total

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->total_time_video:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->videoPlayActivity:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    const v1, 0x7f080023    # com.jrm.localmm.R.id.dlna_progress

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->videoSeekBar:Landroid/widget/SeekBar;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->videoPlayActivity:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    const v1, 0x7f08001c    # com.jrm.localmm.R.id.dlna_video_name_display

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->video_name:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->videoPlayActivity:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    const v1, 0x7f08001d    # com.jrm.localmm.R.id.dlna_video_play_speed_display

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->video_playSpeed:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->videoPlayActivity:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    const v1, 0x7f080019    # com.jrm.localmm.R.id.dlnaVideoPlayerSurfaceView

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/jrm/localmm/business/video/VideoPlayView;

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->mVideoPlayView:Lcom/jrm/localmm/business/video/VideoPlayView;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->videoPlayActivity:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    const v1, 0x7f08001a    # com.jrm.localmm.R.id.dlna_video_player_text

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/jrm/localmm/ui/video/BorderTextViews;

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->videoPlayerTextView:Lcom/jrm/localmm/ui/video/BorderTextViews;

    return-void
.end method

.method public processLeftKey(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v2, 0x0

    const/4 v1, 0x1

    sget v0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->state:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    return v1

    :pswitch_0
    const/4 v0, 0x4

    sput v0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->state:I

    invoke-virtual {p0, v2}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->SetVideoRewindSelect(Z)V

    invoke-virtual {p0, v1}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->SetVideoTimeSelect(Z)V

    goto :goto_0

    :pswitch_1
    sput v1, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->state:I

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->mVideoPlayView:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-virtual {v0}, Lcom/jrm/localmm/business/video/VideoPlayView;->isPlaying()Z

    move-result v0

    invoke-virtual {p0, v2, v0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->SetVideoPlaySelect(ZZ)V

    invoke-virtual {p0, v1}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->SetVideoRewindSelect(Z)V

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x2

    sput v0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->state:I

    invoke-virtual {p0, v2}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->SetVideoWindSelect(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->mVideoPlayView:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-virtual {v0}, Lcom/jrm/localmm/business/video/VideoPlayView;->isPlaying()Z

    move-result v0

    invoke-virtual {p0, v1, v0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->SetVideoPlaySelect(ZZ)V

    goto :goto_0

    :pswitch_3
    const/4 v0, 0x3

    sput v0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->state:I

    invoke-virtual {p0, v2}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->SetVideoTimeSelect(Z)V

    invoke-virtual {p0, v1}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->SetVideoWindSelect(Z)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public processRightKey(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v2, 0x0

    const/4 v1, 0x1

    sget v0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->state:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    return v1

    :pswitch_0
    const/4 v0, 0x2

    sput v0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->state:I

    invoke-virtual {p0, v2}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->SetVideoRewindSelect(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->mVideoPlayView:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-virtual {v0}, Lcom/jrm/localmm/business/video/VideoPlayView;->isPlaying()Z

    move-result v0

    invoke-virtual {p0, v1, v0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->SetVideoPlaySelect(ZZ)V

    goto :goto_0

    :pswitch_1
    const/4 v0, 0x3

    sput v0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->state:I

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->mVideoPlayView:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-virtual {v0}, Lcom/jrm/localmm/business/video/VideoPlayView;->isPlaying()Z

    move-result v0

    invoke-virtual {p0, v2, v0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->SetVideoPlaySelect(ZZ)V

    invoke-virtual {p0, v1}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->SetVideoWindSelect(Z)V

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x4

    sput v0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->state:I

    invoke-virtual {p0, v2}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->SetVideoWindSelect(Z)V

    invoke-virtual {p0, v1}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->SetVideoTimeSelect(Z)V

    goto :goto_0

    :pswitch_3
    sput v1, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->state:I

    invoke-virtual {p0, v2}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->SetVideoTimeSelect(Z)V

    invoke-virtual {p0, v1}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->SetVideoRewindSelect(Z)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public setAllUnSelect(Z)V
    .locals 1
    .param p1    # Z

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->SetVideoRewindSelect(Z)V

    invoke-virtual {p0, v0, p1}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->SetVideoPlaySelect(ZZ)V

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->SetVideoWindSelect(Z)V

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->SetVideoTimeSelect(Z)V

    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1    # Landroid/view/View$OnClickListener;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->bt_videoPlay:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->bt_videoRewind:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->bt_videoWind:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->bt_videoTime:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    return-void
.end method

.method public setPlaySpeed(I)V
    .locals 5
    .param p1    # I

    const/16 v1, 0x40

    if-ge p1, v1, :cond_0

    const/16 v1, -0x40

    if-le p1, v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "*%d "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->videoPlayActivity:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    const v3, 0x7f060092    # com.jrm.localmm.R.string.x_times_speed

    invoke-virtual {v2, v3}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->video_playSpeed:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method public setSubTitleText(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->videoPlayerTextView:Lcom/jrm/localmm/ui/video/BorderTextViews;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->videoPlayerTextView:Lcom/jrm/localmm/ui/video/BorderTextViews;

    invoke-virtual {v0, p1}, Lcom/jrm/localmm/ui/video/BorderTextViews;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method public setVideoName(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->video_name:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/DlnaVideoPlayHolder;->videoPlayActivity:Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;

    const v3, 0x7f060063    # com.jrm.localmm.R.string.playing

    invoke-virtual {v2, v3}, Lcom/jrm/localmm/ui/video/DlnaVideoPlayActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
