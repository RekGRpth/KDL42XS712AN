.class Lcom/jrm/localmm/ui/video/VideoPlayABDialog$1;
.super Landroid/os/Handler;
.source "VideoPlayABDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jrm/localmm/ui/video/VideoPlayABDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/localmm/ui/video/VideoPlayABDialog;


# direct methods
.method constructor <init>(Lcom/jrm/localmm/ui/video/VideoPlayABDialog;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog$1;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1    # Landroid/os/Message;

    const/16 v2, 0x8

    const/4 v1, 0x0

    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog$1;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    iput-boolean v1, v0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->bFlag:Z

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog$1;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    iput-boolean v1, v0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->aFlag:Z

    sput-boolean v1, Lcom/jrm/localmm/util/Constants;->abFlag:Z

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog$1;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->videoPlayHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayABDialog;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v0

    iget-object v0, v0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_playA:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog$1;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->videoPlayHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayABDialog;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v0

    iget-object v0, v0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_playB:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog$1;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->dismiss()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog$1;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->access$100(Lcom/jrm/localmm/ui/video/VideoPlayABDialog;)Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->hideControlDelay()V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog$1;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    iget v1, p1, Landroid/os/Message;->arg1:I

    # invokes: Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->setABVisible(I)V
    invoke-static {v0, v1}, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->access$200(Lcom/jrm/localmm/ui/video/VideoPlayABDialog;I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
