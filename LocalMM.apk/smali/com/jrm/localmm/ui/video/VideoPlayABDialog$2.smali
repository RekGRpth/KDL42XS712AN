.class Lcom/jrm/localmm/ui/video/VideoPlayABDialog$2;
.super Ljava/lang/Object;
.source "VideoPlayABDialog.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->setListeners()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/localmm/ui/video/VideoPlayABDialog;


# direct methods
.method constructor <init>(Lcom/jrm/localmm/ui/video/VideoPlayABDialog;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog$2;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1    # Landroid/view/View;

    const/4 v6, 0x1

    const/4 v5, 0x0

    sget-boolean v2, Lcom/jrm/localmm/util/Constants;->abFlag:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog$2;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;
    invoke-static {v2}, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->access$100(Lcom/jrm/localmm/ui/video/VideoPlayABDialog;)Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->localPause(Z)V

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog$2;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->videoPlayHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v2}, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayABDialog;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView()Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v2

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog$2;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->videoPlayHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v3}, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayABDialog;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView()Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/jrm/localmm/business/video/VideoPlayView;->getCurrentPosition()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/jrm/localmm/business/video/VideoPlayView;->seekTo(I)V

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog$2;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;
    invoke-static {v2}, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->access$100(Lcom/jrm/localmm/ui/video/VideoPlayABDialog;)Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->localResume(Z)V

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog$2;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog$2;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->videoPlayHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v3}, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayABDialog;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView()Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/jrm/localmm/business/video/VideoPlayView;->getCurrentPosition()I

    move-result v3

    iput v3, v2, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->postionA:I

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog$2;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    const-string v3, "APOSITION"

    iget-object v4, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog$2;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    iget v4, v4, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->postionA:I

    # invokes: Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->setPosition(Ljava/lang/String;I)V
    invoke-static {v2, v3, v4}, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->access$300(Lcom/jrm/localmm/ui/video/VideoPlayABDialog;Ljava/lang/String;I)V

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog$2;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->videoPlayHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v2}, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayABDialog;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView()Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/jrm/localmm/business/video/VideoPlayView;->getCurrentPosition()I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog$2;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->total:F
    invoke-static {v3}, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->access$400(Lcom/jrm/localmm/ui/video/VideoPlayABDialog;)F

    move-result v3

    div-float/2addr v2, v3

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog$2;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->videoPlayHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v3}, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayABDialog;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v3

    iget-object v3, v3, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->videoSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v3}, Landroid/widget/SeekBar;->getWidth()I

    move-result v3

    int-to-float v3, v3

    mul-float v0, v2, v3

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog$2;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    iput-boolean v6, v2, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->aFlag:Z

    const/16 v2, 0x11

    sput v2, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->state:I

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog$2;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->videoPlayHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v2}, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayABDialog;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v2

    iget-object v2, v2, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_playA:Landroid/widget/ImageView;

    float-to-int v3, v0

    const/16 v4, 0xf

    invoke-virtual {v2, v3, v5, v5, v4}, Landroid/widget/ImageView;->setPadding(IIII)V

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog$2;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->videoPlayHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v2}, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayABDialog;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v2

    iget-object v2, v2, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_playA:Landroid/widget/ImageView;

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    sput-boolean v6, Lcom/jrm/localmm/util/Constants;->aFlag:Z

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog$2;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;
    invoke-static {v2}, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->access$100(Lcom/jrm/localmm/ui/video/VideoPlayABDialog;)Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    move-result-object v2

    const v3, 0x7f060094    # com.jrm.localmm.R.string.open_ab_repeat

    invoke-virtual {v2, v3}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog$2;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    invoke-virtual {v2, v1}, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->setToastMessage(Ljava/lang/String;)V

    goto :goto_0
.end method
