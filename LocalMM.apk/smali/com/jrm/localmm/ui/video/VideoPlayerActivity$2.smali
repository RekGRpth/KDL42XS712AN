.class Lcom/jrm/localmm/ui/video/VideoPlayerActivity$2;
.super Landroid/os/Handler;
.source "VideoPlayerActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jrm/localmm/ui/video/VideoPlayerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;


# direct methods
.method constructor <init>(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$2;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7
    .param p1    # Landroid/os/Message;

    const/4 v6, 0x0

    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$2;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v3}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getViewId()I

    move-result v1

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$2;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v3}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView(I)Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/jrm/localmm/business/video/VideoPlayView;->getMMediaPlayer()Lcom/mstar/android/media/MMediaPlayer;

    move-result-object v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$2;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v3, v6}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->localPauseFromDualSwitch(Z)V

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$2;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v3}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->openOrCloseDualDecode(Z)V

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$2;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v3, v6}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->localResumeFromDualSwitch(Z)V

    :cond_0
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$2;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v3}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->reset()V

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$2;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    iget-object v3, v3, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->video_position:[I

    add-int/lit8 v4, v1, -0x1

    const-string v5, "index"

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v5

    aput v5, v3, v4

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$2;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    iget-object v3, v3, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->video_position:[I

    add-int/lit8 v4, v1, -0x1

    aget v3, v3, v4

    if-ltz v3, :cond_1

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$2;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    iget-object v3, v3, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->video_position:[I

    add-int/lit8 v4, v1, -0x1

    aget v3, v3, v4

    iget-object v4, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$2;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    iget-object v4, v4, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-le v3, v4, :cond_2

    :cond_1
    iget-object v3, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$2;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    iget-object v3, v3, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->video_position:[I

    add-int/lit8 v4, v1, -0x1

    aput v6, v3, v4

    :cond_2
    iget-object v3, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$2;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    iget-object v3, v3, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayList:Ljava/util/List;

    iget-object v4, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$2;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    iget-object v4, v4, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->video_position:[I

    add-int/lit8 v5, v1, -0x1

    aget v4, v4, v5

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/jrm/localmm/business/data/BaseData;

    invoke-virtual {v3}, Lcom/jrm/localmm/business/data/BaseData;->getName()Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$2;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$2;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    iget-object v3, v3, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayList:Ljava/util/List;

    iget-object v5, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$2;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    iget-object v5, v5, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->video_position:[I

    add-int/lit8 v6, v1, -0x1

    aget v5, v5, v6

    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/jrm/localmm/business/data/BaseData;

    invoke-virtual {v3}, Lcom/jrm/localmm/business/data/BaseData;->getPath()Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->InitVideoPlayer(Ljava/lang/String;I)V
    invoke-static {v4, v3, v1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$1600(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;Ljava/lang/String;I)V

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$2;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v3}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoName(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$2;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v3}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v3

    iget-object v4, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$2;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    iget-object v4, v4, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->video_position:[I

    add-int/lit8 v5, v1, -0x1

    aget v4, v4, v5

    add-int/lit8 v4, v4, 0x1

    iget-object v5, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$2;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    iget-object v5, v5, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayList:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    invoke-virtual {v3, v4, v5}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoListText(II)V

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$2;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # invokes: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->showController()V
    invoke-static {v3}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$1700(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)V

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$2;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v3}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->hideControlDelay()V

    return-void
.end method
