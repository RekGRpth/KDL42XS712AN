.class Lcom/jrm/localmm/ui/video/VideoPlayABDialog$3;
.super Ljava/lang/Object;
.source "VideoPlayABDialog.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->setListeners()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/localmm/ui/video/VideoPlayABDialog;


# direct methods
.method constructor <init>(Lcom/jrm/localmm/ui/video/VideoPlayABDialog;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog$3;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8
    .param p1    # Landroid/view/View;

    const v5, 0x7f060095    # com.jrm.localmm.R.string.set_point_a_first

    const/4 v7, 0x1

    const/4 v6, 0x0

    sget-boolean v3, Lcom/jrm/localmm/util/Constants;->aFlag:Z

    if-eqz v3, :cond_1

    sget-boolean v3, Lcom/jrm/localmm/util/Constants;->abFlag:Z

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog$3;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->videoPlayHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v3}, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayABDialog;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView()Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/jrm/localmm/business/video/VideoPlayView;->getCurrentPosition()I

    move-result v1

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog$3;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->videoPlayHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v3}, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayABDialog;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView()Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/jrm/localmm/business/video/VideoPlayView;->seekTo(I)V

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog$3;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    iget-object v4, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog$3;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->videoPlayHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v4}, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayABDialog;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView()Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v4

    invoke-virtual {v4}, Lcom/jrm/localmm/business/video/VideoPlayView;->getCurrentPosition()I

    move-result v4

    iput v4, v3, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->postionB:I

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog$3;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->videoPlayHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v3}, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayABDialog;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView()Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/jrm/localmm/business/video/VideoPlayView;->getCurrentPosition()I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog$3;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->total:F
    invoke-static {v4}, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->access$400(Lcom/jrm/localmm/ui/video/VideoPlayABDialog;)F

    move-result v4

    div-float/2addr v3, v4

    iget-object v4, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog$3;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->videoPlayHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v4}, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayABDialog;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v4

    iget-object v4, v4, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->videoSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v4}, Landroid/widget/SeekBar;->getWidth()I

    move-result v4

    int-to-float v4, v4

    mul-float v0, v3, v4

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog$3;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    iget-object v3, v3, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->sharedata:Landroid/content/SharedPreferences;

    const-string v4, "APOSITION"

    invoke-interface {v3, v4, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v3

    add-int/lit16 v3, v3, 0x3e8

    iget-object v4, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog$3;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    iget v4, v4, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->postionB:I

    if-ge v3, v4, :cond_0

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog$3;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    const-string v4, "bPOSITION"

    iget-object v5, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog$3;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    iget v5, v5, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->postionB:I

    # invokes: Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->setPosition(Ljava/lang/String;I)V
    invoke-static {v3, v4, v5}, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->access$300(Lcom/jrm/localmm/ui/video/VideoPlayABDialog;Ljava/lang/String;I)V

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog$3;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->videoPlayHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v3}, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayABDialog;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v3

    iget-object v3, v3, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_playB:Landroid/widget/ImageView;

    float-to-int v4, v0

    const/16 v5, 0xc

    invoke-virtual {v3, v4, v6, v6, v5}, Landroid/widget/ImageView;->setPadding(IIII)V

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog$3;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->videoPlayHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v3}, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayABDialog;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v3

    iget-object v3, v3, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_playB:Landroid/widget/ImageView;

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog$3;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    iput-boolean v7, v3, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->bFlag:Z

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog$3;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->videoPlayHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v3}, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayABDialog;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v3

    iget-object v4, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog$3;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->videoPlayHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v4}, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayABDialog;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView()Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v4

    invoke-virtual {v4}, Lcom/jrm/localmm/business/video/VideoPlayView;->isPlaying()Z

    move-result v4

    invoke-virtual {v3, v4}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setAllUnSelect(Z)V

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog$3;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->videoPlayHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v3}, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayABDialog;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v3

    iget-object v3, v3, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->videoSeekBar:Landroid/widget/SeekBar;

    iget-object v4, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog$3;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    iget-object v4, v4, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->sharedata:Landroid/content/SharedPreferences;

    const-string v5, "APOSITION"

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/SeekBar;->setProgress(I)V

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog$3;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->videoPlayHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v3}, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayABDialog;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView()Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v3

    iget-object v4, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog$3;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    iget-object v4, v4, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->sharedata:Landroid/content/SharedPreferences;

    const-string v5, "APOSITION"

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/jrm/localmm/business/video/VideoPlayView;->seekTo(I)V

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog$3;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;
    invoke-static {v3}, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->access$100(Lcom/jrm/localmm/ui/video/VideoPlayABDialog;)Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    move-result-object v3

    invoke-virtual {v3, v6}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->localPause(Z)V

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog$3;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;
    invoke-static {v3}, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->access$100(Lcom/jrm/localmm/ui/video/VideoPlayABDialog;)Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    move-result-object v3

    invoke-virtual {v3, v6}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->localResume(Z)V

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog$3;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->videoPlayHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v3}, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayABDialog;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v3

    invoke-virtual {v3, v7}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoPlayABSelect(Z)V

    :goto_0
    return-void

    :cond_0
    iget-object v3, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog$3;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;
    invoke-static {v3}, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->access$100(Lcom/jrm/localmm/ui/video/VideoPlayABDialog;)Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    move-result-object v3

    invoke-virtual {v3, v5}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog$3;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    invoke-virtual {v3, v2}, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->setToastMessage(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    iget-object v3, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog$3;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;
    invoke-static {v3}, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->access$100(Lcom/jrm/localmm/ui/video/VideoPlayABDialog;)Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    move-result-object v3

    invoke-virtual {v3, v5}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog$3;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    invoke-virtual {v3, v2}, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->setToastMessage(Ljava/lang/String;)V

    goto :goto_0
.end method
