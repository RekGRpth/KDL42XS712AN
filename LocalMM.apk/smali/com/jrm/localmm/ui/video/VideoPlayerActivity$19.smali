.class Lcom/jrm/localmm/ui/video/VideoPlayerActivity$19;
.super Landroid/content/BroadcastReceiver;
.source "VideoPlayerActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jrm/localmm/ui/video/VideoPlayerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;


# direct methods
.method constructor <init>(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$19;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const/4 v7, 0x0

    const/4 v6, 0x0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v3, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    const-string v3, "VideoPlayActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "DiskChangeReceiver: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    const-string v4, "VideoPlayActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "DiskChangeReceiver: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$19;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    iget-object v3, v3, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayList:Ljava/util/List;

    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/jrm/localmm/business/data/BaseData;

    invoke-virtual {v3}, Lcom/jrm/localmm/business/data/BaseData;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$19;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    iget-object v3, v3, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayList:Ljava/util/List;

    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/jrm/localmm/business/data/BaseData;

    invoke-virtual {v3}, Lcom/jrm/localmm/business/data/BaseData;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$19;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$500(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Landroid/os/Handler;

    move-result-object v3

    const/16 v4, 0xe

    invoke-virtual {v3, v4}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$19;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v3}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v3

    iget-object v3, v3, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->mVideoPlayViewOne:Lcom/jrm/localmm/business/video/VideoPlayView;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$19;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v3}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v3

    iget-object v3, v3, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->mVideoPlayViewOne:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-virtual {v3}, Lcom/jrm/localmm/business/video/VideoPlayView;->stopPlayer()V

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$19;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v3}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v3

    iget-object v3, v3, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->mVideoPlayViewOne:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-virtual {v3, v7}, Lcom/jrm/localmm/business/video/VideoPlayView;->setPlayerCallbackListener(Lcom/jrm/localmm/business/video/VideoPlayView$playerCallback;)V

    :cond_0
    iget-object v3, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$19;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v3}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v3

    iget-object v3, v3, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->mVideoPlayViewTwo:Lcom/jrm/localmm/business/video/VideoPlayView;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$19;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v3}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v3

    iget-object v3, v3, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->mVideoPlayViewTwo:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-virtual {v3}, Lcom/jrm/localmm/business/video/VideoPlayView;->stopPlayer()V

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$19;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v3}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v3

    iget-object v3, v3, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->mVideoPlayViewTwo:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-virtual {v3, v7}, Lcom/jrm/localmm/business/video/VideoPlayView;->setPlayerCallbackListener(Lcom/jrm/localmm/business/video/VideoPlayView$playerCallback;)V

    :cond_1
    iget-object v3, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$19;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    const v4, 0x7f06003e    # com.jrm.localmm.R.string.disk_eject

    invoke-static {v3, v4, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    const/16 v3, 0x11

    invoke-virtual {v2, v3, v6, v6}, Landroid/widget/Toast;->setGravity(III)V

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$19;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v3}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->finish()V

    :cond_2
    return-void
.end method
