.class public Lcom/jrm/localmm/ui/video/VideoStartTimeDialog;
.super Landroid/app/Dialog;
.source "VideoStartTimeDialog.java"


# instance fields
.field private bTime:J

.field private bufferTime:Landroid/widget/TextView;

.field private fTime:J

.field private firstFrame:Landroid/widget/TextView;

.field private mName:Ljava/lang/String;

.field private videoName:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;JJLjava/lang/String;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # J
    .param p4    # J
    .param p6    # Ljava/lang/String;

    invoke-direct {p0, p1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    iput-wide p2, p0, Lcom/jrm/localmm/ui/video/VideoStartTimeDialog;->bTime:J

    iput-wide p4, p0, Lcom/jrm/localmm/ui/video/VideoStartTimeDialog;->fTime:J

    iput-object p6, p0, Lcom/jrm/localmm/ui/video/VideoStartTimeDialog;->mName:Ljava/lang/String;

    return-void
.end method

.method private UpdateViews()V
    .locals 4

    const v0, 0x7f0800d2    # com.jrm.localmm.R.id.buffer_time

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/video/VideoStartTimeDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/VideoStartTimeDialog;->bufferTime:Landroid/widget/TextView;

    const v0, 0x7f0800d5    # com.jrm.localmm.R.id.first_frame

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/video/VideoStartTimeDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/VideoStartTimeDialog;->firstFrame:Landroid/widget/TextView;

    const v0, 0x7f0800cf    # com.jrm.localmm.R.id.video_name

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/video/VideoStartTimeDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/VideoStartTimeDialog;->videoName:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoStartTimeDialog;->videoName:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoStartTimeDialog;->mName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoStartTimeDialog;->bufferTime:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v2, p0, Lcom/jrm/localmm/ui/video/VideoStartTimeDialog;->bTime:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ms"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoStartTimeDialog;->firstFrame:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v2, p0, Lcom/jrm/localmm/ui/video/VideoStartTimeDialog;->fTime:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ms"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 9
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    const v5, 0x7f030023    # com.jrm.localmm.R.layout.video_starttime_show

    invoke-virtual {p0, v5}, Lcom/jrm/localmm/ui/video/VideoStartTimeDialog;->setContentView(I)V

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoStartTimeDialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v5

    invoke-interface {v5}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    const/4 v5, 0x0

    invoke-virtual {v2, v5}, Landroid/view/Window;->setTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v5

    int-to-double v5, v5

    const-wide v7, 0x3fd3333333333333L    # 0.3

    mul-double/2addr v5, v7

    double-to-int v3, v5

    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v5

    int-to-double v5, v5

    const-wide v7, 0x3fe3333333333333L    # 0.6

    mul-double/2addr v5, v7

    double-to-int v1, v5

    invoke-virtual {v2, v3, v1}, Landroid/view/Window;->setLayout(II)V

    const/16 v5, 0x11

    invoke-virtual {v2, v5}, Landroid/view/Window;->setGravity(I)V

    invoke-virtual {v2}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v4

    const/high16 v5, 0x3f800000    # 1.0f

    iput v5, v4, Landroid/view/WindowManager$LayoutParams;->screenBrightness:F

    invoke-virtual {v2, v4}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    const v5, 0x106000d    # android.R.color.transparent

    invoke-virtual {v2, v5}, Landroid/view/Window;->setBackgroundDrawableResource(I)V

    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/VideoStartTimeDialog;->UpdateViews()V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v0, 0x1

    return v0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    invoke-super {p0}, Landroid/app/Dialog;->cancel()V

    const/4 v0, 0x1

    return v0
.end method
