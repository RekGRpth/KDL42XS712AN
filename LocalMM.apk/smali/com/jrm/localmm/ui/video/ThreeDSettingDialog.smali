.class public Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;
.super Landroid/app/Dialog;
.source "ThreeDSettingDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/jrm/localmm/ui/video/ThreeDSettingDialog$ViewHolder;,
        Lcom/jrm/localmm/ui/video/ThreeDSettingDialog$MyAdapter;,
        Lcom/jrm/localmm/ui/video/ThreeDSettingDialog$ListItemOnClickListener;
    }
.end annotation


# instance fields
.field private context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

.field public holder:Lcom/jrm/localmm/ui/video/ThreeDSettingDialog$ViewHolder;

.field private mAdapter:Lcom/jrm/localmm/ui/video/ThreeDSettingDialog$MyAdapter;

.field private mItemName:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private selectPosition:I

.field private threeDFormat:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private video3DSettingList:Landroid/widget/ListView;

.field private videoPlaySettingDialog:Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;


# direct methods
.method public constructor <init>(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;)V
    .locals 1
    .param p1    # Lcom/jrm/localmm/ui/video/VideoPlayerActivity;
    .param p2    # Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;

    invoke-direct {p0, p1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;->mItemName:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;->threeDFormat:Ljava/util/ArrayList;

    iput-object p1, p0, Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    iput-object p2, p0, Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;->videoPlaySettingDialog:Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;

    return-void
.end method

.method static synthetic access$000(Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;)I
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;

    iget v0, p0, Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;->selectPosition:I

    return v0
.end method

.method static synthetic access$002(Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;I)I
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;
    .param p1    # I

    iput p1, p0, Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;->selectPosition:I

    return p1
.end method

.method static synthetic access$100(Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;)Lcom/jrm/localmm/ui/video/ThreeDSettingDialog$MyAdapter;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;->mAdapter:Lcom/jrm/localmm/ui/video/ThreeDSettingDialog$MyAdapter;

    return-object v0
.end method

.method static synthetic access$200(Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;I)V
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;->set3DMode(I)V

    return-void
.end method

.method static synthetic access$300(Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;)Ljava/util/ArrayList;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;->mItemName:Ljava/util/ArrayList;

    return-object v0
.end method

.method private initView()V
    .locals 4

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;->mItemName:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f060097    # com.jrm.localmm.R.string.video_3D_auto

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;->mItemName:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f060098    # com.jrm.localmm.R.string.video_3D_top_bottom

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;->mItemName:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f060099    # com.jrm.localmm.R.string.video_3D_side_by_side

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;->mItemName:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f06009a    # com.jrm.localmm.R.string.video_3D_line_alternative

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;->mItemName:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f06009b    # com.jrm.localmm.R.string.video_3D_frame_packing

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;->mItemName:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f06009d    # com.jrm.localmm.R.string.video_3D_2dto3d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;->threeDFormat:Ljava/util/ArrayList;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;->E_ThreeD_Video_DISPLAYFORMAT_AUTO:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;->threeDFormat:Ljava/util/ArrayList;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;->E_ThreeD_Video_DISPLAYFORMAT_TOP_BOTTOM:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;->threeDFormat:Ljava/util/ArrayList;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;->E_ThreeD_Video_DISPLAYFORMAT_SIDE_BY_SIDE:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;->threeDFormat:Ljava/util/ArrayList;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;->E_ThreeD_Video_DISPLAYFORMAT_LINE_ALTERNATIVE:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;->threeDFormat:Ljava/util/ArrayList;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;->E_ThreeD_Video_DISPLAYFORMAT_FRAME_PACKING:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;->threeDFormat:Ljava/util/ArrayList;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;->E_ThreeD_Video_DISPLAYFORMAT_2DTO3D:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const v1, 0x7f0800d9    # com.jrm.localmm.R.id.video3DList

    invoke-virtual {p0, v1}, Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    iput-object v1, p0, Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;->video3DSettingList:Landroid/widget/ListView;

    new-instance v1, Lcom/jrm/localmm/ui/video/ThreeDSettingDialog$MyAdapter;

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-direct {v1, p0, v2}, Lcom/jrm/localmm/ui/video/ThreeDSettingDialog$MyAdapter;-><init>(Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;Landroid/content/Context;)V

    iput-object v1, p0, Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;->mAdapter:Lcom/jrm/localmm/ui/video/ThreeDSettingDialog$MyAdapter;

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;->video3DSettingList:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;->mAdapter:Lcom/jrm/localmm/ui/video/ThreeDSettingDialog$MyAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;->threeDFormat:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    iget-object v1, v1, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->displayFormat:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;->threeDFormat:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-ne v1, v2, :cond_0

    iput v0, p0, Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;->selectPosition:I

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;->video3DSettingList:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setSelection(I)V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;->mAdapter:Lcom/jrm/localmm/ui/video/ThreeDSettingDialog$MyAdapter;

    invoke-virtual {v1}, Lcom/jrm/localmm/ui/video/ThreeDSettingDialog$MyAdapter;->notifyDataSetChanged()V

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;->video3DSettingList:Landroid/widget/ListView;

    new-instance v2, Lcom/jrm/localmm/ui/video/ThreeDSettingDialog$ListItemOnClickListener;

    invoke-direct {v2, p0}, Lcom/jrm/localmm/ui/video/ThreeDSettingDialog$ListItemOnClickListener;-><init>(Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;)V

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    return-void
.end method

.method private set3DMode(I)V
    .locals 2
    .param p1    # I

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;->threeDFormat:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    iput-object v0, v1, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->displayFormat:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    iget-object v0, v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->s3dSkin:Lcom/mstar/android/tv/TvS3DManager;

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    iget-object v1, v1, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->displayFormat:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    invoke-virtual {v0, v1}, Lcom/mstar/android/tv/TvS3DManager;->setDisplayFormat(Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;)Z

    return-void
.end method


# virtual methods
.method public onBackPressed()V
    .locals 1

    invoke-super {p0}, Landroid/app/Dialog;->onBackPressed()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;->videoPlaySettingDialog:Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->show()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 9
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    const v5, 0x7f030025    # com.jrm.localmm.R.layout.video_three_d_setting_dialog

    invoke-virtual {p0, v5}, Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;->setContentView(I)V

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v5

    invoke-interface {v5}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    const/4 v5, 0x0

    invoke-virtual {v2, v5}, Landroid/view/Window;->setTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v5

    int-to-double v5, v5

    const-wide v7, 0x3fd999999999999aL    # 0.4

    mul-double/2addr v5, v7

    double-to-int v3, v5

    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v5

    int-to-double v5, v5

    const-wide v7, 0x3fe6666666666666L    # 0.7

    mul-double/2addr v5, v7

    double-to-int v1, v5

    invoke-virtual {v2, v3, v1}, Landroid/view/Window;->setLayout(II)V

    const/16 v5, 0x11

    invoke-virtual {v2, v5}, Landroid/view/Window;->setGravity(I)V

    invoke-virtual {v2}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    const v5, 0x106000d    # android.R.color.transparent

    invoke-virtual {v2, v5}, Landroid/view/Window;->setBackgroundDrawableResource(I)V

    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;->initView()V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/16 v0, 0x7e

    if-eq v0, p1, :cond_0

    const/16 v0, 0x7f

    if-eq v0, p1, :cond_0

    const/16 v0, 0x57

    if-eq v0, p1, :cond_0

    const/16 v0, 0x58

    if-ne v0, p1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    invoke-super {p0, p1, p2}, Landroid/app/Dialog;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/16 v0, 0x7e

    if-eq v0, p1, :cond_0

    const/16 v0, 0x7f

    if-eq v0, p1, :cond_0

    const/16 v0, 0x57

    if-eq v0, p1, :cond_0

    const/16 v0, 0x58

    if-ne v0, p1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    invoke-super {p0, p1, p2}, Landroid/app/Dialog;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method
