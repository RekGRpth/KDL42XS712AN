.class Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog$3;
.super Landroid/os/Handler;
.source "PlaySettingSubtitleDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;


# direct methods
.method constructor <init>(Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog$3;->this$0:Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 8
    .param p1    # Landroid/os/Message;

    const/4 v7, 0x0

    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    iget-object v4, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog$3;->this$0:Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;

    # getter for: Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->mVideoPlayView:Lcom/jrm/localmm/business/video/VideoPlayView;
    invoke-static {v4}, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->access$1500(Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;)Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v4

    invoke-virtual {v4}, Lcom/jrm/localmm/business/video/VideoPlayView;->getAllSubtitleTrackInfo()Lcom/mstar/android/media/SubtitleTrackInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/media/SubtitleTrackInfo;->getAllSubtitleCount()I

    move-result v3

    const-string v4, "*************"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "***info**"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Lcom/mstar/android/media/SubtitleTrackInfo;->getAllInternalSubtitleCount()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Lcom/mstar/android/media/SubtitleTrackInfo;->getAllSubtitleCount()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-array v2, v3, [Ljava/lang/String;

    invoke-virtual {v1, v2, v7}, Lcom/mstar/android/media/SubtitleTrackInfo;->getSubtitleLanguageType([Ljava/lang/String;Z)V

    if-eqz v3, :cond_1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    const-string v4, "*************"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "*****"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    aget-object v6, v2, v0

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v4, 0x3

    aget-object v5, v2, v7

    iget-object v6, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog$3;->this$0:Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;

    # getter for: Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->viewId:I
    invoke-static {v6}, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->access$1600(Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;)I

    move-result v6

    invoke-static {v4, v5, v6}, Lcom/jrm/localmm/util/Tools;->setSubtitleSettingOpt(ILjava/lang/String;I)V

    iget-object v4, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog$3;->this$0:Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;

    # getter for: Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->viewId:I
    invoke-static {v4}, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->access$1600(Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;)I

    move-result v4

    invoke-static {v2, v4}, Lcom/jrm/localmm/util/Tools;->setSubtitleLanguageType([Ljava/lang/String;I)V

    :goto_1
    iget-object v4, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog$3;->this$0:Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;

    # getter for: Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->adapter:Lcom/jrm/localmm/business/adapter/SubtitleSettingListAdapter;
    invoke-static {v4}, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->access$1200(Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;)Lcom/jrm/localmm/business/adapter/SubtitleSettingListAdapter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/jrm/localmm/business/adapter/SubtitleSettingListAdapter;->notifyDataSetChanged()V

    :goto_2
    return-void

    :cond_1
    const/4 v4, 0x0

    iget-object v5, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog$3;->this$0:Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;

    # getter for: Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->viewId:I
    invoke-static {v5}, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->access$1600(Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;)I

    move-result v5

    invoke-static {v4, v5}, Lcom/jrm/localmm/util/Tools;->setSubtitleLanguageType([Ljava/lang/String;I)V

    iget-object v4, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog$3;->this$0:Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;

    # operator++ for: Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->getInfoNum:I
    invoke-static {v4}, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->access$1708(Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;)I

    iget-object v4, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog$3;->this$0:Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;

    # getter for: Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->getInfoNum:I
    invoke-static {v4}, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->access$1700(Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;)I

    move-result v4

    const/4 v5, 0x2

    if-gt v4, v5, :cond_2

    iget-object v4, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog$3;->this$0:Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;

    # getter for: Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->mHandler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->access$1800(Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;)Landroid/os/Handler;

    move-result-object v4

    const-wide/16 v5, 0x3e8

    invoke-virtual {v4, v7, v5, v6}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_2

    :cond_2
    iget-object v4, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog$3;->this$0:Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;

    # setter for: Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->getInfoNum:I
    invoke-static {v4, v7}, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->access$1702(Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;I)I

    goto :goto_1
.end method
