.class public Lcom/jrm/localmm/ui/video/DetailInfoDialog;
.super Landroid/app/Dialog;
.source "DetailInfoDialog.java"


# instance fields
.field private audio_codec_tv:Landroid/widget/TextView;

.field private current_position:I

.field private duration_tv:Landroid/widget/TextView;

.field private file_format_tv:Landroid/widget/TextView;

.field private file_name_tv:Landroid/widget/TextView;

.field private file_zise_tv:Landroid/widget/TextView;

.field private mAudioCodec:Ljava/lang/String;

.field private mDuration:Ljava/lang/String;

.field private mVideoCodec:Ljava/lang/String;

.field private mVideoPlayActivity:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

.field private video_codec_tv:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1    # Lcom/jrm/localmm/ui/video/VideoPlayerActivity;
    .param p2    # I
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    const-string v0, "00:00:00"

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/DetailInfoDialog;->mDuration:Ljava/lang/String;

    iput-object v1, p0, Lcom/jrm/localmm/ui/video/DetailInfoDialog;->mAudioCodec:Ljava/lang/String;

    iput-object v1, p0, Lcom/jrm/localmm/ui/video/DetailInfoDialog;->mVideoCodec:Ljava/lang/String;

    iput-object p1, p0, Lcom/jrm/localmm/ui/video/DetailInfoDialog;->mVideoPlayActivity:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    iput p2, p0, Lcom/jrm/localmm/ui/video/DetailInfoDialog;->current_position:I

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    iput-object p3, p0, Lcom/jrm/localmm/ui/video/DetailInfoDialog;->mDuration:Ljava/lang/String;

    :cond_0
    iput-object p4, p0, Lcom/jrm/localmm/ui/video/DetailInfoDialog;->mAudioCodec:Ljava/lang/String;

    iput-object p5, p0, Lcom/jrm/localmm/ui/video/DetailInfoDialog;->mVideoCodec:Ljava/lang/String;

    return-void
.end method

.method private findViews()V
    .locals 1

    const v0, 0x7f08003e    # com.jrm.localmm.R.id.file_name

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/video/DetailInfoDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/DetailInfoDialog;->file_name_tv:Landroid/widget/TextView;

    const v0, 0x7f080067    # com.jrm.localmm.R.id.file_zise

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/video/DetailInfoDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/DetailInfoDialog;->file_zise_tv:Landroid/widget/TextView;

    const v0, 0x7f080047    # com.jrm.localmm.R.id.file_format

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/video/DetailInfoDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/DetailInfoDialog;->file_format_tv:Landroid/widget/TextView;

    const v0, 0x7f080041    # com.jrm.localmm.R.id.duration

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/video/DetailInfoDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/DetailInfoDialog;->duration_tv:Landroid/widget/TextView;

    const v0, 0x7f0800a1    # com.jrm.localmm.R.id.audio_codec

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/video/DetailInfoDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/DetailInfoDialog;->audio_codec_tv:Landroid/widget/TextView;

    const v0, 0x7f0800a4    # com.jrm.localmm.R.id.video_codec

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/video/DetailInfoDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/DetailInfoDialog;->video_codec_tv:Landroid/widget/TextView;

    return-void
.end method

.method private getVideoInfor()V
    .locals 7

    iget-object v4, p0, Lcom/jrm/localmm/ui/video/DetailInfoDialog;->mVideoPlayActivity:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v4}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getVideoPlayList()Ljava/util/List;

    move-result-object v1

    iget v4, p0, Lcom/jrm/localmm/ui/video/DetailInfoDialog;->current_position:I

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jrm/localmm/business/data/BaseData;

    invoke-virtual {v0}, Lcom/jrm/localmm/business/data/BaseData;->getName()Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, Lcom/jrm/localmm/ui/video/DetailInfoDialog;->file_name_tv:Landroid/widget/TextView;

    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0}, Lcom/jrm/localmm/business/data/BaseData;->getPath()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "path:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/jrm/localmm/business/data/BaseData;->getSize()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    const-string v4, ""

    invoke-virtual {v0}, Lcom/jrm/localmm/business/data/BaseData;->getSize()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    :cond_0
    iget-object v4, p0, Lcom/jrm/localmm/ui/video/DetailInfoDialog;->file_zise_tv:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/jrm/localmm/ui/video/DetailInfoDialog;->mVideoPlayActivity:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    const v6, 0x7f0600c5    # com.jrm.localmm.R.string.video_size_unknown

    invoke-virtual {v5, v6}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    iget-object v4, p0, Lcom/jrm/localmm/ui/video/DetailInfoDialog;->file_format_tv:Landroid/widget/TextView;

    const-string v5, "."

    invoke-virtual {v3, v5}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {v3, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/jrm/localmm/ui/video/DetailInfoDialog;->duration_tv:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/jrm/localmm/ui/video/DetailInfoDialog;->mDuration:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/jrm/localmm/ui/video/DetailInfoDialog;->audio_codec_tv:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/jrm/localmm/ui/video/DetailInfoDialog;->mAudioCodec:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/jrm/localmm/ui/video/DetailInfoDialog;->video_codec_tv:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/jrm/localmm/ui/video/DetailInfoDialog;->mVideoCodec:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void

    :cond_1
    iget-object v4, p0, Lcom/jrm/localmm/ui/video/DetailInfoDialog;->file_zise_tv:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/jrm/localmm/business/data/BaseData;->getSize()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 9
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    const v5, 0x7f03001b    # com.jrm.localmm.R.layout.video_info

    invoke-virtual {p0, v5}, Lcom/jrm/localmm/ui/video/DetailInfoDialog;->setContentView(I)V

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/DetailInfoDialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v5

    invoke-interface {v5}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    const/4 v5, 0x0

    invoke-virtual {v2, v5}, Landroid/view/Window;->setTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v5

    int-to-double v5, v5

    const-wide v7, 0x3fd999999999999aL    # 0.4

    mul-double/2addr v5, v7

    double-to-int v3, v5

    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v5

    int-to-double v5, v5

    const-wide v7, 0x3fe6666666666666L    # 0.7

    mul-double/2addr v5, v7

    double-to-int v1, v5

    invoke-virtual {v2, v3, v1}, Landroid/view/Window;->setLayout(II)V

    const/16 v5, 0x11

    invoke-virtual {v2, v5}, Landroid/view/Window;->setGravity(I)V

    invoke-virtual {v2}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v4

    const/high16 v5, 0x3f800000    # 1.0f

    iput v5, v4, Landroid/view/WindowManager$LayoutParams;->screenBrightness:F

    invoke-virtual {v2, v4}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    const v5, 0x106000d    # android.R.color.transparent

    invoke-virtual {v2, v5}, Landroid/view/Window;->setBackgroundDrawableResource(I)V

    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/DetailInfoDialog;->findViews()V

    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/DetailInfoDialog;->getVideoInfor()V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v0, 0x1

    return v0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    invoke-super {p0}, Landroid/app/Dialog;->cancel()V

    const/4 v0, 0x1

    return v0
.end method
