.class public Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;
.super Landroid/app/Dialog;
.source "PlaySettingSubtitleDialog.java"


# static fields
.field private static subtitleFlag:Z


# instance fields
.field private adapter:Lcom/jrm/localmm/business/adapter/SubtitleSettingListAdapter;

.field private context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

.field private getInfoNum:I

.field private isInnerSubtitle:Z

.field private mHandler:Landroid/os/Handler;

.field private mImageSubtitleCount:I

.field private mInternalSubtitleCount:I

.field private mVideoPlayView:Lcom/jrm/localmm/business/video/VideoPlayView;

.field private onkeyListenter:Landroid/view/View$OnKeyListener;

.field private playSettingListView:Landroid/widget/ListView;

.field private subtitleLanguageIndex:I

.field private subtitlePosition:I

.field private subtitleTextView:Lcom/jrm/localmm/ui/video/BorderTextViews;

.field private subtitleTime:I

.field private videoPlaySettingDialog:Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;

.field private viewId:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x1

    sput-boolean v0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->subtitleFlag:Z

    return-void
.end method

.method public constructor <init>(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;ILjava/lang/String;Lcom/jrm/localmm/business/video/VideoPlayView;Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;Z)V
    .locals 2
    .param p1    # Lcom/jrm/localmm/ui/video/VideoPlayerActivity;
    .param p2    # I
    .param p3    # Ljava/lang/String;
    .param p4    # Lcom/jrm/localmm/business/video/VideoPlayView;
    .param p5    # Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;
    .param p6    # Z

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    iput v1, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->mInternalSubtitleCount:I

    iput v1, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->mImageSubtitleCount:I

    iput v1, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->subtitlePosition:I

    iput v1, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->subtitleLanguageIndex:I

    iput-boolean v0, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->isInnerSubtitle:Z

    iput v0, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->viewId:I

    new-instance v0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog$2;

    invoke-direct {v0, p0}, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog$2;-><init>(Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->onkeyListenter:Landroid/view/View$OnKeyListener;

    new-instance v0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog$3;

    invoke-direct {v0, p0}, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog$3;-><init>(Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->mHandler:Landroid/os/Handler;

    iput-boolean p6, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->isInnerSubtitle:Z

    iput-object p1, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    iput-object p5, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->videoPlaySettingDialog:Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;

    iput-object p4, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->mVideoPlayView:Lcom/jrm/localmm/business/video/VideoPlayView;

    sput-boolean p6, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->subtitleFlag:Z

    invoke-virtual {p1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getVideoPlayHolder()Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getSubtitleTextView()Lcom/jrm/localmm/ui/video/BorderTextViews;

    move-result-object v0

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->subtitleTextView:Lcom/jrm/localmm/ui/video/BorderTextViews;

    invoke-virtual {p1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getVideoPlayHolder()Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getViewId()I

    move-result v0

    iput v0, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->viewId:I

    iget-boolean v0, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->isInnerSubtitle:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;)Z
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;

    iget-boolean v0, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->isInnerSubtitle:Z

    return v0
.end method

.method static synthetic access$100(Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;)I
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;

    iget v0, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->subtitlePosition:I

    return v0
.end method

.method static synthetic access$1000(Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;)I
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;

    iget v0, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->subtitleTime:I

    return v0
.end method

.method static synthetic access$1100(Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;I)V
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->changeTimeOrder(I)V

    return-void
.end method

.method static synthetic access$1200(Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;)Lcom/jrm/localmm/business/adapter/SubtitleSettingListAdapter;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->adapter:Lcom/jrm/localmm/business/adapter/SubtitleSettingListAdapter;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;)Landroid/widget/ListView;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->playSettingListView:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;IZ)V
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;
    .param p1    # I
    .param p2    # Z

    invoke-direct {p0, p1, p2}, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->changeTimeAdjust(IZ)V

    return-void
.end method

.method static synthetic access$1500(Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;)Lcom/jrm/localmm/business/video/VideoPlayView;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->mVideoPlayView:Lcom/jrm/localmm/business/video/VideoPlayView;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;)I
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;

    iget v0, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->viewId:I

    return v0
.end method

.method static synthetic access$1700(Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;)I
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;

    iget v0, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->getInfoNum:I

    return v0
.end method

.method static synthetic access$1702(Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;I)I
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;
    .param p1    # I

    iput p1, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->getInfoNum:I

    return p1
.end method

.method static synthetic access$1708(Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;)I
    .locals 2
    .param p0    # Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;

    iget v0, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->getInfoNum:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->getInfoNum:I

    return v0
.end method

.method static synthetic access$1800(Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$200(Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;IZ)V
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;
    .param p1    # I
    .param p2    # Z

    invoke-direct {p0, p1, p2}, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->changeSubtitleLanguage(IZ)V

    return-void
.end method

.method static synthetic access$300(Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;)Lcom/jrm/localmm/ui/video/VideoPlayerActivity;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    return-object v0
.end method

.method static synthetic access$400(Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;Z)V
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->changeSubtitleLanguage(Z)V

    return-void
.end method

.method static synthetic access$500(Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;Z)V
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->changeSubtitleFont(Z)V

    return-void
.end method

.method static synthetic access$600(Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;Z)V
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->changeSubtitleFontSize(Z)V

    return-void
.end method

.method static synthetic access$700(Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;Z)V
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->changeSubtitleColor(Z)V

    return-void
.end method

.method static synthetic access$800(Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;)V
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;

    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->changeSubtitleClip()V

    return-void
.end method

.method static synthetic access$900(Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;)V
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;

    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->changeSubtitlePostion()V

    return-void
.end method

.method private changeSubtitleClip()V
    .locals 5

    const v3, 0x7f0600bb    # com.jrm.localmm.R.string.subtitle_7_value_2

    const/4 v4, 0x7

    iget v2, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->viewId:I

    invoke-static {v4, v2}, Lcom/jrm/localmm/util/Tools;->getSubtitleSettingOptValue(II)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v2, v3}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    const v3, 0x7f0600ba    # com.jrm.localmm.R.string.subtitle_7_value_1

    invoke-virtual {v2, v3}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget v3, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->viewId:I

    invoke-static {v4, v2, v3}, Lcom/jrm/localmm/util/Tools;->setSubtitleSettingOpt(ILjava/lang/String;I)V

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->subtitleTextView:Lcom/jrm/localmm/ui/video/BorderTextViews;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/jrm/localmm/ui/video/BorderTextViews;->setPaintColor(I)V

    :goto_0
    iget-object v2, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->subtitleTextView:Lcom/jrm/localmm/ui/video/BorderTextViews;

    invoke-virtual {v2}, Lcom/jrm/localmm/ui/video/BorderTextViews;->invalidate()V

    return-void

    :cond_0
    iget-object v2, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v2, v3}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget v3, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->viewId:I

    invoke-static {v4, v2, v3}, Lcom/jrm/localmm/util/Tools;->setSubtitleSettingOpt(ILjava/lang/String;I)V

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->subtitleTextView:Lcom/jrm/localmm/ui/video/BorderTextViews;

    const/high16 v3, -0x10000

    invoke-virtual {v2, v3}, Lcom/jrm/localmm/ui/video/BorderTextViews;->setPaintColor(I)V

    goto :goto_0
.end method

.method private changeSubtitleColor(Z)V
    .locals 7
    .param p1    # Z

    const/high16 v6, -0x1000000

    const v5, 0x7f0600b9    # com.jrm.localmm.R.string.subtitle_6_value_3

    const v4, 0x7f0600b8    # com.jrm.localmm.R.string.subtitle_6_value_2

    const v2, 0x7f0600b7    # com.jrm.localmm.R.string.subtitle_6_value_1

    const/4 v3, 0x6

    iget v1, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->viewId:I

    invoke-static {v3, v1}, Lcom/jrm/localmm/util/Tools;->getSubtitleSettingOptValue(II)Ljava/lang/String;

    move-result-object v0

    if-eqz p1, :cond_3

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v1, v2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v1, v4}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget v2, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->viewId:I

    invoke-static {v3, v1, v2}, Lcom/jrm/localmm/util/Tools;->setSubtitleSettingOpt(ILjava/lang/String;I)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->subtitleTextView:Lcom/jrm/localmm/ui/video/BorderTextViews;

    invoke-virtual {v1, v6}, Lcom/jrm/localmm/ui/video/BorderTextViews;->setTextColor(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v1, v4}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v1, v5}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget v2, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->viewId:I

    invoke-static {v3, v1, v2}, Lcom/jrm/localmm/util/Tools;->setSubtitleSettingOpt(ILjava/lang/String;I)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->subtitleTextView:Lcom/jrm/localmm/ui/video/BorderTextViews;

    const v2, -0xffff01

    invoke-virtual {v1, v2}, Lcom/jrm/localmm/ui/video/BorderTextViews;->setTextColor(I)V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v1, v5}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v1, v2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget v2, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->viewId:I

    invoke-static {v3, v1, v2}, Lcom/jrm/localmm/util/Tools;->setSubtitleSettingOpt(ILjava/lang/String;I)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->subtitleTextView:Lcom/jrm/localmm/ui/video/BorderTextViews;

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Lcom/jrm/localmm/ui/video/BorderTextViews;->setTextColor(I)V

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v1, v2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v1, v5}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget v2, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->viewId:I

    invoke-static {v3, v1, v2}, Lcom/jrm/localmm/util/Tools;->setSubtitleSettingOpt(ILjava/lang/String;I)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->subtitleTextView:Lcom/jrm/localmm/ui/video/BorderTextViews;

    const v2, -0xffff01

    invoke-virtual {v1, v2}, Lcom/jrm/localmm/ui/video/BorderTextViews;->setTextColor(I)V

    goto :goto_0

    :cond_4
    iget-object v1, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v1, v5}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v1, v4}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget v2, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->viewId:I

    invoke-static {v3, v1, v2}, Lcom/jrm/localmm/util/Tools;->setSubtitleSettingOpt(ILjava/lang/String;I)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->subtitleTextView:Lcom/jrm/localmm/ui/video/BorderTextViews;

    invoke-virtual {v1, v6}, Lcom/jrm/localmm/ui/video/BorderTextViews;->setTextColor(I)V

    goto :goto_0

    :cond_5
    iget-object v1, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v1, v4}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v1, v2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget v2, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->viewId:I

    invoke-static {v3, v1, v2}, Lcom/jrm/localmm/util/Tools;->setSubtitleSettingOpt(ILjava/lang/String;I)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->subtitleTextView:Lcom/jrm/localmm/ui/video/BorderTextViews;

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Lcom/jrm/localmm/ui/video/BorderTextViews;->setTextColor(I)V

    goto/16 :goto_0
.end method

.method private changeSubtitleFont(Z)V
    .locals 7
    .param p1    # Z

    const v6, 0x7f0600b3    # com.jrm.localmm.R.string.subtitle_4_value_3

    const v5, 0x7f0600b2    # com.jrm.localmm.R.string.subtitle_4_value_2

    const v3, 0x7f0600b1    # com.jrm.localmm.R.string.subtitle_4_value_1

    const/4 v4, 0x4

    sget-object v0, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    iget v2, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->viewId:I

    invoke-static {v4, v2}, Lcom/jrm/localmm/util/Tools;->getSubtitleSettingOptValue(II)Ljava/lang/String;

    move-result-object v1

    if-eqz p1, :cond_3

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v2, v3}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v2, v5}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget v3, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->viewId:I

    invoke-static {v4, v2, v3}, Lcom/jrm/localmm/util/Tools;->setSubtitleSettingOpt(ILjava/lang/String;I)V

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v2

    const-string v3, "xingshu.ttf"

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->subtitleTextView:Lcom/jrm/localmm/ui/video/BorderTextViews;

    invoke-virtual {v2, v0}, Lcom/jrm/localmm/ui/video/BorderTextViews;->setTypeface(Landroid/graphics/Typeface;)V

    return-void

    :cond_1
    iget-object v2, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v2, v5}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v2, v6}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget v3, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->viewId:I

    invoke-static {v4, v2, v3}, Lcom/jrm/localmm/util/Tools;->setSubtitleSettingOpt(ILjava/lang/String;I)V

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v2

    const-string v3, "kaishu.ttf"

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v2, v6}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v2, v3}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget v3, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->viewId:I

    invoke-static {v4, v2, v3}, Lcom/jrm/localmm/util/Tools;->setSubtitleSettingOpt(ILjava/lang/String;I)V

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v2

    const-string v3, "songti.ttf"

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v2, v3}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v2, v6}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget v3, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->viewId:I

    invoke-static {v4, v2, v3}, Lcom/jrm/localmm/util/Tools;->setSubtitleSettingOpt(ILjava/lang/String;I)V

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v2

    const-string v3, "kaishu.ttf"

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v2, v5}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v2, v3}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget v3, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->viewId:I

    invoke-static {v4, v2, v3}, Lcom/jrm/localmm/util/Tools;->setSubtitleSettingOpt(ILjava/lang/String;I)V

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v2

    const-string v3, "songti.ttf"

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    goto/16 :goto_0

    :cond_5
    iget-object v2, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v2, v6}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v2, v5}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget v3, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->viewId:I

    invoke-static {v4, v2, v3}, Lcom/jrm/localmm/util/Tools;->setSubtitleSettingOpt(ILjava/lang/String;I)V

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v2

    const-string v3, "xingshu.ttf"

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    goto/16 :goto_0
.end method

.method private changeSubtitleFontSize(Z)V
    .locals 7
    .param p1    # Z

    const v6, 0x7f0600b6    # com.jrm.localmm.R.string.subtitle_5_value_3

    const v5, 0x7f0600b5    # com.jrm.localmm.R.string.subtitle_5_value_2

    const v3, 0x7f0600b4    # com.jrm.localmm.R.string.subtitle_5_value_1

    const/4 v4, 0x5

    const/4 v1, 0x0

    iget v2, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->viewId:I

    invoke-static {v4, v2}, Lcom/jrm/localmm/util/Tools;->getSubtitleSettingOptValue(II)Ljava/lang/String;

    move-result-object v0

    if-eqz p1, :cond_4

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v2, v3}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/high16 v1, 0x420c0000    # 35.0f

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v2, v5}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget v3, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->viewId:I

    invoke-static {v4, v2, v3}, Lcom/jrm/localmm/util/Tools;->setSubtitleSettingOpt(ILjava/lang/String;I)V

    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getVideoPlayHolder()Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v2

    iget v3, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->viewId:I

    invoke-virtual {v2, v3}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->isPIPMode(I)Z

    move-result v2

    if-eqz v2, :cond_1

    const/high16 v2, 0x41c80000    # 25.0f

    cmpl-float v2, v1, v2

    if-nez v2, :cond_7

    const/high16 v1, 0x40e00000    # 7.0f

    :goto_1
    invoke-static {v1}, Lcom/jrm/localmm/business/video/SubtitleTool;->setCurrentPIPSubSize(F)V

    :cond_1
    iget-object v2, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->subtitleTextView:Lcom/jrm/localmm/ui/video/BorderTextViews;

    invoke-virtual {v2, v1}, Lcom/jrm/localmm/ui/video/BorderTextViews;->setTextSize(F)V

    return-void

    :cond_2
    iget-object v2, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v2, v5}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    const/high16 v1, 0x41c80000    # 25.0f

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v2, v6}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget v3, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->viewId:I

    invoke-static {v4, v2, v3}, Lcom/jrm/localmm/util/Tools;->setSubtitleSettingOpt(ILjava/lang/String;I)V

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v2, v6}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/high16 v1, 0x42340000    # 45.0f

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v2, v3}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget v3, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->viewId:I

    invoke-static {v4, v2, v3}, Lcom/jrm/localmm/util/Tools;->setSubtitleSettingOpt(ILjava/lang/String;I)V

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v2, v3}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    const/high16 v1, 0x41c80000    # 25.0f

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v2, v6}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget v3, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->viewId:I

    invoke-static {v4, v2, v3}, Lcom/jrm/localmm/util/Tools;->setSubtitleSettingOpt(ILjava/lang/String;I)V

    goto :goto_0

    :cond_5
    iget-object v2, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v2, v6}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    const/high16 v1, 0x420c0000    # 35.0f

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v2, v5}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget v3, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->viewId:I

    invoke-static {v4, v2, v3}, Lcom/jrm/localmm/util/Tools;->setSubtitleSettingOpt(ILjava/lang/String;I)V

    goto/16 :goto_0

    :cond_6
    iget-object v2, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v2, v5}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/high16 v1, 0x42340000    # 45.0f

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v2, v3}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget v3, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->viewId:I

    invoke-static {v4, v2, v3}, Lcom/jrm/localmm/util/Tools;->setSubtitleSettingOpt(ILjava/lang/String;I)V

    goto/16 :goto_0

    :cond_7
    const/high16 v2, 0x420c0000    # 35.0f

    cmpl-float v2, v1, v2

    if-nez v2, :cond_8

    const/high16 v1, 0x41200000    # 10.0f

    goto/16 :goto_1

    :cond_8
    const/high16 v1, 0x41700000    # 15.0f

    goto/16 :goto_1
.end method

.method private changeSubtitleLanguage(IZ)V
    .locals 5
    .param p1    # I
    .param p2    # Z

    const/4 v4, 0x0

    const v3, 0x7f0600ae    # com.jrm.localmm.R.string.subtitle_2_value_2

    const/4 v2, 0x2

    if-eqz p2, :cond_4

    if-lez p1, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v1, v3}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    add-int/lit8 v1, p1, -0x1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->viewId:I

    invoke-static {v2, v0, v1}, Lcom/jrm/localmm/util/Tools;->setSubtitleSettingOpt(ILjava/lang/String;I)V

    add-int/lit8 v0, p1, -0x1

    iput v0, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->subtitlePosition:I

    :cond_0
    :goto_0
    iget v0, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->mInternalSubtitleCount:I

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->mVideoPlayView:Lcom/jrm/localmm/business/video/VideoPlayView;

    iget v1, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->subtitlePosition:I

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/business/video/VideoPlayView;->setSubtitleTrack(I)V

    const/4 v0, 0x3

    iget v1, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->subtitlePosition:I

    iget v2, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->viewId:I

    invoke-static {v1, v2}, Lcom/jrm/localmm/util/Tools;->getSubtitleLanguage(II)Ljava/lang/String;

    move-result-object v1

    iget v2, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->viewId:I

    invoke-static {v0, v1, v2}, Lcom/jrm/localmm/util/Tools;->setSubtitleSettingOpt(ILjava/lang/String;I)V

    :cond_1
    return-void

    :cond_2
    if-nez p1, :cond_0

    iget v0, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->mInternalSubtitleCount:I

    if-lez v0, :cond_3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v1, v3}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->mInternalSubtitleCount:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->viewId:I

    invoke-static {v2, v0, v1}, Lcom/jrm/localmm/util/Tools;->setSubtitleSettingOpt(ILjava/lang/String;I)V

    iget v0, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->mInternalSubtitleCount:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->subtitlePosition:I

    goto :goto_0

    :cond_3
    iput v4, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->subtitlePosition:I

    goto :goto_0

    :cond_4
    iget v0, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->mInternalSubtitleCount:I

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_5

    iget v0, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->mInternalSubtitleCount:I

    if-ge p1, v0, :cond_5

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v1, v3}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "0"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->viewId:I

    invoke-static {v2, v0, v1}, Lcom/jrm/localmm/util/Tools;->setSubtitleSettingOpt(ILjava/lang/String;I)V

    iput v4, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->subtitlePosition:I

    goto :goto_0

    :cond_5
    iget v0, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->mInternalSubtitleCount:I

    if-ge p1, v0, :cond_0

    if-ltz p1, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v1, v3}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    add-int/lit8 v1, p1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->viewId:I

    invoke-static {v2, v0, v1}, Lcom/jrm/localmm/util/Tools;->setSubtitleSettingOpt(ILjava/lang/String;I)V

    add-int/lit8 v0, p1, 0x1

    iput v0, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->subtitlePosition:I

    goto/16 :goto_0
.end method

.method private changeSubtitleLanguage(Z)V
    .locals 4
    .param p1    # Z

    iget v1, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->viewId:I

    invoke-static {v1}, Lcom/jrm/localmm/util/Tools;->getSubtitleLanguageTypes(I)[Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    array-length v1, v0

    iput v1, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->mImageSubtitleCount:I

    const-string v1, "****changeSubtitleLanguage*****"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mImageSubtitleCount : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->mImageSubtitleCount:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget v1, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->mImageSubtitleCount:I

    const/4 v2, 0x1

    if-gt v1, v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v1, "****changeSubtitleLanguage*****"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "subtitleLanguageIndex : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->subtitleLanguageIndex:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz p1, :cond_3

    iget v1, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->subtitleLanguageIndex:I

    if-nez v1, :cond_2

    iget v1, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->mImageSubtitleCount:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->subtitleLanguageIndex:I

    :goto_1
    const-string v1, "****changeSubtitleLanguage*****"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "subtitleLanguageIndex : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->subtitleLanguageIndex:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x3

    iget v2, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->subtitleLanguageIndex:I

    iget v3, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->viewId:I

    invoke-static {v2, v3}, Lcom/jrm/localmm/util/Tools;->getSubtitleLanguage(II)Ljava/lang/String;

    move-result-object v2

    iget v3, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->viewId:I

    invoke-static {v1, v2, v3}, Lcom/jrm/localmm/util/Tools;->setSubtitleSettingOpt(ILjava/lang/String;I)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->mVideoPlayView:Lcom/jrm/localmm/business/video/VideoPlayView;

    iget v2, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->subtitleLanguageIndex:I

    or-int/lit16 v2, v2, 0x100

    invoke-virtual {v1, v2}, Lcom/jrm/localmm/business/video/VideoPlayView;->setSubtitleTrack(I)V

    goto :goto_0

    :cond_2
    iget v1, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->subtitleLanguageIndex:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->subtitleLanguageIndex:I

    goto :goto_1

    :cond_3
    iget v1, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->subtitleLanguageIndex:I

    iget v2, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->mImageSubtitleCount:I

    add-int/lit8 v2, v2, -0x1

    if-ne v1, v2, :cond_4

    const/4 v1, 0x0

    iput v1, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->subtitleLanguageIndex:I

    goto :goto_1

    :cond_4
    iget v1, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->subtitleLanguageIndex:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->subtitleLanguageIndex:I

    goto :goto_1
.end method

.method private changeSubtitlePostion()V
    .locals 6

    const v3, 0x7f0600bd    # com.jrm.localmm.R.string.subtitle_8_value_2

    const/16 v5, 0x8

    const/4 v4, -0x2

    iget v2, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->viewId:I

    invoke-static {v5, v2}, Lcom/jrm/localmm/util/Tools;->getSubtitleSettingOptValue(II)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v2, v3}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    const v3, 0x7f0600bc    # com.jrm.localmm.R.string.subtitle_8_value_1

    invoke-virtual {v2, v3}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget v3, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->viewId:I

    invoke-static {v5, v2, v3}, Lcom/jrm/localmm/util/Tools;->setSubtitleSettingOpt(ILjava/lang/String;I)V

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    const/16 v2, 0x51

    invoke-direct {v1, v4, v4, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->subtitleTextView:Lcom/jrm/localmm/ui/video/BorderTextViews;

    invoke-virtual {v2, v1}, Lcom/jrm/localmm/ui/video/BorderTextViews;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v2, v3}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget v3, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->viewId:I

    invoke-static {v5, v2, v3}, Lcom/jrm/localmm/util/Tools;->setSubtitleSettingOpt(ILjava/lang/String;I)V

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    const/16 v2, 0x31

    invoke-direct {v1, v4, v4, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->subtitleTextView:Lcom/jrm/localmm/ui/video/BorderTextViews;

    invoke-virtual {v2, v1}, Lcom/jrm/localmm/ui/video/BorderTextViews;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method private changeTimeAdjust(IZ)V
    .locals 3
    .param p1    # I
    .param p2    # Z

    const/16 v2, 0x9

    if-eqz p2, :cond_1

    const/16 v0, -0x26ac

    if-gt p1, v0, :cond_0

    const/16 p1, -0x26ac

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    add-int/lit8 v1, p1, -0x64

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->viewId:I

    invoke-static {v2, v0, v1}, Lcom/jrm/localmm/util/Tools;->setSubtitleSettingOpt(ILjava/lang/String;I)V

    add-int/lit8 v0, p1, -0x64

    iput v0, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->subtitleTime:I

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->mVideoPlayView:Lcom/jrm/localmm/business/video/VideoPlayView;

    iget v1, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->subtitleTime:I

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/business/video/VideoPlayView;->setSubtitleSync(I)I

    :goto_0
    return-void

    :cond_1
    const/16 v0, 0x26ac

    if-lt p1, v0, :cond_2

    const/16 p1, 0x26ac

    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    add-int/lit8 v1, p1, 0x64

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->viewId:I

    invoke-static {v2, v0, v1}, Lcom/jrm/localmm/util/Tools;->setSubtitleSettingOpt(ILjava/lang/String;I)V

    add-int/lit8 v0, p1, 0x64

    iput v0, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->subtitleTime:I

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->mVideoPlayView:Lcom/jrm/localmm/business/video/VideoPlayView;

    iget v1, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->subtitleTime:I

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/business/video/VideoPlayView;->setSubtitleSync(I)I

    goto :goto_0
.end method

.method private changeTimeOrder(I)V
    .locals 3
    .param p1    # I

    const/16 v0, 0x26ac

    if-le p1, v0, :cond_0

    const/16 p1, -0x2774

    :cond_0
    const/16 v0, 0x9

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    add-int/lit8 v2, p1, 0x64

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget v2, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->viewId:I

    invoke-static {v0, v1, v2}, Lcom/jrm/localmm/util/Tools;->setSubtitleSettingOpt(ILjava/lang/String;I)V

    add-int/lit8 v0, p1, 0x64

    iput v0, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->subtitleTime:I

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->mVideoPlayView:Lcom/jrm/localmm/business/video/VideoPlayView;

    iget v1, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->subtitleTime:I

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/business/video/VideoPlayView;->setSubtitleSync(I)I

    return-void
.end method

.method private setListeners()V
    .locals 2

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->playSettingListView:Landroid/widget/ListView;

    new-instance v1, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog$1;

    invoke-direct {v1, p0}, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog$1;-><init>(Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->playSettingListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->onkeyListenter:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    return-void
.end method


# virtual methods
.method public getAdapter()Lcom/jrm/localmm/business/adapter/SubtitleSettingListAdapter;
    .locals 1

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->adapter:Lcom/jrm/localmm/business/adapter/SubtitleSettingListAdapter;

    return-object v0
.end method

.method public onBackPressed()V
    .locals 1

    invoke-super {p0}, Landroid/app/Dialog;->onBackPressed()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->videoPlaySettingDialog:Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->show()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 11
    .param p1    # Landroid/os/Bundle;

    const/4 v10, 0x0

    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    const v5, 0x7f030012    # com.jrm.localmm.R.layout.playsetting_subtitle

    invoke-virtual {p0, v5}, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->setContentView(I)V

    sget v5, Lcom/jrm/localmm/util/Constants;->mVideoSubtitleNo:I

    iput v5, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->mInternalSubtitleCount:I

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v5

    invoke-interface {v5}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    const v5, 0x7f08007d    # com.jrm.localmm.R.id.playsetting_subtitle_list

    invoke-virtual {p0, v5}, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ListView;

    iput-object v5, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->playSettingListView:Landroid/widget/ListView;

    new-instance v5, Lcom/jrm/localmm/business/adapter/SubtitleSettingListAdapter;

    iget-object v6, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    sget-object v7, Lcom/jrm/localmm/util/Constants;->subTitleSettingName:[I

    iget-object v8, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    iget v9, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->viewId:I

    invoke-static {v8, v9}, Lcom/jrm/localmm/util/Tools;->initSubtitleSettingOpt(Landroid/content/Context;I)[Ljava/lang/String;

    move-result-object v8

    iget-boolean v9, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->isInnerSubtitle:Z

    invoke-direct {v5, v6, v7, v8, v9}, Lcom/jrm/localmm/business/adapter/SubtitleSettingListAdapter;-><init>(Landroid/content/Context;[I[Ljava/lang/String;Z)V

    iput-object v5, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->adapter:Lcom/jrm/localmm/business/adapter/SubtitleSettingListAdapter;

    iget-object v5, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->playSettingListView:Landroid/widget/ListView;

    invoke-virtual {v5, v10}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    iget-object v5, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->playSettingListView:Landroid/widget/ListView;

    iget-object v6, p0, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->adapter:Lcom/jrm/localmm/business/adapter/SubtitleSettingListAdapter;

    invoke-virtual {v5, v6}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->setListeners()V

    invoke-virtual {v2, v10}, Landroid/view/Window;->setTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v5

    int-to-double v5, v5

    const-wide v7, 0x3fdb851eb851eb85L    # 0.43

    mul-double/2addr v5, v7

    double-to-int v3, v5

    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v5

    int-to-double v5, v5

    const-wide/high16 v7, 0x3fe8000000000000L    # 0.75

    mul-double/2addr v5, v7

    double-to-int v1, v5

    invoke-virtual {v2, v3, v1}, Landroid/view/Window;->setLayout(II)V

    const/16 v5, 0x11

    invoke-virtual {v2, v5}, Landroid/view/Window;->setGravity(I)V

    invoke-virtual {v2}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/16 v0, 0x7e

    if-eq v0, p1, :cond_0

    const/16 v0, 0x7f

    if-eq v0, p1, :cond_0

    const/16 v0, 0x57

    if-eq v0, p1, :cond_0

    const/16 v0, 0x58

    if-ne v0, p1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    invoke-super {p0, p1, p2}, Landroid/app/Dialog;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/16 v0, 0x7e

    if-eq v0, p1, :cond_0

    const/16 v0, 0x7f

    if-eq v0, p1, :cond_0

    const/16 v0, 0x57

    if-eq v0, p1, :cond_0

    const/16 v0, 0x58

    if-ne v0, p1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    invoke-super {p0, p1, p2}, Landroid/app/Dialog;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method
