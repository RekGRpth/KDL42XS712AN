.class Lcom/jrm/localmm/ui/video/VideoPlayerActivity$6;
.super Ljava/lang/Object;
.source "VideoPlayerActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->init3DMode()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;


# direct methods
.method constructor <init>(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$6;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    :try_start_0
    iget-object v5, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$6;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->mTvManager:Lcom/mstar/android/tvapi/common/TvManager;
    invoke-static {v5}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$2000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$6;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->mTvManager:Lcom/mstar/android/tvapi/common/TvManager;
    invoke-static {v5}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$2000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/mstar/android/tvapi/common/TvManager;->getPipManager()Lcom/mstar/android/tvapi/common/PipManager;

    move-result-object v2

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v3

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/PipManager;->getPipMode()Lcom/mstar/android/tvapi/common/vo/EnumPipModes;

    move-result-object v4

    const-string v5, "VideoPlayActivity"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "******mode:****"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v5, Lcom/mstar/android/tvapi/common/vo/EnumPipModes;->E_PIP_MODE_PIP:Lcom/mstar/android/tvapi/common/vo/EnumPipModes;

    if-ne v4, v5, :cond_1

    sget-object v5, Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;->E_MAIN_WINDOW:Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;

    invoke-virtual {v2, v5}, Lcom/mstar/android/tvapi/common/PipManager;->disablePip(Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;)Z

    invoke-virtual {v3}, Lcom/mstar/android/tv/TvPipPopManager;->disablePip()Z

    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Lcom/mstar/android/tv/TvPipPopManager;->setPipOnFlag(Z)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v5, Lcom/mstar/android/tvapi/common/vo/EnumPipModes;->E_PIP_MODE_POP:Lcom/mstar/android/tvapi/common/vo/EnumPipModes;

    if-ne v4, v5, :cond_3

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_TYPE_NUM:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    iget-object v5, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$6;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    iget-object v5, v5, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->threeDimensionManager:Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    invoke-virtual {v5}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->getCurrent3dFormat()Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    move-result-object v1

    sget-object v5, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_DUALVIEW:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    if-ne v1, v5, :cond_2

    iget-object v5, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$6;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    iget-object v5, v5, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->threeDimensionManager:Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    invoke-virtual {v5}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->disable3dDualView()Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :cond_2
    :try_start_1
    sget-object v5, Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;->E_MAIN_WINDOW:Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;

    invoke-virtual {v2, v5}, Lcom/mstar/android/tvapi/common/PipManager;->disablePop(Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;)Z

    invoke-virtual {v3}, Lcom/mstar/android/tv/TvPipPopManager;->disablePop()Z

    goto :goto_0

    :cond_3
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_TYPE_NUM:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    iget-object v5, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$6;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    iget-object v5, v5, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->threeDimensionManager:Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    invoke-virtual {v5}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->getCurrent3dFormat()Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    move-result-object v1

    const-string v5, "VideoPlayActivity"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "******formatType:****"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v5, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_DUALVIEW:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    if-ne v1, v5, :cond_0

    iget-object v5, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$6;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    iget-object v5, v5, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->threeDimensionManager:Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    invoke-virtual {v5}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->disable3dDualView()Z

    invoke-virtual {v3}, Lcom/mstar/android/tv/TvPipPopManager;->disable3dDualView()Z
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method
