.class public Lcom/jrm/localmm/ui/video/BorderTextViews;
.super Landroid/widget/TextView;
.source "BorderTextViews.java"


# instance fields
.field private color:I

.field private paint:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/BorderTextViews;->paint:Landroid/graphics/Paint;

    return-void
.end method

.method private drawClip(Landroid/graphics/Canvas;)V
    .locals 8
    .param p1    # Landroid/graphics/Canvas;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/BorderTextViews;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    const/4 v2, 0x1

    if-lt v0, v2, :cond_0

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/BorderTextViews;->paint:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/BorderTextViews;->paint:Landroid/graphics/Paint;

    iget v2, p0, Lcom/jrm/localmm/ui/video/BorderTextViews;->color:I

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/BorderTextViews;->paint:Landroid/graphics/Paint;

    const/high16 v2, 0x40400000    # 3.0f

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/BorderTextViews;->getWidth()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    int-to-float v3, v0

    iget-object v5, p0, Lcom/jrm/localmm/ui/video/BorderTextViews;->paint:Landroid/graphics/Paint;

    move-object v0, p1

    move v2, v1

    move v4, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/BorderTextViews;->getHeight()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    int-to-float v4, v0

    iget-object v5, p0, Lcom/jrm/localmm/ui/video/BorderTextViews;->paint:Landroid/graphics/Paint;

    move-object v0, p1

    move v2, v1

    move v3, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/BorderTextViews;->getHeight()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    int-to-float v2, v0

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/BorderTextViews;->getWidth()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    int-to-float v3, v0

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/BorderTextViews;->getHeight()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    int-to-float v4, v0

    iget-object v5, p0, Lcom/jrm/localmm/ui/video/BorderTextViews;->paint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/BorderTextViews;->getWidth()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    int-to-float v3, v0

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/BorderTextViews;->getWidth()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    int-to-float v5, v0

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/BorderTextViews;->getHeight()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    int-to-float v6, v0

    iget-object v7, p0, Lcom/jrm/localmm/ui/video/BorderTextViews;->paint:Landroid/graphics/Paint;

    move-object v2, p1

    move v4, v1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    :cond_0
    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 0
    .param p1    # Landroid/graphics/Canvas;

    invoke-super {p0, p1}, Landroid/widget/TextView;->onDraw(Landroid/graphics/Canvas;)V

    invoke-direct {p0, p1}, Lcom/jrm/localmm/ui/video/BorderTextViews;->drawClip(Landroid/graphics/Canvas;)V

    return-void
.end method

.method public setPaintColor(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/jrm/localmm/ui/video/BorderTextViews;->color:I

    return-void
.end method
