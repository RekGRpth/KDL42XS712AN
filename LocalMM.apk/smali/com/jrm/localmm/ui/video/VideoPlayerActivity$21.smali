.class Lcom/jrm/localmm/ui/video/VideoPlayerActivity$21;
.super Ljava/lang/Object;
.source "VideoPlayerActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jrm/localmm/ui/video/VideoPlayerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;


# direct methods
.method constructor <init>(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$21;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$21;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView()Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/jrm/localmm/business/video/VideoPlayView;->isInPlaybackState()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$21;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->playerIsCreated:Z
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$5100(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Z

    move-result v1

    if-eqz v1, :cond_1

    :try_start_0
    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$21;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    iget-object v1, v1, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->mThreeDimentionControl:Lcom/jrm/localmm/ui/video/ThreeDimentionControl;

    invoke-virtual {v1}, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->ThreeDInit()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$21;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    const/4 v2, 0x0

    # setter for: Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->playerIsCreated:Z
    invoke-static {v1, v2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->access$5102(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;Z)Z

    :cond_0
    :goto_1
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$21;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    iget-object v1, v1, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->mThreeDimentionControl:Lcom/jrm/localmm/ui/video/ThreeDimentionControl;

    invoke-virtual {v1}, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->refreshThreeDMode()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method
