.class public Lcom/jrm/localmm/ui/video/VideoPlayerActivity;
.super Landroid/app/Activity;
.source "VideoPlayerActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;,
        Lcom/jrm/localmm/ui/video/VideoPlayerActivity$errorStruct;
    }
.end annotation


# static fields
.field private static isNeedBreakPointPlay:Z

.field public static screenHeight:I

.field public static screenWidth:I

.field private static seekTimes:I


# instance fields
.field private appSkin:Lcom/mstar/android/tv/TvCommonManager;

.field private breakPoint:I

.field private breakPointDialog:Landroid/app/AlertDialog;

.field private chooseTimePlayDialog:Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;

.field private controlOnKeyListener:Landroid/view/View$OnKeyListener;

.field private currentPlayerPosition:I

.field protected currentViewPosition:I

.field private diskChangeReceiver:Landroid/content/BroadcastReceiver;

.field public displayFormat:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

.field private duration:I

.field private endPreTime:[J

.field private inputSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

.field private isAudioSupportOne:Z

.field private isAudioSupportTwo:Z

.field private isBreakPointPlay:Z

.field private isCanSeek:Z

.field private isControllerShow:Z

.field private isOnePrepared:Z

.field private isPlaying:Z

.field private isTwoPrepared:Z

.field private isVideoSupportOne:Z

.field private isVideoSupportTwo:Z

.field private mDetailInfoDialog:Lcom/jrm/localmm/ui/video/DetailInfoDialog;

.field private mRunnable:Ljava/lang/Runnable;

.field public mThreeDimentionControl:Lcom/jrm/localmm/ui/video/ThreeDimentionControl;

.field private mTvManager:Lcom/mstar/android/tvapi/common/TvManager;

.field private mVideoListDialog:Lcom/jrm/localmm/ui/video/VideoListDialog;

.field public myPlayerCallback:Lcom/jrm/localmm/business/video/VideoPlayView$playerCallback;

.field netDisconnectReceiver:Landroid/content/BroadcastReceiver;

.field private oldAnims:[F

.field private playSettingSubtitleDialogOne:Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;

.field private playSettingSubtitleDialogTwo:Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;

.field private playSpeed:I

.field private playerIsCreated:Z

.field private progressDialog:Landroid/app/ProgressDialog;

.field public s3dSkin:Lcom/mstar/android/tv/TvS3DManager;

.field private seekBarListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

.field private seekPosition:I

.field shutDownReceiver:Landroid/content/BroadcastReceiver;

.field private sourceFrom:I

.field private startPreTime:[J

.field private startRenderingTime:[J

.field public threeDimensionManager:Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

.field private toBePlayedHandler:Landroid/os/Handler;

.field private videoHandler:Landroid/os/Handler;

.field private videoPlayAbDialog:[Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

.field protected videoPlayList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/jrm/localmm/business/data/BaseData;",
            ">;"
        }
    .end annotation
.end field

.field private videoPlaySettingDialogOne:Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;

.field private videoPlaySettingDialogTwo:Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;

.field private videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

.field protected video_position:[I

.field private wakelock:Landroid/os/PowerManager$WakeLock;

.field private wm:Landroid/view/IWindowManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->seekTimes:I

    sput-boolean v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isNeedBreakPointPlay:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    new-array v0, v3, [Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayAbDialog:[Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    iput v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->playSpeed:I

    iput-boolean v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isCanSeek:Z

    iput-boolean v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isOnePrepared:Z

    iput-boolean v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isTwoPrepared:Z

    new-array v0, v3, [I

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->video_position:[I

    iput v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->currentViewPosition:I

    iput v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->currentPlayerPosition:I

    iput v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->duration:I

    iput-object v4, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->wakelock:Landroid/os/PowerManager$WakeLock;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayList:Ljava/util/List;

    iput-boolean v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isControllerShow:Z

    iput-boolean v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isPlaying:Z

    iput v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->seekPosition:I

    iput-boolean v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->playerIsCreated:Z

    iput-boolean v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isAudioSupportOne:Z

    iput-boolean v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isVideoSupportOne:Z

    iput-boolean v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isAudioSupportTwo:Z

    iput-boolean v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isVideoSupportTwo:Z

    new-array v0, v3, [J

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->startPreTime:[J

    new-array v0, v3, [J

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->endPreTime:[J

    new-array v0, v3, [J

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->startRenderingTime:[J

    iput-object v4, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->oldAnims:[F

    iput-boolean v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isBreakPointPlay:Z

    new-instance v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$1;

    invoke-direct {v0, p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$1;-><init>(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoHandler:Landroid/os/Handler;

    new-instance v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$2;

    invoke-direct {v0, p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$2;-><init>(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->toBePlayedHandler:Landroid/os/Handler;

    new-instance v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$7;

    invoke-direct {v0, p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$7;-><init>(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->controlOnKeyListener:Landroid/view/View$OnKeyListener;

    new-instance v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$8;

    invoke-direct {v0, p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$8;-><init>(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->seekBarListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    new-instance v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$11;

    invoke-direct {v0, p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$11;-><init>(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->myPlayerCallback:Lcom/jrm/localmm/business/video/VideoPlayView$playerCallback;

    new-instance v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$17;

    invoke-direct {v0, p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$17;-><init>(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->shutDownReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$18;

    invoke-direct {v0, p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$18;-><init>(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->netDisconnectReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$19;

    invoke-direct {v0, p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$19;-><init>(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->diskChangeReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$21;

    invoke-direct {v0, p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$21;-><init>(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->mRunnable:Ljava/lang/Runnable;

    return-void
.end method

.method private InitVideoPlayer(Ljava/lang/String;I)V
    .locals 8

    const v7, 0x7f0600af    # com.jrm.localmm.R.string.subtitle_3_value_1

    const/4 v6, 0x3

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x1

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v0, p2, v4}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setSeekVar(IZ)V

    invoke-direct {p0, p2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->initTimeVar(I)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v0, p2}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView(I)Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v0, p2}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView(I)Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/jrm/localmm/business/video/VideoPlayView;->stopPlayback()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v0, p2}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView(I)Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v0

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->myPlayerCallback:Lcom/jrm/localmm/business/video/VideoPlayView$playerCallback;

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/business/video/VideoPlayView;->setPlayerCallbackListener(Lcom/jrm/localmm/business/video/VideoPlayView$playerCallback;)V

    :cond_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getDualVideoMode()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->mThreeDimentionControl:Lcom/jrm/localmm/ui/video/ThreeDimentionControl;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->initThreeDimensionMode()V

    :cond_1
    const-string v0, "VideoPlayActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "*******videoPlayPath*****"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    sget-boolean v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isNeedBreakPointPlay:Z

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayList:Ljava/util/List;

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->video_position:[I

    add-int/lit8 v2, p2, -0x1

    aget v1, v1, v2

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jrm/localmm/business/data/BaseData;

    invoke-virtual {v0}, Lcom/jrm/localmm/business/data/BaseData;->getPath()Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayList:Ljava/util/List;

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->video_position:[I

    add-int/lit8 v3, p2, -0x1

    aget v2, v2, v3

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jrm/localmm/business/data/BaseData;

    invoke-virtual {v0}, Lcom/jrm/localmm/business/data/BaseData;->getSize()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0, p0}, Lcom/jrm/localmm/business/video/BreakPointRecord;->getData(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->breakPoint:I

    :goto_0
    const-string v0, "VideoPlayActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "********breakPoint********"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->breakPoint:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->breakPoint:I

    if-lez v0, :cond_a

    iget v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->breakPoint:I

    invoke-direct {p0, p2, v0, p1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->breakPointPlay(IILjava/lang/String;)V

    :cond_2
    :goto_1
    if-ne p2, v4, :cond_b

    iput-boolean v4, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isAudioSupportOne:Z

    iput-boolean v4, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isVideoSupportOne:Z

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlaySettingDialogOne:Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlaySettingDialogOne:Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->dismiss()V

    iput-object v5, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlaySettingDialogOne:Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v6, v0, p2}, Lcom/jrm/localmm/util/Tools;->setSubtitleSettingOpt(ILjava/lang/String;I)V

    :cond_3
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->playSettingSubtitleDialogOne:Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->playSettingSubtitleDialogOne:Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->dismiss()V

    :cond_4
    :goto_2
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->mDetailInfoDialog:Lcom/jrm/localmm/ui/video/DetailInfoDialog;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->mDetailInfoDialog:Lcom/jrm/localmm/ui/video/DetailInfoDialog;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/DetailInfoDialog;->dismiss()V

    :cond_5
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->chooseTimePlayDialog:Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->chooseTimePlayDialog:Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->clearChooseList()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->chooseTimePlayDialog:Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->dismiss()V

    :cond_6
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayAbDialog:[Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    add-int/lit8 v1, p2, -0x1

    aget-object v0, v0, v1

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayAbDialog:[Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    add-int/lit8 v1, p2, -0x1

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayAbDialog:[Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    add-int/lit8 v1, p2, -0x1

    aput-object v5, v0, v1

    :cond_7
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->mVideoListDialog:Lcom/jrm/localmm/ui/video/VideoListDialog;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->mVideoListDialog:Lcom/jrm/localmm/ui/video/VideoListDialog;

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->video_position:[I

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v2}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getViewId()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/video/VideoListDialog;->setSelection(I)V

    :cond_8
    return-void

    :cond_9
    iput v3, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->breakPoint:I

    iput-boolean v3, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isBreakPointPlay:Z

    goto/16 :goto_0

    :cond_a
    if-eqz p1, :cond_2

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v0, p2}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView(I)Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/jrm/localmm/business/video/VideoPlayView;->setVideoPath(Ljava/lang/String;I)V

    goto :goto_1

    :cond_b
    iput-boolean v4, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isAudioSupportTwo:Z

    iput-boolean v4, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isVideoSupportTwo:Z

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlaySettingDialogTwo:Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlaySettingDialogTwo:Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->dismiss()V

    iput-object v5, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlaySettingDialogTwo:Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v6, v0, p2}, Lcom/jrm/localmm/util/Tools;->setSubtitleSettingOpt(ILjava/lang/String;I)V

    :cond_c
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->playSettingSubtitleDialogTwo:Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->playSettingSubtitleDialogTwo:Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;->dismiss()V

    goto :goto_2
.end method

.method private InitView()V
    .locals 3

    new-instance v0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    sget v1, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->screenWidth:I

    sget v2, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->screenHeight:I

    invoke-direct {v0, p0, v1, v2}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;-><init>(Landroid/content/Context;II)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoHandler:Landroid/os/Handler;

    invoke-virtual {v0, p0, v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setActivity(Landroid/app/Activity;Landroid/os/Handler;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->videoSeekBar:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->seekBarListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    const v0, 0x7f0800b5    # com.jrm.localmm.R.id.video_suspension_layout

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, v1, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->playControlLayout:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->playControlLayout:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->controlOnKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    return-void
.end method

.method static synthetic access$000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    return-object v0
.end method

.method static synthetic access$100(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)V
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->hideController()V

    return-void
.end method

.method static synthetic access$1000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Ljava/lang/Runnable;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->mRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Landroid/widget/SeekBar$OnSeekBarChangeListener;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->seekBarListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)I
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    iget v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->seekPosition:I

    return v0
.end method

.method static synthetic access$1202(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;I)I
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/video/VideoPlayerActivity;
    .param p1    # I

    iput p1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->seekPosition:I

    return p1
.end method

.method static synthetic access$1302(I)I
    .locals 0
    .param p0    # I

    sput p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->seekTimes:I

    return p0
.end method

.method static synthetic access$1304()I
    .locals 1

    sget v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->seekTimes:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->seekTimes:I

    return v0
.end method

.method static synthetic access$1400(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)[Lcom/jrm/localmm/ui/video/VideoPlayABDialog;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayAbDialog:[Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Landroid/app/AlertDialog;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->breakPointDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;Ljava/lang/String;I)V
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/video/VideoPlayerActivity;
    .param p1    # Ljava/lang/String;
    .param p2    # I

    invoke-direct {p0, p1, p2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->InitVideoPlayer(Ljava/lang/String;I)V

    return-void
.end method

.method static synthetic access$1700(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)V
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->showController()V

    return-void
.end method

.method static synthetic access$1800(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Landroid/view/IWindowManager;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->wm:Landroid/view/IWindowManager;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)[F
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->oldAnims:[F

    return-object v0
.end method

.method static synthetic access$1902(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;[F)[F
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/video/VideoPlayerActivity;
    .param p1    # [F

    iput-object p1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->oldAnims:[F

    return-object p1
.end method

.method static synthetic access$200(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)I
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    iget v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->duration:I

    return v0
.end method

.method static synthetic access$2000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/mstar/android/tvapi/common/TvManager;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->mTvManager:Lcom/mstar/android/tvapi/common/TvManager;

    return-object v0
.end method

.method static synthetic access$202(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;I)I
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/video/VideoPlayerActivity;
    .param p1    # I

    iput p1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->duration:I

    return p1
.end method

.method static synthetic access$2100(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)V
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->switchDualFocus()V

    return-void
.end method

.method static synthetic access$2200(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/video/VideoPlayerActivity;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->showToastTip(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$2300(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)I
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    iget v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->playSpeed:I

    return v0
.end method

.method static synthetic access$2302(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;I)I
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/video/VideoPlayerActivity;
    .param p1    # I

    iput p1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->playSpeed:I

    return p1
.end method

.method static synthetic access$2400(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Z
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    iget-boolean v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isControllerShow:Z

    return v0
.end method

.method static synthetic access$2500(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Z
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isPrepared()Z

    move-result v0

    return v0
.end method

.method static synthetic access$2600(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;II)V
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/video/VideoPlayerActivity;
    .param p1    # I
    .param p2    # I

    invoke-direct {p0, p1, p2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->moveToNextOrPrevious(II)V

    return-void
.end method

.method static synthetic access$2700(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)V
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->slowForward()V

    return-void
.end method

.method static synthetic access$2800(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Z
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    iget-boolean v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isPlaying:Z

    return v0
.end method

.method static synthetic access$2900(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)V
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->fastForward()V

    return-void
.end method

.method static synthetic access$300(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)I
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    iget v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->currentPlayerPosition:I

    return v0
.end method

.method static synthetic access$3000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)V
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->showVideoTimeSetDialog()V

    return-void
.end method

.method static synthetic access$302(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;I)I
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/video/VideoPlayerActivity;
    .param p1    # I

    iput p1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->currentPlayerPosition:I

    return p1
.end method

.method static synthetic access$3100(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)V
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->showVideoListDialog()V

    return-void
.end method

.method static synthetic access$3200(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)V
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->showVideoInfoDialog()V

    return-void
.end method

.method static synthetic access$3300(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)V
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->showSettingDialog()V

    return-void
.end method

.method static synthetic access$3400(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)V
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->showPlayABDialog()V

    return-void
.end method

.method static synthetic access$3500(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Z
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    iget-boolean v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isOnePrepared:Z

    return v0
.end method

.method static synthetic access$3600(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)V
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->switchDualMode()V

    return-void
.end method

.method static synthetic access$3800(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;Landroid/media/MediaPlayer;II)Lcom/jrm/localmm/ui/video/VideoPlayerActivity$errorStruct;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/video/VideoPlayerActivity;
    .param p1    # Landroid/media/MediaPlayer;
    .param p2    # I
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->processErrorUnknown(Landroid/media/MediaPlayer;II)Lcom/jrm/localmm/ui/video/VideoPlayerActivity$errorStruct;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$3900(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;Ljava/lang/String;I)V
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/video/VideoPlayerActivity;
    .param p1    # Ljava/lang/String;
    .param p2    # I

    invoke-direct {p0, p1, p2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->showErrorToast(Ljava/lang/String;I)V

    return-void
.end method

.method static synthetic access$400(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->chooseTimePlayDialog:Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;

    return-object v0
.end method

.method static synthetic access$4002(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;Z)Z
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/video/VideoPlayerActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isAudioSupportOne:Z

    return p1
.end method

.method static synthetic access$4102(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;Z)Z
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/video/VideoPlayerActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isAudioSupportTwo:Z

    return p1
.end method

.method static synthetic access$4202(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;Z)Z
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/video/VideoPlayerActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isVideoSupportOne:Z

    return p1
.end method

.method static synthetic access$4302(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;Z)Z
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/video/VideoPlayerActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isVideoSupportTwo:Z

    return p1
.end method

.method static synthetic access$4400(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)[J
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->startRenderingTime:[J

    return-object v0
.end method

.method static synthetic access$4500(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Z
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    iget-boolean v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isBreakPointPlay:Z

    return v0
.end method

.method static synthetic access$4502(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;Z)Z
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/video/VideoPlayerActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isBreakPointPlay:Z

    return p1
.end method

.method static synthetic access$4600(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)I
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    iget v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->breakPoint:I

    return v0
.end method

.method static synthetic access$4700(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;I)V
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/video/VideoPlayerActivity;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->initPlayer(I)V

    return-void
.end method

.method static synthetic access$4800(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)I
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    iget v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->sourceFrom:I

    return v0
.end method

.method static synthetic access$4900(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/mstar/android/tv/TvCommonManager;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->appSkin:Lcom/mstar/android/tv/TvCommonManager;

    return-object v0
.end method

.method static synthetic access$500(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$5000(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->inputSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    return-object v0
.end method

.method static synthetic access$5002(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/video/VideoPlayerActivity;
    .param p1    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    iput-object p1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->inputSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    return-object p1
.end method

.method static synthetic access$5100(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Z
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    iget-boolean v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->playerIsCreated:Z

    return v0
.end method

.method static synthetic access$5102(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;Z)Z
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/video/VideoPlayerActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->playerIsCreated:Z

    return p1
.end method

.method static synthetic access$602(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;)Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/video/VideoPlayerActivity;
    .param p1    # Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;

    iput-object p1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->playSettingSubtitleDialogOne:Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;

    return-object p1
.end method

.method static synthetic access$700(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlaySettingDialogOne:Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;

    return-object v0
.end method

.method static synthetic access$802(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;)Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/video/VideoPlayerActivity;
    .param p1    # Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;

    iput-object p1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->playSettingSubtitleDialogTwo:Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;

    return-object p1
.end method

.method static synthetic access$900(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlaySettingDialogTwo:Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;

    return-object v0
.end method

.method private acquireWakeLock()V
    .locals 3

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->wakelock:Landroid/os/PowerManager$WakeLock;

    if-nez v0, :cond_0

    const-string v0, "power"

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    const/16 v1, 0xa

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->wakelock:Landroid/os/PowerManager$WakeLock;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->wakelock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    :cond_0
    return-void
.end method

.method private breakPointPlay(IILjava/lang/String;)V
    .locals 4

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isBreakPointPlay:Z

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoHandler:Landroid/os/Handler;

    const/16 v1, 0x18

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0600a6    # com.jrm.localmm.R.string.video_breakpoint_play

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x108009b    # android.R.drawable.ic_dialog_info

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f060007    # com.jrm.localmm.R.string.exit_ok

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$16;

    invoke-direct {v2, p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$16;-><init>(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f060008    # com.jrm.localmm.R.string.exit_cancel

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$15;

    invoke-direct {v2, p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$15;-><init>(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$14;

    invoke-direct {v1, p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$14;-><init>(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$13;

    invoke-direct {v1, p0, p3, p1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$13;-><init>(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$12;

    invoke-direct {v1, p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$12;-><init>(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->breakPointDialog:Landroid/app/AlertDialog;

    return-void
.end method

.method private checkABCycle(I)V
    .locals 5
    .param p1    # I

    const/16 v4, 0x17

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoHandler:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->removeMessages(I)V

    const/4 v0, 0x0

    const/4 v1, 0x1

    if-ne p1, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayAbDialog:[Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    aget-object v1, v1, v0

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayAbDialog:[Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    aget-object v1, v1, v0

    iget-boolean v1, v1, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->bFlag:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayAbDialog:[Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    aget-object v1, v1, v0

    iget-object v1, v1, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->sharedata:Landroid/content/SharedPreferences;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v1, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private dismissProgressDialog()V
    .locals 2

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->progressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "VideoPlayActivity"

    const-string v1, "dismissProgressDialog"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->progressDialog:Landroid/app/ProgressDialog;

    :cond_0
    return-void
.end method

.method private fastForward()V
    .locals 3

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isPlaying:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView()Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/business/video/VideoPlayView;->setVoice(Z)V

    invoke-virtual {p0, v1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->localResume(Z)V

    :cond_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView()Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/jrm/localmm/business/video/VideoPlayView;->getPlayMode()I

    move-result v0

    iput v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->playSpeed:I

    const-string v0, "VideoPlayActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "get play mode ---"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->playSpeed:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x2

    iget v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->playSpeed:I

    const/16 v2, 0x40

    if-ge v1, v2, :cond_1

    iget v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->playSpeed:I

    if-lez v1, :cond_1

    iget v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->playSpeed:I

    mul-int/lit8 v0, v0, 0x2

    :cond_1
    iput v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->playSpeed:I

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView()Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/jrm/localmm/business/video/VideoPlayView;->setPlayMode(I)Z

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v1, v0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setPlaySpeed(I)V

    return-void
.end method

.method private hideController()V
    .locals 2

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setSystemUiVisibility(I)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->playControlLayout:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->playControlLayout:Landroid/widget/LinearLayout;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isControllerShow:Z

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    iget-boolean v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isControllerShow:Z

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->showVideoFocus(Z)V

    :goto_0
    return-void

    :cond_0
    sget-object v0, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string v1, "playControlLayout is null ptr!!"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private init3DMode()V
    .locals 2

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$6;

    invoke-direct {v1, p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$6;-><init>(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method private initObject()V
    .locals 2

    invoke-static {}, Lcom/mstar/android/tv/TvS3DManager;->getInstance()Lcom/mstar/android/tv/TvS3DManager;

    move-result-object v0

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->s3dSkin:Lcom/mstar/android/tv/TvS3DManager;

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->mTvManager:Lcom/mstar/android/tvapi/common/TvManager;

    new-instance v0, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;-><init>()V

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->threeDimensionManager:Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    invoke-static {}, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->getInstance()Lcom/jrm/localmm/ui/video/ThreeDimentionControl;

    move-result-object v0

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->mThreeDimentionControl:Lcom/jrm/localmm/ui/video/ThreeDimentionControl;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->mThreeDimentionControl:Lcom/jrm/localmm/ui/video/ThreeDimentionControl;

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView()Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->setMediaPlayer(Lcom/jrm/localmm/business/video/VideoPlayView;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->mThreeDimentionControl:Lcom/jrm/localmm/ui/video/ThreeDimentionControl;

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->setHandler(Landroid/os/Handler;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->mThreeDimentionControl:Lcom/jrm/localmm/ui/video/ThreeDimentionControl;

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->s3dSkin:Lcom/mstar/android/tv/TvS3DManager;

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->setS3DSkin(Lcom/mstar/android/tv/TvS3DManager;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->mThreeDimentionControl:Lcom/jrm/localmm/ui/video/ThreeDimentionControl;

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->threeDimensionManager:Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->setThreeDimensionManager(Lcom/mstar/android/tvapi/common/ThreeDimensionManager;)V

    return-void
.end method

.method private initPlayer(I)V
    .locals 6

    const v5, 0x7f0600ab    # com.jrm.localmm.R.string.subtitle_1_value_1

    const/4 v4, 0x1

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->endPreTime:[J

    add-int/lit8 v1, p1, -0x1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    aput-wide v2, v0, v1

    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->dismissProgressDialog()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView()Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/business/video/VideoPlayView;->setVoice(I)V

    if-ne p1, v4, :cond_2

    iput-boolean v4, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isOnePrepared:Z

    :goto_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v0, p1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView(I)Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/jrm/localmm/business/video/VideoPlayView;->getDuration()I

    move-result v0

    iput v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->duration:I

    const-string v0, "VideoPlayActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getDuration()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->duration:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->duration:I

    int-to-long v0, v0

    invoke-static {v0, v1}, Lcom/jrm/localmm/util/Tools;->formatDuration(J)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getViewId()I

    move-result v1

    if-ne p1, v1, :cond_1

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->total_time_video:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->videoSeekBar:Landroid/widget/SeekBar;

    iget v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->duration:I

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setMax(I)V

    if-ne p1, v4, :cond_3

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->playSettingSubtitleDialogOne:Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;

    if-eqz v0, :cond_0

    invoke-virtual {p0, v5}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0, p1}, Lcom/jrm/localmm/util/Tools;->setSubtitleSettingOpt(ILjava/lang/String;I)V

    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v0, v4}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setPlaySpeed(I)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoHandler:Landroid/os/Handler;

    const/16 v1, 0x11

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    const/4 v0, 0x0

    iput v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->playSpeed:I

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView()Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/jrm/localmm/business/video/VideoPlayView;->isPlaying()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setAllUnSelect(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView()Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/jrm/localmm/business/video/VideoPlayView;->isPlaying()Z

    move-result v1

    invoke-virtual {v0, v4, v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoPlaySelect(ZZ)V

    const/4 v0, 0x2

    sput v0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->state:I

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->hideControlDelay()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoHandler:Landroid/os/Handler;

    const/16 v1, 0xe

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_1
    invoke-virtual {p0, p1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isSeekable(I)Z

    invoke-virtual {p0, p1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->is3D(I)Z

    return-void

    :cond_2
    iput-boolean v4, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isTwoPrepared:Z

    goto/16 :goto_0

    :cond_3
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->playSettingSubtitleDialogTwo:Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;

    if-eqz v0, :cond_0

    invoke-virtual {p0, v5}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0, p1}, Lcom/jrm/localmm/util/Tools;->setSubtitleSettingOpt(ILjava/lang/String;I)V

    goto :goto_1
.end method

.method private initTimeVar(I)V
    .locals 4
    .param p1    # I

    const-wide/16 v2, 0x0

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->startPreTime:[J

    add-int/lit8 v1, p1, -0x1

    aput-wide v2, v0, v1

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->endPreTime:[J

    add-int/lit8 v1, p1, -0x1

    aput-wide v2, v0, v1

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->startRenderingTime:[J

    add-int/lit8 v1, p1, -0x1

    aput-wide v2, v0, v1

    return-void
.end method

.method private isPIPMode()Z
    .locals 7

    const/4 v3, 0x1

    :try_start_0
    iget-object v4, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->mTvManager:Lcom/mstar/android/tvapi/common/TvManager;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->mTvManager:Lcom/mstar/android/tvapi/common/TvManager;

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getPipManager()Lcom/mstar/android/tvapi/common/PipManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/PipManager;->getPipMode()Lcom/mstar/android/tvapi/common/vo/EnumPipModes;

    move-result-object v2

    const-string v4, "VideoPlayActivity"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "******mode:****"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/EnumPipModes;->E_PIP_MODE_PIP:Lcom/mstar/android/tvapi/common/vo/EnumPipModes;

    if-eq v2, v4, :cond_0

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/EnumPipModes;->E_PIP_MODE_POP:Lcom/mstar/android/tvapi/common/vo/EnumPipModes;

    if-ne v2, v4, :cond_1

    :cond_0
    :goto_0
    return v3

    :cond_1
    iget-object v4, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->threeDimensionManager:Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->getCurrent3dFormat()Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    move-result-object v4

    sget-object v5, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_DUALVIEW:Lcom/mstar/android/tvapi/common/vo/Enum3dType;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    if-eq v4, v5, :cond_0

    :cond_2
    :goto_1
    const/4 v3, 0x0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1
.end method

.method private isPrepared()Z
    .locals 2

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getViewId()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isOnePrepared:Z

    :goto_0
    return v0

    :cond_0
    iget-boolean v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isTwoPrepared:Z

    goto :goto_0
.end method

.method private moveToNextOrPrevious(II)V
    .locals 6

    const/4 v5, 0x0

    const/4 v0, 0x1

    if-ne p2, v0, :cond_2

    iput-boolean v5, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isOnePrepared:Z

    :goto_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->mVideoPlayViewOne:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-virtual {v0}, Lcom/jrm/localmm/business/video/VideoPlayView;->getCurrentPosition()I

    move-result v0

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->mVideoPlayViewOne:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-virtual {v1}, Lcom/jrm/localmm/business/video/VideoPlayView;->getDuration()I

    move-result v1

    if-eq v0, v1, :cond_3

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayList:Ljava/util/List;

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->video_position:[I

    add-int/lit8 v2, p2, -0x1

    aget v1, v1, v2

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jrm/localmm/business/data/BaseData;

    invoke-virtual {v0}, Lcom/jrm/localmm/business/data/BaseData;->getPath()Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->mVideoPlayViewOne:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-virtual {v0}, Lcom/jrm/localmm/business/video/VideoPlayView;->getCurrentPosition()I

    move-result v2

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayList:Ljava/util/List;

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->video_position:[I

    add-int/lit8 v4, p2, -0x1

    aget v3, v3, v4

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jrm/localmm/business/data/BaseData;

    invoke-virtual {v0}, Lcom/jrm/localmm/business/data/BaseData;->getSize()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0, p0}, Lcom/jrm/localmm/business/video/BreakPointRecord;->addData(Ljava/lang/String;ILjava/lang/String;Landroid/content/Context;)V

    :goto_1
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->video_position:[I

    add-int/lit8 v1, p2, -0x1

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->video_position:[I

    add-int/lit8 v3, p2, -0x1

    aget v2, v2, v3

    add-int/2addr v2, p1

    aput v2, v0, v1

    const-string v0, "VideoPlayActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "moveToNextOrPrevious()  video_position: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->video_position:[I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v2}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getViewId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->video_position:[I

    add-int/lit8 v1, p2, -0x1

    aget v0, v0, v1

    const/4 v1, -0x1

    if-gt v0, v1, :cond_4

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->video_position:[I

    add-int/lit8 v1, p2, -0x1

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    aput v2, v0, v1

    :cond_0
    :goto_2
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getViewId()I

    move-result v0

    if-ne p2, v0, :cond_1

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->reset()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayList:Ljava/util/List;

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->video_position:[I

    add-int/lit8 v2, p2, -0x1

    aget v1, v1, v2

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jrm/localmm/business/data/BaseData;

    invoke-virtual {v0}, Lcom/jrm/localmm/business/data/BaseData;->getName()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v1, v0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoName(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->video_position:[I

    add-int/lit8 v2, p2, -0x1

    aget v1, v1, v2

    add-int/lit8 v1, v1, 0x1

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoListText(II)V

    :cond_1
    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->cancleDelayHide()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayList:Ljava/util/List;

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->video_position:[I

    add-int/lit8 v2, p2, -0x1

    aget v1, v1, v2

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jrm/localmm/business/data/BaseData;

    invoke-virtual {v0}, Lcom/jrm/localmm/business/data/BaseData;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->InitVideoPlayer(Ljava/lang/String;I)V

    return-void

    :cond_2
    iput-boolean v5, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isTwoPrepared:Z

    goto/16 :goto_0

    :cond_3
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayList:Ljava/util/List;

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->video_position:[I

    add-int/lit8 v2, p2, -0x1

    aget v1, v1, v2

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jrm/localmm/business/data/BaseData;

    invoke-virtual {v0}, Lcom/jrm/localmm/business/data/BaseData;->getPath()Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayList:Ljava/util/List;

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->video_position:[I

    add-int/lit8 v3, p2, -0x1

    aget v2, v2, v3

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jrm/localmm/business/data/BaseData;

    invoke-virtual {v0}, Lcom/jrm/localmm/business/data/BaseData;->getSize()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v5, v0, p0}, Lcom/jrm/localmm/business/video/BreakPointRecord;->addData(Ljava/lang/String;ILjava/lang/String;Landroid/content/Context;)V

    goto/16 :goto_1

    :cond_4
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->video_position:[I

    add-int/lit8 v1, p2, -0x1

    aget v0, v0, v1

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->video_position:[I

    add-int/lit8 v1, p2, -0x1

    aput v5, v0, v1

    goto/16 :goto_2
.end method

.method private processErrorUnknown(Landroid/media/MediaPlayer;II)Lcom/jrm/localmm/ui/video/VideoPlayerActivity$errorStruct;
    .locals 3

    new-instance v1, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$errorStruct;

    invoke-direct {v1, p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$errorStruct;-><init>(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)V

    const v0, 0x7f06006a    # com.jrm.localmm.R.string.video_media_error_unknown

    sparse-switch p3, :sswitch_data_0

    :goto_0
    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$errorStruct;->strMessage:Ljava/lang/String;

    return-object v1

    :sswitch_0
    const v0, 0x7f06006b    # com.jrm.localmm.R.string.video_media_error_malformed

    goto :goto_0

    :sswitch_1
    const v0, 0x7f06006c    # com.jrm.localmm.R.string.video_media_error_io

    goto :goto_0

    :sswitch_2
    const v0, 0x7f06006d    # com.jrm.localmm.R.string.video_media_error_unsupported

    goto :goto_0

    :sswitch_3
    const v0, 0x7f06006e    # com.jrm.localmm.R.string.video_media_error_format_unsupport

    goto :goto_0

    :sswitch_4
    const v0, 0x7f06006f    # com.jrm.localmm.R.string.video_media_error_not_connected

    goto :goto_0

    :sswitch_5
    const v0, 0x7f060070    # com.jrm.localmm.R.string.video_media_error_audio_unsupport

    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$errorStruct;->showStateWithError:Z

    goto :goto_0

    :sswitch_6
    const v0, 0x7f060071    # com.jrm.localmm.R.string.video_media_error_video_unsupport

    goto :goto_0

    :sswitch_7
    const v0, 0x7f060072    # com.jrm.localmm.R.string.video_media_error_no_license

    goto :goto_0

    :sswitch_8
    const v0, 0x7f060073    # com.jrm.localmm.R.string.video_media_error_license_expired

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x138b -> :sswitch_3
        -0x138a -> :sswitch_6
        -0x1389 -> :sswitch_5
        -0x7d2 -> :sswitch_8
        -0x7d1 -> :sswitch_7
        -0x3f2 -> :sswitch_2
        -0x3ef -> :sswitch_0
        -0x3ec -> :sswitch_1
        -0x3e9 -> :sswitch_4
    .end sparse-switch
.end method

.method private registerListeners()V
    .locals 2

    const/4 v1, 0x1

    iget-boolean v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isControllerShow:Z

    if-eqz v0, :cond_3

    sget v0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->state:I

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isPrepared()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_videoPre:Landroid/widget/ImageView;

    const v1, 0x7f02006e    # com.jrm.localmm.R.drawable.player_icon_previous_focus

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->cancleDelayHide()V

    const/4 v0, -0x1

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getViewId()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->moveToNextOrPrevious(II)V

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->hideControlDelay()V

    goto :goto_0

    :pswitch_2
    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isPrepared()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->cancleDelayHide()V

    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->slowForward()V

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->hideControlDelay()V

    goto :goto_0

    :pswitch_3
    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isPrepared()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->cancleDelayHide()V

    iget-boolean v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isPlaying:Z

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->playSpeed:I

    if-eqz v0, :cond_1

    invoke-virtual {p0, v1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->localResumeFromSpeed(Z)V

    :goto_1
    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->hideControlDelay()V

    goto :goto_0

    :cond_1
    invoke-virtual {p0, v1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->localPause(Z)V

    goto :goto_1

    :cond_2
    invoke-virtual {p0, v1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->localResume(Z)V

    goto :goto_1

    :pswitch_4
    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isPrepared()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->cancleDelayHide()V

    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->fastForward()V

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->hideControlDelay()V

    goto :goto_0

    :pswitch_5
    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isPrepared()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->cancleDelayHide()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getViewId()I

    move-result v0

    invoke-direct {p0, v1, v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->moveToNextOrPrevious(II)V

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->hideControlDelay()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_videoNext:Landroid/widget/ImageView;

    const v1, 0x7f020067    # com.jrm.localmm.R.drawable.player_icon_next_focus

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto/16 :goto_0

    :pswitch_6
    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isPrepared()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->cancleDelayHide()V

    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->showVideoTimeSetDialog()V

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->hideControlDelay()V

    goto/16 :goto_0

    :pswitch_7
    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->cancleDelayHide()V

    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->showVideoListDialog()V

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->hideControlDelay()V

    goto/16 :goto_0

    :pswitch_8
    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isPrepared()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->cancleDelayHide()V

    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->showVideoInfoDialog()V

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->hideControlDelay()V

    goto/16 :goto_0

    :pswitch_9
    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isPrepared()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->cancleDelayHide()V

    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->showSettingDialog()V

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->hideControlDelay()V

    goto/16 :goto_0

    :pswitch_a
    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isPrepared()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->cancleDelayHide()V

    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->showPlayABDialog()V

    goto/16 :goto_0

    :pswitch_b
    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isPrepared()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->cancleDelayHide()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->updateVoiceBar()V

    goto/16 :goto_0

    :pswitch_c
    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->cancleDelayHide()V

    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->switchDualMode()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoDualSwitchSelect(Z)V

    const/16 v0, 0x14

    sput v0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->state:I

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->hideControlDelay()V

    goto/16 :goto_0

    :pswitch_d
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getDualVideoMode()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->cancleDelayHide()V

    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->switchDualFocus()V

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->hideControlDelay()V

    goto/16 :goto_0

    :pswitch_e
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getDualVideoMode()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->changeDualMode()V

    goto/16 :goto_0

    :cond_3
    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->showController()V

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->hideControlDelay()V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_0
        :pswitch_c
        :pswitch_d
        :pswitch_e
    .end packed-switch
.end method

.method private rewind()V
    .locals 2

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->cancleDelayHide()V

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->videoSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v0

    if-ltz v0, :cond_0

    add-int/lit16 v0, v0, -0x2710

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView()Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/jrm/localmm/business/video/VideoPlayView;->seekTo(I)V

    :cond_0
    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->hideControlDelay()V

    return-void
.end method

.method private showController()V
    .locals 2

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->playControlLayout:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->playControlLayout:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isControllerShow:Z

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    iget-boolean v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isControllerShow:Z

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->showVideoFocus(Z)V

    :goto_0
    return-void

    :cond_0
    const-string v0, "VideoPlayActivity"

    const-string v1, "playControlLayout is null"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private showErrorToast(Ljava/lang/String;I)V
    .locals 2

    const/16 v0, 0x11

    invoke-static {p0, p1, v0}, Lcom/jrm/localmm/util/ToastFactory;->getToast(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f060076    # com.jrm.localmm.R.string.video_media_error_server_died

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->finish()V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    invoke-direct {p0, v0, p2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->moveToNextOrPrevious(II)V

    goto :goto_0
.end method

.method private showPlayABDialog()V
    .locals 7

    const/4 v6, 0x1

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getViewId()I

    move-result v0

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v1, v6}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoPlayABSelect(Z)V

    const/16 v1, 0x11

    sput v1, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->state:I

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v1, v0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->isSeekable(I)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f060078    # com.jrm.localmm.R.string.video_media_infor_no_index_ab

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->showToastTip(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayAbDialog:[Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    add-int/lit8 v2, v0, -0x1

    aget-object v1, v1, v2

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayAbDialog:[Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    add-int/lit8 v2, v0, -0x1

    new-instance v3, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    const/high16 v4, 0x7f070000    # com.jrm.localmm.R.style.dialog

    iget-object v5, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-direct {v3, p0, v4, v5}, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;-><init>(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;ILcom/jrm/localmm/ui/video/VideoPlayerViewHolder;)V

    aput-object v3, v1, v2

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayAbDialog:[Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    add-int/lit8 v2, v0, -0x1

    aget-object v1, v1, v2

    invoke-virtual {v1, v6}, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->requestWindowFeature(I)Z

    :cond_1
    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayAbDialog:[Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    add-int/lit8 v0, v0, -0x1

    aget-object v0, v1, v0

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->show()V

    goto :goto_0
.end method

.method private showProgressDialog(I)V
    .locals 2
    .param p1    # I

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->progressDialog:Landroid/app/ProgressDialog;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->progressDialog:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    :cond_0
    return-void
.end method

.method private showSettingDialog()V
    .locals 7

    const/high16 v6, 0x7f070000    # com.jrm.localmm.R.style.dialog

    const/4 v5, 0x0

    const/4 v4, 0x1

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getViewId()I

    move-result v0

    const-string v1, "VideoPlayActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " audioTrack : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v3}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView()Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v3

    invoke-virtual {v3, v5}, Lcom/jrm/localmm/business/video/VideoPlayView;->getAudioTrackInfo(Z)Lcom/mstar/android/media/AudioTrackInfo;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v1, v4}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoSettingSelect(Z)V

    const/16 v1, 0x10

    sput v1, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->state:I

    invoke-static {p0, v0}, Lcom/jrm/localmm/util/Tools;->initAudioTackSettingOpt(Landroid/content/Context;I)[Ljava/lang/String;

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getCurrentAudioTrackId()I

    move-result v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const v3, 0x7f0600c6    # com.jrm.localmm.R.string.audio_track_setting_0_value_1

    invoke-virtual {p0, v3}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v5, v1, v0}, Lcom/jrm/localmm/util/Tools;->setAudioTackSettingOpt(Landroid/content/Context;ILjava/lang/String;I)V

    if-ne v0, v4, :cond_1

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlaySettingDialogOne:Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;

    if-nez v1, :cond_0

    new-instance v1, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayList:Ljava/util/List;

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->video_position:[I

    add-int/lit8 v0, v0, -0x1

    aget v0, v3, v0

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jrm/localmm/business/data/BaseData;

    invoke-virtual {v0}, Lcom/jrm/localmm/business/data/BaseData;->getPath()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v2}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView()Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v2

    invoke-direct {v1, p0, v6, v0, v2}, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;-><init>(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;ILjava/lang/String;Lcom/jrm/localmm/business/video/VideoPlayView;)V

    iput-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlaySettingDialogOne:Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlaySettingDialogOne:Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;

    invoke-virtual {v0, v4}, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->requestWindowFeature(I)Z

    :cond_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlaySettingDialogOne:Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->show()V

    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlaySettingDialogTwo:Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;

    if-nez v1, :cond_2

    new-instance v1, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayList:Ljava/util/List;

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->video_position:[I

    add-int/lit8 v0, v0, -0x1

    aget v0, v3, v0

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jrm/localmm/business/data/BaseData;

    invoke-virtual {v0}, Lcom/jrm/localmm/business/data/BaseData;->getPath()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v2}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView()Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v2

    invoke-direct {v1, p0, v6, v0, v2}, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;-><init>(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;ILjava/lang/String;Lcom/jrm/localmm/business/video/VideoPlayView;)V

    iput-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlaySettingDialogTwo:Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlaySettingDialogTwo:Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;

    invoke-virtual {v0, v4}, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->requestWindowFeature(I)Z

    :cond_2
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlaySettingDialogTwo:Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->show()V

    goto :goto_0
.end method

.method private showToastTip(Ljava/lang/String;)V
    .locals 8
    .param p1    # Ljava/lang/String;

    const-wide/16 v6, 0x12c

    const/16 v5, 0x16

    const/16 v4, 0xe

    const/4 v3, 0x0

    const/4 v2, 0x1

    const/16 v1, 0x11

    invoke-static {p0, p1, v1}, Lcom/jrm/localmm/util/ToastFactory;->getToast(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    iget-boolean v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isVideoSupportOne:Z

    if-nez v1, :cond_2

    iget-boolean v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isAudioSupportOne:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-gt v1, v2, :cond_6

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoHandler:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->mVideoPlayViewOne:Lcom/jrm/localmm/business/video/VideoPlayView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->mVideoPlayViewOne:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-virtual {v1}, Lcom/jrm/localmm/business/video/VideoPlayView;->stopPlayer()V

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->mVideoPlayViewOne:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-virtual {v1, v3}, Lcom/jrm/localmm/business/video/VideoPlayView;->setPlayerCallbackListener(Lcom/jrm/localmm/business/video/VideoPlayView$playerCallback;)V

    :cond_0
    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->mVideoPlayViewTwo:Lcom/jrm/localmm/business/video/VideoPlayView;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->mVideoPlayViewTwo:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-virtual {v1}, Lcom/jrm/localmm/business/video/VideoPlayView;->stopPlayer()V

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->mVideoPlayViewTwo:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-virtual {v1, v3}, Lcom/jrm/localmm/business/video/VideoPlayView;->setPlayerCallbackListener(Lcom/jrm/localmm/business/video/VideoPlayView$playerCallback;)V

    :cond_1
    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoHandler:Landroid/os/Handler;

    invoke-virtual {v1, v5, v6, v7}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    :cond_2
    :goto_0
    iget-boolean v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isVideoSupportTwo:Z

    if-nez v1, :cond_5

    iget-boolean v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isAudioSupportTwo:Z

    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-gt v1, v2, :cond_7

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoHandler:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->mVideoPlayViewOne:Lcom/jrm/localmm/business/video/VideoPlayView;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->mVideoPlayViewOne:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-virtual {v1}, Lcom/jrm/localmm/business/video/VideoPlayView;->stopPlayer()V

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->mVideoPlayViewOne:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-virtual {v1, v3}, Lcom/jrm/localmm/business/video/VideoPlayView;->setPlayerCallbackListener(Lcom/jrm/localmm/business/video/VideoPlayView$playerCallback;)V

    :cond_3
    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->mVideoPlayViewTwo:Lcom/jrm/localmm/business/video/VideoPlayView;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->mVideoPlayViewTwo:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-virtual {v1}, Lcom/jrm/localmm/business/video/VideoPlayView;->stopPlayer()V

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->mVideoPlayViewTwo:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-virtual {v1, v3}, Lcom/jrm/localmm/business/video/VideoPlayView;->setPlayerCallbackListener(Lcom/jrm/localmm/business/video/VideoPlayView$playerCallback;)V

    :cond_4
    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoHandler:Landroid/os/Handler;

    invoke-virtual {v1, v5, v6, v7}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    :cond_5
    :goto_1
    return-void

    :cond_6
    invoke-direct {p0, v2, v2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->moveToNextOrPrevious(II)V

    goto :goto_0

    :cond_7
    const/4 v1, 0x2

    invoke-direct {p0, v2, v1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->moveToNextOrPrevious(II)V

    goto :goto_1
.end method

.method private showVideoInfoDialog()V
    .locals 7

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->total_time_video:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    move-object v3, v0

    check-cast v3, Ljava/lang/String;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView()Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/jrm/localmm/business/video/VideoPlayView;->getAudioCodecType()Ljava/lang/String;

    move-result-object v4

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView()Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/jrm/localmm/business/video/VideoPlayView;->getVideoInfo()Lcom/mstar/android/media/VideoCodecInfo;

    move-result-object v6

    if-eqz v6, :cond_0

    invoke-virtual {v6}, Lcom/mstar/android/media/VideoCodecInfo;->getCodecType()Ljava/lang/String;

    move-result-object v5

    new-instance v0, Lcom/jrm/localmm/ui/video/DetailInfoDialog;

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->video_position:[I

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v2}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getViewId()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    aget v2, v1, v2

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/jrm/localmm/ui/video/DetailInfoDialog;-><init>(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->mDetailInfoDialog:Lcom/jrm/localmm/ui/video/DetailInfoDialog;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->mDetailInfoDialog:Lcom/jrm/localmm/ui/video/DetailInfoDialog;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/DetailInfoDialog;->show()V

    :cond_0
    return-void
.end method

.method private showVideoListDialog()V
    .locals 2

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->video_position:[I

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getViewId()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    aget v0, v0, v1

    iput v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->currentViewPosition:I

    new-instance v0, Lcom/jrm/localmm/ui/video/VideoListDialog;

    invoke-direct {v0, p0}, Lcom/jrm/localmm/ui/video/VideoListDialog;-><init>(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->mVideoListDialog:Lcom/jrm/localmm/ui/video/VideoListDialog;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->mVideoListDialog:Lcom/jrm/localmm/ui/video/VideoListDialog;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/VideoListDialog;->show()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->mVideoListDialog:Lcom/jrm/localmm/ui/video/VideoListDialog;

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->toBePlayedHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/video/VideoListDialog;->setHandler(Landroid/os/Handler;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->mVideoListDialog:Lcom/jrm/localmm/ui/video/VideoListDialog;

    new-instance v1, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$9;

    invoke-direct {v1, p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$9;-><init>(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)V

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/video/VideoListDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    return-void
.end method

.method private showVideoTimeSetDialog()V
    .locals 4

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoTimeSelect(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->chooseTimePlayDialog:Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;

    if-nez v0, :cond_0

    new-instance v0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;

    const v2, 0x7f070001    # com.jrm.localmm.R.style.choose_time_dialog

    invoke-direct {v0, p0, v2}, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->chooseTimePlayDialog:Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->chooseTimePlayDialog:Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->requestWindowFeature(I)Z

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->chooseTimePlayDialog:Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    const/16 v2, 0x96

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    const/16 v2, 0xb4

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->chooseTimePlayDialog:Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;

    invoke-virtual {v2}, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->chooseTimePlayDialog:Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v3}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getViewId()I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->isSeekable(I)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    iget-object v3, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoHandler:Landroid/os/Handler;

    invoke-virtual {v2, v0, v3}, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->setVariable(ZLandroid/os/Handler;)V

    :cond_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->chooseTimePlayDialog:Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->show()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView()Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/jrm/localmm/business/video/VideoPlayView;->getDuration()I

    move-result v0

    iput v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->duration:I

    const-string v0, "VideoPlayActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getDuration()"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->duration:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->duration:I

    int-to-long v2, v0

    invoke-static {v2, v3}, Lcom/jrm/localmm/util/Tools;->formatDuration(J)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->chooseTimePlayDialog:Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;

    invoke-virtual {v2}, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->getVideoTimeDurationTextView()Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->currentPlayerPosition:I

    int-to-long v2, v2

    invoke-static {v2, v3}, Lcom/jrm/localmm/util/Tools;->formatDuration(J)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->chooseTimePlayDialog:Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;

    invoke-virtual {v3}, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->getVideoTimeCurrentPositionTextView()Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f06005c    # com.jrm.localmm.R.string.default_time

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->chooseTimePlayDialog:Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->setVariable(Z)V

    :cond_1
    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private slowForward()V
    .locals 3

    const/4 v2, 0x0

    iget-boolean v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isPlaying:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView()Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/jrm/localmm/business/video/VideoPlayView;->setVoice(Z)V

    invoke-virtual {p0, v2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->localResume(Z)V

    :cond_0
    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView()Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/jrm/localmm/business/video/VideoPlayView;->getPlayMode()I

    move-result v1

    iput v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->playSpeed:I

    const/4 v0, -0x2

    iget v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->playSpeed:I

    const/16 v2, 0x40

    if-ge v1, v2, :cond_1

    iget v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->playSpeed:I

    if-gez v1, :cond_1

    iget v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->playSpeed:I

    mul-int/lit8 v0, v1, 0x2

    :cond_1
    iput v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->playSpeed:I

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView()Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/jrm/localmm/business/video/VideoPlayView;->setPlayMode(I)Z

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v1, v0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setPlaySpeed(I)V

    return-void
.end method

.method private switchDualFocus()V
    .locals 11

    const/16 v10, 0xe

    const/4 v8, 0x4

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->switchFocusedView()V

    const/4 v1, 0x0

    const/4 v5, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getViewId()I

    move-result v0

    add-int/lit8 v6, v0, -0x1

    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isPrepared()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayList:Ljava/util/List;

    iget-object v4, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->video_position:[I

    aget v4, v4, v6

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jrm/localmm/business/data/BaseData;

    invoke-virtual {v0}, Lcom/jrm/localmm/business/data/BaseData;->getName()Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView()Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/jrm/localmm/business/video/VideoPlayView;->getDuration()I

    move-result v5

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->video_position:[I

    aget v0, v0, v6

    add-int/lit8 v3, v0, 0x1

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView()Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/jrm/localmm/business/video/VideoPlayView;->getPlayMode()I

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x1

    :cond_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    iget-object v4, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual/range {v0 .. v5}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->refreshControlInfo(Ljava/lang/String;IIII)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayAbDialog:[Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    aget-object v0, v0, v6

    if-eqz v0, :cond_2

    new-instance v7, Landroid/os/Message;

    invoke-direct {v7}, Landroid/os/Message;-><init>()V

    const/4 v0, 0x2

    iput v0, v7, Landroid/os/Message;->what:I

    const/4 v0, 0x0

    iput v0, v7, Landroid/os/Message;->arg1:I

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayAbDialog:[Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    aget-object v0, v0, v6

    iget-boolean v0, v0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->bFlag:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayAbDialog:[Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    aget-object v0, v0, v6

    iget-object v0, v0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->sharedata:Landroid/content/SharedPreferences;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    iput v0, v7, Landroid/os/Message;->arg1:I

    :cond_1
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayAbDialog:[Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    aget-object v0, v0, v6

    iget-object v0, v0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v7}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :goto_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getViewId()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->checkABCycle(I)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoHandler:Landroid/os/Handler;

    invoke-virtual {v0, v10}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoHandler:Landroid/os/Handler;

    const-wide/16 v8, 0x3e8

    invoke-virtual {v0, v10, v8, v9}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    return-void

    :cond_2
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_playA:Landroid/widget/ImageView;

    invoke-virtual {v0, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_playB:Landroid/widget/ImageView;

    invoke-virtual {v0, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method private switchDualMode()V
    .locals 6

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getDualVideoMode()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->mThreeDimentionControl:Lcom/jrm/localmm/ui/video/ThreeDimentionControl;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->isThreeDMode()Z

    move-result v0

    if-nez v0, :cond_0

    const v0, 0x7f0600c3    # com.jrm.localmm.R.string.play_setting_0_value_1

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v4}, Lcom/jrm/localmm/util/Tools;->getPlaySettingOpt(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0600a1    # com.jrm.localmm.R.string.can_not_open_dual

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->showToastTip(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isPIPMode()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0600a8    # com.jrm.localmm.R.string.can_not_open_dual_pip

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->showToastTip(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getDualVideoMode()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->showVideoFocus(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    const/4 v1, 0x5

    iput v1, v0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->currentDualMode:I

    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->switchDualFocus()V

    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->showVideoListDialog()V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v0, v4}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView(I)Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v0, v4}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView(I)Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/jrm/localmm/business/video/VideoPlayView;->getCurrentPosition()I

    move-result v0

    :goto_1
    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v2, v4}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->isSeekable(I)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayAbDialog:[Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v3}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getViewId()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    aget-object v2, v2, v3

    if-eqz v2, :cond_6

    :cond_4
    invoke-virtual {p0, v1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->localPauseFromDualSwitch(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getViewId()I

    move-result v0

    if-ne v0, v5, :cond_5

    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->switchDualFocus()V

    :cond_5
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->openOrCloseDualDecode(Z)V

    invoke-virtual {p0, v1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->localResumeFromDualSwitch(Z)V

    goto :goto_0

    :cond_6
    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v1, v5, v4}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setSeekVar(IZ)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->seekBarListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    iget-object v2, v2, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->videoSeekBar:Landroid/widget/SeekBar;

    invoke-interface {v1, v2, v0, v4}, Landroid/widget/SeekBar$OnSeekBarChangeListener;->onProgressChanged(Landroid/widget/SeekBar;IZ)V

    goto :goto_0

    :cond_7
    move v0, v1

    goto :goto_1
.end method

.method private wind()V
    .locals 2

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->cancleDelayHide()V

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->videoSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v0

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView()Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/jrm/localmm/business/video/VideoPlayView;->getDuration()I

    move-result v1

    if-gt v0, v1, :cond_0

    add-int/lit16 v0, v0, 0x2710

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView()Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/jrm/localmm/business/video/VideoPlayView;->seekTo(I)V

    :cond_0
    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->hideControlDelay()V

    return-void
.end method


# virtual methods
.method protected ThreeDInit()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/16 v3, 0x10

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->mThreeDimentionControl:Lcom/jrm/localmm/ui/video/ThreeDimentionControl;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->checkMvcSource()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->mThreeDimentionControl:Lcom/jrm/localmm/ui/video/ThreeDimentionControl;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->isVideoSourceMvc()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->mThreeDimentionControl:Lcom/jrm/localmm/ui/video/ThreeDimentionControl;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->getThreeDMode()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->mThreeDimentionControl:Lcom/jrm/localmm/ui/video/ThreeDimentionControl;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->getThreeDVideoDisplayFormat()Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    move-result-object v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;->E_ThreeD_Video_DISPLAYFORMAT_NONE:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    if-ne v0, v1, :cond_2

    iget-boolean v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->playerIsCreated:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->mThreeDimentionControl:Lcom/jrm/localmm/ui/video/ThreeDimentionControl;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->ThreeDInit()V

    iput-boolean v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->playerIsCreated:Z

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->mThreeDimentionControl:Lcom/jrm/localmm/ui/video/ThreeDimentionControl;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->refreshThreeDMode()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->mThreeDimentionControl:Lcom/jrm/localmm/ui/video/ThreeDimentionControl;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->getThreeDVideoDisplayFormat()Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    move-result-object v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;->E_ThreeD_Video_DISPLAYFORMAT_AUTO:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoHandler:Landroid/os/Handler;

    const-wide/16 v1, 0x1388

    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    :cond_3
    iget-boolean v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->playerIsCreated:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->mThreeDimentionControl:Lcom/jrm/localmm/ui/video/ThreeDimentionControl;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->ThreeDInit()V

    iput-boolean v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->playerIsCreated:Z

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->mThreeDimentionControl:Lcom/jrm/localmm/ui/video/ThreeDimentionControl;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->refreshThreeDMode()V

    goto :goto_0
.end method

.method public cancleDelayHide()V
    .locals 2

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoHandler:Landroid/os/Handler;

    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    return-void
.end method

.method public changeSource()V
    .locals 2

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$20;

    invoke-direct {v1, p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$20;-><init>(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method public getAudioTrackCount()I
    .locals 7

    const/16 v6, 0x23

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView()Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/jrm/localmm/business/video/VideoPlayView;->getMMediaPlayer()Lcom/mstar/android/media/MMediaPlayer;

    move-result-object v3

    const-string v4, "VideoPlayActivity"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getAudioTrackCount: (player == null) = "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    if-nez v3, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-nez v3, :cond_2

    :cond_0
    :goto_1
    return v1

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    invoke-virtual {v3, v1, v1}, Lcom/mstar/android/media/MMediaPlayer;->getMetadata(ZZ)Landroid/media/Metadata;

    move-result-object v0

    const-string v3, "VideoPlayActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getAudioTrackCount: (data != null) = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    if-eqz v0, :cond_3

    move v2, v1

    :cond_3
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v0, :cond_0

    invoke-virtual {v0, v6}, Landroid/media/Metadata;->has(I)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0, v6}, Landroid/media/Metadata;->getInt(I)I

    move-result v1

    const-string v0, "VideoPlayActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getAudioTrackCount: totalTrackNum = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public getAudioTrackDuration()I
    .locals 4

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v2}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView()Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/jrm/localmm/business/video/VideoPlayView;->getAudioTrackInfo(Z)Lcom/mstar/android/media/AudioTrackInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/mstar/android/media/AudioTrackInfo;->getTotalPlayTime()I

    move-result v1

    if-lez v1, :cond_0

    :goto_0
    return v1

    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public getAudioTrackLanguage()Ljava/lang/String;
    .locals 4

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView()Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/business/video/VideoPlayView;->getAudioTrackInfo(Z)Lcom/mstar/android/media/AudioTrackInfo;

    move-result-object v0

    const-string v1, "VideoPlayActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "*******AudioTracklanguage*****"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/mstar/android/media/AudioTrackInfo;->getAudioLanguageType()Ljava/lang/String;

    move-result-object v0

    const-string v1, "VideoPlayActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "*******AudioTracklanguage*****"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0600c5    # com.jrm.localmm.R.string.video_size_unknown

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getBreakPointFlag()Z
    .locals 1

    sget-boolean v0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isNeedBreakPointPlay:Z

    return v0
.end method

.method public getCurrentAudioTrackId()I
    .locals 9

    const/16 v8, 0x24

    const/16 v7, 0x23

    const/4 v5, 0x0

    const/4 v4, 0x1

    iget-object v6, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v6}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView()Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v6

    invoke-virtual {v6}, Lcom/jrm/localmm/business/video/VideoPlayView;->getMMediaPlayer()Lcom/mstar/android/media/MMediaPlayer;

    move-result-object v3

    if-nez v3, :cond_0

    :goto_0
    return v4

    :cond_0
    invoke-virtual {v3, v4, v4}, Lcom/mstar/android/media/MMediaPlayer;->getMetadata(ZZ)Landroid/media/Metadata;

    move-result-object v2

    if-eqz v2, :cond_3

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {v2, v7}, Landroid/media/Metadata;->has(I)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v2, v7}, Landroid/media/Metadata;->getInt(I)I

    move-result v1

    :cond_1
    invoke-virtual {v2, v8}, Landroid/media/Metadata;->has(I)Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {v2, v8}, Landroid/media/Metadata;->getInt(I)I

    move-result v0

    if-eqz v1, :cond_2

    rem-int v4, v0, v1

    goto :goto_0

    :cond_2
    move v4, v5

    goto :goto_0

    :cond_3
    move v4, v5

    goto :goto_0
.end method

.method public getPlaySettingSubtitleDialog(I)Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->playSettingSubtitleDialogOne:Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->playSettingSubtitleDialogTwo:Lcom/jrm/localmm/ui/video/PlaySettingSubtitleDialog;

    goto :goto_0
.end method

.method public getVideoHandler()Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoHandler:Landroid/os/Handler;

    return-object v0
.end method

.method public getVideoPlayHolder()Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    .locals 1

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    return-object v0
.end method

.method protected getVideoPlayList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/jrm/localmm/business/data/BaseData;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayList:Ljava/util/List;

    return-object v0
.end method

.method public hideControlDelay()V
    .locals 4

    const/16 v3, 0xd

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoHandler:Landroid/os/Handler;

    const-wide/16 v1, 0x3a98

    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    return-void
.end method

.method public is3D(I)Z
    .locals 7

    const/16 v6, 0x39

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView()Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/jrm/localmm/business/video/VideoPlayView;->getMMediaPlayer()Lcom/mstar/android/media/MMediaPlayer;

    move-result-object v3

    const-string v4, "VideoPlayActivity"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "***player***"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    if-nez v3, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-nez v3, :cond_1

    :goto_1
    return v2

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    invoke-virtual {v3, v1, v1}, Lcom/mstar/android/media/MMediaPlayer;->getMetadata(ZZ)Landroid/media/Metadata;

    move-result-object v0

    const-string v3, "VideoPlayActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "***data***"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    if-eqz v0, :cond_2

    move v2, v1

    :cond_2
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v0, :cond_3

    const-string v2, "VideoPlayActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "***VIDEO_IS_3D***"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0, v6}, Landroid/media/Metadata;->has(I)Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0, v6}, Landroid/media/Metadata;->has(I)Z

    move-result v2

    if-eqz v2, :cond_3

    const-string v2, "VideoPlayActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "*****VIDEO_IS_3D******"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0, v6}, Landroid/media/Metadata;->getBoolean(I)Z

    move-result v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    move v2, v1

    goto :goto_1
.end method

.method public isSeekable(I)Z
    .locals 7

    const/4 v6, 0x4

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView()Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/jrm/localmm/business/video/VideoPlayView;->getMMediaPlayer()Lcom/mstar/android/media/MMediaPlayer;

    move-result-object v3

    const-string v4, "VideoPlayActivity"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "***player***"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    if-nez v3, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-nez v3, :cond_1

    :goto_1
    return v2

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    invoke-virtual {v3, v1, v1}, Lcom/mstar/android/media/MMediaPlayer;->getMetadata(ZZ)Landroid/media/Metadata;

    move-result-object v0

    const-string v3, "VideoPlayActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "***data***"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    if-eqz v0, :cond_3

    :goto_2
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v0, :cond_2

    const-string v1, "VideoPlayActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "***SEEK_AVAILABLE***"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0, v6}, Landroid/media/Metadata;->has(I)Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0, v6}, Landroid/media/Metadata;->has(I)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v0, v6}, Landroid/media/Metadata;->getBoolean(I)Z

    move-result v0

    invoke-virtual {v1, p1, v0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setSeekVar(IZ)V

    const-string v0, "VideoPlayActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "*****SEEK_AVAILABLE******"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v2, p1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->isSeekable(I)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v0, p1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->isSeekable(I)Z

    move-result v2

    goto :goto_1

    :cond_3
    move v1, v2

    goto :goto_2
.end method

.method protected localPause(Z)V
    .locals 3
    .param p1    # Z

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView()Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/jrm/localmm/business/video/VideoPlayView;->setVoice(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView()Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/jrm/localmm/business/video/VideoPlayView;->setPlayMode(I)Z

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView()Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/jrm/localmm/business/video/VideoPlayView;->pause()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView()Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/business/video/VideoPlayView;->setVoice(Z)V

    iput-boolean v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isPlaying:Z

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    iget-boolean v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isPlaying:Z

    invoke-virtual {v0, p1, v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoPlaySelect(ZZ)V

    return-void
.end method

.method protected localPauseFromDualSwitch(Z)V
    .locals 3
    .param p1    # Z

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView(I)Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/jrm/localmm/business/video/VideoPlayView;->setVoice(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView(I)Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/jrm/localmm/business/video/VideoPlayView;->setPlayMode(I)Z

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView(I)Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/jrm/localmm/business/video/VideoPlayView;->pause()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView(I)Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/business/video/VideoPlayView;->setVoice(Z)V

    return-void
.end method

.method protected localResume(Z)V
    .locals 4
    .param p1    # Z

    const/16 v3, 0xe

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView()Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/jrm/localmm/business/video/VideoPlayView;->start()V

    iput-boolean v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isPlaying:Z

    const/4 v0, 0x0

    iput v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->playSpeed:I

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setPlaySpeed(I)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    iget-boolean v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isPlaying:Z

    invoke-virtual {v0, p1, v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoPlaySelect(ZZ)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoHandler:Landroid/os/Handler;

    const-wide/16 v1, 0x3e8

    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    return-void
.end method

.method protected localResumeFromDualSwitch(Z)V
    .locals 4
    .param p1    # Z

    const/16 v3, 0xe

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView(I)Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/jrm/localmm/business/video/VideoPlayView;->start()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoHandler:Landroid/os/Handler;

    const-wide/16 v1, 0x3e8

    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    return-void
.end method

.method protected localResumeFromSpeed(Z)V
    .locals 4
    .param p1    # Z

    const/16 v3, 0xe

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView()Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/business/video/VideoPlayView;->setVoice(Z)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView()Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/jrm/localmm/business/video/VideoPlayView;->setPlayMode(I)Z

    iput-boolean v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isPlaying:Z

    iput v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->playSpeed:I

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v0, v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setPlaySpeed(I)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    iget-boolean v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isPlaying:Z

    invoke-virtual {v0, p1, v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoPlaySelect(ZZ)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 10
    .param p1    # Landroid/os/Bundle;

    const/4 v9, 0x1

    const/4 v8, 0x0

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getWindow()Landroid/view/Window;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v9}, Landroid/view/View;->setSystemUiVisibility(I)V

    const v5, 0x7f060062    # com.jrm.localmm.R.string.buffering

    invoke-direct {p0, v5}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->showProgressDialog(I)V

    iget-object v5, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->video_position:[I

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v6

    const-string v7, "com.jrm.index"

    invoke-virtual {v6, v7, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    aput v6, v5, v8

    invoke-static {}, Lcom/jrm/localmm/ui/MediaContainerApplication;->getInstance()Lcom/jrm/localmm/ui/MediaContainerApplication;

    move-result-object v5

    const/4 v6, 0x4

    invoke-virtual {v5, v6}, Lcom/jrm/localmm/ui/MediaContainerApplication;->getMediaData(I)Ljava/util/List;

    move-result-object v5

    iput-object v5, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayList:Ljava/util/List;

    iget-object v5, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayList:Ljava/util/List;

    iget-object v6, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->video_position:[I

    aget v6, v6, v8

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/jrm/localmm/business/data/BaseData;

    invoke-virtual {v5}, Lcom/jrm/localmm/business/data/BaseData;->getPath()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayList:Ljava/util/List;

    iget-object v6, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->video_position:[I

    aget v6, v6, v8

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/jrm/localmm/business/data/BaseData;

    invoke-virtual {v5}, Lcom/jrm/localmm/business/data/BaseData;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const-string v6, "sourceFrom"

    invoke-virtual {v5, v6, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    iput v5, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->sourceFrom:I

    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "---video_path---"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "---videoName---"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v5

    invoke-interface {v5}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v5

    sput v5, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->screenWidth:I

    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v5

    sput v5, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->screenHeight:I

    const v5, 0x7f030022    # com.jrm.localmm.R.layout.video_player_frame2

    invoke-virtual {p0, v5}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->setContentView(I)V

    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->InitView()V

    new-instance v2, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;

    invoke-direct {v2, p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$ControlButtonListener;-><init>(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)V

    iget-object v5, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v5, v2}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->initObject()V

    invoke-static {p0}, Lcom/jrm/localmm/business/video/BreakPointRecord;->getBreakPointFlag(Landroid/content/Context;)Z

    move-result v5

    sput-boolean v5, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isNeedBreakPointPlay:Z

    invoke-direct {p0, v4, v9}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->InitVideoPlayer(Ljava/lang/String;I)V

    iget-object v5, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v5, v3}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoName(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    iget-object v6, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->video_position:[I

    iget-object v7, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v7}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getViewId()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    aget v6, v6, v7

    add-int/lit8 v6, v6, 0x1

    iget-object v7, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayList:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    invoke-virtual {v5, v6, v7}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoListText(II)V

    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->acquireWakeLock()V

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->hideControlDelay()V

    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    const-string v5, "android.intent.action.ACTION_SHUTDOWN"

    invoke-virtual {v1, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v5, "source.switch.from.storage"

    invoke-virtual {v1, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->shutDownReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v5, v1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->changeSource()V

    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->init3DMode()V

    const-string v5, "window"

    invoke-static {v5}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v5

    invoke-static {v5}, Landroid/view/IWindowManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindowManager;

    move-result-object v5

    iput-object v5, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->wm:Landroid/view/IWindowManager;

    return-void
.end method

.method protected onDestroy()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x1

    const-string v0, "VideoPlayActivity"

    const-string v1, "***************onDestroy********"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->mVideoPlayViewOne:Lcom/jrm/localmm/business/video/VideoPlayView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->mVideoPlayViewOne:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-virtual {v0, v3}, Lcom/jrm/localmm/business/video/VideoPlayView;->setPlayerCallbackListener(Lcom/jrm/localmm/business/video/VideoPlayView$playerCallback;)V

    :cond_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->mVideoPlayViewTwo:Lcom/jrm/localmm/business/video/VideoPlayView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->mVideoPlayViewTwo:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-virtual {v0, v3}, Lcom/jrm/localmm/business/video/VideoPlayView;->setPlayerCallbackListener(Lcom/jrm/localmm/business/video/VideoPlayView$playerCallback;)V

    :cond_1
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v0, v2, v2}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->setVideoWindowVisable(ZZ)V

    invoke-static {}, Lcom/jrm/localmm/business/video/BreakPointRecord;->closeDB()V

    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->dismissProgressDialog()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->netDisconnectReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->diskChangeReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->shutDownReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$3;

    invoke-direct {v1, p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$3;-><init>(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/16 v3, 0x42

    const/16 v2, 0x17

    const/4 v0, 0x1

    const/16 v1, 0x7e

    if-eq v1, p1, :cond_0

    const/16 v1, 0x7f

    if-eq v1, p1, :cond_0

    const/16 v1, 0x57

    if-eq v1, p1, :cond_0

    const/16 v1, 0x58

    if-ne v1, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-boolean v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isControllerShow:Z

    if-nez v1, :cond_2

    if-eq v3, p1, :cond_2

    if-eq v2, p1, :cond_2

    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0

    :cond_2
    if-eq v3, p1, :cond_0

    if-eq v2, p1, :cond_0

    const/4 v1, 0x4

    if-eq v1, p1, :cond_0

    const/16 v1, 0x15

    if-eq v1, p1, :cond_0

    const/16 v1, 0x16

    if-eq v1, p1, :cond_0

    const/16 v1, 0x14

    if-eq v1, p1, :cond_0

    const/16 v1, 0x13

    if-eq v1, p1, :cond_0

    const/16 v1, 0x52

    if-eq v1, p1, :cond_0

    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 12
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v6, 0x0

    const/4 v5, 0x0

    const/4 v9, 0x1

    const-string v1, "****************8"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "*****keyCode******"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v7, 0x0

    sparse-switch p1, :sswitch_data_0

    const/4 v7, 0x0

    :cond_0
    :goto_0
    if-eqz v7, :cond_12

    move v1, v9

    :goto_1
    return v1

    :sswitch_0
    iget-boolean v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isPlaying:Z

    if-nez v1, :cond_2

    invoke-virtual {p0, v9}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->localResume(Z)V

    :cond_1
    :goto_2
    const/4 v7, 0x1

    goto :goto_0

    :cond_2
    iget v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->playSpeed:I

    if-eqz v1, :cond_1

    invoke-virtual {p0, v9}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->localResumeFromSpeed(Z)V

    goto :goto_2

    :sswitch_1
    iget-boolean v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isPlaying:Z

    if-eqz v1, :cond_3

    invoke-virtual {p0, v9}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->localPause(Z)V

    :cond_3
    const/4 v7, 0x1

    goto :goto_0

    :sswitch_2
    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isPrepared()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getViewId()I

    move-result v1

    invoke-direct {p0, v9, v1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->moveToNextOrPrevious(II)V

    goto :goto_0

    :sswitch_3
    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isPrepared()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, -0x1

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v2}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getViewId()I

    move-result v2

    invoke-direct {p0, v1, v2}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->moveToNextOrPrevious(II)V

    goto :goto_0

    :sswitch_4
    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->slowForward()V

    goto :goto_0

    :sswitch_5
    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->fastForward()V

    goto :goto_0

    :sswitch_6
    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoHandler:Landroid/os/Handler;

    const/16 v2, 0xe

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->mVideoPlayViewOne:Lcom/jrm/localmm/business/video/VideoPlayView;

    if-eqz v1, :cond_5

    iget-boolean v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isOnePrepared:Z

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayList:Ljava/util/List;

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->video_position:[I

    aget v2, v2, v5

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jrm/localmm/business/data/BaseData;

    invoke-virtual {v1}, Lcom/jrm/localmm/business/data/BaseData;->getPath()Ljava/lang/String;

    move-result-object v2

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->mVideoPlayViewOne:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-virtual {v1}, Lcom/jrm/localmm/business/video/VideoPlayView;->getCurrentPosition()I

    move-result v3

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayList:Ljava/util/List;

    iget-object v4, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->video_position:[I

    aget v4, v4, v5

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jrm/localmm/business/data/BaseData;

    invoke-virtual {v1}, Lcom/jrm/localmm/business/data/BaseData;->getSize()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v3, v1, p0}, Lcom/jrm/localmm/business/video/BreakPointRecord;->addData(Ljava/lang/String;ILjava/lang/String;Landroid/content/Context;)V

    :cond_4
    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->mVideoPlayViewOne:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-virtual {v1}, Lcom/jrm/localmm/business/video/VideoPlayView;->stopPlayer()V

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->mVideoPlayViewOne:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-virtual {v1, v6}, Lcom/jrm/localmm/business/video/VideoPlayView;->setPlayerCallbackListener(Lcom/jrm/localmm/business/video/VideoPlayView$playerCallback;)V

    :cond_5
    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->mVideoPlayViewTwo:Lcom/jrm/localmm/business/video/VideoPlayView;

    if-eqz v1, :cond_7

    iget-boolean v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isTwoPrepared:Z

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayList:Ljava/util/List;

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->video_position:[I

    aget v2, v2, v9

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jrm/localmm/business/data/BaseData;

    invoke-virtual {v1}, Lcom/jrm/localmm/business/data/BaseData;->getPath()Ljava/lang/String;

    move-result-object v2

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->mVideoPlayViewOne:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-virtual {v1}, Lcom/jrm/localmm/business/video/VideoPlayView;->getCurrentPosition()I

    move-result v3

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayList:Ljava/util/List;

    iget-object v4, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->video_position:[I

    aget v4, v4, v9

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jrm/localmm/business/data/BaseData;

    invoke-virtual {v1}, Lcom/jrm/localmm/business/data/BaseData;->getSize()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v3, v1, p0}, Lcom/jrm/localmm/business/video/BreakPointRecord;->addData(Ljava/lang/String;ILjava/lang/String;Landroid/content/Context;)V

    :cond_6
    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->mVideoPlayViewTwo:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-virtual {v1}, Lcom/jrm/localmm/business/video/VideoPlayView;->stopPlayer()V

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->mVideoPlayViewTwo:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-virtual {v1, v6}, Lcom/jrm/localmm/business/video/VideoPlayView;->setPlayerCallbackListener(Lcom/jrm/localmm/business/video/VideoPlayView$playerCallback;)V

    :cond_7
    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoHandler:Landroid/os/Handler;

    const/16 v2, 0x16

    const-wide/16 v3, 0x12c

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    const/4 v7, 0x1

    goto/16 :goto_0

    :sswitch_7
    iget-boolean v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isControllerShow:Z

    if-nez v1, :cond_8

    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v1

    goto/16 :goto_1

    :cond_8
    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->cancleDelayHide()V

    iget-boolean v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isCanSeek:Z

    if-nez v1, :cond_9

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v1, p1, p2}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->processLeftKey(ILandroid/view/KeyEvent;)Z

    :goto_3
    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->hideControlDelay()V

    const/4 v7, 0x1

    goto/16 :goto_0

    :cond_9
    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->rewind()V

    goto :goto_3

    :sswitch_8
    iget-boolean v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isControllerShow:Z

    if-nez v1, :cond_a

    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v1

    goto/16 :goto_1

    :cond_a
    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->cancleDelayHide()V

    iget-boolean v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isCanSeek:Z

    if-nez v1, :cond_b

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v1, p1, p2}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->processRightKey(ILandroid/view/KeyEvent;)Z

    :goto_4
    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->hideControlDelay()V

    const/4 v7, 0x1

    goto/16 :goto_0

    :cond_b
    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->wind()V

    goto :goto_4

    :sswitch_9
    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    iget-boolean v1, v1, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->isDualLayoutShow:Z

    if-eqz v1, :cond_c

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v1, p1, p2}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->processOkKey(ILandroid/view/KeyEvent;)Z

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->hideControlDelay()V

    :goto_5
    const/4 v7, 0x1

    goto/16 :goto_0

    :cond_c
    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->registerListeners()V

    goto :goto_5

    :sswitch_a
    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    iget-boolean v1, v1, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->isDualLayoutShow:Z

    if-eqz v1, :cond_d

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->cancleDelayHide()V

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v1, p1, p2}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->processUpKey(ILandroid/view/KeyEvent;)Z

    :cond_d
    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    iget-boolean v1, v1, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->isVoiceLayoutShow:Z

    if-eqz v1, :cond_e

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->cancleDelayHide()V

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v1, v9}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->addVoice(Z)V

    :cond_e
    const/4 v7, 0x1

    goto/16 :goto_0

    :sswitch_b
    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    iget-boolean v1, v1, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->isDualLayoutShow:Z

    if-eqz v1, :cond_f

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->cancleDelayHide()V

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v1, p1, p2}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->processDownKey(ILandroid/view/KeyEvent;)Z

    :cond_f
    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    iget-boolean v1, v1, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->isVoiceLayoutShow:Z

    if-eqz v1, :cond_10

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->cancleDelayHide()V

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v1, v5}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->addVoice(Z)V

    :cond_10
    const/4 v7, 0x1

    goto/16 :goto_0

    :sswitch_c
    iget-boolean v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isControllerShow:Z

    if-nez v1, :cond_11

    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v1

    goto/16 :goto_1

    :cond_11
    const/4 v7, 0x1

    goto/16 :goto_0

    :sswitch_d
    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v1}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getViewId()I

    move-result v1

    add-int/lit8 v8, v1, -0x1

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->startRenderingTime:[J

    aget-wide v1, v1, v8

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->startPreTime:[J

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    add-int/lit8 v3, v8, 0x1

    invoke-virtual {v2, v3}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView(I)Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/jrm/localmm/business/video/VideoPlayView;->getStartTime()J

    move-result-wide v2

    aput-wide v2, v1, v8

    new-instance v0, Lcom/jrm/localmm/ui/video/VideoStartTimeDialog;

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->endPreTime:[J

    aget-wide v1, v1, v8

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->startPreTime:[J

    aget-wide v3, v3, v8

    sub-long v2, v1, v3

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->startRenderingTime:[J

    aget-wide v4, v1, v8

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->endPreTime:[J

    aget-wide v10, v1, v8

    sub-long/2addr v4, v10

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayList:Ljava/util/List;

    iget-object v6, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->video_position:[I

    aget v6, v6, v8

    invoke-interface {v1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jrm/localmm/business/data/BaseData;

    invoke-virtual {v1}, Lcom/jrm/localmm/business/data/BaseData;->getName()Ljava/lang/String;

    move-result-object v6

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/jrm/localmm/ui/video/VideoStartTimeDialog;-><init>(Landroid/content/Context;JJLjava/lang/String;)V

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/VideoStartTimeDialog;->show()V

    goto/16 :goto_0

    :cond_12
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v1

    goto/16 :goto_1

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_6
        0x7 -> :sswitch_d
        0x13 -> :sswitch_a
        0x14 -> :sswitch_b
        0x15 -> :sswitch_7
        0x16 -> :sswitch_8
        0x17 -> :sswitch_9
        0x42 -> :sswitch_9
        0x52 -> :sswitch_c
        0x57 -> :sswitch_2
        0x58 -> :sswitch_3
        0x59 -> :sswitch_4
        0x5a -> :sswitch_5
        0x7e -> :sswitch_0
        0x7f -> :sswitch_1
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 2

    const-string v0, "VideoPlayActivity"

    const-string v1, "***************onPause********"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$4;

    invoke-direct {v1, p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$4;-><init>(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    return-void
.end method

.method protected onResume()V
    .locals 4

    const-string v2, "VideoPlayActivity"

    const-string v3, "***************onResume********"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->stopMediascanner()V

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v2, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v2, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v2, "file"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->diskChangeReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v2, v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "com.mstar.localmm.network.disconnect"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->netDisconnectReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v2, v1}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    const/4 v2, 0x1

    invoke-static {p0, v2}, Lcom/jrm/localmm/util/Tools;->initSubtitleSettingOpt(Landroid/content/Context;I)[Ljava/lang/String;

    const/4 v2, 0x2

    invoke-static {p0, v2}, Lcom/jrm/localmm/util/Tools;->initSubtitleSettingOpt(Landroid/content/Context;I)[Ljava/lang/String;

    new-instance v2, Ljava/lang/Thread;

    new-instance v3, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$5;

    invoke-direct {v3, p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity$5;-><init>(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;)V

    invoke-direct {v2, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    return-void
.end method

.method protected onStop()V
    .locals 3

    const/4 v2, 0x0

    const-string v0, "VideoPlayActivity"

    const-string v1, "***************onStop****finish****"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->mVideoPlayViewOne:Lcom/jrm/localmm/business/video/VideoPlayView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->mVideoPlayViewOne:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-virtual {v0}, Lcom/jrm/localmm/business/video/VideoPlayView;->stopPlayer()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->mVideoPlayViewOne:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-virtual {v0, v2}, Lcom/jrm/localmm/business/video/VideoPlayView;->setPlayerCallbackListener(Lcom/jrm/localmm/business/video/VideoPlayView$playerCallback;)V

    :cond_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->mVideoPlayViewTwo:Lcom/jrm/localmm/business/video/VideoPlayView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->mVideoPlayViewTwo:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-virtual {v0}, Lcom/jrm/localmm/business/video/VideoPlayView;->stopPlayer()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->mVideoPlayViewTwo:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-virtual {v0, v2}, Lcom/jrm/localmm/business/video/VideoPlayView;->setPlayerCallbackListener(Lcom/jrm/localmm/business/video/VideoPlayView$playerCallback;)V

    :cond_1
    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->finish()V

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1    # Landroid/view/MotionEvent;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isControllerShow:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->showController()V

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->hideControlDelay()V

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setSystemUiVisibility(I)V

    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public setAudioTrack(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->videoPlayerHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView()Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/jrm/localmm/business/video/VideoPlayView;->setAudioTrack(I)V

    return-void
.end method

.method public setBreakPointFlag(Z)V
    .locals 0
    .param p1    # Z

    sput-boolean p1, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->isNeedBreakPointPlay:Z

    return-void
.end method

.method public startMediascanner()V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "action_media_scanner_start"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->sendBroadcast(Landroid/content/Intent;)V

    const-string v0, "VideoPlayActivity"

    const-string v1, "startMediascanner"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public stopMediascanner()V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "action_media_scanner_stop"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->sendBroadcast(Landroid/content/Intent;)V

    const-string v0, "VideoPlayActivity"

    const-string v1, "stopMediascanner"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method
