.class Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog$2;
.super Ljava/lang/Object;
.source "VideoPlaySettingDialog.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->setListeners()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;


# direct methods
.method constructor <init>(Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog$2;->this$0:Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 9
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    const/4 v6, 0x1

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog$2;->this$0:Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->viewId:I
    invoke-static {v5}, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->access$100(Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;)I

    move-result v5

    invoke-static {v4, v5}, Lcom/jrm/localmm/util/Tools;->getPlaySettingOpt(II)Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog$2;->this$0:Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->viewId:I
    invoke-static {v4}, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->access$100(Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;)I

    move-result v4

    invoke-static {v6, v4}, Lcom/jrm/localmm/util/Tools;->getPlaySettingOpt(II)Ljava/lang/String;

    move-result-object v3

    if-nez p3, :cond_1

    iget-object v4, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog$2;->this$0:Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;
    invoke-static {v4}, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->access$000(Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;)Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    move-result-object v4

    const v5, 0x7f0600c3    # com.jrm.localmm.R.string.play_setting_0_value_1

    invoke-virtual {v4, v5}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog$2;->this$0:Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;

    invoke-virtual {v4}, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->dismiss()V

    iget-object v4, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog$2;->this$0:Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;

    new-instance v5, Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;

    iget-object v6, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog$2;->this$0:Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;
    invoke-static {v6}, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->access$000(Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;)Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    move-result-object v6

    iget-object v7, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog$2;->this$0:Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;

    invoke-direct {v5, v6, v7}, Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;-><init>(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;)V

    # setter for: Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->threeDSettingDialog:Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;
    invoke-static {v4, v5}, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->access$202(Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;)Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;

    iget-object v4, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog$2;->this$0:Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->threeDSettingDialog:Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;
    invoke-static {v4}, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->access$200(Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;)Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;

    move-result-object v4

    invoke-virtual {v4}, Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;->show()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-ne p3, v6, :cond_2

    iget-object v4, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog$2;->this$0:Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;
    invoke-static {v4}, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->access$000(Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;)Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    move-result-object v4

    const v5, 0x7f0600c4    # com.jrm.localmm.R.string.play_setting_0_value_2

    invoke-virtual {v4, v5}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    iget-object v4, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog$2;->this$0:Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;

    invoke-virtual {v4}, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->dismiss()V

    new-instance v1, Landroid/os/Message;

    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v4, "index"

    invoke-virtual {v0, v4, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    const/16 v4, 0x7c

    iput v4, v1, Landroid/os/Message;->what:I

    iget-object v4, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog$2;->this$0:Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;
    invoke-static {v4}, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->access$000(Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;)Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    move-result-object v4

    invoke-virtual {v4}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getVideoHandler()Landroid/os/Handler;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    :cond_2
    const/4 v4, 0x2

    if-ne p3, v4, :cond_3

    iget-object v4, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog$2;->this$0:Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;

    # invokes: Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->changeBreakPointFlag()V
    invoke-static {v4}, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->access$300(Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;)V

    iget-object v4, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog$2;->this$0:Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->adapter:Lcom/jrm/localmm/business/adapter/VideoPlaySettingListAdapter;
    invoke-static {v4}, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->access$400(Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;)Lcom/jrm/localmm/business/adapter/VideoPlaySettingListAdapter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/jrm/localmm/business/adapter/VideoPlaySettingListAdapter;->notifyDataSetChanged()V

    goto :goto_0

    :cond_3
    const/4 v4, 0x3

    if-ne p3, v4, :cond_0

    iget-object v4, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog$2;->this$0:Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;

    invoke-virtual {v4}, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->dismiss()V

    iget-object v4, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog$2;->this$0:Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;

    new-instance v5, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;

    iget-object v6, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog$2;->this$0:Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;
    invoke-static {v6}, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->access$000(Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;)Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    move-result-object v6

    const/high16 v7, 0x7f070000    # com.jrm.localmm.R.style.dialog

    iget-object v8, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog$2;->this$0:Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;

    invoke-direct {v5, v6, v7, v8}, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;-><init>(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;ILcom/jrm/localmm/ui/video/VideoPlaySettingDialog;)V

    # setter for: Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->playSettingAudioTrackDialog:Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;
    invoke-static {v4, v5}, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->access$502(Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;)Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;

    iget-object v4, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog$2;->this$0:Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->playSettingAudioTrackDialog:Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;
    invoke-static {v4}, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->access$500(Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;)Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;

    move-result-object v4

    invoke-virtual {v4}, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->show()V

    goto :goto_0
.end method
