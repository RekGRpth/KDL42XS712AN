.class Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog$3;
.super Ljava/lang/Object;
.source "PlaySettingAudioTrackDialog.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;


# direct methods
.method constructor <init>(Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog$3;->this$0:Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 5
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # Landroid/view/KeyEvent;

    const/4 v4, 0x0

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog$3;->this$0:Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;

    # getter for: Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->isResponseKey:Z
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->access$000(Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;)Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return v4

    :cond_0
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iget-object v1, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog$3;->this$0:Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;

    # setter for: Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->isResponseKey:Z
    invoke-static {v1, v4}, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->access$002(Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;Z)Z

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog$3;->this$0:Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;

    # getter for: Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->handler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->access$300(Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;)Landroid/os/Handler;

    move-result-object v1

    const-wide/16 v2, 0x12c

    invoke-virtual {v1, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageAtTime(IJ)Z

    packed-switch p2, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    iget-object v1, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog$3;->this$0:Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;

    # getter for: Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->audioTrackListView:Landroid/widget/ListView;
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->access$400(Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;)Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ListView;->getSelectedItemPosition()I

    move-result v0

    packed-switch v0, :pswitch_data_2

    :goto_1
    :pswitch_2
    iget-object v1, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog$3;->this$0:Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;

    # getter for: Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->adapter:Lcom/jrm/localmm/business/adapter/AudioTrackListAdapter;
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->access$200(Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;)Lcom/jrm/localmm/business/adapter/AudioTrackListAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/jrm/localmm/business/adapter/AudioTrackListAdapter;->notifyDataSetChanged()V

    goto :goto_0

    :pswitch_3
    iget-object v1, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog$3;->this$0:Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;

    const/4 v2, -0x1

    # invokes: Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->changeAudioTrack(I)V
    invoke-static {v1, v2}, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->access$100(Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;I)V

    goto :goto_1

    :pswitch_4
    iget-object v1, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog$3;->this$0:Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;

    # getter for: Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->audioTrackListView:Landroid/widget/ListView;
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->access$400(Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;)Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ListView;->getSelectedItemPosition()I

    move-result v0

    packed-switch v0, :pswitch_data_3

    :goto_2
    :pswitch_5
    iget-object v1, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog$3;->this$0:Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;

    # getter for: Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->adapter:Lcom/jrm/localmm/business/adapter/AudioTrackListAdapter;
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->access$200(Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;)Lcom/jrm/localmm/business/adapter/AudioTrackListAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/jrm/localmm/business/adapter/AudioTrackListAdapter;->notifyDataSetChanged()V

    goto :goto_0

    :pswitch_6
    iget-object v1, p0, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog$3;->this$0:Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;

    const/4 v2, 0x1

    # invokes: Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->changeAudioTrack(I)V
    invoke-static {v1, v2}, Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;->access$100(Lcom/jrm/localmm/ui/video/PlaySettingAudioTrackDialog;I)V

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x15
        :pswitch_1
        :pswitch_4
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_6
        :pswitch_5
        :pswitch_5
        :pswitch_5
    .end packed-switch
.end method
