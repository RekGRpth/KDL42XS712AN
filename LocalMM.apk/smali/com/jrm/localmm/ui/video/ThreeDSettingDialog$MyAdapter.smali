.class public Lcom/jrm/localmm/ui/video/ThreeDSettingDialog$MyAdapter;
.super Landroid/widget/BaseAdapter;
.source "ThreeDSettingDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "MyAdapter"
.end annotation


# instance fields
.field private mInflater:Landroid/view/LayoutInflater;

.field final synthetic this$0:Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;


# direct methods
.method public constructor <init>(Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;Landroid/content/Context;)V
    .locals 1
    .param p2    # Landroid/content/Context;

    iput-object p1, p0, Lcom/jrm/localmm/ui/video/ThreeDSettingDialog$MyAdapter;->this$0:Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/ThreeDSettingDialog$MyAdapter;->mInflater:Landroid/view/LayoutInflater;

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/ThreeDSettingDialog$MyAdapter;->this$0:Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;

    # getter for: Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;->mItemName:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;->access$300(Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1    # I

    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/ThreeDSettingDialog$MyAdapter;->this$0:Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;

    new-instance v1, Lcom/jrm/localmm/ui/video/ThreeDSettingDialog$ViewHolder;

    invoke-direct {v1}, Lcom/jrm/localmm/ui/video/ThreeDSettingDialog$ViewHolder;-><init>()V

    iput-object v1, v0, Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;->holder:Lcom/jrm/localmm/ui/video/ThreeDSettingDialog$ViewHolder;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/ThreeDSettingDialog$MyAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f030024    # com.jrm.localmm.R.layout.video_three_d_item

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/ThreeDSettingDialog$MyAdapter;->this$0:Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;

    iget-object v1, v0, Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;->holder:Lcom/jrm/localmm/ui/video/ThreeDSettingDialog$ViewHolder;

    const v0, 0x7f0800d6    # com.jrm.localmm.R.id.ThreeDItemName

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/jrm/localmm/ui/video/ThreeDSettingDialog$ViewHolder;->fileNameText:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/ThreeDSettingDialog$MyAdapter;->this$0:Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;

    iget-object v1, v0, Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;->holder:Lcom/jrm/localmm/ui/video/ThreeDSettingDialog$ViewHolder;

    const v0, 0x7f0800d7    # com.jrm.localmm.R.id.threeDCheckBox

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, v1, Lcom/jrm/localmm/ui/video/ThreeDSettingDialog$ViewHolder;->checkBox:Landroid/widget/CheckBox;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/ThreeDSettingDialog$MyAdapter;->this$0:Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;

    iget-object v0, v0, Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;->holder:Lcom/jrm/localmm/ui/video/ThreeDSettingDialog$ViewHolder;

    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :goto_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/ThreeDSettingDialog$MyAdapter;->this$0:Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;

    iget-object v0, v0, Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;->holder:Lcom/jrm/localmm/ui/video/ThreeDSettingDialog$ViewHolder;

    iget-object v1, v0, Lcom/jrm/localmm/ui/video/ThreeDSettingDialog$ViewHolder;->fileNameText:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/ThreeDSettingDialog$MyAdapter;->this$0:Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;

    # getter for: Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;->mItemName:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;->access$300(Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/ThreeDSettingDialog$MyAdapter;->this$0:Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;

    # getter for: Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;->selectPosition:I
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;->access$000(Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;)I

    move-result v0

    if-ne v0, p1, :cond_1

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/ThreeDSettingDialog$MyAdapter;->this$0:Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;

    iget-object v0, v0, Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;->holder:Lcom/jrm/localmm/ui/video/ThreeDSettingDialog$ViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/video/ThreeDSettingDialog$ViewHolder;->checkBox:Landroid/widget/CheckBox;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    :goto_1
    return-object p2

    :cond_0
    iget-object v1, p0, Lcom/jrm/localmm/ui/video/ThreeDSettingDialog$MyAdapter;->this$0:Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;

    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jrm/localmm/ui/video/ThreeDSettingDialog$ViewHolder;

    iput-object v0, v1, Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;->holder:Lcom/jrm/localmm/ui/video/ThreeDSettingDialog$ViewHolder;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/ThreeDSettingDialog$MyAdapter;->this$0:Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;

    iget-object v0, v0, Lcom/jrm/localmm/ui/video/ThreeDSettingDialog;->holder:Lcom/jrm/localmm/ui/video/ThreeDSettingDialog$ViewHolder;

    iget-object v0, v0, Lcom/jrm/localmm/ui/video/ThreeDSettingDialog$ViewHolder;->checkBox:Landroid/widget/CheckBox;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_1
.end method
