.class public Lcom/jrm/localmm/ui/video/ThreeDimentionControl;
.super Ljava/lang/Object;
.source "ThreeDimentionControl.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/jrm/localmm/ui/video/ThreeDimentionControl$1;
    }
.end annotation


# static fields
.field private static mThreeDimentionControl:Lcom/jrm/localmm/ui/video/ThreeDimentionControl;


# instance fields
.field private detectNum:I

.field private isThreeDMode:Z

.field private mHandler:Landroid/os/Handler;

.field private mS3dSkin:Lcom/mstar/android/tv/TvS3DManager;

.field private mThreeD3DTo2D:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;

.field private mThreeD3DTo2DIndex:I

.field private mThreeDMode:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

.field private mThreeDimensionManager:Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

.field protected mVideoPlayView:Lcom/jrm/localmm/business/video/VideoPlayView;

.field private mVideoSource3DType:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

.field private mVideoSourceIsMvc:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v1, p0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->mVideoSourceIsMvc:Z

    iput-boolean v1, p0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->isThreeDMode:Z

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;->E_ThreeD_Video_DISPLAYFORMAT_AUTO:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->mThreeDMode:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    iput v1, p0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->mThreeD3DTo2DIndex:I

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_NONE:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->mVideoSource3DType:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    iput v1, p0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->detectNum:I

    return-void
.end method

.method public static getInstance()Lcom/jrm/localmm/ui/video/ThreeDimentionControl;
    .locals 1

    sget-object v0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->mThreeDimentionControl:Lcom/jrm/localmm/ui/video/ThreeDimentionControl;

    if-nez v0, :cond_0

    new-instance v0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;

    invoke-direct {v0}, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;-><init>()V

    sput-object v0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->mThreeDimentionControl:Lcom/jrm/localmm/ui/video/ThreeDimentionControl;

    :cond_0
    sget-object v0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->mThreeDimentionControl:Lcom/jrm/localmm/ui/video/ThreeDimentionControl;

    return-object v0
.end method


# virtual methods
.method public ThreeDInit()V
    .locals 5

    const-string v1, "ThreeDimentionControl"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ThreeDInit -> mThreeDMode ="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->mThreeDMode:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->isVideoSourceMvc()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->isThreeDAuto()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x0

    :try_start_0
    iput v1, p0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->detectNum:I

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->mThreeDimensionManager:Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;->E_MAIN_WINDOW:Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->detect3dFormat(Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;)Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    move-result-object v1

    iput-object v1, p0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->mVideoSource3DType:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    const-string v1, "ThreeDimentionControl"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ThreeDInit -> mVideoSource3DType ="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->mVideoSource3DType:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->mVideoSource3DType:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_NONE:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->mHandler:Landroid/os/Handler;

    const/16 v2, 0x10

    const-wide/16 v3, 0x3e8

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    const-string v1, "ThreeDimentionControl"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ThreeDInit -> mThreeDMode="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->mThreeDMode:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_1
    const/4 v1, 0x1

    const/16 v2, 0x5dc

    :try_start_1
    invoke-virtual {p0, v1, v2}, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->setVideoMute(ZI)V

    const-string v1, "ThreeDimentionControl"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "enable3d start:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    new-instance v3, Ljava/util/Date;

    invoke-direct {v3}, Ljava/util/Date;-><init>()V

    invoke-virtual {v3}, Ljava/util/Date;->getTime()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->mThreeDimensionManager:Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->mVideoSource3DType:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->enable3d(Lcom/mstar/android/tvapi/common/vo/Enum3dType;)Z

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->isThreeDMode:Z

    const-string v1, "ThreeDimentionControl"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "enable3d end:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    new-instance v3, Ljava/util/Date;

    invoke-direct {v3}, Ljava/util/Date;-><init>()V

    invoke-virtual {v3}, Ljava/util/Date;->getTime()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->setVideoMute(ZI)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->mThreeDMode:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;->E_ThreeD_Video_DISPLAYFORMAT_NONE:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    if-eq v1, v2, :cond_0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;->E_ThreeD_Video_DISPLAYFORMAT_NONE:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    invoke-virtual {p0, v1}, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->settingThreeDMode(Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;)V

    goto/16 :goto_0
.end method

.method public checkMvcSource()V
    .locals 2

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->mVideoPlayView:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-virtual {v0}, Lcom/jrm/localmm/business/video/VideoPlayView;->isMVCSource()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->mVideoSourceIsMvc:Z

    const-string v0, "ThreeDimentionControl"

    const-string v1, "******is MVC source"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;->E_ThreeD_Video_DISPLAYFORMAT_AUTO:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->mThreeDMode:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    :goto_0
    return-void

    :cond_0
    const-string v0, "ThreeDimentionControl"

    const-string v1, "******is not MVC source"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->mVideoSourceIsMvc:Z

    goto :goto_0
.end method

.method public getCurrent3DFormate()Lcom/mstar/android/tvapi/common/vo/Enum3dType;
    .locals 4

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_NONE:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    :try_start_0
    iget-object v2, p0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->mThreeDimensionManager:Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;->E_MAIN_WINDOW:Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->detect3dFormat(Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;)Lcom/mstar/android/tvapi/common/vo/Enum3dType;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public getThreeDMode()V
    .locals 1

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->mS3dSkin:Lcom/mstar/android/tv/TvS3DManager;

    invoke-virtual {v0}, Lcom/mstar/android/tv/TvS3DManager;->getDisplayFormat()Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    move-result-object v0

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->mThreeDMode:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    return-void
.end method

.method public getThreeDVideoDisplayFormat()Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;
    .locals 3

    const-string v0, "ThreeDimentionControl"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getThreeDVideoDisplayFormat -> mS3dSkin="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->mS3dSkin:Lcom/mstar/android/tv/TvS3DManager;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->mThreeDMode:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    return-object v0
.end method

.method public initThreeDimensionMode()V
    .locals 4

    const/4 v3, 0x0

    :try_start_0
    iget-object v1, p0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->mThreeDimensionManager:Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_NONE:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->enable3d(Lcom/mstar/android/tvapi/common/vo/Enum3dType;)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;->E_ThreeD_Video_DISPLAYFORMAT_NONE:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    iput-object v1, p0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->mThreeDMode:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->mS3dSkin:Lcom/mstar/android/tv/TvS3DManager;

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->mThreeDMode:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tv/TvS3DManager;->setDisplayFormat(Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;)Z

    iput-boolean v3, p0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->isThreeDMode:Z

    iput-boolean v3, p0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->mVideoSourceIsMvc:Z

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public isThreeDAuto()Z
    .locals 2

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->mThreeDMode:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;->E_ThreeD_Video_DISPLAYFORMAT_AUTO:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isThreeDMode()Z
    .locals 2

    iget-boolean v0, p0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->isThreeDMode:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->isVideoSourceMvc()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->mS3dSkin:Lcom/mstar/android/tv/TvS3DManager;

    invoke-virtual {v0}, Lcom/mstar/android/tv/TvS3DManager;->getDisplayFormat()Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    move-result-object v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;->E_ThreeD_Video_DISPLAYFORMAT_NONE:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    if-eq v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isVideoSourceMvc()Z
    .locals 1

    iget-boolean v0, p0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->mVideoSourceIsMvc:Z

    return v0
.end method

.method public refreshThreeDMode()V
    .locals 7

    const/4 v6, 0x0

    const/4 v5, 0x1

    const-string v2, "ThreeDimentionControl"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "refreshThreeDMode -> mThreeDMode ="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->mThreeDMode:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->mVideoPlayView:Lcom/jrm/localmm/business/video/VideoPlayView;

    invoke-virtual {v2}, Lcom/jrm/localmm/business/video/VideoPlayView;->isMVCSource()Z

    move-result v2

    if-eqz v2, :cond_1

    iput-boolean v5, p0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->mVideoSourceIsMvc:Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-boolean v6, p0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->mVideoSourceIsMvc:Z

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->isThreeDAuto()Z

    move-result v2

    if-eqz v2, :cond_5

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_NONE:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->getCurrent3DFormate()Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    move-result-object v1

    const-string v2, "ThreeDimentionControl"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "refreshThreeDMode -> videoSource3DTypetemp ="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_NONE:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    if-ne v1, v2, :cond_3

    :try_start_0
    iget-object v2, p0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->mThreeDimensionManager:Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;->E_MAIN_WINDOW:Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->detect3dFormat(Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;)Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    move-result-object v2

    iput-object v2, p0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->mVideoSource3DType:Lcom/mstar/android/tvapi/common/vo/Enum3dType;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    const-string v2, "ThreeDimentionControl"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "refreshThreeDMode -> mVideoSource3DType="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->mVideoSource3DType:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->mVideoSource3DType:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_NONE:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    if-eq v2, v3, :cond_2

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->mVideoSource3DType:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    if-nez v2, :cond_4

    :cond_2
    iget v2, p0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->detectNum:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->detectNum:I

    const/4 v3, 0x3

    if-ge v2, v3, :cond_0

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->mHandler:Landroid/os/Handler;

    const/16 v3, 0x10

    const-wide/16 v4, 0x3e8

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1

    :cond_3
    iput-object v1, p0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->mVideoSource3DType:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    goto :goto_1

    :cond_4
    const/16 v2, 0x5dc

    invoke-virtual {p0, v5, v2}, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->setVideoMute(ZI)V

    const-string v2, "ThreeDimentionControl"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "enable3d start:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    new-instance v4, Ljava/util/Date;

    invoke-direct {v4}, Ljava/util/Date;-><init>()V

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_1
    iget-object v2, p0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->mThreeDimensionManager:Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->mVideoSource3DType:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->enable3d(Lcom/mstar/android/tvapi/common/vo/Enum3dType;)Z

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->isThreeDMode:Z
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    const-string v2, "ThreeDimentionControl"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "enable3d end:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    new-instance v4, Ljava/util/Date;

    invoke-direct {v4}, Ljava/util/Date;-><init>()V

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, v6, v6}, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->setVideoMute(ZI)V

    goto/16 :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_2

    :cond_5
    iget-object v2, p0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->mThreeDMode:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;->E_ThreeD_Video_DISPLAYFORMAT_NONE:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    if-eq v2, v3, :cond_0

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->mThreeDMode:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->mS3dSkin:Lcom/mstar/android/tv/TvS3DManager;

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->mThreeDMode:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    invoke-virtual {v2, v3}, Lcom/mstar/android/tv/TvS3DManager;->setDisplayFormat(Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;)Z

    :cond_6
    iget v2, p0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->mThreeD3DTo2DIndex:I

    if-ne v2, v5, :cond_0

    iget v2, p0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->mThreeD3DTo2DIndex:I

    invoke-virtual {p0, v2}, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->setting3DTo2D(I)V

    goto/16 :goto_0
.end method

.method public setHandler(Landroid/os/Handler;)V
    .locals 0
    .param p1    # Landroid/os/Handler;

    iput-object p1, p0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method public setMediaPlayer(Lcom/jrm/localmm/business/video/VideoPlayView;)V
    .locals 0
    .param p1    # Lcom/jrm/localmm/business/video/VideoPlayView;

    iput-object p1, p0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->mVideoPlayView:Lcom/jrm/localmm/business/video/VideoPlayView;

    return-void
.end method

.method public setS3DSkin(Lcom/mstar/android/tv/TvS3DManager;)V
    .locals 3
    .param p1    # Lcom/mstar/android/tv/TvS3DManager;

    iput-object p1, p0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->mS3dSkin:Lcom/mstar/android/tv/TvS3DManager;

    const-string v0, "ThreeDimentionControl"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setS3DSkin -> mS3dSkin="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->mS3dSkin:Lcom/mstar/android/tv/TvS3DManager;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public setThreeDimensionManager(Lcom/mstar/android/tvapi/common/ThreeDimensionManager;)V
    .locals 0
    .param p1    # Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    iput-object p1, p0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->mThreeDimensionManager:Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    return-void
.end method

.method public setVideoMute(ZI)V
    .locals 4
    .param p1    # Z
    .param p2    # I

    const-string v1, "ThreeDimentionControl"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "*********setVideoMute********"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;->E_BLACK:Lcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_STORAGE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1, p1, v2, p2, v3}, Lcom/mstar/android/tvapi/common/TvManager;->setVideoMute(ZLcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;ILcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setting3DTo2D(I)V
    .locals 3
    .param p1    # I

    iput p1, p0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->mThreeD3DTo2DIndex:I

    const-string v0, "ThreeDimentionControl"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setting3DTo2D -> mThreeD3DTo2DIndex="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->mThreeD3DTo2DIndex:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->mThreeD3DTo2DIndex:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    const-string v0, "ThreeDimentionControl"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setting3DTo2D 0000000 -> mThreeDMode="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->mThreeDMode:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->mThreeDMode:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->settingThreeDMode(Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;)V

    goto :goto_0

    :pswitch_1
    const-string v0, "ThreeDimentionControl"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setting3DTo2D 1111111 -> mThreeDMode="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->mThreeDMode:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl$1;->$SwitchMap$com$mstar$android$tvapi$common$vo$EnumThreeDVideoDisplayFormat:[I

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->mThreeDMode:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    :goto_1
    const-string v0, "ThreeDimentionControl"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setting3DTo2D 1111111 -> mThreeD3DTo2D= "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->mThreeD3DTo2D:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->mS3dSkin:Lcom/mstar/android/tv/TvS3DManager;

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->mThreeD3DTo2D:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;

    invoke-virtual {v0, v1}, Lcom/mstar/android/tv/TvS3DManager;->set3DTo2D(Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;)Z

    goto :goto_0

    :pswitch_2
    sget-object v0, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;->E_ThreeD_Video_3DTO2D_AUTO:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->mThreeD3DTo2D:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;

    goto :goto_1

    :pswitch_3
    sget-object v0, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;->E_ThreeD_Video_3DTO2D_SIDE_BY_SIDE:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->mThreeD3DTo2D:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;

    goto :goto_1

    :pswitch_4
    sget-object v0, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;->E_ThreeD_Video_3DTO2D_TOP_BOTTOM:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->mThreeD3DTo2D:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;

    goto :goto_1

    :pswitch_5
    sget-object v0, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;->E_ThreeD_Video_3DTO2D_LINE_ALTERNATIVE:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->mThreeD3DTo2D:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;

    goto :goto_1

    :pswitch_6
    sget-object v0, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;->E_ThreeD_Video_3DTO2D_COUNT:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->mThreeD3DTo2D:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;

    goto :goto_1

    :pswitch_7
    sget-object v0, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;->E_ThreeD_Video_3DTO2D_FRAME_PACKING:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->mThreeD3DTo2D:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public settingThreeDMode(Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;)V
    .locals 4
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    const-string v1, "ThreeDimentionControl"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "settingThreeDMode->mThreeDMode="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iput-object p1, p0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->mThreeDMode:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->mThreeDMode:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;->E_ThreeD_Video_DISPLAYFORMAT_AUTO:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->mS3dSkin:Lcom/mstar/android/tv/TvS3DManager;

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->mThreeDMode:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tv/TvS3DManager;->setDisplayFormat(Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;)Z

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->isVideoSourceMvc()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->getCurrent3DFormate()Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    move-result-object v0

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->mVideoSource3DType:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    const-string v1, "ThreeDimentionControl"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "settingThreeDMode -> mThreeDMode="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->mThreeDMode:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "ThreeDimentionControl"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "settingThreeDMode -> mVideoSource3DType="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->mVideoSource3DType:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    const-string v1, "ThreeDimentionControl"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "settingThreeDMode -> mThreeDMode="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->mThreeDMode:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_1
    iget-object v1, p0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->mThreeDMode:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->mS3dSkin:Lcom/mstar/android/tv/TvS3DManager;

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/ThreeDimentionControl;->mThreeDMode:Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tv/TvS3DManager;->setDisplayFormat(Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;)Z

    goto :goto_0
.end method
