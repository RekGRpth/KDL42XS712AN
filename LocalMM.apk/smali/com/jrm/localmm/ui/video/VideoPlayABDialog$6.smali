.class Lcom/jrm/localmm/ui/video/VideoPlayABDialog$6;
.super Ljava/lang/Object;
.source "VideoPlayABDialog.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jrm/localmm/ui/video/VideoPlayABDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/localmm/ui/video/VideoPlayABDialog;


# direct methods
.method constructor <init>(Lcom/jrm/localmm/ui/video/VideoPlayABDialog;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog$6;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # Landroid/view/KeyEvent;

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    return v1

    :pswitch_0
    packed-switch p2, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog$6;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->checkBox:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->access$500(Lcom/jrm/localmm/ui/video/VideoPlayABDialog;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog$6;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->checkBox:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->access$500(Lcom/jrm/localmm/ui/video/VideoPlayABDialog;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog$6;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->checkBox:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->access$500(Lcom/jrm/localmm/ui/video/VideoPlayABDialog;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog$6;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->checkBox:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->access$500(Lcom/jrm/localmm/ui/video/VideoPlayABDialog;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog$6;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->checkBox:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->access$500(Lcom/jrm/localmm/ui/video/VideoPlayABDialog;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog$6;->this$0:Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->checkBox:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->access$500(Lcom/jrm/localmm/ui/video/VideoPlayABDialog;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x15
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
