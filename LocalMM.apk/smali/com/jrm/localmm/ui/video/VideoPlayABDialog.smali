.class public Lcom/jrm/localmm/ui/video/VideoPlayABDialog;
.super Landroid/app/Dialog;
.source "VideoPlayABDialog.java"


# instance fields
.field private aButton:Landroid/widget/Button;

.field public aFlag:Z

.field private bButton:Landroid/widget/Button;

.field public bFlag:Z

.field private checkBox:Landroid/widget/CheckBox;

.field private context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

.field public mHandler:Landroid/os/Handler;

.field private onkeyListenter:Landroid/view/View$OnKeyListener;

.field public postionA:I

.field public postionB:I

.field public sharedata:Landroid/content/SharedPreferences;

.field private total:F

.field private videoPlayHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;


# direct methods
.method public constructor <init>(Lcom/jrm/localmm/ui/video/VideoPlayerActivity;ILcom/jrm/localmm/ui/video/VideoPlayerViewHolder;)V
    .locals 1
    .param p1    # Lcom/jrm/localmm/ui/video/VideoPlayerActivity;
    .param p2    # I
    .param p3    # Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    iput-boolean v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->bFlag:Z

    iput-boolean v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->aFlag:Z

    new-instance v0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog$1;

    invoke-direct {v0, p0}, Lcom/jrm/localmm/ui/video/VideoPlayABDialog$1;-><init>(Lcom/jrm/localmm/ui/video/VideoPlayABDialog;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog$6;

    invoke-direct {v0, p0}, Lcom/jrm/localmm/ui/video/VideoPlayABDialog$6;-><init>(Lcom/jrm/localmm/ui/video/VideoPlayABDialog;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->onkeyListenter:Landroid/view/View$OnKeyListener;

    iput-object p1, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    iput-object p3, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->videoPlayHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    return-void
.end method

.method static synthetic access$000(Lcom/jrm/localmm/ui/video/VideoPlayABDialog;)Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->videoPlayHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    return-object v0
.end method

.method static synthetic access$100(Lcom/jrm/localmm/ui/video/VideoPlayABDialog;)Lcom/jrm/localmm/ui/video/VideoPlayerActivity;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    return-object v0
.end method

.method static synthetic access$200(Lcom/jrm/localmm/ui/video/VideoPlayABDialog;I)V
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/video/VideoPlayABDialog;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->setABVisible(I)V

    return-void
.end method

.method static synthetic access$300(Lcom/jrm/localmm/ui/video/VideoPlayABDialog;Ljava/lang/String;I)V
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/video/VideoPlayABDialog;
    .param p1    # Ljava/lang/String;
    .param p2    # I

    invoke-direct {p0, p1, p2}, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->setPosition(Ljava/lang/String;I)V

    return-void
.end method

.method static synthetic access$400(Lcom/jrm/localmm/ui/video/VideoPlayABDialog;)F
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    iget v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->total:F

    return v0
.end method

.method static synthetic access$500(Lcom/jrm/localmm/ui/video/VideoPlayABDialog;)Landroid/widget/CheckBox;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/video/VideoPlayABDialog;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->checkBox:Landroid/widget/CheckBox;

    return-object v0
.end method

.method private findViews()V
    .locals 1

    const v0, 0x7f0800a9    # com.jrm.localmm.R.id.play_on_or_off_checkbox

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->checkBox:Landroid/widget/CheckBox;

    const v0, 0x7f0800aa    # com.jrm.localmm.R.id.play_ab_a_btn

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->aButton:Landroid/widget/Button;

    const v0, 0x7f0800ab    # com.jrm.localmm.R.id.play_ab_b_btn

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->bButton:Landroid/widget/Button;

    return-void
.end method

.method private setABVisible(I)V
    .locals 5
    .param p1    # I

    const/4 v2, 0x4

    const/4 v4, 0x0

    const/4 v1, 0x1

    if-ne p1, v1, :cond_0

    const-string v1, "APOSITION"

    iget v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->postionA:I

    invoke-direct {p0, v1, v2}, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->setPosition(Ljava/lang/String;I)V

    iget v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->postionA:I

    int-to-float v1, v1

    iget v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->total:F

    div-float/2addr v1, v2

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->videoPlayHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    iget-object v2, v2, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->videoSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v2}, Landroid/widget/SeekBar;->getWidth()I

    move-result v2

    int-to-float v2, v2

    mul-float v0, v1, v2

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->videoPlayHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_playA:Landroid/widget/ImageView;

    float-to-int v2, v0

    const/16 v3, 0xf

    invoke-virtual {v1, v2, v4, v4, v3}, Landroid/widget/ImageView;->setPadding(IIII)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->videoPlayHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_playA:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    const-string v1, "bPOSITION"

    iget v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->postionB:I

    invoke-direct {p0, v1, v2}, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->setPosition(Ljava/lang/String;I)V

    iget v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->postionB:I

    int-to-float v1, v1

    iget v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->total:F

    div-float/2addr v1, v2

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->videoPlayHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    iget-object v2, v2, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->videoSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v2}, Landroid/widget/SeekBar;->getWidth()I

    move-result v2

    int-to-float v2, v2

    mul-float v0, v1, v2

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->videoPlayHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_playB:Landroid/widget/ImageView;

    float-to-int v2, v0

    const/16 v3, 0xc

    invoke-virtual {v1, v2, v4, v4, v3}, Landroid/widget/ImageView;->setPadding(IIII)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->videoPlayHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_playB:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    sput-boolean v4, Lcom/jrm/localmm/util/Constants;->aFlag:Z

    iput-boolean v4, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->aFlag:Z

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->videoPlayHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_playA:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->videoPlayHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    iget-object v1, v1, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->bt_playB:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method private setListeners()V
    .locals 2

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->videoPlayHolder:Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerViewHolder;->getPlayerView()Lcom/jrm/localmm/business/video/VideoPlayView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/jrm/localmm/business/video/VideoPlayView;->getDuration()I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->total:F

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->aButton:Landroid/widget/Button;

    new-instance v1, Lcom/jrm/localmm/ui/video/VideoPlayABDialog$2;

    invoke-direct {v1, p0}, Lcom/jrm/localmm/ui/video/VideoPlayABDialog$2;-><init>(Lcom/jrm/localmm/ui/video/VideoPlayABDialog;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->bButton:Landroid/widget/Button;

    new-instance v1, Lcom/jrm/localmm/ui/video/VideoPlayABDialog$3;

    invoke-direct {v1, p0}, Lcom/jrm/localmm/ui/video/VideoPlayABDialog$3;-><init>(Lcom/jrm/localmm/ui/video/VideoPlayABDialog;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->checkBox:Landroid/widget/CheckBox;

    new-instance v1, Lcom/jrm/localmm/ui/video/VideoPlayABDialog$4;

    invoke-direct {v1, p0}, Lcom/jrm/localmm/ui/video/VideoPlayABDialog$4;-><init>(Lcom/jrm/localmm/ui/video/VideoPlayABDialog;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->checkBox:Landroid/widget/CheckBox;

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->onkeyListenter:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->checkBox:Landroid/widget/CheckBox;

    new-instance v1, Lcom/jrm/localmm/ui/video/VideoPlayABDialog$5;

    invoke-direct {v1, p0}, Lcom/jrm/localmm/ui/video/VideoPlayABDialog$5;-><init>(Lcom/jrm/localmm/ui/video/VideoPlayABDialog;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    return-void
.end method

.method private setPosition(Ljava/lang/String;I)V
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # I

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    const-string v3, "LOCALMM_POSITION"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method


# virtual methods
.method public onBackPressed()V
    .locals 1

    invoke-super {p0}, Landroid/app/Dialog;->onBackPressed()V

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->dismiss()V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-virtual {v0}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->hideControlDelay()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 10
    .param p1    # Landroid/os/Bundle;

    const/4 v9, 0x1

    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    const v5, 0x7f03001d    # com.jrm.localmm.R.layout.video_play_ab

    invoke-virtual {p0, v5}, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->setContentView(I)V

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v5

    invoke-interface {v5}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    const/4 v5, 0x0

    invoke-virtual {v2, v5}, Landroid/view/Window;->setTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v5

    int-to-double v5, v5

    const-wide v7, 0x3fcd70a3d70a3d71L    # 0.23

    mul-double/2addr v5, v7

    double-to-int v3, v5

    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v5

    int-to-double v5, v5

    const-wide v7, 0x3fd6666666666666L    # 0.35

    mul-double/2addr v5, v7

    double-to-int v1, v5

    invoke-virtual {v2, v3, v1}, Landroid/view/Window;->setLayout(II)V

    const/16 v5, 0x11

    invoke-virtual {v2, v5}, Landroid/view/Window;->setGravity(I)V

    invoke-virtual {v2}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    iget-object v5, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    const-string v6, "LOCALMM_POSITION"

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Lcom/jrm/localmm/ui/video/VideoPlayerActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v5

    iput-object v5, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->sharedata:Landroid/content/SharedPreferences;

    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->findViews()V

    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->setListeners()V

    sget-boolean v5, Lcom/jrm/localmm/util/Constants;->abFlag:Z

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->checkBox:Landroid/widget/CheckBox;

    invoke-virtual {v5, v9}, Landroid/widget/CheckBox;->setFocusable(Z)V

    iget-object v5, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->checkBox:Landroid/widget/CheckBox;

    invoke-virtual {v5, v9}, Landroid/widget/CheckBox;->setChecked(Z)V

    :cond_0
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/16 v0, 0x7e

    if-eq v0, p1, :cond_0

    const/16 v0, 0x7f

    if-eq v0, p1, :cond_0

    const/16 v0, 0x57

    if-eq v0, p1, :cond_0

    const/16 v0, 0x58

    if-ne v0, p1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    invoke-super {p0, p1, p2}, Landroid/app/Dialog;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/16 v0, 0x7e

    if-eq v0, p1, :cond_0

    const/16 v0, 0x7f

    if-eq v0, p1, :cond_0

    const/16 v0, 0x57

    if-eq v0, p1, :cond_0

    const/16 v0, 0x58

    if-ne v0, p1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    invoke-super {p0, p1, p2}, Landroid/app/Dialog;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public setToastMessage(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlayABDialog;->context:Lcom/jrm/localmm/ui/video/VideoPlayerActivity;

    invoke-static {v1, p1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    const/16 v1, 0x11

    invoke-virtual {v0, v1, v2, v2}, Landroid/widget/Toast;->setGravity(III)V

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method
