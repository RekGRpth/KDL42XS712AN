.class Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog$3;
.super Ljava/lang/Object;
.source "VideoPlaySettingDialog.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;


# direct methods
.method constructor <init>(Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog$3;->this$0:Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # Landroid/view/KeyEvent;

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :goto_0
    const/4 v1, 0x0

    return v1

    :pswitch_0
    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog$3;->this$0:Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->playSettingListView:Landroid/widget/ListView;
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->access$600(Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;)Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ListView;->getSelectedItemPosition()I

    move-result v0

    packed-switch p2, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    const-string v1, "***********"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "*********position********"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    packed-switch v0, :pswitch_data_2

    :goto_1
    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog$3;->this$0:Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->adapter:Lcom/jrm/localmm/business/adapter/VideoPlaySettingListAdapter;
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->access$400(Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;)Lcom/jrm/localmm/business/adapter/VideoPlaySettingListAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/jrm/localmm/business/adapter/VideoPlaySettingListAdapter;->notifyDataSetChanged()V

    goto :goto_0

    :pswitch_2
    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog$3;->this$0:Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;

    # invokes: Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->change3DState()V
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->access$700(Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;)V

    goto :goto_1

    :pswitch_3
    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog$3;->this$0:Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;

    # invokes: Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->changeSubtitleStateLeft()V
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->access$800(Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;)V

    goto :goto_1

    :pswitch_4
    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog$3;->this$0:Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;

    # invokes: Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->changeBreakPointFlag()V
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->access$300(Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;)V

    goto :goto_1

    :pswitch_5
    const-string v1, "***********"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "*********position********"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    packed-switch v0, :pswitch_data_3

    :goto_2
    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog$3;->this$0:Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;

    # getter for: Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->adapter:Lcom/jrm/localmm/business/adapter/VideoPlaySettingListAdapter;
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->access$400(Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;)Lcom/jrm/localmm/business/adapter/VideoPlaySettingListAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/jrm/localmm/business/adapter/VideoPlaySettingListAdapter;->notifyDataSetChanged()V

    goto :goto_0

    :pswitch_6
    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog$3;->this$0:Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;

    # invokes: Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->change3DState()V
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->access$700(Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;)V

    goto :goto_2

    :pswitch_7
    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog$3;->this$0:Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;

    # invokes: Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->changeSubtitleStateRight()V
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->access$900(Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;)V

    goto :goto_2

    :pswitch_8
    iget-object v1, p0, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog$3;->this$0:Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;

    # invokes: Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->changeBreakPointFlag()V
    invoke-static {v1}, Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;->access$300(Lcom/jrm/localmm/ui/video/VideoPlaySettingDialog;)V

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x15
        :pswitch_1
        :pswitch_5
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method
