.class public Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;
.super Landroid/app/Dialog;
.source "ChooseTimePlayDialog.java"


# instance fields
.field private chooseTimeTouchListener:Landroid/view/View$OnTouchListener;

.field private chooseTimelistener:Landroid/view/View$OnKeyListener;

.field private context:Landroid/content/Context;

.field private mHandler:Landroid/os/Handler;

.field private mbNotSeek:Z

.field private postion:I

.field private size:I

.field private videoTimeChooseList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/widget/EditText;",
            ">;"
        }
    .end annotation
.end field

.field private videoTimeCurrentPositionTextView:Landroid/widget/TextView;

.field private videoTimeDurationTextView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # I

    invoke-direct {p0, p1, p2}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->mbNotSeek:Z

    new-instance v0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog$1;

    invoke-direct {v0, p0}, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog$1;-><init>(Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->chooseTimeTouchListener:Landroid/view/View$OnTouchListener;

    new-instance v0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog$2;

    invoke-direct {v0, p0}, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog$2;-><init>(Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;)V

    iput-object v0, p0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->chooseTimelistener:Landroid/view/View$OnKeyListener;

    iput-object p1, p0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->context:Landroid/content/Context;

    return-void
.end method

.method static synthetic access$000(Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;)I
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;

    iget v0, p0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->postion:I

    return v0
.end method

.method static synthetic access$002(Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;I)I
    .locals 0
    .param p0    # Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;
    .param p1    # I

    iput p1, p0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->postion:I

    return p1
.end method

.method static synthetic access$008(Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;)I
    .locals 2
    .param p0    # Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;

    iget v0, p0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->postion:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->postion:I

    return v0
.end method

.method static synthetic access$010(Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;)I
    .locals 2
    .param p0    # Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;

    iget v0, p0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->postion:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->postion:I

    return v0
.end method

.method static synthetic access$100(Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->videoTimeChooseList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$200(Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;)Z
    .locals 1
    .param p0    # Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;

    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->chooseTime()Z

    move-result v0

    return v0
.end method

.method private chooseTime()Z
    .locals 27

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "0"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->videoTimeChooseList:Ljava/util/List;

    move-object/from16 v24, v0

    const/16 v26, 0x0

    move-object/from16 v0, v24

    move/from16 v1, v26

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Landroid/widget/EditText;

    invoke-virtual/range {v24 .. v24}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v25

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "0"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->videoTimeChooseList:Ljava/util/List;

    move-object/from16 v24, v0

    const/16 v26, 0x1

    move-object/from16 v0, v24

    move/from16 v1, v26

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Landroid/widget/EditText;

    invoke-virtual/range {v24 .. v24}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v25

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "0"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->videoTimeChooseList:Ljava/util/List;

    move-object/from16 v24, v0

    const/16 v26, 0x2

    move-object/from16 v0, v24

    move/from16 v1, v26

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Landroid/widget/EditText;

    invoke-virtual/range {v24 .. v24}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v25

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "0"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->videoTimeChooseList:Ljava/util/List;

    move-object/from16 v24, v0

    const/16 v26, 0x3

    move-object/from16 v0, v24

    move/from16 v1, v26

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Landroid/widget/EditText;

    invoke-virtual/range {v24 .. v24}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v25

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "0"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->videoTimeChooseList:Ljava/util/List;

    move-object/from16 v24, v0

    const/16 v26, 0x4

    move-object/from16 v0, v24

    move/from16 v1, v26

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Landroid/widget/EditText;

    invoke-virtual/range {v24 .. v24}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v25

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "0"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->videoTimeChooseList:Ljava/util/List;

    move-object/from16 v24, v0

    const/16 v26, 0x5

    move-object/from16 v0, v24

    move/from16 v1, v26

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Landroid/widget/EditText;

    invoke-virtual/range {v24 .. v24}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v25

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static {v14}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    invoke-static {v13}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    invoke-static {v15}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v10

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    mul-int/lit8 v24, v4, 0xa

    add-int v2, v24, v3

    mul-int/lit8 v24, v7, 0xa

    add-int v5, v24, v6

    mul-int/lit8 v24, v10, 0xa

    add-int v8, v24, v9

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->videoTimeDurationTextView:Landroid/widget/TextView;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->mbNotSeek:Z

    move/from16 v24, v0

    if-nez v24, :cond_1

    const-string v24, ":"

    move-object/from16 v0, v19

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v24

    const/16 v25, 0x0

    aget-object v24, v24, v25

    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v20

    const-string v24, ":"

    move-object/from16 v0, v19

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v24

    const/16 v25, 0x1

    aget-object v24, v24, v25

    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v21

    const-string v24, ":"

    move-object/from16 v0, v19

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v24

    const/16 v25, 0x2

    aget-object v24, v24, v25

    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v22

    move/from16 v0, v20

    mul-int/lit16 v0, v0, 0xe10

    move/from16 v24, v0

    mul-int/lit8 v25, v21, 0x3c

    add-int v24, v24, v25

    add-int v23, v24, v22

    mul-int/lit16 v0, v3, 0xe10

    move/from16 v24, v0

    mul-int/lit8 v25, v5, 0x3c

    add-int v24, v24, v25

    add-int v11, v24, v8

    move/from16 v0, v20

    if-gt v2, v0, :cond_0

    const/16 v24, 0x3c

    move/from16 v0, v24

    if-ge v5, v0, :cond_0

    const/16 v24, 0x3c

    move/from16 v0, v24

    if-ge v8, v0, :cond_0

    move/from16 v0, v23

    if-ge v11, v0, :cond_0

    new-instance v12, Landroid/os/Message;

    invoke-direct {v12}, Landroid/os/Message;-><init>()V

    const/16 v24, 0x12

    move/from16 v0, v24

    iput v0, v12, Landroid/os/Message;->what:I

    mul-int/lit16 v0, v11, 0x3e8

    move/from16 v24, v0

    move/from16 v0, v24

    iput v0, v12, Landroid/os/Message;->arg1:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->mHandler:Landroid/os/Handler;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v0, v12}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    const/16 v24, 0x1

    :goto_0
    return v24

    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->context:Landroid/content/Context;

    move-object/from16 v24, v0

    const v25, 0x7f0600c2    # com.jrm.localmm.R.string.choose_time_invalid

    invoke-virtual/range {v24 .. v25}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-direct {v0, v1}, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->showToast(Ljava/lang/String;)V

    const/16 v24, 0x1

    goto :goto_0

    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->context:Landroid/content/Context;

    move-object/from16 v24, v0

    const v25, 0x7f0600c1    # com.jrm.localmm.R.string.choose_time_failed

    invoke-virtual/range {v24 .. v25}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-direct {v0, v1}, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->showToast(Ljava/lang/String;)V

    const/16 v24, 0x1

    goto :goto_0
.end method

.method private initView()V
    .locals 5

    const v3, 0x7f080097    # com.jrm.localmm.R.id.videoCurrentTimeTextView

    invoke-virtual {p0, v3}, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->videoTimeCurrentPositionTextView:Landroid/widget/TextView;

    const v3, 0x7f080098    # com.jrm.localmm.R.id.videoTotalDurationTextView

    invoke-virtual {p0, v3}, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->videoTimeDurationTextView:Landroid/widget/TextView;

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->videoTimeChooseList:Ljava/util/List;

    iget-object v4, p0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->videoTimeChooseList:Ljava/util/List;

    const v3, 0x7f080099    # com.jrm.localmm.R.id.videoTimeChooseNumOne

    invoke-virtual {p0, v3}, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/EditText;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v4, p0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->videoTimeChooseList:Ljava/util/List;

    const v3, 0x7f08009b    # com.jrm.localmm.R.id.videoTimeChooseNumTwo

    invoke-virtual {p0, v3}, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/EditText;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v4, p0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->videoTimeChooseList:Ljava/util/List;

    const v3, 0x7f08009c    # com.jrm.localmm.R.id.videoTimeChooseNumThree

    invoke-virtual {p0, v3}, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/EditText;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v4, p0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->videoTimeChooseList:Ljava/util/List;

    const v3, 0x7f08009d    # com.jrm.localmm.R.id.videoTimeChooseNumFour

    invoke-virtual {p0, v3}, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/EditText;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v4, p0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->videoTimeChooseList:Ljava/util/List;

    const v3, 0x7f08009e    # com.jrm.localmm.R.id.videoTimeChooseNumFive

    invoke-virtual {p0, v3}, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/EditText;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v4, p0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->videoTimeChooseList:Ljava/util/List;

    const v3, 0x7f08009a    # com.jrm.localmm.R.id.videoTimeChooseNumSix

    invoke-virtual {p0, v3}, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/EditText;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->videoTimeChooseList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    iput v3, p0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->size:I

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->context:Landroid/content/Context;

    const-string v4, "input_method"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/inputmethod/InputMethodManager;

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->videoTimeChooseList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    iget-object v3, p0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->videoTimeChooseList:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private setListeners()V
    .locals 1

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->chooseTimelistener:Landroid/view/View$OnKeyListener;

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->setTimeChooseKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->chooseTimeTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->setTimeChooseTouchListener(Landroid/view/View$OnTouchListener;)V

    return-void
.end method

.method private setWindowsAttribute()V
    .locals 3

    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/Window;->setTitle(Ljava/lang/CharSequence;)V

    const/16 v2, 0x50

    invoke-virtual {v0, v2}, Landroid/view/Window;->setGravity(I)V

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    return-void
.end method

.method private showToast(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->context:Landroid/content/Context;

    const/4 v2, 0x1

    invoke-static {v1, p1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    const/16 v1, 0x11

    invoke-virtual {v0}, Landroid/widget/Toast;->getXOffset()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    invoke-virtual {v0}, Landroid/widget/Toast;->getYOffset()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/Toast;->setGravity(III)V

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method


# virtual methods
.method public clearChooseList()V
    .locals 4

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->videoTimeChooseList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->videoTimeChooseList:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->videoTimeChooseList:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    const-string v3, "0"

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public getVideoTimeCurrentPositionTextView()Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->videoTimeCurrentPositionTextView:Landroid/widget/TextView;

    return-object v0
.end method

.method public getVideoTimeDurationTextView()Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->videoTimeDurationTextView:Landroid/widget/TextView;

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f03001a    # com.jrm.localmm.R.layout.video_choose_time_play

    invoke-virtual {p0, v0}, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->setContentView(I)V

    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->setWindowsAttribute()V

    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->initView()V

    invoke-direct {p0}, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->setListeners()V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v0, 0x1

    return v0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const-string v0, ""

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onKeyUp, keyCode : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v0, 0x42

    if-eq p1, v0, :cond_0

    const/4 v0, 0x4

    if-ne p1, v0, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->cancel()V

    invoke-super {p0, p1, p2}, Landroid/app/Dialog;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setTimeChooseKeyListener(Landroid/view/View$OnKeyListener;)V
    .locals 2
    .param p1    # Landroid/view/View$OnKeyListener;

    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->size:I

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->videoTimeChooseList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    invoke-virtual {v1, p1}, Landroid/widget/EditText;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public setTimeChooseTouchListener(Landroid/view/View$OnTouchListener;)V
    .locals 2
    .param p1    # Landroid/view/View$OnTouchListener;

    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->size:I

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->videoTimeChooseList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    invoke-virtual {v1, p1}, Landroid/widget/EditText;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public setVariable(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->mbNotSeek:Z

    return-void
.end method

.method public setVariable(ZLandroid/os/Handler;)V
    .locals 0
    .param p1    # Z
    .param p2    # Landroid/os/Handler;

    iput-boolean p1, p0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->mbNotSeek:Z

    iput-object p2, p0, Lcom/jrm/localmm/ui/video/ChooseTimePlayDialog;->mHandler:Landroid/os/Handler;

    return-void
.end method
