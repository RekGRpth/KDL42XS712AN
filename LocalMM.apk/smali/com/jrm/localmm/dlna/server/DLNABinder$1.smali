.class Lcom/jrm/localmm/dlna/server/DLNABinder$1;
.super Ljava/lang/Object;
.source "DLNABinder.java"

# interfaces
.implements Landroid/net/dlna/DeviceListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/jrm/localmm/dlna/server/DLNABinder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/jrm/localmm/dlna/server/DLNABinder;


# direct methods
.method constructor <init>(Lcom/jrm/localmm/dlna/server/DLNABinder;)V
    .locals 0

    iput-object p1, p0, Lcom/jrm/localmm/dlna/server/DLNABinder$1;->this$0:Lcom/jrm/localmm/dlna/server/DLNABinder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public OnDisappeared(Landroid/net/dlna/MediaRendererController;)V
    .locals 0
    .param p1    # Landroid/net/dlna/MediaRendererController;

    return-void
.end method

.method public OnDisappeared(Landroid/net/dlna/MediaServerController;)V
    .locals 4

    const/4 v0, 0x0

    :try_start_0
    invoke-interface {p1}, Landroid/net/dlna/MediaServerController;->GetDeviceInfo()Landroid/net/dlna/DeviceInfo;
    :try_end_0
    .catch Landroid/net/dlna/ActionUnsupportedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/net/dlna/HostUnreachableException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_0

    const-class v1, Lcom/jrm/localmm/dlna/server/DLNABinder;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Del_MediaServerController:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/net/dlna/DeviceInfo;->getUDN()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/jrm/localmm/dlna/server/DLNAConstants;->printE(Ljava/lang/Class;Ljava/lang/String;)V

    new-instance v1, Lcom/jrm/localmm/business/data/MediaServerDisplay;

    const/4 v2, 0x0

    invoke-direct {v1, p1, v2}, Lcom/jrm/localmm/business/data/MediaServerDisplay;-><init>(Landroid/net/dlna/MediaServerController;Z)V

    iget-object v2, p0, Lcom/jrm/localmm/dlna/server/DLNABinder$1;->this$0:Lcom/jrm/localmm/dlna/server/DLNABinder;

    # getter for: Lcom/jrm/localmm/dlna/server/DLNABinder;->mediaServerControllers:Ljava/util/List;
    invoke-static {v2}, Lcom/jrm/localmm/dlna/server/DLNABinder;->access$100(Lcom/jrm/localmm/dlna/server/DLNABinder;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/jrm/localmm/dlna/server/DLNABinder$1;->this$0:Lcom/jrm/localmm/dlna/server/DLNABinder;

    # getter for: Lcom/jrm/localmm/dlna/server/DLNABinder;->mediaServerMap:Ljava/util/Map;
    invoke-static {v1}, Lcom/jrm/localmm/dlna/server/DLNABinder;->access$000(Lcom/jrm/localmm/dlna/server/DLNABinder;)Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0}, Landroid/net/dlna/DeviceInfo;->getUDN()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/jrm/localmm/dlna/server/DLNABinder$1;->this$0:Lcom/jrm/localmm/dlna/server/DLNABinder;

    # getter for: Lcom/jrm/localmm/dlna/server/DLNABinder;->deviceChangeListeners:Ljava/util/List;
    invoke-static {v0}, Lcom/jrm/localmm/dlna/server/DLNABinder;->access$200(Lcom/jrm/localmm/dlna/server/DLNABinder;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jrm/localmm/ui/main/DeviceChangeListener;

    invoke-interface {v0}, Lcom/jrm/localmm/ui/main/DeviceChangeListener;->mediaServerDisappeared()V

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/net/dlna/ActionUnsupportedException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v1

    invoke-virtual {v1}, Landroid/net/dlna/HostUnreachableException;->printStackTrace()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public OnDiscovered(Landroid/net/dlna/MediaRendererController;)V
    .locals 0
    .param p1    # Landroid/net/dlna/MediaRendererController;

    return-void
.end method

.method public OnDiscovered(Landroid/net/dlna/MediaServerController;)V
    .locals 4

    const/4 v0, 0x0

    :try_start_0
    invoke-interface {p1}, Landroid/net/dlna/MediaServerController;->GetDeviceInfo()Landroid/net/dlna/DeviceInfo;
    :try_end_0
    .catch Landroid/net/dlna/ActionUnsupportedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/net/dlna/HostUnreachableException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_1

    const-class v1, Lcom/jrm/localmm/dlna/server/DLNABinder;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Add_MediaServerController:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/net/dlna/DeviceInfo;->getUDN()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/jrm/localmm/dlna/server/DLNAConstants;->printE(Ljava/lang/Class;Ljava/lang/String;)V

    new-instance v1, Lcom/jrm/localmm/business/data/MediaServerDisplay;

    const/4 v2, 0x1

    invoke-direct {v1, p1, v2}, Lcom/jrm/localmm/business/data/MediaServerDisplay;-><init>(Landroid/net/dlna/MediaServerController;Z)V

    iget-object v2, p0, Lcom/jrm/localmm/dlna/server/DLNABinder$1;->this$0:Lcom/jrm/localmm/dlna/server/DLNABinder;

    # getter for: Lcom/jrm/localmm/dlna/server/DLNABinder;->mediaServerMap:Ljava/util/Map;
    invoke-static {v2}, Lcom/jrm/localmm/dlna/server/DLNABinder;->access$000(Lcom/jrm/localmm/dlna/server/DLNABinder;)Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v0}, Landroid/net/dlna/DeviceInfo;->getUDN()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/jrm/localmm/dlna/server/DLNABinder$1;->this$0:Lcom/jrm/localmm/dlna/server/DLNABinder;

    # getter for: Lcom/jrm/localmm/dlna/server/DLNABinder;->mediaServerControllers:Ljava/util/List;
    invoke-static {v2}, Lcom/jrm/localmm/dlna/server/DLNABinder;->access$100(Lcom/jrm/localmm/dlna/server/DLNABinder;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/jrm/localmm/dlna/server/DLNABinder$1;->this$0:Lcom/jrm/localmm/dlna/server/DLNABinder;

    # getter for: Lcom/jrm/localmm/dlna/server/DLNABinder;->mediaServerMap:Ljava/util/Map;
    invoke-static {v2}, Lcom/jrm/localmm/dlna/server/DLNABinder;->access$000(Lcom/jrm/localmm/dlna/server/DLNABinder;)Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v0}, Landroid/net/dlna/DeviceInfo;->getUDN()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    iget-object v0, p0, Lcom/jrm/localmm/dlna/server/DLNABinder$1;->this$0:Lcom/jrm/localmm/dlna/server/DLNABinder;

    # getter for: Lcom/jrm/localmm/dlna/server/DLNABinder;->deviceChangeListeners:Ljava/util/List;
    invoke-static {v0}, Lcom/jrm/localmm/dlna/server/DLNABinder;->access$200(Lcom/jrm/localmm/dlna/server/DLNABinder;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jrm/localmm/ui/main/DeviceChangeListener;

    invoke-interface {v0}, Lcom/jrm/localmm/ui/main/DeviceChangeListener;->mediaServerDiscovered()V

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/net/dlna/ActionUnsupportedException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v1

    invoke-virtual {v1}, Landroid/net/dlna/HostUnreachableException;->printStackTrace()V

    goto :goto_0

    :cond_1
    return-void
.end method
