.class public Lcom/jrm/localmm/dlna/server/DLNAServer;
.super Landroid/app/Service;
.source "DLNAServer.java"


# instance fields
.field private dlnaBinder:Lcom/jrm/localmm/dlna/server/DLNABinder;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/jrm/localmm/dlna/server/DLNAServer;->dlnaBinder:Lcom/jrm/localmm/dlna/server/DLNABinder;

    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2
    .param p1    # Landroid/content/Intent;

    const-string v0, "System.out"

    const-string v1, "DLNAServer.onBind()."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/jrm/localmm/dlna/server/DLNABinder;

    invoke-direct {v0, p0}, Lcom/jrm/localmm/dlna/server/DLNABinder;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/jrm/localmm/dlna/server/DLNAServer;->dlnaBinder:Lcom/jrm/localmm/dlna/server/DLNABinder;

    iget-object v0, p0, Lcom/jrm/localmm/dlna/server/DLNAServer;->dlnaBinder:Lcom/jrm/localmm/dlna/server/DLNABinder;

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    const-string v0, "System.out"

    const-string v1, "dlnaServer onCreate()..."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onDestroy()V
    .locals 2

    const-string v0, "System.out"

    const-string v1, "dlnaServer onDestroy()..."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/jrm/localmm/dlna/server/DLNAServer;->dlnaBinder:Lcom/jrm/localmm/dlna/server/DLNABinder;

    invoke-virtual {v0}, Lcom/jrm/localmm/dlna/server/DLNABinder;->finalizeDlna()V

    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 2
    .param p1    # Landroid/content/Intent;
    .param p2    # I
    .param p3    # I

    const-string v0, "System.out"

    const-string v1, "dlnaServer onStartCommand()..."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x2

    return v0
.end method
