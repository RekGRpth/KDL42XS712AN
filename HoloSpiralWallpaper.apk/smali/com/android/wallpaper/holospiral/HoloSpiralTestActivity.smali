.class public Lcom/android/wallpaper/holospiral/HoloSpiralTestActivity;
.super Landroid/app/Activity;
.source "HoloSpiralTestActivity.java"


# instance fields
.field private mView:Lcom/android/wallpaper/holospiral/HoloSpiralView;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    new-instance v0, Lcom/android/wallpaper/holospiral/HoloSpiralView;

    invoke-direct {v0, p0}, Lcom/android/wallpaper/holospiral/HoloSpiralView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/wallpaper/holospiral/HoloSpiralTestActivity;->mView:Lcom/android/wallpaper/holospiral/HoloSpiralView;

    iget-object v0, p0, Lcom/android/wallpaper/holospiral/HoloSpiralTestActivity;->mView:Lcom/android/wallpaper/holospiral/HoloSpiralView;

    invoke-virtual {p0, v0}, Lcom/android/wallpaper/holospiral/HoloSpiralTestActivity;->setContentView(Landroid/view/View;)V

    return-void
.end method

.method public onPause()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    iget-object v0, p0, Lcom/android/wallpaper/holospiral/HoloSpiralTestActivity;->mView:Lcom/android/wallpaper/holospiral/HoloSpiralView;

    invoke-virtual {v0}, Lcom/android/wallpaper/holospiral/HoloSpiralView;->onPause()V

    return-void
.end method

.method public onResume()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    iget-object v0, p0, Lcom/android/wallpaper/holospiral/HoloSpiralTestActivity;->mView:Lcom/android/wallpaper/holospiral/HoloSpiralView;

    invoke-virtual {v0}, Lcom/android/wallpaper/holospiral/HoloSpiralView;->onResume()V

    return-void
.end method
