.class Lcom/android/wallpaper/holospiral/HoloSpiralWallpaper$RenderScriptEngine;
.super Landroid/service/wallpaper/WallpaperService$Engine;
.source "HoloSpiralWallpaper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/wallpaper/holospiral/HoloSpiralWallpaper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RenderScriptEngine"
.end annotation


# instance fields
.field private mRenderScript:Landroid/renderscript/RenderScriptGL;

.field private mWallpaperRS:Lcom/android/wallpaper/holospiral/HoloSpiralRS;

.field final synthetic this$0:Lcom/android/wallpaper/holospiral/HoloSpiralWallpaper;


# direct methods
.method private constructor <init>(Lcom/android/wallpaper/holospiral/HoloSpiralWallpaper;)V
    .locals 1

    const/4 v0, 0x0

    iput-object p1, p0, Lcom/android/wallpaper/holospiral/HoloSpiralWallpaper$RenderScriptEngine;->this$0:Lcom/android/wallpaper/holospiral/HoloSpiralWallpaper;

    invoke-direct {p0, p1}, Landroid/service/wallpaper/WallpaperService$Engine;-><init>(Landroid/service/wallpaper/WallpaperService;)V

    iput-object v0, p0, Lcom/android/wallpaper/holospiral/HoloSpiralWallpaper$RenderScriptEngine;->mRenderScript:Landroid/renderscript/RenderScriptGL;

    iput-object v0, p0, Lcom/android/wallpaper/holospiral/HoloSpiralWallpaper$RenderScriptEngine;->mWallpaperRS:Lcom/android/wallpaper/holospiral/HoloSpiralRS;

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/wallpaper/holospiral/HoloSpiralWallpaper;Lcom/android/wallpaper/holospiral/HoloSpiralWallpaper$1;)V
    .locals 0
    .param p1    # Lcom/android/wallpaper/holospiral/HoloSpiralWallpaper;
    .param p2    # Lcom/android/wallpaper/holospiral/HoloSpiralWallpaper$1;

    invoke-direct {p0, p1}, Lcom/android/wallpaper/holospiral/HoloSpiralWallpaper$RenderScriptEngine;-><init>(Lcom/android/wallpaper/holospiral/HoloSpiralWallpaper;)V

    return-void
.end method


# virtual methods
.method public destroyRenderer()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/wallpaper/holospiral/HoloSpiralWallpaper$RenderScriptEngine;->mWallpaperRS:Lcom/android/wallpaper/holospiral/HoloSpiralRS;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/wallpaper/holospiral/HoloSpiralWallpaper$RenderScriptEngine;->mWallpaperRS:Lcom/android/wallpaper/holospiral/HoloSpiralRS;

    invoke-virtual {v0}, Lcom/android/wallpaper/holospiral/HoloSpiralRS;->stop()V

    iput-object v1, p0, Lcom/android/wallpaper/holospiral/HoloSpiralWallpaper$RenderScriptEngine;->mWallpaperRS:Lcom/android/wallpaper/holospiral/HoloSpiralRS;

    :cond_0
    iget-object v0, p0, Lcom/android/wallpaper/holospiral/HoloSpiralWallpaper$RenderScriptEngine;->mRenderScript:Landroid/renderscript/RenderScriptGL;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/wallpaper/holospiral/HoloSpiralWallpaper$RenderScriptEngine;->mRenderScript:Landroid/renderscript/RenderScriptGL;

    invoke-virtual {v0, v1, v2, v2}, Landroid/renderscript/RenderScriptGL;->setSurface(Landroid/view/SurfaceHolder;II)V

    iget-object v0, p0, Lcom/android/wallpaper/holospiral/HoloSpiralWallpaper$RenderScriptEngine;->mRenderScript:Landroid/renderscript/RenderScriptGL;

    invoke-virtual {v0}, Landroid/renderscript/RenderScriptGL;->destroy()V

    iput-object v1, p0, Lcom/android/wallpaper/holospiral/HoloSpiralWallpaper$RenderScriptEngine;->mRenderScript:Landroid/renderscript/RenderScriptGL;

    :cond_1
    return-void
.end method

.method public onCommand(Ljava/lang/String;IIILandroid/os/Bundle;Z)Landroid/os/Bundle;
    .locals 7
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # Landroid/os/Bundle;
    .param p6    # Z

    iget-object v0, p0, Lcom/android/wallpaper/holospiral/HoloSpiralWallpaper$RenderScriptEngine;->mWallpaperRS:Lcom/android/wallpaper/holospiral/HoloSpiralRS;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/wallpaper/holospiral/HoloSpiralWallpaper$RenderScriptEngine;->mWallpaperRS:Lcom/android/wallpaper/holospiral/HoloSpiralRS;

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    move v6, p6

    invoke-virtual/range {v0 .. v6}, Lcom/android/wallpaper/holospiral/HoloSpiralRS;->onCommand(Ljava/lang/String;IIILandroid/os/Bundle;Z)Landroid/os/Bundle;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreate(Landroid/view/SurfaceHolder;)V
    .locals 1
    .param p1    # Landroid/view/SurfaceHolder;

    const/4 v0, 0x1

    invoke-super {p0, p1}, Landroid/service/wallpaper/WallpaperService$Engine;->onCreate(Landroid/view/SurfaceHolder;)V

    invoke-virtual {p0, v0}, Lcom/android/wallpaper/holospiral/HoloSpiralWallpaper$RenderScriptEngine;->setTouchEventsEnabled(Z)V

    invoke-interface {p1}, Landroid/view/SurfaceHolder;->setSizeFromLayout()V

    invoke-interface {p1, v0}, Landroid/view/SurfaceHolder;->setFormat(I)V

    return-void
.end method

.method public onDestroy()V
    .locals 0

    invoke-super {p0}, Landroid/service/wallpaper/WallpaperService$Engine;->onDestroy()V

    invoke-virtual {p0}, Lcom/android/wallpaper/holospiral/HoloSpiralWallpaper$RenderScriptEngine;->destroyRenderer()V

    return-void
.end method

.method public onOffsetsChanged(FFFFII)V
    .locals 1
    .param p1    # F
    .param p2    # F
    .param p3    # F
    .param p4    # F
    .param p5    # I
    .param p6    # I

    iget-object v0, p0, Lcom/android/wallpaper/holospiral/HoloSpiralWallpaper$RenderScriptEngine;->mWallpaperRS:Lcom/android/wallpaper/holospiral/HoloSpiralRS;

    invoke-virtual {v0, p1, p2, p5, p6}, Lcom/android/wallpaper/holospiral/HoloSpiralRS;->setOffset(FFII)V

    return-void
.end method

.method public onSurfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 3
    .param p1    # Landroid/view/SurfaceHolder;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    invoke-super {p0, p1, p2, p3, p4}, Landroid/service/wallpaper/WallpaperService$Engine;->onSurfaceChanged(Landroid/view/SurfaceHolder;III)V

    iget-object v0, p0, Lcom/android/wallpaper/holospiral/HoloSpiralWallpaper$RenderScriptEngine;->mRenderScript:Landroid/renderscript/RenderScriptGL;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/wallpaper/holospiral/HoloSpiralWallpaper$RenderScriptEngine;->mRenderScript:Landroid/renderscript/RenderScriptGL;

    invoke-virtual {v0, p1, p3, p4}, Landroid/renderscript/RenderScriptGL;->setSurface(Landroid/view/SurfaceHolder;II)V

    :cond_0
    iget-object v0, p0, Lcom/android/wallpaper/holospiral/HoloSpiralWallpaper$RenderScriptEngine;->mWallpaperRS:Lcom/android/wallpaper/holospiral/HoloSpiralRS;

    if-nez v0, :cond_1

    new-instance v0, Lcom/android/wallpaper/holospiral/HoloSpiralRS;

    iget-object v1, p0, Lcom/android/wallpaper/holospiral/HoloSpiralWallpaper$RenderScriptEngine;->mRenderScript:Landroid/renderscript/RenderScriptGL;

    iget-object v2, p0, Lcom/android/wallpaper/holospiral/HoloSpiralWallpaper$RenderScriptEngine;->this$0:Lcom/android/wallpaper/holospiral/HoloSpiralWallpaper;

    invoke-virtual {v2}, Lcom/android/wallpaper/holospiral/HoloSpiralWallpaper;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/android/wallpaper/holospiral/HoloSpiralRS;-><init>(Landroid/renderscript/RenderScriptGL;Landroid/content/res/Resources;)V

    iput-object v0, p0, Lcom/android/wallpaper/holospiral/HoloSpiralWallpaper$RenderScriptEngine;->mWallpaperRS:Lcom/android/wallpaper/holospiral/HoloSpiralRS;

    iget-object v0, p0, Lcom/android/wallpaper/holospiral/HoloSpiralWallpaper$RenderScriptEngine;->mWallpaperRS:Lcom/android/wallpaper/holospiral/HoloSpiralRS;

    invoke-virtual {v0}, Lcom/android/wallpaper/holospiral/HoloSpiralRS;->start()V

    :cond_1
    iget-object v0, p0, Lcom/android/wallpaper/holospiral/HoloSpiralWallpaper$RenderScriptEngine;->mWallpaperRS:Lcom/android/wallpaper/holospiral/HoloSpiralRS;

    invoke-virtual {v0, p3, p4}, Lcom/android/wallpaper/holospiral/HoloSpiralRS;->resize(II)V

    return-void
.end method

.method public onSurfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 3
    .param p1    # Landroid/view/SurfaceHolder;

    invoke-super {p0, p1}, Landroid/service/wallpaper/WallpaperService$Engine;->onSurfaceCreated(Landroid/view/SurfaceHolder;)V

    new-instance v0, Landroid/renderscript/RenderScriptGL$SurfaceConfig;

    invoke-direct {v0}, Landroid/renderscript/RenderScriptGL$SurfaceConfig;-><init>()V

    new-instance v1, Landroid/renderscript/RenderScriptGL;

    iget-object v2, p0, Lcom/android/wallpaper/holospiral/HoloSpiralWallpaper$RenderScriptEngine;->this$0:Lcom/android/wallpaper/holospiral/HoloSpiralWallpaper;

    invoke-direct {v1, v2, v0}, Landroid/renderscript/RenderScriptGL;-><init>(Landroid/content/Context;Landroid/renderscript/RenderScriptGL$SurfaceConfig;)V

    iput-object v1, p0, Lcom/android/wallpaper/holospiral/HoloSpiralWallpaper$RenderScriptEngine;->mRenderScript:Landroid/renderscript/RenderScriptGL;

    iget-object v1, p0, Lcom/android/wallpaper/holospiral/HoloSpiralWallpaper$RenderScriptEngine;->mRenderScript:Landroid/renderscript/RenderScriptGL;

    sget-object v2, Landroid/renderscript/RenderScript$Priority;->LOW:Landroid/renderscript/RenderScript$Priority;

    invoke-virtual {v1, v2}, Landroid/renderscript/RenderScriptGL;->setPriority(Landroid/renderscript/RenderScript$Priority;)V

    return-void
.end method

.method public onSurfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 0
    .param p1    # Landroid/view/SurfaceHolder;

    invoke-super {p0, p1}, Landroid/service/wallpaper/WallpaperService$Engine;->onSurfaceDestroyed(Landroid/view/SurfaceHolder;)V

    invoke-virtual {p0}, Lcom/android/wallpaper/holospiral/HoloSpiralWallpaper$RenderScriptEngine;->destroyRenderer()V

    return-void
.end method

.method public onVisibilityChanged(Z)V
    .locals 1
    .param p1    # Z

    invoke-super {p0, p1}, Landroid/service/wallpaper/WallpaperService$Engine;->onVisibilityChanged(Z)V

    iget-object v0, p0, Lcom/android/wallpaper/holospiral/HoloSpiralWallpaper$RenderScriptEngine;->mWallpaperRS:Lcom/android/wallpaper/holospiral/HoloSpiralRS;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/android/wallpaper/holospiral/HoloSpiralWallpaper$RenderScriptEngine;->mWallpaperRS:Lcom/android/wallpaper/holospiral/HoloSpiralRS;

    invoke-virtual {v0}, Lcom/android/wallpaper/holospiral/HoloSpiralRS;->start()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/wallpaper/holospiral/HoloSpiralWallpaper$RenderScriptEngine;->mWallpaperRS:Lcom/android/wallpaper/holospiral/HoloSpiralRS;

    invoke-virtual {v0}, Lcom/android/wallpaper/holospiral/HoloSpiralRS;->stop()V

    goto :goto_0
.end method
