.class public final Liv;
.super Lcom/twitter/library/api/upload/ad;
.source "Twttr"


# instance fields
.field private d:J

.field private n:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V
    .locals 1

    const-class v0, Liv;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/api/upload/ad;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    return-void
.end method

.method private a(Lcom/twitter/library/api/MediaEntity;Lcom/twitter/library/api/TweetEntities;Z)J
    .locals 8

    const/4 v7, 0x2

    invoke-virtual {p0}, Liv;->s()Lcom/twitter/library/service/p;

    move-result-object v0

    iget-wide v3, v0, Lcom/twitter/library/service/p;->c:J

    new-instance v0, Lcom/twitter/library/api/upload/a;

    iget-object v1, p0, Liv;->l:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/twitter/library/api/upload/a;-><init>(Landroid/content/Context;)V

    iget-boolean v1, p0, Liv;->n:Z

    invoke-virtual {v0, v1}, Lcom/twitter/library/api/upload/a;->b(Z)Lcom/twitter/library/api/upload/a;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/twitter/library/api/upload/a;->a(Z)Lcom/twitter/library/api/upload/a;

    move-result-object v0

    if-eqz p1, :cond_2

    invoke-direct {p0, p1, v0}, Liv;->a(Lcom/twitter/library/api/MediaEntity;Lcom/twitter/library/api/upload/a;)J

    move-result-wide v0

    move-wide v1, v0

    :goto_0
    const-wide/16 v5, 0x0

    cmp-long v0, v1, v5

    if-nez v0, :cond_1

    new-instance v5, Lcom/twitter/library/scribe/ScribeLog;

    invoke-direct {v5, v3, v4}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v0, 0x3

    new-array v3, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string/jumbo v4, "app:twitter_service:direct_messages"

    aput-object v4, v3, v0

    const/4 v4, 0x1

    if-eqz p3, :cond_3

    const-string/jumbo v0, "retry_dm"

    :goto_1
    aput-object v0, v3, v4

    const-string/jumbo v0, "failure"

    aput-object v0, v3, v7

    invoke-virtual {v5, v3}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    const-string/jumbo v3, "has_media"

    invoke-virtual {v0, v3}, Lcom/twitter/library/scribe/ScribeLog;->h(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    const/4 v3, 0x6

    invoke-virtual {v0, v3}, Lcom/twitter/library/scribe/ScribeLog;->b(I)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v3

    iget-boolean v0, p0, Liv;->n:Z

    if-eqz v0, :cond_0

    invoke-virtual {v3, v7}, Lcom/twitter/library/scribe/ScribeLog;->c(I)Lcom/twitter/library/scribe/ScribeLog;

    :cond_0
    const-string/jumbo v0, "Upload Media Failed"

    invoke-virtual {v3, v0}, Lcom/twitter/library/scribe/ScribeLog;->c(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    iget-object v0, p0, Liv;->l:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/library/telephony/TelephonyUtil;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string/jumbo v0, "connected"

    :goto_2
    invoke-virtual {v3, v0}, Lcom/twitter/library/scribe/ScribeLog;->d(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    iget-object v0, p0, Liv;->l:Landroid/content/Context;

    invoke-static {v0, v3}, Lcom/twitter/library/scribe/ScribeService;->a(Landroid/content/Context;Lcom/twitter/library/scribe/ScribeLog;)V

    :cond_1
    return-wide v1

    :cond_2
    invoke-direct {p0, p2, v0}, Liv;->a(Lcom/twitter/library/api/TweetEntities;Lcom/twitter/library/api/upload/a;)J

    move-result-wide v0

    move-wide v1, v0

    goto :goto_0

    :cond_3
    const-string/jumbo v0, "send_dm"

    goto :goto_1

    :cond_4
    const-string/jumbo v0, "disconnected"

    goto :goto_2
.end method

.method private a(Lcom/twitter/library/api/MediaEntity;Lcom/twitter/library/api/upload/a;)J
    .locals 5

    invoke-virtual {p0}, Liv;->s()Lcom/twitter/library/service/p;

    move-result-object v0

    iget-object v1, p0, Liv;->l:Landroid/content/Context;

    iget-wide v2, v0, Lcom/twitter/library/service/p;->c:J

    const/4 v4, 0x0

    invoke-static {v1, v2, v3, p1, v4}, Lcom/twitter/library/api/upload/j;->a(Landroid/content/Context;JLcom/twitter/library/api/MediaEntity;Z)Lcom/twitter/library/api/upload/h;

    move-result-object v1

    new-instance v2, Lcom/twitter/library/api/upload/f;

    iget-object v3, p0, Liv;->l:Landroid/content/Context;

    invoke-direct {v2, v3, v0}, Lcom/twitter/library/api/upload/f;-><init>(Landroid/content/Context;Lcom/twitter/library/service/p;)V

    invoke-virtual {v2, v1}, Lcom/twitter/library/api/upload/f;->a(Lcom/twitter/library/api/upload/h;)Lcom/twitter/library/api/upload/f;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/twitter/library/api/upload/f;->b(Lcom/twitter/library/api/upload/o;)Lcom/twitter/library/api/upload/ad;

    move-result-object v0

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lcom/twitter/library/api/upload/ad;->c(I)Lcom/twitter/library/service/b;

    invoke-virtual {p0, v2}, Liv;->a(Lcom/twitter/library/api/upload/ad;)Lcom/twitter/library/service/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/service/e;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v2}, Lcom/twitter/library/api/upload/f;->g()J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/twitter/library/api/TweetEntities;Lcom/twitter/library/api/upload/a;)J
    .locals 4

    new-instance v2, Lcom/twitter/library/api/upload/k;

    iget-object v0, p0, Liv;->l:Landroid/content/Context;

    invoke-virtual {p0}, Liv;->s()Lcom/twitter/library/service/p;

    move-result-object v1

    invoke-direct {v2, v0, v1}, Lcom/twitter/library/api/upload/k;-><init>(Landroid/content/Context;Lcom/twitter/library/service/p;)V

    invoke-virtual {v2, p1}, Lcom/twitter/library/api/upload/k;->a(Lcom/twitter/library/api/TweetEntities;)Lcom/twitter/library/api/upload/k;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/twitter/library/api/upload/k;->a(Lcom/twitter/library/api/upload/o;)Lcom/twitter/library/api/upload/k;

    move-result-object v0

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lcom/twitter/library/api/upload/k;->c(I)Lcom/twitter/library/service/b;

    invoke-virtual {p0, v2}, Liv;->a(Lcom/twitter/library/api/upload/ad;)Lcom/twitter/library/service/e;

    move-result-object v3

    const-wide/16 v0, 0x0

    invoke-virtual {v3}, Lcom/twitter/library/service/e;->a()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Lcom/twitter/library/api/upload/k;->b()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    :cond_0
    return-wide v0
.end method

.method private a(JJLjava/lang/String;Ljava/lang/String;Lcom/twitter/library/api/MediaEntity;Lcom/twitter/library/api/TweetEntities;Lcom/twitter/library/service/e;I)V
    .locals 14

    invoke-virtual {p0}, Liv;->s()Lcom/twitter/library/service/p;

    move-result-object v5

    invoke-virtual {p0}, Liv;->w()Lcom/twitter/library/provider/az;

    move-result-object v6

    iget-object v2, p0, Liv;->m:Lcom/twitter/library/network/aa;

    iget-object v2, v2, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string/jumbo v7, "1.1"

    aput-object v7, v3, v4

    const/4 v4, 0x1

    const-string/jumbo v7, "direct_messages"

    aput-object v7, v3, v4

    const/4 v4, 0x2

    const-string/jumbo v7, "new"

    aput-object v7, v3, v4

    invoke-static {v2, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ".json"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    if-nez p5, :cond_1

    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v3, "user_id"

    invoke-static/range {p3 .. p4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_0
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v3, "text"

    move-object/from16 v0, p6

    invoke-direct {v2, v3, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v3, "include_entities"

    const-string/jumbo v4, "true"

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string/jumbo v2, "send_error_codes"

    const/4 v3, 0x1

    invoke-static {v7, v2, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    iget-object v9, p0, Liv;->k:Landroid/os/Bundle;

    if-nez p7, :cond_0

    if-eqz p8, :cond_2

    const/4 v2, 0x1

    move-object/from16 v0, p8

    invoke-virtual {v0, v2}, Lcom/twitter/library/api/TweetEntities;->a(I)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_0
    const/4 v2, 0x1

    move v4, v2

    :goto_1
    const/16 v2, 0x9

    move/from16 v0, p10

    if-ne v0, v2, :cond_3

    const/4 v2, 0x1

    move v3, v2

    :goto_2
    if-eqz v4, :cond_5

    move-object/from16 v0, p7

    move-object/from16 v1, p8

    invoke-direct {p0, v0, v1, v3}, Liv;->a(Lcom/twitter/library/api/MediaEntity;Lcom/twitter/library/api/TweetEntities;Z)J

    move-result-wide v10

    const-wide/16 v12, 0x0

    cmp-long v2, v10, v12

    if-nez v2, :cond_4

    const/4 v2, 0x2

    move-wide v0, p1

    invoke-virtual {v6, v0, v1, v2}, Lcom/twitter/library/provider/az;->k(JI)V

    const/16 v2, 0x1f7

    move-object/from16 v0, p9

    invoke-virtual {v0, v2}, Lcom/twitter/library/service/e;->a(I)V

    const-string/jumbo v2, "draft_id"

    move-wide v0, p1

    invoke-virtual {v9, v2, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    :goto_3
    return-void

    :cond_1
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v3, "screen_name"

    move-object/from16 v0, p5

    invoke-direct {v2, v3, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    move v4, v2

    goto :goto_1

    :cond_3
    const/4 v2, 0x0

    move v3, v2

    goto :goto_2

    :cond_4
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v12, "media_id"

    invoke-static {v10, v11}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v2, v12, v10}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_5
    const/16 v2, 0x1f

    invoke-static {v2}, Lcom/twitter/library/api/ao;->a(I)Lcom/twitter/library/api/ao;

    move-result-object v2

    new-instance v10, Lcom/twitter/library/network/d;

    iget-object v11, p0, Liv;->l:Landroid/content/Context;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v10, v11, v7}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    sget-object v7, Lcom/twitter/internal/network/HttpOperation$RequestMethod;->b:Lcom/twitter/internal/network/HttpOperation$RequestMethod;

    invoke-virtual {v10, v7}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/HttpOperation$RequestMethod;)Lcom/twitter/library/network/d;

    move-result-object v7

    new-instance v10, Lcom/twitter/library/network/n;

    iget-object v11, v5, Lcom/twitter/library/service/p;->d:Lcom/twitter/library/network/OAuthToken;

    invoke-direct {v10, v11}, Lcom/twitter/library/network/n;-><init>(Lcom/twitter/library/network/OAuthToken;)V

    invoke-virtual {v7, v10}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v7

    invoke-virtual {v7, v8}, Lcom/twitter/library/network/d;->a(Ljava/util/ArrayList;)Lcom/twitter/library/network/d;

    move-result-object v7

    invoke-virtual {v7, v2}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v7

    invoke-virtual {v7}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v7

    invoke-virtual {v7}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v7

    invoke-virtual {v7}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v8

    if-eqz v8, :cond_8

    invoke-virtual {v2}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/api/ak;

    move-wide v0, p1

    invoke-virtual {v6, v0, v1, v2}, Lcom/twitter/library/provider/az;->a(JLcom/twitter/library/api/ak;)V

    new-instance v6, Lcom/twitter/library/api/search/a;

    iget-object v8, p0, Liv;->l:Landroid/content/Context;

    iget-object v10, v2, Lcom/twitter/library/api/ak;->e:Lcom/twitter/library/api/TwitterUser;

    invoke-direct {v6, v8, v5, v10}, Lcom/twitter/library/api/search/a;-><init>(Landroid/content/Context;Lcom/twitter/library/service/p;Lcom/twitter/library/api/TwitterUser;)V

    invoke-virtual {p0, v6}, Liv;->b(Lcom/twitter/internal/android/service/a;)V

    const-string/jumbo v6, "user"

    iget-object v2, v2, Lcom/twitter/library/api/ak;->e:Lcom/twitter/library/api/TwitterUser;

    invoke-virtual {v9, v6, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    if-eqz p7, :cond_6

    move-object/from16 v0, p7

    iget-object v2, v0, Lcom/twitter/library/api/MediaEntity;->mediaUrl:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v2}, Lcom/twitter/library/util/n;->b(Landroid/net/Uri;)Z

    :cond_6
    :goto_4
    invoke-virtual {v7}, Lcom/twitter/internal/network/HttpOperation;->l()Lcom/twitter/internal/network/k;

    move-result-object v6

    if-eqz v6, :cond_7

    new-instance v8, Lcom/twitter/library/scribe/ScribeLog;

    iget-wide v9, v5, Lcom/twitter/library/service/p;->c:J

    invoke-direct {v8, v9, v10}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v2, 0x3

    new-array v5, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v9, "app:twitter_service:direct_messages"

    aput-object v9, v5, v2

    const/4 v9, 0x1

    if-eqz v3, :cond_9

    const-string/jumbo v2, "retry_dm"

    :goto_5
    aput-object v2, v5, v9

    const/4 v3, 0x2

    iget v2, v6, Lcom/twitter/internal/network/k;->a:I

    const/16 v9, 0xc8

    if-ne v2, v9, :cond_a

    const-string/jumbo v2, "success"

    :goto_6
    aput-object v2, v5, v3

    invoke-virtual {v8, v5}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v2

    invoke-direct {p0, v2, v6, v4}, Liv;->a(Lcom/twitter/library/scribe/ScribeLog;Lcom/twitter/internal/network/k;Z)V

    :cond_7
    move-object/from16 v0, p9

    invoke-virtual {v0, v7}, Lcom/twitter/library/service/e;->a(Lcom/twitter/internal/network/HttpOperation;)V

    goto/16 :goto_3

    :cond_8
    const/4 v8, 0x2

    move-wide v0, p1

    invoke-virtual {v6, v0, v1, v8}, Lcom/twitter/library/provider/az;->k(JI)V

    invoke-virtual {v2}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    const-string/jumbo v6, "custom_errors"

    invoke-static {v2}, Liv;->c(Ljava/util/ArrayList;)[I

    move-result-object v2

    invoke-virtual {v9, v6, v2}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    const-string/jumbo v2, "draft_id"

    move-wide v0, p1

    invoke-virtual {v9, v2, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    goto :goto_4

    :cond_9
    const-string/jumbo v2, "send_dm"

    goto :goto_5

    :cond_a
    const-string/jumbo v2, "failure"

    goto :goto_6
.end method

.method private a(JLcom/twitter/library/service/e;I)V
    .locals 12

    invoke-virtual {p0}, Liv;->w()Lcom/twitter/library/provider/az;

    move-result-object v0

    iget-wide v1, p0, Liv;->d:J

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/provider/az;->k(J)Landroid/database/Cursor;

    move-result-object v11

    if-eqz v11, :cond_1

    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    invoke-interface {v11, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    const/4 v0, 0x3

    invoke-interface {v11, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v0, 0x1

    invoke-interface {v11, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v0, 0x5

    invoke-interface {v11, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/util/Util;->a([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/TweetEntities;

    if-eqz v0, :cond_2

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/library/api/TweetEntities;->a(I)Z

    move-result v1

    if-eqz v1, :cond_2

    move-object v8, v0

    :goto_0
    const/4 v7, 0x0

    move-object v0, p0

    move-wide v1, p1

    move-object v9, p3

    move/from16 v10, p4

    invoke-direct/range {v0 .. v10}, Liv;->a(JJLjava/lang/String;Ljava/lang/String;Lcom/twitter/library/api/MediaEntity;Lcom/twitter/library/api/TweetEntities;Lcom/twitter/library/service/e;I)V

    :cond_0
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    :cond_1
    return-void

    :cond_2
    const/4 v8, 0x0

    goto :goto_0
.end method

.method private a(Lcom/twitter/library/scribe/ScribeLog;Lcom/twitter/internal/network/k;Z)V
    .locals 1

    if-nez p2, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-boolean v0, p0, Liv;->n:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lcom/twitter/library/scribe/ScribeLog;->c(I)Lcom/twitter/library/scribe/ScribeLog;

    :cond_1
    if-eqz p3, :cond_2

    const-string/jumbo v0, "has_media"

    :goto_1
    invoke-virtual {p1, v0}, Lcom/twitter/library/scribe/ScribeLog;->h(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    invoke-static {p2}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/internal/network/k;)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/twitter/library/scribe/ScribeLog;->b(I)Lcom/twitter/library/scribe/ScribeLog;

    iget-object v0, p0, Liv;->l:Landroid/content/Context;

    invoke-static {v0, p1, p2}, Lcom/twitter/library/scribe/ScribeLog;->a(Landroid/content/Context;Lcom/twitter/library/scribe/ScribeLog;Lcom/twitter/internal/network/k;)V

    iget-object v0, p0, Liv;->l:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/twitter/library/scribe/ScribeService;->a(Landroid/content/Context;Lcom/twitter/library/scribe/ScribeLog;)V

    goto :goto_0

    :cond_2
    const-string/jumbo v0, "no_media"

    goto :goto_1
.end method


# virtual methods
.method public a(JZ)V
    .locals 0

    iput-wide p1, p0, Liv;->d:J

    iput-boolean p3, p0, Liv;->n:Z

    return-void
.end method

.method protected c(Lcom/twitter/library/service/e;)V
    .locals 11

    invoke-virtual {p0}, Liv;->r()I

    move-result v10

    invoke-virtual {p0}, Liv;->w()Lcom/twitter/library/provider/az;

    move-result-object v0

    packed-switch v10, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v7, p0, Liv;->k:Landroid/os/Bundle;

    const-string/jumbo v1, "sender_id"

    invoke-virtual {v7, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v1

    const-string/jumbo v3, "recipient_id"

    invoke-virtual {v7, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v3

    const-string/jumbo v5, "recipient_username"

    invoke-virtual {v7, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "content"

    invoke-virtual {v7, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v8, "media"

    invoke-virtual {v7, v8}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v7

    check-cast v7, Lcom/twitter/library/api/MediaEntity;

    if-eqz v7, :cond_0

    iget-boolean v8, v7, Lcom/twitter/library/api/MediaEntity;->processed:Z

    if-nez v8, :cond_0

    iget-object v8, p0, Liv;->l:Landroid/content/Context;

    invoke-static {v8, v1, v2, v7}, Lcom/twitter/library/util/n;->a(Landroid/content/Context;JLcom/twitter/library/api/MediaEntity;)Z

    iget-object v8, v7, Lcom/twitter/library/api/MediaEntity;->url:Ljava/lang/String;

    iput-object v8, v7, Lcom/twitter/library/api/MediaEntity;->mediaUrl:Ljava/lang/String;

    :cond_0
    invoke-virtual/range {v0 .. v7}, Lcom/twitter/library/provider/az;->a(JJLjava/lang/String;Ljava/lang/String;Lcom/twitter/library/api/MediaEntity;)J

    move-result-wide v1

    const/4 v8, 0x1

    invoke-virtual {v0, v1, v2, v8}, Lcom/twitter/library/provider/az;->k(JI)V

    const/4 v8, 0x0

    move-object v0, p0

    move-object v9, p1

    invoke-direct/range {v0 .. v10}, Liv;->a(JJLjava/lang/String;Ljava/lang/String;Lcom/twitter/library/api/MediaEntity;Lcom/twitter/library/api/TweetEntities;Lcom/twitter/library/service/e;I)V

    goto :goto_0

    :pswitch_1
    iget-wide v1, p0, Liv;->d:J

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/provider/az;->l(J)V

    goto :goto_0

    :pswitch_2
    iget-wide v1, p0, Liv;->d:J

    const/4 v3, 0x3

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/library/provider/az;->k(JI)V

    iget-wide v0, p0, Liv;->d:J

    invoke-direct {p0, v0, v1, p1, v10}, Liv;->a(JLcom/twitter/library/service/e;I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
