.class public Ljl;
.super Lcom/twitter/library/service/o;
.source "Twttr"


# static fields
.field private static final d:Z


# instance fields
.field private final e:Lcom/twitter/library/api/TwitterUser;

.field private f:I

.field private g:J

.field private n:J

.field private o:Z

.field private p:Z

.field private q:Lcom/twitter/library/service/TimelineHelper$FilterType;

.field private r:Lcom/twitter/library/service/r;

.field private s:I

.field private t:I

.field private u:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    invoke-static {}, Lcom/twitter/library/client/App;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "HomeTimeline"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Ljl;->d:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/library/api/TwitterUser;JJI)V
    .locals 9

    new-instance v2, Lcom/twitter/library/service/p;

    invoke-direct {v2, p2}, Lcom/twitter/library/service/p;-><init>(Lcom/twitter/library/client/Session;)V

    move-object v0, p0

    move-object v1, p1

    move-object v3, p3

    move-wide v4, p4

    move-wide v6, p6

    move/from16 v8, p8

    invoke-direct/range {v0 .. v8}, Ljl;-><init>(Landroid/content/Context;Lcom/twitter/library/service/p;Lcom/twitter/library/api/TwitterUser;JJI)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/service/p;Lcom/twitter/library/api/TwitterUser;JJI)V
    .locals 1

    const-class v0, Ljl;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/service/o;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/service/p;)V

    iput-wide p4, p0, Ljl;->g:J

    iput-wide p6, p0, Ljl;->n:J

    if-eqz p8, :cond_0

    :goto_0
    iput p8, p0, Ljl;->f:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Ljl;->p:Z

    sget-object v0, Lcom/twitter/library/service/TimelineHelper$FilterType;->a:Lcom/twitter/library/service/TimelineHelper$FilterType;

    iput-object v0, p0, Ljl;->q:Lcom/twitter/library/service/TimelineHelper$FilterType;

    iput-object p3, p0, Ljl;->e:Lcom/twitter/library/api/TwitterUser;

    return-void

    :cond_0
    const/16 p8, 0x64

    goto :goto_0
.end method


# virtual methods
.method protected a(Lcom/twitter/library/service/e;)Lcom/twitter/internal/network/HttpOperation;
    .locals 19

    move-object/from16 v0, p0

    iget-object v4, v0, Ljl;->e:Lcom/twitter/library/api/TwitterUser;

    invoke-virtual/range {p0 .. p0}, Ljl;->w()Lcom/twitter/library/provider/az;

    move-result-object v2

    move-object/from16 v0, p0

    iget-boolean v1, v0, Ljl;->p:Z

    if-eqz v1, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Ljl;->l:Landroid/content/Context;

    invoke-static {v1}, Lcom/twitter/library/service/TimelineHelper;->a(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-boolean v1, v0, Ljl;->p:Z

    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, Ljl;->k:Landroid/os/Bundle;

    const-string/jumbo v3, "lat"

    const-wide/high16 v5, 0x7ff8000000000000L    # NaN

    invoke-virtual {v1, v3, v5, v6}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;D)D

    move-result-wide v11

    move-object/from16 v0, p0

    iget-object v1, v0, Ljl;->k:Landroid/os/Bundle;

    const-string/jumbo v3, "long"

    const-wide/high16 v5, 0x7ff8000000000000L    # NaN

    invoke-virtual {v1, v3, v5, v6}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;D)D

    move-result-wide v13

    invoke-virtual/range {p0 .. p0}, Ljl;->s()Lcom/twitter/library/service/p;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v1, v0, Ljl;->l:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v3, v0, Ljl;->m:Lcom/twitter/library/network/aa;

    new-instance v5, Lcom/twitter/library/network/n;

    iget-object v6, v6, Lcom/twitter/library/service/p;->d:Lcom/twitter/library/network/OAuthToken;

    invoke-direct {v5, v6}, Lcom/twitter/library/network/n;-><init>(Lcom/twitter/library/network/OAuthToken;)V

    move-object/from16 v0, p0

    iget-wide v6, v0, Ljl;->g:J

    move-object/from16 v0, p0

    iget-wide v8, v0, Ljl;->n:J

    move-object/from16 v0, p0

    iget v10, v0, Ljl;->f:I

    invoke-virtual/range {p0 .. p0}, Ljl;->t()Z

    move-result v15

    move-object/from16 v0, p0

    iget-boolean v0, v0, Ljl;->o:Z

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Ljl;->k:Landroid/os/Bundle;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Ljs;->a(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Ljl;->q:Lcom/twitter/library/service/TimelineHelper$FilterType;

    move-object/from16 v18, v0

    invoke-static/range {v1 .. v18}, Lcom/twitter/library/service/TimelineHelper;->a(Landroid/content/Context;Lcom/twitter/library/provider/az;Lcom/twitter/library/network/aa;Lcom/twitter/library/api/TwitterUser;Lcom/twitter/library/network/a;JJIDDZZLjava/lang/String;Lcom/twitter/library/service/TimelineHelper$FilterType;)Lcom/twitter/library/service/r;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Ljl;->r:Lcom/twitter/library/service/r;

    move-object/from16 v0, p0

    iget-object v1, v0, Ljl;->r:Lcom/twitter/library/service/r;

    iget-object v1, v1, Lcom/twitter/library/service/r;->a:Lcom/twitter/internal/network/HttpOperation;

    :goto_0
    return-object v1

    :cond_1
    move-object/from16 v0, p0

    iget v1, v0, Ljl;->f:I

    const/4 v3, -0x2

    if-ne v1, v3, :cond_4

    const/4 v1, 0x1

    :goto_1
    move-object/from16 v0, p0

    iput-boolean v1, v0, Ljl;->p:Z

    move-object/from16 v0, p0

    iget-boolean v1, v0, Ljl;->p:Z

    if-eqz v1, :cond_0

    const/16 v1, 0x64

    move-object/from16 v0, p0

    iput v1, v0, Ljl;->f:I

    iget-wide v5, v4, Lcom/twitter/library/api/TwitterUser;->userId:J

    const/4 v1, 0x0

    invoke-virtual {v2, v5, v6, v1}, Lcom/twitter/library/provider/az;->h(JI)J

    move-result-wide v5

    move-object/from16 v0, p0

    iput-wide v5, v0, Ljl;->g:J

    iget-wide v5, v4, Lcom/twitter/library/api/TwitterUser;->userId:J

    const/4 v1, 0x0

    invoke-virtual {v2, v5, v6, v1}, Lcom/twitter/library/provider/az;->g(JI)J

    move-result-wide v5

    move-object/from16 v0, p0

    iput-wide v5, v0, Ljl;->n:J

    sget-boolean v1, Ljl;->d:Z

    if-eqz v1, :cond_2

    const-string/jumbo v1, "HomeTimeline"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "[Refresh] sinceId: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-wide v5, v0, Ljl;->g:J

    invoke-virtual {v3, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v5, ", maxId: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-wide v5, v0, Ljl;->n:J

    invoke-virtual {v3, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    move-object/from16 v0, p0

    iget-wide v5, v0, Ljl;->g:J

    const-wide/16 v7, 0x0

    cmp-long v1, v5, v7

    if-eqz v1, :cond_3

    move-object/from16 v0, p0

    iget-wide v5, v0, Ljl;->n:J

    const-wide/16 v7, 0x0

    cmp-long v1, v5, v7

    if-eqz v1, :cond_3

    move-object/from16 v0, p0

    iget-wide v5, v0, Ljl;->n:J

    move-object/from16 v0, p0

    iget-wide v7, v0, Ljl;->g:J

    cmp-long v1, v5, v7

    if-nez v1, :cond_0

    :cond_3
    const/4 v1, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Lcom/twitter/library/service/e;->a(Z)V

    const/4 v1, 0x0

    goto/16 :goto_0

    :cond_4
    const/4 v1, 0x0

    goto/16 :goto_1
.end method

.method public a(I)Ljl;
    .locals 0

    iput p1, p0, Ljl;->s:I

    return-object p0
.end method

.method public a(Lcom/twitter/library/service/TimelineHelper$FilterType;)Ljl;
    .locals 0

    iput-object p1, p0, Ljl;->q:Lcom/twitter/library/service/TimelineHelper$FilterType;

    return-object p0
.end method

.method public a(Z)Ljl;
    .locals 0

    iput-boolean p1, p0, Ljl;->o:Z

    return-object p0
.end method

.method protected a(Lcom/twitter/internal/network/HttpOperation;Lcom/twitter/library/service/e;)V
    .locals 17

    invoke-virtual/range {p0 .. p0}, Ljl;->s()Lcom/twitter/library/service/p;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v2, v0, Ljl;->l:Landroid/content/Context;

    invoke-virtual/range {p0 .. p0}, Ljl;->w()Lcom/twitter/library/provider/az;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Ljl;->e:Lcom/twitter/library/api/TwitterUser;

    move-object/from16 v0, p0

    iget-object v5, v0, Ljl;->r:Lcom/twitter/library/service/r;

    move-object/from16 v0, v16

    iget-object v6, v0, Lcom/twitter/library/service/p;->e:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-wide v7, v0, Ljl;->g:J

    move-object/from16 v0, p0

    iget-wide v9, v0, Ljl;->n:J

    move-object/from16 v0, p0

    iget v11, v0, Ljl;->f:I

    move-object/from16 v0, p0

    iget-boolean v12, v0, Ljl;->p:Z

    invoke-virtual/range {p0 .. p0}, Ljl;->t()Z

    move-result v13

    const/4 v14, 0x0

    move-object/from16 v0, p0

    iget-object v15, v0, Ljl;->q:Lcom/twitter/library/service/TimelineHelper$FilterType;

    invoke-static/range {v2 .. v15}, Lcom/twitter/library/service/TimelineHelper;->a(Landroid/content/Context;Lcom/twitter/library/provider/az;Lcom/twitter/library/api/TwitterUser;Lcom/twitter/library/service/r;Ljava/lang/String;JJIZZZLcom/twitter/library/service/TimelineHelper$FilterType;)V

    invoke-virtual/range {p1 .. p1}, Lcom/twitter/internal/network/HttpOperation;->k()Z

    move-result v2

    if-eqz v2, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Ljl;->r:Lcom/twitter/library/service/r;

    iget v2, v2, Lcom/twitter/library/service/r;->e:I

    if-gtz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Ljl;->r:Lcom/twitter/library/service/r;

    iget-boolean v2, v2, Lcom/twitter/library/service/r;->f:Z

    if-eqz v2, :cond_1

    :cond_0
    sget-object v2, Ljm;->a:[I

    move-object/from16 v0, p0

    iget-object v3, v0, Ljl;->q:Lcom/twitter/library/service/TimelineHelper$FilterType;

    invoke-virtual {v3}, Lcom/twitter/library/service/TimelineHelper$FilterType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    new-instance v2, Liy;

    move-object/from16 v0, p0

    iget-object v3, v0, Ljl;->l:Landroid/content/Context;

    move-object/from16 v0, v16

    invoke-direct {v2, v3, v0}, Liy;-><init>(Landroid/content/Context;Lcom/twitter/library/service/p;)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Ljl;->b(Lcom/twitter/internal/android/service/a;)V

    sget-object v2, Lcom/twitter/library/provider/av;->b:Landroid/net/Uri;

    :goto_0
    move-object/from16 v0, p0

    iget-object v3, v0, Ljl;->l:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v2, v4}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Ljl;->r:Lcom/twitter/library/service/r;

    iget v2, v2, Lcom/twitter/library/service/r;->e:I

    move-object/from16 v0, p0

    iput v2, v0, Ljl;->u:I

    move-object/from16 v0, p0

    iget-object v2, v0, Ljl;->r:Lcom/twitter/library/service/r;

    iget v2, v2, Lcom/twitter/library/service/r;->e:I

    move-object/from16 v0, p0

    iput v2, v0, Ljl;->s:I

    move-object/from16 v0, p0

    iget-object v2, v0, Ljl;->r:Lcom/twitter/library/service/r;

    iget v2, v2, Lcom/twitter/library/service/r;->d:I

    move-object/from16 v0, p0

    iput v2, v0, Ljl;->t:I

    :cond_2
    move-object/from16 v0, p2

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/twitter/library/service/e;->a(Lcom/twitter/internal/network/HttpOperation;)V

    return-void

    :pswitch_0
    sget-object v2, Lcom/twitter/library/provider/av;->e:Landroid/net/Uri;

    goto :goto_0

    :pswitch_1
    sget-object v2, Lcom/twitter/library/provider/av;->f:Landroid/net/Uri;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public b(I)Ljl;
    .locals 0

    iput p1, p0, Ljl;->t:I

    return-object p0
.end method

.method public b(Z)Ljl;
    .locals 0

    iput-boolean p1, p0, Ljl;->p:Z

    return-object p0
.end method

.method public e()J
    .locals 2

    iget-wide v0, p0, Ljl;->n:J

    return-wide v0
.end method

.method public f()I
    .locals 1

    iget v0, p0, Ljl;->s:I

    return v0
.end method
