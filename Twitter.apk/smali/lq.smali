.class public Llq;
.super Lorg/spongycastle/asn1/k;


# instance fields
.field private a:Lorg/spongycastle/asn1/m;

.field private b:Llu;

.field private c:Lorg/spongycastle/asn1/t;


# direct methods
.method public constructor <init>(Llu;Lorg/spongycastle/asn1/d;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Llq;-><init>(Llu;Lorg/spongycastle/asn1/d;Lorg/spongycastle/asn1/t;)V

    return-void
.end method

.method public constructor <init>(Llu;Lorg/spongycastle/asn1/d;Lorg/spongycastle/asn1/t;)V
    .locals 3

    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    new-instance v0, Lorg/spongycastle/asn1/bb;

    invoke-interface {p2}, Lorg/spongycastle/asn1/d;->a()Lorg/spongycastle/asn1/q;

    move-result-object v1

    const-string/jumbo v2, "DER"

    invoke-virtual {v1, v2}, Lorg/spongycastle/asn1/q;->a(Ljava/lang/String;)[B

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/bb;-><init>([B)V

    iput-object v0, p0, Llq;->a:Lorg/spongycastle/asn1/m;

    iput-object p1, p0, Llq;->b:Llu;

    iput-object p3, p0, Llq;->c:Lorg/spongycastle/asn1/t;

    return-void
.end method

.method public constructor <init>(Lorg/spongycastle/asn1/r;)V
    .locals 2

    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    invoke-virtual {p1}, Lorg/spongycastle/asn1/r;->d()Ljava/util/Enumeration;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/i;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/i;->c()Ljava/math/BigInteger;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigInteger;->intValue()I

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "wrong version for private key info"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Llu;->a(Ljava/lang/Object;)Llu;

    move-result-object v0

    iput-object v0, p0, Llq;->b:Llu;

    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/m;->a(Ljava/lang/Object;)Lorg/spongycastle/asn1/m;

    move-result-object v0

    iput-object v0, p0, Llq;->a:Lorg/spongycastle/asn1/m;

    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/w;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lorg/spongycastle/asn1/t;->a(Lorg/spongycastle/asn1/w;Z)Lorg/spongycastle/asn1/t;

    move-result-object v0

    iput-object v0, p0, Llq;->c:Lorg/spongycastle/asn1/t;

    :cond_1
    return-void
.end method

.method public static a(Ljava/lang/Object;)Llq;
    .locals 2

    instance-of v0, p0, Llq;

    if-eqz v0, :cond_0

    check-cast p0, Llq;

    :goto_0
    return-object p0

    :cond_0
    if-eqz p0, :cond_1

    new-instance v0, Llq;

    invoke-static {p0}, Lorg/spongycastle/asn1/r;->a(Ljava/lang/Object;)Lorg/spongycastle/asn1/r;

    move-result-object v1

    invoke-direct {v0, v1}, Llq;-><init>(Lorg/spongycastle/asn1/r;)V

    move-object p0, v0

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()Lorg/spongycastle/asn1/q;
    .locals 4

    const/4 v3, 0x0

    new-instance v0, Lorg/spongycastle/asn1/e;

    invoke-direct {v0}, Lorg/spongycastle/asn1/e;-><init>()V

    new-instance v1, Lorg/spongycastle/asn1/i;

    invoke-direct {v1, v3}, Lorg/spongycastle/asn1/i;-><init>(I)V

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    iget-object v1, p0, Llq;->b:Llu;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    iget-object v1, p0, Llq;->a:Lorg/spongycastle/asn1/m;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    iget-object v1, p0, Llq;->c:Lorg/spongycastle/asn1/t;

    if-eqz v1, :cond_0

    new-instance v1, Lorg/spongycastle/asn1/bk;

    iget-object v2, p0, Llq;->c:Lorg/spongycastle/asn1/t;

    invoke-direct {v1, v3, v3, v2}, Lorg/spongycastle/asn1/bk;-><init>(ZILorg/spongycastle/asn1/d;)V

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    :cond_0
    new-instance v1, Lorg/spongycastle/asn1/bf;

    invoke-direct {v1, v0}, Lorg/spongycastle/asn1/bf;-><init>(Lorg/spongycastle/asn1/e;)V

    return-object v1
.end method

.method public c()Llu;
    .locals 1

    iget-object v0, p0, Llq;->b:Llu;

    return-object v0
.end method

.method public d()Lorg/spongycastle/asn1/d;
    .locals 1

    iget-object v0, p0, Llq;->a:Lorg/spongycastle/asn1/m;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/m;->d()[B

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/q;->a([B)Lorg/spongycastle/asn1/q;

    move-result-object v0

    return-object v0
.end method
