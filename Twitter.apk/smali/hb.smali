.class public Lhb;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field public final a:Ljava/lang/Class;

.field public final b:Landroid/os/Bundle;

.field public final c:Landroid/net/Uri;

.field public final d:Ljava/lang/CharSequence;

.field public final e:I

.field public final f:I

.field public final g:Z

.field public final h:Ljava/lang/String;

.field public final i:I

.field public j:Z

.field public k:I

.field public final l:Z

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/ref/WeakReference;


# direct methods
.method private constructor <init>(Landroid/net/Uri;Ljava/lang/Class;Landroid/os/Bundle;Ljava/lang/CharSequence;IZLjava/lang/String;IZZ)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lhb;->c:Landroid/net/Uri;

    iput-object p2, p0, Lhb;->a:Ljava/lang/Class;

    iput-object p3, p0, Lhb;->b:Landroid/os/Bundle;

    iput-object p4, p0, Lhb;->d:Ljava/lang/CharSequence;

    iput p5, p0, Lhb;->e:I

    invoke-virtual {p1}, Landroid/net/Uri;->hashCode()I

    move-result v0

    iput v0, p0, Lhb;->f:I

    iput-boolean p6, p0, Lhb;->g:Z

    iput-object p7, p0, Lhb;->h:Ljava/lang/String;

    iput p8, p0, Lhb;->i:I

    iput-boolean p9, p0, Lhb;->j:Z

    iput-boolean p10, p0, Lhb;->l:Z

    return-void
.end method

.method synthetic constructor <init>(Landroid/net/Uri;Ljava/lang/Class;Landroid/os/Bundle;Ljava/lang/CharSequence;IZLjava/lang/String;IZZLhc;)V
    .locals 0

    invoke-direct/range {p0 .. p10}, Lhb;-><init>(Landroid/net/Uri;Ljava/lang/Class;Landroid/os/Bundle;Ljava/lang/CharSequence;IZLjava/lang/String;IZZ)V

    return-void
.end method


# virtual methods
.method public a(Landroid/support/v4/app/FragmentManager;)Landroid/support/v4/app/Fragment;
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lhb;->n:Ljava/lang/ref/WeakReference;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lhb;->n:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    if-nez v0, :cond_0

    iget-object v0, p0, Lhb;->m:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v1, p0, Lhb;->n:Ljava/lang/ref/WeakReference;

    :cond_0
    return-object v0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lhb;->m:Ljava/lang/String;

    return-object v0
.end method

.method public a(Landroid/support/v4/app/Fragment;)V
    .locals 1

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lhb;->n:Ljava/lang/ref/WeakReference;

    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->getTag()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhb;->m:Ljava/lang/String;

    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-ne p0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-eq v0, v1, :cond_2

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lhb;->c:Landroid/net/Uri;

    check-cast p1, Lhb;

    iget-object v1, p1, Lhb;->c:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lhb;->c:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->hashCode()I

    move-result v0

    return v0
.end method
