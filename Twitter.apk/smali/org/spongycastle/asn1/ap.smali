.class public Lorg/spongycastle/asn1/ap;
.super Lorg/spongycastle/asn1/q;


# static fields
.field public static final a:Lorg/spongycastle/asn1/b;

.field public static final b:Lorg/spongycastle/asn1/b;

.field private static final c:[B

.field private static final d:[B


# instance fields
.field private e:[B


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-array v0, v3, [B

    const/4 v1, -0x1

    aput-byte v1, v0, v2

    sput-object v0, Lorg/spongycastle/asn1/ap;->c:[B

    new-array v0, v3, [B

    aput-byte v2, v0, v2

    sput-object v0, Lorg/spongycastle/asn1/ap;->d:[B

    new-instance v0, Lorg/spongycastle/asn1/b;

    invoke-direct {v0, v2}, Lorg/spongycastle/asn1/b;-><init>(Z)V

    sput-object v0, Lorg/spongycastle/asn1/ap;->a:Lorg/spongycastle/asn1/b;

    new-instance v0, Lorg/spongycastle/asn1/b;

    invoke-direct {v0, v3}, Lorg/spongycastle/asn1/b;-><init>(Z)V

    sput-object v0, Lorg/spongycastle/asn1/ap;->b:Lorg/spongycastle/asn1/b;

    return-void
.end method

.method public constructor <init>(Z)V
    .locals 1

    invoke-direct {p0}, Lorg/spongycastle/asn1/q;-><init>()V

    if-eqz p1, :cond_0

    sget-object v0, Lorg/spongycastle/asn1/ap;->c:[B

    :goto_0
    iput-object v0, p0, Lorg/spongycastle/asn1/ap;->e:[B

    return-void

    :cond_0
    sget-object v0, Lorg/spongycastle/asn1/ap;->d:[B

    goto :goto_0
.end method

.method constructor <init>([B)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Lorg/spongycastle/asn1/q;-><init>()V

    array-length v0, p1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "byte value should have 1 byte in it"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    aget-byte v0, p1, v2

    if-nez v0, :cond_1

    sget-object v0, Lorg/spongycastle/asn1/ap;->d:[B

    iput-object v0, p0, Lorg/spongycastle/asn1/ap;->e:[B

    :goto_0
    return-void

    :cond_1
    aget-byte v0, p1, v2

    const/16 v1, 0xff

    if-ne v0, v1, :cond_2

    sget-object v0, Lorg/spongycastle/asn1/ap;->c:[B

    iput-object v0, p0, Lorg/spongycastle/asn1/ap;->e:[B

    goto :goto_0

    :cond_2
    invoke-static {p1}, Lmo;->b([B)[B

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/ap;->e:[B

    goto :goto_0
.end method

.method static b([B)Lorg/spongycastle/asn1/b;
    .locals 3

    const/4 v2, 0x0

    array-length v0, p0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "byte value should have 1 byte in it"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    aget-byte v0, p0, v2

    if-nez v0, :cond_1

    sget-object v0, Lorg/spongycastle/asn1/ap;->a:Lorg/spongycastle/asn1/b;

    :goto_0
    return-object v0

    :cond_1
    aget-byte v0, p0, v2

    const/16 v1, 0xff

    if-ne v0, v1, :cond_2

    sget-object v0, Lorg/spongycastle/asn1/ap;->b:Lorg/spongycastle/asn1/b;

    goto :goto_0

    :cond_2
    new-instance v0, Lorg/spongycastle/asn1/b;

    invoke-direct {v0, p0}, Lorg/spongycastle/asn1/b;-><init>([B)V

    goto :goto_0
.end method


# virtual methods
.method a(Lorg/spongycastle/asn1/o;)V
    .locals 2

    const/4 v0, 0x1

    iget-object v1, p0, Lorg/spongycastle/asn1/ap;->e:[B

    invoke-virtual {p1, v0, v1}, Lorg/spongycastle/asn1/o;->a(I[B)V

    return-void
.end method

.method protected a(Lorg/spongycastle/asn1/q;)Z
    .locals 3

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    instance-of v1, p1, Lorg/spongycastle/asn1/ap;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lorg/spongycastle/asn1/ap;->e:[B

    aget-byte v1, v1, v0

    check-cast p1, Lorg/spongycastle/asn1/ap;

    iget-object v2, p1, Lorg/spongycastle/asn1/ap;->e:[B

    aget-byte v2, v2, v0

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method h()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public hashCode()I
    .locals 2

    iget-object v0, p0, Lorg/spongycastle/asn1/ap;->e:[B

    const/4 v1, 0x0

    aget-byte v0, v0, v1

    return v0
.end method

.method i()I
    .locals 1

    const/4 v0, 0x3

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lorg/spongycastle/asn1/ap;->e:[B

    const/4 v1, 0x0

    aget-byte v0, v0, v1

    if-eqz v0, :cond_0

    const-string/jumbo v0, "TRUE"

    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v0, "FALSE"

    goto :goto_0
.end method
