.class public Lorg/spongycastle/asn1/bg;
.super Ljava/lang/Object;

# interfaces
.implements Lorg/spongycastle/asn1/s;


# instance fields
.field private a:Lorg/spongycastle/asn1/v;


# direct methods
.method constructor <init>(Lorg/spongycastle/asn1/v;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/spongycastle/asn1/bg;->a:Lorg/spongycastle/asn1/v;

    return-void
.end method


# virtual methods
.method public a()Lorg/spongycastle/asn1/q;
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lorg/spongycastle/asn1/bg;->e()Lorg/spongycastle/asn1/q;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public e()Lorg/spongycastle/asn1/q;
    .locals 2

    new-instance v0, Lorg/spongycastle/asn1/bf;

    iget-object v1, p0, Lorg/spongycastle/asn1/bg;->a:Lorg/spongycastle/asn1/v;

    invoke-virtual {v1}, Lorg/spongycastle/asn1/v;->b()Lorg/spongycastle/asn1/e;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/bf;-><init>(Lorg/spongycastle/asn1/e;)V

    return-object v0
.end method
