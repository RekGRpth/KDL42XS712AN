.class public Lorg/spongycastle/asn1/ar;
.super Lorg/spongycastle/asn1/q;


# instance fields
.field private a:Lorg/spongycastle/asn1/l;

.field private b:Lorg/spongycastle/asn1/i;

.field private c:Lorg/spongycastle/asn1/q;

.field private d:I

.field private e:Lorg/spongycastle/asn1/q;


# direct methods
.method public constructor <init>(Lorg/spongycastle/asn1/e;)V
    .locals 3

    invoke-direct {p0}, Lorg/spongycastle/asn1/q;-><init>()V

    const/4 v1, 0x0

    invoke-direct {p0, p1, v1}, Lorg/spongycastle/asn1/ar;->a(Lorg/spongycastle/asn1/e;I)Lorg/spongycastle/asn1/q;

    move-result-object v0

    instance-of v2, v0, Lorg/spongycastle/asn1/l;

    if-eqz v2, :cond_0

    check-cast v0, Lorg/spongycastle/asn1/l;

    iput-object v0, p0, Lorg/spongycastle/asn1/ar;->a:Lorg/spongycastle/asn1/l;

    const/4 v1, 0x1

    invoke-direct {p0, p1, v1}, Lorg/spongycastle/asn1/ar;->a(Lorg/spongycastle/asn1/e;I)Lorg/spongycastle/asn1/q;

    move-result-object v0

    :cond_0
    instance-of v2, v0, Lorg/spongycastle/asn1/i;

    if-eqz v2, :cond_1

    check-cast v0, Lorg/spongycastle/asn1/i;

    iput-object v0, p0, Lorg/spongycastle/asn1/ar;->b:Lorg/spongycastle/asn1/i;

    add-int/lit8 v1, v1, 0x1

    invoke-direct {p0, p1, v1}, Lorg/spongycastle/asn1/ar;->a(Lorg/spongycastle/asn1/e;I)Lorg/spongycastle/asn1/q;

    move-result-object v0

    :cond_1
    instance-of v2, v0, Lorg/spongycastle/asn1/bk;

    if-nez v2, :cond_2

    iput-object v0, p0, Lorg/spongycastle/asn1/ar;->c:Lorg/spongycastle/asn1/q;

    add-int/lit8 v1, v1, 0x1

    invoke-direct {p0, p1, v1}, Lorg/spongycastle/asn1/ar;->a(Lorg/spongycastle/asn1/e;I)Lorg/spongycastle/asn1/q;

    move-result-object v0

    :cond_2
    invoke-virtual {p1}, Lorg/spongycastle/asn1/e;->a()I

    move-result v2

    add-int/lit8 v1, v1, 0x1

    if-eq v2, v1, :cond_3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "input vector too large"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    instance-of v1, v0, Lorg/spongycastle/asn1/bk;

    if-nez v1, :cond_4

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "No tagged object found in vector. Structure doesn\'t seem to be of type External"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    check-cast v0, Lorg/spongycastle/asn1/bk;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/bk;->c()I

    move-result v1

    invoke-direct {p0, v1}, Lorg/spongycastle/asn1/ar;->a(I)V

    invoke-virtual {v0}, Lorg/spongycastle/asn1/bk;->j()Lorg/spongycastle/asn1/q;

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/ar;->e:Lorg/spongycastle/asn1/q;

    return-void
.end method

.method private a(Lorg/spongycastle/asn1/e;I)Lorg/spongycastle/asn1/q;
    .locals 2

    invoke-virtual {p1}, Lorg/spongycastle/asn1/e;->a()I

    move-result v0

    if-gt v0, p2, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "too few objects in input vector"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {p1, p2}, Lorg/spongycastle/asn1/e;->a(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    invoke-interface {v0}, Lorg/spongycastle/asn1/d;->a()Lorg/spongycastle/asn1/q;

    move-result-object v0

    return-object v0
.end method

.method private a(I)V
    .locals 3

    if-ltz p1, :cond_0

    const/4 v0, 0x2

    if-le p1, v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "invalid encoding value: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iput p1, p0, Lorg/spongycastle/asn1/ar;->d:I

    return-void
.end method


# virtual methods
.method a(Lorg/spongycastle/asn1/o;)V
    .locals 5

    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    iget-object v1, p0, Lorg/spongycastle/asn1/ar;->a:Lorg/spongycastle/asn1/l;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lorg/spongycastle/asn1/ar;->a:Lorg/spongycastle/asn1/l;

    const-string/jumbo v2, "DER"

    invoke-virtual {v1, v2}, Lorg/spongycastle/asn1/l;->a(Ljava/lang/String;)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/ByteArrayOutputStream;->write([B)V

    :cond_0
    iget-object v1, p0, Lorg/spongycastle/asn1/ar;->b:Lorg/spongycastle/asn1/i;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lorg/spongycastle/asn1/ar;->b:Lorg/spongycastle/asn1/i;

    const-string/jumbo v2, "DER"

    invoke-virtual {v1, v2}, Lorg/spongycastle/asn1/i;->a(Ljava/lang/String;)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/ByteArrayOutputStream;->write([B)V

    :cond_1
    iget-object v1, p0, Lorg/spongycastle/asn1/ar;->c:Lorg/spongycastle/asn1/q;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lorg/spongycastle/asn1/ar;->c:Lorg/spongycastle/asn1/q;

    const-string/jumbo v2, "DER"

    invoke-virtual {v1, v2}, Lorg/spongycastle/asn1/q;->a(Ljava/lang/String;)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/ByteArrayOutputStream;->write([B)V

    :cond_2
    new-instance v1, Lorg/spongycastle/asn1/bk;

    const/4 v2, 0x1

    iget v3, p0, Lorg/spongycastle/asn1/ar;->d:I

    iget-object v4, p0, Lorg/spongycastle/asn1/ar;->e:Lorg/spongycastle/asn1/q;

    invoke-direct {v1, v2, v3, v4}, Lorg/spongycastle/asn1/bk;-><init>(ZILorg/spongycastle/asn1/d;)V

    const-string/jumbo v2, "DER"

    invoke-virtual {v1, v2}, Lorg/spongycastle/asn1/bk;->a(Ljava/lang/String;)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/ByteArrayOutputStream;->write([B)V

    const/16 v1, 0x20

    const/16 v2, 0x8

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    invoke-virtual {p1, v1, v2, v0}, Lorg/spongycastle/asn1/o;->a(II[B)V

    return-void
.end method

.method a(Lorg/spongycastle/asn1/q;)Z
    .locals 3

    const/4 v0, 0x0

    instance-of v1, p1, Lorg/spongycastle/asn1/ar;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-ne p0, p1, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    check-cast p1, Lorg/spongycastle/asn1/ar;

    iget-object v1, p0, Lorg/spongycastle/asn1/ar;->a:Lorg/spongycastle/asn1/l;

    if-eqz v1, :cond_3

    iget-object v1, p1, Lorg/spongycastle/asn1/ar;->a:Lorg/spongycastle/asn1/l;

    if-eqz v1, :cond_0

    iget-object v1, p1, Lorg/spongycastle/asn1/ar;->a:Lorg/spongycastle/asn1/l;

    iget-object v2, p0, Lorg/spongycastle/asn1/ar;->a:Lorg/spongycastle/asn1/l;

    invoke-virtual {v1, v2}, Lorg/spongycastle/asn1/l;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_3
    iget-object v1, p0, Lorg/spongycastle/asn1/ar;->b:Lorg/spongycastle/asn1/i;

    if-eqz v1, :cond_4

    iget-object v1, p1, Lorg/spongycastle/asn1/ar;->b:Lorg/spongycastle/asn1/i;

    if-eqz v1, :cond_0

    iget-object v1, p1, Lorg/spongycastle/asn1/ar;->b:Lorg/spongycastle/asn1/i;

    iget-object v2, p0, Lorg/spongycastle/asn1/ar;->b:Lorg/spongycastle/asn1/i;

    invoke-virtual {v1, v2}, Lorg/spongycastle/asn1/i;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_4
    iget-object v1, p0, Lorg/spongycastle/asn1/ar;->c:Lorg/spongycastle/asn1/q;

    if-eqz v1, :cond_5

    iget-object v1, p1, Lorg/spongycastle/asn1/ar;->c:Lorg/spongycastle/asn1/q;

    if-eqz v1, :cond_0

    iget-object v1, p1, Lorg/spongycastle/asn1/ar;->c:Lorg/spongycastle/asn1/q;

    iget-object v2, p0, Lorg/spongycastle/asn1/ar;->c:Lorg/spongycastle/asn1/q;

    invoke-virtual {v1, v2}, Lorg/spongycastle/asn1/q;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_5
    iget-object v0, p0, Lorg/spongycastle/asn1/ar;->e:Lorg/spongycastle/asn1/q;

    iget-object v1, p1, Lorg/spongycastle/asn1/ar;->e:Lorg/spongycastle/asn1/q;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/q;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method h()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public hashCode()I
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lorg/spongycastle/asn1/ar;->a:Lorg/spongycastle/asn1/l;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lorg/spongycastle/asn1/ar;->a:Lorg/spongycastle/asn1/l;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/l;->hashCode()I

    move-result v0

    :cond_0
    iget-object v1, p0, Lorg/spongycastle/asn1/ar;->b:Lorg/spongycastle/asn1/i;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lorg/spongycastle/asn1/ar;->b:Lorg/spongycastle/asn1/i;

    invoke-virtual {v1}, Lorg/spongycastle/asn1/i;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    :cond_1
    iget-object v1, p0, Lorg/spongycastle/asn1/ar;->c:Lorg/spongycastle/asn1/q;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lorg/spongycastle/asn1/ar;->c:Lorg/spongycastle/asn1/q;

    invoke-virtual {v1}, Lorg/spongycastle/asn1/q;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    :cond_2
    iget-object v1, p0, Lorg/spongycastle/asn1/ar;->e:Lorg/spongycastle/asn1/q;

    invoke-virtual {v1}, Lorg/spongycastle/asn1/q;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    return v0
.end method

.method i()I
    .locals 1

    invoke-virtual {p0}, Lorg/spongycastle/asn1/ar;->b()[B

    move-result-object v0

    array-length v0, v0

    return v0
.end method
