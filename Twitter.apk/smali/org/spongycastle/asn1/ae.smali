.class public Lorg/spongycastle/asn1/ae;
.super Ljava/lang/Object;

# interfaces
.implements Lorg/spongycastle/asn1/n;


# instance fields
.field private a:Lorg/spongycastle/asn1/v;


# direct methods
.method constructor <init>(Lorg/spongycastle/asn1/v;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/spongycastle/asn1/ae;->a:Lorg/spongycastle/asn1/v;

    return-void
.end method


# virtual methods
.method public a()Lorg/spongycastle/asn1/q;
    .locals 4

    :try_start_0
    invoke-virtual {p0}, Lorg/spongycastle/asn1/ae;->e()Lorg/spongycastle/asn1/q;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Lorg/spongycastle/asn1/ASN1ParsingException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "IOException converting stream to byte array: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lorg/spongycastle/asn1/ASN1ParsingException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public c()Ljava/io/InputStream;
    .locals 2

    new-instance v0, Lorg/spongycastle/asn1/al;

    iget-object v1, p0, Lorg/spongycastle/asn1/ae;->a:Lorg/spongycastle/asn1/v;

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/al;-><init>(Lorg/spongycastle/asn1/v;)V

    return-object v0
.end method

.method public e()Lorg/spongycastle/asn1/q;
    .locals 2

    new-instance v0, Lorg/spongycastle/asn1/ac;

    invoke-virtual {p0}, Lorg/spongycastle/asn1/ae;->c()Ljava/io/InputStream;

    move-result-object v1

    invoke-static {v1}, Lmu;->a(Ljava/io/InputStream;)[B

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/ac;-><init>([B)V

    return-object v0
.end method
