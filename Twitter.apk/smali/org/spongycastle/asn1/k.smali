.class public abstract Lorg/spongycastle/asn1/k;
.super Ljava/lang/Object;

# interfaces
.implements Lorg/spongycastle/asn1/d;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract a()Lorg/spongycastle/asn1/q;
.end method

.method public a(Ljava/lang/String;)[B
    .locals 2

    const-string/jumbo v0, "DER"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v1, Lorg/spongycastle/asn1/bd;

    invoke-direct {v1, v0}, Lorg/spongycastle/asn1/bd;-><init>(Ljava/io/OutputStream;)V

    invoke-virtual {v1, p0}, Lorg/spongycastle/asn1/bd;->a(Lorg/spongycastle/asn1/d;)V

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v0, "DL"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v1, Lorg/spongycastle/asn1/bp;

    invoke-direct {v1, v0}, Lorg/spongycastle/asn1/bp;-><init>(Ljava/io/OutputStream;)V

    invoke-virtual {v1, p0}, Lorg/spongycastle/asn1/bp;->a(Lorg/spongycastle/asn1/d;)V

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lorg/spongycastle/asn1/k;->b()[B

    move-result-object v0

    goto :goto_0
.end method

.method public b()[B
    .locals 2

    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v1, Lorg/spongycastle/asn1/o;

    invoke-direct {v1, v0}, Lorg/spongycastle/asn1/o;-><init>(Ljava/io/OutputStream;)V

    invoke-virtual {v1, p0}, Lorg/spongycastle/asn1/o;->a(Lorg/spongycastle/asn1/d;)V

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-ne p0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    instance-of v0, p1, Lorg/spongycastle/asn1/d;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    check-cast p1, Lorg/spongycastle/asn1/d;

    invoke-virtual {p0}, Lorg/spongycastle/asn1/k;->a()Lorg/spongycastle/asn1/q;

    move-result-object v0

    invoke-interface {p1}, Lorg/spongycastle/asn1/d;->a()Lorg/spongycastle/asn1/q;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/q;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    invoke-virtual {p0}, Lorg/spongycastle/asn1/k;->a()Lorg/spongycastle/asn1/q;

    move-result-object v0

    invoke-virtual {v0}, Lorg/spongycastle/asn1/q;->hashCode()I

    move-result v0

    return v0
.end method
