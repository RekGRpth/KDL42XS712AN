.class public Lorg/spongycastle/asn1/bf;
.super Lorg/spongycastle/asn1/r;


# instance fields
.field private b:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lorg/spongycastle/asn1/r;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lorg/spongycastle/asn1/bf;->b:I

    return-void
.end method

.method public constructor <init>(Lorg/spongycastle/asn1/e;)V
    .locals 1

    invoke-direct {p0, p1}, Lorg/spongycastle/asn1/r;-><init>(Lorg/spongycastle/asn1/e;)V

    const/4 v0, -0x1

    iput v0, p0, Lorg/spongycastle/asn1/bf;->b:I

    return-void
.end method

.method private j()I
    .locals 3

    iget v0, p0, Lorg/spongycastle/asn1/bf;->b:I

    if-gez v0, :cond_1

    const/4 v0, 0x0

    invoke-virtual {p0}, Lorg/spongycastle/asn1/bf;->d()Ljava/util/Enumeration;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/d;

    invoke-interface {v0}, Lorg/spongycastle/asn1/d;->a()Lorg/spongycastle/asn1/q;

    move-result-object v0

    invoke-virtual {v0}, Lorg/spongycastle/asn1/q;->f()Lorg/spongycastle/asn1/q;

    move-result-object v0

    invoke-virtual {v0}, Lorg/spongycastle/asn1/q;->i()I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    goto :goto_0

    :cond_0
    iput v1, p0, Lorg/spongycastle/asn1/bf;->b:I

    :cond_1
    iget v0, p0, Lorg/spongycastle/asn1/bf;->b:I

    return v0
.end method


# virtual methods
.method a(Lorg/spongycastle/asn1/o;)V
    .locals 3

    invoke-virtual {p1}, Lorg/spongycastle/asn1/o;->a()Lorg/spongycastle/asn1/o;

    move-result-object v1

    invoke-direct {p0}, Lorg/spongycastle/asn1/bf;->j()I

    move-result v0

    const/16 v2, 0x30

    invoke-virtual {p1, v2}, Lorg/spongycastle/asn1/o;->b(I)V

    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/o;->a(I)V

    invoke-virtual {p0}, Lorg/spongycastle/asn1/bf;->d()Ljava/util/Enumeration;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/d;

    invoke-virtual {v1, v0}, Lorg/spongycastle/asn1/o;->a(Lorg/spongycastle/asn1/d;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method i()I
    .locals 2

    invoke-direct {p0}, Lorg/spongycastle/asn1/bf;->j()I

    move-result v0

    invoke-static {v0}, Lorg/spongycastle/asn1/ca;->a(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    return v0
.end method
