.class public Lorg/spongycastle/asn1/bs;
.super Lorg/spongycastle/asn1/w;


# static fields
.field private static final e:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [B

    sput-object v0, Lorg/spongycastle/asn1/bs;->e:[B

    return-void
.end method

.method public constructor <init>(ZILorg/spongycastle/asn1/d;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lorg/spongycastle/asn1/w;-><init>(ZILorg/spongycastle/asn1/d;)V

    return-void
.end method


# virtual methods
.method a(Lorg/spongycastle/asn1/o;)V
    .locals 3

    const/16 v0, 0xa0

    iget-boolean v1, p0, Lorg/spongycastle/asn1/bs;->b:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lorg/spongycastle/asn1/bs;->d:Lorg/spongycastle/asn1/d;

    invoke-interface {v1}, Lorg/spongycastle/asn1/d;->a()Lorg/spongycastle/asn1/q;

    move-result-object v1

    invoke-virtual {v1}, Lorg/spongycastle/asn1/q;->g()Lorg/spongycastle/asn1/q;

    move-result-object v1

    iget-boolean v2, p0, Lorg/spongycastle/asn1/bs;->c:Z

    if-eqz v2, :cond_0

    iget v2, p0, Lorg/spongycastle/asn1/bs;->a:I

    invoke-virtual {p1, v0, v2}, Lorg/spongycastle/asn1/o;->a(II)V

    invoke-virtual {v1}, Lorg/spongycastle/asn1/q;->i()I

    move-result v0

    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/o;->a(I)V

    invoke-virtual {p1, v1}, Lorg/spongycastle/asn1/o;->a(Lorg/spongycastle/asn1/d;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v1}, Lorg/spongycastle/asn1/q;->h()Z

    move-result v2

    if-eqz v2, :cond_1

    :goto_1
    iget v2, p0, Lorg/spongycastle/asn1/bs;->a:I

    invoke-virtual {p1, v0, v2}, Lorg/spongycastle/asn1/o;->a(II)V

    invoke-virtual {p1, v1}, Lorg/spongycastle/asn1/o;->a(Lorg/spongycastle/asn1/q;)V

    goto :goto_0

    :cond_1
    const/16 v0, 0x80

    goto :goto_1

    :cond_2
    iget v1, p0, Lorg/spongycastle/asn1/bs;->a:I

    sget-object v2, Lorg/spongycastle/asn1/bs;->e:[B

    invoke-virtual {p1, v0, v1, v2}, Lorg/spongycastle/asn1/o;->a(II[B)V

    goto :goto_0
.end method

.method h()Z
    .locals 2

    const/4 v0, 0x1

    iget-boolean v1, p0, Lorg/spongycastle/asn1/bs;->b:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lorg/spongycastle/asn1/bs;->c:Z

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lorg/spongycastle/asn1/bs;->d:Lorg/spongycastle/asn1/d;

    invoke-interface {v0}, Lorg/spongycastle/asn1/d;->a()Lorg/spongycastle/asn1/q;

    move-result-object v0

    invoke-virtual {v0}, Lorg/spongycastle/asn1/q;->g()Lorg/spongycastle/asn1/q;

    move-result-object v0

    invoke-virtual {v0}, Lorg/spongycastle/asn1/q;->h()Z

    move-result v0

    goto :goto_0
.end method

.method i()I
    .locals 3

    iget-boolean v0, p0, Lorg/spongycastle/asn1/bs;->b:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lorg/spongycastle/asn1/bs;->d:Lorg/spongycastle/asn1/d;

    invoke-interface {v0}, Lorg/spongycastle/asn1/d;->a()Lorg/spongycastle/asn1/q;

    move-result-object v0

    invoke-virtual {v0}, Lorg/spongycastle/asn1/q;->g()Lorg/spongycastle/asn1/q;

    move-result-object v0

    invoke-virtual {v0}, Lorg/spongycastle/asn1/q;->i()I

    move-result v0

    iget-boolean v1, p0, Lorg/spongycastle/asn1/bs;->c:Z

    if-eqz v1, :cond_0

    iget v1, p0, Lorg/spongycastle/asn1/bs;->a:I

    invoke-static {v1}, Lorg/spongycastle/asn1/ca;->b(I)I

    move-result v1

    invoke-static {v0}, Lorg/spongycastle/asn1/ca;->a(I)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    :goto_0
    return v0

    :cond_0
    add-int/lit8 v0, v0, -0x1

    iget v1, p0, Lorg/spongycastle/asn1/bs;->a:I

    invoke-static {v1}, Lorg/spongycastle/asn1/ca;->b(I)I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_0

    :cond_1
    iget v0, p0, Lorg/spongycastle/asn1/bs;->a:I

    invoke-static {v0}, Lorg/spongycastle/asn1/ca;->b(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
