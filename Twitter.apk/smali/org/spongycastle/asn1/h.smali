.class public Lorg/spongycastle/asn1/h;
.super Ljava/io/FilterInputStream;


# instance fields
.field private final a:I

.field private final b:Z

.field private final c:[[B


# direct methods
.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 1

    invoke-static {p1}, Lorg/spongycastle/asn1/ca;->a(Ljava/io/InputStream;)I

    move-result v0

    invoke-direct {p0, p1, v0}, Lorg/spongycastle/asn1/h;-><init>(Ljava/io/InputStream;I)V

    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;I)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lorg/spongycastle/asn1/h;-><init>(Ljava/io/InputStream;IZ)V

    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;IZ)V
    .locals 1

    invoke-direct {p0, p1}, Ljava/io/FilterInputStream;-><init>(Ljava/io/InputStream;)V

    iput p2, p0, Lorg/spongycastle/asn1/h;->a:I

    iput-boolean p3, p0, Lorg/spongycastle/asn1/h;->b:Z

    const/16 v0, 0xb

    new-array v0, v0, [[B

    iput-object v0, p0, Lorg/spongycastle/asn1/h;->c:[[B

    return-void
.end method

.method public constructor <init>([B)V
    .locals 2

    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-direct {v0, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    array-length v1, p1

    invoke-direct {p0, v0, v1}, Lorg/spongycastle/asn1/h;-><init>(Ljava/io/InputStream;I)V

    return-void
.end method

.method public constructor <init>([BZ)V
    .locals 2

    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-direct {v0, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    array-length v1, p1

    invoke-direct {p0, v0, v1, p2}, Lorg/spongycastle/asn1/h;-><init>(Ljava/io/InputStream;IZ)V

    return-void
.end method

.method static a(Ljava/io/InputStream;I)I
    .locals 3

    and-int/lit8 v0, p1, 0x1f

    const/16 v1, 0x1f

    if-ne v0, v1, :cond_3

    const/4 v1, 0x0

    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    move-result v0

    and-int/lit8 v2, v0, 0x7f

    if-nez v2, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v1, "corrupted stream - invalid high tag number found"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    :goto_0
    if-ltz v0, :cond_1

    and-int/lit16 v2, v0, 0x80

    if-eqz v2, :cond_1

    and-int/lit8 v0, v0, 0x7f

    or-int/2addr v0, v1

    shl-int/lit8 v1, v0, 0x7

    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    move-result v0

    goto :goto_0

    :cond_1
    if-gez v0, :cond_2

    new-instance v0, Ljava/io/EOFException;

    const-string/jumbo v1, "EOF found inside tag value."

    invoke-direct {v0, v1}, Ljava/io/EOFException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    and-int/lit8 v0, v0, 0x7f

    or-int/2addr v0, v1

    :cond_3
    return v0
.end method

.method static a(ILorg/spongycastle/asn1/bt;[[B)Lorg/spongycastle/asn1/q;
    .locals 3

    packed-switch p0, :pswitch_data_0

    :pswitch_0
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "unknown tag "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " encountered"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_1
    invoke-virtual {p1}, Lorg/spongycastle/asn1/bt;->a()I

    move-result v0

    invoke-static {v0, p1}, Lorg/spongycastle/asn1/ao;->a(ILjava/io/InputStream;)Lorg/spongycastle/asn1/ao;

    move-result-object v0

    :goto_0
    return-object v0

    :pswitch_2
    new-instance v0, Lorg/spongycastle/asn1/an;

    invoke-static {p1}, Lorg/spongycastle/asn1/h;->b(Lorg/spongycastle/asn1/bt;)[C

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/an;-><init>([C)V

    goto :goto_0

    :pswitch_3
    invoke-static {p1, p2}, Lorg/spongycastle/asn1/h;->a(Lorg/spongycastle/asn1/bt;[[B)[B

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/b;->b([B)Lorg/spongycastle/asn1/b;

    move-result-object v0

    goto :goto_0

    :pswitch_4
    invoke-static {p1, p2}, Lorg/spongycastle/asn1/h;->a(Lorg/spongycastle/asn1/bt;[[B)[B

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/f;->b([B)Lorg/spongycastle/asn1/f;

    move-result-object v0

    goto :goto_0

    :pswitch_5
    new-instance v0, Lorg/spongycastle/asn1/g;

    invoke-virtual {p1}, Lorg/spongycastle/asn1/bt;->b()[B

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/g;-><init>([B)V

    goto :goto_0

    :pswitch_6
    new-instance v0, Lorg/spongycastle/asn1/au;

    invoke-virtual {p1}, Lorg/spongycastle/asn1/bt;->b()[B

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/au;-><init>([B)V

    goto :goto_0

    :pswitch_7
    new-instance v0, Lorg/spongycastle/asn1/aw;

    invoke-virtual {p1}, Lorg/spongycastle/asn1/bt;->b()[B

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/aw;-><init>([B)V

    goto :goto_0

    :pswitch_8
    new-instance v0, Lorg/spongycastle/asn1/i;

    invoke-virtual {p1}, Lorg/spongycastle/asn1/bt;->b()[B

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/i;-><init>([B)V

    goto :goto_0

    :pswitch_9
    sget-object v0, Lorg/spongycastle/asn1/ay;->a:Lorg/spongycastle/asn1/ay;

    goto :goto_0

    :pswitch_a
    new-instance v0, Lorg/spongycastle/asn1/az;

    invoke-virtual {p1}, Lorg/spongycastle/asn1/bt;->b()[B

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/az;-><init>([B)V

    goto :goto_0

    :pswitch_b
    invoke-static {p1, p2}, Lorg/spongycastle/asn1/h;->a(Lorg/spongycastle/asn1/bt;[[B)[B

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/l;->b([B)Lorg/spongycastle/asn1/l;

    move-result-object v0

    goto :goto_0

    :pswitch_c
    new-instance v0, Lorg/spongycastle/asn1/bb;

    invoke-virtual {p1}, Lorg/spongycastle/asn1/bt;->b()[B

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/bb;-><init>([B)V

    goto :goto_0

    :pswitch_d
    new-instance v0, Lorg/spongycastle/asn1/be;

    invoke-virtual {p1}, Lorg/spongycastle/asn1/bt;->b()[B

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/be;-><init>([B)V

    goto :goto_0

    :pswitch_e
    new-instance v0, Lorg/spongycastle/asn1/bj;

    invoke-virtual {p1}, Lorg/spongycastle/asn1/bt;->b()[B

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/bj;-><init>([B)V

    goto :goto_0

    :pswitch_f
    new-instance v0, Lorg/spongycastle/asn1/bn;

    invoke-virtual {p1}, Lorg/spongycastle/asn1/bt;->b()[B

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/bn;-><init>([B)V

    goto/16 :goto_0

    :pswitch_10
    new-instance v0, Lorg/spongycastle/asn1/y;

    invoke-virtual {p1}, Lorg/spongycastle/asn1/bt;->b()[B

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/y;-><init>([B)V

    goto/16 :goto_0

    :pswitch_11
    new-instance v0, Lorg/spongycastle/asn1/bm;

    invoke-virtual {p1}, Lorg/spongycastle/asn1/bt;->b()[B

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/bm;-><init>([B)V

    goto/16 :goto_0

    :pswitch_12
    new-instance v0, Lorg/spongycastle/asn1/bo;

    invoke-virtual {p1}, Lorg/spongycastle/asn1/bt;->b()[B

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/bo;-><init>([B)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_8
        :pswitch_1
        :pswitch_c
        :pswitch_9
        :pswitch_b
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_11
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_a
        :pswitch_d
        :pswitch_e
        :pswitch_0
        :pswitch_7
        :pswitch_10
        :pswitch_5
        :pswitch_0
        :pswitch_12
        :pswitch_6
        :pswitch_f
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private static a(Lorg/spongycastle/asn1/bt;[[B)[B
    .locals 3

    invoke-virtual {p0}, Lorg/spongycastle/asn1/bt;->a()I

    move-result v1

    invoke-virtual {p0}, Lorg/spongycastle/asn1/bt;->a()I

    move-result v0

    array-length v2, p1

    if-ge v0, v2, :cond_1

    aget-object v0, p1, v1

    if-nez v0, :cond_0

    new-array v0, v1, [B

    aput-object v0, p1, v1

    :cond_0
    invoke-static {p0, v0}, Lmu;->a(Ljava/io/InputStream;[B)I

    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0}, Lorg/spongycastle/asn1/bt;->b()[B

    move-result-object v0

    goto :goto_0
.end method

.method static b(Ljava/io/InputStream;I)I
    .locals 4

    const/4 v1, 0x0

    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    move-result v0

    if-gez v0, :cond_0

    new-instance v0, Ljava/io/EOFException;

    const-string/jumbo v1, "EOF found when length expected"

    invoke-direct {v0, v1}, Ljava/io/EOFException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/16 v2, 0x80

    if-ne v0, v2, :cond_2

    const/4 v0, -0x1

    :cond_1
    return v0

    :cond_2
    const/16 v2, 0x7f

    if-le v0, v2, :cond_1

    and-int/lit8 v3, v0, 0x7f

    const/4 v0, 0x4

    if-le v3, v0, :cond_3

    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "DER length more than 4 bytes: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    move v0, v1

    :goto_0
    if-ge v1, v3, :cond_5

    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    move-result v2

    if-gez v2, :cond_4

    new-instance v0, Ljava/io/EOFException;

    const-string/jumbo v1, "EOF found reading length"

    invoke-direct {v0, v1}, Ljava/io/EOFException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    shl-int/lit8 v0, v0, 0x8

    add-int/2addr v2, v0

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v2

    goto :goto_0

    :cond_5
    if-gez v0, :cond_6

    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v1, "corrupted stream - negative length found"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    if-lt v0, p1, :cond_1

    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v1, "corrupted stream - out of bounds length found"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static b(Lorg/spongycastle/asn1/bt;)[C
    .locals 6

    invoke-virtual {p0}, Lorg/spongycastle/asn1/bt;->a()I

    move-result v0

    div-int/lit8 v2, v0, 0x2

    new-array v3, v2, [C

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    invoke-virtual {p0}, Lorg/spongycastle/asn1/bt;->read()I

    move-result v4

    if-gez v4, :cond_1

    :cond_0
    return-object v3

    :cond_1
    invoke-virtual {p0}, Lorg/spongycastle/asn1/bt;->read()I

    move-result v5

    if-ltz v5, :cond_0

    add-int/lit8 v1, v0, 0x1

    shl-int/lit8 v4, v4, 0x8

    and-int/lit16 v5, v5, 0xff

    or-int/2addr v4, v5

    int-to-char v4, v4

    aput-char v4, v3, v0

    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method a()I
    .locals 1

    iget v0, p0, Lorg/spongycastle/asn1/h;->a:I

    return v0
.end method

.method a(Lorg/spongycastle/asn1/bt;)Lorg/spongycastle/asn1/e;
    .locals 1

    new-instance v0, Lorg/spongycastle/asn1/h;

    invoke-direct {v0, p1}, Lorg/spongycastle/asn1/h;-><init>(Ljava/io/InputStream;)V

    invoke-virtual {v0}, Lorg/spongycastle/asn1/h;->c()Lorg/spongycastle/asn1/e;

    move-result-object v0

    return-object v0
.end method

.method protected a(III)Lorg/spongycastle/asn1/q;
    .locals 4

    const/4 v1, 0x0

    and-int/lit8 v0, p1, 0x20

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    new-instance v2, Lorg/spongycastle/asn1/bt;

    invoke-direct {v2, p0, p3}, Lorg/spongycastle/asn1/bt;-><init>(Ljava/io/InputStream;I)V

    and-int/lit8 v3, p1, 0x40

    if-eqz v3, :cond_1

    new-instance v1, Lorg/spongycastle/asn1/am;

    invoke-virtual {v2}, Lorg/spongycastle/asn1/bt;->b()[B

    move-result-object v2

    invoke-direct {v1, v0, p2, v2}, Lorg/spongycastle/asn1/am;-><init>(ZI[B)V

    move-object v0, v1

    :goto_1
    return-object v0

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    and-int/lit16 v3, p1, 0x80

    if-eqz v3, :cond_2

    new-instance v1, Lorg/spongycastle/asn1/v;

    invoke-direct {v1, v2}, Lorg/spongycastle/asn1/v;-><init>(Ljava/io/InputStream;)V

    invoke-virtual {v1, v0, p2}, Lorg/spongycastle/asn1/v;->a(ZI)Lorg/spongycastle/asn1/q;

    move-result-object v0

    goto :goto_1

    :cond_2
    if-eqz v0, :cond_5

    sparse-switch p2, :sswitch_data_0

    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "unknown tag "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " encountered"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :sswitch_0
    invoke-virtual {p0, v2}, Lorg/spongycastle/asn1/h;->a(Lorg/spongycastle/asn1/bt;)Lorg/spongycastle/asn1/e;

    move-result-object v2

    invoke-virtual {v2}, Lorg/spongycastle/asn1/e;->a()I

    move-result v0

    new-array v3, v0, [Lorg/spongycastle/asn1/m;

    :goto_2
    array-length v0, v3

    if-eq v1, v0, :cond_3

    invoke-virtual {v2, v1}, Lorg/spongycastle/asn1/e;->a(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/m;

    aput-object v0, v3, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_3
    new-instance v0, Lorg/spongycastle/asn1/ac;

    invoke-direct {v0, v3}, Lorg/spongycastle/asn1/ac;-><init>([Lorg/spongycastle/asn1/m;)V

    goto :goto_1

    :sswitch_1
    iget-boolean v0, p0, Lorg/spongycastle/asn1/h;->b:Z

    if-eqz v0, :cond_4

    new-instance v0, Lorg/spongycastle/asn1/bx;

    invoke-virtual {v2}, Lorg/spongycastle/asn1/bt;->b()[B

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/bx;-><init>([B)V

    goto :goto_1

    :cond_4
    invoke-virtual {p0, v2}, Lorg/spongycastle/asn1/h;->a(Lorg/spongycastle/asn1/bt;)Lorg/spongycastle/asn1/e;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/at;->a(Lorg/spongycastle/asn1/e;)Lorg/spongycastle/asn1/r;

    move-result-object v0

    goto :goto_1

    :sswitch_2
    invoke-virtual {p0, v2}, Lorg/spongycastle/asn1/h;->a(Lorg/spongycastle/asn1/bt;)Lorg/spongycastle/asn1/e;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/at;->b(Lorg/spongycastle/asn1/e;)Lorg/spongycastle/asn1/t;

    move-result-object v0

    goto :goto_1

    :sswitch_3
    new-instance v0, Lorg/spongycastle/asn1/ar;

    invoke-virtual {p0, v2}, Lorg/spongycastle/asn1/h;->a(Lorg/spongycastle/asn1/bt;)Lorg/spongycastle/asn1/e;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/ar;-><init>(Lorg/spongycastle/asn1/e;)V

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lorg/spongycastle/asn1/h;->c:[[B

    invoke-static {p2, v2, v0}, Lorg/spongycastle/asn1/h;->a(ILorg/spongycastle/asn1/bt;[[B)Lorg/spongycastle/asn1/q;

    move-result-object v0

    goto/16 :goto_1

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0x8 -> :sswitch_3
        0x10 -> :sswitch_1
        0x11 -> :sswitch_2
    .end sparse-switch
.end method

.method protected b()I
    .locals 1

    iget v0, p0, Lorg/spongycastle/asn1/h;->a:I

    invoke-static {p0, v0}, Lorg/spongycastle/asn1/h;->b(Ljava/io/InputStream;I)I

    move-result v0

    return v0
.end method

.method c()Lorg/spongycastle/asn1/e;
    .locals 2

    new-instance v0, Lorg/spongycastle/asn1/e;

    invoke-direct {v0}, Lorg/spongycastle/asn1/e;-><init>()V

    :goto_0
    invoke-virtual {p0}, Lorg/spongycastle/asn1/h;->d()Lorg/spongycastle/asn1/q;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public d()Lorg/spongycastle/asn1/q;
    .locals 6

    const/4 v1, 0x1

    invoke-virtual {p0}, Lorg/spongycastle/asn1/h;->read()I

    move-result v2

    if-gtz v2, :cond_1

    if-nez v2, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v1, "unexpected end-of-contents marker"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    invoke-static {p0, v2}, Lorg/spongycastle/asn1/h;->a(Ljava/io/InputStream;I)I

    move-result v3

    and-int/lit8 v0, v2, 0x20

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    invoke-virtual {p0}, Lorg/spongycastle/asn1/h;->b()I

    move-result v4

    if-gez v4, :cond_6

    if-nez v0, :cond_3

    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v1, "indefinite length primitive encoding encountered"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    new-instance v0, Lorg/spongycastle/asn1/bv;

    iget v4, p0, Lorg/spongycastle/asn1/h;->a:I

    invoke-direct {v0, p0, v4}, Lorg/spongycastle/asn1/bv;-><init>(Ljava/io/InputStream;I)V

    new-instance v4, Lorg/spongycastle/asn1/v;

    iget v5, p0, Lorg/spongycastle/asn1/h;->a:I

    invoke-direct {v4, v0, v5}, Lorg/spongycastle/asn1/v;-><init>(Ljava/io/InputStream;I)V

    and-int/lit8 v0, v2, 0x40

    if-eqz v0, :cond_4

    new-instance v0, Lorg/spongycastle/asn1/aa;

    invoke-direct {v0, v3, v4}, Lorg/spongycastle/asn1/aa;-><init>(ILorg/spongycastle/asn1/v;)V

    invoke-virtual {v0}, Lorg/spongycastle/asn1/aa;->e()Lorg/spongycastle/asn1/q;

    move-result-object v0

    goto :goto_0

    :cond_4
    and-int/lit16 v0, v2, 0x80

    if-eqz v0, :cond_5

    new-instance v0, Lorg/spongycastle/asn1/ak;

    invoke-direct {v0, v1, v3, v4}, Lorg/spongycastle/asn1/ak;-><init>(ZILorg/spongycastle/asn1/v;)V

    invoke-virtual {v0}, Lorg/spongycastle/asn1/ak;->e()Lorg/spongycastle/asn1/q;

    move-result-object v0

    goto :goto_0

    :cond_5
    sparse-switch v3, :sswitch_data_0

    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v1, "unknown BER object encountered"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :sswitch_0
    new-instance v0, Lorg/spongycastle/asn1/ae;

    invoke-direct {v0, v4}, Lorg/spongycastle/asn1/ae;-><init>(Lorg/spongycastle/asn1/v;)V

    invoke-virtual {v0}, Lorg/spongycastle/asn1/ae;->e()Lorg/spongycastle/asn1/q;

    move-result-object v0

    goto :goto_0

    :sswitch_1
    new-instance v0, Lorg/spongycastle/asn1/ag;

    invoke-direct {v0, v4}, Lorg/spongycastle/asn1/ag;-><init>(Lorg/spongycastle/asn1/v;)V

    invoke-virtual {v0}, Lorg/spongycastle/asn1/ag;->e()Lorg/spongycastle/asn1/q;

    move-result-object v0

    goto :goto_0

    :sswitch_2
    new-instance v0, Lorg/spongycastle/asn1/ai;

    invoke-direct {v0, v4}, Lorg/spongycastle/asn1/ai;-><init>(Lorg/spongycastle/asn1/v;)V

    invoke-virtual {v0}, Lorg/spongycastle/asn1/ai;->e()Lorg/spongycastle/asn1/q;

    move-result-object v0

    goto :goto_0

    :sswitch_3
    new-instance v0, Lorg/spongycastle/asn1/as;

    invoke-direct {v0, v4}, Lorg/spongycastle/asn1/as;-><init>(Lorg/spongycastle/asn1/v;)V

    invoke-virtual {v0}, Lorg/spongycastle/asn1/as;->e()Lorg/spongycastle/asn1/q;

    move-result-object v0

    goto :goto_0

    :cond_6
    :try_start_0
    invoke-virtual {p0, v2, v3, v4}, Lorg/spongycastle/asn1/h;->a(III)Lorg/spongycastle/asn1/q;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Lorg/spongycastle/asn1/ASN1Exception;

    const-string/jumbo v2, "corrupted stream detected"

    invoke-direct {v1, v2, v0}, Lorg/spongycastle/asn1/ASN1Exception;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0x8 -> :sswitch_3
        0x10 -> :sswitch_1
        0x11 -> :sswitch_2
    .end sparse-switch
.end method
