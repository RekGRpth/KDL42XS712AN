.class public Lorg/spongycastle/asn1/be;
.super Lorg/spongycastle/asn1/q;


# instance fields
.field private a:[B


# direct methods
.method constructor <init>([B)V
    .locals 0

    invoke-direct {p0}, Lorg/spongycastle/asn1/q;-><init>()V

    iput-object p1, p0, Lorg/spongycastle/asn1/be;->a:[B

    return-void
.end method


# virtual methods
.method a(Lorg/spongycastle/asn1/o;)V
    .locals 2

    const/16 v0, 0x13

    iget-object v1, p0, Lorg/spongycastle/asn1/be;->a:[B

    invoke-virtual {p1, v0, v1}, Lorg/spongycastle/asn1/o;->a(I[B)V

    return-void
.end method

.method a(Lorg/spongycastle/asn1/q;)Z
    .locals 2

    instance-of v0, p1, Lorg/spongycastle/asn1/be;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    check-cast p1, Lorg/spongycastle/asn1/be;

    iget-object v0, p0, Lorg/spongycastle/asn1/be;->a:[B

    iget-object v1, p1, Lorg/spongycastle/asn1/be;->a:[B

    invoke-static {v0, v1}, Lmo;->a([B[B)Z

    move-result v0

    goto :goto_0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/spongycastle/asn1/be;->a:[B

    invoke-static {v0}, Lmq;->b([B)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method h()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lorg/spongycastle/asn1/be;->a:[B

    invoke-static {v0}, Lmo;->a([B)I

    move-result v0

    return v0
.end method

.method i()I
    .locals 2

    iget-object v0, p0, Lorg/spongycastle/asn1/be;->a:[B

    array-length v0, v0

    invoke-static {v0}, Lorg/spongycastle/asn1/ca;->a(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lorg/spongycastle/asn1/be;->a:[B

    array-length v1, v1

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lorg/spongycastle/asn1/be;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
