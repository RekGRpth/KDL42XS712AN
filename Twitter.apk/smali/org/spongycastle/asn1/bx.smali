.class Lorg/spongycastle/asn1/bx;
.super Lorg/spongycastle/asn1/r;


# instance fields
.field private b:[B


# direct methods
.method constructor <init>([B)V
    .locals 0

    invoke-direct {p0}, Lorg/spongycastle/asn1/r;-><init>()V

    iput-object p1, p0, Lorg/spongycastle/asn1/bx;->b:[B

    return-void
.end method

.method private j()V
    .locals 3

    new-instance v0, Lorg/spongycastle/asn1/bw;

    iget-object v1, p0, Lorg/spongycastle/asn1/bx;->b:[B

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/bw;-><init>([B)V

    :goto_0
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lorg/spongycastle/asn1/bx;->a:Ljava/util/Vector;

    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/spongycastle/asn1/bx;->b:[B

    return-void
.end method


# virtual methods
.method public declared-synchronized a(I)Lorg/spongycastle/asn1/d;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/spongycastle/asn1/bx;->b:[B

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lorg/spongycastle/asn1/bx;->j()V

    :cond_0
    invoke-super {p0, p1}, Lorg/spongycastle/asn1/r;->a(I)Lorg/spongycastle/asn1/d;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method a(Lorg/spongycastle/asn1/o;)V
    .locals 2

    iget-object v0, p0, Lorg/spongycastle/asn1/bx;->b:[B

    if-eqz v0, :cond_0

    const/16 v0, 0x30

    iget-object v1, p0, Lorg/spongycastle/asn1/bx;->b:[B

    invoke-virtual {p1, v0, v1}, Lorg/spongycastle/asn1/o;->a(I[B)V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0}, Lorg/spongycastle/asn1/r;->g()Lorg/spongycastle/asn1/q;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/spongycastle/asn1/q;->a(Lorg/spongycastle/asn1/o;)V

    goto :goto_0
.end method

.method public declared-synchronized d()Ljava/util/Enumeration;
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/spongycastle/asn1/bx;->b:[B

    if-nez v0, :cond_0

    invoke-super {p0}, Lorg/spongycastle/asn1/r;->d()Ljava/util/Enumeration;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    new-instance v0, Lorg/spongycastle/asn1/bw;

    iget-object v1, p0, Lorg/spongycastle/asn1/bx;->b:[B

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/bw;-><init>([B)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized e()I
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/spongycastle/asn1/bx;->b:[B

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lorg/spongycastle/asn1/bx;->j()V

    :cond_0
    invoke-super {p0}, Lorg/spongycastle/asn1/r;->e()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method f()Lorg/spongycastle/asn1/q;
    .locals 1

    iget-object v0, p0, Lorg/spongycastle/asn1/bx;->b:[B

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lorg/spongycastle/asn1/bx;->j()V

    :cond_0
    invoke-super {p0}, Lorg/spongycastle/asn1/r;->f()Lorg/spongycastle/asn1/q;

    move-result-object v0

    return-object v0
.end method

.method g()Lorg/spongycastle/asn1/q;
    .locals 1

    iget-object v0, p0, Lorg/spongycastle/asn1/bx;->b:[B

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lorg/spongycastle/asn1/bx;->j()V

    :cond_0
    invoke-super {p0}, Lorg/spongycastle/asn1/r;->g()Lorg/spongycastle/asn1/q;

    move-result-object v0

    return-object v0
.end method

.method i()I
    .locals 2

    iget-object v0, p0, Lorg/spongycastle/asn1/bx;->b:[B

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/spongycastle/asn1/bx;->b:[B

    array-length v0, v0

    invoke-static {v0}, Lorg/spongycastle/asn1/ca;->a(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lorg/spongycastle/asn1/bx;->b:[B

    array-length v1, v1

    add-int/2addr v0, v1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Lorg/spongycastle/asn1/r;->g()Lorg/spongycastle/asn1/q;

    move-result-object v0

    invoke-virtual {v0}, Lorg/spongycastle/asn1/q;->i()I

    move-result v0

    goto :goto_0
.end method
