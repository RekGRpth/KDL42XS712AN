.class public Lorg/spongycastle/asn1/bi;
.super Ljava/lang/Object;

# interfaces
.implements Lorg/spongycastle/asn1/u;


# instance fields
.field private a:Lorg/spongycastle/asn1/v;


# direct methods
.method constructor <init>(Lorg/spongycastle/asn1/v;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/spongycastle/asn1/bi;->a:Lorg/spongycastle/asn1/v;

    return-void
.end method


# virtual methods
.method public a()Lorg/spongycastle/asn1/q;
    .locals 3

    :try_start_0
    invoke-virtual {p0}, Lorg/spongycastle/asn1/bi;->e()Lorg/spongycastle/asn1/q;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Lorg/spongycastle/asn1/ASN1ParsingException;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lorg/spongycastle/asn1/ASN1ParsingException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public e()Lorg/spongycastle/asn1/q;
    .locals 3

    new-instance v0, Lorg/spongycastle/asn1/bh;

    iget-object v1, p0, Lorg/spongycastle/asn1/bi;->a:Lorg/spongycastle/asn1/v;

    invoke-virtual {v1}, Lorg/spongycastle/asn1/v;->b()Lorg/spongycastle/asn1/e;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lorg/spongycastle/asn1/bh;-><init>(Lorg/spongycastle/asn1/e;Z)V

    return-object v0
.end method
