.class public Lorg/spongycastle/asn1/an;
.super Lorg/spongycastle/asn1/q;


# instance fields
.field private a:[C


# direct methods
.method constructor <init>([C)V
    .locals 0

    invoke-direct {p0}, Lorg/spongycastle/asn1/q;-><init>()V

    iput-object p1, p0, Lorg/spongycastle/asn1/an;->a:[C

    return-void
.end method


# virtual methods
.method a(Lorg/spongycastle/asn1/o;)V
    .locals 3

    const/16 v0, 0x1e

    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/o;->b(I)V

    iget-object v0, p0, Lorg/spongycastle/asn1/an;->a:[C

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x2

    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/o;->a(I)V

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lorg/spongycastle/asn1/an;->a:[C

    array-length v1, v1

    if-eq v0, v1, :cond_0

    iget-object v1, p0, Lorg/spongycastle/asn1/an;->a:[C

    aget-char v1, v1, v0

    shr-int/lit8 v2, v1, 0x8

    int-to-byte v2, v2

    invoke-virtual {p1, v2}, Lorg/spongycastle/asn1/o;->b(I)V

    int-to-byte v1, v1

    invoke-virtual {p1, v1}, Lorg/spongycastle/asn1/o;->b(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method protected a(Lorg/spongycastle/asn1/q;)Z
    .locals 2

    instance-of v0, p1, Lorg/spongycastle/asn1/an;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    check-cast p1, Lorg/spongycastle/asn1/an;

    iget-object v0, p0, Lorg/spongycastle/asn1/an;->a:[C

    iget-object v1, p1, Lorg/spongycastle/asn1/an;->a:[C

    invoke-static {v0, v1}, Lmo;->a([C[C)Z

    move-result v0

    goto :goto_0
.end method

.method public c()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lorg/spongycastle/asn1/an;->a:[C

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([C)V

    return-object v0
.end method

.method h()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lorg/spongycastle/asn1/an;->a:[C

    invoke-static {v0}, Lmo;->a([C)I

    move-result v0

    return v0
.end method

.method i()I
    .locals 2

    iget-object v0, p0, Lorg/spongycastle/asn1/an;->a:[C

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x2

    invoke-static {v0}, Lorg/spongycastle/asn1/ca;->a(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lorg/spongycastle/asn1/an;->a:[C

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lorg/spongycastle/asn1/an;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
