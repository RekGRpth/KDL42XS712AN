.class public Lorg/spongycastle/asn1/am;
.super Lorg/spongycastle/asn1/q;


# instance fields
.field private final a:Z

.field private final b:I

.field private final c:[B


# direct methods
.method public constructor <init>(ILorg/spongycastle/asn1/e;)V
    .locals 4

    invoke-direct {p0}, Lorg/spongycastle/asn1/q;-><init>()V

    iput p1, p0, Lorg/spongycastle/asn1/am;->b:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/spongycastle/asn1/am;->a:Z

    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p2}, Lorg/spongycastle/asn1/e;->a()I

    move-result v0

    if-eq v1, v0, :cond_0

    :try_start_0
    invoke-virtual {p2, v1}, Lorg/spongycastle/asn1/e;->a(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/k;

    const-string/jumbo v3, "DER"

    invoke-virtual {v0, v3}, Lorg/spongycastle/asn1/k;->a(Ljava/lang/String;)[B

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/io/ByteArrayOutputStream;->write([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Lorg/spongycastle/asn1/ASN1ParsingException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "malformed object: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lorg/spongycastle/asn1/ASN1ParsingException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :cond_0
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    iput-object v0, p0, Lorg/spongycastle/asn1/am;->c:[B

    return-void
.end method

.method constructor <init>(ZI[B)V
    .locals 0

    invoke-direct {p0}, Lorg/spongycastle/asn1/q;-><init>()V

    iput-boolean p1, p0, Lorg/spongycastle/asn1/am;->a:Z

    iput p2, p0, Lorg/spongycastle/asn1/am;->b:I

    iput-object p3, p0, Lorg/spongycastle/asn1/am;->c:[B

    return-void
.end method


# virtual methods
.method a(Lorg/spongycastle/asn1/o;)V
    .locals 3

    const/16 v0, 0x40

    iget-boolean v1, p0, Lorg/spongycastle/asn1/am;->a:Z

    if-eqz v1, :cond_0

    const/16 v0, 0x60

    :cond_0
    iget v1, p0, Lorg/spongycastle/asn1/am;->b:I

    iget-object v2, p0, Lorg/spongycastle/asn1/am;->c:[B

    invoke-virtual {p1, v0, v1, v2}, Lorg/spongycastle/asn1/o;->a(II[B)V

    return-void
.end method

.method a(Lorg/spongycastle/asn1/q;)Z
    .locals 3

    const/4 v0, 0x0

    instance-of v1, p1, Lorg/spongycastle/asn1/am;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    check-cast p1, Lorg/spongycastle/asn1/am;

    iget-boolean v1, p0, Lorg/spongycastle/asn1/am;->a:Z

    iget-boolean v2, p1, Lorg/spongycastle/asn1/am;->a:Z

    if-ne v1, v2, :cond_0

    iget v1, p0, Lorg/spongycastle/asn1/am;->b:I

    iget v2, p1, Lorg/spongycastle/asn1/am;->b:I

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lorg/spongycastle/asn1/am;->c:[B

    iget-object v2, p1, Lorg/spongycastle/asn1/am;->c:[B

    invoke-static {v1, v2}, Lmo;->a([B[B)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public h()Z
    .locals 1

    iget-boolean v0, p0, Lorg/spongycastle/asn1/am;->a:Z

    return v0
.end method

.method public hashCode()I
    .locals 2

    iget-boolean v0, p0, Lorg/spongycastle/asn1/am;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iget v1, p0, Lorg/spongycastle/asn1/am;->b:I

    xor-int/2addr v0, v1

    iget-object v1, p0, Lorg/spongycastle/asn1/am;->c:[B

    invoke-static {v1}, Lmo;->a([B)I

    move-result v1

    xor-int/2addr v0, v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method i()I
    .locals 2

    iget v0, p0, Lorg/spongycastle/asn1/am;->b:I

    invoke-static {v0}, Lorg/spongycastle/asn1/ca;->b(I)I

    move-result v0

    iget-object v1, p0, Lorg/spongycastle/asn1/am;->c:[B

    array-length v1, v1

    invoke-static {v1}, Lorg/spongycastle/asn1/ca;->a(I)I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lorg/spongycastle/asn1/am;->c:[B

    array-length v1, v1

    add-int/2addr v0, v1

    return v0
.end method
