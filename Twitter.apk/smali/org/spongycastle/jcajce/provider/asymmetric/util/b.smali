.class public Lorg/spongycastle/jcajce/provider/asymmetric/util/b;
.super Ljava/lang/Object;


# direct methods
.method public static a(Llq;)[B
    .locals 1

    :try_start_0
    const-string/jumbo v0, "DER"

    invoke-virtual {p0, v0}, Llq;->a(Ljava/lang/String;)[B
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Llu;Lorg/spongycastle/asn1/d;)[B
    .locals 1

    :try_start_0
    new-instance v0, Llw;

    invoke-direct {v0, p0, p1}, Llw;-><init>(Llu;Lorg/spongycastle/asn1/d;)V

    invoke-static {v0}, Lorg/spongycastle/jcajce/provider/asymmetric/util/b;->a(Llw;)[B
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Llw;)[B
    .locals 1

    :try_start_0
    const-string/jumbo v0, "DER"

    invoke-virtual {p0, v0}, Llw;->a(Ljava/lang/String;)[B
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Llu;Lorg/spongycastle/asn1/d;)[B
    .locals 2

    :try_start_0
    new-instance v0, Llq;

    invoke-interface {p1}, Lorg/spongycastle/asn1/d;->a()Lorg/spongycastle/asn1/q;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Llq;-><init>(Llu;Lorg/spongycastle/asn1/d;)V

    invoke-static {v0}, Lorg/spongycastle/jcajce/provider/asymmetric/util/b;->a(Llq;)[B
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method
