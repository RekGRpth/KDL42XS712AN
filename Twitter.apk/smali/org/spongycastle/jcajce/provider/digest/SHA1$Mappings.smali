.class public Lorg/spongycastle/jcajce/provider/digest/SHA1$Mappings;
.super Lorg/spongycastle/jcajce/provider/digest/a;


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lorg/spongycastle/jcajce/provider/digest/SHA1;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lorg/spongycastle/jcajce/provider/digest/SHA1$Mappings;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/spongycastle/jcajce/provider/digest/a;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lorg/spongycastle/jcajce/provider/config/a;)V
    .locals 4

    const-string/jumbo v0, "MessageDigest.SHA-1"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lorg/spongycastle/jcajce/provider/digest/SHA1$Mappings;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "$Digest"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Lorg/spongycastle/jcajce/provider/config/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "Alg.Alias.MessageDigest.SHA1"

    const-string/jumbo v1, "SHA-1"

    invoke-interface {p1, v0, v1}, Lorg/spongycastle/jcajce/provider/config/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "Alg.Alias.MessageDigest.SHA"

    const-string/jumbo v1, "SHA-1"

    invoke-interface {p1, v0, v1}, Lorg/spongycastle/jcajce/provider/config/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "Alg.Alias.MessageDigest."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Llo;->i:Lorg/spongycastle/asn1/l;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "SHA-1"

    invoke-interface {p1, v0, v1}, Lorg/spongycastle/jcajce/provider/config/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "SHA1"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lorg/spongycastle/jcajce/provider/digest/SHA1$Mappings;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "$HashMac"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lorg/spongycastle/jcajce/provider/digest/SHA1$Mappings;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "$KeyGenerator"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, p1, v0, v1, v2}, Lorg/spongycastle/jcajce/provider/digest/SHA1$Mappings;->a(Lorg/spongycastle/jcajce/provider/config/a;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "SHA1"

    sget-object v1, Llp;->H:Lorg/spongycastle/asn1/l;

    invoke-virtual {p0, p1, v0, v1}, Lorg/spongycastle/jcajce/provider/digest/SHA1$Mappings;->a(Lorg/spongycastle/jcajce/provider/config/a;Ljava/lang/String;Lorg/spongycastle/asn1/l;)V

    const-string/jumbo v0, "SHA1"

    sget-object v1, Lln;->c:Lorg/spongycastle/asn1/l;

    invoke-virtual {p0, p1, v0, v1}, Lorg/spongycastle/jcajce/provider/digest/SHA1$Mappings;->a(Lorg/spongycastle/jcajce/provider/config/a;Ljava/lang/String;Lorg/spongycastle/asn1/l;)V

    return-void
.end method
