.class public final Lib;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field public static final actionButtonPadding:I = 0x7f0101b6

.field public static final actionButtonPaddingBottom:I = 0x7f0101ba

.field public static final actionButtonPaddingLeft:I = 0x7f0101b7

.field public static final actionButtonPaddingRight:I = 0x7f0101b9

.field public static final actionButtonPaddingTop:I = 0x7f0101b8

.field public static final actionLayout:I = 0x7f010134

.field public static final actionPromptBackgroundColorBlue:I = 0x7f0101ac

.field public static final actionPromptBackgroundColorGray:I = 0x7f0101ad

.field public static final actionPromptBodyColorGray:I = 0x7f0101aa

.field public static final actionPromptBodyColorWhite:I = 0x7f0101ab

.field public static final actionPromptBorderColor:I = 0x7f0101af

.field public static final actionPromptBorderOffset:I = 0x7f0101a1

.field public static final actionPromptBorderWidth:I = 0x7f0101a0

.field public static final actionPromptContentBottomPadding:I = 0x7f0101a3

.field public static final actionPromptContentLeftPadding:I = 0x7f0101a4

.field public static final actionPromptContentRightPadding:I = 0x7f0101a5

.field public static final actionPromptContentTopPadding:I = 0x7f0101a2

.field public static final actionPromptDismissGray:I = 0x7f0101b0

.field public static final actionPromptDismissMargin:I = 0x7f0101b2

.field public static final actionPromptDismissWhite:I = 0x7f0101b1

.field public static final actionPromptFavoriteIcon:I = 0x7f0101b3

.field public static final actionPromptMarginTop:I = 0x7f01019f

.field public static final actionPromptPointerHeight:I = 0x7f0101a6

.field public static final actionPromptPressedBackgroundColor:I = 0x7f0101ae

.field public static final actionPromptReplyIcon:I = 0x7f0101b4

.field public static final actionPromptTitleColorGray:I = 0x7f0101a7

.field public static final actionPromptTitleColorWhite:I = 0x7f0101a8

.field public static final actionPromptTitleMarginBottom:I = 0x7f0101a9

.field public static final adSize:I = 0x7f010063

.field public static final adUnitId:I = 0x7f010064

.field public static final alertDrawable:I = 0x7f010002

.field public static final allCaps:I = 0x7f010130

.field public static final animation_duration:I = 0x7f010074

.field public static final appDownloadImageHeight:I = 0x7f010179

.field public static final appDownloadImageWidth:I = 0x7f010178

.field public static final attributionColor:I = 0x7f010146

.field public static final attributionTextSize:I = 0x7f010147

.field public static final autoLink:I = 0x7f010197

.field public static final autoUnlock:I = 0x7f010089

.field public static final badgeIndicatorStyle:I = 0x7f010003

.field public static final badgeMode:I = 0x7f01006c

.field public static final badgeSpacing:I = 0x7f01016e

.field public static final bgColorHint:I = 0x7f0100b3

.field public static final borderColor:I = 0x7f0100aa

.field public static final borderHeight:I = 0x7f0100ab

.field public static final bottomDockId:I = 0x7f010084

.field public static final bottomPeek:I = 0x7f010087

.field public static final bylineColor:I = 0x7f010143

.field public static final bylineSize:I = 0x7f010004

.field public static final cameraBearing:I = 0x7f0100c2

.field public static final cameraTargetLat:I = 0x7f0100c3

.field public static final cameraTargetLng:I = 0x7f0100c4

.field public static final cameraTilt:I = 0x7f0100c5

.field public static final cameraZoom:I = 0x7f0100c6

.field public static final cardStyle:I = 0x7f010005

.field public static final cardViewStyle:I = 0x7f010006

.field public static final checkedStyle:I = 0x7f01010f

.field public static final clearDrawablePosition:I = 0x7f010102

.field public static final click_remove_id:I = 0x7f01009a

.field public static final clipRowViewStyle:I = 0x7f010009

.field public static final closeAnimDuration:I = 0x7f0100b0

.field public static final closeInterpolator:I = 0x7f0100ae

.field public static final collapsed_height:I = 0x7f01008a

.field public static final contentColor:I = 0x7f010140

.field public static final contentSize:I = 0x7f01000a

.field public static final cornerRadius:I = 0x7f0100fd

.field public static final cropRectPadding:I = 0x7f01007a

.field public static final cropRectStrokeColor:I = 0x7f01007b

.field public static final cropRectStrokeWidth:I = 0x7f01007c

.field public static final cropShadowColor:I = 0x7f01007d

.field public static final croppableImageViewStyle:I = 0x7f01000b

.field public static final defaultPosition:I = 0x7f0100d7

.field public static final defaultProfileImageDrawable:I = 0x7f0101b5

.field public static final delay_duration:I = 0x7f010073

.field public static final dismissDrawable:I = 0x7f01014a

.field public static final dismissOverlayDrawable:I = 0x7f01000c

.field public static final displayMode:I = 0x7f0100cf

.field public static final dividerColor:I = 0x7f010101

.field public static final dividerHeight:I = 0x7f010100

.field public static final dividerWidth:I = 0x7f0100b8

.field public static final divider_size:I = 0x7f010139

.field public static final dockLayoutStyle:I = 0x7f01000d

.field public static final doubleTapFavoriteAnimation:I = 0x7f010191

.field public static final doubleTapFavoriteDrawable:I = 0x7f010190

.field public static final dragBackground:I = 0x7f010138

.field public static final drag_enabled:I = 0x7f010094

.field public static final drag_handle_id:I = 0x7f010098

.field public static final drag_scroll_start:I = 0x7f01008b

.field public static final drag_start_mode:I = 0x7f010097

.field public static final draggable:I = 0x7f0100b6

.field public static final draggableCorners:I = 0x7f01007e

.field public static final draggableEdgeSize:I = 0x7f0100b5

.field public static final drawerDirection:I = 0x7f0100b4

.field public static final drawerItemStyle:I = 0x7f01000f

.field public static final dropDownListViewStyle:I = 0x7f010010

.field public static final drop_animation_duration:I = 0x7f010093

.field public static final edgePadding:I = 0x7f0100b9

.field public static final elementPressedColor:I = 0x7f010011

.field public static final expandedItemHeight:I = 0x7f010137

.field public static final fillColor:I = 0x7f0100a9

.field public static final fillMode:I = 0x7f0100be

.field public static final fillWidthHeightRatio:I = 0x7f0100ba

.field public static final fling_handle_id:I = 0x7f010099

.field public static final float_alpha:I = 0x7f010090

.field public static final float_background_color:I = 0x7f01008d

.field public static final gapSize:I = 0x7f0100ac

.field public static final gridColor:I = 0x7f010080

.field public static final groupStyle:I = 0x7f010012

.field public static final groupedRowViewStyle:I = 0x7f010013

.field public static final gutterColor:I = 0x7f0100b2

.field public static final gutterSize:I = 0x7f0100b1

.field public static final hiddenDrawerStyle:I = 0x7f010014

.field public static final horizontalListViewStyle:I = 0x7f010015

.field public static final iconMargin:I = 0x7f010108

.field public static final iconSpacing:I = 0x7f010148

.field public static final imagePlaceholderColor:I = 0x7f010016

.field public static final indicatorDrawable:I = 0x7f010065

.field public static final indicatorMarginBottom:I = 0x7f010066

.field public static final inlineActionCountMarginRight:I = 0x7f010161

.field public static final inlineActionFavoriteOffDrawable:I = 0x7f010151

.field public static final inlineActionFavoriteOnDrawable:I = 0x7f010150

.field public static final inlineActionFollowOffDrawable:I = 0x7f010157

.field public static final inlineActionFollowOnDrawable:I = 0x7f010156

.field public static final inlineActionLabelFavoriteColor:I = 0x7f010162

.field public static final inlineActionLabelMarginLeft:I = 0x7f01015f

.field public static final inlineActionLabelMarginRight:I = 0x7f010160

.field public static final inlineActionLabelRetweetColor:I = 0x7f010163

.field public static final inlineActionMarginBottom:I = 0x7f01015c

.field public static final inlineActionMarginMediaBottom:I = 0x7f01015d

.field public static final inlineActionMarginMediaTop:I = 0x7f01015b

.field public static final inlineActionMarginRight:I = 0x7f01015e

.field public static final inlineActionMarginTop:I = 0x7f01015a

.field public static final inlineActionPAcFollowOffDrawable:I = 0x7f010158

.field public static final inlineActionPAcFollowOnDrawable:I = 0x7f010159

.field public static final inlineActionReplyOffDrawable:I = 0x7f010155

.field public static final inlineActionRetweetDisabledDrawable:I = 0x7f010154

.field public static final inlineActionRetweetOffDrawable:I = 0x7f010153

.field public static final inlineActionRetweetOnDrawable:I = 0x7f010152

.field public static final inset:I = 0x7f0100f7

.field public static final insetBottom:I = 0x7f0100fb

.field public static final insetBottomFillColor:I = 0x7f0100fc

.field public static final insetDividerColor:I = 0x7f010072

.field public static final insetDividerHeight:I = 0x7f010071

.field public static final insetHeight:I = 0x7f010070

.field public static final insetLeft:I = 0x7f0100f8

.field public static final insetPaddingStart:I = 0x7f01006e

.field public static final insetRight:I = 0x7f0100fa

.field public static final insetTop:I = 0x7f0100f9

.field public static final insetWidth:I = 0x7f01006f

.field public static final labelColor:I = 0x7f01010b

.field public static final labelSize:I = 0x7f01010a

.field public static final layout_position:I = 0x7f010076

.field public static final leftFadeInDrawable:I = 0x7f0100bf

.field public static final lineSpacingExtra:I = 0x7f01013f

.field public static final lineSpacingMultiplier:I = 0x7f01013e

.field public static final linkColor:I = 0x7f010198

.field public static final linkSelectedColor:I = 0x7f010199

.field public static final listDivider:I = 0x7f0100b7

.field public static final loadingFooterLayout:I = 0x7f0100d6

.field public static final loadingHeaderDivider:I = 0x7f0100d5

.field public static final loadingHeaderLayout:I = 0x7f0100d4

.field public static final mapType:I = 0x7f0100c1

.field public static final max_drag_scroll_speed:I = 0x7f01008c

.field public static final mediaBottomMargin:I = 0x7f010193

.field public static final mediaColor:I = 0x7f010170

.field public static final mediaDivider:I = 0x7f010196

.field public static final mediaGradientDrawable:I = 0x7f01016f

.field public static final mediaIcon:I = 0x7f01014c

.field public static final mediaPlaceholderDrawable:I = 0x7f01018d

.field public static final mediaRetweetDrawable:I = 0x7f010171

.field public static final mediaTagBottomMargin:I = 0x7f010195

.field public static final mediaTagIcon:I = 0x7f01019d

.field public static final mediaTagSummaryColor:I = 0x7f01019c

.field public static final mediaTagSummarySize:I = 0x7f01019b

.field public static final mediaTagTopMargin:I = 0x7f010194

.field public static final mediaTopMargin:I = 0x7f010192

.field public static final minIconWidth:I = 0x7f010109

.field public static final navItemStyle:I = 0x7f01001c

.field public static final normalItemHeight:I = 0x7f010136

.field public static final normalStyle:I = 0x7f01010e

.field public static final notchBg:I = 0x7f0100d1

.field public static final notchDrawable:I = 0x7f0100d0

.field public static final notchLeftOffset:I = 0x7f0100d2

.field public static final notchViewStyle:I = 0x7f01001e

.field public static final numberBackground:I = 0x7f010067

.field public static final numberColor:I = 0x7f010068

.field public static final numberMinHeight:I = 0x7f01006b

.field public static final numberMinWidth:I = 0x7f01006a

.field public static final numberTextSize:I = 0x7f010069

.field public static final openAnimDuration:I = 0x7f0100af

.field public static final openInterpolator:I = 0x7f0100ad

.field public static final order:I = 0x7f01001f

.field public static final overflowIcon:I = 0x7f010132

.field public static final overlayDrawable:I = 0x7f0100d3

.field public static final pageableListViewStyle:I = 0x7f010021

.field public static final panelContentLayoutId:I = 0x7f010107

.field public static final panelHeaderLayoutId:I = 0x7f010106

.field public static final persistentOverlayDrawable:I = 0x7f010022

.field public static final photoErrorOverlayDrawable:I = 0x7f01018f

.field public static final pinned:I = 0x7f010075

.field public static final pivotDividerColor:I = 0x7f010181

.field public static final pivotDividerThickness:I = 0x7f010182

.field public static final pivotTextColor:I = 0x7f010183

.field public static final placeIcon:I = 0x7f01019e

.field public static final placeholderDrawable:I = 0x7f010149

.field public static final playerErrorOverlayDrawable:I = 0x7f01018e

.field public static final playerIcon:I = 0x7f01014d

.field public static final playerOverlay:I = 0x7f010024

.field public static final politicalDrawable:I = 0x7f010025

.field public static final popupEditListStyle:I = 0x7f010026

.field public static final popupEditTextStyle:I = 0x7f010027

.field public static final popupEditTextStyleFullScreen:I = 0x7f010028

.field public static final popupMenuXOffset:I = 0x7f010029

.field public static final popupMenuYOffset:I = 0x7f01002a

.field public static final previewFlags:I = 0x7f01019a

.field public static final primaryItem:I = 0x7f010133

.field public static final priority:I = 0x7f01002b

.field public static final profileImageHeight:I = 0x7f01016a

.field public static final profileImageMarginLeft:I = 0x7f010165

.field public static final profileImageMarginRight:I = 0x7f010167

.field public static final profileImageMarginTop:I = 0x7f010166

.field public static final profileImageOverlayDrawable:I = 0x7f010168

.field public static final profileImageWidth:I = 0x7f010169

.field public static final profileTextColor:I = 0x7f0101bb

.field public static final promoBGColor:I = 0x7f010180

.field public static final promoCTCColor:I = 0x7f01017c

.field public static final promoDividerColor:I = 0x7f01017d

.field public static final promoMarginBottom:I = 0x7f01017b

.field public static final promoMarginTop:I = 0x7f01017a

.field public static final promoPadding:I = 0x7f01017e

.field public static final promotedDrawable:I = 0x7f01002c

.field public static final radioButtonStyle:I = 0x7f01002d

.field public static final remove_animation_duration:I = 0x7f010092

.field public static final remove_enabled:I = 0x7f010096

.field public static final remove_mode:I = 0x7f01008e

.field public static final rightFadeInDrawable:I = 0x7f0100c0

.field public static final screenNameColor:I = 0x7f010145

.field public static final scrollDrawable:I = 0x7f0100bc

.field public static final scrollDrive:I = 0x7f010088

.field public static final scrollHeight:I = 0x7f0100bd

.field public static final scrollOffset:I = 0x7f0100bb

.field public static final searchQueryViewStyle:I = 0x7f010030

.field public static final segmentDivider:I = 0x7f010104

.field public static final segmentLabels:I = 0x7f010105

.field public static final segmentedControlStyle:I = 0x7f010031

.field public static final selectedTextStyle:I = 0x7f010032

.field public static final shadowColor:I = 0x7f010033

.field public static final shadowDx:I = 0x7f010034

.field public static final shadowDy:I = 0x7f010035

.field public static final shadowRadius:I = 0x7f010036

.field public static final shadowTextViewStyle:I = 0x7f010037

.field public static final showAsAction:I = 0x7f010135

.field public static final showAsDropdown:I = 0x7f0100e6

.field public static final showFullScreen:I = 0x7f0100e7

.field public static final showGrid:I = 0x7f01007f

.field public static final showPopupOnInitialFocus:I = 0x7f0100e9

.field public static final single:I = 0x7f010038

.field public static final slide_shuffle_speed:I = 0x7f010091

.field public static final slidingPanelHeaderDividerStyle:I = 0x7f010039

.field public static final socialBylineViewStyle:I = 0x7f01003a

.field public static final socialProofCollection:I = 0x7f01018c

.field public static final socialProofConvoReplyDrawable:I = 0x7f010187

.field public static final socialProofFavDrawable:I = 0x7f010185

.field public static final socialProofFollowDrawable:I = 0x7f010188

.field public static final socialProofNearbyDrawable:I = 0x7f010189

.field public static final socialProofPopularDrawable:I = 0x7f01018a

.field public static final socialProofRecommendation:I = 0x7f01018b

.field public static final socialProofReplyDrawable:I = 0x7f010186

.field public static final socialProofRetweetDrawable:I = 0x7f010184

.field public static final sort_enabled:I = 0x7f010095

.field public static final src:I = 0x7f010103

.field public static final stackFromRight:I = 0x7f01012d

.field public static final state_action_prompt_dismiss_pressed:I = 0x7f01003c

.field public static final state_action_prompt_pressed:I = 0x7f01003d

.field public static final state_attribution_pressed:I = 0x7f01003e

.field public static final state_avatar_pressed:I = 0x7f01003f

.field public static final state_badge_cluster_pressed:I = 0x7f010040

.field public static final state_badge_media:I = 0x7f010041

.field public static final state_collapsed:I = 0x7f010042

.field public static final state_dismiss_pressed:I = 0x7f010043

.field public static final state_docked:I = 0x7f010044

.field public static final state_highlighted:I = 0x7f010045

.field public static final state_inline_action_fav_pressed:I = 0x7f010046

.field public static final state_inline_action_follow_pressed:I = 0x7f010047

.field public static final state_inline_action_pressed:I = 0x7f010048

.field public static final state_inline_action_reply_pressed:I = 0x7f010049

.field public static final state_inline_action_retweet_pressed:I = 0x7f01004a

.field public static final state_media_pressed:I = 0x7f01004b

.field public static final state_numbered:I = 0x7f01004c

.field public static final state_pivot_pressed:I = 0x7f01004d

.field public static final state_promoted_action_pressed:I = 0x7f01004e

.field public static final statsSpacing:I = 0x7f010164

.field public static final strokeColor:I = 0x7f0100ff

.field public static final strokeWidth:I = 0x7f0100fe

.field public static final subtitle:I = 0x7f010131

.field public static final subtitleTextSize:I = 0x7f01012e

.field public static final summaryBgColor:I = 0x7f01017f

.field public static final summaryContentColor:I = 0x7f010141

.field public static final summaryIcon:I = 0x7f01014e

.field public static final summaryImageHeight:I = 0x7f010174

.field public static final summaryImageWidth:I = 0x7f010173

.field public static final summaryPadding:I = 0x7f010172

.field public static final summaryPreviewMarginBottom:I = 0x7f010177

.field public static final summaryPreviewMarginTop:I = 0x7f010176

.field public static final summaryTextSize:I = 0x7f010142

.field public static final summaryUserImageSize:I = 0x7f010175

.field public static final tabDrawable:I = 0x7f0101bc

.field public static final tabMaxHeight:I = 0x7f0101bd

.field public static final textColor:I = 0x7f01004f

.field public static final textSize:I = 0x7f010050

.field public static final textStyle:I = 0x7f010051

.field public static final threshold:I = 0x7f0100e8

.field public static final timestampColor:I = 0x7f010144

.field public static final toolBarCustomViewId:I = 0x7f01012a

.field public static final toolBarDisplayOptions:I = 0x7f01012c

.field public static final toolBarHomeStyle:I = 0x7f010052

.field public static final toolBarIcon:I = 0x7f010127

.field public static final toolBarItemBackground:I = 0x7f010124

.field public static final toolBarItemPadding:I = 0x7f010125

.field public static final toolBarItemStyle:I = 0x7f010053

.field public static final toolBarOverflowContentDescription:I = 0x7f01012b

.field public static final toolBarOverflowDrawable:I = 0x7f010129

.field public static final toolBarOverflowSubtitleTextAppearance:I = 0x7f010054

.field public static final toolBarOverflowTextAppearance:I = 0x7f010055

.field public static final toolBarPopupWindowStyle:I = 0x7f010056

.field public static final toolBarSize:I = 0x7f010057

.field public static final toolBarStyle:I = 0x7f010058

.field public static final toolBarTitle:I = 0x7f010126

.field public static final toolBarUpIndicator:I = 0x7f010128

.field public static final toolbarMargin:I = 0x7f010079

.field public static final topDockId:I = 0x7f010083

.field public static final topPeek:I = 0x7f010086

.field public static final topPillDrawable:I = 0x7f01014b

.field public static final track_drag_sort:I = 0x7f01008f

.field public static final translationIcon:I = 0x7f01014f

.field public static final turtle:I = 0x7f010085

.field public static final tweetViewStyle:I = 0x7f01005a

.field public static final uiCompass:I = 0x7f0100c7

.field public static final uiRotateGestures:I = 0x7f0100c8

.field public static final uiScrollGestures:I = 0x7f0100c9

.field public static final uiTiltGestures:I = 0x7f0100ca

.field public static final uiZoomControls:I = 0x7f0100cb

.field public static final uiZoomGestures:I = 0x7f0100cc

.field public static final upIndicatorDescription:I = 0x7f01012f

.field public static final useViewLifecycle:I = 0x7f0100cd

.field public static final use_default_controller:I = 0x7f01009b

.field public static final userViewStyle:I = 0x7f01005e

.field public static final verticalConnector:I = 0x7f01016b

.field public static final verticalConnectorMargin:I = 0x7f01016d

.field public static final verticalConnectorWidth:I = 0x7f01016c

.field public static final viewPagerScrollBarStyle:I = 0x7f010062

.field public static final zOrderOnTop:I = 0x7f0100ce
