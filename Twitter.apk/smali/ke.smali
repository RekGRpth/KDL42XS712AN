.class public Lke;
.super Lkb;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final a:Lcom/twitter/library/view/h;


# direct methods
.method public constructor <init>(Lcom/twitter/library/view/h;Z)V
    .locals 0

    invoke-direct {p0, p2}, Lkb;-><init>(Z)V

    iput-object p1, p0, Lke;->a:Lcom/twitter/library/view/h;

    return-void
.end method


# virtual methods
.method protected b(Landroid/view/View;)V
    .locals 1

    iget-object v0, p0, Lke;->a:Lcom/twitter/library/view/h;

    invoke-interface {v0, p1}, Lcom/twitter/library/view/h;->onClick(Landroid/view/View;)V

    return-void
.end method

.method protected c(Landroid/view/View;)V
    .locals 1

    iget-object v0, p0, Lke;->a:Lcom/twitter/library/view/h;

    invoke-interface {v0, p1}, Lcom/twitter/library/view/h;->a(Landroid/view/View;)V

    return-void
.end method

.method protected d(Landroid/view/View;)V
    .locals 1

    iget-object v0, p0, Lke;->a:Lcom/twitter/library/view/h;

    invoke-interface {v0, p1}, Lcom/twitter/library/view/h;->b(Landroid/view/View;)V

    return-void
.end method

.method protected e(Landroid/view/View;)V
    .locals 1

    iget-object v0, p0, Lke;->a:Lcom/twitter/library/view/h;

    invoke-interface {v0, p1}, Lcom/twitter/library/view/h;->c(Landroid/view/View;)V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 0

    invoke-virtual {p0, p1}, Lke;->a(Landroid/view/View;)V

    return-void
.end method
