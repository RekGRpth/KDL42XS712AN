.class public Llu;
.super Lorg/spongycastle/asn1/k;


# instance fields
.field private a:Lorg/spongycastle/asn1/l;

.field private b:Lorg/spongycastle/asn1/d;

.field private c:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Llu;->c:Z

    new-instance v0, Lorg/spongycastle/asn1/l;

    invoke-direct {v0, p1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Llu;->a:Lorg/spongycastle/asn1/l;

    return-void
.end method

.method public constructor <init>(Lorg/spongycastle/asn1/l;)V
    .locals 1

    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Llu;->c:Z

    iput-object p1, p0, Llu;->a:Lorg/spongycastle/asn1/l;

    return-void
.end method

.method public constructor <init>(Lorg/spongycastle/asn1/l;Lorg/spongycastle/asn1/d;)V
    .locals 1

    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Llu;->c:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Llu;->c:Z

    iput-object p1, p0, Llu;->a:Lorg/spongycastle/asn1/l;

    iput-object p2, p0, Llu;->b:Lorg/spongycastle/asn1/d;

    return-void
.end method

.method public constructor <init>(Lorg/spongycastle/asn1/r;)V
    .locals 4

    const/4 v3, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    iput-boolean v2, p0, Llu;->c:Z

    invoke-virtual {p1}, Lorg/spongycastle/asn1/r;->e()I

    move-result v0

    if-lt v0, v1, :cond_0

    invoke-virtual {p1}, Lorg/spongycastle/asn1/r;->e()I

    move-result v0

    if-le v0, v3, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Bad sequence size: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lorg/spongycastle/asn1/r;->e()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-virtual {p1, v2}, Lorg/spongycastle/asn1/r;->a(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/l;->a(Ljava/lang/Object;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    iput-object v0, p0, Llu;->a:Lorg/spongycastle/asn1/l;

    invoke-virtual {p1}, Lorg/spongycastle/asn1/r;->e()I

    move-result v0

    if-ne v0, v3, :cond_2

    iput-boolean v1, p0, Llu;->c:Z

    invoke-virtual {p1, v1}, Lorg/spongycastle/asn1/r;->a(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    iput-object v0, p0, Llu;->b:Lorg/spongycastle/asn1/d;

    :goto_0
    return-void

    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Llu;->b:Lorg/spongycastle/asn1/d;

    goto :goto_0
.end method

.method public static a(Ljava/lang/Object;)Llu;
    .locals 3

    if-eqz p0, :cond_0

    instance-of v0, p0, Llu;

    if-eqz v0, :cond_1

    :cond_0
    check-cast p0, Llu;

    :goto_0
    return-object p0

    :cond_1
    instance-of v0, p0, Lorg/spongycastle/asn1/l;

    if-eqz v0, :cond_2

    new-instance v0, Llu;

    check-cast p0, Lorg/spongycastle/asn1/l;

    invoke-direct {v0, p0}, Llu;-><init>(Lorg/spongycastle/asn1/l;)V

    move-object p0, v0

    goto :goto_0

    :cond_2
    instance-of v0, p0, Ljava/lang/String;

    if-eqz v0, :cond_3

    new-instance v0, Llu;

    check-cast p0, Ljava/lang/String;

    invoke-direct {v0, p0}, Llu;-><init>(Ljava/lang/String;)V

    move-object p0, v0

    goto :goto_0

    :cond_3
    instance-of v0, p0, Lorg/spongycastle/asn1/r;

    if-nez v0, :cond_4

    instance-of v0, p0, Lorg/spongycastle/asn1/s;

    if-eqz v0, :cond_5

    :cond_4
    new-instance v0, Llu;

    invoke-static {p0}, Lorg/spongycastle/asn1/r;->a(Ljava/lang/Object;)Lorg/spongycastle/asn1/r;

    move-result-object v1

    invoke-direct {v0, v1}, Llu;-><init>(Lorg/spongycastle/asn1/r;)V

    move-object p0, v0

    goto :goto_0

    :cond_5
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "unknown object in factory: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public a()Lorg/spongycastle/asn1/q;
    .locals 2

    new-instance v0, Lorg/spongycastle/asn1/e;

    invoke-direct {v0}, Lorg/spongycastle/asn1/e;-><init>()V

    iget-object v1, p0, Llu;->a:Lorg/spongycastle/asn1/l;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    iget-boolean v1, p0, Llu;->c:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Llu;->b:Lorg/spongycastle/asn1/d;

    if-eqz v1, :cond_1

    iget-object v1, p0, Llu;->b:Lorg/spongycastle/asn1/d;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    :cond_0
    :goto_0
    new-instance v1, Lorg/spongycastle/asn1/bf;

    invoke-direct {v1, v0}, Lorg/spongycastle/asn1/bf;-><init>(Lorg/spongycastle/asn1/e;)V

    return-object v1

    :cond_1
    sget-object v1, Lorg/spongycastle/asn1/ay;->a:Lorg/spongycastle/asn1/ay;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    goto :goto_0
.end method

.method public c()Lorg/spongycastle/asn1/l;
    .locals 2

    new-instance v0, Lorg/spongycastle/asn1/l;

    iget-object v1, p0, Llu;->a:Lorg/spongycastle/asn1/l;

    invoke-virtual {v1}, Lorg/spongycastle/asn1/l;->c()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    return-object v0
.end method
