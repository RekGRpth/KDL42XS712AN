.class public interface abstract Llt;
.super Ljava/lang/Object;


# static fields
.field public static final A:Lorg/spongycastle/asn1/l;

.field public static final B:Lorg/spongycastle/asn1/l;

.field public static final a:Lorg/spongycastle/asn1/l;

.field public static final b:Lorg/spongycastle/asn1/l;

.field public static final c:Lorg/spongycastle/asn1/l;

.field public static final d:Lorg/spongycastle/asn1/l;

.field public static final e:Lorg/spongycastle/asn1/l;

.field public static final f:Lorg/spongycastle/asn1/l;

.field public static final g:Lorg/spongycastle/asn1/l;

.field public static final h:Lorg/spongycastle/asn1/l;

.field public static final i:Lorg/spongycastle/asn1/l;

.field public static final j:Lorg/spongycastle/asn1/l;

.field public static final k:Lorg/spongycastle/asn1/l;

.field public static final l:Lorg/spongycastle/asn1/l;

.field public static final m:Lorg/spongycastle/asn1/l;

.field public static final n:Lorg/spongycastle/asn1/l;

.field public static final o:Lorg/spongycastle/asn1/l;

.field public static final p:Lorg/spongycastle/asn1/l;

.field public static final q:Lorg/spongycastle/asn1/l;

.field public static final r:Lorg/spongycastle/asn1/l;

.field public static final s:Lorg/spongycastle/asn1/l;

.field public static final t:Lorg/spongycastle/asn1/l;

.field public static final u:Lorg/spongycastle/asn1/l;

.field public static final v:Lorg/spongycastle/asn1/l;

.field public static final w:Lorg/spongycastle/asn1/l;

.field public static final x:Lorg/spongycastle/asn1/l;

.field public static final y:Lorg/spongycastle/asn1/l;

.field public static final z:Lorg/spongycastle/asn1/l;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string/jumbo v1, "1.3.36.3"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Llt;->a:Lorg/spongycastle/asn1/l;

    sget-object v0, Llt;->a:Lorg/spongycastle/asn1/l;

    const-string/jumbo v1, "2.1"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->b(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Llt;->b:Lorg/spongycastle/asn1/l;

    sget-object v0, Llt;->a:Lorg/spongycastle/asn1/l;

    const-string/jumbo v1, "2.2"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->b(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Llt;->c:Lorg/spongycastle/asn1/l;

    sget-object v0, Llt;->a:Lorg/spongycastle/asn1/l;

    const-string/jumbo v1, "2.3"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->b(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Llt;->d:Lorg/spongycastle/asn1/l;

    sget-object v0, Llt;->a:Lorg/spongycastle/asn1/l;

    const-string/jumbo v1, "3.1"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->b(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Llt;->e:Lorg/spongycastle/asn1/l;

    sget-object v0, Llt;->e:Lorg/spongycastle/asn1/l;

    const-string/jumbo v1, "2"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->b(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Llt;->f:Lorg/spongycastle/asn1/l;

    sget-object v0, Llt;->e:Lorg/spongycastle/asn1/l;

    const-string/jumbo v1, "3"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->b(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Llt;->g:Lorg/spongycastle/asn1/l;

    sget-object v0, Llt;->e:Lorg/spongycastle/asn1/l;

    const-string/jumbo v1, "4"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->b(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Llt;->h:Lorg/spongycastle/asn1/l;

    sget-object v0, Llt;->a:Lorg/spongycastle/asn1/l;

    const-string/jumbo v1, "3.2"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->b(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Llt;->i:Lorg/spongycastle/asn1/l;

    sget-object v0, Llt;->i:Lorg/spongycastle/asn1/l;

    const-string/jumbo v1, "1"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->b(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Llt;->j:Lorg/spongycastle/asn1/l;

    sget-object v0, Llt;->i:Lorg/spongycastle/asn1/l;

    const-string/jumbo v1, "2"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->b(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Llt;->k:Lorg/spongycastle/asn1/l;

    sget-object v0, Llt;->a:Lorg/spongycastle/asn1/l;

    const-string/jumbo v1, "3.2.8"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->b(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Llt;->l:Lorg/spongycastle/asn1/l;

    sget-object v0, Llt;->l:Lorg/spongycastle/asn1/l;

    const-string/jumbo v1, "1"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->b(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Llt;->m:Lorg/spongycastle/asn1/l;

    sget-object v0, Llt;->m:Lorg/spongycastle/asn1/l;

    const-string/jumbo v1, "1"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->b(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Llt;->n:Lorg/spongycastle/asn1/l;

    sget-object v0, Llt;->n:Lorg/spongycastle/asn1/l;

    const-string/jumbo v1, "1"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->b(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Llt;->o:Lorg/spongycastle/asn1/l;

    sget-object v0, Llt;->n:Lorg/spongycastle/asn1/l;

    const-string/jumbo v1, "2"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->b(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Llt;->p:Lorg/spongycastle/asn1/l;

    sget-object v0, Llt;->n:Lorg/spongycastle/asn1/l;

    const-string/jumbo v1, "3"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->b(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Llt;->q:Lorg/spongycastle/asn1/l;

    sget-object v0, Llt;->n:Lorg/spongycastle/asn1/l;

    const-string/jumbo v1, "4"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->b(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Llt;->r:Lorg/spongycastle/asn1/l;

    sget-object v0, Llt;->n:Lorg/spongycastle/asn1/l;

    const-string/jumbo v1, "5"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->b(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Llt;->s:Lorg/spongycastle/asn1/l;

    sget-object v0, Llt;->n:Lorg/spongycastle/asn1/l;

    const-string/jumbo v1, "6"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->b(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Llt;->t:Lorg/spongycastle/asn1/l;

    sget-object v0, Llt;->n:Lorg/spongycastle/asn1/l;

    const-string/jumbo v1, "7"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->b(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Llt;->u:Lorg/spongycastle/asn1/l;

    sget-object v0, Llt;->n:Lorg/spongycastle/asn1/l;

    const-string/jumbo v1, "8"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->b(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Llt;->v:Lorg/spongycastle/asn1/l;

    sget-object v0, Llt;->n:Lorg/spongycastle/asn1/l;

    const-string/jumbo v1, "9"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->b(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Llt;->w:Lorg/spongycastle/asn1/l;

    sget-object v0, Llt;->n:Lorg/spongycastle/asn1/l;

    const-string/jumbo v1, "10"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->b(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Llt;->x:Lorg/spongycastle/asn1/l;

    sget-object v0, Llt;->n:Lorg/spongycastle/asn1/l;

    const-string/jumbo v1, "11"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->b(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Llt;->y:Lorg/spongycastle/asn1/l;

    sget-object v0, Llt;->n:Lorg/spongycastle/asn1/l;

    const-string/jumbo v1, "12"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->b(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Llt;->z:Lorg/spongycastle/asn1/l;

    sget-object v0, Llt;->n:Lorg/spongycastle/asn1/l;

    const-string/jumbo v1, "13"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->b(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Llt;->A:Lorg/spongycastle/asn1/l;

    sget-object v0, Llt;->n:Lorg/spongycastle/asn1/l;

    const-string/jumbo v1, "14"

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/l;->b(Ljava/lang/String;)Lorg/spongycastle/asn1/l;

    move-result-object v0

    sput-object v0, Llt;->B:Lorg/spongycastle/asn1/l;

    return-void
.end method
