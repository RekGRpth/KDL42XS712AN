.class public Lhn;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Z

.field private final b:Lcom/twitter/internal/android/widget/ToolBar;

.field private c:Lcom/twitter/internal/android/widget/ToolBarItemView;

.field private d:I

.field private e:I

.field private f:I

.field private g:Landroid/view/View;

.field private h:Lho;

.field private i:Z

.field private j:Z

.field private k:Z

.field private l:Ljava/lang/CharSequence;

.field private m:Ljava/lang/CharSequence;

.field private n:Landroid/content/Intent;

.field private o:I

.field private p:Ljava/lang/CharSequence;

.field private q:Landroid/graphics/drawable/Drawable;

.field private r:I

.field private s:I


# direct methods
.method public constructor <init>(Lcom/twitter/internal/android/widget/ToolBar;Z)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lhn;->b:Lcom/twitter/internal/android/widget/ToolBar;

    iput-boolean p2, p0, Lhn;->a:Z

    iput-boolean v0, p0, Lhn;->j:Z

    iput-boolean v0, p0, Lhn;->k:Z

    iput v0, p0, Lhn;->f:I

    return-void
.end method

.method private r()V
    .locals 2

    iget-object v0, p0, Lhn;->c:Lcom/twitter/internal/android/widget/ToolBarItemView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhn;->m:Ljava/lang/CharSequence;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhn;->c:Lcom/twitter/internal/android/widget/ToolBarItemView;

    iget-object v1, p0, Lhn;->m:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/ToolBarItemView;->setContentDescription(Ljava/lang/CharSequence;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lhn;->l:Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhn;->c:Lcom/twitter/internal/android/widget/ToolBarItemView;

    iget-object v1, p0, Lhn;->l:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/ToolBarItemView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Lhn;->e:I

    return v0
.end method

.method public a(I)Lhn;
    .locals 0

    iput p1, p0, Lhn;->e:I

    return-object p0
.end method

.method public a(Landroid/content/Intent;)Lhn;
    .locals 0

    iput-object p1, p0, Lhn;->n:Landroid/content/Intent;

    return-object p0
.end method

.method public a(Landroid/graphics/drawable/Drawable;)Lhn;
    .locals 1

    iget-object v0, p0, Lhn;->q:Landroid/graphics/drawable/Drawable;

    if-eq p1, v0, :cond_0

    iput-object p1, p0, Lhn;->q:Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, Lhn;->b:Lcom/twitter/internal/android/widget/ToolBar;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/ToolBar;->f()V

    :cond_0
    return-object p0
.end method

.method public a(Lcom/twitter/internal/android/widget/ToolBarItemView;)Lhn;
    .locals 0

    iput-object p1, p0, Lhn;->c:Lcom/twitter/internal/android/widget/ToolBarItemView;

    invoke-direct {p0}, Lhn;->r()V

    return-object p0
.end method

.method public a(Lho;)Lhn;
    .locals 0

    iput-object p1, p0, Lhn;->h:Lho;

    return-object p0
.end method

.method public a(Ljava/lang/CharSequence;)Lhn;
    .locals 0

    iput-object p1, p0, Lhn;->l:Ljava/lang/CharSequence;

    invoke-direct {p0}, Lhn;->r()V

    return-object p0
.end method

.method public a(Z)V
    .locals 0

    iput-boolean p1, p0, Lhn;->i:Z

    return-void
.end method

.method public b(I)Lhn;
    .locals 1

    iput p1, p0, Lhn;->d:I

    iget-object v0, p0, Lhn;->c:Lcom/twitter/internal/android/widget/ToolBarItemView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhn;->c:Lcom/twitter/internal/android/widget/ToolBarItemView;

    invoke-virtual {v0, p1}, Lcom/twitter/internal/android/widget/ToolBarItemView;->setImageResource(I)V

    :cond_0
    return-object p0
.end method

.method public b(Ljava/lang/CharSequence;)Lhn;
    .locals 0

    iput-object p1, p0, Lhn;->m:Ljava/lang/CharSequence;

    invoke-direct {p0}, Lhn;->r()V

    return-object p0
.end method

.method public b(Z)Lhn;
    .locals 1

    iget-boolean v0, p0, Lhn;->j:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lhn;->j:Z

    iget-object v0, p0, Lhn;->b:Lcom/twitter/internal/android/widget/ToolBar;

    invoke-virtual {v0, p0}, Lcom/twitter/internal/android/widget/ToolBar;->d(Lhn;)V

    :cond_0
    return-object p0
.end method

.method public b()Z
    .locals 1

    iget-boolean v0, p0, Lhn;->a:Z

    return v0
.end method

.method public c()I
    .locals 1

    iget v0, p0, Lhn;->d:I

    return v0
.end method

.method public c(I)Lhn;
    .locals 3

    if-eqz p1, :cond_0

    iget-object v0, p0, Lhn;->b:Lcom/twitter/internal/android/widget/ToolBar;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/ToolBar;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lhn;->g:Landroid/view/View;

    :cond_0
    return-object p0
.end method

.method public c(Ljava/lang/CharSequence;)Lhn;
    .locals 0

    iput-object p1, p0, Lhn;->p:Ljava/lang/CharSequence;

    return-object p0
.end method

.method public c(Z)Lhn;
    .locals 1

    iput-boolean p1, p0, Lhn;->k:Z

    iget-object v0, p0, Lhn;->c:Lcom/twitter/internal/android/widget/ToolBarItemView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhn;->c:Lcom/twitter/internal/android/widget/ToolBarItemView;

    invoke-virtual {v0, p1}, Lcom/twitter/internal/android/widget/ToolBarItemView;->setEnabled(Z)V

    :cond_0
    return-object p0
.end method

.method public d()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lhn;->c:Lcom/twitter/internal/android/widget/ToolBarItemView;

    return-object v0
.end method

.method public d(I)Lhn;
    .locals 0

    iput p1, p0, Lhn;->f:I

    return-object p0
.end method

.method public e()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lhn;->g:Landroid/view/View;

    return-object v0
.end method

.method public e(I)V
    .locals 1

    iget-object v0, p0, Lhn;->c:Lcom/twitter/internal/android/widget/ToolBarItemView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhn;->c:Lcom/twitter/internal/android/widget/ToolBarItemView;

    invoke-virtual {v0, p1}, Lcom/twitter/internal/android/widget/ToolBarItemView;->setBadgeMode(I)V

    :cond_0
    return-void
.end method

.method public f(I)V
    .locals 1

    iget v0, p0, Lhn;->o:I

    if-eq v0, p1, :cond_0

    iput p1, p0, Lhn;->o:I

    iget-object v0, p0, Lhn;->c:Lcom/twitter/internal/android/widget/ToolBarItemView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhn;->c:Lcom/twitter/internal/android/widget/ToolBarItemView;

    invoke-virtual {v0, p1}, Lcom/twitter/internal/android/widget/ToolBarItemView;->setNumber(I)V

    :cond_0
    return-void
.end method

.method public f()Z
    .locals 2

    const/4 v0, 0x0

    iget-boolean v1, p0, Lhn;->i:Z

    if-nez v1, :cond_0

    iget v1, p0, Lhn;->f:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_0

    iget-object v1, p0, Lhn;->g:Landroid/view/View;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lhn;->h:Lho;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lhn;->h:Lho;

    invoke-interface {v1, p0}, Lho;->b(Lhn;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    iget-object v0, p0, Lhn;->b:Lcom/twitter/internal/android/widget/ToolBar;

    invoke-virtual {v0, p0}, Lcom/twitter/internal/android/widget/ToolBar;->c(Lhn;)Z

    move-result v0

    goto :goto_0
.end method

.method public g(I)Lhn;
    .locals 1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lhn;->b:Lcom/twitter/internal/android/widget/ToolBar;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/ToolBar;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lhn;->q:Landroid/graphics/drawable/Drawable;

    :cond_0
    return-object p0
.end method

.method public g()Z
    .locals 2

    const/4 v0, 0x0

    iget-boolean v1, p0, Lhn;->i:Z

    if-eqz v1, :cond_0

    iget v1, p0, Lhn;->f:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_0

    iget-object v1, p0, Lhn;->g:Landroid/view/View;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lhn;->h:Lho;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lhn;->h:Lho;

    invoke-interface {v1, p0}, Lho;->a(Lhn;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    iget-object v0, p0, Lhn;->b:Lcom/twitter/internal/android/widget/ToolBar;

    invoke-virtual {v0, p0}, Lcom/twitter/internal/android/widget/ToolBar;->b(Lhn;)Z

    move-result v0

    goto :goto_0
.end method

.method public h(I)Lhn;
    .locals 0

    iput p1, p0, Lhn;->r:I

    return-object p0
.end method

.method public h()Z
    .locals 1

    iget-boolean v0, p0, Lhn;->i:Z

    return v0
.end method

.method public i(I)Lhn;
    .locals 0

    iput p1, p0, Lhn;->s:I

    return-object p0
.end method

.method public i()Z
    .locals 1

    iget-boolean v0, p0, Lhn;->j:Z

    return v0
.end method

.method public j()I
    .locals 1

    iget v0, p0, Lhn;->f:I

    return v0
.end method

.method public k()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lhn;->l:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public l()Z
    .locals 1

    iget-boolean v0, p0, Lhn;->k:Z

    return v0
.end method

.method public m()Landroid/content/Intent;
    .locals 1

    iget-object v0, p0, Lhn;->n:Landroid/content/Intent;

    return-object v0
.end method

.method public n()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lhn;->p:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public o()Landroid/graphics/drawable/Drawable;
    .locals 1

    iget-object v0, p0, Lhn;->q:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public p()I
    .locals 1

    iget v0, p0, Lhn;->r:I

    return v0
.end method

.method public q()I
    .locals 1

    iget v0, p0, Lhn;->s:I

    return v0
.end method
