.class public final Lfo;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lfg;
.implements Lfh;
.implements Ljava/lang/Cloneable;


# instance fields
.field a:Lfy;

.field b:J


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private f(J)[B
    .locals 6

    iget-wide v0, p0, Lfo;->b:J

    const-wide/16 v2, 0x0

    move-wide v4, p1

    invoke-static/range {v0 .. v5}, Lgc;->a(JJJ)V

    const-wide/32 v0, 0x7fffffff

    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "byteCount > Integer.MAX_VALUE: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/4 v0, 0x0

    long-to-int v1, p1

    new-array v1, v1, [B

    :cond_1
    :goto_0
    int-to-long v2, v0

    cmp-long v2, v2, p1

    if-gez v2, :cond_2

    int-to-long v2, v0

    sub-long v2, p1, v2

    iget-object v4, p0, Lfo;->a:Lfy;

    iget v4, v4, Lfy;->c:I

    iget-object v5, p0, Lfo;->a:Lfy;

    iget v5, v5, Lfy;->b:I

    sub-int/2addr v4, v5

    int-to-long v4, v4

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    long-to-int v2, v2

    iget-object v3, p0, Lfo;->a:Lfy;

    iget-object v3, v3, Lfy;->a:[B

    iget-object v4, p0, Lfo;->a:Lfy;

    iget v4, v4, Lfy;->b:I

    invoke-static {v3, v4, v1, v0, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/2addr v0, v2

    iget-object v3, p0, Lfo;->a:Lfy;

    iget v4, v3, Lfy;->b:I

    add-int/2addr v2, v4

    iput v2, v3, Lfy;->b:I

    iget-object v2, p0, Lfo;->a:Lfy;

    iget v2, v2, Lfy;->b:I

    iget-object v3, p0, Lfo;->a:Lfy;

    iget v3, v3, Lfy;->c:I

    if-ne v2, v3, :cond_1

    iget-object v2, p0, Lfo;->a:Lfy;

    invoke-virtual {v2}, Lfy;->a()Lfy;

    move-result-object v3

    iput-object v3, p0, Lfo;->a:Lfy;

    sget-object v3, Lfz;->a:Lfz;

    invoke-virtual {v3, v2}, Lfz;->a(Lfy;)V

    goto :goto_0

    :cond_2
    iget-wide v2, p0, Lfo;->b:J

    sub-long/2addr v2, p1

    iput-wide v2, p0, Lfo;->b:J

    return-object v1
.end method


# virtual methods
.method public a(B)J
    .locals 4

    invoke-virtual {p0, p1}, Lfo;->b(B)J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    :cond_0
    return-wide v0
.end method

.method public a(BJ)J
    .locals 11

    iget-object v2, p0, Lfo;->a:Lfy;

    if-nez v2, :cond_0

    const-wide/16 v0, -0x1

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    :cond_1
    iget v3, v2, Lfy;->c:I

    iget v4, v2, Lfy;->b:I

    sub-int v5, v3, v4

    int-to-long v3, v5

    cmp-long v3, p2, v3

    if-lez v3, :cond_2

    int-to-long v3, v5

    sub-long/2addr p2, v3

    :goto_1
    int-to-long v3, v5

    add-long/2addr v0, v3

    iget-object v2, v2, Lfy;->d:Lfy;

    iget-object v3, p0, Lfo;->a:Lfy;

    if-ne v2, v3, :cond_1

    const-wide/16 v0, -0x1

    goto :goto_0

    :cond_2
    iget-object v6, v2, Lfy;->a:[B

    iget v3, v2, Lfy;->b:I

    int-to-long v3, v3

    add-long/2addr v3, p2

    iget v7, v2, Lfy;->c:I

    int-to-long v7, v7

    :goto_2
    cmp-long v9, v3, v7

    if-gez v9, :cond_4

    long-to-int v9, v3

    aget-byte v9, v6, v9

    if-ne v9, p1, :cond_3

    add-long/2addr v0, v3

    iget v2, v2, Lfy;->b:I

    int-to-long v2, v2

    sub-long/2addr v0, v2

    goto :goto_0

    :cond_3
    const-wide/16 v9, 0x1

    add-long/2addr v3, v9

    goto :goto_2

    :cond_4
    const-wide/16 p2, 0x0

    goto :goto_1
.end method

.method public synthetic a(I)Lfg;
    .locals 1

    invoke-virtual {p0, p1}, Lfo;->d(I)Lfo;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Lfi;)Lfg;
    .locals 1

    invoke-virtual {p0, p1}, Lfo;->b(Lfi;)Lfo;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Ljava/lang/String;)Lfg;
    .locals 1

    invoke-virtual {p0, p1}, Lfo;->b(Ljava/lang/String;)Lfo;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a([B)Lfg;
    .locals 1

    invoke-virtual {p0, p1}, Lfo;->b([B)Lfo;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a([BII)Lfg;
    .locals 1

    invoke-virtual {p0, p1, p2, p3}, Lfo;->c([BII)Lfo;

    move-result-object v0

    return-object v0
.end method

.method public a(Z)Ljava/lang/String;
    .locals 8

    const-wide/16 v6, 0x0

    const-wide/16 v4, 0x1

    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Lfo;->b(B)J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-nez v2, :cond_2

    if-eqz p1, :cond_0

    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    :cond_0
    iget-wide v0, p0, Lfo;->b:J

    cmp-long v0, v0, v6

    if-eqz v0, :cond_1

    iget-wide v0, p0, Lfo;->b:J

    invoke-virtual {p0, v0, v1}, Lfo;->e(J)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    cmp-long v2, v0, v6

    if-lez v2, :cond_3

    sub-long v2, v0, v4

    invoke-virtual {p0, v2, v3}, Lfo;->d(J)B

    move-result v2

    const/16 v3, 0xd

    if-ne v2, v3, :cond_3

    sub-long/2addr v0, v4

    invoke-virtual {p0, v0, v1}, Lfo;->e(J)Ljava/lang/String;

    move-result-object v0

    const-wide/16 v1, 0x2

    invoke-virtual {p0, v1, v2}, Lfo;->b(J)V

    goto :goto_0

    :cond_3
    invoke-virtual {p0, v0, v1}, Lfo;->e(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v4, v5}, Lfo;->b(J)V

    goto :goto_0
.end method

.method public a()V
    .locals 0

    return-void
.end method

.method public a(J)V
    .locals 2

    iget-wide v0, p0, Lfo;->b:J

    cmp-long v0, v0, p1

    if-gez v0, :cond_0

    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    :cond_0
    return-void
.end method

.method public a(Lfo;J)V
    .locals 8

    const-wide/16 v2, 0x0

    if-ne p1, p0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "source == this"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-wide v0, p1, Lfo;->b:J

    move-wide v4, p2

    invoke-static/range {v0 .. v5}, Lgc;->a(JJJ)V

    :goto_0
    cmp-long v0, p2, v2

    if-lez v0, :cond_5

    iget-object v0, p1, Lfo;->a:Lfy;

    iget v0, v0, Lfy;->c:I

    iget-object v1, p1, Lfo;->a:Lfy;

    iget v1, v1, Lfy;->b:I

    sub-int/2addr v0, v1

    int-to-long v0, v0

    cmp-long v0, p2, v0

    if-gez v0, :cond_2

    iget-object v0, p0, Lfo;->a:Lfy;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lfo;->a:Lfy;

    iget-object v0, v0, Lfy;->e:Lfy;

    :goto_1
    if-eqz v0, :cond_1

    iget v1, v0, Lfy;->c:I

    iget v4, v0, Lfy;->b:I

    sub-int/2addr v1, v4

    int-to-long v4, v1

    add-long/2addr v4, p2

    const-wide/16 v6, 0x800

    cmp-long v1, v4, v6

    if-lez v1, :cond_4

    :cond_1
    iget-object v0, p1, Lfo;->a:Lfy;

    long-to-int v1, p2

    invoke-virtual {v0, v1}, Lfy;->a(I)Lfy;

    move-result-object v0

    iput-object v0, p1, Lfo;->a:Lfy;

    :cond_2
    iget-object v0, p1, Lfo;->a:Lfy;

    iget v1, v0, Lfy;->c:I

    iget v4, v0, Lfy;->b:I

    sub-int/2addr v1, v4

    int-to-long v4, v1

    invoke-virtual {v0}, Lfy;->a()Lfy;

    move-result-object v1

    iput-object v1, p1, Lfo;->a:Lfy;

    iget-object v1, p0, Lfo;->a:Lfy;

    if-nez v1, :cond_6

    iput-object v0, p0, Lfo;->a:Lfy;

    iget-object v0, p0, Lfo;->a:Lfy;

    iget-object v1, p0, Lfo;->a:Lfy;

    iget-object v6, p0, Lfo;->a:Lfy;

    iput-object v6, v1, Lfy;->e:Lfy;

    iput-object v6, v0, Lfy;->d:Lfy;

    :goto_2
    iget-wide v0, p1, Lfo;->b:J

    sub-long/2addr v0, v4

    iput-wide v0, p1, Lfo;->b:J

    iget-wide v0, p0, Lfo;->b:J

    add-long/2addr v0, v4

    iput-wide v0, p0, Lfo;->b:J

    sub-long/2addr p2, v4

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    :cond_4
    iget-object v1, p1, Lfo;->a:Lfy;

    long-to-int v2, p2

    invoke-virtual {v1, v0, v2}, Lfy;->a(Lfy;I)V

    iget-wide v0, p1, Lfo;->b:J

    sub-long/2addr v0, p2

    iput-wide v0, p1, Lfo;->b:J

    iget-wide v0, p0, Lfo;->b:J

    add-long/2addr v0, p2

    iput-wide v0, p0, Lfo;->b:J

    :cond_5
    return-void

    :cond_6
    iget-object v1, p0, Lfo;->a:Lfy;

    iget-object v1, v1, Lfy;->e:Lfy;

    invoke-virtual {v1, v0}, Lfy;->a(Lfy;)Lfy;

    move-result-object v0

    invoke-virtual {v0}, Lfy;->b()V

    goto :goto_2
.end method

.method b([BII)I
    .locals 6

    iget-object v1, p0, Lfo;->a:Lfy;

    if-nez v1, :cond_1

    const/4 v0, -0x1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget v0, v1, Lfy;->c:I

    iget v2, v1, Lfy;->b:I

    sub-int/2addr v0, v2

    invoke-static {p3, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    iget-object v2, v1, Lfy;->a:[B

    iget v3, v1, Lfy;->b:I

    invoke-static {v2, v3, p1, p2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget v2, v1, Lfy;->b:I

    add-int/2addr v2, v0

    iput v2, v1, Lfy;->b:I

    iget-wide v2, p0, Lfo;->b:J

    int-to-long v4, v0

    sub-long/2addr v2, v4

    iput-wide v2, p0, Lfo;->b:J

    iget v2, v1, Lfy;->b:I

    iget v3, v1, Lfy;->c:I

    if-ne v2, v3, :cond_0

    invoke-virtual {v1}, Lfy;->a()Lfy;

    move-result-object v2

    iput-object v2, p0, Lfo;->a:Lfy;

    sget-object v2, Lfz;->a:Lfz;

    invoke-virtual {v2, v1}, Lfz;->a(Lfy;)V

    goto :goto_0
.end method

.method public b(B)J
    .locals 2

    const-wide/16 v0, 0x0

    invoke-virtual {p0, p1, v0, v1}, Lfo;->a(BJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public b(Lfo;J)J
    .locals 4

    iget-wide v0, p0, Lfo;->b:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const-wide/16 p2, -0x1

    :goto_0
    return-wide p2

    :cond_0
    iget-wide v0, p0, Lfo;->b:J

    cmp-long v0, p2, v0

    if-lez v0, :cond_1

    iget-wide p2, p0, Lfo;->b:J

    :cond_1
    invoke-virtual {p1, p0, p2, p3}, Lfo;->a(Lfo;J)V

    goto :goto_0
.end method

.method public synthetic b(I)Lfg;
    .locals 1

    invoke-virtual {p0, p1}, Lfo;->e(I)Lfo;

    move-result-object v0

    return-object v0
.end method

.method public b()Lfo;
    .locals 0

    return-object p0
.end method

.method public b(Lfi;)Lfo;
    .locals 3

    iget-object v0, p1, Lfi;->b:[B

    const/4 v1, 0x0

    iget-object v2, p1, Lfi;->b:[B

    array-length v2, v2

    invoke-virtual {p0, v0, v1, v2}, Lfo;->c([BII)Lfo;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/String;)Lfo;
    .locals 3

    :try_start_0
    const-string/jumbo v0, "UTF-8"

    invoke-virtual {p1, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    const/4 v1, 0x0

    array-length v2, v0

    invoke-virtual {p0, v0, v1, v2}, Lfo;->c([BII)Lfo;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
.end method

.method public b([B)Lfo;
    .locals 2

    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, Lfo;->c([BII)Lfo;

    move-result-object v0

    return-object v0
.end method

.method public b(J)V
    .locals 6

    const-wide/16 v2, 0x0

    iget-wide v0, p0, Lfo;->b:J

    move-wide v4, p1

    invoke-static/range {v0 .. v5}, Lgc;->a(JJJ)V

    iget-wide v0, p0, Lfo;->b:J

    sub-long/2addr v0, p1

    iput-wide v0, p0, Lfo;->b:J

    :cond_0
    :goto_0
    cmp-long v0, p1, v2

    if-lez v0, :cond_1

    iget-object v0, p0, Lfo;->a:Lfy;

    iget v0, v0, Lfy;->c:I

    iget-object v1, p0, Lfo;->a:Lfy;

    iget v1, v1, Lfy;->b:I

    sub-int/2addr v0, v1

    int-to-long v0, v0

    invoke-static {p1, p2, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    long-to-int v0, v0

    int-to-long v4, v0

    sub-long/2addr p1, v4

    iget-object v1, p0, Lfo;->a:Lfy;

    iget v4, v1, Lfy;->b:I

    add-int/2addr v0, v4

    iput v0, v1, Lfy;->b:I

    iget-object v0, p0, Lfo;->a:Lfy;

    iget v0, v0, Lfy;->b:I

    iget-object v1, p0, Lfo;->a:Lfy;

    iget v1, v1, Lfy;->c:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lfo;->a:Lfy;

    invoke-virtual {v0}, Lfy;->a()Lfy;

    move-result-object v1

    iput-object v1, p0, Lfo;->a:Lfy;

    sget-object v1, Lfz;->a:Lfz;

    invoke-virtual {v1, v0}, Lfz;->a(Lfy;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public synthetic c()Lfg;
    .locals 1

    invoke-virtual {p0}, Lfo;->m()Lfo;

    move-result-object v0

    return-object v0
.end method

.method public c(J)Lfi;
    .locals 2

    new-instance v0, Lfi;

    invoke-direct {p0, p1, p2}, Lfo;->f(J)[B

    move-result-object v1

    invoke-direct {v0, v1}, Lfi;-><init>([B)V

    return-object v0
.end method

.method public c(I)Lfo;
    .locals 4

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lfo;->f(I)Lfy;

    move-result-object v0

    iget-object v1, v0, Lfy;->a:[B

    iget v2, v0, Lfy;->c:I

    add-int/lit8 v3, v2, 0x1

    iput v3, v0, Lfy;->c:I

    int-to-byte v0, p1

    aput-byte v0, v1, v2

    iget-wide v0, p0, Lfo;->b:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lfo;->b:J

    return-object p0
.end method

.method public c([BII)Lfo;
    .locals 5

    add-int v0, p2, p3

    :goto_0
    if-ge p2, v0, :cond_0

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lfo;->f(I)Lfy;

    move-result-object v1

    sub-int v2, v0, p2

    iget v3, v1, Lfy;->c:I

    rsub-int v3, v3, 0x800

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    iget-object v3, v1, Lfy;->a:[B

    iget v4, v1, Lfy;->c:I

    invoke-static {p1, p2, v3, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/2addr p2, v2

    iget v3, v1, Lfy;->c:I

    add-int/2addr v2, v3

    iput v2, v1, Lfy;->c:I

    goto :goto_0

    :cond_0
    iget-wide v0, p0, Lfo;->b:J

    int-to-long v2, p3

    add-long/2addr v0, v2

    iput-wide v0, p0, Lfo;->b:J

    return-object p0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lfo;->p()Lfo;

    move-result-object v0

    return-object v0
.end method

.method public close()V
    .locals 0

    return-void
.end method

.method public d(J)B
    .locals 6

    iget-wide v0, p0, Lfo;->b:J

    const-wide/16 v4, 0x1

    move-wide v2, p1

    invoke-static/range {v0 .. v5}, Lgc;->a(JJJ)V

    iget-object v0, p0, Lfo;->a:Lfy;

    :goto_0
    iget v1, v0, Lfy;->c:I

    iget v2, v0, Lfy;->b:I

    sub-int/2addr v1, v2

    int-to-long v2, v1

    cmp-long v2, p1, v2

    if-gez v2, :cond_0

    iget-object v1, v0, Lfy;->a:[B

    iget v0, v0, Lfy;->b:I

    long-to-int v2, p1

    add-int/2addr v0, v2

    aget-byte v0, v1, v0

    return v0

    :cond_0
    int-to-long v1, v1

    sub-long/2addr p1, v1

    iget-object v0, v0, Lfy;->d:Lfy;

    goto :goto_0
.end method

.method public d(I)Lfo;
    .locals 5

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lfo;->f(I)Lfy;

    move-result-object v0

    iget-object v1, v0, Lfy;->a:[B

    iget v2, v0, Lfy;->c:I

    add-int/lit8 v3, v2, 0x1

    shr-int/lit8 v4, p1, 0x8

    and-int/lit16 v4, v4, 0xff

    int-to-byte v4, v4

    aput-byte v4, v1, v2

    add-int/lit8 v2, v3, 0x1

    and-int/lit16 v4, p1, 0xff

    int-to-byte v4, v4

    aput-byte v4, v1, v3

    iput v2, v0, Lfy;->c:I

    iget-wide v0, p0, Lfo;->b:J

    const-wide/16 v2, 0x2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lfo;->b:J

    return-object p0
.end method

.method public d()Ljava/io/OutputStream;
    .locals 1

    new-instance v0, Lfp;

    invoke-direct {v0, p0}, Lfp;-><init>(Lfo;)V

    return-object v0
.end method

.method public e(I)Lfo;
    .locals 5

    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lfo;->f(I)Lfy;

    move-result-object v0

    iget-object v1, v0, Lfy;->a:[B

    iget v2, v0, Lfy;->c:I

    add-int/lit8 v3, v2, 0x1

    shr-int/lit8 v4, p1, 0x18

    and-int/lit16 v4, v4, 0xff

    int-to-byte v4, v4

    aput-byte v4, v1, v2

    add-int/lit8 v2, v3, 0x1

    shr-int/lit8 v4, p1, 0x10

    and-int/lit16 v4, v4, 0xff

    int-to-byte v4, v4

    aput-byte v4, v1, v3

    add-int/lit8 v3, v2, 0x1

    shr-int/lit8 v4, p1, 0x8

    and-int/lit16 v4, v4, 0xff

    int-to-byte v4, v4

    aput-byte v4, v1, v2

    add-int/lit8 v2, v3, 0x1

    and-int/lit16 v4, p1, 0xff

    int-to-byte v4, v4

    aput-byte v4, v1, v3

    iput v2, v0, Lfy;->c:I

    iget-wide v0, p0, Lfo;->b:J

    const-wide/16 v2, 0x4

    add-long/2addr v0, v2

    iput-wide v0, p0, Lfo;->b:J

    return-object p0
.end method

.method public e(J)Ljava/lang/String;
    .locals 6

    const-wide/16 v2, 0x0

    iget-wide v0, p0, Lfo;->b:J

    move-wide v4, p1

    invoke-static/range {v0 .. v5}, Lgc;->a(JJJ)V

    const-wide/32 v0, 0x7fffffff

    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "byteCount > Integer.MAX_VALUE: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    cmp-long v0, p1, v2

    if-nez v0, :cond_2

    const-string/jumbo v0, ""

    :cond_1
    :goto_0
    return-object v0

    :cond_2
    iget-object v1, p0, Lfo;->a:Lfy;

    iget v0, v1, Lfy;->b:I

    int-to-long v2, v0

    add-long/2addr v2, p1

    iget v0, v1, Lfy;->c:I

    int-to-long v4, v0

    cmp-long v0, v2, v4

    if-lez v0, :cond_3

    :try_start_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {p0, p1, p2}, Lfo;->f(J)[B

    move-result-object v1

    const-string/jumbo v2, "UTF-8"

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    :cond_3
    :try_start_1
    new-instance v0, Ljava/lang/String;

    iget-object v2, v1, Lfy;->a:[B

    iget v3, v1, Lfy;->b:I

    long-to-int v4, p1

    const-string/jumbo v5, "UTF-8"

    invoke-direct {v0, v2, v3, v4, v5}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    iget v2, v1, Lfy;->b:I

    int-to-long v2, v2

    add-long/2addr v2, p1

    long-to-int v2, v2

    iput v2, v1, Lfy;->b:I

    iget-wide v2, p0, Lfo;->b:J

    sub-long/2addr v2, p1

    iput-wide v2, p0, Lfo;->b:J

    iget v2, v1, Lfy;->b:I

    iget v3, v1, Lfy;->c:I

    if-ne v2, v3, :cond_1

    invoke-virtual {v1}, Lfy;->a()Lfy;

    move-result-object v2

    iput-object v2, p0, Lfo;->a:Lfy;

    sget-object v2, Lfz;->a:Lfz;

    invoke-virtual {v2, v1}, Lfz;->a(Lfy;)V
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
.end method

.method public e()Z
    .locals 4

    iget-wide v0, p0, Lfo;->b:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 14

    const-wide/16 v0, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    instance-of v2, p1, Lfo;

    if-nez v2, :cond_0

    move v0, v6

    :goto_0
    return v0

    :cond_0
    check-cast p1, Lfo;

    iget-wide v2, p0, Lfo;->b:J

    iget-wide v4, p1, Lfo;->b:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    move v0, v6

    goto :goto_0

    :cond_1
    iget-wide v2, p0, Lfo;->b:J

    cmp-long v2, v2, v0

    if-nez v2, :cond_2

    move v0, v7

    goto :goto_0

    :cond_2
    iget-object v5, p0, Lfo;->a:Lfy;

    iget-object v4, p1, Lfo;->a:Lfy;

    iget v3, v5, Lfy;->b:I

    iget v2, v4, Lfy;->b:I

    :goto_1
    iget-wide v8, p0, Lfo;->b:J

    cmp-long v8, v0, v8

    if-gez v8, :cond_7

    iget v8, v5, Lfy;->c:I

    sub-int/2addr v8, v3

    iget v9, v4, Lfy;->c:I

    sub-int/2addr v9, v2

    invoke-static {v8, v9}, Ljava/lang/Math;->min(II)I

    move-result v8

    int-to-long v10, v8

    move v8, v6

    :goto_2
    int-to-long v12, v8

    cmp-long v9, v12, v10

    if-gez v9, :cond_4

    iget-object v12, v5, Lfy;->a:[B

    add-int/lit8 v9, v3, 0x1

    aget-byte v12, v12, v3

    iget-object v13, v4, Lfy;->a:[B

    add-int/lit8 v3, v2, 0x1

    aget-byte v2, v13, v2

    if-eq v12, v2, :cond_3

    move v0, v6

    goto :goto_0

    :cond_3
    add-int/lit8 v2, v8, 0x1

    move v8, v2

    move v2, v3

    move v3, v9

    goto :goto_2

    :cond_4
    iget v8, v5, Lfy;->c:I

    if-ne v3, v8, :cond_5

    iget-object v5, v5, Lfy;->d:Lfy;

    iget v3, v5, Lfy;->b:I

    :cond_5
    iget v8, v4, Lfy;->c:I

    if-ne v2, v8, :cond_6

    iget-object v4, v4, Lfy;->d:Lfy;

    iget v2, v4, Lfy;->b:I

    :cond_6
    add-long/2addr v0, v10

    goto :goto_1

    :cond_7
    move v0, v7

    goto :goto_0
.end method

.method public f()B
    .locals 9

    iget-wide v0, p0, Lfo;->b:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "size == 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lfo;->a:Lfy;

    iget v1, v0, Lfy;->b:I

    iget v2, v0, Lfy;->c:I

    iget-object v3, v0, Lfy;->a:[B

    add-int/lit8 v4, v1, 0x1

    aget-byte v1, v3, v1

    iget-wide v5, p0, Lfo;->b:J

    const-wide/16 v7, 0x1

    sub-long/2addr v5, v7

    iput-wide v5, p0, Lfo;->b:J

    if-ne v4, v2, :cond_1

    invoke-virtual {v0}, Lfy;->a()Lfy;

    move-result-object v2

    iput-object v2, p0, Lfo;->a:Lfy;

    sget-object v2, Lfz;->a:Lfz;

    invoke-virtual {v2, v0}, Lfz;->a(Lfy;)V

    :goto_0
    return v1

    :cond_1
    iput v4, v0, Lfy;->b:I

    goto :goto_0
.end method

.method f(I)Lfy;
    .locals 3

    const/16 v2, 0x800

    const/4 v0, 0x1

    if-lt p1, v0, :cond_0

    if-le p1, v2, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_1
    iget-object v0, p0, Lfo;->a:Lfy;

    if-nez v0, :cond_3

    sget-object v0, Lfz;->a:Lfz;

    invoke-virtual {v0}, Lfz;->a()Lfy;

    move-result-object v0

    iput-object v0, p0, Lfo;->a:Lfy;

    iget-object v1, p0, Lfo;->a:Lfy;

    iget-object v2, p0, Lfo;->a:Lfy;

    iget-object v0, p0, Lfo;->a:Lfy;

    iput-object v0, v2, Lfy;->e:Lfy;

    iput-object v0, v1, Lfy;->d:Lfy;

    :cond_2
    :goto_0
    return-object v0

    :cond_3
    iget-object v0, p0, Lfo;->a:Lfy;

    iget-object v0, v0, Lfy;->e:Lfy;

    iget v1, v0, Lfy;->c:I

    add-int/2addr v1, p1

    if-le v1, v2, :cond_2

    sget-object v1, Lfz;->a:Lfz;

    invoke-virtual {v1}, Lfz;->a()Lfy;

    move-result-object v1

    invoke-virtual {v0, v1}, Lfy;->a(Lfy;)Lfy;

    move-result-object v0

    goto :goto_0
.end method

.method public g()S
    .locals 8

    const-wide/16 v6, 0x2

    iget-wide v0, p0, Lfo;->b:J

    cmp-long v0, v0, v6

    if-gez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "size < 2: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lfo;->b:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lfo;->a:Lfy;

    iget v1, v0, Lfy;->b:I

    iget v2, v0, Lfy;->c:I

    sub-int v3, v2, v1

    const/4 v4, 0x2

    if-ge v3, v4, :cond_1

    invoke-virtual {p0}, Lfo;->f()B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x8

    invoke-virtual {p0}, Lfo;->f()B

    move-result v1

    and-int/lit16 v1, v1, 0xff

    or-int/2addr v0, v1

    int-to-short v0, v0

    :goto_0
    return v0

    :cond_1
    iget-object v3, v0, Lfy;->a:[B

    add-int/lit8 v4, v1, 0x1

    aget-byte v1, v3, v1

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x8

    add-int/lit8 v5, v4, 0x1

    aget-byte v3, v3, v4

    and-int/lit16 v3, v3, 0xff

    or-int/2addr v1, v3

    iget-wide v3, p0, Lfo;->b:J

    sub-long/2addr v3, v6

    iput-wide v3, p0, Lfo;->b:J

    if-ne v5, v2, :cond_2

    invoke-virtual {v0}, Lfy;->a()Lfy;

    move-result-object v2

    iput-object v2, p0, Lfo;->a:Lfy;

    sget-object v2, Lfz;->a:Lfz;

    invoke-virtual {v2, v0}, Lfz;->a(Lfy;)V

    :goto_1
    int-to-short v0, v1

    goto :goto_0

    :cond_2
    iput v5, v0, Lfy;->b:I

    goto :goto_1
.end method

.method public h()I
    .locals 1

    invoke-virtual {p0}, Lfo;->g()S

    move-result v0

    invoke-static {v0}, Lgc;->a(S)I

    move-result v0

    return v0
.end method

.method public hashCode()I
    .locals 5

    iget-object v1, p0, Lfo;->a:Lfy;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    :cond_1
    iget v2, v1, Lfy;->b:I

    iget v4, v1, Lfy;->c:I

    :goto_1
    if-ge v2, v4, :cond_2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v3, v1, Lfy;->a:[B

    aget-byte v3, v3, v2

    add-int/2addr v3, v0

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v3

    goto :goto_1

    :cond_2
    iget-object v1, v1, Lfy;->d:Lfy;

    iget-object v2, p0, Lfo;->a:Lfy;

    if-ne v1, v2, :cond_1

    goto :goto_0
.end method

.method public i()I
    .locals 8

    const-wide/16 v6, 0x4

    iget-wide v0, p0, Lfo;->b:J

    cmp-long v0, v0, v6

    if-gez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "size < 4: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lfo;->b:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v1, p0, Lfo;->a:Lfy;

    iget v0, v1, Lfy;->b:I

    iget v2, v1, Lfy;->c:I

    sub-int v3, v2, v0

    const/4 v4, 0x4

    if-ge v3, v4, :cond_1

    invoke-virtual {p0}, Lfo;->f()B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x18

    invoke-virtual {p0}, Lfo;->f()B

    move-result v1

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x10

    or-int/2addr v0, v1

    invoke-virtual {p0}, Lfo;->f()B

    move-result v1

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    invoke-virtual {p0}, Lfo;->f()B

    move-result v1

    and-int/lit16 v1, v1, 0xff

    or-int/2addr v0, v1

    :goto_0
    return v0

    :cond_1
    iget-object v3, v1, Lfy;->a:[B

    add-int/lit8 v4, v0, 0x1

    aget-byte v0, v3, v0

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x18

    add-int/lit8 v5, v4, 0x1

    aget-byte v4, v3, v4

    and-int/lit16 v4, v4, 0xff

    shl-int/lit8 v4, v4, 0x10

    or-int/2addr v0, v4

    add-int/lit8 v4, v5, 0x1

    aget-byte v5, v3, v5

    and-int/lit16 v5, v5, 0xff

    shl-int/lit8 v5, v5, 0x8

    or-int/2addr v0, v5

    add-int/lit8 v5, v4, 0x1

    aget-byte v3, v3, v4

    and-int/lit16 v3, v3, 0xff

    or-int/2addr v0, v3

    iget-wide v3, p0, Lfo;->b:J

    sub-long/2addr v3, v6

    iput-wide v3, p0, Lfo;->b:J

    if-ne v5, v2, :cond_2

    invoke-virtual {v1}, Lfy;->a()Lfy;

    move-result-object v2

    iput-object v2, p0, Lfo;->a:Lfy;

    sget-object v2, Lfz;->a:Lfz;

    invoke-virtual {v2, v1}, Lfz;->a(Lfy;)V

    goto :goto_0

    :cond_2
    iput v5, v1, Lfy;->b:I

    goto :goto_0
.end method

.method public j()I
    .locals 1

    invoke-virtual {p0}, Lfo;->i()I

    move-result v0

    invoke-static {v0}, Lgc;->a(I)I

    move-result v0

    return v0
.end method

.method public k()Ljava/io/InputStream;
    .locals 1

    new-instance v0, Lfq;

    invoke-direct {v0, p0}, Lfq;-><init>(Lfo;)V

    return-object v0
.end method

.method public l()J
    .locals 2

    iget-wide v0, p0, Lfo;->b:J

    return-wide v0
.end method

.method public m()Lfo;
    .locals 0

    return-object p0
.end method

.method public n()J
    .locals 5

    const-wide/16 v2, 0x0

    iget-wide v0, p0, Lfo;->b:J

    cmp-long v4, v0, v2

    if-nez v4, :cond_1

    move-wide v0, v2

    :cond_0
    :goto_0
    return-wide v0

    :cond_1
    iget-object v2, p0, Lfo;->a:Lfy;

    iget-object v2, v2, Lfy;->e:Lfy;

    iget v3, v2, Lfy;->c:I

    const/16 v4, 0x800

    if-ge v3, v4, :cond_0

    iget v3, v2, Lfy;->c:I

    iget v2, v2, Lfy;->b:I

    sub-int v2, v3, v2

    int-to-long v2, v2

    sub-long/2addr v0, v2

    goto :goto_0
.end method

.method public o()V
    .locals 2

    iget-wide v0, p0, Lfo;->b:J

    invoke-virtual {p0, v0, v1}, Lfo;->b(J)V

    return-void
.end method

.method public p()Lfo;
    .locals 6

    new-instance v1, Lfo;

    invoke-direct {v1}, Lfo;-><init>()V

    invoke-virtual {p0}, Lfo;->l()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-nez v0, :cond_0

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lfo;->a:Lfy;

    iget-object v0, v0, Lfy;->a:[B

    iget-object v2, p0, Lfo;->a:Lfy;

    iget v2, v2, Lfy;->b:I

    iget-object v3, p0, Lfo;->a:Lfy;

    iget v3, v3, Lfy;->c:I

    iget-object v4, p0, Lfo;->a:Lfy;

    iget v4, v4, Lfy;->b:I

    sub-int/2addr v3, v4

    invoke-virtual {v1, v0, v2, v3}, Lfo;->c([BII)Lfo;

    iget-object v0, p0, Lfo;->a:Lfy;

    iget-object v0, v0, Lfy;->d:Lfy;

    :goto_1
    iget-object v2, p0, Lfo;->a:Lfy;

    if-eq v0, v2, :cond_1

    iget-object v2, v0, Lfy;->a:[B

    iget v3, v0, Lfy;->b:I

    iget v4, v0, Lfy;->c:I

    iget v5, v0, Lfy;->b:I

    sub-int/2addr v4, v5

    invoke-virtual {v1, v2, v3, v4}, Lfo;->c([BII)Lfo;

    iget-object v0, v0, Lfy;->d:Lfy;

    goto :goto_1

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 7

    const/4 v4, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    iget-wide v0, p0, Lfo;->b:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const-string/jumbo v0, "OkBuffer[size=0]"

    :goto_0
    return-object v0

    :cond_0
    iget-wide v0, p0, Lfo;->b:J

    const-wide/16 v2, 0x10

    cmp-long v0, v0, v2

    if-gtz v0, :cond_1

    invoke-virtual {p0}, Lfo;->p()Lfo;

    move-result-object v0

    iget-wide v1, p0, Lfo;->b:J

    invoke-virtual {v0, v1, v2}, Lfo;->c(J)Lfi;

    move-result-object v0

    const-string/jumbo v1, "OkBuffer[size=%s data=%s]"

    new-array v2, v4, [Ljava/lang/Object;

    iget-wide v3, p0, Lfo;->b:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v0}, Lfi;->c()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    :try_start_0
    const-string/jumbo v0, "MD5"

    invoke-static {v0}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v1

    iget-object v0, p0, Lfo;->a:Lfy;

    iget-object v0, v0, Lfy;->a:[B

    iget-object v2, p0, Lfo;->a:Lfy;

    iget v2, v2, Lfy;->b:I

    iget-object v3, p0, Lfo;->a:Lfy;

    iget v3, v3, Lfy;->c:I

    iget-object v4, p0, Lfo;->a:Lfy;

    iget v4, v4, Lfy;->b:I

    sub-int/2addr v3, v4

    invoke-virtual {v1, v0, v2, v3}, Ljava/security/MessageDigest;->update([BII)V

    iget-object v0, p0, Lfo;->a:Lfy;

    iget-object v0, v0, Lfy;->d:Lfy;

    :goto_1
    iget-object v2, p0, Lfo;->a:Lfy;

    if-eq v0, v2, :cond_2

    iget-object v2, v0, Lfy;->a:[B

    iget v3, v0, Lfy;->b:I

    iget v4, v0, Lfy;->c:I

    iget v5, v0, Lfy;->b:I

    sub-int/2addr v4, v5

    invoke-virtual {v1, v2, v3, v4}, Ljava/security/MessageDigest;->update([BII)V

    iget-object v0, v0, Lfy;->d:Lfy;

    goto :goto_1

    :cond_2
    const-string/jumbo v0, "OkBuffer[size=%s md5=%s]"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-wide v4, p0, Lfo;->b:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {v1}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v1

    invoke-static {v1}, Lfi;->a([B)Lfi;

    move-result-object v1

    invoke-virtual {v1}, Lfi;->c()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v3

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method
