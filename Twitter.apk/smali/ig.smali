.class public final Lig;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field public static final action_button:I = 0x7f09002c

.field public static final action_button_accept:I = 0x7f09002d

.field public static final action_button_accept_frame:I = 0x7f09002e

.field public static final action_button_deny:I = 0x7f09002f

.field public static final action_button_deny_frame:I = 0x7f090030

.field public static final action_button_frame:I = 0x7f090031

.field public static final always:I = 0x7f090026

.field public static final amplify_media_controller_controls:I = 0x7f090082

.field public static final bold:I = 0x7f090008

.field public static final bottom:I = 0x7f090003

.field public static final check_item:I = 0x7f090034

.field public static final clickRemove:I = 0x7f090010

.field public static final collapseActionView:I = 0x7f090028

.field public static final countdown:I = 0x7f090084

.field public static final decoupled:I = 0x7f09000f

.field public static final disabled:I = 0x7f09000a

.field public static final distribute_evenly:I = 0x7f090019

.field public static final distribute_remainder:I = 0x7f09001a

.field public static final divider:I = 0x7f090037

.field public static final down:I = 0x7f090017

.field public static final email_item:I = 0x7f090039

.field public static final extra_info:I = 0x7f09003c

.field public static final favorite_action_button:I = 0x7f09003d

.field public static final flingRemove:I = 0x7f090011

.field public static final footer_label:I = 0x7f090041

.field public static final footer_progress_bar:I = 0x7f090042

.field public static final fullExpand:I = 0x7f090023

.field public static final fullscreen:I = 0x7f090086

.field public static final headerBottom:I = 0x7f090006

.field public static final headerMiddle:I = 0x7f090005

.field public static final headerTop:I = 0x7f090004

.field public static final header_container:I = 0x7f09007c

.field public static final header_content:I = 0x7f090044

.field public static final home:I = 0x7f090045

.field public static final homeAsUp:I = 0x7f090021

.field public static final hybrid:I = 0x7f09001d

.field public static final icon:I = 0x7f09001f

.field public static final ifRoom:I = 0x7f090025

.field public static final indicator:I = 0x7f09000b

.field public static final italic:I = 0x7f090009

.field public static final left:I = 0x7f09000d

.field public static final mediacontroller_progress:I = 0x7f090088

.field public static final middle:I = 0x7f090002

.field public static final msg:I = 0x7f090081

.field public static final muted_item:I = 0x7f090048

.field public static final name_item:I = 0x7f090049

.field public static final never:I = 0x7f090024

.field public static final none:I = 0x7f090000

.field public static final normal:I = 0x7f090007

.field public static final number:I = 0x7f09000c

.field public static final onDown:I = 0x7f090012

.field public static final onLongPress:I = 0x7f090014

.field public static final onMove:I = 0x7f090013

.field public static final overflow:I = 0x7f09004b

.field public static final overflow_item_icon:I = 0x7f090207

.field public static final overflow_item_subtitle:I = 0x7f090209

.field public static final overflow_item_title:I = 0x7f090208

.field public static final pause:I = 0x7f090083

.field public static final photo:I = 0x7f090029

.field public static final player:I = 0x7f09002b

.field public static final profile_description_item:I = 0x7f09004c

.field public static final promoted:I = 0x7f09004d

.field public static final protected_item:I = 0x7f09004e

.field public static final right:I = 0x7f09000e

.field public static final satellite:I = 0x7f09001b

.field public static final screenname_item:I = 0x7f090054

.field public static final showHome:I = 0x7f090020

.field public static final showTitle:I = 0x7f090022

.field public static final sliding_panel_bottom_header_divider:I = 0x7f090056

.field public static final sliding_panel_content:I = 0x7f090057

.field public static final sliding_panel_top_header_divider:I = 0x7f090058

.field public static final social_byline:I = 0x7f090059

.field public static final summary:I = 0x7f09002a

.field public static final terrain:I = 0x7f09001c

.field public static final text:I = 0x7f09001e

.field public static final time:I = 0x7f090087

.field public static final time_current:I = 0x7f090085

.field public static final toolbar:I = 0x7f09005a

.field public static final top:I = 0x7f090001

.field public static final up:I = 0x7f090018

.field public static final user_checkbox:I = 0x7f09005d

.field public static final user_image:I = 0x7f09005e

.field public static final utilitybar:I = 0x7f09005f

.field public static final verified_item:I = 0x7f090060

.field public static final withText:I = 0x7f090027
