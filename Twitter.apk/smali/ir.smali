.class public Lir;
.super Lcom/twitter/library/service/o;
.source "Twttr"


# instance fields
.field private d:J

.field private volatile e:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V
    .locals 1

    const-class v0, Lir;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/service/o;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    return-void
.end method


# virtual methods
.method public a(J)Lir;
    .locals 0

    iput-wide p1, p0, Lir;->d:J

    return-object p0
.end method

.method protected b(Lcom/twitter/library/service/e;)V
    .locals 10

    invoke-virtual {p0}, Lir;->s()Lcom/twitter/library/service/p;

    move-result-object v2

    new-instance v4, Lcom/twitter/library/network/n;

    iget-object v0, v2, Lcom/twitter/library/service/p;->d:Lcom/twitter/library/network/OAuthToken;

    invoke-direct {v4, v0}, Lcom/twitter/library/network/n;-><init>(Lcom/twitter/library/network/OAuthToken;)V

    iget-wide v5, v2, Lcom/twitter/library/service/p;->c:J

    invoke-virtual {p0}, Lir;->w()Lcom/twitter/library/provider/az;

    move-result-object v3

    iget-object v0, p0, Lir;->m:Lcom/twitter/library/network/aa;

    iget-object v1, p0, Lir;->l:Landroid/content/Context;

    iget-wide v7, p0, Lir;->d:J

    const/4 v9, 0x0

    invoke-static/range {v0 .. v9}, Lcom/twitter/library/client/u;->a(Lcom/twitter/library/network/aa;Landroid/content/Context;Lcom/twitter/library/service/p;Lcom/twitter/library/provider/az;Lcom/twitter/library/network/a;JJLcom/twitter/library/api/TwitterUser;)Lcom/twitter/internal/network/HttpOperation;

    move-result-object v0

    iget-wide v1, p0, Lir;->d:J

    invoke-virtual {v3, v1, v2}, Lcom/twitter/library/provider/az;->h(J)I

    move-result v1

    iput v1, p0, Lir;->e:I

    if-eqz v0, :cond_0

    invoke-virtual {p1, v0}, Lcom/twitter/library/service/e;->a(Lcom/twitter/internal/network/HttpOperation;)V

    :cond_0
    return-void
.end method

.method public e()I
    .locals 1

    iget v0, p0, Lir;->e:I

    return v0
.end method
