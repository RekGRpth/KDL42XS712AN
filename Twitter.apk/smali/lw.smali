.class public Llw;
.super Lorg/spongycastle/asn1/k;


# instance fields
.field private a:Llu;

.field private b:Lorg/spongycastle/asn1/ao;


# direct methods
.method public constructor <init>(Llu;Lorg/spongycastle/asn1/d;)V
    .locals 1

    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    new-instance v0, Lorg/spongycastle/asn1/ao;

    invoke-direct {v0, p2}, Lorg/spongycastle/asn1/ao;-><init>(Lorg/spongycastle/asn1/d;)V

    iput-object v0, p0, Llw;->b:Lorg/spongycastle/asn1/ao;

    iput-object p1, p0, Llw;->a:Llu;

    return-void
.end method

.method public constructor <init>(Lorg/spongycastle/asn1/r;)V
    .locals 3

    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    invoke-virtual {p1}, Lorg/spongycastle/asn1/r;->e()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Bad sequence size: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lorg/spongycastle/asn1/r;->e()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {p1}, Lorg/spongycastle/asn1/r;->d()Ljava/util/Enumeration;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Llu;->a(Ljava/lang/Object;)Llu;

    move-result-object v1

    iput-object v1, p0, Llw;->a:Llu;

    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/ao;->a(Ljava/lang/Object;)Lorg/spongycastle/asn1/ao;

    move-result-object v0

    iput-object v0, p0, Llw;->b:Lorg/spongycastle/asn1/ao;

    return-void
.end method

.method public static a(Ljava/lang/Object;)Llw;
    .locals 2

    instance-of v0, p0, Llw;

    if-eqz v0, :cond_0

    check-cast p0, Llw;

    :goto_0
    return-object p0

    :cond_0
    if-eqz p0, :cond_1

    new-instance v0, Llw;

    invoke-static {p0}, Lorg/spongycastle/asn1/r;->a(Ljava/lang/Object;)Lorg/spongycastle/asn1/r;

    move-result-object v1

    invoke-direct {v0, v1}, Llw;-><init>(Lorg/spongycastle/asn1/r;)V

    move-object p0, v0

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()Lorg/spongycastle/asn1/q;
    .locals 2

    new-instance v0, Lorg/spongycastle/asn1/e;

    invoke-direct {v0}, Lorg/spongycastle/asn1/e;-><init>()V

    iget-object v1, p0, Llw;->a:Llu;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    iget-object v1, p0, Llw;->b:Lorg/spongycastle/asn1/ao;

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    new-instance v1, Lorg/spongycastle/asn1/bf;

    invoke-direct {v1, v0}, Lorg/spongycastle/asn1/bf;-><init>(Lorg/spongycastle/asn1/e;)V

    return-object v1
.end method

.method public c()Llu;
    .locals 1

    iget-object v0, p0, Llw;->a:Llu;

    return-object v0
.end method

.method public d()Lorg/spongycastle/asn1/q;
    .locals 2

    new-instance v0, Lorg/spongycastle/asn1/h;

    iget-object v1, p0, Llw;->b:Lorg/spongycastle/asn1/ao;

    invoke-virtual {v1}, Lorg/spongycastle/asn1/ao;->c()[B

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/h;-><init>([B)V

    invoke-virtual {v0}, Lorg/spongycastle/asn1/h;->d()Lorg/spongycastle/asn1/q;

    move-result-object v0

    return-object v0
.end method
