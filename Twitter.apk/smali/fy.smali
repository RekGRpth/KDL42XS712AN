.class final Lfy;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field final a:[B

.field b:I

.field c:I

.field d:Lfy;

.field e:Lfy;


# direct methods
.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x800

    new-array v0, v0, [B

    iput-object v0, p0, Lfy;->a:[B

    return-void
.end method


# virtual methods
.method public a()Lfy;
    .locals 4

    const/4 v1, 0x0

    iget-object v0, p0, Lfy;->d:Lfy;

    if-eq v0, p0, :cond_0

    iget-object v0, p0, Lfy;->d:Lfy;

    :goto_0
    iget-object v2, p0, Lfy;->e:Lfy;

    iget-object v3, p0, Lfy;->d:Lfy;

    iput-object v3, v2, Lfy;->d:Lfy;

    iget-object v2, p0, Lfy;->d:Lfy;

    iget-object v3, p0, Lfy;->e:Lfy;

    iput-object v3, v2, Lfy;->e:Lfy;

    iput-object v1, p0, Lfy;->d:Lfy;

    iput-object v1, p0, Lfy;->e:Lfy;

    return-object v0

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method public a(I)Lfy;
    .locals 6

    iget v0, p0, Lfy;->c:I

    iget v1, p0, Lfy;->b:I

    sub-int/2addr v0, v1

    sub-int/2addr v0, p1

    if-lez p1, :cond_0

    if-gtz v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_1
    if-ge p1, v0, :cond_2

    sget-object v0, Lfz;->a:Lfz;

    invoke-virtual {v0}, Lfz;->a()Lfy;

    move-result-object v0

    iget-object v1, p0, Lfy;->a:[B

    iget v2, p0, Lfy;->b:I

    iget-object v3, v0, Lfy;->a:[B

    iget v4, v0, Lfy;->b:I

    invoke-static {v1, v2, v3, v4, p1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget v1, p0, Lfy;->b:I

    add-int/2addr v1, p1

    iput v1, p0, Lfy;->b:I

    iget v1, v0, Lfy;->c:I

    add-int/2addr v1, p1

    iput v1, v0, Lfy;->c:I

    iget-object v1, p0, Lfy;->e:Lfy;

    invoke-virtual {v1, v0}, Lfy;->a(Lfy;)Lfy;

    move-object p0, v0

    :goto_0
    return-object p0

    :cond_2
    sget-object v1, Lfz;->a:Lfz;

    invoke-virtual {v1}, Lfz;->a()Lfy;

    move-result-object v1

    iget-object v2, p0, Lfy;->a:[B

    iget v3, p0, Lfy;->b:I

    add-int/2addr v3, p1

    iget-object v4, v1, Lfy;->a:[B

    iget v5, v1, Lfy;->b:I

    invoke-static {v2, v3, v4, v5, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget v2, p0, Lfy;->c:I

    sub-int/2addr v2, v0

    iput v2, p0, Lfy;->c:I

    iget v2, v1, Lfy;->c:I

    add-int/2addr v0, v2

    iput v0, v1, Lfy;->c:I

    invoke-virtual {p0, v1}, Lfy;->a(Lfy;)Lfy;

    goto :goto_0
.end method

.method public a(Lfy;)Lfy;
    .locals 1

    iput-object p0, p1, Lfy;->e:Lfy;

    iget-object v0, p0, Lfy;->d:Lfy;

    iput-object v0, p1, Lfy;->d:Lfy;

    iget-object v0, p0, Lfy;->d:Lfy;

    iput-object p1, v0, Lfy;->e:Lfy;

    iput-object p1, p0, Lfy;->d:Lfy;

    return-object p1
.end method

.method public a(Lfy;I)V
    .locals 6

    const/16 v2, 0x800

    const/4 v5, 0x0

    iget v0, p1, Lfy;->c:I

    iget v1, p1, Lfy;->b:I

    sub-int/2addr v0, v1

    add-int/2addr v0, p2

    if-le v0, v2, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    iget v0, p1, Lfy;->c:I

    add-int/2addr v0, p2

    if-le v0, v2, :cond_1

    iget-object v0, p1, Lfy;->a:[B

    iget v1, p1, Lfy;->b:I

    iget-object v2, p1, Lfy;->a:[B

    iget v3, p1, Lfy;->c:I

    iget v4, p1, Lfy;->b:I

    sub-int/2addr v3, v4

    invoke-static {v0, v1, v2, v5, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget v0, p1, Lfy;->c:I

    iget v1, p1, Lfy;->b:I

    sub-int/2addr v0, v1

    iput v0, p1, Lfy;->c:I

    iput v5, p1, Lfy;->b:I

    :cond_1
    iget-object v0, p0, Lfy;->a:[B

    iget v1, p0, Lfy;->b:I

    iget-object v2, p1, Lfy;->a:[B

    iget v3, p1, Lfy;->c:I

    invoke-static {v0, v1, v2, v3, p2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget v0, p1, Lfy;->c:I

    add-int/2addr v0, p2

    iput v0, p1, Lfy;->c:I

    iget v0, p0, Lfy;->b:I

    add-int/2addr v0, p2

    iput v0, p0, Lfy;->b:I

    return-void
.end method

.method public b()V
    .locals 3

    iget-object v0, p0, Lfy;->e:Lfy;

    if-ne v0, p0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lfy;->e:Lfy;

    iget v0, v0, Lfy;->c:I

    iget-object v1, p0, Lfy;->e:Lfy;

    iget v1, v1, Lfy;->b:I

    sub-int/2addr v0, v1

    iget v1, p0, Lfy;->c:I

    iget v2, p0, Lfy;->b:I

    sub-int/2addr v1, v2

    add-int/2addr v0, v1

    const/16 v1, 0x800

    if-le v0, v1, :cond_1

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lfy;->e:Lfy;

    iget v1, p0, Lfy;->c:I

    iget v2, p0, Lfy;->b:I

    sub-int/2addr v1, v2

    invoke-virtual {p0, v0, v1}, Lfy;->a(Lfy;I)V

    invoke-virtual {p0}, Lfy;->a()Lfy;

    sget-object v0, Lfz;->a:Lfz;

    invoke-virtual {v0, p0}, Lfz;->a(Lfy;)V

    goto :goto_0
.end method
