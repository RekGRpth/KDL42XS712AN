.class public Lit;
.super Lcom/twitter/library/service/o;
.source "Twttr"


# instance fields
.field private d:I

.field private e:I

.field private f:J

.field private g:Ljava/lang/String;

.field private n:Lcom/twitter/library/api/ao;

.field private o:Z

.field private p:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V
    .locals 1

    const-class v0, Lit;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/service/o;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lit;->o:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/service/p;)V
    .locals 1

    const-class v0, Lit;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/service/o;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/service/p;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lit;->o:Z

    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/library/service/e;)Lcom/twitter/internal/network/HttpOperation;
    .locals 12

    const/4 v11, 0x3

    const/4 v1, 0x2

    const/4 v6, 0x1

    const/4 v7, 0x0

    invoke-virtual {p0}, Lit;->s()Lcom/twitter/library/service/p;

    move-result-object v8

    iget-wide v9, v8, Lcom/twitter/library/service/p;->c:J

    invoke-virtual {p0}, Lit;->w()Lcom/twitter/library/provider/az;

    move-result-object v0

    iget v2, p0, Lit;->d:I

    iget-wide v3, p0, Lit;->f:J

    iget v5, p0, Lit;->e:I

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/library/provider/az;->a(IIJI)Ljava/lang/String;

    move-result-object v2

    iget v0, p0, Lit;->d:I

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Invalid list type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lit;->d:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    iget-object v0, p0, Lit;->m:Lcom/twitter/library/network/aa;

    iget-object v0, v0, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    new-array v3, v11, [Ljava/lang/Object;

    const-string/jumbo v4, "1.1"

    aput-object v4, v3, v7

    const-string/jumbo v4, "lists"

    aput-object v4, v3, v6

    const-string/jumbo v4, "ownerships"

    aput-object v4, v3, v1

    invoke-static {v0, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v3, p0, Lit;->f:J

    cmp-long v1, v3, v9

    if-nez v1, :cond_3

    move-object v1, v0

    move v0, v6

    :goto_0
    iget-wide v3, p0, Lit;->f:J

    const-wide/16 v5, 0x0

    cmp-long v3, v3, v5

    if-lez v3, :cond_0

    const-string/jumbo v3, "user_id"

    iget-wide v4, p0, Lit;->f:J

    invoke-static {v1, v3, v4, v5}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;J)V

    :cond_0
    iget-object v3, p0, Lit;->g:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string/jumbo v3, "screen_name"

    iget-object v4, p0, Lit;->g:Ljava/lang/String;

    invoke-static {v1, v3, v4}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    if-eqz v2, :cond_2

    const-string/jumbo v3, "cursor"

    invoke-static {v1, v3, v2}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    new-instance v2, Lcom/twitter/library/util/f;

    iget-object v3, p0, Lit;->l:Landroid/content/Context;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v9, v10, v4}, Lcom/twitter/library/util/f;-><init>(Landroid/content/Context;JLjava/lang/String;)V

    invoke-static {v7, v0, v2}, Lcom/twitter/library/api/ao;->a(ZILcom/twitter/library/util/w;)Lcom/twitter/library/api/ao;

    move-result-object v0

    iput-object v0, p0, Lit;->n:Lcom/twitter/library/api/ao;

    new-instance v0, Lcom/twitter/library/network/d;

    iget-object v2, p0, Lit;->l:Landroid/content/Context;

    invoke-direct {v0, v2, v1}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v9, v10}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/network/n;

    iget-object v2, v8, Lcom/twitter/library/service/p;->d:Lcom/twitter/library/network/OAuthToken;

    invoke-direct {v1, v2}, Lcom/twitter/library/network/n;-><init>(Lcom/twitter/library/network/OAuthToken;)V

    invoke-virtual {v0, v1}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v0

    iget-object v1, p0, Lit;->n:Lcom/twitter/library/api/ao;

    invoke-virtual {v0, v1}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v0

    return-object v0

    :cond_3
    move-object v1, v0

    move v0, v7

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lit;->m:Lcom/twitter/library/network/aa;

    iget-object v0, v0, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    new-array v3, v11, [Ljava/lang/Object;

    const-string/jumbo v4, "1.1"

    aput-object v4, v3, v7

    const-string/jumbo v4, "lists"

    aput-object v4, v3, v6

    const-string/jumbo v4, "memberships"

    aput-object v4, v3, v1

    invoke-static {v0, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    move-object v1, v0

    move v0, v7

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lit;->m:Lcom/twitter/library/network/aa;

    iget-object v0, v0, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    new-array v3, v11, [Ljava/lang/Object;

    const-string/jumbo v4, "1.1"

    aput-object v4, v3, v7

    const-string/jumbo v4, "lists"

    aput-object v4, v3, v6

    const-string/jumbo v4, "subscriptions"

    aput-object v4, v3, v1

    invoke-static {v0, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v3, p0, Lit;->f:J

    cmp-long v1, v3, v9

    if-nez v1, :cond_4

    move-object v1, v0

    move v0, v6

    goto/16 :goto_0

    :cond_4
    move-object v1, v0

    move v0, v7

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public a(I)Lit;
    .locals 0

    iput p1, p0, Lit;->d:I

    return-object p0
.end method

.method public a(J)Lit;
    .locals 0

    iput-wide p1, p0, Lit;->f:J

    return-object p0
.end method

.method public a(Ljava/lang/String;)Lit;
    .locals 0

    iput-object p1, p0, Lit;->g:Ljava/lang/String;

    return-object p0
.end method

.method public a(Z)Lit;
    .locals 0

    iput-boolean p1, p0, Lit;->o:Z

    return-object p0
.end method

.method protected final a(Lcom/twitter/internal/network/HttpOperation;Lcom/twitter/library/service/e;)V
    .locals 8

    const/4 v6, 0x0

    invoke-virtual {p1}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lit;->n:Lcom/twitter/library/api/ao;

    invoke-virtual {v0}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/twitter/library/api/aj;

    if-nez v5, :cond_1

    invoke-virtual {p1}, Lcom/twitter/internal/network/HttpOperation;->l()Lcom/twitter/internal/network/k;

    move-result-object v0

    iput v6, v0, Lcom/twitter/internal/network/k;->a:I

    :cond_0
    :goto_0
    invoke-virtual {p2, p1}, Lcom/twitter/library/service/e;->a(Lcom/twitter/internal/network/HttpOperation;)V

    return-void

    :cond_1
    invoke-virtual {v5}, Lcom/twitter/library/api/aj;->b()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    const-string/jumbo v0, "0"

    invoke-virtual {v5}, Lcom/twitter/library/api/aj;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    :goto_1
    iput-boolean v0, p0, Lit;->p:Z

    invoke-virtual {p0}, Lit;->w()Lcom/twitter/library/provider/az;

    move-result-object v0

    invoke-virtual {v5}, Lcom/twitter/library/api/aj;->b()Ljava/util/ArrayList;

    move-result-object v1

    iget-wide v2, p0, Lit;->f:J

    iget v4, p0, Lit;->d:I

    invoke-virtual {v5}, Lcom/twitter/library/api/aj;->a()Ljava/lang/String;

    move-result-object v5

    iget v7, p0, Lit;->e:I

    if-nez v7, :cond_2

    const/4 v6, 0x1

    :cond_2
    iget-boolean v7, p0, Lit;->o:Z

    invoke-virtual/range {v0 .. v7}, Lcom/twitter/library/provider/az;->a(Ljava/util/Collection;JILjava/lang/String;ZZ)V

    goto :goto_0

    :cond_3
    move v0, v6

    goto :goto_1
.end method

.method public b(I)Lit;
    .locals 0

    iput p1, p0, Lit;->e:I

    return-object p0
.end method

.method public e()Z
    .locals 1

    iget-boolean v0, p0, Lit;->p:Z

    return v0
.end method
