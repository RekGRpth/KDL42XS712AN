.class public final Lfm;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lgb;


# instance fields
.field private a:I

.field private final b:Lfh;

.field private final c:Ljava/util/zip/Inflater;

.field private final d:Lfn;

.field private final e:Ljava/util/zip/CRC32;


# direct methods
.method public constructor <init>(Lgb;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lfm;->a:I

    new-instance v0, Ljava/util/zip/CRC32;

    invoke-direct {v0}, Ljava/util/zip/CRC32;-><init>()V

    iput-object v0, p0, Lfm;->e:Ljava/util/zip/CRC32;

    new-instance v0, Ljava/util/zip/Inflater;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/zip/Inflater;-><init>(Z)V

    iput-object v0, p0, Lfm;->c:Ljava/util/zip/Inflater;

    invoke-static {p1}, Lfr;->a(Lgb;)Lfh;

    move-result-object v0

    iput-object v0, p0, Lfm;->b:Lfh;

    new-instance v0, Lfn;

    iget-object v1, p0, Lfm;->b:Lfh;

    iget-object v2, p0, Lfm;->c:Ljava/util/zip/Inflater;

    invoke-direct {v0, v1, v2}, Lfn;-><init>(Lfh;Ljava/util/zip/Inflater;)V

    iput-object v0, p0, Lfm;->d:Lfn;

    return-void
.end method

.method private a()V
    .locals 14

    const-wide/16 v10, 0x2

    const/4 v8, 0x0

    const-wide/16 v12, 0x1

    const-wide/16 v2, 0x0

    const/4 v7, 0x1

    iget-object v0, p0, Lfm;->b:Lfh;

    const-wide/16 v4, 0xa

    invoke-interface {v0, v4, v5}, Lfh;->a(J)V

    iget-object v0, p0, Lfm;->b:Lfh;

    invoke-interface {v0}, Lfh;->b()Lfo;

    move-result-object v0

    const-wide/16 v4, 0x3

    invoke-virtual {v0, v4, v5}, Lfo;->d(J)B

    move-result v9

    shr-int/lit8 v0, v9, 0x1

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v7, :cond_9

    move v6, v7

    :goto_0
    if-eqz v6, :cond_0

    iget-object v0, p0, Lfm;->b:Lfh;

    invoke-interface {v0}, Lfh;->b()Lfo;

    move-result-object v1

    const-wide/16 v4, 0xa

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lfm;->a(Lfo;JJ)V

    :cond_0
    iget-object v0, p0, Lfm;->b:Lfh;

    invoke-interface {v0}, Lfh;->g()S

    move-result v0

    const-string/jumbo v1, "ID1ID2"

    const/16 v4, 0x1f8b

    invoke-direct {p0, v1, v4, v0}, Lfm;->a(Ljava/lang/String;II)V

    iget-object v0, p0, Lfm;->b:Lfh;

    const-wide/16 v4, 0x8

    invoke-interface {v0, v4, v5}, Lfh;->b(J)V

    shr-int/lit8 v0, v9, 0x2

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v7, :cond_3

    iget-object v0, p0, Lfm;->b:Lfh;

    invoke-interface {v0, v10, v11}, Lfh;->a(J)V

    if-eqz v6, :cond_1

    iget-object v0, p0, Lfm;->b:Lfh;

    invoke-interface {v0}, Lfh;->b()Lfo;

    move-result-object v1

    move-object v0, p0

    move-wide v4, v10

    invoke-direct/range {v0 .. v5}, Lfm;->a(Lfo;JJ)V

    :cond_1
    iget-object v0, p0, Lfm;->b:Lfh;

    invoke-interface {v0}, Lfh;->b()Lfo;

    move-result-object v0

    invoke-virtual {v0}, Lfo;->h()I

    move-result v0

    const v1, 0xffff

    and-int v10, v0, v1

    iget-object v0, p0, Lfm;->b:Lfh;

    int-to-long v4, v10

    invoke-interface {v0, v4, v5}, Lfh;->a(J)V

    if-eqz v6, :cond_2

    iget-object v0, p0, Lfm;->b:Lfh;

    invoke-interface {v0}, Lfh;->b()Lfo;

    move-result-object v1

    int-to-long v4, v10

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lfm;->a(Lfo;JJ)V

    :cond_2
    iget-object v0, p0, Lfm;->b:Lfh;

    int-to-long v4, v10

    invoke-interface {v0, v4, v5}, Lfh;->b(J)V

    :cond_3
    shr-int/lit8 v0, v9, 0x3

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v7, :cond_5

    iget-object v0, p0, Lfm;->b:Lfh;

    invoke-interface {v0, v8}, Lfh;->a(B)J

    move-result-wide v10

    if-eqz v6, :cond_4

    iget-object v0, p0, Lfm;->b:Lfh;

    invoke-interface {v0}, Lfh;->b()Lfo;

    move-result-object v1

    add-long v4, v10, v12

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lfm;->a(Lfo;JJ)V

    :cond_4
    iget-object v0, p0, Lfm;->b:Lfh;

    add-long v4, v10, v12

    invoke-interface {v0, v4, v5}, Lfh;->b(J)V

    :cond_5
    shr-int/lit8 v0, v9, 0x4

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v7, :cond_7

    iget-object v0, p0, Lfm;->b:Lfh;

    invoke-interface {v0, v8}, Lfh;->a(B)J

    move-result-wide v7

    if-eqz v6, :cond_6

    iget-object v0, p0, Lfm;->b:Lfh;

    invoke-interface {v0}, Lfh;->b()Lfo;

    move-result-object v1

    add-long v4, v7, v12

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lfm;->a(Lfo;JJ)V

    :cond_6
    iget-object v0, p0, Lfm;->b:Lfh;

    add-long v1, v7, v12

    invoke-interface {v0, v1, v2}, Lfh;->b(J)V

    :cond_7
    if-eqz v6, :cond_8

    const-string/jumbo v0, "FHCRC"

    iget-object v1, p0, Lfm;->b:Lfh;

    invoke-interface {v1}, Lfh;->h()I

    move-result v1

    iget-object v2, p0, Lfm;->e:Ljava/util/zip/CRC32;

    invoke-virtual {v2}, Ljava/util/zip/CRC32;->getValue()J

    move-result-wide v2

    long-to-int v2, v2

    int-to-short v2, v2

    invoke-direct {p0, v0, v1, v2}, Lfm;->a(Ljava/lang/String;II)V

    iget-object v0, p0, Lfm;->e:Ljava/util/zip/CRC32;

    invoke-virtual {v0}, Ljava/util/zip/CRC32;->reset()V

    :cond_8
    return-void

    :cond_9
    move v6, v8

    goto/16 :goto_0
.end method

.method private a(Lfo;JJ)V
    .locals 9

    iget-object v0, p1, Lfo;->a:Lfy;

    move-object v2, v0

    move-wide v0, p4

    :goto_0
    const-wide/16 v3, 0x0

    cmp-long v3, v0, v3

    if-lez v3, :cond_1

    iget v3, v2, Lfy;->c:I

    iget v4, v2, Lfy;->b:I

    sub-int/2addr v3, v4

    int-to-long v4, v3

    cmp-long v4, p2, v4

    if-gez v4, :cond_0

    int-to-long v4, v3

    sub-long/2addr v4, p2

    invoke-static {v0, v1, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    long-to-int v4, v4

    iget-object v5, p0, Lfm;->e:Ljava/util/zip/CRC32;

    iget-object v6, v2, Lfy;->a:[B

    iget v7, v2, Lfy;->b:I

    int-to-long v7, v7

    add-long/2addr v7, p2

    long-to-int v7, v7

    invoke-virtual {v5, v6, v7, v4}, Ljava/util/zip/CRC32;->update([BII)V

    int-to-long v4, v4

    sub-long/2addr v0, v4

    :cond_0
    int-to-long v3, v3

    sub-long/2addr p2, v3

    iget-object v2, v2, Lfy;->d:Lfy;

    goto :goto_0

    :cond_1
    return-void
.end method

.method private a(Ljava/lang/String;II)V
    .locals 5

    if-eq p3, p2, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v1, "%s: actual 0x%08x != expected 0x%08x"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method private b()V
    .locals 4

    const-string/jumbo v0, "CRC"

    iget-object v1, p0, Lfm;->b:Lfh;

    invoke-interface {v1}, Lfh;->j()I

    move-result v1

    iget-object v2, p0, Lfm;->e:Ljava/util/zip/CRC32;

    invoke-virtual {v2}, Ljava/util/zip/CRC32;->getValue()J

    move-result-wide v2

    long-to-int v2, v2

    invoke-direct {p0, v0, v1, v2}, Lfm;->a(Ljava/lang/String;II)V

    const-string/jumbo v0, "ISIZE"

    iget-object v1, p0, Lfm;->b:Lfh;

    invoke-interface {v1}, Lfh;->j()I

    move-result v1

    iget-object v2, p0, Lfm;->c:Ljava/util/zip/Inflater;

    invoke-virtual {v2}, Ljava/util/zip/Inflater;->getTotalOut()I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lfm;->a(Ljava/lang/String;II)V

    return-void
.end method


# virtual methods
.method public b(Lfo;J)J
    .locals 8

    const-wide/16 v0, -0x1

    const/4 v7, 0x2

    const/4 v3, 0x1

    const-wide/16 v4, 0x0

    cmp-long v2, p2, v4

    if-gez v2, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "byteCount < 0: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    cmp-long v2, p2, v4

    if-nez v2, :cond_1

    :goto_0
    return-wide v4

    :cond_1
    iget v2, p0, Lfm;->a:I

    if-nez v2, :cond_2

    invoke-direct {p0}, Lfm;->a()V

    iput v3, p0, Lfm;->a:I

    :cond_2
    iget v2, p0, Lfm;->a:I

    if-ne v2, v3, :cond_4

    iget-wide v2, p1, Lfo;->b:J

    iget-object v4, p0, Lfm;->d:Lfn;

    invoke-virtual {v4, p1, p2, p3}, Lfn;->b(Lfo;J)J

    move-result-wide v4

    cmp-long v6, v4, v0

    if-eqz v6, :cond_3

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lfm;->a(Lfo;JJ)V

    goto :goto_0

    :cond_3
    iput v7, p0, Lfm;->a:I

    :cond_4
    iget v2, p0, Lfm;->a:I

    if-ne v2, v7, :cond_5

    invoke-direct {p0}, Lfm;->b()V

    const/4 v2, 0x3

    iput v2, p0, Lfm;->a:I

    iget-object v2, p0, Lfm;->b:Lfh;

    invoke-interface {v2}, Lfh;->e()Z

    move-result v2

    if-nez v2, :cond_5

    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v1, "gzip finished without exhausting source"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    move-wide v4, v0

    goto :goto_0
.end method

.method public close()V
    .locals 1

    iget-object v0, p0, Lfm;->d:Lfn;

    invoke-virtual {v0}, Lfn;->close()V

    return-void
.end method
