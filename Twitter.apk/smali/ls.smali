.class public Lls;
.super Lorg/spongycastle/asn1/k;


# instance fields
.field private a:Ljava/math/BigInteger;

.field private b:Ljava/math/BigInteger;


# direct methods
.method public constructor <init>(Ljava/math/BigInteger;Ljava/math/BigInteger;)V
    .locals 0

    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    iput-object p1, p0, Lls;->a:Ljava/math/BigInteger;

    iput-object p2, p0, Lls;->b:Ljava/math/BigInteger;

    return-void
.end method

.method private constructor <init>(Lorg/spongycastle/asn1/r;)V
    .locals 3

    invoke-direct {p0}, Lorg/spongycastle/asn1/k;-><init>()V

    invoke-virtual {p1}, Lorg/spongycastle/asn1/r;->e()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Bad sequence size: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lorg/spongycastle/asn1/r;->e()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {p1}, Lorg/spongycastle/asn1/r;->d()Ljava/util/Enumeration;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Lorg/spongycastle/asn1/i;->a(Ljava/lang/Object;)Lorg/spongycastle/asn1/i;

    move-result-object v1

    invoke-virtual {v1}, Lorg/spongycastle/asn1/i;->d()Ljava/math/BigInteger;

    move-result-object v1

    iput-object v1, p0, Lls;->a:Ljava/math/BigInteger;

    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lorg/spongycastle/asn1/i;->a(Ljava/lang/Object;)Lorg/spongycastle/asn1/i;

    move-result-object v0

    invoke-virtual {v0}, Lorg/spongycastle/asn1/i;->d()Ljava/math/BigInteger;

    move-result-object v0

    iput-object v0, p0, Lls;->b:Ljava/math/BigInteger;

    return-void
.end method

.method public static a(Ljava/lang/Object;)Lls;
    .locals 2

    instance-of v0, p0, Lls;

    if-eqz v0, :cond_0

    check-cast p0, Lls;

    :goto_0
    return-object p0

    :cond_0
    if-eqz p0, :cond_1

    new-instance v0, Lls;

    invoke-static {p0}, Lorg/spongycastle/asn1/r;->a(Ljava/lang/Object;)Lorg/spongycastle/asn1/r;

    move-result-object v1

    invoke-direct {v0, v1}, Lls;-><init>(Lorg/spongycastle/asn1/r;)V

    move-object p0, v0

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()Lorg/spongycastle/asn1/q;
    .locals 3

    new-instance v0, Lorg/spongycastle/asn1/e;

    invoke-direct {v0}, Lorg/spongycastle/asn1/e;-><init>()V

    new-instance v1, Lorg/spongycastle/asn1/i;

    iget-object v2, p0, Lls;->a:Ljava/math/BigInteger;

    invoke-direct {v1, v2}, Lorg/spongycastle/asn1/i;-><init>(Ljava/math/BigInteger;)V

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    new-instance v1, Lorg/spongycastle/asn1/i;

    iget-object v2, p0, Lls;->b:Ljava/math/BigInteger;

    invoke-direct {v1, v2}, Lorg/spongycastle/asn1/i;-><init>(Ljava/math/BigInteger;)V

    invoke-virtual {v0, v1}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    new-instance v1, Lorg/spongycastle/asn1/bf;

    invoke-direct {v1, v0}, Lorg/spongycastle/asn1/bf;-><init>(Lorg/spongycastle/asn1/e;)V

    return-object v1
.end method

.method public c()Ljava/math/BigInteger;
    .locals 1

    iget-object v0, p0, Lls;->a:Ljava/math/BigInteger;

    return-object v0
.end method

.method public d()Ljava/math/BigInteger;
    .locals 1

    iget-object v0, p0, Lls;->b:Ljava/math/BigInteger;

    return-object v0
.end method
