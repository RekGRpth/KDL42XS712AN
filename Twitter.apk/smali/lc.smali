.class public final Llc;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field public static final RefreshableListView:[I

.field public static final RefreshableListView_loadingText:I = 0x3

.field public static final RefreshableListView_pullAfterHeaders:I = 0x9

.field public static final RefreshableListView_pullBackgroundColor:I = 0x5

.field public static final RefreshableListView_pullDivider:I = 0x6

.field public static final RefreshableListView_pullHeaderPosition:I = 0xa

.field public static final RefreshableListView_pullText:I = 0x2

.field public static final RefreshableListView_refreshFooter:I = 0x1

.field public static final RefreshableListView_refreshHeader:I = 0x0

.field public static final RefreshableListView_releaseText:I = 0x4

.field public static final RefreshableListView_rotateDownAnim:I = 0x8

.field public static final RefreshableListView_rotateUpAnim:I = 0x7


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0xb

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Llc;->RefreshableListView:[I

    return-void

    :array_0
    .array-data 4
        0x7f0100ec    # com.twitter.android.R.attr.refreshHeader
        0x7f0100ed    # com.twitter.android.R.attr.refreshFooter
        0x7f0100ee    # com.twitter.android.R.attr.pullText
        0x7f0100ef    # com.twitter.android.R.attr.loadingText
        0x7f0100f0    # com.twitter.android.R.attr.releaseText
        0x7f0100f1    # com.twitter.android.R.attr.pullBackgroundColor
        0x7f0100f2    # com.twitter.android.R.attr.pullDivider
        0x7f0100f3    # com.twitter.android.R.attr.rotateUpAnim
        0x7f0100f4    # com.twitter.android.R.attr.rotateDownAnim
        0x7f0100f5    # com.twitter.android.R.attr.pullAfterHeaders
        0x7f0100f6    # com.twitter.android.R.attr.pullHeaderPosition
    .end array-data
.end method
