.class public Ljt;
.super Lcom/twitter/library/service/o;
.source "Twttr"


# instance fields
.field private final d:Ljava/lang/String;

.field private final e:Lcom/twitter/library/api/search/j;

.field private final f:J

.field private g:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;)V
    .locals 2

    const-class v0, Ljt;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/service/o;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    iput-object p3, p0, Ljt;->d:Ljava/lang/String;

    new-instance v0, Lcom/twitter/library/api/search/j;

    invoke-direct {v0}, Lcom/twitter/library/api/search/j;-><init>()V

    iput-object v0, p0, Ljt;->e:Lcom/twitter/library/api/search/j;

    invoke-virtual {p2}, Lcom/twitter/library/client/Session;->j()Lcom/twitter/library/api/UserSettings;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-wide v0, v0, Lcom/twitter/library/api/UserSettings;->a:J

    iput-wide v0, p0, Ljt;->f:J

    :goto_0
    return-void

    :cond_0
    const-wide/16 v0, 0x1

    iput-wide v0, p0, Ljt;->f:J

    goto :goto_0
.end method


# virtual methods
.method protected a(Lcom/twitter/library/service/e;)Lcom/twitter/internal/network/HttpOperation;
    .locals 5

    const/4 v4, 0x1

    iget-object v0, p0, Ljt;->m:Lcom/twitter/library/network/aa;

    iget-object v0, v0, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string/jumbo v3, "1.1"

    aput-object v3, v1, v2

    const-string/jumbo v2, "trends"

    aput-object v2, v1, v4

    const/4 v2, 0x2

    const-string/jumbo v3, "timeline"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "woeid"

    iget-wide v2, p0, Ljt;->f:J

    invoke-static {v0, v1, v2, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;J)V

    const-string/jumbo v1, "timezone"

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Ljt;->k:Landroid/os/Bundle;

    const-string/jumbo v2, "lang"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string/jumbo v2, "lang"

    invoke-static {v0, v2, v1}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v1, p0, Ljt;->d:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Ljt;->d:Ljava/lang/String;

    const-string/jumbo v2, "TREND"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string/jumbo v1, "trend_types"

    iget-object v2, p0, Ljt;->d:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    const-string/jumbo v1, "pc"

    const-string/jumbo v2, "true"

    invoke-static {v0, v1, v2}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v1, "include_media_features"

    invoke-static {v0, v1, v4}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const-string/jumbo v1, "include_cards"

    invoke-static {v0, v1, v4}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    iget-object v1, p0, Ljt;->m:Lcom/twitter/library/network/aa;

    invoke-virtual {v1, v0}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;)V

    iget-object v1, p0, Ljt;->k:Landroid/os/Bundle;

    invoke-static {v1}, Ljs;->a(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-static {v0, v1, v4}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    :cond_2
    invoke-virtual {p0}, Ljt;->s()Lcom/twitter/library/service/p;

    move-result-object v1

    new-instance v2, Lcom/twitter/library/network/d;

    iget-object v3, p0, Ljt;->l:Landroid/content/Context;

    invoke-direct {v2, v3, v0}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    iget-wide v3, v1, Lcom/twitter/library/service/p;->c:J

    invoke-virtual {v2, v3, v4}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v0

    new-instance v2, Lcom/twitter/library/network/n;

    iget-object v1, v1, Lcom/twitter/library/service/p;->d:Lcom/twitter/library/network/OAuthToken;

    invoke-direct {v2, v1}, Lcom/twitter/library/network/n;-><init>(Lcom/twitter/library/network/OAuthToken;)V

    invoke-virtual {v0, v2}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v0

    iget-object v1, p0, Ljt;->e:Lcom/twitter/library/api/search/j;

    invoke-virtual {v0, v1}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lcom/twitter/internal/network/HttpOperation;Lcom/twitter/library/service/e;)V
    .locals 6

    invoke-virtual {p1}, Lcom/twitter/internal/network/HttpOperation;->k()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Ljt;->e:Lcom/twitter/library/api/search/j;

    invoke-virtual {v0}, Lcom/twitter/library/api/search/j;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/search/h;

    iget-object v1, v0, Lcom/twitter/library/api/search/h;->b:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Ljt;->w()Lcom/twitter/library/provider/az;

    move-result-object v1

    invoke-virtual {p0}, Ljt;->s()Lcom/twitter/library/service/p;

    move-result-object v2

    iget-wide v2, v2, Lcom/twitter/library/service/p;->c:J

    iget-object v4, v0, Lcom/twitter/library/api/search/h;->b:Ljava/util/ArrayList;

    iget-object v5, p0, Ljt;->d:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/twitter/library/provider/az;->a(JLjava/util/Collection;Ljava/lang/String;)I

    :cond_0
    iget-object v1, v0, Lcom/twitter/library/api/search/h;->a:Lcom/twitter/library/api/search/i;

    if-eqz v1, :cond_1

    iget-object v0, v0, Lcom/twitter/library/api/search/h;->a:Lcom/twitter/library/api/search/i;

    iget-boolean v0, v0, Lcom/twitter/library/api/search/i;->c:Z

    iput-boolean v0, p0, Ljt;->g:Z

    :cond_1
    invoke-virtual {p2, p1}, Lcom/twitter/library/service/e;->a(Lcom/twitter/internal/network/HttpOperation;)V

    return-void
.end method

.method public e()Z
    .locals 1

    iget-boolean v0, p0, Ljt;->g:Z

    return v0
.end method
