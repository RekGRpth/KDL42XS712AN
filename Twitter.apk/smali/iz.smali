.class public Liz;
.super Ljb;
.source "Twttr"


# instance fields
.field private final e:Lcom/twitter/library/api/ao;

.field private f:J


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V
    .locals 1

    const-class v0, Liz;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Ljb;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    const/16 v0, 0x11

    invoke-static {v0}, Lcom/twitter/library/api/ao;->a(I)Lcom/twitter/library/api/ao;

    move-result-object v0

    iput-object v0, p0, Liz;->e:Lcom/twitter/library/api/ao;

    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/library/service/e;)Lcom/twitter/internal/network/HttpOperation;
    .locals 6

    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Liz;->m:Lcom/twitter/library/network/aa;

    iget-object v1, v1, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "/1.1/mutes/users/create.json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0, v1}, Liz;->a(Ljava/util/ArrayList;)V

    iget-wide v2, p0, Liz;->f:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v3, "expiry"

    iget-wide v4, p0, Liz;->f:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    invoke-virtual {p0}, Liz;->s()Lcom/twitter/library/service/p;

    move-result-object v2

    new-instance v3, Lcom/twitter/library/network/d;

    iget-object v4, p0, Liz;->l:Landroid/content/Context;

    invoke-direct {v3, v4, v0}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    iget-wide v4, v2, Lcom/twitter/library/service/p;->c:J

    invoke-virtual {v3, v4, v5}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v0

    new-instance v3, Lcom/twitter/library/network/n;

    iget-object v2, v2, Lcom/twitter/library/service/p;->d:Lcom/twitter/library/network/OAuthToken;

    invoke-direct {v3, v2}, Lcom/twitter/library/network/n;-><init>(Lcom/twitter/library/network/OAuthToken;)V

    invoke-virtual {v0, v3}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v0

    sget-object v2, Lcom/twitter/internal/network/HttpOperation$RequestMethod;->b:Lcom/twitter/internal/network/HttpOperation$RequestMethod;

    invoke-virtual {v0, v2}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/HttpOperation$RequestMethod;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/twitter/library/network/d;->a(Ljava/util/ArrayList;)Lcom/twitter/library/network/d;

    move-result-object v0

    iget-object v1, p0, Liz;->e:Lcom/twitter/library/api/ao;

    invoke-virtual {v0, v1}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Lcom/twitter/internal/network/HttpOperation;Lcom/twitter/library/service/e;)V
    .locals 11

    const/4 v7, 0x0

    const/16 v6, 0x2000

    const/4 v9, 0x1

    invoke-virtual {p1}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Liz;->e:Lcom/twitter/library/api/ao;

    invoke-virtual {v0}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/twitter/library/api/TwitterUser;

    invoke-virtual {p0}, Liz;->w()Lcom/twitter/library/provider/az;

    move-result-object v0

    invoke-virtual {p0}, Liz;->s()Lcom/twitter/library/service/p;

    move-result-object v1

    iget-wide v2, v1, Lcom/twitter/library/service/p;->c:J

    iget-wide v4, v10, Lcom/twitter/library/api/TwitterUser;->userId:J

    invoke-virtual {v0, v4, v5, v6}, Lcom/twitter/library/provider/az;->d(JI)V

    iget v1, v10, Lcom/twitter/library/api/TwitterUser;->friendship:I

    invoke-static {v1, v6}, Lcom/twitter/library/provider/ay;->a(II)I

    move-result v1

    iput v1, v10, Lcom/twitter/library/api/TwitterUser;->friendship:I

    new-array v1, v9, [Lcom/twitter/library/api/TwitterUser;

    const/4 v4, 0x0

    aput-object v10, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    const/16 v4, 0x1a

    const-wide/16 v5, -0x1

    move-object v8, v7

    invoke-virtual/range {v0 .. v9}, Lcom/twitter/library/provider/az;->a(Ljava/util/Collection;JIJLjava/lang/String;Ljava/lang/String;Z)I

    iget-object v0, p2, Lcom/twitter/library/service/e;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "muted_username"

    iget-object v2, v10, Lcom/twitter/library/api/TwitterUser;->username:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p2, p1}, Lcom/twitter/library/service/e;->a(Lcom/twitter/internal/network/HttpOperation;)V

    return-void
.end method
