.class public Lhm;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Landroid/app/Activity;


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lhm;->a:Landroid/app/Activity;

    return-void
.end method


# virtual methods
.method public a(ILcom/twitter/internal/android/widget/ToolBar;)V
    .locals 20

    const/4 v3, 0x0

    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lhm;->a:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    move/from16 v0, p1

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;

    move-result-object v3

    invoke-static {v3}, Landroid/util/Xml;->asAttributeSet(Lorg/xmlpull/v1/XmlPullParser;)Landroid/util/AttributeSet;

    move-result-object v5

    invoke-interface {v3}, Landroid/content/res/XmlResourceParser;->getEventType()I

    move-result v4

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    const/4 v2, 0x0

    :goto_0
    if-nez v2, :cond_5

    packed-switch v4, :pswitch_data_0

    :cond_0
    :goto_1
    invoke-interface {v3}, Landroid/content/res/XmlResourceParser;->next()I

    move-result v4

    goto :goto_0

    :pswitch_0
    invoke-interface {v3}, Landroid/content/res/XmlResourceParser;->getName()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v7, "item"

    invoke-virtual {v4, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    move-object/from16 v0, p0

    iget-object v4, v0, Lhm;->a:Landroid/app/Activity;

    sget-object v7, Lcom/twitter/internal/android/f;->ToolBarItem:[I

    invoke-virtual {v4, v5, v7}, Landroid/app/Activity;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v7

    const/4 v4, 0x2

    const/4 v8, 0x0

    invoke-virtual {v7, v4, v8}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v8

    if-eqz v8, :cond_0

    const/4 v4, 0x0

    const/4 v9, 0x0

    invoke-virtual {v7, v4, v9}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v9

    const/4 v4, 0x4

    invoke-virtual {v7, v4}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v10

    if-nez v9, :cond_2

    if-nez v10, :cond_2

    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v4, "ToolBar item requires either icon or title."

    invoke-direct {v2, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catch_0
    move-exception v2

    :try_start_1
    new-instance v4, Landroid/view/InflateException;

    const-string/jumbo v5, "Error inflating menu XML"

    invoke-direct {v4, v5, v2}, Landroid/view/InflateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v2

    if-eqz v3, :cond_1

    invoke-interface {v3}, Landroid/content/res/XmlResourceParser;->close()V

    :cond_1
    throw v2

    :cond_2
    const/16 v4, 0xa

    const/4 v11, 0x0

    :try_start_2
    invoke-virtual {v7, v4, v11}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v11

    const/16 v4, 0xc

    const/4 v12, 0x1

    invoke-virtual {v7, v4, v12}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v12

    const/16 v4, 0xb

    const/4 v13, 0x0

    invoke-virtual {v7, v4, v13}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v13

    const/4 v4, 0x1

    const/4 v14, 0x1

    invoke-virtual {v7, v4, v14}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v14

    const/4 v4, 0x3

    const/4 v15, 0x1

    invoke-virtual {v7, v4, v15}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v15

    const/16 v4, 0x8

    invoke-virtual {v7, v4}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v16

    const/16 v4, 0x9

    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v7, v4, v0}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v17

    const/4 v4, 0x6

    invoke-virtual {v7, v4}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v4, 0x6

    const/16 v18, -0x1

    move/from16 v0, v18

    invoke-virtual {v7, v4, v0}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v4

    if-gtz v4, :cond_4

    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v4, "ToolBar item order must be greater than 0"

    invoke-direct {v2, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_2
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catch_1
    move-exception v2

    :try_start_3
    new-instance v4, Landroid/view/InflateException;

    const-string/jumbo v5, "Error inflating menu XML"

    invoke-direct {v4, v5, v2}, Landroid/view/InflateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_3
    const/4 v4, -0x1

    :cond_4
    const/16 v18, 0x7

    const v19, 0x7fffffff

    :try_start_4
    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v7, v0, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v18

    new-instance v19, Lhn;

    move-object/from16 v0, v19

    move-object/from16 v1, p2

    invoke-direct {v0, v1, v11}, Lhn;-><init>(Lcom/twitter/internal/android/widget/ToolBar;Z)V

    move-object/from16 v0, v19

    invoke-virtual {v0, v8}, Lhn;->a(I)Lhn;

    move-result-object v8

    invoke-virtual {v8, v9}, Lhn;->b(I)Lhn;

    move-result-object v8

    move/from16 v0, v17

    invoke-virtual {v8, v0}, Lhn;->g(I)Lhn;

    move-result-object v8

    invoke-virtual {v8, v4}, Lhn;->h(I)Lhn;

    move-result-object v4

    move/from16 v0, v18

    invoke-virtual {v4, v0}, Lhn;->i(I)Lhn;

    move-result-object v4

    invoke-virtual {v4, v12}, Lhn;->d(I)Lhn;

    move-result-object v4

    invoke-virtual {v4, v13}, Lhn;->c(I)Lhn;

    move-result-object v4

    invoke-virtual {v4, v10}, Lhn;->a(Ljava/lang/CharSequence;)Lhn;

    move-result-object v4

    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Lhn;->c(Ljava/lang/CharSequence;)Lhn;

    move-result-object v4

    invoke-virtual {v4, v14}, Lhn;->c(Z)Lhn;

    move-result-object v4

    invoke-virtual {v4, v15}, Lhn;->b(Z)Lhn;

    move-result-object v4

    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v7}, Landroid/content/res/TypedArray;->recycle()V

    goto/16 :goto_1

    :pswitch_1
    const/4 v2, 0x1

    goto/16 :goto_1

    :cond_5
    move-object/from16 v0, p2

    invoke-virtual {v0, v6}, Lcom/twitter/internal/android/widget/ToolBar;->a(Ljava/util/Collection;)V
    :try_end_4
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    if-eqz v3, :cond_6

    invoke-interface {v3}, Landroid/content/res/XmlResourceParser;->close()V

    :cond_6
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
