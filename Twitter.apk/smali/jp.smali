.class public Ljp;
.super Lcom/twitter/library/service/o;
.source "Twttr"


# instance fields
.field private final d:I

.field private final e:[J


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;ILjava/util/Collection;)V
    .locals 1

    const-class v0, Ljp;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/service/o;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    iput p3, p0, Ljp;->d:I

    invoke-static {p4}, Lcom/twitter/library/util/Util;->b(Ljava/util/Collection;)[J

    move-result-object v0

    iput-object v0, p0, Ljp;->e:[J

    return-void
.end method


# virtual methods
.method protected b(Lcom/twitter/library/service/e;)V
    .locals 9

    const/4 v8, 0x0

    invoke-virtual {p0}, Ljp;->s()Lcom/twitter/library/service/p;

    move-result-object v1

    iget-object v7, v1, Lcom/twitter/library/service/p;->e:Ljava/lang/String;

    iget v3, p0, Ljp;->d:I

    invoke-virtual {p0}, Ljp;->w()Lcom/twitter/library/provider/az;

    move-result-object v0

    iget-object v6, p0, Ljp;->e:[J

    iget-wide v1, v1, Lcom/twitter/library/service/p;->c:J

    const-wide/16 v4, -0x1

    invoke-virtual/range {v0 .. v6}, Lcom/twitter/library/provider/az;->a(JIJ[J)V

    invoke-static {v3}, Lcom/twitter/library/provider/ar;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0, v3, v6}, Lcom/twitter/library/provider/az;->b(I[J)V

    :cond_0
    if-nez v6, :cond_1

    sparse-switch v3, :sswitch_data_0

    :cond_1
    :goto_0
    return-void

    :sswitch_0
    iget-object v0, p0, Ljp;->l:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/library/provider/f;->a(Landroid/content/Context;)Lcom/twitter/library/provider/f;

    move-result-object v0

    const-string/jumbo v1, "tweet"

    invoke-virtual {v0, v7, v1, v8}, Lcom/twitter/library/provider/f;->b(Ljava/lang/String;Ljava/lang/String;I)I

    goto :goto_0

    :sswitch_1
    const/4 v1, 0x0

    invoke-virtual {v0, v8, v1}, Lcom/twitter/library/provider/az;->b(I[J)V

    iget-object v0, p0, Ljp;->l:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/library/provider/f;->a(Landroid/content/Context;)Lcom/twitter/library/provider/f;

    move-result-object v0

    const-string/jumbo v1, "mention"

    invoke-virtual {v0, v7, v1, v8}, Lcom/twitter/library/provider/f;->b(Ljava/lang/String;Ljava/lang/String;I)I

    const-string/jumbo v1, "unread_interactions"

    invoke-virtual {v0, v7, v1, v8}, Lcom/twitter/library/provider/f;->b(Ljava/lang/String;Ljava/lang/String;I)I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x5 -> :sswitch_1
    .end sparse-switch
.end method
