.class Llg;
.super Ljava/io/FilterOutputStream;
.source "Twttr"


# instance fields
.field final synthetic a:Llf;


# direct methods
.method private constructor <init>(Llf;Ljava/io/OutputStream;)V
    .locals 0

    iput-object p1, p0, Llg;->a:Llf;

    invoke-direct {p0, p2}, Ljava/io/FilterOutputStream;-><init>(Ljava/io/OutputStream;)V

    return-void
.end method

.method synthetic constructor <init>(Llf;Ljava/io/OutputStream;Lle;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Llg;-><init>(Llf;Ljava/io/OutputStream;)V

    return-void
.end method


# virtual methods
.method public close()V
    .locals 3

    :try_start_0
    iget-object v0, p0, Llg;->out:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    iget-object v1, p0, Llg;->a:Llf;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Llf;->a(Llf;Z)Z

    throw v0
.end method

.method public flush()V
    .locals 3

    :try_start_0
    iget-object v0, p0, Llg;->out:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    iget-object v1, p0, Llg;->a:Llf;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Llf;->a(Llf;Z)Z

    throw v0
.end method

.method public write(I)V
    .locals 3

    :try_start_0
    iget-object v0, p0, Llg;->out:Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    iget-object v1, p0, Llg;->a:Llf;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Llf;->a(Llf;Z)Z

    throw v0
.end method

.method public write([BII)V
    .locals 3

    :try_start_0
    iget-object v0, p0, Llg;->out:Ljava/io/OutputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/OutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    iget-object v1, p0, Llg;->a:Llf;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Llf;->a(Llf;Z)Z

    throw v0
.end method
