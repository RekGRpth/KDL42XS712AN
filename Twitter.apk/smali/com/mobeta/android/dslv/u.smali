.class public final Lcom/mobeta/android/dslv/u;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field public static final DragSortListView:[I

.field public static final DragSortListView_click_remove_id:I = 0x10

.field public static final DragSortListView_collapsed_height:I = 0x0

.field public static final DragSortListView_drag_enabled:I = 0xa

.field public static final DragSortListView_drag_handle_id:I = 0xe

.field public static final DragSortListView_drag_scroll_start:I = 0x1

.field public static final DragSortListView_drag_start_mode:I = 0xd

.field public static final DragSortListView_drop_animation_duration:I = 0x9

.field public static final DragSortListView_fling_handle_id:I = 0xf

.field public static final DragSortListView_float_alpha:I = 0x6

.field public static final DragSortListView_float_background_color:I = 0x3

.field public static final DragSortListView_max_drag_scroll_speed:I = 0x2

.field public static final DragSortListView_remove_animation_duration:I = 0x8

.field public static final DragSortListView_remove_enabled:I = 0xc

.field public static final DragSortListView_remove_mode:I = 0x4

.field public static final DragSortListView_slide_shuffle_speed:I = 0x7

.field public static final DragSortListView_sort_enabled:I = 0xb

.field public static final DragSortListView_track_drag_sort:I = 0x5

.field public static final DragSortListView_use_default_controller:I = 0x11


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x12

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/mobeta/android/dslv/u;->DragSortListView:[I

    return-void

    :array_0
    .array-data 4
        0x7f01008a    # com.twitter.android.R.attr.collapsed_height
        0x7f01008b    # com.twitter.android.R.attr.drag_scroll_start
        0x7f01008c    # com.twitter.android.R.attr.max_drag_scroll_speed
        0x7f01008d    # com.twitter.android.R.attr.float_background_color
        0x7f01008e    # com.twitter.android.R.attr.remove_mode
        0x7f01008f    # com.twitter.android.R.attr.track_drag_sort
        0x7f010090    # com.twitter.android.R.attr.float_alpha
        0x7f010091    # com.twitter.android.R.attr.slide_shuffle_speed
        0x7f010092    # com.twitter.android.R.attr.remove_animation_duration
        0x7f010093    # com.twitter.android.R.attr.drop_animation_duration
        0x7f010094    # com.twitter.android.R.attr.drag_enabled
        0x7f010095    # com.twitter.android.R.attr.sort_enabled
        0x7f010096    # com.twitter.android.R.attr.remove_enabled
        0x7f010097    # com.twitter.android.R.attr.drag_start_mode
        0x7f010098    # com.twitter.android.R.attr.drag_handle_id
        0x7f010099    # com.twitter.android.R.attr.fling_handle_id
        0x7f01009a    # com.twitter.android.R.attr.click_remove_id
        0x7f01009b    # com.twitter.android.R.attr.use_default_controller
    .end array-data
.end method
