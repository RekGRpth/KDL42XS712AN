.class public abstract Lcom/twitter/internal/android/service/a;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field c:Lcom/twitter/internal/android/service/l;

.field private d:Lcom/twitter/internal/android/service/m;

.field private e:Lcom/twitter/internal/android/service/k;

.field private f:I


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x6

    invoke-static {v0}, Lcom/twitter/internal/util/j;->a(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/internal/android/service/a;->a:Ljava/lang/String;

    iput-object p1, p0, Lcom/twitter/internal/android/service/a;->b:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final C_()I
    .locals 1

    iget v0, p0, Lcom/twitter/internal/android/service/a;->f:I

    return v0
.end method

.method public final D_()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/internal/android/service/a;->d:Lcom/twitter/internal/android/service/m;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/twitter/internal/android/service/k;)Lcom/twitter/internal/android/service/a;
    .locals 0

    iput-object p1, p0, Lcom/twitter/internal/android/service/a;->e:Lcom/twitter/internal/android/service/k;

    return-object p0
.end method

.method public final a(Lcom/twitter/internal/android/service/l;)Lcom/twitter/internal/android/service/a;
    .locals 0

    iput-object p1, p0, Lcom/twitter/internal/android/service/a;->c:Lcom/twitter/internal/android/service/l;

    return-object p0
.end method

.method public final a(Lcom/twitter/internal/android/service/m;)Lcom/twitter/internal/android/service/a;
    .locals 0

    iput-object p1, p0, Lcom/twitter/internal/android/service/a;->d:Lcom/twitter/internal/android/service/m;

    return-object p0
.end method

.method protected abstract b(Lcom/twitter/internal/android/service/AsyncService;)Ljava/lang/Object;
.end method

.method public final c()Lcom/twitter/internal/android/service/k;
    .locals 1

    iget-object v0, p0, Lcom/twitter/internal/android/service/a;->e:Lcom/twitter/internal/android/service/k;

    return-object v0
.end method

.method public final c(Lcom/twitter/internal/android/service/AsyncService;)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/internal/android/service/a;->b(Lcom/twitter/internal/android/service/AsyncService;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final d()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/internal/android/service/a;->d:Lcom/twitter/internal/android/service/m;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/android/service/a;->d:Lcom/twitter/internal/android/service/m;

    invoke-interface {v0, p0}, Lcom/twitter/internal/android/service/m;->a(Lcom/twitter/internal/android/service/a;)V

    :cond_0
    return-void
.end method
