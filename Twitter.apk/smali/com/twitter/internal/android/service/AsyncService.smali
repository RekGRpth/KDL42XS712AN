.class public Lcom/twitter/internal/android/service/AsyncService;
.super Landroid/app/Service;
.source "Twttr"


# static fields
.field static final a:Ljava/util/concurrent/atomic/AtomicLong;


# instance fields
.field private final b:Landroid/os/Handler;

.field private final c:Landroid/os/IBinder;

.field private d:Lcom/twitter/internal/android/service/g;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v1, 0x0

    invoke-direct {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    sput-object v0, Lcom/twitter/internal/android/service/AsyncService;->a:Ljava/util/concurrent/atomic/AtomicLong;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/twitter/internal/android/service/AsyncService;->b:Landroid/os/Handler;

    new-instance v0, Lcom/twitter/internal/android/service/i;

    invoke-direct {v0, p0}, Lcom/twitter/internal/android/service/i;-><init>(Lcom/twitter/internal/android/service/AsyncService;)V

    iput-object v0, p0, Lcom/twitter/internal/android/service/AsyncService;->c:Landroid/os/IBinder;

    return-void
.end method

.method static synthetic a(Lcom/twitter/internal/android/service/AsyncService;)Lcom/twitter/internal/android/service/g;
    .locals 1

    iget-object v0, p0, Lcom/twitter/internal/android/service/AsyncService;->d:Lcom/twitter/internal/android/service/g;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/internal/android/service/AsyncService;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/twitter/internal/android/service/AsyncService;->b:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public a(Lcom/twitter/internal/android/service/a;)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/internal/android/service/AsyncService;->d:Lcom/twitter/internal/android/service/g;

    new-instance v1, Lcom/twitter/internal/android/service/d;

    new-instance v2, Lcom/twitter/internal/android/service/k;

    invoke-direct {v2}, Lcom/twitter/internal/android/service/k;-><init>()V

    invoke-direct {v1, p0, p1, v2}, Lcom/twitter/internal/android/service/d;-><init>(Lcom/twitter/internal/android/service/AsyncService;Lcom/twitter/internal/android/service/a;Lcom/twitter/internal/android/service/k;)V

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/service/g;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    iget-object v0, p0, Lcom/twitter/internal/android/service/AsyncService;->c:Landroid/os/IBinder;

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    new-instance v0, Lcom/twitter/internal/android/service/g;

    const/4 v1, 0x5

    invoke-direct {v0, v1}, Lcom/twitter/internal/android/service/g;-><init>(I)V

    iput-object v0, p0, Lcom/twitter/internal/android/service/AsyncService;->d:Lcom/twitter/internal/android/service/g;

    return-void
.end method

.method public onDestroy()V
    .locals 1

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    iget-object v0, p0, Lcom/twitter/internal/android/service/AsyncService;->d:Lcom/twitter/internal/android/service/g;

    invoke-virtual {v0}, Lcom/twitter/internal/android/service/g;->shutdownNow()Ljava/util/List;

    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "By default, AsyncService is strictly a bound service and does not support starting with Context.startService()"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
