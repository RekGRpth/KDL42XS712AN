.class Lcom/twitter/internal/android/service/e;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/util/Comparator;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private b(Ljava/lang/Runnable;Ljava/lang/Runnable;)I
    .locals 4

    check-cast p1, Lcom/twitter/internal/android/service/f;

    iget-wide v0, p1, Lcom/twitter/internal/android/service/f;->b:J

    check-cast p2, Lcom/twitter/internal/android/service/f;

    iget-wide v2, p2, Lcom/twitter/internal/android/service/f;->b:J

    sub-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method


# virtual methods
.method public a(Ljava/lang/Runnable;Ljava/lang/Runnable;)I
    .locals 2

    move-object v0, p1

    check-cast v0, Lcom/twitter/internal/android/service/f;

    iget v1, v0, Lcom/twitter/internal/android/service/f;->a:I

    move-object v0, p2

    check-cast v0, Lcom/twitter/internal/android/service/f;

    iget v0, v0, Lcom/twitter/internal/android/service/f;->a:I

    if-ne v1, v0, :cond_0

    invoke-direct {p0, p1, p2}, Lcom/twitter/internal/android/service/e;->b(Ljava/lang/Runnable;Ljava/lang/Runnable;)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    sub-int v0, v1, v0

    goto :goto_0
.end method

.method public synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Ljava/lang/Runnable;

    check-cast p2, Ljava/lang/Runnable;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/internal/android/service/e;->a(Ljava/lang/Runnable;Ljava/lang/Runnable;)I

    move-result v0

    return v0
.end method
