.class public Lcom/twitter/internal/android/service/j;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/content/ServiceConnection;


# instance fields
.field private a:Lcom/twitter/internal/android/service/AsyncService;

.field private final b:Ljava/util/HashMap;

.field private final c:Ljava/util/HashMap;

.field private final d:[I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/internal/android/service/j;->b:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/internal/android/service/j;->c:Ljava/util/HashMap;

    const/4 v0, 0x0

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/twitter/internal/android/service/j;->d:[I

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/internal/android/service/a;)Ljava/lang/String;
    .locals 3

    iget-object v0, p1, Lcom/twitter/internal/android/service/a;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/twitter/internal/android/service/j;->d:[I

    monitor-enter v1

    :try_start_0
    iget-object v2, p0, Lcom/twitter/internal/android/service/j;->a:Lcom/twitter/internal/android/service/AsyncService;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/twitter/internal/android/service/j;->c:Ljava/util/HashMap;

    invoke-virtual {v2, v0, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, p0, Lcom/twitter/internal/android/service/j;->a:Lcom/twitter/internal/android/service/AsyncService;

    invoke-virtual {v2, p1}, Lcom/twitter/internal/android/service/AsyncService;->a(Lcom/twitter/internal/android/service/a;)V

    :goto_0
    monitor-exit v1

    return-object v0

    :cond_0
    iget-object v2, p0, Lcom/twitter/internal/android/service/j;->b:Ljava/util/HashMap;

    invoke-virtual {v2, v0, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/internal/android/service/j;->a:Lcom/twitter/internal/android/service/AsyncService;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)Z
    .locals 2

    iget-object v1, p0, Lcom/twitter/internal/android/service/j;->d:[I

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/twitter/internal/android/service/j;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public b()V
    .locals 2

    iget-object v1, p0, Lcom/twitter/internal/android/service/j;->d:[I

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/twitter/internal/android/service/j;->c:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public b(Ljava/lang/String;)V
    .locals 2

    iget-object v1, p0, Lcom/twitter/internal/android/service/j;->d:[I

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/twitter/internal/android/service/j;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 4

    iget-object v1, p0, Lcom/twitter/internal/android/service/j;->d:[I

    monitor-enter v1

    :try_start_0
    check-cast p2, Lcom/twitter/internal/android/service/i;

    invoke-virtual {p2}, Lcom/twitter/internal/android/service/i;->a()Lcom/twitter/internal/android/service/AsyncService;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/internal/android/service/j;->a:Lcom/twitter/internal/android/service/AsyncService;

    iget-object v0, p0, Lcom/twitter/internal/android/service/j;->c:Ljava/util/HashMap;

    iget-object v2, p0, Lcom/twitter/internal/android/service/j;->b:Ljava/util/HashMap;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    iget-object v0, p0, Lcom/twitter/internal/android/service/j;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/service/a;

    iget-object v3, p0, Lcom/twitter/internal/android/service/j;->a:Lcom/twitter/internal/android/service/AsyncService;

    invoke-virtual {v3, v0}, Lcom/twitter/internal/android/service/AsyncService;->a(Lcom/twitter/internal/android/service/a;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/twitter/internal/android/service/j;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2

    iget-object v1, p0, Lcom/twitter/internal/android/service/j;->d:[I

    monitor-enter v1

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/twitter/internal/android/service/j;->a:Lcom/twitter/internal/android/service/AsyncService;

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
