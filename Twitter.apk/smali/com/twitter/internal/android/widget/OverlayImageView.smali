.class public Lcom/twitter/internal/android/widget/OverlayImageView;
.super Landroid/widget/ImageView;
.source "Twttr"


# instance fields
.field private a:Z

.field private b:Landroid/graphics/drawable/Drawable;

.field private c:I

.field private d:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/twitter/internal/android/widget/OverlayImageView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-direct {p0, p1, p2}, Lcom/twitter/internal/android/widget/OverlayImageView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-direct {p0, p1, p2}, Lcom/twitter/internal/android/widget/OverlayImageView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    if-nez p2, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/twitter/internal/android/f;->OverlayImageView:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p0, v1}, Lcom/twitter/internal/android/widget/OverlayImageView;->setOverlayDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_1
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    goto :goto_0
.end method

.method private setDrawableBounds(Landroid/graphics/drawable/Drawable;)V
    .locals 3

    const/4 v2, 0x0

    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/OverlayImageView;->a:Z

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/OverlayImageView;->getMeasuredWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/OverlayImageView;->getMeasuredHeight()I

    move-result v1

    invoke-virtual {p1, v2, v2, v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/OverlayImageView;->invalidate()V

    :cond_0
    return-void
.end method


# virtual methods
.method public a(II)V
    .locals 2

    iget v0, p0, Lcom/twitter/internal/android/widget/OverlayImageView;->c:I

    iget v1, p0, Lcom/twitter/internal/android/widget/OverlayImageView;->d:I

    if-ne v0, p1, :cond_0

    if-eq v1, p2, :cond_1

    :cond_0
    iput p1, p0, Lcom/twitter/internal/android/widget/OverlayImageView;->c:I

    iput p2, p0, Lcom/twitter/internal/android/widget/OverlayImageView;->d:I

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/OverlayImageView;->requestLayout()V

    :cond_1
    return-void
.end method

.method protected drawableStateChanged()V
    .locals 2

    invoke-super {p0}, Landroid/widget/ImageView;->drawableStateChanged()V

    iget-object v0, p0, Lcom/twitter/internal/android/widget/OverlayImageView;->b:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/OverlayImageView;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    :cond_0
    return-void
.end method

.method public invalidateDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/internal/android/widget/OverlayImageView;->b:Landroid/graphics/drawable/Drawable;

    if-ne p1, v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/OverlayImageView;->invalidate()V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/ImageView;->invalidateDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/widget/ImageView;->onDraw(Landroid/graphics/Canvas;)V

    iget-object v0, p0, Lcom/twitter/internal/android/widget/OverlayImageView;->b:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 6

    iget v2, p0, Lcom/twitter/internal/android/widget/OverlayImageView;->c:I

    iget v3, p0, Lcom/twitter/internal/android/widget/OverlayImageView;->d:I

    if-lez v2, :cond_2

    if-lez v3, :cond_2

    invoke-static {v2, p1}, Lcom/twitter/internal/android/widget/OverlayImageView;->getDefaultSize(II)I

    move-result v1

    invoke-static {v3, p2}, Lcom/twitter/internal/android/widget/OverlayImageView;->getDefaultSize(II)I

    move-result v0

    mul-int v4, v2, v0

    mul-int v5, v3, v1

    if-le v4, v5, :cond_1

    div-int v0, v5, v2

    :cond_0
    :goto_0
    invoke-virtual {p0, v1, v0}, Lcom/twitter/internal/android/widget/OverlayImageView;->setMeasuredDimension(II)V

    :goto_1
    iget-object v0, p0, Lcom/twitter/internal/android/widget/OverlayImageView;->b:Landroid/graphics/drawable/Drawable;

    invoke-direct {p0, v0}, Lcom/twitter/internal/android/widget/OverlayImageView;->setDrawableBounds(Landroid/graphics/drawable/Drawable;)V

    return-void

    :cond_1
    if-ge v4, v5, :cond_0

    div-int v1, v4, v3

    goto :goto_0

    :cond_2
    invoke-super {p0, p1, p2}, Landroid/widget/ImageView;->onMeasure(II)V

    goto :goto_1
.end method

.method protected setFrame(IIII)Z
    .locals 2

    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ImageView;->setFrame(IIII)Z

    move-result v0

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/twitter/internal/android/widget/OverlayImageView;->a:Z

    iget-object v1, p0, Lcom/twitter/internal/android/widget/OverlayImageView;->b:Landroid/graphics/drawable/Drawable;

    invoke-direct {p0, v1}, Lcom/twitter/internal/android/widget/OverlayImageView;->setDrawableBounds(Landroid/graphics/drawable/Drawable;)V

    return v0
.end method

.method public setOverlayDrawable(I)V
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/OverlayImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/internal/android/widget/OverlayImageView;->setOverlayDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method public setOverlayDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/internal/android/widget/OverlayImageView;->b:Landroid/graphics/drawable/Drawable;

    if-eq v0, p1, :cond_2

    iget-object v0, p0, Lcom/twitter/internal/android/widget/OverlayImageView;->b:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/OverlayImageView;->b:Landroid/graphics/drawable/Drawable;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    iget-object v0, p0, Lcom/twitter/internal/android/widget/OverlayImageView;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v0}, Lcom/twitter/internal/android/widget/OverlayImageView;->unscheduleDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    iput-object p1, p0, Lcom/twitter/internal/android/widget/OverlayImageView;->b:Landroid/graphics/drawable/Drawable;

    if-eqz p1, :cond_1

    invoke-virtual {p1, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/OverlayImageView;->getDrawableState()[I

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/OverlayImageView;->requestLayout()V

    :cond_2
    return-void
.end method
