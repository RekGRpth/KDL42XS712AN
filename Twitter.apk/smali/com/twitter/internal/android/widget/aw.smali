.class Lcom/twitter/internal/android/widget/aw;
.super Landroid/support/v4/widget/ExploreByTouchHelper;
.source "Twttr"


# instance fields
.field a:I

.field b:I

.field c:I

.field d:Ljava/lang/CharSequence;

.field final e:Ljava/lang/String;

.field private f:Lcom/twitter/internal/android/widget/ToolBarHomeView;


# direct methods
.method public constructor <init>(Lcom/twitter/internal/android/widget/ToolBarHomeView;Landroid/content/res/TypedArray;)V
    .locals 1

    invoke-direct {p0, p1}, Landroid/support/v4/widget/ExploreByTouchHelper;-><init>(Landroid/view/View;)V

    iput-object p1, p0, Lcom/twitter/internal/android/widget/aw;->f:Lcom/twitter/internal/android/widget/ToolBarHomeView;

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/twitter/internal/android/widget/aw;->d:Ljava/lang/CharSequence;

    const/4 v0, 0x6

    invoke-virtual {p2, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    :goto_0
    iput-object v0, p0, Lcom/twitter/internal/android/widget/aw;->e:Ljava/lang/String;

    return-void

    :cond_0
    const-string/jumbo v0, ""

    goto :goto_0
.end method

.method private a(I)Z
    .locals 1

    if-ltz p1, :cond_0

    const/4 v0, 0x2

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(I)Ljava/lang/CharSequence;
    .locals 1

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/aw;->f:Lcom/twitter/internal/android/widget/ToolBarHomeView;

    invoke-static {v0}, Lcom/twitter/internal/android/widget/ToolBarHomeView;->b(Lcom/twitter/internal/android/widget/ToolBarHomeView;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/aw;->e:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/internal/android/widget/aw;->d:Ljava/lang/CharSequence;

    goto :goto_0
.end method

.method private c(I)Landroid/graphics/Rect;
    .locals 5

    const/4 v4, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/aw;->f:Lcom/twitter/internal/android/widget/ToolBarHomeView;

    invoke-static {v0}, Lcom/twitter/internal/android/widget/ToolBarHomeView;->b(Lcom/twitter/internal/android/widget/ToolBarHomeView;)Z

    move-result v3

    if-eq p1, v4, :cond_0

    if-nez v3, :cond_1

    :cond_0
    move v1, v2

    :goto_0
    if-ne p1, v4, :cond_2

    if-eqz v3, :cond_2

    iget-object v0, p0, Lcom/twitter/internal/android/widget/aw;->f:Lcom/twitter/internal/android/widget/ToolBarHomeView;

    invoke-static {v0}, Lcom/twitter/internal/android/widget/ToolBarHomeView;->a(Lcom/twitter/internal/android/widget/ToolBarHomeView;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/twitter/internal/android/widget/aw;->a:I

    :goto_1
    new-instance v3, Landroid/graphics/Rect;

    iget v4, p0, Lcom/twitter/internal/android/widget/aw;->c:I

    invoke-direct {v3, v1, v2, v0, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v3

    :cond_1
    iget v0, p0, Lcom/twitter/internal/android/widget/aw;->a:I

    move v1, v0

    goto :goto_0

    :cond_2
    iget v0, p0, Lcom/twitter/internal/android/widget/aw;->b:I

    iget v3, p0, Lcom/twitter/internal/android/widget/aw;->a:I

    add-int/2addr v0, v3

    goto :goto_1
.end method


# virtual methods
.method protected getVirtualViewAt(FF)I
    .locals 1

    iget v0, p0, Lcom/twitter/internal/android/widget/aw;->a:I

    int-to-float v0, v0

    cmpg-float v0, p1, v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/aw;->f:Lcom/twitter/internal/android/widget/ToolBarHomeView;

    invoke-static {v0}, Lcom/twitter/internal/android/widget/ToolBarHomeView;->a(Lcom/twitter/internal/android/widget/ToolBarHomeView;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected getVisibleVirtualViews(Ljava/util/List;)V
    .locals 1

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method protected onPerformActionForVirtualView(IILandroid/os/Bundle;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected onPopulateEventForVirtualView(ILandroid/view/accessibility/AccessibilityEvent;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/twitter/internal/android/widget/aw;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, p1}, Lcom/twitter/internal/android/widget/aw;->b(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/view/accessibility/AccessibilityEvent;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method protected onPopulateNodeForVirtualView(ILandroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V
    .locals 2

    invoke-direct {p0, p1}, Lcom/twitter/internal/android/widget/aw;->a(I)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0, p1}, Lcom/twitter/internal/android/widget/aw;->b(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->setContentDescription(Ljava/lang/CharSequence;)V

    const/16 v0, 0x10

    invoke-virtual {p2, v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->addAction(I)V

    invoke-direct {p0, p1}, Lcom/twitter/internal/android/widget/aw;->c(I)Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p2, v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->setBoundsInParent(Landroid/graphics/Rect;)V

    goto :goto_0
.end method
